package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.n.n1.c;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.j;
import d0.e0.p.d.m0.n.n1.k;
import d0.e0.p.d.m0.n.n1.l;
import d0.e0.p.d.m0.n.n1.n;
import d0.z.d.m;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Set;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: AbstractTypeChecker.kt */
/* loaded from: classes3.dex */
public abstract class f implements n {
    public int a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f3495b;
    public ArrayDeque<i> c;
    public Set<i> d;

    /* compiled from: AbstractTypeChecker.kt */
    /* loaded from: classes3.dex */
    public enum a {
        CHECK_ONLY_LOWER,
        CHECK_SUBTYPE_AND_LOWER,
        SKIP_LOWER
    }

    /* compiled from: AbstractTypeChecker.kt */
    /* loaded from: classes3.dex */
    public static abstract class b {

        /* compiled from: AbstractTypeChecker.kt */
        /* loaded from: classes3.dex */
        public static abstract class a extends b {
            public a() {
                super(null);
            }
        }

        /* compiled from: AbstractTypeChecker.kt */
        /* renamed from: d0.e0.p.d.m0.n.f$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0358b extends b {
            public static final C0358b a = new C0358b();

            public C0358b() {
                super(null);
            }

            @Override // d0.e0.p.d.m0.n.f.b
            public i transformType(f fVar, h hVar) {
                m.checkNotNullParameter(fVar, "context");
                m.checkNotNullParameter(hVar, "type");
                return fVar.lowerBoundIfFlexible(hVar);
            }
        }

        /* compiled from: AbstractTypeChecker.kt */
        /* loaded from: classes3.dex */
        public static final class c extends b {
            public static final c a = new c();

            public c() {
                super(null);
            }

            @Override // d0.e0.p.d.m0.n.f.b
            public Void transformType(f fVar, h hVar) {
                m.checkNotNullParameter(fVar, "context");
                m.checkNotNullParameter(hVar, "type");
                throw new UnsupportedOperationException("Should not be called");
            }
        }

        /* compiled from: AbstractTypeChecker.kt */
        /* loaded from: classes3.dex */
        public static final class d extends b {
            public static final d a = new d();

            public d() {
                super(null);
            }

            @Override // d0.e0.p.d.m0.n.f.b
            public i transformType(f fVar, h hVar) {
                m.checkNotNullParameter(fVar, "context");
                m.checkNotNullParameter(hVar, "type");
                return fVar.upperBoundIfFlexible(hVar);
            }
        }

        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public abstract i transformType(f fVar, h hVar);
    }

    public static /* synthetic */ Boolean addSubtypeConstraint$default(f fVar, h hVar, h hVar2, boolean z2, int i, Object obj) {
        if (obj == null) {
            if ((i & 4) != 0) {
                z2 = false;
            }
            return fVar.addSubtypeConstraint(hVar, hVar2, z2);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: addSubtypeConstraint");
    }

    public Boolean addSubtypeConstraint(h hVar, h hVar2, boolean z2) {
        m.checkNotNullParameter(hVar, "subType");
        m.checkNotNullParameter(hVar2, "superType");
        return null;
    }

    public final void clear() {
        ArrayDeque<i> arrayDeque = this.c;
        m.checkNotNull(arrayDeque);
        arrayDeque.clear();
        Set<i> set = this.d;
        m.checkNotNull(set);
        set.clear();
        this.f3495b = false;
    }

    public boolean customIsSubtypeOf(h hVar, h hVar2) {
        m.checkNotNullParameter(hVar, "subType");
        m.checkNotNullParameter(hVar2, "superType");
        return true;
    }

    public List<i> fastCorrespondingSupertypes(i iVar, l lVar) {
        return n.a.fastCorrespondingSupertypes(this, iVar, lVar);
    }

    public k get(j jVar, int i) {
        return n.a.get(this, jVar, i);
    }

    public k getArgumentOrNull(i iVar, int i) {
        return n.a.getArgumentOrNull(this, iVar, i);
    }

    public a getLowerCapturedTypePolicy(i iVar, c cVar) {
        m.checkNotNullParameter(iVar, "subType");
        m.checkNotNullParameter(cVar, "superType");
        return a.CHECK_SUBTYPE_AND_LOWER;
    }

    public final ArrayDeque<i> getSupertypesDeque() {
        return this.c;
    }

    public final Set<i> getSupertypesSet() {
        return this.d;
    }

    public boolean hasFlexibleNullability(h hVar) {
        return n.a.hasFlexibleNullability(this, hVar);
    }

    public final void initialize() {
        this.f3495b = true;
        if (this.c == null) {
            this.c = new ArrayDeque<>(4);
        }
        if (this.d == null) {
            this.d = d0.e0.p.d.m0.p.j.j.create();
        }
    }

    public boolean isClassType(i iVar) {
        return n.a.isClassType(this, iVar);
    }

    public boolean isDefinitelyNotNullType(h hVar) {
        return n.a.isDefinitelyNotNullType(this, hVar);
    }

    public boolean isDynamic(h hVar) {
        return n.a.isDynamic(this, hVar);
    }

    public abstract boolean isErrorTypeEqualsToAnything();

    public boolean isIntegerLiteralType(i iVar) {
        return n.a.isIntegerLiteralType(this, iVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public boolean isMarkedNullable(h hVar) {
        return n.a.isMarkedNullable(this, hVar);
    }

    public boolean isNothing(h hVar) {
        return n.a.isNothing(this, hVar);
    }

    public abstract boolean isStubTypeEqualsToAnything();

    @Override // d0.e0.p.d.m0.n.n1.n
    public i lowerBoundIfFlexible(h hVar) {
        return n.a.lowerBoundIfFlexible(this, hVar);
    }

    public abstract h prepareType(h hVar);

    public abstract h refineType(h hVar);

    public int size(j jVar) {
        return n.a.size(this, jVar);
    }

    public abstract b substitutionSupertypePolicy(i iVar);

    @Override // d0.e0.p.d.m0.n.n1.n
    public l typeConstructor(h hVar) {
        return n.a.typeConstructor(this, hVar);
    }

    @Override // d0.e0.p.d.m0.n.n1.n
    public i upperBoundIfFlexible(h hVar) {
        return n.a.upperBoundIfFlexible(this, hVar);
    }
}
