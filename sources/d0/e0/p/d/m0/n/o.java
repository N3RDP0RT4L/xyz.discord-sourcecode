package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.i1.k0;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.z0;
import java.util.HashMap;
import java.util.List;
/* compiled from: DescriptorSubstitutor.java */
/* loaded from: classes3.dex */
public class o {
    public static /* synthetic */ void a(int i) {
        String str = i != 4 ? "Argument for @NotNull parameter '%s' of %s.%s must not be null" : "@NotNull method %s.%s must not return null";
        Object[] objArr = new Object[i != 4 ? 3 : 2];
        switch (i) {
            case 1:
            case 6:
                objArr[0] = "originalSubstitution";
                break;
            case 2:
            case 7:
                objArr[0] = "newContainingDeclaration";
                break;
            case 3:
            case 8:
                objArr[0] = "result";
                break;
            case 4:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/types/DescriptorSubstitutor";
                break;
            case 5:
            default:
                objArr[0] = "typeParameters";
                break;
        }
        if (i != 4) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/types/DescriptorSubstitutor";
        } else {
            objArr[1] = "substituteTypeParameters";
        }
        if (i != 4) {
            objArr[2] = "substituteTypeParameters";
        }
        String format = String.format(str, objArr);
        if (i == 4) {
            throw new IllegalStateException(format);
        }
    }

    public static c1 substituteTypeParameters(List<z0> list, z0 z0Var, m mVar, List<z0> list2) {
        if (list == null) {
            a(0);
            throw null;
        } else if (z0Var == null) {
            a(1);
            throw null;
        } else if (mVar == null) {
            a(2);
            throw null;
        } else if (list2 != null) {
            c1 substituteTypeParameters = substituteTypeParameters(list, z0Var, mVar, list2, null);
            if (substituteTypeParameters != null) {
                return substituteTypeParameters;
            }
            throw new AssertionError("Substitution failed");
        } else {
            a(3);
            throw null;
        }
    }

    public static c1 substituteTypeParameters(List<z0> list, z0 z0Var, m mVar, List<z0> list2, boolean[] zArr) {
        if (list == null) {
            a(5);
            throw null;
        } else if (z0Var == null) {
            a(6);
            throw null;
        } else if (mVar == null) {
            a(7);
            throw null;
        } else if (list2 != null) {
            HashMap hashMap = new HashMap();
            HashMap hashMap2 = new HashMap();
            int i = 0;
            for (z0 z0Var2 : list) {
                i++;
                k0 createForFurtherModification = k0.createForFurtherModification(mVar, z0Var2.getAnnotations(), z0Var2.isReified(), z0Var2.getVariance(), z0Var2.getName(), i, u0.a, z0Var2.getStorageManager());
                hashMap.put(z0Var2.getTypeConstructor(), new y0(createForFurtherModification.getDefaultType()));
                hashMap2.put(z0Var2, createForFurtherModification);
                list2.add(createForFurtherModification);
            }
            c1 createChainedSubstitutor = c1.createChainedSubstitutor(z0Var, v0.createByConstructorsMap(hashMap));
            for (z0 z0Var3 : list) {
                k0 k0Var = (k0) hashMap2.get(z0Var3);
                for (c0 c0Var : z0Var3.getUpperBounds()) {
                    c0 substitute = createChainedSubstitutor.substitute(c0Var, j1.IN_VARIANCE);
                    if (substitute == null) {
                        return null;
                    }
                    if (!(substitute == c0Var || zArr == null)) {
                        zArr[0] = true;
                    }
                    k0Var.addUpperBound(substitute);
                }
                k0Var.setInitialized();
            }
            return createChainedSubstitutor;
        } else {
            a(8);
            throw null;
        }
    }
}
