package d0.e0.p.d.m0.n;

import b.d.b.a.a;
import d0.e0.p.d.m0.n.f;
import d0.e0.p.d.m0.n.n1.b;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.j;
import d0.e0.p.d.m0.n.n1.k;
import d0.e0.p.d.m0.n.n1.l;
import d0.e0.p.d.m0.n.n1.r;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: AbstractTypeChecker.kt */
/* loaded from: classes3.dex */
public final class e {
    public static final e a = new e();

    public static final boolean a(f fVar, e eVar, i iVar, i iVar2, boolean z2) {
        boolean z3;
        Collection<h> possibleIntegerTypes = fVar.possibleIntegerTypes(iVar);
        if ((possibleIntegerTypes instanceof Collection) && possibleIntegerTypes.isEmpty()) {
            return false;
        }
        for (h hVar : possibleIntegerTypes) {
            if (m.areEqual(fVar.typeConstructor(hVar), fVar.typeConstructor(iVar2)) || (z2 && isSubtypeOf$default(eVar, fVar, iVar2, hVar, false, 8, null))) {
                z3 = true;
                continue;
            } else {
                z3 = false;
                continue;
            }
            if (z3) {
                return true;
            }
        }
        return false;
    }

    public static /* synthetic */ boolean isSubtypeOf$default(e eVar, f fVar, h hVar, h hVar2, boolean z2, int i, Object obj) {
        if ((i & 8) != 0) {
            z2 = false;
        }
        return eVar.isSubtypeOf(fVar, hVar, hVar2, z2);
    }

    public final List<i> b(f fVar, i iVar, l lVar) {
        f.b bVar;
        b bVar2 = b.FOR_SUBTYPING;
        List<i> fastCorrespondingSupertypes = fVar.fastCorrespondingSupertypes(iVar, lVar);
        if (fastCorrespondingSupertypes == null) {
            if (!fVar.isClassTypeConstructor(lVar) && fVar.isClassType(iVar)) {
                return n.emptyList();
            }
            if (!fVar.isCommonFinalClassConstructor(lVar)) {
                fastCorrespondingSupertypes = new d0.e0.p.d.m0.p.i<>();
                fVar.initialize();
                ArrayDeque<i> supertypesDeque = fVar.getSupertypesDeque();
                m.checkNotNull(supertypesDeque);
                Set<i> supertypesSet = fVar.getSupertypesSet();
                m.checkNotNull(supertypesSet);
                supertypesDeque.push(iVar);
                while (!supertypesDeque.isEmpty()) {
                    if (supertypesSet.size() <= 1000) {
                        i pop = supertypesDeque.pop();
                        m.checkNotNullExpressionValue(pop, "current");
                        if (supertypesSet.add(pop)) {
                            i captureFromArguments = fVar.captureFromArguments(pop, bVar2);
                            if (captureFromArguments == null) {
                                captureFromArguments = pop;
                            }
                            if (fVar.areEqualTypeConstructors(fVar.typeConstructor(captureFromArguments), lVar)) {
                                fastCorrespondingSupertypes.add(captureFromArguments);
                                bVar = f.b.c.a;
                            } else if (fVar.argumentsCount(captureFromArguments) == 0) {
                                bVar = f.b.C0358b.a;
                            } else {
                                bVar = fVar.substitutionSupertypePolicy(captureFromArguments);
                            }
                            if (!(!m.areEqual(bVar, f.b.c.a))) {
                                bVar = null;
                            }
                            if (bVar != null) {
                                for (h hVar : fVar.supertypes(fVar.typeConstructor(pop))) {
                                    supertypesDeque.add(bVar.transformType(fVar, hVar));
                                }
                            }
                        }
                    } else {
                        StringBuilder X = a.X("Too many supertypes for type: ", iVar, ". Supertypes = ");
                        X.append(u.joinToString$default(supertypesSet, null, null, null, 0, null, null, 63, null));
                        throw new IllegalStateException(X.toString().toString());
                    }
                }
                fVar.clear();
            } else if (!fVar.areEqualTypeConstructors(fVar.typeConstructor(iVar), lVar)) {
                return n.emptyList();
            } else {
                i captureFromArguments2 = fVar.captureFromArguments(iVar, bVar2);
                if (captureFromArguments2 != null) {
                    iVar = captureFromArguments2;
                }
                return d0.t.m.listOf(iVar);
            }
        }
        return fastCorrespondingSupertypes;
    }

    public final List<i> c(f fVar, i iVar, l lVar) {
        List<i> b2 = b(fVar, iVar, lVar);
        if (b2.size() < 2) {
            return b2;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = b2.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            j asArgumentList = fVar.asArgumentList((i) next);
            int size = fVar.size(asArgumentList);
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                }
                if (!(fVar.asFlexibleType(fVar.getType(fVar.get(asArgumentList, i))) == null)) {
                    z2 = false;
                    break;
                }
                i++;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        return arrayList.isEmpty() ^ true ? arrayList : b2;
    }

    public final boolean d(f fVar, h hVar) {
        return fVar.isDenotable(fVar.typeConstructor(hVar)) && !fVar.isDynamic(hVar) && !fVar.isDefinitelyNotNullType(hVar) && m.areEqual(fVar.typeConstructor(fVar.lowerBoundIfFlexible(hVar)), fVar.typeConstructor(fVar.upperBoundIfFlexible(hVar)));
    }

    public final r effectiveVariance(r rVar, r rVar2) {
        m.checkNotNullParameter(rVar, "declared");
        m.checkNotNullParameter(rVar2, "useSite");
        r rVar3 = r.INV;
        if (rVar == rVar3) {
            return rVar2;
        }
        if (rVar2 == rVar3 || rVar == rVar2) {
            return rVar;
        }
        return null;
    }

    public final boolean equalTypes(f fVar, h hVar, h hVar2) {
        m.checkNotNullParameter(fVar, "context");
        m.checkNotNullParameter(hVar, "a");
        m.checkNotNullParameter(hVar2, "b");
        if (hVar == hVar2) {
            return true;
        }
        if (d(fVar, hVar) && d(fVar, hVar2)) {
            h refineType = fVar.refineType(hVar);
            h refineType2 = fVar.refineType(hVar2);
            i lowerBoundIfFlexible = fVar.lowerBoundIfFlexible(refineType);
            if (!fVar.areEqualTypeConstructors(fVar.typeConstructor(refineType), fVar.typeConstructor(refineType2))) {
                return false;
            }
            if (fVar.argumentsCount(lowerBoundIfFlexible) == 0) {
                return fVar.hasFlexibleNullability(refineType) || fVar.hasFlexibleNullability(refineType2) || fVar.isMarkedNullable(lowerBoundIfFlexible) == fVar.isMarkedNullable(fVar.lowerBoundIfFlexible(refineType2));
            }
        }
        return isSubtypeOf$default(this, fVar, hVar, hVar2, false, 8, null) && isSubtypeOf$default(this, fVar, hVar2, hVar, false, 8, null);
    }

    public final List<i> findCorrespondingSupertypes(f fVar, i iVar, l lVar) {
        f.b bVar;
        m.checkNotNullParameter(fVar, "<this>");
        m.checkNotNullParameter(iVar, "subType");
        m.checkNotNullParameter(lVar, "superConstructor");
        if (fVar.isClassType(iVar)) {
            return c(fVar, iVar, lVar);
        }
        if (!(fVar.isClassTypeConstructor(lVar) || fVar.isIntegerLiteralTypeConstructor(lVar))) {
            return b(fVar, iVar, lVar);
        }
        d0.e0.p.d.m0.p.i<i> iVar2 = new d0.e0.p.d.m0.p.i();
        fVar.initialize();
        ArrayDeque<i> supertypesDeque = fVar.getSupertypesDeque();
        m.checkNotNull(supertypesDeque);
        Set<i> supertypesSet = fVar.getSupertypesSet();
        m.checkNotNull(supertypesSet);
        supertypesDeque.push(iVar);
        while (!supertypesDeque.isEmpty()) {
            if (supertypesSet.size() <= 1000) {
                i pop = supertypesDeque.pop();
                m.checkNotNullExpressionValue(pop, "current");
                if (supertypesSet.add(pop)) {
                    if (fVar.isClassType(pop)) {
                        iVar2.add(pop);
                        bVar = f.b.c.a;
                    } else {
                        bVar = f.b.C0358b.a;
                    }
                    if (!(!m.areEqual(bVar, f.b.c.a))) {
                        bVar = null;
                    }
                    if (bVar != null) {
                        for (h hVar : fVar.supertypes(fVar.typeConstructor(pop))) {
                            supertypesDeque.add(bVar.transformType(fVar, hVar));
                        }
                    }
                }
            } else {
                StringBuilder X = a.X("Too many supertypes for type: ", iVar, ". Supertypes = ");
                X.append(u.joinToString$default(supertypesSet, null, null, null, 0, null, null, 63, null));
                throw new IllegalStateException(X.toString().toString());
            }
        }
        fVar.clear();
        ArrayList arrayList = new ArrayList();
        for (i iVar3 : iVar2) {
            m.checkNotNullExpressionValue(iVar3, "it");
            d0.t.r.addAll(arrayList, c(fVar, iVar3, lVar));
        }
        return arrayList;
    }

    public final boolean isSubtypeForSameConstructor(f fVar, j jVar, i iVar) {
        int i;
        int i2;
        boolean z2;
        int i3;
        m.checkNotNullParameter(fVar, "<this>");
        m.checkNotNullParameter(jVar, "capturedSubArguments");
        m.checkNotNullParameter(iVar, "superType");
        l typeConstructor = fVar.typeConstructor(iVar);
        int parametersCount = fVar.parametersCount(typeConstructor);
        if (parametersCount > 0) {
            int i4 = 0;
            while (true) {
                int i5 = i4 + 1;
                k argument = fVar.getArgument(iVar, i4);
                if (!fVar.isStarProjection(argument)) {
                    h type = fVar.getType(argument);
                    k kVar = fVar.get(jVar, i4);
                    fVar.getVariance(kVar);
                    r rVar = r.INV;
                    h type2 = fVar.getType(kVar);
                    r effectiveVariance = effectiveVariance(fVar.getVariance(fVar.getParameter(typeConstructor, i4)), fVar.getVariance(argument));
                    if (effectiveVariance == null) {
                        return fVar.isErrorTypeEqualsToAnything();
                    }
                    i = fVar.a;
                    if (i <= 100) {
                        i2 = fVar.a;
                        fVar.a = i2 + 1;
                        int ordinal = effectiveVariance.ordinal();
                        if (ordinal == 0) {
                            z2 = isSubtypeOf$default(this, fVar, type, type2, false, 8, null);
                        } else if (ordinal == 1) {
                            z2 = isSubtypeOf$default(this, fVar, type2, type, false, 8, null);
                        } else if (ordinal == 2) {
                            z2 = equalTypes(fVar, type2, type);
                        } else {
                            throw new NoWhenBranchMatchedException();
                        }
                        i3 = fVar.a;
                        fVar.a = i3 - 1;
                        if (!z2) {
                            return false;
                        }
                    } else {
                        throw new IllegalStateException(m.stringPlus("Arguments depth is too high. Some related argument: ", type2).toString());
                    }
                }
                if (i5 >= parametersCount) {
                    break;
                }
                i4 = i5;
            }
        }
        return true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:141:0x0271, code lost:
        if ((r27.getVariance(r14) == d0.e0.p.d.m0.n.n1.r.INV) != false) goto L143;
     */
    /* JADX WARN: Removed duplicated region for block: B:111:0x01e7  */
    /* JADX WARN: Removed duplicated region for block: B:194:0x03d1  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean isSubtypeOf(d0.e0.p.d.m0.n.f r27, d0.e0.p.d.m0.n.n1.h r28, d0.e0.p.d.m0.n.n1.h r29, boolean r30) {
        /*
            Method dump skipped, instructions count: 1004
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.e.isSubtypeOf(d0.e0.p.d.m0.n.f, d0.e0.p.d.m0.n.n1.h, d0.e0.p.d.m0.n.n1.h, boolean):boolean");
    }
}
