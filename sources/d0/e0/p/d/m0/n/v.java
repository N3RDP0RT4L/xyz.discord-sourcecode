package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.j.c;
import d0.e0.p.d.m0.j.h;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.n1.f;
import d0.z.d.m;
import java.util.List;
/* compiled from: KotlinType.kt */
/* loaded from: classes3.dex */
public abstract class v extends i1 implements f {
    public final j0 k;
    public final j0 l;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public v(j0 j0Var, j0 j0Var2) {
        super(null);
        m.checkNotNullParameter(j0Var, "lowerBound");
        m.checkNotNullParameter(j0Var2, "upperBound");
        this.k = j0Var;
        this.l = j0Var2;
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        return getDelegate().getAnnotations();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public List<w0> getArguments() {
        return getDelegate().getArguments();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public u0 getConstructor() {
        return getDelegate().getConstructor();
    }

    public abstract j0 getDelegate();

    public final j0 getLowerBound() {
        return this.k;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public i getMemberScope() {
        return getDelegate().getMemberScope();
    }

    public final j0 getUpperBound() {
        return this.l;
    }

    @Override // d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return getDelegate().isMarkedNullable();
    }

    public abstract String render(c cVar, h hVar);

    public String toString() {
        return c.c.renderType(this);
    }
}
