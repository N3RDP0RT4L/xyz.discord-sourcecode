package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.c;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.l;
import d0.e0.p.d.m0.n.n1.n;
import d0.z.d.m;
/* compiled from: TypeSystemCommonBackendContext.kt */
/* loaded from: classes3.dex */
public interface d1 extends n {

    /* compiled from: TypeSystemCommonBackendContext.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static h makeNullable(d1 d1Var, h hVar) {
            m.checkNotNullParameter(d1Var, "this");
            m.checkNotNullParameter(hVar, "receiver");
            i asSimpleType = d1Var.asSimpleType(hVar);
            return asSimpleType == null ? hVar : d1Var.withNullability(asSimpleType, true);
        }
    }

    c getClassFqNameUnsafe(l lVar);

    d0.e0.p.d.m0.b.i getPrimitiveArrayType(l lVar);

    d0.e0.p.d.m0.b.i getPrimitiveType(l lVar);

    h getRepresentativeUpperBound(d0.e0.p.d.m0.n.n1.m mVar);

    h getSubstitutedUnderlyingType(h hVar);

    d0.e0.p.d.m0.n.n1.m getTypeParameterClassifier(l lVar);

    boolean hasAnnotation(h hVar, b bVar);

    boolean isInlineClass(l lVar);

    boolean isUnderKotlinPackage(l lVar);

    h makeNullable(h hVar);
}
