package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.j.c;
import d0.e0.p.d.m0.j.h;
import d0.e0.p.d.m0.n.n1.e;
import d0.e0.p.d.m0.n.o1.a;
import d0.z.d.m;
/* compiled from: dynamicTypes.kt */
/* loaded from: classes3.dex */
public final class q extends v implements e {
    public final g m;

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public q(d0.e0.p.d.m0.b.h r3, d0.e0.p.d.m0.c.g1.g r4) {
        /*
            r2 = this;
            java.lang.String r0 = "builtIns"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "annotations"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            d0.e0.p.d.m0.n.j0 r0 = r3.getNothingType()
            java.lang.String r1 = "builtIns.nothingType"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            d0.e0.p.d.m0.n.j0 r3 = r3.getNullableAnyType()
            java.lang.String r1 = "builtIns.nullableAnyType"
            d0.z.d.m.checkNotNullExpressionValue(r3, r1)
            r2.<init>(r0, r3)
            r2.m = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.q.<init>(d0.e0.p.d.m0.b.h, d0.e0.p.d.m0.c.g1.g):void");
    }

    @Override // d0.e0.p.d.m0.n.v, d0.e0.p.d.m0.c.g1.a
    public g getAnnotations() {
        return this.m;
    }

    @Override // d0.e0.p.d.m0.n.v
    public j0 getDelegate() {
        return getUpperBound();
    }

    @Override // d0.e0.p.d.m0.n.v, d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return false;
    }

    @Override // d0.e0.p.d.m0.n.i1
    public q makeNullableAsSpecified(boolean z2) {
        return this;
    }

    @Override // d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public q refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return this;
    }

    @Override // d0.e0.p.d.m0.n.v
    public String render(c cVar, h hVar) {
        m.checkNotNullParameter(cVar, "renderer");
        m.checkNotNullParameter(hVar, "options");
        return "dynamic";
    }

    @Override // d0.e0.p.d.m0.n.i1
    public q replaceAnnotations(g gVar) {
        m.checkNotNullParameter(gVar, "newAnnotations");
        return new q(a.getBuiltIns(getDelegate()), gVar);
    }
}
