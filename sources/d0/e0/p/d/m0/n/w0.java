package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.n1.k;
/* compiled from: TypeProjection.java */
/* loaded from: classes3.dex */
public interface w0 extends k {
    j1 getProjectionKind();

    c0 getType();

    boolean isStarProjection();

    w0 refine(g gVar);
}
