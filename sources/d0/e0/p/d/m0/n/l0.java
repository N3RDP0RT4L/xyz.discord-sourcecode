package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.g;
import d0.z.d.m;
/* compiled from: TypeWithEnhancement.kt */
/* loaded from: classes3.dex */
public final class l0 extends m implements f1 {
    public final j0 k;
    public final c0 l;

    public l0(j0 j0Var, c0 c0Var) {
        m.checkNotNullParameter(j0Var, "delegate");
        m.checkNotNullParameter(c0Var, "enhancement");
        this.k = j0Var;
        this.l = c0Var;
    }

    @Override // d0.e0.p.d.m0.n.m
    public j0 getDelegate() {
        return this.k;
    }

    @Override // d0.e0.p.d.m0.n.f1
    public c0 getEnhancement() {
        return this.l;
    }

    @Override // d0.e0.p.d.m0.n.f1
    public i1 getOrigin() {
        return this.k;
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public j0 makeNullableAsSpecified(boolean z2) {
        return (j0) g1.wrapEnhancement(getOrigin().makeNullableAsSpecified(z2), getEnhancement().unwrap().makeNullableAsSpecified(z2));
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public j0 replaceAnnotations(g gVar) {
        m.checkNotNullParameter(gVar, "newAnnotations");
        return (j0) g1.wrapEnhancement(getOrigin().replaceAnnotations(gVar), getEnhancement());
    }

    @Override // d0.e0.p.d.m0.n.m
    public l0 replaceDelegate(j0 j0Var) {
        m.checkNotNullParameter(j0Var, "delegate");
        return new l0(j0Var, getEnhancement());
    }

    @Override // d0.e0.p.d.m0.n.m, d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public l0 refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return new l0((j0) gVar.refineType(this.k), gVar.refineType(getEnhancement()));
    }
}
