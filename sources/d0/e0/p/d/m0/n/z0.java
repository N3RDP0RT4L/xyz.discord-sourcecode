package d0.e0.p.d.m0.n;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.g1.g;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypeSubstitution.kt */
/* loaded from: classes3.dex */
public abstract class z0 {
    public static final z0 a = new a();

    /* compiled from: TypeSubstitution.kt */
    /* loaded from: classes3.dex */
    public static final class a extends z0 {
        @Override // d0.e0.p.d.m0.n.z0
        public Void get(c0 c0Var) {
            m.checkNotNullParameter(c0Var, "key");
            return null;
        }

        @Override // d0.e0.p.d.m0.n.z0
        public boolean isEmpty() {
            return true;
        }

        public String toString() {
            return "Empty TypeSubstitution";
        }
    }

    /* compiled from: TypeSubstitution.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new b(null);
    }

    public boolean approximateCapturedTypes() {
        return false;
    }

    public boolean approximateContravariantCapturedTypes() {
        return false;
    }

    public final c1 buildSubstitutor() {
        c1 create = c1.create(this);
        m.checkNotNullExpressionValue(create, "create(this)");
        return create;
    }

    public g filterAnnotations(g gVar) {
        m.checkNotNullParameter(gVar, "annotations");
        return gVar;
    }

    public abstract w0 get(c0 c0Var);

    public boolean isEmpty() {
        return false;
    }

    public c0 prepareTopLevelType(c0 c0Var, j1 j1Var) {
        m.checkNotNullParameter(c0Var, "topLevelType");
        m.checkNotNullParameter(j1Var, ModelAuditLogEntry.CHANGE_KEY_POSITION);
        return c0Var;
    }
}
