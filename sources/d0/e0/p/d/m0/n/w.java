package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.j.c;
import d0.e0.p.d.m0.j.h;
import d0.z.d.m;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: flexibleTypes.kt */
/* loaded from: classes3.dex */
public final class w extends v implements j {

    /* compiled from: flexibleTypes.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new a(null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public w(j0 j0Var, j0 j0Var2) {
        super(j0Var, j0Var2);
        m.checkNotNullParameter(j0Var, "lowerBound");
        m.checkNotNullParameter(j0Var2, "upperBound");
    }

    @Override // d0.e0.p.d.m0.n.v
    public j0 getDelegate() {
        return getLowerBound();
    }

    @Override // d0.e0.p.d.m0.n.j
    public boolean isTypeVariable() {
        return (getLowerBound().getConstructor().getDeclarationDescriptor() instanceof z0) && m.areEqual(getLowerBound().getConstructor(), getUpperBound().getConstructor());
    }

    @Override // d0.e0.p.d.m0.n.i1
    public i1 makeNullableAsSpecified(boolean z2) {
        d0 d0Var = d0.a;
        return d0.flexibleType(getLowerBound().makeNullableAsSpecified(z2), getUpperBound().makeNullableAsSpecified(z2));
    }

    @Override // d0.e0.p.d.m0.n.v
    public String render(c cVar, h hVar) {
        m.checkNotNullParameter(cVar, "renderer");
        m.checkNotNullParameter(hVar, "options");
        if (!hVar.getDebugMode()) {
            return cVar.renderFlexibleType(cVar.renderType(getLowerBound()), cVar.renderType(getUpperBound()), d0.e0.p.d.m0.n.o1.a.getBuiltIns(this));
        }
        StringBuilder O = b.d.b.a.a.O('(');
        O.append(cVar.renderType(getLowerBound()));
        O.append("..");
        O.append(cVar.renderType(getUpperBound()));
        O.append(')');
        return O.toString();
    }

    @Override // d0.e0.p.d.m0.n.i1
    public i1 replaceAnnotations(g gVar) {
        m.checkNotNullParameter(gVar, "newAnnotations");
        d0 d0Var = d0.a;
        return d0.flexibleType(getLowerBound().replaceAnnotations(gVar), getUpperBound().replaceAnnotations(gVar));
    }

    @Override // d0.e0.p.d.m0.n.j
    public c0 substitutionResult(c0 c0Var) {
        i1 i1Var;
        m.checkNotNullParameter(c0Var, "replacement");
        i1 unwrap = c0Var.unwrap();
        if (unwrap instanceof v) {
            i1Var = unwrap;
        } else if (unwrap instanceof j0) {
            d0 d0Var = d0.a;
            j0 j0Var = (j0) unwrap;
            i1Var = d0.flexibleType(j0Var, j0Var.makeNullableAsSpecified(true));
        } else {
            throw new NoWhenBranchMatchedException();
        }
        return g1.inheritEnhancement(i1Var, unwrap);
    }

    @Override // d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public v refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return new w((j0) gVar.refineType(getLowerBound()), (j0) gVar.refineType(getUpperBound()));
    }
}
