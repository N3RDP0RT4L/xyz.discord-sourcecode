package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.t.h0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypeAliasExpansion.kt */
/* loaded from: classes3.dex */
public final class r0 {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final r0 f3509b;
    public final y0 c;
    public final List<w0> d;
    public final Map<z0, w0> e;

    /* compiled from: TypeAliasExpansion.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final r0 create(r0 r0Var, y0 y0Var, List<? extends w0> list) {
            m.checkNotNullParameter(y0Var, "typeAliasDescriptor");
            m.checkNotNullParameter(list, "arguments");
            List<z0> parameters = y0Var.getTypeConstructor().getParameters();
            m.checkNotNullExpressionValue(parameters, "typeAliasDescriptor.typeConstructor.parameters");
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(parameters, 10));
            for (z0 z0Var : parameters) {
                arrayList.add(z0Var.getOriginal());
            }
            return new r0(r0Var, y0Var, list, h0.toMap(u.zip(arrayList, list)), null);
        }
    }

    public r0(r0 r0Var, y0 y0Var, List list, Map map, DefaultConstructorMarker defaultConstructorMarker) {
        this.f3509b = r0Var;
        this.c = y0Var;
        this.d = list;
        this.e = map;
    }

    public final List<w0> getArguments() {
        return this.d;
    }

    public final y0 getDescriptor() {
        return this.c;
    }

    public final w0 getReplacement(u0 u0Var) {
        m.checkNotNullParameter(u0Var, "constructor");
        h declarationDescriptor = u0Var.getDeclarationDescriptor();
        if (declarationDescriptor instanceof z0) {
            return this.e.get(declarationDescriptor);
        }
        return null;
    }

    public final boolean isRecursion(y0 y0Var) {
        m.checkNotNullParameter(y0Var, "descriptor");
        if (!m.areEqual(this.c, y0Var)) {
            r0 r0Var = this.f3509b;
            if (!(r0Var == null ? false : r0Var.isRecursion(y0Var))) {
                return false;
            }
        }
        return true;
    }
}
