package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.l;
import d0.e0.p.d.m0.n.n1.m;
import java.util.HashSet;
/* compiled from: expandedTypeUtils.kt */
/* loaded from: classes3.dex */
public final class u {
    public static final h a(d1 d1Var, h hVar, HashSet<l> hashSet) {
        h a;
        l typeConstructor = d1Var.typeConstructor(hVar);
        if (!hashSet.add(typeConstructor)) {
            return null;
        }
        m typeParameterClassifier = d1Var.getTypeParameterClassifier(typeConstructor);
        if (typeParameterClassifier != null) {
            a = a(d1Var, d1Var.getRepresentativeUpperBound(typeParameterClassifier), hashSet);
            if (a == null) {
                return null;
            }
            if (!d1Var.isNullableType(a) && d1Var.isMarkedNullable(hVar)) {
                return d1Var.makeNullable(a);
            }
        } else if (!d1Var.isInlineClass(typeConstructor)) {
            return hVar;
        } else {
            h substitutedUnderlyingType = d1Var.getSubstitutedUnderlyingType(hVar);
            if (substitutedUnderlyingType == null || (a = a(d1Var, substitutedUnderlyingType, hashSet)) == null) {
                return null;
            }
            if (d1Var.isNullableType(hVar)) {
                return d1Var.isNullableType(a) ? hVar : (!(a instanceof i) || !d1Var.isPrimitiveType((i) a)) ? d1Var.makeNullable(a) : hVar;
            }
        }
        return a;
    }

    public static final h computeExpandedTypeForInlineClass(d1 d1Var, h hVar) {
        d0.z.d.m.checkNotNullParameter(d1Var, "<this>");
        d0.z.d.m.checkNotNullParameter(hVar, "inlineClassType");
        return a(d1Var, hVar, new HashSet());
    }
}
