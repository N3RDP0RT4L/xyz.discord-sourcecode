package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.j;
import d0.g0.q;
import d0.t.u;
import d0.z.d.m;
import java.util.Iterator;
/* compiled from: KotlinType.kt */
/* loaded from: classes3.dex */
public abstract class j0 extends i1 implements i, j {
    public j0() {
        super(null);
    }

    @Override // d0.e0.p.d.m0.n.i1
    public abstract j0 makeNullableAsSpecified(boolean z2);

    @Override // d0.e0.p.d.m0.n.i1
    public abstract j0 replaceAnnotations(g gVar);

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator<c> it = getAnnotations().iterator();
        while (it.hasNext()) {
            q.append(sb, "[", d0.e0.p.d.m0.j.c.renderAnnotation$default(d0.e0.p.d.m0.j.c.c, it.next(), null, 2, null), "] ");
        }
        sb.append(getConstructor());
        if (!getArguments().isEmpty()) {
            u.joinTo$default(getArguments(), sb, ", ", "<", ">", 0, null, null, 112, null);
        }
        if (isMarkedNullable()) {
            sb.append("?");
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
