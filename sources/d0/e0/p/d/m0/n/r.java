package d0.e0.p.d.m0.n;

import d0.z.d.m;
/* compiled from: dynamicTypes.kt */
/* loaded from: classes3.dex */
public final class r {
    public static final boolean isDynamic(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return c0Var.unwrap() instanceof q;
    }
}
