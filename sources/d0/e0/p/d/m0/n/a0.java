package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.n1.g;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: IntersectionTypeConstructor.kt */
/* loaded from: classes3.dex */
public final class a0 implements u0, g {
    public c0 a;

    /* renamed from: b  reason: collision with root package name */
    public final LinkedHashSet<c0> f3490b;
    public final int c;

    /* compiled from: IntersectionTypeConstructor.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<d0.e0.p.d.m0.n.l1.g, j0> {
        public a() {
            super(1);
        }

        public final j0 invoke(d0.e0.p.d.m0.n.l1.g gVar) {
            m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
            return a0.this.refine(gVar).createType();
        }
    }

    public a0(Collection<? extends c0> collection) {
        m.checkNotNullParameter(collection, "typesToIntersect");
        collection.isEmpty();
        LinkedHashSet<c0> linkedHashSet = new LinkedHashSet<>(collection);
        this.f3490b = linkedHashSet;
        this.c = linkedHashSet.hashCode();
    }

    public final i createScopeForKotlinType() {
        return d0.e0.p.d.m0.k.a0.o.f3436b.create("member scope for intersection type", this.f3490b);
    }

    public final j0 createType() {
        d0 d0Var = d0.a;
        return d0.simpleTypeWithNonTrivialMemberScope(d0.e0.p.d.m0.c.g1.g.f.getEMPTY(), this, n.emptyList(), false, createScopeForKotlinType(), new a());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a0)) {
            return false;
        }
        return m.areEqual(this.f3490b, ((a0) obj).f3490b);
    }

    public final c0 getAlternativeType() {
        return this.a;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public h getBuiltIns() {
        h builtIns = this.f3490b.iterator().next().getConstructor().getBuiltIns();
        m.checkNotNullExpressionValue(builtIns, "intersectedTypes.iterator().next().constructor.builtIns");
        return builtIns;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public d0.e0.p.d.m0.c.h getDeclarationDescriptor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public List<z0> getParameters() {
        return n.emptyList();
    }

    @Override // d0.e0.p.d.m0.n.u0
    public Collection<c0> getSupertypes() {
        return this.f3490b;
    }

    public int hashCode() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.n.u0
    public boolean isDenotable() {
        return false;
    }

    public final a0 setAlternative(c0 c0Var) {
        a0 a0Var = new a0(this.f3490b);
        a0Var.a = c0Var;
        return a0Var;
    }

    public String toString() {
        return u.joinToString$default(u.sortedWith(this.f3490b, new b0()), " & ", "{", "}", 0, null, null, 56, null);
    }

    @Override // d0.e0.p.d.m0.n.u0
    public a0 refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        Collection<c0> supertypes = getSupertypes();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(supertypes, 10));
        boolean z2 = false;
        for (c0 c0Var : supertypes) {
            arrayList.add(c0Var.refine(gVar));
            z2 = true;
        }
        a0 a0Var = null;
        c0 c0Var2 = null;
        if (z2) {
            c0 alternativeType = getAlternativeType();
            if (alternativeType != null) {
                c0Var2 = alternativeType.refine(gVar);
            }
            a0Var = new a0(arrayList).setAlternative(c0Var2);
        }
        return a0Var == null ? this : a0Var;
    }
}
