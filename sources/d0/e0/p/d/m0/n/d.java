package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.n.n1.f;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.i;
import d0.e0.p.d.m0.n.n1.k;
import d0.e0.p.d.m0.n.n1.n;
import d0.z.d.m;
/* compiled from: AbstractStrictEqualityTypeChecker.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final d a = new d();

    public final boolean a(n nVar, i iVar, i iVar2) {
        int argumentsCount;
        if (nVar.argumentsCount(iVar) == nVar.argumentsCount(iVar2) && nVar.isMarkedNullable(iVar) == nVar.isMarkedNullable(iVar2)) {
            if ((nVar.asDefinitelyNotNullType(iVar) == null) == (nVar.asDefinitelyNotNullType(iVar2) == null) && nVar.areEqualTypeConstructors(nVar.typeConstructor(iVar), nVar.typeConstructor(iVar2))) {
                if (!nVar.identicalArguments(iVar, iVar2) && (argumentsCount = nVar.argumentsCount(iVar)) > 0) {
                    int i = 0;
                    while (true) {
                        int i2 = i + 1;
                        k argument = nVar.getArgument(iVar, i);
                        k argument2 = nVar.getArgument(iVar2, i);
                        if (nVar.isStarProjection(argument) != nVar.isStarProjection(argument2)) {
                            return false;
                        }
                        if (!nVar.isStarProjection(argument) && (nVar.getVariance(argument) != nVar.getVariance(argument2) || !b(nVar, nVar.getType(argument), nVar.getType(argument2)))) {
                            return false;
                        }
                        if (i2 >= argumentsCount) {
                            break;
                        }
                        i = i2;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public final boolean b(n nVar, h hVar, h hVar2) {
        if (hVar == hVar2) {
            return true;
        }
        i asSimpleType = nVar.asSimpleType(hVar);
        i asSimpleType2 = nVar.asSimpleType(hVar2);
        if (asSimpleType != null && asSimpleType2 != null) {
            return a(nVar, asSimpleType, asSimpleType2);
        }
        f asFlexibleType = nVar.asFlexibleType(hVar);
        f asFlexibleType2 = nVar.asFlexibleType(hVar2);
        return asFlexibleType != null && asFlexibleType2 != null && a(nVar, nVar.lowerBound(asFlexibleType), nVar.lowerBound(asFlexibleType2)) && a(nVar, nVar.upperBound(asFlexibleType), nVar.upperBound(asFlexibleType2));
    }

    public final boolean strictEqualTypes(n nVar, h hVar, h hVar2) {
        m.checkNotNullParameter(nVar, "context");
        m.checkNotNullParameter(hVar, "a");
        m.checkNotNullParameter(hVar2, "b");
        return b(nVar, hVar, hVar2);
    }
}
