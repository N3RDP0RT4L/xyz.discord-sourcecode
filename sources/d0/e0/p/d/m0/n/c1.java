package d0.e0.p.d.m0.n;

import b.c.a.y.b;
import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.g1.l;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.u.a.d;
import d0.e0.p.d.m0.n.l1.j;
import d0.e0.p.d.m0.p.c;
import java.util.ArrayList;
import java.util.List;
/* compiled from: TypeSubstitutor.java */
/* loaded from: classes3.dex */
public class c1 {
    public static final c1 a = create(z0.a);

    /* renamed from: b  reason: collision with root package name */
    public final z0 f3492b;

    /* compiled from: TypeSubstitutor.java */
    /* loaded from: classes3.dex */
    public static final class a extends Exception {
        public a(String str) {
            super(str);
        }
    }

    public c1(z0 z0Var) {
        if (z0Var != null) {
            this.f3492b = z0Var;
        } else {
            a(6);
            throw null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x001d A[FALL_THROUGH] */
    /* JADX WARN: Removed duplicated region for block: B:19:0x002b  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0036 A[FALL_THROUGH] */
    /* JADX WARN: Removed duplicated region for block: B:26:0x003f  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0049  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x004e  */
    /* JADX WARN: Removed duplicated region for block: B:30:0x0053  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0058  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x005d  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0062  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0067  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x006c  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x007b  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0080  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x0085  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0093  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00bc  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00c3  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00c8  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x00cb  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x00ce  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x00d1  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00d4  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x00d9  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x00de  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00e1  */
    /* JADX WARN: Removed duplicated region for block: B:68:0x00e6  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x00f0 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:75:0x00f9  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r13) {
        /*
            Method dump skipped, instructions count: 630
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.c1.a(int):void");
    }

    public static int b(j1 j1Var, j1 j1Var2) {
        j1 j1Var3 = j1.OUT_VARIANCE;
        j1 j1Var4 = j1.IN_VARIANCE;
        if (j1Var == j1Var4 && j1Var2 == j1Var3) {
            return 3;
        }
        return (j1Var == j1Var3 && j1Var2 == j1Var4) ? 2 : 1;
    }

    public static String c(Object obj) {
        try {
            return obj.toString();
        } catch (Throwable th) {
            if (!c.isProcessCanceledException(th)) {
                return "[Exception while computing toString(): " + th + "]";
            }
            throw th;
        }
    }

    public static j1 combine(j1 j1Var, w0 w0Var) {
        if (j1Var == null) {
            a(33);
            throw null;
        } else if (w0Var != null) {
            return w0Var.isStarProjection() ? j1.OUT_VARIANCE : combine(j1Var, w0Var.getProjectionKind());
        } else {
            a(34);
            throw null;
        }
    }

    public static c1 create(z0 z0Var) {
        if (z0Var != null) {
            return new c1(z0Var);
        }
        a(0);
        throw null;
    }

    public static c1 createChainedSubstitutor(z0 z0Var, z0 z0Var2) {
        if (z0Var == null) {
            a(2);
            throw null;
        } else if (z0Var2 != null) {
            return create(p.create(z0Var, z0Var2));
        } else {
            a(3);
            throw null;
        }
    }

    public final w0 d(w0 w0Var, z0 z0Var, int i) throws a {
        c0 c0Var;
        c0 c0Var2 = null;
        if (w0Var != null) {
            z0 z0Var2 = this.f3492b;
            if (i > 100) {
                StringBuilder R = b.d.b.a.a.R("Recursion too deep. Most likely infinite loop while substituting ");
                R.append(c(w0Var));
                R.append("; substitution: ");
                R.append(c(z0Var2));
                throw new IllegalStateException(R.toString());
            } else if (w0Var.isStarProjection()) {
                return w0Var;
            } else {
                c0 type = w0Var.getType();
                if (type instanceof f1) {
                    f1 f1Var = (f1) type;
                    i1 origin = f1Var.getOrigin();
                    c0 enhancement = f1Var.getEnhancement();
                    w0 d = d(new y0(w0Var.getProjectionKind(), origin), z0Var, i + 1);
                    return new y0(d.getProjectionKind(), g1.wrapEnhancement(d.getType().unwrap(), substitute(enhancement, w0Var.getProjectionKind())));
                } else if (r.isDynamic(type) || (type.unwrap() instanceof i0)) {
                    return w0Var;
                } else {
                    w0 w0Var2 = this.f3492b.get(type);
                    if (w0Var2 == null) {
                        w0Var2 = null;
                    } else if (type.getAnnotations().hasAnnotation(k.a.G)) {
                        u0 constructor = w0Var2.getType().getConstructor();
                        if (constructor instanceof j) {
                            w0 projection = ((j) constructor).getProjection();
                            j1 projectionKind = projection.getProjectionKind();
                            if (b(w0Var.getProjectionKind(), projectionKind) == 3) {
                                w0Var2 = new y0(projection.getType());
                            } else if (z0Var != null && b(z0Var.getVariance(), projectionKind) == 3) {
                                w0Var2 = new y0(projection.getType());
                            }
                        }
                    }
                    j1 projectionKind2 = w0Var.getProjectionKind();
                    if (w0Var2 == null && y.isFlexible(type) && !t0.isCustomTypeVariable(type)) {
                        v asFlexibleType = y.asFlexibleType(type);
                        int i2 = i + 1;
                        w0 d2 = d(new y0(projectionKind2, asFlexibleType.getLowerBound()), z0Var, i2);
                        w0 d3 = d(new y0(projectionKind2, asFlexibleType.getUpperBound()), z0Var, i2);
                        return (d2.getType() == asFlexibleType.getLowerBound() && d3.getType() == asFlexibleType.getUpperBound()) ? w0Var : new y0(d2.getProjectionKind(), d0.flexibleType(a1.asSimpleType(d2.getType()), a1.asSimpleType(d3.getType())));
                    } else if (h.isNothing(type) || e0.isError(type)) {
                        return w0Var;
                    } else {
                        if (w0Var2 != null) {
                            int b2 = b(projectionKind2, w0Var2.getProjectionKind());
                            if (!d.isCaptured(type)) {
                                int h = b.h(b2);
                                if (h == 1) {
                                    return new y0(j1.OUT_VARIANCE, type.getConstructor().getBuiltIns().getNullableAnyType());
                                }
                                if (h == 2) {
                                    throw new a("Out-projection in in-position");
                                }
                            }
                            j customTypeVariable = t0.getCustomTypeVariable(type);
                            if (w0Var2.isStarProjection()) {
                                return w0Var2;
                            }
                            if (customTypeVariable != null) {
                                c0Var = customTypeVariable.substitutionResult(w0Var2.getType());
                            } else {
                                c0Var = e1.makeNullableIfNeeded(w0Var2.getType(), type.isMarkedNullable());
                            }
                            if (!type.getAnnotations().isEmpty()) {
                                g filterAnnotations = this.f3492b.filterAnnotations(type.getAnnotations());
                                if (filterAnnotations != null) {
                                    if (filterAnnotations.hasAnnotation(k.a.G)) {
                                        filterAnnotations = new l(filterAnnotations, new b1());
                                    }
                                    c0Var = d0.e0.p.d.m0.n.o1.a.replaceAnnotations(c0Var, new d0.e0.p.d.m0.c.g1.k(c0Var.getAnnotations(), filterAnnotations));
                                } else {
                                    a(31);
                                    throw null;
                                }
                            }
                            if (b2 == 1) {
                                projectionKind2 = combine(projectionKind2, w0Var2.getProjectionKind());
                            }
                            return new y0(projectionKind2, c0Var);
                        }
                        j1 j1Var = j1.INVARIANT;
                        c0 type2 = w0Var.getType();
                        j1 projectionKind3 = w0Var.getProjectionKind();
                        if (type2.getConstructor().getDeclarationDescriptor() instanceof z0) {
                            return w0Var;
                        }
                        j0 abbreviation = m0.getAbbreviation(type2);
                        if (abbreviation != null) {
                            c0Var2 = replaceWithNonApproximatingSubstitution().substitute(abbreviation, j1Var);
                        }
                        List<z0> parameters = type2.getConstructor().getParameters();
                        List<w0> arguments = type2.getArguments();
                        ArrayList arrayList = new ArrayList(parameters.size());
                        boolean z2 = false;
                        for (int i3 = 0; i3 < parameters.size(); i3++) {
                            z0 z0Var3 = parameters.get(i3);
                            w0 w0Var3 = arguments.get(i3);
                            w0 d4 = d(w0Var3, z0Var3, i + 1);
                            int h2 = b.h(b(z0Var3.getVariance(), d4.getProjectionKind()));
                            if (h2 != 0) {
                                if (h2 == 1 || h2 == 2) {
                                    d4 = e1.makeStarProjection(z0Var3);
                                }
                            } else if (z0Var3.getVariance() != j1Var && !d4.isStarProjection()) {
                                d4 = new y0(j1Var, d4.getType());
                            }
                            if (d4 != w0Var3) {
                                z2 = true;
                            }
                            arrayList.add(d4);
                        }
                        if (z2) {
                            arguments = arrayList;
                        }
                        c0 replace = a1.replace(type2, arguments, this.f3492b.filterAnnotations(type2.getAnnotations()));
                        if ((replace instanceof j0) && (c0Var2 instanceof j0)) {
                            replace = m0.withAbbreviation((j0) replace, (j0) c0Var2);
                        }
                        return new y0(projectionKind3, replace);
                    }
                }
            }
        } else {
            a(17);
            throw null;
        }
    }

    public z0 getSubstitution() {
        z0 z0Var = this.f3492b;
        if (z0Var != null) {
            return z0Var;
        }
        a(7);
        throw null;
    }

    public boolean isEmpty() {
        return this.f3492b.isEmpty();
    }

    public c1 replaceWithNonApproximatingSubstitution() {
        z0 z0Var = this.f3492b;
        return (!(z0Var instanceof z) || !z0Var.approximateContravariantCapturedTypes()) ? this : new c1(new z(((z) this.f3492b).getParameters(), ((z) this.f3492b).getArguments(), false));
    }

    public c0 safeSubstitute(c0 c0Var, j1 j1Var) {
        if (c0Var == null) {
            a(8);
            throw null;
        } else if (j1Var == null) {
            a(9);
            throw null;
        } else if (!isEmpty()) {
            try {
                c0 type = d(new y0(j1Var, c0Var), null, 0).getType();
                if (type != null) {
                    return type;
                }
                a(11);
                throw null;
            } catch (a e) {
                j0 createErrorType = t.createErrorType(e.getMessage());
                if (createErrorType != null) {
                    return createErrorType;
                }
                a(12);
                throw null;
            }
        } else if (c0Var != null) {
            return c0Var;
        } else {
            a(10);
            throw null;
        }
    }

    public c0 substitute(c0 c0Var, j1 j1Var) {
        if (c0Var == null) {
            a(13);
            throw null;
        } else if (j1Var != null) {
            w0 substitute = substitute(new y0(j1Var, getSubstitution().prepareTopLevelType(c0Var, j1Var)));
            if (substitute == null) {
                return null;
            }
            return substitute.getType();
        } else {
            a(14);
            throw null;
        }
    }

    public w0 substituteWithoutApproximation(w0 w0Var) {
        if (w0Var == null) {
            a(16);
            throw null;
        } else if (isEmpty()) {
            return w0Var;
        } else {
            try {
                return d(w0Var, null, 0);
            } catch (a unused) {
                return null;
            }
        }
    }

    public static c1 create(c0 c0Var) {
        if (c0Var != null) {
            return create(v0.create(c0Var.getConstructor(), c0Var.getArguments()));
        }
        a(5);
        throw null;
    }

    public static j1 combine(j1 j1Var, j1 j1Var2) {
        if (j1Var == null) {
            a(36);
            throw null;
        } else if (j1Var2 != null) {
            j1 j1Var3 = j1.INVARIANT;
            if (j1Var == j1Var3) {
                if (j1Var2 != null) {
                    return j1Var2;
                }
                a(38);
                throw null;
            } else if (j1Var2 == j1Var3) {
                if (j1Var != null) {
                    return j1Var;
                }
                a(39);
                throw null;
            } else if (j1Var != j1Var2) {
                throw new AssertionError("Variance conflict: type parameter variance '" + j1Var + "' and projection kind '" + j1Var2 + "' cannot be combined");
            } else if (j1Var2 != null) {
                return j1Var2;
            } else {
                a(40);
                throw null;
            }
        } else {
            a(37);
            throw null;
        }
    }

    public w0 substitute(w0 w0Var) {
        if (w0Var != null) {
            w0 substituteWithoutApproximation = substituteWithoutApproximation(w0Var);
            return (this.f3492b.approximateCapturedTypes() || this.f3492b.approximateContravariantCapturedTypes()) ? d0.e0.p.d.m0.n.p1.b.approximateCapturedTypesIfNecessary(substituteWithoutApproximation, this.f3492b.approximateContravariantCapturedTypes()) : substituteWithoutApproximation;
        }
        a(15);
        throw null;
    }
}
