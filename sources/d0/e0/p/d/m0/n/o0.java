package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.z0;
import d0.g;
import d0.i;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
/* compiled from: StarProjectionImpl.kt */
/* loaded from: classes3.dex */
public final class o0 extends x0 {
    public final z0 a;

    /* renamed from: b  reason: collision with root package name */
    public final Lazy f3504b = g.lazy(i.PUBLICATION, new a());

    /* compiled from: StarProjectionImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<c0> {
        public a() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final c0 invoke() {
            return p0.starProjectionType(o0.this.a);
        }
    }

    public o0(z0 z0Var) {
        m.checkNotNullParameter(z0Var, "typeParameter");
        this.a = z0Var;
    }

    @Override // d0.e0.p.d.m0.n.w0
    public j1 getProjectionKind() {
        return j1.OUT_VARIANCE;
    }

    @Override // d0.e0.p.d.m0.n.w0
    public c0 getType() {
        return (c0) this.f3504b.getValue();
    }

    @Override // d0.e0.p.d.m0.n.w0
    public boolean isStarProjection() {
        return true;
    }

    @Override // d0.e0.p.d.m0.n.w0
    public w0 refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return this;
    }
}
