package d0.e0.p.d.m0.n;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.g1.g;
import d0.z.d.m;
/* compiled from: TypeSubstitution.kt */
/* loaded from: classes3.dex */
public class l extends z0 {

    /* renamed from: b  reason: collision with root package name */
    public final z0 f3498b;

    public l(z0 z0Var) {
        m.checkNotNullParameter(z0Var, "substitution");
        this.f3498b = z0Var;
    }

    @Override // d0.e0.p.d.m0.n.z0
    public boolean approximateCapturedTypes() {
        return this.f3498b.approximateCapturedTypes();
    }

    @Override // d0.e0.p.d.m0.n.z0
    public g filterAnnotations(g gVar) {
        m.checkNotNullParameter(gVar, "annotations");
        return this.f3498b.filterAnnotations(gVar);
    }

    @Override // d0.e0.p.d.m0.n.z0
    public w0 get(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "key");
        return this.f3498b.get(c0Var);
    }

    @Override // d0.e0.p.d.m0.n.z0
    public boolean isEmpty() {
        return this.f3498b.isEmpty();
    }

    @Override // d0.e0.p.d.m0.n.z0
    public c0 prepareTopLevelType(c0 c0Var, j1 j1Var) {
        m.checkNotNullParameter(c0Var, "topLevelType");
        m.checkNotNullParameter(j1Var, ModelAuditLogEntry.CHANGE_KEY_POSITION);
        return this.f3498b.prepareTopLevelType(c0Var, j1Var);
    }
}
