package d0.e0.p.d.m0.n;

import andhook.lib.HookHelper;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.b0;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.i;
import d0.e0.p.d.m0.c.j0;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.o;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.t.n;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: ErrorUtils.java */
/* loaded from: classes3.dex */
public class t {

    /* renamed from: b  reason: collision with root package name */
    public static final c f3510b;
    public static final c0 d;
    public static final n0 e;
    public static final Set<n0> f;
    public static final c0 a = new a();
    public static final j0 c = createErrorType("<LOOP IN SUPERTYPES>");

    /* compiled from: ErrorUtils.java */
    /* loaded from: classes3.dex */
    public static class a implements c0 {
        /* JADX WARN: Removed duplicated region for block: B:20:0x002c  */
        /* JADX WARN: Removed duplicated region for block: B:24:0x0037  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x003c  */
        /* JADX WARN: Removed duplicated region for block: B:26:0x0041  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x0046  */
        /* JADX WARN: Removed duplicated region for block: B:28:0x004b  */
        /* JADX WARN: Removed duplicated region for block: B:29:0x0050  */
        /* JADX WARN: Removed duplicated region for block: B:32:0x0056  */
        /* JADX WARN: Removed duplicated region for block: B:47:0x008c  */
        /* JADX WARN: Removed duplicated region for block: B:49:0x0093  */
        /* JADX WARN: Removed duplicated region for block: B:50:0x0098  */
        /* JADX WARN: Removed duplicated region for block: B:51:0x009d  */
        /* JADX WARN: Removed duplicated region for block: B:52:0x00a2  */
        /* JADX WARN: Removed duplicated region for block: B:53:0x00a7  */
        /* JADX WARN: Removed duplicated region for block: B:56:0x00af A[ADDED_TO_REGION] */
        /* JADX WARN: Removed duplicated region for block: B:62:0x00bc  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public static /* synthetic */ void a(int r12) {
            /*
                Method dump skipped, instructions count: 304
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.t.a.a(int):void");
        }

        @Override // d0.e0.p.d.m0.c.m
        public <R, D> R accept(o<R, D> oVar, D d) {
            if (oVar != null) {
                return null;
            }
            a(11);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.g1.a
        public g getAnnotations() {
            g empty = g.f.getEMPTY();
            if (empty != null) {
                return empty;
            }
            a(1);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.c0
        public h getBuiltIns() {
            d0.e0.p.d.m0.b.e eVar = d0.e0.p.d.m0.b.e.getInstance();
            if (eVar != null) {
                return eVar;
            }
            a(14);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.c0
        public <T> T getCapability(b0<T> b0Var) {
            if (b0Var != null) {
                return null;
            }
            a(0);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.m
        public m getContainingDeclaration() {
            return null;
        }

        @Override // d0.e0.p.d.m0.c.c0
        public List<c0> getExpectedByModules() {
            List<c0> emptyList = n.emptyList();
            if (emptyList != null) {
                return emptyList;
            }
            a(9);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.m
        public d0.e0.p.d.m0.g.e getName() {
            d0.e0.p.d.m0.g.e special = d0.e0.p.d.m0.g.e.special("<ERROR MODULE>");
            if (special != null) {
                return special;
            }
            a(5);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.m
        public m getOriginal() {
            return this;
        }

        @Override // d0.e0.p.d.m0.c.c0
        public j0 getPackage(d0.e0.p.d.m0.g.b bVar) {
            if (bVar == null) {
                a(7);
                throw null;
            }
            throw new IllegalStateException("Should not be called!");
        }

        @Override // d0.e0.p.d.m0.c.c0
        public Collection<d0.e0.p.d.m0.g.b> getSubPackagesOf(d0.e0.p.d.m0.g.b bVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
            if (bVar == null) {
                a(2);
                throw null;
            } else if (function1 != null) {
                List emptyList = n.emptyList();
                if (emptyList != null) {
                    return emptyList;
                }
                a(4);
                throw null;
            } else {
                a(3);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.c.c0
        public boolean shouldSeeInternalsOf(c0 c0Var) {
            if (c0Var != null) {
                return false;
            }
            a(12);
            throw null;
        }
    }

    /* compiled from: ErrorUtils.java */
    /* loaded from: classes3.dex */
    public static class b implements u0 {
        public final /* synthetic */ c a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f3511b;

        public b(c cVar, String str) {
            this.a = cVar;
            this.f3511b = str;
        }

        public static /* synthetic */ void a(int i) {
            String str = i != 3 ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
            Object[] objArr = new Object[i != 3 ? 2 : 3];
            if (i != 3) {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$2";
            } else {
                objArr[0] = "kotlinTypeRefiner";
            }
            if (i == 1) {
                objArr[1] = "getSupertypes";
            } else if (i == 2) {
                objArr[1] = "getBuiltIns";
            } else if (i == 3) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$2";
            } else if (i != 4) {
                objArr[1] = "getParameters";
            } else {
                objArr[1] = "refine";
            }
            if (i == 3) {
                objArr[2] = "refine";
            }
            String format = String.format(str, objArr);
            if (i == 3) {
                throw new IllegalArgumentException(format);
            }
        }

        @Override // d0.e0.p.d.m0.n.u0
        public h getBuiltIns() {
            d0.e0.p.d.m0.b.e eVar = d0.e0.p.d.m0.b.e.getInstance();
            if (eVar != null) {
                return eVar;
            }
            a(2);
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public d0.e0.p.d.m0.c.h getDeclarationDescriptor() {
            return this.a;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public List<z0> getParameters() {
            List<z0> emptyList = n.emptyList();
            if (emptyList != null) {
                return emptyList;
            }
            a(0);
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public Collection<c0> getSupertypes() {
            List emptyList = n.emptyList();
            if (emptyList != null) {
                return emptyList;
            }
            a(1);
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public boolean isDenotable() {
            return false;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public u0 refine(d0.e0.p.d.m0.n.l1.g gVar) {
            if (gVar != null) {
                return this;
            }
            a(3);
            throw null;
        }

        public String toString() {
            return this.f3511b;
        }
    }

    /* compiled from: ErrorUtils.java */
    /* loaded from: classes3.dex */
    public static class c extends i {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public c(d0.e0.p.d.m0.g.e r11) {
            /*
                r10 = this;
                if (r11 == 0) goto L4f
                d0.e0.p.d.m0.c.c0 r1 = d0.e0.p.d.m0.n.t.getErrorModule()
                d0.e0.p.d.m0.c.z r3 = d0.e0.p.d.m0.c.z.OPEN
                d0.e0.p.d.m0.c.f r4 = d0.e0.p.d.m0.c.f.CLASS
                java.util.List r5 = java.util.Collections.emptyList()
                d0.e0.p.d.m0.c.u0 r9 = d0.e0.p.d.m0.c.u0.a
                r7 = 0
                d0.e0.p.d.m0.m.o r8 = d0.e0.p.d.m0.m.f.f3486b
                r0 = r10
                r2 = r11
                r6 = r9
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
                d0.e0.p.d.m0.c.g1.g$a r11 = d0.e0.p.d.m0.c.g1.g.f
                d0.e0.p.d.m0.c.g1.g r11 = r11.getEMPTY()
                r0 = 1
                d0.e0.p.d.m0.c.i1.g r11 = d0.e0.p.d.m0.c.i1.g.create(r10, r11, r0, r9)
                java.util.List r0 = java.util.Collections.emptyList()
                d0.e0.p.d.m0.c.u r1 = d0.e0.p.d.m0.c.t.d
                r11.initialize(r0, r1)
                d0.e0.p.d.m0.g.e r0 = r10.getName()
                java.lang.String r0 = r0.asString()
                d0.e0.p.d.m0.k.a0.i r0 = d0.e0.p.d.m0.n.t.createErrorScope(r0)
                d0.e0.p.d.m0.n.s r1 = new d0.e0.p.d.m0.n.s
                java.lang.String r2 = "<ERROR>"
                d0.e0.p.d.m0.n.u0 r2 = d0.e0.p.d.m0.n.t.b(r2, r10)
                r1.<init>(r2, r0)
                r11.setReturnType(r1)
                java.util.Set r1 = java.util.Collections.singleton(r11)
                r10.initialize(r0, r1, r11)
                return
            L4f:
                r11 = 0
                a(r11)
                r11 = 0
                throw r11
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.t.c.<init>(d0.e0.p.d.m0.g.e):void");
        }

        public static /* synthetic */ void a(int i) {
            String str = (i == 2 || i == 5 || i == 8) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
            Object[] objArr = new Object[(i == 2 || i == 5 || i == 8) ? 2 : 3];
            switch (i) {
                case 1:
                    objArr[0] = "substitutor";
                    break;
                case 2:
                case 5:
                case 8:
                    objArr[0] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorClassDescriptor";
                    break;
                case 3:
                    objArr[0] = "typeArguments";
                    break;
                case 4:
                case 7:
                    objArr[0] = "kotlinTypeRefiner";
                    break;
                case 6:
                    objArr[0] = "typeSubstitution";
                    break;
                default:
                    objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                    break;
            }
            if (i == 2) {
                objArr[1] = "substitute";
            } else if (i == 5 || i == 8) {
                objArr[1] = "getMemberScope";
            } else {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorClassDescriptor";
            }
            switch (i) {
                case 1:
                    objArr[2] = "substitute";
                    break;
                case 2:
                case 5:
                case 8:
                    break;
                case 3:
                case 4:
                case 6:
                case 7:
                    objArr[2] = "getMemberScope";
                    break;
                default:
                    objArr[2] = HookHelper.constructorName;
                    break;
            }
            String format = String.format(str, objArr);
            if (i == 2 || i == 5 || i == 8) {
                throw new IllegalStateException(format);
            }
        }

        @Override // d0.e0.p.d.m0.c.i1.a, d0.e0.p.d.m0.c.i1.u
        public d0.e0.p.d.m0.k.a0.i getMemberScope(z0 z0Var, d0.e0.p.d.m0.n.l1.g gVar) {
            if (z0Var == null) {
                a(6);
                throw null;
            } else if (gVar != null) {
                StringBuilder R = b.d.b.a.a.R("Error scope for class ");
                R.append(getName());
                R.append(" with arguments: ");
                R.append(z0Var);
                d0.e0.p.d.m0.k.a0.i createErrorScope = t.createErrorScope(R.toString());
                if (createErrorScope != null) {
                    return createErrorScope;
                }
                a(8);
                throw null;
            } else {
                a(7);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.c.i1.a, d0.e0.p.d.m0.c.w0
        public d0.e0.p.d.m0.c.e substitute(c1 c1Var) {
            if (c1Var != null) {
                return this;
            }
            a(1);
            throw null;
        }

        @Override // d0.e0.p.d.m0.c.i1.i
        public String toString() {
            return getName().asString();
        }
    }

    /* compiled from: ErrorUtils.java */
    /* loaded from: classes3.dex */
    public static class d implements d0.e0.p.d.m0.k.a0.i {

        /* renamed from: b  reason: collision with root package name */
        public final String f3512b;

        public d(String str, a aVar) {
            if (str != null) {
                this.f3512b = str;
            } else {
                a(0);
                throw null;
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:12:0x0017  */
        /* JADX WARN: Removed duplicated region for block: B:16:0x0022  */
        /* JADX WARN: Removed duplicated region for block: B:17:0x0027  */
        /* JADX WARN: Removed duplicated region for block: B:18:0x002c  */
        /* JADX WARN: Removed duplicated region for block: B:19:0x0031  */
        /* JADX WARN: Removed duplicated region for block: B:20:0x0036  */
        /* JADX WARN: Removed duplicated region for block: B:21:0x0039  */
        /* JADX WARN: Removed duplicated region for block: B:22:0x003e  */
        /* JADX WARN: Removed duplicated region for block: B:25:0x004b  */
        /* JADX WARN: Removed duplicated region for block: B:33:0x0068  */
        /* JADX WARN: Removed duplicated region for block: B:35:0x006d  */
        /* JADX WARN: Removed duplicated region for block: B:36:0x0072  */
        /* JADX WARN: Removed duplicated region for block: B:37:0x0077  */
        /* JADX WARN: Removed duplicated region for block: B:38:0x007c  */
        /* JADX WARN: Removed duplicated region for block: B:39:0x007f  */
        /* JADX WARN: Removed duplicated region for block: B:40:0x0084  */
        /* JADX WARN: Removed duplicated region for block: B:41:0x0087  */
        /* JADX WARN: Removed duplicated region for block: B:42:0x008a  */
        /* JADX WARN: Removed duplicated region for block: B:43:0x008f  */
        /* JADX WARN: Removed duplicated region for block: B:46:0x0099 A[ADDED_TO_REGION] */
        /* JADX WARN: Removed duplicated region for block: B:48:0x009e  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public static /* synthetic */ void a(int r10) {
            /*
                r0 = 18
                r1 = 7
                if (r10 == r1) goto Ld
                if (r10 == r0) goto Ld
                switch(r10) {
                    case 10: goto Ld;
                    case 11: goto Ld;
                    case 12: goto Ld;
                    case 13: goto Ld;
                    default: goto La;
                }
            La:
                java.lang.String r2 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
                goto Lf
            Ld:
                java.lang.String r2 = "@NotNull method %s.%s must not return null"
            Lf:
                r3 = 2
                if (r10 == r1) goto L19
                if (r10 == r0) goto L19
                switch(r10) {
                    case 10: goto L19;
                    case 11: goto L19;
                    case 12: goto L19;
                    case 13: goto L19;
                    default: goto L17;
                }
            L17:
                r4 = 3
                goto L1a
            L19:
                r4 = 2
            L1a:
                java.lang.Object[] r4 = new java.lang.Object[r4]
                java.lang.String r5 = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ErrorScope"
                r6 = 0
                switch(r10) {
                    case 1: goto L3e;
                    case 2: goto L39;
                    case 3: goto L3e;
                    case 4: goto L39;
                    case 5: goto L3e;
                    case 6: goto L39;
                    case 7: goto L36;
                    case 8: goto L3e;
                    case 9: goto L39;
                    case 10: goto L36;
                    case 11: goto L36;
                    case 12: goto L36;
                    case 13: goto L36;
                    case 14: goto L3e;
                    case 15: goto L39;
                    case 16: goto L31;
                    case 17: goto L2c;
                    case 18: goto L36;
                    case 19: goto L3e;
                    case 20: goto L27;
                    default: goto L22;
                }
            L22:
                java.lang.String r7 = "debugMessage"
                r4[r6] = r7
                goto L42
            L27:
                java.lang.String r7 = "p"
                r4[r6] = r7
                goto L42
            L2c:
                java.lang.String r7 = "nameFilter"
                r4[r6] = r7
                goto L42
            L31:
                java.lang.String r7 = "kindFilter"
                r4[r6] = r7
                goto L42
            L36:
                r4[r6] = r5
                goto L42
            L39:
                java.lang.String r7 = "location"
                r4[r6] = r7
                goto L42
            L3e:
                java.lang.String r7 = "name"
                r4[r6] = r7
            L42:
                java.lang.String r6 = "getContributedDescriptors"
                java.lang.String r7 = "getContributedFunctions"
                java.lang.String r8 = "getContributedVariables"
                r9 = 1
                if (r10 == r1) goto L68
                if (r10 == r0) goto L65
                switch(r10) {
                    case 10: goto L62;
                    case 11: goto L5d;
                    case 12: goto L58;
                    case 13: goto L53;
                    default: goto L50;
                }
            L50:
                r4[r9] = r5
                goto L6a
            L53:
                java.lang.String r5 = "getClassifierNames"
                r4[r9] = r5
                goto L6a
            L58:
                java.lang.String r5 = "getVariableNames"
                r4[r9] = r5
                goto L6a
            L5d:
                java.lang.String r5 = "getFunctionNames"
                r4[r9] = r5
                goto L6a
            L62:
                r4[r9] = r7
                goto L6a
            L65:
                r4[r9] = r6
                goto L6a
            L68:
                r4[r9] = r8
            L6a:
                switch(r10) {
                    case 1: goto L8f;
                    case 2: goto L8f;
                    case 3: goto L8a;
                    case 4: goto L8a;
                    case 5: goto L87;
                    case 6: goto L87;
                    case 7: goto L93;
                    case 8: goto L84;
                    case 9: goto L84;
                    case 10: goto L93;
                    case 11: goto L93;
                    case 12: goto L93;
                    case 13: goto L93;
                    case 14: goto L7f;
                    case 15: goto L7f;
                    case 16: goto L7c;
                    case 17: goto L7c;
                    case 18: goto L93;
                    case 19: goto L77;
                    case 20: goto L72;
                    default: goto L6d;
                }
            L6d:
                java.lang.String r5 = "<init>"
                r4[r3] = r5
                goto L93
            L72:
                java.lang.String r5 = "printScopeStructure"
                r4[r3] = r5
                goto L93
            L77:
                java.lang.String r5 = "definitelyDoesNotContainName"
                r4[r3] = r5
                goto L93
            L7c:
                r4[r3] = r6
                goto L93
            L7f:
                java.lang.String r5 = "recordLookup"
                r4[r3] = r5
                goto L93
            L84:
                r4[r3] = r7
                goto L93
            L87:
                r4[r3] = r8
                goto L93
            L8a:
                java.lang.String r5 = "getContributedClassifierIncludeDeprecated"
                r4[r3] = r5
                goto L93
            L8f:
                java.lang.String r5 = "getContributedClassifier"
                r4[r3] = r5
            L93:
                java.lang.String r2 = java.lang.String.format(r2, r4)
                if (r10 == r1) goto La4
                if (r10 == r0) goto La4
                switch(r10) {
                    case 10: goto La4;
                    case 11: goto La4;
                    case 12: goto La4;
                    case 13: goto La4;
                    default: goto L9e;
                }
            L9e:
                java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
                r10.<init>(r2)
                goto La9
            La4:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                r10.<init>(r2)
            La9:
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.t.d.a(int):void");
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Set<d0.e0.p.d.m0.g.e> getClassifierNames() {
            Set<d0.e0.p.d.m0.g.e> emptySet = Collections.emptySet();
            if (emptySet != null) {
                return emptySet;
            }
            a(13);
            throw null;
        }

        @Override // d0.e0.p.d.m0.k.a0.l
        public d0.e0.p.d.m0.c.h getContributedClassifier(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            if (eVar == null) {
                a(1);
                throw null;
            } else if (bVar != null) {
                return t.createErrorClass(eVar.asString());
            } else {
                a(2);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.l
        public Collection<m> getContributedDescriptors(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
            if (dVar == null) {
                a(16);
                throw null;
            } else if (function1 != null) {
                List emptyList = Collections.emptyList();
                if (emptyList != null) {
                    return emptyList;
                }
                a(18);
                throw null;
            } else {
                a(17);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Set<? extends n0> getContributedVariables(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            if (eVar == null) {
                a(5);
                throw null;
            } else if (bVar != null) {
                Set<n0> set = t.f;
                if (set != null) {
                    return set;
                }
                a(7);
                throw null;
            } else {
                a(6);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Set<d0.e0.p.d.m0.g.e> getFunctionNames() {
            Set<d0.e0.p.d.m0.g.e> emptySet = Collections.emptySet();
            if (emptySet != null) {
                return emptySet;
            }
            a(11);
            throw null;
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Set<d0.e0.p.d.m0.g.e> getVariableNames() {
            Set<d0.e0.p.d.m0.g.e> emptySet = Collections.emptySet();
            if (emptySet != null) {
                return emptySet;
            }
            a(12);
            throw null;
        }

        public String toString() {
            return b.d.b.a.a.G(b.d.b.a.a.R("ErrorScope{"), this.f3512b, '}');
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Set<? extends t0> getContributedFunctions(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            if (eVar == null) {
                a(8);
                throw null;
            } else if (bVar != null) {
                d0.e0.p.d.m0.n.m1.a aVar = new d0.e0.p.d.m0.n.m1.a(t.f3510b, this);
                aVar.initialize((q0) null, (q0) null, Collections.emptyList(), Collections.emptyList(), (c0) t.createErrorType("<ERROR FUNCTION RETURN TYPE>"), z.OPEN, d0.e0.p.d.m0.c.t.e);
                Set<? extends t0> singleton = Collections.singleton(aVar);
                if (singleton != null) {
                    return singleton;
                }
                a(10);
                throw null;
            } else {
                a(9);
                throw null;
            }
        }
    }

    /* compiled from: ErrorUtils.java */
    /* loaded from: classes3.dex */
    public static class e implements d0.e0.p.d.m0.k.a0.i {

        /* renamed from: b  reason: collision with root package name */
        public final String f3513b;

        public e(String str, a aVar) {
            if (str != null) {
                this.f3513b = str;
            } else {
                a(0);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            switch (i) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 11:
                case 13:
                    objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                    break;
                case 2:
                case 4:
                case 6:
                case 8:
                case 12:
                    objArr[0] = ModelAuditLogEntry.CHANGE_KEY_LOCATION;
                    break;
                case 9:
                    objArr[0] = "kindFilter";
                    break;
                case 10:
                    objArr[0] = "nameFilter";
                    break;
                case 14:
                    objArr[0] = "p";
                    break;
                default:
                    objArr[0] = "message";
                    break;
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$ThrowingScope";
            switch (i) {
                case 1:
                case 2:
                    objArr[2] = "getContributedClassifier";
                    break;
                case 3:
                case 4:
                    objArr[2] = "getContributedClassifierIncludeDeprecated";
                    break;
                case 5:
                case 6:
                    objArr[2] = "getContributedVariables";
                    break;
                case 7:
                case 8:
                    objArr[2] = "getContributedFunctions";
                    break;
                case 9:
                case 10:
                    objArr[2] = "getContributedDescriptors";
                    break;
                case 11:
                case 12:
                    objArr[2] = "recordLookup";
                    break;
                case 13:
                    objArr[2] = "definitelyDoesNotContainName";
                    break;
                case 14:
                    objArr[2] = "printScopeStructure";
                    break;
                default:
                    objArr[2] = HookHelper.constructorName;
                    break;
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Set<d0.e0.p.d.m0.g.e> getClassifierNames() {
            throw new IllegalStateException();
        }

        @Override // d0.e0.p.d.m0.k.a0.l
        public d0.e0.p.d.m0.c.h getContributedClassifier(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            if (eVar == null) {
                a(1);
                throw null;
            } else if (bVar == null) {
                a(2);
                throw null;
            } else {
                throw new IllegalStateException(this.f3513b + ", required name: " + eVar);
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.l
        public Collection<m> getContributedDescriptors(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
            if (dVar == null) {
                a(9);
                throw null;
            } else if (function1 == null) {
                a(10);
                throw null;
            } else {
                throw new IllegalStateException(this.f3513b);
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Collection<? extends t0> getContributedFunctions(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            if (eVar == null) {
                a(7);
                throw null;
            } else if (bVar == null) {
                a(8);
                throw null;
            } else {
                throw new IllegalStateException(this.f3513b + ", required name: " + eVar);
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Collection<? extends n0> getContributedVariables(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
            if (eVar == null) {
                a(5);
                throw null;
            } else if (bVar == null) {
                a(6);
                throw null;
            } else {
                throw new IllegalStateException(this.f3513b + ", required name: " + eVar);
            }
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Set<d0.e0.p.d.m0.g.e> getFunctionNames() {
            throw new IllegalStateException();
        }

        @Override // d0.e0.p.d.m0.k.a0.i
        public Set<d0.e0.p.d.m0.g.e> getVariableNames() {
            throw new IllegalStateException();
        }

        public String toString() {
            return b.d.b.a.a.G(b.d.b.a.a.R("ThrowingScope{"), this.f3513b, '}');
        }
    }

    /* compiled from: ErrorUtils.java */
    /* loaded from: classes3.dex */
    public static class f implements u0 {
        public static /* synthetic */ void a(int i) {
            String str = (i == 1 || i == 2 || i == 3 || i == 4 || i == 6) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
            Object[] objArr = new Object[(i == 1 || i == 2 || i == 3 || i == 4 || i == 6) ? 2 : 3];
            switch (i) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                    objArr[0] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$UninferredParameterTypeConstructor";
                    break;
                case 5:
                    objArr[0] = "kotlinTypeRefiner";
                    break;
                default:
                    objArr[0] = "descriptor";
                    break;
            }
            if (i == 1) {
                objArr[1] = "getTypeParameterDescriptor";
            } else if (i == 2) {
                objArr[1] = "getParameters";
            } else if (i == 3) {
                objArr[1] = "getSupertypes";
            } else if (i == 4) {
                objArr[1] = "getBuiltIns";
            } else if (i != 6) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils$UninferredParameterTypeConstructor";
            } else {
                objArr[1] = "refine";
            }
            switch (i) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                    break;
                case 5:
                    objArr[2] = "refine";
                    break;
                default:
                    objArr[2] = HookHelper.constructorName;
                    break;
            }
            String format = String.format(str, objArr);
            if (i == 1 || i == 2 || i == 3 || i == 4 || i == 6) {
                throw new IllegalStateException(format);
            }
        }

        @Override // d0.e0.p.d.m0.n.u0
        public h getBuiltIns() {
            h builtIns = d0.e0.p.d.m0.k.x.a.getBuiltIns(null);
            if (builtIns != null) {
                return builtIns;
            }
            a(4);
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public d0.e0.p.d.m0.c.h getDeclarationDescriptor() {
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public List<z0> getParameters() {
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public Collection<c0> getSupertypes() {
            throw null;
        }

        public z0 getTypeParameterDescriptor() {
            a(1);
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public boolean isDenotable() {
            throw null;
        }

        @Override // d0.e0.p.d.m0.n.u0
        public u0 refine(d0.e0.p.d.m0.n.l1.g gVar) {
            if (gVar != null) {
                return this;
            }
            a(5);
            throw null;
        }
    }

    static {
        c cVar = new c(d0.e0.p.d.m0.g.e.special("<ERROR CLASS>"));
        f3510b = cVar;
        j0 createErrorType = createErrorType("<ERROR PROPERTY TYPE>");
        d = createErrorType;
        d0.e0.p.d.m0.c.i1.c0 create = d0.e0.p.d.m0.c.i1.c0.create(cVar, g.f.getEMPTY(), z.OPEN, d0.e0.p.d.m0.c.t.e, true, d0.e0.p.d.m0.g.e.special("<ERROR PROPERTY>"), b.a.DECLARATION, u0.a, false, false, false, false, false, false);
        create.setType(createErrorType, Collections.emptyList(), null, null);
        e = create;
        f = Collections.singleton(create);
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 4 || i == 6 || i == 19) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 4 || i == 6 || i == 19) ? 2 : 3];
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 7:
            case 11:
            case 15:
                objArr[0] = "debugMessage";
                break;
            case 4:
            case 6:
            case 19:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils";
                break;
            case 5:
                objArr[0] = "ownerScope";
                break;
            case 8:
            case 9:
            case 16:
            case 17:
                objArr[0] = "debugName";
                break;
            case 10:
                objArr[0] = "typeConstructor";
                break;
            case 12:
            case 14:
                objArr[0] = "arguments";
                break;
            case 13:
                objArr[0] = "presentableName";
                break;
            case 18:
                objArr[0] = "errorClass";
                break;
            case 20:
                objArr[0] = "typeParameterDescriptor";
                break;
            default:
                objArr[0] = "function";
                break;
        }
        if (i == 4) {
            objArr[1] = "createErrorProperty";
        } else if (i == 6) {
            objArr[1] = "createErrorFunction";
        } else if (i != 19) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/types/ErrorUtils";
        } else {
            objArr[1] = "getErrorModule";
        }
        switch (i) {
            case 1:
                objArr[2] = "createErrorClass";
                break;
            case 2:
            case 3:
                objArr[2] = "createErrorScope";
                break;
            case 4:
            case 6:
            case 19:
                break;
            case 5:
                objArr[2] = "createErrorFunction";
                break;
            case 7:
                objArr[2] = "createErrorType";
                break;
            case 8:
                objArr[2] = "createErrorTypeWithCustomDebugName";
                break;
            case 9:
            case 10:
                objArr[2] = "createErrorTypeWithCustomConstructor";
                break;
            case 11:
            case 12:
                objArr[2] = "createErrorTypeWithArguments";
                break;
            case 13:
            case 14:
                objArr[2] = "createUnresolvedType";
                break;
            case 15:
                objArr[2] = "createErrorTypeConstructor";
                break;
            case 16:
            case 17:
            case 18:
                objArr[2] = "createErrorTypeConstructorWithCustomDebugName";
                break;
            case 20:
                objArr[2] = "createUninferredParameterType";
                break;
            default:
                objArr[2] = "containsErrorTypeInParameters";
                break;
        }
        String format = String.format(str, objArr);
        if (i == 4 || i == 6 || i == 19) {
            throw new IllegalStateException(format);
        }
    }

    public static u0 b(String str, c cVar) {
        if (str == null) {
            a(17);
            throw null;
        } else if (cVar != null) {
            return new b(cVar, str);
        } else {
            a(18);
            throw null;
        }
    }

    public static d0.e0.p.d.m0.c.e createErrorClass(String str) {
        if (str != null) {
            return new c(d0.e0.p.d.m0.g.e.special("<ERROR CLASS: " + str + ">"));
        }
        a(1);
        throw null;
    }

    public static d0.e0.p.d.m0.k.a0.i createErrorScope(String str) {
        if (str != null) {
            return createErrorScope(str, false);
        }
        a(2);
        throw null;
    }

    public static j0 createErrorType(String str) {
        if (str != null) {
            return createErrorTypeWithArguments(str, Collections.emptyList());
        }
        a(7);
        throw null;
    }

    public static u0 createErrorTypeConstructor(String str) {
        if (str != null) {
            return b(b.d.b.a.a.w("[ERROR : ", str, "]"), f3510b);
        }
        a(15);
        throw null;
    }

    public static u0 createErrorTypeConstructorWithCustomDebugName(String str) {
        if (str != null) {
            return b(str, f3510b);
        }
        a(16);
        throw null;
    }

    public static j0 createErrorTypeWithArguments(String str, List<w0> list) {
        if (str == null) {
            a(11);
            throw null;
        } else if (list != null) {
            return new s(createErrorTypeConstructor(str), createErrorScope(str), list, false);
        } else {
            a(12);
            throw null;
        }
    }

    public static j0 createErrorTypeWithCustomConstructor(String str, u0 u0Var) {
        if (str == null) {
            a(9);
            throw null;
        } else if (u0Var != null) {
            return new s(u0Var, createErrorScope(str));
        } else {
            a(10);
            throw null;
        }
    }

    public static j0 createErrorTypeWithCustomDebugName(String str) {
        if (str != null) {
            return createErrorTypeWithCustomConstructor(str, createErrorTypeConstructorWithCustomDebugName(str));
        }
        a(8);
        throw null;
    }

    public static c0 getErrorModule() {
        return a;
    }

    public static boolean isError(m mVar) {
        if (mVar == null) {
            return false;
        }
        return (mVar instanceof c) || (mVar.getContainingDeclaration() instanceof c) || mVar == a;
    }

    public static boolean isUninferredParameter(c0 c0Var) {
        return c0Var != null && (c0Var.getConstructor() instanceof f);
    }

    public static d0.e0.p.d.m0.k.a0.i createErrorScope(String str, boolean z2) {
        if (str == null) {
            a(3);
            throw null;
        } else if (z2) {
            return new e(str, null);
        } else {
            return new d(str, null);
        }
    }
}
