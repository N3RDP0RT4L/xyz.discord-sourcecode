package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.m.f;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.l1.g;
import d0.z.d.m;
import kotlin.jvm.functions.Function0;
/* compiled from: SpecialTypes.kt */
/* loaded from: classes3.dex */
public final class f0 extends k1 {
    public final o k;
    public final Function0<c0> l;
    public final j<c0> m;

    /* compiled from: SpecialTypes.kt */
    /* loaded from: classes3.dex */
    public static final class a extends d0.z.d.o implements Function0<c0> {
        public final /* synthetic */ g $kotlinTypeRefiner;
        public final /* synthetic */ f0 this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(g gVar, f0 f0Var) {
            super(0);
            this.$kotlinTypeRefiner = gVar;
            this.this$0 = f0Var;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final c0 invoke() {
            return this.$kotlinTypeRefiner.refineType((c0) this.this$0.l.invoke());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public f0(o oVar, Function0<? extends c0> function0) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(function0, "computation");
        this.k = oVar;
        this.l = function0;
        this.m = oVar.createLazyValue(function0);
    }

    @Override // d0.e0.p.d.m0.n.k1
    public c0 a() {
        return this.m.invoke();
    }

    @Override // d0.e0.p.d.m0.n.k1
    public boolean isComputed() {
        return ((f.h) this.m).isComputed();
    }

    @Override // d0.e0.p.d.m0.n.c0
    public f0 refine(g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return new f0(this.k, new a(gVar, this));
    }
}
