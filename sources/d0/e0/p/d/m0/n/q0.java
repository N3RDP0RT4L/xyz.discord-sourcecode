package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.g1.i;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.l1.f;
import d0.e0.p.d.m0.n.s0;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypeAliasExpander.kt */
/* loaded from: classes3.dex */
public final class q0 {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public final s0 f3508b;
    public final boolean c;

    /* compiled from: TypeAliasExpander.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final void access$assertRecursionDepth(a aVar, int i, y0 y0Var) {
            Objects.requireNonNull(aVar);
            if (i > 100) {
                throw new AssertionError(m.stringPlus("Too deep recursion while expanding type alias ", y0Var.getName()));
            }
        }

        public final void checkBoundsInTypeAlias(s0 s0Var, c0 c0Var, c0 c0Var2, z0 z0Var, c1 c1Var) {
            m.checkNotNullParameter(s0Var, "reportStrategy");
            m.checkNotNullParameter(c0Var, "unsubstitutedArgument");
            m.checkNotNullParameter(c0Var2, "typeArgument");
            m.checkNotNullParameter(z0Var, "typeParameterDescriptor");
            m.checkNotNullParameter(c1Var, "substitutor");
            for (c0 c0Var3 : z0Var.getUpperBounds()) {
                c0 safeSubstitute = c1Var.safeSubstitute(c0Var3, j1.INVARIANT);
                m.checkNotNullExpressionValue(safeSubstitute, "substitutor.safeSubstitute(bound, Variance.INVARIANT)");
                if (!f.a.isSubtypeOf(c0Var2, safeSubstitute)) {
                    s0Var.boundsViolationInSubstitution(safeSubstitute, c0Var, c0Var2, z0Var);
                }
            }
        }
    }

    static {
        new q0(s0.a.a, false);
    }

    public q0(s0 s0Var, boolean z2) {
        m.checkNotNullParameter(s0Var, "reportStrategy");
        this.f3508b = s0Var;
        this.c = z2;
    }

    public final void a(g gVar, g gVar2) {
        HashSet hashSet = new HashSet();
        Iterator<c> it = gVar.iterator();
        while (it.hasNext()) {
            hashSet.add(it.next().getFqName());
        }
        for (c cVar : gVar2) {
            if (hashSet.contains(cVar.getFqName())) {
                this.f3508b.repeatedAnnotation(cVar);
            }
        }
    }

    public final j0 b(j0 j0Var, g gVar) {
        return e0.isError(j0Var) ? j0Var : a1.replace$default(j0Var, null, c(j0Var, gVar), 1, null);
    }

    public final g c(c0 c0Var, g gVar) {
        return e0.isError(c0Var) ? c0Var.getAnnotations() : i.composeAnnotations(gVar, c0Var.getAnnotations());
    }

    public final j0 d(r0 r0Var, g gVar, boolean z2, int i, boolean z3) {
        w0 e = e(new y0(j1.INVARIANT, r0Var.getDescriptor().getUnderlyingType()), r0Var, null, i);
        c0 type = e.getType();
        m.checkNotNullExpressionValue(type, "expandedProjection.type");
        j0 asSimpleType = a1.asSimpleType(type);
        if (e0.isError(asSimpleType)) {
            return asSimpleType;
        }
        e.getProjectionKind();
        a(asSimpleType.getAnnotations(), gVar);
        j0 makeNullableIfNeeded = e1.makeNullableIfNeeded(b(asSimpleType, gVar), z2);
        m.checkNotNullExpressionValue(makeNullableIfNeeded, "expandedType.combineAnnotations(annotations).let { TypeUtils.makeNullableIfNeeded(it, isNullable) }");
        if (!z3) {
            return makeNullableIfNeeded;
        }
        d0 d0Var = d0.a;
        u0 typeConstructor = r0Var.getDescriptor().getTypeConstructor();
        m.checkNotNullExpressionValue(typeConstructor, "descriptor.typeConstructor");
        return m0.withAbbreviation(makeNullableIfNeeded, d0.simpleTypeWithNonTrivialMemberScope(gVar, typeConstructor, r0Var.getArguments(), z2, i.b.f3433b));
    }

    /* JADX WARN: Removed duplicated region for block: B:71:0x01f7  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0206  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final d0.e0.p.d.m0.n.w0 e(d0.e0.p.d.m0.n.w0 r12, d0.e0.p.d.m0.n.r0 r13, d0.e0.p.d.m0.c.z0 r14, int r15) {
        /*
            Method dump skipped, instructions count: 549
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.n.q0.e(d0.e0.p.d.m0.n.w0, d0.e0.p.d.m0.n.r0, d0.e0.p.d.m0.c.z0, int):d0.e0.p.d.m0.n.w0");
    }

    public final j0 expand(r0 r0Var, g gVar) {
        m.checkNotNullParameter(r0Var, "typeAliasExpansion");
        m.checkNotNullParameter(gVar, "annotations");
        return d(r0Var, gVar, false, 0, true);
    }

    public final j0 f(j0 j0Var, r0 r0Var, int i) {
        u0 constructor = j0Var.getConstructor();
        List<w0> arguments = j0Var.getArguments();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(arguments, 10));
        int i2 = 0;
        for (Object obj : arguments) {
            i2++;
            if (i2 < 0) {
                n.throwIndexOverflow();
            }
            w0 w0Var = (w0) obj;
            w0 e = e(w0Var, r0Var, constructor.getParameters().get(i2), i + 1);
            if (!e.isStarProjection()) {
                e = new y0(e.getProjectionKind(), e1.makeNullableIfNeeded(e.getType(), w0Var.getType().isMarkedNullable()));
            }
            arrayList.add(e);
        }
        return a1.replace$default(j0Var, arrayList, null, 2, null);
    }
}
