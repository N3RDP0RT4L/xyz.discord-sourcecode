package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.z.d.m;
/* compiled from: TypeAliasExpansionReportStrategy.kt */
/* loaded from: classes3.dex */
public interface s0 {

    /* compiled from: TypeAliasExpansionReportStrategy.kt */
    /* loaded from: classes3.dex */
    public static final class a implements s0 {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.n.s0
        public void boundsViolationInSubstitution(c0 c0Var, c0 c0Var2, c0 c0Var3, z0 z0Var) {
            m.checkNotNullParameter(c0Var, "bound");
            m.checkNotNullParameter(c0Var2, "unsubstitutedArgument");
            m.checkNotNullParameter(c0Var3, "argument");
            m.checkNotNullParameter(z0Var, "typeParameter");
        }

        @Override // d0.e0.p.d.m0.n.s0
        public void conflictingProjection(y0 y0Var, z0 z0Var, c0 c0Var) {
            m.checkNotNullParameter(y0Var, "typeAlias");
            m.checkNotNullParameter(c0Var, "substitutedArgument");
        }

        @Override // d0.e0.p.d.m0.n.s0
        public void recursiveTypeAlias(y0 y0Var) {
            m.checkNotNullParameter(y0Var, "typeAlias");
        }

        @Override // d0.e0.p.d.m0.n.s0
        public void repeatedAnnotation(c cVar) {
            m.checkNotNullParameter(cVar, "annotation");
        }
    }

    void boundsViolationInSubstitution(c0 c0Var, c0 c0Var2, c0 c0Var3, z0 z0Var);

    void conflictingProjection(y0 y0Var, z0 z0Var, c0 c0Var);

    void recursiveTypeAlias(y0 y0Var);

    void repeatedAnnotation(c cVar);
}
