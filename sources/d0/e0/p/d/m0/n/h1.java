package d0.e0.p.d.m0.n;

import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.l1.g;
import d0.z.d.m;
import java.util.List;
/* compiled from: ErrorType.kt */
/* loaded from: classes3.dex */
public final class h1 extends s {
    public final String p;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h1(String str, u0 u0Var, i iVar, List<? extends w0> list, boolean z2) {
        super(u0Var, iVar, list, z2, null, 16, null);
        m.checkNotNullParameter(str, "presentableName");
        m.checkNotNullParameter(u0Var, "constructor");
        m.checkNotNullParameter(iVar, "memberScope");
        m.checkNotNullParameter(list, "arguments");
        this.p = str;
    }

    @Override // d0.e0.p.d.m0.n.s
    public String getPresentableName() {
        return this.p;
    }

    @Override // d0.e0.p.d.m0.n.s, d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public h1 refine(g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return this;
    }

    @Override // d0.e0.p.d.m0.n.s, d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public j0 makeNullableAsSpecified(boolean z2) {
        return new h1(getPresentableName(), getConstructor(), getMemberScope(), getArguments(), z2);
    }
}
