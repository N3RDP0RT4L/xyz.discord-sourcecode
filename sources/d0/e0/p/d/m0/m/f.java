package d0.e0.p.d.m0.m;

import andhook.lib.HookHelper;
import d0.g0.w;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.jvm.internal.impl.utils.WrappedValues;
/* compiled from: LockBasedStorageManager.java */
/* loaded from: classes3.dex */
public class f implements d0.e0.p.d.m0.m.o {
    public static final String a = w.substringBeforeLast(f.class.getCanonicalName(), ".", "");

    /* renamed from: b  reason: collision with root package name */
    public static final d0.e0.p.d.m0.m.o f3486b = new a("NO_LOCKS", AbstractC0357f.a, d0.e0.p.d.m0.m.e.f3485b);
    public final d0.e0.p.d.m0.m.l c;
    public final AbstractC0357f d;
    public final String e;

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class a extends f {
        public a(String str, AbstractC0357f fVar, d0.e0.p.d.m0.m.l lVar) {
            super(str, fVar, lVar);
        }

        public static /* synthetic */ void a(int i) {
            String str = i != 1 ? "Argument for @NotNull parameter '%s' of %s.%s must not be null" : "@NotNull method %s.%s must not return null";
            Object[] objArr = new Object[i != 1 ? 3 : 2];
            if (i != 1) {
                objArr[0] = "source";
            } else {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$1";
            }
            if (i != 1) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$1";
            } else {
                objArr[1] = "recursionDetectedDefault";
            }
            if (i != 1) {
                objArr[2] = "recursionDetectedDefault";
            }
            String format = String.format(str, objArr);
            if (i == 1) {
                throw new IllegalStateException(format);
            }
        }

        @Override // d0.e0.p.d.m0.m.f
        public <K, V> o<V> c(String str, K k) {
            o<V> fallThrough = o.fallThrough();
            if (fallThrough != null) {
                return fallThrough;
            }
            a(1);
            throw null;
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public class b extends j<T> {
        public final /* synthetic */ Object m;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(f fVar, f fVar2, Function0 function0, Object obj) {
            super(fVar2, function0);
            this.m = obj;
        }

        @Override // d0.e0.p.d.m0.m.f.h
        public o<T> c(boolean z2) {
            o<T> value = o.value(this.m);
            if (value != 0) {
                return value;
            }
            throw new IllegalStateException(String.format("@NotNull method %s.%s must not return null", "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$4", "recursionDetected"));
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public class c extends k<T> {
        public final /* synthetic */ Function1 n;
        public final /* synthetic */ Function1 o;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(f fVar, f fVar2, Function0 function0, Function1 function1, Function1 function12) {
            super(fVar2, function0);
            this.n = function1;
            this.o = function12;
        }

        public static /* synthetic */ void a(int i) {
            String str = i != 2 ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
            Object[] objArr = new Object[i != 2 ? 2 : 3];
            if (i != 2) {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$5";
            } else {
                objArr[0] = "value";
            }
            if (i != 2) {
                objArr[1] = "recursionDetected";
            } else {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$5";
            }
            if (i == 2) {
                objArr[2] = "doPostCompute";
            }
            String format = String.format(str, objArr);
            if (i == 2) {
                throw new IllegalArgumentException(format);
            }
        }

        @Override // d0.e0.p.d.m0.m.f.h
        public o<T> c(boolean z2) {
            Function1 function1 = this.n;
            if (function1 == null) {
                o<T> c = super.c(z2);
                if (c != 0) {
                    return c;
                }
                a(0);
                throw null;
            }
            o<T> value = o.value(function1.invoke(Boolean.valueOf(z2)));
            if (value != 0) {
                return value;
            }
            a(1);
            throw null;
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class d<K, V> extends e<K, V> implements d0.e0.p.d.m0.m.a<K, V> {
        public d(f fVar, ConcurrentMap concurrentMap, a aVar) {
            super(fVar, concurrentMap, null);
        }

        public static /* synthetic */ void a(int i) {
            String str = i != 3 ? "Argument for @NotNull parameter '%s' of %s.%s must not be null" : "@NotNull method %s.%s must not return null";
            Object[] objArr = new Object[i != 3 ? 3 : 2];
            if (i == 1) {
                objArr[0] = "map";
            } else if (i == 2) {
                objArr[0] = "computation";
            } else if (i != 3) {
                objArr[0] = "storageManager";
            } else {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$CacheWithNotNullValuesBasedOnMemoizedFunction";
            }
            if (i != 3) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$CacheWithNotNullValuesBasedOnMemoizedFunction";
            } else {
                objArr[1] = "computeIfAbsent";
            }
            if (i == 2) {
                objArr[2] = "computeIfAbsent";
            } else if (i != 3) {
                objArr[2] = HookHelper.constructorName;
            }
            String format = String.format(str, objArr);
            if (i == 3) {
                throw new IllegalStateException(format);
            }
        }

        @Override // d0.e0.p.d.m0.m.f.e
        public V computeIfAbsent(K k, Function0<? extends V> function0) {
            if (function0 != null) {
                V v = (V) super.computeIfAbsent(k, function0);
                if (v != null) {
                    return v;
                }
                a(3);
                throw null;
            }
            a(2);
            throw null;
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class e<K, V> extends l<g<K, V>, V> implements d0.e0.p.d.m0.m.b<K, V> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(f fVar, ConcurrentMap concurrentMap, a aVar) {
            super(fVar, concurrentMap, new d0.e0.p.d.m0.m.g());
            if (fVar == null) {
                a(0);
                throw null;
            } else if (concurrentMap != null) {
            } else {
                a(1);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i == 1) {
                objArr[0] = "map";
            } else if (i != 2) {
                objArr[0] = "storageManager";
            } else {
                objArr[0] = "computation";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$CacheWithNullableValuesBasedOnMemoizedFunction";
            if (i != 2) {
                objArr[2] = HookHelper.constructorName;
            } else {
                objArr[2] = "computeIfAbsent";
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        public V computeIfAbsent(K k, Function0<? extends V> function0) {
            if (function0 != null) {
                return invoke(new g(k, function0));
            }
            a(2);
            throw null;
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* renamed from: d0.e0.p.d.m0.m.f$f  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public interface AbstractC0357f {
        public static final AbstractC0357f a = new a();

        /* compiled from: LockBasedStorageManager.java */
        /* renamed from: d0.e0.p.d.m0.m.f$f$a */
        /* loaded from: classes3.dex */
        public static class a implements AbstractC0357f {
            public RuntimeException handleException(Throwable th) {
                if (th == null) {
                    throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", "throwable", "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$ExceptionHandlingStrategy$1", "handleException"));
                }
                throw d0.e0.p.d.m0.p.c.rethrow(th);
            }
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class g<K, V> {
        public final K a;

        /* renamed from: b  reason: collision with root package name */
        public final Function0<? extends V> f3487b;

        public g(K k, Function0<? extends V> function0) {
            this.a = k;
            this.f3487b = function0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return obj != null && g.class == obj.getClass() && this.a.equals(((g) obj).a);
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class h<T> implements d0.e0.p.d.m0.m.k<T> {
        public final f j;
        public final Function0<? extends T> k;
        public volatile Object l;

        public h(f fVar, Function0<? extends T> function0) {
            if (fVar == null) {
                a(0);
                throw null;
            } else if (function0 != null) {
                this.l = n.NOT_COMPUTED;
                this.j = fVar;
                this.k = function0;
            } else {
                a(1);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            String str = (i == 2 || i == 3) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
            Object[] objArr = new Object[(i == 2 || i == 3) ? 2 : 3];
            if (i == 1) {
                objArr[0] = "computable";
            } else if (i == 2 || i == 3) {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedLazyValue";
            } else {
                objArr[0] = "storageManager";
            }
            if (i == 2) {
                objArr[1] = "recursionDetected";
            } else if (i != 3) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedLazyValue";
            } else {
                objArr[1] = "renderDebugInformation";
            }
            if (!(i == 2 || i == 3)) {
                objArr[2] = HookHelper.constructorName;
            }
            String format = String.format(str, objArr);
            if (i == 2 || i == 3) {
                throw new IllegalStateException(format);
            }
        }

        public void b(T t) {
        }

        public o<T> c(boolean z2) {
            o<T> c = this.j.c("in a lazy value", null);
            if (c != null) {
                return c;
            }
            a(2);
            throw null;
        }

        @Override // kotlin.jvm.functions.Function0
        public T invoke() {
            T invoke;
            n nVar = n.RECURSION_WAS_DETECTED;
            n nVar2 = n.COMPUTING;
            Object obj = this.l;
            if (!(obj instanceof n)) {
                return (T) WrappedValues.unescapeThrowable(obj);
            }
            this.j.c.lock();
            try {
                Object obj2 = this.l;
                if (!(obj2 instanceof n)) {
                    invoke = (T) WrappedValues.unescapeThrowable(obj2);
                } else {
                    if (obj2 == nVar2) {
                        this.l = nVar;
                        o<T> c = c(true);
                        if (!c.isFallThrough()) {
                            invoke = c.getValue();
                        }
                    }
                    if (obj2 == nVar) {
                        o<T> c2 = c(false);
                        if (!c2.isFallThrough()) {
                            invoke = c2.getValue();
                        }
                    }
                    this.l = nVar2;
                    invoke = this.k.invoke();
                    b(invoke);
                    this.l = invoke;
                }
                return invoke;
            } finally {
                this.j.c.unlock();
            }
        }

        public boolean isComputed() {
            return (this.l == n.NOT_COMPUTED || this.l == n.COMPUTING) ? false : true;
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static abstract class i<T> extends h<T> {
        public volatile d0.e0.p.d.m0.m.m<T> m;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public i(f fVar, Function0<? extends T> function0) {
            super(fVar, function0);
            if (fVar == null) {
                a(0);
                throw null;
            } else if (function0 != null) {
                this.m = null;
            } else {
                a(1);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "storageManager";
            } else {
                objArr[0] = "computable";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedLazyValueWithPostCompute";
            objArr[2] = HookHelper.constructorName;
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.m.f.h
        public final void b(T t) {
            this.m = new d0.e0.p.d.m0.m.m<>(t);
            try {
                c cVar = (c) this;
                if (t != null) {
                    cVar.o.invoke(t);
                } else {
                    c.a(2);
                    throw null;
                }
            } finally {
                this.m = null;
            }
        }

        @Override // d0.e0.p.d.m0.m.f.h, kotlin.jvm.functions.Function0
        public T invoke() {
            d0.e0.p.d.m0.m.m<T> mVar = this.m;
            if (mVar == null || !mVar.hasValue()) {
                return (T) super.invoke();
            }
            return mVar.getValue();
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class j<T> extends h<T> implements d0.e0.p.d.m0.m.j<T> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public j(f fVar, Function0<? extends T> function0) {
            super(fVar, function0);
            if (fVar == null) {
                a(0);
                throw null;
            } else if (function0 != null) {
            } else {
                a(1);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            String str = i != 2 ? "Argument for @NotNull parameter '%s' of %s.%s must not be null" : "@NotNull method %s.%s must not return null";
            Object[] objArr = new Object[i != 2 ? 3 : 2];
            if (i == 1) {
                objArr[0] = "computable";
            } else if (i != 2) {
                objArr[0] = "storageManager";
            } else {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedNotNullLazyValue";
            }
            if (i != 2) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedNotNullLazyValue";
            } else {
                objArr[1] = "invoke";
            }
            if (i != 2) {
                objArr[2] = HookHelper.constructorName;
            }
            String format = String.format(str, objArr);
            if (i == 2) {
                throw new IllegalStateException(format);
            }
        }

        @Override // d0.e0.p.d.m0.m.f.h, kotlin.jvm.functions.Function0
        public T invoke() {
            T t = (T) super.invoke();
            if (t != null) {
                return t;
            }
            a(2);
            throw null;
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static abstract class k<T> extends i<T> implements d0.e0.p.d.m0.m.j<T> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public k(f fVar, Function0<? extends T> function0) {
            super(fVar, function0);
            if (fVar == null) {
                a(0);
                throw null;
            } else if (function0 != null) {
            } else {
                a(1);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            String str = i != 2 ? "Argument for @NotNull parameter '%s' of %s.%s must not be null" : "@NotNull method %s.%s must not return null";
            Object[] objArr = new Object[i != 2 ? 3 : 2];
            if (i == 1) {
                objArr[0] = "computable";
            } else if (i != 2) {
                objArr[0] = "storageManager";
            } else {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedNotNullLazyValueWithPostCompute";
            }
            if (i != 2) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$LockBasedNotNullLazyValueWithPostCompute";
            } else {
                objArr[1] = "invoke";
            }
            if (i != 2) {
                objArr[2] = HookHelper.constructorName;
            }
            String format = String.format(str, objArr);
            if (i == 2) {
                throw new IllegalStateException(format);
            }
        }

        @Override // d0.e0.p.d.m0.m.f.i, d0.e0.p.d.m0.m.f.h, kotlin.jvm.functions.Function0
        public T invoke() {
            T t = (T) super.invoke();
            if (t != null) {
                return t;
            }
            a(2);
            throw null;
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class l<K, V> implements d0.e0.p.d.m0.m.i<K, V> {
        public final f j;
        public final ConcurrentMap<K, Object> k;
        public final Function1<? super K, ? extends V> l;

        public l(f fVar, ConcurrentMap<K, Object> concurrentMap, Function1<? super K, ? extends V> function1) {
            if (fVar == null) {
                a(0);
                throw null;
            } else if (concurrentMap == null) {
                a(1);
                throw null;
            } else if (function1 != null) {
                this.j = fVar;
                this.k = concurrentMap;
                this.l = function1;
            } else {
                a(2);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            String str = (i == 3 || i == 4) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
            Object[] objArr = new Object[(i == 3 || i == 4) ? 2 : 3];
            if (i == 1) {
                objArr[0] = "map";
            } else if (i == 2) {
                objArr[0] = "compute";
            } else if (i == 3 || i == 4) {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$MapBasedMemoizedFunction";
            } else {
                objArr[0] = "storageManager";
            }
            if (i == 3) {
                objArr[1] = "recursionDetected";
            } else if (i != 4) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$MapBasedMemoizedFunction";
            } else {
                objArr[1] = "raceCondition";
            }
            if (!(i == 3 || i == 4)) {
                objArr[2] = HookHelper.constructorName;
            }
            String format = String.format(str, objArr);
            if (i == 3 || i == 4) {
                throw new IllegalStateException(format);
            }
        }

        public final AssertionError b(K k, Object obj) {
            AssertionError assertionError = new AssertionError("Race condition detected on input " + k + ". Old value is " + obj + " under " + this.j);
            f.d(assertionError);
            return assertionError;
        }

        @Override // kotlin.jvm.functions.Function1
        public V invoke(K k) {
            V v;
            n nVar = n.RECURSION_WAS_DETECTED;
            n nVar2 = n.COMPUTING;
            Object obj = this.k.get(k);
            if (obj != null && obj != nVar2) {
                return (V) WrappedValues.unescapeExceptionOrNull(obj);
            }
            this.j.c.lock();
            try {
                Object obj2 = this.k.get(k);
                if (obj2 == nVar2) {
                    o<V> c = this.j.c("", k);
                    if (c == null) {
                        a(3);
                        throw null;
                    } else if (!c.isFallThrough()) {
                        v = c.getValue();
                        return v;
                    } else {
                        obj2 = nVar;
                    }
                }
                if (obj2 == nVar) {
                    o<V> c2 = this.j.c("", k);
                    if (c2 == null) {
                        a(3);
                        throw null;
                    } else if (!c2.isFallThrough()) {
                        v = c2.getValue();
                        return v;
                    }
                }
                if (obj2 != null) {
                    v = (V) WrappedValues.unescapeExceptionOrNull(obj2);
                    return v;
                }
                this.k.put(k, nVar2);
                V invoke = this.l.invoke(k);
                Object put = this.k.put(k, WrappedValues.escapeNull(invoke));
                if (put == nVar2) {
                    return invoke;
                }
                throw b(k, put);
            } finally {
                this.j.c.unlock();
            }
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class m<K, V> extends l<K, V> implements d0.e0.p.d.m0.m.h<K, V> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public m(f fVar, ConcurrentMap<K, Object> concurrentMap, Function1<? super K, ? extends V> function1) {
            super(fVar, concurrentMap, function1);
            if (fVar == null) {
                a(0);
                throw null;
            } else if (concurrentMap == null) {
                a(1);
                throw null;
            } else if (function1 != null) {
            } else {
                a(2);
                throw null;
            }
        }

        public static /* synthetic */ void a(int i) {
            String str = i != 3 ? "Argument for @NotNull parameter '%s' of %s.%s must not be null" : "@NotNull method %s.%s must not return null";
            Object[] objArr = new Object[i != 3 ? 3 : 2];
            if (i == 1) {
                objArr[0] = "map";
            } else if (i == 2) {
                objArr[0] = "compute";
            } else if (i != 3) {
                objArr[0] = "storageManager";
            } else {
                objArr[0] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$MapBasedMemoizedFunctionToNotNull";
            }
            if (i != 3) {
                objArr[1] = "kotlin/reflect/jvm/internal/impl/storage/LockBasedStorageManager$MapBasedMemoizedFunctionToNotNull";
            } else {
                objArr[1] = "invoke";
            }
            if (i != 3) {
                objArr[2] = HookHelper.constructorName;
            }
            String format = String.format(str, objArr);
            if (i == 3) {
                throw new IllegalStateException(format);
            }
        }

        @Override // d0.e0.p.d.m0.m.f.l, kotlin.jvm.functions.Function1
        public V invoke(K k) {
            V v = (V) super.invoke(k);
            if (v != null) {
                return v;
            }
            a(3);
            throw null;
        }
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public enum n {
        NOT_COMPUTED,
        COMPUTING,
        RECURSION_WAS_DETECTED
    }

    /* compiled from: LockBasedStorageManager.java */
    /* loaded from: classes3.dex */
    public static class o<T> {
        public final T a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f3488b;

        public o(T t, boolean z2) {
            this.a = t;
            this.f3488b = z2;
        }

        public static <T> o<T> fallThrough() {
            return new o<>(null, true);
        }

        public static <T> o<T> value(T t) {
            return new o<>(t, false);
        }

        public T getValue() {
            return this.a;
        }

        public boolean isFallThrough() {
            return this.f3488b;
        }

        public String toString() {
            return isFallThrough() ? "FALL_THROUGH" : String.valueOf(this.a);
        }
    }

    public f(String str, AbstractC0357f fVar, d0.e0.p.d.m0.m.l lVar) {
        if (str == null) {
            a(4);
            throw null;
        } else if (fVar == null) {
            a(5);
            throw null;
        } else if (lVar != null) {
            this.c = lVar;
            this.d = fVar;
            this.e = str;
        } else {
            a(6);
            throw null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:45:0x0085  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x008a  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x008d  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0092  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x0095  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x009a  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x009f  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00a4  */
    /* JADX WARN: Removed duplicated region for block: B:53:0x00a9  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00ae  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00b3  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x00b6  */
    /* JADX WARN: Removed duplicated region for block: B:57:0x00b9  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00be  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static /* synthetic */ void a(int r13) {
        /*
            Method dump skipped, instructions count: 354
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.m.f.a(int):void");
    }

    public static <K> ConcurrentMap<K, Object> b() {
        return new ConcurrentHashMap(3, 1.0f, 2);
    }

    public static <T extends Throwable> T d(T t) {
        if (t != null) {
            StackTraceElement[] stackTrace = t.getStackTrace();
            int length = stackTrace.length;
            int i2 = -1;
            int i3 = 0;
            while (true) {
                if (i3 >= length) {
                    break;
                } else if (!stackTrace[i3].getClassName().startsWith(a)) {
                    i2 = i3;
                    break;
                } else {
                    i3++;
                }
            }
            List subList = Arrays.asList(stackTrace).subList(i2, length);
            t.setStackTrace((StackTraceElement[]) subList.toArray(new StackTraceElement[subList.size()]));
            return t;
        }
        a(36);
        throw null;
    }

    public <K, V> o<V> c(String str, K k2) {
        StringBuilder V = b.d.b.a.a.V("Recursion detected ", str);
        V.append(k2 == null ? "" : b.d.b.a.a.u("on input: ", k2));
        V.append(" under ");
        V.append(this);
        AssertionError assertionError = new AssertionError(V.toString());
        d(assertionError);
        throw assertionError;
    }

    @Override // d0.e0.p.d.m0.m.o
    public <T> T compute(Function0<? extends T> function0) {
        AbstractC0357f.a aVar;
        if (function0 != null) {
            this.c.lock();
            try {
                return function0.invoke();
            } finally {
                try {
                    throw aVar.handleException(th);
                } finally {
                }
            }
        } else {
            a(34);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.m.o
    public <K, V> d0.e0.p.d.m0.m.a<K, V> createCacheWithNotNullValues() {
        return new d(this, b(), null);
    }

    @Override // d0.e0.p.d.m0.m.o
    public <K, V> d0.e0.p.d.m0.m.b<K, V> createCacheWithNullableValues() {
        return new e(this, b(), null);
    }

    @Override // d0.e0.p.d.m0.m.o
    public <T> d0.e0.p.d.m0.m.j<T> createLazyValue(Function0<? extends T> function0) {
        if (function0 != null) {
            return new j(this, function0);
        }
        a(23);
        throw null;
    }

    @Override // d0.e0.p.d.m0.m.o
    public <T> d0.e0.p.d.m0.m.j<T> createLazyValueWithPostCompute(Function0<? extends T> function0, Function1<? super Boolean, ? extends T> function1, Function1<? super T, Unit> function12) {
        if (function0 == null) {
            a(28);
            throw null;
        } else if (function12 != null) {
            return new c(this, this, function0, function1, function12);
        } else {
            a(29);
            throw null;
        }
    }

    @Override // d0.e0.p.d.m0.m.o
    public <K, V> d0.e0.p.d.m0.m.h<K, V> createMemoizedFunction(Function1<? super K, ? extends V> function1) {
        if (function1 != null) {
            d0.e0.p.d.m0.m.h<K, V> createMemoizedFunction = createMemoizedFunction(function1, b());
            if (createMemoizedFunction != null) {
                return createMemoizedFunction;
            }
            a(10);
            throw null;
        }
        a(9);
        throw null;
    }

    @Override // d0.e0.p.d.m0.m.o
    public <K, V> d0.e0.p.d.m0.m.i<K, V> createMemoizedFunctionWithNullableValues(Function1<? super K, ? extends V> function1) {
        if (function1 != null) {
            d0.e0.p.d.m0.m.i<K, V> createMemoizedFunctionWithNullableValues = createMemoizedFunctionWithNullableValues(function1, b());
            if (createMemoizedFunctionWithNullableValues != null) {
                return createMemoizedFunctionWithNullableValues;
            }
            a(20);
            throw null;
        }
        a(19);
        throw null;
    }

    @Override // d0.e0.p.d.m0.m.o
    public <T> d0.e0.p.d.m0.m.k<T> createNullableLazyValue(Function0<? extends T> function0) {
        if (function0 != null) {
            return new h(this, function0);
        }
        a(30);
        throw null;
    }

    @Override // d0.e0.p.d.m0.m.o
    public <T> d0.e0.p.d.m0.m.j<T> createRecursionTolerantLazyValue(Function0<? extends T> function0, T t) {
        if (function0 == null) {
            a(26);
            throw null;
        } else if (t != null) {
            return new b(this, this, function0, t);
        } else {
            a(27);
            throw null;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append("@");
        sb.append(Integer.toHexString(hashCode()));
        sb.append(" (");
        return b.d.b.a.a.H(sb, this.e, ")");
    }

    public <K, V> d0.e0.p.d.m0.m.h<K, V> createMemoizedFunction(Function1<? super K, ? extends V> function1, ConcurrentMap<K, Object> concurrentMap) {
        if (function1 == null) {
            a(14);
            throw null;
        } else if (concurrentMap != null) {
            return new m(this, concurrentMap, function1);
        } else {
            a(15);
            throw null;
        }
    }

    public <K, V> d0.e0.p.d.m0.m.i<K, V> createMemoizedFunctionWithNullableValues(Function1<? super K, ? extends V> function1, ConcurrentMap<K, Object> concurrentMap) {
        if (function1 == null) {
            a(21);
            throw null;
        } else if (concurrentMap != null) {
            return new l(this, concurrentMap, function1);
        } else {
            a(22);
            throw null;
        }
    }

    public f(String str) {
        this(str, (Runnable) null, (Function1<InterruptedException, Unit>) null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public f(java.lang.String r3, java.lang.Runnable r4, kotlin.jvm.functions.Function1<java.lang.InterruptedException, kotlin.Unit> r5) {
        /*
            r2 = this;
            d0.e0.p.d.m0.m.f$f r0 = d0.e0.p.d.m0.m.f.AbstractC0357f.a
            int r1 = d0.e0.p.d.m0.m.l.a
            d0.e0.p.d.m0.m.l$a r1 = d0.e0.p.d.m0.m.l.a.a
            d0.e0.p.d.m0.m.d r4 = r1.simpleLock(r4, r5)
            r2.<init>(r3, r0, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.m.f.<init>(java.lang.String, java.lang.Runnable, kotlin.jvm.functions.Function1):void");
    }
}
