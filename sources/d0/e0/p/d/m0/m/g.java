package d0.e0.p.d.m0.m;

import d0.e0.p.d.m0.m.f;
import kotlin.jvm.functions.Function1;
/* compiled from: LockBasedStorageManager.java */
/* loaded from: classes3.dex */
public class g implements Function1<f.g<K, V>, V> {
    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return invoke((f.g<K, Object>) obj);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [V, java.lang.Object] */
    public V invoke(f.g<K, V> gVar) {
        return gVar.f3487b.invoke();
    }
}
