package d0.e0.p.d.m0.m;
/* compiled from: SingleThreadValue.java */
/* loaded from: classes3.dex */
public class m<T> {
    public final T a;

    /* renamed from: b  reason: collision with root package name */
    public final Thread f3489b = Thread.currentThread();

    public m(T t) {
        this.a = t;
    }

    public T getValue() {
        if (hasValue()) {
            return this.a;
        }
        throw new IllegalStateException("No value in this thread (hasValue should be checked before)");
    }

    public boolean hasValue() {
        return this.f3489b == Thread.currentThread();
    }
}
