package d0.e0.p.d.m0.m;

import d0.z.d.m;
import kotlin.reflect.KProperty;
/* compiled from: storage.kt */
/* loaded from: classes3.dex */
public final class n {
    public static final <T> T getValue(j<? extends T> jVar, Object obj, KProperty<?> kProperty) {
        m.checkNotNullParameter(jVar, "<this>");
        m.checkNotNullParameter(kProperty, "p");
        return jVar.invoke();
    }

    public static final <T> T getValue(k<? extends T> kVar, Object obj, KProperty<?> kProperty) {
        m.checkNotNullParameter(kVar, "<this>");
        m.checkNotNullParameter(kProperty, "p");
        return kVar.invoke();
    }
}
