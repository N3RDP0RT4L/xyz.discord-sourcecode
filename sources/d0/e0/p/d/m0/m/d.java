package d0.e0.p.d.m0.m;

import d0.z.d.m;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: locks.kt */
/* loaded from: classes3.dex */
public class d implements l {

    /* renamed from: b  reason: collision with root package name */
    public final Lock f3484b;

    public d(Lock lock) {
        m.checkNotNullParameter(lock, "lock");
        this.f3484b = lock;
    }

    @Override // d0.e0.p.d.m0.m.l
    public void lock() {
        this.f3484b.lock();
    }

    @Override // d0.e0.p.d.m0.m.l
    public void unlock() {
        this.f3484b.unlock();
    }

    public /* synthetic */ d(Lock lock, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? new ReentrantLock() : lock);
    }
}
