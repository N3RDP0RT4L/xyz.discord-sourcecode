package d0.e0.p.d.m0.e.a.g0;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.e.a.h0.i;
import d0.e0.p.d.m0.e.a.i0.g;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.n;
import d0.e0.p.d.m0.n.j0;
import d0.t.h0;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.util.Collection;
import java.util.Map;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
/* compiled from: JavaAnnotationMapper.kt */
/* loaded from: classes3.dex */
public class b implements c, i {
    public static final /* synthetic */ KProperty<Object>[] a = {a0.property1(new y(a0.getOrCreateKotlinClass(b.class), "type", "getType()Lorg/jetbrains/kotlin/types/SimpleType;"))};

    /* renamed from: b  reason: collision with root package name */
    public final d0.e0.p.d.m0.g.b f3286b;
    public final u0 c;
    public final j d;
    public final d0.e0.p.d.m0.e.a.k0.b e;
    public final boolean f;

    /* compiled from: JavaAnnotationMapper.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<j0> {
        public final /* synthetic */ g $c;
        public final /* synthetic */ b this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(g gVar, b bVar) {
            super(0);
            this.$c = gVar;
            this.this$0 = bVar;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final j0 invoke() {
            j0 defaultType = this.$c.getModule().getBuiltIns().getBuiltInClassByFqName(this.this$0.getFqName()).getDefaultType();
            m.checkNotNullExpressionValue(defaultType, "c.module.builtIns.getBuiltInClassByFqName(fqName).defaultType");
            return defaultType;
        }
    }

    public b(g gVar, d0.e0.p.d.m0.e.a.k0.a aVar, d0.e0.p.d.m0.g.b bVar) {
        Collection<d0.e0.p.d.m0.e.a.k0.b> arguments;
        m.checkNotNullParameter(gVar, "c");
        m.checkNotNullParameter(bVar, "fqName");
        this.f3286b = bVar;
        Boolean bool = null;
        u0 source = aVar == null ? null : gVar.getComponents().getSourceElementFactory().source(aVar);
        if (source == null) {
            source = u0.a;
            m.checkNotNullExpressionValue(source, "NO_SOURCE");
        }
        this.c = source;
        this.d = gVar.getStorageManager().createLazyValue(new a(gVar, this));
        this.e = (aVar == null || (arguments = aVar.getArguments()) == null) ? null : (d0.e0.p.d.m0.e.a.k0.b) u.firstOrNull(arguments);
        this.f = m.areEqual(aVar != null ? Boolean.valueOf(aVar.isIdeExternalAnnotation()) : bool, Boolean.TRUE);
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public Map<e, d0.e0.p.d.m0.k.v.g<?>> getAllValueArguments() {
        return h0.emptyMap();
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public d0.e0.p.d.m0.g.b getFqName() {
        return this.f3286b;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public u0 getSource() {
        return this.c;
    }

    @Override // d0.e0.p.d.m0.e.a.h0.i
    public boolean isIdeExternalAnnotation() {
        return this.f;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public j0 getType() {
        return (j0) n.getValue(this.d, this, a[0]);
    }
}
