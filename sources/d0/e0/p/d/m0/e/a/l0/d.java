package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.e.a.i0.c;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.g1;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.m0;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.w0;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: typeEnhancement.kt */
/* loaded from: classes3.dex */
public final class d {
    public final c a;

    /* compiled from: typeEnhancement.kt */
    /* loaded from: classes3.dex */
    public static class a {
        public final c0 a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3315b;
        public final boolean c;

        public a(c0 c0Var, int i, boolean z2) {
            m.checkNotNullParameter(c0Var, "type");
            this.a = c0Var;
            this.f3315b = i;
            this.c = z2;
        }

        public final int getSubtreeSize() {
            return this.f3315b;
        }

        public c0 getType() {
            return this.a;
        }

        public final c0 getTypeIfChanged() {
            c0 type = getType();
            if (getWereChanges()) {
                return type;
            }
            return null;
        }

        public final boolean getWereChanges() {
            return this.c;
        }
    }

    /* compiled from: typeEnhancement.kt */
    /* loaded from: classes3.dex */
    public static final class b extends a {
        public final j0 d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(j0 j0Var, int i, boolean z2) {
            super(j0Var, i, z2);
            m.checkNotNullParameter(j0Var, "type");
            this.d = j0Var;
        }

        @Override // d0.e0.p.d.m0.e.a.l0.d.a
        public j0 getType() {
            return this.d;
        }
    }

    public d(c cVar) {
        m.checkNotNullParameter(cVar, "javaResolverSettings");
        this.a = cVar;
    }

    public final b a(j0 j0Var, Function1<? super Integer, e> function1, int i, t tVar, boolean z2) {
        int subtreeSize;
        w0 createProjection;
        if (!u.shouldEnhance(tVar) && j0Var.getArguments().isEmpty()) {
            return new b(j0Var, 1, false);
        }
        h declarationDescriptor = j0Var.getConstructor().getDeclarationDescriptor();
        if (declarationDescriptor == null) {
            return new b(j0Var, 1, false);
        }
        e invoke = function1.invoke(Integer.valueOf(i));
        c access$enhanceMutability = x.access$enhanceMutability(declarationDescriptor, invoke, tVar);
        h hVar = (h) access$enhanceMutability.component1();
        g component2 = access$enhanceMutability.component2();
        u0 typeConstructor = hVar.getTypeConstructor();
        m.checkNotNullExpressionValue(typeConstructor, "enhancedClassifier.typeConstructor");
        int i2 = i + 1;
        boolean z3 = component2 != null;
        List<w0> arguments = j0Var.getArguments();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(arguments, 10));
        int i3 = 0;
        for (Object obj : arguments) {
            i3++;
            if (i3 < 0) {
                n.throwIndexOverflow();
            }
            w0 w0Var = (w0) obj;
            if (w0Var.isStarProjection()) {
                subtreeSize = i2 + 1;
                if (function1.invoke(Integer.valueOf(i2)).getNullability() != h.NOT_NULL || z2) {
                    createProjection = e1.makeStarProjection(hVar.getTypeConstructor().getParameters().get(i3));
                    m.checkNotNullExpressionValue(createProjection, "{\n                    TypeUtils.makeStarProjection(enhancedClassifier.typeConstructor.parameters[localArgIndex])\n                }");
                } else {
                    c0 makeNotNullable = d0.e0.p.d.m0.n.o1.a.makeNotNullable(w0Var.getType().unwrap());
                    j1 projectionKind = w0Var.getProjectionKind();
                    m.checkNotNullExpressionValue(projectionKind, "arg.projectionKind");
                    createProjection = d0.e0.p.d.m0.n.o1.a.createProjection(makeNotNullable, projectionKind, typeConstructor.getParameters().get(i3));
                }
            } else {
                a b2 = b(w0Var.getType().unwrap(), function1, i2);
                z3 = z3 || b2.getWereChanges();
                subtreeSize = b2.getSubtreeSize() + i2;
                c0 type = b2.getType();
                j1 projectionKind2 = w0Var.getProjectionKind();
                m.checkNotNullExpressionValue(projectionKind2, "arg.projectionKind");
                createProjection = d0.e0.p.d.m0.n.o1.a.createProjection(type, projectionKind2, typeConstructor.getParameters().get(i3));
            }
            i2 = subtreeSize;
            arrayList.add(createProjection);
        }
        c access$getEnhancedNullability = x.access$getEnhancedNullability(j0Var, invoke, tVar);
        boolean booleanValue = ((Boolean) access$getEnhancedNullability.component1()).booleanValue();
        g component22 = access$getEnhancedNullability.component2();
        int i4 = i2 - i;
        if (!(z3 || component22 != null)) {
            return new b(j0Var, i4, false);
        }
        boolean z4 = false;
        g access$compositeAnnotationsOrSingle = x.access$compositeAnnotationsOrSingle(n.listOfNotNull((Object[]) new g[]{j0Var.getAnnotations(), component2, component22}));
        d0 d0Var = d0.a;
        j0 simpleType$default = d0.simpleType$default(access$compositeAnnotationsOrSingle, typeConstructor, arrayList, booleanValue, null, 16, null);
        i1 i1Var = simpleType$default;
        if (invoke.isNotNullTypeParameter()) {
            if (this.a.getCorrectNullabilityForNotNullTypeParameter()) {
                i1Var = m0.makeSimpleTypeDefinitelyNotNullOrNotNull(simpleType$default, true);
            } else {
                i1Var = new g(simpleType$default);
            }
        }
        if (component22 != null && invoke.isNullabilityQualifierForWarning()) {
            z4 = true;
        }
        if (z4) {
            i1Var = g1.wrapEnhancement(j0Var, i1Var);
        }
        return new b((j0) i1Var, i4, true);
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x006e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final d0.e0.p.d.m0.e.a.l0.d.a b(d0.e0.p.d.m0.n.i1 r12, kotlin.jvm.functions.Function1<? super java.lang.Integer, d0.e0.p.d.m0.e.a.l0.e> r13, int r14) {
        /*
            r11 = this;
            boolean r0 = d0.e0.p.d.m0.n.e0.isError(r12)
            r1 = 0
            r2 = 1
            if (r0 == 0) goto Le
            d0.e0.p.d.m0.e.a.l0.d$a r13 = new d0.e0.p.d.m0.e.a.l0.d$a
            r13.<init>(r12, r2, r1)
            return r13
        Le:
            boolean r0 = r12 instanceof d0.e0.p.d.m0.n.v
            if (r0 == 0) goto L9c
            boolean r0 = r12 instanceof d0.e0.p.d.m0.n.i0
            r9 = r12
            d0.e0.p.d.m0.n.v r9 = (d0.e0.p.d.m0.n.v) r9
            d0.e0.p.d.m0.n.j0 r4 = r9.getLowerBound()
            d0.e0.p.d.m0.e.a.l0.t r7 = d0.e0.p.d.m0.e.a.l0.t.FLEXIBLE_LOWER
            r3 = r11
            r5 = r13
            r6 = r14
            r8 = r0
            d0.e0.p.d.m0.e.a.l0.d$b r10 = r3.a(r4, r5, r6, r7, r8)
            d0.e0.p.d.m0.n.j0 r4 = r9.getUpperBound()
            d0.e0.p.d.m0.e.a.l0.t r7 = d0.e0.p.d.m0.e.a.l0.t.FLEXIBLE_UPPER
            d0.e0.p.d.m0.e.a.l0.d$b r13 = r3.a(r4, r5, r6, r7, r8)
            int r14 = r10.getSubtreeSize()
            int r0 = r13.getSubtreeSize()
            boolean r14 = r10.getWereChanges()
            if (r14 != 0) goto L43
            boolean r14 = r13.getWereChanges()
            if (r14 == 0) goto L44
        L43:
            r1 = 1
        L44:
            d0.e0.p.d.m0.n.j0 r14 = r10.getType()
            d0.e0.p.d.m0.n.j0 r0 = r13.getType()
            d0.e0.p.d.m0.n.c0 r0 = d0.e0.p.d.m0.n.g1.getEnhancement(r0)
            d0.e0.p.d.m0.n.c0 r14 = d0.e0.p.d.m0.n.g1.getEnhancement(r14)
            if (r14 != 0) goto L5b
            if (r0 != 0) goto L5a
            r14 = 0
            goto L6c
        L5a:
            r14 = r0
        L5b:
            if (r0 != 0) goto L5e
            goto L6c
        L5e:
            d0.e0.p.d.m0.n.d0 r2 = d0.e0.p.d.m0.n.d0.a
            d0.e0.p.d.m0.n.j0 r14 = d0.e0.p.d.m0.n.y.lowerIfFlexible(r14)
            d0.e0.p.d.m0.n.j0 r0 = d0.e0.p.d.m0.n.y.upperIfFlexible(r0)
            d0.e0.p.d.m0.n.i1 r14 = d0.e0.p.d.m0.n.d0.flexibleType(r14, r0)
        L6c:
            if (r1 == 0) goto L92
            boolean r12 = r12 instanceof d0.e0.p.d.m0.e.a.i0.m.g
            if (r12 == 0) goto L80
            d0.e0.p.d.m0.e.a.i0.m.g r12 = new d0.e0.p.d.m0.e.a.i0.m.g
            d0.e0.p.d.m0.n.j0 r0 = r10.getType()
            d0.e0.p.d.m0.n.j0 r13 = r13.getType()
            r12.<init>(r0, r13)
            goto L8e
        L80:
            d0.e0.p.d.m0.n.d0 r12 = d0.e0.p.d.m0.n.d0.a
            d0.e0.p.d.m0.n.j0 r12 = r10.getType()
            d0.e0.p.d.m0.n.j0 r13 = r13.getType()
            d0.e0.p.d.m0.n.i1 r12 = d0.e0.p.d.m0.n.d0.flexibleType(r12, r13)
        L8e:
            d0.e0.p.d.m0.n.i1 r12 = d0.e0.p.d.m0.n.g1.wrapEnhancement(r12, r14)
        L92:
            d0.e0.p.d.m0.e.a.l0.d$a r13 = new d0.e0.p.d.m0.e.a.l0.d$a
            int r14 = r10.getSubtreeSize()
            r13.<init>(r12, r14, r1)
            goto Lad
        L9c:
            boolean r0 = r12 instanceof d0.e0.p.d.m0.n.j0
            if (r0 == 0) goto Lae
            r2 = r12
            d0.e0.p.d.m0.n.j0 r2 = (d0.e0.p.d.m0.n.j0) r2
            d0.e0.p.d.m0.e.a.l0.t r5 = d0.e0.p.d.m0.e.a.l0.t.INFLEXIBLE
            r6 = 0
            r1 = r11
            r3 = r13
            r4 = r14
            d0.e0.p.d.m0.e.a.l0.d$b r13 = r1.a(r2, r3, r4, r5, r6)
        Lad:
            return r13
        Lae:
            kotlin.NoWhenBranchMatchedException r12 = new kotlin.NoWhenBranchMatchedException
            r12.<init>()
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.l0.d.b(d0.e0.p.d.m0.n.i1, kotlin.jvm.functions.Function1, int):d0.e0.p.d.m0.e.a.l0.d$a");
    }

    public final c0 enhance(c0 c0Var, Function1<? super Integer, e> function1) {
        m.checkNotNullParameter(c0Var, "<this>");
        m.checkNotNullParameter(function1, "qualifiers");
        return b(c0Var.unwrap(), function1, 0).getTypeIfChanged();
    }
}
