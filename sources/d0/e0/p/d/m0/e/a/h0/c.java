package d0.e0.p.d.m0.e.a.h0;

import andhook.lib.HookHelper;
import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.i1.g;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.k.d;
import d0.e0.p.d.m0.n.c0;
import java.util.List;
import kotlin.Pair;
/* compiled from: JavaClassConstructorDescriptor.java */
/* loaded from: classes3.dex */
public class c extends g implements b {
    public Boolean O;
    public Boolean P;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c(e eVar, c cVar, d0.e0.p.d.m0.c.g1.g gVar, boolean z2, b.a aVar, u0 u0Var) {
        super(eVar, cVar, gVar, z2, aVar, u0Var);
        if (eVar == null) {
            a(0);
            throw null;
        } else if (gVar == null) {
            a(1);
            throw null;
        } else if (aVar == null) {
            a(2);
            throw null;
        } else if (u0Var != null) {
            this.O = null;
            this.P = null;
        } else {
            a(3);
            throw null;
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 11 || i == 18) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 11 || i == 18) ? 2 : 3];
        switch (i) {
            case 1:
            case 5:
            case 9:
            case 15:
                objArr[0] = "annotations";
                break;
            case 2:
            case 8:
            case 13:
                objArr[0] = "kind";
                break;
            case 3:
            case 6:
            case 10:
                objArr[0] = "source";
                break;
            case 4:
            default:
                objArr[0] = "containingDeclaration";
                break;
            case 7:
            case 12:
                objArr[0] = "newOwner";
                break;
            case 11:
            case 18:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaClassConstructorDescriptor";
                break;
            case 14:
                objArr[0] = "sourceElement";
                break;
            case 16:
                objArr[0] = "enhancedValueParametersData";
                break;
            case 17:
                objArr[0] = "enhancedReturnType";
                break;
        }
        if (i == 11) {
            objArr[1] = "createSubstitutedCopy";
        } else if (i != 18) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/java/descriptors/JavaClassConstructorDescriptor";
        } else {
            objArr[1] = "enhance";
        }
        switch (i) {
            case 4:
            case 5:
            case 6:
                objArr[2] = "createJavaConstructor";
                break;
            case 7:
            case 8:
            case 9:
            case 10:
                objArr[2] = "createSubstitutedCopy";
                break;
            case 11:
            case 18:
                break;
            case 12:
            case 13:
            case 14:
            case 15:
                objArr[2] = "createDescriptor";
                break;
            case 16:
            case 17:
                objArr[2] = "enhance";
                break;
            default:
                objArr[2] = HookHelper.constructorName;
                break;
        }
        String format = String.format(str, objArr);
        if (i == 11 || i == 18) {
            throw new IllegalStateException(format);
        }
    }

    public static c createJavaConstructor(e eVar, d0.e0.p.d.m0.c.g1.g gVar, boolean z2, u0 u0Var) {
        if (eVar == null) {
            a(4);
            throw null;
        } else if (gVar == null) {
            a(5);
            throw null;
        } else if (u0Var != null) {
            return new c(eVar, null, gVar, z2, b.a.DECLARATION, u0Var);
        } else {
            a(6);
            throw null;
        }
    }

    /* renamed from: f */
    public c e(m mVar, x xVar, b.a aVar, d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.c.g1.g gVar, u0 u0Var) {
        if (mVar == null) {
            a(7);
            throw null;
        } else if (aVar == null) {
            a(8);
            throw null;
        } else if (gVar == null) {
            a(9);
            throw null;
        } else if (u0Var == null) {
            a(10);
            throw null;
        } else if (aVar == b.a.DECLARATION || aVar == b.a.SYNTHESIZED) {
            e eVar2 = (e) mVar;
            c cVar = (c) xVar;
            if (eVar2 == null) {
                a(12);
                throw null;
            } else if (aVar == null) {
                a(13);
                throw null;
            } else if (u0Var == null) {
                a(14);
                throw null;
            } else if (gVar != null) {
                c cVar2 = new c(eVar2, cVar, gVar, this.N, aVar, u0Var);
                cVar2.setHasStableParameterNames(hasStableParameterNames());
                cVar2.setHasSynthesizedParameterNames(hasSynthesizedParameterNames());
                return cVar2;
            } else {
                a(15);
                throw null;
            }
        } else {
            throw new IllegalStateException("Attempt at creating a constructor that is not a declaration: \ncopy from: " + this + "\nnewOwner: " + mVar + "\nkind: " + aVar);
        }
    }

    @Override // d0.e0.p.d.m0.c.i1.q
    public boolean hasStableParameterNames() {
        return this.O.booleanValue();
    }

    @Override // d0.e0.p.d.m0.c.i1.q, d0.e0.p.d.m0.c.a
    public boolean hasSynthesizedParameterNames() {
        return this.P.booleanValue();
    }

    @Override // d0.e0.p.d.m0.c.i1.q
    public void setHasStableParameterNames(boolean z2) {
        this.O = Boolean.valueOf(z2);
    }

    @Override // d0.e0.p.d.m0.c.i1.q
    public void setHasSynthesizedParameterNames(boolean z2) {
        this.P = Boolean.valueOf(z2);
    }

    @Override // d0.e0.p.d.m0.e.a.h0.b
    public c enhance(c0 c0Var, List<l> list, c0 c0Var2, Pair<a.AbstractC0290a<?>, ?> pair) {
        q0 q0Var = null;
        if (list == null) {
            a(16);
            throw null;
        } else if (c0Var2 != null) {
            c f = e(getContainingDeclaration(), null, getKind(), null, getAnnotations(), getSource());
            if (c0Var != null) {
                q0Var = d.createExtensionReceiverParameterForCallable(f, c0Var, d0.e0.p.d.m0.c.g1.g.f.getEMPTY());
            }
            f.initialize(q0Var, getDispatchReceiverParameter(), getTypeParameters(), k.copyValueParameters(list, getValueParameters(), f), c0Var2, getModality(), getVisibility());
            if (pair != null) {
                f.putInUserDataMap(pair.getFirst(), pair.getSecond());
            }
            return f;
        } else {
            a(17);
            throw null;
        }
    }
}
