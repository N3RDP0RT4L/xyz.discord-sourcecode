package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.c;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
/* compiled from: BuiltinSpecialProperties.kt */
/* loaded from: classes3.dex */
public final class j {
    public static final b access$child(b bVar, String str) {
        b child = bVar.child(e.identifier(str));
        m.checkNotNullExpressionValue(child, "child(Name.identifier(name))");
        return child;
    }

    public static final b access$childSafe(c cVar, String str) {
        b safe = cVar.child(e.identifier(str)).toSafe();
        m.checkNotNullExpressionValue(safe, "child(Name.identifier(name)).toSafe()");
        return safe;
    }
}
