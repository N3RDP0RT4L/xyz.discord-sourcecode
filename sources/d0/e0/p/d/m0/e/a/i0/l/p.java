package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.p.b;
import d0.f0.q;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import kotlin.jvm.functions.Function1;
/* compiled from: LazyJavaStaticClassScope.kt */
/* loaded from: classes3.dex */
public final class p implements b.c<e> {
    public static final p a = new p();

    /* compiled from: LazyJavaStaticClassScope.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<c0, e> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final e invoke(c0 c0Var) {
            h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
            if (declarationDescriptor instanceof e) {
                return (e) declarationDescriptor;
            }
            return null;
        }
    }

    public final Iterable<e> getNeighbors(e eVar) {
        Collection<c0> supertypes = eVar.getTypeConstructor().getSupertypes();
        m.checkNotNullExpressionValue(supertypes, "it.typeConstructor.supertypes");
        return q.asIterable(q.mapNotNull(u.asSequence(supertypes), a.j));
    }
}
