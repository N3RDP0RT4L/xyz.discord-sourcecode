package d0.e0.p.d.m0.e.a.l0;

import d0.z.d.m;
/* compiled from: TypeComponentPosition.kt */
/* loaded from: classes3.dex */
public final class u {
    public static final boolean shouldEnhance(t tVar) {
        m.checkNotNullParameter(tVar, "<this>");
        return tVar != t.INFLEXIBLE;
    }
}
