package d0.e0.p.d.m0.e.a.i0;

import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.e.a.k0.d;
import d0.z.d.m;
/* compiled from: LazyJavaAnnotations.kt */
/* loaded from: classes3.dex */
public final class e {
    public static final g resolveAnnotations(g gVar, d dVar) {
        m.checkNotNullParameter(gVar, "<this>");
        m.checkNotNullParameter(dVar, "annotationsOwner");
        return new d(gVar, dVar, false, 4, null);
    }
}
