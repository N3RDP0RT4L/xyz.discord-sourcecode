package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.b.c;
import d0.e0.p.d.m0.b.d;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.v;
import d0.e0.p.d.m0.k.e;
/* compiled from: DescriptorsJvmAbiUtil.java */
/* loaded from: classes3.dex */
public final class n {
    public static /* synthetic */ void a(int i) {
        Object[] objArr = new Object[3];
        if (i == 1 || i == 2) {
            objArr[0] = "companionObject";
        } else if (i != 3) {
            objArr[0] = "propertyDescriptor";
        } else {
            objArr[0] = "memberDescriptor";
        }
        objArr[1] = "kotlin/reflect/jvm/internal/impl/load/java/DescriptorsJvmAbiUtil";
        if (i == 1) {
            objArr[2] = "isClassCompanionObjectWithBackingFieldsInOuter";
        } else if (i == 2) {
            objArr[2] = "isMappedIntrinsicCompanionObject";
        } else if (i != 3) {
            objArr[2] = "isPropertyWithBackingFieldInOuterClass";
        } else {
            objArr[2] = "hasJvmFieldAnnotation";
        }
        throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
    }

    public static boolean hasJvmFieldAnnotation(b bVar) {
        v backingField;
        if (bVar == null) {
            a(3);
            throw null;
        } else if (!(bVar instanceof n0) || (backingField = ((n0) bVar).getBackingField()) == null || !backingField.getAnnotations().hasAnnotation(z.f3332b)) {
            return bVar.getAnnotations().hasAnnotation(z.f3332b);
        } else {
            return true;
        }
    }

    public static boolean isClassCompanionObjectWithBackingFieldsInOuter(m mVar) {
        if (mVar != null) {
            return e.isCompanionObject(mVar) && e.isClassOrEnumClass(mVar.getContainingDeclaration()) && !isMappedIntrinsicCompanionObject((d0.e0.p.d.m0.c.e) mVar);
        }
        a(1);
        throw null;
    }

    public static boolean isMappedIntrinsicCompanionObject(d0.e0.p.d.m0.c.e eVar) {
        if (eVar != null) {
            return d.isMappedIntrinsicCompanionObject(c.a, eVar);
        }
        a(2);
        throw null;
    }

    public static boolean isPropertyWithBackingFieldInOuterClass(n0 n0Var) {
        if (n0Var == null) {
            a(0);
            throw null;
        } else if (n0Var.getKind() == b.a.FAKE_OVERRIDE) {
            return false;
        } else {
            if (isClassCompanionObjectWithBackingFieldsInOuter(n0Var.getContainingDeclaration())) {
                return true;
            }
            return e.isCompanionObject(n0Var.getContainingDeclaration()) && hasJvmFieldAnnotation(n0Var);
        }
    }
}
