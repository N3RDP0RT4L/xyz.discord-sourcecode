package d0.e0.p.d.m0.e.a.k0;
/* compiled from: javaLoading.kt */
/* loaded from: classes3.dex */
public final class p {
    /* JADX WARN: Code restructure failed: missing block: B:13:0x0039, code lost:
        if (r0.equals("hashCode") == false) goto L33;
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x0086, code lost:
        if (r0.equals("toString") == false) goto L33;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x008a, code lost:
        r5 = r5.getValueParameters().isEmpty();
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final boolean isObjectMethodInInterface(d0.e0.p.d.m0.e.a.k0.q r5) {
        /*
            java.lang.String r0 = "<this>"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            d0.e0.p.d.m0.e.a.k0.g r0 = r5.getContainingClass()
            boolean r0 = r0.isInterface()
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L95
            boolean r0 = r5 instanceof d0.e0.p.d.m0.e.a.k0.r
            if (r0 == 0) goto L95
            d0.e0.p.d.m0.e.a.k0.r r5 = (d0.e0.p.d.m0.e.a.k0.r) r5
            d0.e0.p.d.m0.g.e r0 = r5.getName()
            java.lang.String r0 = r0.asString()
            int r3 = r0.hashCode()
            r4 = -1776922004(0xffffffff9616526c, float:-1.2142911E-25)
            if (r3 == r4) goto L80
            r4 = -1295482945(0xffffffffb2c87fbf, float:-2.3341157E-8)
            if (r3 == r4) goto L3c
            r4 = 147696667(0x8cdac1b, float:1.23784505E-33)
            if (r3 == r4) goto L33
            goto L88
        L33:
            java.lang.String r3 = "hashCode"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L8a
            goto L88
        L3c:
            java.lang.String r3 = "equals"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L45
            goto L88
        L45:
            java.util.List r5 = r5.getValueParameters()
            java.lang.Object r5 = d0.t.u.singleOrNull(r5)
            d0.e0.p.d.m0.e.a.k0.a0 r5 = (d0.e0.p.d.m0.e.a.k0.a0) r5
            r0 = 0
            if (r5 != 0) goto L54
            r5 = r0
            goto L58
        L54:
            d0.e0.p.d.m0.e.a.k0.x r5 = r5.getType()
        L58:
            boolean r3 = r5 instanceof d0.e0.p.d.m0.e.a.k0.j
            if (r3 == 0) goto L5f
            r0 = r5
            d0.e0.p.d.m0.e.a.k0.j r0 = (d0.e0.p.d.m0.e.a.k0.j) r0
        L5f:
            if (r0 != 0) goto L62
            goto L88
        L62:
            d0.e0.p.d.m0.e.a.k0.i r5 = r0.getClassifier()
            boolean r0 = r5 instanceof d0.e0.p.d.m0.e.a.k0.g
            if (r0 == 0) goto L88
            d0.e0.p.d.m0.e.a.k0.g r5 = (d0.e0.p.d.m0.e.a.k0.g) r5
            d0.e0.p.d.m0.g.b r5 = r5.getFqName()
            if (r5 == 0) goto L88
            java.lang.String r5 = r5.asString()
            java.lang.String r0 = "java.lang.Object"
            boolean r5 = d0.z.d.m.areEqual(r5, r0)
            if (r5 == 0) goto L88
            r5 = 1
            goto L92
        L80:
            java.lang.String r3 = "toString"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L8a
        L88:
            r5 = 0
            goto L92
        L8a:
            java.util.List r5 = r5.getValueParameters()
            boolean r5 = r5.isEmpty()
        L92:
            if (r5 == 0) goto L95
            goto L96
        L95:
            r1 = 0
        L96:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.k0.p.isObjectMethodInInterface(d0.e0.p.d.m0.e.a.k0.q):boolean");
    }
}
