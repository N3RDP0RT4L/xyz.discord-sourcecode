package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.g1;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.j;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.m;
import d0.e0.p.d.m0.n.o1.a;
import d0.e0.p.d.m0.n.v;
/* compiled from: typeEnhancement.kt */
/* loaded from: classes3.dex */
public final class g extends m implements j {
    public final j0 k;

    public g(j0 j0Var) {
        d0.z.d.m.checkNotNullParameter(j0Var, "delegate");
        this.k = j0Var;
    }

    public final j0 a(j0 j0Var) {
        j0 makeNullableAsSpecified = j0Var.makeNullableAsSpecified(false);
        return !a.isTypeParameter(j0Var) ? makeNullableAsSpecified : new g(makeNullableAsSpecified);
    }

    @Override // d0.e0.p.d.m0.n.m
    public j0 getDelegate() {
        return this.k;
    }

    @Override // d0.e0.p.d.m0.n.m, d0.e0.p.d.m0.n.c0
    public boolean isMarkedNullable() {
        return false;
    }

    @Override // d0.e0.p.d.m0.n.j
    public boolean isTypeVariable() {
        return true;
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public j0 makeNullableAsSpecified(boolean z2) {
        return z2 ? this.k.makeNullableAsSpecified(true) : this;
    }

    @Override // d0.e0.p.d.m0.n.j
    public c0 substitutionResult(c0 c0Var) {
        d0.z.d.m.checkNotNullParameter(c0Var, "replacement");
        i1 unwrap = c0Var.unwrap();
        if (!a.isTypeParameter(unwrap) && !e1.isNullableType(unwrap)) {
            return unwrap;
        }
        if (unwrap instanceof j0) {
            return a((j0) unwrap);
        }
        if (unwrap instanceof v) {
            d0 d0Var = d0.a;
            v vVar = (v) unwrap;
            return g1.wrapEnhancement(d0.flexibleType(a(vVar.getLowerBound()), a(vVar.getUpperBound())), g1.getEnhancement(unwrap));
        }
        throw new IllegalStateException(d0.z.d.m.stringPlus("Incorrect type: ", unwrap).toString());
    }

    @Override // d0.e0.p.d.m0.n.m
    public g replaceDelegate(j0 j0Var) {
        d0.z.d.m.checkNotNullParameter(j0Var, "delegate");
        return new g(j0Var);
    }

    @Override // d0.e0.p.d.m0.n.j0, d0.e0.p.d.m0.n.i1
    public g replaceAnnotations(d0.e0.p.d.m0.c.g1.g gVar) {
        d0.z.d.m.checkNotNullParameter(gVar, "newAnnotations");
        return new g(this.k.replaceAnnotations(gVar));
    }
}
