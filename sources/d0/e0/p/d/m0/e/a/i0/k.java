package d0.e0.p.d.m0.e.a.i0;

import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.k0.y;
import d0.z.d.m;
/* compiled from: resolvers.kt */
/* loaded from: classes3.dex */
public interface k {

    /* compiled from: resolvers.kt */
    /* loaded from: classes3.dex */
    public static final class a implements k {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.e.a.i0.k
        public z0 resolveTypeParameter(y yVar) {
            m.checkNotNullParameter(yVar, "javaTypeParameter");
            return null;
        }
    }

    z0 resolveTypeParameter(y yVar);
}
