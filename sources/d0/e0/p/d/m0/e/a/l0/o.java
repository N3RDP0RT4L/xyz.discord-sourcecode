package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
import kotlin.jvm.functions.Function1;
/* compiled from: signatureEnhancement.kt */
/* loaded from: classes3.dex */
public final class o extends d0.z.d.o implements Function1<b, c0> {
    public static final o j = new o();

    public o() {
        super(1);
    }

    public final c0 invoke(b bVar) {
        m.checkNotNullParameter(bVar, "it");
        c0 returnType = bVar.getReturnType();
        m.checkNotNull(returnType);
        return returnType;
    }
}
