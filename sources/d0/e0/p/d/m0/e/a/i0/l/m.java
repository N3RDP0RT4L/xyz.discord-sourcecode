package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.t0;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: LazyJavaScope.kt */
/* loaded from: classes3.dex */
public final class m extends o implements Function1<t0, a> {
    public static final m j = new m();

    public m() {
        super(1);
    }

    public final a invoke(t0 t0Var) {
        d0.z.d.m.checkNotNullParameter(t0Var, "<this>");
        return t0Var;
    }
}
