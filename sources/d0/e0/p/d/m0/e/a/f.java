package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.k.x.a;
/* compiled from: AnnotationTypeQualifierResolver.kt */
/* loaded from: classes3.dex */
public final class f {
    public static final boolean access$isAnnotatedWithTypeQualifier(e eVar) {
        return b.getBUILT_IN_TYPE_QUALIFIER_FQ_NAMES().contains(a.getFqNameSafe(eVar)) || eVar.getAnnotations().hasAnnotation(b.getTYPE_QUALIFIER_FQNAME());
    }
}
