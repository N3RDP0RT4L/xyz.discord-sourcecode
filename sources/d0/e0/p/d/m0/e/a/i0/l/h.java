package d0.e0.p.d.m0.e.a.i0.l;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.i1.d0;
import d0.e0.p.d.m0.c.i1.e0;
import d0.e0.p.d.m0.c.i1.l0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.f0;
import d0.e0.p.d.m0.e.a.g0.g;
import d0.e0.p.d.m0.e.a.g0.j;
import d0.e0.p.d.m0.e.a.i0.l.k;
import d0.e0.p.d.m0.e.a.k0.k;
import d0.e0.p.d.m0.e.a.k0.n;
import d0.e0.p.d.m0.e.a.k0.r;
import d0.e0.p.d.m0.e.a.k0.w;
import d0.e0.p.d.m0.e.a.k0.x;
import d0.e0.p.d.m0.e.a.k0.y;
import d0.e0.p.d.m0.e.a.l0.l;
import d0.e0.p.d.m0.e.a.s;
import d0.e0.p.d.m0.e.a.z;
import d0.e0.p.d.m0.e.b.u;
import d0.e0.p.d.m0.k.k;
import d0.e0.p.d.m0.l.b.p;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.p.j;
import d0.t.g0;
import d0.t.o0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: LazyJavaClassMemberScope.kt */
/* loaded from: classes3.dex */
public final class h extends k {
    public final d0.e0.p.d.m0.c.e n;
    public final d0.e0.p.d.m0.e.a.k0.g o;
    public final boolean p;
    public final j<List<d0.e0.p.d.m0.c.d>> q;
    public final j<Set<d0.e0.p.d.m0.g.e>> r;

    /* renamed from: s  reason: collision with root package name */
    public final j<Map<d0.e0.p.d.m0.g.e, n>> f3304s;
    public final d0.e0.p.d.m0.m.i<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.c.i1.h> t;

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class a extends d0.z.d.j implements Function1<d0.e0.p.d.m0.g.e, Collection<? extends t0>> {
        public a(h hVar) {
            super(1, hVar);
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public final String getName() {
            return "searchMethodsByNameWithoutBuiltinMagic";
        }

        @Override // d0.z.d.d
        public final KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinClass(h.class);
        }

        @Override // d0.z.d.d
        public final String getSignature() {
            return "searchMethodsByNameWithoutBuiltinMagic(Lorg/jetbrains/kotlin/name/Name;)Ljava/util/Collection;";
        }

        public final Collection<t0> invoke(d0.e0.p.d.m0.g.e eVar) {
            m.checkNotNullParameter(eVar, "p0");
            return h.access$searchMethodsByNameWithoutBuiltinMagic((h) this.receiver, eVar);
        }
    }

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class b extends d0.z.d.j implements Function1<d0.e0.p.d.m0.g.e, Collection<? extends t0>> {
        public b(h hVar) {
            super(1, hVar);
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public final String getName() {
            return "searchMethodsInSupertypesWithoutBuiltinMagic";
        }

        @Override // d0.z.d.d
        public final KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinClass(h.class);
        }

        @Override // d0.z.d.d
        public final String getSignature() {
            return "searchMethodsInSupertypesWithoutBuiltinMagic(Lorg/jetbrains/kotlin/name/Name;)Ljava/util/Collection;";
        }

        public final Collection<t0> invoke(d0.e0.p.d.m0.g.e eVar) {
            m.checkNotNullParameter(eVar, "p0");
            return h.access$searchMethodsInSupertypesWithoutBuiltinMagic((h) this.receiver, eVar);
        }
    }

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<d0.e0.p.d.m0.g.e, Collection<? extends t0>> {
        public c() {
            super(1);
        }

        public final Collection<t0> invoke(d0.e0.p.d.m0.g.e eVar) {
            m.checkNotNullParameter(eVar, "it");
            return h.access$searchMethodsByNameWithoutBuiltinMagic(h.this, eVar);
        }
    }

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<d0.e0.p.d.m0.g.e, Collection<? extends t0>> {
        public d() {
            super(1);
        }

        public final Collection<t0> invoke(d0.e0.p.d.m0.g.e eVar) {
            m.checkNotNullParameter(eVar, "it");
            return h.access$searchMethodsInSupertypesWithoutBuiltinMagic(h.this, eVar);
        }
    }

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function0<List<? extends d0.e0.p.d.m0.c.d>> {
        public final /* synthetic */ d0.e0.p.d.m0.e.a.i0.g $c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(d0.e0.p.d.m0.e.a.i0.g gVar) {
            super(0);
            this.$c = gVar;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r1v3, types: [java.util.List] */
        @Override // kotlin.jvm.functions.Function0
        public final List<? extends d0.e0.p.d.m0.c.d> invoke() {
            Collection<k> constructors = h.this.o.getConstructors();
            ArrayList<d0.e0.p.d.m0.c.d> arrayList = new ArrayList(constructors.size());
            for (k kVar : constructors) {
                arrayList.add(h.access$resolveConstructor(h.this, kVar));
            }
            if (h.this.o.isRecord()) {
                d0.e0.p.d.m0.c.d access$createDefaultRecordConstructor = h.access$createDefaultRecordConstructor(h.this);
                boolean z2 = false;
                String computeJvmDescriptor$default = u.computeJvmDescriptor$default(access$createDefaultRecordConstructor, false, false, 2, null);
                if (!arrayList.isEmpty()) {
                    for (d0.e0.p.d.m0.c.d dVar : arrayList) {
                        if (m.areEqual(u.computeJvmDescriptor$default(dVar, false, false, 2, null), computeJvmDescriptor$default)) {
                            break;
                        }
                    }
                }
                z2 = true;
                if (z2) {
                    arrayList.add(access$createDefaultRecordConstructor);
                    ((g.a) this.$c.getComponents().getJavaResolverCache()).recordConstructor(h.this.o, access$createDefaultRecordConstructor);
                }
            }
            l signatureEnhancement = this.$c.getComponents().getSignatureEnhancement();
            d0.e0.p.d.m0.e.a.i0.g gVar = this.$c;
            h hVar = h.this;
            boolean isEmpty = arrayList.isEmpty();
            ArrayList arrayList2 = arrayList;
            if (isEmpty) {
                arrayList2 = d0.t.n.listOfNotNull(h.access$createDefaultConstructor(hVar));
            }
            return d0.t.u.toList(signatureEnhancement.enhanceSignatures(gVar, arrayList2));
        }
    }

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class f extends o implements Function0<Map<d0.e0.p.d.m0.g.e, ? extends n>> {
        public f() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Map<d0.e0.p.d.m0.g.e, ? extends n> invoke() {
            Collection<n> fields = h.this.o.getFields();
            ArrayList arrayList = new ArrayList();
            for (Object obj : fields) {
                if (((n) obj).isEnumEntry()) {
                    arrayList.add(obj);
                }
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap(d0.d0.f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(arrayList, 10)), 16));
            for (Object obj2 : arrayList) {
                linkedHashMap.put(((n) obj2).getName(), obj2);
            }
            return linkedHashMap;
        }
    }

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class g extends o implements Function1<d0.e0.p.d.m0.g.e, Collection<? extends t0>> {
        public final /* synthetic */ t0 $function;
        public final /* synthetic */ h this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public g(t0 t0Var, h hVar) {
            super(1);
            this.$function = t0Var;
            this.this$0 = hVar;
        }

        public final Collection<t0> invoke(d0.e0.p.d.m0.g.e eVar) {
            m.checkNotNullParameter(eVar, "accessorName");
            if (m.areEqual(this.$function.getName(), eVar)) {
                return d0.t.m.listOf(this.$function);
            }
            return d0.t.u.plus(h.access$searchMethodsByNameWithoutBuiltinMagic(this.this$0, eVar), (Iterable) h.access$searchMethodsInSupertypesWithoutBuiltinMagic(this.this$0, eVar));
        }
    }

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* renamed from: d0.e0.p.d.m0.e.a.i0.l.h$h  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0303h extends o implements Function0<Set<? extends d0.e0.p.d.m0.g.e>> {
        public C0303h() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Set<? extends d0.e0.p.d.m0.g.e> invoke() {
            return d0.t.u.toSet(h.this.o.getInnerClassNames());
        }
    }

    /* compiled from: LazyJavaClassMemberScope.kt */
    /* loaded from: classes3.dex */
    public static final class i extends o implements Function1<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.c.i1.h> {
        public final /* synthetic */ d0.e0.p.d.m0.e.a.i0.g $c;

        /* compiled from: LazyJavaClassMemberScope.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<Set<? extends d0.e0.p.d.m0.g.e>> {
            public final /* synthetic */ h this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(h hVar) {
                super(0);
                this.this$0 = hVar;
            }

            @Override // kotlin.jvm.functions.Function0
            public final Set<? extends d0.e0.p.d.m0.g.e> invoke() {
                return o0.plus((Set) this.this$0.getFunctionNames(), (Iterable) this.this$0.getVariableNames());
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public i(d0.e0.p.d.m0.e.a.i0.g gVar) {
            super(1);
            this.$c = gVar;
        }

        public final d0.e0.p.d.m0.c.i1.h invoke(d0.e0.p.d.m0.g.e eVar) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            if (!((Set) h.this.r.invoke()).contains(eVar)) {
                n nVar = (n) ((Map) h.this.f3304s.invoke()).get(eVar);
                if (nVar == null) {
                    return null;
                }
                return d0.e0.p.d.m0.c.i1.o.create(this.$c.getStorageManager(), h.this.n, eVar, this.$c.getStorageManager().createLazyValue(new a(h.this)), d0.e0.p.d.m0.e.a.i0.e.resolveAnnotations(this.$c, nVar), this.$c.getComponents().getSourceElementFactory().source(nVar));
            }
            s finder = this.$c.getComponents().getFinder();
            d0.e0.p.d.m0.g.a classId = d0.e0.p.d.m0.k.x.a.getClassId(h.this.n);
            m.checkNotNull(classId);
            d0.e0.p.d.m0.g.a createNestedClassId = classId.createNestedClassId(eVar);
            m.checkNotNullExpressionValue(createNestedClassId, "ownerDescriptor.classId!!.createNestedClassId(name)");
            d0.e0.p.d.m0.e.a.k0.g findClass = finder.findClass(new s.a(createNestedClassId, null, h.this.o, 2, null));
            if (findClass == null) {
                return null;
            }
            d0.e0.p.d.m0.e.a.i0.g gVar = this.$c;
            d0.e0.p.d.m0.e.a.i0.l.f fVar = new d0.e0.p.d.m0.e.a.i0.l.f(gVar, h.this.n, findClass, null, 8, null);
            gVar.getComponents().getJavaClassesTracker().reportClass(fVar);
            return fVar;
        }
    }

    public /* synthetic */ h(d0.e0.p.d.m0.e.a.i0.g gVar, d0.e0.p.d.m0.c.e eVar, d0.e0.p.d.m0.e.a.k0.g gVar2, boolean z2, h hVar, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(gVar, eVar, gVar2, z2, (i2 & 16) != 0 ? null : hVar);
    }

    public static final d0.e0.p.d.m0.c.d access$createDefaultConstructor(h hVar) {
        List<c1> list;
        Pair pair;
        boolean isAnnotationType = hVar.o.isAnnotationType();
        if ((hVar.o.isInterface() || !hVar.o.hasDefaultConstructor()) && !isAnnotationType) {
            return null;
        }
        d0.e0.p.d.m0.c.e eVar = hVar.n;
        d0.e0.p.d.m0.e.a.h0.c createJavaConstructor = d0.e0.p.d.m0.e.a.h0.c.createJavaConstructor(eVar, d0.e0.p.d.m0.c.g1.g.f.getEMPTY(), true, hVar.c.getComponents().getSourceElementFactory().source(hVar.o));
        m.checkNotNullExpressionValue(createJavaConstructor, "createJavaConstructor(\n            classDescriptor, Annotations.EMPTY, /* isPrimary = */ true, c.components.sourceElementFactory.source(jClass)\n        )");
        if (isAnnotationType) {
            Collection<r> methods = hVar.o.getMethods();
            list = new ArrayList<>(methods.size());
            d0.e0.p.d.m0.e.a.i0.m.a attributes$default = d0.e0.p.d.m0.e.a.i0.m.e.toAttributes$default(d0.e0.p.d.m0.e.a.g0.k.COMMON, true, null, 2, null);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (Object obj : methods) {
                if (m.areEqual(((r) obj).getName(), d0.e0.p.d.m0.e.a.a0.f3277b)) {
                    arrayList.add(obj);
                } else {
                    arrayList2.add(obj);
                }
            }
            Pair pair2 = new Pair(arrayList, arrayList2);
            List list2 = (List) pair2.component1();
            List<r> list3 = (List) pair2.component2();
            list2.size();
            r rVar = (r) d0.t.u.firstOrNull((List<? extends Object>) list2);
            if (rVar != null) {
                x returnType = rVar.getReturnType();
                if (returnType instanceof d0.e0.p.d.m0.e.a.k0.f) {
                    d0.e0.p.d.m0.e.a.k0.f fVar = (d0.e0.p.d.m0.e.a.k0.f) returnType;
                    pair = new Pair(hVar.c.getTypeResolver().transformArrayType(fVar, attributes$default, true), hVar.c.getTypeResolver().transformJavaType(fVar.getComponentType(), attributes$default));
                } else {
                    pair = new Pair(hVar.c.getTypeResolver().transformJavaType(returnType, attributes$default), null);
                }
                hVar.o(list, createJavaConstructor, 0, rVar, (c0) pair.component1(), (c0) pair.component2());
            }
            int i2 = rVar != null ? 1 : 0;
            int i3 = 0;
            for (r rVar2 : list3) {
                i3++;
                hVar.o(list, createJavaConstructor, i3 + i2, rVar2, hVar.c.getTypeResolver().transformJavaType(rVar2.getReturnType(), attributes$default), null);
            }
        } else {
            list = Collections.emptyList();
        }
        createJavaConstructor.setHasSynthesizedParameterNames(false);
        createJavaConstructor.initialize(list, hVar.B(eVar));
        createJavaConstructor.setHasStableParameterNames(true);
        createJavaConstructor.setReturnType(eVar.getDefaultType());
        ((g.a) hVar.c.getComponents().getJavaResolverCache()).recordConstructor(hVar.o, createJavaConstructor);
        return createJavaConstructor;
    }

    public static final d0.e0.p.d.m0.c.d access$createDefaultRecordConstructor(h hVar) {
        d0.e0.p.d.m0.c.e eVar = hVar.n;
        d0.e0.p.d.m0.e.a.h0.c createJavaConstructor = d0.e0.p.d.m0.e.a.h0.c.createJavaConstructor(eVar, d0.e0.p.d.m0.c.g1.g.f.getEMPTY(), true, hVar.c.getComponents().getSourceElementFactory().source(hVar.o));
        m.checkNotNullExpressionValue(createJavaConstructor, "createJavaConstructor(\n            classDescriptor, Annotations.EMPTY, /* isPrimary = */ true, c.components.sourceElementFactory.source(jClass)\n        )");
        Collection<w> recordComponents = hVar.o.getRecordComponents();
        ArrayList arrayList = new ArrayList(recordComponents.size());
        c0 c0Var = null;
        d0.e0.p.d.m0.e.a.i0.m.a attributes$default = d0.e0.p.d.m0.e.a.i0.m.e.toAttributes$default(d0.e0.p.d.m0.e.a.g0.k.COMMON, false, null, 2, null);
        int i2 = 0;
        for (w wVar : recordComponents) {
            i2++;
            c0 transformJavaType = hVar.c.getTypeResolver().transformJavaType(wVar.getType(), attributes$default);
            c0 arrayElementType = wVar.isVararg() ? hVar.c.getComponents().getModule().getBuiltIns().getArrayElementType(transformJavaType) : c0Var;
            attributes$default = attributes$default;
            c0Var = c0Var;
            arrayList.add(new l0(createJavaConstructor, null, i2, d0.e0.p.d.m0.c.g1.g.f.getEMPTY(), wVar.getName(), transformJavaType, false, false, false, arrayElementType, hVar.c.getComponents().getSourceElementFactory().source(wVar)));
        }
        createJavaConstructor.setHasSynthesizedParameterNames(false);
        createJavaConstructor.initialize(arrayList, hVar.B(eVar));
        createJavaConstructor.setHasStableParameterNames(false);
        createJavaConstructor.setReturnType(eVar.getDefaultType());
        return createJavaConstructor;
    }

    public static final d0.e0.p.d.m0.e.a.h0.c access$resolveConstructor(h hVar, k kVar) {
        d0.e0.p.d.m0.c.e eVar = hVar.n;
        d0.e0.p.d.m0.e.a.h0.c createJavaConstructor = d0.e0.p.d.m0.e.a.h0.c.createJavaConstructor(eVar, d0.e0.p.d.m0.e.a.i0.e.resolveAnnotations(hVar.c, kVar), false, hVar.c.getComponents().getSourceElementFactory().source(kVar));
        m.checkNotNullExpressionValue(createJavaConstructor, "createJavaConstructor(\n            classDescriptor,\n            c.resolveAnnotations(constructor), /* isPrimary = */\n            false,\n            c.components.sourceElementFactory.source(constructor)\n        )");
        d0.e0.p.d.m0.e.a.i0.g childForMethod = d0.e0.p.d.m0.e.a.i0.a.childForMethod(hVar.c, createJavaConstructor, kVar, eVar.getDeclaredTypeParameters().size());
        k.b n = hVar.n(childForMethod, createJavaConstructor, kVar.getValueParameters());
        List<z0> declaredTypeParameters = eVar.getDeclaredTypeParameters();
        m.checkNotNullExpressionValue(declaredTypeParameters, "classDescriptor.declaredTypeParameters");
        List<y> typeParameters = kVar.getTypeParameters();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(typeParameters, 10));
        for (y yVar : typeParameters) {
            z0 resolveTypeParameter = childForMethod.getTypeParameterResolver().resolveTypeParameter(yVar);
            m.checkNotNull(resolveTypeParameter);
            arrayList.add(resolveTypeParameter);
        }
        createJavaConstructor.initialize(n.getDescriptors(), f0.toDescriptorVisibility(kVar.getVisibility()), d0.t.u.plus((Collection) declaredTypeParameters, (Iterable) arrayList));
        createJavaConstructor.setHasStableParameterNames(false);
        createJavaConstructor.setHasSynthesizedParameterNames(n.getHasSynthesizedNames());
        createJavaConstructor.setReturnType(eVar.getDefaultType());
        ((g.a) childForMethod.getComponents().getJavaResolverCache()).recordConstructor(kVar, createJavaConstructor);
        return createJavaConstructor;
    }

    public static final Collection access$searchMethodsByNameWithoutBuiltinMagic(h hVar, d0.e0.p.d.m0.g.e eVar) {
        Collection<r> findMethodsByName = hVar.f.invoke().findMethodsByName(eVar);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(findMethodsByName, 10));
        for (r rVar : findMethodsByName) {
            arrayList.add(hVar.m(rVar));
        }
        return arrayList;
    }

    /* JADX WARN: Removed duplicated region for block: B:15:0x002e A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:17:0x000d A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final java.util.Collection access$searchMethodsInSupertypesWithoutBuiltinMagic(d0.e0.p.d.m0.e.a.i0.l.h r3, d0.e0.p.d.m0.g.e r4) {
        /*
            java.util.Set r3 = r3.C(r4)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.Iterator r3 = r3.iterator()
        Ld:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L32
            java.lang.Object r0 = r3.next()
            r1 = r0
            d0.e0.p.d.m0.c.t0 r1 = (d0.e0.p.d.m0.c.t0) r1
            boolean r2 = d0.e0.p.d.m0.e.a.d0.doesOverrideBuiltinWithDifferentJvmName(r1)
            if (r2 != 0) goto L2b
            d0.e0.p.d.m0.e.a.h r2 = d0.e0.p.d.m0.e.a.h.m
            d0.e0.p.d.m0.c.x r1 = d0.e0.p.d.m0.e.a.h.getOverriddenBuiltinFunctionWithErasedValueParametersInJava(r1)
            if (r1 == 0) goto L29
            goto L2b
        L29:
            r1 = 0
            goto L2c
        L2b:
            r1 = 1
        L2c:
            if (r1 != 0) goto Ld
            r4.add(r0)
            goto Ld
        L32:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.l.h.access$searchMethodsInSupertypesWithoutBuiltinMagic(d0.e0.p.d.m0.e.a.i0.l.h, d0.e0.p.d.m0.g.e):java.util.Collection");
    }

    public final t0 A(n0 n0Var, Function1<? super d0.e0.p.d.m0.g.e, ? extends Collection<? extends t0>> function1) {
        t0 t0Var;
        c0 returnType;
        z zVar = z.a;
        String asString = n0Var.getName().asString();
        m.checkNotNullExpressionValue(asString, "name.asString()");
        d0.e0.p.d.m0.g.e identifier = d0.e0.p.d.m0.g.e.identifier(z.setterName(asString));
        m.checkNotNullExpressionValue(identifier, "identifier(JvmAbi.setterName(name.asString()))");
        Iterator<T> it = function1.invoke(identifier).iterator();
        do {
            t0Var = null;
            if (!it.hasNext()) {
                break;
            }
            t0 t0Var2 = (t0) it.next();
            if (t0Var2.getValueParameters().size() == 1 && (returnType = t0Var2.getReturnType()) != null && d0.e0.p.d.m0.b.h.isUnit(returnType)) {
                d0.e0.p.d.m0.n.l1.f fVar = d0.e0.p.d.m0.n.l1.f.a;
                List<c1> valueParameters = t0Var2.getValueParameters();
                m.checkNotNullExpressionValue(valueParameters, "descriptor.valueParameters");
                if (fVar.equalTypes(((c1) d0.t.u.single((List<? extends Object>) valueParameters)).getType(), n0Var.getType())) {
                    t0Var = t0Var2;
                    continue;
                } else {
                    continue;
                }
            }
        } while (t0Var == null);
        return t0Var;
    }

    public final d0.e0.p.d.m0.c.u B(d0.e0.p.d.m0.c.e eVar) {
        d0.e0.p.d.m0.c.u visibility = eVar.getVisibility();
        m.checkNotNullExpressionValue(visibility, "classDescriptor.visibility");
        if (!m.areEqual(visibility, d0.e0.p.d.m0.e.a.w.f3331b)) {
            return visibility;
        }
        d0.e0.p.d.m0.c.u uVar = d0.e0.p.d.m0.e.a.w.c;
        m.checkNotNullExpressionValue(uVar, "PROTECTED_AND_PACKAGE");
        return uVar;
    }

    public final Set<t0> C(d0.e0.p.d.m0.g.e eVar) {
        Collection<c0> s2 = s();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (c0 c0Var : s2) {
            d0.t.r.addAll(linkedHashSet, c0Var.getMemberScope().getContributedFunctions(eVar, d0.e0.p.d.m0.d.b.d.WHEN_GET_SUPER_MEMBERS));
        }
        return linkedHashSet;
    }

    public final Set<n0> D(d0.e0.p.d.m0.g.e eVar) {
        Collection<c0> s2 = s();
        ArrayList arrayList = new ArrayList();
        for (c0 c0Var : s2) {
            Collection<? extends n0> contributedVariables = c0Var.getMemberScope().getContributedVariables(eVar, d0.e0.p.d.m0.d.b.d.WHEN_GET_SUPER_MEMBERS);
            ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(contributedVariables, 10));
            for (n0 n0Var : contributedVariables) {
                arrayList2.add(n0Var);
            }
            d0.t.r.addAll(arrayList, arrayList2);
        }
        return d0.t.u.toSet(arrayList);
    }

    public final boolean E(t0 t0Var, d0.e0.p.d.m0.c.x xVar) {
        String computeJvmDescriptor$default = u.computeJvmDescriptor$default(t0Var, false, false, 2, null);
        d0.e0.p.d.m0.c.x original = xVar.getOriginal();
        m.checkNotNullExpressionValue(original, "builtinWithErasedParameters.original");
        return m.areEqual(computeJvmDescriptor$default, u.computeJvmDescriptor$default(original, false, false, 2, null)) && !w(t0Var, xVar);
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x006d, code lost:
        if (d0.e0.p.d.m0.e.a.z.isSetterName(r4) == false) goto L22;
     */
    /* JADX WARN: Removed duplicated region for block: B:101:0x0074 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean F(d0.e0.p.d.m0.c.t0 r9) {
        /*
            Method dump skipped, instructions count: 434
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.l.h.F(d0.e0.p.d.m0.c.t0):boolean");
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set<d0.e0.p.d.m0.g.e> a(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        return o0.plus((Set) this.r.invoke(), (Iterable) this.f3304s.invoke().keySet());
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set b(d0.e0.p.d.m0.k.a0.d dVar, Function1 function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        Collection<c0> supertypes = this.n.getTypeConstructor().getSupertypes();
        m.checkNotNullExpressionValue(supertypes, "ownerDescriptor.typeConstructor.supertypes");
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (c0 c0Var : supertypes) {
            d0.t.r.addAll(linkedHashSet, c0Var.getMemberScope().getFunctionNames());
        }
        linkedHashSet.addAll(this.f.invoke().getMethodNames());
        linkedHashSet.addAll(this.f.invoke().getRecordComponentNames());
        linkedHashSet.addAll(a(dVar, function1));
        return linkedHashSet;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public void c(Collection<t0> collection, d0.e0.p.d.m0.g.e eVar) {
        boolean z2;
        m.checkNotNullParameter(collection, "result");
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        if (this.o.isRecord() && this.f.invoke().findRecordComponentByName(eVar) != null) {
            if (!collection.isEmpty()) {
                for (t0 t0Var : collection) {
                    if (t0Var.getValueParameters().isEmpty()) {
                        z2 = false;
                        break;
                    }
                }
            }
            z2 = true;
            if (z2) {
                w findRecordComponentByName = this.f.invoke().findRecordComponentByName(eVar);
                m.checkNotNull(findRecordComponentByName);
                d0.e0.p.d.m0.e.a.h0.f createJavaMethod = d0.e0.p.d.m0.e.a.h0.f.createJavaMethod(this.n, d0.e0.p.d.m0.e.a.i0.e.resolveAnnotations(this.c, findRecordComponentByName), findRecordComponentByName.getName(), this.c.getComponents().getSourceElementFactory().source(findRecordComponentByName), true);
                m.checkNotNullExpressionValue(createJavaMethod, "createJavaMethod(\n            ownerDescriptor, annotations, recordComponent.name, c.components.sourceElementFactory.source(recordComponent), true\n        )");
                createJavaMethod.initialize(null, i(), d0.t.n.emptyList(), d0.t.n.emptyList(), this.c.getTypeResolver().transformJavaType(findRecordComponentByName.getType(), d0.e0.p.d.m0.e.a.i0.m.e.toAttributes$default(d0.e0.p.d.m0.e.a.g0.k.COMMON, false, null, 2, null)), d0.e0.p.d.m0.c.z.j.convertFromFlags(false, false, true), t.e, null);
                createJavaMethod.setParameterNamesStatus(false, false);
                ((g.a) this.c.getComponents().getJavaResolverCache()).recordMethod(findRecordComponentByName, createJavaMethod);
                collection.add(createJavaMethod);
            }
        }
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public d0.e0.p.d.m0.e.a.i0.l.b d() {
        return new d0.e0.p.d.m0.e.a.i0.l.a(this.o, d0.e0.p.d.m0.e.a.i0.l.g.j);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public void f(Collection<t0> collection, d0.e0.p.d.m0.g.e eVar) {
        boolean z2;
        m.checkNotNullParameter(collection, "result");
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        Set<t0> C = C(eVar);
        if (!d0.e0.p.d.m0.e.a.g.m.getSameAsRenamedInJvmBuiltin(eVar) && !d0.e0.p.d.m0.e.a.h.m.getSameAsBuiltinMethodWithErasedValueParameters(eVar)) {
            if (!C.isEmpty()) {
                for (d0.e0.p.d.m0.c.x xVar : C) {
                    if (xVar.isSuspend()) {
                        z2 = false;
                        break;
                    }
                }
            }
            z2 = true;
            if (z2) {
                ArrayList arrayList = new ArrayList();
                for (Object obj : C) {
                    if (F((t0) obj)) {
                        arrayList.add(obj);
                    }
                }
                p(collection, eVar, arrayList, false);
                return;
            }
        }
        Collection<t0> create = d0.e0.p.d.m0.p.j.j.create();
        Collection<? extends t0> resolveOverridesForNonStaticMembers = d0.e0.p.d.m0.e.a.g0.a.resolveOverridesForNonStaticMembers(eVar, C, d0.t.n.emptyList(), this.n, p.a, this.c.getComponents().getKotlinTypeChecker().getOverridingUtil());
        m.checkNotNullExpressionValue(resolveOverridesForNonStaticMembers, "resolveOverridesForNonStaticMembers(\n            name, functionsFromSupertypes, emptyList(), ownerDescriptor, ErrorReporter.DO_NOTHING,\n            c.components.kotlinTypeChecker.overridingUtil\n        )");
        q(eVar, collection, resolveOverridesForNonStaticMembers, collection, new a(this));
        q(eVar, collection, resolveOverridesForNonStaticMembers, create, new b(this));
        ArrayList arrayList2 = new ArrayList();
        for (Object obj2 : C) {
            if (F((t0) obj2)) {
                arrayList2.add(obj2);
            }
        }
        p(collection, eVar, d0.t.u.plus((Collection) arrayList2, (Iterable) create), true);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public void g(d0.e0.p.d.m0.g.e eVar, Collection<n0> collection) {
        r rVar;
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(collection, "result");
        if (this.o.isAnnotationType() && (rVar = (r) d0.t.u.singleOrNull(this.f.invoke().findMethodsByName(eVar))) != null) {
            d0.e0.p.d.m0.e.a.h0.g create = d0.e0.p.d.m0.e.a.h0.g.create(this.n, d0.e0.p.d.m0.e.a.i0.e.resolveAnnotations(this.c, rVar), d0.e0.p.d.m0.c.z.FINAL, f0.toDescriptorVisibility(rVar.getVisibility()), false, rVar.getName(), this.c.getComponents().getSourceElementFactory().source(rVar), false);
            m.checkNotNullExpressionValue(create, "create(\n            ownerDescriptor, annotations, modality, method.visibility.toDescriptorVisibility(),\n            /* isVar = */ false, method.name, c.components.sourceElementFactory.source(method),\n            /* isStaticFinal = */ false\n        )");
            d0 createDefaultGetter = d0.e0.p.d.m0.k.d.createDefaultGetter(create, d0.e0.p.d.m0.c.g1.g.f.getEMPTY());
            m.checkNotNullExpressionValue(createDefaultGetter, "createDefaultGetter(propertyDescriptor, Annotations.EMPTY)");
            create.initialize(createDefaultGetter, null);
            c0 e2 = e(rVar, d0.e0.p.d.m0.e.a.i0.a.childForMethod$default(this.c, create, rVar, 0, 4, null));
            create.setType(e2, d0.t.n.emptyList(), i(), null);
            createDefaultGetter.initialize(e2);
            collection.add(create);
        }
        Set<n0> D = D(eVar);
        if (!D.isEmpty()) {
            j.b bVar = d0.e0.p.d.m0.p.j.j;
            d0.e0.p.d.m0.p.j create2 = bVar.create();
            d0.e0.p.d.m0.p.j create3 = bVar.create();
            r(D, collection, create2, new c());
            r(o0.minus((Set) D, (Iterable) create2), create3, null, new d());
            Collection<? extends n0> resolveOverridesForNonStaticMembers = d0.e0.p.d.m0.e.a.g0.a.resolveOverridesForNonStaticMembers(eVar, o0.plus((Set) D, (Iterable) create3), collection, this.n, this.c.getComponents().getErrorReporter(), this.c.getComponents().getKotlinTypeChecker().getOverridingUtil());
            m.checkNotNullExpressionValue(resolveOverridesForNonStaticMembers, "resolveOverridesForNonStaticMembers(\n                name,\n                propertiesFromSupertypes + propertiesOverridesFromSuperTypes,\n                result,\n                ownerDescriptor,\n                c.components.errorReporter,\n                c.components.kotlinTypeChecker.overridingUtil\n            )");
            collection.addAll(resolveOverridesForNonStaticMembers);
        }
    }

    public final d0.e0.p.d.m0.m.j<List<d0.e0.p.d.m0.c.d>> getConstructors$descriptors_jvm() {
        return this.q;
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public d0.e0.p.d.m0.c.h getContributedClassifier(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        d0.e0.p.d.m0.m.i<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.c.i1.h> iVar;
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        recordLookup(eVar, bVar);
        h hVar = (h) this.d;
        d0.e0.p.d.m0.c.i1.h hVar2 = null;
        if (!(hVar == null || (iVar = hVar.t) == null)) {
            hVar2 = iVar.invoke(eVar);
        }
        return hVar2 == null ? this.t.invoke(eVar) : hVar2;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k, d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Collection<t0> getContributedFunctions(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        recordLookup(eVar, bVar);
        return super.getContributedFunctions(eVar, bVar);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k, d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Collection<n0> getContributedVariables(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        recordLookup(eVar, bVar);
        return super.getContributedVariables(eVar, bVar);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set<d0.e0.p.d.m0.g.e> h(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        if (this.o.isAnnotationType()) {
            return getFunctionNames();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet(this.f.invoke().getFieldNames());
        Collection<c0> supertypes = this.n.getTypeConstructor().getSupertypes();
        m.checkNotNullExpressionValue(supertypes, "ownerDescriptor.typeConstructor.supertypes");
        for (c0 c0Var : supertypes) {
            d0.t.r.addAll(linkedHashSet, c0Var.getMemberScope().getVariableNames());
        }
        return linkedHashSet;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public q0 i() {
        return d0.e0.p.d.m0.k.e.getDispatchReceiverParameterIfNeeded(this.n);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public d0.e0.p.d.m0.c.m j() {
        return this.n;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public boolean k(d0.e0.p.d.m0.e.a.h0.f fVar) {
        m.checkNotNullParameter(fVar, "<this>");
        if (this.o.isAnnotationType()) {
            return false;
        }
        return F(fVar);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public k.a l(r rVar, List<? extends z0> list, c0 c0Var, List<? extends c1> list2) {
        m.checkNotNullParameter(rVar, "method");
        m.checkNotNullParameter(list, "methodTypeParameters");
        m.checkNotNullParameter(c0Var, "returnType");
        m.checkNotNullParameter(list2, "valueParameters");
        j.b resolvePropagatedSignature = ((j.a) this.c.getComponents().getSignaturePropagator()).resolvePropagatedSignature(rVar, this.n, c0Var, null, list2, list);
        m.checkNotNullExpressionValue(resolvePropagatedSignature, "c.components.signaturePropagator.resolvePropagatedSignature(\n            method, ownerDescriptor, returnType, null, valueParameters, methodTypeParameters\n        )");
        c0 returnType = resolvePropagatedSignature.getReturnType();
        m.checkNotNullExpressionValue(returnType, "propagated.returnType");
        c0 receiverType = resolvePropagatedSignature.getReceiverType();
        List<c1> valueParameters = resolvePropagatedSignature.getValueParameters();
        m.checkNotNullExpressionValue(valueParameters, "propagated.valueParameters");
        List<z0> typeParameters = resolvePropagatedSignature.getTypeParameters();
        m.checkNotNullExpressionValue(typeParameters, "propagated.typeParameters");
        boolean hasStableParameterNames = resolvePropagatedSignature.hasStableParameterNames();
        List<String> errors = resolvePropagatedSignature.getErrors();
        m.checkNotNullExpressionValue(errors, "propagated.errors");
        return new k.a(returnType, receiverType, valueParameters, typeParameters, hasStableParameterNames, errors);
    }

    public final void o(List<c1> list, d0.e0.p.d.m0.c.l lVar, int i2, r rVar, c0 c0Var, c0 c0Var2) {
        d0.e0.p.d.m0.c.g1.g empty = d0.e0.p.d.m0.c.g1.g.f.getEMPTY();
        d0.e0.p.d.m0.g.e name = rVar.getName();
        c0 makeNotNullable = e1.makeNotNullable(c0Var);
        m.checkNotNullExpressionValue(makeNotNullable, "makeNotNullable(returnType)");
        list.add(new l0(lVar, null, i2, empty, name, makeNotNullable, rVar.getHasAnnotationParameterDefaultValue(), false, false, c0Var2 == null ? null : e1.makeNotNullable(c0Var2), this.c.getComponents().getSourceElementFactory().source(rVar)));
    }

    public final void p(Collection<t0> collection, d0.e0.p.d.m0.g.e eVar, Collection<? extends t0> collection2, boolean z2) {
        Collection<? extends t0> resolveOverridesForNonStaticMembers = d0.e0.p.d.m0.e.a.g0.a.resolveOverridesForNonStaticMembers(eVar, collection2, collection, this.n, this.c.getComponents().getErrorReporter(), this.c.getComponents().getKotlinTypeChecker().getOverridingUtil());
        m.checkNotNullExpressionValue(resolveOverridesForNonStaticMembers, "resolveOverridesForNonStaticMembers(\n            name, functionsFromSupertypes, result, ownerDescriptor, c.components.errorReporter,\n            c.components.kotlinTypeChecker.overridingUtil\n        )");
        if (!z2) {
            collection.addAll(resolveOverridesForNonStaticMembers);
            return;
        }
        List plus = d0.t.u.plus((Collection) collection, (Iterable) resolveOverridesForNonStaticMembers);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(resolveOverridesForNonStaticMembers, 10));
        for (t0 t0Var : resolveOverridesForNonStaticMembers) {
            t0 t0Var2 = (t0) d0.e0.p.d.m0.e.a.d0.getOverriddenSpecialBuiltin(t0Var);
            if (t0Var2 == null) {
                m.checkNotNullExpressionValue(t0Var, "resolvedOverride");
            } else {
                m.checkNotNullExpressionValue(t0Var, "resolvedOverride");
                t0Var = t(t0Var, t0Var2, plus);
            }
            arrayList.add(t0Var);
        }
        collection.addAll(arrayList);
    }

    /* JADX WARN: Removed duplicated region for block: B:40:0x011f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void q(d0.e0.p.d.m0.g.e r17, java.util.Collection<? extends d0.e0.p.d.m0.c.t0> r18, java.util.Collection<? extends d0.e0.p.d.m0.c.t0> r19, java.util.Collection<d0.e0.p.d.m0.c.t0> r20, kotlin.jvm.functions.Function1<? super d0.e0.p.d.m0.g.e, ? extends java.util.Collection<? extends d0.e0.p.d.m0.c.t0>> r21) {
        /*
            Method dump skipped, instructions count: 343
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.l.h.q(d0.e0.p.d.m0.g.e, java.util.Collection, java.util.Collection, java.util.Collection, kotlin.jvm.functions.Function1):void");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r15v0, types: [d0.e0.p.d.m0.c.i1.c0, d0.e0.p.d.m0.c.n0, d0.e0.p.d.m0.e.a.h0.e, d0.e0.p.d.m0.c.i1.m0] */
    /* JADX WARN: Type inference failed for: r19v0, types: [java.util.Collection<d0.e0.p.d.m0.c.n0>, java.util.Collection] */
    public final void r(Set<? extends n0> set, Collection<n0> collection, Set<n0> set2, Function1<? super d0.e0.p.d.m0.g.e, ? extends Collection<? extends t0>> function1) {
        t0 t0Var;
        d0 d0Var;
        for (n0 n0Var : set) {
            e0 e0Var = null;
            if (v(n0Var, function1)) {
                t0 z2 = z(n0Var, function1);
                m.checkNotNull(z2);
                if (n0Var.isVar()) {
                    t0Var = A(n0Var, function1);
                    m.checkNotNull(t0Var);
                } else {
                    t0Var = null;
                }
                if (t0Var != null) {
                    t0Var.getModality();
                    z2.getModality();
                }
                ?? eVar = new d0.e0.p.d.m0.e.a.h0.e(this.n, z2, t0Var, n0Var);
                c0 returnType = z2.getReturnType();
                m.checkNotNull(returnType);
                eVar.setType(returnType, d0.t.n.emptyList(), i(), null);
                d0 createGetter = d0.e0.p.d.m0.k.d.createGetter(eVar, z2.getAnnotations(), false, false, false, z2.getSource());
                createGetter.setInitialSignatureDescriptor(z2);
                createGetter.initialize(eVar.getType());
                m.checkNotNullExpressionValue(createGetter, "createGetter(\n            propertyDescriptor, getterMethod.annotations, /* isDefault = */false,\n            /* isExternal = */ false, /* isInline = */ false, getterMethod.source\n        ).apply {\n            initialSignatureDescriptor = getterMethod\n            initialize(propertyDescriptor.type)\n        }");
                if (t0Var != null) {
                    List<c1> valueParameters = t0Var.getValueParameters();
                    m.checkNotNullExpressionValue(valueParameters, "setterMethod.valueParameters");
                    c1 c1Var = (c1) d0.t.u.firstOrNull((List<? extends Object>) valueParameters);
                    if (c1Var != null) {
                        e0 createSetter = d0.e0.p.d.m0.k.d.createSetter(eVar, t0Var.getAnnotations(), c1Var.getAnnotations(), false, false, false, t0Var.getVisibility(), t0Var.getSource());
                        createSetter.setInitialSignatureDescriptor(t0Var);
                        e0Var = createSetter;
                        d0Var = createGetter;
                    } else {
                        throw new AssertionError(m.stringPlus("No parameter found for ", t0Var));
                    }
                } else {
                    d0Var = createGetter;
                }
                eVar.initialize(d0Var, e0Var);
                e0Var = eVar;
            }
            if (e0Var != null) {
                collection.add(e0Var);
                if (set2 != null) {
                    ((d0.e0.p.d.m0.p.j) set2).add(n0Var);
                    return;
                }
                return;
            }
        }
    }

    public void recordLookup(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        d0.e0.p.d.m0.d.a.record(this.c.getComponents().getLookupTracker(), bVar, this.n, eVar);
    }

    public final Collection<c0> s() {
        if (!this.p) {
            return this.c.getComponents().getKotlinTypeChecker().getKotlinTypeRefiner().refineSupertypes(this.n);
        }
        Collection<c0> supertypes = this.n.getTypeConstructor().getSupertypes();
        m.checkNotNullExpressionValue(supertypes, "ownerDescriptor.typeConstructor.supertypes");
        return supertypes;
    }

    public final t0 t(t0 t0Var, d0.e0.p.d.m0.c.a aVar, Collection<? extends t0> collection) {
        boolean z2;
        boolean z3 = true;
        if (!(collection instanceof Collection) || !collection.isEmpty()) {
            Iterator<T> it = collection.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                t0 t0Var2 = (t0) it.next();
                if (m.areEqual(t0Var, t0Var2) || t0Var2.getInitialSignatureDescriptor() != null || !w(t0Var2, aVar)) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    z3 = false;
                    break;
                }
            }
        }
        if (z3) {
            return t0Var;
        }
        t0 build = t0Var.newCopyBuilder().setHiddenToOvercomeSignatureClash().build();
        m.checkNotNull(build);
        return build;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public String toString() {
        return m.stringPlus("Lazy Java member scope for ", this.o.getFqName());
    }

    /* JADX WARN: Code restructure failed: missing block: B:17:0x004d, code lost:
        if (d0.e0.p.d.m0.b.l.isContinuation(r3, r5.c.getComponents().getSettings().isReleaseCoroutines()) == false) goto L4;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final d0.e0.p.d.m0.c.t0 u(d0.e0.p.d.m0.c.t0 r6) {
        /*
            r5 = this;
            java.util.List r0 = r6.getValueParameters()
            java.lang.String r1 = "valueParameters"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            java.lang.Object r0 = d0.t.u.lastOrNull(r0)
            d0.e0.p.d.m0.c.c1 r0 = (d0.e0.p.d.m0.c.c1) r0
            r2 = 0
            if (r0 != 0) goto L14
        L12:
            r0 = r2
            goto L4f
        L14:
            d0.e0.p.d.m0.n.c0 r3 = r0.getType()
            d0.e0.p.d.m0.n.u0 r3 = r3.getConstructor()
            d0.e0.p.d.m0.c.h r3 = r3.getDeclarationDescriptor()
            if (r3 != 0) goto L24
            r3 = r2
            goto L28
        L24:
            d0.e0.p.d.m0.g.c r3 = d0.e0.p.d.m0.k.x.a.getFqNameUnsafe(r3)
        L28:
            if (r3 != 0) goto L2c
        L2a:
            r3 = r2
            goto L3b
        L2c:
            boolean r4 = r3.isSafe()
            if (r4 == 0) goto L33
            goto L34
        L33:
            r3 = r2
        L34:
            if (r3 != 0) goto L37
            goto L2a
        L37:
            d0.e0.p.d.m0.g.b r3 = r3.toSafe()
        L3b:
            d0.e0.p.d.m0.e.a.i0.g r4 = r5.c
            d0.e0.p.d.m0.e.a.i0.b r4 = r4.getComponents()
            d0.e0.p.d.m0.e.a.i0.c r4 = r4.getSettings()
            boolean r4 = r4.isReleaseCoroutines()
            boolean r3 = d0.e0.p.d.m0.b.l.isContinuation(r3, r4)
            if (r3 == 0) goto L12
        L4f:
            if (r0 != 0) goto L52
            return r2
        L52:
            d0.e0.p.d.m0.c.x$a r2 = r6.newCopyBuilder()
            java.util.List r6 = r6.getValueParameters()
            d0.z.d.m.checkNotNullExpressionValue(r6, r1)
            r1 = 1
            java.util.List r6 = d0.t.u.dropLast(r6, r1)
            d0.e0.p.d.m0.c.x$a r6 = r2.setValueParameters(r6)
            d0.e0.p.d.m0.n.c0 r0 = r0.getType()
            java.util.List r0 = r0.getArguments()
            r2 = 0
            java.lang.Object r0 = r0.get(r2)
            d0.e0.p.d.m0.n.w0 r0 = (d0.e0.p.d.m0.n.w0) r0
            d0.e0.p.d.m0.n.c0 r0 = r0.getType()
            d0.e0.p.d.m0.c.x$a r6 = r6.setReturnType(r0)
            d0.e0.p.d.m0.c.x r6 = r6.build()
            d0.e0.p.d.m0.c.t0 r6 = (d0.e0.p.d.m0.c.t0) r6
            r0 = r6
            d0.e0.p.d.m0.c.i1.g0 r0 = (d0.e0.p.d.m0.c.i1.g0) r0
            if (r0 != 0) goto L89
            goto L8c
        L89:
            r0.setSuspend(r1)
        L8c:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.l.h.u(d0.e0.p.d.m0.c.t0):d0.e0.p.d.m0.c.t0");
    }

    public final boolean v(n0 n0Var, Function1<? super d0.e0.p.d.m0.g.e, ? extends Collection<? extends t0>> function1) {
        if (d0.e0.p.d.m0.e.a.i0.l.c.isJavaField(n0Var)) {
            return false;
        }
        t0 z2 = z(n0Var, function1);
        t0 A = A(n0Var, function1);
        if (z2 == null) {
            return false;
        }
        if (!n0Var.isVar()) {
            return true;
        }
        return A != null && A.getModality() == z2.getModality();
    }

    public final boolean w(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2) {
        k.d.a result = d0.e0.p.d.m0.k.k.f3440b.isOverridableByWithoutExternalConditions(aVar2, aVar, true).getResult();
        m.checkNotNullExpressionValue(result, "DEFAULT.isOverridableByWithoutExternalConditions(superDescriptor, this, true).result");
        return result == k.d.a.OVERRIDABLE && !d0.e0.p.d.m0.e.a.x.a.doesJavaOverrideHaveIncompatibleValueParameterKinds(aVar2, aVar);
    }

    public final boolean x(t0 t0Var, d0.e0.p.d.m0.c.x xVar) {
        if (d0.e0.p.d.m0.e.a.g.m.isRemoveAtByIndex(t0Var)) {
            xVar = xVar.getOriginal();
        }
        m.checkNotNullExpressionValue(xVar, "if (superDescriptor.isRemoveAtByIndex) subDescriptor.original else subDescriptor");
        return w(xVar, t0Var);
    }

    public final t0 y(n0 n0Var, String str, Function1<? super d0.e0.p.d.m0.g.e, ? extends Collection<? extends t0>> function1) {
        t0 t0Var;
        d0.e0.p.d.m0.g.e identifier = d0.e0.p.d.m0.g.e.identifier(str);
        m.checkNotNullExpressionValue(identifier, "identifier(getterName)");
        Iterator<T> it = function1.invoke(identifier).iterator();
        do {
            t0Var = null;
            if (!it.hasNext()) {
                break;
            }
            t0 t0Var2 = (t0) it.next();
            if (t0Var2.getValueParameters().size() == 0) {
                d0.e0.p.d.m0.n.l1.f fVar = d0.e0.p.d.m0.n.l1.f.a;
                c0 returnType = t0Var2.getReturnType();
                if (returnType == null ? false : fVar.isSubtypeOf(returnType, n0Var.getType())) {
                    t0Var = t0Var2;
                    continue;
                } else {
                    continue;
                }
            }
        } while (t0Var == null);
        return t0Var;
    }

    public final t0 z(n0 n0Var, Function1<? super d0.e0.p.d.m0.g.e, ? extends Collection<? extends t0>> function1) {
        d0.e0.p.d.m0.c.o0 getter = n0Var.getGetter();
        String str = null;
        d0.e0.p.d.m0.c.o0 o0Var = getter == null ? null : (d0.e0.p.d.m0.c.o0) d0.e0.p.d.m0.e.a.d0.getOverriddenBuiltinWithDifferentJvmName(getter);
        if (o0Var != null) {
            str = d0.e0.p.d.m0.e.a.k.a.getBuiltinSpecialPropertyGetterName(o0Var);
        }
        if (str != null && !d0.e0.p.d.m0.e.a.d0.hasRealKotlinSuperClassWithOverrideOf(this.n, o0Var)) {
            return y(n0Var, str, function1);
        }
        z zVar = z.a;
        String asString = n0Var.getName().asString();
        m.checkNotNullExpressionValue(asString, "name.asString()");
        return y(n0Var, z.getterName(asString), function1);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public h(d0.e0.p.d.m0.e.a.i0.g gVar, d0.e0.p.d.m0.c.e eVar, d0.e0.p.d.m0.e.a.k0.g gVar2, boolean z2, h hVar) {
        super(gVar, hVar);
        m.checkNotNullParameter(gVar, "c");
        m.checkNotNullParameter(eVar, "ownerDescriptor");
        m.checkNotNullParameter(gVar2, "jClass");
        this.n = eVar;
        this.o = gVar2;
        this.p = z2;
        this.q = gVar.getStorageManager().createLazyValue(new e(gVar));
        this.r = gVar.getStorageManager().createLazyValue(new C0303h());
        this.f3304s = gVar.getStorageManager().createLazyValue(new f());
        this.t = gVar.getStorageManager().createMemoizedFunctionWithNullableValues(new i(gVar));
    }
}
