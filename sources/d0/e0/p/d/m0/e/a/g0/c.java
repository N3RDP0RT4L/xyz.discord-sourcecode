package d0.e0.p.d.m0.e.a.g0;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.e.a.a0;
import d0.e0.p.d.m0.e.a.i0.g;
import d0.e0.p.d.m0.e.a.k0.a;
import d0.e0.p.d.m0.e.a.k0.d;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.util.Map;
/* compiled from: JavaAnnotationMapper.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final c a = new c();

    /* renamed from: b  reason: collision with root package name */
    public static final e f3287b;
    public static final e c;
    public static final e d;
    public static final Map<b, b> e;
    public static final Map<b, b> f;

    static {
        e identifier = e.identifier("message");
        m.checkNotNullExpressionValue(identifier, "identifier(\"message\")");
        f3287b = identifier;
        e identifier2 = e.identifier("allowedTargets");
        m.checkNotNullExpressionValue(identifier2, "identifier(\"allowedTargets\")");
        c = identifier2;
        e identifier3 = e.identifier("value");
        m.checkNotNullExpressionValue(identifier3, "identifier(\"value\")");
        d = identifier3;
        b bVar = k.a.A;
        b bVar2 = a0.c;
        b bVar3 = k.a.D;
        b bVar4 = a0.d;
        b bVar5 = k.a.E;
        b bVar6 = a0.g;
        b bVar7 = k.a.F;
        b bVar8 = a0.f;
        e = h0.mapOf(o.to(bVar, bVar2), o.to(bVar3, bVar4), o.to(bVar5, bVar6), o.to(bVar7, bVar8));
        f = h0.mapOf(o.to(bVar2, bVar), o.to(bVar4, bVar3), o.to(a0.e, k.a.u), o.to(bVar6, bVar5), o.to(bVar8, bVar7));
    }

    public static /* synthetic */ d0.e0.p.d.m0.c.g1.c mapOrResolveJavaAnnotation$default(c cVar, a aVar, g gVar, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        return cVar.mapOrResolveJavaAnnotation(aVar, gVar, z2);
    }

    public final d0.e0.p.d.m0.c.g1.c findMappedJavaAnnotation(b bVar, d dVar, g gVar) {
        a findAnnotation;
        m.checkNotNullParameter(bVar, "kotlinName");
        m.checkNotNullParameter(dVar, "annotationOwner");
        m.checkNotNullParameter(gVar, "c");
        if (m.areEqual(bVar, k.a.u)) {
            b bVar2 = a0.e;
            m.checkNotNullExpressionValue(bVar2, "DEPRECATED_ANNOTATION");
            a findAnnotation2 = dVar.findAnnotation(bVar2);
            if (findAnnotation2 != null || dVar.isDeprecatedInJavaDoc()) {
                return new e(findAnnotation2, gVar);
            }
        }
        b bVar3 = e.get(bVar);
        if (bVar3 == null || (findAnnotation = dVar.findAnnotation(bVar3)) == null) {
            return null;
        }
        return mapOrResolveJavaAnnotation$default(this, findAnnotation, gVar, false, 4, null);
    }

    public final e getDEPRECATED_ANNOTATION_MESSAGE$descriptors_jvm() {
        return f3287b;
    }

    public final e getRETENTION_ANNOTATION_VALUE$descriptors_jvm() {
        return d;
    }

    public final e getTARGET_ANNOTATION_ALLOWED_TARGETS$descriptors_jvm() {
        return c;
    }

    public final d0.e0.p.d.m0.c.g1.c mapOrResolveJavaAnnotation(a aVar, g gVar, boolean z2) {
        m.checkNotNullParameter(aVar, "annotation");
        m.checkNotNullParameter(gVar, "c");
        d0.e0.p.d.m0.g.a classId = aVar.getClassId();
        if (m.areEqual(classId, d0.e0.p.d.m0.g.a.topLevel(a0.c))) {
            return new i(aVar, gVar);
        }
        if (m.areEqual(classId, d0.e0.p.d.m0.g.a.topLevel(a0.d))) {
            return new h(aVar, gVar);
        }
        if (m.areEqual(classId, d0.e0.p.d.m0.g.a.topLevel(a0.g))) {
            return new b(gVar, aVar, k.a.E);
        }
        if (m.areEqual(classId, d0.e0.p.d.m0.g.a.topLevel(a0.f))) {
            return new b(gVar, aVar, k.a.F);
        }
        if (m.areEqual(classId, d0.e0.p.d.m0.g.a.topLevel(a0.e))) {
            return null;
        }
        return new d0.e0.p.d.m0.e.a.i0.l.e(gVar, aVar, z2);
    }
}
