package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.c.i1.c0;
import d0.e0.p.d.m0.e.a.k0.n;
import d0.e0.p.d.m0.k.v.g;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
/* compiled from: LazyJavaScope.kt */
/* loaded from: classes3.dex */
public final class l extends o implements Function0<g<?>> {
    public final /* synthetic */ n $field;
    public final /* synthetic */ c0 $propertyDescriptor;
    public final /* synthetic */ k this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l(k kVar, n nVar, c0 c0Var) {
        super(0);
        this.this$0 = kVar;
        this.$field = nVar;
        this.$propertyDescriptor = c0Var;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final g<?> invoke() {
        return this.this$0.c.getComponents().getJavaPropertyInitializerEvaluator().getInitializerConstant(this.$field, this.$propertyDescriptor);
    }
}
