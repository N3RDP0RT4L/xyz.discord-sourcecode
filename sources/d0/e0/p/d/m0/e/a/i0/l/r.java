package d0.e0.p.d.m0.e.a.i0.l;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.i0.g;
import d0.e0.p.d.m0.e.a.i0.l.k;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.n.c0;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
/* compiled from: LazyJavaStaticScope.kt */
/* loaded from: classes3.dex */
public abstract class r extends k {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public r(g gVar) {
        super(gVar, null, 2, null);
        m.checkNotNullParameter(gVar, "c");
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public void g(e eVar, Collection<n0> collection) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(collection, "result");
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public q0 i() {
        return null;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public k.a l(d0.e0.p.d.m0.e.a.k0.r rVar, List<? extends z0> list, c0 c0Var, List<? extends c1> list2) {
        m.checkNotNullParameter(rVar, "method");
        m.checkNotNullParameter(list, "methodTypeParameters");
        m.checkNotNullParameter(c0Var, "returnType");
        m.checkNotNullParameter(list2, "valueParameters");
        return new k.a(c0Var, null, list2, list, false, n.emptyList());
    }
}
