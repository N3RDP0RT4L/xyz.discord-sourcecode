package d0.e0.p.d.m0.e.a.i0.m;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.j.c;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.i0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.l1.f;
import d0.e0.p.d.m0.n.v;
import d0.e0.p.d.m0.n.w0;
import d0.g0.w;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
/* compiled from: RawType.kt */
/* loaded from: classes3.dex */
public final class g extends v implements i0 {

    /* compiled from: RawType.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<String, CharSequence> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final CharSequence invoke(String str) {
            m.checkNotNullParameter(str, "it");
            return m.stringPlus("(raw) ", str);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public g(j0 j0Var, j0 j0Var2) {
        super(j0Var, j0Var2);
        m.checkNotNullParameter(j0Var, "lowerBound");
        m.checkNotNullParameter(j0Var2, "upperBound");
        f.a.isSubtypeOf(j0Var, j0Var2);
    }

    public static final List<String> a(c cVar, c0 c0Var) {
        List<w0> arguments = c0Var.getArguments();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(arguments, 10));
        for (w0 w0Var : arguments) {
            arrayList.add(cVar.renderTypeProjection(w0Var));
        }
        return arrayList;
    }

    public static final String b(String str, String str2) {
        if (!w.contains$default((CharSequence) str, '<', false, 2, (Object) null)) {
            return str;
        }
        return w.substringBefore$default(str, '<', (String) null, 2, (Object) null) + '<' + str2 + '>' + w.substringAfterLast$default(str, '>', null, 2, null);
    }

    @Override // d0.e0.p.d.m0.n.v
    public j0 getDelegate() {
        return getLowerBound();
    }

    @Override // d0.e0.p.d.m0.n.v, d0.e0.p.d.m0.n.c0
    public i getMemberScope() {
        h declarationDescriptor = getConstructor().getDeclarationDescriptor();
        e eVar = declarationDescriptor instanceof e ? (e) declarationDescriptor : null;
        if (eVar != null) {
            i memberScope = eVar.getMemberScope(f.f3313b);
            m.checkNotNullExpressionValue(memberScope, "classDescriptor.getMemberScope(RawSubstitution)");
            return memberScope;
        }
        throw new IllegalStateException(m.stringPlus("Incorrect classifier: ", getConstructor().getDeclarationDescriptor()).toString());
    }

    @Override // d0.e0.p.d.m0.n.v
    public String render(c cVar, d0.e0.p.d.m0.j.h hVar) {
        boolean z2;
        m.checkNotNullParameter(cVar, "renderer");
        m.checkNotNullParameter(hVar, "options");
        String renderType = cVar.renderType(getLowerBound());
        String renderType2 = cVar.renderType(getUpperBound());
        if (hVar.getDebugMode()) {
            return "raw (" + renderType + ".." + renderType2 + ')';
        } else if (getUpperBound().getArguments().isEmpty()) {
            return cVar.renderFlexibleType(renderType, renderType2, d0.e0.p.d.m0.n.o1.a.getBuiltIns(this));
        } else {
            List<String> a2 = a(cVar, getLowerBound());
            List<String> a3 = a(cVar, getUpperBound());
            String joinToString$default = u.joinToString$default(a2, ", ", null, null, 0, null, a.j, 30, null);
            List<Pair> zip = u.zip(a2, a3);
            boolean z3 = false;
            if (!(zip instanceof Collection) || !zip.isEmpty()) {
                for (Pair pair : zip) {
                    String str = (String) pair.getSecond();
                    if (m.areEqual((String) pair.getFirst(), w.removePrefix(str, "out ")) || m.areEqual(str, "*")) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (!z2) {
                        break;
                    }
                }
            }
            z3 = true;
            if (z3) {
                renderType2 = b(renderType2, joinToString$default);
            }
            String b2 = b(renderType, joinToString$default);
            return m.areEqual(b2, renderType2) ? b2 : cVar.renderFlexibleType(b2, renderType2, d0.e0.p.d.m0.n.o1.a.getBuiltIns(this));
        }
    }

    @Override // d0.e0.p.d.m0.n.i1
    public g makeNullableAsSpecified(boolean z2) {
        return new g(getLowerBound().makeNullableAsSpecified(z2), getUpperBound().makeNullableAsSpecified(z2));
    }

    @Override // d0.e0.p.d.m0.n.i1
    public g replaceAnnotations(d0.e0.p.d.m0.c.g1.g gVar) {
        m.checkNotNullParameter(gVar, "newAnnotations");
        return new g(getLowerBound().replaceAnnotations(gVar), getUpperBound().replaceAnnotations(gVar));
    }

    public g(j0 j0Var, j0 j0Var2, boolean z2) {
        super(j0Var, j0Var2);
        if (!z2) {
            f.a.isSubtypeOf(j0Var, j0Var2);
        }
    }

    @Override // d0.e0.p.d.m0.n.i1, d0.e0.p.d.m0.n.c0
    public v refine(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return new g((j0) gVar.refineType(getLowerBound()), (j0) gVar.refineType(getUpperBound()), true);
    }
}
