package d0.e0.p.d.m0.e.a.l0;

import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: typeQualifiers.kt */
/* loaded from: classes3.dex */
public final class e {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public static final e f3316b = new e(null, null, false, false, 8, null);
    public final h c;
    public final f d;
    public final boolean e;
    public final boolean f;

    /* compiled from: typeQualifiers.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final e getNONE() {
            return e.f3316b;
        }
    }

    public e(h hVar, f fVar, boolean z2, boolean z3) {
        this.c = hVar;
        this.d = fVar;
        this.e = z2;
        this.f = z3;
    }

    public final f getMutability() {
        return this.d;
    }

    public final h getNullability() {
        return this.c;
    }

    public final boolean isNotNullTypeParameter() {
        return this.e;
    }

    public final boolean isNullabilityQualifierForWarning() {
        return this.f;
    }

    public /* synthetic */ e(h hVar, f fVar, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(hVar, fVar, z2, (i & 8) != 0 ? false : z3);
    }
}
