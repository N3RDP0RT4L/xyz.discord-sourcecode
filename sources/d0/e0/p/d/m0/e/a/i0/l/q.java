package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.p.b;
import d0.z.d.m;
import java.util.Collection;
import java.util.Set;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: LazyJavaStaticClassScope.kt */
/* loaded from: classes3.dex */
public final class q extends b.AbstractC0368b<e, Unit> {
    public final /* synthetic */ e a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Set<R> f3310b;
    public final /* synthetic */ Function1<i, Collection<R>> c;

    /* JADX WARN: Multi-variable type inference failed */
    public q(e eVar, Set<R> set, Function1<? super i, ? extends Collection<? extends R>> function1) {
        this.a = eVar;
        this.f3310b = set;
        this.c = function1;
    }

    @Override // d0.e0.p.d.m0.p.b.d
    public void result() {
    }

    public boolean beforeChildren(e eVar) {
        m.checkNotNullParameter(eVar, "current");
        if (eVar == this.a) {
            return true;
        }
        i staticScope = eVar.getStaticScope();
        m.checkNotNullExpressionValue(staticScope, "current.staticScope");
        if (!(staticScope instanceof r)) {
            return true;
        }
        this.f3310b.addAll((Collection) this.c.invoke(staticScope));
        return false;
    }
}
