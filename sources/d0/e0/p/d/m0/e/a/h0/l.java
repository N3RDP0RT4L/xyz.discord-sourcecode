package d0.e0.p.d.m0.e.a.h0;

import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
/* compiled from: util.kt */
/* loaded from: classes3.dex */
public final class l {
    public final c0 a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3290b;

    public l(c0 c0Var, boolean z2) {
        m.checkNotNullParameter(c0Var, "type");
        this.a = c0Var;
        this.f3290b = z2;
    }

    public final boolean getHasDefaultValue() {
        return this.f3290b;
    }

    public final c0 getType() {
        return this.a;
    }
}
