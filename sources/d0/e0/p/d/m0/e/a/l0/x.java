package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.b.q.d;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.g1.k;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.e.a.a0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.l1.p;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
/* compiled from: typeEnhancement.kt */
/* loaded from: classes3.dex */
public final class x {
    public static final b a;

    /* renamed from: b  reason: collision with root package name */
    public static final b f3326b;

    /* compiled from: typeEnhancement.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a = {1, 2};

        /* renamed from: b  reason: collision with root package name */
        public static final /* synthetic */ int[] f3327b = {1, 2};

        static {
            f.values();
            h.values();
        }
    }

    static {
        b bVar = a0.o;
        m.checkNotNullExpressionValue(bVar, "ENHANCED_NULLABILITY_ANNOTATION");
        a = new b(bVar);
        b bVar2 = a0.p;
        m.checkNotNullExpressionValue(bVar2, "ENHANCED_MUTABILITY_ANNOTATION");
        f3326b = new b(bVar2);
    }

    public static final <T> c<T> a(T t) {
        return new c<>(t, null);
    }

    public static final g access$compositeAnnotationsOrSingle(List list) {
        int size = list.size();
        if (size == 0) {
            throw new IllegalStateException("At least one Annotations object expected".toString());
        } else if (size != 1) {
            return new k(u.toList(list));
        } else {
            return (g) u.single((List<? extends Object>) list);
        }
    }

    public static final c access$enhanceMutability(h hVar, e eVar, t tVar) {
        c cVar;
        if (u.shouldEnhance(tVar) && (hVar instanceof e)) {
            d dVar = d.a;
            f mutability = eVar.getMutability();
            int i = mutability == null ? -1 : a.a[mutability.ordinal()];
            if (i != 1) {
                if (i == 2 && tVar == t.FLEXIBLE_UPPER) {
                    e eVar2 = (e) hVar;
                    if (dVar.isReadOnly(eVar2)) {
                        cVar = new c(dVar.convertReadOnlyToMutable(eVar2), f3326b);
                        return cVar;
                    }
                }
                return a(hVar);
            }
            if (tVar == t.FLEXIBLE_LOWER) {
                e eVar3 = (e) hVar;
                if (dVar.isMutable(eVar3)) {
                    cVar = new c(dVar.convertMutableToReadOnly(eVar3), f3326b);
                    return cVar;
                }
            }
            return a(hVar);
        }
        return a(hVar);
    }

    public static final c access$getEnhancedNullability(c0 c0Var, e eVar, t tVar) {
        c cVar;
        if (!u.shouldEnhance(tVar)) {
            return a(Boolean.valueOf(c0Var.isMarkedNullable()));
        }
        h nullability = eVar.getNullability();
        int i = nullability == null ? -1 : a.f3327b[nullability.ordinal()];
        if (i == 1) {
            cVar = new c(Boolean.TRUE, a);
        } else if (i != 2) {
            return a(Boolean.valueOf(c0Var.isMarkedNullable()));
        } else {
            cVar = new c(Boolean.FALSE, a);
        }
        return cVar;
    }

    public static final boolean hasEnhancedNullability(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return v.hasEnhancedNullability(p.a, c0Var);
    }
}
