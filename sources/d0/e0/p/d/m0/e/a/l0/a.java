package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.n.c0;
import java.util.Map;
/* compiled from: typeEnhancement.kt */
/* loaded from: classes3.dex */
public final class a implements c {
    public static final a a = new a();

    public final Void a() {
        throw new IllegalStateException("No methods should be called on this descriptor. Only its presence matters".toString());
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public Map<e, g<?>> getAllValueArguments() {
        a();
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public b getFqName() {
        return c.a.getFqName(this);
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public u0 getSource() {
        a();
        throw null;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public c0 getType() {
        a();
        throw null;
    }

    public String toString() {
        return "[EnhancedType]";
    }
}
