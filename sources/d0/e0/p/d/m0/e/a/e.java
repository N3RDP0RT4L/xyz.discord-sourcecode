package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.k.v.j;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function2;
/* compiled from: AnnotationTypeQualifierResolver.kt */
/* loaded from: classes3.dex */
public final class e extends o implements Function2<j, a, Boolean> {
    public final /* synthetic */ c this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public e(c cVar) {
        super(2);
        this.this$0 = cVar;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Boolean invoke(j jVar, a aVar) {
        return Boolean.valueOf(invoke2(jVar, aVar));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(j jVar, a aVar) {
        m.checkNotNullParameter(jVar, "<this>");
        m.checkNotNullParameter(aVar, "it");
        return c.access$toKotlinTargetNames(this.this$0, aVar.getJavaTarget()).contains(jVar.getEnumEntryName().getIdentifier());
    }
}
