package d0.e0.p.d.m0.e.a.l0;

import com.adjust.sdk.Constants;
import d0.e0.p.d.m0.e.a.a0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.n.d1;
import d0.e0.p.d.m0.n.n1.h;
import d0.t.o0;
import d0.t.u;
import d0.z.d.m;
import java.util.Set;
/* compiled from: typeEnchancementUtils.kt */
/* loaded from: classes3.dex */
public final class v {
    public static final e createJavaTypeQualifiers(h hVar, f fVar, boolean z2, boolean z3) {
        if (!z3 || hVar != h.NOT_NULL) {
            return new e(hVar, fVar, false, z2);
        }
        return new e(hVar, fVar, true, z2);
    }

    public static final boolean hasEnhancedNullability(d1 d1Var, h hVar) {
        m.checkNotNullParameter(d1Var, "<this>");
        m.checkNotNullParameter(hVar, "type");
        b bVar = a0.o;
        m.checkNotNullExpressionValue(bVar, "ENHANCED_NULLABILITY_ANNOTATION");
        return d1Var.hasAnnotation(hVar, bVar);
    }

    public static final <T> T select(Set<? extends T> set, T t, T t2, T t3, boolean z2) {
        Set<? extends T> set2;
        m.checkNotNullParameter(set, "<this>");
        m.checkNotNullParameter(t, Constants.LOW);
        m.checkNotNullParameter(t2, Constants.HIGH);
        if (z2) {
            T t4 = set.contains(t) ? t : set.contains(t2) ? t2 : null;
            if (!m.areEqual(t4, t) || !m.areEqual(t3, t2)) {
                return t3 == null ? t4 : t3;
            }
            return null;
        }
        if (!(t3 == null || (set2 = u.toSet(o0.plus(set, t3))) == null)) {
            set = set2;
        }
        return (T) u.singleOrNull(set);
    }

    public static final h select(Set<? extends h> set, h hVar, boolean z2) {
        m.checkNotNullParameter(set, "<this>");
        h hVar2 = h.FORCE_FLEXIBILITY;
        return hVar == hVar2 ? hVar2 : (h) select(set, h.NOT_NULL, h.NULLABLE, hVar, z2);
    }
}
