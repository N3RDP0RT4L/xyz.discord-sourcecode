package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.e.a.l0.q;
import java.util.Map;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: predefinedEnhancementInfo.kt */
/* loaded from: classes3.dex */
public final class j {
    public static final d0.e0.p.d.m0.e.a.l0.e a = new d0.e0.p.d.m0.e.a.l0.e(d0.e0.p.d.m0.e.a.l0.h.NULLABLE, null, false, false, 8, null);

    /* renamed from: b  reason: collision with root package name */
    public static final d0.e0.p.d.m0.e.a.l0.e f3318b;
    public static final d0.e0.p.d.m0.e.a.l0.e c;
    public static final Map<String, d0.e0.p.d.m0.e.a.l0.k> d;

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class a extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFConsumer;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str) {
            super(1);
            this.$JFConsumer = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JFConsumer, j.f3318b, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class a0 extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a0(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.returns(d0.e0.p.d.m0.k.y.d.BOOLEAN);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class b0 extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b0(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.returns(d0.e0.p.d.m0.k.y.d.BOOLEAN);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class c extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JLObject, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class d extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.returns(this.$JLObject, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class e extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.returns(this.$JLObject, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class f extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.returns(this.$JLObject, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class g extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ d0.e0.p.d.m0.e.b.w $this_anonymous;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public g(d0.e0.p.d.m0.e.b.w wVar) {
            super(1);
            this.$this_anonymous = wVar;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.returns(this.$this_anonymous.javaUtil("Spliterator"), j.f3318b, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class h extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFPredicate;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public h(String str) {
            super(1);
            this.$JFPredicate = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JFPredicate, j.f3318b, j.f3318b);
            aVar.returns(d0.e0.p.d.m0.k.y.d.BOOLEAN);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class i extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JUStream;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public i(String str) {
            super(1);
            this.$JUStream = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.returns(this.$JUStream, j.f3318b, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* renamed from: d0.e0.p.d.m0.e.a.l0.j$j  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0307j extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JUStream;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0307j(String str) {
            super(1);
            this.$JUStream = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.returns(this.$JUStream, j.f3318b, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class k extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFUnaryOperator;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public k(String str) {
            super(1);
            this.$JFUnaryOperator = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JFUnaryOperator, j.f3318b, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class l extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFBiConsumer;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public l(String str) {
            super(1);
            this.$JFBiConsumer = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JFBiConsumer, j.f3318b, j.f3318b, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class m extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public m(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.returns(this.$JLObject, j.a);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class n extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public n(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.returns(this.$JLObject, j.a);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class o extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public o(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.returns(d0.e0.p.d.m0.k.y.d.BOOLEAN);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class p extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFBiFunction;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public p(String str) {
            super(1);
            this.$JFBiFunction = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JFBiFunction, j.f3318b, j.f3318b, j.f3318b, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class q extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFBiFunction;
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public q(String str, String str2) {
            super(1);
            this.$JLObject = str;
            this.$JFBiFunction = str2;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JFBiFunction, j.f3318b, j.f3318b, j.a, j.a);
            aVar.returns(this.$JLObject, j.a);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class r extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFFunction;
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public r(String str, String str2) {
            super(1);
            this.$JLObject = str;
            this.$JFFunction = str2;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JFFunction, j.f3318b, j.f3318b, j.f3318b);
            aVar.returns(this.$JLObject, j.f3318b);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class s extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFBiFunction;
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public s(String str, String str2) {
            super(1);
            this.$JLObject = str;
            this.$JFBiFunction = str2;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JFBiFunction, j.f3318b, j.f3318b, j.c, j.a);
            aVar.returns(this.$JLObject, j.a);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class t extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFBiFunction;
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public t(String str, String str2) {
            super(1);
            this.$JLObject = str;
            this.$JFBiFunction = str2;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.f3318b);
            aVar.parameter(this.$JLObject, j.c);
            aVar.parameter(this.$JFBiFunction, j.f3318b, j.c, j.c, j.a);
            aVar.returns(this.$JLObject, j.a);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class u extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JUOptional;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public u(String str) {
            super(1);
            this.$JUOptional = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.returns(this.$JUOptional, j.f3318b, j.c);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class v extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;
        public final /* synthetic */ String $JUOptional;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public v(String str, String str2) {
            super(1);
            this.$JLObject = str;
            this.$JUOptional = str2;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.c);
            aVar.returns(this.$JUOptional, j.f3318b, j.c);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class w extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;
        public final /* synthetic */ String $JUOptional;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public w(String str, String str2) {
            super(1);
            this.$JLObject = str;
            this.$JUOptional = str2;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JLObject, j.a);
            aVar.returns(this.$JUOptional, j.f3318b, j.c);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class x extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public x(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.returns(this.$JLObject, j.c);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class y extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JFConsumer;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public y(String str) {
            super(1);
            this.$JFConsumer = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.parameter(this.$JFConsumer, j.f3318b, j.c);
        }
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public static final class z extends d0.z.d.o implements Function1<q.a.C0309a, Unit> {
        public final /* synthetic */ String $JLObject;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public z(String str) {
            super(1);
            this.$JLObject = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(q.a.C0309a aVar) {
            invoke2(aVar);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(q.a.C0309a aVar) {
            d0.z.d.m.checkNotNullParameter(aVar, "<this>");
            aVar.returns(this.$JLObject, j.a);
        }
    }

    static {
        d0.e0.p.d.m0.e.a.l0.h hVar = d0.e0.p.d.m0.e.a.l0.h.NOT_NULL;
        f3318b = new d0.e0.p.d.m0.e.a.l0.e(hVar, null, false, false, 8, null);
        c = new d0.e0.p.d.m0.e.a.l0.e(hVar, null, true, false, 8, null);
        d0.e0.p.d.m0.e.b.w wVar = d0.e0.p.d.m0.e.b.w.a;
        String javaLang = wVar.javaLang("Object");
        String javaFunction = wVar.javaFunction("Predicate");
        String javaFunction2 = wVar.javaFunction("Function");
        String javaFunction3 = wVar.javaFunction("Consumer");
        String javaFunction4 = wVar.javaFunction("BiFunction");
        String javaFunction5 = wVar.javaFunction("BiConsumer");
        String javaFunction6 = wVar.javaFunction("UnaryOperator");
        String javaUtil = wVar.javaUtil("stream/Stream");
        String javaUtil2 = wVar.javaUtil("Optional");
        d0.e0.p.d.m0.e.a.l0.q qVar = new d0.e0.p.d.m0.e.a.l0.q();
        new q.a(qVar, wVar.javaUtil("Iterator")).function("forEachRemaining", new a(javaFunction3));
        new q.a(qVar, wVar.javaLang("Iterable")).function("spliterator", new g(wVar));
        q.a aVar = new q.a(qVar, wVar.javaUtil("Collection"));
        aVar.function("removeIf", new h(javaFunction));
        aVar.function("stream", new i(javaUtil));
        aVar.function("parallelStream", new C0307j(javaUtil));
        new q.a(qVar, wVar.javaUtil("List")).function("replaceAll", new k(javaFunction6));
        q.a aVar2 = new q.a(qVar, wVar.javaUtil("Map"));
        aVar2.function("forEach", new l(javaFunction5));
        aVar2.function("putIfAbsent", new m(javaLang));
        aVar2.function("replace", new n(javaLang));
        aVar2.function("replace", new o(javaLang));
        aVar2.function("replaceAll", new p(javaFunction4));
        aVar2.function("compute", new q(javaLang, javaFunction4));
        aVar2.function("computeIfAbsent", new r(javaLang, javaFunction2));
        aVar2.function("computeIfPresent", new s(javaLang, javaFunction4));
        aVar2.function("merge", new t(javaLang, javaFunction4));
        q.a aVar3 = new q.a(qVar, javaUtil2);
        aVar3.function("empty", new u(javaUtil2));
        aVar3.function("of", new v(javaLang, javaUtil2));
        aVar3.function("ofNullable", new w(javaLang, javaUtil2));
        aVar3.function("get", new x(javaLang));
        aVar3.function("ifPresent", new y(javaFunction3));
        new q.a(qVar, wVar.javaLang("ref/Reference")).function("get", new z(javaLang));
        new q.a(qVar, javaFunction).function("test", new a0(javaLang));
        new q.a(qVar, wVar.javaFunction("BiPredicate")).function("test", new b0(javaLang));
        new q.a(qVar, javaFunction3).function("accept", new b(javaLang));
        new q.a(qVar, javaFunction5).function("accept", new c(javaLang));
        new q.a(qVar, javaFunction2).function("apply", new d(javaLang));
        new q.a(qVar, javaFunction4).function("apply", new e(javaLang));
        new q.a(qVar, wVar.javaFunction("Supplier")).function("get", new f(javaLang));
        d = qVar.build();
    }

    public static final Map<String, d0.e0.p.d.m0.e.a.l0.k> getPREDEFINED_FUNCTION_ENHANCEMENT_INFO_BY_SIGNATURE() {
        return d;
    }
}
