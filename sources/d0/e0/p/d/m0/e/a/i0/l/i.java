package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.c.i1.a0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.e.a.i0.e;
import d0.e0.p.d.m0.e.a.i0.g;
import d0.e0.p.d.m0.e.a.k0.u;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.e.b.q;
import d0.e0.p.d.m0.e.b.v;
import d0.e0.p.d.m0.m.j;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
/* compiled from: LazyJavaPackageFragment.kt */
/* loaded from: classes3.dex */
public final class i extends a0 {
    public static final /* synthetic */ KProperty<Object>[] o = {d0.z.d.a0.property1(new y(d0.z.d.a0.getOrCreateKotlinClass(i.class), "binaryClasses", "getBinaryClasses$descriptors_jvm()Ljava/util/Map;")), d0.z.d.a0.property1(new y(d0.z.d.a0.getOrCreateKotlinClass(i.class), "partToFacade", "getPartToFacade()Ljava/util/HashMap;"))};
    public final u p;
    public final g q;
    public final j r;

    /* renamed from: s  reason: collision with root package name */
    public final d f3305s;
    public final j<List<d0.e0.p.d.m0.g.b>> t;
    public final d0.e0.p.d.m0.c.g1.g u;

    /* compiled from: LazyJavaPackageFragment.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<Map<String, ? extends p>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Map<String, ? extends p> invoke() {
            v packagePartProvider = i.this.q.getComponents().getPackagePartProvider();
            String asString = i.this.getFqName().asString();
            m.checkNotNullExpressionValue(asString, "fqName.asString()");
            List<String> findPackageParts = packagePartProvider.findPackageParts(asString);
            i iVar = i.this;
            ArrayList arrayList = new ArrayList();
            for (String str : findPackageParts) {
                d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(d0.e0.p.d.m0.k.y.c.byInternalName(str).getFqNameForTopLevelClassMaybeWithDollars());
                m.checkNotNullExpressionValue(aVar, "topLevel(JvmClassName.byInternalName(partName).fqNameForTopLevelClassMaybeWithDollars)");
                p findKotlinClass = d0.e0.p.d.m0.e.b.o.findKotlinClass(iVar.q.getComponents().getKotlinClassFinder(), aVar);
                Pair pair = findKotlinClass == null ? null : d0.o.to(str, findKotlinClass);
                if (pair != null) {
                    arrayList.add(pair);
                }
            }
            return h0.toMap(arrayList);
        }
    }

    /* compiled from: LazyJavaPackageFragment.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<HashMap<d0.e0.p.d.m0.k.y.c, d0.e0.p.d.m0.k.y.c>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final HashMap<d0.e0.p.d.m0.k.y.c, d0.e0.p.d.m0.k.y.c> invoke() {
            String multifileClassName;
            HashMap<d0.e0.p.d.m0.k.y.c, d0.e0.p.d.m0.k.y.c> hashMap = new HashMap<>();
            for (Map.Entry<String, p> entry : i.this.getBinaryClasses$descriptors_jvm().entrySet()) {
                d0.e0.p.d.m0.k.y.c byInternalName = d0.e0.p.d.m0.k.y.c.byInternalName(entry.getKey());
                m.checkNotNullExpressionValue(byInternalName, "byInternalName(partInternalName)");
                d0.e0.p.d.m0.e.b.b0.a classHeader = entry.getValue().getClassHeader();
                int ordinal = classHeader.getKind().ordinal();
                if (ordinal == 2) {
                    hashMap.put(byInternalName, byInternalName);
                } else if (ordinal == 5 && (multifileClassName = classHeader.getMultifileClassName()) != null) {
                    d0.e0.p.d.m0.k.y.c byInternalName2 = d0.e0.p.d.m0.k.y.c.byInternalName(multifileClassName);
                    m.checkNotNullExpressionValue(byInternalName2, "byInternalName(header.multifileClassName ?: continue@kotlinClasses)");
                    hashMap.put(byInternalName, byInternalName2);
                }
            }
            return hashMap;
        }
    }

    /* compiled from: LazyJavaPackageFragment.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<List<? extends d0.e0.p.d.m0.g.b>> {
        public c() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends d0.e0.p.d.m0.g.b> invoke() {
            Collection<u> subPackages = i.this.p.getSubPackages();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(subPackages, 10));
            for (u uVar : subPackages) {
                arrayList.add(uVar.getFqName());
            }
            return arrayList;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public i(g gVar, u uVar) {
        super(gVar.getModule(), uVar.getFqName());
        m.checkNotNullParameter(gVar, "outerContext");
        m.checkNotNullParameter(uVar, "jPackage");
        this.p = uVar;
        g childForClassOrPackage$default = d0.e0.p.d.m0.e.a.i0.a.childForClassOrPackage$default(gVar, this, null, 0, 6, null);
        this.q = childForClassOrPackage$default;
        this.r = childForClassOrPackage$default.getStorageManager().createLazyValue(new a());
        this.f3305s = new d(childForClassOrPackage$default, uVar, this);
        this.t = childForClassOrPackage$default.getStorageManager().createRecursionTolerantLazyValue(new c(), n.emptyList());
        this.u = childForClassOrPackage$default.getComponents().getJavaTypeEnhancementState().getDisabledDefaultAnnotations() ? d0.e0.p.d.m0.c.g1.g.f.getEMPTY() : e.resolveAnnotations(childForClassOrPackage$default, uVar);
        childForClassOrPackage$default.getStorageManager().createLazyValue(new b());
    }

    public final d0.e0.p.d.m0.c.e findClassifierByJavaClass$descriptors_jvm(d0.e0.p.d.m0.e.a.k0.g gVar) {
        m.checkNotNullParameter(gVar, "jClass");
        return this.f3305s.getJavaScope$descriptors_jvm().findClassifierByJavaClass$descriptors_jvm(gVar);
    }

    @Override // d0.e0.p.d.m0.c.g1.b, d0.e0.p.d.m0.c.g1.a
    public d0.e0.p.d.m0.c.g1.g getAnnotations() {
        return this.u;
    }

    public final Map<String, p> getBinaryClasses$descriptors_jvm() {
        return (Map) d0.e0.p.d.m0.m.n.getValue(this.r, this, o[0]);
    }

    @Override // d0.e0.p.d.m0.c.i1.a0, d0.e0.p.d.m0.c.i1.l, d0.e0.p.d.m0.c.p
    public u0 getSource() {
        return new q(this);
    }

    public final List<d0.e0.p.d.m0.g.b> getSubPackageFqNames$descriptors_jvm() {
        return this.t.invoke();
    }

    @Override // d0.e0.p.d.m0.c.i1.a0, d0.e0.p.d.m0.c.i1.k
    public String toString() {
        return m.stringPlus("Lazy Java package fragment: ", getFqName());
    }

    @Override // d0.e0.p.d.m0.c.e0
    public d getMemberScope() {
        return this.f3305s;
    }
}
