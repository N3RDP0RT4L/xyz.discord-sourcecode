package d0.e0.p.d.m0.e.a.i0.m;

import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.e.a.i0.g;
import d0.e0.p.d.m0.e.a.i0.k;
import d0.e0.p.d.m0.e.a.k0.b0;
import d0.e0.p.d.m0.e.a.k0.f;
import d0.e0.p.d.m0.e.a.k0.j;
import d0.e0.p.d.m0.e.a.k0.v;
import d0.e0.p.d.m0.e.a.k0.x;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.u0;
import d0.t.u;
import d0.z.d.m;
/* compiled from: JavaTypeResolver.kt */
/* loaded from: classes3.dex */
public final class d {
    public final g a;

    /* renamed from: b  reason: collision with root package name */
    public final k f3312b;

    public d(g gVar, k kVar) {
        m.checkNotNullParameter(gVar, "c");
        m.checkNotNullParameter(kVar, "typeParameterResolver");
        this.a = gVar;
        this.f3312b = kVar;
    }

    public static final j0 c(j jVar) {
        j0 createErrorType = t.createErrorType(m.stringPlus("Unresolved java class ", jVar.getPresentableText()));
        m.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Unresolved java class ${javaType.presentableText}\")");
        return createErrorType;
    }

    public static /* synthetic */ c0 transformArrayType$default(d dVar, f fVar, a aVar, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        return dVar.transformArrayType(fVar, aVar, z2);
    }

    /* JADX WARN: Code restructure failed: missing block: B:50:0x00df, code lost:
        if (r12 == false) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:95:0x017f, code lost:
        if ((!r5.isEmpty()) != false) goto L97;
     */
    /* JADX WARN: Removed duplicated region for block: B:100:0x0192  */
    /* JADX WARN: Removed duplicated region for block: B:110:0x01da  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x012b A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:70:0x012c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final d0.e0.p.d.m0.n.j0 a(d0.e0.p.d.m0.e.a.k0.j r22, d0.e0.p.d.m0.e.a.i0.m.a r23, d0.e0.p.d.m0.n.j0 r24) {
        /*
            Method dump skipped, instructions count: 717
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.m.d.a(d0.e0.p.d.m0.e.a.k0.j, d0.e0.p.d.m0.e.a.i0.m.a, d0.e0.p.d.m0.n.j0):d0.e0.p.d.m0.n.j0");
    }

    public final u0 b(j jVar) {
        a aVar = a.topLevel(new b(jVar.getClassifierQualifiedName()));
        m.checkNotNullExpressionValue(aVar, "topLevel(FqName(javaType.classifierQualifiedName))");
        u0 typeConstructor = this.a.getComponents().getDeserializedDescriptorResolver().getComponents().getNotFoundClasses().getClass(aVar, d0.t.m.listOf(0)).getTypeConstructor();
        m.checkNotNullExpressionValue(typeConstructor, "c.components.deserializedDescriptorResolver.components.notFoundClasses.getClass(classId, listOf(0)).typeConstructor");
        return typeConstructor;
    }

    public final c0 transformArrayType(f fVar, a aVar, boolean z2) {
        j1 j1Var = j1.OUT_VARIANCE;
        j1 j1Var2 = j1.INVARIANT;
        m.checkNotNullParameter(fVar, "arrayType");
        m.checkNotNullParameter(aVar, "attr");
        x componentType = fVar.getComponentType();
        v vVar = componentType instanceof v ? (v) componentType : null;
        i type = vVar == null ? null : vVar.getType();
        d0.e0.p.d.m0.e.a.i0.d dVar = new d0.e0.p.d.m0.e.a.i0.d(this.a, fVar, true);
        if (type != null) {
            j0 primitiveArrayKotlinType = this.a.getModule().getBuiltIns().getPrimitiveArrayKotlinType(type);
            m.checkNotNullExpressionValue(primitiveArrayKotlinType, "c.module.builtIns.getPrimitiveArrayKotlinType(primitiveType)");
            primitiveArrayKotlinType.replaceAnnotations(d0.e0.p.d.m0.c.g1.g.f.create(u.plus((Iterable) dVar, (Iterable) primitiveArrayKotlinType.getAnnotations())));
            if (aVar.isForAnnotationParameter()) {
                return primitiveArrayKotlinType;
            }
            d0 d0Var = d0.a;
            return d0.flexibleType(primitiveArrayKotlinType, primitiveArrayKotlinType.makeNullableAsSpecified(true));
        }
        c0 transformJavaType = transformJavaType(componentType, e.toAttributes$default(d0.e0.p.d.m0.e.a.g0.k.COMMON, aVar.isForAnnotationParameter(), null, 2, null));
        if (aVar.isForAnnotationParameter()) {
            if (!z2) {
                j1Var = j1Var2;
            }
            j0 arrayType = this.a.getModule().getBuiltIns().getArrayType(j1Var, transformJavaType, dVar);
            m.checkNotNullExpressionValue(arrayType, "c.module.builtIns.getArrayType(projectionKind, componentType, annotations)");
            return arrayType;
        }
        d0 d0Var2 = d0.a;
        j0 arrayType2 = this.a.getModule().getBuiltIns().getArrayType(j1Var2, transformJavaType, dVar);
        m.checkNotNullExpressionValue(arrayType2, "c.module.builtIns.getArrayType(INVARIANT, componentType, annotations)");
        return d0.flexibleType(arrayType2, this.a.getModule().getBuiltIns().getArrayType(j1Var, transformJavaType, dVar).makeNullableAsSpecified(true));
    }

    public final c0 transformJavaType(x xVar, a aVar) {
        j0 j0Var;
        m.checkNotNullParameter(aVar, "attr");
        if (xVar instanceof v) {
            i type = ((v) xVar).getType();
            if (type != null) {
                j0Var = this.a.getModule().getBuiltIns().getPrimitiveKotlinType(type);
            } else {
                j0Var = this.a.getModule().getBuiltIns().getUnitType();
            }
            m.checkNotNullExpressionValue(j0Var, "{\n                val primitiveType = javaType.type\n                if (primitiveType != null) c.module.builtIns.getPrimitiveKotlinType(primitiveType)\n                else c.module.builtIns.unitType\n            }");
            return j0Var;
        } else if (xVar instanceof j) {
            j jVar = (j) xVar;
            boolean z2 = !aVar.isForAnnotationParameter() && aVar.getHowThisTypeIsUsed() != d0.e0.p.d.m0.e.a.g0.k.SUPERTYPE;
            boolean isRaw = jVar.isRaw();
            if (isRaw || z2) {
                j0 a = a(jVar, aVar.withFlexibility(b.FLEXIBLE_LOWER_BOUND), null);
                if (a == null) {
                    return c(jVar);
                }
                j0 a2 = a(jVar, aVar.withFlexibility(b.FLEXIBLE_UPPER_BOUND), a);
                if (a2 == null) {
                    return c(jVar);
                }
                if (isRaw) {
                    return new g(a, a2);
                }
                d0 d0Var = d0.a;
                return d0.flexibleType(a, a2);
            }
            j0 a3 = a(jVar, aVar, null);
            if (a3 == null) {
                a3 = c(jVar);
            }
            return a3;
        } else if (xVar instanceof f) {
            return transformArrayType$default(this, (f) xVar, aVar, false, 4, null);
        } else {
            if (xVar instanceof b0) {
                x bound = ((b0) xVar).getBound();
                c0 transformJavaType = bound == null ? null : transformJavaType(bound, aVar);
                if (transformJavaType != null) {
                    return transformJavaType;
                }
                j0 defaultBound = this.a.getModule().getBuiltIns().getDefaultBound();
                m.checkNotNullExpressionValue(defaultBound, "c.module.builtIns.defaultBound");
                return defaultBound;
            } else if (xVar == null) {
                j0 defaultBound2 = this.a.getModule().getBuiltIns().getDefaultBound();
                m.checkNotNullExpressionValue(defaultBound2, "c.module.builtIns.defaultBound");
                return defaultBound2;
            } else {
                throw new UnsupportedOperationException(m.stringPlus("Unsupported type: ", xVar));
            }
        }
    }
}
