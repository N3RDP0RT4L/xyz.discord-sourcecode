package d0.e0.p.d.m0.e.a.k0;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import java.util.Collection;
import kotlin.jvm.functions.Function1;
/* compiled from: javaElements.kt */
/* loaded from: classes3.dex */
public interface u extends d, l {
    Collection<g> getClasses(Function1<? super e, Boolean> function1);

    b getFqName();

    Collection<u> getSubPackages();
}
