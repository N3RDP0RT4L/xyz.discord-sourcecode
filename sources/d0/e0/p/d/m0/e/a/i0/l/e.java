package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.b.q.d;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.e.a.h0.i;
import d0.e0.p.d.m0.e.a.i0.g;
import d0.e0.p.d.m0.k.v.h;
import d0.e0.p.d.m0.k.v.r;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.k;
import d0.e0.p.d.m0.m.n;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.t;
import d0.t.h0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
/* compiled from: LazyJavaAnnotationDescriptor.kt */
/* loaded from: classes3.dex */
public final class e implements d0.e0.p.d.m0.c.g1.c, i {
    public static final /* synthetic */ KProperty<Object>[] a = {a0.property1(new y(a0.getOrCreateKotlinClass(e.class), "fqName", "getFqName()Lorg/jetbrains/kotlin/name/FqName;")), a0.property1(new y(a0.getOrCreateKotlinClass(e.class), "type", "getType()Lorg/jetbrains/kotlin/types/SimpleType;")), a0.property1(new y(a0.getOrCreateKotlinClass(e.class), "allValueArguments", "getAllValueArguments()Ljava/util/Map;"))};

    /* renamed from: b  reason: collision with root package name */
    public final g f3299b;
    public final d0.e0.p.d.m0.e.a.k0.a c;
    public final k d;
    public final j e;
    public final d0.e0.p.d.m0.e.a.j0.a f;
    public final j g;
    public final boolean h;
    public final boolean i;

    /* compiled from: LazyJavaAnnotationDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<Map<d0.e0.p.d.m0.g.e, ? extends d0.e0.p.d.m0.k.v.g<?>>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Map<d0.e0.p.d.m0.g.e, ? extends d0.e0.p.d.m0.k.v.g<?>> invoke() {
            Collection<d0.e0.p.d.m0.e.a.k0.b> arguments = e.this.c.getArguments();
            e eVar = e.this;
            ArrayList arrayList = new ArrayList();
            for (d0.e0.p.d.m0.e.a.k0.b bVar : arguments) {
                d0.e0.p.d.m0.g.e name = bVar.getName();
                if (name == null) {
                    name = d0.e0.p.d.m0.e.a.a0.f3277b;
                }
                d0.e0.p.d.m0.k.v.g a = eVar.a(bVar);
                Pair pair = a == null ? null : d0.o.to(name, a);
                if (pair != null) {
                    arrayList.add(pair);
                }
            }
            return h0.toMap(arrayList);
        }
    }

    /* compiled from: LazyJavaAnnotationDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<d0.e0.p.d.m0.g.b> {
        public b() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d0.e0.p.d.m0.g.b invoke() {
            d0.e0.p.d.m0.g.a classId = e.this.c.getClassId();
            if (classId == null) {
                return null;
            }
            return classId.asSingleFqName();
        }
    }

    /* compiled from: LazyJavaAnnotationDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<j0> {
        public c() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final j0 invoke() {
            d0.e0.p.d.m0.g.b fqName = e.this.getFqName();
            if (fqName == null) {
                return t.createErrorType(m.stringPlus("No fqName: ", e.this.c));
            }
            d0.e0.p.d.m0.c.e mapJavaToKotlin$default = d.mapJavaToKotlin$default(d.a, fqName, e.this.f3299b.getModule().getBuiltIns(), null, 4, null);
            if (mapJavaToKotlin$default == null) {
                d0.e0.p.d.m0.e.a.k0.g resolve = e.this.c.resolve();
                mapJavaToKotlin$default = resolve == null ? null : e.this.f3299b.getComponents().getModuleClassResolver().resolveClass(resolve);
                if (mapJavaToKotlin$default == null) {
                    mapJavaToKotlin$default = e.access$createTypeForMissingDependencies(e.this, fqName);
                }
            }
            return mapJavaToKotlin$default.getDefaultType();
        }
    }

    public e(g gVar, d0.e0.p.d.m0.e.a.k0.a aVar, boolean z2) {
        m.checkNotNullParameter(gVar, "c");
        m.checkNotNullParameter(aVar, "javaAnnotation");
        this.f3299b = gVar;
        this.c = aVar;
        this.d = gVar.getStorageManager().createNullableLazyValue(new b());
        this.e = gVar.getStorageManager().createLazyValue(new c());
        this.f = gVar.getComponents().getSourceElementFactory().source(aVar);
        this.g = gVar.getStorageManager().createLazyValue(new a());
        this.h = aVar.isIdeExternalAnnotation();
        this.i = aVar.isFreshlySupportedTypeUseAnnotation() || z2;
    }

    public static final d0.e0.p.d.m0.c.e access$createTypeForMissingDependencies(e eVar, d0.e0.p.d.m0.g.b bVar) {
        c0 module = eVar.f3299b.getModule();
        d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(bVar);
        m.checkNotNullExpressionValue(aVar, "topLevel(fqName)");
        return w.findNonGenericClassAcrossDependencies(module, aVar, eVar.f3299b.getComponents().getDeserializedDescriptorResolver().getComponents().getNotFoundClasses());
    }

    public final d0.e0.p.d.m0.k.v.g<?> a(d0.e0.p.d.m0.e.a.k0.b bVar) {
        d0.e0.p.d.m0.k.v.g<?> gVar;
        j0 j0Var = null;
        if (bVar instanceof d0.e0.p.d.m0.e.a.k0.o) {
            return h.a.createConstantValue(((d0.e0.p.d.m0.e.a.k0.o) bVar).getValue());
        }
        if (bVar instanceof d0.e0.p.d.m0.e.a.k0.m) {
            d0.e0.p.d.m0.e.a.k0.m mVar = (d0.e0.p.d.m0.e.a.k0.m) bVar;
            d0.e0.p.d.m0.g.a enumClassId = mVar.getEnumClassId();
            d0.e0.p.d.m0.g.e entryName = mVar.getEntryName();
            if (enumClassId == null || entryName == null) {
                return null;
            }
            return new d0.e0.p.d.m0.k.v.j(enumClassId, entryName);
        }
        if (bVar instanceof d0.e0.p.d.m0.e.a.k0.e) {
            d0.e0.p.d.m0.g.e name = bVar.getName();
            if (name == null) {
                name = d0.e0.p.d.m0.e.a.a0.f3277b;
            }
            m.checkNotNullExpressionValue(name, "argument.name ?: DEFAULT_ANNOTATION_MEMBER_NAME");
            List<d0.e0.p.d.m0.e.a.k0.b> elements = ((d0.e0.p.d.m0.e.a.k0.e) bVar).getElements();
            j0 type = getType();
            m.checkNotNullExpressionValue(type, "type");
            if (e0.isError(type)) {
                return null;
            }
            d0.e0.p.d.m0.c.e annotationClass = d0.e0.p.d.m0.k.x.a.getAnnotationClass(this);
            m.checkNotNull(annotationClass);
            c1 annotationParameterByName = d0.e0.p.d.m0.e.a.g0.a.getAnnotationParameterByName(name, annotationClass);
            if (annotationParameterByName != null) {
                j0Var = annotationParameterByName.getType();
            }
            if (j0Var == null) {
                j0Var = this.f3299b.getComponents().getModule().getBuiltIns().getArrayType(j1.INVARIANT, t.createErrorType("Unknown array element type"));
            }
            m.checkNotNullExpressionValue(j0Var, "DescriptorResolverUtils.getAnnotationParameterByName(argumentName, annotationClass!!)?.type\n            // Try to load annotation arguments even if the annotation class is not found\n                ?: c.components.module.builtIns.getArrayType(\n                    Variance.INVARIANT,\n                    ErrorUtils.createErrorType(\"Unknown array element type\")\n                )");
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(elements, 10));
            for (d0.e0.p.d.m0.e.a.k0.b bVar2 : elements) {
                d0.e0.p.d.m0.k.v.g<?> a2 = a(bVar2);
                if (a2 == null) {
                    a2 = new d0.e0.p.d.m0.k.v.t();
                }
                arrayList.add(a2);
            }
            gVar = h.a.createArrayValue(arrayList, j0Var);
        } else if (bVar instanceof d0.e0.p.d.m0.e.a.k0.c) {
            gVar = new d0.e0.p.d.m0.k.v.a(new e(this.f3299b, ((d0.e0.p.d.m0.e.a.k0.c) bVar).getAnnotation(), false, 4, null));
        } else if (!(bVar instanceof d0.e0.p.d.m0.e.a.k0.h)) {
            return null;
        } else {
            return r.f3448b.create(this.f3299b.getTypeResolver().transformJavaType(((d0.e0.p.d.m0.e.a.k0.h) bVar).getReferencedType(), d0.e0.p.d.m0.e.a.i0.m.e.toAttributes$default(d0.e0.p.d.m0.e.a.g0.k.COMMON, false, null, 3, null)));
        }
        return gVar;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public Map<d0.e0.p.d.m0.g.e, d0.e0.p.d.m0.k.v.g<?>> getAllValueArguments() {
        return (Map) n.getValue(this.g, this, a[2]);
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public d0.e0.p.d.m0.g.b getFqName() {
        return (d0.e0.p.d.m0.g.b) n.getValue(this.d, this, a[0]);
    }

    public final boolean isFreshlySupportedTypeUseAnnotation() {
        return this.i;
    }

    @Override // d0.e0.p.d.m0.e.a.h0.i
    public boolean isIdeExternalAnnotation() {
        return this.h;
    }

    public String toString() {
        return d0.e0.p.d.m0.j.c.renderAnnotation$default(d0.e0.p.d.m0.j.c.f3411b, this, null, 2, null);
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public d0.e0.p.d.m0.e.a.j0.a getSource() {
        return this.f;
    }

    @Override // d0.e0.p.d.m0.c.g1.c
    public j0 getType() {
        return (j0) n.getValue(this.e, this, a[1]);
    }

    public /* synthetic */ e(g gVar, d0.e0.p.d.m0.e.a.k0.a aVar, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(gVar, aVar, (i & 4) != 0 ? false : z2);
    }
}
