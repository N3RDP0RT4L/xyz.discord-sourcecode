package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.k.v.j;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function2;
/* compiled from: AnnotationTypeQualifierResolver.kt */
/* loaded from: classes3.dex */
public final class d extends o implements Function2<j, a, Boolean> {
    public static final d j = new d();

    public d() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Boolean invoke(j jVar, a aVar) {
        return Boolean.valueOf(invoke2(jVar, aVar));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(j jVar, a aVar) {
        m.checkNotNullParameter(jVar, "<this>");
        m.checkNotNullParameter(aVar, "it");
        return m.areEqual(jVar.getEnumEntryName().getIdentifier(), aVar.getJavaTarget());
    }
}
