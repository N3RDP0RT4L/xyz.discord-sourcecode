package d0.e0.p.d.m0.e.a.g0;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.g1.m;
import d0.e0.p.d.m0.c.g1.n;
import d0.e0.p.d.m0.e.a.k0.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.k.v.j;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.t;
import d0.o;
import d0.t.h0;
import d0.t.n0;
import d0.t.r;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: JavaAnnotationMapper.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final d a = new d();

    /* renamed from: b  reason: collision with root package name */
    public static final Map<String, EnumSet<n>> f3288b = h0.mapOf(o.to("PACKAGE", EnumSet.noneOf(n.class)), o.to("TYPE", EnumSet.of(n.CLASS, n.FILE)), o.to("ANNOTATION_TYPE", EnumSet.of(n.ANNOTATION_CLASS)), o.to("TYPE_PARAMETER", EnumSet.of(n.TYPE_PARAMETER)), o.to("FIELD", EnumSet.of(n.FIELD)), o.to("LOCAL_VARIABLE", EnumSet.of(n.LOCAL_VARIABLE)), o.to("PARAMETER", EnumSet.of(n.VALUE_PARAMETER)), o.to("CONSTRUCTOR", EnumSet.of(n.CONSTRUCTOR)), o.to("METHOD", EnumSet.of(n.FUNCTION, n.PROPERTY_GETTER, n.PROPERTY_SETTER)), o.to("TYPE_USE", EnumSet.of(n.TYPE)));
    public static final Map<String, m> c = h0.mapOf(o.to("RUNTIME", m.RUNTIME), o.to("CLASS", m.BINARY), o.to("SOURCE", m.SOURCE));

    /* compiled from: JavaAnnotationMapper.kt */
    /* loaded from: classes3.dex */
    public static final class a extends d0.z.d.o implements Function1<c0, d0.e0.p.d.m0.n.c0> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final d0.e0.p.d.m0.n.c0 invoke(c0 c0Var) {
            d0.z.d.m.checkNotNullParameter(c0Var, "module");
            c1 annotationParameterByName = d0.e0.p.d.m0.e.a.g0.a.getAnnotationParameterByName(c.a.getTARGET_ANNOTATION_ALLOWED_TARGETS$descriptors_jvm(), c0Var.getBuiltIns().getBuiltInClassByFqName(k.a.A));
            d0.e0.p.d.m0.n.c0 type = annotationParameterByName == null ? null : annotationParameterByName.getType();
            if (type != null) {
                return type;
            }
            j0 createErrorType = t.createErrorType("Error: AnnotationTarget[]");
            d0.z.d.m.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Error: AnnotationTarget[]\")");
            return createErrorType;
        }
    }

    public final g<?> mapJavaRetentionArgument$descriptors_jvm(b bVar) {
        d0.e0.p.d.m0.e.a.k0.m mVar = bVar instanceof d0.e0.p.d.m0.e.a.k0.m ? (d0.e0.p.d.m0.e.a.k0.m) bVar : null;
        if (mVar == null) {
            return null;
        }
        Map<String, m> map = c;
        e entryName = mVar.getEntryName();
        m mVar2 = map.get(entryName == null ? null : entryName.asString());
        if (mVar2 == null) {
            return null;
        }
        d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(k.a.C);
        d0.z.d.m.checkNotNullExpressionValue(aVar, "topLevel(StandardNames.FqNames.annotationRetention)");
        e identifier = e.identifier(mVar2.name());
        d0.z.d.m.checkNotNullExpressionValue(identifier, "identifier(retention.name)");
        return new j(aVar, identifier);
    }

    public final Set<n> mapJavaTargetArgumentByName(String str) {
        EnumSet<n> enumSet = f3288b.get(str);
        return enumSet == null ? n0.emptySet() : enumSet;
    }

    public final g<?> mapJavaTargetArguments$descriptors_jvm(List<? extends b> list) {
        d0.z.d.m.checkNotNullParameter(list, "arguments");
        ArrayList<d0.e0.p.d.m0.e.a.k0.m> arrayList = new ArrayList();
        for (Object obj : list) {
            if (obj instanceof d0.e0.p.d.m0.e.a.k0.m) {
                arrayList.add(obj);
            }
        }
        ArrayList<n> arrayList2 = new ArrayList();
        for (d0.e0.p.d.m0.e.a.k0.m mVar : arrayList) {
            e entryName = mVar.getEntryName();
            r.addAll(arrayList2, mapJavaTargetArgumentByName(entryName == null ? null : entryName.asString()));
        }
        ArrayList arrayList3 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList2, 10));
        for (n nVar : arrayList2) {
            d0.e0.p.d.m0.g.a aVar = d0.e0.p.d.m0.g.a.topLevel(k.a.B);
            d0.z.d.m.checkNotNullExpressionValue(aVar, "topLevel(StandardNames.FqNames.annotationTarget)");
            e identifier = e.identifier(nVar.name());
            d0.z.d.m.checkNotNullExpressionValue(identifier, "identifier(kotlinTarget.name)");
            arrayList3.add(new j(aVar, identifier));
        }
        return new d0.e0.p.d.m0.k.v.b(arrayList3, a.j);
    }
}
