package d0.e0.p.d.m0.e.a.l0;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.u;
import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
/* compiled from: signatureEnhancement.kt */
/* loaded from: classes3.dex */
public final class s {
    public final c0 a;

    /* renamed from: b  reason: collision with root package name */
    public final u f3325b;
    public final z0 c;
    public final boolean d;

    public s(c0 c0Var, u uVar, z0 z0Var, boolean z2) {
        m.checkNotNullParameter(c0Var, "type");
        this.a = c0Var;
        this.f3325b = uVar;
        this.c = z0Var;
        this.d = z2;
    }

    public final c0 component1() {
        return this.a;
    }

    public final u component2() {
        return this.f3325b;
    }

    public final z0 component3() {
        return this.c;
    }

    public final boolean component4() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof s)) {
            return false;
        }
        s sVar = (s) obj;
        return m.areEqual(this.a, sVar.a) && m.areEqual(this.f3325b, sVar.f3325b) && m.areEqual(this.c, sVar.c) && this.d == sVar.d;
    }

    public final c0 getType() {
        return this.a;
    }

    public int hashCode() {
        int hashCode = this.a.hashCode() * 31;
        u uVar = this.f3325b;
        int i = 0;
        int hashCode2 = (hashCode + (uVar == null ? 0 : uVar.hashCode())) * 31;
        z0 z0Var = this.c;
        if (z0Var != null) {
            i = z0Var.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z2 = this.d;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return i2 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("TypeAndDefaultQualifiers(type=");
        R.append(this.a);
        R.append(", defaultQualifiers=");
        R.append(this.f3325b);
        R.append(", typeParameterForArgument=");
        R.append(this.c);
        R.append(", isFromStarProjection=");
        R.append(this.d);
        R.append(')');
        return R.toString();
    }
}
