package d0.e0.p.d.m0.e.a.i0.l;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.d0.f;
import d0.e0.p.d.m0.e.a.k0.g;
import d0.e0.p.d.m0.e.a.k0.n;
import d0.e0.p.d.m0.e.a.k0.p;
import d0.e0.p.d.m0.e.a.k0.q;
import d0.e0.p.d.m0.e.a.k0.r;
import d0.e0.p.d.m0.e.a.k0.t;
import d0.e0.p.d.m0.e.a.k0.w;
import d0.e0.p.d.m0.g.e;
import d0.t.g0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: DeclaredMemberIndex.kt */
/* loaded from: classes3.dex */
public class a implements b {
    public final g a;

    /* renamed from: b  reason: collision with root package name */
    public final Function1<q, Boolean> f3297b;
    public final Function1<r, Boolean> c;
    public final Map<e, List<r>> d;
    public final Map<e, n> e;
    public final Map<e, w> f;

    /* compiled from: DeclaredMemberIndex.kt */
    /* renamed from: d0.e0.p.d.m0.e.a.i0.l.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0302a extends o implements Function1<r, Boolean> {
        public C0302a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(r rVar) {
            return Boolean.valueOf(invoke2(rVar));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(r rVar) {
            m.checkNotNullParameter(rVar, "m");
            return ((Boolean) a.this.f3297b.invoke(rVar)).booleanValue() && !p.isObjectMethodInInterface(rVar);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public a(g gVar, Function1<? super q, Boolean> function1) {
        m.checkNotNullParameter(gVar, "jClass");
        m.checkNotNullParameter(function1, "memberFilter");
        this.a = gVar;
        this.f3297b = function1;
        C0302a aVar = new C0302a();
        this.c = aVar;
        Sequence filter = d0.f0.q.filter(u.asSequence(gVar.getMethods()), aVar);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : filter) {
            e name = ((r) obj).getName();
            Object obj2 = linkedHashMap.get(name);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(name, obj2);
            }
            ((List) obj2).add(obj);
        }
        this.d = linkedHashMap;
        Sequence filter2 = d0.f0.q.filter(u.asSequence(this.a.getFields()), this.f3297b);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Object obj3 : filter2) {
            linkedHashMap2.put(((n) obj3).getName(), obj3);
        }
        this.e = linkedHashMap2;
        Collection<w> recordComponents = this.a.getRecordComponents();
        Function1<q, Boolean> function12 = this.f3297b;
        ArrayList arrayList = new ArrayList();
        for (Object obj4 : recordComponents) {
            if (((Boolean) function12.invoke(obj4)).booleanValue()) {
                arrayList.add(obj4);
            }
        }
        LinkedHashMap linkedHashMap3 = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(arrayList, 10)), 16));
        for (Object obj5 : arrayList) {
            linkedHashMap3.put(((w) obj5).getName(), obj5);
        }
        this.f = linkedHashMap3;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.b
    public n findFieldByName(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return this.e.get(eVar);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.b
    public Collection<r> findMethodsByName(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        List<r> list = this.d.get(eVar);
        return list == null ? d0.t.n.emptyList() : list;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.b
    public w findRecordComponentByName(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return this.f.get(eVar);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.b
    public Set<e> getFieldNames() {
        Sequence<t> filter = d0.f0.q.filter(u.asSequence(this.a.getFields()), this.f3297b);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (t tVar : filter) {
            linkedHashSet.add(tVar.getName());
        }
        return linkedHashSet;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.b
    public Set<e> getMethodNames() {
        Sequence<t> filter = d0.f0.q.filter(u.asSequence(this.a.getMethods()), this.c);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (t tVar : filter) {
            linkedHashSet.add(tVar.getName());
        }
        return linkedHashSet;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.b
    public Set<e> getRecordComponentNames() {
        return this.f.keySet();
    }
}
