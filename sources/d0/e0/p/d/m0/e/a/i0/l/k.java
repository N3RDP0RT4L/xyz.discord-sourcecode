package d0.e0.p.d.m0.e.a.i0.l;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.f0;
import d0.e0.p.d.m0.e.a.g0.g;
import d0.e0.p.d.m0.e.a.g0.j;
import d0.e0.p.d.m0.e.a.k0.n;
import d0.e0.p.d.m0.e.a.k0.r;
import d0.e0.p.d.m0.k.a0.c;
import d0.e0.p.d.m0.k.p;
import d0.e0.p.d.m0.m.f;
import d0.e0.p.d.m0.n.c0;
import d0.t.g0;
import d0.t.h0;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.o;
import d0.z.d.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
/* compiled from: LazyJavaScope.kt */
/* loaded from: classes3.dex */
public abstract class k extends d0.e0.p.d.m0.k.a0.j {

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ KProperty<Object>[] f3307b = {a0.property1(new y(a0.getOrCreateKotlinClass(k.class), "functionNamesLazy", "getFunctionNamesLazy()Ljava/util/Set;")), a0.property1(new y(a0.getOrCreateKotlinClass(k.class), "propertyNamesLazy", "getPropertyNamesLazy()Ljava/util/Set;")), a0.property1(new y(a0.getOrCreateKotlinClass(k.class), "classNamesLazy", "getClassNamesLazy()Ljava/util/Set;"))};
    public final d0.e0.p.d.m0.e.a.i0.g c;
    public final k d;
    public final d0.e0.p.d.m0.m.j<Collection<m>> e;
    public final d0.e0.p.d.m0.m.j<d0.e0.p.d.m0.e.a.i0.l.b> f;
    public final d0.e0.p.d.m0.m.h<d0.e0.p.d.m0.g.e, Collection<t0>> g;
    public final d0.e0.p.d.m0.m.i<d0.e0.p.d.m0.g.e, n0> h;
    public final d0.e0.p.d.m0.m.h<d0.e0.p.d.m0.g.e, Collection<t0>> i;
    public final d0.e0.p.d.m0.m.j j;
    public final d0.e0.p.d.m0.m.j k;
    public final d0.e0.p.d.m0.m.j l;
    public final d0.e0.p.d.m0.m.h<d0.e0.p.d.m0.g.e, List<n0>> m;

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final c0 a;

        /* renamed from: b  reason: collision with root package name */
        public final c0 f3308b;
        public final List<c1> c;
        public final List<z0> d;
        public final boolean e;
        public final List<String> f;

        /* JADX WARN: Multi-variable type inference failed */
        public a(c0 c0Var, c0 c0Var2, List<? extends c1> list, List<? extends z0> list2, boolean z2, List<String> list3) {
            d0.z.d.m.checkNotNullParameter(c0Var, "returnType");
            d0.z.d.m.checkNotNullParameter(list, "valueParameters");
            d0.z.d.m.checkNotNullParameter(list2, "typeParameters");
            d0.z.d.m.checkNotNullParameter(list3, "errors");
            this.a = c0Var;
            this.f3308b = c0Var2;
            this.c = list;
            this.d = list2;
            this.e = z2;
            this.f = list3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return d0.z.d.m.areEqual(this.a, aVar.a) && d0.z.d.m.areEqual(this.f3308b, aVar.f3308b) && d0.z.d.m.areEqual(this.c, aVar.c) && d0.z.d.m.areEqual(this.d, aVar.d) && this.e == aVar.e && d0.z.d.m.areEqual(this.f, aVar.f);
        }

        public final List<String> getErrors() {
            return this.f;
        }

        public final boolean getHasStableParameterNames() {
            return this.e;
        }

        public final c0 getReceiverType() {
            return this.f3308b;
        }

        public final c0 getReturnType() {
            return this.a;
        }

        public final List<z0> getTypeParameters() {
            return this.d;
        }

        public final List<c1> getValueParameters() {
            return this.c;
        }

        public int hashCode() {
            int hashCode = this.a.hashCode() * 31;
            c0 c0Var = this.f3308b;
            int hashCode2 = c0Var == null ? 0 : c0Var.hashCode();
            int hashCode3 = (this.d.hashCode() + ((this.c.hashCode() + ((hashCode + hashCode2) * 31)) * 31)) * 31;
            boolean z2 = this.e;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return this.f.hashCode() + ((hashCode3 + i) * 31);
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("MethodSignatureData(returnType=");
            R.append(this.a);
            R.append(", receiverType=");
            R.append(this.f3308b);
            R.append(", valueParameters=");
            R.append(this.c);
            R.append(", typeParameters=");
            R.append(this.d);
            R.append(", hasStableParameterNames=");
            R.append(this.e);
            R.append(", errors=");
            R.append(this.f);
            R.append(')');
            return R.toString();
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public final List<c1> a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f3309b;

        /* JADX WARN: Multi-variable type inference failed */
        public b(List<? extends c1> list, boolean z2) {
            d0.z.d.m.checkNotNullParameter(list, "descriptors");
            this.a = list;
            this.f3309b = z2;
        }

        public final List<c1> getDescriptors() {
            return this.a;
        }

        public final boolean getHasSynthesizedNames() {
            return this.f3309b;
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<Collection<? extends m>> {
        public c() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Collection<? extends m> invoke() {
            k kVar = k.this;
            d0.e0.p.d.m0.k.a0.d dVar = d0.e0.p.d.m0.k.a0.d.m;
            Function1<d0.e0.p.d.m0.g.e, Boolean> all_name_filter = d0.e0.p.d.m0.k.a0.i.a.getALL_NAME_FILTER();
            Objects.requireNonNull(kVar);
            d0.z.d.m.checkNotNullParameter(dVar, "kindFilter");
            d0.z.d.m.checkNotNullParameter(all_name_filter, "nameFilter");
            d0.e0.p.d.m0.d.b.d dVar2 = d0.e0.p.d.m0.d.b.d.WHEN_GET_ALL_DESCRIPTORS;
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            if (dVar.acceptsKinds(d0.e0.p.d.m0.k.a0.d.a.getCLASSIFIERS_MASK())) {
                for (d0.e0.p.d.m0.g.e eVar : kVar.a(dVar, all_name_filter)) {
                    if (all_name_filter.invoke(eVar).booleanValue()) {
                        d0.e0.p.d.m0.p.a.addIfNotNull(linkedHashSet, kVar.getContributedClassifier(eVar, dVar2));
                    }
                }
            }
            if (dVar.acceptsKinds(d0.e0.p.d.m0.k.a0.d.a.getFUNCTIONS_MASK()) && !dVar.getExcludes().contains(c.a.a)) {
                for (d0.e0.p.d.m0.g.e eVar2 : kVar.b(dVar, all_name_filter)) {
                    if (all_name_filter.invoke(eVar2).booleanValue()) {
                        linkedHashSet.addAll(kVar.getContributedFunctions(eVar2, dVar2));
                    }
                }
            }
            if (dVar.acceptsKinds(d0.e0.p.d.m0.k.a0.d.a.getVARIABLES_MASK()) && !dVar.getExcludes().contains(c.a.a)) {
                for (d0.e0.p.d.m0.g.e eVar3 : kVar.h(dVar, all_name_filter)) {
                    if (all_name_filter.invoke(eVar3).booleanValue()) {
                        linkedHashSet.addAll(kVar.getContributedVariables(eVar3, dVar2));
                    }
                }
            }
            return u.toList(linkedHashSet);
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function0<Set<? extends d0.e0.p.d.m0.g.e>> {
        public d() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Set<? extends d0.e0.p.d.m0.g.e> invoke() {
            return k.this.a(d0.e0.p.d.m0.k.a0.d.o, null);
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function1<d0.e0.p.d.m0.g.e, n0> {
        public e() {
            super(1);
        }

        public final n0 invoke(d0.e0.p.d.m0.g.e eVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            k kVar = k.this;
            k kVar2 = kVar.d;
            if (kVar2 != null) {
                return (n0) kVar2.h.invoke(eVar);
            }
            n findFieldByName = kVar.f.invoke().findFieldByName(eVar);
            if (findFieldByName == null || findFieldByName.isEnumEntry()) {
                return null;
            }
            return k.access$resolveProperty(k.this, findFieldByName);
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class f extends o implements Function1<d0.e0.p.d.m0.g.e, Collection<? extends t0>> {
        public f() {
            super(1);
        }

        public final Collection<t0> invoke(d0.e0.p.d.m0.g.e eVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            k kVar = k.this.d;
            if (kVar != null) {
                return (Collection) ((f.m) kVar.g).invoke(eVar);
            }
            ArrayList arrayList = new ArrayList();
            for (r rVar : k.this.f.invoke().findMethodsByName(eVar)) {
                d0.e0.p.d.m0.e.a.h0.f m = k.this.m(rVar);
                if (k.this.k(m)) {
                    ((g.a) k.this.c.getComponents().getJavaResolverCache()).recordMethod(rVar, m);
                    arrayList.add(m);
                }
            }
            k.this.c(arrayList, eVar);
            return arrayList;
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class g extends o implements Function0<d0.e0.p.d.m0.e.a.i0.l.b> {
        public g() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d0.e0.p.d.m0.e.a.i0.l.b invoke() {
            return k.this.d();
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class h extends o implements Function0<Set<? extends d0.e0.p.d.m0.g.e>> {
        public h() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Set<? extends d0.e0.p.d.m0.g.e> invoke() {
            return k.this.b(d0.e0.p.d.m0.k.a0.d.p, null);
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class i extends o implements Function1<d0.e0.p.d.m0.g.e, Collection<? extends t0>> {
        public i() {
            super(1);
        }

        public final Collection<t0> invoke(d0.e0.p.d.m0.g.e eVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            LinkedHashSet linkedHashSet = new LinkedHashSet((Collection) ((f.m) k.this.g).invoke(eVar));
            k.access$retainMostSpecificMethods(k.this, linkedHashSet);
            k.this.f(linkedHashSet, eVar);
            return u.toList(k.this.c.getComponents().getSignatureEnhancement().enhanceSignatures(k.this.c, linkedHashSet));
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* loaded from: classes3.dex */
    public static final class j extends o implements Function1<d0.e0.p.d.m0.g.e, List<? extends n0>> {
        public j() {
            super(1);
        }

        public final List<n0> invoke(d0.e0.p.d.m0.g.e eVar) {
            d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            ArrayList arrayList = new ArrayList();
            d0.e0.p.d.m0.p.a.addIfNotNull(arrayList, k.this.h.invoke(eVar));
            k.this.g(eVar, arrayList);
            if (d0.e0.p.d.m0.k.e.isAnnotationClass(k.this.j())) {
                return u.toList(arrayList);
            }
            return u.toList(k.this.c.getComponents().getSignatureEnhancement().enhanceSignatures(k.this.c, arrayList));
        }
    }

    /* compiled from: LazyJavaScope.kt */
    /* renamed from: d0.e0.p.d.m0.e.a.i0.l.k$k  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0305k extends o implements Function0<Set<? extends d0.e0.p.d.m0.g.e>> {
        public C0305k() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Set<? extends d0.e0.p.d.m0.g.e> invoke() {
            return k.this.h(d0.e0.p.d.m0.k.a0.d.q, null);
        }
    }

    public /* synthetic */ k(d0.e0.p.d.m0.e.a.i0.g gVar, k kVar, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(gVar, (i2 & 2) != 0 ? null : kVar);
    }

    /* JADX WARN: Code restructure failed: missing block: B:20:0x0083, code lost:
        if (r12.getHasConstantNotNullInitializer() != false) goto L22;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final d0.e0.p.d.m0.c.n0 access$resolveProperty(d0.e0.p.d.m0.e.a.i0.l.k r11, d0.e0.p.d.m0.e.a.k0.n r12) {
        /*
            java.util.Objects.requireNonNull(r11)
            boolean r0 = r12.isFinal()
            r1 = 1
            r6 = r0 ^ 1
            d0.e0.p.d.m0.e.a.i0.g r0 = r11.c
            d0.e0.p.d.m0.c.g1.g r3 = d0.e0.p.d.m0.e.a.i0.e.resolveAnnotations(r0, r12)
            d0.e0.p.d.m0.c.m r2 = r11.j()
            d0.e0.p.d.m0.c.z r4 = d0.e0.p.d.m0.c.z.FINAL
            d0.e0.p.d.m0.c.f1 r0 = r12.getVisibility()
            d0.e0.p.d.m0.c.u r5 = d0.e0.p.d.m0.e.a.f0.toDescriptorVisibility(r0)
            d0.e0.p.d.m0.g.e r7 = r12.getName()
            d0.e0.p.d.m0.e.a.i0.g r0 = r11.c
            d0.e0.p.d.m0.e.a.i0.b r0 = r0.getComponents()
            d0.e0.p.d.m0.e.a.j0.b r0 = r0.getSourceElementFactory()
            d0.e0.p.d.m0.e.a.j0.a r8 = r0.source(r12)
            boolean r0 = r12.isFinal()
            r10 = 0
            if (r0 == 0) goto L3f
            boolean r0 = r12.isStatic()
            if (r0 == 0) goto L3f
            r9 = 1
            goto L40
        L3f:
            r9 = 0
        L40:
            d0.e0.p.d.m0.e.a.h0.g r0 = d0.e0.p.d.m0.e.a.h0.g.create(r2, r3, r4, r5, r6, r7, r8, r9)
        */
        //  java.lang.String r2 = "create(\n            ownerDescriptor, annotations, Modality.FINAL, field.visibility.toDescriptorVisibility(), isVar, field.name,\n            c.components.sourceElementFactory.source(field), /* isConst = */ field.isFinalStatic\n        )"
        /*
            d0.z.d.m.checkNotNullExpressionValue(r0, r2)
            r2 = 0
            r0.initialize(r2, r2, r2, r2)
            d0.e0.p.d.m0.e.a.i0.g r3 = r11.c
            d0.e0.p.d.m0.e.a.i0.m.d r3 = r3.getTypeResolver()
            d0.e0.p.d.m0.e.a.k0.x r4 = r12.getType()
            d0.e0.p.d.m0.e.a.g0.k r5 = d0.e0.p.d.m0.e.a.g0.k.COMMON
            r6 = 3
            d0.e0.p.d.m0.e.a.i0.m.a r5 = d0.e0.p.d.m0.e.a.i0.m.e.toAttributes$default(r5, r10, r2, r6, r2)
            d0.e0.p.d.m0.n.c0 r3 = r3.transformJavaType(r4, r5)
            boolean r4 = d0.e0.p.d.m0.b.h.isPrimitiveType(r3)
            if (r4 != 0) goto L6e
            boolean r4 = d0.e0.p.d.m0.b.h.isString(r3)
            if (r4 == 0) goto L86
        L6e:
            boolean r4 = r12.isFinal()
            if (r4 == 0) goto L7c
            boolean r4 = r12.isStatic()
            if (r4 == 0) goto L7c
            r4 = 1
            goto L7d
        L7c:
            r4 = 0
        L7d:
            if (r4 == 0) goto L86
            boolean r4 = r12.getHasConstantNotNullInitializer()
            if (r4 == 0) goto L86
            goto L87
        L86:
            r1 = 0
        L87:
            if (r1 == 0) goto L92
            d0.e0.p.d.m0.n.c0 r3 = d0.e0.p.d.m0.n.e1.makeNotNullable(r3)
            java.lang.String r1 = "makeNotNullable(propertyType)"
            d0.z.d.m.checkNotNullExpressionValue(r3, r1)
        L92:
            java.util.List r1 = d0.t.n.emptyList()
            d0.e0.p.d.m0.c.q0 r4 = r11.i()
            r0.setType(r3, r1, r4, r2)
            d0.e0.p.d.m0.n.c0 r1 = r0.getType()
            boolean r1 = d0.e0.p.d.m0.k.e.shouldRecordInitializerForProperty(r0, r1)
            if (r1 == 0) goto Lb9
            d0.e0.p.d.m0.e.a.i0.g r1 = r11.c
            d0.e0.p.d.m0.m.o r1 = r1.getStorageManager()
            d0.e0.p.d.m0.e.a.i0.l.l r2 = new d0.e0.p.d.m0.e.a.i0.l.l
            r2.<init>(r11, r12, r0)
            d0.e0.p.d.m0.m.k r1 = r1.createNullableLazyValue(r2)
            r0.setCompileTimeInitializer(r1)
        Lb9:
            d0.e0.p.d.m0.e.a.i0.g r11 = r11.c
            d0.e0.p.d.m0.e.a.i0.b r11 = r11.getComponents()
            d0.e0.p.d.m0.e.a.g0.g r11 = r11.getJavaResolverCache()
            d0.e0.p.d.m0.e.a.g0.g$a r11 = (d0.e0.p.d.m0.e.a.g0.g.a) r11
            r11.recordField(r12, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.l.k.access$resolveProperty(d0.e0.p.d.m0.e.a.i0.l.k, d0.e0.p.d.m0.e.a.k0.n):d0.e0.p.d.m0.c.n0");
    }

    public static final void access$retainMostSpecificMethods(k kVar, Set set) {
        Objects.requireNonNull(kVar);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : set) {
            String computeJvmDescriptor$default = d0.e0.p.d.m0.e.b.u.computeJvmDescriptor$default((t0) obj, false, false, 2, null);
            Object obj2 = linkedHashMap.get(computeJvmDescriptor$default);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(computeJvmDescriptor$default, obj2);
            }
            ((List) obj2).add(obj);
        }
        for (List list : linkedHashMap.values()) {
            if (list.size() != 1) {
                Collection selectMostSpecificInEachOverridableGroup = p.selectMostSpecificInEachOverridableGroup(list, m.j);
                set.removeAll(list);
                set.addAll(selectMostSpecificInEachOverridableGroup);
            }
        }
    }

    public abstract Set<d0.e0.p.d.m0.g.e> a(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1);

    public abstract Set<d0.e0.p.d.m0.g.e> b(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1);

    public void c(Collection<t0> collection, d0.e0.p.d.m0.g.e eVar) {
        d0.z.d.m.checkNotNullParameter(collection, "result");
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
    }

    public abstract d0.e0.p.d.m0.e.a.i0.l.b d();

    public final c0 e(r rVar, d0.e0.p.d.m0.e.a.i0.g gVar) {
        d0.z.d.m.checkNotNullParameter(rVar, "method");
        d0.z.d.m.checkNotNullParameter(gVar, "c");
        return gVar.getTypeResolver().transformJavaType(rVar.getReturnType(), d0.e0.p.d.m0.e.a.i0.m.e.toAttributes$default(d0.e0.p.d.m0.e.a.g0.k.COMMON, rVar.getContainingClass().isAnnotationType(), null, 2, null));
    }

    public abstract void f(Collection<t0> collection, d0.e0.p.d.m0.g.e eVar);

    public abstract void g(d0.e0.p.d.m0.g.e eVar, Collection<n0> collection);

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Set<d0.e0.p.d.m0.g.e> getClassifierNames() {
        return (Set) d0.e0.p.d.m0.m.n.getValue(this.l, this, f3307b[2]);
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public Collection<m> getContributedDescriptors(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
        d0.z.d.m.checkNotNullParameter(dVar, "kindFilter");
        d0.z.d.m.checkNotNullParameter(function1, "nameFilter");
        return this.e.invoke();
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Collection<t0> getContributedFunctions(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return !getFunctionNames().contains(eVar) ? d0.t.n.emptyList() : (Collection) ((f.m) this.i).invoke(eVar);
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Collection<n0> getContributedVariables(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        d0.z.d.m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return !getVariableNames().contains(eVar) ? d0.t.n.emptyList() : (Collection) ((f.m) this.m).invoke(eVar);
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Set<d0.e0.p.d.m0.g.e> getFunctionNames() {
        return (Set) d0.e0.p.d.m0.m.n.getValue(this.j, this, f3307b[0]);
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Set<d0.e0.p.d.m0.g.e> getVariableNames() {
        return (Set) d0.e0.p.d.m0.m.n.getValue(this.k, this, f3307b[1]);
    }

    public abstract Set<d0.e0.p.d.m0.g.e> h(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1);

    public abstract q0 i();

    public abstract m j();

    public boolean k(d0.e0.p.d.m0.e.a.h0.f fVar) {
        d0.z.d.m.checkNotNullParameter(fVar, "<this>");
        return true;
    }

    public abstract a l(r rVar, List<? extends z0> list, c0 c0Var, List<? extends c1> list2);

    public final d0.e0.p.d.m0.e.a.h0.f m(r rVar) {
        Map<? extends a.AbstractC0290a<?>, ?> map;
        d0.z.d.m.checkNotNullParameter(rVar, "method");
        d0.e0.p.d.m0.e.a.h0.f createJavaMethod = d0.e0.p.d.m0.e.a.h0.f.createJavaMethod(j(), d0.e0.p.d.m0.e.a.i0.e.resolveAnnotations(this.c, rVar), rVar.getName(), this.c.getComponents().getSourceElementFactory().source(rVar), this.f.invoke().findRecordComponentByName(rVar.getName()) != null && rVar.getValueParameters().isEmpty());
        d0.z.d.m.checkNotNullExpressionValue(createJavaMethod, "createJavaMethod(\n            ownerDescriptor, annotations, method.name, c.components.sourceElementFactory.source(method),\n            declaredMemberIndex().findRecordComponentByName(method.name) != null && method.valueParameters.isEmpty()\n        )");
        d0.e0.p.d.m0.e.a.i0.g childForMethod$default = d0.e0.p.d.m0.e.a.i0.a.childForMethod$default(this.c, createJavaMethod, rVar, 0, 4, null);
        List<d0.e0.p.d.m0.e.a.k0.y> typeParameters = rVar.getTypeParameters();
        List<? extends z0> arrayList = new ArrayList<>(d0.t.o.collectionSizeOrDefault(typeParameters, 10));
        for (d0.e0.p.d.m0.e.a.k0.y yVar : typeParameters) {
            z0 resolveTypeParameter = childForMethod$default.getTypeParameterResolver().resolveTypeParameter(yVar);
            d0.z.d.m.checkNotNull(resolveTypeParameter);
            arrayList.add(resolveTypeParameter);
        }
        b n = n(childForMethod$default, createJavaMethod, rVar.getValueParameters());
        a l = l(rVar, arrayList, e(rVar, childForMethod$default), n.getDescriptors());
        c0 receiverType = l.getReceiverType();
        q0 createExtensionReceiverParameterForCallable = receiverType == null ? null : d0.e0.p.d.m0.k.d.createExtensionReceiverParameterForCallable(createJavaMethod, receiverType, d0.e0.p.d.m0.c.g1.g.f.getEMPTY());
        q0 i2 = i();
        List<z0> typeParameters2 = l.getTypeParameters();
        List<c1> valueParameters = l.getValueParameters();
        c0 returnType = l.getReturnType();
        z convertFromFlags = z.j.convertFromFlags(false, rVar.isAbstract(), !rVar.isFinal());
        d0.e0.p.d.m0.c.u descriptorVisibility = f0.toDescriptorVisibility(rVar.getVisibility());
        if (l.getReceiverType() != null) {
            map = g0.mapOf(d0.o.to(d0.e0.p.d.m0.e.a.h0.f.M, u.first((List<? extends Object>) n.getDescriptors())));
        } else {
            map = h0.emptyMap();
        }
        createJavaMethod.initialize(createExtensionReceiverParameterForCallable, i2, typeParameters2, valueParameters, returnType, convertFromFlags, descriptorVisibility, map);
        createJavaMethod.setParameterNamesStatus(l.getHasStableParameterNames(), n.getHasSynthesizedNames());
        if (!l.getErrors().isEmpty()) {
            ((j.a) childForMethod$default.getComponents().getSignaturePropagator()).reportSignatureErrors(createJavaMethod, l.getErrors());
        }
        return createJavaMethod;
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x007f  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x00b1  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x0103  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x011f  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x0122  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x0135 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final d0.e0.p.d.m0.e.a.i0.l.k.b n(d0.e0.p.d.m0.e.a.i0.g r23, d0.e0.p.d.m0.c.x r24, java.util.List<? extends d0.e0.p.d.m0.e.a.k0.a0> r25) {
        /*
            Method dump skipped, instructions count: 377
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.l.k.n(d0.e0.p.d.m0.e.a.i0.g, d0.e0.p.d.m0.c.x, java.util.List):d0.e0.p.d.m0.e.a.i0.l.k$b");
    }

    public String toString() {
        return d0.z.d.m.stringPlus("Lazy scope for ", j());
    }

    public k(d0.e0.p.d.m0.e.a.i0.g gVar, k kVar) {
        d0.z.d.m.checkNotNullParameter(gVar, "c");
        this.c = gVar;
        this.d = kVar;
        this.e = gVar.getStorageManager().createRecursionTolerantLazyValue(new c(), d0.t.n.emptyList());
        this.f = gVar.getStorageManager().createLazyValue(new g());
        this.g = gVar.getStorageManager().createMemoizedFunction(new f());
        this.h = gVar.getStorageManager().createMemoizedFunctionWithNullableValues(new e());
        this.i = gVar.getStorageManager().createMemoizedFunction(new i());
        this.j = gVar.getStorageManager().createLazyValue(new h());
        this.k = gVar.getStorageManager().createLazyValue(new C0305k());
        this.l = gVar.getStorageManager().createLazyValue(new d());
        this.m = gVar.getStorageManager().createMemoizedFunction(new j());
    }
}
