package d0.e0.p.d.m0.e.a.l0;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: NullabilityQualifierWithMigrationStatus.kt */
/* loaded from: classes3.dex */
public final class i {
    public final h a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3317b;

    public i(h hVar, boolean z2) {
        m.checkNotNullParameter(hVar, "qualifier");
        this.a = hVar;
        this.f3317b = z2;
    }

    public static /* synthetic */ i copy$default(i iVar, h hVar, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            hVar = iVar.a;
        }
        if ((i & 2) != 0) {
            z2 = iVar.f3317b;
        }
        return iVar.copy(hVar, z2);
    }

    public final i copy(h hVar, boolean z2) {
        m.checkNotNullParameter(hVar, "qualifier");
        return new i(hVar, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof i)) {
            return false;
        }
        i iVar = (i) obj;
        return this.a == iVar.a && this.f3317b == iVar.f3317b;
    }

    public final h getQualifier() {
        return this.a;
    }

    public int hashCode() {
        int hashCode = this.a.hashCode() * 31;
        boolean z2 = this.f3317b;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        return hashCode + i;
    }

    public final boolean isForWarningOnly() {
        return this.f3317b;
    }

    public String toString() {
        StringBuilder R = a.R("NullabilityQualifierWithMigrationStatus(qualifier=");
        R.append(this.a);
        R.append(", isForWarningOnly=");
        R.append(this.f3317b);
        R.append(')');
        return R.toString();
    }

    public /* synthetic */ i(h hVar, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(hVar, (i & 2) != 0 ? false : z2);
    }
}
