package d0.e0.p.d.m0.e.a;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.o.m.a;
import d0.g0.t;
import d0.g0.w;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
/* compiled from: propertiesConventionUtil.kt */
/* loaded from: classes3.dex */
public final class c0 {
    public static e a(e eVar, String str, boolean z2, String str2, int i) {
        if ((i & 4) != 0) {
            z2 = true;
        }
        if ((i & 8) != 0) {
            str2 = null;
        }
        if (!eVar.isSpecial()) {
            String identifier = eVar.getIdentifier();
            m.checkNotNullExpressionValue(identifier, "methodName.identifier");
            boolean z3 = false;
            if (t.startsWith$default(identifier, str, false, 2, null) && identifier.length() != str.length()) {
                char charAt = identifier.charAt(str.length());
                if ('a' <= charAt && charAt <= 'z') {
                    z3 = true;
                }
                if (!z3) {
                    if (str2 != null) {
                        return e.identifier(m.stringPlus(str2, w.removePrefix(identifier, str)));
                    }
                    if (!z2) {
                        return eVar;
                    }
                    String decapitalizeSmartForCompiler = a.decapitalizeSmartForCompiler(w.removePrefix(identifier, str), true);
                    if (e.isValidIdentifier(decapitalizeSmartForCompiler)) {
                        return e.identifier(decapitalizeSmartForCompiler);
                    }
                }
            }
        }
        return null;
    }

    public static final List<e> getPropertyNamesCandidatesByAccessorName(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        String asString = eVar.asString();
        m.checkNotNullExpressionValue(asString, "name.asString()");
        z zVar = z.a;
        if (z.isGetterName(asString)) {
            return n.listOfNotNull(propertyNameByGetMethodName(eVar));
        }
        if (z.isSetterName(asString)) {
            return propertyNamesBySetMethodName(eVar);
        }
        return i.a.getPropertyNameCandidatesBySpecialGetterName(eVar);
    }

    public static final e propertyNameByGetMethodName(e eVar) {
        m.checkNotNullParameter(eVar, "methodName");
        e a = a(eVar, "get", false, null, 12);
        return a == null ? a(eVar, "is", false, null, 8) : a;
    }

    public static final e propertyNameBySetMethodName(e eVar, boolean z2) {
        m.checkNotNullParameter(eVar, "methodName");
        return a(eVar, "set", false, z2 ? "is" : null, 4);
    }

    public static final List<e> propertyNamesBySetMethodName(e eVar) {
        m.checkNotNullParameter(eVar, "methodName");
        return n.listOfNotNull((Object[]) new e[]{propertyNameBySetMethodName(eVar, false), propertyNameBySetMethodName(eVar, true)});
    }
}
