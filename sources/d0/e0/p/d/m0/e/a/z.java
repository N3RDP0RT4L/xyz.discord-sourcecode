package d0.e0.p.d.m0.e.a;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.b;
import d0.g0.t;
import d0.z.d.m;
/* compiled from: JvmAbi.kt */
/* loaded from: classes3.dex */
public final class z {
    public static final z a = null;

    /* renamed from: b  reason: collision with root package name */
    public static final b f3332b = new b("kotlin.jvm.JvmField");

    static {
        m.checkNotNullExpressionValue(a.topLevel(new b("kotlin.reflect.jvm.internal.ReflectionFactoryImpl")), "topLevel(FqName(\"kotlin.reflect.jvm.internal.ReflectionFactoryImpl\"))");
    }

    public static final String getterName(String str) {
        m.checkNotNullParameter(str, "propertyName");
        return startsWithIsPrefix(str) ? str : m.stringPlus("get", d0.e0.p.d.m0.o.m.a.capitalizeAsciiOnly(str));
    }

    public static final boolean isGetterName(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return t.startsWith$default(str, "get", false, 2, null) || t.startsWith$default(str, "is", false, 2, null);
    }

    public static final boolean isSetterName(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return t.startsWith$default(str, "set", false, 2, null);
    }

    public static final String setterName(String str) {
        String str2;
        m.checkNotNullParameter(str, "propertyName");
        if (startsWithIsPrefix(str)) {
            str2 = str.substring(2);
            m.checkNotNullExpressionValue(str2, "(this as java.lang.String).substring(startIndex)");
        } else {
            str2 = d0.e0.p.d.m0.o.m.a.capitalizeAsciiOnly(str);
        }
        return m.stringPlus("set", str2);
    }

    public static final boolean startsWithIsPrefix(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        if (!t.startsWith$default(str, "is", false, 2, null) || str.length() == 2) {
            return false;
        }
        char charAt = str.charAt(2);
        return m.compare(97, charAt) > 0 || m.compare(charAt, 122) > 0;
    }
}
