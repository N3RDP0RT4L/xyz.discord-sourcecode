package d0.e0.p.d.m0.e.a.i0;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.m.i;
import d0.f0.q;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: LazyJavaAnnotations.kt */
/* loaded from: classes3.dex */
public final class d implements g {
    public final g j;
    public final d0.e0.p.d.m0.e.a.k0.d k;
    public final boolean l;
    public final i<d0.e0.p.d.m0.e.a.k0.a, c> m;

    /* compiled from: LazyJavaAnnotations.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<d0.e0.p.d.m0.e.a.k0.a, c> {
        public a() {
            super(1);
        }

        public final c invoke(d0.e0.p.d.m0.e.a.k0.a aVar) {
            m.checkNotNullParameter(aVar, "annotation");
            return d0.e0.p.d.m0.e.a.g0.c.a.mapOrResolveJavaAnnotation(aVar, d.this.j, d.this.l);
        }
    }

    public d(g gVar, d0.e0.p.d.m0.e.a.k0.d dVar, boolean z2) {
        m.checkNotNullParameter(gVar, "c");
        m.checkNotNullParameter(dVar, "annotationOwner");
        this.j = gVar;
        this.k = dVar;
        this.l = z2;
        this.m = gVar.getComponents().getStorageManager().createMemoizedFunctionWithNullableValues(new a());
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public c findAnnotation(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        d0.e0.p.d.m0.e.a.k0.a findAnnotation = this.k.findAnnotation(bVar);
        c invoke = findAnnotation == null ? null : this.m.invoke(findAnnotation);
        return invoke == null ? d0.e0.p.d.m0.e.a.g0.c.a.findMappedJavaAnnotation(bVar, this.k, this.j) : invoke;
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public boolean hasAnnotation(b bVar) {
        return g.b.hasAnnotation(this, bVar);
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public boolean isEmpty() {
        return this.k.getAnnotations().isEmpty() && !this.k.isDeprecatedInJavaDoc();
    }

    @Override // java.lang.Iterable
    public Iterator<c> iterator() {
        return q.filterNotNull(q.plus(q.map(u.asSequence(this.k.getAnnotations()), this.m), d0.e0.p.d.m0.e.a.g0.c.a.findMappedJavaAnnotation(k.a.u, this.k, this.j))).iterator();
    }

    public /* synthetic */ d(g gVar, d0.e0.p.d.m0.e.a.k0.d dVar, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(gVar, dVar, (i & 4) != 0 ? false : z2);
    }
}
