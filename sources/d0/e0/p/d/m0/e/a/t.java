package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.e.a.h0.d;
import d0.z.d.m;
/* compiled from: JavaClassesTracker.kt */
/* loaded from: classes3.dex */
public interface t {

    /* compiled from: JavaClassesTracker.kt */
    /* loaded from: classes3.dex */
    public static final class a implements t {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.e.a.t
        public void reportClass(d dVar) {
            m.checkNotNullParameter(dVar, "classDescriptor");
        }
    }

    void reportClass(d dVar);
}
