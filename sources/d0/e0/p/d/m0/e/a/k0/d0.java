package d0.e0.p.d.m0.e.a.k0;

import d0.e0.p.d.m0.g.b;
import d0.z.d.m;
import java.util.Iterator;
/* compiled from: javaElements.kt */
/* loaded from: classes3.dex */
public interface d0 extends d {

    /* compiled from: javaElements.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static d0.e0.p.d.m0.e.a.k0.a findAnnotation(d0 d0Var, b bVar) {
            Object obj;
            m.checkNotNullParameter(d0Var, "this");
            m.checkNotNullParameter(bVar, "fqName");
            Iterator<T> it = d0Var.getAnnotations().iterator();
            while (true) {
                obj = null;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                d0.e0.p.d.m0.g.a classId = ((d0.e0.p.d.m0.e.a.k0.a) next).getClassId();
                if (classId != null) {
                    obj = classId.asSingleFqName();
                }
                if (m.areEqual(obj, bVar)) {
                    obj = next;
                    break;
                }
            }
            return (d0.e0.p.d.m0.e.a.k0.a) obj;
        }
    }
}
