package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.y.c;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
/* compiled from: JvmAnnotationNames.java */
/* loaded from: classes3.dex */
public final class a0 {
    public static final b a;

    /* renamed from: b  reason: collision with root package name */
    public static final e f3277b = e.identifier("value");
    public static final b c = new b(Target.class.getCanonicalName());
    public static final b d = new b(Retention.class.getCanonicalName());
    public static final b e = new b(Deprecated.class.getCanonicalName());
    public static final b f = new b(Documented.class.getCanonicalName());
    public static final b g = new b("java.lang.annotation.Repeatable");
    public static final b h = new b("org.jetbrains.annotations.NotNull");
    public static final b i = new b("org.jetbrains.annotations.Nullable");
    public static final b j = new b("org.jetbrains.annotations.Mutable");
    public static final b k = new b("org.jetbrains.annotations.ReadOnly");
    public static final b l = new b("kotlin.annotations.jvm.ReadOnly");
    public static final b m = new b("kotlin.annotations.jvm.Mutable");
    public static final b n = new b("kotlin.jvm.PurelyImplements");
    public static final b o = new b("kotlin.jvm.internal.EnhancedNullability");
    public static final b p = new b("kotlin.jvm.internal.EnhancedMutability");
    public static final b q = new b("kotlin.annotations.jvm.internal.ParameterName");
    public static final b r = new b("kotlin.annotations.jvm.internal.DefaultValue");

    /* renamed from: s  reason: collision with root package name */
    public static final b f3278s = new b("kotlin.annotations.jvm.internal.DefaultNull");

    static {
        b bVar = new b("kotlin.Metadata");
        a = bVar;
        c.byFqNameWithoutInnerClasses(bVar).getInternalName();
        new b("kotlin.jvm.internal");
    }
}
