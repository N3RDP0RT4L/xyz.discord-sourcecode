package d0.e0.p.d.m0.e.a.l0;

import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: predefinedEnhancementInfo.kt */
/* loaded from: classes3.dex */
public final class k {
    public final w a;

    /* renamed from: b  reason: collision with root package name */
    public final List<w> f3319b;

    public k() {
        this(null, null, 3, null);
    }

    public k(w wVar, List<w> list) {
        m.checkNotNullParameter(list, "parametersInfo");
        this.a = wVar;
        this.f3319b = list;
    }

    public final List<w> getParametersInfo() {
        return this.f3319b;
    }

    public final w getReturnTypeInfo() {
        return this.a;
    }

    public /* synthetic */ k(w wVar, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : wVar, (i & 2) != 0 ? n.emptyList() : list);
    }
}
