package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.c.a1;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.f1;
import d0.e0.p.d.m0.c.i1.h;
import d0.e0.p.d.m0.c.r0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.x0;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.f0;
import d0.e0.p.d.m0.e.a.g0.g;
import d0.e0.p.d.m0.e.a.g0.k;
import d0.e0.p.d.m0.e.a.k0.y;
import d0.e0.p.d.m0.e.a.w;
import d0.e0.p.d.m0.k.a0.g;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.n.u0;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: LazyJavaClassDescriptor.kt */
/* loaded from: classes3.dex */
public final class f extends h implements d0.e0.p.d.m0.e.a.h0.d {
    public final h A;
    public final r0<h> B;
    public final g C;
    public final o D;
    public final d0.e0.p.d.m0.c.g1.g E;
    public final j<List<z0>> F;
    public final d0.e0.p.d.m0.e.a.i0.g r;

    /* renamed from: s  reason: collision with root package name */
    public final d0.e0.p.d.m0.e.a.k0.g f3300s;
    public final e t;
    public final d0.e0.p.d.m0.e.a.i0.g u;
    public final d0.e0.p.d.m0.c.f v;
    public final z w;

    /* renamed from: x  reason: collision with root package name */
    public final f1 f3301x;

    /* renamed from: y  reason: collision with root package name */
    public final boolean f3302y;

    /* renamed from: z  reason: collision with root package name */
    public final b f3303z;

    /* compiled from: LazyJavaClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: LazyJavaClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public final class b extends d0.e0.p.d.m0.n.b {
        public final j<List<z0>> c;
        public final /* synthetic */ f d;

        /* compiled from: LazyJavaClassDescriptor.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<List<? extends z0>> {
            public final /* synthetic */ f this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(f fVar) {
                super(0);
                this.this$0 = fVar;
            }

            @Override // kotlin.jvm.functions.Function0
            public final List<? extends z0> invoke() {
                return a1.computeConstructorTypeParameters(this.this$0);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(f fVar) {
            super(fVar.u.getStorageManager());
            m.checkNotNullParameter(fVar, "this$0");
            this.d = fVar;
            this.c = fVar.u.getStorageManager().createLazyValue(new a(fVar));
        }

        /* JADX WARN: Code restructure failed: missing block: B:24:0x0073, code lost:
            if ((!r7.isRoot() && r7.startsWith(d0.e0.p.d.m0.b.k.k)) != false) goto L26;
         */
        /* JADX WARN: Code restructure failed: missing block: B:29:0x0087, code lost:
            if (r9 == null) goto L30;
         */
        /* JADX WARN: Removed duplicated region for block: B:18:0x0062  */
        /* JADX WARN: Removed duplicated region for block: B:28:0x007b  */
        /* JADX WARN: Removed duplicated region for block: B:31:0x008c  */
        /* JADX WARN: Removed duplicated region for block: B:34:0x00a0  */
        /* JADX WARN: Removed duplicated region for block: B:52:0x0135  */
        /* JADX WARN: Removed duplicated region for block: B:69:0x01b1  */
        /* JADX WARN: Removed duplicated region for block: B:72:0x01d0  */
        /* JADX WARN: Removed duplicated region for block: B:79:0x020f  */
        /* JADX WARN: Removed duplicated region for block: B:80:0x0214  */
        @Override // d0.e0.p.d.m0.n.g
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public java.util.Collection<d0.e0.p.d.m0.n.c0> a() {
            /*
                Method dump skipped, instructions count: 555
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.l.f.b.a():java.util.Collection");
        }

        @Override // d0.e0.p.d.m0.n.g
        public x0 d() {
            return this.d.u.getComponents().getSupertypeLoopChecker();
        }

        @Override // d0.e0.p.d.m0.n.u0
        public List<z0> getParameters() {
            return this.c.invoke();
        }

        @Override // d0.e0.p.d.m0.n.u0
        public boolean isDenotable() {
            return true;
        }

        public String toString() {
            String asString = this.d.getName().asString();
            m.checkNotNullExpressionValue(asString, "name.asString()");
            return asString;
        }

        @Override // d0.e0.p.d.m0.n.b, d0.e0.p.d.m0.n.g, d0.e0.p.d.m0.n.u0
        public e getDeclarationDescriptor() {
            return this.d;
        }
    }

    /* compiled from: LazyJavaClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<List<? extends z0>> {
        public c() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends z0> invoke() {
            List<y> typeParameters = f.this.getJClass().getTypeParameters();
            f fVar = f.this;
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(typeParameters, 10));
            for (y yVar : typeParameters) {
                z0 resolveTypeParameter = fVar.u.getTypeParameterResolver().resolveTypeParameter(yVar);
                if (resolveTypeParameter != null) {
                    arrayList.add(resolveTypeParameter);
                } else {
                    throw new AssertionError("Parameter " + yVar + " surely belongs to class " + fVar.getJClass() + ", so it must be resolved");
                }
            }
            return arrayList;
        }
    }

    /* compiled from: LazyJavaClassDescriptor.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<d0.e0.p.d.m0.n.l1.g, h> {
        public d() {
            super(1);
        }

        public final h invoke(d0.e0.p.d.m0.n.l1.g gVar) {
            m.checkNotNullParameter(gVar, "it");
            d0.e0.p.d.m0.e.a.i0.g gVar2 = f.this.u;
            f fVar = f.this;
            return new h(gVar2, fVar, fVar.getJClass(), f.this.t != null, f.this.A);
        }
    }

    static {
        new a(null);
        n0.setOf((Object[]) new String[]{"equals", "hashCode", "getClass", "wait", "notify", "notifyAll", "toString"});
    }

    public /* synthetic */ f(d0.e0.p.d.m0.e.a.i0.g gVar, d0.e0.p.d.m0.c.m mVar, d0.e0.p.d.m0.e.a.k0.g gVar2, e eVar, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(gVar, mVar, gVar2, (i & 8) != 0 ? null : eVar);
    }

    public final f copy$descriptors_jvm(d0.e0.p.d.m0.e.a.g0.g gVar, e eVar) {
        m.checkNotNullParameter(gVar, "javaResolverCache");
        d0.e0.p.d.m0.e.a.i0.g gVar2 = this.u;
        d0.e0.p.d.m0.e.a.i0.g replaceComponents = d0.e0.p.d.m0.e.a.i0.a.replaceComponents(gVar2, gVar2.getComponents().replace(gVar));
        d0.e0.p.d.m0.c.m containingDeclaration = getContainingDeclaration();
        m.checkNotNullExpressionValue(containingDeclaration, "containingDeclaration");
        return new f(replaceComponents, containingDeclaration, this.f3300s, eVar);
    }

    @Override // d0.e0.p.d.m0.c.g1.a
    public d0.e0.p.d.m0.c.g1.g getAnnotations() {
        return this.E;
    }

    @Override // d0.e0.p.d.m0.c.e
    public e getCompanionObjectDescriptor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.i
    public List<z0> getDeclaredTypeParameters() {
        return this.F.invoke();
    }

    public final d0.e0.p.d.m0.e.a.k0.g getJClass() {
        return this.f3300s;
    }

    @Override // d0.e0.p.d.m0.c.e
    public d0.e0.p.d.m0.c.f getKind() {
        return this.v;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.y
    public z getModality() {
        return this.w;
    }

    @Override // d0.e0.p.d.m0.c.e
    public Collection<e> getSealedSubclasses() {
        if (this.w != z.SEALED) {
            return n.emptyList();
        }
        d0.e0.p.d.m0.e.a.i0.m.a attributes$default = d0.e0.p.d.m0.e.a.i0.m.e.toAttributes$default(k.COMMON, false, null, 3, null);
        Collection<d0.e0.p.d.m0.e.a.k0.j> permittedTypes = this.f3300s.getPermittedTypes();
        ArrayList arrayList = new ArrayList();
        for (d0.e0.p.d.m0.e.a.k0.j jVar : permittedTypes) {
            d0.e0.p.d.m0.c.h declarationDescriptor = this.u.getTypeResolver().transformJavaType(jVar, attributes$default).getConstructor().getDeclarationDescriptor();
            e eVar = declarationDescriptor instanceof e ? (e) declarationDescriptor : null;
            if (eVar != null) {
                arrayList.add(eVar);
            }
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.c.e
    public i getStaticScope() {
        return this.D;
    }

    @Override // d0.e0.p.d.m0.c.h
    public u0 getTypeConstructor() {
        return this.f3303z;
    }

    @Override // d0.e0.p.d.m0.c.i1.a, d0.e0.p.d.m0.c.e
    public i getUnsubstitutedInnerClassesScope() {
        return this.C;
    }

    @Override // d0.e0.p.d.m0.c.i1.u
    public i getUnsubstitutedMemberScope(d0.e0.p.d.m0.n.l1.g gVar) {
        m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
        return this.B.getScope(gVar);
    }

    @Override // d0.e0.p.d.m0.c.e
    public d0.e0.p.d.m0.c.d getUnsubstitutedPrimaryConstructor() {
        return null;
    }

    @Override // d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.q, d0.e0.p.d.m0.c.y
    public u getVisibility() {
        if (!m.areEqual(this.f3301x, t.a) || this.f3300s.getOuterClass() != null) {
            return f0.toDescriptorVisibility(this.f3301x);
        }
        u uVar = w.a;
        m.checkNotNullExpressionValue(uVar, "{\n            JavaDescriptorVisibilities.PACKAGE_VISIBILITY\n        }");
        return uVar;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isActual() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isCompanionObject() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isData() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.y
    public boolean isExpect() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isFun() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isInline() {
        return false;
    }

    @Override // d0.e0.p.d.m0.c.i
    public boolean isInner() {
        return this.f3302y;
    }

    @Override // d0.e0.p.d.m0.c.e
    public boolean isValue() {
        return false;
    }

    public String toString() {
        return m.stringPlus("Lazy Java class ", d0.e0.p.d.m0.k.x.a.getFqNameUnsafe(this));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f(d0.e0.p.d.m0.e.a.i0.g gVar, d0.e0.p.d.m0.c.m mVar, d0.e0.p.d.m0.e.a.k0.g gVar2, e eVar) {
        super(gVar.getStorageManager(), mVar, gVar2.getName(), gVar.getComponents().getSourceElementFactory().source(gVar2), false);
        d0.e0.p.d.m0.c.f fVar;
        z zVar;
        m.checkNotNullParameter(gVar, "outerContext");
        m.checkNotNullParameter(mVar, "containingDeclaration");
        m.checkNotNullParameter(gVar2, "jClass");
        this.r = gVar;
        this.f3300s = gVar2;
        this.t = eVar;
        d0.e0.p.d.m0.e.a.i0.g childForClassOrPackage$default = d0.e0.p.d.m0.e.a.i0.a.childForClassOrPackage$default(gVar, this, gVar2, 0, 4, null);
        this.u = childForClassOrPackage$default;
        ((g.a) childForClassOrPackage$default.getComponents().getJavaResolverCache()).recordClass(gVar2, this);
        gVar2.getLightClassOriginKind();
        if (gVar2.isAnnotationType()) {
            fVar = d0.e0.p.d.m0.c.f.ANNOTATION_CLASS;
        } else if (gVar2.isInterface()) {
            fVar = d0.e0.p.d.m0.c.f.INTERFACE;
        } else {
            fVar = gVar2.isEnum() ? d0.e0.p.d.m0.c.f.ENUM_CLASS : d0.e0.p.d.m0.c.f.CLASS;
        }
        this.v = fVar;
        if (gVar2.isAnnotationType() || gVar2.isEnum()) {
            zVar = z.FINAL;
        } else {
            zVar = z.j.convertFromFlags(false, gVar2.isSealed() || gVar2.isAbstract() || gVar2.isInterface(), !gVar2.isFinal());
        }
        this.w = zVar;
        this.f3301x = gVar2.getVisibility();
        this.f3302y = gVar2.getOuterClass() != null && !gVar2.isStatic();
        this.f3303z = new b(this);
        h hVar = new h(childForClassOrPackage$default, this, gVar2, eVar != null, null, 16, null);
        this.A = hVar;
        this.B = r0.a.create(this, childForClassOrPackage$default.getStorageManager(), childForClassOrPackage$default.getComponents().getKotlinTypeChecker().getKotlinTypeRefiner(), new d());
        this.C = new d0.e0.p.d.m0.k.a0.g(hVar);
        this.D = new o(childForClassOrPackage$default, gVar2, this);
        this.E = d0.e0.p.d.m0.e.a.i0.e.resolveAnnotations(childForClassOrPackage$default, gVar2);
        this.F = childForClassOrPackage$default.getStorageManager().createLazyValue(new c());
    }

    @Override // d0.e0.p.d.m0.c.e
    public List<d0.e0.p.d.m0.c.d> getConstructors() {
        return this.A.getConstructors$descriptors_jvm().invoke();
    }

    @Override // d0.e0.p.d.m0.c.i1.a, d0.e0.p.d.m0.c.e
    public h getUnsubstitutedMemberScope() {
        return (h) super.getUnsubstitutedMemberScope();
    }
}
