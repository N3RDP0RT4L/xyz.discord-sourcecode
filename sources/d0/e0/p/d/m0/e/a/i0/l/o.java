package d0.e0.p.d.m0.e.a.i0.l;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.d.b.d;
import d0.e0.p.d.m0.e.a.h0.k;
import d0.e0.p.d.m0.e.a.k0.g;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.t.n;
import d0.t.r;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: LazyJavaStaticClassScope.kt */
/* loaded from: classes3.dex */
public final class o extends r {
    public final g n;
    public final f o;

    /* compiled from: LazyJavaStaticClassScope.kt */
    /* loaded from: classes3.dex */
    public static final class a extends d0.z.d.o implements Function1<i, Collection<? extends n0>> {
        public final /* synthetic */ e $name;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(e eVar) {
            super(1);
            this.$name = eVar;
        }

        public final Collection<? extends n0> invoke(i iVar) {
            m.checkNotNullParameter(iVar, "it");
            return iVar.getContributedVariables(this.$name, d.WHEN_GET_SUPER_MEMBERS);
        }
    }

    /* compiled from: LazyJavaStaticClassScope.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function1<i, Collection<? extends e>> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        public final Collection<e> invoke(i iVar) {
            m.checkNotNullParameter(iVar, "it");
            return iVar.getVariableNames();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public o(d0.e0.p.d.m0.e.a.i0.g gVar, g gVar2, f fVar) {
        super(gVar);
        m.checkNotNullParameter(gVar, "c");
        m.checkNotNullParameter(gVar2, "jClass");
        m.checkNotNullParameter(fVar, "ownerDescriptor");
        this.n = gVar2;
        this.o = fVar;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set<e> a(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        return d0.t.n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set<e> b(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        Set<e> mutableSet = u.toMutableSet(this.f.invoke().getMethodNames());
        o parentJavaStaticClassScope = k.getParentJavaStaticClassScope(this.o);
        Set<e> functionNames = parentJavaStaticClassScope == null ? null : parentJavaStaticClassScope.getFunctionNames();
        if (functionNames == null) {
            functionNames = d0.t.n0.emptySet();
        }
        mutableSet.addAll(functionNames);
        if (this.n.isEnum()) {
            mutableSet.addAll(n.listOf((Object[]) new e[]{d0.e0.p.d.m0.b.k.c, d0.e0.p.d.m0.b.k.f3187b}));
        }
        return mutableSet;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public d0.e0.p.d.m0.e.a.i0.l.b d() {
        return new d0.e0.p.d.m0.e.a.i0.l.a(this.n, n.j);
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public void f(Collection<t0> collection, e eVar) {
        m.checkNotNullParameter(collection, "result");
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        o parentJavaStaticClassScope = k.getParentJavaStaticClassScope(this.o);
        Collection<? extends t0> resolveOverridesForStaticMembers = d0.e0.p.d.m0.e.a.g0.a.resolveOverridesForStaticMembers(eVar, parentJavaStaticClassScope == null ? d0.t.n0.emptySet() : u.toSet(parentJavaStaticClassScope.getContributedFunctions(eVar, d.WHEN_GET_SUPER_MEMBERS)), collection, this.o, this.c.getComponents().getErrorReporter(), this.c.getComponents().getKotlinTypeChecker().getOverridingUtil());
        m.checkNotNullExpressionValue(resolveOverridesForStaticMembers, "resolveOverridesForStaticMembers(\n            name,\n            functionsFromSupertypes,\n            result,\n            ownerDescriptor,\n            c.components.errorReporter,\n            c.components.kotlinTypeChecker.overridingUtil\n        )");
        collection.addAll(resolveOverridesForStaticMembers);
        if (!this.n.isEnum()) {
            return;
        }
        if (m.areEqual(eVar, d0.e0.p.d.m0.b.k.c)) {
            t0 createEnumValueOfMethod = d0.e0.p.d.m0.k.d.createEnumValueOfMethod(this.o);
            m.checkNotNullExpressionValue(createEnumValueOfMethod, "createEnumValueOfMethod(ownerDescriptor)");
            collection.add(createEnumValueOfMethod);
        } else if (m.areEqual(eVar, d0.e0.p.d.m0.b.k.f3187b)) {
            t0 createEnumValuesMethod = d0.e0.p.d.m0.k.d.createEnumValuesMethod(this.o);
            m.checkNotNullExpressionValue(createEnumValuesMethod, "createEnumValuesMethod(ownerDescriptor)");
            collection.add(createEnumValuesMethod);
        }
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.r, d0.e0.p.d.m0.e.a.i0.l.k
    public void g(e eVar, Collection<n0> collection) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(collection, "result");
        f fVar = this.o;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        d0.e0.p.d.m0.p.b.dfs(d0.t.m.listOf(fVar), p.a, new q(fVar, linkedHashSet, new a(eVar)));
        if (!collection.isEmpty()) {
            Collection<? extends n0> resolveOverridesForStaticMembers = d0.e0.p.d.m0.e.a.g0.a.resolveOverridesForStaticMembers(eVar, linkedHashSet, collection, this.o, this.c.getComponents().getErrorReporter(), this.c.getComponents().getKotlinTypeChecker().getOverridingUtil());
            m.checkNotNullExpressionValue(resolveOverridesForStaticMembers, "resolveOverridesForStaticMembers(\n                    name,\n                    propertiesFromSupertypes,\n                    result,\n                    ownerDescriptor,\n                    c.components.errorReporter,\n                    c.components.kotlinTypeChecker.overridingUtil\n                )");
            collection.addAll(resolveOverridesForStaticMembers);
            return;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : linkedHashSet) {
            n0 o = o((n0) obj);
            Object obj2 = linkedHashMap.get(o);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(o, obj2);
            }
            ((List) obj2).add(obj);
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            Collection resolveOverridesForStaticMembers2 = d0.e0.p.d.m0.e.a.g0.a.resolveOverridesForStaticMembers(eVar, (Collection) entry.getValue(), collection, this.o, this.c.getComponents().getErrorReporter(), this.c.getComponents().getKotlinTypeChecker().getOverridingUtil());
            m.checkNotNullExpressionValue(resolveOverridesForStaticMembers2, "resolveOverridesForStaticMembers(\n                    name, it.value, result, ownerDescriptor, c.components.errorReporter,\n                    c.components.kotlinTypeChecker.overridingUtil\n                )");
            r.addAll(arrayList, resolveOverridesForStaticMembers2);
        }
        collection.addAll(arrayList);
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public h getContributedClassifier(e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return null;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set<e> h(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        Set<e> mutableSet = u.toMutableSet(this.f.invoke().getFieldNames());
        f fVar = this.o;
        d0.e0.p.d.m0.p.b.dfs(d0.t.m.listOf(fVar), p.a, new q(fVar, mutableSet, b.j));
        return mutableSet;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public d0.e0.p.d.m0.c.m j() {
        return this.o;
    }

    public final n0 o(n0 n0Var) {
        if (n0Var.getKind().isReal()) {
            return n0Var;
        }
        Collection<? extends n0> overriddenDescriptors = n0Var.getOverriddenDescriptors();
        m.checkNotNullExpressionValue(overriddenDescriptors, "this.overriddenDescriptors");
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(overriddenDescriptors, 10));
        for (n0 n0Var2 : overriddenDescriptors) {
            m.checkNotNullExpressionValue(n0Var2, "it");
            arrayList.add(o(n0Var2));
        }
        return (n0) u.single((List<? extends Object>) u.distinct(arrayList));
    }
}
