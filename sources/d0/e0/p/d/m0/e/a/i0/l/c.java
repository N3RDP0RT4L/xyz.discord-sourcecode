package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.c.n0;
import d0.z.d.m;
/* compiled from: JavaDescriptorUtil.kt */
/* loaded from: classes3.dex */
public final class c {
    public static final boolean isJavaField(n0 n0Var) {
        m.checkNotNullParameter(n0Var, "<this>");
        return n0Var.getGetter() == null;
    }
}
