package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.c.g1.n;
import d0.e0.p.d.m0.e.a.g0.d;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.m.i;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.p.e;
import d0.e0.p.d.m0.p.h;
import d0.t.r;
import d0.z.d.a0;
import d0.z.d.j;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.reflect.KDeclarationContainer;
/* compiled from: AnnotationTypeQualifierResolver.kt */
/* loaded from: classes3.dex */
public final class c {
    public final e a;

    /* renamed from: b  reason: collision with root package name */
    public final i<d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.g1.c> f3281b;

    /* compiled from: AnnotationTypeQualifierResolver.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final d0.e0.p.d.m0.c.g1.c a;

        /* renamed from: b  reason: collision with root package name */
        public final int f3282b;

        public a(d0.e0.p.d.m0.c.g1.c cVar, int i) {
            m.checkNotNullParameter(cVar, "typeQualifier");
            this.a = cVar;
            this.f3282b = i;
        }

        public final d0.e0.p.d.m0.c.g1.c component1() {
            return this.a;
        }

        public final List<d0.e0.p.d.m0.e.a.a> component2() {
            d0.e0.p.d.m0.e.a.a[] values = d0.e0.p.d.m0.e.a.a.values();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 6; i++) {
                d0.e0.p.d.m0.e.a.a aVar = values[i];
                boolean z2 = true;
                if (!((this.f3282b & (1 << aVar.ordinal())) != 0)) {
                    if (!((this.f3282b & 8) != 0) || aVar == d0.e0.p.d.m0.e.a.a.TYPE_PARAMETER_BOUNDS) {
                        z2 = false;
                    }
                }
                if (z2) {
                    arrayList.add(aVar);
                }
            }
            return arrayList;
        }
    }

    /* compiled from: AnnotationTypeQualifierResolver.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class b extends j implements Function1<d0.e0.p.d.m0.c.e, d0.e0.p.d.m0.c.g1.c> {
        public b(c cVar) {
            super(1, cVar);
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public final String getName() {
            return "computeTypeQualifierNickname";
        }

        @Override // d0.z.d.d
        public final KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinClass(c.class);
        }

        @Override // d0.z.d.d
        public final String getSignature() {
            return "computeTypeQualifierNickname(Lorg/jetbrains/kotlin/descriptors/ClassDescriptor;)Lorg/jetbrains/kotlin/descriptors/annotations/AnnotationDescriptor;";
        }

        public final d0.e0.p.d.m0.c.g1.c invoke(d0.e0.p.d.m0.c.e eVar) {
            m.checkNotNullParameter(eVar, "p0");
            return c.access$computeTypeQualifierNickname((c) this.receiver, eVar);
        }
    }

    public c(o oVar, e eVar) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(eVar, "javaTypeEnhancementState");
        this.a = eVar;
        this.f3281b = oVar.createMemoizedFunctionWithNullableValues(new b(this));
    }

    public static final d0.e0.p.d.m0.c.g1.c access$computeTypeQualifierNickname(c cVar, d0.e0.p.d.m0.c.e eVar) {
        Objects.requireNonNull(cVar);
        if (!eVar.getAnnotations().hasAnnotation(d0.e0.p.d.m0.e.a.b.getTYPE_QUALIFIER_NICKNAME_FQNAME())) {
            return null;
        }
        for (d0.e0.p.d.m0.c.g1.c cVar2 : eVar.getAnnotations()) {
            d0.e0.p.d.m0.c.g1.c resolveTypeQualifierAnnotation = cVar.resolveTypeQualifierAnnotation(cVar2);
            if (resolveTypeQualifierAnnotation != null) {
                return resolveTypeQualifierAnnotation;
            }
        }
        return null;
    }

    public static final List access$toKotlinTargetNames(c cVar, String str) {
        Objects.requireNonNull(cVar);
        Set<n> mapJavaTargetArgumentByName = d.a.mapJavaTargetArgumentByName(str);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(mapJavaTargetArgumentByName, 10));
        for (n nVar : mapJavaTargetArgumentByName) {
            arrayList.add(nVar.name());
        }
        return arrayList;
    }

    public final List<d0.e0.p.d.m0.e.a.a> a(g<?> gVar, Function2<? super d0.e0.p.d.m0.k.v.j, ? super d0.e0.p.d.m0.e.a.a, Boolean> function2) {
        d0.e0.p.d.m0.e.a.a aVar;
        if (gVar instanceof d0.e0.p.d.m0.k.v.b) {
            ArrayList arrayList = new ArrayList();
            for (g<?> gVar2 : ((d0.e0.p.d.m0.k.v.b) gVar).getValue()) {
                r.addAll(arrayList, a(gVar2, function2));
            }
            return arrayList;
        } else if (!(gVar instanceof d0.e0.p.d.m0.k.v.j)) {
            return d0.t.n.emptyList();
        } else {
            d0.e0.p.d.m0.e.a.a[] values = d0.e0.p.d.m0.e.a.a.values();
            int i = 0;
            while (true) {
                if (i >= 6) {
                    aVar = null;
                    break;
                }
                aVar = values[i];
                if (function2.invoke(gVar, aVar).booleanValue()) {
                    break;
                }
                i++;
            }
            return d0.t.n.listOfNotNull(aVar);
        }
    }

    public final a resolveAnnotation(d0.e0.p.d.m0.c.g1.c cVar) {
        m.checkNotNullParameter(cVar, "annotationDescriptor");
        d0.e0.p.d.m0.c.e annotationClass = d0.e0.p.d.m0.k.x.a.getAnnotationClass(cVar);
        if (annotationClass == null) {
            return null;
        }
        d0.e0.p.d.m0.c.g1.g annotations = annotationClass.getAnnotations();
        d0.e0.p.d.m0.g.b bVar = a0.c;
        m.checkNotNullExpressionValue(bVar, "TARGET_ANNOTATION");
        d0.e0.p.d.m0.c.g1.c findAnnotation = annotations.findAnnotation(bVar);
        if (findAnnotation == null) {
            return null;
        }
        Map<d0.e0.p.d.m0.g.e, g<?>> allValueArguments = findAnnotation.getAllValueArguments();
        ArrayList<d0.e0.p.d.m0.e.a.a> arrayList = new ArrayList();
        for (Map.Entry<d0.e0.p.d.m0.g.e, g<?>> entry : allValueArguments.entrySet()) {
            r.addAll(arrayList, a(entry.getValue(), new e(this)));
        }
        int i = 0;
        for (d0.e0.p.d.m0.e.a.a aVar : arrayList) {
            i |= 1 << aVar.ordinal();
        }
        return new a(cVar, i);
    }

    public final h resolveJsr305AnnotationState(d0.e0.p.d.m0.c.g1.c cVar) {
        m.checkNotNullParameter(cVar, "annotationDescriptor");
        h resolveJsr305CustomState = resolveJsr305CustomState(cVar);
        return resolveJsr305CustomState == null ? this.a.getGlobalJsr305Level() : resolveJsr305CustomState;
    }

    public final h resolveJsr305CustomState(d0.e0.p.d.m0.c.g1.c cVar) {
        m.checkNotNullParameter(cVar, "annotationDescriptor");
        Map<String, h> userDefinedLevelForSpecificJsr305Annotation = this.a.getUserDefinedLevelForSpecificJsr305Annotation();
        d0.e0.p.d.m0.g.b fqName = cVar.getFqName();
        h hVar = userDefinedLevelForSpecificJsr305Annotation.get(fqName == null ? null : fqName.asString());
        if (hVar != null) {
            return hVar;
        }
        d0.e0.p.d.m0.c.e annotationClass = d0.e0.p.d.m0.k.x.a.getAnnotationClass(cVar);
        if (annotationClass == null) {
            return null;
        }
        d0.e0.p.d.m0.c.g1.c findAnnotation = annotationClass.getAnnotations().findAnnotation(d0.e0.p.d.m0.e.a.b.getMIGRATION_ANNOTATION_FQNAME());
        g<?> firstArgument = findAnnotation == null ? null : d0.e0.p.d.m0.k.x.a.firstArgument(findAnnotation);
        d0.e0.p.d.m0.k.v.j jVar = firstArgument instanceof d0.e0.p.d.m0.k.v.j ? (d0.e0.p.d.m0.k.v.j) firstArgument : null;
        if (jVar == null) {
            return null;
        }
        h migrationLevelForJsr305 = this.a.getMigrationLevelForJsr305();
        if (migrationLevelForJsr305 != null) {
            return migrationLevelForJsr305;
        }
        String asString = jVar.getEnumEntryName().asString();
        int hashCode = asString.hashCode();
        if (hashCode != -2137067054) {
            if (hashCode != -1838656823) {
                if (hashCode == 2656902 && asString.equals("WARN")) {
                    return h.WARN;
                }
                return null;
            } else if (!asString.equals("STRICT")) {
                return null;
            } else {
                return h.STRICT;
            }
        } else if (!asString.equals("IGNORE")) {
            return null;
        } else {
            return h.IGNORE;
        }
    }

    public final u resolveQualifierBuiltInDefaultAnnotation(d0.e0.p.d.m0.c.g1.c cVar) {
        u uVar;
        h hVar;
        m.checkNotNullParameter(cVar, "annotationDescriptor");
        if (this.a.getDisabledDefaultAnnotations() || (uVar = d0.e0.p.d.m0.e.a.b.getBUILT_IN_TYPE_QUALIFIER_DEFAULT_ANNOTATIONS().get(cVar.getFqName())) == null) {
            return null;
        }
        if (d0.e0.p.d.m0.e.a.b.getJSPECIFY_DEFAULT_ANNOTATIONS().containsKey(cVar.getFqName())) {
            hVar = this.a.getJspecifyReportLevel();
        } else {
            hVar = resolveJsr305AnnotationState(cVar);
        }
        if (!(hVar != h.IGNORE)) {
            hVar = null;
        }
        if (hVar == null) {
            return null;
        }
        return u.copy$default(uVar, d0.e0.p.d.m0.e.a.l0.i.copy$default(uVar.getNullabilityQualifier(), null, hVar.isWarning(), 1, null), null, false, 6, null);
    }

    public final d0.e0.p.d.m0.c.g1.c resolveTypeQualifierAnnotation(d0.e0.p.d.m0.c.g1.c cVar) {
        d0.e0.p.d.m0.c.e annotationClass;
        m.checkNotNullParameter(cVar, "annotationDescriptor");
        if (this.a.getDisabledJsr305() || (annotationClass = d0.e0.p.d.m0.k.x.a.getAnnotationClass(cVar)) == null) {
            return null;
        }
        if (f.access$isAnnotatedWithTypeQualifier(annotationClass)) {
            return cVar;
        }
        if (annotationClass.getKind() != f.ANNOTATION_CLASS) {
            return null;
        }
        return this.f3281b.invoke(annotationClass);
    }

    public final a resolveTypeQualifierDefaultAnnotation(d0.e0.p.d.m0.c.g1.c cVar) {
        d0.e0.p.d.m0.c.g1.c cVar2;
        boolean z2;
        List<d0.e0.p.d.m0.e.a.a> list;
        m.checkNotNullParameter(cVar, "annotationDescriptor");
        if (this.a.getDisabledJsr305()) {
            return null;
        }
        d0.e0.p.d.m0.c.e annotationClass = d0.e0.p.d.m0.k.x.a.getAnnotationClass(cVar);
        if (annotationClass == null || !annotationClass.getAnnotations().hasAnnotation(d0.e0.p.d.m0.e.a.b.getTYPE_QUALIFIER_DEFAULT_FQNAME())) {
            annotationClass = null;
        }
        if (annotationClass == null) {
            return null;
        }
        d0.e0.p.d.m0.c.e annotationClass2 = d0.e0.p.d.m0.k.x.a.getAnnotationClass(cVar);
        m.checkNotNull(annotationClass2);
        d0.e0.p.d.m0.c.g1.c findAnnotation = annotationClass2.getAnnotations().findAnnotation(d0.e0.p.d.m0.e.a.b.getTYPE_QUALIFIER_DEFAULT_FQNAME());
        m.checkNotNull(findAnnotation);
        Map<d0.e0.p.d.m0.g.e, g<?>> allValueArguments = findAnnotation.getAllValueArguments();
        ArrayList<d0.e0.p.d.m0.e.a.a> arrayList = new ArrayList();
        for (Map.Entry<d0.e0.p.d.m0.g.e, g<?>> entry : allValueArguments.entrySet()) {
            g<?> value = entry.getValue();
            if (m.areEqual(entry.getKey(), a0.f3277b)) {
                list = a(value, d.j);
            } else {
                list = d0.t.n.emptyList();
            }
            r.addAll(arrayList, list);
        }
        int i = 0;
        for (d0.e0.p.d.m0.e.a.a aVar : arrayList) {
            i |= 1 << aVar.ordinal();
        }
        Iterator<d0.e0.p.d.m0.c.g1.c> it = annotationClass.getAnnotations().iterator();
        while (true) {
            if (!it.hasNext()) {
                cVar2 = null;
                break;
            }
            cVar2 = it.next();
            if (resolveTypeQualifierAnnotation(cVar2) != null) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        d0.e0.p.d.m0.c.g1.c cVar3 = cVar2;
        if (cVar3 == null) {
            return null;
        }
        return new a(cVar3, i);
    }
}
