package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.c.f1;
import d0.e0.p.d.m0.c.u;
import d0.z.d.m;
/* compiled from: utils.kt */
/* loaded from: classes3.dex */
public final class f0 {
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00b8, code lost:
        if (d0.e0.p.d.m0.b.h.isString(r4) != false) goto L40;
     */
    /* JADX WARN: Removed duplicated region for block: B:41:0x00be  */
    /* JADX WARN: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:39:0x00bb -> B:40:0x00bc). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final d0.e0.p.d.m0.e.a.v lexicalCastFrom(d0.e0.p.d.m0.n.c0 r4, java.lang.String r5) {
        /*
            java.lang.String r0 = "<this>"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            java.lang.String r0 = "value"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            d0.e0.p.d.m0.n.u0 r0 = r4.getConstructor()
            d0.e0.p.d.m0.c.h r0 = r0.getDeclarationDescriptor()
            boolean r1 = r0 instanceof d0.e0.p.d.m0.c.e
            r2 = 0
            if (r1 == 0) goto L48
            d0.e0.p.d.m0.c.e r0 = (d0.e0.p.d.m0.c.e) r0
            d0.e0.p.d.m0.c.f r1 = r0.getKind()
            d0.e0.p.d.m0.c.f r3 = d0.e0.p.d.m0.c.f.ENUM_CLASS
            if (r1 != r3) goto L48
            d0.e0.p.d.m0.k.a0.i r4 = r0.getUnsubstitutedInnerClassesScope()
            d0.e0.p.d.m0.g.e r5 = d0.e0.p.d.m0.g.e.identifier(r5)
            java.lang.String r0 = "identifier(value)"
            d0.z.d.m.checkNotNullExpressionValue(r5, r0)
            d0.e0.p.d.m0.d.b.d r0 = d0.e0.p.d.m0.d.b.d.FROM_BACKEND
            d0.e0.p.d.m0.c.h r4 = r4.getContributedClassifier(r5, r0)
            boolean r5 = r4 instanceof d0.e0.p.d.m0.c.e
            if (r5 == 0) goto L47
            d0.e0.p.d.m0.c.e r4 = (d0.e0.p.d.m0.c.e) r4
            d0.e0.p.d.m0.c.f r5 = r4.getKind()
            d0.e0.p.d.m0.c.f r0 = d0.e0.p.d.m0.c.f.ENUM_ENTRY
            if (r5 != r0) goto L47
            d0.e0.p.d.m0.e.a.o r2 = new d0.e0.p.d.m0.e.a.o
            r2.<init>(r4)
        L47:
            return r2
        L48:
            d0.e0.p.d.m0.n.c0 r4 = d0.e0.p.d.m0.n.o1.a.makeNotNullable(r4)
            d0.e0.p.d.m0.p.f r0 = d0.e0.p.d.m0.p.g.extractRadix(r5)
            java.lang.String r1 = r0.component1()
            int r0 = r0.component2()
            boolean r3 = d0.e0.p.d.m0.b.h.isBoolean(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r3 == 0) goto L67
            boolean r4 = java.lang.Boolean.parseBoolean(r5)     // Catch: java.lang.IllegalArgumentException -> Lbb
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            goto Lbc
        L67:
            boolean r3 = d0.e0.p.d.m0.b.h.isChar(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r3 == 0) goto L72
            java.lang.Character r5 = d0.g0.y.singleOrNull(r5)     // Catch: java.lang.IllegalArgumentException -> Lbb
            goto Lbc
        L72:
            boolean r3 = d0.e0.p.d.m0.b.h.isByte(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r3 == 0) goto L7d
            java.lang.Byte r5 = d0.g0.s.toByteOrNull(r1, r0)     // Catch: java.lang.IllegalArgumentException -> Lbb
            goto Lbc
        L7d:
            boolean r3 = d0.e0.p.d.m0.b.h.isShort(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r3 == 0) goto L88
            java.lang.Short r5 = d0.g0.s.toShortOrNull(r1, r0)     // Catch: java.lang.IllegalArgumentException -> Lbb
            goto Lbc
        L88:
            boolean r3 = d0.e0.p.d.m0.b.h.isInt(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r3 == 0) goto L93
            java.lang.Integer r5 = d0.g0.s.toIntOrNull(r1, r0)     // Catch: java.lang.IllegalArgumentException -> Lbb
            goto Lbc
        L93:
            boolean r3 = d0.e0.p.d.m0.b.h.isLong(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r3 == 0) goto L9e
            java.lang.Long r5 = d0.g0.s.toLongOrNull(r1, r0)     // Catch: java.lang.IllegalArgumentException -> Lbb
            goto Lbc
        L9e:
            boolean r0 = d0.e0.p.d.m0.b.h.isFloat(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r0 == 0) goto La9
            java.lang.Float r5 = d0.g0.r.toFloatOrNull(r5)     // Catch: java.lang.IllegalArgumentException -> Lbb
            goto Lbc
        La9:
            boolean r0 = d0.e0.p.d.m0.b.h.isDouble(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r0 == 0) goto Lb4
            java.lang.Double r5 = d0.g0.r.toDoubleOrNull(r5)     // Catch: java.lang.IllegalArgumentException -> Lbb
            goto Lbc
        Lb4:
            boolean r4 = d0.e0.p.d.m0.b.h.isString(r4)     // Catch: java.lang.IllegalArgumentException -> Lbb
            if (r4 == 0) goto Lbb
            goto Lbc
        Lbb:
            r5 = r2
        Lbc:
            if (r5 == 0) goto Lc3
            d0.e0.p.d.m0.e.a.l r2 = new d0.e0.p.d.m0.e.a.l
            r2.<init>(r5)
        Lc3:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.f0.lexicalCastFrom(d0.e0.p.d.m0.n.c0, java.lang.String):d0.e0.p.d.m0.e.a.v");
    }

    public static final u toDescriptorVisibility(f1 f1Var) {
        m.checkNotNullParameter(f1Var, "<this>");
        u descriptorVisibility = w.toDescriptorVisibility(f1Var);
        m.checkNotNullExpressionValue(descriptorVisibility, "toDescriptorVisibility(this)");
        return descriptorVisibility;
    }
}
