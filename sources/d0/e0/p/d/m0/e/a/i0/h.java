package d0.e0.p.d.m0.e.a.i0;

import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.i0.l.s;
import d0.e0.p.d.m0.e.a.k0.y;
import d0.e0.p.d.m0.e.a.k0.z;
import d0.e0.p.d.m0.m.i;
import d0.z.d.o;
import java.util.Map;
import kotlin.jvm.functions.Function1;
/* compiled from: resolvers.kt */
/* loaded from: classes3.dex */
public final class h implements k {
    public final g a;

    /* renamed from: b  reason: collision with root package name */
    public final m f3296b;
    public final int c;
    public final Map<y, Integer> d;
    public final i<y, s> e;

    /* compiled from: resolvers.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<y, s> {
        public a() {
            super(1);
        }

        public final s invoke(y yVar) {
            d0.z.d.m.checkNotNullParameter(yVar, "typeParameter");
            Integer num = (Integer) h.this.d.get(yVar);
            if (num == null) {
                return null;
            }
            h hVar = h.this;
            return new s(d0.e0.p.d.m0.e.a.i0.a.copyWithNewDefaultTypeQualifiers(d0.e0.p.d.m0.e.a.i0.a.child(hVar.a, hVar), hVar.f3296b.getAnnotations()), yVar, hVar.c + num.intValue(), hVar.f3296b);
        }
    }

    public h(g gVar, m mVar, z zVar, int i) {
        d0.z.d.m.checkNotNullParameter(gVar, "c");
        d0.z.d.m.checkNotNullParameter(mVar, "containingDeclaration");
        d0.z.d.m.checkNotNullParameter(zVar, "typeParameterOwner");
        this.a = gVar;
        this.f3296b = mVar;
        this.c = i;
        this.d = d0.e0.p.d.m0.p.a.mapToIndex(zVar.getTypeParameters());
        this.e = gVar.getStorageManager().createMemoizedFunctionWithNullableValues(new a());
    }

    @Override // d0.e0.p.d.m0.e.a.i0.k
    public z0 resolveTypeParameter(y yVar) {
        d0.z.d.m.checkNotNullParameter(yVar, "javaTypeParameter");
        s invoke = this.e.invoke(yVar);
        return invoke == null ? this.a.getTypeParameterResolver().resolveTypeParameter(yVar) : invoke;
    }
}
