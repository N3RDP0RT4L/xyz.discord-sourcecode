package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.f1;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.q;
import d0.e0.p.d.m0.c.r;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.k.a0.p.d;
import d0.e0.p.d.m0.k.e;
import java.util.HashMap;
import java.util.Map;
/* compiled from: JavaDescriptorVisibilities.java */
/* loaded from: classes3.dex */
public class w {
    public static final u a;

    /* renamed from: b  reason: collision with root package name */
    public static final u f3331b;
    public static final u c;
    public static final Map<f1, u> d = new HashMap();

    /* compiled from: JavaDescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class a extends r {
        public a(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/java/JavaDescriptorVisibilities$1";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar != null) {
                return w.c(qVar, mVar);
            } else {
                a(1);
                throw null;
            }
        }
    }

    /* compiled from: JavaDescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class b extends r {
        public b(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/java/JavaDescriptorVisibilities$2";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar != null) {
                return w.b(dVar, qVar, mVar);
            } else {
                a(1);
                throw null;
            }
        }
    }

    /* compiled from: JavaDescriptorVisibilities.java */
    /* loaded from: classes3.dex */
    public static class c extends r {
        public c(f1 f1Var) {
            super(f1Var);
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i != 1) {
                objArr[0] = "what";
            } else {
                objArr[0] = "from";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/java/JavaDescriptorVisibilities$3";
            objArr[2] = "isVisible";
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.c.u
        public boolean isVisible(d dVar, q qVar, m mVar) {
            if (qVar == null) {
                a(0);
                throw null;
            } else if (mVar != null) {
                return w.b(dVar, qVar, mVar);
            } else {
                a(1);
                throw null;
            }
        }
    }

    static {
        a aVar = new a(d0.e0.p.d.m0.c.j1.a.c);
        a = aVar;
        b bVar = new b(d0.e0.p.d.m0.c.j1.c.c);
        f3331b = bVar;
        c cVar = new c(d0.e0.p.d.m0.c.j1.b.c);
        c = cVar;
        d(aVar);
        d(bVar);
        d(cVar);
    }

    public static /* synthetic */ void a(int i) {
        String str = (i == 5 || i == 6) ? "@NotNull method %s.%s must not return null" : "Argument for @NotNull parameter '%s' of %s.%s must not be null";
        Object[] objArr = new Object[(i == 5 || i == 6) ? 2 : 3];
        switch (i) {
            case 1:
                objArr[0] = "from";
                break;
            case 2:
                objArr[0] = "first";
                break;
            case 3:
                objArr[0] = "second";
                break;
            case 4:
                objArr[0] = "visibility";
                break;
            case 5:
            case 6:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/load/java/JavaDescriptorVisibilities";
                break;
            default:
                objArr[0] = "what";
                break;
        }
        if (i == 5 || i == 6) {
            objArr[1] = "toDescriptorVisibility";
        } else {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/java/JavaDescriptorVisibilities";
        }
        if (i == 2 || i == 3) {
            objArr[2] = "areInSamePackage";
        } else if (i == 4) {
            objArr[2] = "toDescriptorVisibility";
        } else if (!(i == 5 || i == 6)) {
            objArr[2] = "isVisibleForProtectedAndPackage";
        }
        String format = String.format(str, objArr);
        if (i == 5 || i == 6) {
            throw new IllegalStateException(format);
        }
    }

    public static boolean b(d dVar, q qVar, m mVar) {
        if (qVar == null) {
            a(0);
            throw null;
        } else if (mVar == null) {
            a(1);
            throw null;
        } else if (c(e.unwrapFakeOverrideToAnyDeclaration(qVar), mVar)) {
            return true;
        } else {
            return t.c.isVisible(dVar, qVar, mVar);
        }
    }

    public static boolean c(m mVar, m mVar2) {
        if (mVar == null) {
            a(2);
            throw null;
        } else if (mVar2 != null) {
            e0 e0Var = (e0) e.getParentOfType(mVar, e0.class, false);
            e0 e0Var2 = (e0) e.getParentOfType(mVar2, e0.class, false);
            return (e0Var2 == null || e0Var == null || !e0Var.getFqName().equals(e0Var2.getFqName())) ? false : true;
        } else {
            a(3);
            throw null;
        }
    }

    public static void d(u uVar) {
        d.put(uVar.getDelegate(), uVar);
    }

    public static u toDescriptorVisibility(f1 f1Var) {
        if (f1Var != null) {
            u uVar = d.get(f1Var);
            if (uVar != null) {
                return uVar;
            }
            u descriptorVisibility = t.toDescriptorVisibility(f1Var);
            if (descriptorVisibility != null) {
                return descriptorVisibility;
            }
            a(5);
            throw null;
        }
        a(4);
        throw null;
    }
}
