package d0.e0.p.d.m0.e.a.k0;

import java.util.List;
/* compiled from: javaTypes.kt */
/* loaded from: classes3.dex */
public interface j extends x {
    i getClassifier();

    String getClassifierQualifiedName();

    String getPresentableText();

    List<x> getTypeArguments();

    boolean isRaw();
}
