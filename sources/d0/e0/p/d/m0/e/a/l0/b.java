package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.g1.g;
import d0.t.n;
import d0.z.d.m;
import java.util.Iterator;
/* compiled from: typeEnhancement.kt */
/* loaded from: classes3.dex */
public final class b implements g {
    public final d0.e0.p.d.m0.g.b j;

    public b(d0.e0.p.d.m0.g.b bVar) {
        m.checkNotNullParameter(bVar, "fqNameToMatch");
        this.j = bVar;
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public boolean hasAnnotation(d0.e0.p.d.m0.g.b bVar) {
        return g.b.hasAnnotation(this, bVar);
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public boolean isEmpty() {
        return false;
    }

    @Override // java.lang.Iterable
    public Iterator<c> iterator() {
        return n.emptyList().iterator();
    }

    @Override // d0.e0.p.d.m0.c.g1.g
    public a findAnnotation(d0.e0.p.d.m0.g.b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        if (m.areEqual(bVar, this.j)) {
            return a.a;
        }
        return null;
    }
}
