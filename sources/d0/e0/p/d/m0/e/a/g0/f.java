package d0.e0.p.d.m0.e.a.g0;

import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.e.a.k0.n;
import d0.e0.p.d.m0.k.v.g;
import d0.z.d.m;
/* compiled from: JavaPropertyInitializerEvaluator.kt */
/* loaded from: classes3.dex */
public interface f {

    /* compiled from: JavaPropertyInitializerEvaluator.kt */
    /* loaded from: classes3.dex */
    public static final class a implements f {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.e.a.g0.f
        public g<?> getInitializerConstant(n nVar, n0 n0Var) {
            m.checkNotNullParameter(nVar, "field");
            m.checkNotNullParameter(n0Var, "descriptor");
            return null;
        }
    }

    g<?> getInitializerConstant(n nVar, n0 n0Var);
}
