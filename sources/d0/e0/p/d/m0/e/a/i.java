package d0.e0.p.d.m0.e.a;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.c;
import d0.e0.p.d.m0.g.e;
import d0.o;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Pair;
/* compiled from: BuiltinSpecialProperties.kt */
/* loaded from: classes3.dex */
public final class i {
    public static final i a = new i();

    /* renamed from: b  reason: collision with root package name */
    public static final Map<b, e> f3291b;
    public static final Map<e, List<e>> c;
    public static final Set<b> d;
    public static final Set<e> e;

    static {
        c cVar = k.a.r;
        b bVar = k.a.N;
        Map<b, e> mapOf = h0.mapOf(o.to(j.access$childSafe(cVar, ModelAuditLogEntry.CHANGE_KEY_NAME), e.identifier(ModelAuditLogEntry.CHANGE_KEY_NAME)), o.to(j.access$childSafe(cVar, "ordinal"), e.identifier("ordinal")), o.to(j.access$child(k.a.J, "size"), e.identifier("size")), o.to(j.access$child(bVar, "size"), e.identifier("size")), o.to(j.access$childSafe(k.a.f, "length"), e.identifier("length")), o.to(j.access$child(bVar, "keys"), e.identifier("keySet")), o.to(j.access$child(bVar, "values"), e.identifier("values")), o.to(j.access$child(bVar, "entries"), e.identifier("entrySet")));
        f3291b = mapOf;
        Set<Map.Entry<b, e>> entrySet = mapOf.entrySet();
        ArrayList<Pair> arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(entrySet, 10));
        Iterator<T> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            arrayList.add(new Pair(((b) entry.getKey()).shortName(), entry.getValue()));
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Pair pair : arrayList) {
            e eVar = (e) pair.getSecond();
            Object obj = linkedHashMap.get(eVar);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(eVar, obj);
            }
            ((List) obj).add((e) pair.getFirst());
        }
        c = linkedHashMap;
        Set<b> keySet = f3291b.keySet();
        d = keySet;
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(keySet, 10));
        for (b bVar2 : keySet) {
            arrayList2.add(bVar2.shortName());
        }
        e = u.toSet(arrayList2);
    }

    public final Map<b, e> getPROPERTY_FQ_NAME_TO_JVM_GETTER_NAME_MAP() {
        return f3291b;
    }

    public final List<e> getPropertyNameCandidatesBySpecialGetterName(e eVar) {
        m.checkNotNullParameter(eVar, "name1");
        List<e> list = c.get(eVar);
        return list == null ? n.emptyList() : list;
    }

    public final Set<b> getSPECIAL_FQ_NAMES() {
        return d;
    }

    public final Set<e> getSPECIAL_SHORT_NAMES() {
        return e;
    }
}
