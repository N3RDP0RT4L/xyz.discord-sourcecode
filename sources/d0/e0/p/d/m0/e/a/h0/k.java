package d0.e0.p.d.m0.e.a.h0;

import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.g1.c;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.i1.l0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.e.a.a0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.v.w;
import d0.e0.p.d.m0.n.c0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Pair;
/* compiled from: util.kt */
/* loaded from: classes3.dex */
public final class k {
    public static final List<c1> copyValueParameters(Collection<l> collection, Collection<? extends c1> collection2, a aVar) {
        m.checkNotNullParameter(collection, "newValueParametersTypes");
        m.checkNotNullParameter(collection2, "oldValueParameters");
        m.checkNotNullParameter(aVar, "newOwner");
        collection.size();
        collection2.size();
        List<Pair> zip = u.zip(collection, collection2);
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(zip, 10));
        for (Pair pair : zip) {
            l lVar = (l) pair.component1();
            c1 c1Var = (c1) pair.component2();
            int index = c1Var.getIndex();
            g annotations = c1Var.getAnnotations();
            e name = c1Var.getName();
            m.checkNotNullExpressionValue(name, "oldParameter.name");
            c0 type = lVar.getType();
            boolean hasDefaultValue = lVar.getHasDefaultValue();
            boolean isCrossinline = c1Var.isCrossinline();
            boolean isNoinline = c1Var.isNoinline();
            c0 arrayElementType = c1Var.getVarargElementType() != null ? d0.e0.p.d.m0.k.x.a.getModule(aVar).getBuiltIns().getArrayElementType(lVar.getType()) : null;
            u0 source = c1Var.getSource();
            m.checkNotNullExpressionValue(source, "oldParameter.source");
            arrayList.add(new l0(aVar, null, index, annotations, name, type, hasDefaultValue, isCrossinline, isNoinline, arrayElementType, source));
        }
        return arrayList;
    }

    public static final a getDefaultValueFromAnnotation(c1 c1Var) {
        w wVar;
        String str;
        d0.e0.p.d.m0.k.v.g<?> firstArgument;
        m.checkNotNullParameter(c1Var, "<this>");
        g annotations = c1Var.getAnnotations();
        b bVar = a0.r;
        m.checkNotNullExpressionValue(bVar, "DEFAULT_VALUE_FQ_NAME");
        c findAnnotation = annotations.findAnnotation(bVar);
        if (findAnnotation == null || (firstArgument = d0.e0.p.d.m0.k.x.a.firstArgument(findAnnotation)) == null) {
            wVar = null;
        } else {
            if (!(firstArgument instanceof w)) {
                firstArgument = null;
            }
            wVar = (w) firstArgument;
        }
        if (wVar != null && (str = (String) wVar.getValue()) != null) {
            return new j(str);
        }
        g annotations2 = c1Var.getAnnotations();
        b bVar2 = a0.f3278s;
        m.checkNotNullExpressionValue(bVar2, "DEFAULT_NULL_FQ_NAME");
        if (annotations2.hasAnnotation(bVar2)) {
            return h.a;
        }
        return null;
    }

    public static final d0.e0.p.d.m0.e.a.i0.l.o getParentJavaStaticClassScope(d0.e0.p.d.m0.c.e eVar) {
        m.checkNotNullParameter(eVar, "<this>");
        d0.e0.p.d.m0.c.e superClassNotAny = d0.e0.p.d.m0.k.x.a.getSuperClassNotAny(eVar);
        d0.e0.p.d.m0.e.a.i0.l.o oVar = null;
        if (superClassNotAny == null) {
            return null;
        }
        i staticScope = superClassNotAny.getStaticScope();
        if (staticScope instanceof d0.e0.p.d.m0.e.a.i0.l.o) {
            oVar = (d0.e0.p.d.m0.e.a.i0.l.o) staticScope;
        }
        return oVar == null ? getParentJavaStaticClassScope(superClassNotAny) : oVar;
    }
}
