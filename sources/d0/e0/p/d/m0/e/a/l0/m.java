package d0.e0.p.d.m0.e.a.l0;

import d0.t.k;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: signatureEnhancement.kt */
/* loaded from: classes3.dex */
public final class m extends o implements Function1<Integer, e> {
    public final /* synthetic */ e[] $computedResult;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public m(e[] eVarArr) {
        super(1);
        this.$computedResult = eVarArr;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ e invoke(Integer num) {
        return invoke(num.intValue());
    }

    public final e invoke(int i) {
        e[] eVarArr = this.$computedResult;
        return (i < 0 || i > k.getLastIndex(eVarArr)) ? e.a.getNONE() : eVarArr[i];
    }
}
