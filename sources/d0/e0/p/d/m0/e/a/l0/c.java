package d0.e0.p.d.m0.e.a.l0;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.g1.g;
import d0.z.d.m;
/* compiled from: typeEnhancement.kt */
/* loaded from: classes3.dex */
public final class c<T> {
    public final T a;

    /* renamed from: b  reason: collision with root package name */
    public final g f3314b;

    public c(T t, g gVar) {
        this.a = t;
        this.f3314b = gVar;
    }

    public final T component1() {
        return this.a;
    }

    public final g component2() {
        return this.f3314b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        return m.areEqual(this.a, cVar.a) && m.areEqual(this.f3314b, cVar.f3314b);
    }

    public int hashCode() {
        T t = this.a;
        int i = 0;
        int hashCode = (t == null ? 0 : t.hashCode()) * 31;
        g gVar = this.f3314b;
        if (gVar != null) {
            i = gVar.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("EnhancementResult(result=");
        R.append(this.a);
        R.append(", enhancementAnnotations=");
        R.append(this.f3314b);
        R.append(')');
        return R.toString();
    }
}
