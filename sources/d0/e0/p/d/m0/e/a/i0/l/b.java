package d0.e0.p.d.m0.e.a.i0.l;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.e.a.k0.n;
import d0.e0.p.d.m0.e.a.k0.r;
import d0.e0.p.d.m0.e.a.k0.w;
import d0.e0.p.d.m0.g.e;
import d0.t.n0;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Set;
/* compiled from: DeclaredMemberIndex.kt */
/* loaded from: classes3.dex */
public interface b {

    /* compiled from: DeclaredMemberIndex.kt */
    /* loaded from: classes3.dex */
    public static final class a implements b {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.e.a.i0.l.b
        public n findFieldByName(e eVar) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            return null;
        }

        @Override // d0.e0.p.d.m0.e.a.i0.l.b
        public w findRecordComponentByName(e eVar) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            return null;
        }

        @Override // d0.e0.p.d.m0.e.a.i0.l.b
        public Set<e> getFieldNames() {
            return n0.emptySet();
        }

        @Override // d0.e0.p.d.m0.e.a.i0.l.b
        public Set<e> getMethodNames() {
            return n0.emptySet();
        }

        @Override // d0.e0.p.d.m0.e.a.i0.l.b
        public Set<e> getRecordComponentNames() {
            return n0.emptySet();
        }

        @Override // d0.e0.p.d.m0.e.a.i0.l.b
        public List<r> findMethodsByName(e eVar) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            return d0.t.n.emptyList();
        }
    }

    n findFieldByName(e eVar);

    Collection<r> findMethodsByName(e eVar);

    w findRecordComponentByName(e eVar);

    Set<e> getFieldNames();

    Set<e> getMethodNames();

    Set<e> getRecordComponentNames();
}
