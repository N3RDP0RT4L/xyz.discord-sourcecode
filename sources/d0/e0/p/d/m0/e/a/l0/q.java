package d0.e0.p.d.m0.e.a.l0;

import androidx.exifinterface.media.ExifInterface;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.d0.f;
import d0.e0.p.d.m0.e.b.w;
import d0.e0.p.d.m0.k.y.d;
import d0.o;
import d0.t.g0;
import d0.t.k;
import d0.t.z;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: predefinedEnhancementInfo.kt */
/* loaded from: classes3.dex */
public final class q {
    public final Map<String, k> a = new LinkedHashMap();

    public final Map<String, k> build() {
        return this.a;
    }

    /* compiled from: predefinedEnhancementInfo.kt */
    /* loaded from: classes3.dex */
    public final class a {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ q f3323b;

        public a(q qVar, String str) {
            m.checkNotNullParameter(qVar, "this$0");
            m.checkNotNullParameter(str, "className");
            this.f3323b = qVar;
            this.a = str;
        }

        public final void function(String str, Function1<? super C0309a, Unit> function1) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(function1, "block");
            Map map = this.f3323b.a;
            C0309a aVar = new C0309a(this, str);
            function1.invoke(aVar);
            Pair<String, k> build = aVar.build();
            map.put(build.getFirst(), build.getSecond());
        }

        public final String getClassName() {
            return this.a;
        }

        /* compiled from: predefinedEnhancementInfo.kt */
        /* renamed from: d0.e0.p.d.m0.e.a.l0.q$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public final class C0309a {
            public final String a;

            /* renamed from: b  reason: collision with root package name */
            public final List<Pair<String, w>> f3324b = new ArrayList();
            public Pair<String, w> c = o.to(ExifInterface.GPS_MEASUREMENT_INTERRUPTED, null);
            public final /* synthetic */ a d;

            public C0309a(a aVar, String str) {
                m.checkNotNullParameter(aVar, "this$0");
                m.checkNotNullParameter(str, "functionName");
                this.d = aVar;
                this.a = str;
            }

            public final Pair<String, k> build() {
                w wVar = w.a;
                String className = this.d.getClassName();
                String functionName = getFunctionName();
                List<Pair<String, w>> list = this.f3324b;
                ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
                Iterator<T> it = list.iterator();
                while (it.hasNext()) {
                    arrayList.add((String) ((Pair) it.next()).getFirst());
                }
                String signature = wVar.signature(className, wVar.jvmDescriptor(functionName, arrayList, this.c.getFirst()));
                w second = this.c.getSecond();
                List<Pair<String, w>> list2 = this.f3324b;
                ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(list2, 10));
                Iterator<T> it2 = list2.iterator();
                while (it2.hasNext()) {
                    arrayList2.add((w) ((Pair) it2.next()).getSecond());
                }
                return o.to(signature, new k(second, arrayList2));
            }

            public final String getFunctionName() {
                return this.a;
            }

            public final void parameter(String str, e... eVarArr) {
                w wVar;
                m.checkNotNullParameter(str, "type");
                m.checkNotNullParameter(eVarArr, "qualifiers");
                List<Pair<String, w>> list = this.f3324b;
                if (eVarArr.length == 0) {
                    wVar = null;
                } else {
                    Iterable<z> withIndex = k.withIndex(eVarArr);
                    LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(withIndex, 10)), 16));
                    for (z zVar : withIndex) {
                        linkedHashMap.put(Integer.valueOf(zVar.getIndex()), (e) zVar.getValue());
                    }
                    wVar = new w(linkedHashMap);
                }
                list.add(o.to(str, wVar));
            }

            public final void returns(String str, e... eVarArr) {
                m.checkNotNullParameter(str, "type");
                m.checkNotNullParameter(eVarArr, "qualifiers");
                Iterable<z> withIndex = k.withIndex(eVarArr);
                LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(withIndex, 10)), 16));
                for (z zVar : withIndex) {
                    linkedHashMap.put(Integer.valueOf(zVar.getIndex()), (e) zVar.getValue());
                }
                this.c = o.to(str, new w(linkedHashMap));
            }

            public final void returns(d dVar) {
                m.checkNotNullParameter(dVar, "type");
                String desc = dVar.getDesc();
                m.checkNotNullExpressionValue(desc, "type.desc");
                this.c = o.to(desc, null);
            }
        }
    }
}
