package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.g.b;
import d0.z.d.m;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/* compiled from: FakePureImplementationsProvider.kt */
/* loaded from: classes3.dex */
public final class q {
    public static final q a;

    /* renamed from: b  reason: collision with root package name */
    public static final HashMap<b, b> f3328b = new HashMap<>();

    static {
        q qVar = new q();
        a = qVar;
        qVar.b(k.a.S, qVar.a("java.util.ArrayList", "java.util.LinkedList"));
        qVar.b(k.a.U, qVar.a("java.util.HashSet", "java.util.TreeSet", "java.util.LinkedHashSet"));
        qVar.b(k.a.V, qVar.a("java.util.HashMap", "java.util.TreeMap", "java.util.LinkedHashMap", "java.util.concurrent.ConcurrentHashMap", "java.util.concurrent.ConcurrentSkipListMap"));
        qVar.b(new b("java.util.function.Function"), qVar.a("java.util.function.UnaryOperator"));
        qVar.b(new b("java.util.function.BiFunction"), qVar.a("java.util.function.BinaryOperator"));
    }

    public final List<b> a(String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String str : strArr) {
            arrayList.add(new b(str));
        }
        return arrayList;
    }

    public final void b(b bVar, List<b> list) {
        AbstractMap abstractMap = f3328b;
        for (Object obj : list) {
            b bVar2 = (b) obj;
            abstractMap.put(obj, bVar);
        }
    }

    public final b getPurelyImplementedInterface(b bVar) {
        m.checkNotNullParameter(bVar, "classFqName");
        return f3328b.get(bVar);
    }
}
