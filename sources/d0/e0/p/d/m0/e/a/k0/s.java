package d0.e0.p.d.m0.e.a.k0;

import d0.e0.p.d.m0.c.f1;
/* compiled from: javaElements.kt */
/* loaded from: classes3.dex */
public interface s extends l {
    f1 getVisibility();

    boolean isAbstract();

    boolean isFinal();

    boolean isStatic();
}
