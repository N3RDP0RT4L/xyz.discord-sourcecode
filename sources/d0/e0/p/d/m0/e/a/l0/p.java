package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: signatureEnhancement.kt */
/* loaded from: classes3.dex */
public final class p extends o implements Function1<b, c0> {
    public final /* synthetic */ c1 $p;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public p(c1 c1Var) {
        super(1);
        this.$p = c1Var;
    }

    public final c0 invoke(b bVar) {
        m.checkNotNullParameter(bVar, "it");
        c0 type = bVar.getValueParameters().get(this.$p.getIndex()).getType();
        m.checkNotNullExpressionValue(type, "it.valueParameters[p.index].type");
        return type;
    }
}
