package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.i0.m.g;
import d0.e0.p.d.m0.k.f;
import d0.e0.p.d.m0.k.k;
import d0.e0.p.d.m0.n.c0;
import d0.f0.q;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.sequences.Sequence;
/* compiled from: ErasedOverridabilityCondition.kt */
/* loaded from: classes3.dex */
public final class p implements f {

    /* compiled from: ErasedOverridabilityCondition.kt */
    /* loaded from: classes3.dex */
    public /* synthetic */ class a {
        public static final /* synthetic */ int[] a = {1};

        static {
            k.d.a.values();
        }
    }

    /* compiled from: ErasedOverridabilityCondition.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<c1, c0> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        public final c0 invoke(c1 c1Var) {
            return c1Var.getType();
        }
    }

    @Override // d0.e0.p.d.m0.k.f
    public f.a getContract() {
        return f.a.SUCCESS_ONLY;
    }

    @Override // d0.e0.p.d.m0.k.f
    public f.b isOverridable(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2, e eVar) {
        boolean z2;
        t0 substitute;
        boolean z3;
        f.b bVar = f.b.UNKNOWN;
        m.checkNotNullParameter(aVar, "superDescriptor");
        m.checkNotNullParameter(aVar2, "subDescriptor");
        if (!(aVar2 instanceof d0.e0.p.d.m0.e.a.h0.f)) {
            return bVar;
        }
        d0.e0.p.d.m0.e.a.h0.f fVar = (d0.e0.p.d.m0.e.a.h0.f) aVar2;
        List<z0> typeParameters = fVar.getTypeParameters();
        m.checkNotNullExpressionValue(typeParameters, "subDescriptor.typeParameters");
        if (!typeParameters.isEmpty()) {
            return bVar;
        }
        k.d basicOverridabilityProblem = k.getBasicOverridabilityProblem(aVar, aVar2);
        c0 c0Var = null;
        if ((basicOverridabilityProblem == null ? null : basicOverridabilityProblem.getResult()) != null) {
            return bVar;
        }
        List<c1> valueParameters = fVar.getValueParameters();
        m.checkNotNullExpressionValue(valueParameters, "subDescriptor.valueParameters");
        Sequence map = q.map(u.asSequence(valueParameters), b.j);
        c0 returnType = fVar.getReturnType();
        m.checkNotNull(returnType);
        Sequence plus = q.plus(map, returnType);
        q0 extensionReceiverParameter = fVar.getExtensionReceiverParameter();
        if (extensionReceiverParameter != null) {
            c0Var = extensionReceiverParameter.getType();
        }
        Iterator it = q.plus(plus, (Iterable) n.listOfNotNull(c0Var)).iterator();
        while (true) {
            if (!it.hasNext()) {
                z2 = false;
                break;
            }
            c0 c0Var2 = (c0) it.next();
            if (!(!c0Var2.getArguments().isEmpty()) || (c0Var2.unwrap() instanceof g)) {
                z3 = false;
                continue;
            } else {
                z3 = true;
                continue;
            }
            if (z3) {
                z2 = true;
                break;
            }
        }
        if (z2 || (substitute = aVar.substitute(d0.e0.p.d.m0.e.a.i0.m.f.f3313b.buildSubstitutor())) == null) {
            return bVar;
        }
        if (substitute instanceof t0) {
            t0 t0Var = (t0) substitute;
            List<z0> typeParameters2 = t0Var.getTypeParameters();
            m.checkNotNullExpressionValue(typeParameters2, "erasedSuper.typeParameters");
            if (!typeParameters2.isEmpty()) {
                substitute = t0Var.newCopyBuilder().setTypeParameters(n.emptyList()).build();
                m.checkNotNull(substitute);
            }
        }
        k.d.a result = k.f3440b.isOverridableByWithoutExternalConditions(substitute, aVar2, false).getResult();
        m.checkNotNullExpressionValue(result, "DEFAULT.isOverridableByWithoutExternalConditions(erasedSuper, subDescriptor, false).result");
        return a.a[result.ordinal()] == 1 ? f.b.OVERRIDABLE : bVar;
    }
}
