package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.v;
/* compiled from: signatureEnhancement.kt */
/* loaded from: classes3.dex */
public final class r {
    public static final boolean access$isNullabilityFlexible(c0 c0Var) {
        i1 unwrap = c0Var.unwrap();
        v vVar = unwrap instanceof v ? (v) unwrap : null;
        return (vVar == null || vVar.getLowerBound().isMarkedNullable() == vVar.getUpperBound().isMarkedNullable()) ? false : true;
    }
}
