package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: signatureEnhancement.kt */
/* loaded from: classes3.dex */
public final class n extends o implements Function1<b, c0> {
    public static final n j = new n();

    public n() {
        super(1);
    }

    public final c0 invoke(b bVar) {
        m.checkNotNullParameter(bVar, "it");
        q0 extensionReceiverParameter = bVar.getExtensionReceiverParameter();
        m.checkNotNull(extensionReceiverParameter);
        c0 type = extensionReceiverParameter.getType();
        m.checkNotNullExpressionValue(type, "it.extensionReceiverParameter!!.type");
        return type;
    }
}
