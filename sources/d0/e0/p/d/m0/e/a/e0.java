package d0.e0.p.d.m0.e.a;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.e.b.w;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.y.d;
import d0.t.g0;
import d0.t.h0;
import d0.t.n0;
import d0.t.o;
import d0.t.o0;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: SpecialGenericSignatures.kt */
/* loaded from: classes3.dex */
public class e0 {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public static final List<a.C0298a> f3283b;
    public static final List<String> c;
    public static final Map<a.C0298a, c> d;
    public static final Map<String, c> e;
    public static final Set<e> f;
    public static final Set<String> g;
    public static final a.C0298a h;
    public static final Map<a.C0298a, e> i;
    public static final Map<String, e> j;
    public static final List<e> k;
    public static final Map<e, List<e>> l;

    /* compiled from: SpecialGenericSignatures.kt */
    /* loaded from: classes3.dex */
    public static final class a {

        /* compiled from: SpecialGenericSignatures.kt */
        /* renamed from: d0.e0.p.d.m0.e.a.e0$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0298a {
            public final e a;

            /* renamed from: b  reason: collision with root package name */
            public final String f3284b;

            public C0298a(e eVar, String str) {
                m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullParameter(str, "signature");
                this.a = eVar;
                this.f3284b = str;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof C0298a)) {
                    return false;
                }
                C0298a aVar = (C0298a) obj;
                return m.areEqual(this.a, aVar.a) && m.areEqual(this.f3284b, aVar.f3284b);
            }

            public final e getName() {
                return this.a;
            }

            public final String getSignature() {
                return this.f3284b;
            }

            public int hashCode() {
                return this.f3284b.hashCode() + (this.a.hashCode() * 31);
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("NameAndSignature(name=");
                R.append(this.a);
                R.append(", signature=");
                return b.d.b.a.a.G(R, this.f3284b, ')');
            }
        }

        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final C0298a access$method(a aVar, String str, String str2, String str3, String str4) {
            Objects.requireNonNull(aVar);
            e identifier = e.identifier(str2);
            m.checkNotNullExpressionValue(identifier, "identifier(name)");
            w wVar = w.a;
            return new C0298a(identifier, wVar.signature(str, str2 + '(' + str3 + ')' + str4));
        }

        public final List<String> getERASED_COLLECTION_PARAMETER_SIGNATURES() {
            return e0.c;
        }

        public final Set<e> getERASED_VALUE_PARAMETERS_SHORT_NAMES() {
            return e0.f;
        }

        public final Set<String> getERASED_VALUE_PARAMETERS_SIGNATURES() {
            return e0.g;
        }

        public final Map<e, List<e>> getJVM_SHORT_NAME_TO_BUILTIN_SHORT_NAMES_MAP() {
            return e0.l;
        }

        public final List<e> getORIGINAL_SHORT_NAMES() {
            return e0.k;
        }

        public final C0298a getREMOVE_AT_NAME_AND_SIGNATURE() {
            return e0.h;
        }

        public final Map<String, c> getSIGNATURE_TO_DEFAULT_VALUES_MAP() {
            return e0.e;
        }

        public final Map<String, e> getSIGNATURE_TO_JVM_REPRESENTATION_NAME() {
            return e0.j;
        }

        public final b getSpecialSignatureInfo(String str) {
            m.checkNotNullParameter(str, "builtinSignature");
            if (getERASED_COLLECTION_PARAMETER_SIGNATURES().contains(str)) {
                return b.ONE_COLLECTION_PARAMETER;
            }
            if (((c) h0.getValue(getSIGNATURE_TO_DEFAULT_VALUES_MAP(), str)) == c.j) {
                return b.OBJECT_PARAMETER_GENERIC;
            }
            return b.OBJECT_PARAMETER_NON_GENERIC;
        }
    }

    /* compiled from: SpecialGenericSignatures.kt */
    /* loaded from: classes3.dex */
    public enum b {
        ONE_COLLECTION_PARAMETER("Ljava/util/Collection<+Ljava/lang/Object;>;", false),
        OBJECT_PARAMETER_NON_GENERIC(null, true),
        OBJECT_PARAMETER_GENERIC("Ljava/lang/Object;", true);
        
        private final boolean isObjectReplacedWithTypeParameter;
        private final String valueParametersSignature;

        b(String str, boolean z2) {
            this.valueParametersSignature = str;
            this.isObjectReplacedWithTypeParameter = z2;
        }
    }

    /* JADX WARN: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: SpecialGenericSignatures.kt */
    /* loaded from: classes3.dex */
    public static final class c extends Enum<c> {
        public static final c j;
        public static final c k;
        public static final c l;
        public static final c m;
        public static final /* synthetic */ c[] n;
        private final Object defaultValue;

        /* compiled from: SpecialGenericSignatures.kt */
        /* loaded from: classes3.dex */
        public static final class a extends c {
            public a(String str, int i) {
                super(str, i, null, null);
            }
        }

        static {
            c cVar = new c("NULL", 0, null);
            j = cVar;
            c cVar2 = new c("INDEX", 1, -1);
            k = cVar2;
            c cVar3 = new c("FALSE", 2, Boolean.FALSE);
            l = cVar3;
            a aVar = new a("MAP_GET_OR_DEFAULT", 3);
            m = aVar;
            n = new c[]{cVar, cVar2, cVar3, aVar};
        }

        public c(String str, int i, Object obj) {
            this.defaultValue = obj;
        }

        public static c valueOf(String str) {
            m.checkNotNullParameter(str, "value");
            return (c) Enum.valueOf(c.class, str);
        }

        public static c[] values() {
            c[] cVarArr = n;
            c[] cVarArr2 = new c[cVarArr.length];
            System.arraycopy(cVarArr, 0, cVarArr2, 0, cVarArr.length);
            return cVarArr2;
        }

        public c(String str, int i, Object obj, DefaultConstructorMarker defaultConstructorMarker) {
            this.defaultValue = obj;
        }
    }

    static {
        Set<String> of = n0.setOf((Object[]) new String[]{"containsAll", "removeAll", "retainAll"});
        ArrayList<a.C0298a> arrayList = new ArrayList(o.collectionSizeOrDefault(of, 10));
        for (String str : of) {
            a aVar = a;
            String desc = d.BOOLEAN.getDesc();
            m.checkNotNullExpressionValue(desc, "BOOLEAN.desc");
            arrayList.add(a.access$method(aVar, "java/util/Collection", str, "Ljava/util/Collection;", desc));
        }
        f3283b = arrayList;
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        for (a.C0298a aVar2 : arrayList) {
            arrayList2.add(aVar2.getSignature());
        }
        c = arrayList2;
        List<a.C0298a> list = f3283b;
        ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (a.C0298a aVar3 : list) {
            arrayList3.add(aVar3.getName().asString());
        }
        w wVar = w.a;
        a aVar4 = a;
        String javaUtil = wVar.javaUtil("Collection");
        d dVar = d.BOOLEAN;
        String desc2 = dVar.getDesc();
        m.checkNotNullExpressionValue(desc2, "BOOLEAN.desc");
        a.C0298a access$method = a.access$method(aVar4, javaUtil, "contains", "Ljava/lang/Object;", desc2);
        c cVar = c.l;
        String javaUtil2 = wVar.javaUtil("Collection");
        String desc3 = dVar.getDesc();
        m.checkNotNullExpressionValue(desc3, "BOOLEAN.desc");
        String javaUtil3 = wVar.javaUtil("Map");
        String desc4 = dVar.getDesc();
        m.checkNotNullExpressionValue(desc4, "BOOLEAN.desc");
        String javaUtil4 = wVar.javaUtil("Map");
        String desc5 = dVar.getDesc();
        m.checkNotNullExpressionValue(desc5, "BOOLEAN.desc");
        String javaUtil5 = wVar.javaUtil("Map");
        String desc6 = dVar.getDesc();
        m.checkNotNullExpressionValue(desc6, "BOOLEAN.desc");
        a.C0298a access$method2 = a.access$method(aVar4, wVar.javaUtil("Map"), "get", "Ljava/lang/Object;", "Ljava/lang/Object;");
        c cVar2 = c.j;
        String javaUtil6 = wVar.javaUtil("List");
        d dVar2 = d.INT;
        String desc7 = dVar2.getDesc();
        m.checkNotNullExpressionValue(desc7, "INT.desc");
        a.C0298a access$method3 = a.access$method(aVar4, javaUtil6, "indexOf", "Ljava/lang/Object;", desc7);
        c cVar3 = c.k;
        String javaUtil7 = wVar.javaUtil("List");
        String desc8 = dVar2.getDesc();
        m.checkNotNullExpressionValue(desc8, "INT.desc");
        Map<a.C0298a, c> mapOf = h0.mapOf(d0.o.to(access$method, cVar), d0.o.to(a.access$method(aVar4, javaUtil2, "remove", "Ljava/lang/Object;", desc3), cVar), d0.o.to(a.access$method(aVar4, javaUtil3, "containsKey", "Ljava/lang/Object;", desc4), cVar), d0.o.to(a.access$method(aVar4, javaUtil4, "containsValue", "Ljava/lang/Object;", desc5), cVar), d0.o.to(a.access$method(aVar4, javaUtil5, "remove", "Ljava/lang/Object;Ljava/lang/Object;", desc6), cVar), d0.o.to(a.access$method(aVar4, wVar.javaUtil("Map"), "getOrDefault", "Ljava/lang/Object;Ljava/lang/Object;", "Ljava/lang/Object;"), c.m), d0.o.to(access$method2, cVar2), d0.o.to(a.access$method(aVar4, wVar.javaUtil("Map"), "remove", "Ljava/lang/Object;", "Ljava/lang/Object;"), cVar2), d0.o.to(access$method3, cVar3), d0.o.to(a.access$method(aVar4, javaUtil7, "lastIndexOf", "Ljava/lang/Object;", desc8), cVar3));
        d = mapOf;
        LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(mapOf.size()));
        Iterator<T> it = mapOf.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            linkedHashMap.put(((a.C0298a) entry.getKey()).getSignature(), entry.getValue());
        }
        e = linkedHashMap;
        Set<a.C0298a> plus = o0.plus((Set) d.keySet(), (Iterable) f3283b);
        ArrayList arrayList4 = new ArrayList(o.collectionSizeOrDefault(plus, 10));
        for (a.C0298a aVar5 : plus) {
            arrayList4.add(aVar5.getName());
        }
        f = u.toSet(arrayList4);
        ArrayList arrayList5 = new ArrayList(o.collectionSizeOrDefault(plus, 10));
        for (a.C0298a aVar6 : plus) {
            arrayList5.add(aVar6.getSignature());
        }
        g = u.toSet(arrayList5);
        a aVar7 = a;
        d dVar3 = d.INT;
        String desc9 = dVar3.getDesc();
        m.checkNotNullExpressionValue(desc9, "INT.desc");
        h = a.access$method(aVar7, "java/util/List", "removeAt", desc9, "Ljava/lang/Object;");
        w wVar2 = w.a;
        String javaLang = wVar2.javaLang("Number");
        String desc10 = d.BYTE.getDesc();
        m.checkNotNullExpressionValue(desc10, "BYTE.desc");
        String javaLang2 = wVar2.javaLang("Number");
        String desc11 = d.SHORT.getDesc();
        m.checkNotNullExpressionValue(desc11, "SHORT.desc");
        String javaLang3 = wVar2.javaLang("Number");
        String desc12 = dVar3.getDesc();
        m.checkNotNullExpressionValue(desc12, "INT.desc");
        String javaLang4 = wVar2.javaLang("Number");
        String desc13 = d.LONG.getDesc();
        m.checkNotNullExpressionValue(desc13, "LONG.desc");
        String javaLang5 = wVar2.javaLang("Number");
        String desc14 = d.FLOAT.getDesc();
        m.checkNotNullExpressionValue(desc14, "FLOAT.desc");
        String javaLang6 = wVar2.javaLang("Number");
        String desc15 = d.DOUBLE.getDesc();
        m.checkNotNullExpressionValue(desc15, "DOUBLE.desc");
        String javaLang7 = wVar2.javaLang("CharSequence");
        String desc16 = dVar3.getDesc();
        m.checkNotNullExpressionValue(desc16, "INT.desc");
        String desc17 = d.CHAR.getDesc();
        m.checkNotNullExpressionValue(desc17, "CHAR.desc");
        Map<a.C0298a, e> mapOf2 = h0.mapOf(d0.o.to(a.access$method(aVar7, javaLang, "toByte", "", desc10), e.identifier("byteValue")), d0.o.to(a.access$method(aVar7, javaLang2, "toShort", "", desc11), e.identifier("shortValue")), d0.o.to(a.access$method(aVar7, javaLang3, "toInt", "", desc12), e.identifier("intValue")), d0.o.to(a.access$method(aVar7, javaLang4, "toLong", "", desc13), e.identifier("longValue")), d0.o.to(a.access$method(aVar7, javaLang5, "toFloat", "", desc14), e.identifier("floatValue")), d0.o.to(a.access$method(aVar7, javaLang6, "toDouble", "", desc15), e.identifier("doubleValue")), d0.o.to(aVar7.getREMOVE_AT_NAME_AND_SIGNATURE(), e.identifier("remove")), d0.o.to(a.access$method(aVar7, javaLang7, "get", desc16, desc17), e.identifier("charAt")));
        i = mapOf2;
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(mapOf2.size()));
        Iterator<T> it2 = mapOf2.entrySet().iterator();
        while (it2.hasNext()) {
            Map.Entry entry2 = (Map.Entry) it2.next();
            linkedHashMap2.put(((a.C0298a) entry2.getKey()).getSignature(), entry2.getValue());
        }
        j = linkedHashMap2;
        Set<a.C0298a> keySet = i.keySet();
        ArrayList arrayList6 = new ArrayList(o.collectionSizeOrDefault(keySet, 10));
        for (a.C0298a aVar8 : keySet) {
            arrayList6.add(aVar8.getName());
        }
        k = arrayList6;
        Set<Map.Entry<a.C0298a, e>> entrySet = i.entrySet();
        ArrayList<Pair> arrayList7 = new ArrayList(o.collectionSizeOrDefault(entrySet, 10));
        Iterator<T> it3 = entrySet.iterator();
        while (it3.hasNext()) {
            Map.Entry entry3 = (Map.Entry) it3.next();
            arrayList7.add(new Pair(((a.C0298a) entry3.getKey()).getName(), entry3.getValue()));
        }
        LinkedHashMap linkedHashMap3 = new LinkedHashMap();
        for (Pair pair : arrayList7) {
            e eVar = (e) pair.getSecond();
            Object obj = linkedHashMap3.get(eVar);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap3.put(eVar, obj);
            }
            ((List) obj).add((e) pair.getFirst());
        }
        l = linkedHashMap3;
    }
}
