package d0.e0.p.d.m0.e.a.i0;

import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.i0;
import d0.e0.p.d.m0.e.a.i0.k;
import d0.e0.p.d.m0.e.a.i0.l.i;
import d0.e0.p.d.m0.e.a.k0.u;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.m.f;
import d0.h;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: LazyJavaPackageFragmentProvider.kt */
/* loaded from: classes3.dex */
public final class f implements i0 {
    public final g a;

    /* renamed from: b  reason: collision with root package name */
    public final d0.e0.p.d.m0.m.a<b, i> f3294b;

    /* compiled from: LazyJavaPackageFragmentProvider.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<i> {
        public final /* synthetic */ u $jPackage;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(u uVar) {
            super(0);
            this.$jPackage = uVar;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final i invoke() {
            return new i(f.this.a, this.$jPackage);
        }
    }

    public f(b bVar) {
        m.checkNotNullParameter(bVar, "components");
        g gVar = new g(bVar, k.a.a, h.lazyOf(null));
        this.a = gVar;
        this.f3294b = gVar.getStorageManager().createCacheWithNotNullValues();
    }

    public final i a(b bVar) {
        u findPackage = this.a.getComponents().getFinder().findPackage(bVar);
        if (findPackage == null) {
            return null;
        }
        return (i) ((f.d) this.f3294b).computeIfAbsent(bVar, new a(findPackage));
    }

    @Override // d0.e0.p.d.m0.c.i0
    public void collectPackageFragments(b bVar, Collection<e0> collection) {
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(collection, "packageFragments");
        d0.e0.p.d.m0.p.a.addIfNotNull(collection, a(bVar));
    }

    @Override // d0.e0.p.d.m0.c.f0
    public List<i> getPackageFragments(b bVar) {
        m.checkNotNullParameter(bVar, "fqName");
        return n.listOfNotNull(a(bVar));
    }

    @Override // d0.e0.p.d.m0.c.f0
    public List<b> getSubPackagesOf(b bVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(bVar, "fqName");
        m.checkNotNullParameter(function1, "nameFilter");
        i a2 = a(bVar);
        List<b> subPackageFqNames$descriptors_jvm = a2 == null ? null : a2.getSubPackageFqNames$descriptors_jvm();
        return subPackageFqNames$descriptors_jvm != null ? subPackageFqNames$descriptors_jvm : n.emptyList();
    }
}
