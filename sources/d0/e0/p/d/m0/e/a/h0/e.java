package d0.e0.p.d.m0.e.a.h0;

import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t0;
import d0.z.d.m;
/* compiled from: JavaForKotlinOverridePropertyDescriptor.kt */
/* loaded from: classes3.dex */
public final class e extends g {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public e(d0.e0.p.d.m0.c.e eVar, t0 t0Var, t0 t0Var2, n0 n0Var) {
        super(eVar, g.f.getEMPTY(), t0Var.getModality(), t0Var.getVisibility(), t0Var2 != null, n0Var.getName(), t0Var.getSource(), null, b.a.DECLARATION, false, null);
        m.checkNotNullParameter(eVar, "ownerDescriptor");
        m.checkNotNullParameter(t0Var, "getterMethod");
        m.checkNotNullParameter(n0Var, "overriddenProperty");
    }
}
