package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.e.a.i0.l.c;
import d0.e0.p.d.m0.k.f;
import d0.z.d.m;
/* compiled from: FieldOverridabilityCondition.kt */
/* loaded from: classes3.dex */
public final class r implements f {
    @Override // d0.e0.p.d.m0.k.f
    public f.a getContract() {
        return f.a.BOTH;
    }

    @Override // d0.e0.p.d.m0.k.f
    public f.b isOverridable(a aVar, a aVar2, e eVar) {
        f.b bVar = f.b.UNKNOWN;
        m.checkNotNullParameter(aVar, "superDescriptor");
        m.checkNotNullParameter(aVar2, "subDescriptor");
        if (!(aVar2 instanceof n0) || !(aVar instanceof n0)) {
            return bVar;
        }
        n0 n0Var = (n0) aVar2;
        n0 n0Var2 = (n0) aVar;
        return !m.areEqual(n0Var.getName(), n0Var2.getName()) ? bVar : (!c.isJavaField(n0Var) || !c.isJavaField(n0Var2)) ? (c.isJavaField(n0Var) || c.isJavaField(n0Var2)) ? f.b.INCOMPATIBLE : bVar : f.b.OVERRIDABLE;
    }
}
