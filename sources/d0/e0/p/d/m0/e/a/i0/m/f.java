package d0.e0.p.d.m0.e.a.i0.m;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.e.a.g0.k;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.l1.g;
import d0.e0.p.d.m0.n.t;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.w0;
import d0.e0.p.d.m0.n.y;
import d0.e0.p.d.m0.n.y0;
import d0.e0.p.d.m0.n.z0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
/* compiled from: RawType.kt */
/* loaded from: classes3.dex */
public final class f extends z0 {

    /* renamed from: b  reason: collision with root package name */
    public static final f f3313b = new f();
    public static final d0.e0.p.d.m0.e.a.i0.m.a c;
    public static final d0.e0.p.d.m0.e.a.i0.m.a d;

    /* compiled from: RawType.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<g, j0> {
        public final /* synthetic */ d0.e0.p.d.m0.e.a.i0.m.a $attr;
        public final /* synthetic */ e $declaration;
        public final /* synthetic */ j0 $type;
        public final /* synthetic */ f this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(e eVar, f fVar, j0 j0Var, d0.e0.p.d.m0.e.a.i0.m.a aVar) {
            super(1);
            this.$declaration = eVar;
            this.this$0 = fVar;
            this.$type = j0Var;
            this.$attr = aVar;
        }

        public final j0 invoke(g gVar) {
            e findClassAcrossModuleDependencies;
            m.checkNotNullParameter(gVar, "kotlinTypeRefiner");
            e eVar = this.$declaration;
            if (!(eVar instanceof e)) {
                eVar = null;
            }
            d0.e0.p.d.m0.g.a classId = eVar == null ? null : d0.e0.p.d.m0.k.x.a.getClassId(eVar);
            if (classId == null || (findClassAcrossModuleDependencies = gVar.findClassAcrossModuleDependencies(classId)) == null || m.areEqual(findClassAcrossModuleDependencies, this.$declaration)) {
                return null;
            }
            return (j0) this.this$0.a(this.$type, findClassAcrossModuleDependencies, this.$attr).getFirst();
        }
    }

    static {
        k kVar = k.COMMON;
        c = e.toAttributes$default(kVar, false, null, 3, null).withFlexibility(b.FLEXIBLE_LOWER_BOUND);
        d = e.toAttributes$default(kVar, false, null, 3, null).withFlexibility(b.FLEXIBLE_UPPER_BOUND);
    }

    public static /* synthetic */ w0 computeProjection$default(f fVar, d0.e0.p.d.m0.c.z0 z0Var, d0.e0.p.d.m0.e.a.i0.m.a aVar, c0 c0Var, int i, Object obj) {
        if ((i & 4) != 0) {
            c0Var = e.getErasedUpperBound$default(z0Var, null, null, 3, null);
        }
        return fVar.computeProjection(z0Var, aVar, c0Var);
    }

    public final Pair<j0, Boolean> a(j0 j0Var, e eVar, d0.e0.p.d.m0.e.a.i0.m.a aVar) {
        if (j0Var.getConstructor().getParameters().isEmpty()) {
            return d0.o.to(j0Var, Boolean.FALSE);
        }
        if (h.isArray(j0Var)) {
            w0 w0Var = j0Var.getArguments().get(0);
            j1 projectionKind = w0Var.getProjectionKind();
            c0 type = w0Var.getType();
            m.checkNotNullExpressionValue(type, "componentTypeProjection.type");
            List listOf = d0.t.m.listOf(new y0(projectionKind, b(type)));
            d0 d0Var = d0.a;
            return d0.o.to(d0.simpleType$default(j0Var.getAnnotations(), j0Var.getConstructor(), listOf, j0Var.isMarkedNullable(), null, 16, null), Boolean.FALSE);
        } else if (e0.isError(j0Var)) {
            j0 createErrorType = t.createErrorType(m.stringPlus("Raw error type: ", j0Var.getConstructor()));
            m.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Raw error type: ${type.constructor}\")");
            return d0.o.to(createErrorType, Boolean.FALSE);
        } else {
            i memberScope = eVar.getMemberScope(this);
            m.checkNotNullExpressionValue(memberScope, "declaration.getMemberScope(RawSubstitution)");
            d0 d0Var2 = d0.a;
            d0.e0.p.d.m0.c.g1.g annotations = j0Var.getAnnotations();
            u0 typeConstructor = eVar.getTypeConstructor();
            m.checkNotNullExpressionValue(typeConstructor, "declaration.typeConstructor");
            List<d0.e0.p.d.m0.c.z0> parameters = eVar.getTypeConstructor().getParameters();
            m.checkNotNullExpressionValue(parameters, "declaration.typeConstructor.parameters");
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(parameters, 10));
            for (d0.e0.p.d.m0.c.z0 z0Var : parameters) {
                m.checkNotNullExpressionValue(z0Var, "parameter");
                arrayList.add(computeProjection$default(this, z0Var, aVar, null, 4, null));
            }
            return d0.o.to(d0.simpleTypeWithNonTrivialMemberScope(annotations, typeConstructor, arrayList, j0Var.isMarkedNullable(), memberScope, new a(eVar, this, j0Var, aVar)), Boolean.TRUE);
        }
    }

    public final c0 b(c0 c0Var) {
        d0.e0.p.d.m0.c.h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        if (declarationDescriptor instanceof d0.e0.p.d.m0.c.z0) {
            return b(e.getErasedUpperBound$default((d0.e0.p.d.m0.c.z0) declarationDescriptor, null, null, 3, null));
        }
        if (declarationDescriptor instanceof e) {
            d0.e0.p.d.m0.c.h declarationDescriptor2 = y.upperIfFlexible(c0Var).getConstructor().getDeclarationDescriptor();
            if (declarationDescriptor2 instanceof e) {
                Pair<j0, Boolean> a2 = a(y.lowerIfFlexible(c0Var), (e) declarationDescriptor, c);
                j0 component1 = a2.component1();
                boolean booleanValue = a2.component2().booleanValue();
                Pair<j0, Boolean> a3 = a(y.upperIfFlexible(c0Var), (e) declarationDescriptor2, d);
                j0 component12 = a3.component1();
                boolean booleanValue2 = a3.component2().booleanValue();
                if (booleanValue || booleanValue2) {
                    return new g(component1, component12);
                }
                d0 d0Var = d0.a;
                return d0.flexibleType(component1, component12);
            }
            throw new IllegalStateException(("For some reason declaration for upper bound is not a class but \"" + declarationDescriptor2 + "\" while for lower it's \"" + declarationDescriptor + '\"').toString());
        }
        throw new IllegalStateException(m.stringPlus("Unexpected declaration kind: ", declarationDescriptor).toString());
    }

    public final w0 computeProjection(d0.e0.p.d.m0.c.z0 z0Var, d0.e0.p.d.m0.e.a.i0.m.a aVar, c0 c0Var) {
        j1 j1Var = j1.INVARIANT;
        m.checkNotNullParameter(z0Var, "parameter");
        m.checkNotNullParameter(aVar, "attr");
        m.checkNotNullParameter(c0Var, "erasedUpperBound");
        int ordinal = aVar.getFlexibility().ordinal();
        if (ordinal == 0 || ordinal == 1) {
            if (!z0Var.getVariance().getAllowsOutPosition()) {
                return new y0(j1Var, d0.e0.p.d.m0.k.x.a.getBuiltIns(z0Var).getNothingType());
            }
            List<d0.e0.p.d.m0.c.z0> parameters = c0Var.getConstructor().getParameters();
            m.checkNotNullExpressionValue(parameters, "erasedUpperBound.constructor.parameters");
            if (!parameters.isEmpty()) {
                return new y0(j1.OUT_VARIANCE, c0Var);
            }
            return e.makeStarProjection(z0Var, aVar);
        } else if (ordinal == 2) {
            return new y0(j1Var, c0Var);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    @Override // d0.e0.p.d.m0.n.z0
    public boolean isEmpty() {
        return false;
    }

    @Override // d0.e0.p.d.m0.n.z0
    public y0 get(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "key");
        return new y0(b(c0Var));
    }
}
