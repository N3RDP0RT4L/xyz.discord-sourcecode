package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.g.e;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import kotlin.jvm.functions.Function1;
/* compiled from: ClassicBuiltinSpecialProperties.kt */
/* loaded from: classes3.dex */
public final class k {
    public static final k a = new k();

    /* compiled from: ClassicBuiltinSpecialProperties.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<b, Boolean> {
        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(b bVar) {
            return Boolean.valueOf(invoke2(bVar));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(b bVar) {
            m.checkNotNullParameter(bVar, "it");
            return k.this.hasBuiltinSpecialPropertyFqName(bVar);
        }
    }

    public final String getBuiltinSpecialPropertyGetterName(b bVar) {
        e eVar;
        m.checkNotNullParameter(bVar, "<this>");
        h.isBuiltIn(bVar);
        b firstOverridden$default = d0.e0.p.d.m0.k.x.a.firstOverridden$default(d0.e0.p.d.m0.k.x.a.getPropertyIfAccessor(bVar), false, new a(), 1, null);
        if (firstOverridden$default == null || (eVar = i.a.getPROPERTY_FQ_NAME_TO_JVM_GETTER_NAME_MAP().get(d0.e0.p.d.m0.k.x.a.getFqNameSafe(firstOverridden$default))) == null) {
            return null;
        }
        return eVar.asString();
    }

    public final boolean hasBuiltinSpecialPropertyFqName(b bVar) {
        m.checkNotNullParameter(bVar, "callableMemberDescriptor");
        i iVar = i.a;
        if (!iVar.getSPECIAL_SHORT_NAMES().contains(bVar.getName())) {
            return false;
        }
        if (u.contains(iVar.getSPECIAL_FQ_NAMES(), d0.e0.p.d.m0.k.x.a.fqNameOrNull(bVar)) && bVar.getValueParameters().isEmpty()) {
            return true;
        }
        if (!h.isBuiltIn(bVar)) {
            return false;
        }
        Collection<? extends b> overriddenDescriptors = bVar.getOverriddenDescriptors();
        m.checkNotNullExpressionValue(overriddenDescriptors, "overriddenDescriptors");
        if (overriddenDescriptors.isEmpty()) {
            return false;
        }
        for (b bVar2 : overriddenDescriptors) {
            m.checkNotNullExpressionValue(bVar2, "it");
            if (hasBuiltinSpecialPropertyFqName(bVar2)) {
                return true;
            }
        }
        return false;
    }
}
