package d0.e0.p.d.m0.e.a.i0.l;

import d0.e0.p.d.m0.e.a.k0.q;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: LazyJavaStaticClassScope.kt */
/* loaded from: classes3.dex */
public final class n extends o implements Function1<q, Boolean> {
    public static final n j = new n();

    public n() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(q qVar) {
        return Boolean.valueOf(invoke2(qVar));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(q qVar) {
        m.checkNotNullParameter(qVar, "it");
        return qVar.isStatic();
    }
}
