package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.e.a.k0.g;
import d0.e0.p.d.m0.e.a.k0.u;
import d0.e0.p.d.m0.g.b;
import d0.z.d.m;
import java.util.Arrays;
import java.util.Set;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: JavaClassFinder.kt */
/* loaded from: classes3.dex */
public interface s {
    g findClass(a aVar);

    u findPackage(b bVar);

    Set<String> knownClassNamesInPackage(b bVar);

    /* compiled from: JavaClassFinder.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final d0.e0.p.d.m0.g.a a;

        /* renamed from: b  reason: collision with root package name */
        public final byte[] f3329b;
        public final g c;

        public a(d0.e0.p.d.m0.g.a aVar, byte[] bArr, g gVar) {
            m.checkNotNullParameter(aVar, "classId");
            this.a = aVar;
            this.f3329b = bArr;
            this.c = gVar;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return m.areEqual(this.a, aVar.a) && m.areEqual(this.f3329b, aVar.f3329b) && m.areEqual(this.c, aVar.c);
        }

        public final d0.e0.p.d.m0.g.a getClassId() {
            return this.a;
        }

        public int hashCode() {
            int hashCode = this.a.hashCode() * 31;
            byte[] bArr = this.f3329b;
            int i = 0;
            int hashCode2 = (hashCode + (bArr == null ? 0 : Arrays.hashCode(bArr))) * 31;
            g gVar = this.c;
            if (gVar != null) {
                i = gVar.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Request(classId=");
            R.append(this.a);
            R.append(", previouslyFoundClassFileContent=");
            R.append(Arrays.toString(this.f3329b));
            R.append(", outerClass=");
            R.append(this.c);
            R.append(')');
            return R.toString();
        }

        public /* synthetic */ a(d0.e0.p.d.m0.g.a aVar, byte[] bArr, g gVar, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(aVar, (i & 2) != 0 ? null : bArr, (i & 4) != 0 ? null : gVar);
        }
    }
}
