package d0.e0.p.d.m0.e.a.g0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.d;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.i;
import d0.e0.p.d.m0.k.k;
import d0.e0.p.d.m0.l.b.p;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: DescriptorResolverUtils.java */
/* loaded from: classes3.dex */
public final class a {

    /* compiled from: DescriptorResolverUtils.java */
    /* renamed from: d0.e0.p.d.m0.e.a.g0.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static class C0299a extends i {
        public final /* synthetic */ p a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Set f3285b;
        public final /* synthetic */ boolean c;

        /* compiled from: DescriptorResolverUtils.java */
        /* renamed from: d0.e0.p.d.m0.e.a.g0.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class C0300a implements Function1<b, Unit> {
            public C0300a() {
            }

            public Unit invoke(b bVar) {
                if (bVar != null) {
                    C0299a.this.a.reportCannotInferVisibility(bVar);
                    return Unit.a;
                }
                throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", "descriptor", "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils$1$1", "invoke"));
            }
        }

        public C0299a(p pVar, Set set, boolean z2) {
            this.a = pVar;
            this.f3285b = set;
            this.c = z2;
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i == 1) {
                objArr[0] = "fromSuper";
            } else if (i == 2) {
                objArr[0] = "fromCurrent";
            } else if (i == 3) {
                objArr[0] = "member";
            } else if (i != 4) {
                objArr[0] = "fakeOverride";
            } else {
                objArr[0] = "overridden";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils$1";
            if (i == 1 || i == 2) {
                objArr[2] = "conflict";
            } else if (i == 3 || i == 4) {
                objArr[2] = "setOverriddenDescriptors";
            } else {
                objArr[2] = "addFakeOverride";
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.k.j
        public void addFakeOverride(b bVar) {
            if (bVar != null) {
                k.resolveUnknownVisibilityForMember(bVar, new C0300a());
                this.f3285b.add(bVar);
                return;
            }
            a(0);
            throw null;
        }

        @Override // d0.e0.p.d.m0.k.i
        public void conflict(b bVar, b bVar2) {
            if (bVar == null) {
                a(1);
                throw null;
            } else if (bVar2 == null) {
                a(2);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.k.j
        public void setOverriddenDescriptors(b bVar, Collection<? extends b> collection) {
            if (bVar == null) {
                a(3);
                throw null;
            } else if (collection == null) {
                a(4);
                throw null;
            } else if (!this.c || bVar.getKind() == b.a.FAKE_OVERRIDE) {
                super.setOverriddenDescriptors(bVar, collection);
            }
        }
    }

    public static /* synthetic */ void a(int i) {
        String str = i != 18 ? "Argument for @NotNull parameter '%s' of %s.%s must not be null" : "@NotNull method %s.%s must not return null";
        Object[] objArr = new Object[i != 18 ? 3 : 2];
        switch (i) {
            case 1:
            case 7:
            case 13:
                objArr[0] = "membersFromSupertypes";
                break;
            case 2:
            case 8:
            case 14:
                objArr[0] = "membersFromCurrent";
                break;
            case 3:
            case 9:
            case 15:
                objArr[0] = "classDescriptor";
                break;
            case 4:
            case 10:
            case 16:
                objArr[0] = "errorReporter";
                break;
            case 5:
            case 11:
            case 17:
                objArr[0] = "overridingUtil";
                break;
            case 6:
            case 12:
            case 19:
            default:
                objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
                break;
            case 18:
                objArr[0] = "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils";
                break;
            case 20:
                objArr[0] = "annotationClass";
                break;
        }
        if (i != 18) {
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/java/components/DescriptorResolverUtils";
        } else {
            objArr[1] = "resolveOverrides";
        }
        switch (i) {
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                objArr[2] = "resolveOverridesForStaticMembers";
                break;
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
                objArr[2] = "resolveOverrides";
                break;
            case 18:
                break;
            case 19:
            case 20:
                objArr[2] = "getAnnotationParameterByName";
                break;
            default:
                objArr[2] = "resolveOverridesForNonStaticMembers";
                break;
        }
        String format = String.format(str, objArr);
        if (i == 18) {
            throw new IllegalStateException(format);
        }
    }

    public static <D extends b> Collection<D> b(e eVar, Collection<D> collection, Collection<D> collection2, d0.e0.p.d.m0.c.e eVar2, p pVar, k kVar, boolean z2) {
        if (eVar == null) {
            a(12);
            throw null;
        } else if (collection == null) {
            a(13);
            throw null;
        } else if (collection2 == null) {
            a(14);
            throw null;
        } else if (eVar2 == null) {
            a(15);
            throw null;
        } else if (pVar == null) {
            a(16);
            throw null;
        } else if (kVar != null) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            kVar.generateOverridesInFunctionGroup(eVar, collection, collection2, eVar2, new C0299a(pVar, linkedHashSet, z2));
            return linkedHashSet;
        } else {
            a(17);
            throw null;
        }
    }

    public static c1 getAnnotationParameterByName(e eVar, d0.e0.p.d.m0.c.e eVar2) {
        if (eVar == null) {
            a(19);
            throw null;
        } else if (eVar2 != null) {
            Collection<d> constructors = eVar2.getConstructors();
            if (constructors.size() != 1) {
                return null;
            }
            for (c1 c1Var : constructors.iterator().next().getValueParameters()) {
                if (c1Var.getName().equals(eVar)) {
                    return c1Var;
                }
            }
            return null;
        } else {
            a(20);
            throw null;
        }
    }

    public static <D extends b> Collection<D> resolveOverridesForNonStaticMembers(e eVar, Collection<D> collection, Collection<D> collection2, d0.e0.p.d.m0.c.e eVar2, p pVar, k kVar) {
        if (eVar == null) {
            a(0);
            throw null;
        } else if (collection == null) {
            a(1);
            throw null;
        } else if (collection2 == null) {
            a(2);
            throw null;
        } else if (eVar2 == null) {
            a(3);
            throw null;
        } else if (pVar == null) {
            a(4);
            throw null;
        } else if (kVar != null) {
            return b(eVar, collection, collection2, eVar2, pVar, kVar, false);
        } else {
            a(5);
            throw null;
        }
    }

    public static <D extends b> Collection<D> resolveOverridesForStaticMembers(e eVar, Collection<D> collection, Collection<D> collection2, d0.e0.p.d.m0.c.e eVar2, p pVar, k kVar) {
        if (eVar == null) {
            a(6);
            throw null;
        } else if (collection == null) {
            a(7);
            throw null;
        } else if (collection2 == null) {
            a(8);
            throw null;
        } else if (eVar2 == null) {
            a(9);
            throw null;
        } else if (pVar == null) {
            a(10);
            throw null;
        } else if (kVar != null) {
            return b(eVar, collection, collection2, eVar2, pVar, kVar, true);
        } else {
            a(11);
            throw null;
        }
    }
}
