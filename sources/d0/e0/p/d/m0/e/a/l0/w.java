package d0.e0.p.d.m0.e.a.l0;

import d0.z.d.m;
import java.util.Map;
/* compiled from: predefinedEnhancementInfo.kt */
/* loaded from: classes3.dex */
public final class w {
    public final Map<Integer, e> a;

    public w(Map<Integer, e> map) {
        m.checkNotNullParameter(map, "map");
        this.a = map;
    }

    public final Map<Integer, e> getMap() {
        return this.a;
    }
}
