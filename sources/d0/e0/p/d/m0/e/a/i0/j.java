package d0.e0.p.d.m0.e.a.i0;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.e.a.k0.g;
import d0.e0.p.d.m0.k.y.b;
import d0.z.d.m;
/* compiled from: ModuleClassResolver.kt */
/* loaded from: classes3.dex */
public final class j implements i {
    public b a;

    public final b getResolver() {
        b bVar = this.a;
        if (bVar != null) {
            return bVar;
        }
        m.throwUninitializedPropertyAccessException("resolver");
        throw null;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.i
    public e resolveClass(g gVar) {
        m.checkNotNullParameter(gVar, "javaClass");
        return getResolver().resolveClass(gVar);
    }

    public final void setResolver(b bVar) {
        m.checkNotNullParameter(bVar, "<set-?>");
        this.a = bVar;
    }
}
