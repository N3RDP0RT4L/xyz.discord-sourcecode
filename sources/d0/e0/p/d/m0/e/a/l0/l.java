package d0.e0.p.d.m0.e.a.l0;

import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.a.i0.g;
import d0.e0.p.d.m0.e.a.i0.l.s;
import d0.e0.p.d.m0.e.a.u;
import d0.e0.p.d.m0.e.a.y;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e0;
import d0.e0.p.d.m0.n.i0;
import d0.e0.p.d.m0.n.i1;
import d0.e0.p.d.m0.n.w0;
import d0.e0.p.d.m0.p.e;
import d0.e0.p.d.m0.p.h;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: signatureEnhancement.kt */
/* loaded from: classes3.dex */
public final class l {
    public final d0.e0.p.d.m0.e.a.c a;

    /* renamed from: b  reason: collision with root package name */
    public final e f3320b;
    public final d0.e0.p.d.m0.e.a.l0.d c;

    /* compiled from: signatureEnhancement.kt */
    /* loaded from: classes3.dex */
    public static class a {
        public final c0 a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f3321b;
        public final boolean c;

        public a(c0 c0Var, boolean z2, boolean z3) {
            m.checkNotNullParameter(c0Var, "type");
            this.a = c0Var;
            this.f3321b = z2;
            this.c = z3;
        }

        public final boolean getContainsFunctionN() {
            return this.c;
        }

        public final c0 getType() {
            return this.a;
        }

        public final boolean getWereChanges() {
            return this.f3321b;
        }
    }

    /* compiled from: signatureEnhancement.kt */
    /* loaded from: classes3.dex */
    public static final class c extends a {
        public final boolean d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(c0 c0Var, boolean z2, boolean z3, boolean z4) {
            super(c0Var, z3, z4);
            m.checkNotNullParameter(c0Var, "type");
            this.d = z2;
        }

        public final boolean getHasDefaultValue() {
            return this.d;
        }
    }

    /* compiled from: signatureEnhancement.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<i1, Boolean> {
        public static final d j = new d();

        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(i1 i1Var) {
            return Boolean.valueOf(invoke2(i1Var));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(i1 i1Var) {
            m.checkNotNullParameter(i1Var, "it");
            return i1Var instanceof i0;
        }
    }

    public l(d0.e0.p.d.m0.e.a.c cVar, e eVar, d0.e0.p.d.m0.e.a.l0.d dVar) {
        m.checkNotNullParameter(cVar, "annotationTypeQualifierResolver");
        m.checkNotNullParameter(eVar, "javaTypeEnhancementState");
        m.checkNotNullParameter(dVar, "typeEnhancement");
        this.a = cVar;
        this.f3320b = eVar;
        this.c = dVar;
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x00d6, code lost:
        if (r2.equals("NEVER") == false) goto L71;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x00df, code lost:
        if (r2.equals("MAYBE") == false) goto L71;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x00e2, code lost:
        r10 = new d0.e0.p.d.m0.e.a.l0.i(r1, r9);
     */
    /* JADX WARN: Removed duplicated region for block: B:26:0x005b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final d0.e0.p.d.m0.e.a.l0.i a(d0.e0.p.d.m0.c.g1.c r8, boolean r9, boolean r10) {
        /*
            Method dump skipped, instructions count: 360
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.l0.l.a(d0.e0.p.d.m0.c.g1.c, boolean, boolean):d0.e0.p.d.m0.e.a.l0.i");
    }

    public final b b(d0.e0.p.d.m0.c.b bVar, d0.e0.p.d.m0.c.g1.a aVar, boolean z2, g gVar, d0.e0.p.d.m0.e.a.a aVar2, Function1<? super d0.e0.p.d.m0.c.b, ? extends c0> function1) {
        c0 invoke = function1.invoke(bVar);
        Collection<? extends d0.e0.p.d.m0.c.b> overriddenDescriptors = bVar.getOverriddenDescriptors();
        m.checkNotNullExpressionValue(overriddenDescriptors, "this.overriddenDescriptors");
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(overriddenDescriptors, 10));
        for (d0.e0.p.d.m0.c.b bVar2 : overriddenDescriptors) {
            m.checkNotNullExpressionValue(bVar2, "it");
            arrayList.add(function1.invoke(bVar2));
        }
        return new b(aVar, invoke, arrayList, z2, d0.e0.p.d.m0.e.a.i0.a.copyWithNewDefaultTypeQualifiers(gVar, function1.invoke(bVar).getAnnotations()), aVar2, false, 64, null);
    }

    public final b c(d0.e0.p.d.m0.c.b bVar, c1 c1Var, g gVar, Function1<? super d0.e0.p.d.m0.c.b, ? extends c0> function1) {
        g copyWithNewDefaultTypeQualifiers;
        return b(bVar, c1Var, false, (c1Var == null || (copyWithNewDefaultTypeQualifiers = d0.e0.p.d.m0.e.a.i0.a.copyWithNewDefaultTypeQualifiers(gVar, c1Var.getAnnotations())) == null) ? gVar : copyWithNewDefaultTypeQualifiers, d0.e0.p.d.m0.e.a.a.VALUE_PARAMETER, function1);
    }

    /* JADX WARN: Removed duplicated region for block: B:101:0x01f0  */
    /* JADX WARN: Removed duplicated region for block: B:102:0x01f2  */
    /* JADX WARN: Removed duplicated region for block: B:105:0x0200  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x020d  */
    /* JADX WARN: Removed duplicated region for block: B:117:0x0229  */
    /* JADX WARN: Removed duplicated region for block: B:120:0x022e  */
    /* JADX WARN: Removed duplicated region for block: B:121:0x0230  */
    /* JADX WARN: Removed duplicated region for block: B:124:0x0240  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x024d  */
    /* JADX WARN: Removed duplicated region for block: B:138:0x026c  */
    /* JADX WARN: Removed duplicated region for block: B:139:0x027a  */
    /* JADX WARN: Removed duplicated region for block: B:141:0x027d  */
    /* JADX WARN: Removed duplicated region for block: B:142:0x027f  */
    /* JADX WARN: Removed duplicated region for block: B:146:0x0298 A[LOOP:4: B:144:0x0292->B:146:0x0298, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0089  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00ab  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00b1  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00b5  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00df  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0109  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x01b4  */
    /* JADX WARN: Removed duplicated region for block: B:86:0x01b6  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x01bb  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x01bd  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x01cd  */
    /* JADX WARN: Removed duplicated region for block: B:94:0x01d0  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x01e4  */
    /* JADX WARN: Removed duplicated region for block: B:98:0x01e6  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final <D extends d0.e0.p.d.m0.c.b> java.util.Collection<D> enhanceSignatures(d0.e0.p.d.m0.e.a.i0.g r21, java.util.Collection<? extends D> r22) {
        /*
            Method dump skipped, instructions count: 701
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.l0.l.enhanceSignatures(d0.e0.p.d.m0.e.a.i0.g, java.util.Collection):java.util.Collection");
    }

    public final c0 enhanceSuperType(c0 c0Var, g gVar) {
        m.checkNotNullParameter(c0Var, "type");
        m.checkNotNullParameter(gVar, "context");
        return b.enhance$default(new b(null, c0Var, n.emptyList(), false, gVar, d0.e0.p.d.m0.e.a.a.TYPE_USE, false, 64, null), null, 1, null).getType();
    }

    public final List<c0> enhanceTypeParameterBounds(z0 z0Var, List<? extends c0> list, g gVar) {
        m.checkNotNullParameter(z0Var, "typeParameter");
        m.checkNotNullParameter(list, "bounds");
        m.checkNotNullParameter(gVar, "context");
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        for (c0 c0Var : list) {
            if (!d0.e0.p.d.m0.n.o1.a.contains(c0Var, d.j)) {
                c0Var = b.enhance$default(new b(this, z0Var, c0Var, n.emptyList(), false, gVar, d0.e0.p.d.m0.e.a.a.TYPE_PARAMETER_BOUNDS, true), null, 1, null).getType();
            }
            arrayList.add(c0Var);
        }
        return arrayList;
    }

    public final i extractNullability(d0.e0.p.d.m0.c.g1.c cVar, boolean z2, boolean z3) {
        i a2;
        m.checkNotNullParameter(cVar, "annotationDescriptor");
        i a3 = a(cVar, z2, z3);
        if (a3 != null) {
            return a3;
        }
        d0.e0.p.d.m0.c.g1.c resolveTypeQualifierAnnotation = this.a.resolveTypeQualifierAnnotation(cVar);
        if (resolveTypeQualifierAnnotation == null) {
            return null;
        }
        h resolveJsr305AnnotationState = this.a.resolveJsr305AnnotationState(cVar);
        if (!resolveJsr305AnnotationState.isIgnore() && (a2 = a(resolveTypeQualifierAnnotation, z2, z3)) != null) {
            return i.copy$default(a2, null, resolveJsr305AnnotationState.isWarning(), 1, null);
        }
        return null;
    }

    /* compiled from: signatureEnhancement.kt */
    /* loaded from: classes3.dex */
    public final class b {
        public final d0.e0.p.d.m0.c.g1.a a;

        /* renamed from: b  reason: collision with root package name */
        public final c0 f3322b;
        public final Collection<c0> c;
        public final boolean d;
        public final g e;
        public final d0.e0.p.d.m0.e.a.a f;
        public final boolean g;

        /* compiled from: signatureEnhancement.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function1<i1, Boolean> {
            public static final a j = new a();

            public a() {
                super(1);
            }

            public final Boolean invoke(i1 i1Var) {
                d0.e0.p.d.m0.c.h declarationDescriptor = i1Var.getConstructor().getDeclarationDescriptor();
                if (declarationDescriptor == null) {
                    return Boolean.FALSE;
                }
                d0.e0.p.d.m0.g.e name = declarationDescriptor.getName();
                d0.e0.p.d.m0.b.q.c cVar = d0.e0.p.d.m0.b.q.c.a;
                return Boolean.valueOf(m.areEqual(name, cVar.getFUNCTION_N_FQ_NAME().shortName()) && m.areEqual(d0.e0.p.d.m0.k.x.a.fqNameOrNull(declarationDescriptor), cVar.getFUNCTION_N_FQ_NAME()));
            }
        }

        /* compiled from: signatureEnhancement.kt */
        /* renamed from: d0.e0.p.d.m0.e.a.l0.l$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0308b extends o implements Function1<Integer, e> {
            public final /* synthetic */ w $predefined;
            public final /* synthetic */ Function1<Integer, e> $qualifiers;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public C0308b(w wVar, Function1<? super Integer, e> function1) {
                super(1);
                this.$predefined = wVar;
                this.$qualifiers = function1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ e invoke(Integer num) {
                return invoke(num.intValue());
            }

            public final e invoke(int i) {
                e eVar = this.$predefined.getMap().get(Integer.valueOf(i));
                return eVar == null ? this.$qualifiers.invoke(Integer.valueOf(i)) : eVar;
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public b(l lVar, d0.e0.p.d.m0.c.g1.a aVar, c0 c0Var, Collection<? extends c0> collection, boolean z2, g gVar, d0.e0.p.d.m0.e.a.a aVar2, boolean z3) {
            m.checkNotNullParameter(lVar, "this$0");
            m.checkNotNullParameter(c0Var, "fromOverride");
            m.checkNotNullParameter(collection, "fromOverridden");
            m.checkNotNullParameter(gVar, "containerContext");
            m.checkNotNullParameter(aVar2, "containerApplicabilityType");
            l.this = lVar;
            this.a = aVar;
            this.f3322b = c0Var;
            this.c = collection;
            this.d = z2;
            this.e = gVar;
            this.f = aVar2;
            this.g = z3;
        }

        public static final <T> T c(List<d0.e0.p.d.m0.g.b> list, d0.e0.p.d.m0.c.g1.g gVar, T t) {
            boolean z2;
            boolean z3 = false;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (gVar.findAnnotation((d0.e0.p.d.m0.g.b) it.next()) != null) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (z2) {
                            z3 = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (z3) {
                return t;
            }
            return null;
        }

        public static final void d(b bVar, ArrayList<s> arrayList, c0 c0Var, g gVar, z0 z0Var) {
            u uVar;
            d0.e0.p.d.m0.e.a.a aVar;
            g copyWithNewDefaultTypeQualifiers = d0.e0.p.d.m0.e.a.i0.a.copyWithNewDefaultTypeQualifiers(gVar, c0Var.getAnnotations());
            y defaultTypeQualifiers = copyWithNewDefaultTypeQualifiers.getDefaultTypeQualifiers();
            if (defaultTypeQualifiers == null) {
                uVar = null;
            } else {
                if (bVar.g) {
                    aVar = d0.e0.p.d.m0.e.a.a.TYPE_PARAMETER_BOUNDS;
                } else {
                    aVar = d0.e0.p.d.m0.e.a.a.TYPE_USE;
                }
                uVar = defaultTypeQualifiers.get(aVar);
            }
            arrayList.add(new s(c0Var, uVar, z0Var, false));
            List<w0> arguments = c0Var.getArguments();
            List<z0> parameters = c0Var.getConstructor().getParameters();
            m.checkNotNullExpressionValue(parameters, "type.constructor.parameters");
            for (Pair pair : d0.t.u.zip(arguments, parameters)) {
                w0 w0Var = (w0) pair.component1();
                z0 z0Var2 = (z0) pair.component2();
                if (w0Var.isStarProjection()) {
                    c0 type = w0Var.getType();
                    m.checkNotNullExpressionValue(type, "arg.type");
                    arrayList.add(new s(type, uVar, z0Var2, true));
                } else {
                    c0 type2 = w0Var.getType();
                    m.checkNotNullExpressionValue(type2, "arg.type");
                    d(bVar, arrayList, type2, copyWithNewDefaultTypeQualifiers, z0Var2);
                }
            }
        }

        public static /* synthetic */ a enhance$default(b bVar, w wVar, int i, Object obj) {
            if ((i & 1) != 0) {
                wVar = null;
            }
            return bVar.enhance(wVar);
        }

        public final h a(z0 z0Var) {
            boolean z2;
            boolean z3;
            if (!(z0Var instanceof s)) {
                return null;
            }
            s sVar = (s) z0Var;
            List<c0> upperBounds = sVar.getUpperBounds();
            m.checkNotNullExpressionValue(upperBounds, "upperBounds");
            boolean z4 = false;
            if (!(upperBounds instanceof Collection) || !upperBounds.isEmpty()) {
                for (c0 c0Var : upperBounds) {
                    if (!e0.isError(c0Var)) {
                        z2 = false;
                        break;
                    }
                }
            }
            z2 = true;
            if (z2) {
                return null;
            }
            List<c0> upperBounds2 = sVar.getUpperBounds();
            m.checkNotNullExpressionValue(upperBounds2, "upperBounds");
            if (!(upperBounds2 instanceof Collection) || !upperBounds2.isEmpty()) {
                for (c0 c0Var2 : upperBounds2) {
                    if (!r.access$isNullabilityFlexible(c0Var2)) {
                        z3 = false;
                        break;
                    }
                }
            }
            z3 = true;
            if (z3) {
                return null;
            }
            List<c0> upperBounds3 = sVar.getUpperBounds();
            m.checkNotNullExpressionValue(upperBounds3, "upperBounds");
            if (!(upperBounds3 instanceof Collection) || !upperBounds3.isEmpty()) {
                Iterator<T> it = upperBounds3.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    c0 c0Var3 = (c0) it.next();
                    m.checkNotNullExpressionValue(c0Var3, "it");
                    if (!e0.isNullable(c0Var3)) {
                        z4 = true;
                        break;
                    }
                }
            }
            return z4 ? h.NOT_NULL : h.NULLABLE;
        }

        /* JADX WARN: Removed duplicated region for block: B:16:0x0048  */
        /* JADX WARN: Removed duplicated region for block: B:17:0x004b  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final d0.e0.p.d.m0.e.a.l0.e b(d0.e0.p.d.m0.n.c0 r12) {
            /*
                r11 = this;
                boolean r0 = d0.e0.p.d.m0.n.y.isFlexible(r12)
                if (r0 == 0) goto L18
                d0.e0.p.d.m0.n.v r0 = d0.e0.p.d.m0.n.y.asFlexibleType(r12)
                kotlin.Pair r1 = new kotlin.Pair
                d0.e0.p.d.m0.n.j0 r2 = r0.getLowerBound()
                d0.e0.p.d.m0.n.j0 r0 = r0.getUpperBound()
                r1.<init>(r2, r0)
                goto L1d
            L18:
                kotlin.Pair r1 = new kotlin.Pair
                r1.<init>(r12, r12)
            L1d:
                java.lang.Object r0 = r1.component1()
                d0.e0.p.d.m0.n.c0 r0 = (d0.e0.p.d.m0.n.c0) r0
                java.lang.Object r1 = r1.component2()
                d0.e0.p.d.m0.n.c0 r1 = (d0.e0.p.d.m0.n.c0) r1
                d0.e0.p.d.m0.b.q.d r2 = d0.e0.p.d.m0.b.q.d.a
                d0.e0.p.d.m0.e.a.l0.e r10 = new d0.e0.p.d.m0.e.a.l0.e
                boolean r3 = r0.isMarkedNullable()
                r4 = 0
                if (r3 == 0) goto L38
                d0.e0.p.d.m0.e.a.l0.h r3 = d0.e0.p.d.m0.e.a.l0.h.NULLABLE
            L36:
                r5 = r3
                goto L42
            L38:
                boolean r3 = r1.isMarkedNullable()
                if (r3 != 0) goto L41
                d0.e0.p.d.m0.e.a.l0.h r3 = d0.e0.p.d.m0.e.a.l0.h.NOT_NULL
                goto L36
            L41:
                r5 = r4
            L42:
                boolean r0 = r2.isReadOnly(r0)
                if (r0 == 0) goto L4b
                d0.e0.p.d.m0.e.a.l0.f r0 = d0.e0.p.d.m0.e.a.l0.f.READ_ONLY
                goto L55
            L4b:
                boolean r0 = r2.isMutable(r1)
                if (r0 == 0) goto L54
                d0.e0.p.d.m0.e.a.l0.f r0 = d0.e0.p.d.m0.e.a.l0.f.MUTABLE
                goto L55
            L54:
                r0 = r4
            L55:
                d0.e0.p.d.m0.n.i1 r12 = r12.unwrap()
                boolean r6 = r12 instanceof d0.e0.p.d.m0.e.a.l0.g
                r7 = 0
                r8 = 8
                r9 = 0
                r3 = r10
                r4 = r5
                r5 = r0
                r3.<init>(r4, r5, r6, r7, r8, r9)
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.l0.l.b.b(d0.e0.p.d.m0.n.c0):d0.e0.p.d.m0.e.a.l0.e");
        }

        /* JADX WARN: Code restructure failed: missing block: B:104:0x0237, code lost:
            if ((r14.getAffectsTypeParameterBasedTypes() || !d0.e0.p.d.m0.n.o1.a.isTypeParameter(r13)) != false) goto L106;
         */
        /* JADX WARN: Code restructure failed: missing block: B:156:0x0312, code lost:
            if (r9.getQualifier() == r5) goto L164;
         */
        /* JADX WARN: Code restructure failed: missing block: B:162:0x0329, code lost:
            if (d0.z.d.m.areEqual(r14 == null ? r2 : java.lang.Boolean.valueOf(r14.getMakesTypeParameterNotNull()), java.lang.Boolean.TRUE) != false) goto L164;
         */
        /* JADX WARN: Code restructure failed: missing block: B:163:0x032c, code lost:
            r3 = false;
         */
        /* JADX WARN: Code restructure failed: missing block: B:215:0x03ca, code lost:
            if ((((r10 == null ? r2 : r10.getVarargElementType()) != null) && r11 && r6 == r8) == false) goto L217;
         */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Removed duplicated region for block: B:108:0x0247  */
        /* JADX WARN: Removed duplicated region for block: B:109:0x024a  */
        /* JADX WARN: Removed duplicated region for block: B:111:0x024d  */
        /* JADX WARN: Removed duplicated region for block: B:112:0x024f  */
        /* JADX WARN: Removed duplicated region for block: B:114:0x0255  */
        /* JADX WARN: Removed duplicated region for block: B:115:0x0264  */
        /* JADX WARN: Removed duplicated region for block: B:123:0x029f  */
        /* JADX WARN: Removed duplicated region for block: B:131:0x02b7  */
        /* JADX WARN: Removed duplicated region for block: B:153:0x030a  */
        /* JADX WARN: Removed duplicated region for block: B:155:0x030e  */
        /* JADX WARN: Removed duplicated region for block: B:157:0x0315  */
        /* JADX WARN: Removed duplicated region for block: B:167:0x0333  */
        /* JADX WARN: Removed duplicated region for block: B:168:0x0335  */
        /* JADX WARN: Removed duplicated region for block: B:171:0x034f A[ADDED_TO_REGION] */
        /* JADX WARN: Removed duplicated region for block: B:176:0x035d  */
        /* JADX WARN: Removed duplicated region for block: B:179:0x0362  */
        /* JADX WARN: Removed duplicated region for block: B:184:0x036d  */
        /* JADX WARN: Removed duplicated region for block: B:185:0x036f  */
        /* JADX WARN: Removed duplicated region for block: B:188:0x0388  */
        /* JADX WARN: Removed duplicated region for block: B:189:0x038a  */
        /* JADX WARN: Removed duplicated region for block: B:191:0x038d  */
        /* JADX WARN: Removed duplicated region for block: B:192:0x038f  */
        /* JADX WARN: Removed duplicated region for block: B:195:0x039b A[ADDED_TO_REGION] */
        /* JADX WARN: Removed duplicated region for block: B:200:0x03ab  */
        /* JADX WARN: Removed duplicated region for block: B:219:0x03dc  */
        /* JADX WARN: Removed duplicated region for block: B:222:0x03e7  */
        /* JADX WARN: Removed duplicated region for block: B:226:0x03f0  */
        /* JADX WARN: Removed duplicated region for block: B:239:0x0418 A[ADDED_TO_REGION] */
        /* JADX WARN: Removed duplicated region for block: B:23:0x0073  */
        /* JADX WARN: Removed duplicated region for block: B:242:0x0426  */
        /* JADX WARN: Removed duplicated region for block: B:243:0x0428  */
        /* JADX WARN: Removed duplicated region for block: B:248:0x044f  */
        /* JADX WARN: Removed duplicated region for block: B:249:0x0451  */
        /* JADX WARN: Removed duplicated region for block: B:24:0x0075  */
        /* JADX WARN: Removed duplicated region for block: B:252:0x0469  */
        /* JADX WARN: Removed duplicated region for block: B:255:0x0470  */
        /* JADX WARN: Removed duplicated region for block: B:256:0x0472  */
        /* JADX WARN: Removed duplicated region for block: B:258:0x047a  */
        /* JADX WARN: Removed duplicated region for block: B:27:0x007e  */
        /* JADX WARN: Removed duplicated region for block: B:285:0x02ac A[SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:288:? A[RETURN, SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:92:0x0211  */
        /* JADX WARN: Removed duplicated region for block: B:96:0x0222  */
        /* JADX WARN: Removed duplicated region for block: B:98:0x0227  */
        /* JADX WARN: Type inference failed for: r1v12, types: [d0.e0.p.d.m0.e.a.l0.e] */
        /* JADX WARN: Type inference failed for: r1v25 */
        /* JADX WARN: Type inference failed for: r9v3, types: [d0.e0.p.d.m0.e.a.l0.l$b$b] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final d0.e0.p.d.m0.e.a.l0.l.a enhance(d0.e0.p.d.m0.e.a.l0.w r28) {
            /*
                Method dump skipped, instructions count: 1155
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.l0.l.b.enhance(d0.e0.p.d.m0.e.a.l0.w):d0.e0.p.d.m0.e.a.l0.l$a");
        }

        public /* synthetic */ b(d0.e0.p.d.m0.c.g1.a aVar, c0 c0Var, Collection collection, boolean z2, g gVar, d0.e0.p.d.m0.e.a.a aVar2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(l.this, aVar, c0Var, collection, z2, gVar, aVar2, (i & 64) != 0 ? false : z3);
        }
    }
}
