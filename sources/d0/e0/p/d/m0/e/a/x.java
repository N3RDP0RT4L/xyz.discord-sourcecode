package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.e.b.k;
import d0.e0.p.d.m0.e.b.u;
import d0.e0.p.d.m0.k.f;
import d0.e0.p.d.m0.n.c0;
import java.util.List;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: JavaIncompatibilityRulesOverridabilityCondition.kt */
/* loaded from: classes3.dex */
public final class x implements f {
    public static final a a = new a(null);

    /* compiled from: JavaIncompatibilityRulesOverridabilityCondition.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final k a(d0.e0.p.d.m0.c.x xVar, c1 c1Var) {
            if (!u.forceSingleValueParameterBoxing(xVar)) {
                boolean z2 = true;
                boolean z3 = false;
                if (xVar.getValueParameters().size() == 1) {
                    m containingDeclaration = xVar.getContainingDeclaration();
                    e eVar = null;
                    e eVar2 = containingDeclaration instanceof e ? (e) containingDeclaration : null;
                    if (eVar2 != null) {
                        List<c1> valueParameters = xVar.getValueParameters();
                        d0.z.d.m.checkNotNullExpressionValue(valueParameters, "f.valueParameters");
                        h declarationDescriptor = ((c1) d0.t.u.single((List<? extends Object>) valueParameters)).getType().getConstructor().getDeclarationDescriptor();
                        if (declarationDescriptor instanceof e) {
                            eVar = (e) declarationDescriptor;
                        }
                        if (eVar != null) {
                            if (!d0.e0.p.d.m0.b.h.isPrimitiveClass(eVar2) || !d0.z.d.m.areEqual(d0.e0.p.d.m0.k.x.a.getFqNameSafe(eVar2), d0.e0.p.d.m0.k.x.a.getFqNameSafe(eVar))) {
                                z2 = false;
                            }
                            z3 = z2;
                        }
                    }
                }
                if (!z3) {
                    c0 type = c1Var.getType();
                    d0.z.d.m.checkNotNullExpressionValue(type, "valueParameterDescriptor.type");
                    return u.mapToJvmType(type);
                }
            }
            c0 type2 = c1Var.getType();
            d0.z.d.m.checkNotNullExpressionValue(type2, "valueParameterDescriptor.type");
            return u.mapToJvmType(d0.e0.p.d.m0.n.o1.a.makeNullable(type2));
        }

        public final boolean doesJavaOverrideHaveIncompatibleValueParameterKinds(d0.e0.p.d.m0.c.a aVar, d0.e0.p.d.m0.c.a aVar2) {
            d0.z.d.m.checkNotNullParameter(aVar, "superDescriptor");
            d0.z.d.m.checkNotNullParameter(aVar2, "subDescriptor");
            if ((aVar2 instanceof d0.e0.p.d.m0.e.a.h0.f) && (aVar instanceof d0.e0.p.d.m0.c.x)) {
                d0.e0.p.d.m0.e.a.h0.f fVar = (d0.e0.p.d.m0.e.a.h0.f) aVar2;
                fVar.getValueParameters().size();
                d0.e0.p.d.m0.c.x xVar = (d0.e0.p.d.m0.c.x) aVar;
                xVar.getValueParameters().size();
                List<c1> valueParameters = fVar.getOriginal().getValueParameters();
                d0.z.d.m.checkNotNullExpressionValue(valueParameters, "subDescriptor.original.valueParameters");
                List<c1> valueParameters2 = xVar.getOriginal().getValueParameters();
                d0.z.d.m.checkNotNullExpressionValue(valueParameters2, "superDescriptor.original.valueParameters");
                for (Pair pair : d0.t.u.zip(valueParameters, valueParameters2)) {
                    c1 c1Var = (c1) pair.component1();
                    c1 c1Var2 = (c1) pair.component2();
                    d0.z.d.m.checkNotNullExpressionValue(c1Var, "subParameter");
                    boolean z2 = a((d0.e0.p.d.m0.c.x) aVar2, c1Var) instanceof k.d;
                    d0.z.d.m.checkNotNullExpressionValue(c1Var2, "superParameter");
                    if (z2 != (a(xVar, c1Var2) instanceof k.d)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    @Override // d0.e0.p.d.m0.k.f
    public f.a getContract() {
        return f.a.CONFLICTS_ONLY;
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x003e, code lost:
        if (r1.getSameAsRenamedInJvmBuiltin(r4) == false) goto L39;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00b4, code lost:
        if (d0.z.d.m.areEqual(r1, d0.e0.p.d.m0.e.b.u.computeJvmDescriptor$default(r3, false, false, 2, null)) != false) goto L39;
     */
    @Override // d0.e0.p.d.m0.k.f
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public d0.e0.p.d.m0.k.f.b isOverridable(d0.e0.p.d.m0.c.a r9, d0.e0.p.d.m0.c.a r10, d0.e0.p.d.m0.c.e r11) {
        /*
            r8 = this;
            d0.e0.p.d.m0.k.f$b r0 = d0.e0.p.d.m0.k.f.b.INCOMPATIBLE
            java.lang.String r1 = "superDescriptor"
            d0.z.d.m.checkNotNullParameter(r9, r1)
            java.lang.String r1 = "subDescriptor"
            d0.z.d.m.checkNotNullParameter(r10, r1)
            boolean r1 = r9 instanceof d0.e0.p.d.m0.c.b
            r2 = 0
            if (r1 == 0) goto Lb6
            boolean r1 = r10 instanceof d0.e0.p.d.m0.c.x
            if (r1 == 0) goto Lb6
            boolean r1 = d0.e0.p.d.m0.b.h.isBuiltIn(r10)
            if (r1 == 0) goto L1d
            goto Lb6
        L1d:
            d0.e0.p.d.m0.e.a.h r1 = d0.e0.p.d.m0.e.a.h.m
            r3 = r10
            d0.e0.p.d.m0.c.x r3 = (d0.e0.p.d.m0.c.x) r3
            d0.e0.p.d.m0.g.e r4 = r3.getName()
            java.lang.String r5 = "subDescriptor.name"
            d0.z.d.m.checkNotNullExpressionValue(r4, r5)
            boolean r1 = r1.getSameAsBuiltinMethodWithErasedValueParameters(r4)
            if (r1 != 0) goto L42
            d0.e0.p.d.m0.e.a.g r1 = d0.e0.p.d.m0.e.a.g.m
            d0.e0.p.d.m0.g.e r4 = r3.getName()
            d0.z.d.m.checkNotNullExpressionValue(r4, r5)
            boolean r1 = r1.getSameAsRenamedInJvmBuiltin(r4)
            if (r1 != 0) goto L42
            goto Lb6
        L42:
            r1 = r9
            d0.e0.p.d.m0.c.b r1 = (d0.e0.p.d.m0.c.b) r1
            d0.e0.p.d.m0.c.b r1 = d0.e0.p.d.m0.e.a.d0.getOverriddenSpecialBuiltin(r1)
            boolean r4 = r3.isHiddenToOvercomeSignatureClash()
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)
            boolean r5 = r9 instanceof d0.e0.p.d.m0.c.x
            r6 = 0
            if (r5 == 0) goto L5a
            r7 = r9
            d0.e0.p.d.m0.c.x r7 = (d0.e0.p.d.m0.c.x) r7
            goto L5b
        L5a:
            r7 = r6
        L5b:
            if (r7 != 0) goto L5f
            r7 = r6
            goto L67
        L5f:
            boolean r7 = r7.isHiddenToOvercomeSignatureClash()
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
        L67:
            boolean r4 = d0.z.d.m.areEqual(r4, r7)
            r7 = 1
            r4 = r4 ^ r7
            if (r4 == 0) goto L79
            if (r1 == 0) goto L77
            boolean r4 = r3.isHiddenToOvercomeSignatureClash()
            if (r4 != 0) goto L79
        L77:
            r2 = 1
            goto Lb6
        L79:
            boolean r4 = r11 instanceof d0.e0.p.d.m0.e.a.h0.d
            if (r4 == 0) goto Lb6
            d0.e0.p.d.m0.c.x r4 = r3.getInitialSignatureDescriptor()
            if (r4 == 0) goto L84
            goto Lb6
        L84:
            if (r1 == 0) goto Lb6
            boolean r11 = d0.e0.p.d.m0.e.a.d0.hasRealKotlinSuperClassWithOverrideOf(r11, r1)
            if (r11 == 0) goto L8d
            goto Lb6
        L8d:
            boolean r11 = r1 instanceof d0.e0.p.d.m0.c.x
            if (r11 == 0) goto L77
            if (r5 == 0) goto L77
            d0.e0.p.d.m0.c.x r1 = (d0.e0.p.d.m0.c.x) r1
            d0.e0.p.d.m0.c.x r11 = d0.e0.p.d.m0.e.a.h.getOverriddenBuiltinFunctionWithErasedValueParametersInJava(r1)
            if (r11 == 0) goto L77
            r11 = 2
            java.lang.String r1 = d0.e0.p.d.m0.e.b.u.computeJvmDescriptor$default(r3, r2, r2, r11, r6)
            r3 = r9
            d0.e0.p.d.m0.c.x r3 = (d0.e0.p.d.m0.c.x) r3
            d0.e0.p.d.m0.c.x r3 = r3.getOriginal()
            java.lang.String r4 = "superDescriptor.original"
            d0.z.d.m.checkNotNullExpressionValue(r3, r4)
            java.lang.String r11 = d0.e0.p.d.m0.e.b.u.computeJvmDescriptor$default(r3, r2, r2, r11, r6)
            boolean r11 = d0.z.d.m.areEqual(r1, r11)
            if (r11 == 0) goto L77
        Lb6:
            if (r2 == 0) goto Lb9
            return r0
        Lb9:
            d0.e0.p.d.m0.e.a.x$a r11 = d0.e0.p.d.m0.e.a.x.a
            boolean r9 = r11.doesJavaOverrideHaveIncompatibleValueParameterKinds(r9, r10)
            if (r9 == 0) goto Lc2
            return r0
        Lc2:
            d0.e0.p.d.m0.k.f$b r9 = d0.e0.p.d.m0.k.f.b.UNKNOWN
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.x.isOverridable(d0.e0.p.d.m0.c.a, d0.e0.p.d.m0.c.a, d0.e0.p.d.m0.c.e):d0.e0.p.d.m0.k.f$b");
    }
}
