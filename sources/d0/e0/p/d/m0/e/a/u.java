package d0.e0.p.d.m0.e.a;

import b.d.b.a.a;
import d0.e0.p.d.m0.e.a.l0.h;
import d0.e0.p.d.m0.e.a.l0.i;
import d0.z.d.m;
import java.util.Collection;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: AnnotationQualifiersFqNames.kt */
/* loaded from: classes3.dex */
public final class u {
    public final i a;

    /* renamed from: b  reason: collision with root package name */
    public final Collection<a> f3330b;
    public final boolean c;

    /* JADX WARN: Multi-variable type inference failed */
    public u(i iVar, Collection<? extends a> collection, boolean z2) {
        m.checkNotNullParameter(iVar, "nullabilityQualifier");
        m.checkNotNullParameter(collection, "qualifierApplicabilityTypes");
        this.a = iVar;
        this.f3330b = collection;
        this.c = z2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ u copy$default(u uVar, i iVar, Collection collection, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            iVar = uVar.a;
        }
        if ((i & 2) != 0) {
            collection = uVar.f3330b;
        }
        if ((i & 4) != 0) {
            z2 = uVar.c;
        }
        return uVar.copy(iVar, collection, z2);
    }

    public final u copy(i iVar, Collection<? extends a> collection, boolean z2) {
        m.checkNotNullParameter(iVar, "nullabilityQualifier");
        m.checkNotNullParameter(collection, "qualifierApplicabilityTypes");
        return new u(iVar, collection, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u)) {
            return false;
        }
        u uVar = (u) obj;
        return m.areEqual(this.a, uVar.a) && m.areEqual(this.f3330b, uVar.f3330b) && this.c == uVar.c;
    }

    public final boolean getAffectsTypeParameterBasedTypes() {
        return this.c;
    }

    public final boolean getMakesTypeParameterNotNull() {
        return this.a.getQualifier() == h.NOT_NULL && this.c;
    }

    public final i getNullabilityQualifier() {
        return this.a;
    }

    public final Collection<a> getQualifierApplicabilityTypes() {
        return this.f3330b;
    }

    public int hashCode() {
        int hashCode = (this.f3330b.hashCode() + (this.a.hashCode() * 31)) * 31;
        boolean z2 = this.c;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("JavaDefaultQualifiers(nullabilityQualifier=");
        R.append(this.a);
        R.append(", qualifierApplicabilityTypes=");
        R.append(this.f3330b);
        R.append(", affectsTypeParameterBasedTypes=");
        R.append(this.c);
        R.append(')');
        return R.toString();
    }

    public /* synthetic */ u(i iVar, Collection collection, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(iVar, collection, (i & 4) != 0 ? iVar.getQualifier() == h.NOT_NULL : z2);
    }
}
