package d0.e0.p.d.m0.e.a.k0;

import d0.e0.p.d.m0.g.b;
import java.util.Collection;
/* compiled from: javaElements.kt */
/* loaded from: classes3.dex */
public interface d extends l {
    a findAnnotation(b bVar);

    Collection<a> getAnnotations();

    boolean isDeprecatedInJavaDoc();
}
