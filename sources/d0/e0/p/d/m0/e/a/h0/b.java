package d0.e0.p.d.m0.e.a.h0;

import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.n.c0;
import java.util.List;
import kotlin.Pair;
/* compiled from: JavaCallableMemberDescriptor.java */
/* loaded from: classes3.dex */
public interface b extends d0.e0.p.d.m0.c.b {
    b enhance(c0 c0Var, List<l> list, c0 c0Var2, Pair<a.AbstractC0290a<?>, ?> pair);
}
