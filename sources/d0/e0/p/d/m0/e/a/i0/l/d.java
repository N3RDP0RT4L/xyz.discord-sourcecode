package d0.e0.p.d.m0.e.a.i0.l;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.d.b.b;
import d0.e0.p.d.m0.e.a.i0.g;
import d0.e0.p.d.m0.e.a.k0.u;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.k.a0.k;
import d0.e0.p.d.m0.m.j;
import d0.e0.p.d.m0.m.n;
import d0.t.n0;
import d0.t.r;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KProperty;
/* compiled from: JvmPackageScope.kt */
/* loaded from: classes3.dex */
public final class d implements i {

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ KProperty<Object>[] f3298b = {a0.property1(new y(a0.getOrCreateKotlinClass(d.class), "kotlinScopes", "getKotlinScopes()[Lorg/jetbrains/kotlin/resolve/scopes/MemberScope;"))};
    public final g c;
    public final i d;
    public final j e;
    public final j f;

    /* compiled from: JvmPackageScope.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<i[]> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final i[] invoke() {
            Collection<p> values = d.this.d.getBinaryClasses$descriptors_jvm().values();
            d dVar = d.this;
            ArrayList arrayList = new ArrayList();
            for (p pVar : values) {
                i createKotlinPackagePartScope = dVar.c.getComponents().getDeserializedDescriptorResolver().createKotlinPackagePartScope(dVar.d, pVar);
                if (createKotlinPackagePartScope != null) {
                    arrayList.add(createKotlinPackagePartScope);
                }
            }
            Object[] array = d0.e0.p.d.m0.o.n.a.listOfNonEmptyScopes(arrayList).toArray(new i[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            return (i[]) array;
        }
    }

    public d(g gVar, u uVar, i iVar) {
        m.checkNotNullParameter(gVar, "c");
        m.checkNotNullParameter(uVar, "jPackage");
        m.checkNotNullParameter(iVar, "packageFragment");
        this.c = gVar;
        this.d = iVar;
        this.e = new j(gVar, uVar, iVar);
        this.f = gVar.getStorageManager().createLazyValue(new a());
    }

    public final i[] a() {
        return (i[]) n.getValue(this.f, this, f3298b[0]);
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getClassifierNames() {
        Set<e> flatMapClassifierNamesOrNull = k.flatMapClassifierNamesOrNull(d0.t.k.asIterable(a()));
        if (flatMapClassifierNamesOrNull == null) {
            return null;
        }
        flatMapClassifierNamesOrNull.addAll(getJavaScope$descriptors_jvm().getClassifierNames());
        return flatMapClassifierNamesOrNull;
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public h getContributedClassifier(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        recordLookup(eVar, bVar);
        d0.e0.p.d.m0.c.e contributedClassifier = this.e.getContributedClassifier(eVar, bVar);
        if (contributedClassifier != null) {
            return contributedClassifier;
        }
        i[] a2 = a();
        h hVar = null;
        int i = 0;
        int length = a2.length;
        while (i < length) {
            i iVar = a2[i];
            i++;
            h contributedClassifier2 = iVar.getContributedClassifier(eVar, bVar);
            if (contributedClassifier2 != null) {
                if (!(contributedClassifier2 instanceof d0.e0.p.d.m0.c.i) || !((d0.e0.p.d.m0.c.i) contributedClassifier2).isExpect()) {
                    return contributedClassifier2;
                }
                if (hVar == null) {
                    hVar = contributedClassifier2;
                }
            }
        }
        return hVar;
    }

    @Override // d0.e0.p.d.m0.k.a0.l
    public Collection<d0.e0.p.d.m0.c.m> getContributedDescriptors(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        m.checkNotNullParameter(function1, "nameFilter");
        j jVar = this.e;
        i[] a2 = a();
        Collection<d0.e0.p.d.m0.c.m> contributedDescriptors = jVar.getContributedDescriptors(dVar, function1);
        int length = a2.length;
        int i = 0;
        while (i < length) {
            i iVar = a2[i];
            i++;
            contributedDescriptors = d0.e0.p.d.m0.o.n.a.concat(contributedDescriptors, iVar.getContributedDescriptors(dVar, function1));
        }
        return contributedDescriptors == null ? n0.emptySet() : contributedDescriptors;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<t0> getContributedFunctions(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        recordLookup(eVar, bVar);
        j jVar = this.e;
        i[] a2 = a();
        Collection<? extends t0> contributedFunctions = jVar.getContributedFunctions(eVar, bVar);
        int length = a2.length;
        int i = 0;
        Collection collection = contributedFunctions;
        while (i < length) {
            i iVar = a2[i];
            i++;
            collection = d0.e0.p.d.m0.o.n.a.concat(collection, iVar.getContributedFunctions(eVar, bVar));
        }
        return collection == null ? n0.emptySet() : collection;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Collection<d0.e0.p.d.m0.c.n0> getContributedVariables(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        recordLookup(eVar, bVar);
        j jVar = this.e;
        i[] a2 = a();
        Collection<? extends d0.e0.p.d.m0.c.n0> contributedVariables = jVar.getContributedVariables(eVar, bVar);
        int length = a2.length;
        int i = 0;
        Collection collection = contributedVariables;
        while (i < length) {
            i iVar = a2[i];
            i++;
            collection = d0.e0.p.d.m0.o.n.a.concat(collection, iVar.getContributedVariables(eVar, bVar));
        }
        return collection == null ? n0.emptySet() : collection;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getFunctionNames() {
        i[] a2 = a();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (i iVar : a2) {
            r.addAll(linkedHashSet, iVar.getFunctionNames());
        }
        linkedHashSet.addAll(getJavaScope$descriptors_jvm().getFunctionNames());
        return linkedHashSet;
    }

    public final j getJavaScope$descriptors_jvm() {
        return this.e;
    }

    @Override // d0.e0.p.d.m0.k.a0.i
    public Set<e> getVariableNames() {
        i[] a2 = a();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (i iVar : a2) {
            r.addAll(linkedHashSet, iVar.getVariableNames());
        }
        linkedHashSet.addAll(getJavaScope$descriptors_jvm().getVariableNames());
        return linkedHashSet;
    }

    public void recordLookup(e eVar, b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        d0.e0.p.d.m0.d.a.record(this.c.getComponents().getLookupTracker(), bVar, this.d, eVar);
    }
}
