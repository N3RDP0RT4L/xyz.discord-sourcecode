package d0.e0.p.d.m0.e.a.k0;

import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.e;
import java.util.Collection;
/* compiled from: javaElements.kt */
/* loaded from: classes3.dex */
public interface g extends i, s, z {
    Collection<k> getConstructors();

    Collection<n> getFields();

    b getFqName();

    Collection<e> getInnerClassNames();

    c0 getLightClassOriginKind();

    Collection<r> getMethods();

    g getOuterClass();

    Collection<j> getPermittedTypes();

    Collection<w> getRecordComponents();

    Collection<j> getSupertypes();

    boolean hasDefaultConstructor();

    boolean isAnnotationType();

    boolean isEnum();

    boolean isInterface();

    boolean isRecord();

    boolean isSealed();
}
