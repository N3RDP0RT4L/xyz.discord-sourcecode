package d0.e0.p.d.m0.e.a.i0.l;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.e.a.i0.l.b;
import d0.e0.p.d.m0.e.a.k0.c0;
import d0.e0.p.d.m0.e.a.k0.g;
import d0.e0.p.d.m0.e.a.k0.u;
import d0.e0.p.d.m0.e.a.s;
import d0.e0.p.d.m0.e.b.b0.a;
import d0.e0.p.d.m0.e.b.n;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.m.i;
import d0.e0.p.d.m0.m.k;
import d0.t.n0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: LazyJavaPackageScope.kt */
/* loaded from: classes3.dex */
public final class j extends r {
    public final u n;
    public final i o;
    public final k<Set<String>> p;
    public final i<a, e> q;

    /* compiled from: LazyJavaPackageScope.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final d0.e0.p.d.m0.g.e a;

        /* renamed from: b  reason: collision with root package name */
        public final g f3306b;

        public a(d0.e0.p.d.m0.g.e eVar, g gVar) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            this.a = eVar;
            this.f3306b = gVar;
        }

        public boolean equals(Object obj) {
            return (obj instanceof a) && m.areEqual(this.a, ((a) obj).a);
        }

        public final g getJavaClass() {
            return this.f3306b;
        }

        public final d0.e0.p.d.m0.g.e getName() {
            return this.a;
        }

        public int hashCode() {
            return this.a.hashCode();
        }
    }

    /* compiled from: LazyJavaPackageScope.kt */
    /* loaded from: classes3.dex */
    public static abstract class b {

        /* compiled from: LazyJavaPackageScope.kt */
        /* loaded from: classes3.dex */
        public static final class a extends b {
            public final e a;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(e eVar) {
                super(null);
                m.checkNotNullParameter(eVar, "descriptor");
                this.a = eVar;
            }

            public final e getDescriptor() {
                return this.a;
            }
        }

        /* compiled from: LazyJavaPackageScope.kt */
        /* renamed from: d0.e0.p.d.m0.e.a.i0.l.j$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0304b extends b {
            public static final C0304b a = new C0304b();

            public C0304b() {
                super(null);
            }
        }

        /* compiled from: LazyJavaPackageScope.kt */
        /* loaded from: classes3.dex */
        public static final class c extends b {
            public static final c a = new c();

            public c() {
                super(null);
            }
        }

        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: LazyJavaPackageScope.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function1<a, e> {
        public final /* synthetic */ d0.e0.p.d.m0.e.a.i0.g $c;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(d0.e0.p.d.m0.e.a.i0.g gVar) {
            super(1);
            this.$c = gVar;
        }

        public final e invoke(a aVar) {
            n.a aVar2;
            byte[] bArr;
            m.checkNotNullParameter(aVar, "request");
            d0.e0.p.d.m0.g.a aVar3 = new d0.e0.p.d.m0.g.a(j.this.o.getFqName(), aVar.getName());
            if (aVar.getJavaClass() != null) {
                aVar2 = this.$c.getComponents().getKotlinClassFinder().findKotlinClassOrContent(aVar.getJavaClass());
            } else {
                aVar2 = this.$c.getComponents().getKotlinClassFinder().findKotlinClassOrContent(aVar3);
            }
            p kotlinJvmBinaryClass = aVar2 == null ? null : aVar2.toKotlinJvmBinaryClass();
            d0.e0.p.d.m0.g.a classId = kotlinJvmBinaryClass == null ? null : kotlinJvmBinaryClass.getClassId();
            if (classId != null && (classId.isNestedClass() || classId.isLocal())) {
                return null;
            }
            b access$resolveKotlinBinaryClass = j.access$resolveKotlinBinaryClass(j.this, kotlinJvmBinaryClass);
            if (access$resolveKotlinBinaryClass instanceof b.a) {
                return ((b.a) access$resolveKotlinBinaryClass).getDescriptor();
            }
            if (access$resolveKotlinBinaryClass instanceof b.c) {
                return null;
            }
            if (access$resolveKotlinBinaryClass instanceof b.C0304b) {
                g javaClass = aVar.getJavaClass();
                if (javaClass == null) {
                    s finder = this.$c.getComponents().getFinder();
                    if (aVar2 != null) {
                        if (!(aVar2 instanceof n.a.C0316a)) {
                            aVar2 = null;
                        }
                        n.a.C0316a aVar4 = (n.a.C0316a) aVar2;
                        if (aVar4 != null) {
                            bArr = aVar4.getContent();
                            javaClass = finder.findClass(new s.a(aVar3, bArr, null, 4, null));
                        }
                    }
                    bArr = null;
                    javaClass = finder.findClass(new s.a(aVar3, bArr, null, 4, null));
                }
                g gVar = javaClass;
                if ((gVar == null ? null : gVar.getLightClassOriginKind()) != c0.BINARY) {
                    d0.e0.p.d.m0.g.b fqName = gVar == null ? null : gVar.getFqName();
                    if (fqName == null || fqName.isRoot() || !m.areEqual(fqName.parent(), j.this.o.getFqName())) {
                        return null;
                    }
                    f fVar = new f(this.$c, j.this.o, gVar, null, 8, null);
                    this.$c.getComponents().getJavaClassesTracker().reportClass(fVar);
                    return fVar;
                }
                throw new IllegalStateException("Couldn't find kotlin binary class for light class created by kotlin binary file\nJavaClass: " + gVar + "\nClassId: " + aVar3 + "\nfindKotlinClass(JavaClass) = " + d0.e0.p.d.m0.e.b.o.findKotlinClass(this.$c.getComponents().getKotlinClassFinder(), gVar) + "\nfindKotlinClass(ClassId) = " + d0.e0.p.d.m0.e.b.o.findKotlinClass(this.$c.getComponents().getKotlinClassFinder(), aVar3) + '\n');
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    /* compiled from: LazyJavaPackageScope.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function0<Set<? extends String>> {
        public final /* synthetic */ d0.e0.p.d.m0.e.a.i0.g $c;
        public final /* synthetic */ j this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(d0.e0.p.d.m0.e.a.i0.g gVar, j jVar) {
            super(0);
            this.$c = gVar;
            this.this$0 = jVar;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Set<? extends String> invoke() {
            return this.$c.getComponents().getFinder().knownClassNamesInPackage(this.this$0.o.getFqName());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public j(d0.e0.p.d.m0.e.a.i0.g gVar, u uVar, i iVar) {
        super(gVar);
        m.checkNotNullParameter(gVar, "c");
        m.checkNotNullParameter(uVar, "jPackage");
        m.checkNotNullParameter(iVar, "ownerDescriptor");
        this.n = uVar;
        this.o = iVar;
        this.p = gVar.getStorageManager().createNullableLazyValue(new d(gVar, this));
        this.q = gVar.getStorageManager().createMemoizedFunctionWithNullableValues(new c(gVar));
    }

    public static final b access$resolveKotlinBinaryClass(j jVar, p pVar) {
        Objects.requireNonNull(jVar);
        if (pVar == null) {
            return b.C0304b.a;
        }
        if (pVar.getClassHeader().getKind() != a.EnumC0312a.CLASS) {
            return b.c.a;
        }
        e resolveClass = jVar.c.getComponents().getDeserializedDescriptorResolver().resolveClass(pVar);
        return resolveClass != null ? new b.a(resolveClass) : b.C0304b.a;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set<d0.e0.p.d.m0.g.e> a(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        if (!dVar.acceptsKinds(d0.e0.p.d.m0.k.a0.d.a.getNON_SINGLETON_CLASSIFIERS_MASK())) {
            return n0.emptySet();
        }
        Set<String> invoke = this.p.invoke();
        if (invoke != null) {
            HashSet hashSet = new HashSet();
            for (String str : invoke) {
                hashSet.add(d0.e0.p.d.m0.g.e.identifier(str));
            }
            return hashSet;
        }
        u uVar = this.n;
        if (function1 == null) {
            function1 = d0.e0.p.d.m0.p.d.alwaysTrue();
        }
        Collection<g> classes = uVar.getClasses(function1);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (g gVar : classes) {
            d0.e0.p.d.m0.g.e name = gVar.getLightClassOriginKind() == c0.SOURCE ? null : gVar.getName();
            if (name != null) {
                linkedHashSet.add(name);
            }
        }
        return linkedHashSet;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set<d0.e0.p.d.m0.g.e> b(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        return n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public d0.e0.p.d.m0.e.a.i0.l.b d() {
        return b.a.a;
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public void f(Collection<t0> collection, d0.e0.p.d.m0.g.e eVar) {
        m.checkNotNullParameter(collection, "result");
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
    }

    public final e findClassifierByJavaClass$descriptors_jvm(g gVar) {
        m.checkNotNullParameter(gVar, "javaClass");
        return o(gVar.getName(), gVar);
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x005e A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0031 A[SYNTHETIC] */
    @Override // d0.e0.p.d.m0.e.a.i0.l.k, d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.util.Collection<d0.e0.p.d.m0.c.m> getContributedDescriptors(d0.e0.p.d.m0.k.a0.d r5, kotlin.jvm.functions.Function1<? super d0.e0.p.d.m0.g.e, java.lang.Boolean> r6) {
        /*
            r4 = this;
            java.lang.String r0 = "kindFilter"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            java.lang.String r0 = "nameFilter"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            d0.e0.p.d.m0.k.a0.d$a r0 = d0.e0.p.d.m0.k.a0.d.a
            int r1 = r0.getCLASSIFIERS_MASK()
            int r0 = r0.getNON_SINGLETON_CLASSIFIERS_MASK()
            r0 = r0 | r1
            boolean r5 = r5.acceptsKinds(r0)
            if (r5 != 0) goto L20
            java.util.List r5 = d0.t.n.emptyList()
            goto L63
        L20:
            d0.e0.p.d.m0.m.j<java.util.Collection<d0.e0.p.d.m0.c.m>> r5 = r4.e
            java.lang.Object r5 = r5.invoke()
            java.lang.Iterable r5 = (java.lang.Iterable) r5
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r5 = r5.iterator()
        L31:
            boolean r1 = r5.hasNext()
            if (r1 == 0) goto L62
            java.lang.Object r1 = r5.next()
            r2 = r1
            d0.e0.p.d.m0.c.m r2 = (d0.e0.p.d.m0.c.m) r2
            boolean r3 = r2 instanceof d0.e0.p.d.m0.c.e
            if (r3 == 0) goto L5b
            d0.e0.p.d.m0.c.e r2 = (d0.e0.p.d.m0.c.e) r2
            d0.e0.p.d.m0.g.e r2 = r2.getName()
            java.lang.String r3 = "it.name"
            d0.z.d.m.checkNotNullExpressionValue(r2, r3)
            java.lang.Object r2 = r6.invoke(r2)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L5b
            r2 = 1
            goto L5c
        L5b:
            r2 = 0
        L5c:
            if (r2 == 0) goto L31
            r0.add(r1)
            goto L31
        L62:
            r5 = r0
        L63:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.a.i0.l.j.getContributedDescriptors(d0.e0.p.d.m0.k.a0.d, kotlin.jvm.functions.Function1):java.util.Collection");
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k, d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.i
    public Collection<d0.e0.p.d.m0.c.n0> getContributedVariables(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return d0.t.n.emptyList();
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public Set<d0.e0.p.d.m0.g.e> h(d0.e0.p.d.m0.k.a0.d dVar, Function1<? super d0.e0.p.d.m0.g.e, Boolean> function1) {
        m.checkNotNullParameter(dVar, "kindFilter");
        return n0.emptySet();
    }

    @Override // d0.e0.p.d.m0.e.a.i0.l.k
    public d0.e0.p.d.m0.c.m j() {
        return this.o;
    }

    public final e o(d0.e0.p.d.m0.g.e eVar, g gVar) {
        if (!d0.e0.p.d.m0.g.g.isSafeIdentifier(eVar)) {
            return null;
        }
        Set<String> invoke = this.p.invoke();
        if (gVar != null || invoke == null || invoke.contains(eVar.asString())) {
            return this.q.invoke(new a(eVar, gVar));
        }
        return null;
    }

    @Override // d0.e0.p.d.m0.k.a0.j, d0.e0.p.d.m0.k.a0.l
    public e getContributedClassifier(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.d.b.b bVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(bVar, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        return o(eVar, null);
    }
}
