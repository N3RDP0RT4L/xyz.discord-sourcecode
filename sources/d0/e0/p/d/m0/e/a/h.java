package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.e.a.e0;
import d0.e0.p.d.m0.g.e;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.jvm.functions.Function1;
/* compiled from: specialBuiltinMembers.kt */
/* loaded from: classes3.dex */
public final class h extends e0 {
    public static final h m = new h();

    /* compiled from: specialBuiltinMembers.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<d0.e0.p.d.m0.c.b, Boolean> {
        public a() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(d0.e0.p.d.m0.c.b bVar) {
            return Boolean.valueOf(invoke2(bVar));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(d0.e0.p.d.m0.c.b bVar) {
            m.checkNotNullParameter(bVar, "it");
            return h.access$getHasErasedValueParametersInJava(h.this, bVar);
        }
    }

    /* compiled from: specialBuiltinMembers.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<d0.e0.p.d.m0.c.b, Boolean> {
        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(d0.e0.p.d.m0.c.b bVar) {
            return Boolean.valueOf(invoke2(bVar));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(d0.e0.p.d.m0.c.b bVar) {
            m.checkNotNullParameter(bVar, "it");
            return (bVar instanceof x) && h.access$getHasErasedValueParametersInJava(h.this, bVar);
        }
    }

    public static final boolean access$getHasErasedValueParametersInJava(h hVar, d0.e0.p.d.m0.c.b bVar) {
        Objects.requireNonNull(hVar);
        return u.contains(e0.a.getERASED_VALUE_PARAMETERS_SIGNATURES(), d0.e0.p.d.m0.e.b.u.computeJvmSignature(bVar));
    }

    public static final x getOverriddenBuiltinFunctionWithErasedValueParametersInJava(x xVar) {
        m.checkNotNullParameter(xVar, "functionDescriptor");
        h hVar = m;
        e name = xVar.getName();
        m.checkNotNullExpressionValue(name, "functionDescriptor.name");
        if (!hVar.getSameAsBuiltinMethodWithErasedValueParameters(name)) {
            return null;
        }
        return (x) d0.e0.p.d.m0.k.x.a.firstOverridden$default(xVar, false, new a(), 1, null);
    }

    public static final e0.b getSpecialSignatureInfo(d0.e0.p.d.m0.c.b bVar) {
        m.checkNotNullParameter(bVar, "<this>");
        e0.a aVar = e0.a;
        if (!aVar.getERASED_VALUE_PARAMETERS_SHORT_NAMES().contains(bVar.getName())) {
            return null;
        }
        d0.e0.p.d.m0.c.b firstOverridden$default = d0.e0.p.d.m0.k.x.a.firstOverridden$default(bVar, false, new b(), 1, null);
        String computeJvmSignature = firstOverridden$default == null ? null : d0.e0.p.d.m0.e.b.u.computeJvmSignature(firstOverridden$default);
        if (computeJvmSignature == null) {
            return null;
        }
        return aVar.getSpecialSignatureInfo(computeJvmSignature);
    }

    public final boolean getSameAsBuiltinMethodWithErasedValueParameters(e eVar) {
        m.checkNotNullParameter(eVar, "<this>");
        return e0.a.getERASED_VALUE_PARAMETERS_SHORT_NAMES().contains(eVar);
    }
}
