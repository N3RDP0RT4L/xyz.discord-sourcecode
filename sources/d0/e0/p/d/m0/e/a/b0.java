package d0.e0.p.d.m0.e.a;

import d0.e0.p.d.m0.g.b;
import d0.t.n;
import d0.t.o0;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
/* compiled from: JvmAnnotationNames.kt */
/* loaded from: classes3.dex */
public final class b0 {
    public static final b a;

    /* renamed from: b  reason: collision with root package name */
    public static final b f3280b;
    public static final b c;
    public static final List<b> d;
    public static final b e;
    public static final List<b> g;
    public static final b h;
    public static final b i;
    public static final b j;
    public static final b k;
    public static final b f = new b("javax.annotation.CheckForNull");
    public static final List<b> l = n.listOf((Object[]) new b[]{a0.k, a0.l});
    public static final List<b> m = n.listOf((Object[]) new b[]{a0.j, a0.m});

    static {
        b bVar = new b("org.jspecify.annotations.Nullable");
        a = bVar;
        b bVar2 = new b("org.jspecify.annotations.NullnessUnspecified");
        f3280b = bVar2;
        b bVar3 = new b("org.jspecify.annotations.DefaultNonNull");
        c = bVar3;
        List<b> listOf = n.listOf((Object[]) new b[]{a0.i, new b("androidx.annotation.Nullable"), new b("androidx.annotation.Nullable"), new b("android.annotation.Nullable"), new b("com.android.annotations.Nullable"), new b("org.eclipse.jdt.annotation.Nullable"), new b("org.checkerframework.checker.nullness.qual.Nullable"), new b("javax.annotation.Nullable"), new b("javax.annotation.CheckForNull"), new b("edu.umd.cs.findbugs.annotations.CheckForNull"), new b("edu.umd.cs.findbugs.annotations.Nullable"), new b("edu.umd.cs.findbugs.annotations.PossiblyNull"), new b("io.reactivex.annotations.Nullable")});
        d = listOf;
        b bVar4 = new b("javax.annotation.Nonnull");
        e = bVar4;
        List<b> listOf2 = n.listOf((Object[]) new b[]{a0.h, new b("edu.umd.cs.findbugs.annotations.NonNull"), new b("androidx.annotation.NonNull"), new b("androidx.annotation.NonNull"), new b("android.annotation.NonNull"), new b("com.android.annotations.NonNull"), new b("org.eclipse.jdt.annotation.NonNull"), new b("org.checkerframework.checker.nullness.qual.NonNull"), new b("lombok.NonNull"), new b("io.reactivex.annotations.NonNull")});
        g = listOf2;
        b bVar5 = new b("org.checkerframework.checker.nullness.compatqual.NullableDecl");
        h = bVar5;
        b bVar6 = new b("org.checkerframework.checker.nullness.compatqual.NonNullDecl");
        i = bVar6;
        b bVar7 = new b("androidx.annotation.RecentlyNullable");
        j = bVar7;
        b bVar8 = new b("androidx.annotation.RecentlyNonNull");
        k = bVar8;
        o0.plus(o0.plus(o0.plus(o0.plus(o0.plus(o0.plus(o0.plus(o0.plus(o0.plus(o0.plus((Set) new LinkedHashSet(), (Iterable) listOf), bVar4), (Iterable) listOf2), bVar5), bVar6), bVar7), bVar8), bVar), bVar2), bVar3);
    }

    public static final b getANDROIDX_RECENTLY_NON_NULL_ANNOTATION() {
        return k;
    }

    public static final b getANDROIDX_RECENTLY_NULLABLE_ANNOTATION() {
        return j;
    }

    public static final b getCOMPATQUAL_NONNULL_ANNOTATION() {
        return i;
    }

    public static final b getCOMPATQUAL_NULLABLE_ANNOTATION() {
        return h;
    }

    public static final b getJAVAX_CHECKFORNULL_ANNOTATION() {
        return f;
    }

    public static final b getJAVAX_NONNULL_ANNOTATION() {
        return e;
    }

    public static final b getJSPECIFY_DEFAULT_NOT_NULL() {
        return c;
    }

    public static final b getJSPECIFY_NULLABLE() {
        return a;
    }

    public static final b getJSPECIFY_NULLNESS_UNKNOWN() {
        return f3280b;
    }

    public static final List<b> getMUTABLE_ANNOTATIONS() {
        return m;
    }

    public static final List<b> getNOT_NULL_ANNOTATIONS() {
        return g;
    }

    public static final List<b> getNULLABLE_ANNOTATIONS() {
        return d;
    }

    public static final List<b> getREAD_ONLY_ANNOTATIONS() {
        return l;
    }
}
