package d0.e0.p.d.m0.e.b;

import androidx.core.app.FrameMetricsAggregator;
import androidx.core.view.PointerIconCompat;
import androidx.media.AudioAttributesCompat;
import d0.e0.p.d.m0.n.j1;
import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: TypeMappingMode.kt */
/* loaded from: classes3.dex */
public final class z {
    public static final z a;

    /* renamed from: b  reason: collision with root package name */
    public static final z f3355b;
    public static final z c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final z i;
    public final boolean j;
    public final z k;
    public final z l;
    public final boolean m;

    /* compiled from: TypeMappingMode.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new a(null);
        z zVar = new z(false, false, false, false, false, null, false, null, null, false, AudioAttributesCompat.FLAG_ALL, null);
        a = zVar;
        z zVar2 = new z(false, false, false, false, false, null, false, null, null, true, FrameMetricsAggregator.EVERY_DURATION, null);
        f3355b = zVar2;
        new z(false, true, false, false, false, null, false, null, null, false, PointerIconCompat.TYPE_GRABBING, null);
        c = new z(false, false, false, false, false, zVar, false, null, null, false, 988, null);
        new z(false, false, false, false, false, zVar2, false, null, null, true, 476, null);
        new z(false, true, false, false, false, zVar, false, null, null, false, 988, null);
        new z(false, false, false, true, false, zVar, false, null, null, false, 983, null);
        new z(false, false, false, true, false, zVar, false, null, null, false, 919, null);
        new z(false, false, true, false, false, zVar, false, null, null, false, 984, null);
    }

    public z(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, z zVar, boolean z7, z zVar2, z zVar3, boolean z8) {
        this.d = z2;
        this.e = z3;
        this.f = z4;
        this.g = z5;
        this.h = z6;
        this.i = zVar;
        this.j = z7;
        this.k = zVar2;
        this.l = zVar3;
        this.m = z8;
    }

    public final boolean getKotlinCollectionsToJavaCollections() {
        return this.j;
    }

    public final boolean getMapTypeAliases() {
        return this.m;
    }

    public final boolean getNeedInlineClassWrapping() {
        return this.e;
    }

    public final boolean getNeedPrimitiveBoxing() {
        return this.d;
    }

    public final boolean isForAnnotationParameter() {
        return this.f;
    }

    public final z toGenericArgumentMode(j1 j1Var, boolean z2) {
        m.checkNotNullParameter(j1Var, "effectiveVariance");
        if (!z2 || !this.f) {
            int ordinal = j1Var.ordinal();
            if (ordinal == 0) {
                z zVar = this.l;
                if (zVar != null) {
                    return zVar;
                }
            } else if (ordinal != 1) {
                z zVar2 = this.i;
                if (zVar2 != null) {
                    return zVar2;
                }
            } else {
                z zVar3 = this.k;
                if (zVar3 != null) {
                    return zVar3;
                }
            }
        }
        return this;
    }

    public final z wrapInlineClassesMode() {
        return new z(this.d, true, this.f, this.g, this.h, this.i, this.j, this.k, this.l, false, 512, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ z(boolean r12, boolean r13, boolean r14, boolean r15, boolean r16, d0.e0.p.d.m0.e.b.z r17, boolean r18, d0.e0.p.d.m0.e.b.z r19, d0.e0.p.d.m0.e.b.z r20, boolean r21, int r22, kotlin.jvm.internal.DefaultConstructorMarker r23) {
        /*
            r11 = this;
            r0 = r22
            r1 = r0 & 1
            r2 = 1
            if (r1 == 0) goto L9
            r1 = 1
            goto La
        L9:
            r1 = r12
        La:
            r3 = r0 & 2
            if (r3 == 0) goto L10
            r3 = 1
            goto L11
        L10:
            r3 = r13
        L11:
            r4 = r0 & 4
            r5 = 0
            if (r4 == 0) goto L18
            r4 = 0
            goto L19
        L18:
            r4 = r14
        L19:
            r6 = r0 & 8
            if (r6 == 0) goto L1f
            r6 = 0
            goto L20
        L1f:
            r6 = r15
        L20:
            r7 = r0 & 16
            if (r7 == 0) goto L26
            r7 = 0
            goto L28
        L26:
            r7 = r16
        L28:
            r8 = r0 & 32
            if (r8 == 0) goto L2e
            r8 = 0
            goto L30
        L2e:
            r8 = r17
        L30:
            r9 = r0 & 64
            if (r9 == 0) goto L35
            goto L37
        L35:
            r2 = r18
        L37:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L3d
            r9 = r8
            goto L3f
        L3d:
            r9 = r19
        L3f:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L45
            r10 = r8
            goto L47
        L45:
            r10 = r20
        L47:
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 == 0) goto L4c
            goto L4e
        L4c:
            r5 = r21
        L4e:
            r12 = r11
            r13 = r1
            r14 = r3
            r15 = r4
            r16 = r6
            r17 = r7
            r18 = r8
            r19 = r2
            r20 = r9
            r21 = r10
            r22 = r5
            r12.<init>(r13, r14, r15, r16, r17, r18, r19, r20, r21, r22)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.b.z.<init>(boolean, boolean, boolean, boolean, boolean, d0.e0.p.d.m0.e.b.z, boolean, d0.e0.p.d.m0.e.b.z, d0.e0.p.d.m0.e.b.z, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
