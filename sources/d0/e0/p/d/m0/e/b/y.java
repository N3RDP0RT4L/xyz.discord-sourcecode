package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.e.b.x;
import d0.e0.p.d.m0.n.c0;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: methodSignatureMapping.kt */
/* loaded from: classes3.dex */
public final class y implements x<k> {
    public static final y a = new y();

    @Override // d0.e0.p.d.m0.e.b.x
    public c0 commonSupertype(Collection<? extends c0> collection) {
        m.checkNotNullParameter(collection, "types");
        throw new AssertionError(m.stringPlus("There should be no intersection type in existing descriptors, but found: ", u.joinToString$default(collection, null, null, null, 0, null, null, 63, null)));
    }

    @Override // d0.e0.p.d.m0.e.b.x
    public String getPredefinedFullInternalNameForClass(e eVar) {
        return x.a.getPredefinedFullInternalNameForClass(this, eVar);
    }

    @Override // d0.e0.p.d.m0.e.b.x
    public String getPredefinedInternalNameForClass(e eVar) {
        m.checkNotNullParameter(eVar, "classDescriptor");
        return null;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // d0.e0.p.d.m0.e.b.x
    public k getPredefinedTypeForClass(e eVar) {
        m.checkNotNullParameter(eVar, "classDescriptor");
        return null;
    }

    @Override // d0.e0.p.d.m0.e.b.x
    public c0 preprocessType(c0 c0Var) {
        return x.a.preprocessType(this, c0Var);
    }

    @Override // d0.e0.p.d.m0.e.b.x
    public void processErrorType(c0 c0Var, e eVar) {
        m.checkNotNullParameter(c0Var, "kotlinType");
        m.checkNotNullParameter(eVar, "descriptor");
    }

    @Override // d0.e0.p.d.m0.e.b.x
    public boolean releaseCoroutines() {
        return x.a.releaseCoroutines(this);
    }
}
