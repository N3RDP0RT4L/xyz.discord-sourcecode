package d0.e0.p.d.m0.e.b;

import androidx.exifinterface.media.ExifInterface;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.e.b.s;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.k.v.h;
import d0.g0.w;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
/* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
/* loaded from: classes3.dex */
public final class b implements p.d {
    public final /* synthetic */ d0.e0.p.d.m0.e.b.a<A, C> a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ HashMap<s, List<A>> f3336b;
    public final /* synthetic */ HashMap<s, C> c;

    /* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
    /* loaded from: classes3.dex */
    public final class a extends C0311b implements p.e {
        public final /* synthetic */ b d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(b bVar, s sVar) {
            super(bVar, sVar);
            m.checkNotNullParameter(bVar, "this$0");
            m.checkNotNullParameter(sVar, "signature");
            this.d = bVar;
        }

        @Override // d0.e0.p.d.m0.e.b.p.e
        public p.a visitParameterAnnotation(int i, d0.e0.p.d.m0.g.a aVar, u0 u0Var) {
            m.checkNotNullParameter(aVar, "classId");
            m.checkNotNullParameter(u0Var, "source");
            s fromMethodSignatureAndParameterIndex = s.a.fromMethodSignatureAndParameterIndex(this.a, i);
            List list = (List) this.d.f3336b.get(fromMethodSignatureAndParameterIndex);
            if (list == null) {
                list = new ArrayList();
                this.d.f3336b.put(fromMethodSignatureAndParameterIndex, list);
            }
            return d0.e0.p.d.m0.e.b.a.access$loadAnnotationIfNotSpecial(this.d.a, aVar, u0Var, list);
        }
    }

    /* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
    /* renamed from: d0.e0.p.d.m0.e.b.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public class C0311b implements p.c {
        public final s a;

        /* renamed from: b  reason: collision with root package name */
        public final ArrayList<A> f3337b = new ArrayList<>();
        public final /* synthetic */ b c;

        public C0311b(b bVar, s sVar) {
            m.checkNotNullParameter(bVar, "this$0");
            m.checkNotNullParameter(sVar, "signature");
            this.c = bVar;
            this.a = sVar;
        }

        @Override // d0.e0.p.d.m0.e.b.p.c
        public p.a visitAnnotation(d0.e0.p.d.m0.g.a aVar, u0 u0Var) {
            m.checkNotNullParameter(aVar, "classId");
            m.checkNotNullParameter(u0Var, "source");
            return d0.e0.p.d.m0.e.b.a.access$loadAnnotationIfNotSpecial(this.c.a, aVar, u0Var, this.f3337b);
        }

        @Override // d0.e0.p.d.m0.e.b.p.c
        public void visitEnd() {
            if (!this.f3337b.isEmpty()) {
                this.c.f3336b.put(this.a, this.f3337b);
            }
        }
    }

    public b(d0.e0.p.d.m0.e.b.a<A, C> aVar, HashMap<s, List<A>> hashMap, HashMap<s, C> hashMap2) {
        this.a = aVar;
        this.f3336b = hashMap;
        this.c = hashMap2;
    }

    @Override // d0.e0.p.d.m0.e.b.p.d
    public p.c visitField(e eVar, String str, Object obj) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str, "desc");
        s.a aVar = s.a;
        String asString = eVar.asString();
        m.checkNotNullExpressionValue(asString, "name.asString()");
        s fromFieldNameAndDesc = aVar.fromFieldNameAndDesc(asString, str);
        if (obj != null) {
            Objects.requireNonNull((c) this.a);
            m.checkNotNullParameter(str, "desc");
            m.checkNotNullParameter(obj, "initializer");
            boolean z2 = false;
            if (w.contains$default((CharSequence) "ZBCS", (CharSequence) str, false, 2, (Object) null)) {
                int intValue = ((Integer) obj).intValue();
                int hashCode = str.hashCode();
                if (hashCode == 66) {
                    if (str.equals("B")) {
                        obj = Byte.valueOf((byte) intValue);
                    }
                    throw new AssertionError(str);
                } else if (hashCode == 67) {
                    if (str.equals("C")) {
                        obj = Character.valueOf((char) intValue);
                    }
                    throw new AssertionError(str);
                } else if (hashCode != 83) {
                    if (hashCode == 90 && str.equals("Z")) {
                        if (intValue != 0) {
                            z2 = true;
                        }
                        obj = Boolean.valueOf(z2);
                    }
                    throw new AssertionError(str);
                } else {
                    if (str.equals(ExifInterface.LATITUDE_SOUTH)) {
                        obj = Short.valueOf((short) intValue);
                    }
                    throw new AssertionError(str);
                }
            }
            g<?> createConstantValue = h.a.createConstantValue(obj);
            if (createConstantValue != null) {
                this.c.put(fromFieldNameAndDesc, createConstantValue);
            }
        }
        return new C0311b(this, fromFieldNameAndDesc);
    }

    @Override // d0.e0.p.d.m0.e.b.p.d
    public p.e visitMethod(e eVar, String str) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str, "desc");
        s.a aVar = s.a;
        String asString = eVar.asString();
        m.checkNotNullExpressionValue(asString, "name.asString()");
        return new a(this, aVar.fromMethodNameAndDesc(asString, str));
    }
}
