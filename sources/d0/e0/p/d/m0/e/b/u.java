package d0.e0.p.d.m0.e.b;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.b.q.c;
import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.l;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.e.a.d0;
import d0.e0.p.d.m0.e.a.h;
import d0.e0.p.d.m0.e.b.k;
import d0.e0.p.d.m0.k.e;
import d0.e0.p.d.m0.k.y.d;
import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
import java.util.List;
/* compiled from: methodSignatureMapping.kt */
/* loaded from: classes3.dex */
public final class u {
    public static final void a(StringBuilder sb, c0 c0Var) {
        sb.append(mapToJvmType(c0Var));
    }

    public static final String computeJvmDescriptor(x xVar, boolean z2, boolean z3) {
        String str;
        m.checkNotNullParameter(xVar, "<this>");
        StringBuilder sb = new StringBuilder();
        if (z3) {
            if (xVar instanceof l) {
                str = HookHelper.constructorName;
            } else {
                str = xVar.getName().asString();
                m.checkNotNullExpressionValue(str, "name.asString()");
            }
            sb.append(str);
        }
        sb.append("(");
        q0 extensionReceiverParameter = xVar.getExtensionReceiverParameter();
        if (extensionReceiverParameter != null) {
            c0 type = extensionReceiverParameter.getType();
            m.checkNotNullExpressionValue(type, "it.type");
            a(sb, type);
        }
        for (c1 c1Var : xVar.getValueParameters()) {
            c0 type2 = c1Var.getType();
            m.checkNotNullExpressionValue(type2, "parameter.type");
            a(sb, type2);
        }
        sb.append(")");
        if (z2) {
            if (d.hasVoidReturnType(xVar)) {
                sb.append(ExifInterface.GPS_MEASUREMENT_INTERRUPTED);
            } else {
                c0 returnType = xVar.getReturnType();
                m.checkNotNull(returnType);
                a(sb, returnType);
            }
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public static /* synthetic */ String computeJvmDescriptor$default(x xVar, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        if ((i & 2) != 0) {
            z3 = true;
        }
        return computeJvmDescriptor(xVar, z2, z3);
    }

    public static final String computeJvmSignature(a aVar) {
        m.checkNotNullParameter(aVar, "<this>");
        w wVar = w.a;
        if (e.isLocal(aVar)) {
            return null;
        }
        d0.e0.p.d.m0.c.m containingDeclaration = aVar.getContainingDeclaration();
        d0.e0.p.d.m0.c.e eVar = containingDeclaration instanceof d0.e0.p.d.m0.c.e ? (d0.e0.p.d.m0.c.e) containingDeclaration : null;
        if (eVar == null || eVar.getName().isSpecial()) {
            return null;
        }
        a original = aVar.getOriginal();
        t0 t0Var = original instanceof t0 ? (t0) original : null;
        if (t0Var == null) {
            return null;
        }
        return t.signature(wVar, eVar, computeJvmDescriptor$default(t0Var, false, false, 3, null));
    }

    public static final boolean forceSingleValueParameterBoxing(a aVar) {
        m.checkNotNullParameter(aVar, "f");
        if (!(aVar instanceof x)) {
            return false;
        }
        x xVar = (x) aVar;
        if (!m.areEqual(xVar.getName().asString(), "remove") || xVar.getValueParameters().size() != 1 || d0.isFromJavaOrBuiltins((b) aVar)) {
            return false;
        }
        List<c1> valueParameters = xVar.getOriginal().getValueParameters();
        m.checkNotNullExpressionValue(valueParameters, "f.original.valueParameters");
        c0 type = ((c1) d0.t.u.single((List<? extends Object>) valueParameters)).getType();
        m.checkNotNullExpressionValue(type, "f.original.valueParameters.single().type");
        k mapToJvmType = mapToJvmType(type);
        d dVar = null;
        k.d dVar2 = mapToJvmType instanceof k.d ? (k.d) mapToJvmType : null;
        if (dVar2 != null) {
            dVar = dVar2.getJvmPrimitiveType();
        }
        if (dVar != d.INT) {
            return false;
        }
        h hVar = h.m;
        x overriddenBuiltinFunctionWithErasedValueParametersInJava = h.getOverriddenBuiltinFunctionWithErasedValueParametersInJava(xVar);
        if (overriddenBuiltinFunctionWithErasedValueParametersInJava == null) {
            return false;
        }
        List<c1> valueParameters2 = overriddenBuiltinFunctionWithErasedValueParametersInJava.getOriginal().getValueParameters();
        m.checkNotNullExpressionValue(valueParameters2, "overridden.original.valueParameters");
        c0 type2 = ((c1) d0.t.u.single((List<? extends Object>) valueParameters2)).getType();
        m.checkNotNullExpressionValue(type2, "overridden.original.valueParameters.single().type");
        k mapToJvmType2 = mapToJvmType(type2);
        d0.e0.p.d.m0.c.m containingDeclaration = overriddenBuiltinFunctionWithErasedValueParametersInJava.getContainingDeclaration();
        m.checkNotNullExpressionValue(containingDeclaration, "overridden.containingDeclaration");
        return m.areEqual(d0.e0.p.d.m0.k.x.a.getFqNameUnsafe(containingDeclaration), k.a.R.toUnsafe()) && (mapToJvmType2 instanceof k.c) && m.areEqual(((k.c) mapToJvmType2).getInternalName(), "java/lang/Object");
    }

    public static final String getInternalName(d0.e0.p.d.m0.c.e eVar) {
        m.checkNotNullParameter(eVar, "<this>");
        c cVar = c.a;
        d0.e0.p.d.m0.g.c unsafe = d0.e0.p.d.m0.k.x.a.getFqNameSafe(eVar).toUnsafe();
        m.checkNotNullExpressionValue(unsafe, "fqNameSafe.toUnsafe()");
        d0.e0.p.d.m0.g.a mapKotlinToJava = cVar.mapKotlinToJava(unsafe);
        if (mapKotlinToJava == null) {
            return d.computeInternalName$default(eVar, null, 2, null);
        }
        String internalName = d0.e0.p.d.m0.k.y.c.byClassId(mapKotlinToJava).getInternalName();
        m.checkNotNullExpressionValue(internalName, "byClassId(it).internalName");
        return internalName;
    }

    public static final k mapToJvmType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "<this>");
        return (k) d.mapType$default(c0Var, m.a, z.c, y.a, null, null, 32, null);
    }
}
