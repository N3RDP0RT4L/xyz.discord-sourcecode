package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.c.e;
import d0.z.d.m;
/* compiled from: methodSignatureBuildingUtils.kt */
/* loaded from: classes3.dex */
public final class t {
    public static final String signature(w wVar, e eVar, String str) {
        m.checkNotNullParameter(wVar, "<this>");
        m.checkNotNullParameter(eVar, "classDescriptor");
        m.checkNotNullParameter(str, "jvmDescriptor");
        return wVar.signature(u.getInternalName(eVar), str);
    }
}
