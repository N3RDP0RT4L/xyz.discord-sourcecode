package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.v0;
import d0.e0.p.d.m0.e.a.i0.l.i;
import d0.z.d.m;
/* compiled from: KotlinJvmBinaryPackageSourceElement.kt */
/* loaded from: classes3.dex */
public final class q implements u0 {

    /* renamed from: b  reason: collision with root package name */
    public final i f3352b;

    public q(i iVar) {
        m.checkNotNullParameter(iVar, "packageFragment");
        this.f3352b = iVar;
    }

    @Override // d0.e0.p.d.m0.c.u0
    public v0 getContainingFile() {
        v0 v0Var = v0.a;
        m.checkNotNullExpressionValue(v0Var, "NO_SOURCE_FILE");
        return v0Var;
    }

    public String toString() {
        return this.f3352b + ": " + this.f3352b.getBinaryClasses$descriptors_jvm().keySet();
    }
}
