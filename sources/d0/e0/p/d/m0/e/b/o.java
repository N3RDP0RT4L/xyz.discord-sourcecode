package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.e.a.k0.g;
import d0.e0.p.d.m0.e.b.n;
import d0.e0.p.d.m0.g.a;
import d0.z.d.m;
/* compiled from: KotlinClassFinder.kt */
/* loaded from: classes3.dex */
public final class o {
    public static final p findKotlinClass(n nVar, a aVar) {
        m.checkNotNullParameter(nVar, "<this>");
        m.checkNotNullParameter(aVar, "classId");
        n.a findKotlinClassOrContent = nVar.findKotlinClassOrContent(aVar);
        if (findKotlinClassOrContent == null) {
            return null;
        }
        return findKotlinClassOrContent.toKotlinJvmBinaryClass();
    }

    public static final p findKotlinClass(n nVar, g gVar) {
        m.checkNotNullParameter(nVar, "<this>");
        m.checkNotNullParameter(gVar, "javaClass");
        n.a findKotlinClassOrContent = nVar.findKotlinClassOrContent(gVar);
        if (findKotlinClassOrContent == null) {
            return null;
        }
        return findKotlinClassOrContent.toKotlinJvmBinaryClass();
    }
}
