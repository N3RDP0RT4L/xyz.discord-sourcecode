package d0.e0.p.d.m0.e.b;

import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.m0.c.v0;
import d0.e0.p.d.m0.f.a0.a;
import d0.e0.p.d.m0.f.l;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.k.y.c;
import d0.e0.p.d.m0.l.b.e0.e;
import d0.e0.p.d.m0.l.b.e0.f;
import d0.e0.p.d.m0.l.b.r;
import d0.g0.w;
import d0.z.d.m;
/* compiled from: JvmPackagePartSource.kt */
/* loaded from: classes3.dex */
public final class j implements f {

    /* renamed from: b  reason: collision with root package name */
    public final c f3350b;
    public final c c;
    public final p d;

    public j(c cVar, c cVar2, l lVar, d0.e0.p.d.m0.f.z.c cVar3, r<d0.e0.p.d.m0.f.a0.b.f> rVar, boolean z2, e eVar, p pVar) {
        m.checkNotNullParameter(cVar, "className");
        m.checkNotNullParameter(lVar, "packageProto");
        m.checkNotNullParameter(cVar3, "nameResolver");
        m.checkNotNullParameter(eVar, "abiStability");
        this.f3350b = cVar;
        this.c = cVar2;
        this.d = pVar;
        g.f<l, Integer> fVar = a.m;
        m.checkNotNullExpressionValue(fVar, "packageModuleName");
        Integer num = (Integer) d0.e0.p.d.m0.f.z.e.getExtensionOrNull(lVar, fVar);
        if (num != null) {
            cVar3.getString(num.intValue());
        }
    }

    public final d0.e0.p.d.m0.g.a getClassId() {
        return new d0.e0.p.d.m0.g.a(this.f3350b.getPackageFqName(), getSimpleName());
    }

    @Override // d0.e0.p.d.m0.c.u0
    public v0 getContainingFile() {
        v0 v0Var = v0.a;
        m.checkNotNullExpressionValue(v0Var, "NO_SOURCE_FILE");
        return v0Var;
    }

    public final c getFacadeClassName() {
        return this.c;
    }

    public final p getKnownJvmBinaryClass() {
        return this.d;
    }

    @Override // d0.e0.p.d.m0.l.b.e0.f
    public String getPresentableString() {
        StringBuilder R = b.d.b.a.a.R("Class '");
        R.append(getClassId().asSingleFqName().asString());
        R.append('\'');
        return R.toString();
    }

    public final d0.e0.p.d.m0.g.e getSimpleName() {
        String internalName = this.f3350b.getInternalName();
        m.checkNotNullExpressionValue(internalName, "className.internalName");
        d0.e0.p.d.m0.g.e identifier = d0.e0.p.d.m0.g.e.identifier(w.substringAfterLast$default(internalName, MentionUtilsKt.SLASH_CHAR, null, 2, null));
        m.checkNotNullExpressionValue(identifier, "identifier(className.internalName.substringAfterLast('/'))");
        return identifier;
    }

    public String toString() {
        return ((Object) j.class.getSimpleName()) + ": " + this.f3350b;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public j(d0.e0.p.d.m0.e.b.p r11, d0.e0.p.d.m0.f.l r12, d0.e0.p.d.m0.f.z.c r13, d0.e0.p.d.m0.l.b.r<d0.e0.p.d.m0.f.a0.b.f> r14, boolean r15, d0.e0.p.d.m0.l.b.e0.e r16) {
        /*
            r10 = this;
            java.lang.String r0 = "kotlinClass"
            r9 = r11
            d0.z.d.m.checkNotNullParameter(r11, r0)
            java.lang.String r0 = "packageProto"
            r4 = r12
            d0.z.d.m.checkNotNullParameter(r12, r0)
            java.lang.String r0 = "nameResolver"
            r5 = r13
            d0.z.d.m.checkNotNullParameter(r13, r0)
            java.lang.String r0 = "abiStability"
            r8 = r16
            d0.z.d.m.checkNotNullParameter(r8, r0)
            d0.e0.p.d.m0.g.a r0 = r11.getClassId()
            d0.e0.p.d.m0.k.y.c r2 = d0.e0.p.d.m0.k.y.c.byClassId(r0)
            java.lang.String r0 = "byClassId(kotlinClass.classId)"
            d0.z.d.m.checkNotNullExpressionValue(r2, r0)
            d0.e0.p.d.m0.e.b.b0.a r0 = r11.getClassHeader()
            java.lang.String r0 = r0.getMultifileClassName()
            r1 = 0
            if (r0 != 0) goto L33
        L31:
            r3 = r1
            goto L43
        L33:
            int r3 = r0.length()
            if (r3 <= 0) goto L3b
            r3 = 1
            goto L3c
        L3b:
            r3 = 0
        L3c:
            if (r3 == 0) goto L31
            d0.e0.p.d.m0.k.y.c r1 = d0.e0.p.d.m0.k.y.c.byInternalName(r0)
            goto L31
        L43:
            r1 = r10
            r4 = r12
            r5 = r13
            r6 = r14
            r7 = r15
            r8 = r16
            r9 = r11
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.b.j.<init>(d0.e0.p.d.m0.e.b.p, d0.e0.p.d.m0.f.l, d0.e0.p.d.m0.f.z.c, d0.e0.p.d.m0.l.b.r, boolean, d0.e0.p.d.m0.l.b.e0.e):void");
    }
}
