package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.n.c0;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: descriptorBasedTypeSignatureMapping.kt */
/* loaded from: classes3.dex */
public interface x<T> {

    /* compiled from: descriptorBasedTypeSignatureMapping.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static <T> String getPredefinedFullInternalNameForClass(x<? extends T> xVar, e eVar) {
            m.checkNotNullParameter(xVar, "this");
            m.checkNotNullParameter(eVar, "classDescriptor");
            return null;
        }

        public static <T> c0 preprocessType(x<? extends T> xVar, c0 c0Var) {
            m.checkNotNullParameter(xVar, "this");
            m.checkNotNullParameter(c0Var, "kotlinType");
            return null;
        }

        public static <T> boolean releaseCoroutines(x<? extends T> xVar) {
            m.checkNotNullParameter(xVar, "this");
            return true;
        }
    }

    c0 commonSupertype(Collection<c0> collection);

    String getPredefinedFullInternalNameForClass(e eVar);

    String getPredefinedInternalNameForClass(e eVar);

    T getPredefinedTypeForClass(e eVar);

    c0 preprocessType(c0 c0Var);

    void processErrorType(c0 c0Var, e eVar);

    boolean releaseCoroutines();
}
