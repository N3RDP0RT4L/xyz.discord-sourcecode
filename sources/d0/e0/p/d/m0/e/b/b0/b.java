package d0.e0.p.d.m0.e.b.b0;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.e.a.a0;
import d0.e0.p.d.m0.e.b.b0.a;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.v.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/* compiled from: ReadKotlinClassHeaderAnnotationVisitor.java */
/* loaded from: classes3.dex */
public class b implements p.c {
    public static final boolean a = "true".equals(System.getProperty("kotlin.ignore.old.metadata"));

    /* renamed from: b  reason: collision with root package name */
    public static final Map<d0.e0.p.d.m0.g.a, a.EnumC0312a> f3340b;
    public int[] c = null;
    public d0.e0.p.d.m0.f.a0.b.c d = null;
    public String e = null;
    public int f = 0;
    public String g = null;
    public String[] h = null;
    public String[] i = null;
    public String[] j = null;
    public a.EnumC0312a k = null;

    /* compiled from: ReadKotlinClassHeaderAnnotationVisitor.java */
    /* renamed from: d0.e0.p.d.m0.e.b.b0.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static abstract class AbstractC0314b implements p.b {
        public final List<String> a = new ArrayList();

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i == 1) {
                objArr[0] = "enumEntryName";
            } else if (i != 2) {
                objArr[0] = "enumClassId";
            } else {
                objArr[0] = "classLiteralValue";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$CollectStringArrayAnnotationVisitor";
            if (i != 2) {
                objArr[2] = "visitEnum";
            } else {
                objArr[2] = "visitClassLiteral";
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        public abstract void b(String[] strArr);

        @Override // d0.e0.p.d.m0.e.b.p.b
        public void visit(Object obj) {
            if (obj instanceof String) {
                this.a.add((String) obj);
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.b
        public void visitClassLiteral(f fVar) {
            if (fVar == null) {
                a(2);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.b
        public void visitEnd() {
            b((String[]) this.a.toArray(new String[0]));
        }

        @Override // d0.e0.p.d.m0.e.b.p.b
        public void visitEnum(d0.e0.p.d.m0.g.a aVar, e eVar) {
            if (aVar == null) {
                a(0);
                throw null;
            } else if (eVar == null) {
                a(1);
                throw null;
            }
        }
    }

    /* compiled from: ReadKotlinClassHeaderAnnotationVisitor.java */
    /* loaded from: classes3.dex */
    public class c implements p.a {
        public c(a aVar) {
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i == 1) {
                objArr[0] = "classLiteralValue";
            } else if (i == 7) {
                objArr[0] = "classId";
            } else if (i == 4) {
                objArr[0] = "enumClassId";
            } else if (i != 5) {
                objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
            } else {
                objArr[0] = "enumEntryName";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$KotlinMetadataArgumentVisitor";
            switch (i) {
                case 2:
                    objArr[2] = "visitArray";
                    break;
                case 3:
                case 4:
                case 5:
                    objArr[2] = "visitEnum";
                    break;
                case 6:
                case 7:
                    objArr[2] = "visitAnnotation";
                    break;
                default:
                    objArr[2] = "visitClassLiteral";
                    break;
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visit(e eVar, Object obj) {
            if (eVar != null) {
                String asString = eVar.asString();
                if ("k".equals(asString)) {
                    if (obj instanceof Integer) {
                        b.this.k = a.EnumC0312a.getById(((Integer) obj).intValue());
                    }
                } else if ("mv".equals(asString)) {
                    if (obj instanceof int[]) {
                        b.this.c = (int[]) obj;
                    }
                } else if ("bv".equals(asString)) {
                    if (obj instanceof int[]) {
                        b.this.d = new d0.e0.p.d.m0.f.a0.b.c((int[]) obj);
                    }
                } else if ("xs".equals(asString)) {
                    if (obj instanceof String) {
                        b.this.e = (String) obj;
                    }
                } else if ("xi".equals(asString)) {
                    if (obj instanceof Integer) {
                        b.this.f = ((Integer) obj).intValue();
                    }
                } else if ("pn".equals(asString) && (obj instanceof String)) {
                    b.this.g = (String) obj;
                }
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public p.a visitAnnotation(e eVar, d0.e0.p.d.m0.g.a aVar) {
            if (eVar == null) {
                a(6);
                throw null;
            } else if (aVar != null) {
                return null;
            } else {
                a(7);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public p.b visitArray(e eVar) {
            if (eVar != null) {
                String asString = eVar.asString();
                if ("d1".equals(asString)) {
                    return new d0.e0.p.d.m0.e.b.b0.c(this);
                }
                if ("d2".equals(asString)) {
                    return new d0.e0.p.d.m0.e.b.b0.d(this);
                }
                return null;
            }
            a(2);
            throw null;
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitClassLiteral(e eVar, f fVar) {
            if (eVar == null) {
                a(0);
                throw null;
            } else if (fVar == null) {
                a(1);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitEnd() {
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitEnum(e eVar, d0.e0.p.d.m0.g.a aVar, e eVar2) {
            if (eVar == null) {
                a(3);
                throw null;
            } else if (aVar == null) {
                a(4);
                throw null;
            } else if (eVar2 == null) {
                a(5);
                throw null;
            }
        }
    }

    /* compiled from: ReadKotlinClassHeaderAnnotationVisitor.java */
    /* loaded from: classes3.dex */
    public class d implements p.a {
        public d(a aVar) {
        }

        public static /* synthetic */ void a(int i) {
            Object[] objArr = new Object[3];
            if (i == 1) {
                objArr[0] = "classLiteralValue";
            } else if (i == 7) {
                objArr[0] = "classId";
            } else if (i == 4) {
                objArr[0] = "enumClassId";
            } else if (i != 5) {
                objArr[0] = ModelAuditLogEntry.CHANGE_KEY_NAME;
            } else {
                objArr[0] = "enumEntryName";
            }
            objArr[1] = "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor$OldDeprecatedAnnotationArgumentVisitor";
            switch (i) {
                case 2:
                    objArr[2] = "visitArray";
                    break;
                case 3:
                case 4:
                case 5:
                    objArr[2] = "visitEnum";
                    break;
                case 6:
                case 7:
                    objArr[2] = "visitAnnotation";
                    break;
                default:
                    objArr[2] = "visitClassLiteral";
                    break;
            }
            throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visit(e eVar, Object obj) {
            if (eVar != null) {
                String asString = eVar.asString();
                if ("version".equals(asString)) {
                    if (obj instanceof int[]) {
                        b bVar = b.this;
                        int[] iArr = (int[]) obj;
                        bVar.c = iArr;
                        if (bVar.d == null) {
                            bVar.d = new d0.e0.p.d.m0.f.a0.b.c(iArr);
                        }
                    }
                } else if ("multifileClassName".equals(asString)) {
                    b.this.e = obj instanceof String ? (String) obj : null;
                }
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public p.a visitAnnotation(e eVar, d0.e0.p.d.m0.g.a aVar) {
            if (eVar == null) {
                a(6);
                throw null;
            } else if (aVar != null) {
                return null;
            } else {
                a(7);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public p.b visitArray(e eVar) {
            if (eVar != null) {
                String asString = eVar.asString();
                if ("data".equals(asString) || "filePartClassNames".equals(asString)) {
                    return new e(this);
                }
                if ("strings".equals(asString)) {
                    return new f(this);
                }
                return null;
            }
            a(2);
            throw null;
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitClassLiteral(e eVar, f fVar) {
            if (eVar == null) {
                a(0);
                throw null;
            } else if (fVar == null) {
                a(1);
                throw null;
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitEnd() {
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitEnum(e eVar, d0.e0.p.d.m0.g.a aVar, e eVar2) {
            if (eVar == null) {
                a(3);
                throw null;
            } else if (aVar == null) {
                a(4);
                throw null;
            } else if (eVar2 == null) {
                a(5);
                throw null;
            }
        }
    }

    static {
        HashMap hashMap = new HashMap();
        f3340b = hashMap;
        hashMap.put(d0.e0.p.d.m0.g.a.topLevel(new d0.e0.p.d.m0.g.b("kotlin.jvm.internal.KotlinClass")), a.EnumC0312a.CLASS);
        hashMap.put(d0.e0.p.d.m0.g.a.topLevel(new d0.e0.p.d.m0.g.b("kotlin.jvm.internal.KotlinFileFacade")), a.EnumC0312a.FILE_FACADE);
        hashMap.put(d0.e0.p.d.m0.g.a.topLevel(new d0.e0.p.d.m0.g.b("kotlin.jvm.internal.KotlinMultifileClass")), a.EnumC0312a.MULTIFILE_CLASS);
        hashMap.put(d0.e0.p.d.m0.g.a.topLevel(new d0.e0.p.d.m0.g.b("kotlin.jvm.internal.KotlinMultifileClassPart")), a.EnumC0312a.MULTIFILE_CLASS_PART);
        hashMap.put(d0.e0.p.d.m0.g.a.topLevel(new d0.e0.p.d.m0.g.b("kotlin.jvm.internal.KotlinSyntheticClass")), a.EnumC0312a.SYNTHETIC_CLASS);
    }

    public static /* synthetic */ void a(int i) {
        Object[] objArr = new Object[3];
        if (i != 1) {
            objArr[0] = "classId";
        } else {
            objArr[0] = "source";
        }
        objArr[1] = "kotlin/reflect/jvm/internal/impl/load/kotlin/header/ReadKotlinClassHeaderAnnotationVisitor";
        objArr[2] = "visitAnnotation";
        throw new IllegalArgumentException(String.format("Argument for @NotNull parameter '%s' of %s.%s must not be null", objArr));
    }

    public d0.e0.p.d.m0.e.b.b0.a createHeader() {
        if (this.k == null || this.c == null) {
            return null;
        }
        boolean z2 = true;
        d0.e0.p.d.m0.f.a0.b.f fVar = new d0.e0.p.d.m0.f.a0.b.f(this.c, (this.f & 8) != 0);
        if (!fVar.isCompatible()) {
            this.j = this.h;
            this.h = null;
        } else {
            a.EnumC0312a aVar = this.k;
            if (!(aVar == a.EnumC0312a.CLASS || aVar == a.EnumC0312a.FILE_FACADE || aVar == a.EnumC0312a.MULTIFILE_CLASS_PART)) {
                z2 = false;
            }
            if (z2 && this.h == null) {
                return null;
            }
        }
        a.EnumC0312a aVar2 = this.k;
        d0.e0.p.d.m0.f.a0.b.c cVar = this.d;
        if (cVar == null) {
            cVar = d0.e0.p.d.m0.f.a0.b.c.f;
        }
        return new d0.e0.p.d.m0.e.b.b0.a(aVar2, fVar, cVar, this.h, this.j, this.i, this.e, this.f, this.g);
    }

    @Override // d0.e0.p.d.m0.e.b.p.c
    public p.a visitAnnotation(d0.e0.p.d.m0.g.a aVar, u0 u0Var) {
        a.EnumC0312a aVar2;
        if (aVar == null) {
            a(0);
            throw null;
        } else if (u0Var == null) {
            a(1);
            throw null;
        } else if (aVar.asSingleFqName().equals(a0.a)) {
            return new c(null);
        } else {
            if (a || this.k != null || (aVar2 = f3340b.get(aVar)) == null) {
                return null;
            }
            this.k = aVar2;
            return new d(null);
        }
    }

    @Override // d0.e0.p.d.m0.e.b.p.c
    public void visitEnd() {
    }
}
