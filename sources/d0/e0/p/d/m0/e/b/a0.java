package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.b.q.c;
import d0.e0.p.d.m0.e.a.l0.v;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.c;
import d0.e0.p.d.m0.k.y.d;
import d0.e0.p.d.m0.n.d1;
import d0.e0.p.d.m0.n.n1.h;
import d0.e0.p.d.m0.n.n1.l;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
/* compiled from: typeSignatureMapping.kt */
/* loaded from: classes3.dex */
public final class a0 {
    public static final <T> T boxTypeIfNeeded(l<T> lVar, T t, boolean z2) {
        m.checkNotNullParameter(lVar, "<this>");
        m.checkNotNullParameter(t, "possiblyPrimitiveType");
        return z2 ? lVar.boxType(t) : t;
    }

    public static final <T> T mapBuiltInType(d1 d1Var, h hVar, l<T> lVar, z zVar) {
        m.checkNotNullParameter(d1Var, "<this>");
        m.checkNotNullParameter(hVar, "type");
        m.checkNotNullParameter(lVar, "typeFactory");
        m.checkNotNullParameter(zVar, "mode");
        l typeConstructor = d1Var.typeConstructor(hVar);
        if (!d1Var.isClassTypeConstructor(typeConstructor)) {
            return null;
        }
        i primitiveType = d1Var.getPrimitiveType(typeConstructor);
        boolean z2 = true;
        if (primitiveType != null) {
            T createPrimitiveType = lVar.createPrimitiveType(primitiveType);
            if (!d1Var.isNullableType(hVar) && !v.hasEnhancedNullability(d1Var, hVar)) {
                z2 = false;
            }
            return (T) boxTypeIfNeeded(lVar, createPrimitiveType, z2);
        }
        i primitiveArrayType = d1Var.getPrimitiveArrayType(typeConstructor);
        if (primitiveArrayType != null) {
            return lVar.createFromString(m.stringPlus("[", d.get(primitiveArrayType).getDesc()));
        }
        if (d1Var.isUnderKotlinPackage(typeConstructor)) {
            c classFqNameUnsafe = d1Var.getClassFqNameUnsafe(typeConstructor);
            a mapKotlinToJava = classFqNameUnsafe == null ? null : d0.e0.p.d.m0.b.q.c.a.mapKotlinToJava(classFqNameUnsafe);
            if (mapKotlinToJava != null) {
                if (!zVar.getKotlinCollectionsToJavaCollections()) {
                    List<c.a> mutabilityMappings = d0.e0.p.d.m0.b.q.c.a.getMutabilityMappings();
                    if (!(mutabilityMappings instanceof Collection) || !mutabilityMappings.isEmpty()) {
                        for (c.a aVar : mutabilityMappings) {
                            if (m.areEqual(aVar.getJavaClass(), mapKotlinToJava)) {
                                break;
                            }
                        }
                    }
                    z2 = false;
                    if (z2) {
                        return null;
                    }
                }
                String internalName = d0.e0.p.d.m0.k.y.c.byClassId(mapKotlinToJava).getInternalName();
                m.checkNotNullExpressionValue(internalName, "byClassId(classId).internalName");
                return lVar.createObjectType(internalName);
            }
        }
        return null;
    }
}
