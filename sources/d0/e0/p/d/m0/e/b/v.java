package d0.e0.p.d.m0.e.b;

import d0.t.n;
import d0.z.d.m;
import java.util.List;
/* compiled from: PackagePartProvider.kt */
/* loaded from: classes3.dex */
public interface v {

    /* compiled from: PackagePartProvider.kt */
    /* loaded from: classes3.dex */
    public static final class a implements v {
        public static final a a = new a();

        @Override // d0.e0.p.d.m0.e.b.v
        public List<String> findPackageParts(String str) {
            m.checkNotNullParameter(str, "packageFqName");
            return n.emptyList();
        }
    }

    List<String> findPackageParts(String str);
}
