package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.e.b.b0.a;
import d0.e0.p.d.m0.f.a0.b.g;
import d0.e0.p.d.m0.f.a0.b.h;
import d0.e0.p.d.m0.f.c;
import d0.e0.p.d.m0.f.l;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.l.b.j;
import d0.e0.p.d.m0.l.b.r;
import d0.t.m0;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.Set;
import kotlin.Pair;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.jvm.internal.impl.protobuf.InvalidProtocolBufferException;
/* compiled from: DeserializedDescriptorResolver.kt */
/* loaded from: classes3.dex */
public final class f {
    public static final a a = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public static final Set<a.EnumC0312a> f3348b = m0.setOf(a.EnumC0312a.CLASS);
    public static final Set<a.EnumC0312a> c = n0.setOf((Object[]) new a.EnumC0312a[]{a.EnumC0312a.FILE_FACADE, a.EnumC0312a.MULTIFILE_CLASS_PART});
    public static final d0.e0.p.d.m0.f.a0.b.f d = new d0.e0.p.d.m0.f.a0.b.f(1, 1, 2);
    public static final d0.e0.p.d.m0.f.a0.b.f e = new d0.e0.p.d.m0.f.a0.b.f(1, 1, 11);
    public static final d0.e0.p.d.m0.f.a0.b.f f = new d0.e0.p.d.m0.f.a0.b.f(1, 1, 13);
    public j g;

    /* compiled from: DeserializedDescriptorResolver.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final d0.e0.p.d.m0.f.a0.b.f getKOTLIN_1_3_RC_METADATA_VERSION$descriptors_jvm() {
            return f.f;
        }

        public final Set<a.EnumC0312a> getKOTLIN_CLASS$descriptors_jvm() {
            return f.f3348b;
        }
    }

    /* compiled from: DeserializedDescriptorResolver.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<Collection<? extends e>> {
        public static final b j = new b();

        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Collection<? extends e> invoke() {
            return n.emptyList();
        }
    }

    public static final boolean access$getSkipMetadataVersionCheck(f fVar) {
        return fVar.getComponents().getConfiguration().getSkipMetadataVersionCheck();
    }

    public final d0.e0.p.d.m0.l.b.e0.e a(p pVar) {
        d0.e0.p.d.m0.l.b.e0.e eVar = d0.e0.p.d.m0.l.b.e0.e.STABLE;
        return getComponents().getConfiguration().getAllowUnstableDependencies() ? eVar : pVar.getClassHeader().isUnstableFirBinary() ? d0.e0.p.d.m0.l.b.e0.e.FIR_UNSTABLE : pVar.getClassHeader().isUnstableJvmIrBinary() ? d0.e0.p.d.m0.l.b.e0.e.IR_UNSTABLE : eVar;
    }

    public final r<d0.e0.p.d.m0.f.a0.b.f> b(p pVar) {
        if (getComponents().getConfiguration().getSkipMetadataVersionCheck() || pVar.getClassHeader().getMetadataVersion().isCompatible()) {
            return null;
        }
        return new r<>(pVar.getClassHeader().getMetadataVersion(), d0.e0.p.d.m0.f.a0.b.f.f, pVar.getLocation(), pVar.getClassId());
    }

    public final boolean c(p pVar) {
        if (!getComponents().getConfiguration().getReportErrorsOnPreReleaseDependencies() || (!pVar.getClassHeader().isPreRelease() && !m.areEqual(pVar.getClassHeader().getMetadataVersion(), d))) {
            if (!(!getComponents().getConfiguration().getSkipPrereleaseCheck() && pVar.getClassHeader().isPreRelease() && m.areEqual(pVar.getClassHeader().getMetadataVersion(), e))) {
                return false;
            }
        }
        return true;
    }

    public final i createKotlinPackagePartScope(e0 e0Var, p pVar) {
        String[] strings;
        Pair<g, l> pair;
        m.checkNotNullParameter(e0Var, "descriptor");
        m.checkNotNullParameter(pVar, "kotlinClass");
        String[] d2 = d(pVar, c);
        if (d2 == null || (strings = pVar.getClassHeader().getStrings()) == null) {
            return null;
        }
        try {
            try {
                h hVar = h.a;
                pair = h.readPackageDataFrom(d2, strings);
            } catch (InvalidProtocolBufferException e2) {
                throw new IllegalStateException(m.stringPlus("Could not read data from ", pVar.getLocation()), e2);
            }
        } catch (Throwable th) {
            if (access$getSkipMetadataVersionCheck(this) || pVar.getClassHeader().getMetadataVersion().isCompatible()) {
                throw th;
            }
            pair = null;
        }
        if (pair == null) {
            return null;
        }
        g component1 = pair.component1();
        l component2 = pair.component2();
        return new d0.e0.p.d.m0.l.b.e0.i(e0Var, component2, component1, pVar.getClassHeader().getMetadataVersion(), new j(pVar, component2, component1, b(pVar), c(pVar), a(pVar)), getComponents(), b.j);
    }

    public final String[] d(p pVar, Set<? extends a.EnumC0312a> set) {
        d0.e0.p.d.m0.e.b.b0.a classHeader = pVar.getClassHeader();
        String[] data = classHeader.getData();
        if (data == null) {
            data = classHeader.getIncompatibleData();
        }
        if (data != null && set.contains(classHeader.getKind())) {
            return data;
        }
        return null;
    }

    public final j getComponents() {
        j jVar = this.g;
        if (jVar != null) {
            return jVar;
        }
        m.throwUninitializedPropertyAccessException("components");
        throw null;
    }

    public final d0.e0.p.d.m0.l.b.f readClassData$descriptors_jvm(p pVar) {
        String[] strings;
        Pair<g, c> pair;
        m.checkNotNullParameter(pVar, "kotlinClass");
        String[] d2 = d(pVar, a.getKOTLIN_CLASS$descriptors_jvm());
        if (d2 == null || (strings = pVar.getClassHeader().getStrings()) == null) {
            return null;
        }
        try {
            try {
                h hVar = h.a;
                pair = h.readClassDataFrom(d2, strings);
            } catch (InvalidProtocolBufferException e2) {
                throw new IllegalStateException(m.stringPlus("Could not read data from ", pVar.getLocation()), e2);
            }
        } catch (Throwable th) {
            if (access$getSkipMetadataVersionCheck(this) || pVar.getClassHeader().getMetadataVersion().isCompatible()) {
                throw th;
            }
            pair = null;
        }
        if (pair == null) {
            return null;
        }
        return new d0.e0.p.d.m0.l.b.f(pair.component1(), pair.component2(), pVar.getClassHeader().getMetadataVersion(), new r(pVar, b(pVar), c(pVar), a(pVar)));
    }

    public final d0.e0.p.d.m0.c.e resolveClass(p pVar) {
        m.checkNotNullParameter(pVar, "kotlinClass");
        d0.e0.p.d.m0.l.b.f readClassData$descriptors_jvm = readClassData$descriptors_jvm(pVar);
        if (readClassData$descriptors_jvm == null) {
            return null;
        }
        return getComponents().getClassDeserializer().deserializeClass(pVar.getClassId(), readClassData$descriptors_jvm);
    }

    public final void setComponents(j jVar) {
        m.checkNotNullParameter(jVar, "<set-?>");
        this.g = jVar;
    }

    public final void setComponents(e eVar) {
        m.checkNotNullParameter(eVar, "components");
        setComponents(eVar.getComponents());
    }
}
