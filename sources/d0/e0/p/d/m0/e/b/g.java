package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.l.b.f;
import d0.z.d.m;
/* compiled from: JavaClassDataFinder.kt */
/* loaded from: classes3.dex */
public final class g implements d0.e0.p.d.m0.l.b.g {
    public final n a;

    /* renamed from: b  reason: collision with root package name */
    public final f f3349b;

    public g(n nVar, f fVar) {
        m.checkNotNullParameter(nVar, "kotlinClassFinder");
        m.checkNotNullParameter(fVar, "deserializedDescriptorResolver");
        this.a = nVar;
        this.f3349b = fVar;
    }

    @Override // d0.e0.p.d.m0.l.b.g
    public f findClassData(a aVar) {
        m.checkNotNullParameter(aVar, "classId");
        p findKotlinClass = o.findKotlinClass(this.a, aVar);
        if (findKotlinClass == null) {
            return null;
        }
        m.areEqual(findKotlinClass.getClassId(), aVar);
        return this.f3349b.readClassData$descriptors_jvm(findKotlinClass);
    }
}
