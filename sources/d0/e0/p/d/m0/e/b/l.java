package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.b.i;
/* compiled from: typeSignatureMapping.kt */
/* loaded from: classes3.dex */
public interface l<T> {
    T boxType(T t);

    T createFromString(String str);

    T createObjectType(String str);

    T createPrimitiveType(i iVar);

    T getJavaLangClassType();

    String toString(T t);
}
