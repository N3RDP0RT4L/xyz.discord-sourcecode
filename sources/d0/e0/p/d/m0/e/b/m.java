package d0.e0.p.d.m0.e.b;

import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.e.b.k;
import d0.e0.p.d.m0.k.y.c;
import d0.e0.p.d.m0.k.y.d;
import d0.g0.w;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: methodSignatureMapping.kt */
/* loaded from: classes3.dex */
public final class m implements l<k> {
    public static final m a = new m();

    public k boxType(k kVar) {
        d0.z.d.m.checkNotNullParameter(kVar, "possiblyPrimitiveType");
        if (!(kVar instanceof k.d)) {
            return kVar;
        }
        k.d dVar = (k.d) kVar;
        if (dVar.getJvmPrimitiveType() == null) {
            return kVar;
        }
        String internalName = c.byFqNameWithoutInnerClasses(dVar.getJvmPrimitiveType().getWrapperFqName()).getInternalName();
        d0.z.d.m.checkNotNullExpressionValue(internalName, "byFqNameWithoutInnerClasses(possiblyPrimitiveType.jvmPrimitiveType.wrapperFqName).internalName");
        return createObjectType2(internalName);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // d0.e0.p.d.m0.e.b.l
    public k createFromString(String str) {
        d dVar;
        k cVar;
        d0.z.d.m.checkNotNullParameter(str, "representation");
        str.length();
        char charAt = str.charAt(0);
        d[] values = d.values();
        int i = 0;
        while (true) {
            if (i >= 8) {
                dVar = null;
                break;
            }
            dVar = values[i];
            if (dVar.getDesc().charAt(0) == charAt) {
                break;
            }
            i++;
        }
        if (dVar != null) {
            return new k.d(dVar);
        }
        if (charAt == 'V') {
            return new k.d(null);
        }
        if (charAt == '[') {
            String substring = str.substring(1);
            d0.z.d.m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
            cVar = new k.a(createFromString(substring));
        } else {
            if (charAt == 'L') {
                w.endsWith$default((CharSequence) str, ';', false, 2, (Object) null);
            }
            String substring2 = str.substring(1, str.length() - 1);
            d0.z.d.m.checkNotNullExpressionValue(substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            cVar = new k.c(substring2);
        }
        return cVar;
    }

    @Override // d0.e0.p.d.m0.e.b.l
    /* renamed from: createObjectType */
    public k createObjectType2(String str) {
        d0.z.d.m.checkNotNullParameter(str, "internalName");
        return new k.c(str);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // d0.e0.p.d.m0.e.b.l
    public k createPrimitiveType(i iVar) {
        d0.z.d.m.checkNotNullParameter(iVar, "primitiveType");
        switch (iVar.ordinal()) {
            case 0:
                return k.a.getBOOLEAN$descriptors_jvm();
            case 1:
                return k.a.getCHAR$descriptors_jvm();
            case 2:
                return k.a.getBYTE$descriptors_jvm();
            case 3:
                return k.a.getSHORT$descriptors_jvm();
            case 4:
                return k.a.getINT$descriptors_jvm();
            case 5:
                return k.a.getFLOAT$descriptors_jvm();
            case 6:
                return k.a.getLONG$descriptors_jvm();
            case 7:
                return k.a.getDOUBLE$descriptors_jvm();
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // d0.e0.p.d.m0.e.b.l
    public k getJavaLangClassType() {
        return createObjectType2("java/lang/Class");
    }

    public String toString(k kVar) {
        String desc;
        d0.z.d.m.checkNotNullParameter(kVar, "type");
        if (kVar instanceof k.a) {
            return d0.z.d.m.stringPlus("[", toString(((k.a) kVar).getElementType()));
        }
        if (kVar instanceof k.d) {
            d jvmPrimitiveType = ((k.d) kVar).getJvmPrimitiveType();
            return (jvmPrimitiveType == null || (desc = jvmPrimitiveType.getDesc()) == null) ? ExifInterface.GPS_MEASUREMENT_INTERRUPTED : desc;
        } else if (kVar instanceof k.c) {
            StringBuilder O = a.O('L');
            O.append(((k.c) kVar).getInternalName());
            O.append(';');
            return O.toString();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }
}
