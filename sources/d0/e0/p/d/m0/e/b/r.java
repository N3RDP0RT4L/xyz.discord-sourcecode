package d0.e0.p.d.m0.e.b;

import b.d.b.a.a;
import d0.e0.p.d.m0.c.v0;
import d0.e0.p.d.m0.l.b.e0.e;
import d0.e0.p.d.m0.l.b.e0.f;
import d0.z.d.m;
/* compiled from: KotlinJvmBinarySourceElement.kt */
/* loaded from: classes3.dex */
public final class r implements f {

    /* renamed from: b  reason: collision with root package name */
    public final p f3353b;

    public r(p pVar, d0.e0.p.d.m0.l.b.r<d0.e0.p.d.m0.f.a0.b.f> rVar, boolean z2, e eVar) {
        m.checkNotNullParameter(pVar, "binaryClass");
        m.checkNotNullParameter(eVar, "abiStability");
        this.f3353b = pVar;
    }

    public final p getBinaryClass() {
        return this.f3353b;
    }

    @Override // d0.e0.p.d.m0.c.u0
    public v0 getContainingFile() {
        v0 v0Var = v0.a;
        m.checkNotNullExpressionValue(v0Var, "NO_SOURCE_FILE");
        return v0Var;
    }

    @Override // d0.e0.p.d.m0.l.b.e0.f
    public String getPresentableString() {
        StringBuilder R = a.R("Class '");
        R.append(this.f3353b.getClassId().asSingleFqName().asString());
        R.append('\'');
        return R.toString();
    }

    public String toString() {
        return ((Object) r.class.getSimpleName()) + ": " + this.f3353b;
    }
}
