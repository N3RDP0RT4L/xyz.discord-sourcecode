package d0.e0.p.d.m0.e.b;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.g1.d;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.w;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.k.v.f;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.k.v.h;
import d0.e0.p.d.m0.k.v.j;
import d0.e0.p.d.m0.k.v.k;
import d0.e0.p.d.m0.k.v.r;
import d0.e0.p.d.m0.l.b.e;
import d0.e0.p.d.m0.m.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/* compiled from: BinaryClassAnnotationAndConstantLoaderImpl.kt */
/* loaded from: classes3.dex */
public final class c extends d0.e0.p.d.m0.e.b.a<d0.e0.p.d.m0.c.g1.c, g<?>> {
    public final c0 c;
    public final d0 d;
    public final e e;

    /* compiled from: BinaryClassAnnotationAndConstantLoaderImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a implements p.a {
        public final HashMap<d0.e0.p.d.m0.g.e, g<?>> a = new HashMap<>();

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ d0.e0.p.d.m0.c.e f3345b;
        public final /* synthetic */ c c;
        public final /* synthetic */ List<d0.e0.p.d.m0.c.g1.c> d;
        public final /* synthetic */ u0 e;

        /* compiled from: BinaryClassAnnotationAndConstantLoaderImpl.kt */
        /* renamed from: d0.e0.p.d.m0.e.b.c$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0315a implements p.a {
            public final /* synthetic */ p.a a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ p.a f3346b;
            public final /* synthetic */ a c;
            public final /* synthetic */ d0.e0.p.d.m0.g.e d;
            public final /* synthetic */ ArrayList<d0.e0.p.d.m0.c.g1.c> e;

            public C0315a(p.a aVar, a aVar2, d0.e0.p.d.m0.g.e eVar, ArrayList<d0.e0.p.d.m0.c.g1.c> arrayList) {
                this.f3346b = aVar;
                this.c = aVar2;
                this.d = eVar;
                this.e = arrayList;
                this.a = aVar;
            }

            @Override // d0.e0.p.d.m0.e.b.p.a
            public void visit(d0.e0.p.d.m0.g.e eVar, Object obj) {
                this.a.visit(eVar, obj);
            }

            @Override // d0.e0.p.d.m0.e.b.p.a
            public p.a visitAnnotation(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.g.a aVar) {
                m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullParameter(aVar, "classId");
                return this.a.visitAnnotation(eVar, aVar);
            }

            @Override // d0.e0.p.d.m0.e.b.p.a
            public p.b visitArray(d0.e0.p.d.m0.g.e eVar) {
                m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
                return this.a.visitArray(eVar);
            }

            @Override // d0.e0.p.d.m0.e.b.p.a
            public void visitClassLiteral(d0.e0.p.d.m0.g.e eVar, f fVar) {
                m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullParameter(fVar, "value");
                this.a.visitClassLiteral(eVar, fVar);
            }

            @Override // d0.e0.p.d.m0.e.b.p.a
            public void visitEnd() {
                this.f3346b.visitEnd();
                this.c.a.put(this.d, new d0.e0.p.d.m0.k.v.a((d0.e0.p.d.m0.c.g1.c) u.single((List<? extends Object>) this.e)));
            }

            @Override // d0.e0.p.d.m0.e.b.p.a
            public void visitEnum(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.g.a aVar, d0.e0.p.d.m0.g.e eVar2) {
                m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullParameter(aVar, "enumClassId");
                m.checkNotNullParameter(eVar2, "enumEntryName");
                this.a.visitEnum(eVar, aVar, eVar2);
            }
        }

        /* compiled from: BinaryClassAnnotationAndConstantLoaderImpl.kt */
        /* loaded from: classes3.dex */
        public static final class b implements p.b {
            public final ArrayList<g<?>> a = new ArrayList<>();
            public final /* synthetic */ d0.e0.p.d.m0.g.e c;
            public final /* synthetic */ d0.e0.p.d.m0.c.e d;

            public b(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.c.e eVar2) {
                this.c = eVar;
                this.d = eVar2;
            }

            @Override // d0.e0.p.d.m0.e.b.p.b
            public void visit(Object obj) {
                this.a.add(a.this.a(this.c, obj));
            }

            @Override // d0.e0.p.d.m0.e.b.p.b
            public void visitClassLiteral(f fVar) {
                m.checkNotNullParameter(fVar, "value");
                this.a.add(new r(fVar));
            }

            @Override // d0.e0.p.d.m0.e.b.p.b
            public void visitEnd() {
                c1 annotationParameterByName = d0.e0.p.d.m0.e.a.g0.a.getAnnotationParameterByName(this.c, this.d);
                if (annotationParameterByName != null) {
                    HashMap hashMap = a.this.a;
                    d0.e0.p.d.m0.g.e eVar = this.c;
                    h hVar = h.a;
                    List<? extends g<?>> compact = d0.e0.p.d.m0.p.a.compact(this.a);
                    d0.e0.p.d.m0.n.c0 type = annotationParameterByName.getType();
                    m.checkNotNullExpressionValue(type, "parameter.type");
                    hashMap.put(eVar, hVar.createArrayValue(compact, type));
                }
            }

            @Override // d0.e0.p.d.m0.e.b.p.b
            public void visitEnum(d0.e0.p.d.m0.g.a aVar, d0.e0.p.d.m0.g.e eVar) {
                m.checkNotNullParameter(aVar, "enumClassId");
                m.checkNotNullParameter(eVar, "enumEntryName");
                this.a.add(new j(aVar, eVar));
            }
        }

        public a(d0.e0.p.d.m0.c.e eVar, c cVar, List<d0.e0.p.d.m0.c.g1.c> list, u0 u0Var) {
            this.f3345b = eVar;
            this.c = cVar;
            this.d = list;
            this.e = u0Var;
        }

        public final g<?> a(d0.e0.p.d.m0.g.e eVar, Object obj) {
            g<?> createConstantValue = h.a.createConstantValue(obj);
            return createConstantValue == null ? k.f3446b.create(m.stringPlus("Unsupported annotation argument: ", eVar)) : createConstantValue;
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visit(d0.e0.p.d.m0.g.e eVar, Object obj) {
            if (eVar != null) {
                this.a.put(eVar, a(eVar, obj));
            }
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public p.a visitAnnotation(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.g.a aVar) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(aVar, "classId");
            ArrayList arrayList = new ArrayList();
            c cVar = this.c;
            u0 u0Var = u0.a;
            m.checkNotNullExpressionValue(u0Var, "NO_SOURCE");
            p.a h = cVar.h(aVar, u0Var, arrayList);
            m.checkNotNull(h);
            return new C0315a(h, this, eVar, arrayList);
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public p.b visitArray(d0.e0.p.d.m0.g.e eVar) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            return new b(eVar, this.f3345b);
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitClassLiteral(d0.e0.p.d.m0.g.e eVar, f fVar) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(fVar, "value");
            this.a.put(eVar, new r(fVar));
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitEnd() {
            this.d.add(new d(this.f3345b.getDefaultType(), this.a, this.e));
        }

        @Override // d0.e0.p.d.m0.e.b.p.a
        public void visitEnum(d0.e0.p.d.m0.g.e eVar, d0.e0.p.d.m0.g.a aVar, d0.e0.p.d.m0.g.e eVar2) {
            m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(aVar, "enumClassId");
            m.checkNotNullParameter(eVar2, "enumEntryName");
            this.a.put(eVar, new j(aVar, eVar2));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public c(c0 c0Var, d0 d0Var, o oVar, n nVar) {
        super(oVar, nVar);
        m.checkNotNullParameter(c0Var, "module");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(nVar, "kotlinClassFinder");
        this.c = c0Var;
        this.d = d0Var;
        this.e = new e(c0Var, d0Var);
    }

    @Override // d0.e0.p.d.m0.e.b.a
    public p.a h(d0.e0.p.d.m0.g.a aVar, u0 u0Var, List<d0.e0.p.d.m0.c.g1.c> list) {
        m.checkNotNullParameter(aVar, "annotationClassId");
        m.checkNotNullParameter(u0Var, "source");
        m.checkNotNullParameter(list, "result");
        return new a(w.findNonGenericClassAcrossDependencies(this.c, aVar, this.d), this, list, u0Var);
    }
}
