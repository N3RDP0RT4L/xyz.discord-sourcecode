package d0.e0.p.d.m0.e.b;

import andhook.lib.xposed.ClassUtils;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.functions.Function1;
/* compiled from: SignatureBuildingComponents.kt */
/* loaded from: classes3.dex */
public final class w {
    public static final w a = new w();

    /* compiled from: SignatureBuildingComponents.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<String, CharSequence> {
        public a() {
            super(1);
        }

        public final CharSequence invoke(String str) {
            m.checkNotNullParameter(str, "it");
            return w.this.a(str);
        }
    }

    public final String a(String str) {
        if (str.length() <= 1) {
            return str;
        }
        return 'L' + str + ';';
    }

    public final String[] constructors(String... strArr) {
        m.checkNotNullParameter(strArr, "signatures");
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String str : strArr) {
            arrayList.add("<init>(" + str + ")V");
        }
        Object[] array = arrayList.toArray(new String[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        return (String[]) array;
    }

    public final Set<String> inClass(String str, String... strArr) {
        m.checkNotNullParameter(str, "internalName");
        m.checkNotNullParameter(strArr, "signatures");
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (String str2 : strArr) {
            linkedHashSet.add(str + ClassUtils.PACKAGE_SEPARATOR_CHAR + str2);
        }
        return linkedHashSet;
    }

    public final Set<String> inJavaLang(String str, String... strArr) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(strArr, "signatures");
        String javaLang = javaLang(str);
        String[] strArr2 = new String[strArr.length];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        return inClass(javaLang, strArr2);
    }

    public final Set<String> inJavaUtil(String str, String... strArr) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(strArr, "signatures");
        String javaUtil = javaUtil(str);
        String[] strArr2 = new String[strArr.length];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        return inClass(javaUtil, strArr2);
    }

    public final String javaFunction(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return m.stringPlus("java/util/function/", str);
    }

    public final String javaLang(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return m.stringPlus("java/lang/", str);
    }

    public final String javaUtil(String str) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return m.stringPlus("java/util/", str);
    }

    public final String jvmDescriptor(String str, List<String> list, String str2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(list, "parameters");
        m.checkNotNullParameter(str2, "ret");
        return str + '(' + u.joinToString$default(list, "", null, null, 0, null, new a(), 30, null) + ')' + a(str2);
    }

    public final String signature(String str, String str2) {
        m.checkNotNullParameter(str, "internalName");
        m.checkNotNullParameter(str2, "jvmDescriptor");
        return str + ClassUtils.PACKAGE_SEPARATOR_CHAR + str2;
    }
}
