package d0.e0.p.d.m0.e.b;

import andhook.lib.xposed.ClassUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.e.b.s;
import d0.e0.p.d.m0.f.a0.a;
import d0.e0.p.d.m0.f.a0.b.e;
import d0.e0.p.d.m0.f.c;
import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.q;
import d0.e0.p.d.m0.f.z.g;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.i.n;
import d0.e0.p.d.m0.k.v.a0;
import d0.e0.p.d.m0.k.v.b0;
import d0.e0.p.d.m0.k.v.v;
import d0.e0.p.d.m0.k.v.x;
import d0.e0.p.d.m0.k.v.z;
import d0.e0.p.d.m0.l.b.y;
import d0.e0.p.d.m0.m.f;
import d0.e0.p.d.m0.m.h;
import d0.e0.p.d.m0.n.c0;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.jvm.functions.Function1;
/* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
/* loaded from: classes3.dex */
public abstract class a<A, C> implements d0.e0.p.d.m0.l.b.c<A, C> {
    public final n a;

    /* renamed from: b  reason: collision with root package name */
    public final h<p, b<A, C>> f3333b;

    /* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
    /* renamed from: d0.e0.p.d.m0.e.b.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public enum EnumC0310a {
        PROPERTY,
        BACKING_FIELD,
        DELEGATE_FIELD
    }

    /* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
    /* loaded from: classes3.dex */
    public static final class b<A, C> {
        public final Map<s, List<A>> a;

        /* renamed from: b  reason: collision with root package name */
        public final Map<s, C> f3334b;

        /* JADX WARN: Multi-variable type inference failed */
        public b(Map<s, ? extends List<? extends A>> map, Map<s, ? extends C> map2) {
            m.checkNotNullParameter(map, "memberAnnotations");
            m.checkNotNullParameter(map2, "propertyConstants");
            this.a = map;
            this.f3334b = map2;
        }

        public final Map<s, List<A>> getMemberAnnotations() {
            return this.a;
        }

        public final Map<s, C> getPropertyConstants() {
            return this.f3334b;
        }
    }

    /* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
    /* loaded from: classes3.dex */
    public static final class c implements p.c {
        public final /* synthetic */ a<A, C> a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ ArrayList<A> f3335b;

        public c(a<A, C> aVar, ArrayList<A> arrayList) {
            this.a = aVar;
            this.f3335b = arrayList;
        }

        @Override // d0.e0.p.d.m0.e.b.p.c
        public p.a visitAnnotation(d0.e0.p.d.m0.g.a aVar, u0 u0Var) {
            m.checkNotNullParameter(aVar, "classId");
            m.checkNotNullParameter(u0Var, "source");
            return a.access$loadAnnotationIfNotSpecial(this.a, aVar, u0Var, this.f3335b);
        }

        @Override // d0.e0.p.d.m0.e.b.p.c
        public void visitEnd() {
        }
    }

    /* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<p, b<? extends A, ? extends C>> {
        public final /* synthetic */ a<A, C> this$0;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(a<A, C> aVar) {
            super(1);
            this.this$0 = aVar;
        }

        public final b<A, C> invoke(p pVar) {
            m.checkNotNullParameter(pVar, "kotlinClass");
            return a.access$loadAnnotationsAndInitializers(this.this$0, pVar);
        }
    }

    public a(d0.e0.p.d.m0.m.o oVar, n nVar) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(nVar, "kotlinClassFinder");
        this.a = nVar;
        this.f3333b = oVar.createMemoizedFunction(new d(this));
    }

    public static final p.a access$loadAnnotationIfNotSpecial(a aVar, d0.e0.p.d.m0.g.a aVar2, u0 u0Var, List list) {
        Objects.requireNonNull(aVar);
        if (d0.e0.p.d.m0.a.a.getSPECIAL_ANNOTATIONS().contains(aVar2)) {
            return null;
        }
        return aVar.h(aVar2, u0Var, list);
    }

    public static final b access$loadAnnotationsAndInitializers(a aVar, p pVar) {
        Objects.requireNonNull(aVar);
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        d0.e0.p.d.m0.e.b.b bVar = new d0.e0.p.d.m0.e.b.b(aVar, hashMap, hashMap2);
        m.checkNotNullParameter(pVar, "kotlinClass");
        pVar.visitMembers(bVar, null);
        return new b(hashMap, hashMap2);
    }

    public static /* synthetic */ List b(a aVar, y yVar, s sVar, boolean z2, boolean z3, Boolean bool, boolean z4, int i, Object obj) {
        return aVar.a(yVar, sVar, (i & 4) != 0 ? false : z2, (i & 8) != 0 ? false : z3, (i & 16) != 0 ? null : bool, (i & 32) != 0 ? false : z4);
    }

    public static /* synthetic */ s d(a aVar, n nVar, d0.e0.p.d.m0.f.z.c cVar, g gVar, d0.e0.p.d.m0.l.b.b bVar, boolean z2, int i, Object obj) {
        return aVar.c(nVar, cVar, gVar, bVar, (i & 16) != 0 ? false : z2);
    }

    public static /* synthetic */ s f(a aVar, d0.e0.p.d.m0.f.n nVar, d0.e0.p.d.m0.f.z.c cVar, g gVar, boolean z2, boolean z3, boolean z4, int i, Object obj) {
        return aVar.e(nVar, cVar, gVar, (i & 8) != 0 ? false : z2, (i & 16) != 0 ? false : z3, (i & 32) != 0 ? true : z4);
    }

    public final List<A> a(y yVar, s sVar, boolean z2, boolean z3, Boolean bool, boolean z4) {
        p g = g(yVar, z2, z3, bool, z4);
        if (g == null) {
            g = yVar instanceof y.a ? j((y.a) yVar) : null;
        }
        if (g == null) {
            return d0.t.n.emptyList();
        }
        List<A> list = ((b) ((f.m) this.f3333b).invoke(g)).getMemberAnnotations().get(sVar);
        return list == null ? d0.t.n.emptyList() : list;
    }

    public final s c(n nVar, d0.e0.p.d.m0.f.z.c cVar, g gVar, d0.e0.p.d.m0.l.b.b bVar, boolean z2) {
        if (nVar instanceof d0.e0.p.d.m0.f.d) {
            s.a aVar = s.a;
            e.b jvmConstructorSignature = d0.e0.p.d.m0.f.a0.b.h.a.getJvmConstructorSignature((d0.e0.p.d.m0.f.d) nVar, cVar, gVar);
            if (jvmConstructorSignature == null) {
                return null;
            }
            return aVar.fromJvmMemberSignature(jvmConstructorSignature);
        } else if (nVar instanceof i) {
            s.a aVar2 = s.a;
            e.b jvmMethodSignature = d0.e0.p.d.m0.f.a0.b.h.a.getJvmMethodSignature((i) nVar, cVar, gVar);
            if (jvmMethodSignature == null) {
                return null;
            }
            return aVar2.fromJvmMemberSignature(jvmMethodSignature);
        } else if (!(nVar instanceof d0.e0.p.d.m0.f.n)) {
            return null;
        } else {
            g.f<d0.e0.p.d.m0.f.n, a.d> fVar = d0.e0.p.d.m0.f.a0.a.d;
            m.checkNotNullExpressionValue(fVar, "propertySignature");
            a.d dVar = (a.d) d0.e0.p.d.m0.f.z.e.getExtensionOrNull((g.d) nVar, fVar);
            if (dVar == null) {
                return null;
            }
            int ordinal = bVar.ordinal();
            if (ordinal == 1) {
                return e((d0.e0.p.d.m0.f.n) nVar, cVar, gVar, true, true, z2);
            }
            if (ordinal != 2) {
                if (ordinal != 3 || !dVar.hasSetter()) {
                    return null;
                }
                s.a aVar3 = s.a;
                a.c setter = dVar.getSetter();
                m.checkNotNullExpressionValue(setter, "signature.setter");
                return aVar3.fromMethod(cVar, setter);
            } else if (!dVar.hasGetter()) {
                return null;
            } else {
                s.a aVar4 = s.a;
                a.c getter = dVar.getGetter();
                m.checkNotNullExpressionValue(getter, "signature.getter");
                return aVar4.fromMethod(cVar, getter);
            }
        }
    }

    public final s e(d0.e0.p.d.m0.f.n nVar, d0.e0.p.d.m0.f.z.c cVar, d0.e0.p.d.m0.f.z.g gVar, boolean z2, boolean z3, boolean z4) {
        g.f<d0.e0.p.d.m0.f.n, a.d> fVar = d0.e0.p.d.m0.f.a0.a.d;
        m.checkNotNullExpressionValue(fVar, "propertySignature");
        a.d dVar = (a.d) d0.e0.p.d.m0.f.z.e.getExtensionOrNull(nVar, fVar);
        if (dVar == null) {
            return null;
        }
        if (z2) {
            e.a jvmFieldSignature = d0.e0.p.d.m0.f.a0.b.h.a.getJvmFieldSignature(nVar, cVar, gVar, z4);
            if (jvmFieldSignature == null) {
                return null;
            }
            return s.a.fromJvmMemberSignature(jvmFieldSignature);
        } else if (!z3 || !dVar.hasSyntheticMethod()) {
            return null;
        } else {
            s.a aVar = s.a;
            a.c syntheticMethod = dVar.getSyntheticMethod();
            m.checkNotNullExpressionValue(syntheticMethod, "signature.syntheticMethod");
            return aVar.fromMethod(cVar, syntheticMethod);
        }
    }

    public final p g(y yVar, boolean z2, boolean z3, Boolean bool, boolean z4) {
        y.a outerClass;
        c.EnumC0329c cVar = c.EnumC0329c.INTERFACE;
        if (z2) {
            if (bool != null) {
                if (yVar instanceof y.a) {
                    y.a aVar = (y.a) yVar;
                    if (aVar.getKind() == cVar) {
                        n nVar = this.a;
                        d0.e0.p.d.m0.g.a createNestedClassId = aVar.getClassId().createNestedClassId(d0.e0.p.d.m0.g.e.identifier("DefaultImpls"));
                        m.checkNotNullExpressionValue(createNestedClassId, "container.classId.createNestedClassId(Name.identifier(JvmAbi.DEFAULT_IMPLS_CLASS_NAME))");
                        return o.findKotlinClass(nVar, createNestedClassId);
                    }
                }
                if (bool.booleanValue() && (yVar instanceof y.b)) {
                    u0 source = yVar.getSource();
                    j jVar = source instanceof j ? (j) source : null;
                    d0.e0.p.d.m0.k.y.c facadeClassName = jVar == null ? null : jVar.getFacadeClassName();
                    if (facadeClassName != null) {
                        n nVar2 = this.a;
                        String internalName = facadeClassName.getInternalName();
                        m.checkNotNullExpressionValue(internalName, "facadeClassName.internalName");
                        d0.e0.p.d.m0.g.a aVar2 = d0.e0.p.d.m0.g.a.topLevel(new d0.e0.p.d.m0.g.b(t.replace$default(internalName, (char) MentionUtilsKt.SLASH_CHAR, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, false, 4, (Object) null)));
                        m.checkNotNullExpressionValue(aVar2, "topLevel(FqName(facadeClassName.internalName.replace('/', '.')))");
                        return o.findKotlinClass(nVar2, aVar2);
                    }
                }
            } else {
                throw new IllegalStateException(("isConst should not be null for property (container=" + yVar + ')').toString());
            }
        }
        if (z3 && (yVar instanceof y.a)) {
            y.a aVar3 = (y.a) yVar;
            if (aVar3.getKind() == c.EnumC0329c.COMPANION_OBJECT && (outerClass = aVar3.getOuterClass()) != null && (outerClass.getKind() == c.EnumC0329c.CLASS || outerClass.getKind() == c.EnumC0329c.ENUM_CLASS || (z4 && (outerClass.getKind() == cVar || outerClass.getKind() == c.EnumC0329c.ANNOTATION_CLASS)))) {
                return j(outerClass);
            }
        }
        if (!(yVar instanceof y.b) || !(yVar.getSource() instanceof j)) {
            return null;
        }
        u0 source2 = yVar.getSource();
        Objects.requireNonNull(source2, "null cannot be cast to non-null type org.jetbrains.kotlin.load.kotlin.JvmPackagePartSource");
        j jVar2 = (j) source2;
        p knownJvmBinaryClass = jVar2.getKnownJvmBinaryClass();
        return knownJvmBinaryClass == null ? o.findKotlinClass(this.a, jVar2.getClassId()) : knownJvmBinaryClass;
    }

    public abstract p.a h(d0.e0.p.d.m0.g.a aVar, u0 u0Var, List<A> list);

    public final List<A> i(y yVar, d0.e0.p.d.m0.f.n nVar, EnumC0310a aVar) {
        Boolean bool = d0.e0.p.d.m0.f.z.b.f3388z.get(nVar.getFlags());
        m.checkNotNullExpressionValue(bool, "IS_CONST.get(proto.flags)");
        boolean booleanValue = bool.booleanValue();
        d0.e0.p.d.m0.f.a0.b.h hVar = d0.e0.p.d.m0.f.a0.b.h.a;
        boolean isMovedFromInterfaceCompanion = d0.e0.p.d.m0.f.a0.b.h.isMovedFromInterfaceCompanion(nVar);
        if (aVar == EnumC0310a.PROPERTY) {
            s f = f(this, nVar, yVar.getNameResolver(), yVar.getTypeTable(), false, true, false, 40, null);
            return f == null ? d0.t.n.emptyList() : b(this, yVar, f, true, false, Boolean.valueOf(booleanValue), isMovedFromInterfaceCompanion, 8, null);
        }
        s f2 = f(this, nVar, yVar.getNameResolver(), yVar.getTypeTable(), true, false, false, 48, null);
        if (f2 == null) {
            return d0.t.n.emptyList();
        }
        boolean z2 = false;
        boolean contains$default = w.contains$default((CharSequence) f2.getSignature(), (CharSequence) "$delegate", false, 2, (Object) null);
        if (aVar == EnumC0310a.DELEGATE_FIELD) {
            z2 = true;
        }
        return contains$default != z2 ? d0.t.n.emptyList() : a(yVar, f2, true, true, Boolean.valueOf(booleanValue), isMovedFromInterfaceCompanion);
    }

    public final p j(y.a aVar) {
        u0 source = aVar.getSource();
        r rVar = source instanceof r ? (r) source : null;
        if (rVar == null) {
            return null;
        }
        return rVar.getBinaryClass();
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<A> loadCallableAnnotations(y yVar, n nVar, d0.e0.p.d.m0.l.b.b bVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        m.checkNotNullParameter(bVar, "kind");
        if (bVar == d0.e0.p.d.m0.l.b.b.PROPERTY) {
            return i(yVar, (d0.e0.p.d.m0.f.n) nVar, EnumC0310a.PROPERTY);
        }
        s d2 = d(this, nVar, yVar.getNameResolver(), yVar.getTypeTable(), bVar, false, 16, null);
        return d2 == null ? d0.t.n.emptyList() : b(this, yVar, d2, false, false, null, false, 60, null);
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<A> loadClassAnnotations(y.a aVar) {
        m.checkNotNullParameter(aVar, "container");
        p j = j(aVar);
        if (j != null) {
            ArrayList arrayList = new ArrayList(1);
            c cVar = new c(this, arrayList);
            m.checkNotNullParameter(j, "kotlinClass");
            j.loadClassAnnotations(cVar, null);
            return arrayList;
        }
        throw new IllegalStateException(m.stringPlus("Class for loading annotations is not found: ", aVar.debugFqName()).toString());
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<A> loadEnumEntryAnnotations(y yVar, d0.e0.p.d.m0.f.g gVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(gVar, "proto");
        s.a aVar = s.a;
        String string = yVar.getNameResolver().getString(gVar.getName());
        d0.e0.p.d.m0.f.a0.b.b bVar = d0.e0.p.d.m0.f.a0.b.b.a;
        String asString = ((y.a) yVar).getClassId().asString();
        m.checkNotNullExpressionValue(asString, "container as ProtoContainer.Class).classId.asString()");
        return b(this, yVar, aVar.fromFieldNameAndDesc(string, d0.e0.p.d.m0.f.a0.b.b.mapClass(asString)), false, false, null, false, 60, null);
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<A> loadExtensionReceiverParameterAnnotations(y yVar, n nVar, d0.e0.p.d.m0.l.b.b bVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        m.checkNotNullParameter(bVar, "kind");
        s d2 = d(this, nVar, yVar.getNameResolver(), yVar.getTypeTable(), bVar, false, 16, null);
        if (d2 != null) {
            return b(this, yVar, s.a.fromMethodSignatureAndParameterIndex(d2, 0), false, false, null, false, 60, null);
        }
        return d0.t.n.emptyList();
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<A> loadPropertyBackingFieldAnnotations(y yVar, d0.e0.p.d.m0.f.n nVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        return i(yVar, nVar, EnumC0310a.BACKING_FIELD);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // d0.e0.p.d.m0.l.b.c
    public C loadPropertyConstant(y yVar, d0.e0.p.d.m0.f.n nVar, c0 c0Var) {
        C c2;
        b0 b0Var;
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        m.checkNotNullParameter(c0Var, "expectedType");
        Boolean bool = d0.e0.p.d.m0.f.z.b.f3388z.get(nVar.getFlags());
        d0.e0.p.d.m0.f.a0.b.h hVar = d0.e0.p.d.m0.f.a0.b.h.a;
        p g = g(yVar, true, true, bool, d0.e0.p.d.m0.f.a0.b.h.isMovedFromInterfaceCompanion(nVar));
        if (g == null) {
            g = yVar instanceof y.a ? j((y.a) yVar) : null;
        }
        if (g == null) {
            return null;
        }
        s c3 = c(nVar, yVar.getNameResolver(), yVar.getTypeTable(), d0.e0.p.d.m0.l.b.b.PROPERTY, g.getClassHeader().getMetadataVersion().isAtLeast(f.a.getKOTLIN_1_3_RC_METADATA_VERSION$descriptors_jvm()));
        if (c3 == null || (c2 = ((b) ((f.m) this.f3333b).invoke(g)).getPropertyConstants().get(c3)) == null) {
            return null;
        }
        d0.e0.p.d.m0.b.o oVar = d0.e0.p.d.m0.b.o.a;
        if (!d0.e0.p.d.m0.b.o.isUnsignedType(c0Var)) {
            return c2;
        }
        C c4 = (C) ((d0.e0.p.d.m0.k.v.g) c2);
        m.checkNotNullParameter(c4, "constant");
        if (c4 instanceof d0.e0.p.d.m0.k.v.d) {
            b0Var = new x(((d0.e0.p.d.m0.k.v.d) c4).getValue().byteValue());
        } else if (c4 instanceof v) {
            b0Var = new a0(((v) c4).getValue().shortValue());
        } else if (c4 instanceof d0.e0.p.d.m0.k.v.m) {
            b0Var = new d0.e0.p.d.m0.k.v.y(((d0.e0.p.d.m0.k.v.m) c4).getValue().intValue());
        } else if (!(c4 instanceof d0.e0.p.d.m0.k.v.s)) {
            return c4;
        } else {
            b0Var = new z(((d0.e0.p.d.m0.k.v.s) c4).getValue().longValue());
        }
        return b0Var;
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<A> loadPropertyDelegateFieldAnnotations(y yVar, d0.e0.p.d.m0.f.n nVar) {
        m.checkNotNullParameter(yVar, "container");
        m.checkNotNullParameter(nVar, "proto");
        return i(yVar, nVar, EnumC0310a.DELEGATE_FIELD);
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<A> loadTypeAnnotations(q qVar, d0.e0.p.d.m0.f.z.c cVar) {
        m.checkNotNullParameter(qVar, "proto");
        m.checkNotNullParameter(cVar, "nameResolver");
        Object extension = qVar.getExtension(d0.e0.p.d.m0.f.a0.a.f);
        m.checkNotNullExpressionValue(extension, "proto.getExtension(JvmProtoBuf.typeAnnotation)");
        Iterable<d0.e0.p.d.m0.f.b> iterable = (Iterable) extension;
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(iterable, 10));
        for (d0.e0.p.d.m0.f.b bVar : iterable) {
            m.checkNotNullExpressionValue(bVar, "it");
            m.checkNotNullParameter(bVar, "proto");
            m.checkNotNullParameter(cVar, "nameResolver");
            arrayList.add(((d0.e0.p.d.m0.e.b.c) this).e.deserializeAnnotation(bVar, cVar));
        }
        return arrayList;
    }

    @Override // d0.e0.p.d.m0.l.b.c
    public List<A> loadTypeParameterAnnotations(d0.e0.p.d.m0.f.s sVar, d0.e0.p.d.m0.f.z.c cVar) {
        m.checkNotNullParameter(sVar, "proto");
        m.checkNotNullParameter(cVar, "nameResolver");
        Object extension = sVar.getExtension(d0.e0.p.d.m0.f.a0.a.h);
        m.checkNotNullExpressionValue(extension, "proto.getExtension(JvmProtoBuf.typeParameterAnnotation)");
        Iterable<d0.e0.p.d.m0.f.b> iterable = (Iterable) extension;
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(iterable, 10));
        for (d0.e0.p.d.m0.f.b bVar : iterable) {
            m.checkNotNullExpressionValue(bVar, "it");
            m.checkNotNullParameter(bVar, "proto");
            m.checkNotNullParameter(cVar, "nameResolver");
            arrayList.add(((d0.e0.p.d.m0.e.b.c) this).e.deserializeAnnotation(bVar, cVar));
        }
        return arrayList;
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0041, code lost:
        if (d0.e0.p.d.m0.f.z.f.hasReceiver((d0.e0.p.d.m0.f.n) r11) != false) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0059, code lost:
        if (r11.isInner() != false) goto L20;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x005c, code lost:
        r0 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x0034, code lost:
        if (d0.e0.p.d.m0.f.z.f.hasReceiver((d0.e0.p.d.m0.f.i) r11) != false) goto L20;
     */
    @Override // d0.e0.p.d.m0.l.b.c
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.util.List<A> loadValueParameterAnnotations(d0.e0.p.d.m0.l.b.y r10, d0.e0.p.d.m0.i.n r11, d0.e0.p.d.m0.l.b.b r12, int r13, d0.e0.p.d.m0.f.u r14) {
        /*
            r9 = this;
            java.lang.String r0 = "container"
            d0.z.d.m.checkNotNullParameter(r10, r0)
            java.lang.String r0 = "callableProto"
            d0.z.d.m.checkNotNullParameter(r11, r0)
            java.lang.String r0 = "kind"
            d0.z.d.m.checkNotNullParameter(r12, r0)
            java.lang.String r0 = "proto"
            d0.z.d.m.checkNotNullParameter(r14, r0)
            d0.e0.p.d.m0.f.z.c r3 = r10.getNameResolver()
            d0.e0.p.d.m0.f.z.g r4 = r10.getTypeTable()
            r6 = 0
            r7 = 16
            r8 = 0
            r1 = r9
            r2 = r11
            r5 = r12
            d0.e0.p.d.m0.e.b.s r12 = d(r1, r2, r3, r4, r5, r6, r7, r8)
            if (r12 == 0) goto L82
            boolean r14 = r11 instanceof d0.e0.p.d.m0.f.i
            r0 = 1
            if (r14 == 0) goto L37
            d0.e0.p.d.m0.f.i r11 = (d0.e0.p.d.m0.f.i) r11
            boolean r11 = d0.e0.p.d.m0.f.z.f.hasReceiver(r11)
            if (r11 == 0) goto L5c
            goto L5d
        L37:
            boolean r14 = r11 instanceof d0.e0.p.d.m0.f.n
            if (r14 == 0) goto L44
            d0.e0.p.d.m0.f.n r11 = (d0.e0.p.d.m0.f.n) r11
            boolean r11 = d0.e0.p.d.m0.f.z.f.hasReceiver(r11)
            if (r11 == 0) goto L5c
            goto L5d
        L44:
            boolean r14 = r11 instanceof d0.e0.p.d.m0.f.d
            if (r14 == 0) goto L72
            r11 = r10
            d0.e0.p.d.m0.l.b.y$a r11 = (d0.e0.p.d.m0.l.b.y.a) r11
            d0.e0.p.d.m0.f.c$c r14 = r11.getKind()
            d0.e0.p.d.m0.f.c$c r1 = d0.e0.p.d.m0.f.c.EnumC0329c.ENUM_CLASS
            if (r14 != r1) goto L55
            r0 = 2
            goto L5d
        L55:
            boolean r11 = r11.isInner()
            if (r11 == 0) goto L5c
            goto L5d
        L5c:
            r0 = 0
        L5d:
            int r13 = r13 + r0
            d0.e0.p.d.m0.e.b.s$a r11 = d0.e0.p.d.m0.e.b.s.a
            d0.e0.p.d.m0.e.b.s r2 = r11.fromMethodSignatureAndParameterIndex(r12, r13)
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 60
            r8 = 0
            r0 = r9
            r1 = r10
            java.util.List r10 = b(r0, r1, r2, r3, r4, r5, r6, r7, r8)
            return r10
        L72:
            java.lang.UnsupportedOperationException r10 = new java.lang.UnsupportedOperationException
            java.lang.Class r11 = r11.getClass()
            java.lang.String r12 = "Unsupported message: "
            java.lang.String r11 = d0.z.d.m.stringPlus(r12, r11)
            r10.<init>(r11)
            throw r10
        L82:
            java.util.List r10 = d0.t.n.emptyList()
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.m0.e.b.a.loadValueParameterAnnotations(d0.e0.p.d.m0.l.b.y, d0.e0.p.d.m0.i.n, d0.e0.p.d.m0.l.b.b, int, d0.e0.p.d.m0.f.u):java.util.List");
    }
}
