package d0.e0.p.d.m0.e.b.b0;

import d0.e0.p.d.m0.f.a0.b.c;
import d0.e0.p.d.m0.f.a0.b.f;
import d0.t.g0;
import d0.t.j;
import d0.t.n;
import d0.z.d.m;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: KotlinClassHeader.kt */
/* loaded from: classes3.dex */
public final class a {
    public final EnumC0312a a;

    /* renamed from: b  reason: collision with root package name */
    public final f f3338b;
    public final String[] c;
    public final String[] d;
    public final String[] e;
    public final String f;
    public final int g;

    /* compiled from: KotlinClassHeader.kt */
    /* renamed from: d0.e0.p.d.m0.e.b.b0.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public enum EnumC0312a {
        UNKNOWN(0),
        CLASS(1),
        FILE_FACADE(2),
        SYNTHETIC_CLASS(3),
        MULTIFILE_CLASS(4),
        MULTIFILE_CLASS_PART(5);
        
        public static final C0313a j = new C0313a(null);
        public static final Map<Integer, EnumC0312a> k;

        /* renamed from: id  reason: collision with root package name */
        private final int f3339id;

        /* compiled from: KotlinClassHeader.kt */
        /* renamed from: d0.e0.p.d.m0.e.b.b0.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0313a {
            public C0313a(DefaultConstructorMarker defaultConstructorMarker) {
            }

            public final EnumC0312a getById(int i) {
                EnumC0312a aVar = (EnumC0312a) EnumC0312a.k.get(Integer.valueOf(i));
                return aVar == null ? EnumC0312a.UNKNOWN : aVar;
            }
        }

        static {
            EnumC0312a[] values = values();
            LinkedHashMap linkedHashMap = new LinkedHashMap(d0.d0.f.coerceAtLeast(g0.mapCapacity(6), 16));
            for (int i = 0; i < 6; i++) {
                EnumC0312a aVar = values[i];
                linkedHashMap.put(Integer.valueOf(aVar.getId()), aVar);
            }
            k = linkedHashMap;
        }

        EnumC0312a(int i) {
            this.f3339id = i;
        }

        public static final EnumC0312a getById(int i) {
            return j.getById(i);
        }

        public final int getId() {
            return this.f3339id;
        }
    }

    public a(EnumC0312a aVar, f fVar, c cVar, String[] strArr, String[] strArr2, String[] strArr3, String str, int i, String str2) {
        m.checkNotNullParameter(aVar, "kind");
        m.checkNotNullParameter(fVar, "metadataVersion");
        m.checkNotNullParameter(cVar, "bytecodeVersion");
        this.a = aVar;
        this.f3338b = fVar;
        this.c = strArr;
        this.d = strArr2;
        this.e = strArr3;
        this.f = str;
        this.g = i;
    }

    public final boolean a(int i, int i2) {
        return (i & i2) != 0;
    }

    public final String[] getData() {
        return this.c;
    }

    public final String[] getIncompatibleData() {
        return this.d;
    }

    public final EnumC0312a getKind() {
        return this.a;
    }

    public final f getMetadataVersion() {
        return this.f3338b;
    }

    public final String getMultifileClassName() {
        String str = this.f;
        if (getKind() == EnumC0312a.MULTIFILE_CLASS_PART) {
            return str;
        }
        return null;
    }

    public final List<String> getMultifilePartNames() {
        String[] strArr = this.c;
        List<String> list = null;
        if (!(getKind() == EnumC0312a.MULTIFILE_CLASS)) {
            strArr = null;
        }
        if (strArr != null) {
            list = j.asList(strArr);
        }
        return list != null ? list : n.emptyList();
    }

    public final String[] getStrings() {
        return this.e;
    }

    public final boolean isPreRelease() {
        return a(this.g, 2);
    }

    public final boolean isUnstableFirBinary() {
        return a(this.g, 64) && !a(this.g, 32);
    }

    public final boolean isUnstableJvmIrBinary() {
        return a(this.g, 16) && !a(this.g, 32);
    }

    public String toString() {
        return this.a + " version=" + this.f3338b;
    }
}
