package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.b.q.g;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.d0;
import d0.e0.p.d.m0.c.h1.a;
import d0.e0.p.d.m0.c.h1.c;
import d0.e0.p.d.m0.d.b.c;
import d0.e0.p.d.m0.e.a.i0.f;
import d0.e0.p.d.m0.k.z.b;
import d0.e0.p.d.m0.l.b.i;
import d0.e0.p.d.m0.l.b.j;
import d0.e0.p.d.m0.l.b.k;
import d0.e0.p.d.m0.l.b.p;
import d0.e0.p.d.m0.l.b.t;
import d0.e0.p.d.m0.m.o;
import d0.e0.p.d.m0.n.l1.l;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
/* compiled from: DeserializationComponentsForJava.kt */
/* loaded from: classes3.dex */
public final class e {
    public final j a;

    public e(o oVar, c0 c0Var, k kVar, g gVar, c cVar, f fVar, d0 d0Var, p pVar, c cVar2, i iVar, l lVar) {
        m.checkNotNullParameter(oVar, "storageManager");
        m.checkNotNullParameter(c0Var, "moduleDescriptor");
        m.checkNotNullParameter(kVar, "configuration");
        m.checkNotNullParameter(gVar, "classDataFinder");
        m.checkNotNullParameter(cVar, "annotationAndConstantLoader");
        m.checkNotNullParameter(fVar, "packageFragmentProvider");
        m.checkNotNullParameter(d0Var, "notFoundClasses");
        m.checkNotNullParameter(pVar, "errorReporter");
        m.checkNotNullParameter(cVar2, "lookupTracker");
        m.checkNotNullParameter(iVar, "contractDeserializer");
        m.checkNotNullParameter(lVar, "kotlinTypeChecker");
        h builtIns = c0Var.getBuiltIns();
        g gVar2 = null;
        d0.e0.p.d.m0.b.q.f fVar2 = builtIns instanceof d0.e0.p.d.m0.b.q.f ? (d0.e0.p.d.m0.b.q.f) builtIns : null;
        t.a aVar = t.a.a;
        h hVar = h.a;
        List emptyList = n.emptyList();
        a customizer = fVar2 == null ? null : fVar2.getCustomizer();
        a aVar2 = customizer == null ? a.C0292a.a : customizer;
        gVar2 = fVar2 != null ? fVar2.getCustomizer() : gVar2;
        this.a = new j(oVar, c0Var, kVar, gVar, cVar, fVar, aVar, pVar, cVar2, hVar, emptyList, d0Var, iVar, aVar2, gVar2 == null ? c.b.a : gVar2, d0.e0.p.d.m0.f.a0.b.h.a.getEXTENSION_REGISTRY(), lVar, new b(oVar, n.emptyList()), null, 262144, null);
    }

    public final j getComponents() {
        return this.a;
    }
}
