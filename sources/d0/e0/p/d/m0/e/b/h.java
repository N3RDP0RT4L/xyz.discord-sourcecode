package d0.e0.p.d.m0.e.b;

import d0.e0.p.d.m0.e.a.i0.m.g;
import d0.e0.p.d.m0.f.a0.a;
import d0.e0.p.d.m0.l.b.q;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.d0;
import d0.e0.p.d.m0.n.j0;
import d0.e0.p.d.m0.n.t;
import d0.z.d.m;
/* compiled from: JavaFlexibleTypeDeserializer.kt */
/* loaded from: classes3.dex */
public final class h implements q {
    public static final h a = new h();

    @Override // d0.e0.p.d.m0.l.b.q
    public c0 create(d0.e0.p.d.m0.f.q qVar, String str, j0 j0Var, j0 j0Var2) {
        m.checkNotNullParameter(qVar, "proto");
        m.checkNotNullParameter(str, "flexibleId");
        m.checkNotNullParameter(j0Var, "lowerBound");
        m.checkNotNullParameter(j0Var2, "upperBound");
        if (!m.areEqual(str, "kotlin.jvm.PlatformType")) {
            j0 createErrorType = t.createErrorType("Error java flexible type with id: " + str + ". (" + j0Var + ".." + j0Var2 + ')');
            m.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Error java flexible type with id: $flexibleId. ($lowerBound..$upperBound)\")");
            return createErrorType;
        } else if (qVar.hasExtension(a.g)) {
            return new g(j0Var, j0Var2);
        } else {
            d0 d0Var = d0.a;
            return d0.flexibleType(j0Var, j0Var2);
        }
    }
}
