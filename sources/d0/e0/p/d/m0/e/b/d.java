package d0.e0.p.d.m0.e.b;

import andhook.lib.xposed.ClassUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.m0.b.h;
import d0.e0.p.d.m0.c.a;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.f;
import d0.e0.p.d.m0.c.l;
import d0.e0.p.d.m0.c.o0;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.g.g;
import d0.e0.p.d.m0.n.a0;
import d0.e0.p.d.m0.n.c0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.j1;
import d0.e0.p.d.m0.n.l1.p;
import d0.e0.p.d.m0.n.u;
import d0.e0.p.d.m0.n.u0;
import d0.e0.p.d.m0.n.w0;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
/* compiled from: descriptorBasedTypeSignatureMapping.kt */
/* loaded from: classes3.dex */
public final class d {
    public static final String computeInternalName(e eVar, x<?> xVar) {
        m.checkNotNullParameter(eVar, "klass");
        m.checkNotNullParameter(xVar, "typeMappingConfiguration");
        String predefinedFullInternalNameForClass = xVar.getPredefinedFullInternalNameForClass(eVar);
        if (predefinedFullInternalNameForClass != null) {
            return predefinedFullInternalNameForClass;
        }
        d0.e0.p.d.m0.c.m containingDeclaration = eVar.getContainingDeclaration();
        m.checkNotNullExpressionValue(containingDeclaration, "klass.containingDeclaration");
        String identifier = g.safeIdentifier(eVar.getName()).getIdentifier();
        m.checkNotNullExpressionValue(identifier, "safeIdentifier(klass.name).identifier");
        if (containingDeclaration instanceof e0) {
            b fqName = ((e0) containingDeclaration).getFqName();
            if (fqName.isRoot()) {
                return identifier;
            }
            StringBuilder sb = new StringBuilder();
            String asString = fqName.asString();
            m.checkNotNullExpressionValue(asString, "fqName.asString()");
            sb.append(t.replace$default(asString, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, (char) MentionUtilsKt.SLASH_CHAR, false, 4, (Object) null));
            sb.append(MentionUtilsKt.SLASH_CHAR);
            sb.append(identifier);
            return sb.toString();
        }
        e eVar2 = containingDeclaration instanceof e ? (e) containingDeclaration : null;
        if (eVar2 != null) {
            String predefinedInternalNameForClass = xVar.getPredefinedInternalNameForClass(eVar2);
            if (predefinedInternalNameForClass == null) {
                predefinedInternalNameForClass = computeInternalName(eVar2, xVar);
            }
            return predefinedInternalNameForClass + ClassUtils.INNER_CLASS_SEPARATOR_CHAR + identifier;
        }
        throw new IllegalArgumentException("Unexpected container: " + containingDeclaration + " for " + eVar);
    }

    public static /* synthetic */ String computeInternalName$default(e eVar, x xVar, int i, Object obj) {
        if ((i & 2) != 0) {
            xVar = y.a;
        }
        return computeInternalName(eVar, xVar);
    }

    public static final boolean hasVoidReturnType(a aVar) {
        m.checkNotNullParameter(aVar, "descriptor");
        if (aVar instanceof l) {
            return true;
        }
        c0 returnType = aVar.getReturnType();
        m.checkNotNull(returnType);
        if (h.isUnit(returnType)) {
            c0 returnType2 = aVar.getReturnType();
            m.checkNotNull(returnType2);
            if (!e1.isNullableType(returnType2) && !(aVar instanceof o0)) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r11v1, types: [T, java.lang.Object] */
    public static final <T> T mapType(c0 c0Var, l<T> lVar, z zVar, x<? extends T> xVar, i<T> iVar, Function3<? super c0, ? super T, ? super z, Unit> function3) {
        Object obj;
        c0 c0Var2;
        Object obj2;
        m.checkNotNullParameter(c0Var, "kotlinType");
        m.checkNotNullParameter(lVar, "factory");
        m.checkNotNullParameter(zVar, "mode");
        m.checkNotNullParameter(xVar, "typeMappingConfiguration");
        m.checkNotNullParameter(function3, "writeGenericType");
        c0 preprocessType = xVar.preprocessType(c0Var);
        if (preprocessType != null) {
            return (T) mapType(preprocessType, lVar, zVar, xVar, iVar, function3);
        }
        if (d0.e0.p.d.m0.b.g.isSuspendFunctionType(c0Var)) {
            return (T) mapType(d0.e0.p.d.m0.b.l.transformSuspendFunctionToRuntimeFunctionType(c0Var, xVar.releaseCoroutines()), lVar, zVar, xVar, iVar, function3);
        }
        p pVar = p.a;
        Object mapBuiltInType = a0.mapBuiltInType(pVar, c0Var, lVar, zVar);
        if (mapBuiltInType == null) {
            u0 constructor = c0Var.getConstructor();
            if (constructor instanceof a0) {
                a0 a0Var = (a0) constructor;
                c0 alternativeType = a0Var.getAlternativeType();
                if (alternativeType == null) {
                    alternativeType = xVar.commonSupertype(a0Var.getSupertypes());
                }
                return (T) mapType(d0.e0.p.d.m0.n.o1.a.replaceArgumentsWithStarProjections(alternativeType), lVar, zVar, xVar, iVar, function3);
            }
            d0.e0.p.d.m0.c.h declarationDescriptor = constructor.getDeclarationDescriptor();
            if (declarationDescriptor == null) {
                throw new UnsupportedOperationException(m.stringPlus("no descriptor for type constructor of ", c0Var));
            } else if (d0.e0.p.d.m0.n.t.isError(declarationDescriptor)) {
                T t = (T) lVar.createObjectType("error/NonExistentClass");
                xVar.processErrorType(c0Var, (e) declarationDescriptor);
                if (iVar == null) {
                    return t;
                }
                throw null;
            } else {
                boolean z2 = declarationDescriptor instanceof e;
                if (!z2 || !h.isArray(c0Var)) {
                    if (z2) {
                        if (d0.e0.p.d.m0.k.g.isInlineClass(declarationDescriptor) && !zVar.getNeedInlineClassWrapping() && (c0Var2 = (c0) u.computeExpandedTypeForInlineClass(pVar, c0Var)) != null) {
                            return (T) mapType(c0Var2, lVar, zVar.wrapInlineClassesMode(), xVar, iVar, function3);
                        }
                        if (!zVar.isForAnnotationParameter() || !h.isKClass((e) declarationDescriptor)) {
                            e eVar = (e) declarationDescriptor;
                            e original = eVar.getOriginal();
                            m.checkNotNullExpressionValue(original, "descriptor.original");
                            T predefinedTypeForClass = xVar.getPredefinedTypeForClass(original);
                            if (predefinedTypeForClass == null) {
                                if (eVar.getKind() == f.ENUM_ENTRY) {
                                    eVar = (e) eVar.getContainingDeclaration();
                                }
                                e original2 = eVar.getOriginal();
                                m.checkNotNullExpressionValue(original2, "enumClassIfEnumEntry.original");
                                obj = (Object) lVar.createObjectType(computeInternalName(original2, xVar));
                            } else {
                                obj = (Object) predefinedTypeForClass;
                            }
                        } else {
                            obj = (Object) lVar.getJavaLangClassType();
                        }
                        function3.invoke(c0Var, obj, zVar);
                        return (T) obj;
                    } else if (declarationDescriptor instanceof z0) {
                        T t2 = (T) mapType(d0.e0.p.d.m0.n.o1.a.getRepresentativeUpperBound((z0) declarationDescriptor), lVar, zVar, xVar, null, d0.e0.p.d.m0.p.d.getDO_NOTHING_3());
                        if (iVar == null) {
                            return t2;
                        }
                        m.checkNotNullExpressionValue(declarationDescriptor.getName(), "descriptor.getName()");
                        throw null;
                    } else if ((declarationDescriptor instanceof y0) && zVar.getMapTypeAliases()) {
                        return (T) mapType(((y0) declarationDescriptor).getExpandedType(), lVar, zVar, xVar, iVar, function3);
                    } else {
                        throw new UnsupportedOperationException(m.stringPlus("Unknown type ", c0Var));
                    }
                } else if (c0Var.getArguments().size() == 1) {
                    w0 w0Var = c0Var.getArguments().get(0);
                    c0 type = w0Var.getType();
                    m.checkNotNullExpressionValue(type, "memberProjection.type");
                    if (w0Var.getProjectionKind() == j1.IN_VARIANCE) {
                        obj2 = lVar.createObjectType("java/lang/Object");
                        if (iVar != null) {
                            throw null;
                        }
                    } else if (iVar == null) {
                        j1 projectionKind = w0Var.getProjectionKind();
                        m.checkNotNullExpressionValue(projectionKind, "memberProjection.projectionKind");
                        obj2 = mapType(type, lVar, zVar.toGenericArgumentMode(projectionKind, true), xVar, iVar, function3);
                        if (iVar != null) {
                            throw null;
                        }
                    } else {
                        throw null;
                    }
                    return (T) lVar.createFromString(m.stringPlus("[", lVar.toString(obj2)));
                } else {
                    throw new UnsupportedOperationException("arrays must have one type argument");
                }
            }
        } else {
            ?? r11 = (Object) a0.boxTypeIfNeeded(lVar, mapBuiltInType, zVar.getNeedPrimitiveBoxing());
            function3.invoke(c0Var, r11, zVar);
            return r11;
        }
    }

    public static /* synthetic */ Object mapType$default(c0 c0Var, l lVar, z zVar, x xVar, i iVar, Function3 function3, int i, Object obj) {
        if ((i & 32) != 0) {
            function3 = d0.e0.p.d.m0.p.d.getDO_NOTHING_3();
        }
        return mapType(c0Var, lVar, zVar, xVar, iVar, function3);
    }
}
