package d0.e0.p.d.m0;

import d0.e0.p.d.m0.e.a.a0;
import d0.e0.p.d.m0.g.b;
import d0.t.n;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
/* compiled from: SpecialJvmAnnotations.kt */
/* loaded from: classes3.dex */
public final class a {
    public static final a a = new a();

    /* renamed from: b  reason: collision with root package name */
    public static final Set<d0.e0.p.d.m0.g.a> f3180b;

    static {
        List<b> listOf = n.listOf((Object[]) new b[]{a0.a, a0.h, a0.i, a0.c, a0.d, a0.f});
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (b bVar : listOf) {
            linkedHashSet.add(d0.e0.p.d.m0.g.a.topLevel(bVar));
        }
        f3180b = linkedHashSet;
    }

    public final Set<d0.e0.p.d.m0.g.a> getSPECIAL_ANNOTATIONS() {
        return f3180b;
    }
}
