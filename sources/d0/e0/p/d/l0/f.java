package d0.e0.p.d.l0;

import d0.z.d.m;
/* compiled from: Caller.kt */
/* loaded from: classes3.dex */
public final class f {
    public static final int getArity(d<?> dVar) {
        m.checkNotNullParameter(dVar, "$this$arity");
        return dVar.getParameterTypes().size();
    }
}
