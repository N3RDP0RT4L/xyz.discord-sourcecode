package d0.e0.p.d.l0;

import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.a0;
import d0.g;
import d0.t.k;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KProperty;
import org.objectweb.asm.Opcodes;
/* compiled from: AnnotationConstructorCaller.kt */
/* loaded from: classes3.dex */
public final class b {

    /* compiled from: AnnotationConstructorCaller.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<Object, Boolean> {
        public final /* synthetic */ Class $annotationClass;
        public final /* synthetic */ List $methods;
        public final /* synthetic */ Map $values;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Class cls, List list, Map map) {
            super(1);
            this.$annotationClass = cls;
            this.$methods = list;
            this.$values = map;
        }

        @Override // kotlin.jvm.functions.Function1
        /* renamed from: invoke */
        public final Boolean invoke2(Object obj) {
            boolean z2;
            boolean z3;
            d0.e0.c annotationClass;
            Class cls = null;
            Annotation annotation = (Annotation) (!(obj instanceof Annotation) ? null : obj);
            if (!(annotation == null || (annotationClass = d0.z.a.getAnnotationClass(annotation)) == null)) {
                cls = d0.z.a.getJavaClass(annotationClass);
            }
            if (m.areEqual(cls, this.$annotationClass)) {
                List<Method> list = this.$methods;
                if (!(list instanceof Collection) || !list.isEmpty()) {
                    for (Method method : list) {
                        Object obj2 = this.$values.get(method.getName());
                        Object invoke = method.invoke(obj, new Object[0]);
                        if (obj2 instanceof boolean[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.BooleanArray");
                            z3 = Arrays.equals((boolean[]) obj2, (boolean[]) invoke);
                            continue;
                        } else if (obj2 instanceof char[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.CharArray");
                            z3 = Arrays.equals((char[]) obj2, (char[]) invoke);
                            continue;
                        } else if (obj2 instanceof byte[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.ByteArray");
                            z3 = Arrays.equals((byte[]) obj2, (byte[]) invoke);
                            continue;
                        } else if (obj2 instanceof short[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.ShortArray");
                            z3 = Arrays.equals((short[]) obj2, (short[]) invoke);
                            continue;
                        } else if (obj2 instanceof int[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.IntArray");
                            z3 = Arrays.equals((int[]) obj2, (int[]) invoke);
                            continue;
                        } else if (obj2 instanceof float[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.FloatArray");
                            z3 = Arrays.equals((float[]) obj2, (float[]) invoke);
                            continue;
                        } else if (obj2 instanceof long[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.LongArray");
                            z3 = Arrays.equals((long[]) obj2, (long[]) invoke);
                            continue;
                        } else if (obj2 instanceof double[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.DoubleArray");
                            z3 = Arrays.equals((double[]) obj2, (double[]) invoke);
                            continue;
                        } else if (obj2 instanceof Object[]) {
                            Objects.requireNonNull(invoke, "null cannot be cast to non-null type kotlin.Array<*>");
                            z3 = Arrays.equals((Object[]) obj2, (Object[]) invoke);
                            continue;
                        } else {
                            z3 = m.areEqual(obj2, invoke);
                            continue;
                        }
                        if (!z3) {
                            z2 = false;
                            break;
                        }
                    }
                }
                z2 = true;
                if (z2) {
                    return 1;
                }
            }
            return null;
        }
    }

    /* compiled from: AnnotationConstructorCaller.kt */
    /* renamed from: d0.e0.p.d.l0.b$b */
    /* loaded from: classes3.dex */
    public static final class C0280b extends o implements Function0<Integer> {
        public final /* synthetic */ Map $values;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0280b(Map map) {
            super(0);
            this.$values = map;
        }

        /* JADX WARN: Type inference failed for: r1v0 */
        /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Integer, int] */
        /* JADX WARN: Type inference failed for: r1v3 */
        @Override // kotlin.jvm.functions.Function0
        public final Integer invoke() {
            int i;
            ?? r1 = 0;
            for (Map.Entry entry : this.$values.entrySet()) {
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                if (value instanceof boolean[]) {
                    i = Arrays.hashCode((boolean[]) value);
                } else if (value instanceof char[]) {
                    i = Arrays.hashCode((char[]) value);
                } else if (value instanceof byte[]) {
                    i = Arrays.hashCode((byte[]) value);
                } else if (value instanceof short[]) {
                    i = Arrays.hashCode((short[]) value);
                } else if (value instanceof int[]) {
                    i = Arrays.hashCode((int[]) value);
                } else if (value instanceof float[]) {
                    i = Arrays.hashCode((float[]) value);
                } else if (value instanceof long[]) {
                    i = Arrays.hashCode((long[]) value);
                } else if (value instanceof double[]) {
                    i = Arrays.hashCode((double[]) value);
                } else {
                    i = value instanceof Object[] ? Arrays.hashCode((Object[]) value) : value.hashCode();
                }
                r1 += i ^ (str.hashCode() * Opcodes.LAND);
            }
            return r1;
        }
    }

    /* compiled from: AnnotationConstructorCaller.kt */
    /* loaded from: classes3.dex */
    public static final class c implements InvocationHandler {
        public final /* synthetic */ Class a;

        /* renamed from: b */
        public final /* synthetic */ Lazy f3175b;
        public final /* synthetic */ Lazy c;
        public final /* synthetic */ a d;
        public final /* synthetic */ Map e;

        public c(Class cls, Lazy lazy, KProperty kProperty, Lazy lazy2, KProperty kProperty2, a aVar, Map map) {
            this.a = cls;
            this.f3175b = lazy;
            this.c = lazy2;
            this.d = aVar;
            this.e = map;
        }

        @Override // java.lang.reflect.InvocationHandler
        public final Object invoke(Object obj, Method method, Object[] objArr) {
            m.checkNotNullExpressionValue(method, "method");
            String name = method.getName();
            if (name != null) {
                int hashCode = name.hashCode();
                if (hashCode != -1776922004) {
                    if (hashCode != 147696667) {
                        if (hashCode == 1444986633 && name.equals("annotationType")) {
                            return this.a;
                        }
                    } else if (name.equals("hashCode")) {
                        return this.c.getValue();
                    }
                } else if (name.equals("toString")) {
                    return this.f3175b.getValue();
                }
            }
            if (m.areEqual(name, "equals") && objArr != null && objArr.length == 1) {
                return Boolean.valueOf(this.d.invoke2(k.single(objArr)));
            }
            if (this.e.containsKey(name)) {
                return this.e.get(name);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Method is not supported: ");
            sb.append(method);
            sb.append(" (args: ");
            if (objArr == null) {
                objArr = new Object[0];
            }
            sb.append(k.toList(objArr));
            sb.append(')');
            throw new a0(sb.toString());
        }
    }

    /* compiled from: AnnotationConstructorCaller.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function0<String> {
        public final /* synthetic */ Class $annotationClass;
        public final /* synthetic */ Map $values;

        /* compiled from: AnnotationConstructorCaller.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function1<Map.Entry<? extends String, ? extends Object>, CharSequence> {
            public static final a j = new a();

            public a() {
                super(1);
            }

            /* renamed from: invoke */
            public final CharSequence invoke2(Map.Entry<String, ? extends Object> entry) {
                String str;
                m.checkNotNullParameter(entry, "entry");
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value instanceof boolean[]) {
                    str = Arrays.toString((boolean[]) value);
                } else if (value instanceof char[]) {
                    str = Arrays.toString((char[]) value);
                } else if (value instanceof byte[]) {
                    str = Arrays.toString((byte[]) value);
                } else if (value instanceof short[]) {
                    str = Arrays.toString((short[]) value);
                } else if (value instanceof int[]) {
                    str = Arrays.toString((int[]) value);
                } else if (value instanceof float[]) {
                    str = Arrays.toString((float[]) value);
                } else if (value instanceof long[]) {
                    str = Arrays.toString((long[]) value);
                } else if (value instanceof double[]) {
                    str = Arrays.toString((double[]) value);
                } else {
                    str = value instanceof Object[] ? Arrays.toString((Object[]) value) : value.toString();
                }
                return key + '=' + str;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ CharSequence invoke(Map.Entry<? extends String, ? extends Object> entry) {
                return invoke2((Map.Entry<String, ? extends Object>) entry);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(Class cls, Map map) {
            super(0);
            this.$annotationClass = cls;
            this.$values = map;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String invoke() {
            StringBuilder O = b.d.b.a.a.O(MentionUtilsKt.MENTIONS_CHAR);
            O.append(this.$annotationClass.getCanonicalName());
            u.joinTo$default(this.$values.entrySet(), O, ", ", "(", ")", 0, null, a.j, 48, null);
            String sb = O.toString();
            m.checkNotNullExpressionValue(sb, "StringBuilder().apply(builderAction).toString()");
            return sb;
        }
    }

    public static final Void access$throwIllegalArgumentType(int i, String str, Class cls) {
        d0.e0.c cVar;
        String str2;
        if (m.areEqual(cls, Class.class)) {
            cVar = d0.z.d.a0.getOrCreateKotlinClass(d0.e0.c.class);
        } else if (!cls.isArray() || !m.areEqual(cls.getComponentType(), Class.class)) {
            cVar = d0.z.a.getKotlinClass(cls);
        } else {
            cVar = d0.z.d.a0.getOrCreateKotlinClass(d0.e0.c[].class);
        }
        if (m.areEqual(cVar.getQualifiedName(), d0.z.d.a0.getOrCreateKotlinClass(Object[].class).getQualifiedName())) {
            StringBuilder sb = new StringBuilder();
            sb.append(cVar.getQualifiedName());
            sb.append('<');
            Class<?> componentType = d0.z.a.getJavaClass(cVar).getComponentType();
            m.checkNotNullExpressionValue(componentType, "kotlinClass.java.componentType");
            sb.append(d0.z.a.getKotlinClass(componentType).getQualifiedName());
            sb.append('>');
            str2 = sb.toString();
        } else {
            str2 = cVar.getQualifiedName();
        }
        throw new IllegalArgumentException("Argument #" + i + ' ' + str + " is not of the required type " + str2);
    }

    public static final Object access$transformKotlinToJvm(Object obj, Class cls) {
        if (obj instanceof Class) {
            return null;
        }
        if (obj instanceof d0.e0.c) {
            obj = d0.z.a.getJavaClass((d0.e0.c) obj);
        } else if (obj instanceof Object[]) {
            Object[] objArr = (Object[]) obj;
            if (objArr instanceof Class[]) {
                return null;
            }
            if (objArr instanceof d0.e0.c[]) {
                Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlin.Array<kotlin.reflect.KClass<*>>");
                d0.e0.c[] cVarArr = (d0.e0.c[]) obj;
                ArrayList arrayList = new ArrayList(cVarArr.length);
                for (d0.e0.c cVar : cVarArr) {
                    arrayList.add(d0.z.a.getJavaClass(cVar));
                }
                obj = arrayList.toArray(new Class[0]);
                Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlin.Array<T>");
            } else {
                obj = objArr;
            }
        }
        if (cls.isInstance(obj)) {
            return obj;
        }
        return null;
    }

    public static final <T> T createAnnotationInstance(Class<T> cls, Map<String, ? extends Object> map, List<Method> list) {
        m.checkNotNullParameter(cls, "annotationClass");
        m.checkNotNullParameter(map, "values");
        m.checkNotNullParameter(list, "methods");
        a aVar = new a(cls, list, map);
        Lazy lazy = g.lazy(new C0280b(map));
        T t = (T) Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new c(cls, g.lazy(new d(cls, map)), null, lazy, null, aVar, map));
        Objects.requireNonNull(t, "null cannot be cast to non-null type T");
        return t;
    }

    public static /* synthetic */ Object createAnnotationInstance$default(Class cls, Map map, List list, int i, Object obj) {
        if ((i & 4) != 0) {
            Set<String> keySet = map.keySet();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(keySet, 10));
            for (String str : keySet) {
                arrayList.add(cls.getDeclaredMethod(str, new Class[0]));
            }
            list = arrayList;
        }
        return createAnnotationInstance(cls, map, list);
    }
}
