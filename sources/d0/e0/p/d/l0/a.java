package d0.e0.p.d.l0;

import d0.e0.p.d.l0.d;
import d0.t.h0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
/* compiled from: AnnotationConstructorCaller.kt */
/* loaded from: classes3.dex */
public final class a implements d {
    public final List<Type> a;

    /* renamed from: b  reason: collision with root package name */
    public final List<Class<?>> f3174b;
    public final List<Object> c;
    public final Class<?> d;
    public final List<String> e;
    public final EnumC0279a f;
    public final List<Method> g;

    /* compiled from: AnnotationConstructorCaller.kt */
    /* renamed from: d0.e0.p.d.l0.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public enum EnumC0279a {
        CALL_BY_NAME,
        POSITIONAL_CALL
    }

    /* compiled from: AnnotationConstructorCaller.kt */
    /* loaded from: classes3.dex */
    public enum b {
        JAVA,
        KOTLIN
    }

    public a(Class<?> cls, List<String> list, EnumC0279a aVar, b bVar, List<Method> list2) {
        m.checkNotNullParameter(cls, "jClass");
        m.checkNotNullParameter(list, "parameterNames");
        m.checkNotNullParameter(aVar, "callMode");
        m.checkNotNullParameter(bVar, "origin");
        m.checkNotNullParameter(list2, "methods");
        this.d = cls;
        this.e = list;
        this.f = aVar;
        this.g = list2;
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list2, 10));
        for (Method method : list2) {
            arrayList.add(method.getGenericReturnType());
        }
        this.a = arrayList;
        List<Method> list3 = this.g;
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(list3, 10));
        for (Method method2 : list3) {
            Class<?> returnType = method2.getReturnType();
            m.checkNotNullExpressionValue(returnType, "it");
            Class<?> wrapperByPrimitive = d0.e0.p.d.m0.c.k1.b.b.getWrapperByPrimitive(returnType);
            if (wrapperByPrimitive != null) {
                returnType = wrapperByPrimitive;
            }
            arrayList2.add(returnType);
        }
        this.f3174b = arrayList2;
        List<Method> list4 = this.g;
        ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(list4, 10));
        for (Method method3 : list4) {
            arrayList3.add(method3.getDefaultValue());
        }
        this.c = arrayList3;
        if (this.f == EnumC0279a.POSITIONAL_CALL && bVar == b.JAVA && (!u.minus(this.e, "value").isEmpty())) {
            throw new UnsupportedOperationException("Positional call of a Java annotation constructor is allowed only if there are no parameters or one parameter named \"value\". This restriction exists because Java annotations (in contrast to Kotlin)do not impose any order on their arguments. Use KCallable#callBy instead.");
        }
    }

    @Override // d0.e0.p.d.l0.d
    public Object call(Object[] objArr) {
        m.checkNotNullParameter(objArr, "args");
        checkArguments(objArr);
        ArrayList arrayList = new ArrayList(objArr.length);
        int length = objArr.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            Object obj = objArr[i2];
            i++;
            Object access$transformKotlinToJvm = (obj == null && this.f == EnumC0279a.CALL_BY_NAME) ? this.c.get(i) : d0.e0.p.d.l0.b.access$transformKotlinToJvm(obj, this.f3174b.get(i));
            if (access$transformKotlinToJvm != null) {
                arrayList.add(access$transformKotlinToJvm);
            } else {
                d0.e0.p.d.l0.b.access$throwIllegalArgumentType(i, this.e.get(i), this.f3174b.get(i));
                throw null;
            }
        }
        return d0.e0.p.d.l0.b.createAnnotationInstance(this.d, h0.toMap(u.zip(this.e, arrayList)), this.g);
    }

    public void checkArguments(Object[] objArr) {
        m.checkNotNullParameter(objArr, "args");
        d.a.checkArguments(this, objArr);
    }

    @Override // d0.e0.p.d.l0.d
    public Void getMember() {
        return null;
    }

    @Override // d0.e0.p.d.l0.d
    public List<Type> getParameterTypes() {
        return this.a;
    }

    @Override // d0.e0.p.d.l0.d
    public Type getReturnType() {
        return this.d;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ a(java.lang.Class r7, java.util.List r8, d0.e0.p.d.l0.a.EnumC0279a r9, d0.e0.p.d.l0.a.b r10, java.util.List r11, int r12, kotlin.jvm.internal.DefaultConstructorMarker r13) {
        /*
            r6 = this;
            r12 = r12 & 16
            if (r12 == 0) goto L2a
            java.util.ArrayList r11 = new java.util.ArrayList
            r12 = 10
            int r12 = d0.t.o.collectionSizeOrDefault(r8, r12)
            r11.<init>(r12)
            java.util.Iterator r12 = r8.iterator()
        L13:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L2a
            java.lang.Object r13 = r12.next()
            java.lang.String r13 = (java.lang.String) r13
            r0 = 0
            java.lang.Class[] r0 = new java.lang.Class[r0]
            java.lang.reflect.Method r13 = r7.getDeclaredMethod(r13, r0)
            r11.add(r13)
            goto L13
        L2a:
            r5 = r11
            r0 = r6
            r1 = r7
            r2 = r8
            r3 = r9
            r4 = r10
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.a.<init>(java.lang.Class, java.util.List, d0.e0.p.d.l0.a$a, d0.e0.p.d.l0.a$b, java.util.List, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
