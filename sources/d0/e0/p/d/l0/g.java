package d0.e0.p.d.l0;

import d0.e0.p.d.j0;
import d0.z.d.m;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import kotlin.ranges.IntRange;
/* compiled from: InlineClassAwareCaller.kt */
/* loaded from: classes3.dex */
public final class g<M extends Member> implements d<M> {
    public final a a;

    /* renamed from: b  reason: collision with root package name */
    public final d<M> f3177b;
    public final boolean c;

    /* compiled from: InlineClassAwareCaller.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final IntRange a;

        /* renamed from: b  reason: collision with root package name */
        public final Method[] f3178b;
        public final Method c;

        public a(IntRange intRange, Method[] methodArr, Method method) {
            m.checkNotNullParameter(intRange, "argumentRange");
            m.checkNotNullParameter(methodArr, "unbox");
            this.a = intRange;
            this.f3178b = methodArr;
            this.c = method;
        }

        public final IntRange component1() {
            return this.a;
        }

        public final Method[] component2() {
            return this.f3178b;
        }

        public final Method component3() {
            return this.c;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x004f, code lost:
        if ((r9 instanceof d0.e0.p.d.l0.c) != false) goto L23;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public g(d0.e0.p.d.m0.c.b r8, d0.e0.p.d.l0.d<? extends M> r9, boolean r10) {
        /*
            Method dump skipped, instructions count: 395
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.g.<init>(d0.e0.p.d.m0.c.b, d0.e0.p.d.l0.d, boolean):void");
    }

    @Override // d0.e0.p.d.l0.d
    public Object call(Object[] objArr) {
        Object invoke;
        m.checkNotNullParameter(objArr, "args");
        a aVar = this.a;
        IntRange component1 = aVar.component1();
        Method[] component2 = aVar.component2();
        Method component3 = aVar.component3();
        Object[] copyOf = Arrays.copyOf(objArr, objArr.length);
        m.checkNotNullExpressionValue(copyOf, "java.util.Arrays.copyOf(this, size)");
        Objects.requireNonNull(copyOf, "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        int first = component1.getFirst();
        int last = component1.getLast();
        if (first <= last) {
            while (true) {
                Method method = component2[first];
                Object obj = objArr[first];
                if (method != null) {
                    if (obj != null) {
                        obj = method.invoke(obj, new Object[0]);
                    } else {
                        Class<?> returnType = method.getReturnType();
                        m.checkNotNullExpressionValue(returnType, "method.returnType");
                        obj = j0.defaultPrimitiveValue(returnType);
                    }
                }
                copyOf[first] = obj;
                if (first == last) {
                    break;
                }
                first++;
            }
        }
        Object call = this.f3177b.call(copyOf);
        return (component3 == null || (invoke = component3.invoke(null, call)) == null) ? call : invoke;
    }

    @Override // d0.e0.p.d.l0.d
    public M getMember() {
        return this.f3177b.getMember();
    }

    @Override // d0.e0.p.d.l0.d
    public List<Type> getParameterTypes() {
        return this.f3177b.getParameterTypes();
    }

    @Override // d0.e0.p.d.l0.d
    public Type getReturnType() {
        return this.f3177b.getReturnType();
    }
}
