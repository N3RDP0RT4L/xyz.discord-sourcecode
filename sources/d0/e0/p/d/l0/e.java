package d0.e0.p.d.l0;

import d0.e0.p.d.l0.d;
import d0.t.j;
import d0.t.k;
import d0.z.d.c0;
import d0.z.d.m;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: CallerImpl.kt */
/* loaded from: classes3.dex */
public abstract class e<M extends Member> implements d0.e0.p.d.l0.d<M> {
    public static final d a = new d(null);

    /* renamed from: b  reason: collision with root package name */
    public final List<Type> f3176b;
    public final M c;
    public final Type d;
    public final Class<?> e;

    /* compiled from: CallerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends e<Constructor<?>> implements d0.e0.p.d.l0.c {
        public final Object f;

        /* JADX WARN: Illegal instructions before constructor call */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v4, types: [java.lang.Object[], java.lang.Object] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public a(java.lang.reflect.Constructor<?> r8, java.lang.Object r9) {
            /*
                r7 = this;
                java.lang.String r0 = "constructor"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                java.lang.Class r3 = r8.getDeclaringClass()
                java.lang.String r0 = "constructor.declaringClass"
                d0.z.d.m.checkNotNullExpressionValue(r3, r0)
                java.lang.reflect.Type[] r0 = r8.getGenericParameterTypes()
                java.lang.String r1 = "constructor.genericParameterTypes"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                int r1 = r0.length
                r2 = 2
                if (r1 > r2) goto L1f
                r0 = 0
                java.lang.reflect.Type[] r0 = new java.lang.reflect.Type[r0]
                goto L2b
            L1f:
                int r1 = r0.length
                r2 = 1
                int r1 = r1 - r2
                java.lang.Object[] r0 = d0.t.j.copyOfRange(r0, r2, r1)
                java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
                java.util.Objects.requireNonNull(r0, r1)
            L2b:
                r5 = r0
                java.lang.reflect.Type[] r5 = (java.lang.reflect.Type[]) r5
                r6 = 0
                r4 = 0
                r1 = r7
                r2 = r8
                r1.<init>(r2, r3, r4, r5, r6)
                r7.f = r9
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.a.<init>(java.lang.reflect.Constructor, java.lang.Object):void");
        }

        @Override // d0.e0.p.d.l0.d
        public Object call(Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            checkArguments(objArr);
            c0 c0Var = new c0(3);
            c0Var.add(this.f);
            c0Var.addSpread(objArr);
            c0Var.add(null);
            return getMember().newInstance(c0Var.toArray(new Object[c0Var.size()]));
        }
    }

    /* compiled from: CallerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends e<Constructor<?>> {
        /* JADX WARN: Illegal instructions before constructor call */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v4, types: [java.lang.Object[], java.lang.Object] */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b(java.lang.reflect.Constructor<?> r8) {
            /*
                r7 = this;
                java.lang.String r0 = "constructor"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                java.lang.Class r3 = r8.getDeclaringClass()
                java.lang.String r0 = "constructor.declaringClass"
                d0.z.d.m.checkNotNullExpressionValue(r3, r0)
                java.lang.reflect.Type[] r0 = r8.getGenericParameterTypes()
                java.lang.String r1 = "constructor.genericParameterTypes"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                int r1 = r0.length
                r2 = 0
                r4 = 1
                if (r1 > r4) goto L1f
                java.lang.reflect.Type[] r0 = new java.lang.reflect.Type[r2]
                goto L2a
            L1f:
                int r1 = r0.length
                int r1 = r1 - r4
                java.lang.Object[] r0 = d0.t.j.copyOfRange(r0, r2, r1)
                java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
                java.util.Objects.requireNonNull(r0, r1)
            L2a:
                r5 = r0
                java.lang.reflect.Type[] r5 = (java.lang.reflect.Type[]) r5
                r6 = 0
                r4 = 0
                r1 = r7
                r2 = r8
                r1.<init>(r2, r3, r4, r5, r6)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.b.<init>(java.lang.reflect.Constructor):void");
        }

        @Override // d0.e0.p.d.l0.d
        public Object call(Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            checkArguments(objArr);
            c0 c0Var = new c0(2);
            c0Var.addSpread(objArr);
            c0Var.add(null);
            return getMember().newInstance(c0Var.toArray(new Object[c0Var.size()]));
        }
    }

    /* compiled from: CallerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class c extends e<Constructor<?>> implements d0.e0.p.d.l0.c {
        public final Object f;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public c(java.lang.reflect.Constructor<?> r8, java.lang.Object r9) {
            /*
                r7 = this;
                java.lang.String r0 = "constructor"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                java.lang.Class r3 = r8.getDeclaringClass()
                java.lang.String r0 = "constructor.declaringClass"
                d0.z.d.m.checkNotNullExpressionValue(r3, r0)
                java.lang.reflect.Type[] r5 = r8.getGenericParameterTypes()
                java.lang.String r0 = "constructor.genericParameterTypes"
                d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                r4 = 0
                r6 = 0
                r1 = r7
                r2 = r8
                r1.<init>(r2, r3, r4, r5, r6)
                r7.f = r9
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.c.<init>(java.lang.reflect.Constructor, java.lang.Object):void");
        }

        @Override // d0.e0.p.d.l0.d
        public Object call(Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            checkArguments(objArr);
            c0 c0Var = new c0(2);
            c0Var.add(this.f);
            c0Var.addSpread(objArr);
            return getMember().newInstance(c0Var.toArray(new Object[c0Var.size()]));
        }
    }

    /* compiled from: CallerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class d {
        public d(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: CallerImpl.kt */
    /* renamed from: d0.e0.p.d.l0.e$e  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0281e extends e<Constructor<?>> {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public C0281e(java.lang.reflect.Constructor<?> r8) {
            /*
                r7 = this;
                java.lang.String r0 = "constructor"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                java.lang.Class r3 = r8.getDeclaringClass()
                java.lang.String r0 = "constructor.declaringClass"
                d0.z.d.m.checkNotNullExpressionValue(r3, r0)
                java.lang.Class r0 = r8.getDeclaringClass()
                java.lang.String r1 = "klass"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                java.lang.Class r1 = r0.getDeclaringClass()
                if (r1 == 0) goto L29
                int r0 = r0.getModifiers()
                boolean r0 = java.lang.reflect.Modifier.isStatic(r0)
                if (r0 != 0) goto L29
                r4 = r1
                goto L2b
            L29:
                r0 = 0
                r4 = r0
            L2b:
                java.lang.reflect.Type[] r5 = r8.getGenericParameterTypes()
                java.lang.String r0 = "constructor.genericParameterTypes"
                d0.z.d.m.checkNotNullExpressionValue(r5, r0)
                r6 = 0
                r1 = r7
                r2 = r8
                r1.<init>(r2, r3, r4, r5, r6)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.C0281e.<init>(java.lang.reflect.Constructor):void");
        }

        @Override // d0.e0.p.d.l0.d
        public Object call(Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            checkArguments(objArr);
            return getMember().newInstance(Arrays.copyOf(objArr, objArr.length));
        }
    }

    /* compiled from: CallerImpl.kt */
    /* loaded from: classes3.dex */
    public static abstract class f extends e<Field> {

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends f implements d0.e0.p.d.l0.c {
            public final Object f;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(Field field, Object obj) {
                super(field, false, null);
                m.checkNotNullParameter(field, "field");
                this.f = obj;
            }

            @Override // d0.e0.p.d.l0.e.f, d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                return getMember().get(this.f);
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class b extends f implements d0.e0.p.d.l0.c {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public b(Field field) {
                super(field, false, null);
                m.checkNotNullParameter(field, "field");
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class c extends f {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public c(Field field) {
                super(field, true, null);
                m.checkNotNullParameter(field, "field");
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class d extends f {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public d(Field field) {
                super(field, true, null);
                m.checkNotNullParameter(field, "field");
            }

            @Override // d0.e0.p.d.l0.e
            public void checkArguments(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                e.super.checkArguments(objArr);
                a(k.firstOrNull(objArr));
            }
        }

        /* compiled from: CallerImpl.kt */
        /* renamed from: d0.e0.p.d.l0.e$f$e  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0282e extends f {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0282e(Field field) {
                super(field, false, null);
                m.checkNotNullParameter(field, "field");
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public f(java.lang.reflect.Field r7, boolean r8, kotlin.jvm.internal.DefaultConstructorMarker r9) {
            /*
                r6 = this;
                java.lang.reflect.Type r2 = r7.getGenericType()
                java.lang.String r9 = "field.genericType"
                d0.z.d.m.checkNotNullExpressionValue(r2, r9)
                if (r8 == 0) goto L10
                java.lang.Class r8 = r7.getDeclaringClass()
                goto L11
            L10:
                r8 = 0
            L11:
                r3 = r8
                r8 = 0
                java.lang.reflect.Type[] r4 = new java.lang.reflect.Type[r8]
                r5 = 0
                r0 = r6
                r1 = r7
                r0.<init>(r1, r2, r3, r4, r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.f.<init>(java.lang.reflect.Field, boolean, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        @Override // d0.e0.p.d.l0.d
        public Object call(Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            checkArguments(objArr);
            return getMember().get(getInstanceClass() != null ? k.first(objArr) : null);
        }
    }

    /* compiled from: CallerImpl.kt */
    /* loaded from: classes3.dex */
    public static abstract class g extends e<Field> {
        public final boolean f;

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends g implements d0.e0.p.d.l0.c {
            public final Object g;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(Field field, boolean z2, Object obj) {
                super(field, z2, false, null);
                m.checkNotNullParameter(field, "field");
                this.g = obj;
            }

            @Override // d0.e0.p.d.l0.e.g, d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                getMember().set(this.g, k.first(objArr));
                return Unit.a;
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class b extends g implements d0.e0.p.d.l0.c {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public b(Field field, boolean z2) {
                super(field, z2, false, null);
                m.checkNotNullParameter(field, "field");
            }

            @Override // d0.e0.p.d.l0.e.g, d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                getMember().set(null, k.last(objArr));
                return Unit.a;
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class c extends g {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public c(Field field, boolean z2) {
                super(field, z2, true, null);
                m.checkNotNullParameter(field, "field");
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class d extends g {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public d(Field field, boolean z2) {
                super(field, z2, true, null);
                m.checkNotNullParameter(field, "field");
            }

            @Override // d0.e0.p.d.l0.e.g, d0.e0.p.d.l0.e
            public void checkArguments(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                super.checkArguments(objArr);
                a(k.firstOrNull(objArr));
            }
        }

        /* compiled from: CallerImpl.kt */
        /* renamed from: d0.e0.p.d.l0.e$g$e  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0283e extends g {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0283e(Field field, boolean z2) {
                super(field, z2, false, null);
                m.checkNotNullParameter(field, "field");
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public g(java.lang.reflect.Field r7, boolean r8, boolean r9, kotlin.jvm.internal.DefaultConstructorMarker r10) {
            /*
                r6 = this;
                java.lang.Class r2 = java.lang.Void.TYPE
                java.lang.String r10 = "Void.TYPE"
                d0.z.d.m.checkNotNullExpressionValue(r2, r10)
                if (r9 == 0) goto Le
                java.lang.Class r9 = r7.getDeclaringClass()
                goto Lf
            Le:
                r9 = 0
            Lf:
                r3 = r9
                r9 = 1
                java.lang.reflect.Type[] r4 = new java.lang.reflect.Type[r9]
                r9 = 0
                java.lang.reflect.Type r10 = r7.getGenericType()
                java.lang.String r0 = "field.genericType"
                d0.z.d.m.checkNotNullExpressionValue(r10, r0)
                r4[r9] = r10
                r5 = 0
                r0 = r6
                r1 = r7
                r0.<init>(r1, r2, r3, r4, r5)
                r6.f = r8
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.g.<init>(java.lang.reflect.Field, boolean, boolean, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        @Override // d0.e0.p.d.l0.d
        public Object call(Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            checkArguments(objArr);
            getMember().set(getInstanceClass() != null ? k.first(objArr) : null, k.last(objArr));
            return Unit.a;
        }

        @Override // d0.e0.p.d.l0.e
        public void checkArguments(Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            e.super.checkArguments(objArr);
            if (this.f && k.last(objArr) == null) {
                throw new IllegalArgumentException("null is not allowed as a value for this property.");
            }
        }
    }

    /* compiled from: CallerImpl.kt */
    /* loaded from: classes3.dex */
    public static abstract class h extends e<Method> {
        public final boolean f;

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends h implements d0.e0.p.d.l0.c {
            public final Object g;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public a(Method method, Object obj) {
                super(method, false, (Type[]) null, 4);
                m.checkNotNullParameter(method, "method");
                this.g = obj;
            }

            @Override // d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                return b(this.g, objArr);
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class b extends h implements d0.e0.p.d.l0.c {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public b(Method method) {
                super(method, false, (Type[]) null, 4);
                m.checkNotNullParameter(method, "method");
            }

            @Override // d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                return b(null, objArr);
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class c extends h implements d0.e0.p.d.l0.c {
            public final Object g;

            /* JADX WARN: Illegal instructions before constructor call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public c(java.lang.reflect.Method r5, java.lang.Object r6) {
                /*
                    r4 = this;
                    java.lang.String r0 = "method"
                    d0.z.d.m.checkNotNullParameter(r5, r0)
                    java.lang.reflect.Type[] r0 = r5.getGenericParameterTypes()
                    java.lang.String r1 = "method.genericParameterTypes"
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    int r1 = r0.length
                    r2 = 1
                    r3 = 0
                    if (r1 > r2) goto L16
                    java.lang.reflect.Type[] r0 = new java.lang.reflect.Type[r3]
                    goto L20
                L16:
                    int r1 = r0.length
                    java.lang.Object[] r0 = d0.t.j.copyOfRange(r0, r2, r1)
                    java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
                    java.util.Objects.requireNonNull(r0, r1)
                L20:
                    java.lang.reflect.Type[] r0 = (java.lang.reflect.Type[]) r0
                    r1 = 0
                    r4.<init>(r5, r3, r0, r1)
                    r4.g = r6
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.h.c.<init>(java.lang.reflect.Method, java.lang.Object):void");
            }

            @Override // d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                c0 c0Var = new c0(2);
                c0Var.add(this.g);
                c0Var.addSpread(objArr);
                return b(null, c0Var.toArray(new Object[c0Var.size()]));
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class d extends h {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public d(Method method) {
                super(method, false, (Type[]) null, 6);
                m.checkNotNullParameter(method, "method");
            }

            @Override // d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                Object[] objArr2;
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                Object obj = objArr[0];
                if (objArr.length <= 1) {
                    objArr2 = new Object[0];
                } else {
                    objArr2 = j.copyOfRange(objArr, 1, objArr.length);
                    Objects.requireNonNull(objArr2, "null cannot be cast to non-null type kotlin.Array<T>");
                }
                return b(obj, objArr2);
            }
        }

        /* compiled from: CallerImpl.kt */
        /* renamed from: d0.e0.p.d.l0.e$h$e  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0284e extends h {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0284e(Method method) {
                super(method, true, (Type[]) null, 4);
                m.checkNotNullParameter(method, "method");
            }

            @Override // d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                Object[] objArr2;
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                a(k.firstOrNull(objArr));
                if (objArr.length <= 1) {
                    objArr2 = new Object[0];
                } else {
                    objArr2 = j.copyOfRange(objArr, 1, objArr.length);
                    Objects.requireNonNull(objArr2, "null cannot be cast to non-null type kotlin.Array<T>");
                }
                return b(null, objArr2);
            }
        }

        /* compiled from: CallerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class f extends h {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public f(Method method) {
                super(method, false, (Type[]) null, 6);
                m.checkNotNullParameter(method, "method");
            }

            @Override // d0.e0.p.d.l0.d
            public Object call(Object[] objArr) {
                m.checkNotNullParameter(objArr, "args");
                checkArguments(objArr);
                return b(null, objArr);
            }
        }

        public /* synthetic */ h(Method method, boolean z2, Type[] typeArr, DefaultConstructorMarker defaultConstructorMarker) {
            this(method, z2, typeArr);
        }

        public final Object b(Object obj, Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            return this.f ? Unit.a : getMember().invoke(obj, Arrays.copyOf(objArr, objArr.length));
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public /* synthetic */ h(java.lang.reflect.Method r1, boolean r2, java.lang.reflect.Type[] r3, int r4) {
            /*
                r0 = this;
                r3 = r4 & 2
                if (r3 == 0) goto Le
                int r2 = r1.getModifiers()
                boolean r2 = java.lang.reflect.Modifier.isStatic(r2)
                r2 = r2 ^ 1
            Le:
                r3 = r4 & 4
                if (r3 == 0) goto L1c
                java.lang.reflect.Type[] r3 = r1.getGenericParameterTypes()
                java.lang.String r4 = "method.genericParameterTypes"
                d0.z.d.m.checkNotNullExpressionValue(r3, r4)
                goto L1d
            L1c:
                r3 = 0
            L1d:
                r0.<init>(r1, r2, r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.h.<init>(java.lang.reflect.Method, boolean, java.lang.reflect.Type[], int):void");
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public h(java.lang.reflect.Method r7, boolean r8, java.lang.reflect.Type[] r9) {
            /*
                r6 = this;
                java.lang.reflect.Type r2 = r7.getGenericReturnType()
                java.lang.String r0 = "method.genericReturnType"
                d0.z.d.m.checkNotNullExpressionValue(r2, r0)
                if (r8 == 0) goto L10
                java.lang.Class r8 = r7.getDeclaringClass()
                goto L11
            L10:
                r8 = 0
            L11:
                r3 = r8
                r5 = 0
                r0 = r6
                r1 = r7
                r4 = r9
                r0.<init>(r1, r2, r3, r4, r5)
                java.lang.reflect.Type r7 = r6.getReturnType()
                java.lang.Class r8 = java.lang.Void.TYPE
                boolean r7 = d0.z.d.m.areEqual(r7, r8)
                r6.f = r7
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.h.<init>(java.lang.reflect.Method, boolean, java.lang.reflect.Type[]):void");
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:5:0x0027, code lost:
        if (r1 != null) goto L7;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public e(java.lang.reflect.Member r1, java.lang.reflect.Type r2, java.lang.Class r3, java.lang.reflect.Type[] r4, kotlin.jvm.internal.DefaultConstructorMarker r5) {
        /*
            r0 = this;
            r0.<init>()
            r0.c = r1
            r0.d = r2
            r0.e = r3
            if (r3 == 0) goto L2a
            d0.z.d.c0 r1 = new d0.z.d.c0
            r2 = 2
            r1.<init>(r2)
            r1.add(r3)
            r1.addSpread(r4)
            int r2 = r1.size()
            java.lang.reflect.Type[] r2 = new java.lang.reflect.Type[r2]
            java.lang.Object[] r1 = r1.toArray(r2)
            java.lang.reflect.Type[] r1 = (java.lang.reflect.Type[]) r1
            java.util.List r1 = d0.t.n.listOf(r1)
            if (r1 == 0) goto L2a
            goto L2e
        L2a:
            java.util.List r1 = d0.t.k.toList(r4)
        L2e:
            r0.f3176b = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.e.<init>(java.lang.reflect.Member, java.lang.reflect.Type, java.lang.Class, java.lang.reflect.Type[], kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final void a(Object obj) {
        if (obj == null || !this.c.getDeclaringClass().isInstance(obj)) {
            throw new IllegalArgumentException("An object member requires the object instance passed as the first argument.");
        }
    }

    public void checkArguments(Object[] objArr) {
        m.checkNotNullParameter(objArr, "args");
        d.a.checkArguments(this, objArr);
    }

    public final Class<?> getInstanceClass() {
        return this.e;
    }

    @Override // d0.e0.p.d.l0.d
    public final M getMember() {
        return this.c;
    }

    @Override // d0.e0.p.d.l0.d
    public List<Type> getParameterTypes() {
        return this.f3176b;
    }

    @Override // d0.e0.p.d.l0.d
    public final Type getReturnType() {
        return this.d;
    }
}
