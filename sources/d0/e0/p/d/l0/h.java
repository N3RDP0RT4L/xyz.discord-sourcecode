package d0.e0.p.d.l0;

import b.d.b.a.a;
import d0.e0.p.d.a0;
import d0.e0.p.d.j0;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.d1;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.l;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.k.g;
import d0.e0.p.d.m0.n.c0;
import java.lang.reflect.Method;
/* compiled from: InlineClassAwareCaller.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final c0 a(b bVar) {
        q0 extensionReceiverParameter = bVar.getExtensionReceiverParameter();
        q0 dispatchReceiverParameter = bVar.getDispatchReceiverParameter();
        if (extensionReceiverParameter != null) {
            return extensionReceiverParameter.getType();
        }
        if (dispatchReceiverParameter == null) {
            return null;
        }
        if (bVar instanceof l) {
            return dispatchReceiverParameter.getType();
        }
        m containingDeclaration = bVar.getContainingDeclaration();
        if (!(containingDeclaration instanceof e)) {
            containingDeclaration = null;
        }
        e eVar = (e) containingDeclaration;
        if (eVar != null) {
            return eVar.getDefaultType();
        }
        return null;
    }

    public static final Object coerceToExpectedReceiverType(Object obj, b bVar) {
        c0 a;
        Class<?> inlineClass;
        Method unboxMethod;
        d0.z.d.m.checkNotNullParameter(bVar, "descriptor");
        return (((bVar instanceof n0) && g.isUnderlyingPropertyOfInlineClass((d1) bVar)) || (a = a(bVar)) == null || (inlineClass = toInlineClass(a)) == null || (unboxMethod = getUnboxMethod(inlineClass, bVar)) == null) ? obj : unboxMethod.invoke(obj, new Object[0]);
    }

    /* JADX WARN: Code restructure failed: missing block: B:28:0x006d, code lost:
        if ((r0 != null && d0.e0.p.d.m0.k.g.isInlineClassType(r0)) != false) goto L29;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0072  */
    /* JADX WARN: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final <M extends java.lang.reflect.Member> d0.e0.p.d.l0.d<M> createInlineClassAwareCallerIfNeeded(d0.e0.p.d.l0.d<? extends M> r5, d0.e0.p.d.m0.c.b r6, boolean r7) {
        /*
            java.lang.String r0 = "$this$createInlineClassAwareCallerIfNeeded"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            java.lang.String r0 = "descriptor"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            boolean r0 = d0.e0.p.d.m0.k.g.isGetterOfUnderlyingPropertyOfInlineClass(r6)
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L6f
            java.util.List r0 = r6.getValueParameters()
            java.lang.String r3 = "descriptor.valueParameters"
            d0.z.d.m.checkNotNullExpressionValue(r0, r3)
            boolean r3 = r0 instanceof java.util.Collection
            if (r3 == 0) goto L27
            boolean r3 = r0.isEmpty()
            if (r3 == 0) goto L27
        L25:
            r0 = 0
            goto L4c
        L27:
            java.util.Iterator r0 = r0.iterator()
        L2b:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L25
            java.lang.Object r3 = r0.next()
            d0.e0.p.d.m0.c.c1 r3 = (d0.e0.p.d.m0.c.c1) r3
            java.lang.String r4 = "it"
            d0.z.d.m.checkNotNullExpressionValue(r3, r4)
            d0.e0.p.d.m0.n.c0 r3 = r3.getType()
            java.lang.String r4 = "it.type"
            d0.z.d.m.checkNotNullExpressionValue(r3, r4)
            boolean r3 = d0.e0.p.d.m0.k.g.isInlineClassType(r3)
            if (r3 == 0) goto L2b
            r0 = 1
        L4c:
            if (r0 != 0) goto L6f
            d0.e0.p.d.m0.n.c0 r0 = r6.getReturnType()
            if (r0 == 0) goto L5a
            boolean r0 = d0.e0.p.d.m0.k.g.isInlineClassType(r0)
            if (r0 == r2) goto L6f
        L5a:
            boolean r0 = r5 instanceof d0.e0.p.d.l0.c
            if (r0 != 0) goto L70
            d0.e0.p.d.m0.n.c0 r0 = a(r6)
            if (r0 == 0) goto L6c
            boolean r0 = d0.e0.p.d.m0.k.g.isInlineClassType(r0)
            if (r0 != r2) goto L6c
            r0 = 1
            goto L6d
        L6c:
            r0 = 0
        L6d:
            if (r0 == 0) goto L70
        L6f:
            r1 = 1
        L70:
            if (r1 == 0) goto L78
            d0.e0.p.d.l0.g r0 = new d0.e0.p.d.l0.g
            r0.<init>(r6, r5, r7)
            r5 = r0
        L78:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.l0.h.createInlineClassAwareCallerIfNeeded(d0.e0.p.d.l0.d, d0.e0.p.d.m0.c.b, boolean):d0.e0.p.d.l0.d");
    }

    public static /* synthetic */ d createInlineClassAwareCallerIfNeeded$default(d dVar, b bVar, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        return createInlineClassAwareCallerIfNeeded(dVar, bVar, z2);
    }

    public static final Method getBoxMethod(Class<?> cls, b bVar) {
        d0.z.d.m.checkNotNullParameter(cls, "$this$getBoxMethod");
        d0.z.d.m.checkNotNullParameter(bVar, "descriptor");
        try {
            Method declaredMethod = cls.getDeclaredMethod("box-impl", getUnboxMethod(cls, bVar).getReturnType());
            d0.z.d.m.checkNotNullExpressionValue(declaredMethod, "getDeclaredMethod(\"box\" …d(descriptor).returnType)");
            return declaredMethod;
        } catch (NoSuchMethodException unused) {
            throw new a0("No box method found in inline class: " + cls + " (calling " + bVar + ')');
        }
    }

    public static final Method getUnboxMethod(Class<?> cls, b bVar) {
        d0.z.d.m.checkNotNullParameter(cls, "$this$getUnboxMethod");
        d0.z.d.m.checkNotNullParameter(bVar, "descriptor");
        try {
            Method declaredMethod = cls.getDeclaredMethod("unbox-impl", new Class[0]);
            d0.z.d.m.checkNotNullExpressionValue(declaredMethod, "getDeclaredMethod(\"unbox…FOR_INLINE_CLASS_MEMBERS)");
            return declaredMethod;
        } catch (NoSuchMethodException unused) {
            throw new a0("No unbox method found in inline class: " + cls + " (calling " + bVar + ')');
        }
    }

    public static final Class<?> toInlineClass(c0 c0Var) {
        d0.z.d.m.checkNotNullParameter(c0Var, "$this$toInlineClass");
        return toInlineClass(c0Var.getConstructor().getDeclarationDescriptor());
    }

    public static final Class<?> toInlineClass(m mVar) {
        if (!(mVar instanceof e) || !g.isInlineClass(mVar)) {
            return null;
        }
        e eVar = (e) mVar;
        Class<?> javaClass = j0.toJavaClass(eVar);
        if (javaClass != null) {
            return javaClass;
        }
        StringBuilder R = a.R("Class object for the class ");
        R.append(eVar.getName());
        R.append(" cannot be found (classId=");
        R.append(d0.e0.p.d.m0.k.x.a.getClassId((d0.e0.p.d.m0.c.h) mVar));
        R.append(')');
        throw new a0(R.toString());
    }
}
