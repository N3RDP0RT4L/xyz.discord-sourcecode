package d0.e0.p.d.l0;

import d0.z.d.m;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.List;
/* compiled from: Caller.kt */
/* loaded from: classes3.dex */
public interface d<M extends Member> {

    /* compiled from: Caller.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static <M extends Member> void checkArguments(d<? extends M> dVar, Object[] objArr) {
            m.checkNotNullParameter(objArr, "args");
            if (f.getArity(dVar) != objArr.length) {
                StringBuilder R = b.d.b.a.a.R("Callable expects ");
                R.append(f.getArity(dVar));
                R.append(" arguments, but ");
                throw new IllegalArgumentException(b.d.b.a.a.A(R, objArr.length, " were provided."));
            }
        }
    }

    Object call(Object[] objArr);

    M getMember();

    List<Type> getParameterTypes();

    Type getReturnType();
}
