package d0.e0.p.d;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.f;
import d0.e0.p.d.c;
import d0.e0.p.d.c0;
import d0.e0.p.d.d;
import d0.e0.p.d.l0.a;
import d0.e0.p.d.l0.d;
import d0.e0.p.d.l0.e;
import d0.e0.p.d.l0.h;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.x;
import d0.z.d.a0;
import d0.z.d.i;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KFunction;
import kotlin.reflect.KProperty;
/* compiled from: KFunctionImpl.kt */
/* loaded from: classes3.dex */
public final class j extends f<Object> implements i<Object>, KFunction<Object>, d0.e0.p.d.c {
    public static final /* synthetic */ KProperty[] n = {a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "descriptor", "getDescriptor()Lorg/jetbrains/kotlin/descriptors/FunctionDescriptor;")), a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "caller", "getCaller()Lkotlin/reflect/jvm/internal/calls/Caller;")), a0.property1(new y(a0.getOrCreateKotlinClass(j.class), "defaultCaller", "getDefaultCaller()Lkotlin/reflect/jvm/internal/calls/Caller;"))};
    public final c0.a o;
    public final c0.b p;
    public final c0.b q;
    public final i r;

    /* renamed from: s  reason: collision with root package name */
    public final String f3172s;
    public final Object t;

    /* compiled from: KFunctionImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<d<? extends Member>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke */
        public final d<? extends Member> invoke2() {
            Object obj;
            d dVar;
            a.EnumC0279a aVar = a.EnumC0279a.POSITIONAL_CALL;
            d mapSignature = f0.f3170b.mapSignature(j.this.getDescriptor());
            if (mapSignature instanceof d.C0274d) {
                if (j.this.b()) {
                    Class<?> jClass = j.this.getContainer().getJClass();
                    List<f> parameters = j.this.getParameters();
                    ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(parameters, 10));
                    for (f fVar : parameters) {
                        String name = fVar.getName();
                        m.checkNotNull(name);
                        arrayList.add(name);
                    }
                    return new d0.e0.p.d.l0.a(jClass, arrayList, aVar, a.b.KOTLIN, null, 16, null);
                }
                obj = j.this.getContainer().findConstructorBySignature(((d.C0274d) mapSignature).getConstructorDesc());
            } else if (mapSignature instanceof d.e) {
                d.e eVar = (d.e) mapSignature;
                obj = j.this.getContainer().findMethodBySignature(eVar.getMethodName(), eVar.getMethodDesc());
            } else if (mapSignature instanceof d.c) {
                obj = ((d.c) mapSignature).getMethod();
            } else if (mapSignature instanceof d.b) {
                obj = ((d.b) mapSignature).getConstructor();
            } else if (mapSignature instanceof d.a) {
                List<Method> methods = ((d.a) mapSignature).getMethods();
                Class<?> jClass2 = j.this.getContainer().getJClass();
                ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(methods, 10));
                for (Method method : methods) {
                    m.checkNotNullExpressionValue(method, "it");
                    arrayList2.add(method.getName());
                }
                return new d0.e0.p.d.l0.a(jClass2, arrayList2, aVar, a.b.JAVA, methods);
            } else {
                throw new NoWhenBranchMatchedException();
            }
            if (obj instanceof Constructor) {
                j jVar = j.this;
                dVar = j.access$createConstructorCaller(jVar, (Constructor) obj, jVar.getDescriptor());
            } else if (obj instanceof Method) {
                Method method2 = (Method) obj;
                if (!Modifier.isStatic(method2.getModifiers())) {
                    dVar = j.access$createInstanceMethodCaller(j.this, method2);
                } else if (j.this.getDescriptor().getAnnotations().findAnnotation(j0.getJVM_STATIC()) != null) {
                    dVar = j.access$createJvmStaticInObjectCaller(j.this, method2);
                } else {
                    dVar = j.access$createStaticMethodCaller(j.this, method2);
                }
            } else {
                StringBuilder R = b.d.b.a.a.R("Could not compute caller for function: ");
                R.append(j.this.getDescriptor());
                R.append(" (member = ");
                R.append(obj);
                R.append(')');
                throw new a0(R.toString());
            }
            return h.createInlineClassAwareCallerIfNeeded$default(dVar, j.this.getDescriptor(), false, 2, null);
        }
    }

    /* compiled from: KFunctionImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<d0.e0.p.d.l0.d<? extends Member>> {
        public b() {
            super(0);
        }

        /* JADX WARN: Type inference failed for: r5v6, types: [java.lang.reflect.Member, java.lang.Object] */
        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke */
        public final d0.e0.p.d.l0.d<? extends Member> invoke2() {
            Object obj;
            d0.e0.p.d.l0.d dVar;
            ?? member;
            a.EnumC0279a aVar = a.EnumC0279a.CALL_BY_NAME;
            d mapSignature = f0.f3170b.mapSignature(j.this.getDescriptor());
            if (mapSignature instanceof d.e) {
                i container = j.this.getContainer();
                d.e eVar = (d.e) mapSignature;
                String methodName = eVar.getMethodName();
                String methodDesc = eVar.getMethodDesc();
                m.checkNotNull(j.this.getCaller().getMember());
                obj = container.findDefaultMethod(methodName, methodDesc, !Modifier.isStatic(member.getModifiers()));
            } else if (mapSignature instanceof d.C0274d) {
                if (j.this.b()) {
                    Class<?> jClass = j.this.getContainer().getJClass();
                    List<f> parameters = j.this.getParameters();
                    ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(parameters, 10));
                    for (f fVar : parameters) {
                        String name = fVar.getName();
                        m.checkNotNull(name);
                        arrayList.add(name);
                    }
                    return new d0.e0.p.d.l0.a(jClass, arrayList, aVar, a.b.KOTLIN, null, 16, null);
                }
                obj = j.this.getContainer().findDefaultConstructor(((d.C0274d) mapSignature).getConstructorDesc());
            } else if (mapSignature instanceof d.a) {
                List<Method> methods = ((d.a) mapSignature).getMethods();
                Class<?> jClass2 = j.this.getContainer().getJClass();
                ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(methods, 10));
                for (Method method : methods) {
                    m.checkNotNullExpressionValue(method, "it");
                    arrayList2.add(method.getName());
                }
                return new d0.e0.p.d.l0.a(jClass2, arrayList2, aVar, a.b.JAVA, methods);
            } else {
                obj = null;
            }
            if (obj instanceof Constructor) {
                j jVar = j.this;
                dVar = j.access$createConstructorCaller(jVar, (Constructor) obj, jVar.getDescriptor());
            } else if (obj instanceof Method) {
                if (j.this.getDescriptor().getAnnotations().findAnnotation(j0.getJVM_STATIC()) != null) {
                    d0.e0.p.d.m0.c.m containingDeclaration = j.this.getDescriptor().getContainingDeclaration();
                    Objects.requireNonNull(containingDeclaration, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor");
                    if (!((e) containingDeclaration).isCompanionObject()) {
                        dVar = j.access$createJvmStaticInObjectCaller(j.this, (Method) obj);
                    }
                }
                dVar = j.access$createStaticMethodCaller(j.this, (Method) obj);
            } else {
                dVar = null;
            }
            if (dVar != null) {
                return h.createInlineClassAwareCallerIfNeeded(dVar, j.this.getDescriptor(), true);
            }
            return null;
        }
    }

    /* compiled from: KFunctionImpl.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<x> {
        public final /* synthetic */ String $name;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(String str) {
            super(0);
            this.$name = str;
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final x invoke() {
            return j.this.getContainer().findFunctionDescriptor(this.$name, j.this.f3172s);
        }
    }

    public j(i iVar, String str, String str2, x xVar, Object obj) {
        this.r = iVar;
        this.f3172s = str2;
        this.t = obj;
        this.o = c0.lazySoft(xVar, new c(str));
        this.p = c0.lazy(new a());
        this.q = c0.lazy(new b());
    }

    public static final d0.e0.p.d.l0.e access$createConstructorCaller(j jVar, Constructor constructor, x xVar) {
        Objects.requireNonNull(jVar);
        if (d0.e0.p.d.m0.k.y.a.shouldHideConstructorDueToInlineClassTypeValueParameters(xVar)) {
            if (jVar.isBound()) {
                return new e.a(constructor, jVar.getBoundReceiver());
            }
            return new e.b(constructor);
        } else if (jVar.isBound()) {
            return new e.c(constructor, jVar.getBoundReceiver());
        } else {
            return new e.C0281e(constructor);
        }
    }

    public static final e.h access$createInstanceMethodCaller(j jVar, Method method) {
        return jVar.isBound() ? new e.h.a(method, jVar.getBoundReceiver()) : new e.h.d(method);
    }

    public static final e.h access$createJvmStaticInObjectCaller(j jVar, Method method) {
        return jVar.isBound() ? new e.h.b(method) : new e.h.C0284e(method);
    }

    public static final e.h access$createStaticMethodCaller(j jVar, Method method) {
        return jVar.isBound() ? new e.h.c(method, jVar.getBoundReceiver()) : new e.h.f(method);
    }

    public boolean equals(Object obj) {
        j asKFunctionImpl = j0.asKFunctionImpl(obj);
        return asKFunctionImpl != null && m.areEqual(getContainer(), asKFunctionImpl.getContainer()) && m.areEqual(getName(), asKFunctionImpl.getName()) && m.areEqual(this.f3172s, asKFunctionImpl.f3172s) && m.areEqual(this.t, asKFunctionImpl.t);
    }

    @Override // d0.z.d.i
    public int getArity() {
        return d0.e0.p.d.l0.f.getArity(getCaller());
    }

    public final Object getBoundReceiver() {
        return h.coerceToExpectedReceiverType(this.t, getDescriptor());
    }

    @Override // d0.e0.p.d.f
    public d0.e0.p.d.l0.d<?> getCaller() {
        return (d0.e0.p.d.l0.d) this.p.getValue(this, n[1]);
    }

    @Override // d0.e0.p.d.f
    public i getContainer() {
        return this.r;
    }

    @Override // d0.e0.p.d.f
    public d0.e0.p.d.l0.d<?> getDefaultCaller() {
        return (d0.e0.p.d.l0.d) this.q.getValue(this, n[2]);
    }

    @Override // d0.e0.p.d.f
    public x getDescriptor() {
        return (x) this.o.getValue(this, n[0]);
    }

    @Override // kotlin.reflect.KCallable
    public String getName() {
        String asString = getDescriptor().getName().asString();
        m.checkNotNullExpressionValue(asString, "descriptor.name.asString()");
        return asString;
    }

    public int hashCode() {
        int hashCode = getName().hashCode();
        return this.f3172s.hashCode() + ((hashCode + (getContainer().hashCode() * 31)) * 31);
    }

    @Override // kotlin.jvm.functions.Function0
    public Object invoke() {
        return c.a.invoke(this);
    }

    @Override // d0.e0.p.d.f
    public boolean isBound() {
        return !m.areEqual(this.t, d0.z.d.d.NO_RECEIVER);
    }

    @Override // kotlin.reflect.KFunction
    public boolean isExternal() {
        return getDescriptor().isExternal();
    }

    @Override // kotlin.reflect.KFunction
    public boolean isInfix() {
        return getDescriptor().isInfix();
    }

    @Override // kotlin.reflect.KFunction
    public boolean isInline() {
        return getDescriptor().isInline();
    }

    @Override // kotlin.reflect.KFunction
    public boolean isOperator() {
        return getDescriptor().isOperator();
    }

    @Override // kotlin.reflect.KCallable, kotlin.reflect.KFunction
    public boolean isSuspend() {
        return getDescriptor().isSuspend();
    }

    public String toString() {
        return e0.f3169b.renderFunction(getDescriptor());
    }

    @Override // kotlin.jvm.functions.Function1
    public Object invoke(Object obj) {
        return c.a.invoke(this, obj);
    }

    @Override // kotlin.jvm.functions.Function2
    public Object invoke(Object obj, Object obj2) {
        return c.a.invoke(this, obj, obj2);
    }

    @Override // kotlin.jvm.functions.Function3
    public Object invoke(Object obj, Object obj2, Object obj3) {
        return c.a.invoke(this, obj, obj2, obj3);
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public j(i iVar, String str, String str2, Object obj) {
        this(iVar, str, str2, null, obj);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "signature");
    }

    @Override // kotlin.jvm.functions.Function4
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4) {
        return c.a.invoke(this, obj, obj2, obj3, obj4);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public j(d0.e0.p.d.i r8, d0.e0.p.d.m0.c.x r9) {
        /*
            r7 = this;
            java.lang.String r0 = "container"
            d0.z.d.m.checkNotNullParameter(r8, r0)
            java.lang.String r0 = "descriptor"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            d0.e0.p.d.m0.g.e r0 = r9.getName()
            java.lang.String r3 = r0.asString()
            java.lang.String r0 = "descriptor.name.asString()"
            d0.z.d.m.checkNotNullExpressionValue(r3, r0)
            d0.e0.p.d.f0 r0 = d0.e0.p.d.f0.f3170b
            d0.e0.p.d.d r0 = r0.mapSignature(r9)
            java.lang.String r4 = r0.asString()
            java.lang.Object r6 = d0.z.d.d.NO_RECEIVER
            r1 = r7
            r2 = r8
            r5 = r9
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.j.<init>(d0.e0.p.d.i, d0.e0.p.d.m0.c.x):void");
    }

    @Override // kotlin.jvm.functions.Function5
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5);
    }

    @Override // kotlin.jvm.functions.Function6
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6);
    }

    @Override // kotlin.jvm.functions.Function7
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7);
    }

    @Override // kotlin.jvm.functions.Function8
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8);
    }

    @Override // kotlin.jvm.functions.Function9
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9);
    }

    @Override // kotlin.jvm.functions.Function10
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10);
    }

    @Override // kotlin.jvm.functions.Function11
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11);
    }

    @Override // kotlin.jvm.functions.Function12
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12);
    }

    @Override // kotlin.jvm.functions.Function13
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13);
    }

    @Override // kotlin.jvm.functions.Function14
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13, Object obj14) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14);
    }

    @Override // kotlin.jvm.functions.Function15
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13, Object obj14, Object obj15) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15);
    }

    @Override // kotlin.jvm.functions.Function16
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13, Object obj14, Object obj15, Object obj16) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16);
    }

    @Override // kotlin.jvm.functions.Function22
    public Object invoke(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13, Object obj14, Object obj15, Object obj16, Object obj17, Object obj18, Object obj19, Object obj20, Object obj21, Object obj22) {
        return c.a.invoke(this, obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16, obj17, obj18, obj19, obj20, obj21, obj22);
    }
}
