package d0.e0.p.d;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.m0.c.l;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.g.e;
import d0.z.d.m;
import java.util.Collection;
/* compiled from: EmptyContainerForLocal.kt */
/* loaded from: classes3.dex */
public final class b extends i {
    public static final b m = new b();

    @Override // d0.e0.p.d.i
    public Collection<l> getConstructorDescriptors() {
        j();
        throw null;
    }

    @Override // d0.e0.p.d.i
    public Collection<x> getFunctions(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        j();
        throw null;
    }

    @Override // d0.z.d.e
    public Class<?> getJClass() {
        j();
        throw null;
    }

    @Override // d0.e0.p.d.i
    public n0 getLocalProperty(int i) {
        return null;
    }

    @Override // d0.e0.p.d.i
    public Collection<n0> getProperties(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        j();
        throw null;
    }

    public final Void j() {
        throw new a0("Introspecting local functions, lambdas, anonymous functions, local variables and typealiases is not yet fully supported in Kotlin reflection");
    }
}
