package d0.e0.p.d;

import d0.z.d.m;
/* compiled from: KotlinReflectionInternalError.kt */
/* loaded from: classes3.dex */
public final class a0 extends Error {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public a0(String str) {
        super(str);
        m.checkNotNullParameter(str, "message");
    }
}
