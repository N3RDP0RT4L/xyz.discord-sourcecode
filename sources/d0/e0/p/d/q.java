package d0.e0.p.d;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.g;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.s;
import d0.i;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.reflect.Field;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
/* compiled from: KProperty1Impl.kt */
/* loaded from: classes3.dex */
public class q<T, V> extends s<V> implements g<T, V> {
    public final c0.b<a<T, V>> u;
    public final Lazy<Field> v = d0.g.lazy(i.PUBLICATION, new c());

    /* compiled from: KProperty1Impl.kt */
    /* loaded from: classes3.dex */
    public static final class a<T, V> extends s.c<V> implements g.a<T, V> {
        public final q<T, V> q;

        /* JADX WARN: Multi-variable type inference failed */
        public a(q<T, ? extends V> qVar) {
            m.checkNotNullParameter(qVar, "property");
            this.q = qVar;
        }

        @Override // d0.e0.p.d.s.a
        public q<T, V> getProperty() {
            return this.q;
        }

        @Override // kotlin.jvm.functions.Function1
        public V invoke(T t) {
            return getProperty().get(t);
        }
    }

    /* compiled from: KProperty1Impl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<a<T, ? extends V>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final a<T, V> invoke() {
            return new a<>(q.this);
        }
    }

    /* compiled from: KProperty1Impl.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<Field> {
        public c() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Field invoke() {
            return q.this.c();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public q(i iVar, String str, String str2, Object obj) {
        super(iVar, str, str2, obj);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "signature");
        c0.b<a<T, V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Getter(this) }");
        this.u = lazy;
    }

    @Override // d0.e0.g
    public V get(T t) {
        return getGetter().call(t);
    }

    @Override // kotlin.jvm.functions.Function1
    public V invoke(T t) {
        return get(t);
    }

    @Override // d0.e0.p.d.s, kotlin.reflect.KProperty0
    public a<T, V> getGetter() {
        a<T, V> invoke = this.u.invoke();
        m.checkNotNullExpressionValue(invoke, "_getter()");
        return invoke;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public q(i iVar, n0 n0Var) {
        super(iVar, n0Var);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(n0Var, "descriptor");
        c0.b<a<T, V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Getter(this) }");
        this.u = lazy;
    }
}
