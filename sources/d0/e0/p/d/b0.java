package d0.e0.p.d;

import d0.e0.p.d.m0.c.k1.a.k;
import d0.e0.p.d.m0.c.k1.b.b;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
/* compiled from: moduleByClassLoader.kt */
/* loaded from: classes3.dex */
public final class b0 {
    public static final ConcurrentMap<k0, WeakReference<k>> a = new ConcurrentHashMap();

    public static final k getOrCreateModule(Class<?> cls) {
        m.checkNotNullParameter(cls, "$this$getOrCreateModule");
        ClassLoader safeClassLoader = b.getSafeClassLoader(cls);
        k0 k0Var = new k0(safeClassLoader);
        ConcurrentMap<k0, WeakReference<k>> concurrentMap = a;
        WeakReference<k> weakReference = concurrentMap.get(k0Var);
        if (weakReference != null) {
            k kVar = weakReference.get();
            if (kVar != null) {
                m.checkNotNullExpressionValue(kVar, "it");
                return kVar;
            }
            concurrentMap.remove(k0Var, weakReference);
        }
        k create = k.a.create(safeClassLoader);
        while (true) {
            try {
                ConcurrentMap<k0, WeakReference<k>> concurrentMap2 = a;
                WeakReference<k> putIfAbsent = concurrentMap2.putIfAbsent(k0Var, new WeakReference<>(create));
                if (putIfAbsent == null) {
                    return create;
                }
                k kVar2 = putIfAbsent.get();
                if (kVar2 != null) {
                    return kVar2;
                }
                concurrentMap2.remove(k0Var, putIfAbsent);
            } finally {
                k0Var.setTemporaryStrongRef(null);
            }
        }
    }
}
