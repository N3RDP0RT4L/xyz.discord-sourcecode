package d0.e0.p.d;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.e;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.s;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.reflect.KMutableProperty$Setter;
/* compiled from: KProperty1Impl.kt */
/* loaded from: classes3.dex */
public final class l<T, V> extends q<T, V> implements e<T, V> {
    public final c0.b<a<T, V>> w;

    /* compiled from: KProperty1Impl.kt */
    /* loaded from: classes3.dex */
    public static final class a<T, V> extends s.d<V> implements KMutableProperty$Setter, Function2 {
        public final l<T, V> q;

        public a(l<T, V> lVar) {
            m.checkNotNullParameter(lVar, "property");
            this.q = lVar;
        }

        @Override // d0.e0.p.d.s.a
        public l<T, V> getProperty() {
            return this.q;
        }

        @Override // kotlin.jvm.functions.Function2
        public void invoke(T t, V v) {
            getProperty().set(t, v);
        }
    }

    /* compiled from: KProperty1Impl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<a<T, V>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final a<T, V> invoke() {
            return new a<>(l.this);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l(i iVar, String str, String str2, Object obj) {
        super(iVar, str, str2, obj);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "signature");
        c0.b<a<T, V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Setter(this) }");
        this.w = lazy;
    }

    public a<T, V> getSetter() {
        a<T, V> invoke = this.w.invoke();
        m.checkNotNullExpressionValue(invoke, "_setter()");
        return invoke;
    }

    public void set(T t, V v) {
        getSetter().call(t, v);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public l(i iVar, n0 n0Var) {
        super(iVar, n0Var);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(n0Var, "descriptor");
        c0.b<a<T, V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Setter(this) }");
        this.w = lazy;
    }
}
