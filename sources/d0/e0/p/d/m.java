package d0.e0.p.d;

import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.s;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.reflect.KMutableProperty$Setter;
import kotlin.reflect.KProperty;
/* compiled from: KProperty2Impl.kt */
/* loaded from: classes3.dex */
public final class m<D, E, V> extends r<D, E, V> implements KProperty, Function2 {
    public final c0.b<a<D, E, V>> w;

    /* compiled from: KProperty2Impl.kt */
    /* loaded from: classes3.dex */
    public static final class a<D, E, V> extends s.d<V> implements KMutableProperty$Setter, Function3 {
        public final m<D, E, V> q;

        public a(m<D, E, V> mVar) {
            d0.z.d.m.checkNotNullParameter(mVar, "property");
            this.q = mVar;
        }

        @Override // d0.e0.p.d.s.a
        public m<D, E, V> getProperty() {
            return this.q;
        }

        @Override // kotlin.jvm.functions.Function3
        public void invoke(D d, E e, V v) {
            getProperty().set(d, e, v);
        }
    }

    /* compiled from: KProperty2Impl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<a<D, E, V>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final a<D, E, V> invoke() {
            return new a<>(m.this);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public m(i iVar, n0 n0Var) {
        super(iVar, n0Var);
        d0.z.d.m.checkNotNullParameter(iVar, "container");
        d0.z.d.m.checkNotNullParameter(n0Var, "descriptor");
        c0.b<a<D, E, V>> lazy = c0.lazy(new b());
        d0.z.d.m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Setter(this) }");
        this.w = lazy;
    }

    public a<D, E, V> getSetter() {
        a<D, E, V> invoke = this.w.invoke();
        d0.z.d.m.checkNotNullExpressionValue(invoke, "_setter()");
        return invoke;
    }

    public void set(D d, E e, V v) {
        getSetter().call(d, e, v);
    }
}
