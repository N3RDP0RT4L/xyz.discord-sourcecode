package d0.e0.p.d;

import d0.z.d.m;
import java.lang.ref.WeakReference;
/* compiled from: moduleByClassLoader.kt */
/* loaded from: classes3.dex */
public final class k0 {
    public final WeakReference<ClassLoader> a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3173b;

    public k0(ClassLoader classLoader) {
        m.checkNotNullParameter(classLoader, "classLoader");
        this.a = new WeakReference<>(classLoader);
        this.f3173b = System.identityHashCode(classLoader);
    }

    public boolean equals(Object obj) {
        return (obj instanceof k0) && this.a.get() == ((k0) obj).a.get();
    }

    public int hashCode() {
        return this.f3173b;
    }

    public final void setTemporaryStrongRef(ClassLoader classLoader) {
    }

    public String toString() {
        String classLoader;
        ClassLoader classLoader2 = this.a.get();
        return (classLoader2 == null || (classLoader = classLoader2.toString()) == null) ? "<null>" : classLoader;
    }
}
