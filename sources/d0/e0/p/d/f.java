package d0.e0.p.d;

import andhook.lib.HookHelper;
import d0.e0.f;
import d0.e0.h;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.k0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.c.z;
import d0.e0.p.d.m0.c.z0;
import d0.t.k;
import d0.t.q;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KCallable;
import kotlin.reflect.KType;
import kotlin.reflect.KVisibility;
import kotlin.reflect.full.IllegalCallableAccessException;
/* compiled from: KCallableImpl.kt */
/* loaded from: classes3.dex */
public abstract class f<R> implements KCallable<R>, z {
    public final c0.a<List<Annotation>> j;
    public final c0.a<ArrayList<d0.e0.f>> k;
    public final c0.a<x> l;
    public final c0.a<List<y>> m;

    /* compiled from: KCallableImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends Annotation>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends Annotation> invoke() {
            return j0.computeAnnotations(f.this.getDescriptor());
        }
    }

    /* compiled from: KCallableImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<ArrayList<d0.e0.f>> {

        /* compiled from: Comparisons.kt */
        /* loaded from: classes3.dex */
        public static final class a<T> implements Comparator<T> {
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return d0.u.a.compareValues(((d0.e0.f) t).getName(), ((d0.e0.f) t2).getName());
            }
        }

        /* compiled from: KCallableImpl.kt */
        /* renamed from: d0.e0.p.d.f$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0275b extends o implements Function0<k0> {
            public final /* synthetic */ q0 $instanceReceiver;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0275b(q0 q0Var) {
                super(0);
                this.$instanceReceiver = q0Var;
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final k0 invoke() {
                return this.$instanceReceiver;
            }
        }

        /* compiled from: KCallableImpl.kt */
        /* loaded from: classes3.dex */
        public static final class c extends o implements Function0<k0> {
            public final /* synthetic */ q0 $extensionReceiver;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public c(q0 q0Var) {
                super(0);
                this.$extensionReceiver = q0Var;
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final k0 invoke() {
                return this.$extensionReceiver;
            }
        }

        /* compiled from: KCallableImpl.kt */
        /* loaded from: classes3.dex */
        public static final class d extends o implements Function0<k0> {
            public final /* synthetic */ d0.e0.p.d.m0.c.b $descriptor;
            public final /* synthetic */ int $i;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public d(d0.e0.p.d.m0.c.b bVar, int i) {
                super(0);
                this.$descriptor = bVar;
                this.$i = i;
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final k0 invoke() {
                c1 c1Var = this.$descriptor.getValueParameters().get(this.$i);
                m.checkNotNullExpressionValue(c1Var, "descriptor.valueParameters[i]");
                return c1Var;
            }
        }

        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final ArrayList<d0.e0.f> invoke() {
            int i;
            d0.e0.p.d.m0.c.b descriptor = f.this.getDescriptor();
            ArrayList<d0.e0.f> arrayList = new ArrayList<>();
            if (!f.this.isBound()) {
                q0 instanceReceiverParameter = j0.getInstanceReceiverParameter(descriptor);
                if (instanceReceiverParameter != null) {
                    arrayList.add(new o(f.this, 0, f.a.INSTANCE, new C0275b(instanceReceiverParameter)));
                    i = 1;
                } else {
                    i = 0;
                }
                q0 extensionReceiverParameter = descriptor.getExtensionReceiverParameter();
                if (extensionReceiverParameter != null) {
                    i++;
                    arrayList.add(new o(f.this, i, f.a.EXTENSION_RECEIVER, new c(extensionReceiverParameter)));
                }
            } else {
                i = 0;
            }
            List<c1> valueParameters = descriptor.getValueParameters();
            m.checkNotNullExpressionValue(valueParameters, "descriptor.valueParameters");
            int size = valueParameters.size();
            for (int i2 = 0; i2 < size; i2++) {
                i++;
                arrayList.add(new o(f.this, i, f.a.VALUE, new d(descriptor, i2)));
            }
            if (f.this.b() && (descriptor instanceof d0.e0.p.d.m0.e.a.h0.b) && arrayList.size() > 1) {
                q.sortWith(arrayList, new a());
            }
            arrayList.trimToSize();
            return arrayList;
        }
    }

    /* compiled from: KCallableImpl.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<x> {

        /* compiled from: KCallableImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<Type> {
            public a() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public final Type invoke() {
                Type access$extractContinuationArgument = f.access$extractContinuationArgument(f.this);
                return access$extractContinuationArgument != null ? access$extractContinuationArgument : f.this.getCaller().getReturnType();
            }
        }

        public c() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final x invoke() {
            d0.e0.p.d.m0.n.c0 returnType = f.this.getDescriptor().getReturnType();
            m.checkNotNull(returnType);
            m.checkNotNullExpressionValue(returnType, "descriptor.returnType!!");
            return new x(returnType, new a());
        }
    }

    /* compiled from: KCallableImpl.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function0<List<? extends y>> {
        public d() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends y> invoke() {
            List<z0> typeParameters = f.this.getDescriptor().getTypeParameters();
            m.checkNotNullExpressionValue(typeParameters, "descriptor.typeParameters");
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(typeParameters, 10));
            for (z0 z0Var : typeParameters) {
                f fVar = f.this;
                m.checkNotNullExpressionValue(z0Var, "descriptor");
                arrayList.add(new y(fVar, z0Var));
            }
            return arrayList;
        }
    }

    public f() {
        c0.a<List<Annotation>> lazySoft = c0.lazySoft(new a());
        m.checkNotNullExpressionValue(lazySoft, "ReflectProperties.lazySo…or.computeAnnotations() }");
        this.j = lazySoft;
        c0.a<ArrayList<d0.e0.f>> lazySoft2 = c0.lazySoft(new b());
        m.checkNotNullExpressionValue(lazySoft2, "ReflectProperties.lazySo…ze()\n        result\n    }");
        this.k = lazySoft2;
        c0.a<x> lazySoft3 = c0.lazySoft(new c());
        m.checkNotNullExpressionValue(lazySoft3, "ReflectProperties.lazySo…eturnType\n        }\n    }");
        this.l = lazySoft3;
        c0.a<List<y>> lazySoft4 = c0.lazySoft(new d());
        m.checkNotNullExpressionValue(lazySoft4, "ReflectProperties.lazySo…this, descriptor) }\n    }");
        this.m = lazySoft4;
    }

    public static final Type access$extractContinuationArgument(f fVar) {
        Type[] lowerBounds;
        d0.e0.p.d.m0.c.b descriptor = fVar.getDescriptor();
        if (!(descriptor instanceof x)) {
            descriptor = null;
        }
        x xVar = (x) descriptor;
        if (xVar == null || !xVar.isSuspend()) {
            return null;
        }
        Object lastOrNull = u.lastOrNull((List<? extends Object>) fVar.getCaller().getParameterTypes());
        if (!(lastOrNull instanceof ParameterizedType)) {
            lastOrNull = null;
        }
        ParameterizedType parameterizedType = (ParameterizedType) lastOrNull;
        if (!m.areEqual(parameterizedType != null ? parameterizedType.getRawType() : null, Continuation.class)) {
            return null;
        }
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        m.checkNotNullExpressionValue(actualTypeArguments, "continuationType.actualTypeArguments");
        Object single = k.single(actualTypeArguments);
        if (!(single instanceof WildcardType)) {
            single = null;
        }
        WildcardType wildcardType = (WildcardType) single;
        if (wildcardType == null || (lowerBounds = wildcardType.getLowerBounds()) == null) {
            return null;
        }
        return (Type) k.first(lowerBounds);
    }

    public final Object a(KType kType) {
        Class javaClass = d0.z.a.getJavaClass(d0.e0.p.a.getJvmErasure(kType));
        if (javaClass.isArray()) {
            Object newInstance = Array.newInstance(javaClass.getComponentType(), 0);
            m.checkNotNullExpressionValue(newInstance, "type.jvmErasure.java.run…\"\n            )\n        }");
            return newInstance;
        }
        StringBuilder R = b.d.b.a.a.R("Cannot instantiate the default empty array of type ");
        R.append(javaClass.getSimpleName());
        R.append(", because it is not an array type");
        throw new a0(R.toString());
    }

    public final boolean b() {
        return m.areEqual(getName(), HookHelper.constructorName) && getContainer().getJClass().isAnnotation();
    }

    @Override // kotlin.reflect.KCallable
    public R call(Object... objArr) {
        m.checkNotNullParameter(objArr, "args");
        try {
            return (R) getCaller().call(objArr);
        } catch (IllegalAccessException e) {
            throw new IllegalCallableAccessException(e);
        }
    }

    @Override // kotlin.reflect.KCallable
    public R callBy(Map<d0.e0.f, ? extends Object> map) {
        Object obj;
        m.checkNotNullParameter(map, "args");
        if (!b()) {
            return callDefaultMethod$kotlin_reflection(map, null);
        }
        List<d0.e0.f> parameters = getParameters();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(parameters, 10));
        for (d0.e0.f fVar : parameters) {
            if (map.containsKey(fVar)) {
                obj = map.get(fVar);
                if (obj == null) {
                    throw new IllegalArgumentException("Annotation argument value cannot be null (" + fVar + ')');
                }
            } else if (fVar.isOptional()) {
                obj = null;
            } else if (fVar.isVararg()) {
                obj = a(fVar.getType());
            } else {
                throw new IllegalArgumentException("No argument provided for a required parameter: " + fVar);
            }
            arrayList.add(obj);
        }
        d0.e0.p.d.l0.d<?> defaultCaller = getDefaultCaller();
        if (defaultCaller != null) {
            try {
                Object[] array = arrayList.toArray(new Object[0]);
                if (array != null) {
                    return (R) defaultCaller.call(array);
                }
                throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T>");
            } catch (IllegalAccessException e) {
                throw new IllegalCallableAccessException(e);
            }
        } else {
            StringBuilder R = b.d.b.a.a.R("This callable does not support a default call: ");
            R.append(getDescriptor());
            throw new a0(R.toString());
        }
    }

    public final R callDefaultMethod$kotlin_reflection(Map<d0.e0.f, ? extends Object> map, Continuation<?> continuation) {
        m.checkNotNullParameter(map, "args");
        List<d0.e0.f> parameters = getParameters();
        ArrayList arrayList = new ArrayList(parameters.size());
        ArrayList arrayList2 = new ArrayList(1);
        Iterator<d0.e0.f> it = parameters.iterator();
        int i = 0;
        boolean z2 = false;
        int i2 = 0;
        while (true) {
            Object obj = null;
            if (it.hasNext()) {
                d0.e0.f next = it.next();
                if (i != 0 && i % 32 == 0) {
                    arrayList2.add(Integer.valueOf(i2));
                    i2 = 0;
                }
                if (map.containsKey(next)) {
                    arrayList.add(map.get(next));
                } else if (next.isOptional()) {
                    if (!j0.isInlineClassType(next.getType())) {
                        obj = j0.defaultPrimitiveValue(d0.e0.p.b.getJavaType(next.getType()));
                    }
                    arrayList.add(obj);
                    i2 = (1 << (i % 32)) | i2;
                    z2 = true;
                } else if (next.isVararg()) {
                    arrayList.add(a(next.getType()));
                } else {
                    throw new IllegalArgumentException("No argument provided for a required parameter: " + next);
                }
                if (next.getKind() == f.a.VALUE) {
                    i++;
                }
            } else {
                if (continuation != null) {
                    arrayList.add(continuation);
                }
                if (!z2) {
                    Object[] array = arrayList.toArray(new Object[0]);
                    Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
                    return call(Arrays.copyOf(array, array.length));
                }
                arrayList2.add(Integer.valueOf(i2));
                d0.e0.p.d.l0.d<?> defaultCaller = getDefaultCaller();
                if (defaultCaller != null) {
                    arrayList.addAll(arrayList2);
                    arrayList.add(null);
                    try {
                        Object[] array2 = arrayList.toArray(new Object[0]);
                        if (array2 != null) {
                            return (R) defaultCaller.call(array2);
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T>");
                    } catch (IllegalAccessException e) {
                        throw new IllegalCallableAccessException(e);
                    }
                } else {
                    StringBuilder R = b.d.b.a.a.R("This callable does not support a default call: ");
                    R.append(getDescriptor());
                    throw new a0(R.toString());
                }
            }
        }
    }

    @Override // d0.e0.b
    public List<Annotation> getAnnotations() {
        List<Annotation> invoke = this.j.invoke();
        m.checkNotNullExpressionValue(invoke, "_annotations()");
        return invoke;
    }

    public abstract d0.e0.p.d.l0.d<?> getCaller();

    public abstract i getContainer();

    public abstract d0.e0.p.d.l0.d<?> getDefaultCaller();

    public abstract d0.e0.p.d.m0.c.b getDescriptor();

    @Override // kotlin.reflect.KCallable
    public List<d0.e0.f> getParameters() {
        ArrayList<d0.e0.f> invoke = this.k.invoke();
        m.checkNotNullExpressionValue(invoke, "_parameters()");
        return invoke;
    }

    @Override // kotlin.reflect.KCallable
    public KType getReturnType() {
        x invoke = this.l.invoke();
        m.checkNotNullExpressionValue(invoke, "_returnType()");
        return invoke;
    }

    @Override // kotlin.reflect.KCallable
    public List<h> getTypeParameters() {
        List<y> invoke = this.m.invoke();
        m.checkNotNullExpressionValue(invoke, "_typeParameters()");
        return invoke;
    }

    @Override // kotlin.reflect.KCallable
    public KVisibility getVisibility() {
        d0.e0.p.d.m0.c.u visibility = getDescriptor().getVisibility();
        m.checkNotNullExpressionValue(visibility, "descriptor.visibility");
        return j0.toKVisibility(visibility);
    }

    @Override // kotlin.reflect.KCallable
    public boolean isAbstract() {
        return getDescriptor().getModality() == z.ABSTRACT;
    }

    public abstract boolean isBound();

    @Override // kotlin.reflect.KCallable
    public boolean isFinal() {
        return getDescriptor().getModality() == z.FINAL;
    }

    @Override // kotlin.reflect.KCallable
    public boolean isOpen() {
        return getDescriptor().getModality() == z.OPEN;
    }
}
