package d0.e0.p.d;

import d0.e0.p.d.m0.c.i1.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.x;
import kotlin.Unit;
/* compiled from: util.kt */
/* loaded from: classes3.dex */
public class a extends m<f<?>, Unit> {
    public final i a;

    public a(i iVar) {
        d0.z.d.m.checkNotNullParameter(iVar, "container");
        this.a = iVar;
    }

    public f<?> visitFunctionDescriptor(x xVar, Unit unit) {
        d0.z.d.m.checkNotNullParameter(xVar, "descriptor");
        d0.z.d.m.checkNotNullParameter(unit, "data");
        return new j(this.a, xVar);
    }

    public f<?> visitPropertyDescriptor(n0 n0Var, Unit unit) {
        d0.z.d.m.checkNotNullParameter(n0Var, "descriptor");
        d0.z.d.m.checkNotNullParameter(unit, "data");
        int i = 0;
        int i2 = n0Var.getDispatchReceiverParameter() != null ? 1 : 0;
        if (n0Var.getExtensionReceiverParameter() != null) {
            i = 1;
        }
        int i3 = i2 + i;
        if (n0Var.isVar()) {
            if (i3 == 0) {
                return new k(this.a, n0Var);
            }
            if (i3 == 1) {
                return new l(this.a, n0Var);
            }
            if (i3 == 2) {
                return new m(this.a, n0Var);
            }
        } else if (i3 == 0) {
            return new p(this.a, n0Var);
        } else {
            if (i3 == 1) {
                return new q(this.a, n0Var);
            }
            if (i3 == 2) {
                return new r(this.a, n0Var);
            }
        }
        throw new a0("Unsupported property: " + n0Var);
    }
}
