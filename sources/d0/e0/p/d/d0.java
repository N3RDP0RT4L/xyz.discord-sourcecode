package d0.e0.p.d;

import d0.e0.c;
import d0.e0.e;
import d0.e0.g;
import d0.z.d.b0;
import d0.z.d.d;
import d0.z.d.i;
import d0.z.d.j;
import d0.z.d.o;
import d0.z.d.p;
import d0.z.d.r;
import d0.z.d.v;
import d0.z.d.x;
import kotlin.reflect.KDeclarationContainer;
import kotlin.reflect.KFunction;
import kotlin.reflect.KMutableProperty0;
import kotlin.reflect.KProperty0;
/* compiled from: ReflectionFactoryImpl.java */
/* loaded from: classes3.dex */
public class d0 extends b0 {
    public static i a(d dVar) {
        KDeclarationContainer owner = dVar.getOwner();
        return owner instanceof i ? (i) owner : b.m;
    }

    @Override // d0.z.d.b0
    public KFunction function(j jVar) {
        return new j(a(jVar), jVar.getName(), jVar.getSignature(), jVar.getBoundReceiver());
    }

    @Override // d0.z.d.b0
    public c getOrCreateKotlinClass(Class cls) {
        return g.getOrCreateKotlinClass(cls);
    }

    @Override // d0.z.d.b0
    public KDeclarationContainer getOrCreateKotlinPackage(Class cls, String str) {
        return new n(cls, str);
    }

    @Override // d0.z.d.b0
    public KMutableProperty0 mutableProperty0(p pVar) {
        return new k(a(pVar), pVar.getName(), pVar.getSignature(), pVar.getBoundReceiver());
    }

    @Override // d0.z.d.b0
    public e mutableProperty1(r rVar) {
        return new l(a(rVar), rVar.getName(), rVar.getSignature(), rVar.getBoundReceiver());
    }

    @Override // d0.z.d.b0
    public KProperty0 property0(v vVar) {
        return new p(a(vVar), vVar.getName(), vVar.getSignature(), vVar.getBoundReceiver());
    }

    @Override // d0.z.d.b0
    public g property1(x xVar) {
        return new q(a(xVar), xVar.getName(), xVar.getSignature(), xVar.getBoundReceiver());
    }

    @Override // d0.z.d.b0
    public String renderLambdaToString(o oVar) {
        return renderLambdaToString((i) oVar);
    }

    @Override // d0.z.d.b0
    public String renderLambdaToString(i iVar) {
        j asKFunctionImpl;
        KFunction reflect = d0.e0.p.c.reflect(iVar);
        if (reflect == null || (asKFunctionImpl = j0.asKFunctionImpl(reflect)) == null) {
            return super.renderLambdaToString(iVar);
        }
        return e0.f3169b.renderLambda(asKFunctionImpl.getDescriptor());
    }
}
