package d0.e0.p.d;

import andhook.lib.xposed.ClassUtils;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import d0.e0.p.d.m0.b.q.c;
import d0.e0.p.d.m0.c.c0;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.k1.a.e;
import d0.e0.p.d.m0.c.k1.a.f;
import d0.e0.p.d.m0.c.k1.a.m;
import d0.e0.p.d.m0.c.k1.b.n;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.s;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.k.v.g;
import d0.e0.p.d.m0.k.v.k;
import d0.e0.p.d.m0.k.v.r;
import d0.e0.p.d.m0.l.b.l;
import d0.e0.p.d.m0.l.b.u;
import d0.g0.t;
import d0.o;
import d0.t.h0;
import d0.z.d.j;
import d0.z.d.m;
import d0.z.d.z;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;
import kotlin.reflect.KCallable;
import kotlin.reflect.KType;
import kotlin.reflect.KVisibility;
/* compiled from: util.kt */
/* loaded from: classes3.dex */
public final class j0 {
    public static final b a = new b("kotlin.jvm.JvmStatic");

    public static final Class<?> a(ClassLoader classLoader, a aVar, int i) {
        c cVar = c.a;
        d0.e0.p.d.m0.g.c unsafe = aVar.asSingleFqName().toUnsafe();
        m.checkNotNullExpressionValue(unsafe, "kotlinClassId.asSingleFqName().toUnsafe()");
        a mapKotlinToJava = cVar.mapKotlinToJava(unsafe);
        if (mapKotlinToJava != null) {
            aVar = mapKotlinToJava;
        }
        String asString = aVar.getPackageFqName().asString();
        m.checkNotNullExpressionValue(asString, "javaClassId.packageFqName.asString()");
        String asString2 = aVar.getRelativeClassName().asString();
        m.checkNotNullExpressionValue(asString2, "javaClassId.relativeClassName.asString()");
        if (m.areEqual(asString, "kotlin")) {
            switch (asString2.hashCode()) {
                case -901856463:
                    if (asString2.equals("BooleanArray")) {
                        return boolean[].class;
                    }
                    break;
                case -763279523:
                    if (asString2.equals("ShortArray")) {
                        return short[].class;
                    }
                    break;
                case -755911549:
                    if (asString2.equals("CharArray")) {
                        return char[].class;
                    }
                    break;
                case -74930671:
                    if (asString2.equals("ByteArray")) {
                        return byte[].class;
                    }
                    break;
                case 22374632:
                    if (asString2.equals("DoubleArray")) {
                        return double[].class;
                    }
                    break;
                case 63537721:
                    if (asString2.equals("Array")) {
                        return Object[].class;
                    }
                    break;
                case 601811914:
                    if (asString2.equals("IntArray")) {
                        return int[].class;
                    }
                    break;
                case 948852093:
                    if (asString2.equals("FloatArray")) {
                        return float[].class;
                    }
                    break;
                case 2104330525:
                    if (asString2.equals("LongArray")) {
                        return long[].class;
                    }
                    break;
            }
        }
        String str = asString + ClassUtils.PACKAGE_SEPARATOR_CHAR + t.replace$default(asString2, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, (char) ClassUtils.INNER_CLASS_SEPARATOR_CHAR, false, 4, (Object) null);
        if (i > 0) {
            str = t.repeat("[", i) + 'L' + str + ';';
        }
        return e.tryLoadClass(classLoader, str);
    }

    public static final j asKFunctionImpl(Object obj) {
        j jVar = null;
        j jVar2 = (j) (!(obj instanceof j) ? null : obj);
        if (jVar2 != null) {
            return jVar2;
        }
        if (!(obj instanceof j)) {
            obj = null;
        }
        j jVar3 = (j) obj;
        KCallable compute = jVar3 != null ? jVar3.compute() : null;
        if (compute instanceof j) {
            jVar = compute;
        }
        return jVar;
    }

    public static final s<?> asKPropertyImpl(Object obj) {
        s<?> sVar = null;
        s<?> sVar2 = (s) (!(obj instanceof s) ? null : obj);
        if (sVar2 != null) {
            return sVar2;
        }
        if (!(obj instanceof z)) {
            obj = null;
        }
        z zVar = (z) obj;
        KCallable compute = zVar != null ? zVar.compute() : null;
        if (compute instanceof s) {
            sVar = compute;
        }
        return sVar;
    }

    public static final Annotation b(d0.e0.p.d.m0.c.g1.c cVar) {
        d0.e0.p.d.m0.c.e annotationClass = d0.e0.p.d.m0.k.x.a.getAnnotationClass(cVar);
        Class<?> javaClass = annotationClass != null ? toJavaClass(annotationClass) : null;
        if (!(javaClass instanceof Class)) {
            javaClass = null;
        }
        if (javaClass == null) {
            return null;
        }
        Set<Map.Entry<d0.e0.p.d.m0.g.e, g<?>>> entrySet = cVar.getAllValueArguments().entrySet();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            d0.e0.p.d.m0.g.e eVar = (d0.e0.p.d.m0.g.e) entry.getKey();
            ClassLoader classLoader = javaClass.getClassLoader();
            m.checkNotNullExpressionValue(classLoader, "annotationClass.classLoader");
            Object c = c((g) entry.getValue(), classLoader);
            Pair pair = c != null ? o.to(eVar.asString(), c) : null;
            if (pair != null) {
                arrayList.add(pair);
            }
        }
        return (Annotation) d0.e0.p.d.l0.b.createAnnotationInstance$default(javaClass, h0.toMap(arrayList), null, 4, null);
    }

    public static final Object c(g<?> gVar, ClassLoader classLoader) {
        if (gVar instanceof d0.e0.p.d.m0.k.v.a) {
            return b(((d0.e0.p.d.m0.k.v.a) gVar).getValue());
        }
        if (gVar instanceof d0.e0.p.d.m0.k.v.b) {
            List<? extends g<?>> value = ((d0.e0.p.d.m0.k.v.b) gVar).getValue();
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(value, 10));
            Iterator<T> it = value.iterator();
            while (it.hasNext()) {
                arrayList.add(c((g) it.next(), classLoader));
            }
            Object[] array = arrayList.toArray(new Object[0]);
            Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
            return array;
        } else if (gVar instanceof d0.e0.p.d.m0.k.v.j) {
            Pair<? extends a, ? extends d0.e0.p.d.m0.g.e> value2 = ((d0.e0.p.d.m0.k.v.j) gVar).getValue();
            d0.e0.p.d.m0.g.e component2 = value2.component2();
            Class<?> a2 = a(classLoader, value2.component1(), 0);
            if (a2 != null) {
                return i0.getEnumConstantByName(a2, component2.asString());
            }
            return null;
        } else if (gVar instanceof r) {
            r.b value3 = ((r) gVar).getValue();
            if (value3 instanceof r.b.C0345b) {
                r.b.C0345b bVar = (r.b.C0345b) value3;
                return a(classLoader, bVar.getClassId(), bVar.getArrayDimensions());
            } else if (value3 instanceof r.b.a) {
                h declarationDescriptor = ((r.b.a) value3).getType().getConstructor().getDeclarationDescriptor();
                if (!(declarationDescriptor instanceof d0.e0.p.d.m0.c.e)) {
                    declarationDescriptor = null;
                }
                d0.e0.p.d.m0.c.e eVar = (d0.e0.p.d.m0.c.e) declarationDescriptor;
                if (eVar != null) {
                    return toJavaClass(eVar);
                }
                return null;
            } else {
                throw new NoWhenBranchMatchedException();
            }
        } else if (!(gVar instanceof k) && !(gVar instanceof d0.e0.p.d.m0.k.v.t)) {
            return gVar.getValue();
        } else {
            return null;
        }
    }

    public static final List<Annotation> computeAnnotations(d0.e0.p.d.m0.c.g1.a aVar) {
        m.checkNotNullParameter(aVar, "$this$computeAnnotations");
        d0.e0.p.d.m0.c.g1.g annotations = aVar.getAnnotations();
        ArrayList arrayList = new ArrayList();
        for (d0.e0.p.d.m0.c.g1.c cVar : annotations) {
            u0 source = cVar.getSource();
            Annotation annotation = null;
            if (source instanceof d0.e0.p.d.m0.c.k1.a.b) {
                annotation = ((d0.e0.p.d.m0.c.k1.a.b) source).getAnnotation();
            } else if (source instanceof m.a) {
                n javaElement = ((m.a) source).getJavaElement();
                if (!(javaElement instanceof d0.e0.p.d.m0.c.k1.b.c)) {
                    javaElement = null;
                }
                d0.e0.p.d.m0.c.k1.b.c cVar2 = (d0.e0.p.d.m0.c.k1.b.c) javaElement;
                if (cVar2 != null) {
                    annotation = cVar2.getAnnotation();
                }
            } else {
                annotation = b(cVar);
            }
            if (annotation != null) {
                arrayList.add(annotation);
            }
        }
        return arrayList;
    }

    public static final Object defaultPrimitiveValue(Type type) {
        d0.z.d.m.checkNotNullParameter(type, "type");
        if (!(type instanceof Class) || !((Class) type).isPrimitive()) {
            return null;
        }
        if (d0.z.d.m.areEqual(type, Boolean.TYPE)) {
            return Boolean.FALSE;
        }
        if (d0.z.d.m.areEqual(type, Character.TYPE)) {
            return Character.valueOf((char) 0);
        }
        if (d0.z.d.m.areEqual(type, Byte.TYPE)) {
            return Byte.valueOf((byte) 0);
        }
        if (d0.z.d.m.areEqual(type, Short.TYPE)) {
            return Short.valueOf((short) 0);
        }
        if (d0.z.d.m.areEqual(type, Integer.TYPE)) {
            return 0;
        }
        if (d0.z.d.m.areEqual(type, Float.TYPE)) {
            return Float.valueOf(0.0f);
        }
        if (d0.z.d.m.areEqual(type, Long.TYPE)) {
            return 0L;
        }
        if (d0.z.d.m.areEqual(type, Double.TYPE)) {
            return Double.valueOf((double) ShadowDrawableWrapper.COS_45);
        }
        if (d0.z.d.m.areEqual(type, Void.TYPE)) {
            throw new IllegalStateException("Parameter with void type is illegal");
        }
        throw new UnsupportedOperationException(b.d.b.a.a.y("Unknown primitive: ", type));
    }

    public static final <M extends d0.e0.p.d.m0.i.n, D extends d0.e0.p.d.m0.c.a> D deserializeToDescriptor(Class<?> cls, M m, d0.e0.p.d.m0.f.z.c cVar, d0.e0.p.d.m0.f.z.g gVar, d0.e0.p.d.m0.f.z.a aVar, Function2<? super u, ? super M, ? extends D> function2) {
        List<s> typeParameterList;
        d0.z.d.m.checkNotNullParameter(cls, "moduleAnchor");
        d0.z.d.m.checkNotNullParameter(m, "proto");
        d0.z.d.m.checkNotNullParameter(cVar, "nameResolver");
        d0.z.d.m.checkNotNullParameter(gVar, "typeTable");
        d0.z.d.m.checkNotNullParameter(aVar, "metadataVersion");
        d0.z.d.m.checkNotNullParameter(function2, "createDescriptor");
        d0.e0.p.d.m0.c.k1.a.k orCreateModule = b0.getOrCreateModule(cls);
        if (m instanceof i) {
            typeParameterList = ((i) m).getTypeParameterList();
        } else if (m instanceof d0.e0.p.d.m0.f.n) {
            typeParameterList = ((d0.e0.p.d.m0.f.n) m).getTypeParameterList();
        } else {
            throw new IllegalStateException(("Unsupported message: " + m).toString());
        }
        List<s> list = typeParameterList;
        d0.e0.p.d.m0.l.b.j deserialization = orCreateModule.getDeserialization();
        c0 module = orCreateModule.getModule();
        d0.e0.p.d.m0.f.z.i empty = d0.e0.p.d.m0.f.z.i.a.getEMPTY();
        d0.z.d.m.checkNotNullExpressionValue(list, "typeParameters");
        return function2.invoke(new u(new l(deserialization, cVar, module, gVar, empty, aVar, null, null, list)), m);
    }

    public static final q0 getInstanceReceiverParameter(d0.e0.p.d.m0.c.a aVar) {
        d0.z.d.m.checkNotNullParameter(aVar, "$this$instanceReceiverParameter");
        if (aVar.getDispatchReceiverParameter() == null) {
            return null;
        }
        d0.e0.p.d.m0.c.m containingDeclaration = aVar.getContainingDeclaration();
        Objects.requireNonNull(containingDeclaration, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor");
        return ((d0.e0.p.d.m0.c.e) containingDeclaration).getThisAsReceiverParameter();
    }

    public static final b getJVM_STATIC() {
        return a;
    }

    public static final boolean isInlineClassType(KType kType) {
        d0.e0.p.d.m0.n.c0 type;
        d0.z.d.m.checkNotNullParameter(kType, "$this$isInlineClassType");
        if (!(kType instanceof x)) {
            kType = null;
        }
        x xVar = (x) kType;
        return (xVar == null || (type = xVar.getType()) == null || !d0.e0.p.d.m0.k.g.isInlineClassType(type)) ? false : true;
    }

    public static final Class<?> toJavaClass(d0.e0.p.d.m0.c.e eVar) {
        d0.z.d.m.checkNotNullParameter(eVar, "$this$toJavaClass");
        u0 source = eVar.getSource();
        d0.z.d.m.checkNotNullExpressionValue(source, "source");
        if (source instanceof d0.e0.p.d.m0.e.b.r) {
            p binaryClass = ((d0.e0.p.d.m0.e.b.r) source).getBinaryClass();
            Objects.requireNonNull(binaryClass, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.runtime.components.ReflectKotlinClass");
            return ((f) binaryClass).getKlass();
        } else if (source instanceof m.a) {
            n javaElement = ((m.a) source).getJavaElement();
            Objects.requireNonNull(javaElement, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.runtime.structure.ReflectJavaClass");
            return ((d0.e0.p.d.m0.c.k1.b.j) javaElement).getElement();
        } else {
            a classId = d0.e0.p.d.m0.k.x.a.getClassId(eVar);
            if (classId != null) {
                return a(d0.e0.p.d.m0.c.k1.b.b.getSafeClassLoader(eVar.getClass()), classId, 0);
            }
            return null;
        }
    }

    public static final KVisibility toKVisibility(d0.e0.p.d.m0.c.u uVar) {
        d0.z.d.m.checkNotNullParameter(uVar, "$this$toKVisibility");
        if (d0.z.d.m.areEqual(uVar, d0.e0.p.d.m0.c.t.e)) {
            return KVisibility.PUBLIC;
        }
        if (d0.z.d.m.areEqual(uVar, d0.e0.p.d.m0.c.t.c)) {
            return KVisibility.PROTECTED;
        }
        if (d0.z.d.m.areEqual(uVar, d0.e0.p.d.m0.c.t.d)) {
            return KVisibility.INTERNAL;
        }
        if (!d0.z.d.m.areEqual(uVar, d0.e0.p.d.m0.c.t.a) && !d0.z.d.m.areEqual(uVar, d0.e0.p.d.m0.c.t.f3272b)) {
            return null;
        }
        return KVisibility.PRIVATE;
    }
}
