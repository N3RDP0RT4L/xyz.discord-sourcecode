package d0.e0.p.d;

import d0.e0.p.d.d;
import d0.e0.p.d.m0.c.e0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.e.a.z;
import d0.e0.p.d.m0.f.a0.a;
import d0.e0.p.d.m0.f.a0.b.e;
import d0.e0.p.d.m0.f.a0.b.h;
import d0.e0.p.d.m0.f.n;
import d0.e0.p.d.m0.f.z.g;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.l.b.e0.f;
import d0.e0.p.d.m0.l.b.e0.j;
import d0.z.d.m;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: RuntimeTypeMapper.kt */
/* loaded from: classes3.dex */
public abstract class e {

    /* compiled from: RuntimeTypeMapper.kt */
    /* loaded from: classes3.dex */
    public static final class a extends e {
        public final Field a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Field field) {
            super(null);
            m.checkNotNullParameter(field, "field");
            this.a = field;
        }

        @Override // d0.e0.p.d.e
        public String asString() {
            StringBuilder sb = new StringBuilder();
            String name = this.a.getName();
            m.checkNotNullExpressionValue(name, "field.name");
            sb.append(z.getterName(name));
            sb.append("()");
            Class<?> type = this.a.getType();
            m.checkNotNullExpressionValue(type, "field.type");
            sb.append(d0.e0.p.d.m0.c.k1.b.b.getDesc(type));
            return sb.toString();
        }

        public final Field getField() {
            return this.a;
        }
    }

    /* compiled from: RuntimeTypeMapper.kt */
    /* loaded from: classes3.dex */
    public static final class b extends e {
        public final Method a;

        /* renamed from: b  reason: collision with root package name */
        public final Method f3166b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(Method method, Method method2) {
            super(null);
            m.checkNotNullParameter(method, "getterMethod");
            this.a = method;
            this.f3166b = method2;
        }

        @Override // d0.e0.p.d.e
        public String asString() {
            return h0.access$getSignature$p(this.a);
        }

        public final Method getGetterMethod() {
            return this.a;
        }

        public final Method getSetterMethod() {
            return this.f3166b;
        }
    }

    /* compiled from: RuntimeTypeMapper.kt */
    /* loaded from: classes3.dex */
    public static final class c extends e {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final n0 f3167b;
        public final n c;
        public final a.d d;
        public final d0.e0.p.d.m0.f.z.c e;
        public final g f;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(n0 n0Var, n nVar, a.d dVar, d0.e0.p.d.m0.f.z.c cVar, g gVar) {
            super(null);
            String str;
            String str2;
            m.checkNotNullParameter(n0Var, "descriptor");
            m.checkNotNullParameter(nVar, "proto");
            m.checkNotNullParameter(dVar, "signature");
            m.checkNotNullParameter(cVar, "nameResolver");
            m.checkNotNullParameter(gVar, "typeTable");
            this.f3167b = n0Var;
            this.c = nVar;
            this.d = dVar;
            this.e = cVar;
            this.f = gVar;
            if (dVar.hasGetter()) {
                StringBuilder sb = new StringBuilder();
                a.c getter = dVar.getGetter();
                m.checkNotNullExpressionValue(getter, "signature.getter");
                sb.append(cVar.getString(getter.getName()));
                a.c getter2 = dVar.getGetter();
                m.checkNotNullExpressionValue(getter2, "signature.getter");
                sb.append(cVar.getString(getter2.getDesc()));
                str = sb.toString();
            } else {
                e.a jvmFieldSignature$default = h.getJvmFieldSignature$default(h.a, nVar, cVar, gVar, false, 8, null);
                if (jvmFieldSignature$default != null) {
                    String component1 = jvmFieldSignature$default.component1();
                    String component2 = jvmFieldSignature$default.component2();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(z.getterName(component1));
                    d0.e0.p.d.m0.c.m containingDeclaration = n0Var.getContainingDeclaration();
                    m.checkNotNullExpressionValue(containingDeclaration, "descriptor.containingDeclaration");
                    if (!m.areEqual(n0Var.getVisibility(), t.d) || !(containingDeclaration instanceof d0.e0.p.d.m0.l.b.e0.d)) {
                        if (m.areEqual(n0Var.getVisibility(), t.a) && (containingDeclaration instanceof e0)) {
                            Objects.requireNonNull(n0Var, "null cannot be cast to non-null type org.jetbrains.kotlin.serialization.deserialization.descriptors.DeserializedPropertyDescriptor");
                            f containerSource = ((j) n0Var).getContainerSource();
                            if (containerSource instanceof d0.e0.p.d.m0.e.b.j) {
                                d0.e0.p.d.m0.e.b.j jVar = (d0.e0.p.d.m0.e.b.j) containerSource;
                                if (jVar.getFacadeClassName() != null) {
                                    StringBuilder R = b.d.b.a.a.R("$");
                                    R.append(jVar.getSimpleName().asString());
                                    str2 = R.toString();
                                }
                            }
                        }
                        str2 = "";
                    } else {
                        d0.e0.p.d.m0.f.c classProto = ((d0.e0.p.d.m0.l.b.e0.d) containingDeclaration).getClassProto();
                        g.f<d0.e0.p.d.m0.f.c, Integer> fVar = d0.e0.p.d.m0.f.a0.a.i;
                        m.checkNotNullExpressionValue(fVar, "JvmProtoBuf.classModuleName");
                        Integer num = (Integer) d0.e0.p.d.m0.f.z.e.getExtensionOrNull(classProto, fVar);
                        String str3 = (num == null || (str3 = cVar.getString(num.intValue())) == null) ? "main" : str3;
                        StringBuilder R2 = b.d.b.a.a.R("$");
                        R2.append(d0.e0.p.d.m0.g.f.sanitizeAsJavaIdentifier(str3));
                        str2 = R2.toString();
                    }
                    str = b.d.b.a.a.J(sb2, str2, "()", component2);
                } else {
                    throw new a0("No field signature for property: " + n0Var);
                }
            }
            this.a = str;
        }

        @Override // d0.e0.p.d.e
        public String asString() {
            return this.a;
        }

        public final n0 getDescriptor() {
            return this.f3167b;
        }

        public final d0.e0.p.d.m0.f.z.c getNameResolver() {
            return this.e;
        }

        public final n getProto() {
            return this.c;
        }

        public final a.d getSignature() {
            return this.d;
        }

        public final d0.e0.p.d.m0.f.z.g getTypeTable() {
            return this.f;
        }
    }

    /* compiled from: RuntimeTypeMapper.kt */
    /* loaded from: classes3.dex */
    public static final class d extends e {
        public final d.e a;

        /* renamed from: b  reason: collision with root package name */
        public final d.e f3168b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(d.e eVar, d.e eVar2) {
            super(null);
            m.checkNotNullParameter(eVar, "getterSignature");
            this.a = eVar;
            this.f3168b = eVar2;
        }

        @Override // d0.e0.p.d.e
        public String asString() {
            return this.a.asString();
        }

        public final d.e getGetterSignature() {
            return this.a;
        }

        public final d.e getSetterSignature() {
            return this.f3168b;
        }
    }

    public e(DefaultConstructorMarker defaultConstructorMarker) {
    }

    public abstract String asString();
}
