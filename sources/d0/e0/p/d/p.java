package d0.e0.p.d;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.s;
import d0.g;
import d0.i;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty0;
/* compiled from: KProperty0Impl.kt */
/* loaded from: classes3.dex */
public class p<V> extends s<V> implements KProperty0<V> {
    public final c0.b<a<V>> u;
    public final Lazy<Object> v = g.lazy(i.PUBLICATION, new c());

    /* compiled from: KProperty0Impl.kt */
    /* loaded from: classes3.dex */
    public static final class a<R> extends s.c<R> implements KProperty0.Getter<R> {
        public final p<R> q;

        /* JADX WARN: Multi-variable type inference failed */
        public a(p<? extends R> pVar) {
            m.checkNotNullParameter(pVar, "property");
            this.q = pVar;
        }

        @Override // d0.e0.p.d.s.a
        public p<R> getProperty() {
            return this.q;
        }

        @Override // kotlin.jvm.functions.Function0
        public R invoke() {
            return getProperty().get();
        }
    }

    /* compiled from: KProperty0Impl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<a<? extends V>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final a<V> invoke() {
            return new a<>(p.this);
        }
    }

    /* compiled from: KProperty0Impl.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<Object> {
        public c() {
            super(0);
        }

        /* JADX WARN: Code restructure failed: missing block: B:17:?, code lost:
            return r1.get(r2);
         */
        @Override // kotlin.jvm.functions.Function0
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final java.lang.Object invoke() {
            /*
                r4 = this;
                d0.e0.p.d.p r0 = d0.e0.p.d.p.this
                java.lang.reflect.Field r1 = r0.c()
                d0.e0.p.d.p r2 = d0.e0.p.d.p.this
                java.lang.Object r2 = r2.getBoundReceiver()
                java.util.Objects.requireNonNull(r0)
                java.lang.Object r3 = d0.e0.p.d.s.n     // Catch: java.lang.IllegalAccessException -> L48
                if (r2 != r3) goto L3f
                d0.e0.p.d.m0.c.n0 r3 = r0.getDescriptor()     // Catch: java.lang.IllegalAccessException -> L48
                d0.e0.p.d.m0.c.q0 r3 = r3.getExtensionReceiverParameter()     // Catch: java.lang.IllegalAccessException -> L48
                if (r3 == 0) goto L1e
                goto L3f
            L1e:
                java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch: java.lang.IllegalAccessException -> L48
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch: java.lang.IllegalAccessException -> L48
                r2.<init>()     // Catch: java.lang.IllegalAccessException -> L48
                r3 = 39
                r2.append(r3)     // Catch: java.lang.IllegalAccessException -> L48
                r2.append(r0)     // Catch: java.lang.IllegalAccessException -> L48
                java.lang.String r0 = "' is not an extension property and thus getExtensionDelegate() "
                r2.append(r0)     // Catch: java.lang.IllegalAccessException -> L48
                java.lang.String r0 = "is not going to work, use getDelegate() instead"
                r2.append(r0)     // Catch: java.lang.IllegalAccessException -> L48
                java.lang.String r0 = r2.toString()     // Catch: java.lang.IllegalAccessException -> L48
                r1.<init>(r0)     // Catch: java.lang.IllegalAccessException -> L48
                throw r1     // Catch: java.lang.IllegalAccessException -> L48
            L3f:
                if (r1 == 0) goto L46
                java.lang.Object r0 = r1.get(r2)     // Catch: java.lang.IllegalAccessException -> L48
                goto L47
            L46:
                r0 = 0
            L47:
                return r0
            L48:
                r0 = move-exception
                kotlin.reflect.full.IllegalPropertyDelegateAccessException r1 = new kotlin.reflect.full.IllegalPropertyDelegateAccessException
                r1.<init>(r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.p.c.invoke():java.lang.Object");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public p(i iVar, n0 n0Var) {
        super(iVar, n0Var);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(n0Var, "descriptor");
        c0.b<a<V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Getter(this) }");
        this.u = lazy;
    }

    @Override // kotlin.reflect.KProperty0
    public V get() {
        return getGetter().call(new Object[0]);
    }

    @Override // kotlin.reflect.KProperty0
    public Object getDelegate() {
        return this.v.getValue();
    }

    @Override // kotlin.jvm.functions.Function0
    public V invoke() {
        return get();
    }

    @Override // d0.e0.p.d.s, kotlin.reflect.KProperty0
    public a<V> getGetter() {
        a<V> invoke = this.u.invoke();
        m.checkNotNullExpressionValue(invoke, "_getter()");
        return invoke;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public p(i iVar, String str, String str2, Object obj) {
        super(iVar, str, str2, obj);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "signature");
        c0.b<a<V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Getter(this) }");
        this.u = lazy;
    }
}
