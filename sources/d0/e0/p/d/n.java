package d0.e0.p.d;

import andhook.lib.xposed.ClassUtils;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.c0;
import d0.e0.p.d.i;
import d0.e0.p.d.m0.c.k1.a.f;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.d.b.d;
import d0.e0.p.d.m0.f.a0.b.g;
import d0.e0.p.d.m0.f.a0.b.h;
import d0.e0.p.d.m0.f.l;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.k.a0.i;
import d0.e0.p.d.m0.l.b.u;
import d0.g0.t;
import d0.z.d.a0;
import d0.z.d.j;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.util.Collection;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.reflect.KDeclarationContainer;
import kotlin.reflect.KProperty;
/* compiled from: KPackageImpl.kt */
/* loaded from: classes3.dex */
public final class n extends i {
    public final c0.b<a> m;
    public final Class<?> n;

    /* compiled from: KPackageImpl.kt */
    /* loaded from: classes3.dex */
    public final class a extends i.b {
        public static final /* synthetic */ KProperty[] d = {a0.property1(new y(a0.getOrCreateKotlinClass(a.class), "kotlinClass", "getKotlinClass()Lorg/jetbrains/kotlin/descriptors/runtime/components/ReflectKotlinClass;")), a0.property1(new y(a0.getOrCreateKotlinClass(a.class), "scope", "getScope()Lorg/jetbrains/kotlin/resolve/scopes/MemberScope;")), a0.property1(new y(a0.getOrCreateKotlinClass(a.class), "multifileFacade", "getMultifileFacade()Ljava/lang/Class;")), a0.property1(new y(a0.getOrCreateKotlinClass(a.class), "metadata", "getMetadata()Lkotlin/Triple;")), a0.property1(new y(a0.getOrCreateKotlinClass(a.class), "members", "getMembers()Ljava/util/Collection;"))};
        public final c0.a e = c0.lazySoft(new C0370a());
        public final c0.a f = c0.lazySoft(new e());
        public final c0.b g = c0.lazy(new d());
        public final c0.b h = c0.lazy(new c());

        /* compiled from: KPackageImpl.kt */
        /* renamed from: d0.e0.p.d.n$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0370a extends o implements Function0<f> {
            public C0370a() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final f invoke() {
                return f.a.create(n.this.getJClass());
            }
        }

        /* compiled from: KPackageImpl.kt */
        /* loaded from: classes3.dex */
        public static final class b extends o implements Function0<Collection<? extends f<?>>> {
            public b() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public final Collection<? extends f<?>> invoke() {
                a aVar = a.this;
                return n.this.b(aVar.getScope(), i.c.DECLARED);
            }
        }

        /* compiled from: KPackageImpl.kt */
        /* loaded from: classes3.dex */
        public static final class c extends o implements Function0<Triple<? extends g, ? extends l, ? extends d0.e0.p.d.m0.f.a0.b.f>> {
            public c() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public final Triple<? extends g, ? extends l, ? extends d0.e0.p.d.m0.f.a0.b.f> invoke() {
                d0.e0.p.d.m0.e.b.b0.a classHeader;
                f access$getKotlinClass$p = a.access$getKotlinClass$p(a.this);
                if (access$getKotlinClass$p == null || (classHeader = access$getKotlinClass$p.getClassHeader()) == null) {
                    return null;
                }
                String[] data = classHeader.getData();
                String[] strings = classHeader.getStrings();
                if (data == null || strings == null) {
                    return null;
                }
                Pair<g, l> readPackageDataFrom = h.readPackageDataFrom(data, strings);
                return new Triple<>(readPackageDataFrom.component1(), readPackageDataFrom.component2(), classHeader.getMetadataVersion());
            }
        }

        /* compiled from: KPackageImpl.kt */
        /* loaded from: classes3.dex */
        public static final class d extends o implements Function0<Class<?>> {
            public d() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final Class<?> invoke() {
                d0.e0.p.d.m0.e.b.b0.a classHeader;
                f access$getKotlinClass$p = a.access$getKotlinClass$p(a.this);
                String multifileClassName = (access$getKotlinClass$p == null || (classHeader = access$getKotlinClass$p.getClassHeader()) == null) ? null : classHeader.getMultifileClassName();
                if (multifileClassName == null) {
                    return null;
                }
                if (multifileClassName.length() > 0) {
                    return n.this.getJClass().getClassLoader().loadClass(t.replace$default(multifileClassName, (char) MentionUtilsKt.SLASH_CHAR, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, false, 4, (Object) null));
                }
                return null;
            }
        }

        /* compiled from: KPackageImpl.kt */
        /* loaded from: classes3.dex */
        public static final class e extends o implements Function0<d0.e0.p.d.m0.k.a0.i> {
            public e() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final d0.e0.p.d.m0.k.a0.i invoke() {
                f access$getKotlinClass$p = a.access$getKotlinClass$p(a.this);
                if (access$getKotlinClass$p != null) {
                    return a.this.getModuleData().getPackagePartScopeCache().getPackagePartScope(access$getKotlinClass$p);
                }
                return i.b.f3433b;
            }
        }

        public a() {
            super();
            c0.lazySoft(new b());
        }

        public static final f access$getKotlinClass$p(a aVar) {
            return (f) aVar.e.getValue(aVar, d[0]);
        }

        public final Triple<g, l, d0.e0.p.d.m0.f.a0.b.f> getMetadata() {
            return (Triple) this.h.getValue(this, d[3]);
        }

        public final Class<?> getMultifileFacade() {
            return (Class) this.g.getValue(this, d[2]);
        }

        public final d0.e0.p.d.m0.k.a0.i getScope() {
            return (d0.e0.p.d.m0.k.a0.i) this.f.getValue(this, d[1]);
        }
    }

    /* compiled from: KPackageImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<a> {
        public b() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final a invoke() {
            return new a();
        }
    }

    /* compiled from: KPackageImpl.kt */
    /* loaded from: classes3.dex */
    public static final /* synthetic */ class c extends j implements Function2<u, d0.e0.p.d.m0.f.n, n0> {
        public static final c j = new c();

        public c() {
            super(2);
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public final String getName() {
            return "loadProperty";
        }

        @Override // d0.z.d.d
        public final KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinClass(u.class);
        }

        @Override // d0.z.d.d
        public final String getSignature() {
            return "loadProperty(Lorg/jetbrains/kotlin/metadata/ProtoBuf$Property;)Lorg/jetbrains/kotlin/descriptors/PropertyDescriptor;";
        }

        public final n0 invoke(u uVar, d0.e0.p.d.m0.f.n nVar) {
            m.checkNotNullParameter(uVar, "p1");
            m.checkNotNullParameter(nVar, "p2");
            return uVar.loadProperty(nVar);
        }
    }

    public n(Class<?> cls, String str) {
        m.checkNotNullParameter(cls, "jClass");
        this.n = cls;
        c0.b<a> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Data() }");
        this.m = lazy;
    }

    @Override // d0.e0.p.d.i
    public Class<?> c() {
        Class<?> multifileFacade = this.m.invoke().getMultifileFacade();
        return multifileFacade != null ? multifileFacade : getJClass();
    }

    public boolean equals(Object obj) {
        return (obj instanceof n) && m.areEqual(getJClass(), ((n) obj).getJClass());
    }

    @Override // d0.e0.p.d.i
    public Collection<d0.e0.p.d.m0.c.l> getConstructorDescriptors() {
        return d0.t.n.emptyList();
    }

    @Override // d0.e0.p.d.i
    public Collection<x> getFunctions(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return this.m.invoke().getScope().getContributedFunctions(eVar, d.FROM_REFLECTION);
    }

    @Override // d0.z.d.e
    public Class<?> getJClass() {
        return this.n;
    }

    @Override // d0.e0.p.d.i
    public n0 getLocalProperty(int i) {
        Triple<g, l, d0.e0.p.d.m0.f.a0.b.f> metadata = this.m.invoke().getMetadata();
        if (metadata == null) {
            return null;
        }
        g component1 = metadata.component1();
        l component2 = metadata.component2();
        d0.e0.p.d.m0.f.a0.b.f component3 = metadata.component3();
        g.f<l, List<d0.e0.p.d.m0.f.n>> fVar = d0.e0.p.d.m0.f.a0.a.n;
        m.checkNotNullExpressionValue(fVar, "JvmProtoBuf.packageLocalVariable");
        d0.e0.p.d.m0.f.n nVar = (d0.e0.p.d.m0.f.n) d0.e0.p.d.m0.f.z.e.getExtensionOrNull(component2, fVar, i);
        if (nVar == null) {
            return null;
        }
        Class<?> jClass = getJClass();
        d0.e0.p.d.m0.f.t typeTable = component2.getTypeTable();
        m.checkNotNullExpressionValue(typeTable, "packageProto.typeTable");
        return (n0) j0.deserializeToDescriptor(jClass, nVar, component1, new d0.e0.p.d.m0.f.z.g(typeTable), component3, c.j);
    }

    @Override // d0.e0.p.d.i
    public Collection<n0> getProperties(e eVar) {
        m.checkNotNullParameter(eVar, ModelAuditLogEntry.CHANGE_KEY_NAME);
        return this.m.invoke().getScope().getContributedVariables(eVar, d.FROM_REFLECTION);
    }

    public int hashCode() {
        return getJClass().hashCode();
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("file class ");
        R.append(d0.e0.p.d.m0.c.k1.b.b.getClassId(getJClass()).asSingleFqName());
        return R.toString();
    }
}
