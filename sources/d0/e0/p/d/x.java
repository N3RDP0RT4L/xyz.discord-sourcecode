package d0.e0.p.d;

import d0.e0.d;
import d0.e0.i;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.h;
import d0.e0.p.d.m0.c.y0;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.m0.n.w0;
import d0.g;
import d0.j;
import d0.t.k;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.n;
import d0.z.d.o;
import d0.z.d.y;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
/* compiled from: KTypeImpl.kt */
/* loaded from: classes3.dex */
public final class x implements n {
    public static final /* synthetic */ KProperty[] j = {a0.property1(new y(a0.getOrCreateKotlinClass(x.class), "classifier", "getClassifier()Lkotlin/reflect/KClassifier;")), a0.property1(new y(a0.getOrCreateKotlinClass(x.class), "arguments", "getArguments()Ljava/util/List;"))};
    public final c0.a<Type> k;
    public final c0.a l;
    public final c0.a m;
    public final d0.e0.p.d.m0.n.c0 n;

    /* compiled from: KTypeImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends i>> {
        public final /* synthetic */ Function0 $computeJavaType;

        /* compiled from: KTypeImpl.kt */
        /* renamed from: d0.e0.p.d.x$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0372a extends o implements Function0<Type> {
            public final /* synthetic */ int $i;
            public final /* synthetic */ Lazy $parameterizedTypeArguments$inlined;
            public final /* synthetic */ KProperty $parameterizedTypeArguments$metadata$inlined = null;
            public final /* synthetic */ a this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0372a(int i, a aVar, Lazy lazy, KProperty kProperty) {
                super(0);
                this.$i = i;
                this.this$0 = aVar;
                this.$parameterizedTypeArguments$inlined = lazy;
            }

            @Override // kotlin.jvm.functions.Function0
            public final Type invoke() {
                Type javaType = x.this.getJavaType();
                if (javaType instanceof Class) {
                    Class cls = (Class) javaType;
                    Class componentType = cls.isArray() ? cls.getComponentType() : Object.class;
                    m.checkNotNullExpressionValue(componentType, "if (javaType.isArray) ja…Type else Any::class.java");
                    return componentType;
                } else if (javaType instanceof GenericArrayType) {
                    if (this.$i == 0) {
                        Type genericComponentType = ((GenericArrayType) javaType).getGenericComponentType();
                        m.checkNotNullExpressionValue(genericComponentType, "javaType.genericComponentType");
                        return genericComponentType;
                    }
                    StringBuilder R = b.d.b.a.a.R("Array type has been queried for a non-0th argument: ");
                    R.append(x.this);
                    throw new a0(R.toString());
                } else if (javaType instanceof ParameterizedType) {
                    Type type = (Type) ((List) this.$parameterizedTypeArguments$inlined.getValue()).get(this.$i);
                    if (type instanceof WildcardType) {
                        WildcardType wildcardType = (WildcardType) type;
                        Type[] lowerBounds = wildcardType.getLowerBounds();
                        m.checkNotNullExpressionValue(lowerBounds, "argument.lowerBounds");
                        Type type2 = (Type) k.firstOrNull(lowerBounds);
                        if (type2 != null) {
                            type = type2;
                        } else {
                            Type[] upperBounds = wildcardType.getUpperBounds();
                            m.checkNotNullExpressionValue(upperBounds, "argument.upperBounds");
                            type = (Type) k.first(upperBounds);
                        }
                    }
                    m.checkNotNullExpressionValue(type, "if (argument !is Wildcar…ument.upperBounds.first()");
                    return type;
                } else {
                    StringBuilder R2 = b.d.b.a.a.R("Non-generic type has been queried for arguments: ");
                    R2.append(x.this);
                    throw new a0(R2.toString());
                }
            }
        }

        /* compiled from: KTypeImpl.kt */
        /* loaded from: classes3.dex */
        public static final class b extends o implements Function0<List<? extends Type>> {
            public b() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public final List<? extends Type> invoke() {
                Type javaType = x.this.getJavaType();
                m.checkNotNull(javaType);
                return d0.e0.p.d.m0.c.k1.b.b.getParameterizedTypeArguments(javaType);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Function0 function0) {
            super(0);
            this.$computeJavaType = function0;
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends i> invoke() {
            i iVar;
            List<w0> arguments = x.this.getType().getArguments();
            if (arguments.isEmpty()) {
                return d0.t.n.emptyList();
            }
            Lazy lazy = g.lazy(d0.i.PUBLICATION, new b());
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(arguments, 10));
            int i = 0;
            for (Object obj : arguments) {
                i++;
                if (i < 0) {
                    d0.t.n.throwIndexOverflow();
                }
                w0 w0Var = (w0) obj;
                if (w0Var.isStarProjection()) {
                    iVar = i.f3162b.getSTAR();
                } else {
                    d0.e0.p.d.m0.n.c0 type = w0Var.getType();
                    m.checkNotNullExpressionValue(type, "typeProjection.type");
                    C0372a aVar = null;
                    if (this.$computeJavaType != null) {
                        aVar = new C0372a(i, this, lazy, null);
                    }
                    x xVar = new x(type, aVar);
                    int ordinal = w0Var.getProjectionKind().ordinal();
                    if (ordinal == 0) {
                        iVar = i.f3162b.invariant(xVar);
                    } else if (ordinal == 1) {
                        iVar = i.f3162b.contravariant(xVar);
                    } else if (ordinal == 2) {
                        iVar = i.f3162b.covariant(xVar);
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                }
                arrayList.add(iVar);
            }
            return arrayList;
        }
    }

    /* compiled from: KTypeImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<d> {
        public b() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final d invoke() {
            x xVar = x.this;
            return xVar.a(xVar.getType());
        }
    }

    public x(d0.e0.p.d.m0.n.c0 c0Var, Function0<? extends Type> function0) {
        m.checkNotNullParameter(c0Var, "type");
        this.n = c0Var;
        c0.a<Type> aVar = null;
        c0.a<Type> aVar2 = (c0.a) (!(function0 instanceof c0.a) ? null : function0);
        if (aVar2 != null) {
            aVar = aVar2;
        } else if (function0 != null) {
            aVar = c0.lazySoft(function0);
        }
        this.k = aVar;
        this.l = c0.lazySoft(new b());
        this.m = c0.lazySoft(new a(function0));
    }

    public final d a(d0.e0.p.d.m0.n.c0 c0Var) {
        d0.e0.p.d.m0.n.c0 type;
        h declarationDescriptor = c0Var.getConstructor().getDeclarationDescriptor();
        if (declarationDescriptor instanceof e) {
            Class<?> javaClass = j0.toJavaClass((e) declarationDescriptor);
            if (javaClass == null) {
                return null;
            }
            if (javaClass.isArray()) {
                w0 w0Var = (w0) u.singleOrNull((List<? extends Object>) c0Var.getArguments());
                if (w0Var == null || (type = w0Var.getType()) == null) {
                    return new h(javaClass);
                }
                m.checkNotNullExpressionValue(type, "type.arguments.singleOrN…return KClassImpl(jClass)");
                d a2 = a(type);
                if (a2 != null) {
                    return new h(d0.e0.p.d.m0.c.k1.b.b.createArrayType(d0.z.a.getJavaClass(d0.e0.p.a.getJvmErasure(a2))));
                }
                throw new a0("Cannot determine classifier for array element type: " + this);
            } else if (e1.isNullableType(c0Var)) {
                return new h(javaClass);
            } else {
                Class<?> primitiveByWrapper = d0.e0.p.d.m0.c.k1.b.b.getPrimitiveByWrapper(javaClass);
                if (primitiveByWrapper != null) {
                    javaClass = primitiveByWrapper;
                }
                return new h(javaClass);
            }
        } else if (declarationDescriptor instanceof z0) {
            return new y(null, (z0) declarationDescriptor);
        } else {
            if (!(declarationDescriptor instanceof y0)) {
                return null;
            }
            throw new j(b.d.b.a.a.v("An operation is not implemented: ", "Type alias classifiers are not yet supported"));
        }
    }

    public boolean equals(Object obj) {
        return (obj instanceof x) && m.areEqual(this.n, ((x) obj).n);
    }

    @Override // kotlin.reflect.KType
    public List<i> getArguments() {
        return (List) this.m.getValue(this, j[1]);
    }

    @Override // kotlin.reflect.KType
    public d getClassifier() {
        return (d) this.l.getValue(this, j[0]);
    }

    @Override // d0.z.d.n
    public Type getJavaType() {
        c0.a<Type> aVar = this.k;
        if (aVar != null) {
            return aVar.invoke();
        }
        return null;
    }

    public final d0.e0.p.d.m0.n.c0 getType() {
        return this.n;
    }

    public int hashCode() {
        return this.n.hashCode();
    }

    public String toString() {
        return e0.f3169b.renderType(this.n);
    }

    public /* synthetic */ x(d0.e0.p.d.m0.n.c0 c0Var, Function0 function0, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(c0Var, (i & 2) != 0 ? null : function0);
    }
}
