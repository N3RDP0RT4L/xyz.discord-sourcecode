package d0.e0.p.d;

import d0.e0.p.d.m0.n.e1;
import d0.e0.p.d.s;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
/* compiled from: KPropertyImpl.kt */
/* loaded from: classes3.dex */
public final class u extends o implements Function0<Boolean> {
    public final /* synthetic */ s.a $this_computeCallerForAccessor;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public u(s.a aVar) {
        super(0);
        this.$this_computeCallerForAccessor = aVar;
    }

    /* JADX WARN: Type inference failed for: r0v5, types: [java.lang.Boolean] */
    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2() {
        return !e1.isNullableType(this.$this_computeCallerForAccessor.getProperty().getDescriptor().getType()) ? 1 : 0;
    }
}
