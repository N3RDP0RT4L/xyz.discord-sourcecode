package d0.e0.p.d;

import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.s;
import d0.g;
import d0.i;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.reflect.Field;
import kotlin.Lazy;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.reflect.KProperty;
/* compiled from: KProperty2Impl.kt */
/* loaded from: classes3.dex */
public class r<D, E, V> extends s<V> implements KProperty, Function2 {
    public final c0.b<a<D, E, V>> u;
    public final Lazy<Field> v = g.lazy(i.PUBLICATION, new c());

    /* compiled from: KProperty2Impl.kt */
    /* loaded from: classes3.dex */
    public static final class a<D, E, V> extends s.c<V> implements KProperty.Getter, Function2 {
        public final r<D, E, V> q;

        /* JADX WARN: Multi-variable type inference failed */
        public a(r<D, E, ? extends V> rVar) {
            m.checkNotNullParameter(rVar, "property");
            this.q = rVar;
        }

        @Override // d0.e0.p.d.s.a
        public r<D, E, V> getProperty() {
            return this.q;
        }

        @Override // kotlin.jvm.functions.Function2
        public V invoke(D d, E e) {
            return getProperty().get(d, e);
        }
    }

    /* compiled from: KProperty2Impl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<a<D, E, ? extends V>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final a<D, E, V> invoke() {
            return new a<>(r.this);
        }
    }

    /* compiled from: KProperty2Impl.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function0<Field> {
        public c() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Field invoke() {
            return r.this.c();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public r(i iVar, n0 n0Var) {
        super(iVar, n0Var);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(n0Var, "descriptor");
        c0.b<a<D, E, V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Getter(this) }");
        this.u = lazy;
    }

    public V get(D d, E e) {
        return getGetter().call(d, e);
    }

    @Override // kotlin.jvm.functions.Function2
    public V invoke(D d, E e) {
        return get(d, e);
    }

    @Override // d0.e0.p.d.s, kotlin.reflect.KProperty0
    public a<D, E, V> getGetter() {
        a<D, E, V> invoke = this.u.invoke();
        m.checkNotNullExpressionValue(invoke, "_getter()");
        return invoke;
    }
}
