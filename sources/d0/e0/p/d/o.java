package d0.e0.p.d;

import d0.e0.f;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.k0;
import d0.e0.p.d.m0.c.q0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.y;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
import kotlin.reflect.KType;
/* compiled from: KParameterImpl.kt */
/* loaded from: classes3.dex */
public final class o implements f {
    public static final /* synthetic */ KProperty[] j = {a0.property1(new y(a0.getOrCreateKotlinClass(o.class), "descriptor", "getDescriptor()Lorg/jetbrains/kotlin/descriptors/ParameterDescriptor;")), a0.property1(new y(a0.getOrCreateKotlinClass(o.class), "annotations", "getAnnotations()Ljava/util/List;"))};
    public final c0.a k;
    public final f<?> l;
    public final int m;
    public final f.a n;

    /* compiled from: KParameterImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends d0.z.d.o implements Function0<List<? extends Annotation>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends Annotation> invoke() {
            return j0.computeAnnotations(o.this.a());
        }
    }

    /* compiled from: KParameterImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d0.z.d.o implements Function0<Type> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Type invoke() {
            k0 a = o.this.a();
            if (!(a instanceof q0) || !m.areEqual(j0.getInstanceReceiverParameter(o.this.getCallable().getDescriptor()), a) || o.this.getCallable().getDescriptor().getKind() != b.a.FAKE_OVERRIDE) {
                return o.this.getCallable().getCaller().getParameterTypes().get(o.this.getIndex());
            }
            d0.e0.p.d.m0.c.m containingDeclaration = o.this.getCallable().getDescriptor().getContainingDeclaration();
            Objects.requireNonNull(containingDeclaration, "null cannot be cast to non-null type org.jetbrains.kotlin.descriptors.ClassDescriptor");
            Class<?> javaClass = j0.toJavaClass((e) containingDeclaration);
            if (javaClass != null) {
                return javaClass;
            }
            throw new a0("Cannot determine receiver Java type of inherited declaration: " + a);
        }
    }

    public o(f<?> fVar, int i, f.a aVar, Function0<? extends k0> function0) {
        m.checkNotNullParameter(fVar, "callable");
        m.checkNotNullParameter(aVar, "kind");
        m.checkNotNullParameter(function0, "computeDescriptor");
        this.l = fVar;
        this.m = i;
        this.n = aVar;
        this.k = c0.lazySoft(function0);
        c0.lazySoft(new a());
    }

    public final k0 a() {
        return (k0) this.k.getValue(this, j[0]);
    }

    public boolean equals(Object obj) {
        if (obj instanceof o) {
            o oVar = (o) obj;
            if (m.areEqual(this.l, oVar.l) && getIndex() == oVar.getIndex()) {
                return true;
            }
        }
        return false;
    }

    public final f<?> getCallable() {
        return this.l;
    }

    public int getIndex() {
        return this.m;
    }

    @Override // d0.e0.f
    public f.a getKind() {
        return this.n;
    }

    @Override // d0.e0.f
    public String getName() {
        k0 a2 = a();
        if (!(a2 instanceof c1)) {
            a2 = null;
        }
        c1 c1Var = (c1) a2;
        if (c1Var == null || c1Var.getContainingDeclaration().hasSynthesizedParameterNames()) {
            return null;
        }
        d0.e0.p.d.m0.g.e name = c1Var.getName();
        m.checkNotNullExpressionValue(name, "valueParameter.name");
        if (name.isSpecial()) {
            return null;
        }
        return name.asString();
    }

    @Override // d0.e0.f
    public KType getType() {
        d0.e0.p.d.m0.n.c0 type = a().getType();
        m.checkNotNullExpressionValue(type, "descriptor.type");
        return new x(type, new b());
    }

    public int hashCode() {
        return Integer.valueOf(getIndex()).hashCode() + (this.l.hashCode() * 31);
    }

    @Override // d0.e0.f
    public boolean isOptional() {
        k0 a2 = a();
        if (!(a2 instanceof c1)) {
            a2 = null;
        }
        c1 c1Var = (c1) a2;
        if (c1Var != null) {
            return d0.e0.p.d.m0.k.x.a.declaresOrInheritsDefaultValue(c1Var);
        }
        return false;
    }

    @Override // d0.e0.f
    public boolean isVararg() {
        k0 a2 = a();
        return (a2 instanceof c1) && ((c1) a2).getVarargElementType() != null;
    }

    public String toString() {
        return e0.f3169b.renderParameter(this);
    }
}
