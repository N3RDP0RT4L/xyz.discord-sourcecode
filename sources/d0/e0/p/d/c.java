package d0.e0.p.d;

import d0.z.c.b;
import d0.z.c.d;
import d0.z.c.e;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function10;
import kotlin.jvm.functions.Function11;
import kotlin.jvm.functions.Function12;
import kotlin.jvm.functions.Function13;
import kotlin.jvm.functions.Function14;
import kotlin.jvm.functions.Function15;
import kotlin.jvm.functions.Function16;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function22;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.functions.Function6;
import kotlin.jvm.functions.Function7;
import kotlin.jvm.functions.Function8;
import kotlin.jvm.functions.Function9;
import kotlin.reflect.KCallable;
/* compiled from: FunctionWithAllInvokes.kt */
/* loaded from: classes3.dex */
public interface c extends Function0<Object>, Function1<Object, Object>, Function10<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function11<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function12<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function13<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function14<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function15<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function16<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, d0.z.c.a<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, b<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, d0.z.c.c<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function2<Object, Object, Object>, d<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, e<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function22<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function3<Object, Object, Object, Object>, Function4<Object, Object, Object, Object, Object>, Function5<Object, Object, Object, Object, Object, Object>, Function6<Object, Object, Object, Object, Object, Object, Object>, Function7<Object, Object, Object, Object, Object, Object, Object, Object>, Function8<Object, Object, Object, Object, Object, Object, Object, Object, Object>, Function9<Object, Object, Object, Object, Object, Object, Object, Object, Object, Object>, KCallable<Object> {

    /* compiled from: FunctionWithAllInvokes.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public static Object invoke(c cVar) {
            return ((f) cVar).call(new Object[0]);
        }

        public static Object invoke(c cVar, Object obj) {
            return ((f) cVar).call(obj);
        }

        public static Object invoke(c cVar, Object obj, Object obj2) {
            return ((f) cVar).call(obj, obj2);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3) {
            return ((f) cVar).call(obj, obj2, obj3);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4) {
            return ((f) cVar).call(obj, obj2, obj3, obj4);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13, Object obj14) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13, Object obj14, Object obj15) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13, Object obj14, Object obj15, Object obj16) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16);
        }

        public static Object invoke(c cVar, Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9, Object obj10, Object obj11, Object obj12, Object obj13, Object obj14, Object obj15, Object obj16, Object obj17, Object obj18, Object obj19, Object obj20, Object obj21, Object obj22) {
            return ((f) cVar).call(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16, obj17, obj18, obj19, obj20, obj21, obj22);
        }
    }
}
