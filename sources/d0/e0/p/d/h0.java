package d0.e0.p.d;

import d0.e0.p.d.m0.c.k1.b.b;
import d0.t.k;
import d0.z.d.m;
import java.lang.reflect.Method;
/* compiled from: RuntimeTypeMapper.kt */
/* loaded from: classes3.dex */
public final class h0 {
    public static final String access$getSignature$p(Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append(method.getName());
        Class<?>[] parameterTypes = method.getParameterTypes();
        m.checkNotNullExpressionValue(parameterTypes, "parameterTypes");
        sb.append(k.joinToString$default(parameterTypes, "", "(", ")", 0, (CharSequence) null, g0.j, 24, (Object) null));
        Class<?> returnType = method.getReturnType();
        m.checkNotNullExpressionValue(returnType, "returnType");
        sb.append(b.getDesc(returnType));
        return sb.toString();
    }
}
