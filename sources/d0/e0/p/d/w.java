package d0.e0.p.d;

import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.f.a0.b.h;
import d0.e0.p.d.m0.k.e;
import d0.e0.p.d.m0.l.b.e0.j;
import d0.e0.p.d.s;
/* compiled from: KPropertyImpl.kt */
/* loaded from: classes3.dex */
public final class w {
    /* JADX WARN: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0080  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0120  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final d0.e0.p.d.l0.d access$computeCallerForAccessor(d0.e0.p.d.s.a r7, boolean r8) {
        /*
            Method dump skipped, instructions count: 584
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.w.access$computeCallerForAccessor(d0.e0.p.d.s$a, boolean):d0.e0.p.d.l0.d");
    }

    public static final boolean access$isJvmFieldPropertyInCompanionObject(n0 n0Var) {
        m containingDeclaration = n0Var.getContainingDeclaration();
        d0.z.d.m.checkNotNullExpressionValue(containingDeclaration, "containingDeclaration");
        if (!e.isCompanionObject(containingDeclaration)) {
            return false;
        }
        m containingDeclaration2 = containingDeclaration.getContainingDeclaration();
        return (!e.isInterface(containingDeclaration2) && !e.isAnnotationClass(containingDeclaration2)) || ((n0Var instanceof j) && h.isMovedFromInterfaceCompanion(((j) n0Var).getProto()));
    }

    public static final Object getBoundReceiver(s.a<?, ?> aVar) {
        d0.z.d.m.checkNotNullParameter(aVar, "$this$boundReceiver");
        return aVar.getProperty().getBoundReceiver();
    }
}
