package d0.e0.p.d;

import d0.e0.p.d.l0.e;
import d0.e0.p.d.s;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import kotlin.jvm.functions.Function1;
/* compiled from: KPropertyImpl.kt */
/* loaded from: classes3.dex */
public final class v extends o implements Function1<Field, e<? extends Field>> {
    public final /* synthetic */ boolean $isGetter;
    public final /* synthetic */ t $isJvmStaticProperty$1;
    public final /* synthetic */ u $isNotNullProperty$2;
    public final /* synthetic */ s.a $this_computeCallerForAccessor;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public v(s.a aVar, boolean z2, u uVar, t tVar) {
        super(1);
        this.$this_computeCallerForAccessor = aVar;
        this.$isGetter = z2;
        this.$isNotNullProperty$2 = uVar;
        this.$isJvmStaticProperty$1 = tVar;
    }

    public final e<Field> invoke(Field field) {
        m.checkNotNullParameter(field, "field");
        return (w.access$isJvmFieldPropertyInCompanionObject(this.$this_computeCallerForAccessor.getProperty().getDescriptor()) || !Modifier.isStatic(field.getModifiers())) ? this.$isGetter ? this.$this_computeCallerForAccessor.isBound() ? new e.f.a(field, w.getBoundReceiver(this.$this_computeCallerForAccessor)) : new e.f.c(field) : this.$this_computeCallerForAccessor.isBound() ? new e.g.a(field, this.$isNotNullProperty$2.invoke2(), w.getBoundReceiver(this.$this_computeCallerForAccessor)) : new e.g.c(field, this.$isNotNullProperty$2.invoke2()) : this.$isJvmStaticProperty$1.invoke2() ? this.$isGetter ? this.$this_computeCallerForAccessor.isBound() ? new e.f.b(field) : new e.f.d(field) : this.$this_computeCallerForAccessor.isBound() ? new e.g.b(field, this.$isNotNullProperty$2.invoke2()) : new e.g.d(field, this.$isNotNullProperty$2.invoke2()) : this.$isGetter ? new e.f.C0282e(field) : new e.g.C0283e(field, this.$isNotNullProperty$2.invoke2());
    }
}
