package d0.e0.p.d;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.k1.a.k;
import d0.e0.p.d.m0.c.l;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.t;
import d0.e0.p.d.m0.c.u;
import d0.e0.p.d.m0.c.x;
import d0.g0.w;
import d0.t.g0;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import d0.z.d.y;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
/* compiled from: KDeclarationContainerImpl.kt */
/* loaded from: classes3.dex */
public abstract class i implements d0.z.d.e {
    public static final a l = new a(null);
    public static final Class<?> j = Class.forName("kotlin.jvm.internal.DefaultConstructorMarker");
    public static final Regex k = new Regex("<v#(\\d+)>");

    /* compiled from: KDeclarationContainerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final Regex getLOCAL_PROPERTY_SIGNATURE$kotlin_reflection() {
            return i.k;
        }
    }

    /* compiled from: KDeclarationContainerImpl.kt */
    /* loaded from: classes3.dex */
    public abstract class b {
        public static final /* synthetic */ KProperty[] a = {a0.property1(new y(a0.getOrCreateKotlinClass(b.class), "moduleData", "getModuleData()Lorg/jetbrains/kotlin/descriptors/runtime/components/RuntimeModuleData;"))};

        /* renamed from: b  reason: collision with root package name */
        public final c0.a f3171b = c0.lazySoft(new a());

        /* compiled from: KDeclarationContainerImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<k> {
            public a() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final k invoke() {
                return b0.getOrCreateModule(i.this.getJClass());
            }
        }

        public b() {
        }

        public final k getModuleData() {
            return (k) this.f3171b.getValue(this, a[0]);
        }
    }

    /* compiled from: KDeclarationContainerImpl.kt */
    /* loaded from: classes3.dex */
    public enum c {
        DECLARED,
        INHERITED;

        public final boolean accept(d0.e0.p.d.m0.c.b bVar) {
            m.checkNotNullParameter(bVar, "member");
            b.a kind = bVar.getKind();
            m.checkNotNullExpressionValue(kind, "member.kind");
            return kind.isReal() == (this == DECLARED);
        }
    }

    /* compiled from: KDeclarationContainerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class d extends o implements Function1<x, CharSequence> {
        public static final d j = new d();

        public d() {
            super(1);
        }

        public final CharSequence invoke(x xVar) {
            m.checkNotNullParameter(xVar, "descriptor");
            return d0.e0.p.d.m0.j.c.c.render(xVar) + " | " + f0.f3170b.mapSignature(xVar).asString();
        }
    }

    /* compiled from: KDeclarationContainerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function1<n0, CharSequence> {
        public static final e j = new e();

        public e() {
            super(1);
        }

        public final CharSequence invoke(n0 n0Var) {
            m.checkNotNullParameter(n0Var, "descriptor");
            return d0.e0.p.d.m0.j.c.c.render(n0Var) + " | " + f0.f3170b.mapPropertySignature(n0Var).asString();
        }
    }

    /* compiled from: KDeclarationContainerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class f<T> implements Comparator<u> {
        public static final f j = new f();

        public final int compare(u uVar, u uVar2) {
            Integer compare = t.compare(uVar, uVar2);
            if (compare != null) {
                return compare.intValue();
            }
            return 0;
        }
    }

    /* compiled from: KDeclarationContainerImpl.kt */
    /* loaded from: classes3.dex */
    public static final class g extends d0.e0.p.d.a {
        public g(i iVar, i iVar2) {
            super(iVar2);
        }

        public d0.e0.p.d.f<?> visitConstructorDescriptor(l lVar, Unit unit) {
            m.checkNotNullParameter(lVar, "descriptor");
            m.checkNotNullParameter(unit, "data");
            throw new IllegalStateException("No constructors should appear here: " + lVar);
        }
    }

    public final void a(List<Class<?>> list, String str, boolean z2) {
        List<Class<?>> d2 = d(str);
        list.addAll(d2);
        int size = ((((ArrayList) d2).size() + 32) - 1) / 32;
        for (int i = 0; i < size; i++) {
            Class<?> cls = Integer.TYPE;
            m.checkNotNullExpressionValue(cls, "Integer.TYPE");
            list.add(cls);
        }
        Class cls2 = z2 ? j : Object.class;
        m.checkNotNullExpressionValue(cls2, "if (isConstructor) DEFAU…RKER else Any::class.java");
        list.add(cls2);
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0051 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:20:0x001e A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.Collection<d0.e0.p.d.f<?>> b(d0.e0.p.d.m0.k.a0.i r8, d0.e0.p.d.i.c r9) {
        /*
            r7 = this;
            java.lang.String r0 = "scope"
            d0.z.d.m.checkNotNullParameter(r8, r0)
            java.lang.String r0 = "belonginess"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            d0.e0.p.d.i$g r0 = new d0.e0.p.d.i$g
            r0.<init>(r7, r7)
            r1 = 0
            r2 = 3
            java.util.Collection r8 = d0.e0.p.d.m0.k.a0.l.a.getContributedDescriptors$default(r8, r1, r1, r2, r1)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r8 = r8.iterator()
        L1e:
            boolean r3 = r8.hasNext()
            if (r3 == 0) goto L55
            java.lang.Object r3 = r8.next()
            d0.e0.p.d.m0.c.m r3 = (d0.e0.p.d.m0.c.m) r3
            boolean r4 = r3 instanceof d0.e0.p.d.m0.c.b
            if (r4 == 0) goto L4e
            r4 = r3
            d0.e0.p.d.m0.c.b r4 = (d0.e0.p.d.m0.c.b) r4
            d0.e0.p.d.m0.c.u r5 = r4.getVisibility()
            d0.e0.p.d.m0.c.u r6 = d0.e0.p.d.m0.c.t.h
            boolean r5 = d0.z.d.m.areEqual(r5, r6)
            r5 = r5 ^ 1
            if (r5 == 0) goto L4e
            boolean r4 = r9.accept(r4)
            if (r4 == 0) goto L4e
            kotlin.Unit r4 = kotlin.Unit.a
            java.lang.Object r3 = r3.accept(r0, r4)
            d0.e0.p.d.f r3 = (d0.e0.p.d.f) r3
            goto L4f
        L4e:
            r3 = r1
        L4f:
            if (r3 == 0) goto L1e
            r2.add(r3)
            goto L1e
        L55:
            java.util.List r8 = d0.t.u.toList(r2)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.i.b(d0.e0.p.d.m0.k.a0.i, d0.e0.p.d.i$c):java.util.Collection");
    }

    public Class<?> c() {
        Class<?> wrapperByPrimitive = d0.e0.p.d.m0.c.k1.b.b.getWrapperByPrimitive(getJClass());
        return wrapperByPrimitive != null ? wrapperByPrimitive : getJClass();
    }

    public final List<Class<?>> d(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 1;
        while (str.charAt(i) != ')') {
            int i2 = i;
            while (str.charAt(i2) == '[') {
                i2++;
            }
            char charAt = str.charAt(i2);
            if (!w.contains$default((CharSequence) "VZCBSIFJD", charAt, false, 2, (Object) null)) {
                if (charAt == 'L') {
                    i2 = w.indexOf$default((CharSequence) str, ';', i, false, 4, (Object) null);
                } else {
                    throw new a0(b.d.b.a.a.v("Unknown type prefix in the method signature: ", str));
                }
            }
            int i3 = i2 + 1;
            arrayList.add(g(str, i, i3));
            i = i3;
        }
        return arrayList;
    }

    public final Class<?> e(String str) {
        return g(str, w.indexOf$default((CharSequence) str, ')', 0, false, 6, (Object) null) + 1, str.length());
    }

    public final Method f(Class<?> cls, String str, Class<?>[] clsArr, Class<?> cls2, boolean z2) {
        Class<?>[] interfaces;
        Method f2;
        if (z2) {
            clsArr[0] = cls;
        }
        Method i = i(cls, str, clsArr, cls2);
        if (i != null) {
            return i;
        }
        Class<? super Object> superclass = cls.getSuperclass();
        if (!(superclass == null || (f2 = f(superclass, str, clsArr, cls2, z2)) == null)) {
            return f2;
        }
        for (Class<?> cls3 : cls.getInterfaces()) {
            m.checkNotNullExpressionValue(cls3, "superInterface");
            Method f3 = f(cls3, str, clsArr, cls2, z2);
            if (f3 != null) {
                return f3;
            }
            if (z2) {
                Class<?> tryLoadClass = d0.e0.p.d.m0.c.k1.a.e.tryLoadClass(d0.e0.p.d.m0.c.k1.b.b.getSafeClassLoader(cls3), cls3.getName() + "$DefaultImpls");
                if (tryLoadClass != null) {
                    clsArr[0] = cls3;
                    Method i2 = i(tryLoadClass, str, clsArr, cls2);
                    if (i2 != null) {
                        return i2;
                    }
                } else {
                    continue;
                }
            }
        }
        return null;
    }

    public final Constructor<?> findConstructorBySignature(String str) {
        m.checkNotNullParameter(str, "desc");
        return h(getJClass(), d(str));
    }

    public final Constructor<?> findDefaultConstructor(String str) {
        m.checkNotNullParameter(str, "desc");
        Class<?> jClass = getJClass();
        ArrayList arrayList = new ArrayList();
        a(arrayList, str, true);
        return h(jClass, arrayList);
    }

    public final Method findDefaultMethod(String str, String str2, boolean z2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "desc");
        if (m.areEqual(str, HookHelper.constructorName)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (z2) {
            arrayList.add(getJClass());
        }
        a(arrayList, str2, false);
        Class<?> c2 = c();
        String v = b.d.b.a.a.v(str, "$default");
        Object[] array = arrayList.toArray(new Class[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        return f(c2, v, (Class[]) array, e(str2), z2);
    }

    public final x findFunctionDescriptor(String str, String str2) {
        Collection<x> collection;
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "signature");
        if (m.areEqual(str, HookHelper.constructorName)) {
            collection = d0.t.u.toList(getConstructorDescriptors());
        } else {
            d0.e0.p.d.m0.g.e identifier = d0.e0.p.d.m0.g.e.identifier(str);
            m.checkNotNullExpressionValue(identifier, "Name.identifier(name)");
            collection = getFunctions(identifier);
        }
        Collection<x> collection2 = collection;
        ArrayList arrayList = new ArrayList();
        for (Object obj : collection2) {
            if (m.areEqual(f0.f3170b.mapSignature((x) obj).asString(), str2)) {
                arrayList.add(obj);
            }
        }
        boolean z2 = true;
        if (arrayList.size() == 1) {
            return (x) d0.t.u.single((List<? extends Object>) arrayList);
        }
        String joinToString$default = d0.t.u.joinToString$default(collection2, "\n", null, null, 0, null, d.j, 30, null);
        StringBuilder sb = new StringBuilder();
        sb.append("Function '");
        sb.append(str);
        sb.append("' (JVM signature: ");
        sb.append(str2);
        sb.append(") not resolved in ");
        sb.append(this);
        sb.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        if (joinToString$default.length() != 0) {
            z2 = false;
        }
        sb.append(z2 ? " no members found" : '\n' + joinToString$default);
        throw new a0(sb.toString());
    }

    public final Method findMethodBySignature(String str, String str2) {
        Method f2;
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "desc");
        if (m.areEqual(str, HookHelper.constructorName)) {
            return null;
        }
        Object[] array = d(str2).toArray(new Class[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        Class<?>[] clsArr = (Class[]) array;
        Class<?> e2 = e(str2);
        Method f3 = f(c(), str, clsArr, e2, false);
        if (f3 != null) {
            return f3;
        }
        if (!c().isInterface() || (f2 = f(Object.class, str, clsArr, e2, false)) == null) {
            return null;
        }
        return f2;
    }

    public final n0 findPropertyDescriptor(String str, String str2) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "signature");
        MatchResult matchEntire = k.matchEntire(str2);
        boolean z2 = true;
        if (matchEntire != null) {
            String str3 = matchEntire.getDestructured().getMatch().getGroupValues().get(1);
            n0 localProperty = getLocalProperty(Integer.parseInt(str3));
            if (localProperty != null) {
                return localProperty;
            }
            StringBuilder W = b.d.b.a.a.W("Local property #", str3, " not found in ");
            W.append(getJClass());
            throw new a0(W.toString());
        }
        d0.e0.p.d.m0.g.e identifier = d0.e0.p.d.m0.g.e.identifier(str);
        m.checkNotNullExpressionValue(identifier, "Name.identifier(name)");
        Collection<n0> properties = getProperties(identifier);
        ArrayList arrayList = new ArrayList();
        for (Object obj : properties) {
            if (m.areEqual(f0.f3170b.mapPropertySignature((n0) obj).asString(), str2)) {
                arrayList.add(obj);
            }
        }
        if (arrayList.isEmpty()) {
            throw new a0("Property '" + str + "' (JVM signature: " + str2 + ") not resolved in " + this);
        } else if (arrayList.size() == 1) {
            return (n0) d0.t.u.single((List<? extends Object>) arrayList);
        } else {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Object obj2 : arrayList) {
                u visibility = ((n0) obj2).getVisibility();
                Object obj3 = linkedHashMap.get(visibility);
                if (obj3 == null) {
                    obj3 = new ArrayList();
                    linkedHashMap.put(visibility, obj3);
                }
                ((List) obj3).add(obj2);
            }
            Collection values = g0.toSortedMap(linkedHashMap, f.j).values();
            m.checkNotNullExpressionValue(values, "properties\n             …                }).values");
            List list = (List) d0.t.u.last(values);
            if (list.size() == 1) {
                m.checkNotNullExpressionValue(list, "mostVisibleProperties");
                return (n0) d0.t.u.first((List<? extends Object>) list);
            }
            d0.e0.p.d.m0.g.e identifier2 = d0.e0.p.d.m0.g.e.identifier(str);
            m.checkNotNullExpressionValue(identifier2, "Name.identifier(name)");
            String joinToString$default = d0.t.u.joinToString$default(getProperties(identifier2), "\n", null, null, 0, null, e.j, 30, null);
            StringBuilder sb = new StringBuilder();
            sb.append("Property '");
            sb.append(str);
            sb.append("' (JVM signature: ");
            sb.append(str2);
            sb.append(") not resolved in ");
            sb.append(this);
            sb.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
            if (joinToString$default.length() != 0) {
                z2 = false;
            }
            sb.append(z2 ? " no members found" : '\n' + joinToString$default);
            throw new a0(sb.toString());
        }
    }

    public final Class<?> g(String str, int i, int i2) {
        char charAt = str.charAt(i);
        if (charAt == 'F') {
            return Float.TYPE;
        }
        if (charAt == 'L') {
            ClassLoader safeClassLoader = d0.e0.p.d.m0.c.k1.b.b.getSafeClassLoader(getJClass());
            String substring = str.substring(i + 1, i2 - 1);
            m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            Class<?> loadClass = safeClassLoader.loadClass(d0.g0.t.replace$default(substring, (char) MentionUtilsKt.SLASH_CHAR, (char) ClassUtils.PACKAGE_SEPARATOR_CHAR, false, 4, (Object) null));
            m.checkNotNullExpressionValue(loadClass, "jClass.safeClassLoader.l…d - 1).replace('/', '.'))");
            return loadClass;
        } else if (charAt == 'S') {
            return Short.TYPE;
        } else {
            if (charAt == 'V') {
                Class<?> cls = Void.TYPE;
                m.checkNotNullExpressionValue(cls, "Void.TYPE");
                return cls;
            } else if (charAt == 'I') {
                return Integer.TYPE;
            } else {
                if (charAt == 'J') {
                    return Long.TYPE;
                }
                if (charAt == 'Z') {
                    return Boolean.TYPE;
                }
                if (charAt == '[') {
                    return d0.e0.p.d.m0.c.k1.b.b.createArrayType(g(str, i + 1, i2));
                }
                switch (charAt) {
                    case 'B':
                        return Byte.TYPE;
                    case 'C':
                        return Character.TYPE;
                    case 'D':
                        return Double.TYPE;
                    default:
                        throw new a0(b.d.b.a.a.v("Unknown type prefix in the method signature: ", str));
                }
            }
        }
    }

    public abstract Collection<l> getConstructorDescriptors();

    public abstract Collection<x> getFunctions(d0.e0.p.d.m0.g.e eVar);

    public abstract n0 getLocalProperty(int i);

    public abstract Collection<n0> getProperties(d0.e0.p.d.m0.g.e eVar);

    public final Constructor<?> h(Class<?> cls, List<? extends Class<?>> list) {
        try {
            Object[] array = list.toArray(new Class[0]);
            if (array != null) {
                Class[] clsArr = (Class[]) array;
                return cls.getDeclaredConstructor((Class[]) Arrays.copyOf(clsArr, clsArr.length));
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T>");
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x005a A[LOOP:0: B:7:0x0029->B:18:0x005a, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x0058 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.reflect.Method i(java.lang.Class<?> r7, java.lang.String r8, java.lang.Class<?>[] r9, java.lang.Class<?> r10) {
        /*
            r6 = this;
            r0 = 0
            int r1 = r9.length     // Catch: java.lang.NoSuchMethodException -> L5d
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r9, r1)     // Catch: java.lang.NoSuchMethodException -> L5d
            java.lang.Class[] r1 = (java.lang.Class[]) r1     // Catch: java.lang.NoSuchMethodException -> L5d
            java.lang.reflect.Method r1 = r7.getDeclaredMethod(r8, r1)     // Catch: java.lang.NoSuchMethodException -> L5d
            java.lang.String r2 = "result"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)     // Catch: java.lang.NoSuchMethodException -> L5d
            java.lang.Class r2 = r1.getReturnType()     // Catch: java.lang.NoSuchMethodException -> L5d
            boolean r2 = d0.z.d.m.areEqual(r2, r10)     // Catch: java.lang.NoSuchMethodException -> L5d
            if (r2 == 0) goto L1d
            r0 = r1
            goto L5d
        L1d:
            java.lang.reflect.Method[] r7 = r7.getDeclaredMethods()     // Catch: java.lang.NoSuchMethodException -> L5d
            java.lang.String r1 = "declaredMethods"
            d0.z.d.m.checkNotNullExpressionValue(r7, r1)     // Catch: java.lang.NoSuchMethodException -> L5d
            int r1 = r7.length     // Catch: java.lang.NoSuchMethodException -> L5d
            r2 = 0
            r3 = 0
        L29:
            if (r3 >= r1) goto L5d
            r4 = r7[r3]     // Catch: java.lang.NoSuchMethodException -> L5d
            java.lang.String r5 = "method"
            d0.z.d.m.checkNotNullExpressionValue(r4, r5)     // Catch: java.lang.NoSuchMethodException -> L5d
            java.lang.String r5 = r4.getName()     // Catch: java.lang.NoSuchMethodException -> L5d
            boolean r5 = d0.z.d.m.areEqual(r5, r8)     // Catch: java.lang.NoSuchMethodException -> L5d
            if (r5 == 0) goto L55
            java.lang.Class r5 = r4.getReturnType()     // Catch: java.lang.NoSuchMethodException -> L5d
            boolean r5 = d0.z.d.m.areEqual(r5, r10)     // Catch: java.lang.NoSuchMethodException -> L5d
            if (r5 == 0) goto L55
            java.lang.Class[] r5 = r4.getParameterTypes()     // Catch: java.lang.NoSuchMethodException -> L5d
            d0.z.d.m.checkNotNull(r5)     // Catch: java.lang.NoSuchMethodException -> L5d
            boolean r5 = java.util.Arrays.equals(r5, r9)     // Catch: java.lang.NoSuchMethodException -> L5d
            if (r5 == 0) goto L55
            r5 = 1
            goto L56
        L55:
            r5 = 0
        L56:
            if (r5 == 0) goto L5a
            r0 = r4
            goto L5d
        L5a:
            int r3 = r3 + 1
            goto L29
        L5d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.i.i(java.lang.Class, java.lang.String, java.lang.Class[], java.lang.Class):java.lang.reflect.Method");
    }
}
