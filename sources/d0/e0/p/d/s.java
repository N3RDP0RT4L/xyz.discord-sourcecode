package d0.e0.p.d;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.c0;
import d0.e0.p.d.e;
import d0.e0.p.d.m0.c.g1.g;
import d0.e0.p.d.m0.c.m;
import d0.e0.p.d.m0.c.m0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.o0;
import d0.e0.p.d.m0.c.p0;
import d0.e0.p.d.m0.e.a.n;
import d0.e0.p.d.m0.f.a0.b.e;
import d0.e0.p.d.m0.f.a0.b.h;
import d0.z.d.a0;
import d0.z.d.o;
import d0.z.d.y;
import java.lang.reflect.Field;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KFunction;
import kotlin.reflect.KMutableProperty$Setter;
import kotlin.reflect.KProperty;
/* compiled from: KPropertyImpl.kt */
/* loaded from: classes3.dex */
public abstract class s<V> extends d0.e0.p.d.f<V> implements KProperty<V> {
    public static final Object n = new Object();
    public final c0.b<Field> o;
    public final c0.a<n0> p;
    public final i q;
    public final String r;

    /* renamed from: s  reason: collision with root package name */
    public final String f3540s;
    public final Object t;

    /* compiled from: KPropertyImpl.kt */
    /* loaded from: classes3.dex */
    public static abstract class a<PropertyType, ReturnType> extends d0.e0.p.d.f<ReturnType> implements KFunction<ReturnType> {
        @Override // d0.e0.p.d.f
        public i getContainer() {
            return getProperty().getContainer();
        }

        @Override // d0.e0.p.d.f
        public d0.e0.p.d.l0.d<?> getDefaultCaller() {
            return null;
        }

        @Override // d0.e0.p.d.f
        public abstract m0 getDescriptor();

        public abstract s<PropertyType> getProperty();

        @Override // d0.e0.p.d.f
        public boolean isBound() {
            return getProperty().isBound();
        }

        @Override // kotlin.reflect.KFunction
        public boolean isExternal() {
            return getDescriptor().isExternal();
        }

        @Override // kotlin.reflect.KFunction
        public boolean isInfix() {
            return getDescriptor().isInfix();
        }

        @Override // kotlin.reflect.KFunction
        public boolean isInline() {
            return getDescriptor().isInline();
        }

        @Override // kotlin.reflect.KFunction
        public boolean isOperator() {
            return getDescriptor().isOperator();
        }

        @Override // kotlin.reflect.KCallable, kotlin.reflect.KFunction
        public boolean isSuspend() {
            return getDescriptor().isSuspend();
        }
    }

    /* compiled from: KPropertyImpl.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: KPropertyImpl.kt */
    /* loaded from: classes3.dex */
    public static abstract class c<V> extends a<V, V> implements KProperty.Getter<V> {
        public static final /* synthetic */ KProperty[] n = {a0.property1(new y(a0.getOrCreateKotlinClass(c.class), "descriptor", "getDescriptor()Lorg/jetbrains/kotlin/descriptors/PropertyGetterDescriptor;")), a0.property1(new y(a0.getOrCreateKotlinClass(c.class), "caller", "getCaller()Lkotlin/reflect/jvm/internal/calls/Caller;"))};
        public final c0.a o = c0.lazySoft(new b());
        public final c0.b p = c0.lazy(new a());

        /* compiled from: KPropertyImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<d0.e0.p.d.l0.d<?>> {
            public a() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final d0.e0.p.d.l0.d<?> invoke() {
                return w.access$computeCallerForAccessor(c.this, true);
            }
        }

        /* compiled from: KPropertyImpl.kt */
        /* loaded from: classes3.dex */
        public static final class b extends o implements Function0<o0> {
            public b() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final o0 invoke() {
                o0 getter = c.this.getProperty().getDescriptor().getGetter();
                return getter != null ? getter : d0.e0.p.d.m0.k.d.createDefaultGetter(c.this.getProperty().getDescriptor(), g.f.getEMPTY());
            }
        }

        @Override // d0.e0.p.d.f
        public d0.e0.p.d.l0.d<?> getCaller() {
            return (d0.e0.p.d.l0.d) this.p.getValue(this, n[1]);
        }

        @Override // d0.e0.p.d.s.a, d0.e0.p.d.f
        public o0 getDescriptor() {
            return (o0) this.o.getValue(this, n[0]);
        }

        @Override // kotlin.reflect.KCallable
        public String getName() {
            StringBuilder R = b.d.b.a.a.R("<get-");
            R.append(getProperty().getName());
            R.append('>');
            return R.toString();
        }
    }

    /* compiled from: KPropertyImpl.kt */
    /* loaded from: classes3.dex */
    public static abstract class d<V> extends a<V, Unit> implements KMutableProperty$Setter<V> {
        public static final /* synthetic */ KProperty[] n = {a0.property1(new y(a0.getOrCreateKotlinClass(d.class), "descriptor", "getDescriptor()Lorg/jetbrains/kotlin/descriptors/PropertySetterDescriptor;")), a0.property1(new y(a0.getOrCreateKotlinClass(d.class), "caller", "getCaller()Lkotlin/reflect/jvm/internal/calls/Caller;"))};
        public final c0.a o = c0.lazySoft(new b());
        public final c0.b p = c0.lazy(new a());

        /* compiled from: KPropertyImpl.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function0<d0.e0.p.d.l0.d<?>> {
            public a() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final d0.e0.p.d.l0.d<?> invoke() {
                return w.access$computeCallerForAccessor(d.this, false);
            }
        }

        /* compiled from: KPropertyImpl.kt */
        /* loaded from: classes3.dex */
        public static final class b extends o implements Function0<p0> {
            public b() {
                super(0);
            }

            /* JADX WARN: Can't rename method to resolve collision */
            @Override // kotlin.jvm.functions.Function0
            public final p0 invoke() {
                p0 setter = d.this.getProperty().getDescriptor().getSetter();
                if (setter != null) {
                    return setter;
                }
                n0 descriptor = d.this.getProperty().getDescriptor();
                g.a aVar = g.f;
                return d0.e0.p.d.m0.k.d.createDefaultSetter(descriptor, aVar.getEMPTY(), aVar.getEMPTY());
            }
        }

        @Override // d0.e0.p.d.f
        public d0.e0.p.d.l0.d<?> getCaller() {
            return (d0.e0.p.d.l0.d) this.p.getValue(this, n[1]);
        }

        @Override // d0.e0.p.d.s.a, d0.e0.p.d.f
        public p0 getDescriptor() {
            return (p0) this.o.getValue(this, n[0]);
        }

        @Override // kotlin.reflect.KCallable
        public String getName() {
            StringBuilder R = b.d.b.a.a.R("<set-");
            R.append(getProperty().getName());
            R.append('>');
            return R.toString();
        }
    }

    /* compiled from: KPropertyImpl.kt */
    /* loaded from: classes3.dex */
    public static final class e extends o implements Function0<n0> {
        public e() {
            super(0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // kotlin.jvm.functions.Function0
        public final n0 invoke() {
            return s.this.getContainer().findPropertyDescriptor(s.this.getName(), s.this.getSignature());
        }
    }

    /* compiled from: KPropertyImpl.kt */
    /* loaded from: classes3.dex */
    public static final class f extends o implements Function0<Field> {
        public f() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final Field invoke() {
            Class<?> cls;
            d0.e0.p.d.e mapPropertySignature = f0.f3170b.mapPropertySignature(s.this.getDescriptor());
            if (mapPropertySignature instanceof e.c) {
                e.c cVar = (e.c) mapPropertySignature;
                n0 descriptor = cVar.getDescriptor();
                e.a jvmFieldSignature$default = h.getJvmFieldSignature$default(h.a, cVar.getProto(), cVar.getNameResolver(), cVar.getTypeTable(), false, 8, null);
                if (jvmFieldSignature$default == null) {
                    return null;
                }
                if (n.isPropertyWithBackingFieldInOuterClass(descriptor) || h.isMovedFromInterfaceCompanion(cVar.getProto())) {
                    cls = s.this.getContainer().getJClass().getEnclosingClass();
                } else {
                    m containingDeclaration = descriptor.getContainingDeclaration();
                    cls = containingDeclaration instanceof d0.e0.p.d.m0.c.e ? j0.toJavaClass((d0.e0.p.d.m0.c.e) containingDeclaration) : s.this.getContainer().getJClass();
                }
                if (cls == null) {
                    return null;
                }
                try {
                    return cls.getDeclaredField(jvmFieldSignature$default.getName());
                } catch (NoSuchFieldException unused) {
                    return null;
                }
            } else if (mapPropertySignature instanceof e.a) {
                return ((e.a) mapPropertySignature).getField();
            } else {
                if ((mapPropertySignature instanceof e.b) || (mapPropertySignature instanceof e.d)) {
                    return null;
                }
                throw new NoWhenBranchMatchedException();
            }
        }
    }

    static {
        new b(null);
    }

    public s(i iVar, String str, String str2, n0 n0Var, Object obj) {
        this.q = iVar;
        this.r = str;
        this.f3540s = str2;
        this.t = obj;
        c0.b<Field> lazy = c0.lazy(new f());
        d0.z.d.m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy {…y -> null\n        }\n    }");
        this.o = lazy;
        c0.a<n0> lazySoft = c0.lazySoft(n0Var, new e());
        d0.z.d.m.checkNotNullExpressionValue(lazySoft, "ReflectProperties.lazySo…or(name, signature)\n    }");
        this.p = lazySoft;
    }

    public final Field c() {
        if (getDescriptor().isDelegated()) {
            return getJavaField();
        }
        return null;
    }

    public boolean equals(Object obj) {
        s<?> asKPropertyImpl = j0.asKPropertyImpl(obj);
        return asKPropertyImpl != null && d0.z.d.m.areEqual(getContainer(), asKPropertyImpl.getContainer()) && d0.z.d.m.areEqual(getName(), asKPropertyImpl.getName()) && d0.z.d.m.areEqual(this.f3540s, asKPropertyImpl.f3540s) && d0.z.d.m.areEqual(this.t, asKPropertyImpl.t);
    }

    public final Object getBoundReceiver() {
        return d0.e0.p.d.l0.h.coerceToExpectedReceiverType(this.t, getDescriptor());
    }

    @Override // d0.e0.p.d.f
    public d0.e0.p.d.l0.d<?> getCaller() {
        return getGetter().getCaller();
    }

    @Override // d0.e0.p.d.f
    public i getContainer() {
        return this.q;
    }

    @Override // d0.e0.p.d.f
    public d0.e0.p.d.l0.d<?> getDefaultCaller() {
        return getGetter().getDefaultCaller();
    }

    public abstract c<V> getGetter();

    public final Field getJavaField() {
        return this.o.invoke();
    }

    @Override // kotlin.reflect.KCallable
    public String getName() {
        return this.r;
    }

    public final String getSignature() {
        return this.f3540s;
    }

    public int hashCode() {
        int hashCode = getName().hashCode();
        return this.f3540s.hashCode() + ((hashCode + (getContainer().hashCode() * 31)) * 31);
    }

    @Override // d0.e0.p.d.f
    public boolean isBound() {
        return !d0.z.d.m.areEqual(this.t, d0.z.d.d.NO_RECEIVER);
    }

    @Override // kotlin.reflect.KProperty
    public boolean isConst() {
        return getDescriptor().isConst();
    }

    @Override // kotlin.reflect.KProperty
    public boolean isLateinit() {
        return getDescriptor().isLateInit();
    }

    @Override // kotlin.reflect.KCallable, kotlin.reflect.KFunction
    public boolean isSuspend() {
        return false;
    }

    public String toString() {
        return e0.f3169b.renderProperty(getDescriptor());
    }

    @Override // d0.e0.p.d.f
    public n0 getDescriptor() {
        n0 invoke = this.p.invoke();
        d0.z.d.m.checkNotNullExpressionValue(invoke, "_descriptor()");
        return invoke;
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public s(i iVar, String str, String str2, Object obj) {
        this(iVar, str, str2, null, obj);
        d0.z.d.m.checkNotNullParameter(iVar, "container");
        d0.z.d.m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        d0.z.d.m.checkNotNullParameter(str2, "signature");
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public s(d0.e0.p.d.i r8, d0.e0.p.d.m0.c.n0 r9) {
        /*
            r7 = this;
            java.lang.String r0 = "container"
            d0.z.d.m.checkNotNullParameter(r8, r0)
            java.lang.String r0 = "descriptor"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            d0.e0.p.d.m0.g.e r0 = r9.getName()
            java.lang.String r3 = r0.asString()
            java.lang.String r0 = "descriptor.name.asString()"
            d0.z.d.m.checkNotNullExpressionValue(r3, r0)
            d0.e0.p.d.f0 r0 = d0.e0.p.d.f0.f3170b
            d0.e0.p.d.e r0 = r0.mapPropertySignature(r9)
            java.lang.String r4 = r0.asString()
            java.lang.Object r6 = d0.z.d.d.NO_RECEIVER
            r1 = r7
            r2 = r8
            r5 = r9
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.e0.p.d.s.<init>(d0.e0.p.d.i, d0.e0.p.d.m0.c.n0):void");
    }
}
