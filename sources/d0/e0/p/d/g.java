package d0.e0.p.d;

import d0.e0.p.d.n0.b;
import d0.z.d.m;
import java.lang.ref.WeakReference;
/* compiled from: kClassCache.kt */
/* loaded from: classes3.dex */
public final class g {
    public static b<String, Object> a;

    static {
        b<String, Object> empty = b.empty();
        m.checkNotNullExpressionValue(empty, "HashPMap.empty<String, Any>()");
        a = empty;
    }

    public static final <T> h<T> getOrCreateKotlinClass(Class<T> cls) {
        m.checkNotNullParameter(cls, "jClass");
        String name = cls.getName();
        Object obj = a.get(name);
        Class<T> cls2 = null;
        if (obj instanceof WeakReference) {
            h<T> hVar = (h) ((WeakReference) obj).get();
            if (hVar != null) {
                cls2 = hVar.getJClass();
            }
            if (m.areEqual(cls2, cls)) {
                return hVar;
            }
        } else if (obj != null) {
            for (WeakReference weakReference : (WeakReference[]) obj) {
                h<T> hVar2 = (h) weakReference.get();
                if (m.areEqual(hVar2 != null ? hVar2.getJClass() : null, cls)) {
                    return hVar2;
                }
            }
            int length = ((Object[]) obj).length;
            WeakReference[] weakReferenceArr = new WeakReference[length + 1];
            System.arraycopy(obj, 0, weakReferenceArr, 0, length);
            h<T> hVar3 = new h<>(cls);
            weakReferenceArr[length] = new WeakReference(hVar3);
            b<String, Object> plus = a.plus(name, weakReferenceArr);
            m.checkNotNullExpressionValue(plus, "K_CLASS_CACHE.plus(name, newArray)");
            a = plus;
            return hVar3;
        }
        h<T> hVar4 = new h<>(cls);
        b<String, Object> plus2 = a.plus(name, new WeakReference(hVar4));
        m.checkNotNullExpressionValue(plus2, "K_CLASS_CACHE.plus(name, WeakReference(newKClass))");
        a = plus2;
        return hVar4;
    }
}
