package d0.e0.p.d;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.s;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KMutableProperty0;
/* compiled from: KProperty0Impl.kt */
/* loaded from: classes3.dex */
public final class k<V> extends p<V> implements KMutableProperty0<V> {
    public final c0.b<a<V>> w;

    /* compiled from: KProperty0Impl.kt */
    /* loaded from: classes3.dex */
    public static final class a<R> extends s.d<R> implements KMutableProperty0.Setter<R> {
        public final k<R> q;

        public a(k<R> kVar) {
            m.checkNotNullParameter(kVar, "property");
            this.q = kVar;
        }

        @Override // d0.e0.p.d.s.a
        public k<R> getProperty() {
            return this.q;
        }

        @Override // kotlin.jvm.functions.Function1
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public void invoke2(R r) {
            getProperty().set(r);
        }
    }

    /* compiled from: KProperty0Impl.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function0<a<V>> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final a<V> invoke() {
            return new a<>(k.this);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k(i iVar, n0 n0Var) {
        super(iVar, n0Var);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(n0Var, "descriptor");
        c0.b<a<V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Setter(this) }");
        this.w = lazy;
    }

    public void set(V v) {
        getSetter().call(v);
    }

    @Override // kotlin.reflect.KMutableProperty0
    public a<V> getSetter() {
        a<V> invoke = this.w.invoke();
        m.checkNotNullExpressionValue(invoke, "_setter()");
        return invoke;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k(i iVar, String str, String str2, Object obj) {
        super(iVar, str, str2, obj);
        m.checkNotNullParameter(iVar, "container");
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        m.checkNotNullParameter(str2, "signature");
        c0.b<a<V>> lazy = c0.lazy(new b());
        m.checkNotNullExpressionValue(lazy, "ReflectProperties.lazy { Setter(this) }");
        this.w = lazy;
    }
}
