package d0.e0.p.d;

import d0.e0.p.d.m0.f.a0.b.e;
import d0.t.k;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: RuntimeTypeMapper.kt */
/* loaded from: classes3.dex */
public abstract class d {

    /* compiled from: RuntimeTypeMapper.kt */
    /* loaded from: classes3.dex */
    public static final class a extends d {
        public final List<Method> a;

        /* renamed from: b  reason: collision with root package name */
        public final Class<?> f3163b;

        /* compiled from: Comparisons.kt */
        /* renamed from: d0.e0.p.d.d$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0273a<T> implements Comparator<T> {
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                Method method = (Method) t;
                m.checkNotNullExpressionValue(method, "it");
                String name = method.getName();
                Method method2 = (Method) t2;
                m.checkNotNullExpressionValue(method2, "it");
                return d0.u.a.compareValues(name, method2.getName());
            }
        }

        /* compiled from: RuntimeTypeMapper.kt */
        /* loaded from: classes3.dex */
        public static final class b extends o implements Function1<Method, CharSequence> {
            public static final b j = new b();

            public b() {
                super(1);
            }

            public final CharSequence invoke(Method method) {
                m.checkNotNullExpressionValue(method, "it");
                Class<?> returnType = method.getReturnType();
                m.checkNotNullExpressionValue(returnType, "it.returnType");
                return d0.e0.p.d.m0.c.k1.b.b.getDesc(returnType);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(Class<?> cls) {
            super(null);
            m.checkNotNullParameter(cls, "jClass");
            this.f3163b = cls;
            Method[] declaredMethods = cls.getDeclaredMethods();
            m.checkNotNullExpressionValue(declaredMethods, "jClass.declaredMethods");
            this.a = k.sortedWith(declaredMethods, new C0273a());
        }

        @Override // d0.e0.p.d.d
        public String asString() {
            return u.joinToString$default(this.a, "", "<init>(", ")V", 0, null, b.j, 24, null);
        }

        public final List<Method> getMethods() {
            return this.a;
        }
    }

    /* compiled from: RuntimeTypeMapper.kt */
    /* loaded from: classes3.dex */
    public static final class b extends d {
        public final Constructor<?> a;

        /* compiled from: RuntimeTypeMapper.kt */
        /* loaded from: classes3.dex */
        public static final class a extends o implements Function1<Class<?>, CharSequence> {
            public static final a j = new a();

            public a() {
                super(1);
            }

            public final CharSequence invoke(Class<?> cls) {
                m.checkNotNullExpressionValue(cls, "it");
                return d0.e0.p.d.m0.c.k1.b.b.getDesc(cls);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(Constructor<?> constructor) {
            super(null);
            m.checkNotNullParameter(constructor, "constructor");
            this.a = constructor;
        }

        @Override // d0.e0.p.d.d
        public String asString() {
            Class<?>[] parameterTypes = this.a.getParameterTypes();
            m.checkNotNullExpressionValue(parameterTypes, "constructor.parameterTypes");
            return k.joinToString$default(parameterTypes, "", "<init>(", ")V", 0, (CharSequence) null, a.j, 24, (Object) null);
        }

        public final Constructor<?> getConstructor() {
            return this.a;
        }
    }

    /* compiled from: RuntimeTypeMapper.kt */
    /* loaded from: classes3.dex */
    public static final class c extends d {
        public final Method a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(Method method) {
            super(null);
            m.checkNotNullParameter(method, "method");
            this.a = method;
        }

        @Override // d0.e0.p.d.d
        public String asString() {
            return h0.access$getSignature$p(this.a);
        }

        public final Method getMethod() {
            return this.a;
        }
    }

    /* compiled from: RuntimeTypeMapper.kt */
    /* renamed from: d0.e0.p.d.d$d  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0274d extends d {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final e.b f3164b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0274d(e.b bVar) {
            super(null);
            m.checkNotNullParameter(bVar, "signature");
            this.f3164b = bVar;
            this.a = bVar.asString();
        }

        @Override // d0.e0.p.d.d
        public String asString() {
            return this.a;
        }

        public final String getConstructorDesc() {
            return this.f3164b.getDesc();
        }
    }

    /* compiled from: RuntimeTypeMapper.kt */
    /* loaded from: classes3.dex */
    public static final class e extends d {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final e.b f3165b;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(e.b bVar) {
            super(null);
            m.checkNotNullParameter(bVar, "signature");
            this.f3165b = bVar;
            this.a = bVar.asString();
        }

        @Override // d0.e0.p.d.d
        public String asString() {
            return this.a;
        }

        public final String getMethodDesc() {
            return this.f3165b.getDesc();
        }

        public final String getMethodName() {
            return this.f3165b.getName();
        }
    }

    public d(DefaultConstructorMarker defaultConstructorMarker) {
    }

    public abstract String asString();
}
