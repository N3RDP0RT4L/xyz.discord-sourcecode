package d0.e0.p.d;

import d0.e0.p.d.m0.c.c1;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.q0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.g.e;
import d0.e0.p.d.m0.j.c;
import d0.e0.p.d.m0.n.c0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.jvm.functions.Function1;
/* compiled from: ReflectionObjectRenderer.kt */
/* loaded from: classes3.dex */
public final class e0 {

    /* renamed from: b  reason: collision with root package name */
    public static final e0 f3169b = new e0();
    public static final c a = c.f3411b;

    /* compiled from: ReflectionObjectRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function1<c1, CharSequence> {
        public static final a j = new a();

        public a() {
            super(1);
        }

        public final CharSequence invoke(c1 c1Var) {
            e0 e0Var = e0.f3169b;
            m.checkNotNullExpressionValue(c1Var, "it");
            c0 type = c1Var.getType();
            m.checkNotNullExpressionValue(type, "it.type");
            return e0Var.renderType(type);
        }
    }

    /* compiled from: ReflectionObjectRenderer.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function1<c1, CharSequence> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        public final CharSequence invoke(c1 c1Var) {
            e0 e0Var = e0.f3169b;
            m.checkNotNullExpressionValue(c1Var, "it");
            c0 type = c1Var.getType();
            m.checkNotNullExpressionValue(type, "it.type");
            return e0Var.renderType(type);
        }
    }

    public final void a(StringBuilder sb, q0 q0Var) {
        if (q0Var != null) {
            c0 type = q0Var.getType();
            m.checkNotNullExpressionValue(type, "receiver.type");
            sb.append(renderType(type));
            sb.append(".");
        }
    }

    public final void b(StringBuilder sb, d0.e0.p.d.m0.c.a aVar) {
        q0 instanceReceiverParameter = j0.getInstanceReceiverParameter(aVar);
        q0 extensionReceiverParameter = aVar.getExtensionReceiverParameter();
        a(sb, instanceReceiverParameter);
        boolean z2 = (instanceReceiverParameter == null || extensionReceiverParameter == null) ? false : true;
        if (z2) {
            sb.append("(");
        }
        a(sb, extensionReceiverParameter);
        if (z2) {
            sb.append(")");
        }
    }

    public final String renderFunction(x xVar) {
        m.checkNotNullParameter(xVar, "descriptor");
        StringBuilder sb = new StringBuilder();
        sb.append("fun ");
        e0 e0Var = f3169b;
        e0Var.b(sb, xVar);
        c cVar = a;
        e name = xVar.getName();
        m.checkNotNullExpressionValue(name, "descriptor.name");
        sb.append(cVar.renderName(name, true));
        List<c1> valueParameters = xVar.getValueParameters();
        m.checkNotNullExpressionValue(valueParameters, "descriptor.valueParameters");
        u.joinTo$default(valueParameters, sb, ", ", "(", ")", 0, null, a.j, 48, null);
        sb.append(": ");
        c0 returnType = xVar.getReturnType();
        m.checkNotNull(returnType);
        m.checkNotNullExpressionValue(returnType, "descriptor.returnType!!");
        sb.append(e0Var.renderType(returnType));
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public final String renderLambda(x xVar) {
        m.checkNotNullParameter(xVar, "invoke");
        StringBuilder sb = new StringBuilder();
        e0 e0Var = f3169b;
        e0Var.b(sb, xVar);
        List<c1> valueParameters = xVar.getValueParameters();
        m.checkNotNullExpressionValue(valueParameters, "invoke.valueParameters");
        u.joinTo$default(valueParameters, sb, ", ", "(", ")", 0, null, b.j, 48, null);
        sb.append(" -> ");
        c0 returnType = xVar.getReturnType();
        m.checkNotNull(returnType);
        m.checkNotNullExpressionValue(returnType, "invoke.returnType!!");
        sb.append(e0Var.renderType(returnType));
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public final String renderParameter(o oVar) {
        String str;
        m.checkNotNullParameter(oVar, "parameter");
        StringBuilder sb = new StringBuilder();
        int ordinal = oVar.getKind().ordinal();
        if (ordinal == 0) {
            sb.append("instance parameter");
        } else if (ordinal == 1) {
            sb.append("extension receiver parameter");
        } else if (ordinal == 2) {
            StringBuilder R = b.d.b.a.a.R("parameter #");
            R.append(oVar.getIndex());
            R.append(' ');
            R.append(oVar.getName());
            sb.append(R.toString());
        }
        sb.append(" of ");
        e0 e0Var = f3169b;
        d0.e0.p.d.m0.c.b descriptor = oVar.getCallable().getDescriptor();
        if (descriptor instanceof n0) {
            str = e0Var.renderProperty((n0) descriptor);
        } else if (descriptor instanceof x) {
            str = e0Var.renderFunction((x) descriptor);
        } else {
            throw new IllegalStateException(("Illegal callable: " + descriptor).toString());
        }
        sb.append(str);
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public final String renderProperty(n0 n0Var) {
        m.checkNotNullParameter(n0Var, "descriptor");
        StringBuilder sb = new StringBuilder();
        sb.append(n0Var.isVar() ? "var " : "val ");
        e0 e0Var = f3169b;
        e0Var.b(sb, n0Var);
        c cVar = a;
        e name = n0Var.getName();
        m.checkNotNullExpressionValue(name, "descriptor.name");
        sb.append(cVar.renderName(name, true));
        sb.append(": ");
        c0 type = n0Var.getType();
        m.checkNotNullExpressionValue(type, "descriptor.type");
        sb.append(e0Var.renderType(type));
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public final String renderType(c0 c0Var) {
        m.checkNotNullParameter(c0Var, "type");
        return a.renderType(c0Var);
    }
}
