package d0.e0.p.d;

import d0.e0.p.d.m0.c.k1.b.b;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function1;
/* compiled from: RuntimeTypeMapper.kt */
/* loaded from: classes3.dex */
public final class g0 extends o implements Function1<Class<?>, CharSequence> {
    public static final g0 j = new g0();

    public g0() {
        super(1);
    }

    public final CharSequence invoke(Class<?> cls) {
        m.checkNotNullExpressionValue(cls, "it");
        return b.getDesc(cls);
    }
}
