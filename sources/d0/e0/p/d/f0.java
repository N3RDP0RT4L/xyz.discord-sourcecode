package d0.e0.p.d;

import d0.e0.p.d.d;
import d0.e0.p.d.e;
import d0.e0.p.d.m0.b.i;
import d0.e0.p.d.m0.b.k;
import d0.e0.p.d.m0.b.q.c;
import d0.e0.p.d.m0.c.k1.b.p;
import d0.e0.p.d.m0.c.k1.b.s;
import d0.e0.p.d.m0.c.n0;
import d0.e0.p.d.m0.c.o0;
import d0.e0.p.d.m0.c.p0;
import d0.e0.p.d.m0.c.u0;
import d0.e0.p.d.m0.c.x;
import d0.e0.p.d.m0.e.a.d0;
import d0.e0.p.d.m0.e.a.h0.f;
import d0.e0.p.d.m0.e.a.k0.l;
import d0.e0.p.d.m0.e.a.z;
import d0.e0.p.d.m0.e.b.u;
import d0.e0.p.d.m0.f.a0.a;
import d0.e0.p.d.m0.f.a0.b.e;
import d0.e0.p.d.m0.f.a0.b.h;
import d0.e0.p.d.m0.f.n;
import d0.e0.p.d.m0.g.a;
import d0.e0.p.d.m0.g.b;
import d0.e0.p.d.m0.i.g;
import d0.e0.p.d.m0.k.y.d;
import d0.e0.p.d.m0.l.b.e0.j;
import d0.z.d.m;
import java.lang.reflect.Method;
/* compiled from: RuntimeTypeMapper.kt */
/* loaded from: classes3.dex */
public final class f0 {
    public static final a a;

    /* renamed from: b  reason: collision with root package name */
    public static final f0 f3170b = new f0();

    static {
        a aVar = a.topLevel(new b("java.lang.Void"));
        m.checkNotNullExpressionValue(aVar, "ClassId.topLevel(FqName(\"java.lang.Void\"))");
        a = aVar;
    }

    public final i a(Class<?> cls) {
        if (!cls.isPrimitive()) {
            return null;
        }
        d dVar = d.get(cls.getSimpleName());
        m.checkNotNullExpressionValue(dVar, "JvmPrimitiveType.get(simpleName)");
        return dVar.getPrimitiveType();
    }

    public final d.e b(x xVar) {
        String jvmMethodNameIfSpecial = d0.getJvmMethodNameIfSpecial(xVar);
        if (jvmMethodNameIfSpecial == null) {
            if (xVar instanceof o0) {
                String asString = d0.e0.p.d.m0.k.x.a.getPropertyIfAccessor(xVar).getName().asString();
                m.checkNotNullExpressionValue(asString, "descriptor.propertyIfAccessor.name.asString()");
                jvmMethodNameIfSpecial = z.getterName(asString);
            } else if (xVar instanceof p0) {
                String asString2 = d0.e0.p.d.m0.k.x.a.getPropertyIfAccessor(xVar).getName().asString();
                m.checkNotNullExpressionValue(asString2, "descriptor.propertyIfAccessor.name.asString()");
                jvmMethodNameIfSpecial = z.setterName(asString2);
            } else {
                jvmMethodNameIfSpecial = xVar.getName().asString();
                m.checkNotNullExpressionValue(jvmMethodNameIfSpecial, "descriptor.name.asString()");
            }
        }
        return new d.e(new e.b(jvmMethodNameIfSpecial, u.computeJvmDescriptor$default(xVar, false, false, 1, null)));
    }

    public final a mapJvmClassToKotlinClassId(Class<?> cls) {
        m.checkNotNullParameter(cls, "klass");
        if (cls.isArray()) {
            Class<?> componentType = cls.getComponentType();
            m.checkNotNullExpressionValue(componentType, "klass.componentType");
            i a2 = a(componentType);
            if (a2 != null) {
                return new a(k.l, a2.getArrayTypeName());
            }
            a aVar = a.topLevel(k.a.h.toSafe());
            m.checkNotNullExpressionValue(aVar, "ClassId.topLevel(Standar…s.FqNames.array.toSafe())");
            return aVar;
        } else if (m.areEqual(cls, Void.TYPE)) {
            return a;
        } else {
            i a3 = a(cls);
            if (a3 != null) {
                return new a(k.l, a3.getTypeName());
            }
            a classId = d0.e0.p.d.m0.c.k1.b.b.getClassId(cls);
            if (!classId.isLocal()) {
                c cVar = c.a;
                b asSingleFqName = classId.asSingleFqName();
                m.checkNotNullExpressionValue(asSingleFqName, "classId.asSingleFqName()");
                a mapJavaToKotlin = cVar.mapJavaToKotlin(asSingleFqName);
                if (mapJavaToKotlin != null) {
                    return mapJavaToKotlin;
                }
            }
            return classId;
        }
    }

    public final e mapPropertySignature(n0 n0Var) {
        m.checkNotNullParameter(n0Var, "possiblyOverriddenProperty");
        d0.e0.p.d.m0.c.b unwrapFakeOverride = d0.e0.p.d.m0.k.e.unwrapFakeOverride(n0Var);
        m.checkNotNullExpressionValue(unwrapFakeOverride, "DescriptorUtils.unwrapFa…ssiblyOverriddenProperty)");
        n0 original = ((n0) unwrapFakeOverride).getOriginal();
        m.checkNotNullExpressionValue(original, "DescriptorUtils.unwrapFa…rriddenProperty).original");
        d.e eVar = null;
        Method method = null;
        if (original instanceof j) {
            j jVar = (j) original;
            n proto = jVar.getProto();
            g.f<n, a.d> fVar = d0.e0.p.d.m0.f.a0.a.d;
            m.checkNotNullExpressionValue(fVar, "JvmProtoBuf.propertySignature");
            a.d dVar = (a.d) d0.e0.p.d.m0.f.z.e.getExtensionOrNull(proto, fVar);
            if (dVar != null) {
                return new e.c(original, proto, dVar, jVar.getNameResolver(), jVar.getTypeTable());
            }
        } else if (original instanceof d0.e0.p.d.m0.e.a.h0.g) {
            u0 source = ((d0.e0.p.d.m0.e.a.h0.g) original).getSource();
            if (!(source instanceof d0.e0.p.d.m0.e.a.j0.a)) {
                source = null;
            }
            d0.e0.p.d.m0.e.a.j0.a aVar = (d0.e0.p.d.m0.e.a.j0.a) source;
            l javaElement = aVar != null ? aVar.getJavaElement() : null;
            if (javaElement instanceof p) {
                return new e.a(((p) javaElement).getMember());
            }
            if (javaElement instanceof s) {
                Method member = ((s) javaElement).getMember();
                p0 setter = original.getSetter();
                u0 source2 = setter != null ? setter.getSource() : null;
                if (!(source2 instanceof d0.e0.p.d.m0.e.a.j0.a)) {
                    source2 = null;
                }
                d0.e0.p.d.m0.e.a.j0.a aVar2 = (d0.e0.p.d.m0.e.a.j0.a) source2;
                l javaElement2 = aVar2 != null ? aVar2.getJavaElement() : null;
                if (!(javaElement2 instanceof s)) {
                    javaElement2 = null;
                }
                s sVar = (s) javaElement2;
                if (sVar != null) {
                    method = sVar.getMember();
                }
                return new e.b(member, method);
            }
            throw new a0("Incorrect resolution sequence for Java field " + original + " (source = " + javaElement + ')');
        }
        o0 getter = original.getGetter();
        m.checkNotNull(getter);
        d.e b2 = b(getter);
        p0 setter2 = original.getSetter();
        if (setter2 != null) {
            eVar = b(setter2);
        }
        return new e.d(b2, eVar);
    }

    public final d mapSignature(x xVar) {
        Method member;
        e.b jvmConstructorSignature;
        e.b jvmMethodSignature;
        m.checkNotNullParameter(xVar, "possiblySubstitutedFunction");
        d0.e0.p.d.m0.c.b unwrapFakeOverride = d0.e0.p.d.m0.k.e.unwrapFakeOverride(xVar);
        m.checkNotNullExpressionValue(unwrapFakeOverride, "DescriptorUtils.unwrapFa…siblySubstitutedFunction)");
        x original = ((x) unwrapFakeOverride).getOriginal();
        m.checkNotNullExpressionValue(original, "DescriptorUtils.unwrapFa…titutedFunction).original");
        if (original instanceof d0.e0.p.d.m0.l.b.e0.b) {
            d0.e0.p.d.m0.l.b.e0.b bVar = (d0.e0.p.d.m0.l.b.e0.b) original;
            d0.e0.p.d.m0.i.n proto = bVar.getProto();
            if ((proto instanceof d0.e0.p.d.m0.f.i) && (jvmMethodSignature = h.a.getJvmMethodSignature((d0.e0.p.d.m0.f.i) proto, bVar.getNameResolver(), bVar.getTypeTable())) != null) {
                return new d.e(jvmMethodSignature);
            }
            if (!(proto instanceof d0.e0.p.d.m0.f.d) || (jvmConstructorSignature = h.a.getJvmConstructorSignature((d0.e0.p.d.m0.f.d) proto, bVar.getNameResolver(), bVar.getTypeTable())) == null) {
                return b(original);
            }
            d0.e0.p.d.m0.c.m containingDeclaration = xVar.getContainingDeclaration();
            m.checkNotNullExpressionValue(containingDeclaration, "possiblySubstitutedFunction.containingDeclaration");
            if (d0.e0.p.d.m0.k.g.isInlineClass(containingDeclaration)) {
                return new d.e(jvmConstructorSignature);
            }
            return new d.C0274d(jvmConstructorSignature);
        }
        l lVar = null;
        if (original instanceof f) {
            u0 source = ((f) original).getSource();
            if (!(source instanceof d0.e0.p.d.m0.e.a.j0.a)) {
                source = null;
            }
            d0.e0.p.d.m0.e.a.j0.a aVar = (d0.e0.p.d.m0.e.a.j0.a) source;
            l javaElement = aVar != null ? aVar.getJavaElement() : null;
            if (javaElement instanceof s) {
                lVar = javaElement;
            }
            s sVar = (s) lVar;
            if (sVar != null && (member = sVar.getMember()) != null) {
                return new d.c(member);
            }
            throw new a0("Incorrect resolution sequence for Java method " + original);
        } else if (original instanceof d0.e0.p.d.m0.e.a.h0.c) {
            u0 source2 = ((d0.e0.p.d.m0.e.a.h0.c) original).getSource();
            if (!(source2 instanceof d0.e0.p.d.m0.e.a.j0.a)) {
                source2 = null;
            }
            d0.e0.p.d.m0.e.a.j0.a aVar2 = (d0.e0.p.d.m0.e.a.j0.a) source2;
            if (aVar2 != null) {
                lVar = aVar2.getJavaElement();
            }
            if (lVar instanceof d0.e0.p.d.m0.c.k1.b.m) {
                return new d.b(((d0.e0.p.d.m0.c.k1.b.m) lVar).getMember());
            }
            if (lVar instanceof d0.e0.p.d.m0.c.k1.b.j) {
                d0.e0.p.d.m0.c.k1.b.j jVar = (d0.e0.p.d.m0.c.k1.b.j) lVar;
                if (jVar.isAnnotationType()) {
                    return new d.a(jVar.getElement());
                }
            }
            throw new a0("Incorrect resolution sequence for Java constructor " + original + " (" + lVar + ')');
        } else {
            boolean z2 = true;
            if (!d0.e0.p.d.m0.k.d.isEnumValueOfMethod(original) && !d0.e0.p.d.m0.k.d.isEnumValuesMethod(original) && (!m.areEqual(original.getName(), d0.e0.p.d.m0.b.q.a.e.getCLONE_NAME()) || !original.getValueParameters().isEmpty())) {
                z2 = false;
            }
            if (z2) {
                return b(original);
            }
            throw new a0("Unknown origin of " + original + " (" + original.getClass() + ')');
        }
    }
}
