package d0.e0.p.d;

import d0.e0.c;
import d0.e0.h;
import d0.e0.p.d.c0;
import d0.e0.p.d.m0.c.b;
import d0.e0.p.d.m0.c.e;
import d0.e0.p.d.m0.c.k1.a.f;
import d0.e0.p.d.m0.c.z0;
import d0.e0.p.d.m0.e.b.j;
import d0.e0.p.d.m0.e.b.p;
import d0.e0.p.d.m0.l.b.e0.g;
import d0.z.d.a0;
import d0.z.d.f0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
import kotlin.reflect.KType;
/* compiled from: KTypeParameterImpl.kt */
/* loaded from: classes3.dex */
public final class y implements h {
    public static final /* synthetic */ KProperty[] j = {a0.property1(new d0.z.d.y(a0.getOrCreateKotlinClass(y.class), "upperBounds", "getUpperBounds()Ljava/util/List;"))};
    public final c0.a k = c0.lazySoft(new a());
    public final z l;
    public final z0 m;

    /* compiled from: KTypeParameterImpl.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function0<List<? extends x>> {
        public a() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public final List<? extends x> invoke() {
            List<d0.e0.p.d.m0.n.c0> upperBounds = y.this.getDescriptor().getUpperBounds();
            m.checkNotNullExpressionValue(upperBounds, "descriptor.upperBounds");
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(upperBounds, 10));
            for (d0.e0.p.d.m0.n.c0 c0Var : upperBounds) {
                arrayList.add(new x(c0Var, null, 2, null));
            }
            return arrayList;
        }
    }

    public y(z zVar, z0 z0Var) {
        Object obj;
        h<?> hVar;
        Class<?> klass;
        m.checkNotNullParameter(z0Var, "descriptor");
        this.m = z0Var;
        if (zVar == null) {
            d0.e0.p.d.m0.c.m containingDeclaration = getDescriptor().getContainingDeclaration();
            m.checkNotNullExpressionValue(containingDeclaration, "descriptor.containingDeclaration");
            if (containingDeclaration instanceof e) {
                obj = a((e) containingDeclaration);
            } else if (containingDeclaration instanceof b) {
                d0.e0.p.d.m0.c.m containingDeclaration2 = ((b) containingDeclaration).getContainingDeclaration();
                m.checkNotNullExpressionValue(containingDeclaration2, "declaration.containingDeclaration");
                if (containingDeclaration2 instanceof e) {
                    hVar = a((e) containingDeclaration2);
                } else {
                    f fVar = null;
                    g gVar = (g) (!(containingDeclaration instanceof g) ? null : containingDeclaration);
                    if (gVar != null) {
                        d0.e0.p.d.m0.l.b.e0.f containerSource = gVar.getContainerSource();
                        j jVar = (j) (!(containerSource instanceof j) ? null : containerSource);
                        p knownJvmBinaryClass = jVar != null ? jVar.getKnownJvmBinaryClass() : null;
                        f fVar2 = knownJvmBinaryClass instanceof f ? knownJvmBinaryClass : fVar;
                        if (fVar2 == null || (klass = fVar2.getKlass()) == null) {
                            throw new a0("Container of deserialized member is not resolved: " + gVar);
                        }
                        c kotlinClass = d0.z.a.getKotlinClass(klass);
                        Objects.requireNonNull(kotlinClass, "null cannot be cast to non-null type kotlin.reflect.jvm.internal.KClassImpl<*>");
                        hVar = (h) kotlinClass;
                    } else {
                        throw new a0("Non-class callable descriptor must be deserialized: " + containingDeclaration);
                    }
                }
                obj = containingDeclaration.accept(new d0.e0.p.d.a(hVar), Unit.a);
            } else {
                throw new a0("Unknown type parameter container: " + containingDeclaration);
            }
            m.checkNotNullExpressionValue(obj, "when (val declaration = … $declaration\")\n        }");
            zVar = (z) obj;
        }
        this.l = zVar;
    }

    public final h<?> a(e eVar) {
        Class<?> javaClass = j0.toJavaClass(eVar);
        h<?> hVar = (h) (javaClass != null ? d0.z.a.getKotlinClass(javaClass) : null);
        if (hVar != null) {
            return hVar;
        }
        StringBuilder R = b.d.b.a.a.R("Type parameter container is not resolved: ");
        R.append(eVar.getContainingDeclaration());
        throw new a0(R.toString());
    }

    public boolean equals(Object obj) {
        if (obj instanceof y) {
            y yVar = (y) obj;
            if (m.areEqual(this.l, yVar.l) && m.areEqual(getName(), yVar.getName())) {
                return true;
            }
        }
        return false;
    }

    public z0 getDescriptor() {
        return this.m;
    }

    @Override // d0.e0.h
    public String getName() {
        String asString = getDescriptor().getName().asString();
        m.checkNotNullExpressionValue(asString, "descriptor.name.asString()");
        return asString;
    }

    @Override // d0.e0.h
    public List<KType> getUpperBounds() {
        return (List) this.k.getValue(this, j[0]);
    }

    @Override // d0.e0.h
    public d0.e0.j getVariance() {
        int ordinal = getDescriptor().getVariance().ordinal();
        if (ordinal == 0) {
            return d0.e0.j.INVARIANT;
        }
        if (ordinal == 1) {
            return d0.e0.j.IN;
        }
        if (ordinal == 2) {
            return d0.e0.j.OUT;
        }
        throw new NoWhenBranchMatchedException();
    }

    public int hashCode() {
        return getName().hashCode() + (this.l.hashCode() * 31);
    }

    public String toString() {
        return f0.j.toString(this);
    }
}
