package d0.e0.p;

import d0.e0.p.d.b;
import d0.e0.p.d.j0;
import d0.e0.p.d.m0.c.t0;
import d0.e0.p.d.m0.f.a0.b.f;
import d0.e0.p.d.m0.f.a0.b.g;
import d0.e0.p.d.m0.f.a0.b.h;
import d0.e0.p.d.m0.f.i;
import d0.e0.p.d.m0.f.t;
import d0.e0.p.d.m0.l.b.u;
import d0.z.d.a0;
import d0.z.d.j;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;
import kotlin.reflect.KDeclarationContainer;
import kotlin.reflect.KFunction;
/* compiled from: reflectLambda.kt */
/* loaded from: classes3.dex */
public final class c {

    /* compiled from: reflectLambda.kt */
    /* loaded from: classes3.dex */
    public static final /* synthetic */ class a extends j implements Function2<u, i, t0> {
        public static final a j = new a();

        public a() {
            super(2);
        }

        @Override // d0.z.d.d, kotlin.reflect.KCallable
        public final String getName() {
            return "loadFunction";
        }

        @Override // d0.z.d.d
        public final KDeclarationContainer getOwner() {
            return a0.getOrCreateKotlinClass(u.class);
        }

        @Override // d0.z.d.d
        public final String getSignature() {
            return "loadFunction(Lorg/jetbrains/kotlin/metadata/ProtoBuf$Function;)Lorg/jetbrains/kotlin/descriptors/SimpleFunctionDescriptor;";
        }

        public final t0 invoke(u uVar, i iVar) {
            m.checkNotNullParameter(uVar, "p1");
            m.checkNotNullParameter(iVar, "p2");
            return uVar.loadFunction(iVar);
        }
    }

    public static final <R> KFunction<R> reflect(d0.c<? extends R> cVar) {
        m.checkNotNullParameter(cVar, "$this$reflect");
        Metadata metadata = (Metadata) cVar.getClass().getAnnotation(Metadata.class);
        if (metadata != null) {
            String[] d1 = metadata.d1();
            boolean z2 = true;
            if (d1.length == 0) {
                d1 = null;
            }
            if (d1 != null) {
                Pair<g, i> readFunctionDataFrom = h.readFunctionDataFrom(d1, metadata.d2());
                g component1 = readFunctionDataFrom.component1();
                i component2 = readFunctionDataFrom.component2();
                int[] mv = metadata.mv();
                if ((metadata.xi() & 8) == 0) {
                    z2 = false;
                }
                f fVar = new f(mv, z2);
                Class<?> cls = cVar.getClass();
                t typeTable = component2.getTypeTable();
                m.checkNotNullExpressionValue(typeTable, "proto.typeTable");
                t0 t0Var = (t0) j0.deserializeToDescriptor(cls, component2, component1, new d0.e0.p.d.m0.f.z.g(typeTable), fVar, a.j);
                if (t0Var != null) {
                    return new d0.e0.p.d.j(b.m, t0Var);
                }
            }
        }
        return null;
    }
}
