package d0.e0;

import d0.z.d.m;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import kotlin.jvm.functions.Function1;
/* compiled from: TypesJVM.kt */
/* loaded from: classes3.dex */
public final class k implements ParameterizedType, Type {
    public final Type[] j;
    public final Class<?> k;
    public final Type l;

    /* compiled from: TypesJVM.kt */
    /* loaded from: classes3.dex */
    public static final /* synthetic */ class a extends d0.z.d.k implements Function1<Type, String> {
        public static final a j = new a();

        public a() {
            super(1, n.class, "typeToString", "typeToString(Ljava/lang/reflect/Type;)Ljava/lang/String;", 1);
        }

        public final String invoke(Type type) {
            m.checkNotNullParameter(type, "p1");
            return n.access$typeToString(type);
        }
    }

    public k(Class<?> cls, Type type, List<? extends Type> list) {
        m.checkNotNullParameter(cls, "rawType");
        m.checkNotNullParameter(list, "typeArguments");
        this.k = cls;
        this.l = type;
        Object[] array = list.toArray(new Type[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        this.j = (Type[]) array;
    }

    public boolean equals(Object obj) {
        if (obj instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) obj;
            if (m.areEqual(this.k, parameterizedType.getRawType()) && m.areEqual(this.l, parameterizedType.getOwnerType()) && Arrays.equals(getActualTypeArguments(), parameterizedType.getActualTypeArguments())) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.reflect.ParameterizedType
    public Type[] getActualTypeArguments() {
        return this.j;
    }

    @Override // java.lang.reflect.ParameterizedType
    public Type getOwnerType() {
        return this.l;
    }

    @Override // java.lang.reflect.ParameterizedType
    public Type getRawType() {
        return this.k;
    }

    @Override // java.lang.reflect.Type
    public String getTypeName() {
        StringBuilder sb = new StringBuilder();
        Type type = this.l;
        if (type != null) {
            sb.append(n.access$typeToString(type));
            sb.append("$");
            sb.append(this.k.getSimpleName());
        } else {
            sb.append(n.access$typeToString(this.k));
        }
        Type[] typeArr = this.j;
        if (!(typeArr.length == 0)) {
            d0.t.k.joinTo$default(typeArr, sb, null, "<", ">", 0, null, a.j, 50, null);
        }
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    public int hashCode() {
        int hashCode = this.k.hashCode();
        Type type = this.l;
        return (hashCode ^ (type != null ? type.hashCode() : 0)) ^ Arrays.hashCode(getActualTypeArguments());
    }

    public String toString() {
        return getTypeName();
    }
}
