package d0.e0;

import d0.z.d.k;
import kotlin.jvm.functions.Function1;
/* compiled from: TypesJVM.kt */
/* loaded from: classes3.dex */
public final /* synthetic */ class m extends k implements Function1<Class<? extends Object>, Class<?>> {
    public static final m j = new m();

    public m() {
        super(1, Class.class, "getComponentType", "getComponentType()Ljava/lang/Class;", 0);
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Class<?> invoke2(Class<?> cls) {
        d0.z.d.m.checkNotNullParameter(cls, "p1");
        return cls.getComponentType();
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Class<?> invoke(Class<? extends Object> cls) {
        return invoke2((Class<?>) cls);
    }
}
