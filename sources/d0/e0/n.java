package d0.e0;

import d0.f0.q;
import d0.g0.t;
import d0.t.o;
import d0.t.u;
import d0.z.a;
import d0.z.d.m;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import kotlin.reflect.KType;
import kotlin.sequences.Sequence;
/* compiled from: TypesJVM.kt */
/* loaded from: classes3.dex */
public final class n {
    public static final Type a(KType kType, boolean z2) {
        d classifier = kType.getClassifier();
        if (classifier instanceof h) {
            return new l((h) classifier);
        }
        if (classifier instanceof c) {
            c cVar = (c) classifier;
            Class javaObjectType = z2 ? a.getJavaObjectType(cVar) : a.getJavaClass(cVar);
            List<i> arguments = kType.getArguments();
            if (arguments.isEmpty()) {
                return javaObjectType;
            }
            if (!javaObjectType.isArray()) {
                return b(javaObjectType, arguments);
            }
            Class<?> componentType = javaObjectType.getComponentType();
            m.checkNotNullExpressionValue(componentType, "jClass.componentType");
            if (componentType.isPrimitive()) {
                return javaObjectType;
            }
            i iVar = (i) u.singleOrNull((List<? extends Object>) arguments);
            if (iVar != null) {
                j component1 = iVar.component1();
                KType component2 = iVar.component2();
                if (component1 == null) {
                    return javaObjectType;
                }
                int ordinal = component1.ordinal();
                if (ordinal != 0) {
                    if (ordinal == 1) {
                        return javaObjectType;
                    }
                    if (ordinal != 2) {
                        throw new NoWhenBranchMatchedException();
                    }
                }
                m.checkNotNull(component2);
                Type a = a(component2, false);
                return a instanceof Class ? javaObjectType : new a(a);
            }
            throw new IllegalArgumentException("kotlin.Array must have exactly one type argument: " + kType);
        }
        throw new UnsupportedOperationException("Unsupported type classifier: " + kType);
    }

    public static final String access$typeToString(Type type) {
        String str;
        if (!(type instanceof Class)) {
            return type.toString();
        }
        Class cls = (Class) type;
        if (cls.isArray()) {
            Sequence generateSequence = d0.f0.n.generateSequence(type, m.j);
            str = ((Class) q.last(generateSequence)).getName() + t.repeat("[]", q.count(generateSequence));
        } else {
            str = cls.getName();
        }
        m.checkNotNullExpressionValue(str, "if (type.isArray) {\n    …\n        } else type.name");
        return str;
    }

    public static final Type b(Class<?> cls, List<i> list) {
        Class<?> declaringClass = cls.getDeclaringClass();
        if (declaringClass == null) {
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (i iVar : list) {
                arrayList.add(c(iVar));
            }
            return new k(cls, null, arrayList);
        } else if (Modifier.isStatic(cls.getModifiers())) {
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (i iVar2 : list) {
                arrayList2.add(c(iVar2));
            }
            return new k(cls, declaringClass, arrayList2);
        } else {
            int length = cls.getTypeParameters().length;
            Type b2 = b(declaringClass, list.subList(length, list.size()));
            List<i> subList = list.subList(0, length);
            ArrayList arrayList3 = new ArrayList(o.collectionSizeOrDefault(subList, 10));
            for (i iVar3 : subList) {
                arrayList3.add(c(iVar3));
            }
            return new k(cls, b2, arrayList3);
        }
    }

    public static final Type c(i iVar) {
        j variance = iVar.getVariance();
        if (variance == null) {
            return o.k.getSTAR();
        }
        KType type = iVar.getType();
        m.checkNotNull(type);
        int ordinal = variance.ordinal();
        if (ordinal == 0) {
            return a(type, true);
        }
        if (ordinal == 1) {
            return new o(null, a(type, true));
        }
        if (ordinal == 2) {
            return new o(a(type, true), null);
        }
        throw new NoWhenBranchMatchedException();
    }

    public static final Type getJavaType(KType kType) {
        Type javaType;
        m.checkNotNullParameter(kType, "$this$javaType");
        return (!(kType instanceof d0.z.d.n) || (javaType = ((d0.z.d.n) kType).getJavaType()) == null) ? a(kType, false) : javaType;
    }
}
