package d0;

import kotlin.Pair;
/* compiled from: Tuples.kt */
/* loaded from: classes3.dex */
public final class o {
    public static final <A, B> Pair<A, B> to(A a, B b2) {
        return new Pair<>(a, b2);
    }
}
