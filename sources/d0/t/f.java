package d0.t;

import d0.z.d.g0.e;
import java.util.AbstractSet;
import java.util.Set;
/* compiled from: AbstractMutableSet.kt */
/* loaded from: classes3.dex */
public abstract class f<E> extends AbstractSet<E> implements Set<E>, e {
    public abstract int getSize();

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public final /* bridge */ int size() {
        return getSize();
    }
}
