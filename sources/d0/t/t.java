package d0.t;

import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
/* compiled from: _CollectionsJvm.kt */
/* loaded from: classes3.dex */
public class t extends s {
    public static final <R> List<R> filterIsInstance(Iterable<?> iterable, Class<R> cls) {
        m.checkNotNullParameter(iterable, "$this$filterIsInstance");
        m.checkNotNullParameter(cls, "klass");
        return (List) filterIsInstanceTo(iterable, new ArrayList(), cls);
    }

    public static final <C extends Collection<? super R>, R> C filterIsInstanceTo(Iterable<?> iterable, C c, Class<R> cls) {
        m.checkNotNullParameter(iterable, "$this$filterIsInstanceTo");
        m.checkNotNullParameter(c, "destination");
        m.checkNotNullParameter(cls, "klass");
        for (Object obj : iterable) {
            if (cls.isInstance(obj)) {
                c.add(obj);
            }
        }
        return c;
    }

    public static final <T> void reverse(List<T> list) {
        m.checkNotNullParameter(list, "$this$reverse");
        Collections.reverse(list);
    }

    public static final <T> SortedSet<T> toSortedSet(Iterable<? extends T> iterable, Comparator<? super T> comparator) {
        m.checkNotNullParameter(iterable, "$this$toSortedSet");
        m.checkNotNullParameter(comparator, "comparator");
        return (SortedSet) u.toCollection(iterable, new TreeSet(comparator));
    }
}
