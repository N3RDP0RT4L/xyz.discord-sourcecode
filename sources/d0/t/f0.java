package d0.t;

import d0.z.d.m;
import java.util.Map;
import java.util.NoSuchElementException;
/* compiled from: MapWithDefault.kt */
/* loaded from: classes3.dex */
public class f0 {
    public static final <K, V> V getOrImplicitDefaultNullable(Map<K, ? extends V> map, K k) {
        m.checkNotNullParameter(map, "$this$getOrImplicitDefault");
        if (map instanceof e0) {
            return (V) ((e0) map).getOrImplicitDefault(k);
        }
        V v = map.get(k);
        if (v != null || map.containsKey(k)) {
            return v;
        }
        throw new NoSuchElementException("Key " + k + " is missing in the map.");
    }
}
