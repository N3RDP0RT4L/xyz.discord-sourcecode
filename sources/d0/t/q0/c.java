package d0.t.q0;

import d0.t.j;
import d0.z.d.m;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MapBuilder.kt */
/* loaded from: classes3.dex */
public final class c<K, V> implements Map<K, V>, d0.z.d.g0.d {
    public static final a j = new a(null);
    public int k;
    public int l;
    public d0.t.q0.e<K> m;
    public d0.t.q0.f<V> n;
    public d0.t.q0.d<K, V> o;
    public boolean p;
    public K[] q;
    public V[] r;

    /* renamed from: s  reason: collision with root package name */
    public int[] f3555s;
    public int[] t;
    public int u;
    public int v;

    /* compiled from: MapBuilder.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final int access$computeHashSize(a aVar, int i) {
            Objects.requireNonNull(aVar);
            return Integer.highestOneBit(d0.d0.f.coerceAtLeast(i, 1) * 3);
        }

        public static final int access$computeShift(a aVar, int i) {
            Objects.requireNonNull(aVar);
            return Integer.numberOfLeadingZeros(i) + 1;
        }
    }

    /* compiled from: MapBuilder.kt */
    /* loaded from: classes3.dex */
    public static final class b<K, V> extends d<K, V> implements Iterator<Map.Entry<K, V>>, d0.z.d.g0.a {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(c<K, V> cVar) {
            super(cVar);
            m.checkNotNullParameter(cVar, "map");
        }

        public final void nextAppendString(StringBuilder sb) {
            m.checkNotNullParameter(sb, "sb");
            if (getIndex$kotlin_stdlib() < getMap$kotlin_stdlib().v) {
                int index$kotlin_stdlib = getIndex$kotlin_stdlib();
                setIndex$kotlin_stdlib(index$kotlin_stdlib + 1);
                setLastIndex$kotlin_stdlib(index$kotlin_stdlib);
                Object obj = getMap$kotlin_stdlib().q[getLastIndex$kotlin_stdlib()];
                if (m.areEqual(obj, getMap$kotlin_stdlib())) {
                    sb.append("(this Map)");
                } else {
                    sb.append(obj);
                }
                sb.append('=');
                Object[] objArr = getMap$kotlin_stdlib().r;
                m.checkNotNull(objArr);
                Object obj2 = objArr[getLastIndex$kotlin_stdlib()];
                if (m.areEqual(obj2, getMap$kotlin_stdlib())) {
                    sb.append("(this Map)");
                } else {
                    sb.append(obj2);
                }
                initNext$kotlin_stdlib();
                return;
            }
            throw new NoSuchElementException();
        }

        public final int nextHashCode$kotlin_stdlib() {
            if (getIndex$kotlin_stdlib() < getMap$kotlin_stdlib().v) {
                int index$kotlin_stdlib = getIndex$kotlin_stdlib();
                setIndex$kotlin_stdlib(index$kotlin_stdlib + 1);
                setLastIndex$kotlin_stdlib(index$kotlin_stdlib);
                Object obj = getMap$kotlin_stdlib().q[getLastIndex$kotlin_stdlib()];
                int i = 0;
                int hashCode = obj != null ? obj.hashCode() : 0;
                Object[] objArr = getMap$kotlin_stdlib().r;
                m.checkNotNull(objArr);
                Object obj2 = objArr[getLastIndex$kotlin_stdlib()];
                if (obj2 != null) {
                    i = obj2.hashCode();
                }
                int i2 = hashCode ^ i;
                initNext$kotlin_stdlib();
                return i2;
            }
            throw new NoSuchElementException();
        }

        @Override // java.util.Iterator
        public C0375c<K, V> next() {
            if (getIndex$kotlin_stdlib() < getMap$kotlin_stdlib().v) {
                int index$kotlin_stdlib = getIndex$kotlin_stdlib();
                setIndex$kotlin_stdlib(index$kotlin_stdlib + 1);
                setLastIndex$kotlin_stdlib(index$kotlin_stdlib);
                C0375c<K, V> cVar = new C0375c<>(getMap$kotlin_stdlib(), getLastIndex$kotlin_stdlib());
                initNext$kotlin_stdlib();
                return cVar;
            }
            throw new NoSuchElementException();
        }
    }

    /* compiled from: MapBuilder.kt */
    /* renamed from: d0.t.q0.c$c  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0375c<K, V> implements Map.Entry<K, V>, d0.z.d.g0.a {
        public final c<K, V> j;
        public final int k;

        public C0375c(c<K, V> cVar, int i) {
            m.checkNotNullParameter(cVar, "map");
            this.j = cVar;
            this.k = i;
        }

        @Override // java.util.Map.Entry
        public boolean equals(Object obj) {
            if (obj instanceof Map.Entry) {
                Map.Entry entry = (Map.Entry) obj;
                if (m.areEqual(entry.getKey(), getKey()) && m.areEqual(entry.getValue(), getValue())) {
                    return true;
                }
            }
            return false;
        }

        @Override // java.util.Map.Entry
        public K getKey() {
            return (K) this.j.q[this.k];
        }

        @Override // java.util.Map.Entry
        public V getValue() {
            Object[] objArr = this.j.r;
            m.checkNotNull(objArr);
            return (V) objArr[this.k];
        }

        @Override // java.util.Map.Entry
        public int hashCode() {
            K key = getKey();
            int i = 0;
            int hashCode = key != null ? key.hashCode() : 0;
            V value = getValue();
            if (value != null) {
                i = value.hashCode();
            }
            return hashCode ^ i;
        }

        @Override // java.util.Map.Entry
        public V setValue(V v) {
            this.j.checkIsMutable$kotlin_stdlib();
            Object[] a = this.j.a();
            int i = this.k;
            V v2 = (V) a[i];
            a[i] = v;
            return v2;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(getKey());
            sb.append('=');
            sb.append(getValue());
            return sb.toString();
        }
    }

    /* compiled from: MapBuilder.kt */
    /* loaded from: classes3.dex */
    public static class d<K, V> {
        public int j;
        public int k = -1;
        public final c<K, V> l;

        public d(c<K, V> cVar) {
            m.checkNotNullParameter(cVar, "map");
            this.l = cVar;
            initNext$kotlin_stdlib();
        }

        public final int getIndex$kotlin_stdlib() {
            return this.j;
        }

        public final int getLastIndex$kotlin_stdlib() {
            return this.k;
        }

        public final c<K, V> getMap$kotlin_stdlib() {
            return this.l;
        }

        public final boolean hasNext() {
            return this.j < this.l.v;
        }

        public final void initNext$kotlin_stdlib() {
            while (this.j < this.l.v) {
                int[] iArr = this.l.f3555s;
                int i = this.j;
                if (iArr[i] < 0) {
                    this.j = i + 1;
                } else {
                    return;
                }
            }
        }

        public final void remove() {
            if (this.k != -1) {
                this.l.checkIsMutable$kotlin_stdlib();
                this.l.g(this.k);
                this.k = -1;
                return;
            }
            throw new IllegalStateException("Call next() before removing element from the iterator.".toString());
        }

        public final void setIndex$kotlin_stdlib(int i) {
            this.j = i;
        }

        public final void setLastIndex$kotlin_stdlib(int i) {
            this.k = i;
        }
    }

    /* compiled from: MapBuilder.kt */
    /* loaded from: classes3.dex */
    public static final class e<K, V> extends d<K, V> implements Iterator<K>, d0.z.d.g0.a {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(c<K, V> cVar) {
            super(cVar);
            m.checkNotNullParameter(cVar, "map");
        }

        @Override // java.util.Iterator
        public K next() {
            if (getIndex$kotlin_stdlib() < getMap$kotlin_stdlib().v) {
                int index$kotlin_stdlib = getIndex$kotlin_stdlib();
                setIndex$kotlin_stdlib(index$kotlin_stdlib + 1);
                setLastIndex$kotlin_stdlib(index$kotlin_stdlib);
                K k = (K) getMap$kotlin_stdlib().q[getLastIndex$kotlin_stdlib()];
                initNext$kotlin_stdlib();
                return k;
            }
            throw new NoSuchElementException();
        }
    }

    /* compiled from: MapBuilder.kt */
    /* loaded from: classes3.dex */
    public static final class f<K, V> extends d<K, V> implements Iterator<V>, d0.z.d.g0.a {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(c<K, V> cVar) {
            super(cVar);
            m.checkNotNullParameter(cVar, "map");
        }

        @Override // java.util.Iterator
        public V next() {
            if (getIndex$kotlin_stdlib() < getMap$kotlin_stdlib().v) {
                int index$kotlin_stdlib = getIndex$kotlin_stdlib();
                setIndex$kotlin_stdlib(index$kotlin_stdlib + 1);
                setLastIndex$kotlin_stdlib(index$kotlin_stdlib);
                Object[] objArr = getMap$kotlin_stdlib().r;
                m.checkNotNull(objArr);
                V v = (V) objArr[getLastIndex$kotlin_stdlib()];
                initNext$kotlin_stdlib();
                return v;
            }
            throw new NoSuchElementException();
        }
    }

    public c() {
        this(8);
    }

    public final V[] a() {
        V[] vArr = this.r;
        if (vArr != null) {
            return vArr;
        }
        V[] vArr2 = (V[]) d0.t.q0.b.arrayOfUninitializedElements(this.q.length);
        this.r = vArr2;
        return vArr2;
    }

    public final int addKey$kotlin_stdlib(K k) {
        checkIsMutable$kotlin_stdlib();
        while (true) {
            int e2 = e(k);
            int coerceAtMost = d0.d0.f.coerceAtMost(this.u * 2, this.t.length / 2);
            int i = 0;
            while (true) {
                int[] iArr = this.t;
                int i2 = iArr[e2];
                if (i2 <= 0) {
                    int i3 = this.v;
                    K[] kArr = this.q;
                    if (i3 >= kArr.length) {
                        b(1);
                    } else {
                        int i4 = i3 + 1;
                        this.v = i4;
                        kArr[i3] = k;
                        this.f3555s[i3] = e2;
                        iArr[e2] = i4;
                        this.l = size() + 1;
                        if (i > this.u) {
                            this.u = i;
                        }
                        return i3;
                    }
                } else if (m.areEqual(this.q[i2 - 1], k)) {
                    return -i2;
                } else {
                    i++;
                    if (i > coerceAtMost) {
                        f(this.t.length * 2);
                        break;
                    }
                    e2--;
                    if (e2 == 0) {
                        e2 = this.t.length - 1;
                    }
                }
            }
        }
    }

    public final void b(int i) {
        int i2 = this.v;
        int i3 = i + i2;
        K[] kArr = this.q;
        if (i3 > kArr.length) {
            int length = (kArr.length * 3) / 2;
            if (i3 <= length) {
                i3 = length;
            }
            this.q = (K[]) d0.t.q0.b.copyOfUninitializedElements(kArr, i3);
            V[] vArr = this.r;
            this.r = vArr != null ? (V[]) d0.t.q0.b.copyOfUninitializedElements(vArr, i3) : null;
            int[] copyOf = Arrays.copyOf(this.f3555s, i3);
            m.checkNotNullExpressionValue(copyOf, "java.util.Arrays.copyOf(this, newSize)");
            this.f3555s = copyOf;
            int access$computeHashSize = a.access$computeHashSize(j, i3);
            if (access$computeHashSize > this.t.length) {
                f(access$computeHashSize);
            }
        } else if ((i2 + i3) - size() > this.q.length) {
            f(this.t.length);
        }
    }

    public final Map<K, V> build() {
        checkIsMutable$kotlin_stdlib();
        this.p = true;
        return this;
    }

    public final int c(K k) {
        int e2 = e(k);
        int i = this.u;
        while (true) {
            int i2 = this.t[e2];
            if (i2 == 0) {
                return -1;
            }
            if (i2 > 0) {
                int i3 = i2 - 1;
                if (m.areEqual(this.q[i3], k)) {
                    return i3;
                }
            }
            i--;
            if (i < 0) {
                return -1;
            }
            e2--;
            if (e2 == 0) {
                e2 = this.t.length - 1;
            }
        }
    }

    public final void checkIsMutable$kotlin_stdlib() {
        if (this.p) {
            throw new UnsupportedOperationException();
        }
    }

    @Override // java.util.Map
    public void clear() {
        checkIsMutable$kotlin_stdlib();
        int i = this.v - 1;
        if (i >= 0) {
            int i2 = 0;
            while (true) {
                int[] iArr = this.f3555s;
                int i3 = iArr[i2];
                if (i3 >= 0) {
                    this.t[i3] = 0;
                    iArr[i2] = -1;
                }
                if (i2 == i) {
                    break;
                }
                i2++;
            }
        }
        d0.t.q0.b.resetRange(this.q, 0, this.v);
        V[] vArr = this.r;
        if (vArr != null) {
            d0.t.q0.b.resetRange(vArr, 0, this.v);
        }
        this.l = 0;
        this.v = 0;
    }

    public final boolean containsAllEntries$kotlin_stdlib(Collection<?> collection) {
        m.checkNotNullParameter(collection, "m");
        for (Object obj : collection) {
            if (obj != null) {
                try {
                    if (!containsEntry$kotlin_stdlib((Map.Entry) obj)) {
                    }
                } catch (ClassCastException unused) {
                }
            }
            return false;
        }
        return true;
    }

    public final boolean containsEntry$kotlin_stdlib(Map.Entry<? extends K, ? extends V> entry) {
        m.checkNotNullParameter(entry, "entry");
        int c = c(entry.getKey());
        if (c < 0) {
            return false;
        }
        V[] vArr = this.r;
        m.checkNotNull(vArr);
        return m.areEqual(vArr[c], entry.getValue());
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public boolean containsKey(Object obj) {
        return c(obj) >= 0;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public boolean containsValue(Object obj) {
        return d(obj) >= 0;
    }

    public final int d(V v) {
        int i = this.v;
        while (true) {
            i--;
            if (i < 0) {
                return -1;
            }
            if (this.f3555s[i] >= 0) {
                V[] vArr = this.r;
                m.checkNotNull(vArr);
                if (m.areEqual(vArr[i], v)) {
                    return i;
                }
            }
        }
    }

    public final int e(K k) {
        return ((k != null ? k.hashCode() : 0) * (-1640531527)) >>> this.k;
    }

    public final b<K, V> entriesIterator$kotlin_stdlib() {
        return new b<>(this);
    }

    @Override // java.util.Map
    public final /* bridge */ Set<Map.Entry<K, V>> entrySet() {
        return getEntries();
    }

    @Override // java.util.Map
    public boolean equals(Object obj) {
        if (obj != this) {
            if (!(obj instanceof Map)) {
                return false;
            }
            Map map = (Map) obj;
            if (!(size() == map.size() && containsAllEntries$kotlin_stdlib(map.entrySet()))) {
                return false;
            }
        }
        return true;
    }

    public final void f(int i) {
        boolean z2;
        int i2;
        if (this.v > size()) {
            V[] vArr = this.r;
            int i3 = 0;
            int i4 = 0;
            while (true) {
                i2 = this.v;
                if (i3 >= i2) {
                    break;
                }
                if (this.f3555s[i3] >= 0) {
                    K[] kArr = this.q;
                    kArr[i4] = kArr[i3];
                    if (vArr != null) {
                        vArr[i4] = vArr[i3];
                    }
                    i4++;
                }
                i3++;
            }
            d0.t.q0.b.resetRange(this.q, i4, i2);
            if (vArr != null) {
                d0.t.q0.b.resetRange(vArr, i4, this.v);
            }
            this.v = i4;
        }
        int[] iArr = this.t;
        if (i != iArr.length) {
            this.t = new int[i];
            this.k = a.access$computeShift(j, i);
        } else {
            j.fill(iArr, 0, 0, iArr.length);
        }
        int i5 = 0;
        while (i5 < this.v) {
            int i6 = i5 + 1;
            int e2 = e(this.q[i5]);
            int i7 = this.u;
            while (true) {
                int[] iArr2 = this.t;
                z2 = true;
                if (iArr2[e2] == 0) {
                    iArr2[e2] = i6;
                    this.f3555s[i5] = e2;
                    break;
                }
                i7--;
                if (i7 < 0) {
                    z2 = false;
                    break;
                }
                e2--;
                if (e2 == 0) {
                    e2 = iArr2.length - 1;
                }
            }
            if (z2) {
                i5 = i6;
            } else {
                throw new IllegalStateException("This cannot happen with fixed magic multiplier and grow-only hash array. Have object hashCodes changed?");
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x005b A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void g(int r12) {
        /*
            r11 = this;
            K[] r0 = r11.q
            d0.t.q0.b.resetAt(r0, r12)
            int[] r0 = r11.f3555s
            r0 = r0[r12]
            int r1 = r11.u
            int r1 = r1 * 2
            int[] r2 = r11.t
            int r2 = r2.length
            int r2 = r2 / 2
            int r1 = d0.d0.f.coerceAtMost(r1, r2)
            r2 = 0
            r3 = r1
            r4 = 0
            r1 = r0
        L1a:
            int r5 = r0 + (-1)
            r6 = -1
            if (r0 != 0) goto L24
            int[] r0 = r11.t
            int r0 = r0.length
            int r0 = r0 + r6
            goto L25
        L24:
            r0 = r5
        L25:
            int r4 = r4 + 1
            int r5 = r11.u
            if (r4 <= r5) goto L30
            int[] r0 = r11.t
            r0[r1] = r2
            goto L5f
        L30:
            int[] r5 = r11.t
            r7 = r5[r0]
            if (r7 != 0) goto L39
            r5[r1] = r2
            goto L5f
        L39:
            if (r7 >= 0) goto L3e
            r5[r1] = r6
            goto L56
        L3e:
            K[] r5 = r11.q
            int r8 = r7 + (-1)
            r5 = r5[r8]
            int r5 = r11.e(r5)
            int r5 = r5 - r0
            int[] r9 = r11.t
            int r10 = r9.length
            int r10 = r10 + r6
            r5 = r5 & r10
            if (r5 < r4) goto L58
            r9[r1] = r7
            int[] r4 = r11.f3555s
            r4[r8] = r1
        L56:
            r1 = r0
            r4 = 0
        L58:
            int r3 = r3 + r6
            if (r3 >= 0) goto L1a
            int[] r0 = r11.t
            r0[r1] = r6
        L5f:
            int[] r0 = r11.f3555s
            r0[r12] = r6
            int r12 = r11.size()
            int r12 = r12 + r6
            r11.l = r12
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d0.t.q0.c.g(int):void");
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public V get(Object obj) {
        int c = c(obj);
        if (c < 0) {
            return null;
        }
        V[] vArr = this.r;
        m.checkNotNull(vArr);
        return vArr[c];
    }

    public Set<Map.Entry<K, V>> getEntries() {
        d0.t.q0.d<K, V> dVar = this.o;
        if (dVar != null) {
            return dVar;
        }
        d0.t.q0.d<K, V> dVar2 = new d0.t.q0.d<>(this);
        this.o = dVar2;
        return dVar2;
    }

    public Set<K> getKeys() {
        d0.t.q0.e<K> eVar = this.m;
        if (eVar != null) {
            return eVar;
        }
        d0.t.q0.e<K> eVar2 = new d0.t.q0.e<>(this);
        this.m = eVar2;
        return eVar2;
    }

    public int getSize() {
        return this.l;
    }

    public Collection<V> getValues() {
        d0.t.q0.f<V> fVar = this.n;
        if (fVar != null) {
            return fVar;
        }
        d0.t.q0.f<V> fVar2 = new d0.t.q0.f<>(this);
        this.n = fVar2;
        return fVar2;
    }

    @Override // java.util.Map
    public int hashCode() {
        b<K, V> entriesIterator$kotlin_stdlib = entriesIterator$kotlin_stdlib();
        int i = 0;
        while (entriesIterator$kotlin_stdlib.hasNext()) {
            i += entriesIterator$kotlin_stdlib.nextHashCode$kotlin_stdlib();
        }
        return i;
    }

    @Override // java.util.Map
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override // java.util.Map
    public final /* bridge */ Set<K> keySet() {
        return getKeys();
    }

    public final e<K, V> keysIterator$kotlin_stdlib() {
        return new e<>(this);
    }

    @Override // java.util.Map
    public V put(K k, V v) {
        checkIsMutable$kotlin_stdlib();
        int addKey$kotlin_stdlib = addKey$kotlin_stdlib(k);
        V[] a2 = a();
        if (addKey$kotlin_stdlib < 0) {
            int i = (-addKey$kotlin_stdlib) - 1;
            V v2 = a2[i];
            a2[i] = v;
            return v2;
        }
        a2[addKey$kotlin_stdlib] = v;
        return null;
    }

    @Override // java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        m.checkNotNullParameter(map, "from");
        checkIsMutable$kotlin_stdlib();
        Set<Map.Entry<? extends K, ? extends V>> entrySet = map.entrySet();
        if (!entrySet.isEmpty()) {
            b(entrySet.size());
            for (Map.Entry<? extends K, ? extends V> entry : entrySet) {
                int addKey$kotlin_stdlib = addKey$kotlin_stdlib(entry.getKey());
                V[] a2 = a();
                if (addKey$kotlin_stdlib >= 0) {
                    a2[addKey$kotlin_stdlib] = entry.getValue();
                } else {
                    int i = (-addKey$kotlin_stdlib) - 1;
                    if (!m.areEqual(entry.getValue(), a2[i])) {
                        a2[i] = entry.getValue();
                    }
                }
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public V remove(Object obj) {
        int removeKey$kotlin_stdlib = removeKey$kotlin_stdlib(obj);
        if (removeKey$kotlin_stdlib < 0) {
            return null;
        }
        V[] vArr = this.r;
        m.checkNotNull(vArr);
        V v = vArr[removeKey$kotlin_stdlib];
        d0.t.q0.b.resetAt(vArr, removeKey$kotlin_stdlib);
        return v;
    }

    public final boolean removeEntry$kotlin_stdlib(Map.Entry<? extends K, ? extends V> entry) {
        m.checkNotNullParameter(entry, "entry");
        checkIsMutable$kotlin_stdlib();
        int c = c(entry.getKey());
        if (c < 0) {
            return false;
        }
        V[] vArr = this.r;
        m.checkNotNull(vArr);
        if (!m.areEqual(vArr[c], entry.getValue())) {
            return false;
        }
        g(c);
        return true;
    }

    public final int removeKey$kotlin_stdlib(K k) {
        checkIsMutable$kotlin_stdlib();
        int c = c(k);
        if (c < 0) {
            return -1;
        }
        g(c);
        return c;
    }

    public final boolean removeValue$kotlin_stdlib(V v) {
        checkIsMutable$kotlin_stdlib();
        int d2 = d(v);
        if (d2 < 0) {
            return false;
        }
        g(d2);
        return true;
    }

    @Override // java.util.Map
    public final /* bridge */ int size() {
        return getSize();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((size() * 3) + 2);
        sb.append("{");
        b<K, V> entriesIterator$kotlin_stdlib = entriesIterator$kotlin_stdlib();
        int i = 0;
        while (entriesIterator$kotlin_stdlib.hasNext()) {
            if (i > 0) {
                sb.append(", ");
            }
            entriesIterator$kotlin_stdlib.nextAppendString(sb);
            i++;
        }
        sb.append("}");
        String sb2 = sb.toString();
        m.checkNotNullExpressionValue(sb2, "sb.toString()");
        return sb2;
    }

    @Override // java.util.Map
    public final /* bridge */ Collection<V> values() {
        return getValues();
    }

    public final f<K, V> valuesIterator$kotlin_stdlib() {
        return new f<>(this);
    }

    public c(int i) {
        int[] iArr = new int[i];
        a aVar = j;
        int access$computeHashSize = a.access$computeHashSize(aVar, i);
        this.q = (K[]) d0.t.q0.b.arrayOfUninitializedElements(i);
        this.r = null;
        this.f3555s = iArr;
        this.t = new int[access$computeHashSize];
        this.u = 2;
        this.v = 0;
        this.k = a.access$computeShift(aVar, access$computeHashSize);
    }
}
