package d0.t;

import b.d.b.a.a;
import d0.z.d.m;
/* compiled from: IndexedValue.kt */
/* loaded from: classes3.dex */
public final class z<T> {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final T f3556b;

    public z(int i, T t) {
        this.a = i;
        this.f3556b = t;
    }

    public final int component1() {
        return this.a;
    }

    public final T component2() {
        return this.f3556b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof z)) {
            return false;
        }
        z zVar = (z) obj;
        return this.a == zVar.a && m.areEqual(this.f3556b, zVar.f3556b);
    }

    public final int getIndex() {
        return this.a;
    }

    public final T getValue() {
        return this.f3556b;
    }

    public int hashCode() {
        int i = this.a * 31;
        T t = this.f3556b;
        return i + (t != null ? t.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("IndexedValue(index=");
        R.append(this.a);
        R.append(", value=");
        R.append(this.f3556b);
        R.append(")");
        return R.toString();
    }
}
