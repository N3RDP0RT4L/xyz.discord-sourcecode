package d0.t;

import d0.z.d.m;
import java.lang.reflect.Array;
import java.util.Objects;
/* compiled from: ArraysJVM.kt */
/* loaded from: classes3.dex */
public class h {
    public static final <T> T[] arrayOfNulls(T[] tArr, int i) {
        m.checkNotNullParameter(tArr, "reference");
        Object newInstance = Array.newInstance(tArr.getClass().getComponentType(), i);
        Objects.requireNonNull(newInstance, "null cannot be cast to non-null type kotlin.Array<T>");
        return (T[]) ((Object[]) newInstance);
    }

    public static final void copyOfRangeToIndexCheck(int i, int i2) {
        if (i > i2) {
            throw new IndexOutOfBoundsException("toIndex (" + i + ") is greater than size (" + i2 + ").");
        }
    }
}
