package d0.t;

import d0.z.d.g0.b;
import java.util.AbstractCollection;
import java.util.Collection;
/* compiled from: AbstractMutableCollection.kt */
/* loaded from: classes3.dex */
public abstract class d<E> extends AbstractCollection<E> implements Collection<E>, b {
    public abstract int getSize();

    @Override // java.util.AbstractCollection, java.util.Collection
    public final /* bridge */ int size() {
        return getSize();
    }
}
