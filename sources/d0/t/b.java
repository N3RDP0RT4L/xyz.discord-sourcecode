package d0.t;

import d0.z.d.g0.a;
import java.util.Iterator;
import java.util.NoSuchElementException;
/* compiled from: AbstractIterator.kt */
/* loaded from: classes3.dex */
public abstract class b<T> implements Iterator<T>, a {
    public int j = 2;
    public T k;

    public abstract void a();

    public final void b(T t) {
        this.k = t;
        this.j = 1;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        int i = this.j;
        if (i != 4) {
            int h = b.c.a.y.b.h(i);
            if (h != 0) {
                if (h == 2) {
                    return false;
                }
                this.j = 4;
                a();
                if (this.j != 1) {
                    return false;
                }
            }
            return true;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @Override // java.util.Iterator
    public T next() {
        if (hasNext()) {
            this.j = 2;
            return this.k;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
