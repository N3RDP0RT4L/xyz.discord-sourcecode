package d0.t;

import d0.z.d.m;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
/* compiled from: SetsJVM.kt */
/* loaded from: classes3.dex */
public class m0 {
    public static final <T> Set<T> setOf(T t) {
        Set<T> singleton = Collections.singleton(t);
        m.checkNotNullExpressionValue(singleton, "java.util.Collections.singleton(element)");
        return singleton;
    }

    public static final <T> TreeSet<T> sortedSetOf(Comparator<? super T> comparator, T... tArr) {
        m.checkNotNullParameter(comparator, "comparator");
        m.checkNotNullParameter(tArr, "elements");
        return (TreeSet) k.toCollection(tArr, new TreeSet(comparator));
    }
}
