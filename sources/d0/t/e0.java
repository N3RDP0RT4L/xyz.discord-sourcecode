package d0.t;

import d0.z.d.g0.a;
import java.util.Map;
/* compiled from: MapWithDefault.kt */
/* loaded from: classes3.dex */
public interface e0<K, V> extends Map<K, V>, a {
    V getOrImplicitDefault(K k);
}
