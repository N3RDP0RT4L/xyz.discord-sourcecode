package d0.t;

import d0.z.d.m;
import java.util.Iterator;
/* compiled from: Iterators.kt */
/* loaded from: classes3.dex */
public class p extends o {
    public static final <T> Iterator<z<T>> withIndex(Iterator<? extends T> it) {
        m.checkNotNullParameter(it, "$this$withIndex");
        return new b0(it);
    }
}
