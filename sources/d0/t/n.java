package d0.t;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.ranges.IntRange;
/* compiled from: Collections.kt */
/* loaded from: classes3.dex */
public class n extends m {
    public static final <T> ArrayList<T> arrayListOf(T... tArr) {
        m.checkNotNullParameter(tArr, "elements");
        return tArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new g(tArr, true));
    }

    public static final <T> Collection<T> asCollection(T[] tArr) {
        m.checkNotNullParameter(tArr, "$this$asCollection");
        return new g(tArr, false);
    }

    public static final <T extends Comparable<? super T>> int binarySearch(List<? extends T> list, T t, int i, int i2) {
        m.checkNotNullParameter(list, "$this$binarySearch");
        int size = list.size();
        if (i > i2) {
            throw new IllegalArgumentException("fromIndex (" + i + ") is greater than toIndex (" + i2 + ").");
        } else if (i < 0) {
            throw new IndexOutOfBoundsException(a.q("fromIndex (", i, ") is less than zero."));
        } else if (i2 <= size) {
            int i3 = i2 - 1;
            while (i <= i3) {
                int i4 = (i + i3) >>> 1;
                int compareValues = d0.u.a.compareValues(list.get(i4), t);
                if (compareValues < 0) {
                    i = i4 + 1;
                } else if (compareValues <= 0) {
                    return i4;
                } else {
                    i3 = i4 - 1;
                }
            }
            return -(i + 1);
        } else {
            throw new IndexOutOfBoundsException("toIndex (" + i2 + ") is greater than size (" + size + ").");
        }
    }

    public static /* synthetic */ int binarySearch$default(List list, Comparable comparable, int i, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            i2 = list.size();
        }
        return binarySearch(list, comparable, i, i2);
    }

    public static final <T> List<T> emptyList() {
        return w.j;
    }

    public static final IntRange getIndices(Collection<?> collection) {
        m.checkNotNullParameter(collection, "$this$indices");
        return new IntRange(0, collection.size() - 1);
    }

    public static final <T> int getLastIndex(List<? extends T> list) {
        m.checkNotNullParameter(list, "$this$lastIndex");
        return list.size() - 1;
    }

    public static final <T> List<T> listOf(T... tArr) {
        m.checkNotNullParameter(tArr, "elements");
        return tArr.length > 0 ? j.asList(tArr) : emptyList();
    }

    public static final <T> List<T> listOfNotNull(T t) {
        return t != null ? m.listOf(t) : emptyList();
    }

    public static final <T> List<T> mutableListOf(T... tArr) {
        m.checkNotNullParameter(tArr, "elements");
        return tArr.length == 0 ? new ArrayList() : new ArrayList(new g(tArr, true));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> List<T> optimizeReadOnlyList(List<? extends T> list) {
        m.checkNotNullParameter(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        if (size != 0) {
            return size != 1 ? list : m.listOf(list.get(0));
        }
        return emptyList();
    }

    public static final void throwCountOverflow() {
        throw new ArithmeticException("Count overflow has happened.");
    }

    public static final void throwIndexOverflow() {
        throw new ArithmeticException("Index overflow has happened.");
    }

    public static final <T> List<T> listOfNotNull(T... tArr) {
        m.checkNotNullParameter(tArr, "elements");
        return k.filterNotNull(tArr);
    }
}
