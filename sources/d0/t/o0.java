package d0.t;

import d0.z.d.m;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
/* compiled from: _Sets.kt */
/* loaded from: classes3.dex */
public class o0 extends n0 {
    public static final <T> Set<T> minus(Set<? extends T> set, T t) {
        m.checkNotNullParameter(set, "$this$minus");
        LinkedHashSet linkedHashSet = new LinkedHashSet(g0.mapCapacity(set.size()));
        boolean z2 = false;
        for (T t2 : set) {
            boolean z3 = true;
            if (!z2 && m.areEqual(t2, t)) {
                z2 = true;
                z3 = false;
            }
            if (z3) {
                linkedHashSet.add(t2);
            }
        }
        return linkedHashSet;
    }

    public static final <T> Set<T> plus(Set<? extends T> set, T t) {
        m.checkNotNullParameter(set, "$this$plus");
        LinkedHashSet linkedHashSet = new LinkedHashSet(g0.mapCapacity(set.size() + 1));
        linkedHashSet.addAll(set);
        linkedHashSet.add(t);
        return linkedHashSet;
    }

    public static final <T> Set<T> minus(Set<? extends T> set, Iterable<? extends T> iterable) {
        m.checkNotNullParameter(set, "$this$minus");
        m.checkNotNullParameter(iterable, "elements");
        Collection<?> convertToSetForSetOperationWith = o.convertToSetForSetOperationWith(iterable, set);
        if (convertToSetForSetOperationWith.isEmpty()) {
            return u.toSet(set);
        }
        if (convertToSetForSetOperationWith instanceof Set) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (T t : set) {
                if (!convertToSetForSetOperationWith.contains(t)) {
                    linkedHashSet.add(t);
                }
            }
            return linkedHashSet;
        }
        LinkedHashSet linkedHashSet2 = new LinkedHashSet(set);
        linkedHashSet2.removeAll(convertToSetForSetOperationWith);
        return linkedHashSet2;
    }

    public static final <T> Set<T> plus(Set<? extends T> set, Iterable<? extends T> iterable) {
        int i;
        m.checkNotNullParameter(set, "$this$plus");
        m.checkNotNullParameter(iterable, "elements");
        Integer collectionSizeOrNull = o.collectionSizeOrNull(iterable);
        if (collectionSizeOrNull != null) {
            i = set.size() + collectionSizeOrNull.intValue();
        } else {
            i = set.size() * 2;
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet(g0.mapCapacity(i));
        linkedHashSet.addAll(set);
        r.addAll(linkedHashSet, iterable);
        return linkedHashSet;
    }
}
