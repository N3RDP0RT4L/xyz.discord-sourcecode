package d0.t;

import d0.d0.f;
import d0.z.d.m;
import java.util.Arrays;
import java.util.Iterator;
import java.util.RandomAccess;
/* compiled from: SlidingWindow.kt */
/* loaded from: classes3.dex */
public final class l0<T> extends c<T> implements RandomAccess {
    public final int k;
    public int l;
    public int m;
    public final Object[] n;

    /* compiled from: SlidingWindow.kt */
    /* loaded from: classes3.dex */
    public static final class a extends b<T> {
        public int l;
        public int m;

        public a() {
            this.l = l0.this.size();
            this.m = l0.this.l;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // d0.t.b
        public void a() {
            if (this.l == 0) {
                this.j = 3;
                return;
            }
            b(l0.this.n[this.m]);
            this.m = (this.m + 1) % l0.this.k;
            this.l--;
        }
    }

    public l0(Object[] objArr, int i) {
        m.checkNotNullParameter(objArr, "buffer");
        this.n = objArr;
        boolean z2 = true;
        if (i >= 0) {
            if (i > objArr.length ? false : z2) {
                this.k = objArr.length;
                this.m = i;
                return;
            }
            StringBuilder S = b.d.b.a.a.S("ring buffer filled size: ", i, " cannot be larger than the buffer size: ");
            S.append(objArr.length);
            throw new IllegalArgumentException(S.toString().toString());
        }
        throw new IllegalArgumentException(b.d.b.a.a.p("ring buffer filled size should not be negative but it is ", i).toString());
    }

    @Override // d0.t.a, java.util.Collection
    public final void add(T t) {
        if (!isFull()) {
            this.n[(size() + this.l) % this.k] = t;
            this.m = size() + 1;
            return;
        }
        throw new IllegalStateException("ring buffer is full");
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final l0<T> expanded(int i) {
        Object[] objArr;
        int i2 = this.k;
        int coerceAtMost = f.coerceAtMost(i2 + (i2 >> 1) + 1, i);
        if (this.l == 0) {
            objArr = Arrays.copyOf(this.n, coerceAtMost);
            m.checkNotNullExpressionValue(objArr, "java.util.Arrays.copyOf(this, newSize)");
        } else {
            objArr = toArray(new Object[coerceAtMost]);
        }
        return new l0<>(objArr, size());
    }

    @Override // d0.t.c, java.util.List
    public T get(int i) {
        c.j.checkElementIndex$kotlin_stdlib(i, size());
        return (T) this.n[(this.l + i) % this.k];
    }

    @Override // d0.t.a
    public int getSize() {
        return this.m;
    }

    public final boolean isFull() {
        return size() == this.k;
    }

    @Override // d0.t.c, java.util.Collection, java.lang.Iterable, java.util.List
    public Iterator<T> iterator() {
        return new a();
    }

    public final void removeFirst(int i) {
        boolean z2 = true;
        if (i >= 0) {
            if (i > size()) {
                z2 = false;
            }
            if (!z2) {
                StringBuilder S = b.d.b.a.a.S("n shouldn't be greater than the buffer size: n = ", i, ", size = ");
                S.append(size());
                throw new IllegalArgumentException(S.toString().toString());
            } else if (i > 0) {
                int i2 = this.l;
                int i3 = (i2 + i) % this.k;
                if (i2 > i3) {
                    j.fill(this.n, (Object) null, i2, this.k);
                    j.fill(this.n, (Object) null, 0, i3);
                } else {
                    j.fill(this.n, (Object) null, i2, i3);
                }
                this.l = i3;
                this.m = size() - i;
            }
        } else {
            throw new IllegalArgumentException(b.d.b.a.a.p("n shouldn't be negative but it is ", i).toString());
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // d0.t.a, java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        m.checkNotNullParameter(tArr, "array");
        if (tArr.length < size()) {
            tArr = (T[]) Arrays.copyOf(tArr, size());
            m.checkNotNullExpressionValue(tArr, "java.util.Arrays.copyOf(this, newSize)");
        }
        int size = size();
        int i = 0;
        int i2 = 0;
        for (int i3 = this.l; i2 < size && i3 < this.k; i3++) {
            tArr[i2] = this.n[i3];
            i2++;
        }
        while (i2 < size) {
            tArr[i2] = this.n[i];
            i2++;
            i++;
        }
        if (tArr.length > size()) {
            tArr[size()] = null;
        }
        return tArr;
    }

    public l0(int i) {
        this(new Object[i], 0);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // d0.t.a, java.util.Collection
    public Object[] toArray() {
        return toArray(new Object[size()]);
    }
}
