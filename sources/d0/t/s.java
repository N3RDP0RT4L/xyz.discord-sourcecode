package d0.t;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.List;
import kotlin.ranges.IntRange;
/* compiled from: ReversedViews.kt */
/* loaded from: classes3.dex */
public class s extends r {
    public static final int access$reverseElementIndex(List list, int i) {
        int lastIndex = n.getLastIndex(list);
        if (i >= 0 && lastIndex >= i) {
            return n.getLastIndex(list) - i;
        }
        StringBuilder S = a.S("Element index ", i, " must be in range [");
        S.append(new IntRange(0, n.getLastIndex(list)));
        S.append("].");
        throw new IndexOutOfBoundsException(S.toString());
    }

    public static final int access$reversePositionIndex(List list, int i) {
        int size = list.size();
        if (i >= 0 && size >= i) {
            return list.size() - i;
        }
        StringBuilder S = a.S("Position index ", i, " must be in range [");
        S.append(new IntRange(0, list.size()));
        S.append("].");
        throw new IndexOutOfBoundsException(S.toString());
    }

    public static final <T> List<T> asReversed(List<? extends T> list) {
        m.checkNotNullParameter(list, "$this$asReversed");
        return new k0(list);
    }

    public static final <T> List<T> asReversedMutable(List<T> list) {
        m.checkNotNullParameter(list, "$this$asReversed");
        return new j0(list);
    }
}
