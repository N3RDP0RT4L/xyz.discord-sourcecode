package d0.w;

import d0.k;
import d0.w.h.b;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
/* compiled from: Continuation.kt */
/* loaded from: classes3.dex */
public final class e {
    public static final <R, T> void startCoroutine(Function2<? super R, ? super Continuation<? super T>, ? extends Object> function2, R r, Continuation<? super T> continuation) {
        m.checkNotNullParameter(function2, "$this$startCoroutine");
        m.checkNotNullParameter(continuation, "completion");
        Continuation intercepted = b.intercepted(b.createCoroutineUnintercepted(function2, r, continuation));
        Unit unit = Unit.a;
        k.a aVar = k.j;
        intercepted.resumeWith(k.m73constructorimpl(unit));
    }
}
