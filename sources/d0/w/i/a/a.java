package d0.w.i.a;

import d0.k;
import d0.l;
import d0.w.h.c;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
/* compiled from: ContinuationImpl.kt */
/* loaded from: classes3.dex */
public abstract class a implements Continuation<Object>, CoroutineStackFrame, Serializable {
    private final Continuation<Object> completion;

    public a(Continuation<Object> continuation) {
        this.completion = continuation;
    }

    public Continuation<Unit> create(Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        throw new UnsupportedOperationException("create(Continuation) has not been overridden");
    }

    public CoroutineStackFrame getCallerFrame() {
        Continuation<Object> continuation = this.completion;
        if (!(continuation instanceof CoroutineStackFrame)) {
            continuation = null;
        }
        return (CoroutineStackFrame) continuation;
    }

    public final Continuation<Object> getCompletion() {
        return this.completion;
    }

    public StackTraceElement getStackTraceElement() {
        return f.getStackTraceElement(this);
    }

    public abstract Object invokeSuspend(Object obj);

    public void releaseIntercepted() {
    }

    @Override // kotlin.coroutines.Continuation
    public final void resumeWith(Object obj) {
        Object invokeSuspend;
        a aVar = this;
        while (true) {
            g.probeCoroutineResumed(aVar);
            Continuation<Object> continuation = aVar.completion;
            m.checkNotNull(continuation);
            try {
                invokeSuspend = aVar.invokeSuspend(obj);
            } catch (Throwable th) {
                k.a aVar2 = k.j;
                obj = k.m73constructorimpl(l.createFailure(th));
            }
            if (invokeSuspend != c.getCOROUTINE_SUSPENDED()) {
                k.a aVar3 = k.j;
                obj = k.m73constructorimpl(invokeSuspend);
                aVar.releaseIntercepted();
                if (continuation instanceof a) {
                    aVar = (a) continuation;
                } else {
                    continuation.resumeWith(obj);
                    return;
                }
            } else {
                return;
            }
        }
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("Continuation at ");
        Object stackTraceElement = getStackTraceElement();
        if (stackTraceElement == null) {
            stackTraceElement = getClass().getName();
        }
        R.append(stackTraceElement);
        return R.toString();
    }

    public Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        throw new UnsupportedOperationException("create(Any?;Continuation) has not been overridden");
    }
}
