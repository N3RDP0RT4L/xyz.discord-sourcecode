package d0.w.i.a;

import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.z.d.m;
import java.lang.reflect.Field;
/* compiled from: DebugMetadata.kt */
/* loaded from: classes3.dex */
public final class f {
    public static final StackTraceElement getStackTraceElement(a aVar) {
        int i;
        String str;
        m.checkNotNullParameter(aVar, "$this$getStackTraceElementImpl");
        e eVar = (e) aVar.getClass().getAnnotation(e.class);
        Object obj = null;
        if (eVar == null) {
            return null;
        }
        int v = eVar.v();
        if (v <= 1) {
            int i2 = -1;
            try {
                Field declaredField = aVar.getClass().getDeclaredField("label");
                m.checkNotNullExpressionValue(declaredField, "field");
                declaredField.setAccessible(true);
                Object obj2 = declaredField.get(aVar);
                if (obj2 instanceof Integer) {
                    obj = obj2;
                }
                Integer num = (Integer) obj;
                i = (num != null ? num.intValue() : 0) - 1;
            } catch (Exception unused) {
                i = -1;
            }
            if (i >= 0) {
                i2 = eVar.l()[i];
            }
            String moduleName = h.c.getModuleName(aVar);
            if (moduleName == null) {
                str = eVar.c();
            } else {
                str = moduleName + MentionUtilsKt.SLASH_CHAR + eVar.c();
            }
            return new StackTraceElement(str, eVar.m(), eVar.f(), i2);
        }
        throw new IllegalStateException(("Debug metadata version mismatch. Expected: 1, got " + v + ". Please update the Kotlin standard library.").toString());
    }
}
