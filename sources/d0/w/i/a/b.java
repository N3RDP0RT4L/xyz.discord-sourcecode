package d0.w.i.a;
/* compiled from: boxing.kt */
/* loaded from: classes3.dex */
public final class b {
    public static final Boolean boxBoolean(boolean z2) {
        return Boolean.valueOf(z2);
    }

    public static final Integer boxInt(int i) {
        return new Integer(i);
    }

    public static final Long boxLong(long j) {
        return new Long(j);
    }
}
