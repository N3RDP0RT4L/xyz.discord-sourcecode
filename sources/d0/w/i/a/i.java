package d0.w.i.a;

import d0.w.f;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
/* compiled from: ContinuationImpl.kt */
/* loaded from: classes3.dex */
public abstract class i extends a {
    public i(Continuation<Object> continuation) {
        super(continuation);
        if (continuation != null) {
            if (!(continuation.getContext() == f.j)) {
                throw new IllegalArgumentException("Coroutines with restricted suspension must have EmptyCoroutineContext".toString());
            }
        }
    }

    @Override // kotlin.coroutines.Continuation
    public CoroutineContext getContext() {
        return f.j;
    }
}
