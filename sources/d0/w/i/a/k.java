package d0.w.i.a;

import d0.z.d.a0;
import d0.z.d.i;
import d0.z.d.m;
import kotlin.coroutines.Continuation;
/* compiled from: ContinuationImpl.kt */
/* loaded from: classes3.dex */
public abstract class k extends d implements i<Object> {
    private final int arity;

    public k(int i, Continuation<Object> continuation) {
        super(continuation);
        this.arity = i;
    }

    @Override // d0.z.d.i
    public int getArity() {
        return this.arity;
    }

    @Override // d0.w.i.a.a
    public String toString() {
        if (getCompletion() != null) {
            return super.toString();
        }
        String renderLambdaToString = a0.renderLambdaToString(this);
        m.checkNotNullExpressionValue(renderLambdaToString, "Reflection.renderLambdaToString(this)");
        return renderLambdaToString;
    }

    public k(int i) {
        this(i, null);
    }
}
