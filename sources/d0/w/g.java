package d0.w;

import d0.k;
import d0.w.h.c;
import d0.z.d.m;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: SafeContinuationJvm.kt */
/* loaded from: classes3.dex */
public final class g<T> implements Continuation<T>, CoroutineStackFrame {
    @Deprecated
    public static final AtomicReferenceFieldUpdater<g<?>, Object> j = AtomicReferenceFieldUpdater.newUpdater(g.class, Object.class, "result");
    public final Continuation<T> k;
    public volatile Object result;

    /* compiled from: SafeContinuationJvm.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new a(null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public g(Continuation<? super T> continuation, Object obj) {
        m.checkNotNullParameter(continuation, "delegate");
        this.k = continuation;
        this.result = obj;
    }

    @Override // kotlin.coroutines.Continuation
    public CoroutineContext getContext() {
        return this.k.getContext();
    }

    public final Object getOrThrow() {
        Object obj = this.result;
        d0.w.h.a aVar = d0.w.h.a.UNDECIDED;
        if (obj == aVar) {
            if (j.compareAndSet(this, aVar, c.getCOROUTINE_SUSPENDED())) {
                return c.getCOROUTINE_SUSPENDED();
            }
            obj = this.result;
        }
        if (obj == d0.w.h.a.RESUMED) {
            return c.getCOROUTINE_SUSPENDED();
        }
        if (!(obj instanceof k.b)) {
            return obj;
        }
        throw ((k.b) obj).exception;
    }

    @Override // kotlin.coroutines.Continuation
    public void resumeWith(Object obj) {
        while (true) {
            Object obj2 = this.result;
            d0.w.h.a aVar = d0.w.h.a.UNDECIDED;
            if (obj2 == aVar) {
                if (j.compareAndSet(this, aVar, obj)) {
                    return;
                }
            } else if (obj2 != c.getCOROUTINE_SUSPENDED()) {
                throw new IllegalStateException("Already resumed");
            } else if (j.compareAndSet(this, c.getCOROUTINE_SUSPENDED(), d0.w.h.a.RESUMED)) {
                this.k.resumeWith(obj);
                return;
            }
        }
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("SafeContinuation for ");
        R.append(this.k);
        return R.toString();
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public g(Continuation<? super T> continuation) {
        this(continuation, d0.w.h.a.UNDECIDED);
        m.checkNotNullParameter(continuation, "delegate");
    }
}
