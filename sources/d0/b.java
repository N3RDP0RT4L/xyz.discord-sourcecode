package d0;

import d0.z.d.m;
/* compiled from: Exceptions.kt */
/* loaded from: classes3.dex */
public class b {
    public static final void addSuppressed(Throwable th, Throwable th2) {
        m.checkNotNullParameter(th, "$this$addSuppressed");
        m.checkNotNullParameter(th2, "exception");
        if (th != th2) {
            d0.x.b.a.addSuppressed(th, th2);
        }
    }
}
