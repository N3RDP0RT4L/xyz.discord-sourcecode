package d0;

import d0.z.d.m;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Standard.kt */
/* loaded from: classes3.dex */
public final class j extends Error {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public j(String str) {
        super(str);
        m.checkNotNullParameter(str, "message");
    }

    public /* synthetic */ j(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "An operation is not implemented." : str);
    }
}
