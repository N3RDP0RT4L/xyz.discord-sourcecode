package d0.d0;

import d0.t.d0;
import java.util.NoSuchElementException;
/* compiled from: ProgressionIterators.kt */
/* loaded from: classes3.dex */
public final class d extends d0 {
    public final long j;
    public boolean k;
    public long l;
    public final long m;

    public d(long j, long j2, long j3) {
        this.m = j3;
        this.j = j2;
        boolean z2 = true;
        int i = (j3 > 0L ? 1 : (j3 == 0L ? 0 : -1));
        int i2 = (j > j2 ? 1 : (j == j2 ? 0 : -1));
        if (i <= 0 ? i2 < 0 : i2 > 0) {
            z2 = false;
        }
        this.k = z2;
        this.l = !z2 ? j2 : j;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.k;
    }

    @Override // d0.t.d0
    public long nextLong() {
        long j = this.l;
        if (j != this.j) {
            this.l = this.m + j;
        } else if (this.k) {
            this.k = false;
        } else {
            throw new NoSuchElementException();
        }
        return j;
    }
}
