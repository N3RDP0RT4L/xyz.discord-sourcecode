package d0.d0;

import andhook.lib.xposed.ClassUtils;
import d0.z.d.m;
/* compiled from: Ranges.kt */
/* loaded from: classes3.dex */
public class e {
    public static final void checkStepIsPositive(boolean z2, Number number) {
        m.checkNotNullParameter(number, "step");
        if (!z2) {
            throw new IllegalArgumentException("Step must be positive, was: " + number + ClassUtils.PACKAGE_SEPARATOR_CHAR);
        }
    }
}
