package d0.d0;

import java.util.Iterator;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Progressions.kt */
/* loaded from: classes3.dex */
public class c implements Iterable<Long>, d0.z.d.g0.a {
    public final long j;
    public final long k;
    public final long l;

    /* compiled from: Progressions.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    static {
        new a(null);
    }

    public c(long j, long j2, long j3) {
        if (j3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (j3 != Long.MIN_VALUE) {
            this.j = j;
            this.k = d0.x.c.getProgressionLastElement(j, j2, j3);
            this.l = j3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Long.MIN_VALUE to avoid overflow on negation.");
        }
    }

    public final long getFirst() {
        return this.j;
    }

    public final long getLast() {
        return this.k;
    }

    @Override // java.lang.Iterable
    /* renamed from: iterator */
    public Iterator<Long> iterator2() {
        return new d(this.j, this.k, this.l);
    }
}
