package com.swift.sandhook.wrapper;
/* loaded from: classes4.dex */
public class HookErrorException extends Exception {
    public HookErrorException(String str) {
        super(str);
    }

    public HookErrorException(String str, Throwable th) {
        super(str, th);
    }
}
