package com.facebook.animated.webp;

import android.graphics.Bitmap;
import b.f.d.d.c;
import b.f.j.a.a.d;
import b.f.j.d.b;
import java.nio.ByteBuffer;
@c
/* loaded from: classes2.dex */
public class WebPImage implements b.f.j.a.a.c, b.f.j.a.b.c {
    public Bitmap.Config a = null;
    @c
    private long mNativeContext;

    @c
    public WebPImage() {
    }

    private static native WebPImage nativeCreateFromDirectByteBuffer(ByteBuffer byteBuffer);

    private static native WebPImage nativeCreateFromNativeMemory(long j, int i);

    private native void nativeDispose();

    private native void nativeFinalize();

    private native int nativeGetDuration();

    private native WebPFrame nativeGetFrame(int i);

    private native int nativeGetFrameCount();

    private native int[] nativeGetFrameDurations();

    private native int nativeGetHeight();

    private native int nativeGetLoopCount();

    private native int nativeGetSizeInBytes();

    private native int nativeGetWidth();

    @Override // b.f.j.a.a.c
    public int a() {
        return nativeGetFrameCount();
    }

    @Override // b.f.j.a.a.c
    public int b() {
        return nativeGetLoopCount();
    }

    @Override // b.f.j.a.b.c
    public b.f.j.a.a.c c(ByteBuffer byteBuffer, b bVar) {
        b.f.j.m.b.a();
        byteBuffer.rewind();
        WebPImage nativeCreateFromDirectByteBuffer = nativeCreateFromDirectByteBuffer(byteBuffer);
        if (bVar != null) {
            nativeCreateFromDirectByteBuffer.a = bVar.e;
        }
        return nativeCreateFromDirectByteBuffer;
    }

    @Override // b.f.j.a.a.c
    public Bitmap.Config d() {
        return this.a;
    }

    @Override // b.f.j.a.a.c
    public d e(int i) {
        return nativeGetFrame(i);
    }

    @Override // b.f.j.a.a.c
    public boolean f() {
        return true;
    }

    public void finalize() {
        nativeFinalize();
    }

    @Override // b.f.j.a.a.c
    public b.f.j.a.a.b g(int i) {
        WebPFrame nativeGetFrame = nativeGetFrame(i);
        try {
            return new b.f.j.a.a.b(i, nativeGetFrame.b(), nativeGetFrame.c(), nativeGetFrame.getWidth(), nativeGetFrame.getHeight(), nativeGetFrame.d() ? 1 : 2, nativeGetFrame.e() ? 2 : 1);
        } finally {
            nativeGetFrame.dispose();
        }
    }

    @Override // b.f.j.a.a.c
    public int getHeight() {
        return nativeGetHeight();
    }

    @Override // b.f.j.a.a.c
    public int getWidth() {
        return nativeGetWidth();
    }

    @Override // b.f.j.a.b.c
    public b.f.j.a.a.c h(long j, int i, b bVar) {
        b.f.j.m.b.a();
        b.c.a.a0.d.i(Boolean.valueOf(j != 0));
        WebPImage nativeCreateFromNativeMemory = nativeCreateFromNativeMemory(j, i);
        if (bVar != null) {
            nativeCreateFromNativeMemory.a = bVar.e;
        }
        return nativeCreateFromNativeMemory;
    }

    @Override // b.f.j.a.a.c
    public int[] i() {
        return nativeGetFrameDurations();
    }

    @Override // b.f.j.a.a.c
    public int j() {
        return nativeGetSizeInBytes();
    }

    @c
    public WebPImage(long j) {
        this.mNativeContext = j;
    }
}
