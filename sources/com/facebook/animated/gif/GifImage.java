package com.facebook.animated.gif;

import android.graphics.Bitmap;
import b.f.d.d.c;
import b.f.j.a.a.d;
import b.f.j.d.b;
import b.f.m.n.a;
import java.nio.ByteBuffer;
@c
/* loaded from: classes2.dex */
public class GifImage implements b.f.j.a.a.c, b.f.j.a.b.c {
    public static volatile boolean a;

    /* renamed from: b  reason: collision with root package name */
    public Bitmap.Config f2853b = null;
    @c
    private long mNativeContext;

    @c
    public GifImage() {
    }

    public static synchronized void k() {
        synchronized (GifImage.class) {
            if (!a) {
                a = true;
                a.c("gifimage");
            }
        }
    }

    @c
    private static native GifImage nativeCreateFromDirectByteBuffer(ByteBuffer byteBuffer, int i, boolean z2);

    @c
    private static native GifImage nativeCreateFromFileDescriptor(int i, int i2, boolean z2);

    @c
    private static native GifImage nativeCreateFromNativeMemory(long j, int i, int i2, boolean z2);

    @c
    private native void nativeDispose();

    @c
    private native void nativeFinalize();

    @c
    private native int nativeGetDuration();

    @c
    private native GifFrame nativeGetFrame(int i);

    @c
    private native int nativeGetFrameCount();

    @c
    private native int[] nativeGetFrameDurations();

    @c
    private native int nativeGetHeight();

    @c
    private native int nativeGetLoopCount();

    @c
    private native int nativeGetSizeInBytes();

    @c
    private native int nativeGetWidth();

    @c
    private native boolean nativeIsAnimated();

    @Override // b.f.j.a.a.c
    public int a() {
        return nativeGetFrameCount();
    }

    @Override // b.f.j.a.a.c
    public int b() {
        int nativeGetLoopCount = nativeGetLoopCount();
        if (nativeGetLoopCount == -1) {
            return 1;
        }
        if (nativeGetLoopCount != 0) {
            return nativeGetLoopCount + 1;
        }
        return 0;
    }

    @Override // b.f.j.a.b.c
    public b.f.j.a.a.c c(ByteBuffer byteBuffer, b bVar) {
        k();
        byteBuffer.rewind();
        GifImage nativeCreateFromDirectByteBuffer = nativeCreateFromDirectByteBuffer(byteBuffer, bVar.c, false);
        nativeCreateFromDirectByteBuffer.f2853b = bVar.e;
        return nativeCreateFromDirectByteBuffer;
    }

    @Override // b.f.j.a.a.c
    public Bitmap.Config d() {
        return this.f2853b;
    }

    @Override // b.f.j.a.a.c
    public d e(int i) {
        return nativeGetFrame(i);
    }

    @Override // b.f.j.a.a.c
    public boolean f() {
        return false;
    }

    public void finalize() {
        nativeFinalize();
    }

    @Override // b.f.j.a.a.c
    public b.f.j.a.a.b g(int i) {
        int i2;
        GifFrame nativeGetFrame = nativeGetFrame(i);
        try {
            int b2 = nativeGetFrame.b();
            int c = nativeGetFrame.c();
            int width = nativeGetFrame.getWidth();
            int height = nativeGetFrame.getHeight();
            int d = nativeGetFrame.d();
            if (!(d == 0 || d == 1)) {
                i2 = 3;
                if (d == 2) {
                    i2 = 2;
                } else if (d == 3) {
                }
                return new b.f.j.a.a.b(i, b2, c, width, height, 1, i2);
            }
            i2 = 1;
            return new b.f.j.a.a.b(i, b2, c, width, height, 1, i2);
        } finally {
            nativeGetFrame.dispose();
        }
    }

    @Override // b.f.j.a.a.c
    public int getHeight() {
        return nativeGetHeight();
    }

    @Override // b.f.j.a.a.c
    public int getWidth() {
        return nativeGetWidth();
    }

    @Override // b.f.j.a.b.c
    public b.f.j.a.a.c h(long j, int i, b bVar) {
        k();
        b.c.a.a0.d.i(Boolean.valueOf(j != 0));
        GifImage nativeCreateFromNativeMemory = nativeCreateFromNativeMemory(j, i, bVar.c, false);
        nativeCreateFromNativeMemory.f2853b = bVar.e;
        return nativeCreateFromNativeMemory;
    }

    @Override // b.f.j.a.a.c
    public int[] i() {
        return nativeGetFrameDurations();
    }

    @Override // b.f.j.a.a.c
    public int j() {
        return nativeGetSizeInBytes();
    }

    @c
    public GifImage(long j) {
        this.mNativeContext = j;
    }
}
