package com.facebook.soloader;

import android.annotation.TargetApi;
import android.os.Trace;
import b.d.b.a.a;
import b.f.m.d;
import org.objectweb.asm.Opcodes;
@d
@TargetApi(18)
/* loaded from: classes2.dex */
public class Api18TraceUtils {
    public static void a(String str, String str2, String str3) {
        String w = a.w(str, str2, str3);
        if (w.length() > 127 && str2 != null) {
            StringBuilder R = a.R(str);
            R.append(str2.substring(0, (Opcodes.LAND - str.length()) - str3.length()));
            R.append(str3);
            w = R.toString();
        }
        Trace.beginSection(w);
    }
}
