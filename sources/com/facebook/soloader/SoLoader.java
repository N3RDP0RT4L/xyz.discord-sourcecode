package com.facebook.soloader;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;
import android.os.Trace;
import android.text.TextUtils;
import android.util.Log;
import b.f.m.c;
import b.f.m.d;
import b.f.m.e;
import b.f.m.j;
import b.f.m.k;
import b.f.m.l;
import b.f.m.m;
import com.adjust.sdk.Constants;
import dalvik.system.BaseDexClassLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.objectweb.asm.Opcodes;
/* loaded from: classes2.dex */
public class SoLoader {

    /* renamed from: b  reason: collision with root package name */
    public static k f2878b;
    public static m[] f;
    public static b.f.m.b g;
    public static int k;
    public static boolean l;
    public static final ReentrantReadWriteLock c = new ReentrantReadWriteLock();
    public static l[] d = null;
    public static volatile int e = 0;
    public static final HashSet<String> h = new HashSet<>();
    public static final Map<String, Object> i = new HashMap();
    public static final Set<String> j = Collections.newSetFromMap(new ConcurrentHashMap());
    public static final boolean a = true;

    @d
    @TargetApi(14)
    /* loaded from: classes2.dex */
    public static class Api14Utils {
        private Api14Utils() {
        }

        public static String a() {
            ClassLoader classLoader = SoLoader.class.getClassLoader();
            if (classLoader == null || (classLoader instanceof BaseDexClassLoader)) {
                try {
                    return (String) BaseDexClassLoader.class.getMethod("getLdLibraryPath", new Class[0]).invoke((BaseDexClassLoader) classLoader, new Object[0]);
                } catch (Exception e) {
                    throw new RuntimeException("Cannot call getLdLibraryPath", e);
                }
            } else {
                StringBuilder R = b.d.b.a.a.R("ClassLoader ");
                R.append(classLoader.getClass().getName());
                R.append(" should be of type BaseDexClassLoader");
                throw new IllegalStateException(R.toString());
            }
        }
    }

    /* loaded from: classes2.dex */
    public static class a implements k {
        public final /* synthetic */ boolean a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f2879b;
        public final /* synthetic */ String c;
        public final /* synthetic */ Runtime d;
        public final /* synthetic */ Method e;

        public a(boolean z2, String str, String str2, Runtime runtime, Method method) {
            this.a = z2;
            this.f2879b = str;
            this.c = str2;
            this.d = runtime;
            this.e = method;
        }

        public final String a(String str) {
            try {
                File file = new File(str);
                MessageDigest messageDigest = MessageDigest.getInstance(Constants.MD5);
                FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    byte[] bArr = new byte[4096];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read > 0) {
                            messageDigest.update(bArr, 0, read);
                        } else {
                            String format = String.format("%32x", new BigInteger(1, messageDigest.digest()));
                            fileInputStream.close();
                            return format;
                        }
                    }
                } catch (Throwable th) {
                    try {
                        throw th;
                    } catch (Throwable th2) {
                        try {
                            fileInputStream.close();
                        } catch (Throwable th3) {
                            th.addSuppressed(th3);
                        }
                        throw th2;
                    }
                }
            } catch (IOException e) {
                return e.toString();
            } catch (SecurityException e2) {
                return e2.toString();
            } catch (NoSuchAlgorithmException e3) {
                return e3.toString();
            }
        }

        /* JADX WARN: Removed duplicated region for block: B:41:0x0094  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void b(java.lang.String r9, int r10) {
            /*
                r8 = this;
                boolean r0 = r8.a
                if (r0 == 0) goto Lb5
                r0 = 4
                r10 = r10 & r0
                r1 = 1
                r2 = 0
                if (r10 != r0) goto Lc
                r10 = 1
                goto Ld
            Lc:
                r10 = 0
            Ld:
                if (r10 == 0) goto L12
                java.lang.String r10 = r8.f2879b
                goto L14
            L12:
                java.lang.String r10 = r8.c
            L14:
                r0 = 0
                java.lang.Runtime r3 = r8.d     // Catch: java.lang.Throwable -> L71 java.lang.reflect.InvocationTargetException -> L74 java.lang.IllegalArgumentException -> L76 java.lang.IllegalAccessException -> L78
                monitor-enter(r3)     // Catch: java.lang.Throwable -> L71 java.lang.reflect.InvocationTargetException -> L74 java.lang.IllegalArgumentException -> L76 java.lang.IllegalAccessException -> L78
                java.lang.reflect.Method r4 = r8.e     // Catch: java.lang.Throwable -> L65
                java.lang.Runtime r5 = r8.d     // Catch: java.lang.Throwable -> L65
                r6 = 3
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch: java.lang.Throwable -> L65
                r6[r2] = r9     // Catch: java.lang.Throwable -> L65
                java.lang.Class<com.facebook.soloader.SoLoader> r2 = com.facebook.soloader.SoLoader.class
                java.lang.ClassLoader r2 = r2.getClassLoader()     // Catch: java.lang.Throwable -> L65
                r6[r1] = r2     // Catch: java.lang.Throwable -> L65
                r1 = 2
                r6[r1] = r10     // Catch: java.lang.Throwable -> L65
                java.lang.Object r1 = r4.invoke(r5, r6)     // Catch: java.lang.Throwable -> L65
                java.lang.String r1 = (java.lang.String) r1     // Catch: java.lang.Throwable -> L65
                if (r1 != 0) goto L59
                monitor-exit(r3)     // Catch: java.lang.Throwable -> L5f
                if (r1 == 0) goto Lb8
                java.lang.String r0 = "SoLoader"
                java.lang.String r2 = "Error when loading lib: "
                java.lang.String r3 = " lib hash: "
                java.lang.StringBuilder r1 = b.d.b.a.a.W(r2, r1, r3)
                java.lang.String r9 = r8.a(r9)
                r1.append(r9)
                java.lang.String r9 = " search path is "
                r1.append(r9)
                r1.append(r10)
                java.lang.String r9 = r1.toString()
                android.util.Log.e(r0, r9)
                goto Lb8
            L59:
                java.lang.UnsatisfiedLinkError r0 = new java.lang.UnsatisfiedLinkError     // Catch: java.lang.Throwable -> L5f
                r0.<init>(r1)     // Catch: java.lang.Throwable -> L5f
                throw r0     // Catch: java.lang.Throwable -> L5f
            L5f:
                r0 = move-exception
                r2 = r8
                r7 = r1
                r1 = r0
                r0 = r7
                goto L67
            L65:
                r1 = move-exception
                r2 = r8
            L67:
                monitor-exit(r3)     // Catch: java.lang.Throwable -> L6f
                throw r1     // Catch: java.lang.reflect.InvocationTargetException -> L69 java.lang.IllegalArgumentException -> L6b java.lang.IllegalAccessException -> L6d java.lang.Throwable -> L91
            L69:
                r1 = move-exception
                goto L7a
            L6b:
                r1 = move-exception
                goto L7a
            L6d:
                r1 = move-exception
                goto L7a
            L6f:
                r1 = move-exception
                goto L67
            L71:
                r1 = move-exception
                r2 = r8
                goto L92
            L74:
                r1 = move-exception
                goto L79
            L76:
                r1 = move-exception
                goto L79
            L78:
                r1 = move-exception
            L79:
                r2 = r8
            L7a:
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch: java.lang.Throwable -> L91
                r3.<init>()     // Catch: java.lang.Throwable -> L91
                java.lang.String r4 = "Error: Cannot load "
                r3.append(r4)     // Catch: java.lang.Throwable -> L91
                r3.append(r9)     // Catch: java.lang.Throwable -> L91
                java.lang.String r0 = r3.toString()     // Catch: java.lang.Throwable -> L91
                java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch: java.lang.Throwable -> L91
                r3.<init>(r0, r1)     // Catch: java.lang.Throwable -> L91
                throw r3     // Catch: java.lang.Throwable -> L91
            L91:
                r1 = move-exception
            L92:
                if (r0 == 0) goto Lb4
                java.lang.String r3 = "SoLoader"
                java.lang.String r4 = "Error when loading lib: "
                java.lang.String r5 = " lib hash: "
                java.lang.StringBuilder r0 = b.d.b.a.a.W(r4, r0, r5)
                java.lang.String r9 = r2.a(r9)
                r0.append(r9)
                java.lang.String r9 = " search path is "
                r0.append(r9)
                r0.append(r10)
                java.lang.String r9 = r0.toString()
                android.util.Log.e(r3, r9)
            Lb4:
                throw r1
            Lb5:
                java.lang.System.load(r9)
            Lb8:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.SoLoader.a.b(java.lang.String, int):void");
        }
    }

    /* loaded from: classes2.dex */
    public static final class b extends UnsatisfiedLinkError {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b(java.lang.Throwable r3, java.lang.String r4) {
            /*
                r2 = this;
                java.lang.String r0 = "APK was built for a different platform. Supported ABIs: "
                java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
                java.lang.String[] r1 = b.c.a.a0.d.C0()
                java.lang.String r1 = java.util.Arrays.toString(r1)
                r0.append(r1)
                java.lang.String r1 = " error: "
                r0.append(r1)
                r0.append(r4)
                java.lang.String r4 = r0.toString()
                r2.<init>(r4)
                r2.initCause(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.SoLoader.b.<init>(java.lang.Throwable, java.lang.String):void");
        }
    }

    /* JADX WARN: Finally extract failed */
    public static void a(String str, int i2, StrictMode.ThreadPolicy threadPolicy) throws UnsatisfiedLinkError {
        boolean z2;
        Throwable th;
        int i3;
        m[] mVarArr;
        ReentrantReadWriteLock reentrantReadWriteLock = c;
        reentrantReadWriteLock.readLock().lock();
        try {
            if (d != null) {
                reentrantReadWriteLock.readLock().unlock();
                if (threadPolicy == null) {
                    threadPolicy = StrictMode.allowThreadDiskReads();
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (a) {
                    Api18TraceUtils.a("SoLoader.loadLibrary[", str, "]");
                }
                try {
                    reentrantReadWriteLock.readLock().lock();
                    i3 = 0;
                    int i4 = 0;
                    while (i3 == 0) {
                        l[] lVarArr = d;
                        if (i4 < lVarArr.length) {
                            i3 = lVarArr[i4].a(str, i2, threadPolicy);
                            if (i3 != 3 || f == null) {
                                i4++;
                            } else {
                                Log.d("SoLoader", "Trying backup SoSource for " + str);
                                for (m mVar : f) {
                                    synchronized (mVar) {
                                        synchronized (mVar.h(str)) {
                                            mVar.d = str;
                                            mVar.b(2);
                                        }
                                    }
                                    int a2 = mVar.a(str, i2, threadPolicy);
                                    if (a2 == 1) {
                                        i3 = a2;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    i3 = 0;
                }
                try {
                    ReentrantReadWriteLock reentrantReadWriteLock2 = c;
                    reentrantReadWriteLock2.readLock().unlock();
                    if (a) {
                        Trace.endSection();
                    }
                    if (z2) {
                        StrictMode.setThreadPolicy(threadPolicy);
                    }
                    if (i3 == 0 || i3 == 3) {
                        StringBuilder V = b.d.b.a.a.V("couldn't find DSO to load: ", str);
                        reentrantReadWriteLock2.readLock().lock();
                        for (int i5 = 0; i5 < d.length; i5++) {
                            V.append("\n\tSoSource ");
                            V.append(i5);
                            V.append(": ");
                            V.append(d[i5].toString());
                        }
                        b.f.m.b bVar = g;
                        if (bVar != null) {
                            File d2 = b.f.m.b.d(bVar.e());
                            V.append("\n\tNative lib dir: ");
                            V.append(d2.getAbsolutePath());
                            V.append("\n");
                        }
                        c.readLock().unlock();
                        V.append(" result: ");
                        V.append(i3);
                        String sb = V.toString();
                        Log.e("SoLoader", sb);
                        throw new UnsatisfiedLinkError(sb);
                    }
                } catch (Throwable th3) {
                    th = th3;
                    if (a) {
                        Trace.endSection();
                    }
                    if (z2) {
                        StrictMode.setThreadPolicy(threadPolicy);
                    }
                    if (i3 == 0 || i3 == 3) {
                        StringBuilder V2 = b.d.b.a.a.V("couldn't find DSO to load: ", str);
                        String message = th.getMessage();
                        if (message == null) {
                            message = th.toString();
                        }
                        V2.append(" caused by: ");
                        V2.append(message);
                        th.printStackTrace();
                        V2.append(" result: ");
                        V2.append(i3);
                        String sb2 = V2.toString();
                        Log.e("SoLoader", sb2);
                        UnsatisfiedLinkError unsatisfiedLinkError = new UnsatisfiedLinkError(sb2);
                        unsatisfiedLinkError.initCause(th);
                        throw unsatisfiedLinkError;
                    }
                }
            } else {
                Log.e("SoLoader", "Could not load: " + str + " because no SO source exists");
                throw new UnsatisfiedLinkError("couldn't find DSO to load: " + str);
            }
        } catch (Throwable th4) {
            c.readLock().unlock();
            throw th4;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:18:0x0042  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0044  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0047 A[Catch: all -> 0x0082, TryCatch #0 {, blocks: (B:5:0x0005, B:8:0x0009, B:13:0x001d, B:15:0x0038, B:21:0x0047, B:25:0x0052, B:27:0x0061, B:30:0x006c, B:31:0x006f, B:32:0x0072, B:33:0x0077), top: B:38:0x0003 }] */
    /* JADX WARN: Removed duplicated region for block: B:22:0x004d  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0052 A[Catch: all -> 0x0082, TryCatch #0 {, blocks: (B:5:0x0005, B:8:0x0009, B:13:0x001d, B:15:0x0038, B:21:0x0047, B:25:0x0052, B:27:0x0061, B:30:0x006c, B:31:0x006f, B:32:0x0072, B:33:0x0077), top: B:38:0x0003 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static synchronized void b(b.f.m.k r11) {
        /*
            java.lang.Class<com.facebook.soloader.SoLoader> r0 = com.facebook.soloader.SoLoader.class
            monitor-enter(r0)
            if (r11 == 0) goto L9
            com.facebook.soloader.SoLoader.f2878b = r11     // Catch: java.lang.Throwable -> L82
            monitor-exit(r0)
            return
        L9:
            java.lang.Runtime r5 = java.lang.Runtime.getRuntime()     // Catch: java.lang.Throwable -> L82
            java.lang.Class<java.lang.String> r11 = java.lang.String.class
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch: java.lang.Throwable -> L82
            r2 = 23
            r3 = 1
            r4 = 0
            r6 = 0
            if (r1 < r2) goto L3f
            r2 = 27
            if (r1 <= r2) goto L1d
            goto L3f
        L1d:
            java.lang.Class<java.lang.Runtime> r1 = java.lang.Runtime.class
            java.lang.String r2 = "nativeLoad"
            r7 = 3
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch: java.lang.SecurityException -> L35 java.lang.NoSuchMethodException -> L37 java.lang.Throwable -> L82
            r7[r4] = r11     // Catch: java.lang.SecurityException -> L35 java.lang.NoSuchMethodException -> L37 java.lang.Throwable -> L82
            java.lang.Class<java.lang.ClassLoader> r8 = java.lang.ClassLoader.class
            r7[r3] = r8     // Catch: java.lang.SecurityException -> L35 java.lang.NoSuchMethodException -> L37 java.lang.Throwable -> L82
            r8 = 2
            r7[r8] = r11     // Catch: java.lang.SecurityException -> L35 java.lang.NoSuchMethodException -> L37 java.lang.Throwable -> L82
            java.lang.reflect.Method r11 = r1.getDeclaredMethod(r2, r7)     // Catch: java.lang.SecurityException -> L35 java.lang.NoSuchMethodException -> L37 java.lang.Throwable -> L82
            r11.setAccessible(r3)     // Catch: java.lang.SecurityException -> L35 java.lang.NoSuchMethodException -> L37 java.lang.Throwable -> L82
            goto L40
        L35:
            r11 = move-exception
            goto L38
        L37:
            r11 = move-exception
        L38:
            java.lang.String r1 = "SoLoader"
            java.lang.String r2 = "Cannot get nativeLoad method"
            android.util.Log.w(r1, r2, r11)     // Catch: java.lang.Throwable -> L82
        L3f:
            r11 = r6
        L40:
            if (r11 == 0) goto L44
            r2 = 1
            goto L45
        L44:
            r2 = 0
        L45:
            if (r2 == 0) goto L4d
            java.lang.String r1 = com.facebook.soloader.SoLoader.Api14Utils.a()     // Catch: java.lang.Throwable -> L82
            r3 = r1
            goto L4e
        L4d:
            r3 = r6
        L4e:
            if (r3 != 0) goto L52
            r4 = r6
            goto L77
        L52:
            java.lang.String r1 = ":"
            java.lang.String[] r6 = r3.split(r1)     // Catch: java.lang.Throwable -> L82
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch: java.lang.Throwable -> L82
            int r8 = r6.length     // Catch: java.lang.Throwable -> L82
            r7.<init>(r8)     // Catch: java.lang.Throwable -> L82
            int r8 = r6.length     // Catch: java.lang.Throwable -> L82
        L5f:
            if (r4 >= r8) goto L72
            r9 = r6[r4]     // Catch: java.lang.Throwable -> L82
            java.lang.String r10 = "!"
            boolean r10 = r9.contains(r10)     // Catch: java.lang.Throwable -> L82
            if (r10 == 0) goto L6c
            goto L6f
        L6c:
            r7.add(r9)     // Catch: java.lang.Throwable -> L82
        L6f:
            int r4 = r4 + 1
            goto L5f
        L72:
            java.lang.String r1 = android.text.TextUtils.join(r1, r7)     // Catch: java.lang.Throwable -> L82
            r4 = r1
        L77:
            com.facebook.soloader.SoLoader$a r7 = new com.facebook.soloader.SoLoader$a     // Catch: java.lang.Throwable -> L82
            r1 = r7
            r6 = r11
            r1.<init>(r2, r3, r4, r5, r6)     // Catch: java.lang.Throwable -> L82
            com.facebook.soloader.SoLoader.f2878b = r7     // Catch: java.lang.Throwable -> L82
            monitor-exit(r0)
            return
        L82:
            r11 = move-exception
            monitor-exit(r0)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.SoLoader.b(b.f.m.k):void");
    }

    public static void c(Context context, int i2) throws IOException {
        String[] split;
        int i3;
        b.f.m.a aVar;
        boolean z2;
        c.writeLock().lock();
        try {
            if (d == null) {
                Log.d("SoLoader", "init start");
                k = i2;
                ArrayList arrayList = new ArrayList();
                String str = System.getenv("LD_LIBRARY_PATH");
                int i4 = 0;
                if (str == null) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        z2 = SysUtil$MarshmallowSysdeps.is64Bit();
                    } else {
                        try {
                            z2 = SysUtil$LollipopSysdeps.is64Bit();
                        } catch (Exception e2) {
                            Log.e("SysUtil", String.format("Could not read /proc/self/exe. Err msg: %s", e2.getMessage()));
                            z2 = false;
                        }
                    }
                    str = z2 ? "/vendor/lib64:/system/lib64" : "/vendor/lib:/system/lib";
                }
                for (String str2 : str.split(":")) {
                    Log.d("SoLoader", "adding system library source: " + str2);
                    arrayList.add(new c(new File(str2), 2));
                }
                if (context != null) {
                    if ((i2 & 1) != 0) {
                        f = null;
                        Log.d("SoLoader", "adding exo package source: lib-main");
                        arrayList.add(0, new e(context, "lib-main"));
                    } else {
                        if (l) {
                            i3 = 0;
                        } else {
                            g = new b.f.m.b(context, 0);
                            Log.d("SoLoader", "adding application source: " + g.toString());
                            arrayList.add(0, g);
                            i3 = 1;
                        }
                        if ((k & 8) != 0) {
                            f = null;
                        } else {
                            File file = new File(context.getApplicationInfo().sourceDir);
                            ArrayList arrayList2 = new ArrayList();
                            arrayList2.add(new b.f.m.a(context, file, "lib-main", i3));
                            Log.d("SoLoader", "adding backup source from : " + aVar.toString());
                            if (context.getApplicationInfo().splitSourceDirs != null) {
                                Log.d("SoLoader", "adding backup sources from split apks");
                                int i5 = 0;
                                for (String str3 : context.getApplicationInfo().splitSourceDirs) {
                                    File file2 = new File(str3);
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("lib-");
                                    i5++;
                                    sb.append(i5);
                                    b.f.m.a aVar2 = new b.f.m.a(context, file2, sb.toString(), i3);
                                    Log.d("SoLoader", "adding backup source: " + aVar2.toString());
                                    arrayList2.add(aVar2);
                                }
                            }
                            f = (m[]) arrayList2.toArray(new m[arrayList2.size()]);
                            arrayList.addAll(0, arrayList2);
                        }
                    }
                }
                l[] lVarArr = (l[]) arrayList.toArray(new l[arrayList.size()]);
                ReentrantReadWriteLock reentrantReadWriteLock = c;
                reentrantReadWriteLock.writeLock().lock();
                if ((k & 2) != 0) {
                    i4 = 1;
                }
                reentrantReadWriteLock.writeLock().unlock();
                int length = lVarArr.length;
                while (true) {
                    int i6 = length - 1;
                    if (length <= 0) {
                        break;
                    }
                    Log.d("SoLoader", "Preparing SO source: " + lVarArr[i6]);
                    lVarArr[i6].b(i4);
                    length = i6;
                }
                d = lVarArr;
                e++;
                Log.d("SoLoader", "init finish: " + d.length + " SO sources prepared");
            }
        } finally {
            Log.d("SoLoader", "init exiting");
            c.writeLock().unlock();
        }
    }

    public static boolean d(String str, String str2, String str3, int i2, StrictMode.ThreadPolicy threadPolicy) {
        boolean z2;
        Object obj;
        boolean z3 = false;
        if (!TextUtils.isEmpty(str2) && j.contains(str2)) {
            return false;
        }
        synchronized (SoLoader.class) {
            HashSet<String> hashSet = h;
            if (!hashSet.contains(str)) {
                z2 = false;
            } else if (str3 == null) {
                return false;
            } else {
                z2 = true;
            }
            Map<String, Object> map = i;
            if (map.containsKey(str)) {
                obj = map.get(str);
            } else {
                Object obj2 = new Object();
                map.put(str, obj2);
                obj = obj2;
            }
            ReentrantReadWriteLock reentrantReadWriteLock = c;
            reentrantReadWriteLock.readLock().lock();
            try {
                synchronized (obj) {
                    if (!z2) {
                        synchronized (SoLoader.class) {
                            if (hashSet.contains(str)) {
                                if (str3 == null) {
                                    reentrantReadWriteLock.readLock().unlock();
                                    return false;
                                }
                                z2 = true;
                            }
                            if (!z2) {
                                try {
                                    Log.d("SoLoader", "About to load: " + str);
                                    a(str, i2, threadPolicy);
                                    synchronized (SoLoader.class) {
                                        Log.d("SoLoader", "Loaded: " + str);
                                        hashSet.add(str);
                                    }
                                } catch (UnsatisfiedLinkError e2) {
                                    String message = e2.getMessage();
                                    if (message == null || !message.contains("unexpected e_machine:")) {
                                        throw e2;
                                    }
                                    throw new b(e2, message.substring(message.lastIndexOf("unexpected e_machine:")));
                                }
                            }
                        }
                    }
                    if ((i2 & 16) == 0) {
                        if (!TextUtils.isEmpty(str2) && j.contains(str2)) {
                            z3 = true;
                        }
                        if (str3 != null && !z3) {
                            if (a) {
                                Api18TraceUtils.a("MergedSoMapping.invokeJniOnload[", str2, "]");
                            }
                            try {
                                Log.d("SoLoader", "About to merge: " + str2 + " / " + str);
                                b.c.a.a0.d.Q0(str2);
                                throw null;
                            } catch (UnsatisfiedLinkError e3) {
                                throw new RuntimeException("Failed to call JNI_OnLoad from '" + str2 + "', which has been merged into '" + str + "'.  See comment for details.", e3);
                            }
                        }
                    }
                    reentrantReadWriteLock.readLock().unlock();
                    return !z2;
                }
            } catch (Throwable th) {
                c.readLock().unlock();
                throw th;
            }
        }
    }

    public static void init(Context context, int i2) throws IOException {
        StrictMode.ThreadPolicy allowThreadDiskWrites = StrictMode.allowThreadDiskWrites();
        boolean z2 = false;
        if ((i2 & 32) == 0 && context != null) {
            try {
                if ((context.getApplicationInfo().flags & Opcodes.LOR) != 0) {
                    z2 = true;
                }
            } finally {
                StrictMode.setThreadPolicy(allowThreadDiskWrites);
            }
        }
        l = z2;
        b(null);
        c(context, i2);
        b.f.m.n.a.a(new j());
    }
}
