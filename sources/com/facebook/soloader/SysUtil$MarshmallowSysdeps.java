package com.facebook.soloader;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Process;
import b.f.m.d;
import b.f.m.i;
import java.util.ArrayList;
import java.util.TreeSet;
@d
@TargetApi(23)
/* loaded from: classes2.dex */
public final class SysUtil$MarshmallowSysdeps {
    private SysUtil$MarshmallowSysdeps() {
    }

    @d
    public static String[] getSupportedAbis() {
        String[] strArr = Build.SUPPORTED_ABIS;
        TreeSet treeSet = new TreeSet();
        if (is64Bit()) {
            treeSet.add(i.AARCH64.toString());
            treeSet.add(i.X86_64.toString());
        } else {
            treeSet.add(i.ARM.toString());
            treeSet.add(i.X86.toString());
        }
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (treeSet.contains(str)) {
                arrayList.add(str);
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    @d
    public static boolean is64Bit() {
        return Process.is64Bit();
    }
}
