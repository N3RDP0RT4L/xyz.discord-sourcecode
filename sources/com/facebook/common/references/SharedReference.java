package com.facebook.common.references;

import android.graphics.Bitmap;
import b.c.a.a0.d;
import b.f.d.e.a;
import b.f.d.h.c;
import b.f.d.h.f;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Objects;
/* loaded from: classes2.dex */
public class SharedReference<T> {
    public static final Map<Object, Integer> a = new IdentityHashMap();

    /* renamed from: b  reason: collision with root package name */
    public T f2856b;
    public int c = 1;
    public final f<T> d;

    /* loaded from: classes2.dex */
    public static class NullReferenceException extends RuntimeException {
        public NullReferenceException() {
            super("Null shared reference");
        }
    }

    public SharedReference(T t, f<T> fVar) {
        Objects.requireNonNull(t);
        this.f2856b = t;
        Objects.requireNonNull(fVar);
        this.d = fVar;
        if (!(CloseableReference.k == 3) || (!(t instanceof Bitmap) && !(t instanceof c))) {
            Map<Object, Integer> map = a;
            synchronized (map) {
                Integer num = map.get(t);
                if (num == null) {
                    map.put(t, 1);
                } else {
                    map.put(t, Integer.valueOf(num.intValue() + 1));
                }
            }
        }
    }

    public void a() {
        int i;
        T t;
        synchronized (this) {
            b();
            d.i(Boolean.valueOf(this.c > 0));
            i = this.c - 1;
            this.c = i;
        }
        if (i == 0) {
            synchronized (this) {
                t = this.f2856b;
                this.f2856b = null;
            }
            if (t != null) {
                this.d.release(t);
                Map<Object, Integer> map = a;
                synchronized (map) {
                    Integer num = map.get(t);
                    if (num == null) {
                        a.p("SharedReference", "No entry in sLiveObjects for value of type %s", t.getClass());
                    } else if (num.intValue() == 1) {
                        map.remove(t);
                    } else {
                        map.put(t, Integer.valueOf(num.intValue() - 1));
                    }
                }
            }
        }
    }

    public final void b() {
        boolean z2;
        boolean z3;
        synchronized (this) {
            z2 = false;
            z3 = this.c > 0;
        }
        if (z3) {
            z2 = true;
        }
        if (!z2) {
            throw new NullReferenceException();
        }
    }

    public synchronized T c() {
        return this.f2856b;
    }
}
