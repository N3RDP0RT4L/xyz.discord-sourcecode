package com.facebook.cache.common;
/* loaded from: classes2.dex */
public interface CacheKey {
    boolean a();

    String b();

    boolean equals(Object obj);

    int hashCode();
}
