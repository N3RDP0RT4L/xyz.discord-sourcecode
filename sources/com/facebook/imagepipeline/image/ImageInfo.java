package com.facebook.imagepipeline.image;

import b.f.j.j.g;
/* loaded from: classes2.dex */
public interface ImageInfo extends g {
    int getHeight();

    int getWidth();
}
