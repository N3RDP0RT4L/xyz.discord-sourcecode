package com.facebook.imagepipeline.decoder;

import b.f.j.j.e;
/* loaded from: classes2.dex */
public class DecodeException extends RuntimeException {
    private final e mEncodedImage;

    public DecodeException(String str, e eVar) {
        super(str);
        this.mEncodedImage = eVar;
    }

    public e a() {
        return this.mEncodedImage;
    }
}
