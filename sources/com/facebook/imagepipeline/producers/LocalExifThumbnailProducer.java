package com.facebook.imagepipeline.producers;

import android.content.ContentResolver;
import b.f.d.d.f;
import b.f.d.g.g;
import b.f.j.j.e;
import b.f.j.p.e1;
import b.f.j.p.k1;
import b.f.j.p.l;
import b.f.j.p.x0;
import b.f.j.p.z0;
import b.f.m.d;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Map;
import java.util.concurrent.Executor;
/* loaded from: classes2.dex */
public class LocalExifThumbnailProducer implements k1<e> {
    public final Executor a;

    /* renamed from: b  reason: collision with root package name */
    public final g f2873b;
    public final ContentResolver c;

    @d
    /* loaded from: classes2.dex */
    public class Api24Utils {
    }

    /* loaded from: classes2.dex */
    public class a extends e1<e> {
        public final /* synthetic */ ImageRequest o;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(l lVar, z0 z0Var, x0 x0Var, String str, ImageRequest imageRequest) {
            super(lVar, z0Var, x0Var, str);
            this.o = imageRequest;
        }

        @Override // b.f.j.p.e1
        public void b(e eVar) {
            e eVar2 = eVar;
            if (eVar2 != null) {
                eVar2.close();
            }
        }

        @Override // b.f.j.p.e1
        public Map c(e eVar) {
            return f.of("createdThumbnail", Boolean.toString(eVar != null));
        }

        /* JADX WARN: Removed duplicated region for block: B:16:0x0034  */
        /* JADX WARN: Removed duplicated region for block: B:44:0x0088 A[Catch: StackOverflowError -> 0x0094, IOException -> 0x009b, TryCatch #7 {IOException -> 0x009b, StackOverflowError -> 0x0094, blocks: (B:28:0x0050, B:30:0x005b, B:34:0x0064, B:35:0x006a, B:37:0x0072, B:40:0x007c, B:42:0x0082, B:44:0x0088, B:46:0x008f), top: B:94:0x0050 }] */
        /* JADX WARN: Removed duplicated region for block: B:45:0x008e  */
        /* JADX WARN: Removed duplicated region for block: B:55:0x00cb  */
        /* JADX WARN: Removed duplicated region for block: B:65:0x0111  */
        /* JADX WARN: Removed duplicated region for block: B:66:0x011a  */
        /* JADX WARN: Removed duplicated region for block: B:68:0x011d  */
        /* JADX WARN: Removed duplicated region for block: B:72:0x0130 A[DONT_GENERATE] */
        @Override // b.f.j.p.e1
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b.f.j.j.e d() throws java.lang.Exception {
            /*
                Method dump skipped, instructions count: 333
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.producers.LocalExifThumbnailProducer.a.d():java.lang.Object");
        }
    }

    /* loaded from: classes2.dex */
    public class b extends b.f.j.p.e {
        public final /* synthetic */ e1 a;

        public b(LocalExifThumbnailProducer localExifThumbnailProducer, e1 e1Var) {
            this.a = e1Var;
        }

        @Override // b.f.j.p.y0
        public void a() {
            this.a.a();
        }
    }

    public LocalExifThumbnailProducer(Executor executor, g gVar, ContentResolver contentResolver) {
        this.a = executor;
        this.f2873b = gVar;
        this.c = contentResolver;
    }

    @Override // b.f.j.p.k1
    public boolean a(b.f.j.d.e eVar) {
        return b.c.a.a0.d.S0(512, 512, eVar);
    }

    @Override // b.f.j.p.w0
    public void b(l<e> lVar, x0 x0Var) {
        z0 o = x0Var.o();
        ImageRequest e = x0Var.e();
        x0Var.i("local", "exif");
        a aVar = new a(lVar, o, x0Var, "LocalExifThumbnailProducer", e);
        x0Var.f(new b(this, aVar));
        this.a.execute(aVar);
    }
}
