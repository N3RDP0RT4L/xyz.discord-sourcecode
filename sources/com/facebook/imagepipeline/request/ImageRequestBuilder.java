package com.facebook.imagepipeline.request;

import android.net.Uri;
import b.f.j.d.a;
import b.f.j.d.b;
import b.f.j.d.d;
import b.f.j.d.f;
import b.f.j.k.e;
import com.discord.models.domain.ModelAuditLogEntry;
import com.facebook.imagepipeline.request.ImageRequest;
import java.util.Objects;
/* loaded from: classes2.dex */
public class ImageRequestBuilder {
    public e n;
    public int p;
    public Uri a = null;

    /* renamed from: b  reason: collision with root package name */
    public ImageRequest.c f2876b = ImageRequest.c.FULL_FETCH;
    public int c = 0;
    public b.f.j.d.e d = null;
    public f e = null;
    public b f = b.a;
    public ImageRequest.b g = ImageRequest.b.DEFAULT;
    public boolean h = false;
    public boolean i = false;
    public boolean j = false;
    public d k = d.HIGH;
    public b.f.j.q.b l = null;
    public Boolean m = null;
    public a o = null;

    /* loaded from: classes2.dex */
    public static class BuilderException extends RuntimeException {
        public BuilderException(String str) {
            super(b.d.b.a.a.v("Invalid request builder: ", str));
        }
    }

    public static ImageRequestBuilder b(Uri uri) {
        ImageRequestBuilder imageRequestBuilder = new ImageRequestBuilder();
        Objects.requireNonNull(uri);
        imageRequestBuilder.a = uri;
        return imageRequestBuilder;
    }

    public ImageRequest a() {
        Uri uri = this.a;
        if (uri != null) {
            if ("res".equals(b.f.d.l.b.a(uri))) {
                if (!this.a.isAbsolute()) {
                    throw new BuilderException("Resource URI path must be absolute.");
                } else if (!this.a.getPath().isEmpty()) {
                    try {
                        Integer.parseInt(this.a.getPath().substring(1));
                    } catch (NumberFormatException unused) {
                        throw new BuilderException("Resource URI path must be a resource id.");
                    }
                } else {
                    throw new BuilderException("Resource URI must not be empty");
                }
            }
            if (!ModelAuditLogEntry.CHANGE_KEY_ASSET.equals(b.f.d.l.b.a(this.a)) || this.a.isAbsolute()) {
                return new ImageRequest(this);
            }
            throw new BuilderException("Asset URI path must be absolute.");
        }
        throw new BuilderException("Source must be set!");
    }
}
