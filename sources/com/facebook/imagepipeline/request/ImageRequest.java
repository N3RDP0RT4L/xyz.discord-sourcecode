package com.facebook.imagepipeline.request;

import android.net.Uri;
import androidx.core.app.NotificationCompat;
import b.f.d.d.d;
import b.f.d.d.i;
import b.f.j.d.e;
import b.f.j.d.f;
import com.discord.models.domain.ModelAuditLogEntry;
import com.facebook.cache.common.CacheKey;
import java.io.File;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
/* loaded from: classes2.dex */
public class ImageRequest {
    public static final d<ImageRequest, Uri> a = new a();

    /* renamed from: b  reason: collision with root package name */
    public final b f2874b;
    public final Uri c;
    public final int d;
    public File e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final b.f.j.d.b i;
    public final e j;
    public final f k;
    public final b.f.j.d.a l;
    public final b.f.j.d.d m;
    public final c n;
    public final int o;
    public final boolean p;
    public final boolean q;
    public final Boolean r;

    /* renamed from: s  reason: collision with root package name */
    public final b.f.j.q.b f2875s;
    public final b.f.j.k.e t;
    public final int u;

    /* loaded from: classes2.dex */
    public static class a implements d<ImageRequest, Uri> {
    }

    /* loaded from: classes2.dex */
    public enum b {
        SMALL,
        DEFAULT
    }

    /* loaded from: classes2.dex */
    public enum c {
        FULL_FETCH(1),
        DISK_CACHE(2),
        ENCODED_MEMORY_CACHE(3),
        BITMAP_MEMORY_CACHE(4);
        
        private int mValue;

        c(int i) {
            this.mValue = i;
        }

        public static c f(c cVar, c cVar2) {
            return cVar.mValue > cVar2.mValue ? cVar : cVar2;
        }

        public int g() {
            return this.mValue;
        }
    }

    public ImageRequest(ImageRequestBuilder imageRequestBuilder) {
        this.f2874b = imageRequestBuilder.g;
        Uri uri = imageRequestBuilder.a;
        this.c = uri;
        boolean z2 = true;
        int i = -1;
        if (uri != null) {
            if (b.f.d.l.b.e(uri)) {
                i = 0;
            } else if (b.f.d.l.b.d(uri)) {
                String path = uri.getPath();
                Map<String, String> map = b.f.d.f.a.a;
                int lastIndexOf = path.lastIndexOf(46);
                String str = null;
                String substring = (lastIndexOf < 0 || lastIndexOf == path.length() + (-1)) ? null : path.substring(lastIndexOf + 1);
                if (substring != null) {
                    String lowerCase = substring.toLowerCase(Locale.US);
                    String str2 = b.f.d.f.b.c.get(lowerCase);
                    str = str2 == null ? b.f.d.f.b.a.getMimeTypeFromExtension(lowerCase) : str2;
                    if (str == null) {
                        str = b.f.d.f.a.a.get(lowerCase);
                    }
                }
                i = str != null && str.startsWith("video/") ? 2 : 3;
            } else if (b.f.d.l.b.c(uri)) {
                i = 4;
            } else if (ModelAuditLogEntry.CHANGE_KEY_ASSET.equals(b.f.d.l.b.a(uri))) {
                i = 5;
            } else if ("res".equals(b.f.d.l.b.a(uri))) {
                i = 6;
            } else if ("data".equals(b.f.d.l.b.a(uri))) {
                i = 7;
            } else if ("android.resource".equals(b.f.d.l.b.a(uri))) {
                i = 8;
            }
        }
        this.d = i;
        this.f = imageRequestBuilder.h;
        this.g = imageRequestBuilder.i;
        this.h = imageRequestBuilder.j;
        this.i = imageRequestBuilder.f;
        this.j = imageRequestBuilder.d;
        f fVar = imageRequestBuilder.e;
        this.k = fVar == null ? f.a : fVar;
        this.l = imageRequestBuilder.o;
        this.m = imageRequestBuilder.k;
        this.n = imageRequestBuilder.f2876b;
        int i2 = imageRequestBuilder.c;
        this.o = i2;
        this.p = (i2 & 48) == 0 && b.f.d.l.b.e(imageRequestBuilder.a);
        this.q = (imageRequestBuilder.c & 15) != 0 ? false : z2;
        this.r = imageRequestBuilder.m;
        this.f2875s = imageRequestBuilder.l;
        this.t = imageRequestBuilder.n;
        this.u = imageRequestBuilder.p;
    }

    public synchronized File a() {
        if (this.e == null) {
            this.e = new File(this.c.getPath());
        }
        return this.e;
    }

    public boolean b(int i) {
        return (i & this.o) == 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ImageRequest)) {
            return false;
        }
        ImageRequest imageRequest = (ImageRequest) obj;
        if (this.g != imageRequest.g || this.p != imageRequest.p || this.q != imageRequest.q || !b.c.a.a0.d.g0(this.c, imageRequest.c) || !b.c.a.a0.d.g0(this.f2874b, imageRequest.f2874b) || !b.c.a.a0.d.g0(this.e, imageRequest.e) || !b.c.a.a0.d.g0(this.l, imageRequest.l) || !b.c.a.a0.d.g0(this.i, imageRequest.i) || !b.c.a.a0.d.g0(this.j, imageRequest.j) || !b.c.a.a0.d.g0(this.m, imageRequest.m) || !b.c.a.a0.d.g0(this.n, imageRequest.n) || !b.c.a.a0.d.g0(Integer.valueOf(this.o), Integer.valueOf(imageRequest.o)) || !b.c.a.a0.d.g0(this.r, imageRequest.r)) {
            return false;
        }
        CacheKey cacheKey = null;
        if (!b.c.a.a0.d.g0(null, null) || !b.c.a.a0.d.g0(this.k, imageRequest.k) || this.h != imageRequest.h) {
            return false;
        }
        b.f.j.q.b bVar = this.f2875s;
        CacheKey postprocessorCacheKey = bVar != null ? bVar.getPostprocessorCacheKey() : null;
        b.f.j.q.b bVar2 = imageRequest.f2875s;
        if (bVar2 != null) {
            cacheKey = bVar2.getPostprocessorCacheKey();
        }
        return b.c.a.a0.d.g0(postprocessorCacheKey, cacheKey) && this.u == imageRequest.u;
    }

    public int hashCode() {
        b.f.j.q.b bVar = this.f2875s;
        return Arrays.hashCode(new Object[]{this.f2874b, this.c, Boolean.valueOf(this.g), this.l, this.m, this.n, Integer.valueOf(this.o), Boolean.valueOf(this.p), Boolean.valueOf(this.q), this.i, this.r, this.j, this.k, bVar != null ? bVar.getPostprocessorCacheKey() : null, null, Integer.valueOf(this.u), Boolean.valueOf(this.h)});
    }

    public String toString() {
        i h2 = b.c.a.a0.d.h2(this);
        h2.c(NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, this.c);
        h2.c("cacheChoice", this.f2874b);
        h2.c("decodeOptions", this.i);
        h2.c("postprocessor", this.f2875s);
        h2.c("priority", this.m);
        h2.c("resizeOptions", this.j);
        h2.c("rotationOptions", this.k);
        h2.c("bytesRange", this.l);
        h2.c("resizingAllowedOverride", null);
        h2.b("progressiveRenderingEnabled", this.f);
        h2.b("localThumbnailPreviewsEnabled", this.g);
        h2.b("loadThumbnailOnly", this.h);
        h2.c("lowestPermittedRequestLevel", this.n);
        h2.a("cachesDisabled", this.o);
        h2.b("isDiskCacheEnabled", this.p);
        h2.b("isMemoryCacheEnabled", this.q);
        h2.c("decodePrefetches", this.r);
        h2.a("delayMs", this.u);
        return h2.toString();
    }
}
