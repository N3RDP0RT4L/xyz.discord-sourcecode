package com.facebook.imagepipeline.cache;
/* loaded from: classes2.dex */
public class MemoryCacheParams {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f2867b;
    public final int c;
    public final int d;
    public final int e;
    public final long f;

    public MemoryCacheParams(int i, int i2, int i3, int i4, int i5, long j) {
        this.a = i;
        this.f2867b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = j;
    }
}
