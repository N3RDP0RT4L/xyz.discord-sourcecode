package com.facebook.imagepipeline.platform;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.MemoryFile;
import b.f.d.d.c;
import b.f.d.d.m;
import b.f.d.g.h;
import b.f.d.j.a;
import b.f.d.m.b;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Objects;
@c
/* loaded from: classes2.dex */
public class GingerbreadPurgeableDecoder extends DalvikPurgeableDecoder {
    public static Method c;
    public final b d = b.f.d.m.c.c();

    public static MemoryFile g(CloseableReference<PooledByteBuffer> closeableReference, int i, byte[] bArr) throws IOException {
        OutputStream outputStream;
        Throwable th;
        a aVar;
        h hVar = null;
        OutputStream outputStream2 = null;
        MemoryFile memoryFile = new MemoryFile(null, (bArr == null ? 0 : bArr.length) + i);
        memoryFile.allowPurging(false);
        try {
            h hVar2 = new h(closeableReference.u());
            try {
                aVar = new a(hVar2, i);
                try {
                    outputStream2 = memoryFile.getOutputStream();
                    Objects.requireNonNull(outputStream2);
                    byte[] bArr2 = new byte[4096];
                    while (true) {
                        int read = aVar.read(bArr2);
                        if (read == -1) {
                            break;
                        }
                        outputStream2.write(bArr2, 0, read);
                    }
                    if (bArr != null) {
                        memoryFile.writeBytes(bArr, 0, i, bArr.length);
                    }
                    closeableReference.close();
                    b.f.d.d.a.b(hVar2);
                    b.f.d.d.a.b(aVar);
                    b.f.d.d.a.a(outputStream2, true);
                    return memoryFile;
                } catch (Throwable th2) {
                    th = th2;
                    outputStream = outputStream2;
                    hVar = hVar2;
                    Class<CloseableReference> cls = CloseableReference.j;
                    if (closeableReference != null) {
                        closeableReference.close();
                    }
                    b.f.d.d.a.b(hVar);
                    b.f.d.d.a.b(aVar);
                    b.f.d.d.a.a(outputStream, true);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                outputStream = null;
                aVar = null;
            }
        } catch (Throwable th4) {
            th = th4;
            outputStream = null;
            aVar = null;
        }
    }

    @Override // com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder
    public Bitmap c(CloseableReference<PooledByteBuffer> closeableReference, BitmapFactory.Options options) {
        return h(closeableReference, closeableReference.u().size(), null, options);
    }

    @Override // com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder
    public Bitmap d(CloseableReference<PooledByteBuffer> closeableReference, int i, BitmapFactory.Options options) {
        return h(closeableReference, i, DalvikPurgeableDecoder.e(closeableReference, i) ? null : DalvikPurgeableDecoder.a, options);
    }

    /* JADX WARN: Removed duplicated region for block: B:22:0x003c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final android.graphics.Bitmap h(com.facebook.common.references.CloseableReference<com.facebook.common.memory.PooledByteBuffer> r2, int r3, byte[] r4, android.graphics.BitmapFactory.Options r5) {
        /*
            r1 = this;
            r0 = 0
            android.os.MemoryFile r2 = g(r2, r3, r4)     // Catch: java.lang.Throwable -> L2a java.io.IOException -> L2c
            java.io.FileDescriptor r3 = r1.j(r2)     // Catch: java.lang.Throwable -> L24 java.io.IOException -> L27
            b.f.d.m.b r4 = r1.d     // Catch: java.lang.Throwable -> L24 java.io.IOException -> L27
            if (r4 == 0) goto L1c
            android.graphics.Bitmap r3 = r4.a(r3, r0, r5)     // Catch: java.lang.Throwable -> L24 java.io.IOException -> L27
            java.lang.String r4 = "BitmapFactory returned null"
            b.c.a.a0.d.y(r3, r4)     // Catch: java.lang.Throwable -> L24 java.io.IOException -> L27
            android.graphics.Bitmap r3 = (android.graphics.Bitmap) r3     // Catch: java.lang.Throwable -> L24 java.io.IOException -> L27
            r2.close()
            return r3
        L1c:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException     // Catch: java.lang.Throwable -> L24 java.io.IOException -> L27
            java.lang.String r4 = "WebpBitmapFactory is null"
            r3.<init>(r4)     // Catch: java.lang.Throwable -> L24 java.io.IOException -> L27
            throw r3     // Catch: java.lang.Throwable -> L24 java.io.IOException -> L27
        L24:
            r3 = move-exception
            r0 = r2
            goto L3a
        L27:
            r3 = move-exception
            r0 = r2
            goto L2d
        L2a:
            r3 = move-exception
            goto L3a
        L2c:
            r3 = move-exception
        L2d:
            b.f.d.d.m.a(r3)     // Catch: java.lang.Throwable -> L38
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch: java.lang.Throwable -> L38
            r2.<init>(r3)     // Catch: java.lang.Throwable -> L38
            throw r2     // Catch: java.lang.Throwable -> L38
        L36:
            r3 = r2
            goto L3a
        L38:
            r2 = move-exception
            goto L36
        L3a:
            if (r0 == 0) goto L3f
            r0.close()
        L3f:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.platform.GingerbreadPurgeableDecoder.h(com.facebook.common.references.CloseableReference, int, byte[], android.graphics.BitmapFactory$Options):android.graphics.Bitmap");
    }

    public final synchronized Method i() {
        if (c == null) {
            try {
                c = MemoryFile.class.getDeclaredMethod("getFileDescriptor", new Class[0]);
            } catch (Exception e) {
                m.a(e);
                throw new RuntimeException(e);
            }
        }
        return c;
    }

    public final FileDescriptor j(MemoryFile memoryFile) {
        try {
            Object invoke = i().invoke(memoryFile, new Object[0]);
            Objects.requireNonNull(invoke);
            return (FileDescriptor) invoke;
        } catch (Exception e) {
            m.a(e);
            throw new RuntimeException(e);
        }
    }
}
