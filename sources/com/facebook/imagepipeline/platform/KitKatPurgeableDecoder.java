package com.facebook.imagepipeline.platform;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.exifinterface.media.ExifInterface;
import b.c.a.a0.d;
import b.f.d.d.c;
import b.f.j.l.n;
import com.facebook.common.memory.PooledByteBuffer;
import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder;
@c
@TargetApi(19)
/* loaded from: classes2.dex */
public class KitKatPurgeableDecoder extends DalvikPurgeableDecoder {
    public final n c;

    @c
    public KitKatPurgeableDecoder(n nVar) {
        this.c = nVar;
    }

    @Override // com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder
    public Bitmap c(CloseableReference<PooledByteBuffer> closeableReference, BitmapFactory.Options options) {
        PooledByteBuffer u = closeableReference.u();
        int size = u.size();
        CloseableReference<byte[]> a = this.c.a(size);
        try {
            byte[] u2 = a.u();
            u.i(0, u2, 0, size);
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(u2, 0, size, options);
            d.y(decodeByteArray, "BitmapFactory returned null");
            a.close();
            return decodeByteArray;
        } catch (Throwable th) {
            if (a != null) {
                a.close();
            }
            throw th;
        }
    }

    @Override // com.facebook.imagepipeline.nativecode.DalvikPurgeableDecoder
    public Bitmap d(CloseableReference<PooledByteBuffer> closeableReference, int i, BitmapFactory.Options options) {
        byte[] bArr = DalvikPurgeableDecoder.e(closeableReference, i) ? null : DalvikPurgeableDecoder.a;
        PooledByteBuffer u = closeableReference.u();
        d.i(Boolean.valueOf(i <= u.size()));
        int i2 = i + 2;
        CloseableReference<byte[]> a = this.c.a(i2);
        try {
            byte[] u2 = a.u();
            u.i(0, u2, 0, i);
            if (bArr != null) {
                u2[i] = -1;
                u2[i + 1] = ExifInterface.MARKER_EOI;
                i = i2;
            }
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(u2, 0, i, options);
            d.y(decodeByteArray, "BitmapFactory returned null");
            a.close();
            return decodeByteArray;
        } catch (Throwable th) {
            if (a != null) {
                a.close();
            }
            throw th;
        }
    }
}
