package com.facebook.imagepipeline.memory;

import android.annotation.SuppressLint;
import android.util.SparseArray;
import android.util.SparseIntArray;
import androidx.annotation.VisibleForTesting;
import b.c.a.a0.d;
import b.f.d.d.m;
import b.f.d.g.c;
import b.f.d.g.e;
import b.f.j.l.f;
import b.f.j.l.y;
import b.f.j.l.z;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Objects;
import java.util.Set;
/* loaded from: classes2.dex */
public abstract class BasePool<V> implements e<V> {
    public final Class<?> a = getClass();

    /* renamed from: b  reason: collision with root package name */
    public final c f2868b;
    public final y c;
    @VisibleForTesting
    public final SparseArray<f<V>> d;
    @VisibleForTesting
    public final Set<V> e;
    public boolean f;
    @VisibleForTesting
    public final a g;
    @VisibleForTesting
    public final a h;
    public final z i;
    public boolean j;

    /* loaded from: classes2.dex */
    public static class InvalidSizeException extends RuntimeException {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public InvalidSizeException(java.lang.Object r2) {
            /*
                r1 = this;
                java.lang.String r0 = "Invalid size: "
                java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
                java.lang.String r2 = r2.toString()
                r0.append(r2)
                java.lang.String r2 = r0.toString()
                r1.<init>(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.memory.BasePool.InvalidSizeException.<init>(java.lang.Object):void");
        }
    }

    /* loaded from: classes2.dex */
    public static class InvalidValueException extends RuntimeException {
    }

    /* loaded from: classes2.dex */
    public static class PoolSizeViolationException extends RuntimeException {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public PoolSizeViolationException(int r4, int r5, int r6, int r7) {
            /*
                r3 = this;
                java.lang.String r0 = "Pool hard cap violation? Hard cap = "
                java.lang.String r1 = " Used size = "
                java.lang.String r2 = " Free size = "
                java.lang.StringBuilder r4 = b.d.b.a.a.U(r0, r4, r1, r5, r2)
                r4.append(r6)
                java.lang.String r5 = " Request size = "
                r4.append(r5)
                r4.append(r7)
                java.lang.String r4 = r4.toString()
                r3.<init>(r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.memory.BasePool.PoolSizeViolationException.<init>(int, int, int, int):void");
        }
    }

    /* loaded from: classes2.dex */
    public static class SizeTooLargeException extends InvalidSizeException {
    }

    @VisibleForTesting
    /* loaded from: classes2.dex */
    public static class a {
        public int a;

        /* renamed from: b  reason: collision with root package name */
        public int f2869b;

        public void a(int i) {
            int i2;
            int i3 = this.f2869b;
            if (i3 < i || (i2 = this.a) <= 0) {
                b.f.d.e.a.p("com.facebook.imagepipeline.memory.BasePool.Counter", "Unexpected decrement of %d. Current numBytes = %d, count = %d", Integer.valueOf(i), Integer.valueOf(this.f2869b), Integer.valueOf(this.a));
                return;
            }
            this.a = i2 - 1;
            this.f2869b = i3 - i;
        }

        public void b(int i) {
            this.a++;
            this.f2869b += i;
        }
    }

    public BasePool(c cVar, y yVar, z zVar) {
        Objects.requireNonNull(cVar);
        this.f2868b = cVar;
        Objects.requireNonNull(yVar);
        this.c = yVar;
        Objects.requireNonNull(zVar);
        this.i = zVar;
        SparseArray<f<V>> sparseArray = new SparseArray<>();
        this.d = sparseArray;
        SparseIntArray sparseIntArray = new SparseIntArray(0);
        synchronized (this) {
            sparseArray.clear();
            SparseIntArray sparseIntArray2 = yVar.c;
            if (sparseIntArray2 != null) {
                for (int i = 0; i < sparseIntArray2.size(); i++) {
                    int keyAt = sparseIntArray2.keyAt(i);
                    int valueAt = sparseIntArray2.valueAt(i);
                    int i2 = sparseIntArray.get(keyAt, 0);
                    SparseArray<f<V>> sparseArray2 = this.d;
                    int k = k(keyAt);
                    Objects.requireNonNull(this.c);
                    sparseArray2.put(keyAt, new f<>(k, valueAt, i2, false));
                }
                this.f = false;
            } else {
                this.f = true;
            }
        }
        this.e = Collections.newSetFromMap(new IdentityHashMap());
        this.h = new a();
        this.g = new a();
    }

    public abstract V e(int i);

    @VisibleForTesting
    public synchronized boolean f(int i) {
        if (this.j) {
            return true;
        }
        y yVar = this.c;
        int i2 = yVar.a;
        int i3 = this.g.f2869b;
        if (i > i2 - i3) {
            this.i.f();
            return false;
        }
        int i4 = yVar.f598b;
        if (i > i4 - (i3 + this.h.f2869b)) {
            r(i4 - i);
        }
        if (i <= i2 - (this.g.f2869b + this.h.f2869b)) {
            return true;
        }
        this.i.f();
        return false;
    }

    @VisibleForTesting
    public abstract void g(V v);

    @Override // b.f.d.g.e
    public V get(int i) {
        boolean z2;
        V l;
        synchronized (this) {
            if (n() && this.h.f2869b != 0) {
                z2 = false;
                d.B(z2);
            }
            z2 = true;
            d.B(z2);
        }
        int i2 = i(i);
        synchronized (this) {
            f<V> h = h(i2);
            if (h == null || (l = l(h)) == null) {
                int k = k(i2);
                if (f(k)) {
                    this.g.b(k);
                    if (h != null) {
                        h.e++;
                    }
                    V v = null;
                    try {
                        v = e(i2);
                    } catch (Throwable th) {
                        synchronized (this) {
                            this.g.a(k);
                            f<V> h2 = h(i2);
                            if (h2 != null) {
                                h2.b();
                            }
                            m.a(th);
                        }
                    }
                    synchronized (this) {
                        d.B(this.e.add(v));
                        synchronized (this) {
                            if (n()) {
                                r(this.c.f598b);
                            }
                        }
                        return v;
                    }
                    this.i.a(k);
                    p();
                    if (b.f.d.e.a.h(2)) {
                        System.identityHashCode(v);
                    }
                    return v;
                }
                throw new PoolSizeViolationException(this.c.a, this.g.f2869b, this.h.f2869b, k);
            }
            d.B(this.e.add(l));
            int k2 = k(j(l));
            this.g.b(k2);
            this.h.a(k2);
            this.i.b(k2);
            p();
            if (b.f.d.e.a.h(2)) {
                System.identityHashCode(l);
            }
            return l;
        }
    }

    @VisibleForTesting
    public synchronized f<V> h(int i) {
        f<V> fVar = this.d.get(i);
        if (fVar == null && this.f) {
            b.f.d.e.a.h(2);
            f<V> q = q(i);
            this.d.put(i, q);
            return q;
        }
        return fVar;
    }

    public abstract int i(int i);

    public abstract int j(V v);

    public abstract int k(int i);

    public synchronized V l(f<V> fVar) {
        V c;
        c = fVar.c();
        if (c != null) {
            fVar.e++;
        }
        return c;
    }

    public void m() {
        this.f2868b.a(this);
        this.i.c(this);
    }

    @VisibleForTesting
    public synchronized boolean n() {
        boolean z2;
        z2 = this.g.f2869b + this.h.f2869b > this.c.f598b;
        if (z2) {
            this.i.d();
        }
        return z2;
    }

    public boolean o(V v) {
        return true;
    }

    @SuppressLint({"InvalidAccessToGuardedField"})
    public final void p() {
        if (b.f.d.e.a.h(2)) {
            a aVar = this.g;
            int i = aVar.a;
            int i2 = aVar.f2869b;
            a aVar2 = this.h;
            int i3 = aVar2.a;
            int i4 = aVar2.f2869b;
        }
    }

    public f<V> q(int i) {
        int k = k(i);
        Objects.requireNonNull(this.c);
        return new f<>(k, Integer.MAX_VALUE, 0, false);
    }

    @VisibleForTesting
    public synchronized void r(int i) {
        int i2 = this.g.f2869b;
        int i3 = this.h.f2869b;
        int min = Math.min((i2 + i3) - i, i3);
        if (min > 0) {
            if (b.f.d.e.a.h(2)) {
                b.f.d.e.a.j(this.a, "trimToSize: TargetSize = %d; Initial Size = %d; Bytes to free = %d", Integer.valueOf(i), Integer.valueOf(this.g.f2869b + this.h.f2869b), Integer.valueOf(min));
            }
            p();
            for (int i4 = 0; i4 < this.d.size() && min > 0; i4++) {
                f<V> valueAt = this.d.valueAt(i4);
                Objects.requireNonNull(valueAt);
                f<V> fVar = valueAt;
                while (min > 0) {
                    V c = fVar.c();
                    if (c == null) {
                        break;
                    }
                    g(c);
                    int i5 = fVar.a;
                    min -= i5;
                    this.h.a(i5);
                }
            }
            p();
            if (b.f.d.e.a.h(2)) {
                int i6 = this.g.f2869b;
                int i7 = this.h.f2869b;
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:25:0x0081, code lost:
        r2.b();
     */
    @Override // b.f.d.g.e, b.f.d.h.f
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void release(V r9) {
        /*
            r8 = this;
            java.util.Objects.requireNonNull(r9)
            int r0 = r8.j(r9)
            int r1 = r8.k(r0)
            monitor-enter(r8)
            monitor-enter(r8)     // Catch: java.lang.Throwable -> L9f
            android.util.SparseArray<b.f.j.l.f<V>> r2 = r8.d     // Catch: java.lang.Throwable -> La1
            java.lang.Object r2 = r2.get(r0)     // Catch: java.lang.Throwable -> La1
            b.f.j.l.f r2 = (b.f.j.l.f) r2     // Catch: java.lang.Throwable -> La1
            monitor-exit(r8)     // Catch: java.lang.Throwable -> L9f
            java.util.Set<V> r3 = r8.e     // Catch: java.lang.Throwable -> L9f
            boolean r3 = r3.remove(r9)     // Catch: java.lang.Throwable -> L9f
            r4 = 1
            r5 = 0
            r6 = 2
            if (r3 != 0) goto L43
            java.lang.Class<?> r2 = r8.a     // Catch: java.lang.Throwable -> L9f
            java.lang.String r3 = "release (free, value unrecognized) (object, size) = (%x, %s)"
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch: java.lang.Throwable -> L9f
            int r7 = java.lang.System.identityHashCode(r9)     // Catch: java.lang.Throwable -> L9f
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch: java.lang.Throwable -> L9f
            r6[r5] = r7     // Catch: java.lang.Throwable -> L9f
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch: java.lang.Throwable -> L9f
            r6[r4] = r0     // Catch: java.lang.Throwable -> L9f
            b.f.d.e.a.c(r2, r3, r6)     // Catch: java.lang.Throwable -> L9f
            r8.g(r9)     // Catch: java.lang.Throwable -> L9f
            b.f.j.l.z r9 = r8.i     // Catch: java.lang.Throwable -> L9f
            r9.e(r1)     // Catch: java.lang.Throwable -> L9f
            goto L9a
        L43:
            if (r2 == 0) goto L7f
            int r0 = r2.e     // Catch: java.lang.Throwable -> L9f
            java.util.Queue r3 = r2.c     // Catch: java.lang.Throwable -> L9f
            int r3 = r3.size()     // Catch: java.lang.Throwable -> L9f
            int r0 = r0 + r3
            int r3 = r2.f590b     // Catch: java.lang.Throwable -> L9f
            if (r0 <= r3) goto L53
            goto L54
        L53:
            r4 = 0
        L54:
            if (r4 != 0) goto L7f
            boolean r0 = r8.n()     // Catch: java.lang.Throwable -> L9f
            if (r0 != 0) goto L7f
            boolean r0 = r8.o(r9)     // Catch: java.lang.Throwable -> L9f
            if (r0 != 0) goto L63
            goto L7f
        L63:
            r2.d(r9)     // Catch: java.lang.Throwable -> L9f
            com.facebook.imagepipeline.memory.BasePool$a r0 = r8.h     // Catch: java.lang.Throwable -> L9f
            r0.b(r1)     // Catch: java.lang.Throwable -> L9f
            com.facebook.imagepipeline.memory.BasePool$a r0 = r8.g     // Catch: java.lang.Throwable -> L9f
            r0.a(r1)     // Catch: java.lang.Throwable -> L9f
            b.f.j.l.z r0 = r8.i     // Catch: java.lang.Throwable -> L9f
            r0.g(r1)     // Catch: java.lang.Throwable -> L9f
            boolean r0 = b.f.d.e.a.h(r6)     // Catch: java.lang.Throwable -> L9f
            if (r0 == 0) goto L9a
            java.lang.System.identityHashCode(r9)     // Catch: java.lang.Throwable -> L9f
            goto L9a
        L7f:
            if (r2 == 0) goto L84
            r2.b()     // Catch: java.lang.Throwable -> L9f
        L84:
            boolean r0 = b.f.d.e.a.h(r6)     // Catch: java.lang.Throwable -> L9f
            if (r0 == 0) goto L8d
            java.lang.System.identityHashCode(r9)     // Catch: java.lang.Throwable -> L9f
        L8d:
            r8.g(r9)     // Catch: java.lang.Throwable -> L9f
            com.facebook.imagepipeline.memory.BasePool$a r9 = r8.g     // Catch: java.lang.Throwable -> L9f
            r9.a(r1)     // Catch: java.lang.Throwable -> L9f
            b.f.j.l.z r9 = r8.i     // Catch: java.lang.Throwable -> L9f
            r9.e(r1)     // Catch: java.lang.Throwable -> L9f
        L9a:
            r8.p()     // Catch: java.lang.Throwable -> L9f
            monitor-exit(r8)     // Catch: java.lang.Throwable -> L9f
            return
        L9f:
            r9 = move-exception
            goto La4
        La1:
            r9 = move-exception
            monitor-exit(r8)     // Catch: java.lang.Throwable -> L9f
            throw r9     // Catch: java.lang.Throwable -> L9f
        La4:
            monitor-exit(r8)     // Catch: java.lang.Throwable -> L9f
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.imagepipeline.memory.BasePool.release(java.lang.Object):void");
    }
}
