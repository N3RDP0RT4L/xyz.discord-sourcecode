package com.facebook.drawee.controller;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import b.f.d.d.f;
import b.f.d.d.i;
import b.f.g.b.a;
import b.f.g.b.c;
import b.f.g.b.d;
import b.f.g.e.p;
import b.f.g.g.a;
import b.f.h.b.a.b;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.interfaces.DraweeHierarchy;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
/* loaded from: classes2.dex */
public abstract class AbstractDraweeController<T, INFO> implements DraweeController, a.AbstractC0067a, a.AbstractC0068a {
    public static final Map<String, Object> a = f.of("component_tag", "drawee");

    /* renamed from: b  reason: collision with root package name */
    public static final Map<String, Object> f2857b = f.of("origin", "memory_bitmap", "origin_sub", "shortcut");
    public static final Class<?> c = AbstractDraweeController.class;
    public final c d;
    public final b.f.g.b.a e;
    public final Executor f;
    public d g;
    public b.f.g.g.a h;
    public ControllerListener<INFO> i;
    public b.f.h.b.a.c<INFO> j;
    public b.f.g.h.a k;
    public Drawable l;
    public String m;
    public Object n;
    public boolean o;
    public boolean p;
    public boolean q;
    public boolean r;

    /* renamed from: s  reason: collision with root package name */
    public String f2858s;
    public DataSource<T> t;
    public T u;
    public boolean v;
    public Drawable w;

    /* loaded from: classes2.dex */
    public class a extends b.f.e.d<T> {
        public final /* synthetic */ String a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ boolean f2859b;

        public a(String str, boolean z2) {
            this.a = str;
            this.f2859b = z2;
        }

        @Override // b.f.e.d
        public void onFailureImpl(DataSource<T> dataSource) {
            AbstractDraweeController abstractDraweeController = AbstractDraweeController.this;
            String str = this.a;
            Throwable d = dataSource.d();
            Map<String, Object> map = AbstractDraweeController.a;
            abstractDraweeController.u(str, dataSource, d, true);
        }

        @Override // b.f.e.d
        public void onNewResultImpl(DataSource<T> dataSource) {
            boolean c = dataSource.c();
            boolean e = dataSource.e();
            float progress = dataSource.getProgress();
            T result = dataSource.getResult();
            if (result != null) {
                AbstractDraweeController abstractDraweeController = AbstractDraweeController.this;
                String str = this.a;
                boolean z2 = this.f2859b;
                Map<String, Object> map = AbstractDraweeController.a;
                abstractDraweeController.w(str, dataSource, result, progress, c, z2, e);
            } else if (c) {
                AbstractDraweeController abstractDraweeController2 = AbstractDraweeController.this;
                String str2 = this.a;
                NullPointerException nullPointerException = new NullPointerException();
                Map<String, Object> map2 = AbstractDraweeController.a;
                abstractDraweeController2.u(str2, dataSource, nullPointerException, true);
            }
        }

        @Override // b.f.e.d, b.f.e.f
        public void onProgressUpdate(DataSource<T> dataSource) {
            b.f.e.c cVar = (b.f.e.c) dataSource;
            boolean c = cVar.c();
            float progress = cVar.getProgress();
            AbstractDraweeController abstractDraweeController = AbstractDraweeController.this;
            String str = this.a;
            Map<String, Object> map = AbstractDraweeController.a;
            if (!abstractDraweeController.o(str, cVar)) {
                abstractDraweeController.p("ignore_old_datasource @ onProgress", null);
                cVar.close();
            } else if (!c) {
                abstractDraweeController.k.d(progress, false);
            }
        }
    }

    /* loaded from: classes2.dex */
    public static class b<INFO> extends b.f.g.c.d<INFO> {
    }

    public AbstractDraweeController(b.f.g.b.a aVar, Executor executor, String str, Object obj) {
        this.d = c.f490b ? new c() : c.a;
        this.j = new b.f.h.b.a.c<>();
        this.v = true;
        this.e = aVar;
        this.f = executor;
        n(null, null);
    }

    public void A(ControllerListener<? super INFO> controllerListener) {
        Objects.requireNonNull(controllerListener);
        ControllerListener<INFO> controllerListener2 = this.i;
        if (controllerListener2 instanceof b) {
            b bVar = (b) controllerListener2;
            synchronized (bVar) {
                int indexOf = bVar.a.indexOf(controllerListener);
                if (indexOf != -1) {
                    bVar.a.set(indexOf, null);
                }
            }
        } else if (controllerListener2 == controllerListener) {
            this.i = null;
        }
    }

    public void B(DataSource<T> dataSource, INFO info) {
        i().onSubmit(this.m, this.n);
        this.j.a(this.m, this.n, r(dataSource, info, m()));
    }

    public final void C(String str, T t, DataSource<T> dataSource) {
        INFO l = l(t);
        i().onFinalImageSet(str, l, c());
        this.j.d(str, l, r(dataSource, l, null));
    }

    public final boolean D() {
        d dVar;
        if (this.q && (dVar = this.g) != null) {
            if (dVar.a && dVar.c < dVar.f495b) {
                return true;
            }
        }
        return false;
    }

    public void E() {
        b.f.j.r.b.b();
        T h = h();
        if (h != null) {
            b.f.j.r.b.b();
            this.t = null;
            this.p = true;
            this.q = false;
            this.d.a(c.a.ON_SUBMIT_CACHE_HIT);
            B(this.t, l(h));
            v(this.m, h);
            w(this.m, this.t, h, 1.0f, true, true, true);
            b.f.j.r.b.b();
            b.f.j.r.b.b();
            return;
        }
        this.d.a(c.a.ON_DATASOURCE_SUBMIT);
        this.k.d(0.0f, true);
        this.p = true;
        this.q = false;
        DataSource<T> j = j();
        this.t = j;
        B(j, null);
        if (b.f.d.e.a.h(2)) {
            b.f.d.e.a.j(c, "controller %x %s: submitRequest: dataSource: %x", Integer.valueOf(System.identityHashCode(this)), this.m, Integer.valueOf(System.identityHashCode(this.t)));
        }
        this.t.f(new a(this.m, this.t.b()), this.f);
        b.f.j.r.b.b();
    }

    @Override // com.facebook.drawee.interfaces.DraweeController
    public void a() {
        b.f.j.r.b.b();
        if (b.f.d.e.a.h(2)) {
            System.identityHashCode(this);
        }
        this.d.a(c.a.ON_DETACH_CONTROLLER);
        boolean z2 = false;
        this.o = false;
        b.f.g.b.b bVar = (b.f.g.b.b) this.e;
        Objects.requireNonNull(bVar);
        if (!(Looper.getMainLooper().getThread() == Thread.currentThread())) {
            release();
        } else {
            synchronized (bVar.f489b) {
                if (!bVar.d.contains(this)) {
                    bVar.d.add(this);
                    if (bVar.d.size() == 1) {
                        z2 = true;
                    }
                    if (z2) {
                        bVar.c.post(bVar.f);
                    }
                }
            }
        }
        b.f.j.r.b.b();
    }

    @Override // com.facebook.drawee.interfaces.DraweeController
    public DraweeHierarchy b() {
        return this.k;
    }

    @Override // com.facebook.drawee.interfaces.DraweeController
    public Animatable c() {
        Drawable drawable = this.w;
        if (drawable instanceof Animatable) {
            return (Animatable) drawable;
        }
        return null;
    }

    @Override // com.facebook.drawee.interfaces.DraweeController
    public void d() {
        b.f.j.r.b.b();
        if (b.f.d.e.a.h(2)) {
            b.f.d.e.a.j(c, "controller %x %s: onAttach: %s", Integer.valueOf(System.identityHashCode(this)), this.m, this.p ? "request already submitted" : "request needs submit");
        }
        this.d.a(c.a.ON_ATTACH_CONTROLLER);
        Objects.requireNonNull(this.k);
        this.e.a(this);
        this.o = true;
        if (!this.p) {
            E();
        }
        b.f.j.r.b.b();
    }

    @Override // com.facebook.drawee.interfaces.DraweeController
    public void e(DraweeHierarchy draweeHierarchy) {
        if (b.f.d.e.a.h(2)) {
            b.f.d.e.a.j(c, "controller %x %s: setHierarchy: %s", Integer.valueOf(System.identityHashCode(this)), this.m, draweeHierarchy);
        }
        this.d.a(draweeHierarchy != null ? c.a.ON_SET_HIERARCHY : c.a.ON_CLEAR_HIERARCHY);
        if (this.p) {
            this.e.a(this);
            release();
        }
        b.f.g.h.a aVar = this.k;
        if (aVar != null) {
            aVar.a(null);
            this.k = null;
        }
        if (draweeHierarchy != null) {
            b.c.a.a0.d.i(Boolean.valueOf(draweeHierarchy instanceof b.f.g.h.a));
            b.f.g.h.a aVar2 = (b.f.g.h.a) draweeHierarchy;
            this.k = aVar2;
            aVar2.a(this.l);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public void f(ControllerListener<? super INFO> controllerListener) {
        Objects.requireNonNull(controllerListener);
        ControllerListener<INFO> controllerListener2 = this.i;
        if (controllerListener2 instanceof b) {
            ((b) controllerListener2).a(controllerListener);
        } else if (controllerListener2 != null) {
            b.f.j.r.b.b();
            b bVar = new b();
            bVar.a(controllerListener2);
            bVar.a(controllerListener);
            b.f.j.r.b.b();
            this.i = bVar;
        } else {
            this.i = controllerListener;
        }
    }

    public abstract Drawable g(T t);

    public T h() {
        return null;
    }

    public ControllerListener<INFO> i() {
        ControllerListener<INFO> controllerListener = this.i;
        return controllerListener == null ? b.f.g.c.c.getNoOpListener() : controllerListener;
    }

    public abstract DataSource<T> j();

    public int k(T t) {
        return System.identityHashCode(t);
    }

    public abstract INFO l(T t);

    public Uri m() {
        return null;
    }

    public final synchronized void n(String str, Object obj) {
        b.f.g.b.a aVar;
        b.f.j.r.b.b();
        this.d.a(c.a.ON_INIT_CONTROLLER);
        if (!this.v && (aVar = this.e) != null) {
            aVar.a(this);
        }
        this.o = false;
        y();
        this.r = false;
        d dVar = this.g;
        if (dVar != null) {
            dVar.a = false;
            dVar.f495b = 4;
            dVar.c = 0;
        }
        b.f.g.g.a aVar2 = this.h;
        if (aVar2 != null) {
            aVar2.a = null;
            aVar2.c = false;
            aVar2.d = false;
            aVar2.a = this;
        }
        ControllerListener<INFO> controllerListener = this.i;
        if (controllerListener instanceof b) {
            b bVar = (b) controllerListener;
            synchronized (bVar) {
                bVar.a.clear();
            }
        } else {
            this.i = null;
        }
        b.f.g.h.a aVar3 = this.k;
        if (aVar3 != null) {
            aVar3.reset();
            this.k.a(null);
            this.k = null;
        }
        this.l = null;
        if (b.f.d.e.a.h(2)) {
            b.f.d.e.a.j(c, "controller %x %s -> %s: initialize", Integer.valueOf(System.identityHashCode(this)), this.m, str);
        }
        this.m = str;
        this.n = obj;
        b.f.j.r.b.b();
    }

    public final boolean o(String str, DataSource<T> dataSource) {
        if (dataSource == null && this.t == null) {
            return true;
        }
        return str.equals(this.m) && dataSource == this.t && this.p;
    }

    @Override // com.facebook.drawee.interfaces.DraweeController
    public boolean onTouchEvent(MotionEvent motionEvent) {
        a.AbstractC0068a aVar;
        boolean h = b.f.d.e.a.h(2);
        if (h) {
            b.f.d.e.a.j(c, "controller %x %s: onTouchEvent %s", Integer.valueOf(System.identityHashCode(this)), this.m, motionEvent);
        }
        b.f.g.g.a aVar2 = this.h;
        if (aVar2 == null) {
            return false;
        }
        if (!aVar2.c && !D()) {
            return false;
        }
        b.f.g.g.a aVar3 = this.h;
        Objects.requireNonNull(aVar3);
        int action = motionEvent.getAction();
        if (action == 0) {
            aVar3.c = true;
            aVar3.d = true;
            aVar3.e = motionEvent.getEventTime();
            aVar3.f = motionEvent.getX();
            aVar3.g = motionEvent.getY();
        } else if (action == 1) {
            aVar3.c = false;
            if (Math.abs(motionEvent.getX() - aVar3.f) > aVar3.f520b || Math.abs(motionEvent.getY() - aVar3.g) > aVar3.f520b) {
                aVar3.d = false;
            }
            if (aVar3.d && motionEvent.getEventTime() - aVar3.e <= ViewConfiguration.getLongPressTimeout() && (aVar = aVar3.a) != null) {
                AbstractDraweeController abstractDraweeController = (AbstractDraweeController) aVar;
                if (h) {
                    System.identityHashCode(abstractDraweeController);
                }
                if (abstractDraweeController.D()) {
                    abstractDraweeController.g.c++;
                    abstractDraweeController.k.reset();
                    abstractDraweeController.E();
                }
            }
            aVar3.d = false;
        } else if (action != 2) {
            if (action == 3) {
                aVar3.c = false;
                aVar3.d = false;
            }
        } else if (Math.abs(motionEvent.getX() - aVar3.f) > aVar3.f520b || Math.abs(motionEvent.getY() - aVar3.g) > aVar3.f520b) {
            aVar3.d = false;
        }
        return true;
    }

    public final void p(String str, Throwable th) {
        if (b.f.d.e.a.h(2)) {
            System.identityHashCode(this);
        }
    }

    public final void q(String str, T t) {
        if (b.f.d.e.a.h(2)) {
            System.identityHashCode(this);
            if (t != null) {
                t.getClass().getSimpleName();
            }
            k(t);
        }
    }

    public final b.a r(DataSource<T> dataSource, INFO info, Uri uri) {
        return s(dataSource == null ? null : dataSource.a(), t(info), uri);
    }

    @Override // b.f.g.b.a.AbstractC0067a
    public void release() {
        this.d.a(c.a.ON_RELEASE_CONTROLLER);
        d dVar = this.g;
        if (dVar != null) {
            dVar.c = 0;
        }
        b.f.g.g.a aVar = this.h;
        if (aVar != null) {
            aVar.c = false;
            aVar.d = false;
        }
        b.f.g.h.a aVar2 = this.k;
        if (aVar2 != null) {
            aVar2.reset();
        }
        y();
    }

    public final b.a s(Map<String, Object> map, Map<String, Object> map2, Uri uri) {
        b.f.g.h.a aVar = this.k;
        Rect rect = null;
        if (aVar instanceof GenericDraweeHierarchy) {
            GenericDraweeHierarchy genericDraweeHierarchy = (GenericDraweeHierarchy) aVar;
            String.valueOf(!(genericDraweeHierarchy.k(2) instanceof p) ? null : genericDraweeHierarchy.l(2).n);
            if (genericDraweeHierarchy.k(2) instanceof p) {
                PointF pointF = genericDraweeHierarchy.l(2).p;
            }
        }
        Map<String, Object> map3 = a;
        Map<String, Object> map4 = f2857b;
        b.f.g.h.a aVar2 = this.k;
        if (aVar2 != null) {
            rect = aVar2.getBounds();
        }
        Object obj = this.n;
        b.a aVar3 = new b.a();
        if (rect != null) {
            rect.width();
            rect.height();
        }
        aVar3.e = obj;
        aVar3.c = map;
        aVar3.d = map2;
        aVar3.f534b = map4;
        aVar3.a = map3;
        return aVar3;
    }

    public abstract Map<String, Object> t(INFO info);

    public String toString() {
        i h2 = b.c.a.a0.d.h2(this);
        h2.b("isAttached", this.o);
        h2.b("isRequestSubmitted", this.p);
        h2.b("hasFetchFailed", this.q);
        h2.a("fetchedImage", k(this.u));
        h2.c("events", this.d.toString());
        return h2.toString();
    }

    public final void u(String str, DataSource<T> dataSource, Throwable th, boolean z2) {
        Drawable drawable;
        b.f.j.r.b.b();
        if (!o(str, dataSource)) {
            p("ignore_old_datasource @ onFailure", th);
            dataSource.close();
            b.f.j.r.b.b();
            return;
        }
        this.d.a(z2 ? c.a.ON_DATASOURCE_FAILURE : c.a.ON_DATASOURCE_FAILURE_INT);
        if (z2) {
            p("final_failed @ onFailure", th);
            this.t = null;
            this.q = true;
            b.f.g.h.a aVar = this.k;
            if (aVar != null) {
                if (this.r && (drawable = this.w) != null) {
                    aVar.f(drawable, 1.0f, true);
                } else if (D()) {
                    aVar.b(th);
                } else {
                    aVar.c(th);
                }
            }
            b.a r = r(dataSource, null, null);
            i().onFailure(this.m, th);
            this.j.b(this.m, th, r);
        } else {
            p("intermediate_failed @ onFailure", th);
            i().onIntermediateImageFailed(this.m, th);
            Objects.requireNonNull(this.j);
        }
        b.f.j.r.b.b();
    }

    public void v(String str, T t) {
    }

    public final void w(String str, DataSource<T> dataSource, T t, float f, boolean z2, boolean z3, boolean z4) {
        try {
            b.f.j.r.b.b();
            if (!o(str, dataSource)) {
                q("ignore_old_datasource @ onNewResult", t);
                z(t);
                dataSource.close();
                return;
            }
            this.d.a(z2 ? c.a.ON_DATASOURCE_RESULT : c.a.ON_DATASOURCE_RESULT_INT);
            Drawable g = g(t);
            T t2 = this.u;
            Drawable drawable = this.w;
            this.u = t;
            this.w = g;
            if (z2) {
                q("set_final_result @ onNewResult", t);
                this.t = null;
                this.k.f(g, 1.0f, z3);
                C(str, t, dataSource);
            } else if (z4) {
                q("set_temporary_result @ onNewResult", t);
                this.k.f(g, 1.0f, z3);
                C(str, t, dataSource);
            } else {
                q("set_intermediate_result @ onNewResult", t);
                this.k.f(g, f, z3);
                i().onIntermediateImageSet(str, l(t));
                Objects.requireNonNull(this.j);
            }
            if (!(drawable == null || drawable == g)) {
                x(drawable);
            }
            if (!(t2 == null || t2 == t)) {
                q("release_previous_result @ onNewResult", t2);
                z(t2);
            }
        } catch (Exception e) {
            q("drawable_failed @ onNewResult", t);
            z(t);
            u(str, dataSource, e, z2);
        } finally {
            b.f.j.r.b.b();
        }
    }

    public abstract void x(Drawable drawable);

    public final void y() {
        Map<String, Object> map;
        Map<String, Object> map2;
        boolean z2 = this.p;
        this.p = false;
        this.q = false;
        DataSource<T> dataSource = this.t;
        if (dataSource != null) {
            map = dataSource.a();
            this.t.close();
            this.t = null;
        } else {
            map = null;
        }
        Drawable drawable = this.w;
        if (drawable != null) {
            x(drawable);
        }
        if (this.f2858s != null) {
            this.f2858s = null;
        }
        this.w = null;
        T t = this.u;
        if (t != null) {
            map2 = t(l(t));
            q("release", this.u);
            z(this.u);
            this.u = null;
        } else {
            map2 = null;
        }
        if (z2) {
            i().onRelease(this.m);
            this.j.c(this.m, s(map, map2, null));
        }
    }

    public abstract void z(T t);
}
