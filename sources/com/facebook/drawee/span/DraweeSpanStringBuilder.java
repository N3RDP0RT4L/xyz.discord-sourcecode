package com.facebook.drawee.span;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.text.SpannableStringBuilder;
import android.view.View;
import b.f.g.b.c;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.interfaces.DraweeHierarchy;
import com.facebook.drawee.view.DraweeHolder;
import com.facebook.imagepipeline.image.ImageInfo;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
/* loaded from: classes2.dex */
public class DraweeSpanStringBuilder extends SpannableStringBuilder {
    public static final /* synthetic */ int j = 0;
    public final Set<b.f.g.i.a> k = new HashSet();
    public final b l = new b(null);
    public View m;

    /* loaded from: classes2.dex */
    public class b implements Drawable.Callback {
        public b(a aVar) {
        }

        @Override // android.graphics.drawable.Drawable.Callback
        public void invalidateDrawable(Drawable drawable) {
            DraweeSpanStringBuilder draweeSpanStringBuilder = DraweeSpanStringBuilder.this;
            View view = draweeSpanStringBuilder.m;
            if (view != null) {
                view.invalidate();
            } else {
                Objects.requireNonNull(draweeSpanStringBuilder);
            }
        }

        @Override // android.graphics.drawable.Drawable.Callback
        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            DraweeSpanStringBuilder draweeSpanStringBuilder = DraweeSpanStringBuilder.this;
            if (draweeSpanStringBuilder.m != null) {
                DraweeSpanStringBuilder.this.m.postDelayed(runnable, j - SystemClock.uptimeMillis());
                return;
            }
            Objects.requireNonNull(draweeSpanStringBuilder);
        }

        @Override // android.graphics.drawable.Drawable.Callback
        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            DraweeSpanStringBuilder draweeSpanStringBuilder = DraweeSpanStringBuilder.this;
            View view = draweeSpanStringBuilder.m;
            if (view != null) {
                view.removeCallbacks(runnable);
            } else {
                Objects.requireNonNull(draweeSpanStringBuilder);
            }
        }
    }

    /* loaded from: classes2.dex */
    public class c extends b.f.g.c.c<ImageInfo> {
        public final b.f.g.i.a a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2863b;
        public final int c;

        public c(b.f.g.i.a aVar, boolean z2, int i) {
            this.a = aVar;
            this.f2863b = z2;
            this.c = i;
        }

        @Override // b.f.g.c.c, com.facebook.drawee.controller.ControllerListener
        public void onFinalImageSet(String str, Object obj, Animatable animatable) {
            ImageInfo imageInfo = (ImageInfo) obj;
            if (this.f2863b && imageInfo != null && this.a.p.d() != null) {
                Drawable d = this.a.p.d();
                Rect bounds = d.getBounds();
                int i = this.c;
                if (i != -1) {
                    int height = (int) ((i / imageInfo.getHeight()) * imageInfo.getWidth());
                    if (bounds.width() != height || bounds.height() != this.c) {
                        d.setBounds(0, 0, height, this.c);
                        DraweeSpanStringBuilder draweeSpanStringBuilder = DraweeSpanStringBuilder.this;
                        int i2 = DraweeSpanStringBuilder.j;
                        Objects.requireNonNull(draweeSpanStringBuilder);
                    }
                } else if (bounds.width() != imageInfo.getWidth() || bounds.height() != imageInfo.getHeight()) {
                    d.setBounds(0, 0, imageInfo.getWidth(), imageInfo.getHeight());
                    DraweeSpanStringBuilder draweeSpanStringBuilder2 = DraweeSpanStringBuilder.this;
                    int i3 = DraweeSpanStringBuilder.j;
                    Objects.requireNonNull(draweeSpanStringBuilder2);
                }
            }
        }
    }

    public void a(View view) {
        View view2 = this.m;
        if (view2 != null && view2 == view2) {
            this.m = null;
        }
        this.m = view;
        for (b.f.g.i.a aVar : this.k) {
            DraweeHolder draweeHolder = aVar.p;
            draweeHolder.f.a(c.a.ON_HOLDER_ATTACH);
            draweeHolder.f2865b = true;
            draweeHolder.b();
        }
    }

    public void b(View view) {
        if (view == this.m) {
            this.m = null;
        }
        for (b.f.g.i.a aVar : this.k) {
            DraweeHolder draweeHolder = aVar.p;
            draweeHolder.f.a(c.a.ON_HOLDER_DETACH);
            draweeHolder.f2865b = false;
            draweeHolder.b();
        }
    }

    public void c(Context context, DraweeHierarchy draweeHierarchy, DraweeController draweeController, int i, int i2, int i3, int i4, boolean z2, int i5) {
        DraweeHolder draweeHolder = new DraweeHolder(draweeHierarchy);
        draweeHolder.g(draweeController);
        if (i2 < length()) {
            Drawable d = draweeHolder.d();
            if (d != null) {
                if (d.getBounds().isEmpty()) {
                    d.setBounds(0, 0, i3, i4);
                }
                d.setCallback(this.l);
            }
            b.f.g.i.a aVar = new b.f.g.i.a(draweeHolder, i5);
            DraweeController draweeController2 = draweeHolder.e;
            if (draweeController2 instanceof AbstractDraweeController) {
                ((AbstractDraweeController) draweeController2).f(new c(aVar, z2, i4));
            }
            this.k.add(aVar);
            setSpan(aVar, i, i2 + 1, 33);
        }
    }
}
