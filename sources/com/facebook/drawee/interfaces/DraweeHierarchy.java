package com.facebook.drawee.interfaces;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
/* loaded from: classes2.dex */
public interface DraweeHierarchy {
    Drawable e();

    Rect getBounds();
}
