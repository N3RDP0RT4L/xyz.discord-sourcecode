package com.facebook.drawee.interfaces;

import android.graphics.drawable.Animatable;
import android.view.MotionEvent;
/* loaded from: classes2.dex */
public interface DraweeController {
    void a();

    DraweeHierarchy b();

    Animatable c();

    void d();

    void e(DraweeHierarchy draweeHierarchy);

    boolean onTouchEvent(MotionEvent motionEvent);
}
