package com.facebook.drawee.drawable;

import android.graphics.Matrix;
import android.graphics.Rect;
import b.f.g.e.a0;
import b.f.g.e.b0;
import b.f.g.e.r;
import b.f.g.e.s;
import b.f.g.e.t;
import b.f.g.e.u;
import b.f.g.e.v;
import b.f.g.e.w;
import b.f.g.e.x;
import b.f.g.e.y;
import b.f.g.e.z;
/* loaded from: classes2.dex */
public interface ScalingUtils$ScaleType {
    public static final ScalingUtils$ScaleType a = z.l;

    /* renamed from: b  reason: collision with root package name */
    public static final ScalingUtils$ScaleType f2861b = y.l;
    public static final ScalingUtils$ScaleType c = a0.l;
    public static final ScalingUtils$ScaleType d = x.l;
    public static final ScalingUtils$ScaleType e = v.l;
    public static final ScalingUtils$ScaleType f = w.l;
    public static final ScalingUtils$ScaleType g = r.l;
    public static final ScalingUtils$ScaleType h = t.l;
    public static final ScalingUtils$ScaleType i = s.l;
    public static final ScalingUtils$ScaleType j = b0.l;
    public static final ScalingUtils$ScaleType k = u.l;

    Matrix a(Matrix matrix, Rect rect, int i2, int i3, float f2, float f3);
}
