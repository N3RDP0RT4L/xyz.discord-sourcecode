package com.facebook.drawee.view;

import android.graphics.drawable.Drawable;
import b.c.a.a0.d;
import b.f.d.d.i;
import b.f.g.b.c;
import b.f.g.e.f0;
import b.f.g.e.g0;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.interfaces.DraweeHierarchy;
import java.util.Objects;
/* loaded from: classes2.dex */
public class DraweeHolder<DH extends DraweeHierarchy> implements g0 {
    public DH d;
    public final c f;
    public boolean a = false;

    /* renamed from: b  reason: collision with root package name */
    public boolean f2865b = false;
    public boolean c = true;
    public DraweeController e = null;

    public DraweeHolder(DH dh) {
        this.f = c.f490b ? new c() : c.a;
        if (dh != null) {
            h(dh);
        }
    }

    public final void a() {
        if (!this.a) {
            this.f.a(c.a.ON_ATTACH_CONTROLLER);
            this.a = true;
            DraweeController draweeController = this.e;
            if (draweeController != null && draweeController.b() != null) {
                this.e.d();
            }
        }
    }

    public final void b() {
        if (!this.f2865b || !this.c) {
            c();
        } else {
            a();
        }
    }

    public final void c() {
        if (this.a) {
            this.f.a(c.a.ON_DETACH_CONTROLLER);
            this.a = false;
            if (e()) {
                this.e.a();
            }
        }
    }

    public Drawable d() {
        DH dh = this.d;
        if (dh == null) {
            return null;
        }
        return dh.e();
    }

    public boolean e() {
        DraweeController draweeController = this.e;
        return draweeController != null && draweeController.b() == this.d;
    }

    public void f(boolean z2) {
        if (this.c != z2) {
            this.f.a(z2 ? c.a.ON_DRAWABLE_SHOW : c.a.ON_DRAWABLE_HIDE);
            this.c = z2;
            b();
        }
    }

    public void g(DraweeController draweeController) {
        boolean z2 = this.a;
        if (z2) {
            c();
        }
        if (e()) {
            this.f.a(c.a.ON_CLEAR_OLD_CONTROLLER);
            this.e.e(null);
        }
        this.e = draweeController;
        if (draweeController != null) {
            this.f.a(c.a.ON_SET_CONTROLLER);
            this.e.e(this.d);
        } else {
            this.f.a(c.a.ON_CLEAR_CONTROLLER);
        }
        if (z2) {
            a();
        }
    }

    public void h(DH dh) {
        this.f.a(c.a.ON_SET_HIERARCHY);
        boolean e = e();
        Drawable d = d();
        if (d instanceof f0) {
            ((f0) d).k(null);
        }
        Objects.requireNonNull(dh);
        this.d = dh;
        Drawable e2 = dh.e();
        f(e2 == null || e2.isVisible());
        Drawable d2 = d();
        if (d2 instanceof f0) {
            ((f0) d2).k(this);
        }
        if (e) {
            this.e.e(dh);
        }
    }

    public String toString() {
        i h2 = d.h2(this);
        h2.b("controllerAttached", this.a);
        h2.b("holderAttached", this.f2865b);
        h2.b("drawableVisible", this.c);
        h2.c("events", this.f.toString());
        return h2.toString();
    }
}
