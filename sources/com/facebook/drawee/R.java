package com.facebook.drawee;
/* loaded from: classes2.dex */
public final class R {

    /* loaded from: classes2.dex */
    public static final class a {
        public static final int ColorStateListItem_alpha = 0x00000002;
        public static final int ColorStateListItem_android_alpha = 0x00000001;
        public static final int ColorStateListItem_android_color = 0x00000000;
        public static final int FontFamilyFont_android_font = 0x00000000;
        public static final int FontFamilyFont_android_fontStyle = 0x00000002;
        public static final int FontFamilyFont_android_fontVariationSettings = 0x00000004;
        public static final int FontFamilyFont_android_fontWeight = 0x00000001;
        public static final int FontFamilyFont_android_ttcIndex = 0x00000003;
        public static final int FontFamilyFont_font = 0x00000005;
        public static final int FontFamilyFont_fontStyle = 0x00000006;
        public static final int FontFamilyFont_fontVariationSettings = 0x00000007;
        public static final int FontFamilyFont_fontWeight = 0x00000008;
        public static final int FontFamilyFont_ttcIndex = 0x00000009;
        public static final int FontFamily_fontProviderAuthority = 0x00000000;
        public static final int FontFamily_fontProviderCerts = 0x00000001;
        public static final int FontFamily_fontProviderFetchStrategy = 0x00000002;
        public static final int FontFamily_fontProviderFetchTimeout = 0x00000003;
        public static final int FontFamily_fontProviderPackage = 0x00000004;
        public static final int FontFamily_fontProviderQuery = 0x00000005;
        public static final int FontFamily_fontProviderSystemFontFamily = 0x00000006;
        public static final int GenericDraweeHierarchy_actualImageScaleType = 0x00000000;
        public static final int GenericDraweeHierarchy_backgroundImage = 0x00000001;
        public static final int GenericDraweeHierarchy_fadeDuration = 0x00000002;
        public static final int GenericDraweeHierarchy_failureImage = 0x00000003;
        public static final int GenericDraweeHierarchy_failureImageScaleType = 0x00000004;
        public static final int GenericDraweeHierarchy_overlayImage = 0x00000005;
        public static final int GenericDraweeHierarchy_placeholderImage = 0x00000006;
        public static final int GenericDraweeHierarchy_placeholderImageScaleType = 0x00000007;
        public static final int GenericDraweeHierarchy_pressedStateOverlayImage = 0x00000008;
        public static final int GenericDraweeHierarchy_progressBarAutoRotateInterval = 0x00000009;
        public static final int GenericDraweeHierarchy_progressBarImage = 0x0000000a;
        public static final int GenericDraweeHierarchy_progressBarImageScaleType = 0x0000000b;
        public static final int GenericDraweeHierarchy_retryImage = 0x0000000c;
        public static final int GenericDraweeHierarchy_retryImageScaleType = 0x0000000d;
        public static final int GenericDraweeHierarchy_roundAsCircle = 0x0000000e;
        public static final int GenericDraweeHierarchy_roundBottomEnd = 0x0000000f;
        public static final int GenericDraweeHierarchy_roundBottomLeft = 0x00000010;
        public static final int GenericDraweeHierarchy_roundBottomRight = 0x00000011;
        public static final int GenericDraweeHierarchy_roundBottomStart = 0x00000012;
        public static final int GenericDraweeHierarchy_roundTopEnd = 0x00000013;
        public static final int GenericDraweeHierarchy_roundTopLeft = 0x00000014;
        public static final int GenericDraweeHierarchy_roundTopRight = 0x00000015;
        public static final int GenericDraweeHierarchy_roundTopStart = 0x00000016;
        public static final int GenericDraweeHierarchy_roundWithOverlayColor = 0x00000017;
        public static final int GenericDraweeHierarchy_roundedCornerRadius = 0x00000018;
        public static final int GenericDraweeHierarchy_roundingBorderColor = 0x00000019;
        public static final int GenericDraweeHierarchy_roundingBorderPadding = 0x0000001a;
        public static final int GenericDraweeHierarchy_roundingBorderWidth = 0x0000001b;
        public static final int GenericDraweeHierarchy_viewAspectRatio = 0x0000001c;
        public static final int GradientColorItem_android_color = 0x00000000;
        public static final int GradientColorItem_android_offset = 0x00000001;
        public static final int GradientColor_android_centerColor = 0x00000007;
        public static final int GradientColor_android_centerX = 0x00000003;
        public static final int GradientColor_android_centerY = 0x00000004;
        public static final int GradientColor_android_endColor = 0x00000001;
        public static final int GradientColor_android_endX = 0x0000000a;
        public static final int GradientColor_android_endY = 0x0000000b;
        public static final int GradientColor_android_gradientRadius = 0x00000005;
        public static final int GradientColor_android_startColor = 0x00000000;
        public static final int GradientColor_android_startX = 0x00000008;
        public static final int GradientColor_android_startY = 0x00000009;
        public static final int GradientColor_android_tileMode = 0x00000006;
        public static final int GradientColor_android_type = 0x00000002;
        public static final int SimpleDraweeView_actualImageResource = 0x00000000;
        public static final int SimpleDraweeView_actualImageUri = 0x00000001;
        public static final int SimpleDraweeView_backgroundImage = 0x00000002;
        public static final int SimpleDraweeView_fadeDuration = 0x00000003;
        public static final int SimpleDraweeView_failureImage = 0x00000004;
        public static final int SimpleDraweeView_failureImageScaleType = 0x00000005;
        public static final int SimpleDraweeView_overlayImage = 0x00000006;
        public static final int SimpleDraweeView_placeholderImage = 0x00000007;
        public static final int SimpleDraweeView_placeholderImageScaleType = 0x00000008;
        public static final int SimpleDraweeView_pressedStateOverlayImage = 0x00000009;
        public static final int SimpleDraweeView_progressBarAutoRotateInterval = 0x0000000a;
        public static final int SimpleDraweeView_progressBarImage = 0x0000000b;
        public static final int SimpleDraweeView_progressBarImageScaleType = 0x0000000c;
        public static final int SimpleDraweeView_retryImage = 0x0000000d;
        public static final int SimpleDraweeView_retryImageScaleType = 0x0000000e;
        public static final int SimpleDraweeView_roundAsCircle = 0x0000000f;
        public static final int SimpleDraweeView_roundBottomEnd = 0x00000010;
        public static final int SimpleDraweeView_roundBottomLeft = 0x00000011;
        public static final int SimpleDraweeView_roundBottomRight = 0x00000012;
        public static final int SimpleDraweeView_roundBottomStart = 0x00000013;
        public static final int SimpleDraweeView_roundTopEnd = 0x00000014;
        public static final int SimpleDraweeView_roundTopLeft = 0x00000015;
        public static final int SimpleDraweeView_roundTopRight = 0x00000016;
        public static final int SimpleDraweeView_roundTopStart = 0x00000017;
        public static final int SimpleDraweeView_roundWithOverlayColor = 0x00000018;
        public static final int SimpleDraweeView_roundedCornerRadius = 0x00000019;
        public static final int SimpleDraweeView_roundingBorderColor = 0x0000001a;
        public static final int SimpleDraweeView_roundingBorderPadding = 0x0000001b;
        public static final int SimpleDraweeView_roundingBorderWidth = 0x0000001c;
        public static final int SimpleDraweeView_viewAspectRatio = 0x0000001d;
        public static final int[] ColorStateListItem = {16843173, 16843551, xyz.discord.R.attr.alpha};
        public static final int[] FontFamily = {xyz.discord.R.attr.fontProviderAuthority, xyz.discord.R.attr.fontProviderCerts, xyz.discord.R.attr.fontProviderFetchStrategy, xyz.discord.R.attr.fontProviderFetchTimeout, xyz.discord.R.attr.fontProviderPackage, xyz.discord.R.attr.fontProviderQuery, xyz.discord.R.attr.fontProviderSystemFontFamily};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, xyz.discord.R.attr.font, xyz.discord.R.attr.fontStyle, xyz.discord.R.attr.fontVariationSettings, xyz.discord.R.attr.fontWeight, xyz.discord.R.attr.ttcIndex};
        public static final int[] GenericDraweeHierarchy = {xyz.discord.R.attr.actualImageScaleType, xyz.discord.R.attr.backgroundImage, xyz.discord.R.attr.fadeDuration, xyz.discord.R.attr.failureImage, xyz.discord.R.attr.failureImageScaleType, xyz.discord.R.attr.overlayImage, xyz.discord.R.attr.placeholderImage, xyz.discord.R.attr.placeholderImageScaleType, xyz.discord.R.attr.pressedStateOverlayImage, xyz.discord.R.attr.progressBarAutoRotateInterval, xyz.discord.R.attr.progressBarImage, xyz.discord.R.attr.progressBarImageScaleType, xyz.discord.R.attr.retryImage, xyz.discord.R.attr.retryImageScaleType, xyz.discord.R.attr.roundAsCircle, xyz.discord.R.attr.roundBottomEnd, xyz.discord.R.attr.roundBottomLeft, xyz.discord.R.attr.roundBottomRight, xyz.discord.R.attr.roundBottomStart, xyz.discord.R.attr.roundTopEnd, xyz.discord.R.attr.roundTopLeft, xyz.discord.R.attr.roundTopRight, xyz.discord.R.attr.roundTopStart, xyz.discord.R.attr.roundWithOverlayColor, xyz.discord.R.attr.roundedCornerRadius, xyz.discord.R.attr.roundingBorderColor, xyz.discord.R.attr.roundingBorderPadding, xyz.discord.R.attr.roundingBorderWidth, xyz.discord.R.attr.viewAspectRatio};
        public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
        public static final int[] GradientColorItem = {16843173, 16844052};
        public static final int[] SimpleDraweeView = {xyz.discord.R.attr.actualImageResource, xyz.discord.R.attr.actualImageUri, xyz.discord.R.attr.backgroundImage, xyz.discord.R.attr.fadeDuration, xyz.discord.R.attr.failureImage, xyz.discord.R.attr.failureImageScaleType, xyz.discord.R.attr.overlayImage, xyz.discord.R.attr.placeholderImage, xyz.discord.R.attr.placeholderImageScaleType, xyz.discord.R.attr.pressedStateOverlayImage, xyz.discord.R.attr.progressBarAutoRotateInterval, xyz.discord.R.attr.progressBarImage, xyz.discord.R.attr.progressBarImageScaleType, xyz.discord.R.attr.retryImage, xyz.discord.R.attr.retryImageScaleType, xyz.discord.R.attr.roundAsCircle, xyz.discord.R.attr.roundBottomEnd, xyz.discord.R.attr.roundBottomLeft, xyz.discord.R.attr.roundBottomRight, xyz.discord.R.attr.roundBottomStart, xyz.discord.R.attr.roundTopEnd, xyz.discord.R.attr.roundTopLeft, xyz.discord.R.attr.roundTopRight, xyz.discord.R.attr.roundTopStart, xyz.discord.R.attr.roundWithOverlayColor, xyz.discord.R.attr.roundedCornerRadius, xyz.discord.R.attr.roundingBorderColor, xyz.discord.R.attr.roundingBorderPadding, xyz.discord.R.attr.roundingBorderWidth, xyz.discord.R.attr.viewAspectRatio};

        private a() {
        }
    }

    private R() {
    }
}
