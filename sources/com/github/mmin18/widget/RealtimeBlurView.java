package com.github.mmin18.widget;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import b.h.a.a.c;
import b.h.a.a.d;
import b.h.a.a.e;
import com.github.mmin18.realtimeblurview.R;
/* loaded from: classes2.dex */
public class RealtimeBlurView extends View {
    public static int j;
    public static int k;
    public static b l = new b(null);
    public float m;
    public int n;
    public float o;
    public boolean q;
    public Bitmap r;

    /* renamed from: s  reason: collision with root package name */
    public Bitmap f2882s;
    public Canvas t;
    public boolean u;

    /* renamed from: y  reason: collision with root package name */
    public View f2884y;

    /* renamed from: z  reason: collision with root package name */
    public boolean f2885z;
    public final Rect w = new Rect();

    /* renamed from: x  reason: collision with root package name */
    public final Rect f2883x = new Rect();
    public final ViewTreeObserver.OnPreDrawListener A = new a();
    public final c p = getBlurImpl();
    public Paint v = new Paint();

    /* loaded from: classes2.dex */
    public class a implements ViewTreeObserver.OnPreDrawListener {
        public a() {
        }

        /*  JADX ERROR: JadxRuntimeException in pass: AttachTryCatchVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Null type added to not empty exception handler: OutOfMemoryError -> 0x0095
            	at jadx.core.dex.trycatch.ExceptionHandler.addCatchType(ExceptionHandler.java:54)
            	at jadx.core.dex.visitors.AttachTryCatchVisitor.createHandler(AttachTryCatchVisitor.java:136)
            	at jadx.core.dex.visitors.AttachTryCatchVisitor.convertToHandlers(AttachTryCatchVisitor.java:123)
            	at jadx.core.dex.visitors.AttachTryCatchVisitor.initTryCatches(AttachTryCatchVisitor.java:59)
            	at jadx.core.dex.visitors.AttachTryCatchVisitor.visit(AttachTryCatchVisitor.java:47)
            */
        @Override // android.view.ViewTreeObserver.OnPreDrawListener
        public boolean onPreDraw() {
            /*
                Method dump skipped, instructions count: 357
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.github.mmin18.widget.RealtimeBlurView.a.onPreDraw():boolean");
        }
    }

    /* loaded from: classes2.dex */
    public static class b extends RuntimeException {
        public b(a aVar) {
        }
    }

    public RealtimeBlurView(Context context, AttributeSet attributeSet) {
        super(context, null);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, R.a.RealtimeBlurView);
        this.o = obtainStyledAttributes.getDimension(R.a.RealtimeBlurView_realtimeBlurRadius, TypedValue.applyDimension(1, 10.0f, context.getResources().getDisplayMetrics()));
        this.m = obtainStyledAttributes.getFloat(R.a.RealtimeBlurView_realtimeDownsampleFactor, 4.0f);
        this.n = obtainStyledAttributes.getColor(R.a.RealtimeBlurView_realtimeOverlayColor, -1426063361);
        obtainStyledAttributes.recycle();
    }

    public static /* synthetic */ int a() {
        int i = j;
        j = i - 1;
        return i;
    }

    public void b() {
        c();
        this.p.release();
    }

    public final void c() {
        Bitmap bitmap = this.r;
        if (bitmap != null) {
            bitmap.recycle();
            this.r = null;
        }
        Bitmap bitmap2 = this.f2882s;
        if (bitmap2 != null) {
            bitmap2.recycle();
            this.f2882s = null;
        }
    }

    @Override // android.view.View
    public void draw(Canvas canvas) {
        if (this.u) {
            throw l;
        } else if (j <= 0) {
            super.draw(canvas);
        }
    }

    public View getActivityDecorView() {
        Context context = getContext();
        for (int i = 0; i < 4 && context != null && !(context instanceof Activity) && (context instanceof ContextWrapper); i++) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (context instanceof Activity) {
            return ((Activity) context).getWindow().getDecorView();
        }
        return null;
    }

    public c getBlurImpl() {
        if (k == 0) {
            try {
                b.h.a.a.a aVar = new b.h.a.a.a();
                Bitmap createBitmap = Bitmap.createBitmap(4, 4, Bitmap.Config.ARGB_8888);
                aVar.b(getContext(), createBitmap, 4.0f);
                aVar.release();
                createBitmap.recycle();
                k = 3;
            } catch (Throwable unused) {
            }
        }
        if (k == 0) {
            try {
                getClass().getClassLoader().loadClass("androidx.renderscript.RenderScript");
                b.h.a.a.b bVar = new b.h.a.a.b();
                Bitmap createBitmap2 = Bitmap.createBitmap(4, 4, Bitmap.Config.ARGB_8888);
                bVar.b(getContext(), createBitmap2, 4.0f);
                bVar.release();
                createBitmap2.recycle();
                k = 1;
            } catch (Throwable unused2) {
            }
        }
        if (k == 0) {
            try {
                getClass().getClassLoader().loadClass("androidx.renderscript.RenderScript");
                e eVar = new e();
                Bitmap createBitmap3 = Bitmap.createBitmap(4, 4, Bitmap.Config.ARGB_8888);
                eVar.b(getContext(), createBitmap3, 4.0f);
                eVar.release();
                createBitmap3.recycle();
                k = 2;
            } catch (Throwable unused3) {
            }
        }
        if (k == 0) {
            k = -1;
        }
        int i = k;
        if (i == 1) {
            return new b.h.a.a.b();
        }
        if (i == 2) {
            return new e();
        }
        if (i != 3) {
            return new d();
        }
        return new b.h.a.a.a();
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        View activityDecorView = getActivityDecorView();
        this.f2884y = activityDecorView;
        boolean z2 = false;
        if (activityDecorView != null) {
            activityDecorView.getViewTreeObserver().addOnPreDrawListener(this.A);
            if (this.f2884y.getRootView() != getRootView()) {
                z2 = true;
            }
            this.f2885z = z2;
            if (z2) {
                this.f2884y.postInvalidate();
                return;
            }
            return;
        }
        this.f2885z = false;
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        View view = this.f2884y;
        if (view != null) {
            view.getViewTreeObserver().removeOnPreDrawListener(this.A);
        }
        b();
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Bitmap bitmap = this.f2882s;
        int i = this.n;
        if (bitmap != null) {
            this.w.right = bitmap.getWidth();
            this.w.bottom = bitmap.getHeight();
            this.f2883x.right = getWidth();
            this.f2883x.bottom = getHeight();
            canvas.drawBitmap(bitmap, this.w, this.f2883x, (Paint) null);
        }
        this.v.setColor(i);
        canvas.drawRect(this.f2883x, this.v);
    }

    public void setBlurRadius(float f) {
        if (this.o != f) {
            this.o = f;
            this.q = true;
            invalidate();
        }
    }

    public void setDownsampleFactor(float f) {
        if (f <= 0.0f) {
            throw new IllegalArgumentException("Downsample factor must be greater than 0.");
        } else if (this.m != f) {
            this.m = f;
            this.q = true;
            c();
            invalidate();
        }
    }

    public void setOverlayColor(int i) {
        if (this.n != i) {
            this.n = i;
            invalidate();
        }
    }
}
