package com.github.mmin18.realtimeblurview;
/* loaded from: classes2.dex */
public final class R {

    /* loaded from: classes2.dex */
    public static final class a {
        public static final int[] RealtimeBlurView = {xyz.discord.R.attr.realtimeBlurRadius, xyz.discord.R.attr.realtimeDownsampleFactor, xyz.discord.R.attr.realtimeOverlayColor};
        public static final int RealtimeBlurView_realtimeBlurRadius = 0x00000000;
        public static final int RealtimeBlurView_realtimeDownsampleFactor = 0x00000001;
        public static final int RealtimeBlurView_realtimeOverlayColor = 0x00000002;

        private a() {
        }
    }

    private R() {
    }
}
