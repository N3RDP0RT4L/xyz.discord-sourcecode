package com.google.android.material.slider;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
/* loaded from: classes3.dex */
public interface BaseOnChangeListener<S> {
    void onValueChange(@NonNull S s2, float f, boolean z2);
}
