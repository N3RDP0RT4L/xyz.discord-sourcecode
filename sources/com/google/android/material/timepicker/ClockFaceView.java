package com.google.android.material.timepicker;

import android.content.res.ColorStateList;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import b.i.a.g.k.e;
import com.google.android.material.R;
import com.google.android.material.timepicker.ClockHandView;
/* loaded from: classes3.dex */
public class ClockFaceView extends e implements ClockHandView.OnRotateListener {
    public final ClockHandView m;
    public final AccessibilityDelegateCompat q;
    public final int[] r;
    public final int t;
    public String[] u;
    public float v;
    public final ColorStateList w;
    public final Rect n = new Rect();
    public final RectF o = new RectF();
    public final SparseArray<TextView> p = new SparseArray<>();

    /* renamed from: s  reason: collision with root package name */
    public final float[] f3045s = {0.0f, 0.9f, 1.0f};

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public ClockFaceView(@androidx.annotation.NonNull android.content.Context r8, @androidx.annotation.Nullable android.util.AttributeSet r9) {
        /*
            r7 = this;
            int r0 = com.google.android.material.R.attr.materialClockStyle
            r7.<init>(r8, r9, r0)
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            r7.n = r1
            android.graphics.RectF r1 = new android.graphics.RectF
            r1.<init>()
            r7.o = r1
            android.util.SparseArray r1 = new android.util.SparseArray
            r1.<init>()
            r7.p = r1
            r1 = 3
            float[] r2 = new float[r1]
            r2 = {x00b6: FILL_ARRAY_DATA  , data: [0, 1063675494, 1065353216} // fill-array
            r7.f3045s = r2
            int[] r2 = com.google.android.material.R.styleable.ClockFaceView
            int r3 = com.google.android.material.R.style.Widget_MaterialComponents_TimePicker_Clock
            android.content.res.TypedArray r9 = r8.obtainStyledAttributes(r9, r2, r0, r3)
            android.content.res.Resources r0 = r7.getResources()
            int r2 = com.google.android.material.R.styleable.ClockFaceView_clockNumberTextColor
            android.content.res.ColorStateList r2 = com.google.android.material.resources.MaterialResources.getColorStateList(r8, r9, r2)
            r7.w = r2
            android.view.LayoutInflater r3 = android.view.LayoutInflater.from(r8)
            int r4 = com.google.android.material.R.layout.material_clockface_view
            r5 = 1
            r3.inflate(r4, r7, r5)
            int r3 = com.google.android.material.R.id.material_clock_hand
            android.view.View r3 = r7.findViewById(r3)
            com.google.android.material.timepicker.ClockHandView r3 = (com.google.android.material.timepicker.ClockHandView) r3
            r7.m = r3
            int r4 = com.google.android.material.R.dimen.material_clock_hand_padding
            int r0 = r0.getDimensionPixelSize(r4)
            r7.t = r0
            int[] r0 = new int[r5]
            r4 = 16842913(0x10100a1, float:2.369401E-38)
            r6 = 0
            r0[r6] = r4
            int r4 = r2.getDefaultColor()
            int r0 = r2.getColorForState(r0, r4)
            int[] r1 = new int[r1]
            r1[r6] = r0
            r1[r5] = r0
            int r0 = r2.getDefaultColor()
            r2 = 2
            r1[r2] = r0
            r7.r = r1
            java.util.List<com.google.android.material.timepicker.ClockHandView$OnRotateListener> r0 = r3.q
            r0.add(r7)
            int r0 = com.google.android.material.R.color.material_timepicker_clockface
            android.content.res.ColorStateList r0 = androidx.appcompat.content.res.AppCompatResources.getColorStateList(r8, r0)
            int r0 = r0.getDefaultColor()
            int r1 = com.google.android.material.R.styleable.ClockFaceView_clockFaceBackgroundColor
            android.content.res.ColorStateList r8 = com.google.android.material.resources.MaterialResources.getColorStateList(r8, r9, r1)
            if (r8 != 0) goto L89
            goto L8d
        L89:
            int r0 = r8.getDefaultColor()
        L8d:
            r7.setBackgroundColor(r0)
            android.view.ViewTreeObserver r8 = r7.getViewTreeObserver()
            b.i.a.g.k.b r0 = new b.i.a.g.k.b
            r0.<init>(r7)
            r8.addOnPreDrawListener(r0)
            r7.setFocusable(r5)
            r9.recycle()
            b.i.a.g.k.c r8 = new b.i.a.g.k.c
            r8.<init>(r7)
            r7.q = r8
            r8 = 12
            java.lang.String[] r8 = new java.lang.String[r8]
            java.lang.String r9 = ""
            java.util.Arrays.fill(r8, r9)
            r7.c(r8, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.timepicker.ClockFaceView.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public final void b() {
        RectF rectF = this.m.u;
        for (int i = 0; i < this.p.size(); i++) {
            TextView textView = this.p.get(i);
            if (textView != null) {
                textView.getDrawingRect(this.n);
                this.n.offset(textView.getPaddingLeft(), textView.getPaddingTop());
                offsetDescendantRectToMyCoords(textView, this.n);
                this.o.set(this.n);
                textView.getPaint().setShader(!RectF.intersects(rectF, this.o) ? null : new RadialGradient(rectF.centerX() - this.o.left, rectF.centerY() - this.o.top, 0.5f * rectF.width(), this.r, this.f3045s, Shader.TileMode.CLAMP));
                textView.invalidate();
            }
        }
    }

    public void c(String[] strArr, @StringRes int i) {
        this.u = strArr;
        LayoutInflater from = LayoutInflater.from(getContext());
        int size = this.p.size();
        for (int i2 = 0; i2 < Math.max(this.u.length, size); i2++) {
            TextView textView = this.p.get(i2);
            if (i2 >= this.u.length) {
                removeView(textView);
                this.p.remove(i2);
            } else {
                if (textView == null) {
                    textView = (TextView) from.inflate(R.layout.material_clockface_textview, (ViewGroup) this, false);
                    this.p.put(i2, textView);
                    addView(textView);
                }
                textView.setVisibility(0);
                textView.setText(this.u[i2]);
                textView.setTag(R.id.material_value_index, Integer.valueOf(i2));
                ViewCompat.setAccessibilityDelegate(textView, this.q);
                textView.setTextColor(this.w);
                if (i != 0) {
                    textView.setContentDescription(getResources().getString(i, this.u[i2]));
                }
            }
        }
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(@NonNull AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        AccessibilityNodeInfoCompat.wrap(accessibilityNodeInfo).setCollectionInfo(AccessibilityNodeInfoCompat.CollectionInfoCompat.obtain(1, this.u.length, false, 1));
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        b();
    }

    @Override // com.google.android.material.timepicker.ClockHandView.OnRotateListener
    public void onRotate(float f, boolean z2) {
        if (Math.abs(this.v - f) > 0.001f) {
            this.v = f;
            b();
        }
    }
}
