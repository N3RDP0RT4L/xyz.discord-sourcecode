package com.google.android.material.timepicker;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import androidx.annotation.FloatRange;
import androidx.annotation.Px;
import java.util.ArrayList;
import java.util.List;
/* loaded from: classes3.dex */
public class ClockHandView extends View {
    public static final /* synthetic */ int j = 0;
    public int A;
    public ValueAnimator k;
    public boolean l;
    public float m;
    public float n;
    public boolean o;
    public int p;
    public final int r;

    /* renamed from: s  reason: collision with root package name */
    public final float f3046s;
    public final Paint t;
    @Px
    public final int v;
    public float w;

    /* renamed from: x  reason: collision with root package name */
    public boolean f3047x;

    /* renamed from: y  reason: collision with root package name */
    public OnActionUpListener f3048y;

    /* renamed from: z  reason: collision with root package name */
    public double f3049z;
    public final List<OnRotateListener> q = new ArrayList();
    public final RectF u = new RectF();

    /* loaded from: classes3.dex */
    public interface OnActionUpListener {
        void onActionUp(@FloatRange(from = 0.0d, to = 360.0d) float f, boolean z2);
    }

    /* loaded from: classes3.dex */
    public interface OnRotateListener {
        void onRotate(@FloatRange(from = 0.0d, to = 360.0d) float f, boolean z2);
    }

    /* loaded from: classes3.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            ClockHandView clockHandView = ClockHandView.this;
            int i = ClockHandView.j;
            clockHandView.c(floatValue, true);
        }
    }

    /* loaded from: classes3.dex */
    public class b extends AnimatorListenerAdapter {
        public b(ClockHandView clockHandView) {
        }

        @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            animator.end();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public ClockHandView(android.content.Context r5, @androidx.annotation.Nullable android.util.AttributeSet r6) {
        /*
            r4 = this;
            int r0 = com.google.android.material.R.attr.materialClockStyle
            r4.<init>(r5, r6, r0)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r4.q = r1
            android.graphics.Paint r1 = new android.graphics.Paint
            r1.<init>()
            r4.t = r1
            android.graphics.RectF r2 = new android.graphics.RectF
            r2.<init>()
            r4.u = r2
            int[] r2 = com.google.android.material.R.styleable.ClockHandView
            int r3 = com.google.android.material.R.style.Widget_MaterialComponents_TimePicker_Clock
            android.content.res.TypedArray r6 = r5.obtainStyledAttributes(r6, r2, r0, r3)
            int r0 = com.google.android.material.R.styleable.ClockHandView_materialCircleRadius
            r2 = 0
            int r0 = r6.getDimensionPixelSize(r0, r2)
            r4.A = r0
            int r0 = com.google.android.material.R.styleable.ClockHandView_selectorSize
            int r0 = r6.getDimensionPixelSize(r0, r2)
            r4.r = r0
            android.content.res.Resources r0 = r4.getResources()
            int r3 = com.google.android.material.R.dimen.material_clock_hand_stroke_width
            int r3 = r0.getDimensionPixelSize(r3)
            r4.v = r3
            int r3 = com.google.android.material.R.dimen.material_clock_hand_center_dot_radius
            int r0 = r0.getDimensionPixelSize(r3)
            float r0 = (float) r0
            r4.f3046s = r0
            int r0 = com.google.android.material.R.styleable.ClockHandView_clockHandColor
            int r0 = r6.getColor(r0, r2)
            r3 = 1
            r1.setAntiAlias(r3)
            r1.setColor(r0)
            r0 = 0
            r4.b(r0, r2)
            android.view.ViewConfiguration r5 = android.view.ViewConfiguration.get(r5)
            int r5 = r5.getScaledTouchSlop()
            r4.p = r5
            r5 = 2
            androidx.core.view.ViewCompat.setImportantForAccessibility(r4, r5)
            r6.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.timepicker.ClockHandView.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public final int a(float f, float f2) {
        int degrees = ((int) Math.toDegrees(Math.atan2(f2 - (getHeight() / 2), f - (getWidth() / 2)))) + 90;
        return degrees < 0 ? degrees + 360 : degrees;
    }

    public void b(@FloatRange(from = 0.0d, to = 360.0d) float f, boolean z2) {
        ValueAnimator valueAnimator = this.k;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        if (!z2) {
            c(f, false);
            return;
        }
        float f2 = this.w;
        if (Math.abs(f2 - f) > 180.0f) {
            if (f2 > 180.0f && f < 180.0f) {
                f += 360.0f;
            }
            if (f2 < 180.0f && f > 180.0f) {
                f2 += 360.0f;
            }
        }
        Pair pair = new Pair(Float.valueOf(f2), Float.valueOf(f));
        ValueAnimator ofFloat = ValueAnimator.ofFloat(((Float) pair.first).floatValue(), ((Float) pair.second).floatValue());
        this.k = ofFloat;
        ofFloat.setDuration(200L);
        this.k.addUpdateListener(new a());
        this.k.addListener(new b(this));
        this.k.start();
    }

    public final void c(@FloatRange(from = 0.0d, to = 360.0d) float f, boolean z2) {
        float f2 = f % 360.0f;
        this.w = f2;
        this.f3049z = Math.toRadians(f2 - 90.0f);
        float cos = (this.A * ((float) Math.cos(this.f3049z))) + (getWidth() / 2);
        float sin = (this.A * ((float) Math.sin(this.f3049z))) + (getHeight() / 2);
        RectF rectF = this.u;
        int i = this.r;
        rectF.set(cos - i, sin - i, cos + i, sin + i);
        for (OnRotateListener onRotateListener : this.q) {
            onRotateListener.onRotate(f2, z2);
        }
        invalidate();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int width;
        super.onDraw(canvas);
        int height = getHeight() / 2;
        float width2 = getWidth() / 2;
        float f = height;
        this.t.setStrokeWidth(0.0f);
        canvas.drawCircle((this.A * ((float) Math.cos(this.f3049z))) + width2, (this.A * ((float) Math.sin(this.f3049z))) + f, this.r, this.t);
        double sin = Math.sin(this.f3049z);
        double cos = Math.cos(this.f3049z);
        this.t.setStrokeWidth(this.v);
        canvas.drawLine(width2, f, width + ((int) (cos * r6)), height + ((int) (r6 * sin)), this.t);
        canvas.drawCircle(width2, f, this.f3046s, this.t);
    }

    @Override // android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        super.onLayout(z2, i, i2, i3, i4);
        b(this.w, false);
    }

    @Override // android.view.View
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        OnActionUpListener onActionUpListener;
        int actionMasked = motionEvent.getActionMasked();
        float x2 = motionEvent.getX();
        float y2 = motionEvent.getY();
        boolean z6 = false;
        if (actionMasked != 0) {
            if (actionMasked == 1 || actionMasked == 2) {
                int i = (int) (x2 - this.m);
                int i2 = (int) (y2 - this.n);
                this.o = (i2 * i2) + (i * i) > this.p;
                z3 = this.f3047x;
                z4 = actionMasked == 1;
            } else {
                z4 = false;
                z3 = false;
            }
            z2 = false;
        } else {
            this.m = x2;
            this.n = y2;
            this.o = true;
            this.f3047x = false;
            z4 = false;
            z3 = false;
            z2 = true;
        }
        boolean z7 = this.f3047x;
        float a2 = a(x2, y2);
        boolean z8 = this.w != a2;
        if (!z2 || !z8) {
            if (z8 || z3) {
                if (z4 && this.l) {
                    z6 = true;
                }
                b(a2, z6);
            }
            z5 = z6 | z7;
            this.f3047x = z5;
            if (z5 && z4 && (onActionUpListener = this.f3048y) != null) {
                onActionUpListener.onActionUp(a(x2, y2), this.o);
            }
            return true;
        }
        z6 = true;
        z5 = z6 | z7;
        this.f3047x = z5;
        if (z5) {
            onActionUpListener.onActionUp(a(x2, y2), this.o);
        }
        return true;
    }
}
