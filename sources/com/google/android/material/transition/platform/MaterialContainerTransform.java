package com.google.android.material.transition.platform;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.transition.ArcMotion;
import android.transition.PathMotion;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import androidx.annotation.ColorInt;
import androidx.annotation.FloatRange;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.RestrictTo;
import androidx.annotation.StyleRes;
import androidx.core.util.Preconditions;
import androidx.core.view.InputDeviceCompat;
import androidx.core.view.ViewCompat;
import b.i.a.g.l.l.e;
import b.i.a.g.l.l.f;
import b.i.a.g.l.l.g;
import b.i.a.g.l.l.i;
import b.i.a.g.l.l.j;
import b.i.a.g.l.l.k;
import com.google.android.material.R;
import com.google.android.material.animation.AnimationUtils;
import com.google.android.material.internal.ViewUtils;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import com.google.android.material.shape.AbsoluteCornerSize;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.google.android.material.shape.Shapeable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;
@RequiresApi(21)
/* loaded from: classes3.dex */
public final class MaterialContainerTransform extends Transition {
    private static final float ELEVATION_NOT_SET = -1.0f;
    public static final int FADE_MODE_CROSS = 2;
    public static final int FADE_MODE_IN = 0;
    public static final int FADE_MODE_OUT = 1;
    public static final int FADE_MODE_THROUGH = 3;
    public static final int FIT_MODE_AUTO = 0;
    public static final int FIT_MODE_HEIGHT = 2;
    public static final int FIT_MODE_WIDTH = 1;
    public static final int TRANSITION_DIRECTION_AUTO = 0;
    public static final int TRANSITION_DIRECTION_ENTER = 1;
    public static final int TRANSITION_DIRECTION_RETURN = 2;
    private boolean elevationShadowEnabled;
    private float endElevation;
    @Nullable
    private ShapeAppearanceModel endShapeAppearanceModel;
    @Nullable
    private View endView;
    @Nullable
    private ProgressThresholds fadeProgressThresholds;
    @Nullable
    private ProgressThresholds scaleMaskProgressThresholds;
    @Nullable
    private ProgressThresholds scaleProgressThresholds;
    @Nullable
    private ProgressThresholds shapeMaskProgressThresholds;
    private float startElevation;
    @Nullable
    private ShapeAppearanceModel startShapeAppearanceModel;
    @Nullable
    private View startView;
    private static final String TAG = MaterialContainerTransform.class.getSimpleName();
    private static final String PROP_BOUNDS = "materialContainerTransition:bounds";
    private static final String PROP_SHAPE_APPEARANCE = "materialContainerTransition:shapeAppearance";
    private static final String[] TRANSITION_PROPS = {PROP_BOUNDS, PROP_SHAPE_APPEARANCE};
    private static final c DEFAULT_ENTER_THRESHOLDS = new c(new ProgressThresholds(0.0f, 0.25f), new ProgressThresholds(0.0f, 1.0f), new ProgressThresholds(0.0f, 1.0f), new ProgressThresholds(0.0f, 0.75f), null);
    private static final c DEFAULT_RETURN_THRESHOLDS = new c(new ProgressThresholds(0.6f, 0.9f), new ProgressThresholds(0.0f, 1.0f), new ProgressThresholds(0.0f, 0.9f), new ProgressThresholds(0.3f, 0.9f), null);
    private static final c DEFAULT_ENTER_THRESHOLDS_ARC = new c(new ProgressThresholds(0.1f, 0.4f), new ProgressThresholds(0.1f, 1.0f), new ProgressThresholds(0.1f, 1.0f), new ProgressThresholds(0.1f, 0.9f), null);
    private static final c DEFAULT_RETURN_THRESHOLDS_ARC = new c(new ProgressThresholds(0.6f, 0.9f), new ProgressThresholds(0.0f, 0.9f), new ProgressThresholds(0.0f, 0.9f), new ProgressThresholds(0.2f, 0.9f), null);
    private boolean drawDebugEnabled = false;
    private boolean holdAtEndEnabled = false;
    @IdRes
    private int drawingViewId = 16908290;
    @IdRes
    private int startViewId = -1;
    @IdRes
    private int endViewId = -1;
    @ColorInt
    private int containerColor = 0;
    @ColorInt
    private int startContainerColor = 0;
    @ColorInt
    private int endContainerColor = 0;
    @ColorInt
    private int scrimColor = 1375731712;
    private int transitionDirection = 0;
    private int fadeMode = 0;
    private int fitMode = 0;

    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    /* loaded from: classes3.dex */
    public @interface FadeMode {
    }

    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    /* loaded from: classes3.dex */
    public @interface FitMode {
    }

    /* loaded from: classes3.dex */
    public static class ProgressThresholds {
        @FloatRange(from = ShadowDrawableWrapper.COS_45, to = 1.0d)
        private final float end;
        @FloatRange(from = ShadowDrawableWrapper.COS_45, to = 1.0d)
        private final float start;

        public ProgressThresholds(@FloatRange(from = 0.0d, to = 1.0d) float f, @FloatRange(from = 0.0d, to = 1.0d) float f2) {
            this.start = f;
            this.end = f2;
        }

        @FloatRange(from = ShadowDrawableWrapper.COS_45, to = 1.0d)
        public float getEnd() {
            return this.end;
        }

        @FloatRange(from = ShadowDrawableWrapper.COS_45, to = 1.0d)
        public float getStart() {
            return this.start;
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    /* loaded from: classes3.dex */
    public @interface TransitionDirection {
    }

    /* loaded from: classes3.dex */
    public class a implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ d a;

        public a(MaterialContainerTransform materialContainerTransform, d dVar) {
            this.a = dVar;
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            d dVar = this.a;
            float animatedFraction = valueAnimator.getAnimatedFraction();
            if (dVar.L != animatedFraction) {
                dVar.e(animatedFraction);
            }
        }
    }

    /* loaded from: classes3.dex */
    public class b extends i {
        public final /* synthetic */ View a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ d f3071b;
        public final /* synthetic */ View c;
        public final /* synthetic */ View d;

        public b(View view, d dVar, View view2, View view3) {
            this.a = view;
            this.f3071b = dVar;
            this.c = view2;
            this.d = view3;
        }

        @Override // b.i.a.g.l.l.i, android.transition.Transition.TransitionListener
        public void onTransitionEnd(@NonNull Transition transition) {
            MaterialContainerTransform.this.removeListener(this);
            if (!MaterialContainerTransform.this.holdAtEndEnabled) {
                this.c.setAlpha(1.0f);
                this.d.setAlpha(1.0f);
                ViewUtils.getOverlay(this.a).remove(this.f3071b);
            }
        }

        @Override // b.i.a.g.l.l.i, android.transition.Transition.TransitionListener
        public void onTransitionStart(@NonNull Transition transition) {
            ViewUtils.getOverlay(this.a).add(this.f3071b);
            this.c.setAlpha(0.0f);
            this.d.setAlpha(0.0f);
        }
    }

    /* loaded from: classes3.dex */
    public static class c {
        @NonNull
        public final ProgressThresholds a;
        @NonNull

        /* renamed from: b  reason: collision with root package name */
        public final ProgressThresholds f3072b;
        @NonNull
        public final ProgressThresholds c;
        @NonNull
        public final ProgressThresholds d;

        public c(ProgressThresholds progressThresholds, ProgressThresholds progressThresholds2, ProgressThresholds progressThresholds3, ProgressThresholds progressThresholds4, a aVar) {
            this.a = progressThresholds;
            this.f3072b = progressThresholds2;
            this.c = progressThresholds3;
            this.d = progressThresholds4;
        }
    }

    /* loaded from: classes3.dex */
    public static final class d extends Drawable {
        public final c A;
        public final b.i.a.g.l.l.a B;
        public final b.i.a.g.l.l.d C;
        public final boolean D;
        public final Paint E;
        public b.i.a.g.l.l.c G;
        public f H;
        public RectF I;
        public float J;
        public float K;
        public float L;
        public final View a;

        /* renamed from: b  reason: collision with root package name */
        public final RectF f3073b;
        public final ShapeAppearanceModel c;
        public final float d;
        public final View e;
        public final RectF f;
        public final ShapeAppearanceModel g;
        public final float h;
        public final Paint i;
        public final Paint j;
        public final Paint k;
        public final Paint m;
        public final PathMeasure o;
        public final float p;
        public final float[] q;
        public final boolean r;

        /* renamed from: s  reason: collision with root package name */
        public final float f3074s;
        public final float t;
        public final boolean u;
        public final MaterialShapeDrawable v;
        public final RectF w;

        /* renamed from: x  reason: collision with root package name */
        public final RectF f3075x;

        /* renamed from: y  reason: collision with root package name */
        public final RectF f3076y;

        /* renamed from: z  reason: collision with root package name */
        public final RectF f3077z;
        public final Paint l = new Paint();
        public final g n = new g();
        public final Path F = new Path();

        /* loaded from: classes3.dex */
        public class a implements k.a {
            public a() {
            }

            @Override // b.i.a.g.l.l.k.a
            public void a(Canvas canvas) {
                d.this.a.draw(canvas);
            }
        }

        /* loaded from: classes3.dex */
        public class b implements k.a {
            public b() {
            }

            @Override // b.i.a.g.l.l.k.a
            public void a(Canvas canvas) {
                d.this.e.draw(canvas);
            }
        }

        public d(PathMotion pathMotion, View view, RectF rectF, ShapeAppearanceModel shapeAppearanceModel, float f, View view2, RectF rectF2, ShapeAppearanceModel shapeAppearanceModel2, float f2, int i, int i2, int i3, int i4, boolean z2, boolean z3, b.i.a.g.l.l.a aVar, b.i.a.g.l.l.d dVar, c cVar, boolean z4, a aVar2) {
            Paint paint = new Paint();
            this.i = paint;
            Paint paint2 = new Paint();
            this.j = paint2;
            Paint paint3 = new Paint();
            this.k = paint3;
            Paint paint4 = new Paint();
            this.m = paint4;
            this.q = r7;
            MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable();
            this.v = materialShapeDrawable;
            Paint paint5 = new Paint();
            this.E = paint5;
            this.a = view;
            this.f3073b = rectF;
            this.c = shapeAppearanceModel;
            this.d = f;
            this.e = view2;
            this.f = rectF2;
            this.g = shapeAppearanceModel2;
            this.h = f2;
            this.r = z2;
            this.u = z3;
            this.B = aVar;
            this.C = dVar;
            this.A = cVar;
            this.D = z4;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            this.f3074s = displayMetrics.widthPixels;
            this.t = displayMetrics.heightPixels;
            paint.setColor(i);
            paint2.setColor(i2);
            paint3.setColor(i3);
            materialShapeDrawable.setFillColor(ColorStateList.valueOf(0));
            materialShapeDrawable.setShadowCompatibilityMode(2);
            materialShapeDrawable.setShadowBitmapDrawingEnable(false);
            materialShapeDrawable.setShadowColor(-7829368);
            RectF rectF3 = new RectF(rectF);
            this.w = rectF3;
            this.f3075x = new RectF(rectF3);
            RectF rectF4 = new RectF(rectF3);
            this.f3076y = rectF4;
            this.f3077z = new RectF(rectF4);
            PointF c = c(rectF);
            PointF c2 = c(rectF2);
            PathMeasure pathMeasure = new PathMeasure(pathMotion.getPath(c.x, c.y, c2.x, c2.y), false);
            this.o = pathMeasure;
            this.p = pathMeasure.getLength();
            float[] fArr = {rectF.centerX(), rectF.top};
            paint4.setStyle(Paint.Style.FILL);
            RectF rectF5 = k.a;
            paint4.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, 0.0f, i4, i4, Shader.TileMode.CLAMP));
            paint5.setStyle(Paint.Style.STROKE);
            paint5.setStrokeWidth(10.0f);
            e(0.0f);
        }

        public static PointF c(RectF rectF) {
            return new PointF(rectF.centerX(), rectF.top);
        }

        public final void a(Canvas canvas) {
            d(canvas, this.k);
            Rect bounds = getBounds();
            RectF rectF = this.f3076y;
            k.h(canvas, bounds, rectF.left, rectF.top, this.H.f1635b, this.G.f1633b, new b());
        }

        public final void b(Canvas canvas) {
            d(canvas, this.j);
            Rect bounds = getBounds();
            RectF rectF = this.w;
            k.h(canvas, bounds, rectF.left, rectF.top, this.H.a, this.G.a, new a());
        }

        public final void d(Canvas canvas, Paint paint) {
            if (paint.getColor() != 0 && paint.getAlpha() > 0) {
                canvas.drawRect(getBounds(), paint);
            }
        }

        @Override // android.graphics.drawable.Drawable
        public void draw(@NonNull Canvas canvas) {
            if (this.m.getAlpha() > 0) {
                canvas.drawRect(getBounds(), this.m);
            }
            int save = this.D ? canvas.save() : -1;
            if (this.u && this.J > 0.0f) {
                canvas.save();
                canvas.clipPath(this.n.a, Region.Op.DIFFERENCE);
                if (Build.VERSION.SDK_INT > 28) {
                    ShapeAppearanceModel shapeAppearanceModel = this.n.e;
                    if (shapeAppearanceModel.isRoundRect(this.I)) {
                        float cornerSize = shapeAppearanceModel.getTopLeftCornerSize().getCornerSize(this.I);
                        canvas.drawRoundRect(this.I, cornerSize, cornerSize, this.l);
                    } else {
                        canvas.drawPath(this.n.a, this.l);
                    }
                } else {
                    MaterialShapeDrawable materialShapeDrawable = this.v;
                    RectF rectF = this.I;
                    materialShapeDrawable.setBounds((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
                    this.v.setElevation(this.J);
                    this.v.setShadowVerticalOffset((int) this.K);
                    this.v.setShapeAppearanceModel(this.n.e);
                    this.v.draw(canvas);
                }
                canvas.restore();
            }
            g gVar = this.n;
            if (Build.VERSION.SDK_INT >= 23) {
                canvas.clipPath(gVar.a);
            } else {
                canvas.clipPath(gVar.f1636b);
                canvas.clipPath(gVar.c, Region.Op.UNION);
            }
            d(canvas, this.i);
            if (this.G.c) {
                b(canvas);
                a(canvas);
            } else {
                a(canvas);
                b(canvas);
            }
            if (this.D) {
                canvas.restoreToCount(save);
                RectF rectF2 = this.w;
                Path path = this.F;
                PointF c = c(rectF2);
                if (this.L == 0.0f) {
                    path.reset();
                    path.moveTo(c.x, c.y);
                } else {
                    path.lineTo(c.x, c.y);
                    this.E.setColor(-65281);
                    canvas.drawPath(path, this.E);
                }
                RectF rectF3 = this.f3075x;
                this.E.setColor(InputDeviceCompat.SOURCE_ANY);
                canvas.drawRect(rectF3, this.E);
                RectF rectF4 = this.w;
                this.E.setColor(-16711936);
                canvas.drawRect(rectF4, this.E);
                RectF rectF5 = this.f3077z;
                this.E.setColor(-16711681);
                canvas.drawRect(rectF5, this.E);
                RectF rectF6 = this.f3076y;
                this.E.setColor(-16776961);
                canvas.drawRect(rectF6, this.E);
            }
        }

        public final void e(float f) {
            float f2;
            float f3;
            RectF rectF;
            this.L = f;
            this.m.setAlpha((int) (this.r ? k.d(0.0f, 255.0f, f) : k.d(255.0f, 0.0f, f)));
            this.o.getPosTan(this.p * f, this.q, null);
            float[] fArr = this.q;
            boolean z2 = false;
            float f4 = fArr[0];
            float f5 = fArr[1];
            int i = (f > 1.0f ? 1 : (f == 1.0f ? 0 : -1));
            if (i > 0 || f < 0.0f) {
                if (i > 0) {
                    f3 = 0.99f;
                    f2 = (f - 1.0f) / 0.00999999f;
                } else {
                    f3 = 0.01f;
                    f2 = (f / 0.01f) * (-1.0f);
                }
                this.o.getPosTan(this.p * f3, fArr, null);
                float[] fArr2 = this.q;
                float f6 = fArr2[0];
                float f7 = fArr2[1];
                f4 = b.d.b.a.a.a(f4, f6, f2, f4);
                f5 = b.d.b.a.a.a(f5, f7, f2, f5);
            }
            float f8 = f4;
            float f9 = f5;
            f a2 = this.C.a(f, ((Float) Preconditions.checkNotNull(Float.valueOf(this.A.f3072b.start))).floatValue(), ((Float) Preconditions.checkNotNull(Float.valueOf(this.A.f3072b.end))).floatValue(), this.f3073b.width(), this.f3073b.height(), this.f.width(), this.f.height());
            this.H = a2;
            RectF rectF2 = this.w;
            float f10 = a2.c / 2.0f;
            rectF2.set(f8 - f10, f9, f10 + f8, a2.d + f9);
            RectF rectF3 = this.f3076y;
            f fVar = this.H;
            float f11 = fVar.e / 2.0f;
            rectF3.set(f8 - f11, f9, f11 + f8, fVar.f + f9);
            this.f3075x.set(this.w);
            this.f3077z.set(this.f3076y);
            float floatValue = ((Float) Preconditions.checkNotNull(Float.valueOf(this.A.c.start))).floatValue();
            float floatValue2 = ((Float) Preconditions.checkNotNull(Float.valueOf(this.A.c.end))).floatValue();
            boolean b2 = this.C.b(this.H);
            if (b2) {
                rectF = this.f3075x;
            } else {
                rectF = this.f3077z;
            }
            float e = k.e(0.0f, 1.0f, floatValue, floatValue2, f);
            if (!b2) {
                e = 1.0f - e;
            }
            this.C.c(rectF, e, this.H);
            this.I = new RectF(Math.min(this.f3075x.left, this.f3077z.left), Math.min(this.f3075x.top, this.f3077z.top), Math.max(this.f3075x.right, this.f3077z.right), Math.max(this.f3075x.bottom, this.f3077z.bottom));
            g gVar = this.n;
            ShapeAppearanceModel shapeAppearanceModel = this.c;
            ShapeAppearanceModel shapeAppearanceModel2 = this.g;
            RectF rectF4 = this.w;
            RectF rectF5 = this.f3075x;
            RectF rectF6 = this.f3077z;
            ProgressThresholds progressThresholds = this.A.d;
            Objects.requireNonNull(gVar);
            float start = progressThresholds.getStart();
            float end = progressThresholds.getEnd();
            RectF rectF7 = k.a;
            if (f >= start) {
                if (f > end) {
                    shapeAppearanceModel = shapeAppearanceModel2;
                } else {
                    if (!(shapeAppearanceModel.getTopLeftCornerSize().getCornerSize(rectF4) == 0.0f && shapeAppearanceModel.getTopRightCornerSize().getCornerSize(rectF4) == 0.0f && shapeAppearanceModel.getBottomRightCornerSize().getCornerSize(rectF4) == 0.0f && shapeAppearanceModel.getBottomLeftCornerSize().getCornerSize(rectF4) == 0.0f)) {
                        z2 = true;
                    }
                    shapeAppearanceModel = (z2 ? shapeAppearanceModel : shapeAppearanceModel2).toBuilder().setTopLeftCornerSize(new AbsoluteCornerSize(k.e(shapeAppearanceModel.getTopLeftCornerSize().getCornerSize(rectF4), shapeAppearanceModel2.getTopLeftCornerSize().getCornerSize(rectF6), start, end, f))).setTopRightCornerSize(new AbsoluteCornerSize(k.e(shapeAppearanceModel.getTopRightCornerSize().getCornerSize(rectF4), shapeAppearanceModel2.getTopRightCornerSize().getCornerSize(rectF6), start, end, f))).setBottomLeftCornerSize(new AbsoluteCornerSize(k.e(shapeAppearanceModel.getBottomLeftCornerSize().getCornerSize(rectF4), shapeAppearanceModel2.getBottomLeftCornerSize().getCornerSize(rectF6), start, end, f))).setBottomRightCornerSize(new AbsoluteCornerSize(k.e(shapeAppearanceModel.getBottomRightCornerSize().getCornerSize(rectF4), shapeAppearanceModel2.getBottomRightCornerSize().getCornerSize(rectF6), start, end, f))).build();
                }
            }
            gVar.e = shapeAppearanceModel;
            gVar.d.calculatePath(shapeAppearanceModel, 1.0f, rectF5, gVar.f1636b);
            gVar.d.calculatePath(gVar.e, 1.0f, rectF6, gVar.c);
            if (Build.VERSION.SDK_INT >= 23) {
                gVar.a.op(gVar.f1636b, gVar.c, Path.Op.UNION);
            }
            this.J = k.d(this.d, this.h, f);
            RectF rectF8 = this.I;
            float f12 = this.f3074s;
            RectF rectF9 = this.I;
            float f13 = this.t;
            float f14 = this.J;
            float centerY = (int) ((rectF9.centerY() / f13) * 1.5f * f14);
            this.K = centerY;
            this.l.setShadowLayer(f14, (int) (((rectF8.centerX() / (f12 / 2.0f)) - 1.0f) * 0.3f * f14), centerY, 754974720);
            this.G = this.B.a(f, ((Float) Preconditions.checkNotNull(Float.valueOf(this.A.a.start))).floatValue(), ((Float) Preconditions.checkNotNull(Float.valueOf(this.A.a.end))).floatValue());
            if (this.j.getColor() != 0) {
                this.j.setAlpha(this.G.a);
            }
            if (this.k.getColor() != 0) {
                this.k.setAlpha(this.G.f1633b);
            }
            invalidateSelf();
        }

        @Override // android.graphics.drawable.Drawable
        public int getOpacity() {
            return -3;
        }

        @Override // android.graphics.drawable.Drawable
        public void setAlpha(int i) {
            throw new UnsupportedOperationException("Setting alpha on is not supported");
        }

        @Override // android.graphics.drawable.Drawable
        public void setColorFilter(@Nullable ColorFilter colorFilter) {
            throw new UnsupportedOperationException("Setting a color filter is not supported");
        }
    }

    public MaterialContainerTransform() {
        boolean z2 = false;
        this.elevationShadowEnabled = Build.VERSION.SDK_INT >= 28 ? true : z2;
        this.startElevation = -1.0f;
        this.endElevation = -1.0f;
        setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
    }

    private c buildThresholdsGroup(boolean z2) {
        PathMotion pathMotion = getPathMotion();
        if ((pathMotion instanceof ArcMotion) || (pathMotion instanceof MaterialArcMotion)) {
            return getThresholdsOrDefault(z2, DEFAULT_ENTER_THRESHOLDS_ARC, DEFAULT_RETURN_THRESHOLDS_ARC);
        }
        return getThresholdsOrDefault(z2, DEFAULT_ENTER_THRESHOLDS, DEFAULT_RETURN_THRESHOLDS);
    }

    private static RectF calculateDrawableBounds(View view, @Nullable View view2, float f, float f2) {
        if (view2 == null) {
            return new RectF(0.0f, 0.0f, view.getWidth(), view.getHeight());
        }
        RectF c2 = k.c(view2);
        c2.offset(f, f2);
        return c2;
    }

    private static ShapeAppearanceModel captureShapeAppearance(@NonNull View view, @NonNull RectF rectF, @Nullable ShapeAppearanceModel shapeAppearanceModel) {
        ShapeAppearanceModel shapeAppearance = getShapeAppearance(view, shapeAppearanceModel);
        RectF rectF2 = k.a;
        return shapeAppearance.withTransformedCornerSizes(new j(rectF));
    }

    private static void captureValues(@NonNull TransitionValues transitionValues, @Nullable View view, @IdRes int i, @Nullable ShapeAppearanceModel shapeAppearanceModel) {
        RectF rectF;
        if (i != -1) {
            View view2 = transitionValues.view;
            RectF rectF2 = k.a;
            View findViewById = view2.findViewById(i);
            if (findViewById == null) {
                findViewById = k.b(view2, i);
            }
            transitionValues.view = findViewById;
        } else if (view != null) {
            transitionValues.view = view;
        } else {
            View view3 = transitionValues.view;
            int i2 = R.id.mtrl_motion_snapshot_view;
            if (view3.getTag(i2) instanceof View) {
                transitionValues.view.setTag(i2, null);
                transitionValues.view = (View) transitionValues.view.getTag(i2);
            }
        }
        View view4 = transitionValues.view;
        if (ViewCompat.isLaidOut(view4) || view4.getWidth() != 0 || view4.getHeight() != 0) {
            if (view4.getParent() == null) {
                RectF rectF3 = k.a;
                rectF = new RectF(view4.getLeft(), view4.getTop(), view4.getRight(), view4.getBottom());
            } else {
                rectF = k.c(view4);
            }
            transitionValues.values.put(PROP_BOUNDS, rectF);
            transitionValues.values.put(PROP_SHAPE_APPEARANCE, captureShapeAppearance(view4, rectF, shapeAppearanceModel));
        }
    }

    private static float getElevationOrDefault(float f, View view) {
        return f != -1.0f ? f : ViewCompat.getElevation(view);
    }

    private static ShapeAppearanceModel getShapeAppearance(@NonNull View view, @Nullable ShapeAppearanceModel shapeAppearanceModel) {
        if (shapeAppearanceModel != null) {
            return shapeAppearanceModel;
        }
        int i = R.id.mtrl_motion_snapshot_view;
        if (view.getTag(i) instanceof ShapeAppearanceModel) {
            return (ShapeAppearanceModel) view.getTag(i);
        }
        Context context = view.getContext();
        int transitionShapeAppearanceResId = getTransitionShapeAppearanceResId(context);
        if (transitionShapeAppearanceResId != -1) {
            return ShapeAppearanceModel.builder(context, transitionShapeAppearanceResId, 0).build();
        }
        if (view instanceof Shapeable) {
            return ((Shapeable) view).getShapeAppearanceModel();
        }
        return ShapeAppearanceModel.builder().build();
    }

    private c getThresholdsOrDefault(boolean z2, c cVar, c cVar2) {
        if (!z2) {
            cVar = cVar2;
        }
        return new c((ProgressThresholds) k.a(this.fadeProgressThresholds, cVar.a), (ProgressThresholds) k.a(this.scaleProgressThresholds, cVar.f3072b), (ProgressThresholds) k.a(this.scaleMaskProgressThresholds, cVar.c), (ProgressThresholds) k.a(this.shapeMaskProgressThresholds, cVar.d), null);
    }

    @StyleRes
    private static int getTransitionShapeAppearanceResId(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[]{R.attr.transitionShapeAppearance});
        int resourceId = obtainStyledAttributes.getResourceId(0, -1);
        obtainStyledAttributes.recycle();
        return resourceId;
    }

    private boolean isEntering(@NonNull RectF rectF, @NonNull RectF rectF2) {
        int i = this.transitionDirection;
        if (i == 0) {
            RectF rectF3 = k.a;
            return rectF2.height() * rectF2.width() > rectF.height() * rectF.width();
        } else if (i == 1) {
            return true;
        } else {
            if (i == 2) {
                return false;
            }
            StringBuilder R = b.d.b.a.a.R("Invalid transition direction: ");
            R.append(this.transitionDirection);
            throw new IllegalArgumentException(R.toString());
        }
    }

    @Override // android.transition.Transition
    public void captureEndValues(@NonNull TransitionValues transitionValues) {
        captureValues(transitionValues, this.endView, this.endViewId, this.endShapeAppearanceModel);
    }

    @Override // android.transition.Transition
    public void captureStartValues(@NonNull TransitionValues transitionValues) {
        captureValues(transitionValues, this.startView, this.startViewId, this.startShapeAppearanceModel);
    }

    @Override // android.transition.Transition
    @Nullable
    public Animator createAnimator(@NonNull ViewGroup viewGroup, @Nullable TransitionValues transitionValues, @Nullable TransitionValues transitionValues2) {
        View view;
        b.i.a.g.l.l.a aVar;
        b.i.a.g.l.l.d dVar;
        if (!(transitionValues == null || transitionValues2 == null)) {
            RectF rectF = (RectF) transitionValues.values.get(PROP_BOUNDS);
            ShapeAppearanceModel shapeAppearanceModel = (ShapeAppearanceModel) transitionValues.values.get(PROP_SHAPE_APPEARANCE);
            if (rectF == null || shapeAppearanceModel == null) {
                Log.w(TAG, "Skipping due to null start bounds. Ensure start view is laid out and measured.");
            } else {
                RectF rectF2 = (RectF) transitionValues2.values.get(PROP_BOUNDS);
                ShapeAppearanceModel shapeAppearanceModel2 = (ShapeAppearanceModel) transitionValues2.values.get(PROP_SHAPE_APPEARANCE);
                if (rectF2 == null || shapeAppearanceModel2 == null) {
                    Log.w(TAG, "Skipping due to null end bounds. Ensure end view is laid out and measured.");
                    return null;
                }
                View view2 = transitionValues.view;
                View view3 = transitionValues2.view;
                View view4 = view3.getParent() != null ? view3 : view2;
                if (this.drawingViewId == view4.getId()) {
                    view = (View) view4.getParent();
                } else {
                    view = k.b(view4, this.drawingViewId);
                    view4 = null;
                }
                RectF c2 = k.c(view);
                float f = -c2.left;
                float f2 = -c2.top;
                RectF calculateDrawableBounds = calculateDrawableBounds(view, view4, f, f2);
                rectF.offset(f, f2);
                rectF2.offset(f, f2);
                boolean isEntering = isEntering(rectF, rectF2);
                PathMotion pathMotion = getPathMotion();
                float elevationOrDefault = getElevationOrDefault(this.startElevation, view2);
                float elevationOrDefault2 = getElevationOrDefault(this.endElevation, view3);
                int i = this.containerColor;
                int i2 = this.startContainerColor;
                int i3 = this.endContainerColor;
                View view5 = view;
                int i4 = this.scrimColor;
                boolean z2 = this.elevationShadowEnabled;
                int i5 = this.fadeMode;
                boolean z3 = true;
                if (i5 == 0) {
                    aVar = isEntering ? b.i.a.g.l.l.b.a : b.i.a.g.l.l.b.f1632b;
                } else if (i5 == 1) {
                    aVar = isEntering ? b.i.a.g.l.l.b.f1632b : b.i.a.g.l.l.b.a;
                } else if (i5 == 2) {
                    aVar = b.i.a.g.l.l.b.c;
                } else if (i5 == 3) {
                    aVar = b.i.a.g.l.l.b.d;
                } else {
                    throw new IllegalArgumentException(b.d.b.a.a.p("Invalid fade mode: ", i5));
                }
                b.i.a.g.l.l.a aVar2 = aVar;
                int i6 = this.fitMode;
                if (i6 == 0) {
                    float width = rectF.width();
                    float height = rectF.height();
                    float width2 = rectF2.width();
                    float height2 = rectF2.height();
                    float f3 = (height2 * width) / width2;
                    float f4 = (width2 * height) / width;
                    if (!isEntering ? f4 < height2 : f3 < height) {
                        z3 = false;
                    }
                    dVar = z3 ? e.a : e.f1634b;
                } else if (i6 == 1) {
                    dVar = e.a;
                } else if (i6 == 2) {
                    dVar = e.f1634b;
                } else {
                    throw new IllegalArgumentException(b.d.b.a.a.p("Invalid fit mode: ", i6));
                }
                d dVar2 = new d(pathMotion, view2, rectF, shapeAppearanceModel, elevationOrDefault, view3, rectF2, shapeAppearanceModel2, elevationOrDefault2, i, i2, i3, i4, isEntering, z2, aVar2, dVar, buildThresholdsGroup(isEntering), this.drawDebugEnabled, null);
                dVar2.setBounds(Math.round(calculateDrawableBounds.left), Math.round(calculateDrawableBounds.top), Math.round(calculateDrawableBounds.right), Math.round(calculateDrawableBounds.bottom));
                ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                ofFloat.addUpdateListener(new a(this, dVar2));
                addListener(new b(view5, dVar2, view2, view3));
                return ofFloat;
            }
        }
        return null;
    }

    @ColorInt
    public int getContainerColor() {
        return this.containerColor;
    }

    @IdRes
    public int getDrawingViewId() {
        return this.drawingViewId;
    }

    @ColorInt
    public int getEndContainerColor() {
        return this.endContainerColor;
    }

    public float getEndElevation() {
        return this.endElevation;
    }

    @Nullable
    public ShapeAppearanceModel getEndShapeAppearanceModel() {
        return this.endShapeAppearanceModel;
    }

    @Nullable
    public View getEndView() {
        return this.endView;
    }

    @IdRes
    public int getEndViewId() {
        return this.endViewId;
    }

    public int getFadeMode() {
        return this.fadeMode;
    }

    @Nullable
    public ProgressThresholds getFadeProgressThresholds() {
        return this.fadeProgressThresholds;
    }

    public int getFitMode() {
        return this.fitMode;
    }

    @Nullable
    public ProgressThresholds getScaleMaskProgressThresholds() {
        return this.scaleMaskProgressThresholds;
    }

    @Nullable
    public ProgressThresholds getScaleProgressThresholds() {
        return this.scaleProgressThresholds;
    }

    @ColorInt
    public int getScrimColor() {
        return this.scrimColor;
    }

    @Nullable
    public ProgressThresholds getShapeMaskProgressThresholds() {
        return this.shapeMaskProgressThresholds;
    }

    @ColorInt
    public int getStartContainerColor() {
        return this.startContainerColor;
    }

    public float getStartElevation() {
        return this.startElevation;
    }

    @Nullable
    public ShapeAppearanceModel getStartShapeAppearanceModel() {
        return this.startShapeAppearanceModel;
    }

    @Nullable
    public View getStartView() {
        return this.startView;
    }

    @IdRes
    public int getStartViewId() {
        return this.startViewId;
    }

    public int getTransitionDirection() {
        return this.transitionDirection;
    }

    @Override // android.transition.Transition
    @Nullable
    public String[] getTransitionProperties() {
        return TRANSITION_PROPS;
    }

    public boolean isDrawDebugEnabled() {
        return this.drawDebugEnabled;
    }

    public boolean isElevationShadowEnabled() {
        return this.elevationShadowEnabled;
    }

    public boolean isHoldAtEndEnabled() {
        return this.holdAtEndEnabled;
    }

    public void setAllContainerColors(@ColorInt int i) {
        this.containerColor = i;
        this.startContainerColor = i;
        this.endContainerColor = i;
    }

    public void setContainerColor(@ColorInt int i) {
        this.containerColor = i;
    }

    public void setDrawDebugEnabled(boolean z2) {
        this.drawDebugEnabled = z2;
    }

    public void setDrawingViewId(@IdRes int i) {
        this.drawingViewId = i;
    }

    public void setElevationShadowEnabled(boolean z2) {
        this.elevationShadowEnabled = z2;
    }

    public void setEndContainerColor(@ColorInt int i) {
        this.endContainerColor = i;
    }

    public void setEndElevation(float f) {
        this.endElevation = f;
    }

    public void setEndShapeAppearanceModel(@Nullable ShapeAppearanceModel shapeAppearanceModel) {
        this.endShapeAppearanceModel = shapeAppearanceModel;
    }

    public void setEndView(@Nullable View view) {
        this.endView = view;
    }

    public void setEndViewId(@IdRes int i) {
        this.endViewId = i;
    }

    public void setFadeMode(int i) {
        this.fadeMode = i;
    }

    public void setFadeProgressThresholds(@Nullable ProgressThresholds progressThresholds) {
        this.fadeProgressThresholds = progressThresholds;
    }

    public void setFitMode(int i) {
        this.fitMode = i;
    }

    public void setHoldAtEndEnabled(boolean z2) {
        this.holdAtEndEnabled = z2;
    }

    public void setScaleMaskProgressThresholds(@Nullable ProgressThresholds progressThresholds) {
        this.scaleMaskProgressThresholds = progressThresholds;
    }

    public void setScaleProgressThresholds(@Nullable ProgressThresholds progressThresholds) {
        this.scaleProgressThresholds = progressThresholds;
    }

    public void setScrimColor(@ColorInt int i) {
        this.scrimColor = i;
    }

    public void setShapeMaskProgressThresholds(@Nullable ProgressThresholds progressThresholds) {
        this.shapeMaskProgressThresholds = progressThresholds;
    }

    public void setStartContainerColor(@ColorInt int i) {
        this.startContainerColor = i;
    }

    public void setStartElevation(float f) {
        this.startElevation = f;
    }

    public void setStartShapeAppearanceModel(@Nullable ShapeAppearanceModel shapeAppearanceModel) {
        this.startShapeAppearanceModel = shapeAppearanceModel;
    }

    public void setStartView(@Nullable View view) {
        this.startView = view;
    }

    public void setStartViewId(@IdRes int i) {
        this.startViewId = i;
    }

    public void setTransitionDirection(int i) {
        this.transitionDirection = i;
    }
}
