package com.google.android.material.datepicker;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.g.d.l;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
/* loaded from: classes3.dex */
public final class Month implements Comparable<Month>, Parcelable {
    public static final Parcelable.Creator<Month> CREATOR = new a();
    @NonNull
    public final Calendar j;
    public final int k;
    public final int l;
    public final int m;
    public final int n;
    public final long o;
    @Nullable
    public String p;

    /* loaded from: classes3.dex */
    public static class a implements Parcelable.Creator<Month> {
        @Override // android.os.Parcelable.Creator
        @NonNull
        public Month createFromParcel(@NonNull Parcel parcel) {
            return Month.g(parcel.readInt(), parcel.readInt());
        }

        @Override // android.os.Parcelable.Creator
        @NonNull
        public Month[] newArray(int i) {
            return new Month[i];
        }
    }

    public Month(@NonNull Calendar calendar) {
        calendar.set(5, 1);
        Calendar d = l.d(calendar);
        this.j = d;
        this.k = d.get(2);
        this.l = d.get(1);
        this.m = d.getMaximum(7);
        this.n = d.getActualMaximum(5);
        this.o = d.getTimeInMillis();
    }

    @NonNull
    public static Month g(int i, int i2) {
        Calendar i3 = l.i();
        i3.set(1, i);
        i3.set(2, i2);
        return new Month(i3);
    }

    @NonNull
    public static Month h(long j) {
        Calendar i = l.i();
        i.setTimeInMillis(j);
        return new Month(i);
    }

    @NonNull
    public static Month i() {
        return new Month(l.h());
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Month)) {
            return false;
        }
        Month month = (Month) obj;
        return this.k == month.k && this.l == month.l;
    }

    /* renamed from: f */
    public int compareTo(@NonNull Month month) {
        return this.j.compareTo(month.j);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.k), Integer.valueOf(this.l)});
    }

    public int j() {
        int firstDayOfWeek = this.j.get(7) - this.j.getFirstDayOfWeek();
        return firstDayOfWeek < 0 ? firstDayOfWeek + this.m : firstDayOfWeek;
    }

    public long k(int i) {
        Calendar d = l.d(this.j);
        d.set(5, i);
        return d.getTimeInMillis();
    }

    @NonNull
    public String l(Context context) {
        if (this.p == null) {
            long timeInMillis = this.j.getTimeInMillis();
            this.p = DateUtils.formatDateTime(context, timeInMillis - TimeZone.getDefault().getOffset(timeInMillis), 36);
        }
        return this.p;
    }

    @NonNull
    public Month m(int i) {
        Calendar d = l.d(this.j);
        d.add(2, i);
        return new Month(d);
    }

    public int o(@NonNull Month month) {
        if (this.j instanceof GregorianCalendar) {
            return (month.k - this.k) + ((month.l - this.l) * 12);
        }
        throw new IllegalArgumentException("Only Gregorian calendars are supported.");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeInt(this.l);
        parcel.writeInt(this.k);
    }
}
