package com.google.android.material.datepicker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.g.d.g;
import b.i.a.g.d.h;
import com.google.android.material.R;
import com.google.android.material.datepicker.MaterialCalendar;
/* loaded from: classes3.dex */
public class MonthsPagerAdapter extends RecyclerView.Adapter<ViewHolder> {
    public final Context a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final CalendarConstraints f3008b;
    public final DateSelector<?> c;
    public final MaterialCalendar.l d;
    public final int e;

    /* loaded from: classes3.dex */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final MaterialCalendarGridView monthGrid;
        public final TextView monthTitle;

        public ViewHolder(@NonNull LinearLayout linearLayout, boolean z2) {
            super(linearLayout);
            TextView textView = (TextView) linearLayout.findViewById(R.id.month_title);
            this.monthTitle = textView;
            ViewCompat.setAccessibilityHeading(textView, true);
            this.monthGrid = (MaterialCalendarGridView) linearLayout.findViewById(R.id.month_grid);
            if (!z2) {
                textView.setVisibility(8);
            }
        }
    }

    public MonthsPagerAdapter(@NonNull Context context, DateSelector<?> dateSelector, @NonNull CalendarConstraints calendarConstraints, MaterialCalendar.l lVar) {
        Month start = calendarConstraints.getStart();
        Month end = calendarConstraints.getEnd();
        Month openAt = calendarConstraints.getOpenAt();
        if (start.compareTo(openAt) > 0) {
            throw new IllegalArgumentException("firstPage cannot be after currentPage");
        } else if (openAt.compareTo(end) <= 0) {
            int dayHeight = MaterialCalendar.getDayHeight(context) * g.j;
            int dayHeight2 = MaterialDatePicker.isFullscreen(context) ? MaterialCalendar.getDayHeight(context) : 0;
            this.a = context;
            this.e = dayHeight + dayHeight2;
            this.f3008b = calendarConstraints;
            this.c = dateSelector;
            this.d = lVar;
            setHasStableIds(true);
        } else {
            throw new IllegalArgumentException("currentPage cannot be after lastPage");
        }
    }

    @NonNull
    public Month a(int i) {
        return this.f3008b.getStart().m(i);
    }

    public int b(@NonNull Month month) {
        return this.f3008b.getStart().o(month);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.f3008b.getMonthSpan();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return this.f3008b.getStart().m(i).j.getTimeInMillis();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ViewHolder viewHolder2 = viewHolder;
        Month m = this.f3008b.getStart().m(i);
        viewHolder2.monthTitle.setText(m.l(viewHolder2.itemView.getContext()));
        MaterialCalendarGridView materialCalendarGridView = (MaterialCalendarGridView) viewHolder2.monthGrid.findViewById(R.id.month_grid);
        if (materialCalendarGridView.getAdapter2() == null || !m.equals(materialCalendarGridView.getAdapter2().k)) {
            g gVar = new g(m, this.c, this.f3008b);
            materialCalendarGridView.setNumColumns(m.m);
            materialCalendarGridView.setAdapter((ListAdapter) gVar);
        } else {
            materialCalendarGridView.invalidate();
            g a = materialCalendarGridView.getAdapter2();
            for (Long l : a.m) {
                a.f(materialCalendarGridView, l.longValue());
            }
            DateSelector<?> dateSelector = a.l;
            if (dateSelector != null) {
                for (Long l2 : dateSelector.getSelectedDays()) {
                    a.f(materialCalendarGridView, l2.longValue());
                }
                a.m = a.l.getSelectedDays();
            }
        }
        materialCalendarGridView.setOnItemClickListener(new h(this, materialCalendarGridView));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mtrl_calendar_month_labeled, viewGroup, false);
        if (!MaterialDatePicker.isFullscreen(viewGroup.getContext())) {
            return new ViewHolder(linearLayout, false);
        }
        linearLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, this.e));
        return new ViewHolder(linearLayout, true);
    }
}
