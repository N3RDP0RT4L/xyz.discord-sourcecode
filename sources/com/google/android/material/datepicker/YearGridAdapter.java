package com.google.android.material.datepicker;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.g.d.a;
import b.i.a.g.d.b;
import b.i.a.g.d.l;
import b.i.a.g.d.m;
import com.google.android.material.R;
import java.util.Calendar;
import java.util.Locale;
/* loaded from: classes3.dex */
public class YearGridAdapter extends RecyclerView.Adapter<ViewHolder> {
    public final MaterialCalendar<?> a;

    /* loaded from: classes3.dex */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView textView;

        public ViewHolder(TextView textView) {
            super(textView);
            this.textView = textView;
        }
    }

    public YearGridAdapter(MaterialCalendar<?> materialCalendar) {
        this.a = materialCalendar;
    }

    public int a(int i) {
        return i - this.a.getCalendarConstraints().getStart().l;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.a.getCalendarConstraints().getYearSpan();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ViewHolder viewHolder2 = viewHolder;
        int i2 = this.a.getCalendarConstraints().getStart().l + i;
        String string = viewHolder2.textView.getContext().getString(R.string.mtrl_picker_navigate_to_year_description);
        viewHolder2.textView.setText(String.format(Locale.getDefault(), "%d", Integer.valueOf(i2)));
        viewHolder2.textView.setContentDescription(String.format(string, Integer.valueOf(i2)));
        b calendarStyle = this.a.getCalendarStyle();
        Calendar h = l.h();
        a aVar = h.get(1) == i2 ? calendarStyle.f : calendarStyle.d;
        for (Long l : this.a.getDateSelector().getSelectedDays()) {
            h.setTimeInMillis(l.longValue());
            if (h.get(1) == i2) {
                aVar = calendarStyle.e;
            }
        }
        aVar.b(viewHolder2.textView);
        viewHolder2.textView.setOnClickListener(new m(this, i2));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mtrl_calendar_year, viewGroup, false));
    }
}
