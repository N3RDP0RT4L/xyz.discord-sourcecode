package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Keep;
import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNonNull;
import b.i.a.f.e.h.j.i;
import b.i.a.f.e.h.j.j;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public class LifecycleCallback {
    @RecentlyNonNull
    public final j j;

    public LifecycleCallback(@RecentlyNonNull j jVar) {
        this.j = jVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x002f, code lost:
        if (r3 != false) goto L12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0069, code lost:
        if (r2 == null) goto L31;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x007b, code lost:
        if (r3 != false) goto L25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x001d, code lost:
        if (r2 == null) goto L33;
     */
    @androidx.annotation.RecentlyNonNull
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static b.i.a.f.e.h.j.j c(@androidx.annotation.RecentlyNonNull android.app.Activity r4) {
        /*
            java.lang.String r0 = "Activity must not be null"
            b.c.a.a0.d.z(r4, r0)
            boolean r0 = r4 instanceof androidx.fragment.app.FragmentActivity
            if (r0 == 0) goto L57
            androidx.fragment.app.FragmentActivity r4 = (androidx.fragment.app.FragmentActivity) r4
            java.lang.String r0 = "SupportLifecycleFragmentImpl"
            java.util.WeakHashMap<androidx.fragment.app.FragmentActivity, java.lang.ref.WeakReference<b.i.a.f.e.h.j.f1>> r1 = b.i.a.f.e.h.j.f1.j
            java.lang.Object r2 = r1.get(r4)
            java.lang.ref.WeakReference r2 = (java.lang.ref.WeakReference) r2
            if (r2 == 0) goto L1f
            java.lang.Object r2 = r2.get()
            b.i.a.f.e.h.j.f1 r2 = (b.i.a.f.e.h.j.f1) r2
            if (r2 != 0) goto L99
        L1f:
            androidx.fragment.app.FragmentManager r2 = r4.getSupportFragmentManager()     // Catch: java.lang.ClassCastException -> L4e
            androidx.fragment.app.Fragment r2 = r2.findFragmentByTag(r0)     // Catch: java.lang.ClassCastException -> L4e
            b.i.a.f.e.h.j.f1 r2 = (b.i.a.f.e.h.j.f1) r2     // Catch: java.lang.ClassCastException -> L4e
            if (r2 == 0) goto L31
            boolean r3 = r2.isRemoving()
            if (r3 == 0) goto L45
        L31:
            b.i.a.f.e.h.j.f1 r2 = new b.i.a.f.e.h.j.f1
            r2.<init>()
            androidx.fragment.app.FragmentManager r3 = r4.getSupportFragmentManager()
            androidx.fragment.app.FragmentTransaction r3 = r3.beginTransaction()
            androidx.fragment.app.FragmentTransaction r0 = r3.add(r2, r0)
            r0.commitAllowingStateLoss()
        L45:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r2)
            r1.put(r4, r0)
            goto L99
        L4e:
            r4 = move-exception
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl"
            r0.<init>(r1, r4)
            throw r0
        L57:
            java.lang.String r0 = "LifecycleFragmentImpl"
            java.util.WeakHashMap<android.app.Activity, java.lang.ref.WeakReference<b.i.a.f.e.h.j.d1>> r1 = b.i.a.f.e.h.j.d1.j
            java.lang.Object r2 = r1.get(r4)
            java.lang.ref.WeakReference r2 = (java.lang.ref.WeakReference) r2
            if (r2 == 0) goto L6b
            java.lang.Object r2 = r2.get()
            b.i.a.f.e.h.j.d1 r2 = (b.i.a.f.e.h.j.d1) r2
            if (r2 != 0) goto L99
        L6b:
            android.app.FragmentManager r2 = r4.getFragmentManager()     // Catch: java.lang.ClassCastException -> L9a
            android.app.Fragment r2 = r2.findFragmentByTag(r0)     // Catch: java.lang.ClassCastException -> L9a
            b.i.a.f.e.h.j.d1 r2 = (b.i.a.f.e.h.j.d1) r2     // Catch: java.lang.ClassCastException -> L9a
            if (r2 == 0) goto L7d
            boolean r3 = r2.isRemoving()
            if (r3 == 0) goto L91
        L7d:
            b.i.a.f.e.h.j.d1 r2 = new b.i.a.f.e.h.j.d1
            r2.<init>()
            android.app.FragmentManager r3 = r4.getFragmentManager()
            android.app.FragmentTransaction r3 = r3.beginTransaction()
            android.app.FragmentTransaction r0 = r3.add(r2, r0)
            r0.commitAllowingStateLoss()
        L91:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r2)
            r1.put(r4, r0)
        L99:
            return r2
        L9a:
            r4 = move-exception
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl"
            r0.<init>(r1, r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.LifecycleCallback.c(android.app.Activity):b.i.a.f.e.h.j.j");
    }

    @Keep
    private static j getChimeraLifecycleFragmentImpl(i iVar) {
        throw new IllegalStateException("Method not available in SDK.");
    }

    @MainThread
    public void a() {
    }

    @RecentlyNonNull
    public Activity b() {
        return this.j.e();
    }

    @MainThread
    public void d(int i, int i2, @RecentlyNonNull Intent intent) {
    }

    @MainThread
    public void e(@Nullable Bundle bundle) {
    }

    @MainThread
    public void f() {
    }

    @MainThread
    public void g() {
    }

    @MainThread
    public void h(@RecentlyNonNull Bundle bundle) {
    }

    @MainThread
    public void i() {
    }

    @MainThread
    public void j() {
    }
}
