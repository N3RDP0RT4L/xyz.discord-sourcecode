package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.e.h.d;
import b.i.a.f.e.h.f;
import b.i.a.f.e.h.h;
import b.i.a.f.e.h.i;
import b.i.a.f.e.h.j.l0;
import b.i.a.f.e.h.j.w0;
import b.i.a.f.h.e.c;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.Status;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
@KeepName
/* loaded from: classes3.dex */
public abstract class BasePendingResult<R extends h> extends d<R> {
    public static final ThreadLocal<Boolean> a = new w0();

    /* renamed from: b  reason: collision with root package name */
    public final Object f2976b;
    public final a<R> c;
    public final CountDownLatch d;
    public final ArrayList<d.a> e;
    public final AtomicReference<l0> f;
    @Nullable
    public R g;
    public Status h;
    public volatile boolean i;
    public boolean j;
    public boolean k;
    @KeepName
    public b mResultGuardian;

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static class a<R extends h> extends c {
        public a(Looper looper) {
            super(looper);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // android.os.Handler
        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                Pair pair = (Pair) message.obj;
                i iVar = (i) pair.first;
                h hVar = (h) pair.second;
                try {
                    iVar.a(hVar);
                } catch (RuntimeException e) {
                    BasePendingResult.h(hVar);
                    throw e;
                }
            } else if (i != 2) {
                Log.wtf("BasePendingResult", b.d.b.a.a.f(45, "Don't know how to handle message: ", i), new Exception());
            } else {
                ((BasePendingResult) message.obj).e(Status.m);
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public final class b {
        public b(w0 w0Var) {
        }

        public final void finalize() throws Throwable {
            BasePendingResult.h(BasePendingResult.this.g);
            super.finalize();
        }
    }

    @Deprecated
    public BasePendingResult() {
        this.f2976b = new Object();
        this.d = new CountDownLatch(1);
        this.e = new ArrayList<>();
        this.f = new AtomicReference<>();
        this.k = false;
        this.c = new a<>(Looper.getMainLooper());
        new WeakReference(null);
    }

    public static void h(@Nullable h hVar) {
        if (hVar instanceof f) {
            try {
                ((f) hVar).release();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(hVar);
                StringBuilder sb = new StringBuilder(valueOf.length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("BasePendingResult", sb.toString(), e);
            }
        }
    }

    @Override // b.i.a.f.e.h.d
    public final void c(d.a aVar) {
        b.c.a.a0.d.o(true, "Callback cannot be null.");
        synchronized (this.f2976b) {
            if (f()) {
                aVar.a(this.h);
            } else {
                this.e.add(aVar);
            }
        }
    }

    @NonNull
    public abstract R d(Status status);

    @Deprecated
    public final void e(Status status) {
        synchronized (this.f2976b) {
            if (!f()) {
                b(d(status));
                this.j = true;
            }
        }
    }

    public final boolean f() {
        return this.d.getCount() == 0;
    }

    /* renamed from: g */
    public final void b(R r) {
        synchronized (this.f2976b) {
            if (!this.j) {
                f();
                boolean z2 = true;
                b.c.a.a0.d.G(!f(), "Results have already been set");
                if (this.i) {
                    z2 = false;
                }
                b.c.a.a0.d.G(z2, "Result has already been consumed");
                i(r);
                return;
            }
            h(r);
        }
    }

    public final void i(R r) {
        this.g = r;
        this.h = r.b0();
        this.d.countDown();
        if (this.g instanceof f) {
            this.mResultGuardian = new b(null);
        }
        ArrayList<d.a> arrayList = this.e;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            d.a aVar = arrayList.get(i);
            i++;
            aVar.a(this.h);
        }
        this.e.clear();
    }

    public final R j() {
        R r;
        synchronized (this.f2976b) {
            b.c.a.a0.d.G(!this.i, "Result has already been consumed.");
            b.c.a.a0.d.G(f(), "Result is not ready.");
            r = this.g;
            this.g = null;
            this.i = true;
        }
        l0 andSet = this.f.getAndSet(null);
        if (andSet != null) {
            andSet.a(this);
        }
        Objects.requireNonNull(r, "null reference");
        return r;
    }

    public BasePendingResult(@Nullable b.i.a.f.e.h.c cVar) {
        this.f2976b = new Object();
        this.d = new CountDownLatch(1);
        this.e = new ArrayList<>();
        this.f = new AtomicReference<>();
        this.k = false;
        this.c = new a<>(cVar != null ? cVar.c() : Looper.getMainLooper());
        new WeakReference(cVar);
    }
}
