package com.google.android.gms.common.api;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import b.i.a.f.e.h.j.b;
import com.google.android.gms.common.ConnectionResult;
import java.util.ArrayList;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public class AvailabilityException extends Exception {
    private final ArrayMap<b<?>, ConnectionResult> zaa;

    @Override // java.lang.Throwable
    @NonNull
    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z2 = true;
        for (b<?> bVar : this.zaa.keySet()) {
            ConnectionResult connectionResult = this.zaa.get(bVar);
            if (connectionResult.x0()) {
                z2 = false;
            }
            String str = bVar.f1350b.c;
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(valueOf.length() + String.valueOf(str).length() + 2);
            sb.append(str);
            sb.append(": ");
            sb.append(valueOf);
            arrayList.add(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        if (z2) {
            sb2.append("None of the queried APIs are available. ");
        } else {
            sb2.append("Some of the queried APIs are unavailable. ");
        }
        sb2.append(TextUtils.join("; ", arrayList));
        return sb2.toString();
    }
}
