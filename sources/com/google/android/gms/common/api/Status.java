package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNonNull;
import b.c.a.a0.d;
import b.i.a.f.e.h.h;
import b.i.a.f.e.h.m;
import b.i.a.f.e.k.j;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Arrays;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class Status extends AbstractSafeParcelable implements h, ReflectedParcelable {
    public final int o;
    public final int p;
    @Nullable
    public final String q;
    @Nullable
    public final PendingIntent r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public final ConnectionResult f2975s;
    @RecentlyNonNull
    public static final Status j = new Status(0, null);
    @RecentlyNonNull
    public static final Status k = new Status(14, null);
    @RecentlyNonNull
    public static final Status l = new Status(8, null);
    @RecentlyNonNull
    public static final Status m = new Status(15, null);
    @RecentlyNonNull
    public static final Status n = new Status(16, null);
    @RecentlyNonNull
    public static final Parcelable.Creator<Status> CREATOR = new m();

    static {
        new Status(17, null);
        new Status(18, null);
    }

    public Status(int i, int i2, @Nullable String str, @Nullable PendingIntent pendingIntent, @Nullable ConnectionResult connectionResult) {
        this.o = i;
        this.p = i2;
        this.q = str;
        this.r = pendingIntent;
        this.f2975s = connectionResult;
    }

    public Status(int i, @Nullable String str) {
        this.o = 1;
        this.p = i;
        this.q = str;
        this.r = null;
        this.f2975s = null;
    }

    @Override // b.i.a.f.e.h.h
    @RecentlyNonNull
    public Status b0() {
        return this;
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        return this.o == status.o && this.p == status.p && d.h0(this.q, status.q) && d.h0(this.r, status.r) && d.h0(this.f2975s, status.f2975s);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.o), Integer.valueOf(this.p), this.q, this.r, this.f2975s});
    }

    @RecentlyNonNull
    public String toString() {
        j jVar = new j(this);
        jVar.a("statusCode", x0());
        jVar.a("resolution", this.r);
        return jVar.toString();
    }

    public boolean w0() {
        return this.p <= 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(@RecentlyNonNull Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = this.p;
        parcel.writeInt(262145);
        parcel.writeInt(i2);
        d.t2(parcel, 2, this.q, false);
        d.s2(parcel, 3, this.r, i, false);
        d.s2(parcel, 4, this.f2975s, i, false);
        int i3 = this.o;
        parcel.writeInt(263144);
        parcel.writeInt(i3);
        d.A2(parcel, y2);
    }

    @RecentlyNonNull
    public final String x0() {
        String str = this.q;
        return str != null ? str : d.B0(this.p);
    }

    public Status(int i, @Nullable String str, @Nullable PendingIntent pendingIntent) {
        this.o = 1;
        this.p = i;
        this.q = str;
        this.r = pendingIntent;
        this.f2975s = null;
    }
}
