package com.google.android.gms.common.api;

import androidx.annotation.RecentlyNonNull;
import b.d.b.a.a;
import com.google.android.gms.common.Feature;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class UnsupportedApiCallException extends UnsupportedOperationException {
    private final Feature zza;

    public UnsupportedApiCallException(@RecentlyNonNull Feature feature) {
        this.zza = feature;
    }

    @Override // java.lang.Throwable
    @RecentlyNonNull
    public String getMessage() {
        String valueOf = String.valueOf(this.zza);
        return a.H(new StringBuilder(valueOf.length() + 8), "Missing ", valueOf);
    }
}
