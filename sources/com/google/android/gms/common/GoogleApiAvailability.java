package com.google.android.gms.common;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.SimpleArrayMap;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import b.c.a.a0.d;
import b.i.a.f.e.b;
import b.i.a.f.e.c;
import b.i.a.f.e.e;
import b.i.a.f.e.g;
import b.i.a.f.e.h.j.b0;
import b.i.a.f.e.h.j.j;
import b.i.a.f.e.k.l;
import b.i.a.f.e.k.m;
import b.i.a.f.e.k.n;
import b.i.a.f.e.k.o;
import b.i.a.f.e.o.f;
import com.google.android.gms.base.R;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.atomic.AtomicBoolean;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public class GoogleApiAvailability extends c {
    public static final Object c = new Object();
    public static final GoogleApiAvailability d = new GoogleApiAvailability();
    public static final int e = c.a;

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    @SuppressLint({"HandlerLeak"})
    /* loaded from: classes3.dex */
    public class a extends b.i.a.f.h.e.c {
        public final Context a;

        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.a = context.getApplicationContext();
        }

        @Override // android.os.Handler
        public final void handleMessage(Message message) {
            int i = message.what;
            if (i != 1) {
                b.d.b.a.a.d0(50, "Don't know how to handle this message: ", i, "GoogleApiAvailability");
                return;
            }
            int c = GoogleApiAvailability.this.c(this.a);
            if (GoogleApiAvailability.this.d(c)) {
                GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.this;
                Context context = this.a;
                Intent a = googleApiAvailability.a(context, c, "n");
                googleApiAvailability.i(context, c, a == null ? null : PendingIntent.getActivity(context, 0, a, 134217728));
            }
        }
    }

    @Nullable
    public static Dialog g(@NonNull Context context, int i, n nVar, @Nullable DialogInterface.OnCancelListener onCancelListener) {
        String str;
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(l.e(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        Resources resources = context.getResources();
        if (i == 1) {
            str = resources.getString(R.b.common_google_play_services_install_button);
        } else if (i == 2) {
            str = resources.getString(R.b.common_google_play_services_update_button);
        } else if (i != 3) {
            str = resources.getString(17039370);
        } else {
            str = resources.getString(R.b.common_google_play_services_enable_button);
        }
        if (str != null) {
            builder.setPositiveButton(str, nVar);
        }
        String a2 = l.a(context, i);
        if (a2 != null) {
            builder.setTitle(a2);
        }
        return builder.create();
    }

    public static void h(Activity activity, Dialog dialog, String str, @Nullable DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            FragmentManager supportFragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
            g gVar = new g();
            d.z(dialog, "Cannot display null dialog");
            dialog.setOnCancelListener(null);
            dialog.setOnDismissListener(null);
            gVar.j = dialog;
            if (onCancelListener != null) {
                gVar.k = onCancelListener;
            }
            gVar.show(supportFragmentManager, str);
            return;
        }
        android.app.FragmentManager fragmentManager = activity.getFragmentManager();
        b bVar = new b();
        d.z(dialog, "Cannot display null dialog");
        dialog.setOnCancelListener(null);
        dialog.setOnDismissListener(null);
        bVar.j = dialog;
        if (onCancelListener != null) {
            bVar.k = onCancelListener;
        }
        bVar.show(fragmentManager, str);
    }

    @Override // b.i.a.f.e.c
    @Nullable
    public Intent a(Context context, int i, @Nullable String str) {
        return super.a(context, i, str);
    }

    @Override // b.i.a.f.e.c
    public int b(Context context, int i) {
        return super.b(context, i);
    }

    public int c(Context context) {
        return b(context, c.a);
    }

    public final boolean d(int i) {
        AtomicBoolean atomicBoolean = e.a;
        return i == 1 || i == 2 || i == 3 || i == 9;
    }

    @NonNull
    @MainThread
    public Task<Void> e(Activity activity) {
        int i = e;
        d.u("makeGooglePlayServicesAvailable must be called from the main thread");
        int b2 = super.b(activity, i);
        if (b2 == 0) {
            return f.Z(null);
        }
        j c2 = LifecycleCallback.c(activity);
        b0 b0Var = (b0) c2.d("GmsAvailabilityHelper", b0.class);
        if (b0Var == null) {
            b0Var = new b0(c2);
        } else if (b0Var.o.a.o()) {
            b0Var.o = new TaskCompletionSource<>();
        }
        b0Var.n(new ConnectionResult(b2, null), 0);
        return b0Var.o.a;
    }

    public boolean f(Activity activity, int i, int i2, @Nullable DialogInterface.OnCancelListener onCancelListener) {
        Dialog g = g(activity, i, new m(super.a(activity, i, "d"), activity, i2), onCancelListener);
        if (g == null) {
            return false;
        }
        h(activity, g, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    @TargetApi(20)
    public final void i(Context context, int i, PendingIntent pendingIntent) {
        String str;
        String str2;
        int i2;
        Log.w("GoogleApiAvailability", String.format("GMS core API Availability. ConnectionResult=%s, tag=%s", Integer.valueOf(i), null), new IllegalArgumentException());
        if (i == 18) {
            new a(context).sendEmptyMessageDelayed(1, 120000L);
        } else if (pendingIntent != null) {
            if (i == 6) {
                str = l.b(context, "common_google_play_services_resolution_required_title");
            } else {
                str = l.a(context, i);
            }
            if (str == null) {
                str = context.getResources().getString(R.b.common_google_play_services_notification_ticker);
            }
            if (i == 6 || i == 19) {
                str2 = l.c(context, "common_google_play_services_resolution_required_text", l.d(context));
            } else {
                str2 = l.e(context, i);
            }
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            NotificationCompat.Builder style = new NotificationCompat.Builder(context).setLocalOnly(true).setAutoCancel(true).setContentTitle(str).setStyle(new NotificationCompat.BigTextStyle().bigText(str2));
            if (d.Z0(context)) {
                d.F(true);
                style.setSmallIcon(context.getApplicationInfo().icon).setPriority(2);
                if (d.a1(context)) {
                    style.addAction(R.a.common_full_open_on_phone, resources.getString(R.b.common_open_on_phone), pendingIntent);
                } else {
                    style.setContentIntent(pendingIntent);
                }
            } else {
                style.setSmallIcon(17301642).setTicker(resources.getString(R.b.common_google_play_services_notification_ticker)).setWhen(System.currentTimeMillis()).setContentIntent(pendingIntent).setContentText(str2);
            }
            if (f.A0()) {
                d.F(f.A0());
                synchronized (c) {
                }
                NotificationChannel notificationChannel = notificationManager.getNotificationChannel("com.google.android.gms.availability");
                SimpleArrayMap<String, String> simpleArrayMap = l.a;
                String string = context.getResources().getString(R.b.common_google_play_services_notification_channel_name);
                if (notificationChannel == null) {
                    notificationManager.createNotificationChannel(new NotificationChannel("com.google.android.gms.availability", string, 4));
                } else if (!string.contentEquals(notificationChannel.getName())) {
                    notificationChannel.setName(string);
                    notificationManager.createNotificationChannel(notificationChannel);
                }
                style.setChannelId("com.google.android.gms.availability");
            }
            Notification build = style.build();
            if (i == 1 || i == 2 || i == 3) {
                i2 = 10436;
                e.a.set(false);
            } else {
                i2 = 39789;
            }
            notificationManager.notify(i2, build);
        } else if (i == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }

    public final boolean j(@NonNull Activity activity, @NonNull j jVar, int i, @Nullable DialogInterface.OnCancelListener onCancelListener) {
        Dialog g = g(activity, i, new o(super.a(activity, i, "d"), jVar, 2), onCancelListener);
        if (g == null) {
            return false;
        }
        h(activity, g, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }
}
