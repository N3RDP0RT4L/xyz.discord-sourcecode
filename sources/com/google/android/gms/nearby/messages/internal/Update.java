package com.google.android.gms.nearby.messages.internal;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.f.j.b.e.k0;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.internal.nearby.zzgs;
import com.google.android.gms.nearby.messages.Message;
import java.util.Arrays;
/* loaded from: classes3.dex */
public class Update extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<Update> CREATOR = new k0();
    public final int j;
    public final int k;
    public final Message l;
    @Nullable
    public final zze m;
    @Nullable
    public final zza n;
    @Nullable
    public final zzgs o;
    @Nullable
    public final byte[] p;

    public Update(int i, int i2, Message message, @Nullable zze zzeVar, @Nullable zza zzaVar, @Nullable zzgs zzgsVar, @Nullable byte[] bArr) {
        this.j = i;
        zzeVar = null;
        if ((i2 & 2) != 0) {
            i2 = 2;
            zzaVar = null;
            zzgsVar = null;
            bArr = null;
        }
        this.k = i2;
        this.l = message;
        this.m = zzeVar;
        this.n = zzaVar;
        this.o = zzgsVar;
        this.p = bArr;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Update)) {
            return false;
        }
        Update update = (Update) obj;
        return this.k == update.k && d.h0(this.l, update.l) && d.h0(this.m, update.m) && d.h0(this.n, update.n) && d.h0(this.o, update.o) && Arrays.equals(this.p, update.p);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.k), this.l, this.m, this.n, this.o, this.p});
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0121  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0124  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x0147  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x014c  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.lang.String toString() {
        /*
            Method dump skipped, instructions count: 436
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.nearby.messages.internal.Update.toString():java.lang.String");
    }

    public final boolean w0(int i) {
        return (i & this.k) != 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = this.j;
        parcel.writeInt(262145);
        parcel.writeInt(i2);
        int i3 = this.k;
        parcel.writeInt(262146);
        parcel.writeInt(i3);
        d.s2(parcel, 3, this.l, i, false);
        d.s2(parcel, 4, this.m, i, false);
        d.s2(parcel, 5, this.n, i, false);
        d.s2(parcel, 6, this.o, i, false);
        d.q2(parcel, 7, this.p, false);
        d.A2(parcel, y2);
    }
}
