package com.google.android.gms.nearby.messages.internal;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.f.j.b.e.a;
import b.i.a.f.j.b.e.c;
import b.i.a.f.j.b.e.h0;
import b.i.a.f.j.b.e.o0;
import b.i.a.f.j.b.e.q0;
import b.i.a.f.j.b.e.r0;
import b.i.a.f.j.b.e.t0;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.Strategy;
/* loaded from: classes3.dex */
public final class SubscribeRequest extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<SubscribeRequest> CREATOR = new h0();
    public final int j;
    @Nullable
    public final o0 k;
    public final Strategy l;
    public final r0 m;
    public final MessageFilter n;
    @Nullable
    public final PendingIntent o;
    @Deprecated
    public final int p;
    @Nullable
    @Deprecated
    public final String q;
    @Nullable
    @Deprecated
    public final String r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public final byte[] f2992s;
    @Deprecated
    public final boolean t;
    @Nullable
    public final a u;
    @Deprecated
    public final boolean v;
    @Deprecated
    public final ClientAppContext w;

    /* renamed from: x  reason: collision with root package name */
    public final boolean f2993x;

    /* renamed from: y  reason: collision with root package name */
    public final int f2994y;

    /* renamed from: z  reason: collision with root package name */
    public final int f2995z;

    public SubscribeRequest(int i, @Nullable IBinder iBinder, Strategy strategy, IBinder iBinder2, MessageFilter messageFilter, @Nullable PendingIntent pendingIntent, int i2, @Nullable String str, @Nullable String str2, @Nullable byte[] bArr, boolean z2, @Nullable IBinder iBinder3, boolean z3, @Nullable ClientAppContext clientAppContext, boolean z4, int i3, int i4) {
        o0 o0Var;
        r0 r0Var;
        this.j = i;
        a aVar = null;
        if (iBinder == null) {
            o0Var = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.nearby.messages.internal.IMessageListener");
            o0Var = queryLocalInterface instanceof o0 ? (o0) queryLocalInterface : new q0(iBinder);
        }
        this.k = o0Var;
        this.l = strategy;
        if (iBinder2 == null) {
            r0Var = null;
        } else {
            IInterface queryLocalInterface2 = iBinder2.queryLocalInterface("com.google.android.gms.nearby.messages.internal.INearbyMessagesCallback");
            r0Var = queryLocalInterface2 instanceof r0 ? (r0) queryLocalInterface2 : new t0(iBinder2);
        }
        this.m = r0Var;
        this.n = messageFilter;
        this.o = pendingIntent;
        this.p = i2;
        this.q = str;
        this.r = str2;
        this.f2992s = bArr;
        this.t = z2;
        if (iBinder3 != null) {
            IInterface queryLocalInterface3 = iBinder3.queryLocalInterface("com.google.android.gms.nearby.messages.internal.ISubscribeCallback");
            aVar = queryLocalInterface3 instanceof a ? (a) queryLocalInterface3 : new c(iBinder3);
        }
        this.u = aVar;
        this.v = z3;
        this.w = ClientAppContext.w0(clientAppContext, str2, str, z3);
        this.f2993x = z4;
        this.f2994y = i3;
        this.f2995z = i4;
    }

    public final String toString() {
        String str;
        String valueOf = String.valueOf(this.k);
        String valueOf2 = String.valueOf(this.l);
        String valueOf3 = String.valueOf(this.m);
        String valueOf4 = String.valueOf(this.n);
        String valueOf5 = String.valueOf(this.o);
        byte[] bArr = this.f2992s;
        if (bArr == null) {
            str = null;
        } else {
            int length = bArr.length;
            StringBuilder sb = new StringBuilder(19);
            sb.append("<");
            sb.append(length);
            sb.append(" bytes>");
            str = sb.toString();
        }
        String valueOf6 = String.valueOf(this.u);
        boolean z2 = this.v;
        String valueOf7 = String.valueOf(this.w);
        boolean z3 = this.f2993x;
        String str2 = this.q;
        String str3 = this.r;
        boolean z4 = this.t;
        int i = this.f2995z;
        StringBuilder Q = b.d.b.a.a.Q(b.d.b.a.a.b(str3, b.d.b.a.a.b(str2, valueOf7.length() + valueOf6.length() + b.d.b.a.a.b(str, valueOf5.length() + valueOf4.length() + valueOf3.length() + valueOf2.length() + valueOf.length() + 291))), "SubscribeRequest{messageListener=", valueOf, ", strategy=", valueOf2);
        b.d.b.a.a.q0(Q, ", callback=", valueOf3, ", filter=", valueOf4);
        b.d.b.a.a.q0(Q, ", pendingIntent=", valueOf5, ", hint=", str);
        Q.append(", subscribeCallback=");
        Q.append(valueOf6);
        Q.append(", useRealClientApiKey=");
        Q.append(z2);
        Q.append(", clientAppContext=");
        Q.append(valueOf7);
        Q.append(", isDiscardPendingIntent=");
        Q.append(z3);
        b.d.b.a.a.q0(Q, ", zeroPartyPackageName=", str2, ", realClientPackageName=", str3);
        Q.append(", isIgnoreNearbyPermission=");
        Q.append(z4);
        Q.append(", callingContext=");
        Q.append(i);
        Q.append("}");
        return Q.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = this.j;
        parcel.writeInt(262145);
        parcel.writeInt(i2);
        o0 o0Var = this.k;
        IBinder iBinder = null;
        d.r2(parcel, 2, o0Var == null ? null : o0Var.asBinder(), false);
        d.s2(parcel, 3, this.l, i, false);
        r0 r0Var = this.m;
        d.r2(parcel, 4, r0Var == null ? null : r0Var.asBinder(), false);
        d.s2(parcel, 5, this.n, i, false);
        d.s2(parcel, 6, this.o, i, false);
        int i3 = this.p;
        parcel.writeInt(262151);
        parcel.writeInt(i3);
        d.t2(parcel, 8, this.q, false);
        d.t2(parcel, 9, this.r, false);
        d.q2(parcel, 10, this.f2992s, false);
        boolean z2 = this.t;
        parcel.writeInt(262155);
        parcel.writeInt(z2 ? 1 : 0);
        a aVar = this.u;
        if (aVar != null) {
            iBinder = aVar.asBinder();
        }
        d.r2(parcel, 12, iBinder, false);
        boolean z3 = this.v;
        parcel.writeInt(262157);
        parcel.writeInt(z3 ? 1 : 0);
        d.s2(parcel, 14, this.w, i, false);
        boolean z4 = this.f2993x;
        parcel.writeInt(262159);
        parcel.writeInt(z4 ? 1 : 0);
        int i4 = this.f2994y;
        parcel.writeInt(262160);
        parcel.writeInt(i4);
        int i5 = this.f2995z;
        parcel.writeInt(262161);
        parcel.writeInt(i5);
        d.A2(parcel, y2);
    }
}
