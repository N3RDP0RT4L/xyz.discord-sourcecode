package com.google.android.gms.nearby.messages.internal;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.f.j.b.e.b1;
import b.i.a.f.j.b.e.g0;
import b.i.a.f.j.b.e.r0;
import b.i.a.f.j.b.e.t0;
import b.i.a.f.j.b.e.z0;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
/* loaded from: classes3.dex */
public final class zzcb extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzcb> CREATOR = new g0();
    public final int j;
    public final r0 k;
    public final z0 l;
    public boolean m;
    @Nullable
    @Deprecated
    public String n;
    @Nullable
    @Deprecated
    public final ClientAppContext o;

    public zzcb(int i, IBinder iBinder, IBinder iBinder2, boolean z2, @Nullable String str, @Nullable ClientAppContext clientAppContext) {
        r0 r0Var;
        z0 z0Var;
        this.j = i;
        if (iBinder == null) {
            r0Var = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.nearby.messages.internal.INearbyMessagesCallback");
            r0Var = queryLocalInterface instanceof r0 ? (r0) queryLocalInterface : new t0(iBinder);
        }
        this.k = r0Var;
        if (iBinder2 == null) {
            z0Var = null;
        } else {
            IInterface queryLocalInterface2 = iBinder2.queryLocalInterface("com.google.android.gms.nearby.messages.internal.IStatusCallback");
            z0Var = queryLocalInterface2 instanceof z0 ? (z0) queryLocalInterface2 : new b1(iBinder2);
        }
        this.l = z0Var;
        this.m = z2;
        this.n = str;
        this.o = ClientAppContext.w0(clientAppContext, null, str, false);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = this.j;
        parcel.writeInt(262145);
        parcel.writeInt(i2);
        d.r2(parcel, 2, this.k.asBinder(), false);
        d.r2(parcel, 3, this.l.asBinder(), false);
        boolean z2 = this.m;
        parcel.writeInt(262148);
        parcel.writeInt(z2 ? 1 : 0);
        d.t2(parcel, 5, this.n, false);
        d.s2(parcel, 6, this.o, i, false);
        d.A2(parcel, y2);
    }
}
