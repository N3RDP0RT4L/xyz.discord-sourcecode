package com.google.android.gms.nearby.messages;

import androidx.annotation.Nullable;
import b.i.a.f.j.b.b;
import b.i.a.f.j.b.i;
/* loaded from: classes3.dex */
public final class PublishOptions {
    public final Strategy a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public final b f2989b;

    static {
        Strategy strategy = Strategy.j;
    }

    public PublishOptions(Strategy strategy, b bVar, i iVar) {
        this.a = strategy;
        this.f2989b = bVar;
    }
}
