package com.google.android.gms.nearby.messages;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.f.j.b.j;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
/* loaded from: classes3.dex */
public class Strategy extends AbstractSafeParcelable {
    public static final Parcelable.Creator<Strategy> CREATOR = new j();
    public static final Strategy j = new Strategy(2, 0, 300, 0, false, -1, 3, 0);
    public static final Strategy k = new Strategy(2, 0, Integer.MAX_VALUE, 0, false, 2, 3, 0);
    public final int l;
    @Deprecated
    public final int m;
    public final int n;
    public final int o;
    @Deprecated
    public final boolean p;
    public final int q;
    public final int r;

    /* renamed from: s  reason: collision with root package name */
    public final int f2990s;

    static {
        d.n(true, "mTtlSeconds(%d) must either be TTL_SECONDS_INFINITE, or it must be between 1 and TTL_SECONDS_MAX(%d) inclusive", Integer.MAX_VALUE, 86400);
    }

    /* JADX WARN: Removed duplicated region for block: B:12:0x001f  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0027  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public Strategy(int r2, int r3, int r4, int r5, boolean r6, int r7, int r8, int r9) {
        /*
            r1 = this;
            r1.<init>()
            r1.l = r2
            r1.m = r3
            r2 = 1
            r0 = 2
            if (r3 != 0) goto Le
        Lb:
            r1.r = r8
            goto L19
        Le:
            if (r3 == r0) goto L17
            r8 = 3
            if (r3 == r8) goto L14
            goto Lb
        L14:
            r1.r = r0
            goto L19
        L17:
            r1.r = r2
        L19:
            r1.o = r5
            r1.p = r6
            if (r6 == 0) goto L27
            r1.q = r0
            r2 = 2147483647(0x7fffffff, float:NaN)
            r1.n = r2
            goto L38
        L27:
            r1.n = r4
            r3 = -1
            if (r7 == r3) goto L36
            if (r7 == 0) goto L36
            if (r7 == r2) goto L36
            r2 = 6
            if (r7 == r2) goto L36
            r1.q = r7
            goto L38
        L36:
            r1.q = r3
        L38:
            r1.f2990s = r9
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.nearby.messages.Strategy.<init>(int, int, int, int, boolean, int, int, int):void");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Strategy)) {
            return false;
        }
        Strategy strategy = (Strategy) obj;
        return this.l == strategy.l && this.r == strategy.r && this.n == strategy.n && this.o == strategy.o && this.q == strategy.q && this.f2990s == strategy.f2990s;
    }

    public int hashCode() {
        return (((((((((this.l * 31) + this.r) * 31) + this.n) * 31) + this.o) * 31) + this.q) * 31) + this.f2990s;
    }

    public String toString() {
        String str;
        String str2;
        int i = this.n;
        int i2 = this.o;
        String str3 = "DEFAULT";
        String f = i2 != 0 ? i2 != 1 ? a.f(19, "UNKNOWN:", i2) : "EARSHOT" : str3;
        int i3 = this.q;
        if (i3 == -1) {
            str = str3;
        } else {
            ArrayList arrayList = new ArrayList();
            if ((i3 & 4) > 0) {
                arrayList.add("ULTRASOUND");
            }
            if ((i3 & 2) > 0) {
                arrayList.add("BLE");
            }
            str = arrayList.isEmpty() ? a.f(19, "UNKNOWN:", i3) : arrayList.toString();
        }
        int i4 = this.r;
        if (i4 == 3) {
            str2 = str3;
        } else {
            ArrayList arrayList2 = new ArrayList();
            if ((i4 & 1) > 0) {
                arrayList2.add("BROADCAST");
            }
            if ((i4 & 2) > 0) {
                arrayList2.add("SCAN");
            }
            str2 = arrayList2.isEmpty() ? a.f(19, "UNKNOWN:", i4) : arrayList2.toString();
        }
        int i5 = this.f2990s;
        if (i5 != 0) {
            str3 = i5 != 1 ? a.f(20, "UNKNOWN: ", i5) : "ALWAYS_ON";
        }
        StringBuilder sb = new StringBuilder(a.b(str3, a.b(str2, a.b(str, a.b(f, 102)))));
        sb.append("Strategy{ttlSeconds=");
        sb.append(i);
        sb.append(", distanceType=");
        sb.append(f);
        a.q0(sb, ", discoveryMedium=", str, ", discoveryMode=", str2);
        sb.append(", backgroundScanMode=");
        sb.append(str3);
        sb.append('}');
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = this.m;
        parcel.writeInt(262145);
        parcel.writeInt(i2);
        int i3 = this.n;
        parcel.writeInt(262146);
        parcel.writeInt(i3);
        int i4 = this.o;
        parcel.writeInt(262147);
        parcel.writeInt(i4);
        boolean z2 = this.p;
        parcel.writeInt(262148);
        parcel.writeInt(z2 ? 1 : 0);
        int i5 = this.q;
        parcel.writeInt(262149);
        parcel.writeInt(i5);
        int i6 = this.r;
        parcel.writeInt(262150);
        parcel.writeInt(i6);
        int i7 = this.f2990s;
        parcel.writeInt(262151);
        parcel.writeInt(i7);
        int i8 = this.l;
        parcel.writeInt(263144);
        parcel.writeInt(i8);
        d.A2(parcel, y2);
    }
}
