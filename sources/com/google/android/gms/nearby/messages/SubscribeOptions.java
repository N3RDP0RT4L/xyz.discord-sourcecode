package com.google.android.gms.nearby.messages;

import androidx.annotation.Nullable;
import b.d.b.a.a;
import b.i.a.f.j.b.d;
/* loaded from: classes3.dex */
public final class SubscribeOptions {
    public final Strategy a;

    /* renamed from: b  reason: collision with root package name */
    public final MessageFilter f2991b;
    @Nullable
    public final d c = null;
    public final boolean d = false;

    static {
        Strategy strategy = Strategy.j;
        MessageFilter messageFilter = MessageFilter.j;
    }

    public SubscribeOptions(Strategy strategy, MessageFilter messageFilter, d dVar) {
        this.a = strategy;
        this.f2991b = messageFilter;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.a);
        String valueOf2 = String.valueOf(this.f2991b);
        StringBuilder Q = a.Q(valueOf2.length() + valueOf.length() + 36, "SubscribeOptions{strategy=", valueOf, ", filter=", valueOf2);
        Q.append('}');
        return Q.toString();
    }
}
