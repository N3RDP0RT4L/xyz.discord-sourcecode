package com.google.android.gms.nearby.messages;

import com.google.android.gms.nearby.messages.internal.zze;
/* loaded from: classes3.dex */
public interface Distance extends Comparable<Distance> {
    static {
        new zze();
    }

    double r0();
}
