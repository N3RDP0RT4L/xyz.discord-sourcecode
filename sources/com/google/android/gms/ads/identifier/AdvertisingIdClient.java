package com.google.android.gms.ads.identifier;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import androidx.annotation.Nullable;
import b.i.a.f.e.c;
import b.i.a.f.h.a.b;
import b.i.a.f.h.a.d;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
/* loaded from: classes3.dex */
public class AdvertisingIdClient {
    @Nullable
    public b.i.a.f.e.a a;
    @Nullable

    /* renamed from: b  reason: collision with root package name */
    public b f2965b;
    public boolean c;
    public final Object d = new Object();
    @Nullable
    public a e;
    public final Context f;
    public final boolean g;
    public final long h;

    /* loaded from: classes3.dex */
    public static final class Info {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2966b;

        public Info(String str, boolean z2) {
            this.a = str;
            this.f2966b = z2;
        }

        public final String getId() {
            return this.a;
        }

        public final boolean isLimitAdTrackingEnabled() {
            return this.f2966b;
        }

        public final String toString() {
            String str = this.a;
            boolean z2 = this.f2966b;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 7);
            sb.append("{");
            sb.append(str);
            sb.append("}");
            sb.append(z2);
            return sb.toString();
        }
    }

    /* loaded from: classes3.dex */
    public static class a extends Thread {
        public WeakReference<AdvertisingIdClient> j;
        public long k;
        public CountDownLatch l = new CountDownLatch(1);
        public boolean m = false;

        public a(AdvertisingIdClient advertisingIdClient, long j) {
            this.j = new WeakReference<>(advertisingIdClient);
            this.k = j;
            start();
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public final void run() {
            AdvertisingIdClient advertisingIdClient;
            try {
                if (!this.l.await(this.k, TimeUnit.MILLISECONDS) && (advertisingIdClient = this.j.get()) != null) {
                    advertisingIdClient.a();
                    this.m = true;
                }
            } catch (InterruptedException unused) {
                AdvertisingIdClient advertisingIdClient2 = this.j.get();
                if (advertisingIdClient2 != null) {
                    advertisingIdClient2.a();
                    this.m = true;
                }
            }
        }
    }

    public AdvertisingIdClient(Context context, long j, boolean z2, boolean z3) {
        Context applicationContext;
        Objects.requireNonNull(context, "null reference");
        if (z2 && (applicationContext = context.getApplicationContext()) != null) {
            context = applicationContext;
        }
        this.f = context;
        this.c = false;
        this.h = j;
        this.g = z3;
    }

    public static b.i.a.f.e.a c(Context context, boolean z2) throws IOException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            int b2 = c.f1342b.b(context, 12451000);
            if (b2 == 0 || b2 == 2) {
                String str = z2 ? "com.google.android.gms.ads.identifier.service.PERSISTENT_START" : "com.google.android.gms.ads.identifier.service.START";
                b.i.a.f.e.a aVar = new b.i.a.f.e.a();
                Intent intent = new Intent(str);
                intent.setPackage("com.google.android.gms");
                try {
                    if (b.i.a.f.e.n.a.b().a(context, intent, aVar, 1)) {
                        return aVar;
                    }
                    throw new IOException("Connection failure");
                } catch (Throwable th) {
                    throw new IOException(th);
                }
            } else {
                throw new IOException("Google Play services not available");
            }
        } catch (PackageManager.NameNotFoundException unused) {
            throw new GooglePlayServicesNotAvailableException(9);
        }
    }

    public static b d(b.i.a.f.e.a aVar) throws IOException {
        try {
            IBinder a2 = aVar.a(10000L, TimeUnit.MILLISECONDS);
            int i = b.i.a.f.h.a.c.a;
            IInterface queryLocalInterface = a2.queryLocalInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
            return queryLocalInterface instanceof b ? (b) queryLocalInterface : new d(a2);
        } catch (InterruptedException unused) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:14:0x002a  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x002b A[Catch: all -> 0x0030, TRY_LEAVE, TryCatch #0 {all -> 0x0030, blocks: (B:12:0x0026, B:15:0x002b), top: B:30:0x0026 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static com.google.android.gms.ads.identifier.AdvertisingIdClient.Info getAdvertisingIdInfo(android.content.Context r13) throws java.io.IOException, java.lang.IllegalStateException, com.google.android.gms.common.GooglePlayServicesNotAvailableException, com.google.android.gms.common.GooglePlayServicesRepairableException {
        /*
            java.lang.String r0 = "Error while reading from SharedPreferences "
            java.lang.String r1 = "GmscoreFlag"
            b.i.a.f.a.a.b r2 = new b.i.a.f.a.a.b
            r2.<init>(r13)
            java.lang.String r3 = "gads:ad_id_app_context:enabled"
            boolean r3 = r2.a(r3)
            java.lang.String r4 = "gads:ad_id_app_context:ping_ratio"
            r5 = 0
            android.content.SharedPreferences r6 = r2.a     // Catch: java.lang.Throwable -> L1d
            if (r6 != 0) goto L17
            goto L21
        L17:
            float r4 = r6.getFloat(r4, r5)     // Catch: java.lang.Throwable -> L1d
            r12 = r4
            goto L22
        L1d:
            r4 = move-exception
            android.util.Log.w(r1, r0, r4)
        L21:
            r12 = 0
        L22:
            java.lang.String r4 = "gads:ad_id_use_shared_preference:experiment_id"
            java.lang.String r5 = ""
            android.content.SharedPreferences r6 = r2.a     // Catch: java.lang.Throwable -> L30
            if (r6 != 0) goto L2b
            goto L34
        L2b:
            java.lang.String r0 = r6.getString(r4, r5)     // Catch: java.lang.Throwable -> L30
            goto L35
        L30:
            r4 = move-exception
            android.util.Log.w(r1, r0, r4)
        L34:
            r0 = r5
        L35:
            java.lang.String r1 = "gads:ad_id_use_persistent_service:enabled"
            boolean r9 = r2.a(r1)
            com.google.android.gms.ads.identifier.AdvertisingIdClient r1 = new com.google.android.gms.ads.identifier.AdvertisingIdClient
            r6 = -1
            r4 = r1
            r5 = r13
            r8 = r3
            r4.<init>(r5, r6, r8, r9)
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch: java.lang.Throwable -> L64
            r13 = 0
            r1.f(r13)     // Catch: java.lang.Throwable -> L64
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r13 = r1.b()     // Catch: java.lang.Throwable -> L64
            long r6 = android.os.SystemClock.elapsedRealtime()     // Catch: java.lang.Throwable -> L64
            long r8 = r6 - r4
            r11 = 0
            r4 = r1
            r5 = r13
            r6 = r3
            r7 = r12
            r10 = r0
            r4.g(r5, r6, r7, r8, r10, r11)     // Catch: java.lang.Throwable -> L64
            r1.a()
            return r13
        L64:
            r13 = move-exception
            r5 = 0
            r8 = -1
            r4 = r1
            r6 = r3
            r7 = r12
            r10 = r0
            r11 = r13
            r4.g(r5, r6, r7, r8, r10, r11)     // Catch: java.lang.Throwable -> L71
            throw r13     // Catch: java.lang.Throwable -> L71
        L71:
            r13 = move-exception
            r1.a()
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo(android.content.Context):com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
    }

    public final void a() {
        b.c.a.a0.d.x("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.f != null && this.a != null) {
                if (this.c) {
                    b.i.a.f.e.n.a.b().c(this.f, this.a);
                }
                this.c = false;
                this.f2965b = null;
                this.a = null;
            }
        }
    }

    public Info b() throws IOException {
        Info info;
        b.c.a.a0.d.x("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (!this.c) {
                synchronized (this.d) {
                    a aVar = this.e;
                    if (aVar == null || !aVar.m) {
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
                try {
                    f(false);
                    if (!this.c) {
                        throw new IOException("AdvertisingIdClient cannot reconnect.");
                    }
                } catch (Exception e) {
                    throw new IOException("AdvertisingIdClient cannot reconnect.", e);
                }
            }
            Objects.requireNonNull(this.a, "null reference");
            Objects.requireNonNull(this.f2965b, "null reference");
            try {
                info = new Info(this.f2965b.getId(), this.f2965b.D(true));
            } catch (RemoteException e2) {
                Log.i("AdvertisingIdClient", "GMS remote exception ", e2);
                throw new IOException("Remote exception");
            }
        }
        e();
        return info;
    }

    public final void e() {
        synchronized (this.d) {
            a aVar = this.e;
            if (aVar != null) {
                aVar.l.countDown();
                try {
                    this.e.join();
                } catch (InterruptedException unused) {
                }
            }
            if (this.h > 0) {
                this.e = new a(this, this.h);
            }
        }
    }

    public final void f(boolean z2) throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        b.c.a.a0.d.x("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.c) {
                a();
            }
            b.i.a.f.e.a c = c(this.f, this.g);
            this.a = c;
            this.f2965b = d(c);
            this.c = true;
            if (z2) {
                e();
            }
        }
    }

    public void finalize() throws Throwable {
        a();
        super.finalize();
    }

    public final boolean g(Info info, boolean z2, float f, long j, String str, Throwable th) {
        if (Math.random() > f) {
            return false;
        }
        HashMap hashMap = new HashMap();
        String str2 = "1";
        hashMap.put("app_context", z2 ? str2 : "0");
        if (info != null) {
            if (!info.isLimitAdTrackingEnabled()) {
                str2 = "0";
            }
            hashMap.put("limit_ad_tracking", str2);
        }
        if (!(info == null || info.getId() == null)) {
            hashMap.put("ad_id_size", Integer.toString(info.getId().length()));
        }
        if (th != null) {
            hashMap.put("error", th.getClass().getName());
        }
        if (str != null && !str.isEmpty()) {
            hashMap.put("experiment_id", str);
        }
        hashMap.put("tag", "AdvertisingIdClient");
        hashMap.put("time_spent", Long.toString(j));
        new b.i.a.f.a.a.a(hashMap).start();
        return true;
    }
}
