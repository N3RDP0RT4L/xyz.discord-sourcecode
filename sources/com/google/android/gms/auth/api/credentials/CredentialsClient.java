package com.google.android.gms.auth.api.credentials;

import android.content.Context;
import androidx.annotation.NonNull;
import b.i.a.f.c.a.a;
import b.i.a.f.e.h.b;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public class CredentialsClient extends b<a.C0109a> {
    public CredentialsClient(@NonNull Context context, @NonNull a.C0109a aVar) {
        super(context, a.e, aVar, new b.i.a.f.e.h.j.a());
    }
}
