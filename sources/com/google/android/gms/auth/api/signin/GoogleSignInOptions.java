package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import b.c.a.a0.d;
import b.i.a.f.c.a.f.e;
import b.i.a.f.c.a.f.f;
import b.i.a.f.e.h.a;
import com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
/* loaded from: classes3.dex */
public class GoogleSignInOptions extends AbstractSafeParcelable implements a.d, ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInOptions> CREATOR;
    public static final Scope j;
    public static final Scope k;
    public static final Scope l;
    public static final Scope m;
    public static final GoogleSignInOptions n;
    public static Comparator<Scope> o;
    public final int p;
    public final ArrayList<Scope> q;
    @Nullable
    public Account r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f2969s;
    public final boolean t;
    public final boolean u;
    @Nullable
    public String v;
    @Nullable
    public String w;

    /* renamed from: x  reason: collision with root package name */
    public ArrayList<GoogleSignInOptionsExtensionParcelable> f2970x;
    @Nullable

    /* renamed from: y  reason: collision with root package name */
    public String f2971y;

    /* renamed from: z  reason: collision with root package name */
    public Map<Integer, GoogleSignInOptionsExtensionParcelable> f2972z;

    static {
        Scope scope = new Scope("profile");
        j = scope;
        new Scope(NotificationCompat.CATEGORY_EMAIL);
        Scope scope2 = new Scope("openid");
        k = scope2;
        Scope scope3 = new Scope("https://www.googleapis.com/auth/games_lite");
        l = scope3;
        Scope scope4 = new Scope("https://www.googleapis.com/auth/games");
        m = scope4;
        HashSet hashSet = new HashSet();
        HashMap hashMap = new HashMap();
        hashSet.add(scope2);
        hashSet.add(scope);
        if (hashSet.contains(scope4) && hashSet.contains(scope3)) {
            hashSet.remove(scope3);
        }
        n = new GoogleSignInOptions(3, new ArrayList(hashSet), null, false, false, false, null, null, hashMap, null);
        HashSet hashSet2 = new HashSet();
        HashMap hashMap2 = new HashMap();
        hashSet2.add(scope3);
        hashSet2.addAll(Arrays.asList(new Scope[0]));
        if (hashSet2.contains(scope4) && hashSet2.contains(scope3)) {
            hashSet2.remove(scope3);
        }
        new GoogleSignInOptions(3, new ArrayList(hashSet2), null, false, false, false, null, null, hashMap2, null);
        CREATOR = new f();
        o = new e();
    }

    public GoogleSignInOptions(int i, ArrayList<Scope> arrayList, @Nullable Account account, boolean z2, boolean z3, boolean z4, @Nullable String str, @Nullable String str2, Map<Integer, GoogleSignInOptionsExtensionParcelable> map, @Nullable String str3) {
        this.p = i;
        this.q = arrayList;
        this.r = account;
        this.f2969s = z2;
        this.t = z3;
        this.u = z4;
        this.v = str;
        this.w = str2;
        this.f2970x = new ArrayList<>(map.values());
        this.f2972z = map;
        this.f2971y = str3;
    }

    @Nullable
    public static GoogleSignInOptions x0(@Nullable String str) throws JSONException {
        String str2 = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("scopes");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            hashSet.add(new Scope(jSONArray.getString(i)));
        }
        String optString = jSONObject.has("accountName") ? jSONObject.optString("accountName") : null;
        Account account = !TextUtils.isEmpty(optString) ? new Account(optString, "com.google") : null;
        ArrayList arrayList = new ArrayList(hashSet);
        boolean z2 = jSONObject.getBoolean("idTokenRequested");
        boolean z3 = jSONObject.getBoolean("serverAuthRequested");
        boolean z4 = jSONObject.getBoolean("forceCodeForRefreshToken");
        String optString2 = jSONObject.has("serverClientId") ? jSONObject.optString("serverClientId") : null;
        if (jSONObject.has("hostedDomain")) {
            str2 = jSONObject.optString("hostedDomain");
        }
        return new GoogleSignInOptions(3, arrayList, account, z2, z3, z4, optString2, str2, new HashMap(), null);
    }

    public static Map<Integer, GoogleSignInOptionsExtensionParcelable> y0(@Nullable List<GoogleSignInOptionsExtensionParcelable> list) {
        HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        for (GoogleSignInOptionsExtensionParcelable googleSignInOptionsExtensionParcelable : list) {
            hashMap.put(Integer.valueOf(googleSignInOptionsExtensionParcelable.k), googleSignInOptionsExtensionParcelable);
        }
        return hashMap;
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x0043, code lost:
        if (r1.equals(r4.r) != false) goto L22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x005e, code lost:
        if (r3.v.equals(r4.v) != false) goto L29;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean equals(@androidx.annotation.Nullable java.lang.Object r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L4
            return r0
        L4:
            com.google.android.gms.auth.api.signin.GoogleSignInOptions r4 = (com.google.android.gms.auth.api.signin.GoogleSignInOptions) r4     // Catch: java.lang.ClassCastException -> L7e
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r3.f2970x     // Catch: java.lang.ClassCastException -> L7e
            int r1 = r1.size()     // Catch: java.lang.ClassCastException -> L7e
            if (r1 > 0) goto L7e
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r4.f2970x     // Catch: java.lang.ClassCastException -> L7e
            int r1 = r1.size()     // Catch: java.lang.ClassCastException -> L7e
            if (r1 <= 0) goto L17
            goto L7e
        L17:
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.q     // Catch: java.lang.ClassCastException -> L7e
            int r1 = r1.size()     // Catch: java.lang.ClassCastException -> L7e
            java.util.ArrayList r2 = r4.w0()     // Catch: java.lang.ClassCastException -> L7e
            int r2 = r2.size()     // Catch: java.lang.ClassCastException -> L7e
            if (r1 != r2) goto L7e
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.q     // Catch: java.lang.ClassCastException -> L7e
            java.util.ArrayList r2 = r4.w0()     // Catch: java.lang.ClassCastException -> L7e
            boolean r1 = r1.containsAll(r2)     // Catch: java.lang.ClassCastException -> L7e
            if (r1 != 0) goto L34
            goto L7e
        L34:
            android.accounts.Account r1 = r3.r     // Catch: java.lang.ClassCastException -> L7e
            if (r1 != 0) goto L3d
            android.accounts.Account r1 = r4.r     // Catch: java.lang.ClassCastException -> L7e
            if (r1 != 0) goto L7e
            goto L45
        L3d:
            android.accounts.Account r2 = r4.r     // Catch: java.lang.ClassCastException -> L7e
            boolean r1 = r1.equals(r2)     // Catch: java.lang.ClassCastException -> L7e
            if (r1 == 0) goto L7e
        L45:
            java.lang.String r1 = r3.v     // Catch: java.lang.ClassCastException -> L7e
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch: java.lang.ClassCastException -> L7e
            if (r1 == 0) goto L56
            java.lang.String r1 = r4.v     // Catch: java.lang.ClassCastException -> L7e
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch: java.lang.ClassCastException -> L7e
            if (r1 == 0) goto L7e
            goto L60
        L56:
            java.lang.String r1 = r3.v     // Catch: java.lang.ClassCastException -> L7e
            java.lang.String r2 = r4.v     // Catch: java.lang.ClassCastException -> L7e
            boolean r1 = r1.equals(r2)     // Catch: java.lang.ClassCastException -> L7e
            if (r1 == 0) goto L7e
        L60:
            boolean r1 = r3.u     // Catch: java.lang.ClassCastException -> L7e
            boolean r2 = r4.u     // Catch: java.lang.ClassCastException -> L7e
            if (r1 != r2) goto L7e
            boolean r1 = r3.f2969s     // Catch: java.lang.ClassCastException -> L7e
            boolean r2 = r4.f2969s     // Catch: java.lang.ClassCastException -> L7e
            if (r1 != r2) goto L7e
            boolean r1 = r3.t     // Catch: java.lang.ClassCastException -> L7e
            boolean r2 = r4.t     // Catch: java.lang.ClassCastException -> L7e
            if (r1 != r2) goto L7e
            java.lang.String r1 = r3.f2971y     // Catch: java.lang.ClassCastException -> L7e
            java.lang.String r4 = r4.f2971y     // Catch: java.lang.ClassCastException -> L7e
            boolean r4 = android.text.TextUtils.equals(r1, r4)     // Catch: java.lang.ClassCastException -> L7e
            if (r4 == 0) goto L7e
            r4 = 1
            return r4
        L7e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.auth.api.signin.GoogleSignInOptions.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        ArrayList arrayList = new ArrayList();
        ArrayList<Scope> arrayList2 = this.q;
        int size = arrayList2.size();
        int i = 0;
        int i2 = 0;
        while (i2 < size) {
            Scope scope = arrayList2.get(i2);
            i2++;
            arrayList.add(scope.k);
        }
        Collections.sort(arrayList);
        Account account = this.r;
        int hashCode = ((arrayList.hashCode() + 31) * 31) + (account == null ? 0 : account.hashCode());
        String str = this.v;
        int hashCode2 = (((((((hashCode * 31) + (str == null ? 0 : str.hashCode())) * 31) + (this.u ? 1 : 0)) * 31) + (this.f2969s ? 1 : 0)) * 31) + (this.t ? 1 : 0);
        String str2 = this.f2971y;
        int i3 = hashCode2 * 31;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return i3 + i;
    }

    public ArrayList<Scope> w0() {
        return new ArrayList<>(this.q);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = this.p;
        parcel.writeInt(262145);
        parcel.writeInt(i2);
        d.w2(parcel, 2, w0(), false);
        d.s2(parcel, 3, this.r, i, false);
        boolean z2 = this.f2969s;
        parcel.writeInt(262148);
        parcel.writeInt(z2 ? 1 : 0);
        boolean z3 = this.t;
        parcel.writeInt(262149);
        parcel.writeInt(z3 ? 1 : 0);
        boolean z4 = this.u;
        parcel.writeInt(262150);
        parcel.writeInt(z4 ? 1 : 0);
        d.t2(parcel, 7, this.v, false);
        d.t2(parcel, 8, this.w, false);
        d.w2(parcel, 9, this.f2970x, false);
        d.t2(parcel, 10, this.f2971y, false);
        d.A2(parcel, y2);
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.3.0 */
    /* loaded from: classes3.dex */
    public static final class a {
        public Set<Scope> a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f2973b;
        public boolean c;
        public boolean d;
        @Nullable
        public String e;
        @Nullable
        public Account f;
        @Nullable
        public String g;
        public Map<Integer, GoogleSignInOptionsExtensionParcelable> h;
        @Nullable
        public String i;

        public a() {
            this.a = new HashSet();
            this.h = new HashMap();
        }

        public final GoogleSignInOptions a() {
            if (this.a.contains(GoogleSignInOptions.m)) {
                Set<Scope> set = this.a;
                Scope scope = GoogleSignInOptions.l;
                if (set.contains(scope)) {
                    this.a.remove(scope);
                }
            }
            if (this.d && (this.f == null || !this.a.isEmpty())) {
                this.a.add(GoogleSignInOptions.k);
            }
            return new GoogleSignInOptions(3, new ArrayList(this.a), this.f, this.d, this.f2973b, this.c, this.e, this.g, this.h, this.i);
        }

        public a(@NonNull GoogleSignInOptions googleSignInOptions) {
            this.a = new HashSet();
            this.h = new HashMap();
            this.a = new HashSet(googleSignInOptions.q);
            this.f2973b = googleSignInOptions.t;
            this.c = googleSignInOptions.u;
            this.d = googleSignInOptions.f2969s;
            this.e = googleSignInOptions.v;
            this.f = googleSignInOptions.r;
            this.g = googleSignInOptions.w;
            this.h = GoogleSignInOptions.y0(googleSignInOptions.f2970x);
            this.i = googleSignInOptions.f2971y;
        }
    }
}
