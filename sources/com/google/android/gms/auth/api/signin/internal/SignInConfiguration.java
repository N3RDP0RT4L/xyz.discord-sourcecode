package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.f.c.a.f.b.s;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* loaded from: classes3.dex */
public final class SignInConfiguration extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<SignInConfiguration> CREATOR = new s();
    public final String j;
    public GoogleSignInOptions k;

    public SignInConfiguration(String str, GoogleSignInOptions googleSignInOptions) {
        d.w(str);
        this.j = str;
        this.k = googleSignInOptions;
    }

    public final boolean equals(@Nullable Object obj) {
        if (!(obj instanceof SignInConfiguration)) {
            return false;
        }
        SignInConfiguration signInConfiguration = (SignInConfiguration) obj;
        if (this.j.equals(signInConfiguration.j)) {
            GoogleSignInOptions googleSignInOptions = this.k;
            if (googleSignInOptions == null) {
                if (signInConfiguration.k == null) {
                    return true;
                }
            } else if (googleSignInOptions.equals(signInConfiguration.k)) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        String str = this.j;
        int i = 0;
        int hashCode = str == null ? 0 : str.hashCode();
        GoogleSignInOptions googleSignInOptions = this.k;
        int i2 = (hashCode + 31) * 31;
        if (googleSignInOptions != null) {
            i = googleSignInOptions.hashCode();
        }
        return i2 + i;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        d.t2(parcel, 2, this.j, false);
        d.s2(parcel, 5, this.k, i, false);
        d.A2(parcel, y2);
    }
}
