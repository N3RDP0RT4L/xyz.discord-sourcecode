package com.google.android.gms.auth;

import android.content.Intent;
/* loaded from: classes3.dex */
public class UserRecoverableAuthException extends GoogleAuthException {
    private final Intent mIntent;
}
