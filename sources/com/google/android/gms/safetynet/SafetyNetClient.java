package com.google.android.gms.safetynet;

import b.i.a.f.e.h.a;
import b.i.a.f.e.h.b;
/* loaded from: classes3.dex */
public class SafetyNetClient extends b<a.d.c> {
    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public SafetyNetClient(@androidx.annotation.NonNull android.app.Activity r6) {
        /*
            r5 = this;
            b.i.a.f.e.h.a<b.i.a.f.e.h.a$d$c> r0 = b.i.a.f.k.a.c
            b.i.a.f.e.h.j.a r1 = new b.i.a.f.e.h.j.a
            r1.<init>()
            java.lang.String r2 = "StatusExceptionMapper must not be null."
            b.c.a.a0.d.z(r1, r2)
            android.os.Looper r2 = r6.getMainLooper()
            java.lang.String r3 = "Looper must not be null."
            b.c.a.a0.d.z(r2, r3)
            b.i.a.f.e.h.b$a r3 = new b.i.a.f.e.h.b$a
            r4 = 0
            r3.<init>(r1, r4, r2)
            r5.<init>(r6, r0, r4, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.safetynet.SafetyNetClient.<init>(android.app.Activity):void");
    }
}
