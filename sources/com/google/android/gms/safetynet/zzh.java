package com.google.android.gms.safetynet;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import b.i.a.f.k.f;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
/* loaded from: classes3.dex */
public final class zzh extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzh> CREATOR = new f();
    public final int j;
    public final boolean k;

    public zzh(int i, boolean z2) {
        this.j = i;
        this.k = z2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = this.j;
        parcel.writeInt(262146);
        parcel.writeInt(i2);
        boolean z2 = this.k;
        parcel.writeInt(262147);
        parcel.writeInt(z2 ? 1 : 0);
        d.A2(parcel, y2);
    }
}
