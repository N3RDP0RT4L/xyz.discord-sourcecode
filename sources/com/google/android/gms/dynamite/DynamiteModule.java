package com.google.android.gms.dynamite;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.annotation.RecentlyNonNull;
import androidx.annotation.RecentlyNullable;
import b.i.a.f.g.b;
import b.i.a.f.g.c;
import b.i.a.f.g.d;
import b.i.a.f.g.e;
import b.i.a.f.g.f;
import b.i.a.f.g.g;
import b.i.a.f.g.h;
import b.i.a.f.g.i;
import b.i.a.f.g.j;
import b.i.a.f.g.k;
import b.i.a.f.g.l;
import com.google.android.gms.common.util.DynamiteApi;
import dalvik.system.DelegateLastClassLoader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public final class DynamiteModule {
    @Nullable
    public static Boolean d = null;
    @Nullable
    public static String e = null;
    public static int f = -1;
    @Nullable
    public static k j;
    @Nullable
    public static l k;
    public final Context l;
    public static final ThreadLocal<g> g = new ThreadLocal<>();
    public static final ThreadLocal<Long> h = new b();
    public static final i i = new c();
    @RecentlyNonNull
    public static final a a = new d();
    @RecentlyNonNull

    /* renamed from: b  reason: collision with root package name */
    public static final a f2978b = new e();
    @RecentlyNonNull
    public static final a c = new f();

    /* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
    @DynamiteApi
    /* loaded from: classes3.dex */
    public static class DynamiteLoaderClassLoader {
        @RecentlyNullable
        public static ClassLoader sClassLoader;
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
    /* loaded from: classes3.dex */
    public static class LoadingException extends Exception {
        public /* synthetic */ LoadingException(String str) {
            super(str);
        }

        public /* synthetic */ LoadingException(String str, Throwable th) {
            super(str, th);
        }
    }

    /* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
    /* loaded from: classes3.dex */
    public interface a {
        j a(Context context, String str, i iVar) throws LoadingException;
    }

    public DynamiteModule(Context context) {
        Objects.requireNonNull(context, "null reference");
        this.l = context;
    }

    public static int a(@RecentlyNonNull Context context, @RecentlyNonNull String str) {
        try {
            ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 61);
            sb.append("com.google.android.gms.dynamite.descriptors.");
            sb.append(str);
            sb.append(".");
            sb.append("ModuleDescriptor");
            Class<?> loadClass = classLoader.loadClass(sb.toString());
            Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (b.c.a.a0.d.h0(declaredField.get(null), str)) {
                return declaredField2.getInt(null);
            }
            String valueOf = String.valueOf(declaredField.get(null));
            StringBuilder sb2 = new StringBuilder(valueOf.length() + 51 + String.valueOf(str).length());
            sb2.append("Module descriptor id '");
            sb2.append(valueOf);
            sb2.append("' didn't match expected id '");
            sb2.append(str);
            sb2.append("'");
            Log.e("DynamiteModule", sb2.toString());
            return 0;
        } catch (ClassNotFoundException unused) {
            StringBuilder sb3 = new StringBuilder(String.valueOf(str).length() + 45);
            sb3.append("Local module descriptor class for ");
            sb3.append(str);
            sb3.append(" not found.");
            Log.w("DynamiteModule", sb3.toString());
            return 0;
        } catch (Exception e2) {
            String valueOf2 = String.valueOf(e2.getMessage());
            Log.e("DynamiteModule", valueOf2.length() != 0 ? "Failed to load module descriptor class: ".concat(valueOf2) : new String("Failed to load module descriptor class: "));
            return 0;
        }
    }

    @RecentlyNonNull
    public static DynamiteModule c(@RecentlyNonNull Context context, @RecentlyNonNull a aVar, @RecentlyNonNull String str) throws LoadingException {
        Boolean bool;
        DynamiteModule dynamiteModule;
        b.i.a.f.f.a aVar2;
        l lVar;
        Boolean valueOf;
        b.i.a.f.f.a aVar3;
        ThreadLocal<g> threadLocal = g;
        g gVar = threadLocal.get();
        g gVar2 = new g(null);
        threadLocal.set(gVar2);
        ThreadLocal<Long> threadLocal2 = h;
        long longValue = threadLocal2.get().longValue();
        try {
            threadLocal2.set(Long.valueOf(SystemClock.elapsedRealtime()));
            j a2 = aVar.a(context, str, i);
            int i2 = a2.a;
            int i3 = a2.f1398b;
            StringBuilder sb = new StringBuilder(str.length() + 68 + str.length());
            sb.append("Considering local module ");
            sb.append(str);
            sb.append(":");
            sb.append(i2);
            sb.append(" and remote module ");
            sb.append(str);
            sb.append(":");
            sb.append(i3);
            Log.i("DynamiteModule", sb.toString());
            int i4 = a2.c;
            if (i4 == 0 || ((i4 == -1 && a2.a == 0) || (i4 == 1 && a2.f1398b == 0))) {
                int i5 = a2.a;
                int i6 = a2.f1398b;
                StringBuilder sb2 = new StringBuilder(91);
                sb2.append("No acceptable module found. Local version is ");
                sb2.append(i5);
                sb2.append(" and remote version is ");
                sb2.append(i6);
                sb2.append(".");
                throw new LoadingException(sb2.toString());
            } else if (i4 == -1) {
                DynamiteModule g2 = g(context, str);
                if (longValue == 0) {
                    threadLocal2.remove();
                } else {
                    threadLocal2.set(Long.valueOf(longValue));
                }
                Cursor cursor = gVar2.a;
                if (cursor != null) {
                    cursor.close();
                }
                threadLocal.set(gVar);
                return g2;
            } else if (i4 == 1) {
                try {
                    int i7 = a2.f1398b;
                    try {
                        synchronized (DynamiteModule.class) {
                            bool = d;
                        }
                        if (bool != null) {
                            if (bool.booleanValue()) {
                                StringBuilder sb3 = new StringBuilder(str.length() + 51);
                                sb3.append("Selected remote version of ");
                                sb3.append(str);
                                sb3.append(", version >= ");
                                sb3.append(i7);
                                Log.i("DynamiteModule", sb3.toString());
                                synchronized (DynamiteModule.class) {
                                    lVar = k;
                                }
                                if (lVar != null) {
                                    g gVar3 = threadLocal.get();
                                    if (gVar3 == null || gVar3.a == null) {
                                        throw new LoadingException("No result cursor");
                                    }
                                    Context applicationContext = context.getApplicationContext();
                                    Cursor cursor2 = gVar3.a;
                                    new b.i.a.f.f.b(null);
                                    synchronized (DynamiteModule.class) {
                                        valueOf = Boolean.valueOf(f >= 2);
                                    }
                                    if (valueOf.booleanValue()) {
                                        Log.v("DynamiteModule", "Dynamite loader version >= 2, using loadModule2NoCrashUtils");
                                        aVar3 = lVar.t0(new b.i.a.f.f.b(applicationContext), str, i7, new b.i.a.f.f.b(cursor2));
                                    } else {
                                        Log.w("DynamiteModule", "Dynamite loader version < 2, falling back to loadModule2");
                                        aVar3 = lVar.i(new b.i.a.f.f.b(applicationContext), str, i7, new b.i.a.f.f.b(cursor2));
                                    }
                                    Context context2 = (Context) b.i.a.f.f.b.i(aVar3);
                                    if (context2 != null) {
                                        dynamiteModule = new DynamiteModule(context2);
                                    } else {
                                        throw new LoadingException("Failed to get module context");
                                    }
                                } else {
                                    throw new LoadingException("DynamiteLoaderV2 was not cached.");
                                }
                            } else {
                                StringBuilder sb4 = new StringBuilder(str.length() + 51);
                                sb4.append("Selected remote version of ");
                                sb4.append(str);
                                sb4.append(", version >= ");
                                sb4.append(i7);
                                Log.i("DynamiteModule", sb4.toString());
                                k i8 = i(context);
                                if (i8 != null) {
                                    Parcel c2 = i8.c(6, i8.g());
                                    int readInt = c2.readInt();
                                    c2.recycle();
                                    if (readInt >= 3) {
                                        g gVar4 = threadLocal.get();
                                        if (gVar4 != null) {
                                            aVar2 = i8.v0(new b.i.a.f.f.b(context), str, i7, new b.i.a.f.f.b(gVar4.a));
                                        } else {
                                            throw new LoadingException("No cached result cursor holder");
                                        }
                                    } else if (readInt == 2) {
                                        Log.w("DynamiteModule", "IDynamite loader version = 2");
                                        aVar2 = i8.t0(new b.i.a.f.f.b(context), str, i7);
                                    } else {
                                        Log.w("DynamiteModule", "Dynamite loader version < 2, falling back to createModuleContext");
                                        aVar2 = i8.i(new b.i.a.f.f.b(context), str, i7);
                                    }
                                    if (b.i.a.f.f.b.i(aVar2) != null) {
                                        dynamiteModule = new DynamiteModule((Context) b.i.a.f.f.b.i(aVar2));
                                    } else {
                                        throw new LoadingException("Failed to load remote module.");
                                    }
                                } else {
                                    throw new LoadingException("Failed to create IDynamiteLoader.");
                                }
                            }
                            if (longValue == 0) {
                                threadLocal2.remove();
                            } else {
                                threadLocal2.set(Long.valueOf(longValue));
                            }
                            Cursor cursor3 = gVar2.a;
                            if (cursor3 != null) {
                                cursor3.close();
                            }
                            threadLocal.set(gVar);
                            return dynamiteModule;
                        }
                        throw new LoadingException("Failed to determine which loading route to use.");
                    } catch (RemoteException e2) {
                        throw new LoadingException("Failed to load remote module.", e2);
                    } catch (LoadingException e3) {
                        throw e3;
                    } catch (Throwable th) {
                        try {
                            Objects.requireNonNull(context, "null reference");
                        } catch (Exception e4) {
                            Log.e("CrashUtils", "Error adding exception to DropBox!", e4);
                        }
                        throw new LoadingException("Failed to load remote module.", th);
                    }
                } catch (LoadingException e5) {
                    String valueOf2 = String.valueOf(e5.getMessage());
                    Log.w("DynamiteModule", valueOf2.length() != 0 ? "Failed to load remote module: ".concat(valueOf2) : new String("Failed to load remote module: "));
                    int i9 = a2.a;
                    if (i9 == 0 || aVar.a(context, str, new h(i9)).c != -1) {
                        throw new LoadingException("Remote load failed. No local fallback found.", e5);
                    }
                    DynamiteModule g3 = g(context, str);
                    if (longValue == 0) {
                        h.remove();
                    } else {
                        h.set(Long.valueOf(longValue));
                    }
                    Cursor cursor4 = gVar2.a;
                    if (cursor4 != null) {
                        cursor4.close();
                    }
                    g.set(gVar);
                    return g3;
                }
            } else {
                throw new LoadingException("VersionPolicy returned invalid code:0");
            }
        } catch (Throwable th2) {
            if (longValue == 0) {
                h.remove();
            } else {
                h.set(Long.valueOf(longValue));
            }
            Cursor cursor5 = gVar2.a;
            if (cursor5 != null) {
                cursor5.close();
            }
            g.set(gVar);
            throw th2;
        }
    }

    /* JADX WARN: Finally extract failed */
    /* JADX WARN: Multi-variable type inference failed */
    public static int d(@RecentlyNonNull Context context, @RecentlyNonNull String str, boolean z2) {
        Field declaredField;
        ClassLoader classLoader;
        Throwable th;
        Cursor cursor;
        RemoteException e2;
        int i2;
        try {
            synchronized (DynamiteModule.class) {
                Boolean bool = d;
                Cursor cursor2 = null;
                if (bool == null) {
                    try {
                        declaredField = context.getApplicationContext().getClassLoader().loadClass(DynamiteLoaderClassLoader.class.getName()).getDeclaredField("sClassLoader");
                    } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException e3) {
                        String valueOf = String.valueOf(e3);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 30);
                        sb.append("Failed to load module via V2: ");
                        sb.append(valueOf);
                        Log.w("DynamiteModule", sb.toString());
                        bool = Boolean.FALSE;
                    }
                    synchronized (declaredField.getDeclaringClass()) {
                        ClassLoader classLoader2 = (ClassLoader) declaredField.get(null);
                        if (classLoader2 != null) {
                            if (classLoader2 == ClassLoader.getSystemClassLoader()) {
                                bool = Boolean.FALSE;
                            } else {
                                try {
                                    h(classLoader2);
                                } catch (LoadingException unused) {
                                }
                                bool = Boolean.TRUE;
                            }
                        } else if ("com.google.android.gms".equals(context.getApplicationContext().getPackageName())) {
                            declaredField.set(null, ClassLoader.getSystemClassLoader());
                            bool = Boolean.FALSE;
                        } else {
                            try {
                                int e4 = e(context, str, z2);
                                String str2 = e;
                                if (str2 != null && !str2.isEmpty()) {
                                    if (Build.VERSION.SDK_INT >= 29) {
                                        String str3 = e;
                                        Objects.requireNonNull(str3, "null reference");
                                        classLoader = new DelegateLastClassLoader(str3, ClassLoader.getSystemClassLoader());
                                    } else {
                                        String str4 = e;
                                        Objects.requireNonNull(str4, "null reference");
                                        classLoader = new b.i.a.f.g.a(str4, ClassLoader.getSystemClassLoader());
                                    }
                                    h(classLoader);
                                    declaredField.set(null, classLoader);
                                    d = Boolean.TRUE;
                                    return e4;
                                }
                                return e4;
                            } catch (LoadingException unused2) {
                                declaredField.set(null, ClassLoader.getSystemClassLoader());
                                bool = Boolean.FALSE;
                            }
                        }
                        d = bool;
                    }
                }
                if (bool.booleanValue()) {
                    try {
                        return e(context, str, z2);
                    } catch (LoadingException e5) {
                        String valueOf2 = String.valueOf(e5.getMessage());
                        Log.w("DynamiteModule", valueOf2.length() != 0 ? "Failed to retrieve remote module version: ".concat(valueOf2) : new String("Failed to retrieve remote module version: "));
                        return 0;
                    }
                } else {
                    k i3 = i(context);
                    try {
                        if (i3 == null) {
                            return 0;
                        }
                        try {
                            Parcel c2 = i3.c(6, i3.g());
                            int readInt = c2.readInt();
                            c2.recycle();
                            if (readInt >= 3) {
                                cursor = (Cursor) b.i.a.f.f.b.i(i3.u0(new b.i.a.f.f.b(context), str, z2, h.get().longValue()));
                                if (cursor != null) {
                                    try {
                                        if (cursor.moveToFirst()) {
                                            i2 = cursor.getInt(0);
                                            if (i2 <= 0 || !f(cursor)) {
                                                cursor2 = cursor;
                                            }
                                            if (cursor2 != null) {
                                                cursor2.close();
                                            }
                                        }
                                    } catch (RemoteException e6) {
                                        e2 = e6;
                                        cursor2 = cursor;
                                        String valueOf3 = String.valueOf(e2.getMessage());
                                        Log.w("DynamiteModule", valueOf3.length() != 0 ? "Failed to retrieve remote module version: ".concat(valueOf3) : new String("Failed to retrieve remote module version: "));
                                        if (cursor2 == null) {
                                            return 0;
                                        }
                                        cursor2.close();
                                        return 0;
                                    } catch (Throwable th2) {
                                        th = th2;
                                        if (cursor != null) {
                                            cursor.close();
                                        }
                                        throw th;
                                    }
                                }
                                Log.w("DynamiteModule", "Failed to retrieve remote module version.");
                                if (cursor == null) {
                                    return 0;
                                }
                                cursor.close();
                                return 0;
                            } else if (readInt == 2) {
                                Log.w("DynamiteModule", "IDynamite loader version = 2, no high precision latency measurement.");
                                b.i.a.f.f.b bVar = new b.i.a.f.f.b(context);
                                Parcel g2 = i3.g();
                                b.i.a.f.h.g.c.b(g2, bVar);
                                g2.writeString(str);
                                g2.writeInt(z2);
                                Parcel c3 = i3.c(5, g2);
                                i2 = c3.readInt();
                                c3.recycle();
                            } else {
                                Log.w("DynamiteModule", "IDynamite loader version < 2, falling back to getModuleVersion2");
                                b.i.a.f.f.b bVar2 = new b.i.a.f.f.b(context);
                                Parcel g3 = i3.g();
                                b.i.a.f.h.g.c.b(g3, bVar2);
                                g3.writeString(str);
                                g3.writeInt(z2 ? 1 : 0);
                                Parcel c4 = i3.c(3, g3);
                                i2 = c4.readInt();
                                c4.recycle();
                            }
                            return i2;
                        } catch (RemoteException e7) {
                            e2 = e7;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        cursor = null;
                    }
                }
            }
        } catch (Throwable th4) {
            try {
                Objects.requireNonNull(context, "null reference");
            } catch (Exception e8) {
                Log.e("CrashUtils", "Error adding exception to DropBox!", e8);
            }
            throw th4;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:20:0x0074, code lost:
        if (f(r9) != false) goto L22;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static int e(android.content.Context r9, java.lang.String r10, boolean r11) throws com.google.android.gms.dynamite.DynamiteModule.LoadingException {
        /*
            r0 = 0
            java.lang.ThreadLocal<java.lang.Long> r1 = com.google.android.gms.dynamite.DynamiteModule.h     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            java.lang.Object r1 = r1.get()     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            java.lang.Long r1 = (java.lang.Long) r1     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            long r1 = r1.longValue()     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            android.content.ContentResolver r3 = r9.getContentResolver()     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            java.lang.String r9 = "api_force_staging"
            java.lang.String r4 = "api"
            r5 = 1
            if (r5 == r11) goto L19
            r9 = r4
        L19:
            android.net.Uri$Builder r11 = new android.net.Uri$Builder     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            r11.<init>()     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            java.lang.String r4 = "content"
            android.net.Uri$Builder r11 = r11.scheme(r4)     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            java.lang.String r4 = "com.google.android.gms.chimera"
            android.net.Uri$Builder r11 = r11.authority(r4)     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            android.net.Uri$Builder r9 = r11.path(r9)     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            android.net.Uri$Builder r9 = r9.appendPath(r10)     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            java.lang.String r10 = "requestStartTime"
            java.lang.String r11 = java.lang.String.valueOf(r1)     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            android.net.Uri$Builder r9 = r9.appendQueryParameter(r10, r11)     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            android.net.Uri r4 = r9.build()     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r9 = r3.query(r4, r5, r6, r7, r8)     // Catch: java.lang.Throwable -> L96 java.lang.Exception -> L99
            if (r9 == 0) goto L81
            boolean r10 = r9.moveToFirst()     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
            if (r10 == 0) goto L81
            r10 = 0
            int r10 = r9.getInt(r10)     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
            if (r10 <= 0) goto L7a
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r11 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r11)     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
            r1 = 2
            java.lang.String r1 = r9.getString(r1)     // Catch: java.lang.Throwable -> L77
            com.google.android.gms.dynamite.DynamiteModule.e = r1     // Catch: java.lang.Throwable -> L77
            java.lang.String r1 = "loaderVersion"
            int r1 = r9.getColumnIndex(r1)     // Catch: java.lang.Throwable -> L77
            if (r1 < 0) goto L6f
            int r1 = r9.getInt(r1)     // Catch: java.lang.Throwable -> L77
            com.google.android.gms.dynamite.DynamiteModule.f = r1     // Catch: java.lang.Throwable -> L77
        L6f:
            monitor-exit(r11)     // Catch: java.lang.Throwable -> L77
            boolean r11 = f(r9)     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
            if (r11 == 0) goto L7a
            goto L7b
        L77:
            r10 = move-exception
            monitor-exit(r11)     // Catch: java.lang.Throwable -> L77
            throw r10     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
        L7a:
            r0 = r9
        L7b:
            if (r0 == 0) goto L80
            r0.close()
        L80:
            return r10
        L81:
            java.lang.String r10 = "DynamiteModule"
            java.lang.String r11 = "Failed to retrieve remote module version."
            android.util.Log.w(r10, r11)     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r10 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
            java.lang.String r11 = "Failed to connect to dynamite module ContentResolver."
            r10.<init>(r11)     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
            throw r10     // Catch: java.lang.Throwable -> L90 java.lang.Exception -> L93
        L90:
            r10 = move-exception
            r0 = r9
            goto La9
        L93:
            r10 = move-exception
            r0 = r9
            goto L9b
        L96:
            r9 = move-exception
            r10 = r9
            goto La9
        L99:
            r9 = move-exception
            r10 = r9
        L9b:
            boolean r9 = r10 instanceof com.google.android.gms.dynamite.DynamiteModule.LoadingException     // Catch: java.lang.Throwable -> L96
            if (r9 == 0) goto La1
            throw r10     // Catch: java.lang.Throwable -> L96
        La1:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r9 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch: java.lang.Throwable -> L96
            java.lang.String r11 = "V2 version check failed"
            r9.<init>(r11, r10)     // Catch: java.lang.Throwable -> L96
            throw r9     // Catch: java.lang.Throwable -> L96
        La9:
            if (r0 == 0) goto Lae
            r0.close()
        Lae:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.e(android.content.Context, java.lang.String, boolean):int");
    }

    public static boolean f(Cursor cursor) {
        g gVar = g.get();
        if (gVar == null || gVar.a != null) {
            return false;
        }
        gVar.a = cursor;
        return true;
    }

    public static DynamiteModule g(Context context, String str) {
        String valueOf = String.valueOf(str);
        Log.i("DynamiteModule", valueOf.length() != 0 ? "Selected local version of ".concat(valueOf) : new String("Selected local version of "));
        return new DynamiteModule(context.getApplicationContext());
    }

    public static void h(ClassLoader classLoader) throws LoadingException {
        l lVar;
        try {
            IBinder iBinder = (IBinder) classLoader.loadClass("com.google.android.gms.dynamiteloader.DynamiteLoaderV2").getConstructor(new Class[0]).newInstance(new Object[0]);
            if (iBinder == null) {
                lVar = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoaderV2");
                if (queryLocalInterface instanceof l) {
                    lVar = (l) queryLocalInterface;
                } else {
                    lVar = new l(iBinder);
                }
            }
            k = lVar;
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e2) {
            throw new LoadingException("Failed to instantiate dynamite loader", e2);
        }
    }

    @Nullable
    public static k i(Context context) {
        k kVar;
        synchronized (DynamiteModule.class) {
            k kVar2 = j;
            if (kVar2 != null) {
                return kVar2;
            }
            try {
                IBinder iBinder = (IBinder) context.createPackageContext("com.google.android.gms", 3).getClassLoader().loadClass("com.google.android.gms.chimera.container.DynamiteLoaderImpl").newInstance();
                if (iBinder == null) {
                    kVar = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoader");
                    if (queryLocalInterface instanceof k) {
                        kVar = (k) queryLocalInterface;
                    } else {
                        kVar = new k(iBinder);
                    }
                }
                if (kVar != null) {
                    j = kVar;
                    return kVar;
                }
            } catch (Exception e2) {
                String valueOf = String.valueOf(e2.getMessage());
                Log.e("DynamiteModule", valueOf.length() != 0 ? "Failed to load IDynamiteLoader from GmsCore: ".concat(valueOf) : new String("Failed to load IDynamiteLoader from GmsCore: "));
            }
            return null;
        }
    }

    @RecentlyNonNull
    public IBinder b(@RecentlyNonNull String str) throws LoadingException {
        try {
            return (IBinder) this.l.getClassLoader().loadClass(str).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e2) {
            throw new LoadingException(str.length() != 0 ? "Failed to instantiate module class: ".concat(str) : new String("Failed to instantiate module class: "), e2);
        }
    }
}
