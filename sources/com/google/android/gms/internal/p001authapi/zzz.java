package com.google.android.gms.internal.p001authapi;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import b.i.a.f.h.c.u;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
/* compiled from: com.google.android.gms:play-services-auth@@19.0.0 */
/* renamed from: com.google.android.gms.internal.auth-api.zzz  reason: invalid package */
/* loaded from: classes3.dex */
public final class zzz extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzz> CREATOR = new u();
    public final Credential j;

    public zzz(Credential credential) {
        this.j = credential;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        d.s2(parcel, 1, this.j, i, false);
        d.A2(parcel, y2);
    }
}
