package com.google.android.gms.internal.icing;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import b.i.a.f.h.k.o;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class zzk extends AbstractSafeParcelable {
    public final String k;
    public final zzt l;
    public final int m;
    public final byte[] n;
    public static final int j = Integer.parseInt("-1");
    public static final Parcelable.Creator<zzk> CREATOR = new o();

    static {
        ArrayList arrayList = new ArrayList();
        new zzt("SsbContext", "blob", true, 1, false, null, (zzm[]) arrayList.toArray(new zzm[arrayList.size()]), null, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:11:0x0019  */
    /* JADX WARN: Removed duplicated region for block: B:15:0x003c  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x004c  */
    /* JADX WARN: Removed duplicated region for block: B:27:0x0059 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:28:0x005a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public zzk(java.lang.String r7, com.google.android.gms.internal.icing.zzt r8, int r9, byte[] r10) {
        /*
            r6 = this;
            r6.<init>()
            int r0 = com.google.android.gms.internal.icing.zzk.j
            r1 = 0
            if (r9 == r0) goto L1b
            java.lang.String[] r2 = b.i.a.f.h.k.q.a
            if (r9 < 0) goto L15
            java.lang.String[] r2 = b.i.a.f.h.k.q.a
            int r3 = r2.length
            if (r9 < r3) goto L12
            goto L15
        L12:
            r2 = r2[r9]
            goto L16
        L15:
            r2 = r1
        L16:
            if (r2 == 0) goto L19
            goto L1b
        L19:
            r2 = 0
            goto L1c
        L1b:
            r2 = 1
        L1c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r4 = 32
            r3.<init>(r4)
            java.lang.String r5 = "Invalid section type "
            r3.append(r5)
            r3.append(r9)
            java.lang.String r3 = r3.toString()
            b.c.a.a0.d.o(r2, r3)
            r6.k = r7
            r6.l = r8
            r6.m = r9
            r6.n = r10
            if (r9 == r0) goto L51
            java.lang.String[] r8 = b.i.a.f.h.k.q.a
            if (r9 < 0) goto L49
            java.lang.String[] r8 = b.i.a.f.h.k.q.a
            int r0 = r8.length
            if (r9 < r0) goto L46
            goto L49
        L46:
            r8 = r8[r9]
            goto L4a
        L49:
            r8 = r1
        L4a:
            if (r8 != 0) goto L51
            java.lang.String r1 = b.d.b.a.a.f(r4, r5, r9)
            goto L57
        L51:
            if (r7 == 0) goto L57
            if (r10 == 0) goto L57
            java.lang.String r1 = "Both content and blobContent set"
        L57:
            if (r1 != 0) goto L5a
            return
        L5a:
            java.lang.IllegalArgumentException r7 = new java.lang.IllegalArgumentException
            r7.<init>(r1)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.icing.zzk.<init>(java.lang.String, com.google.android.gms.internal.icing.zzt, int, byte[]):void");
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        d.t2(parcel, 1, this.k, false);
        d.s2(parcel, 3, this.l, i, false);
        int i2 = this.m;
        parcel.writeInt(262148);
        parcel.writeInt(i2);
        d.q2(parcel, 5, this.n, false);
        d.A2(parcel, y2);
    }
}
