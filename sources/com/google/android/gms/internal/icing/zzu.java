package com.google.android.gms.internal.icing;

import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import b.i.a.f.h.k.t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class zzu extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzu> CREATOR = new t();
    public final boolean j;

    public zzu(boolean z2) {
        this.j = z2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof zzu) && this.j == ((zzu) obj).j;
    }

    public final int hashCode() {
        return this.j ? 1 : 0;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        boolean z2 = this.j;
        parcel.writeInt(262145);
        parcel.writeInt(z2 ? 1 : 0);
        d.A2(parcel, y2);
    }
}
