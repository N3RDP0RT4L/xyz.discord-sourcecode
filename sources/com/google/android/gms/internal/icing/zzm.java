package com.google.android.gms.internal.icing;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import b.c.a.a0.d;
import b.i.a.f.h.k.p;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Arrays;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class zzm extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzm> CREATOR = new p();
    public final int j;
    public final Bundle k;

    public zzm(int i, Bundle bundle) {
        this.j = i;
        this.k = bundle;
    }

    /* JADX WARN: Removed duplicated region for block: B:26:0x003f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            r0 = 1
            if (r6 != r7) goto L4
            return r0
        L4:
            boolean r1 = r7 instanceof com.google.android.gms.internal.icing.zzm
            r2 = 0
            if (r1 != 0) goto La
            return r2
        La:
            com.google.android.gms.internal.icing.zzm r7 = (com.google.android.gms.internal.icing.zzm) r7
            int r1 = r6.j
            int r3 = r7.j
            if (r1 == r3) goto L13
            return r2
        L13:
            android.os.Bundle r1 = r6.k
            if (r1 != 0) goto L1d
            android.os.Bundle r7 = r7.k
            if (r7 != 0) goto L1c
            return r0
        L1c:
            return r2
        L1d:
            android.os.Bundle r3 = r7.k
            if (r3 != 0) goto L22
            return r2
        L22:
            int r1 = r1.size()
            android.os.Bundle r3 = r7.k
            int r3 = r3.size()
            if (r1 == r3) goto L2f
            return r2
        L2f:
            android.os.Bundle r1 = r6.k
            java.util.Set r1 = r1.keySet()
            java.util.Iterator r1 = r1.iterator()
        L39:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L60
            java.lang.Object r3 = r1.next()
            java.lang.String r3 = (java.lang.String) r3
            android.os.Bundle r4 = r7.k
            boolean r4 = r4.containsKey(r3)
            if (r4 == 0) goto L5f
            android.os.Bundle r4 = r6.k
            java.lang.String r4 = r4.getString(r3)
            android.os.Bundle r5 = r7.k
            java.lang.String r3 = r5.getString(r3)
            boolean r3 = b.c.a.a0.d.h0(r4, r3)
            if (r3 != 0) goto L39
        L5f:
            return r2
        L60:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.icing.zzm.equals(java.lang.Object):boolean");
    }

    public final int hashCode() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf(this.j));
        Bundle bundle = this.k;
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                arrayList.add(str);
                arrayList.add(this.k.getString(str));
            }
        }
        return Arrays.hashCode(arrayList.toArray(new Object[0]));
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        int i2 = this.j;
        parcel.writeInt(262145);
        parcel.writeInt(i2);
        d.p2(parcel, 2, this.k, false);
        d.A2(parcel, y2);
    }
}
