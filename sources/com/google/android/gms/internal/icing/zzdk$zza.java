package com.google.android.gms.internal.icing;

import java.io.IOException;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class zzdk$zza extends IOException {
    public zzdk$zza() {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.");
    }
}
