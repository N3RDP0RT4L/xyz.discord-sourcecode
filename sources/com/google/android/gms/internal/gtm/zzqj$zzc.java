package com.google.android.gms.internal.gtm;

import java.io.IOException;
/* loaded from: classes3.dex */
public final class zzqj$zzc extends IOException {
    public zzqj$zzc() {
        super("CodedOutputStream was writing to a flat byte array and ran out of space.");
    }
}
