package com.google.android.gms.internal.nearby;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.f.h.m.f;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Arrays;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class zzgs extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzgs> CREATOR = new f();
    public static final zzgs j = new zzgs(1, "", null);
    public final int k;
    public final String l;
    @Nullable
    public final String m;

    public zzgs(int i, @Nullable String str, @Nullable String str2) {
        Integer valueOf = Integer.valueOf(i);
        Objects.requireNonNull(valueOf, "null reference");
        this.k = valueOf.intValue();
        this.l = str == null ? "" : str;
        this.m = str2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zzgs)) {
            return false;
        }
        zzgs zzgsVar = (zzgs) obj;
        return d.h0(this.l, zzgsVar.l) && d.h0(this.m, zzgsVar.m);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.l, this.m});
    }

    public final String toString() {
        String str = this.l;
        String str2 = this.m;
        StringBuilder Q = a.Q(a.b(str2, a.b(str, 40)), "NearbyDevice{handle=", str, ", bluetoothAddress=", str2);
        Q.append("}");
        return Q.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int y2 = d.y2(parcel, 20293);
        d.t2(parcel, 3, this.l, false);
        d.t2(parcel, 6, this.m, false);
        int i2 = this.k;
        parcel.writeInt(263144);
        parcel.writeInt(i2);
        d.A2(parcel, y2);
    }
}
