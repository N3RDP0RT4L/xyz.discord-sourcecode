package com.google.android.gms.tasks;

import androidx.annotation.Nullable;
/* compiled from: com.google.android.gms:play-services-tasks@@17.2.1 */
/* loaded from: classes3.dex */
public final class DuplicateTaskCompletionException extends IllegalStateException {
    public static final /* synthetic */ int j = 0;

    public DuplicateTaskCompletionException(String str, @Nullable Throwable th) {
        super(str, th);
    }
}
