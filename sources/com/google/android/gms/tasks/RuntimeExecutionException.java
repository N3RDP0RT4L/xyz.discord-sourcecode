package com.google.android.gms.tasks;

import androidx.annotation.RecentlyNonNull;
/* compiled from: com.google.android.gms:play-services-tasks@@17.2.1 */
/* loaded from: classes3.dex */
public class RuntimeExecutionException extends RuntimeException {
    public RuntimeExecutionException(@RecentlyNonNull Throwable th) {
        super(th);
    }
}
