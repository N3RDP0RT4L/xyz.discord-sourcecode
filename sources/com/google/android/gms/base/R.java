package com.google.android.gms.base;
/* loaded from: classes3.dex */
public final class R {

    /* loaded from: classes3.dex */
    public static final class a {
        public static final int common_full_open_on_phone = 0x7f080131;
        public static final int common_google_signin_btn_icon_dark = 0x7f080132;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f080133;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f080134;
        public static final int common_google_signin_btn_icon_dark_normal_background = 0x7f080135;
        public static final int common_google_signin_btn_icon_disabled = 0x7f080136;
        public static final int common_google_signin_btn_icon_light = 0x7f080137;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f080138;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f080139;
        public static final int common_google_signin_btn_icon_light_normal_background = 0x7f08013a;
        public static final int common_google_signin_btn_text_dark = 0x7f08013b;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f08013c;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f08013d;
        public static final int common_google_signin_btn_text_dark_normal_background = 0x7f08013e;
        public static final int common_google_signin_btn_text_disabled = 0x7f08013f;
        public static final int common_google_signin_btn_text_light = 0x7f080140;
        public static final int common_google_signin_btn_text_light_focused = 0x7f080141;
        public static final int common_google_signin_btn_text_light_normal = 0x7f080142;
        public static final int common_google_signin_btn_text_light_normal_background = 0x7f080143;
        public static final int googleg_disabled_color_18 = 0x7f0802b7;
        public static final int googleg_standard_color_18 = 0x7f0802b8;

        private a() {
        }
    }

    /* loaded from: classes3.dex */
    public static final class b {
        public static final int common_google_play_services_enable_button = 0x7f120608;
        public static final int common_google_play_services_enable_text = 0x7f120609;
        public static final int common_google_play_services_enable_title = 0x7f12060a;
        public static final int common_google_play_services_install_button = 0x7f12060b;
        public static final int common_google_play_services_install_text = 0x7f12060c;
        public static final int common_google_play_services_install_title = 0x7f12060d;
        public static final int common_google_play_services_notification_channel_name = 0x7f12060e;
        public static final int common_google_play_services_notification_ticker = 0x7f12060f;
        public static final int common_google_play_services_unsupported_text = 0x7f120611;
        public static final int common_google_play_services_update_button = 0x7f120612;
        public static final int common_google_play_services_update_text = 0x7f120613;
        public static final int common_google_play_services_update_title = 0x7f120614;
        public static final int common_google_play_services_updating_text = 0x7f120615;
        public static final int common_google_play_services_wear_update_text = 0x7f120616;
        public static final int common_open_on_phone = 0x7f120617;
        public static final int common_signin_button_text = 0x7f120618;
        public static final int common_signin_button_text_long = 0x7f120619;

        private b() {
        }
    }

    private R() {
    }
}
