package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import androidx.annotation.GuardedBy;
import androidx.collection.ArrayMap;
import b.c.a.a0.d;
import b.i.a.f.h.l.c;
import b.i.a.f.h.l.dc;
import b.i.a.f.h.l.ea;
import b.i.a.f.h.l.fc;
import b.i.a.f.h.l.t8;
import b.i.a.f.i.b.a6;
import b.i.a.f.i.b.a7;
import b.i.a.f.i.b.c6;
import b.i.a.f.i.b.d6;
import b.i.a.f.i.b.g6;
import b.i.a.f.i.b.h6;
import b.i.a.f.i.b.h7;
import b.i.a.f.i.b.i6;
import b.i.a.f.i.b.i7;
import b.i.a.f.i.b.l6;
import b.i.a.f.i.b.m6;
import b.i.a.f.i.b.p;
import b.i.a.f.i.b.s6;
import b.i.a.f.i.b.t6;
import b.i.a.f.i.b.t9;
import b.i.a.f.i.b.u4;
import b.i.a.f.i.b.u6;
import b.i.a.f.i.b.v6;
import b.i.a.f.i.b.w5;
import b.i.a.f.i.b.w9;
import b.i.a.f.i.b.x9;
import b.i.a.f.i.b.y6;
import b.i.a.f.i.b.y7;
import b.i.a.f.i.b.z5;
import b.i.a.f.i.b.z8;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.internal.measurement.zzae;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@18.0.0 */
@DynamiteApi
/* loaded from: classes3.dex */
public class AppMeasurementDynamiteService extends dc {
    public u4 a = null;
    @GuardedBy("listenerMap")

    /* renamed from: b  reason: collision with root package name */
    public final Map<Integer, z5> f2981b = new ArrayMap();

    /* compiled from: com.google.android.gms:play-services-measurement-sdk@@18.0.0 */
    /* loaded from: classes3.dex */
    public class a implements w5 {
        public c a;

        public a(c cVar) {
            this.a = cVar;
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-sdk@@18.0.0 */
    /* loaded from: classes3.dex */
    public class b implements z5 {
        public c a;

        public b(c cVar) {
            this.a = cVar;
        }

        @Override // b.i.a.f.i.b.z5
        public final void a(String str, String str2, Bundle bundle, long j) {
            try {
                this.a.Z(str, str2, bundle, j);
            } catch (RemoteException e) {
                AppMeasurementDynamiteService.this.a.g().i.b("Event listener threw exception", e);
            }
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void beginAdUnitExposure(String str, long j) throws RemoteException {
        g();
        this.a.A().v(str, j);
    }

    @Override // b.i.a.f.h.l.ec
    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        g();
        this.a.s().R(str, str2, bundle);
    }

    @Override // b.i.a.f.h.l.ec
    public void clearMeasurementEnabled(long j) throws RemoteException {
        g();
        c6 s2 = this.a.s();
        s2.t();
        s2.f().v(new u6(s2, null));
    }

    @Override // b.i.a.f.h.l.ec
    public void endAdUnitExposure(String str, long j) throws RemoteException {
        g();
        this.a.A().y(str, j);
    }

    public final void g() {
        if (this.a == null) {
            throw new IllegalStateException("Attempting to perform action before initialize.");
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void generateEventId(fc fcVar) throws RemoteException {
        g();
        this.a.t().K(fcVar, this.a.t().t0());
    }

    @Override // b.i.a.f.h.l.ec
    public void getAppInstanceId(fc fcVar) throws RemoteException {
        g();
        this.a.f().v(new a6(this, fcVar));
    }

    @Override // b.i.a.f.h.l.ec
    public void getCachedAppInstanceId(fc fcVar) throws RemoteException {
        g();
        this.a.t().M(fcVar, this.a.s().g.get());
    }

    @Override // b.i.a.f.h.l.ec
    public void getConditionalUserProperties(String str, String str2, fc fcVar) throws RemoteException {
        g();
        this.a.f().v(new z8(this, fcVar, str, str2));
    }

    @Override // b.i.a.f.h.l.ec
    public void getCurrentScreenClass(fc fcVar) throws RemoteException {
        g();
        i7 i7Var = this.a.s().a.w().c;
        this.a.t().M(fcVar, i7Var != null ? i7Var.f1533b : null);
    }

    @Override // b.i.a.f.h.l.ec
    public void getCurrentScreenName(fc fcVar) throws RemoteException {
        g();
        i7 i7Var = this.a.s().a.w().c;
        this.a.t().M(fcVar, i7Var != null ? i7Var.a : null);
    }

    @Override // b.i.a.f.h.l.ec
    public void getGmpAppId(fc fcVar) throws RemoteException {
        g();
        this.a.t().M(fcVar, this.a.s().O());
    }

    @Override // b.i.a.f.h.l.ec
    public void getMaxUserProperties(String str, fc fcVar) throws RemoteException {
        g();
        this.a.s();
        d.w(str);
        this.a.t().J(fcVar, 25);
    }

    @Override // b.i.a.f.h.l.ec
    public void getTestFlag(fc fcVar, int i) throws RemoteException {
        g();
        if (i == 0) {
            t9 t = this.a.t();
            c6 s2 = this.a.s();
            Objects.requireNonNull(s2);
            AtomicReference atomicReference = new AtomicReference();
            t.M(fcVar, (String) s2.f().s(atomicReference, 15000L, "String test flag value", new m6(s2, atomicReference)));
        } else if (i == 1) {
            t9 t2 = this.a.t();
            c6 s3 = this.a.s();
            Objects.requireNonNull(s3);
            AtomicReference atomicReference2 = new AtomicReference();
            t2.K(fcVar, ((Long) s3.f().s(atomicReference2, 15000L, "long test flag value", new t6(s3, atomicReference2))).longValue());
        } else if (i == 2) {
            t9 t3 = this.a.t();
            c6 s4 = this.a.s();
            Objects.requireNonNull(s4);
            AtomicReference atomicReference3 = new AtomicReference();
            double doubleValue = ((Double) s4.f().s(atomicReference3, 15000L, "double test flag value", new v6(s4, atomicReference3))).doubleValue();
            Bundle bundle = new Bundle();
            bundle.putDouble("r", doubleValue);
            try {
                fcVar.f(bundle);
            } catch (RemoteException e) {
                t3.a.g().i.b("Error returning double value to wrapper", e);
            }
        } else if (i == 3) {
            t9 t4 = this.a.t();
            c6 s5 = this.a.s();
            Objects.requireNonNull(s5);
            AtomicReference atomicReference4 = new AtomicReference();
            t4.J(fcVar, ((Integer) s5.f().s(atomicReference4, 15000L, "int test flag value", new s6(s5, atomicReference4))).intValue());
        } else if (i == 4) {
            t9 t5 = this.a.t();
            c6 s6 = this.a.s();
            Objects.requireNonNull(s6);
            AtomicReference atomicReference5 = new AtomicReference();
            t5.O(fcVar, ((Boolean) s6.f().s(atomicReference5, 15000L, "boolean test flag value", new d6(s6, atomicReference5))).booleanValue());
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void getUserProperties(String str, String str2, boolean z2, fc fcVar) throws RemoteException {
        g();
        this.a.f().v(new a7(this, fcVar, str, str2, z2));
    }

    @Override // b.i.a.f.h.l.ec
    public void initForTests(Map map) throws RemoteException {
        g();
    }

    @Override // b.i.a.f.h.l.ec
    public void initialize(b.i.a.f.f.a aVar, zzae zzaeVar, long j) throws RemoteException {
        Context context = (Context) b.i.a.f.f.b.i(aVar);
        u4 u4Var = this.a;
        if (u4Var == null) {
            this.a = u4.b(context, zzaeVar, Long.valueOf(j));
        } else {
            u4Var.g().i.a("Attempting to initialize multiple times");
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void isDataCollectionEnabled(fc fcVar) throws RemoteException {
        g();
        this.a.f().v(new x9(this, fcVar));
    }

    @Override // b.i.a.f.h.l.ec
    public void logEvent(String str, String str2, Bundle bundle, boolean z2, boolean z3, long j) throws RemoteException {
        g();
        this.a.s().I(str, str2, bundle, z2, z3, j);
    }

    @Override // b.i.a.f.h.l.ec
    public void logEventAndBundle(String str, String str2, Bundle bundle, fc fcVar, long j) throws RemoteException {
        g();
        d.w(str2);
        (bundle != null ? new Bundle(bundle) : new Bundle()).putString("_o", "app");
        this.a.f().v(new y7(this, fcVar, new zzaq(str2, new zzap(bundle), "app", j), str));
    }

    @Override // b.i.a.f.h.l.ec
    public void logHealthData(int i, String str, b.i.a.f.f.a aVar, b.i.a.f.f.a aVar2, b.i.a.f.f.a aVar3) throws RemoteException {
        g();
        Object obj = null;
        Object i2 = aVar == null ? null : b.i.a.f.f.b.i(aVar);
        Object i3 = aVar2 == null ? null : b.i.a.f.f.b.i(aVar2);
        if (aVar3 != null) {
            obj = b.i.a.f.f.b.i(aVar3);
        }
        this.a.g().w(i, true, false, str, i2, i3, obj);
    }

    @Override // b.i.a.f.h.l.ec
    public void onActivityCreated(b.i.a.f.f.a aVar, Bundle bundle, long j) throws RemoteException {
        g();
        y6 y6Var = this.a.s().c;
        if (y6Var != null) {
            this.a.s().M();
            y6Var.onActivityCreated((Activity) b.i.a.f.f.b.i(aVar), bundle);
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void onActivityDestroyed(b.i.a.f.f.a aVar, long j) throws RemoteException {
        g();
        y6 y6Var = this.a.s().c;
        if (y6Var != null) {
            this.a.s().M();
            y6Var.onActivityDestroyed((Activity) b.i.a.f.f.b.i(aVar));
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void onActivityPaused(b.i.a.f.f.a aVar, long j) throws RemoteException {
        g();
        y6 y6Var = this.a.s().c;
        if (y6Var != null) {
            this.a.s().M();
            y6Var.onActivityPaused((Activity) b.i.a.f.f.b.i(aVar));
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void onActivityResumed(b.i.a.f.f.a aVar, long j) throws RemoteException {
        g();
        y6 y6Var = this.a.s().c;
        if (y6Var != null) {
            this.a.s().M();
            y6Var.onActivityResumed((Activity) b.i.a.f.f.b.i(aVar));
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void onActivitySaveInstanceState(b.i.a.f.f.a aVar, fc fcVar, long j) throws RemoteException {
        g();
        y6 y6Var = this.a.s().c;
        Bundle bundle = new Bundle();
        if (y6Var != null) {
            this.a.s().M();
            y6Var.onActivitySaveInstanceState((Activity) b.i.a.f.f.b.i(aVar), bundle);
        }
        try {
            fcVar.f(bundle);
        } catch (RemoteException e) {
            this.a.g().i.b("Error returning bundle value to wrapper", e);
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void onActivityStarted(b.i.a.f.f.a aVar, long j) throws RemoteException {
        g();
        if (this.a.s().c != null) {
            this.a.s().M();
            Activity activity = (Activity) b.i.a.f.f.b.i(aVar);
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void onActivityStopped(b.i.a.f.f.a aVar, long j) throws RemoteException {
        g();
        if (this.a.s().c != null) {
            this.a.s().M();
            Activity activity = (Activity) b.i.a.f.f.b.i(aVar);
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void performAction(Bundle bundle, fc fcVar, long j) throws RemoteException {
        g();
        fcVar.f(null);
    }

    @Override // b.i.a.f.h.l.ec
    public void registerOnMeasurementEventListener(c cVar) throws RemoteException {
        z5 z5Var;
        g();
        synchronized (this.f2981b) {
            z5Var = this.f2981b.get(Integer.valueOf(cVar.a()));
            if (z5Var == null) {
                z5Var = new b(cVar);
                this.f2981b.put(Integer.valueOf(cVar.a()), z5Var);
            }
        }
        c6 s2 = this.a.s();
        s2.t();
        if (!s2.e.add(z5Var)) {
            s2.g().i.a("OnEventListener already registered");
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void resetAnalyticsData(long j) throws RemoteException {
        g();
        c6 s2 = this.a.s();
        s2.g.set(null);
        s2.f().v(new l6(s2, j));
    }

    @Override // b.i.a.f.h.l.ec
    public void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException {
        g();
        if (bundle == null) {
            this.a.g().f.a("Conditional user property must not be null");
        } else {
            this.a.s().y(bundle, j);
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void setConsent(Bundle bundle, long j) throws RemoteException {
        g();
        c6 s2 = this.a.s();
        if (t8.b() && s2.a.h.u(null, p.H0)) {
            s2.x(bundle, 30, j);
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void setConsentThirdParty(Bundle bundle, long j) throws RemoteException {
        g();
        c6 s2 = this.a.s();
        if (t8.b() && s2.a.h.u(null, p.I0)) {
            s2.x(bundle, 10, j);
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void setCurrentScreen(b.i.a.f.f.a aVar, String str, String str2, long j) throws RemoteException {
        g();
        h7 w = this.a.w();
        Activity activity = (Activity) b.i.a.f.f.b.i(aVar);
        if (!w.a.h.z().booleanValue()) {
            w.g().k.a("setCurrentScreen cannot be called while screen reporting is disabled.");
        } else if (w.c == null) {
            w.g().k.a("setCurrentScreen cannot be called while no activity active");
        } else if (w.f.get(activity) == null) {
            w.g().k.a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = h7.x(activity.getClass().getCanonicalName());
            }
            boolean q0 = t9.q0(w.c.f1533b, str2);
            boolean q02 = t9.q0(w.c.a, str);
            if (q0 && q02) {
                w.g().k.a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                w.g().k.b("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                w.g().n.c("Setting current screen to name, class", str == null ? "null" : str, str2);
                i7 i7Var = new i7(str, str2, w.e().t0());
                w.f.put(activity, i7Var);
                w.z(activity, i7Var, true);
            } else {
                w.g().k.b("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void setDataCollectionEnabled(boolean z2) throws RemoteException {
        g();
        c6 s2 = this.a.s();
        s2.t();
        s2.f().v(new g6(s2, z2));
    }

    @Override // b.i.a.f.h.l.ec
    public void setDefaultEventParameters(Bundle bundle) {
        g();
        final c6 s2 = this.a.s();
        final Bundle bundle2 = bundle == null ? null : new Bundle(bundle);
        s2.f().v(new Runnable(s2, bundle2) { // from class: b.i.a.f.i.b.b6
            public final c6 j;
            public final Bundle k;

            {
                this.j = s2;
                this.k = bundle2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                boolean z2;
                c6 c6Var = this.j;
                Bundle bundle3 = this.k;
                Objects.requireNonNull(c6Var);
                if (ea.b() && c6Var.a.h.o(p.z0)) {
                    if (bundle3 == null) {
                        c6Var.l().D.b(new Bundle());
                        return;
                    }
                    Bundle a2 = c6Var.l().D.a();
                    for (String str : bundle3.keySet()) {
                        Object obj = bundle3.get(str);
                        if (obj != null && !(obj instanceof String) && !(obj instanceof Long) && !(obj instanceof Double)) {
                            c6Var.e();
                            if (t9.W(obj)) {
                                c6Var.e().R(c6Var.p, 27, null, null, 0);
                            }
                            c6Var.g().k.c("Invalid default event parameter type. Name, value", str, obj);
                        } else if (t9.r0(str)) {
                            c6Var.g().k.b("Invalid default event parameter name. Name", str);
                        } else if (obj == null) {
                            a2.remove(str);
                        } else if (c6Var.e().b0("param", str, 100, obj)) {
                            c6Var.e().I(a2, str, obj);
                        }
                    }
                    c6Var.e();
                    int t = c6Var.a.h.t();
                    if (a2.size() > t) {
                        Iterator it = new TreeSet(a2.keySet()).iterator();
                        int i = 0;
                        while (true) {
                            z2 = true;
                            if (!it.hasNext()) {
                                break;
                            }
                            String str2 = (String) it.next();
                            i++;
                            if (i > t) {
                                a2.remove(str2);
                            }
                        }
                    } else {
                        z2 = false;
                    }
                    if (z2) {
                        c6Var.e().R(c6Var.p, 26, null, null, 0);
                        c6Var.g().k.a("Too many default event parameters set. Discarding beyond event parameter limit");
                    }
                    c6Var.l().D.b(a2);
                    q7 p = c6Var.p();
                    p.b();
                    p.t();
                    p.z(new a8(p, a2, p.I(false)));
                }
            }
        });
    }

    @Override // b.i.a.f.h.l.ec
    public void setEventInterceptor(c cVar) throws RemoteException {
        g();
        a aVar = new a(cVar);
        if (this.a.f().y()) {
            this.a.s().B(aVar);
        } else {
            this.a.f().v(new w9(this, aVar));
        }
    }

    @Override // b.i.a.f.h.l.ec
    public void setInstanceIdProvider(b.i.a.f.h.l.d dVar) throws RemoteException {
        g();
    }

    @Override // b.i.a.f.h.l.ec
    public void setMeasurementEnabled(boolean z2, long j) throws RemoteException {
        g();
        c6 s2 = this.a.s();
        Boolean valueOf = Boolean.valueOf(z2);
        s2.t();
        s2.f().v(new u6(s2, valueOf));
    }

    @Override // b.i.a.f.h.l.ec
    public void setMinimumSessionDuration(long j) throws RemoteException {
        g();
        c6 s2 = this.a.s();
        s2.f().v(new i6(s2, j));
    }

    @Override // b.i.a.f.h.l.ec
    public void setSessionTimeoutDuration(long j) throws RemoteException {
        g();
        c6 s2 = this.a.s();
        s2.f().v(new h6(s2, j));
    }

    @Override // b.i.a.f.h.l.ec
    public void setUserId(String str, long j) throws RemoteException {
        g();
        this.a.s().L(null, "_id", str, true, j);
    }

    @Override // b.i.a.f.h.l.ec
    public void setUserProperty(String str, String str2, b.i.a.f.f.a aVar, boolean z2, long j) throws RemoteException {
        g();
        this.a.s().L(str, str2, b.i.a.f.f.b.i(aVar), z2, j);
    }

    @Override // b.i.a.f.h.l.ec
    public void unregisterOnMeasurementEventListener(c cVar) throws RemoteException {
        z5 remove;
        g();
        synchronized (this.f2981b) {
            remove = this.f2981b.remove(Integer.valueOf(cVar.a()));
        }
        if (remove == null) {
            remove = new b(cVar);
        }
        c6 s2 = this.a.s();
        s2.t();
        if (!s2.e.remove(remove)) {
            s2.g().i.a("OnEventListener had not been registered");
        }
    }
}
