package com.google.android.exoplayer2.metadata.id3;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import b.i.a.c.f3.e0;
import b.i.a.c.p1;
import java.util.Arrays;
/* loaded from: classes3.dex */
public final class ApicFrame extends Id3Frame {
    public static final Parcelable.Creator<ApicFrame> CREATOR = new a();
    public final String k;
    @Nullable
    public final String l;
    public final int m;
    public final byte[] n;

    /* loaded from: classes3.dex */
    public class a implements Parcelable.Creator<ApicFrame> {
        @Override // android.os.Parcelable.Creator
        public ApicFrame createFromParcel(Parcel parcel) {
            return new ApicFrame(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public ApicFrame[] newArray(int i) {
            return new ApicFrame[i];
        }
    }

    public ApicFrame(String str, @Nullable String str2, int i, byte[] bArr) {
        super("APIC");
        this.k = str;
        this.l = str2;
        this.m = i;
        this.n = bArr;
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ApicFrame.class != obj.getClass()) {
            return false;
        }
        ApicFrame apicFrame = (ApicFrame) obj;
        return this.m == apicFrame.m && e0.a(this.k, apicFrame.k) && e0.a(this.l, apicFrame.l) && Arrays.equals(this.n, apicFrame.n);
    }

    public int hashCode() {
        int i = (527 + this.m) * 31;
        String str = this.k;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.l;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return Arrays.hashCode(this.n) + ((hashCode + i2) * 31);
    }

    @Override // com.google.android.exoplayer2.metadata.id3.Id3Frame, com.google.android.exoplayer2.metadata.Metadata.Entry
    public void n(p1.b bVar) {
        bVar.b(this.n, this.m);
    }

    @Override // com.google.android.exoplayer2.metadata.id3.Id3Frame
    public String toString() {
        String str = this.j;
        String str2 = this.k;
        String str3 = this.l;
        StringBuilder Q = b.d.b.a.a.Q(b.d.b.a.a.b(str3, b.d.b.a.a.b(str2, b.d.b.a.a.b(str, 25))), str, ": mimeType=", str2, ", description=");
        Q.append(str3);
        return Q.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeInt(this.m);
        parcel.writeByteArray(this.n);
    }

    public ApicFrame(Parcel parcel) {
        super("APIC");
        String readString = parcel.readString();
        int i = e0.a;
        this.k = readString;
        this.l = parcel.readString();
        this.m = parcel.readInt();
        this.n = parcel.createByteArray();
    }
}
