package com.google.android.exoplayer2.video;

import android.view.Surface;
import androidx.annotation.Nullable;
import b.i.a.c.y2.u;
import com.google.android.exoplayer2.mediacodec.MediaCodecDecoderException;
/* loaded from: classes3.dex */
public class MediaCodecVideoDecoderException extends MediaCodecDecoderException {
    public final boolean isSurfaceValid;
    public final int surfaceIdentityHashCode;

    public MediaCodecVideoDecoderException(Throwable th, @Nullable u uVar, @Nullable Surface surface) {
        super(th, uVar);
        this.surfaceIdentityHashCode = System.identityHashCode(surface);
        this.isSurfaceValid = surface == null || surface.isValid();
    }
}
