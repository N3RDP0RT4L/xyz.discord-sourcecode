package com.google.android.exoplayer2.video;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.view.Surface;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.c.a.a0.d;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import com.google.android.exoplayer2.util.EGLSurfaceTexture;
import com.google.android.exoplayer2.util.GlUtil;
import java.util.Objects;
@RequiresApi(17)
/* loaded from: classes3.dex */
public final class DummySurface extends Surface {
    public static int j;
    public static boolean k;
    public final boolean l;
    public final b m;
    public boolean n;

    /* loaded from: classes3.dex */
    public static class b extends HandlerThread implements Handler.Callback {
        public EGLSurfaceTexture j;
        public Handler k;
        @Nullable
        public Error l;
        @Nullable
        public RuntimeException m;
        @Nullable
        public DummySurface n;

        public b() {
            super("ExoPlayer:DummySurface");
        }

        public final void a(int i) {
            EGLSurface eGLSurface;
            Objects.requireNonNull(this.j);
            EGLSurfaceTexture eGLSurfaceTexture = this.j;
            Objects.requireNonNull(eGLSurfaceTexture);
            boolean z2 = false;
            EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
            if (eglGetDisplay != null) {
                int[] iArr = new int[2];
                if (EGL14.eglInitialize(eglGetDisplay, iArr, 0, iArr, 1)) {
                    eGLSurfaceTexture.m = eglGetDisplay;
                    EGLConfig[] eGLConfigArr = new EGLConfig[1];
                    int[] iArr2 = new int[1];
                    boolean eglChooseConfig = EGL14.eglChooseConfig(eglGetDisplay, EGLSurfaceTexture.j, 0, eGLConfigArr, 0, 1, iArr2, 0);
                    if (!eglChooseConfig || iArr2[0] <= 0 || eGLConfigArr[0] == null) {
                        throw new EGLSurfaceTexture.GlException(e0.k("eglChooseConfig failed: success=%b, numConfigs[0]=%d, configs[0]=%s", Boolean.valueOf(eglChooseConfig), Integer.valueOf(iArr2[0]), eGLConfigArr[0]), null);
                    }
                    EGLConfig eGLConfig = eGLConfigArr[0];
                    EGLContext eglCreateContext = EGL14.eglCreateContext(eGLSurfaceTexture.m, eGLConfig, EGL14.EGL_NO_CONTEXT, i == 0 ? new int[]{12440, 2, 12344} : new int[]{12440, 2, 12992, 1, 12344}, 0);
                    if (eglCreateContext != null) {
                        eGLSurfaceTexture.n = eglCreateContext;
                        EGLDisplay eGLDisplay = eGLSurfaceTexture.m;
                        if (i == 1) {
                            eGLSurface = EGL14.EGL_NO_SURFACE;
                        } else {
                            eGLSurface = EGL14.eglCreatePbufferSurface(eGLDisplay, eGLConfig, i == 2 ? new int[]{12375, 1, 12374, 1, 12992, 1, 12344} : new int[]{12375, 1, 12374, 1, 12344}, 0);
                            if (eGLSurface == null) {
                                throw new EGLSurfaceTexture.GlException("eglCreatePbufferSurface failed", null);
                            }
                        }
                        if (EGL14.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, eglCreateContext)) {
                            eGLSurfaceTexture.o = eGLSurface;
                            GLES20.glGenTextures(1, eGLSurfaceTexture.l, 0);
                            GlUtil.a();
                            SurfaceTexture surfaceTexture = new SurfaceTexture(eGLSurfaceTexture.l[0]);
                            eGLSurfaceTexture.p = surfaceTexture;
                            surfaceTexture.setOnFrameAvailableListener(eGLSurfaceTexture);
                            SurfaceTexture surfaceTexture2 = this.j.p;
                            Objects.requireNonNull(surfaceTexture2);
                            if (i != 0) {
                                z2 = true;
                            }
                            this.n = new DummySurface(this, surfaceTexture2, z2, null);
                            return;
                        }
                        throw new EGLSurfaceTexture.GlException("eglMakeCurrent failed", null);
                    }
                    throw new EGLSurfaceTexture.GlException("eglCreateContext failed", null);
                }
                throw new EGLSurfaceTexture.GlException("eglInitialize failed", null);
            }
            throw new EGLSurfaceTexture.GlException("eglGetDisplay failed", null);
        }

        /* JADX WARN: Finally extract failed */
        public final void b() {
            Objects.requireNonNull(this.j);
            EGLSurfaceTexture eGLSurfaceTexture = this.j;
            eGLSurfaceTexture.k.removeCallbacks(eGLSurfaceTexture);
            try {
                SurfaceTexture surfaceTexture = eGLSurfaceTexture.p;
                if (surfaceTexture != null) {
                    surfaceTexture.release();
                    GLES20.glDeleteTextures(1, eGLSurfaceTexture.l, 0);
                }
                EGLDisplay eGLDisplay = eGLSurfaceTexture.m;
                if (eGLDisplay != null && !eGLDisplay.equals(EGL14.EGL_NO_DISPLAY)) {
                    EGLDisplay eGLDisplay2 = eGLSurfaceTexture.m;
                    EGLSurface eGLSurface = EGL14.EGL_NO_SURFACE;
                    EGL14.eglMakeCurrent(eGLDisplay2, eGLSurface, eGLSurface, EGL14.EGL_NO_CONTEXT);
                }
                EGLSurface eGLSurface2 = eGLSurfaceTexture.o;
                if (eGLSurface2 != null && !eGLSurface2.equals(EGL14.EGL_NO_SURFACE)) {
                    EGL14.eglDestroySurface(eGLSurfaceTexture.m, eGLSurfaceTexture.o);
                }
                EGLContext eGLContext = eGLSurfaceTexture.n;
                if (eGLContext != null) {
                    EGL14.eglDestroyContext(eGLSurfaceTexture.m, eGLContext);
                }
                if (e0.a >= 19) {
                    EGL14.eglReleaseThread();
                }
                EGLDisplay eGLDisplay3 = eGLSurfaceTexture.m;
                if (eGLDisplay3 != null && !eGLDisplay3.equals(EGL14.EGL_NO_DISPLAY)) {
                    EGL14.eglTerminate(eGLSurfaceTexture.m);
                }
                eGLSurfaceTexture.m = null;
                eGLSurfaceTexture.n = null;
                eGLSurfaceTexture.o = null;
                eGLSurfaceTexture.p = null;
            } catch (Throwable th) {
                EGLDisplay eGLDisplay4 = eGLSurfaceTexture.m;
                if (eGLDisplay4 != null && !eGLDisplay4.equals(EGL14.EGL_NO_DISPLAY)) {
                    EGLDisplay eGLDisplay5 = eGLSurfaceTexture.m;
                    EGLSurface eGLSurface3 = EGL14.EGL_NO_SURFACE;
                    EGL14.eglMakeCurrent(eGLDisplay5, eGLSurface3, eGLSurface3, EGL14.EGL_NO_CONTEXT);
                }
                EGLSurface eGLSurface4 = eGLSurfaceTexture.o;
                if (eGLSurface4 != null && !eGLSurface4.equals(EGL14.EGL_NO_SURFACE)) {
                    EGL14.eglDestroySurface(eGLSurfaceTexture.m, eGLSurfaceTexture.o);
                }
                EGLContext eGLContext2 = eGLSurfaceTexture.n;
                if (eGLContext2 != null) {
                    EGL14.eglDestroyContext(eGLSurfaceTexture.m, eGLContext2);
                }
                if (e0.a >= 19) {
                    EGL14.eglReleaseThread();
                }
                EGLDisplay eGLDisplay6 = eGLSurfaceTexture.m;
                if (eGLDisplay6 != null && !eGLDisplay6.equals(EGL14.EGL_NO_DISPLAY)) {
                    EGL14.eglTerminate(eGLSurfaceTexture.m);
                }
                eGLSurfaceTexture.m = null;
                eGLSurfaceTexture.n = null;
                eGLSurfaceTexture.o = null;
                eGLSurfaceTexture.p = null;
                throw th;
            }
        }

        @Override // android.os.Handler.Callback
        public boolean handleMessage(Message message) {
            int i = message.what;
            try {
                if (i == 1) {
                    try {
                        a(message.arg1);
                        synchronized (this) {
                            notify();
                        }
                    } catch (Error e) {
                        q.b("DummySurface", "Failed to initialize dummy surface", e);
                        this.l = e;
                        synchronized (this) {
                            notify();
                        }
                    } catch (RuntimeException e2) {
                        q.b("DummySurface", "Failed to initialize dummy surface", e2);
                        this.m = e2;
                        synchronized (this) {
                            notify();
                        }
                    }
                    return true;
                } else if (i != 2) {
                    return true;
                } else {
                    try {
                        b();
                    } finally {
                        try {
                            return true;
                        } finally {
                        }
                    }
                    return true;
                }
            } catch (Throwable th) {
                synchronized (this) {
                    notify();
                    throw th;
                }
            }
        }
    }

    public DummySurface(b bVar, SurfaceTexture surfaceTexture, boolean z2, a aVar) {
        super(surfaceTexture);
        this.m = bVar;
        this.l = z2;
    }

    public static int a(Context context) {
        String eglQueryString;
        String eglQueryString2;
        int i = e0.a;
        boolean z2 = false;
        if (!(i >= 24 && (i >= 26 || (!"samsung".equals(e0.c) && !"XT1650".equals(e0.d))) && ((i >= 26 || context.getPackageManager().hasSystemFeature("android.hardware.vr.high_performance")) && (eglQueryString2 = EGL14.eglQueryString(EGL14.eglGetDisplay(0), 12373)) != null && eglQueryString2.contains("EGL_EXT_protected_content")))) {
            return 0;
        }
        if (i >= 17 && (eglQueryString = EGL14.eglQueryString(EGL14.eglGetDisplay(0), 12373)) != null && eglQueryString.contains("EGL_KHR_surfaceless_context")) {
            z2 = true;
        }
        return z2 ? 1 : 2;
    }

    public static synchronized boolean b(Context context) {
        boolean z2;
        synchronized (DummySurface.class) {
            z2 = true;
            if (!k) {
                j = a(context);
                k = true;
            }
            if (j == 0) {
                z2 = false;
            }
        }
        return z2;
    }

    public static DummySurface c(Context context, boolean z2) {
        boolean z3 = false;
        d.D(!z2 || b(context));
        b bVar = new b();
        int i = z2 ? j : 0;
        bVar.start();
        Handler handler = new Handler(bVar.getLooper(), bVar);
        bVar.k = handler;
        bVar.j = new EGLSurfaceTexture(handler);
        synchronized (bVar) {
            bVar.k.obtainMessage(1, i, 0).sendToTarget();
            while (bVar.n == null && bVar.m == null && bVar.l == null) {
                try {
                    bVar.wait();
                } catch (InterruptedException unused) {
                    z3 = true;
                }
            }
        }
        if (z3) {
            Thread.currentThread().interrupt();
        }
        RuntimeException runtimeException = bVar.m;
        if (runtimeException == null) {
            Error error = bVar.l;
            if (error == null) {
                DummySurface dummySurface = bVar.n;
                Objects.requireNonNull(dummySurface);
                return dummySurface;
            }
            throw error;
        }
        throw runtimeException;
    }

    @Override // android.view.Surface
    public void release() {
        super.release();
        synchronized (this.m) {
            if (!this.n) {
                b bVar = this.m;
                Objects.requireNonNull(bVar.k);
                bVar.k.sendEmptyMessage(2);
                this.n = true;
            }
        }
    }
}
