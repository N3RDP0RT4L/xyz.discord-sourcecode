package com.google.android.exoplayer2.drm;

import android.annotation.SuppressLint;
import android.media.NotProvisionedException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.i.a.c.a3.t;
import b.i.a.c.e3.w;
import b.i.a.c.f3.k;
import b.i.a.c.f3.l;
import b.i.a.c.f3.q;
import b.i.a.c.w2.a0;
import b.i.a.c.w2.e0;
import b.i.a.c.w2.s;
import b.i.a.c.w2.v;
import b.i.a.c.w2.x;
import b.i.b.b.p;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.google.android.exoplayer2.drm.DefaultDrmSession;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.checkerframework.checker.nullness.qual.EnsuresNonNullIf;
@RequiresApi(18)
/* loaded from: classes3.dex */
public class DefaultDrmSession implements DrmSession {
    @Nullable
    public final List<DrmInitData.SchemeData> a;

    /* renamed from: b  reason: collision with root package name */
    public final a0 f2897b;
    public final a c;
    public final b d;
    public final int e;
    public final boolean f;
    public final boolean g;
    public final HashMap<String, String> h;
    public final l<s.a> i;
    public final w j;
    public final e0 k;
    public final UUID l;
    public final e m;
    public int n;
    public int o;
    @Nullable
    public HandlerThread p;
    @Nullable
    public c q;
    @Nullable
    public b.i.a.c.v2.b r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public DrmSession.DrmSessionException f2898s;
    @Nullable
    public byte[] t;
    public byte[] u;
    @Nullable
    public a0.a v;
    @Nullable
    public a0.d w;

    /* loaded from: classes3.dex */
    public static final class UnexpectedDrmSessionException extends IOException {
        public UnexpectedDrmSessionException(@Nullable Throwable th) {
            super(th);
        }
    }

    /* loaded from: classes3.dex */
    public interface a {
    }

    /* loaded from: classes3.dex */
    public interface b {
    }

    @SuppressLint({"HandlerLeak"})
    /* loaded from: classes3.dex */
    public class c extends Handler {
        @GuardedBy("this")
        public boolean a;

        public c(Looper looper) {
            super(looper);
        }

        public void a(int i, Object obj, boolean z2) {
            obtainMessage(i, new d(t.a.getAndIncrement(), z2, SystemClock.elapsedRealtime(), obj)).sendToTarget();
        }

        /* JADX WARN: Removed duplicated region for block: B:45:0x00ca  */
        /* JADX WARN: Removed duplicated region for block: B:53:0x00dd A[RETURN] */
        /* JADX WARN: Removed duplicated region for block: B:54:0x00de  */
        @Override // android.os.Handler
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public void handleMessage(android.os.Message r11) {
            /*
                Method dump skipped, instructions count: 264
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.drm.DefaultDrmSession.c.handleMessage(android.os.Message):void");
        }
    }

    /* loaded from: classes3.dex */
    public static final class d {
        public final long a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2900b;
        public final long c;
        public final Object d;
        public int e;

        public d(long j, boolean z2, long j2, Object obj) {
            this.a = j;
            this.f2900b = z2;
            this.c = j2;
            this.d = obj;
        }
    }

    @SuppressLint({"HandlerLeak"})
    /* loaded from: classes3.dex */
    public class e extends Handler {
        public e(Looper looper) {
            super(looper);
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            Pair pair = (Pair) message.obj;
            Object obj = pair.first;
            Object obj2 = pair.second;
            int i = message.what;
            if (i == 0) {
                DefaultDrmSession defaultDrmSession = DefaultDrmSession.this;
                if (obj != defaultDrmSession.w) {
                    return;
                }
                if (defaultDrmSession.n == 2 || defaultDrmSession.j()) {
                    defaultDrmSession.w = null;
                    if (obj2 instanceof Exception) {
                        ((DefaultDrmSessionManager.e) defaultDrmSession.c).a((Exception) obj2, false);
                        return;
                    }
                    try {
                        defaultDrmSession.f2897b.j((byte[]) obj2);
                        DefaultDrmSessionManager.e eVar = (DefaultDrmSessionManager.e) defaultDrmSession.c;
                        eVar.f2905b = null;
                        p n = p.n(eVar.a);
                        eVar.a.clear();
                        b.i.b.b.a p = n.listIterator();
                        while (p.hasNext()) {
                            DefaultDrmSession defaultDrmSession2 = (DefaultDrmSession) p.next();
                            if (defaultDrmSession2.m()) {
                                defaultDrmSession2.i(true);
                            }
                        }
                    } catch (Exception e) {
                        ((DefaultDrmSessionManager.e) defaultDrmSession.c).a(e, true);
                    }
                }
            } else if (i == 1) {
                DefaultDrmSession defaultDrmSession3 = DefaultDrmSession.this;
                if (obj == defaultDrmSession3.v && defaultDrmSession3.j()) {
                    defaultDrmSession3.v = null;
                    if (obj2 instanceof Exception) {
                        defaultDrmSession3.l((Exception) obj2, false);
                        return;
                    }
                    try {
                        byte[] bArr = (byte[]) obj2;
                        if (defaultDrmSession3.e == 3) {
                            a0 a0Var = defaultDrmSession3.f2897b;
                            byte[] bArr2 = defaultDrmSession3.u;
                            int i2 = b.i.a.c.f3.e0.a;
                            a0Var.i(bArr2, bArr);
                            defaultDrmSession3.h(b.i.a.c.w2.a.a);
                            return;
                        }
                        byte[] i3 = defaultDrmSession3.f2897b.i(defaultDrmSession3.t, bArr);
                        int i4 = defaultDrmSession3.e;
                        if (!((i4 != 2 && (i4 != 0 || defaultDrmSession3.u == null)) || i3 == null || i3.length == 0)) {
                            defaultDrmSession3.u = i3;
                        }
                        defaultDrmSession3.n = 4;
                        defaultDrmSession3.h(b.i.a.c.w2.p.a);
                    } catch (Exception e2) {
                        defaultDrmSession3.l(e2, true);
                    }
                }
            }
        }
    }

    public DefaultDrmSession(UUID uuid, a0 a0Var, a aVar, b bVar, @Nullable List<DrmInitData.SchemeData> list, int i, boolean z2, boolean z3, @Nullable byte[] bArr, HashMap<String, String> hashMap, e0 e0Var, Looper looper, w wVar) {
        if (i == 1 || i == 3) {
            Objects.requireNonNull(bArr);
        }
        this.l = uuid;
        this.c = aVar;
        this.d = bVar;
        this.f2897b = a0Var;
        this.e = i;
        this.f = z2;
        this.g = z3;
        if (bArr != null) {
            this.u = bArr;
            this.a = null;
        } else {
            Objects.requireNonNull(list);
            this.a = Collections.unmodifiableList(list);
        }
        this.h = hashMap;
        this.k = e0Var;
        this.i = new l<>();
        this.j = wVar;
        this.n = 2;
        this.m = new e(looper);
    }

    @Override // com.google.android.exoplayer2.drm.DrmSession
    public void a(@Nullable s.a aVar) {
        int i = this.o;
        boolean z2 = false;
        if (i < 0) {
            StringBuilder sb = new StringBuilder(51);
            sb.append("Session reference count less than zero: ");
            sb.append(i);
            Log.e("DefaultDrmSession", sb.toString());
            this.o = 0;
        }
        if (aVar != null) {
            l<s.a> lVar = this.i;
            synchronized (lVar.j) {
                ArrayList arrayList = new ArrayList(lVar.m);
                arrayList.add(aVar);
                lVar.m = Collections.unmodifiableList(arrayList);
                Integer num = lVar.k.get(aVar);
                if (num == null) {
                    HashSet hashSet = new HashSet(lVar.l);
                    hashSet.add(aVar);
                    lVar.l = Collections.unmodifiableSet(hashSet);
                }
                lVar.k.put(aVar, Integer.valueOf(num != null ? num.intValue() + 1 : 1));
            }
        }
        int i2 = this.o + 1;
        this.o = i2;
        if (i2 == 1) {
            if (this.n == 2) {
                z2 = true;
            }
            b.c.a.a0.d.D(z2);
            HandlerThread handlerThread = new HandlerThread("ExoPlayer:DrmRequestHandler");
            this.p = handlerThread;
            handlerThread.start();
            this.q = new c(this.p.getLooper());
            if (m()) {
                i(true);
            }
        } else if (aVar != null && j() && this.i.c(aVar) == 1) {
            aVar.d(this.n);
        }
        DefaultDrmSessionManager.f fVar = (DefaultDrmSessionManager.f) this.d;
        DefaultDrmSessionManager defaultDrmSessionManager = DefaultDrmSessionManager.this;
        if (defaultDrmSessionManager.l != -9223372036854775807L) {
            defaultDrmSessionManager.o.remove(this);
            Handler handler = DefaultDrmSessionManager.this.u;
            Objects.requireNonNull(handler);
            handler.removeCallbacksAndMessages(this);
        }
    }

    @Override // com.google.android.exoplayer2.drm.DrmSession
    public void b(@Nullable s.a aVar) {
        int i = this.o;
        if (i <= 0) {
            Log.e("DefaultDrmSession", "release() called on a session that's already fully released.");
            return;
        }
        int i2 = i - 1;
        this.o = i2;
        if (i2 == 0) {
            this.n = 0;
            e eVar = this.m;
            int i3 = b.i.a.c.f3.e0.a;
            eVar.removeCallbacksAndMessages(null);
            c cVar = this.q;
            synchronized (cVar) {
                cVar.removeCallbacksAndMessages(null);
                cVar.a = true;
            }
            this.q = null;
            this.p.quit();
            this.p = null;
            this.r = null;
            this.f2898s = null;
            this.v = null;
            this.w = null;
            byte[] bArr = this.t;
            if (bArr != null) {
                this.f2897b.g(bArr);
                this.t = null;
            }
        }
        if (aVar != null) {
            l<s.a> lVar = this.i;
            synchronized (lVar.j) {
                Integer num = lVar.k.get(aVar);
                if (num != null) {
                    ArrayList arrayList = new ArrayList(lVar.m);
                    arrayList.remove(aVar);
                    lVar.m = Collections.unmodifiableList(arrayList);
                    if (num.intValue() == 1) {
                        lVar.k.remove(aVar);
                        HashSet hashSet = new HashSet(lVar.l);
                        hashSet.remove(aVar);
                        lVar.l = Collections.unmodifiableSet(hashSet);
                    } else {
                        lVar.k.put(aVar, Integer.valueOf(num.intValue() - 1));
                    }
                }
            }
            if (this.i.c(aVar) == 0) {
                aVar.f();
            }
        }
        b bVar = this.d;
        int i4 = this.o;
        DefaultDrmSessionManager.f fVar = (DefaultDrmSessionManager.f) bVar;
        if (i4 == 1) {
            DefaultDrmSessionManager defaultDrmSessionManager = DefaultDrmSessionManager.this;
            if (defaultDrmSessionManager.p > 0 && defaultDrmSessionManager.l != -9223372036854775807L) {
                defaultDrmSessionManager.o.add(this);
                Handler handler = DefaultDrmSessionManager.this.u;
                Objects.requireNonNull(handler);
                handler.postAtTime(new Runnable() { // from class: b.i.a.c.w2.f
                    @Override // java.lang.Runnable
                    public final void run() {
                        DefaultDrmSession.this.b(null);
                    }
                }, this, SystemClock.uptimeMillis() + DefaultDrmSessionManager.this.l);
                DefaultDrmSessionManager.this.k();
            }
        }
        if (i4 == 0) {
            DefaultDrmSessionManager.this.m.remove(this);
            DefaultDrmSessionManager defaultDrmSessionManager2 = DefaultDrmSessionManager.this;
            if (defaultDrmSessionManager2.r == this) {
                defaultDrmSessionManager2.r = null;
            }
            if (defaultDrmSessionManager2.f2902s == this) {
                defaultDrmSessionManager2.f2902s = null;
            }
            DefaultDrmSessionManager.e eVar2 = defaultDrmSessionManager2.i;
            eVar2.a.remove(this);
            if (eVar2.f2905b == this) {
                eVar2.f2905b = null;
                if (!eVar2.a.isEmpty()) {
                    DefaultDrmSession next = eVar2.a.iterator().next();
                    eVar2.f2905b = next;
                    next.o();
                }
            }
            DefaultDrmSessionManager defaultDrmSessionManager3 = DefaultDrmSessionManager.this;
            if (defaultDrmSessionManager3.l != -9223372036854775807L) {
                Handler handler2 = defaultDrmSessionManager3.u;
                Objects.requireNonNull(handler2);
                handler2.removeCallbacksAndMessages(this);
                DefaultDrmSessionManager.this.o.remove(this);
            }
        }
        DefaultDrmSessionManager.this.k();
    }

    @Override // com.google.android.exoplayer2.drm.DrmSession
    public final UUID c() {
        return this.l;
    }

    @Override // com.google.android.exoplayer2.drm.DrmSession
    public boolean d() {
        return this.f;
    }

    @Override // com.google.android.exoplayer2.drm.DrmSession
    public boolean e(String str) {
        a0 a0Var = this.f2897b;
        byte[] bArr = this.t;
        b.c.a.a0.d.H(bArr);
        return a0Var.e(bArr, str);
    }

    @Override // com.google.android.exoplayer2.drm.DrmSession
    @Nullable
    public final DrmSession.DrmSessionException f() {
        if (this.n == 1) {
            return this.f2898s;
        }
        return null;
    }

    @Override // com.google.android.exoplayer2.drm.DrmSession
    @Nullable
    public final b.i.a.c.v2.b g() {
        return this.r;
    }

    @Override // com.google.android.exoplayer2.drm.DrmSession
    public final int getState() {
        return this.n;
    }

    public final void h(k<s.a> kVar) {
        Set<s.a> set;
        l<s.a> lVar = this.i;
        synchronized (lVar.j) {
            set = lVar.l;
        }
        for (s.a aVar : set) {
            kVar.accept(aVar);
        }
    }

    /* JADX WARN: Can't wrap try/catch for region: R(10:36|63|37|(6:39|41|65|42|(1:44)|45)|40|41|65|42|(0)|45) */
    /* JADX WARN: Removed duplicated region for block: B:44:0x0098 A[Catch: NumberFormatException -> 0x009c, TRY_LEAVE, TryCatch #3 {NumberFormatException -> 0x009c, blocks: (B:42:0x0090, B:44:0x0098), top: B:65:0x0090 }] */
    @org.checkerframework.checker.nullness.qual.RequiresNonNull({"sessionId"})
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void i(boolean r10) {
        /*
            Method dump skipped, instructions count: 248
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.drm.DefaultDrmSession.i(boolean):void");
    }

    @EnsuresNonNullIf(expression = {"sessionId"}, result = GoogleSmartLockManager.SET_DISCORD_ACCOUNT_DETAILS)
    public final boolean j() {
        int i = this.n;
        return i == 3 || i == 4;
    }

    public final void k(final Exception exc, int i) {
        int i2;
        int i3 = b.i.a.c.f3.e0.a;
        if (i3 < 21 || !b.i.a.c.w2.w.a(exc)) {
            if (i3 < 23 || !x.a(exc)) {
                if (i3 < 18 || !v.b(exc)) {
                    if (i3 >= 18 && v.a(exc)) {
                        i2 = 6007;
                    } else if (exc instanceof UnsupportedDrmException) {
                        i2 = 6001;
                    } else if (exc instanceof DefaultDrmSessionManager.MissingSchemeDataException) {
                        i2 = 6003;
                    } else if (exc instanceof KeysExpiredException) {
                        i2 = 6008;
                    } else if (i != 1) {
                        if (i == 2) {
                            i2 = 6004;
                        } else if (i != 3) {
                            throw new IllegalArgumentException();
                        }
                    }
                }
                i2 = 6002;
            }
            i2 = 6006;
        } else {
            i2 = b.i.a.c.w2.w.b(exc);
        }
        this.f2898s = new DrmSession.DrmSessionException(exc, i2);
        q.b("DefaultDrmSession", "DRM session error", exc);
        h(new k() { // from class: b.i.a.c.w2.b
            @Override // b.i.a.c.f3.k
            public final void accept(Object obj) {
                ((s.a) obj).e(exc);
            }
        });
        if (this.n != 4) {
            this.n = 1;
        }
    }

    public final void l(Exception exc, boolean z2) {
        if (exc instanceof NotProvisionedException) {
            DefaultDrmSessionManager.e eVar = (DefaultDrmSessionManager.e) this.c;
            eVar.a.add(this);
            if (eVar.f2905b == null) {
                eVar.f2905b = this;
                o();
                return;
            }
            return;
        }
        k(exc, z2 ? 1 : 2);
    }

    @EnsuresNonNullIf(expression = {"sessionId"}, result = GoogleSmartLockManager.SET_DISCORD_ACCOUNT_DETAILS)
    public final boolean m() {
        if (j()) {
            return true;
        }
        try {
            byte[] d2 = this.f2897b.d();
            this.t = d2;
            this.r = this.f2897b.c(d2);
            this.n = 3;
            h(new k() { // from class: b.i.a.c.w2.c
                @Override // b.i.a.c.f3.k
                public final void accept(Object obj) {
                    ((s.a) obj).d(r1);
                }
            });
            Objects.requireNonNull(this.t);
            return true;
        } catch (NotProvisionedException unused) {
            DefaultDrmSessionManager.e eVar = (DefaultDrmSessionManager.e) this.c;
            eVar.a.add(this);
            if (eVar.f2905b != null) {
                return false;
            }
            eVar.f2905b = this;
            o();
            return false;
        } catch (Exception e2) {
            k(e2, 1);
            return false;
        }
    }

    public final void n(byte[] bArr, int i, boolean z2) {
        try {
            a0.a k = this.f2897b.k(bArr, this.a, i, this.h);
            this.v = k;
            c cVar = this.q;
            int i2 = b.i.a.c.f3.e0.a;
            Objects.requireNonNull(k);
            cVar.a(1, k, z2);
        } catch (Exception e2) {
            l(e2, true);
        }
    }

    public void o() {
        a0.d b2 = this.f2897b.b();
        this.w = b2;
        c cVar = this.q;
        int i = b.i.a.c.f3.e0.a;
        Objects.requireNonNull(b2);
        cVar.a(0, b2, true);
    }

    @Nullable
    public Map<String, String> p() {
        byte[] bArr = this.t;
        if (bArr == null) {
            return null;
        }
        return this.f2897b.a(bArr);
    }
}
