package com.google.android.exoplayer2.drm;

import android.annotation.SuppressLint;
import android.media.ResourceBusyException;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.i.a.c.e3.w;
import b.i.a.c.f3.q;
import b.i.a.c.f3.t;
import b.i.a.c.j1;
import b.i.a.c.w2.a0;
import b.i.a.c.w2.b0;
import b.i.a.c.w2.e0;
import b.i.a.c.w2.s;
import b.i.a.c.w2.u;
import b.i.a.c.w2.z;
import b.i.a.c.x0;
import b.i.b.b.h;
import b.i.b.b.h0;
import b.i.b.b.p;
import b.i.b.b.r;
import com.google.android.exoplayer2.drm.DefaultDrmSession;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
@RequiresApi(18)
/* loaded from: classes3.dex */
public class DefaultDrmSessionManager implements u {

    /* renamed from: b  reason: collision with root package name */
    public final UUID f2901b;
    public final a0.c c;
    public final e0 d;
    public final HashMap<String, String> e;
    public final boolean f;
    public final int[] g;
    public final boolean h;
    public final w j;
    public final long l;
    public int p;
    @Nullable
    public a0 q;
    @Nullable
    public DefaultDrmSession r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public DefaultDrmSession f2902s;
    public Looper t;
    public Handler u;
    @Nullable
    public byte[] w;
    @Nullable

    /* renamed from: x  reason: collision with root package name */
    public volatile c f2903x;
    public final e i = new e();
    public final f k = new f(null);
    public int v = 0;
    public final List<DefaultDrmSession> m = new ArrayList();
    public final Set<d> n = h.c();
    public final Set<DefaultDrmSession> o = h.c();

    /* loaded from: classes3.dex */
    public static final class MissingSchemeDataException extends Exception {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public MissingSchemeDataException(java.util.UUID r2, com.google.android.exoplayer2.drm.DefaultDrmSessionManager.a r3) {
            /*
                r1 = this;
                java.lang.String r2 = java.lang.String.valueOf(r2)
                int r3 = r2.length()
                int r3 = r3 + 29
                java.lang.String r0 = "Media does not support uuid: "
                java.lang.String r2 = b.d.b.a.a.i(r3, r0, r2)
                r1.<init>(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.drm.DefaultDrmSessionManager.MissingSchemeDataException.<init>(java.util.UUID, com.google.android.exoplayer2.drm.DefaultDrmSessionManager$a):void");
        }
    }

    /* loaded from: classes3.dex */
    public class b implements a0.b {
        public b(a aVar) {
        }
    }

    @SuppressLint({"HandlerLeak"})
    /* loaded from: classes3.dex */
    public class c extends Handler {
        public c(Looper looper) {
            super(looper);
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            byte[] bArr = (byte[]) message.obj;
            if (bArr != null) {
                for (DefaultDrmSession defaultDrmSession : DefaultDrmSessionManager.this.m) {
                    if (Arrays.equals(defaultDrmSession.t, bArr)) {
                        if (message.what == 2 && defaultDrmSession.e == 0 && defaultDrmSession.n == 4) {
                            int i = b.i.a.c.f3.e0.a;
                            defaultDrmSession.i(false);
                            return;
                        }
                        return;
                    }
                }
            }
        }
    }

    /* loaded from: classes3.dex */
    public class d implements u.b {
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public final s.a f2904b;
        @Nullable
        public DrmSession c;
        public boolean d;

        public d(@Nullable s.a aVar) {
            this.f2904b = aVar;
        }

        @Override // b.i.a.c.w2.u.b
        public void release() {
            Handler handler = DefaultDrmSessionManager.this.u;
            Objects.requireNonNull(handler);
            b.i.a.c.f3.e0.E(handler, new b.i.a.c.w2.d(this));
        }
    }

    /* loaded from: classes3.dex */
    public class e implements DefaultDrmSession.a {
        public final Set<DefaultDrmSession> a = new HashSet();
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public DefaultDrmSession f2905b;

        public void a(Exception exc, boolean z2) {
            this.f2905b = null;
            p n = p.n(this.a);
            this.a.clear();
            b.i.b.b.a p = n.listIterator();
            while (p.hasNext()) {
                ((DefaultDrmSession) p.next()).k(exc, z2 ? 1 : 3);
            }
        }
    }

    /* loaded from: classes3.dex */
    public class f implements DefaultDrmSession.b {
        public f(a aVar) {
        }
    }

    public DefaultDrmSessionManager(UUID uuid, a0.c cVar, e0 e0Var, HashMap hashMap, boolean z2, int[] iArr, boolean z3, w wVar, long j, a aVar) {
        Objects.requireNonNull(uuid);
        b.c.a.a0.d.m(!x0.f1154b.equals(uuid), "Use C.CLEARKEY_UUID instead");
        this.f2901b = uuid;
        this.c = cVar;
        this.d = e0Var;
        this.e = hashMap;
        this.f = z2;
        this.g = iArr;
        this.h = z3;
        this.j = wVar;
        this.l = j;
    }

    public static boolean f(DrmSession drmSession) {
        DefaultDrmSession defaultDrmSession = (DefaultDrmSession) drmSession;
        if (defaultDrmSession.n == 1) {
            if (b.i.a.c.f3.e0.a < 19) {
                return true;
            }
            DrmSession.DrmSessionException f2 = defaultDrmSession.f();
            Objects.requireNonNull(f2);
            if (f2.getCause() instanceof ResourceBusyException) {
                return true;
            }
        }
        return false;
    }

    public static List<DrmInitData.SchemeData> i(DrmInitData drmInitData, UUID uuid, boolean z2) {
        ArrayList arrayList = new ArrayList(drmInitData.m);
        for (int i = 0; i < drmInitData.m; i++) {
            DrmInitData.SchemeData schemeData = drmInitData.j[i];
            if ((schemeData.a(uuid) || (x0.c.equals(uuid) && schemeData.a(x0.f1154b))) && (schemeData.n != null || z2)) {
                arrayList.add(schemeData);
            }
        }
        return arrayList;
    }

    @Override // b.i.a.c.w2.u
    public final void a() {
        int i = this.p;
        this.p = i + 1;
        if (i == 0) {
            if (this.q == null) {
                a0 a2 = this.c.a(this.f2901b);
                this.q = a2;
                a2.h(new b(null));
            } else if (this.l != -9223372036854775807L) {
                for (int i2 = 0; i2 < this.m.size(); i2++) {
                    this.m.get(i2).a(null);
                }
            }
        }
    }

    @Override // b.i.a.c.w2.u
    public u.b b(Looper looper, @Nullable s.a aVar, final j1 j1Var) {
        b.c.a.a0.d.D(this.p > 0);
        j(looper);
        final d dVar = new d(aVar);
        Handler handler = this.u;
        Objects.requireNonNull(handler);
        handler.post(new Runnable() { // from class: b.i.a.c.w2.e
            @Override // java.lang.Runnable
            public final void run() {
                DefaultDrmSessionManager.d dVar2 = DefaultDrmSessionManager.d.this;
                j1 j1Var2 = j1Var;
                DefaultDrmSessionManager defaultDrmSessionManager = DefaultDrmSessionManager.this;
                if (defaultDrmSessionManager.p != 0 && !dVar2.d) {
                    Looper looper2 = defaultDrmSessionManager.t;
                    Objects.requireNonNull(looper2);
                    dVar2.c = defaultDrmSessionManager.e(looper2, dVar2.f2904b, j1Var2, false);
                    DefaultDrmSessionManager.this.n.add(dVar2);
                }
            }
        });
        return dVar;
    }

    @Override // b.i.a.c.w2.u
    @Nullable
    public DrmSession c(Looper looper, @Nullable s.a aVar, j1 j1Var) {
        b.c.a.a0.d.D(this.p > 0);
        j(looper);
        return e(looper, aVar, j1Var, true);
    }

    /* JADX WARN: Removed duplicated region for block: B:36:0x0093 A[ORIG_RETURN, RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    @Override // b.i.a.c.w2.u
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int d(b.i.a.c.j1 r7) {
        /*
            r6 = this;
            b.i.a.c.w2.a0 r0 = r6.q
            java.util.Objects.requireNonNull(r0)
            int r0 = r0.l()
            com.google.android.exoplayer2.drm.DrmInitData r1 = r7.f1016z
            r2 = 0
            if (r1 != 0) goto L2b
            java.lang.String r7 = r7.w
            int r7 = b.i.a.c.f3.t.g(r7)
            int[] r1 = r6.g
            int r2 = b.i.a.c.f3.e0.a
            r2 = 0
        L19:
            int r3 = r1.length
            r4 = -1
            if (r2 >= r3) goto L25
            r3 = r1[r2]
            if (r3 != r7) goto L22
            goto L26
        L22:
            int r2 = r2 + 1
            goto L19
        L25:
            r2 = -1
        L26:
            if (r2 == r4) goto L29
            goto L2a
        L29:
            r0 = 0
        L2a:
            return r0
        L2b:
            byte[] r7 = r6.w
            r3 = 1
            if (r7 == 0) goto L31
            goto L8f
        L31:
            java.util.UUID r7 = r6.f2901b
            java.util.List r7 = i(r1, r7, r3)
            java.util.ArrayList r7 = (java.util.ArrayList) r7
            boolean r7 = r7.isEmpty()
            if (r7 == 0) goto L62
            int r7 = r1.m
            if (r7 != r3) goto L90
            com.google.android.exoplayer2.drm.DrmInitData$SchemeData[] r7 = r1.j
            r7 = r7[r2]
            java.util.UUID r3 = b.i.a.c.x0.f1154b
            boolean r7 = r7.a(r3)
            if (r7 == 0) goto L90
            java.util.UUID r7 = r6.f2901b
            java.lang.String r7 = java.lang.String.valueOf(r7)
            int r3 = r7.length()
            int r3 = r3 + 72
            java.lang.String r4 = "DrmInitData only contains common PSSH SchemeData. Assuming support for: "
            java.lang.String r5 = "DefaultDrmSessionMgr"
            b.d.b.a.a.f0(r3, r4, r7, r5)
        L62:
            java.lang.String r7 = r1.l
            if (r7 == 0) goto L8f
            java.lang.String r1 = "cenc"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L6f
            goto L8f
        L6f:
            java.lang.String r1 = "cbcs"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L7e
            int r7 = b.i.a.c.f3.e0.a
            r1 = 25
            if (r7 < r1) goto L90
            goto L8f
        L7e:
            java.lang.String r1 = "cbc1"
            boolean r1 = r1.equals(r7)
            if (r1 != 0) goto L90
            java.lang.String r1 = "cens"
            boolean r7 = r1.equals(r7)
            if (r7 == 0) goto L8f
            goto L90
        L8f:
            r2 = 1
        L90:
            if (r2 == 0) goto L93
            goto L94
        L93:
            r0 = 1
        L94:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.drm.DefaultDrmSessionManager.d(b.i.a.c.j1):int");
    }

    @Nullable
    public final DrmSession e(Looper looper, @Nullable s.a aVar, j1 j1Var, boolean z2) {
        List<DrmInitData.SchemeData> list;
        if (this.f2903x == null) {
            this.f2903x = new c(looper);
        }
        DrmInitData drmInitData = j1Var.f1016z;
        DefaultDrmSession defaultDrmSession = null;
        int i = 0;
        if (drmInitData == null) {
            int g = t.g(j1Var.w);
            a0 a0Var = this.q;
            Objects.requireNonNull(a0Var);
            if (a0Var.l() == 2 && b0.a) {
                return null;
            }
            int[] iArr = this.g;
            int i2 = b.i.a.c.f3.e0.a;
            while (true) {
                if (i >= iArr.length) {
                    i = -1;
                    break;
                } else if (iArr[i] == g) {
                    break;
                } else {
                    i++;
                }
            }
            if (i == -1 || a0Var.l() == 1) {
                return null;
            }
            DefaultDrmSession defaultDrmSession2 = this.r;
            if (defaultDrmSession2 == null) {
                b.i.b.b.a<Object> aVar2 = p.k;
                DefaultDrmSession h = h(h0.l, true, null, z2);
                this.m.add(h);
                this.r = h;
            } else {
                defaultDrmSession2.a(null);
            }
            return this.r;
        }
        if (this.w == null) {
            list = i(drmInitData, this.f2901b, false);
            if (((ArrayList) list).isEmpty()) {
                MissingSchemeDataException missingSchemeDataException = new MissingSchemeDataException(this.f2901b, null);
                q.b("DefaultDrmSessionMgr", "DRM error", missingSchemeDataException);
                if (aVar != null) {
                    aVar.e(missingSchemeDataException);
                }
                return new z(new DrmSession.DrmSessionException(missingSchemeDataException, 6003));
            }
        } else {
            list = null;
        }
        if (this.f) {
            Iterator<DefaultDrmSession> it = this.m.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                DefaultDrmSession next = it.next();
                if (b.i.a.c.f3.e0.a(next.a, list)) {
                    defaultDrmSession = next;
                    break;
                }
            }
        } else {
            defaultDrmSession = this.f2902s;
        }
        if (defaultDrmSession == null) {
            defaultDrmSession = h(list, false, aVar, z2);
            if (!this.f) {
                this.f2902s = defaultDrmSession;
            }
            this.m.add(defaultDrmSession);
        } else {
            defaultDrmSession.a(aVar);
        }
        return defaultDrmSession;
    }

    public final DefaultDrmSession g(@Nullable List<DrmInitData.SchemeData> list, boolean z2, @Nullable s.a aVar) {
        Objects.requireNonNull(this.q);
        UUID uuid = this.f2901b;
        a0 a0Var = this.q;
        e eVar = this.i;
        f fVar = this.k;
        int i = this.v;
        byte[] bArr = this.w;
        HashMap<String, String> hashMap = this.e;
        e0 e0Var = this.d;
        Looper looper = this.t;
        Objects.requireNonNull(looper);
        DefaultDrmSession defaultDrmSession = new DefaultDrmSession(uuid, a0Var, eVar, fVar, list, i, this.h | z2, z2, bArr, hashMap, e0Var, looper, this.j);
        defaultDrmSession.a(aVar);
        if (this.l != -9223372036854775807L) {
            defaultDrmSession.a(null);
        }
        return defaultDrmSession;
    }

    public final DefaultDrmSession h(@Nullable List<DrmInitData.SchemeData> list, boolean z2, @Nullable s.a aVar, boolean z3) {
        DefaultDrmSession g = g(list, z2, aVar);
        if (f(g) && !this.o.isEmpty()) {
            l();
            g.b(aVar);
            if (this.l != -9223372036854775807L) {
                g.b(null);
            }
            g = g(list, z2, aVar);
        }
        if (!f(g) || !z3 || this.n.isEmpty()) {
            return g;
        }
        m();
        if (!this.o.isEmpty()) {
            l();
        }
        g.b(aVar);
        if (this.l != -9223372036854775807L) {
            g.b(null);
        }
        return g(list, z2, aVar);
    }

    @EnsuresNonNull({"this.playbackLooper", "this.playbackHandler"})
    public final synchronized void j(Looper looper) {
        Looper looper2 = this.t;
        if (looper2 == null) {
            this.t = looper;
            this.u = new Handler(looper);
        } else {
            b.c.a.a0.d.D(looper2 == looper);
            Objects.requireNonNull(this.u);
        }
    }

    public final void k() {
        if (this.q != null && this.p == 0 && this.m.isEmpty() && this.n.isEmpty()) {
            a0 a0Var = this.q;
            Objects.requireNonNull(a0Var);
            a0Var.release();
            this.q = null;
        }
    }

    public final void l() {
        Iterator j = r.m(this.o).iterator();
        while (j.hasNext()) {
            ((DrmSession) j.next()).b(null);
        }
    }

    public final void m() {
        Iterator j = r.m(this.n).iterator();
        while (j.hasNext()) {
            d dVar = (d) j.next();
            Handler handler = DefaultDrmSessionManager.this.u;
            Objects.requireNonNull(handler);
            b.i.a.c.f3.e0.E(handler, new b.i.a.c.w2.d(dVar));
        }
    }

    @Override // b.i.a.c.w2.u
    public final void release() {
        int i = this.p - 1;
        this.p = i;
        if (i == 0) {
            if (this.l != -9223372036854775807L) {
                ArrayList arrayList = new ArrayList(this.m);
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    ((DefaultDrmSession) arrayList.get(i2)).b(null);
                }
            }
            m();
            k();
        }
    }
}
