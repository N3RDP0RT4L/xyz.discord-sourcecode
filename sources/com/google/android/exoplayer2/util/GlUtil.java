package com.google.android.exoplayer2.util;

import android.opengl.GLES20;
import android.opengl.GLU;
import android.text.TextUtils;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
/* loaded from: classes3.dex */
public final class GlUtil {

    /* loaded from: classes3.dex */
    public static final class GlException extends RuntimeException {
    }

    /* loaded from: classes3.dex */
    public static final class UnsupportedEglVersionException extends Exception {
    }

    public static void a() {
        int i = 0;
        while (true) {
            int glGetError = GLES20.glGetError();
            if (glGetError == 0) {
                break;
            }
            String valueOf = String.valueOf(GLU.gluErrorString(glGetError));
            Log.e("GlUtil", valueOf.length() != 0 ? "glError ".concat(valueOf) : new String("glError "));
            i = glGetError;
        }
        if (i != 0) {
            String valueOf2 = String.valueOf(GLU.gluErrorString(i));
            Log.e("GlUtil", valueOf2.length() != 0 ? "glError ".concat(valueOf2) : new String("glError "));
        }
    }

    public static FloatBuffer b(float[] fArr) {
        return (FloatBuffer) ByteBuffer.allocateDirect(fArr.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer().put(fArr).flip();
    }

    /* loaded from: classes3.dex */
    public static final class a {
        public final int a = GLES20.glCreateProgram();

        public a(String str, String str2) {
            GlUtil.a();
            a(35633, str);
            a(35632, str2);
        }

        public final void a(int i, String str) {
            int glCreateShader = GLES20.glCreateShader(i);
            GLES20.glShaderSource(glCreateShader, str);
            GLES20.glCompileShader(glCreateShader);
            int[] iArr = {0};
            GLES20.glGetShaderiv(glCreateShader, 35713, iArr, 0);
            if (iArr[0] != 1) {
                String glGetShaderInfoLog = GLES20.glGetShaderInfoLog(glCreateShader);
                StringBuilder sb = new StringBuilder(b.d.b.a.a.b(str, b.d.b.a.a.b(glGetShaderInfoLog, 10)));
                sb.append(glGetShaderInfoLog);
                sb.append(", source: ");
                sb.append(str);
                Log.e("GlUtil", sb.toString());
            }
            GLES20.glAttachShader(this.a, glCreateShader);
            GLES20.glDeleteShader(glCreateShader);
            GlUtil.a();
        }

        public int b(String str) {
            return GLES20.glGetUniformLocation(this.a, str);
        }

        public void c() {
            GLES20.glLinkProgram(this.a);
            int[] iArr = {0};
            GLES20.glGetProgramiv(this.a, 35714, iArr, 0);
            if (iArr[0] != 1) {
                String valueOf = String.valueOf(GLES20.glGetProgramInfoLog(this.a));
                Log.e("GlUtil", valueOf.length() != 0 ? "Unable to link shader program: \n".concat(valueOf) : new String("Unable to link shader program: \n"));
            }
            GlUtil.a();
            GLES20.glUseProgram(this.a);
        }

        public a(String[] strArr, String[] strArr2) {
            String join = TextUtils.join("\n", strArr);
            String join2 = TextUtils.join("\n", strArr2);
            GlUtil.a();
            a(35633, join);
            a(35632, join2);
        }
    }
}
