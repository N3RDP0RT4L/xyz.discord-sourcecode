package com.google.android.exoplayer2.source;

import java.io.IOException;
/* loaded from: classes3.dex */
public final class MergingMediaSource$IllegalMergeException extends IOException {
    public final int reason;
}
