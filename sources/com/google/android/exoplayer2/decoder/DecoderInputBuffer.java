package com.google.android.exoplayer2.decoder;

import androidx.annotation.Nullable;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.i.a.c.i1;
import b.i.a.c.v2.a;
import b.i.a.c.v2.c;
import java.nio.ByteBuffer;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
/* loaded from: classes3.dex */
public class DecoderInputBuffer extends a {
    @Nullable
    public ByteBuffer l;
    public boolean m;
    public long n;
    @Nullable
    public ByteBuffer o;
    public final int p;
    public final c k = new c();
    public final int q = 0;

    /* loaded from: classes3.dex */
    public static final class InsufficientCapacityException extends IllegalStateException {
        public final int currentCapacity;
        public final int requiredCapacity;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public InsufficientCapacityException(int r3, int r4) {
            /*
                r2 = this;
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r1 = 44
                r0.<init>(r1)
                java.lang.String r1 = "Buffer too small ("
                r0.append(r1)
                r0.append(r3)
                java.lang.String r1 = " < "
                r0.append(r1)
                r0.append(r4)
                java.lang.String r1 = ")"
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r2.<init>(r0)
                r2.currentCapacity = r3
                r2.requiredCapacity = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.decoder.DecoderInputBuffer.InsufficientCapacityException.<init>(int, int):void");
        }
    }

    static {
        i1.a("goog.exo.decoder");
    }

    public DecoderInputBuffer(int i) {
        this.p = i;
    }

    public void p() {
        this.j = 0;
        ByteBuffer byteBuffer = this.l;
        if (byteBuffer != null) {
            byteBuffer.clear();
        }
        ByteBuffer byteBuffer2 = this.o;
        if (byteBuffer2 != null) {
            byteBuffer2.clear();
        }
        this.m = false;
    }

    public final ByteBuffer q(int i) {
        int i2 = this.p;
        if (i2 == 1) {
            return ByteBuffer.allocate(i);
        }
        if (i2 == 2) {
            return ByteBuffer.allocateDirect(i);
        }
        ByteBuffer byteBuffer = this.l;
        throw new InsufficientCapacityException(byteBuffer == null ? 0 : byteBuffer.capacity(), i);
    }

    @EnsuresNonNull({"data"})
    public void r(int i) {
        int i2 = i + this.q;
        ByteBuffer byteBuffer = this.l;
        if (byteBuffer == null) {
            this.l = q(i2);
            return;
        }
        int capacity = byteBuffer.capacity();
        int position = byteBuffer.position();
        int i3 = i2 + position;
        if (capacity >= i3) {
            this.l = byteBuffer;
            return;
        }
        ByteBuffer q = q(i3);
        q.order(byteBuffer.order());
        if (position > 0) {
            byteBuffer.flip();
            q.put(byteBuffer);
        }
        this.l = q;
    }

    public final void s() {
        ByteBuffer byteBuffer = this.l;
        if (byteBuffer != null) {
            byteBuffer.flip();
        }
        ByteBuffer byteBuffer2 = this.o;
        if (byteBuffer2 != null) {
            byteBuffer2.flip();
        }
    }

    public final boolean t() {
        return k(BasicMeasure.EXACTLY);
    }
}
