package com.google.android.exoplayer2.upstream.cache;

import android.util.Log;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.c.e3.b0.p;
import b.i.a.c.e3.j;
import b.i.a.c.e3.n;
import b.i.a.c.f3.e0;
import com.discord.api.permission.Permission;
import com.google.android.exoplayer2.upstream.cache.Cache;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;
/* loaded from: classes3.dex */
public final class CacheDataSink implements j {
    public final Cache a;

    /* renamed from: b  reason: collision with root package name */
    public final long f2953b;
    public final int c;
    @Nullable
    public n d;
    public long e;
    @Nullable
    public File f;
    @Nullable
    public OutputStream g;
    public long h;
    public long i;
    public p j;

    /* loaded from: classes3.dex */
    public static final class CacheDataSinkException extends Cache.CacheException {
        public CacheDataSinkException(IOException iOException) {
            super(iOException);
        }
    }

    public CacheDataSink(Cache cache, long j) {
        d.E(j > 0 || j == -1, "fragmentSize must be positive or C.LENGTH_UNSET.");
        int i = (j > (-1L) ? 1 : (j == (-1L) ? 0 : -1));
        if (i != 0 && j < Permission.SPEAK) {
            Log.w("CacheDataSink", "fragmentSize is below the minimum recommended value of 2097152. This may cause poor cache performance.");
        }
        Objects.requireNonNull(cache);
        this.a = cache;
        this.f2953b = i == 0 ? RecyclerView.FOREVER_NS : j;
        this.c = 20480;
    }

    @Override // b.i.a.c.e3.j
    public void a(n nVar) throws CacheDataSinkException {
        Objects.requireNonNull(nVar.h);
        if (nVar.g != -1 || !nVar.c(2)) {
            this.d = nVar;
            this.e = nVar.c(4) ? this.f2953b : RecyclerView.FOREVER_NS;
            this.i = 0L;
            try {
                c(nVar);
            } catch (IOException e) {
                throw new CacheDataSinkException(e);
            }
        } else {
            this.d = null;
        }
    }

    public final void b() throws IOException {
        OutputStream outputStream = this.g;
        if (outputStream != null) {
            try {
                outputStream.flush();
                OutputStream outputStream2 = this.g;
                int i = e0.a;
                if (outputStream2 != null) {
                    try {
                        outputStream2.close();
                    } catch (IOException unused) {
                    }
                }
                this.g = null;
                File file = this.f;
                this.f = null;
                this.a.g(file, this.h);
            } catch (Throwable th) {
                OutputStream outputStream3 = this.g;
                int i2 = e0.a;
                if (outputStream3 != null) {
                    try {
                        outputStream3.close();
                    } catch (IOException unused2) {
                    }
                }
                this.g = null;
                File file2 = this.f;
                this.f = null;
                file2.delete();
                throw th;
            }
        }
    }

    public final void c(n nVar) throws IOException {
        long j = nVar.g;
        long j2 = -1;
        if (j != -1) {
            j2 = Math.min(j - this.i, this.e);
        }
        Cache cache = this.a;
        String str = nVar.h;
        int i = e0.a;
        this.f = cache.a(str, nVar.f + this.i, j2);
        FileOutputStream fileOutputStream = new FileOutputStream(this.f);
        if (this.c > 0) {
            p pVar = this.j;
            if (pVar == null) {
                this.j = new p(fileOutputStream, this.c);
            } else {
                pVar.a(fileOutputStream);
            }
            this.g = this.j;
        } else {
            this.g = fileOutputStream;
        }
        this.h = 0L;
    }

    @Override // b.i.a.c.e3.j
    public void close() throws CacheDataSinkException {
        if (this.d != null) {
            try {
                b();
            } catch (IOException e) {
                throw new CacheDataSinkException(e);
            }
        }
    }

    @Override // b.i.a.c.e3.j
    public void write(byte[] bArr, int i, int i2) throws CacheDataSinkException {
        n nVar = this.d;
        if (nVar != null) {
            int i3 = 0;
            while (i3 < i2) {
                try {
                    if (this.h == this.e) {
                        b();
                        c(nVar);
                    }
                    int min = (int) Math.min(i2 - i3, this.e - this.h);
                    OutputStream outputStream = this.g;
                    int i4 = e0.a;
                    outputStream.write(bArr, i + i3, min);
                    i3 += min;
                    long j = min;
                    this.h += j;
                    this.i += j;
                } catch (IOException e) {
                    throw new CacheDataSinkException(e);
                }
            }
        }
    }
}
