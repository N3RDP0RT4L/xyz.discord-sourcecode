package com.google.android.exoplayer2.ui;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import b.c.a.a0.d;
import b.i.a.c.a2;
import b.i.a.c.a3.o0;
import b.i.a.c.b3.b;
import b.i.a.c.c1;
import b.i.a.c.c3.n;
import b.i.a.c.d3.i;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.m;
import b.i.a.c.g3.y;
import b.i.a.c.o1;
import b.i.a.c.o2;
import b.i.a.c.p1;
import b.i.a.c.p2;
import b.i.a.c.x1;
import b.i.a.c.y1;
import b.i.a.c.z1;
import b.i.b.b.p;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.checkerframework.checker.nullness.qual.EnsuresNonNullIf;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;
/* loaded from: classes3.dex */
public class PlayerView extends FrameLayout {
    public static final /* synthetic */ int j = 0;
    @Nullable
    public Drawable A;
    public int B;
    public boolean C;
    @Nullable
    public m<? super PlaybackException> D;
    @Nullable
    public CharSequence E;
    public int F;
    public boolean G;
    public boolean H;
    public boolean I;
    public int J;
    public boolean K;
    public final a k;
    @Nullable
    public final AspectRatioFrameLayout l;
    @Nullable
    public final View m;
    @Nullable
    public final View n;
    public final boolean o;
    @Nullable
    public final ImageView p;
    @Nullable
    public final SubtitleView q;
    @Nullable
    public final View r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public final TextView f2944s;
    @Nullable
    public final PlayerControlView t;
    @Nullable
    public final FrameLayout u;
    @Nullable
    public final FrameLayout v;
    @Nullable
    public y1 w;

    /* renamed from: x  reason: collision with root package name */
    public boolean f2945x;
    @Nullable

    /* renamed from: y  reason: collision with root package name */
    public PlayerControlView.e f2946y;

    /* renamed from: z  reason: collision with root package name */
    public boolean f2947z;

    /* loaded from: classes3.dex */
    public final class a implements y1.e, View.OnLayoutChangeListener, View.OnClickListener, PlayerControlView.e {
        public final o2.b j = new o2.b();
        @Nullable
        public Object k;

        public a() {
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void A(p1 p1Var) {
            a2.i(this, p1Var);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void D(boolean z2) {
            a2.t(this, z2);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void E(y1 y1Var, y1.d dVar) {
            a2.e(this, y1Var, dVar);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void G(int i, boolean z2) {
            a2.d(this, i, z2);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void H(boolean z2, int i) {
            z1.k(this, z2, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void L(int i) {
            a2.s(this, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void M(o1 o1Var, int i) {
            a2.h(this, o1Var, i);
        }

        @Override // b.i.a.c.y1.c
        public void W(boolean z2, int i) {
            PlayerView playerView = PlayerView.this;
            int i2 = PlayerView.j;
            playerView.l();
            PlayerView playerView2 = PlayerView.this;
            if (!playerView2.e() || !playerView2.H) {
                playerView2.f(false);
            } else {
                playerView2.d();
            }
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void Y(o0 o0Var, n nVar) {
            z1.r(this, o0Var, nVar);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void a() {
            z1.o(this);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void a0(int i, int i2) {
            a2.v(this, i, i2);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void b(Metadata metadata) {
            a2.j(this, metadata);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void b0(x1 x1Var) {
            a2.l(this, x1Var);
        }

        @Override // b.i.a.c.y1.e
        public void c() {
            View view = PlayerView.this.m;
            if (view != null) {
                view.setVisibility(4);
            }
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void d(boolean z2) {
            a2.u(this, z2);
        }

        @Override // b.i.a.c.y1.e
        public void e(List<b> list) {
            SubtitleView subtitleView = PlayerView.this.q;
            if (subtitleView != null) {
                subtitleView.setCues(list);
            }
        }

        @Override // b.i.a.c.y1.e
        public void f(y yVar) {
            PlayerView playerView = PlayerView.this;
            int i = PlayerView.j;
            playerView.k();
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void f0(PlaybackException playbackException) {
            a2.p(this, playbackException);
        }

        @Override // b.i.a.c.y1.c
        public void g(y1.f fVar, y1.f fVar2, int i) {
            PlayerView playerView = PlayerView.this;
            int i2 = PlayerView.j;
            if (playerView.e()) {
                PlayerView playerView2 = PlayerView.this;
                if (playerView2.H) {
                    playerView2.d();
                }
            }
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void h(int i) {
            a2.n(this, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void i(boolean z2) {
            z1.d(this, z2);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void j(int i) {
            z1.l(this, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void j0(boolean z2) {
            a2.g(this, z2);
        }

        @Override // com.google.android.exoplayer2.ui.PlayerControlView.e
        public void k(int i) {
            PlayerView playerView = PlayerView.this;
            int i2 = PlayerView.j;
            playerView.m();
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            PlayerView playerView = PlayerView.this;
            int i = PlayerView.j;
            playerView.j();
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            PlayerView.a((TextureView) view, PlayerView.this.J);
        }

        @Override // b.i.a.c.y1.c
        public void p(p2 p2Var) {
            y1 y1Var = PlayerView.this.w;
            Objects.requireNonNull(y1Var);
            o2 K = y1Var.K();
            if (K.q()) {
                this.k = null;
            } else if (!y1Var.H().k.isEmpty()) {
                this.k = K.g(y1Var.m(), this.j, true).k;
            } else {
                Object obj = this.k;
                if (obj != null) {
                    int b2 = K.b(obj);
                    if (b2 != -1) {
                        if (y1Var.C() == K.f(b2, this.j).l) {
                            return;
                        }
                    }
                    this.k = null;
                }
            }
            PlayerView.this.o(false);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void r(boolean z2) {
            a2.f(this, z2);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void s(PlaybackException playbackException) {
            a2.o(this, playbackException);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void t(y1.b bVar) {
            a2.a(this, bVar);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void v(o2 o2Var, int i) {
            a2.w(this, o2Var, i);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void w(float f) {
            a2.z(this, f);
        }

        @Override // b.i.a.c.y1.c
        public void y(int i) {
            PlayerView playerView = PlayerView.this;
            int i2 = PlayerView.j;
            playerView.l();
            PlayerView.this.n();
            PlayerView playerView2 = PlayerView.this;
            if (!playerView2.e() || !playerView2.H) {
                playerView2.f(false);
            } else {
                playerView2.d();
            }
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void z(c1 c1Var) {
            a2.c(this, c1Var);
        }
    }

    /* JADX WARN: Finally extract failed */
    public PlayerView(Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        boolean z2;
        int i;
        int i2;
        boolean z3;
        boolean z4;
        int i3;
        int i4;
        int i5;
        boolean z5;
        int i6;
        boolean z6;
        boolean z7;
        int i7;
        boolean z8;
        a aVar = new a();
        this.k = aVar;
        if (isInEditMode()) {
            this.l = null;
            this.m = null;
            this.n = null;
            this.o = false;
            this.p = null;
            this.q = null;
            this.r = null;
            this.f2944s = null;
            this.t = null;
            this.u = null;
            this.v = null;
            ImageView imageView = new ImageView(context);
            if (e0.a >= 23) {
                Resources resources = getResources();
                imageView.setImageDrawable(resources.getDrawable(R.b.exo_edit_mode_logo, null));
                imageView.setBackgroundColor(resources.getColor(R.a.exo_edit_mode_background_color, null));
            } else {
                Resources resources2 = getResources();
                imageView.setImageDrawable(resources2.getDrawable(R.b.exo_edit_mode_logo));
                imageView.setBackgroundColor(resources2.getColor(R.a.exo_edit_mode_background_color));
            }
            addView(imageView);
            return;
        }
        int i8 = R.e.exo_player_view;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.g.PlayerView, 0, 0);
            try {
                int i9 = R.g.PlayerView_shutter_background_color;
                z4 = obtainStyledAttributes.hasValue(i9);
                int color = obtainStyledAttributes.getColor(i9, 0);
                i6 = obtainStyledAttributes.getResourceId(R.g.PlayerView_player_layout_id, i8);
                z3 = obtainStyledAttributes.getBoolean(R.g.PlayerView_use_artwork, true);
                int resourceId = obtainStyledAttributes.getResourceId(R.g.PlayerView_default_artwork, 0);
                z2 = obtainStyledAttributes.getBoolean(R.g.PlayerView_use_controller, true);
                i4 = obtainStyledAttributes.getInt(R.g.PlayerView_surface_type, 1);
                i5 = obtainStyledAttributes.getInt(R.g.PlayerView_resize_mode, 0);
                int i10 = obtainStyledAttributes.getInt(R.g.PlayerView_show_timeout, 5000);
                boolean z9 = obtainStyledAttributes.getBoolean(R.g.PlayerView_hide_on_touch, true);
                z6 = obtainStyledAttributes.getBoolean(R.g.PlayerView_auto_show, true);
                i7 = obtainStyledAttributes.getInteger(R.g.PlayerView_show_buffering, 0);
                this.C = obtainStyledAttributes.getBoolean(R.g.PlayerView_keep_content_on_player_reset, this.C);
                boolean z10 = obtainStyledAttributes.getBoolean(R.g.PlayerView_hide_during_ads, true);
                obtainStyledAttributes.recycle();
                z7 = z9;
                i = i10;
                i2 = resourceId;
                i3 = color;
                z5 = z10;
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        } else {
            i6 = i8;
            i7 = 0;
            z7 = true;
            z6 = true;
            z5 = true;
            i5 = 0;
            i4 = 1;
            i3 = 0;
            z4 = false;
            z3 = true;
            i2 = 0;
            i = 5000;
            z2 = true;
        }
        LayoutInflater.from(context).inflate(i6, this);
        setDescendantFocusability(262144);
        AspectRatioFrameLayout aspectRatioFrameLayout = (AspectRatioFrameLayout) findViewById(R.c.exo_content_frame);
        this.l = aspectRatioFrameLayout;
        if (aspectRatioFrameLayout != null) {
            aspectRatioFrameLayout.setResizeMode(i5);
        }
        View findViewById = findViewById(R.c.exo_shutter);
        this.m = findViewById;
        if (findViewById != null && z4) {
            findViewById.setBackgroundColor(i3);
        }
        if (aspectRatioFrameLayout == null || i4 == 0) {
            this.n = null;
            z8 = false;
        } else {
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
            if (i4 == 2) {
                this.n = new TextureView(context);
            } else if (i4 == 3) {
                try {
                    this.n = (View) Class.forName("b.i.a.c.g3.z.k").getConstructor(Context.class).newInstance(context);
                    z8 = true;
                    this.n.setLayoutParams(layoutParams);
                    this.n.setOnClickListener(aVar);
                    this.n.setClickable(false);
                    aspectRatioFrameLayout.addView(this.n, 0);
                } catch (Exception e) {
                    throw new IllegalStateException("spherical_gl_surface_view requires an ExoPlayer dependency", e);
                }
            } else if (i4 != 4) {
                this.n = new SurfaceView(context);
            } else {
                try {
                    this.n = (View) Class.forName("b.i.a.c.g3.s").getConstructor(Context.class).newInstance(context);
                } catch (Exception e2) {
                    throw new IllegalStateException("video_decoder_gl_surface_view requires an ExoPlayer dependency", e2);
                }
            }
            z8 = false;
            this.n.setLayoutParams(layoutParams);
            this.n.setOnClickListener(aVar);
            this.n.setClickable(false);
            aspectRatioFrameLayout.addView(this.n, 0);
        }
        this.o = z8;
        this.u = (FrameLayout) findViewById(R.c.exo_ad_overlay);
        this.v = (FrameLayout) findViewById(R.c.exo_overlay);
        ImageView imageView2 = (ImageView) findViewById(R.c.exo_artwork);
        this.p = imageView2;
        this.f2947z = z3 && imageView2 != null;
        if (i2 != 0) {
            this.A = ContextCompat.getDrawable(getContext(), i2);
        }
        SubtitleView subtitleView = (SubtitleView) findViewById(R.c.exo_subtitles);
        this.q = subtitleView;
        if (subtitleView != null) {
            subtitleView.k();
            subtitleView.l();
        }
        View findViewById2 = findViewById(R.c.exo_buffering);
        this.r = findViewById2;
        if (findViewById2 != null) {
            findViewById2.setVisibility(8);
        }
        this.B = i7;
        TextView textView = (TextView) findViewById(R.c.exo_error_message);
        this.f2944s = textView;
        if (textView != null) {
            textView.setVisibility(8);
        }
        int i11 = R.c.exo_controller;
        PlayerControlView playerControlView = (PlayerControlView) findViewById(i11);
        View findViewById3 = findViewById(R.c.exo_controller_placeholder);
        if (playerControlView != null) {
            this.t = playerControlView;
        } else if (findViewById3 != null) {
            PlayerControlView playerControlView2 = new PlayerControlView(context, null, 0, attributeSet);
            this.t = playerControlView2;
            playerControlView2.setId(i11);
            playerControlView2.setLayoutParams(findViewById3.getLayoutParams());
            ViewGroup viewGroup = (ViewGroup) findViewById3.getParent();
            int indexOfChild = viewGroup.indexOfChild(findViewById3);
            viewGroup.removeView(findViewById3);
            viewGroup.addView(playerControlView2, indexOfChild);
        } else {
            this.t = null;
        }
        PlayerControlView playerControlView3 = this.t;
        this.F = playerControlView3 != null ? i : 0;
        this.I = z7;
        this.G = z6;
        this.H = z5;
        this.f2945x = z2 && playerControlView3 != null;
        d();
        m();
        PlayerControlView playerControlView4 = this.t;
        if (playerControlView4 != null) {
            playerControlView4.l.add(aVar);
        }
    }

    public static void a(TextureView textureView, int i) {
        Matrix matrix = new Matrix();
        float width = textureView.getWidth();
        float height = textureView.getHeight();
        if (!(width == 0.0f || height == 0.0f || i == 0)) {
            float f = width / 2.0f;
            float f2 = height / 2.0f;
            matrix.postRotate(i, f, f2);
            RectF rectF = new RectF(0.0f, 0.0f, width, height);
            RectF rectF2 = new RectF();
            matrix.mapRect(rectF2, rectF);
            matrix.postScale(width / rectF2.width(), height / rectF2.height(), f, f2);
        }
        textureView.setTransform(matrix);
    }

    public final void b() {
        View view = this.m;
        if (view != null) {
            view.setVisibility(0);
        }
    }

    public final void c() {
        ImageView imageView = this.p;
        if (imageView != null) {
            imageView.setImageResource(17170445);
            this.p.setVisibility(4);
        }
    }

    public void d() {
        PlayerControlView playerControlView = this.t;
        if (playerControlView != null) {
            playerControlView.c();
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        y1 y1Var = this.w;
        if (y1Var != null && y1Var.f()) {
            return super.dispatchKeyEvent(keyEvent);
        }
        int keyCode = keyEvent.getKeyCode();
        boolean z2 = keyCode == 19 || keyCode == 270 || keyCode == 22 || keyCode == 271 || keyCode == 20 || keyCode == 269 || keyCode == 21 || keyCode == 268 || keyCode == 23;
        if (!z2 || !p() || this.t.e()) {
            if ((p() && this.t.a(keyEvent)) || super.dispatchKeyEvent(keyEvent)) {
                f(true);
            } else if (!z2 || !p()) {
                return false;
            } else {
                f(true);
                return false;
            }
        } else {
            f(true);
        }
        return true;
    }

    public final boolean e() {
        y1 y1Var = this.w;
        return y1Var != null && y1Var.f() && this.w.j();
    }

    public final void f(boolean z2) {
        if ((!e() || !this.H) && p()) {
            boolean z3 = this.t.e() && this.t.getShowTimeoutMs() <= 0;
            boolean h = h();
            if (z2 || z3 || h) {
                i(h);
            }
        }
    }

    @RequiresNonNull({"artworkView"})
    public final boolean g(@Nullable Drawable drawable) {
        if (drawable != null) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (intrinsicWidth > 0 && intrinsicHeight > 0) {
                float f = intrinsicWidth / intrinsicHeight;
                AspectRatioFrameLayout aspectRatioFrameLayout = this.l;
                if (aspectRatioFrameLayout != null) {
                    aspectRatioFrameLayout.setAspectRatio(f);
                }
                this.p.setImageDrawable(drawable);
                this.p.setVisibility(0);
                return true;
            }
        }
        return false;
    }

    public List<i> getAdOverlayInfos() {
        ArrayList arrayList = new ArrayList();
        FrameLayout frameLayout = this.v;
        if (frameLayout != null) {
            arrayList.add(new i(frameLayout, 3, "Transparent overlay does not impact viewability"));
        }
        PlayerControlView playerControlView = this.t;
        if (playerControlView != null) {
            arrayList.add(new i(playerControlView, 0));
        }
        return p.n(arrayList);
    }

    public ViewGroup getAdViewGroup() {
        FrameLayout frameLayout = this.u;
        if (frameLayout != null) {
            return frameLayout;
        }
        throw new IllegalStateException("exo_ad_overlay must be present for ad playback");
    }

    public boolean getControllerAutoShow() {
        return this.G;
    }

    public boolean getControllerHideOnTouch() {
        return this.I;
    }

    public int getControllerShowTimeoutMs() {
        return this.F;
    }

    @Nullable
    public Drawable getDefaultArtwork() {
        return this.A;
    }

    @Nullable
    public FrameLayout getOverlayFrameLayout() {
        return this.v;
    }

    @Nullable
    public y1 getPlayer() {
        return this.w;
    }

    public int getResizeMode() {
        d.H(this.l);
        return this.l.getResizeMode();
    }

    @Nullable
    public SubtitleView getSubtitleView() {
        return this.q;
    }

    public boolean getUseArtwork() {
        return this.f2947z;
    }

    public boolean getUseController() {
        return this.f2945x;
    }

    @Nullable
    public View getVideoSurfaceView() {
        return this.n;
    }

    public final boolean h() {
        y1 y1Var = this.w;
        if (y1Var == null) {
            return true;
        }
        int y2 = y1Var.y();
        return this.G && (y2 == 1 || y2 == 4 || !this.w.j());
    }

    public final void i(boolean z2) {
        if (p()) {
            this.t.setShowTimeoutMs(z2 ? 0 : this.F);
            this.t.i();
        }
    }

    public final boolean j() {
        if (!p() || this.w == null) {
            return false;
        }
        if (!this.t.e()) {
            f(true);
        } else if (this.I) {
            this.t.c();
        }
        return true;
    }

    public final void k() {
        y1 y1Var = this.w;
        y o = y1Var != null ? y1Var.o() : y.j;
        int i = o.k;
        int i2 = o.l;
        int i3 = o.m;
        float f = 0.0f;
        float f2 = (i2 == 0 || i == 0) ? 0.0f : (i * o.n) / i2;
        View view = this.n;
        if (view instanceof TextureView) {
            if (f2 > 0.0f && (i3 == 90 || i3 == 270)) {
                f2 = 1.0f / f2;
            }
            if (this.J != 0) {
                view.removeOnLayoutChangeListener(this.k);
            }
            this.J = i3;
            if (i3 != 0) {
                this.n.addOnLayoutChangeListener(this.k);
            }
            a((TextureView) this.n, this.J);
        }
        AspectRatioFrameLayout aspectRatioFrameLayout = this.l;
        if (!this.o) {
            f = f2;
        }
        if (aspectRatioFrameLayout != null) {
            aspectRatioFrameLayout.setAspectRatio(f);
        }
    }

    public final void l() {
        int i;
        if (this.r != null) {
            y1 y1Var = this.w;
            boolean z2 = true;
            int i2 = 0;
            if (y1Var == null || y1Var.y() != 2 || ((i = this.B) != 2 && (i != 1 || !this.w.j()))) {
                z2 = false;
            }
            View view = this.r;
            if (!z2) {
                i2 = 8;
            }
            view.setVisibility(i2);
        }
    }

    public final void m() {
        PlayerControlView playerControlView = this.t;
        String str = null;
        if (playerControlView == null || !this.f2945x) {
            setContentDescription(null);
        } else if (playerControlView.getVisibility() == 0) {
            if (this.I) {
                str = getResources().getString(R.f.exo_controls_hide);
            }
            setContentDescription(str);
        } else {
            setContentDescription(getResources().getString(R.f.exo_controls_show));
        }
    }

    public final void n() {
        m<? super PlaybackException> mVar;
        TextView textView = this.f2944s;
        if (textView != null) {
            CharSequence charSequence = this.E;
            if (charSequence != null) {
                textView.setText(charSequence);
                this.f2944s.setVisibility(0);
                return;
            }
            y1 y1Var = this.w;
            PlaybackException t = y1Var != null ? y1Var.t() : null;
            if (t == null || (mVar = this.D) == null) {
                this.f2944s.setVisibility(8);
                return;
            }
            this.f2944s.setText((CharSequence) mVar.a(t).second);
            this.f2944s.setVisibility(0);
        }
    }

    public final void o(boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5;
        y1 y1Var = this.w;
        if (y1Var != null && y1Var.D(30) && !y1Var.H().k.isEmpty()) {
            if (z2 && !this.C) {
                b();
            }
            p2 H = y1Var.H();
            boolean z6 = false;
            int i = 0;
            while (true) {
                z3 = true;
                if (i >= H.k.size()) {
                    z4 = false;
                    break;
                }
                p2.a aVar = H.k.get(i);
                boolean[] zArr = aVar.m;
                int length = zArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z5 = false;
                        break;
                    } else if (zArr[i2]) {
                        z5 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z5 && aVar.l == 2) {
                    z4 = true;
                    break;
                }
                i++;
            }
            if (z4) {
                c();
                return;
            }
            b();
            if (this.f2947z) {
                d.H(this.p);
            } else {
                z3 = false;
            }
            if (z3) {
                byte[] bArr = y1Var.S().v;
                if (bArr != null) {
                    z6 = g(new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(bArr, 0, bArr.length)));
                }
                if (z6 || g(this.A)) {
                    return;
                }
            }
            c();
        } else if (!this.C) {
            c();
            b();
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!p() || this.w == null) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            this.K = true;
            return true;
        } else if (action != 1 || !this.K) {
            return false;
        } else {
            this.K = false;
            performClick();
            return true;
        }
    }

    @Override // android.view.View
    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (!p() || this.w == null) {
            return false;
        }
        f(true);
        return true;
    }

    @EnsuresNonNullIf(expression = {"controller"}, result = GoogleSmartLockManager.SET_DISCORD_ACCOUNT_DETAILS)
    public final boolean p() {
        if (!this.f2945x) {
            return false;
        }
        d.H(this.t);
        return true;
    }

    @Override // android.view.View
    public boolean performClick() {
        super.performClick();
        return j();
    }

    public void setAspectRatioListener(@Nullable AspectRatioFrameLayout.b bVar) {
        d.H(this.l);
        this.l.setAspectRatioListener(bVar);
    }

    public void setControllerAutoShow(boolean z2) {
        this.G = z2;
    }

    public void setControllerHideDuringAds(boolean z2) {
        this.H = z2;
    }

    public void setControllerHideOnTouch(boolean z2) {
        d.H(this.t);
        this.I = z2;
        m();
    }

    public void setControllerShowTimeoutMs(int i) {
        d.H(this.t);
        this.F = i;
        if (this.t.e()) {
            i(h());
        }
    }

    public void setControllerVisibilityListener(@Nullable PlayerControlView.e eVar) {
        d.H(this.t);
        PlayerControlView.e eVar2 = this.f2946y;
        if (eVar2 != eVar) {
            if (eVar2 != null) {
                this.t.l.remove(eVar2);
            }
            this.f2946y = eVar;
            if (eVar != null) {
                PlayerControlView playerControlView = this.t;
                Objects.requireNonNull(playerControlView);
                playerControlView.l.add(eVar);
            }
        }
    }

    public void setCustomErrorMessage(@Nullable CharSequence charSequence) {
        d.D(this.f2944s != null);
        this.E = charSequence;
        n();
    }

    public void setDefaultArtwork(@Nullable Drawable drawable) {
        if (this.A != drawable) {
            this.A = drawable;
            o(false);
        }
    }

    public void setErrorMessageProvider(@Nullable m<? super PlaybackException> mVar) {
        if (this.D != mVar) {
            this.D = mVar;
            n();
        }
    }

    public void setKeepContentOnPlayerReset(boolean z2) {
        if (this.C != z2) {
            this.C = z2;
            o(false);
        }
    }

    public void setPlayer(@Nullable y1 y1Var) {
        d.D(Looper.myLooper() == Looper.getMainLooper());
        d.j(y1Var == null || y1Var.L() == Looper.getMainLooper());
        y1 y1Var2 = this.w;
        if (y1Var2 != y1Var) {
            if (y1Var2 != null) {
                y1Var2.p(this.k);
                if (y1Var2.D(27)) {
                    View view = this.n;
                    if (view instanceof TextureView) {
                        y1Var2.n((TextureView) view);
                    } else if (view instanceof SurfaceView) {
                        y1Var2.F((SurfaceView) view);
                    }
                }
            }
            SubtitleView subtitleView = this.q;
            if (subtitleView != null) {
                subtitleView.setCues(null);
            }
            this.w = y1Var;
            if (p()) {
                this.t.setPlayer(y1Var);
            }
            l();
            n();
            o(true);
            if (y1Var != null) {
                if (y1Var.D(27)) {
                    View view2 = this.n;
                    if (view2 instanceof TextureView) {
                        y1Var.Q((TextureView) view2);
                    } else if (view2 instanceof SurfaceView) {
                        y1Var.r((SurfaceView) view2);
                    }
                    k();
                }
                if (this.q != null && y1Var.D(28)) {
                    this.q.setCues(y1Var.A());
                }
                y1Var.x(this.k);
                f(false);
                return;
            }
            d();
        }
    }

    public void setRepeatToggleModes(int i) {
        d.H(this.t);
        this.t.setRepeatToggleModes(i);
    }

    public void setResizeMode(int i) {
        d.H(this.l);
        this.l.setResizeMode(i);
    }

    public void setShowBuffering(int i) {
        if (this.B != i) {
            this.B = i;
            l();
        }
    }

    public void setShowFastForwardButton(boolean z2) {
        d.H(this.t);
        this.t.setShowFastForwardButton(z2);
    }

    public void setShowMultiWindowTimeBar(boolean z2) {
        d.H(this.t);
        this.t.setShowMultiWindowTimeBar(z2);
    }

    public void setShowNextButton(boolean z2) {
        d.H(this.t);
        this.t.setShowNextButton(z2);
    }

    public void setShowPreviousButton(boolean z2) {
        d.H(this.t);
        this.t.setShowPreviousButton(z2);
    }

    public void setShowRewindButton(boolean z2) {
        d.H(this.t);
        this.t.setShowRewindButton(z2);
    }

    public void setShowShuffleButton(boolean z2) {
        d.H(this.t);
        this.t.setShowShuffleButton(z2);
    }

    public void setShutterBackgroundColor(int i) {
        View view = this.m;
        if (view != null) {
            view.setBackgroundColor(i);
        }
    }

    public void setUseArtwork(boolean z2) {
        d.D(!z2 || this.p != null);
        if (this.f2947z != z2) {
            this.f2947z = z2;
            o(false);
        }
    }

    public void setUseController(boolean z2) {
        d.D(!z2 || this.t != null);
        if (this.f2945x != z2) {
            this.f2945x = z2;
            if (p()) {
                this.t.setPlayer(this.w);
            } else {
                PlayerControlView playerControlView = this.t;
                if (playerControlView != null) {
                    playerControlView.c();
                    this.t.setPlayer(null);
                }
            }
            m();
        }
    }

    @Override // android.view.View
    public void setVisibility(int i) {
        super.setVisibility(i);
        View view = this.n;
        if (view instanceof SurfaceView) {
            view.setVisibility(i);
        }
    }
}
