package com.google.android.exoplayer2.ui;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.DoNotInline;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.i.a.c.a2;
import b.i.a.c.a3.o0;
import b.i.a.c.c1;
import b.i.a.c.c3.n;
import b.i.a.c.d3.o;
import b.i.a.c.f3.e0;
import b.i.a.c.g3.y;
import b.i.a.c.i1;
import b.i.a.c.o1;
import b.i.a.c.o2;
import b.i.a.c.p1;
import b.i.a.c.p2;
import b.i.a.c.x1;
import b.i.a.c.y1;
import b.i.a.c.z1;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.ui.PlayerControlView;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
/* loaded from: classes3.dex */
public class PlayerControlView extends FrameLayout {
    public static final /* synthetic */ int j = 0;
    public final o2.b A;
    public final o2.c B;
    public final Runnable C;
    public final Runnable D;
    public final Drawable E;
    public final Drawable F;
    public final Drawable G;
    public final String H;
    public final String I;
    public final String J;
    public final Drawable K;
    public final Drawable L;
    public final float M;
    public final float N;
    public final String O;
    public final String P;
    @Nullable
    public y1 Q;
    @Nullable
    public d R;
    public boolean S;
    public boolean T;
    public boolean U;
    public boolean V;
    public int W;

    /* renamed from: a0  reason: collision with root package name */
    public int f2930a0;

    /* renamed from: b0  reason: collision with root package name */
    public int f2931b0;

    /* renamed from: c0  reason: collision with root package name */
    public boolean f2932c0;

    /* renamed from: d0  reason: collision with root package name */
    public boolean f2933d0;

    /* renamed from: e0  reason: collision with root package name */
    public boolean f2934e0;

    /* renamed from: f0  reason: collision with root package name */
    public boolean f2935f0;

    /* renamed from: g0  reason: collision with root package name */
    public boolean f2936g0;

    /* renamed from: h0  reason: collision with root package name */
    public long f2937h0;

    /* renamed from: i0  reason: collision with root package name */
    public long[] f2938i0;

    /* renamed from: j0  reason: collision with root package name */
    public boolean[] f2939j0;
    public final c k;
    public long[] k0;
    public final CopyOnWriteArrayList<e> l;
    public boolean[] l0;
    @Nullable
    public final View m;
    public long m0;
    @Nullable
    public final View n;
    public long n0;
    @Nullable
    public final View o;
    public long o0;
    @Nullable
    public final View p;
    @Nullable
    public final View q;
    @Nullable
    public final View r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public final ImageView f2940s;
    @Nullable
    public final ImageView t;
    @Nullable
    public final View u;
    @Nullable
    public final TextView v;
    @Nullable
    public final TextView w;
    @Nullable

    /* renamed from: x  reason: collision with root package name */
    public final o f2941x;

    /* renamed from: y  reason: collision with root package name */
    public final StringBuilder f2942y;

    /* renamed from: z  reason: collision with root package name */
    public final Formatter f2943z;

    @RequiresApi(21)
    /* loaded from: classes3.dex */
    public static final class b {
        @DoNotInline
        public static boolean a(View view) {
            return view.isAccessibilityFocused();
        }
    }

    /* loaded from: classes3.dex */
    public final class c implements y1.e, o.a, View.OnClickListener {
        public c(a aVar) {
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void A(p1 p1Var) {
            a2.i(this, p1Var);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void D(boolean z2) {
            a2.t(this, z2);
        }

        @Override // b.i.a.c.y1.c
        public void E(y1 y1Var, y1.d dVar) {
            if (dVar.a(4, 5)) {
                PlayerControlView playerControlView = PlayerControlView.this;
                int i = PlayerControlView.j;
                playerControlView.m();
            }
            if (dVar.a(4, 5, 7)) {
                PlayerControlView playerControlView2 = PlayerControlView.this;
                int i2 = PlayerControlView.j;
                playerControlView2.n();
            }
            if (dVar.a.a.get(8)) {
                PlayerControlView playerControlView3 = PlayerControlView.this;
                int i3 = PlayerControlView.j;
                playerControlView3.o();
            }
            if (dVar.a.a.get(9)) {
                PlayerControlView playerControlView4 = PlayerControlView.this;
                int i4 = PlayerControlView.j;
                playerControlView4.p();
            }
            if (dVar.a(8, 9, 11, 0, 13)) {
                PlayerControlView playerControlView5 = PlayerControlView.this;
                int i5 = PlayerControlView.j;
                playerControlView5.l();
            }
            if (dVar.a(11, 0)) {
                PlayerControlView playerControlView6 = PlayerControlView.this;
                int i6 = PlayerControlView.j;
                playerControlView6.q();
            }
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void G(int i, boolean z2) {
            a2.d(this, i, z2);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void H(boolean z2, int i) {
            z1.k(this, z2, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void L(int i) {
            a2.s(this, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void M(o1 o1Var, int i) {
            a2.h(this, o1Var, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void W(boolean z2, int i) {
            a2.k(this, z2, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void Y(o0 o0Var, n nVar) {
            z1.r(this, o0Var, nVar);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void a() {
            z1.o(this);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void a0(int i, int i2) {
            a2.v(this, i, i2);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void b(Metadata metadata) {
            a2.j(this, metadata);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void b0(x1 x1Var) {
            a2.l(this, x1Var);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void c() {
            a2.r(this);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void d(boolean z2) {
            a2.u(this, z2);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void e(List list) {
            a2.b(this, list);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void f(y yVar) {
            a2.y(this, yVar);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void f0(PlaybackException playbackException) {
            a2.p(this, playbackException);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void g(y1.f fVar, y1.f fVar2, int i) {
            a2.q(this, fVar, fVar2, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void h(int i) {
            a2.n(this, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void i(boolean z2) {
            z1.d(this, z2);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void j(int i) {
            z1.l(this, i);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void j0(boolean z2) {
            a2.g(this, z2);
        }

        @Override // b.i.a.c.d3.o.a
        public void k(o oVar, long j) {
            PlayerControlView playerControlView = PlayerControlView.this;
            TextView textView = playerControlView.w;
            if (textView != null) {
                textView.setText(e0.u(playerControlView.f2942y, playerControlView.f2943z, j));
            }
        }

        @Override // b.i.a.c.d3.o.a
        public void l(o oVar, long j, boolean z2) {
            y1 y1Var;
            PlayerControlView playerControlView = PlayerControlView.this;
            int i = 0;
            playerControlView.V = false;
            if (!z2 && (y1Var = playerControlView.Q) != null) {
                o2 K = y1Var.K();
                if (playerControlView.U && !K.q()) {
                    int p = K.p();
                    while (true) {
                        long b2 = K.n(i, playerControlView.B).b();
                        if (j < b2) {
                            break;
                        } else if (i == p - 1) {
                            j = b2;
                            break;
                        } else {
                            j -= b2;
                            i++;
                        }
                    }
                } else {
                    i = y1Var.C();
                }
                y1Var.h(i, j);
                playerControlView.n();
            }
        }

        @Override // b.i.a.c.d3.o.a
        public void m(o oVar, long j) {
            PlayerControlView playerControlView = PlayerControlView.this;
            playerControlView.V = true;
            TextView textView = playerControlView.w;
            if (textView != null) {
                textView.setText(e0.u(playerControlView.f2942y, playerControlView.f2943z, j));
            }
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            PlayerControlView playerControlView = PlayerControlView.this;
            y1 y1Var = playerControlView.Q;
            if (y1Var != null) {
                if (playerControlView.n == view) {
                    y1Var.O();
                } else if (playerControlView.m == view) {
                    y1Var.s();
                } else if (playerControlView.q == view) {
                    if (y1Var.y() != 4) {
                        y1Var.P();
                    }
                } else if (playerControlView.r == view) {
                    y1Var.R();
                } else if (playerControlView.o == view) {
                    playerControlView.b(y1Var);
                } else if (playerControlView.p == view) {
                    Objects.requireNonNull(playerControlView);
                    y1Var.d();
                } else if (playerControlView.f2940s == view) {
                    int I = y1Var.I();
                    int i = PlayerControlView.this.f2931b0;
                    int i2 = 1;
                    while (true) {
                        if (i2 > 2) {
                            break;
                        }
                        int i3 = (I + i2) % 3;
                        boolean z2 = false;
                        if (i3 == 0 || (i3 == 1 ? (i & 1) != 0 : !(i3 != 2 || (i & 2) == 0))) {
                            z2 = true;
                        }
                        if (z2) {
                            I = i3;
                            break;
                        }
                        i2++;
                    }
                    y1Var.E(I);
                } else if (playerControlView.t == view) {
                    y1Var.k(!y1Var.M());
                }
            }
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void p(p2 p2Var) {
            a2.x(this, p2Var);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void r(boolean z2) {
            a2.f(this, z2);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void s(PlaybackException playbackException) {
            a2.o(this, playbackException);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void t(y1.b bVar) {
            a2.a(this, bVar);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void v(o2 o2Var, int i) {
            a2.w(this, o2Var, i);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void w(float f) {
            a2.z(this, f);
        }

        @Override // b.i.a.c.y1.c
        public /* synthetic */ void y(int i) {
            a2.m(this, i);
        }

        @Override // b.i.a.c.y1.e
        public /* synthetic */ void z(c1 c1Var) {
            a2.c(this, c1Var);
        }
    }

    /* loaded from: classes3.dex */
    public interface d {
        void a(long j, long j2);
    }

    /* loaded from: classes3.dex */
    public interface e {
        void k(int i);
    }

    static {
        i1.a("goog.exo.ui");
    }

    public PlayerControlView(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0, attributeSet);
    }

    public boolean a(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        y1 y1Var = this.Q;
        if (y1Var != null) {
            if (keyCode == 90 || keyCode == 89 || keyCode == 85 || keyCode == 79 || keyCode == 126 || keyCode == 127 || keyCode == 87 || keyCode == 88) {
                if (keyEvent.getAction() == 0) {
                    if (keyCode == 90) {
                        if (y1Var.y() != 4) {
                            y1Var.P();
                        }
                    } else if (keyCode == 89) {
                        y1Var.R();
                    } else if (keyEvent.getRepeatCount() == 0) {
                        if (keyCode == 79 || keyCode == 85) {
                            int y2 = y1Var.y();
                            if (y2 == 1 || y2 == 4 || !y1Var.j()) {
                                b(y1Var);
                            } else {
                                y1Var.d();
                            }
                        } else if (keyCode == 87) {
                            y1Var.O();
                        } else if (keyCode == 88) {
                            y1Var.s();
                        } else if (keyCode == 126) {
                            b(y1Var);
                        } else if (keyCode == 127) {
                            y1Var.d();
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    public final void b(y1 y1Var) {
        int y2 = y1Var.y();
        if (y2 == 1) {
            y1Var.a();
        } else if (y2 == 4) {
            y1Var.h(y1Var.C(), -9223372036854775807L);
        }
        y1Var.e();
    }

    public void c() {
        if (e()) {
            setVisibility(8);
            Iterator<e> it = this.l.iterator();
            while (it.hasNext()) {
                it.next().k(getVisibility());
            }
            removeCallbacks(this.C);
            removeCallbacks(this.D);
            this.f2937h0 = -9223372036854775807L;
        }
    }

    public final void d() {
        removeCallbacks(this.D);
        if (this.W > 0) {
            long uptimeMillis = SystemClock.uptimeMillis();
            int i = this.W;
            this.f2937h0 = uptimeMillis + i;
            if (this.S) {
                postDelayed(this.D, i);
                return;
            }
            return;
        }
        this.f2937h0 = -9223372036854775807L;
    }

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return a(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }

    @Override // android.view.ViewGroup, android.view.View
    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            removeCallbacks(this.D);
        } else if (motionEvent.getAction() == 1) {
            d();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean e() {
        return getVisibility() == 0;
    }

    public final void f() {
        View view;
        View view2;
        boolean h = h();
        if (!h && (view2 = this.o) != null) {
            view2.sendAccessibilityEvent(8);
        } else if (h && (view = this.p) != null) {
            view.sendAccessibilityEvent(8);
        }
    }

    public final void g() {
        View view;
        View view2;
        boolean h = h();
        if (!h && (view2 = this.o) != null) {
            view2.requestFocus();
        } else if (h && (view = this.p) != null) {
            view.requestFocus();
        }
    }

    @Nullable
    public y1 getPlayer() {
        return this.Q;
    }

    public int getRepeatToggleModes() {
        return this.f2931b0;
    }

    public boolean getShowShuffleButton() {
        return this.f2936g0;
    }

    public int getShowTimeoutMs() {
        return this.W;
    }

    public boolean getShowVrButton() {
        View view = this.u;
        return view != null && view.getVisibility() == 0;
    }

    public final boolean h() {
        y1 y1Var = this.Q;
        return (y1Var == null || y1Var.y() == 4 || this.Q.y() == 1 || !this.Q.j()) ? false : true;
    }

    public void i() {
        if (!e()) {
            setVisibility(0);
            Iterator<e> it = this.l.iterator();
            while (it.hasNext()) {
                it.next().k(getVisibility());
            }
            j();
            g();
            f();
        }
        d();
    }

    public final void j() {
        m();
        l();
        o();
        p();
        q();
    }

    public final void k(boolean z2, boolean z3, @Nullable View view) {
        if (view != null) {
            view.setEnabled(z3);
            view.setAlpha(z3 ? this.M : this.N);
            view.setVisibility(z2 ? 0 : 8);
        }
    }

    public final void l() {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        if (e() && this.S) {
            y1 y1Var = this.Q;
            boolean z6 = false;
            if (y1Var != null) {
                boolean D = y1Var.D(5);
                boolean D2 = y1Var.D(7);
                z3 = y1Var.D(11);
                z2 = y1Var.D(12);
                z5 = y1Var.D(9);
                z4 = D;
                z6 = D2;
            } else {
                z5 = false;
                z4 = false;
                z3 = false;
                z2 = false;
            }
            k(this.f2934e0, z6, this.m);
            k(this.f2932c0, z3, this.r);
            k(this.f2933d0, z2, this.q);
            k(this.f2935f0, z5, this.n);
            o oVar = this.f2941x;
            if (oVar != null) {
                oVar.setEnabled(z4);
            }
        }
    }

    public final void m() {
        boolean z2;
        boolean z3;
        boolean z4;
        if (e() && this.S) {
            boolean h = h();
            View view = this.o;
            int i = 8;
            boolean z5 = true;
            if (view != null) {
                z3 = (h && view.isFocused()) | false;
                if (e0.a < 21) {
                    z4 = z3;
                } else {
                    z4 = h && b.a(this.o);
                }
                z2 = z4 | false;
                this.o.setVisibility(h ? 8 : 0);
            } else {
                z3 = false;
                z2 = false;
            }
            View view2 = this.p;
            if (view2 != null) {
                z3 |= !h && view2.isFocused();
                if (e0.a < 21) {
                    z5 = z3;
                } else if (h || !b.a(this.p)) {
                    z5 = false;
                }
                z2 |= z5;
                View view3 = this.p;
                if (h) {
                    i = 0;
                }
                view3.setVisibility(i);
            }
            if (z3) {
                g();
            }
            if (z2) {
                f();
            }
        }
    }

    public final void n() {
        long j2;
        if (e() && this.S) {
            y1 y1Var = this.Q;
            long j3 = 0;
            if (y1Var != null) {
                j3 = this.m0 + y1Var.w();
                j2 = this.m0 + y1Var.N();
            } else {
                j2 = 0;
            }
            boolean z2 = false;
            boolean z3 = j3 != this.n0;
            if (j2 != this.o0) {
                z2 = true;
            }
            this.n0 = j3;
            this.o0 = j2;
            TextView textView = this.w;
            if (textView != null && !this.V && z3) {
                textView.setText(e0.u(this.f2942y, this.f2943z, j3));
            }
            o oVar = this.f2941x;
            if (oVar != null) {
                oVar.setPosition(j3);
                this.f2941x.setBufferedPosition(j2);
            }
            d dVar = this.R;
            if (dVar != null && (z3 || z2)) {
                dVar.a(j3, j2);
            }
            removeCallbacks(this.C);
            int y2 = y1Var == null ? 1 : y1Var.y();
            long j4 = 1000;
            if (y1Var != null && y1Var.z()) {
                o oVar2 = this.f2941x;
                long min = Math.min(oVar2 != null ? oVar2.getPreferredUpdateDelay() : 1000L, 1000 - (j3 % 1000));
                float f = y1Var.c().k;
                if (f > 0.0f) {
                    j4 = ((float) min) / f;
                }
                postDelayed(this.C, e0.i(j4, this.f2930a0, 1000L));
            } else if (y2 != 4 && y2 != 1) {
                postDelayed(this.C, 1000L);
            }
        }
    }

    public final void o() {
        ImageView imageView;
        if (e() && this.S && (imageView = this.f2940s) != null) {
            if (this.f2931b0 == 0) {
                k(false, false, imageView);
                return;
            }
            y1 y1Var = this.Q;
            if (y1Var == null) {
                k(true, false, imageView);
                this.f2940s.setImageDrawable(this.E);
                this.f2940s.setContentDescription(this.H);
                return;
            }
            k(true, true, imageView);
            int I = y1Var.I();
            if (I == 0) {
                this.f2940s.setImageDrawable(this.E);
                this.f2940s.setContentDescription(this.H);
            } else if (I == 1) {
                this.f2940s.setImageDrawable(this.F);
                this.f2940s.setContentDescription(this.I);
            } else if (I == 2) {
                this.f2940s.setImageDrawable(this.G);
                this.f2940s.setContentDescription(this.J);
            }
            this.f2940s.setVisibility(0);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.S = true;
        long j2 = this.f2937h0;
        if (j2 != -9223372036854775807L) {
            long uptimeMillis = j2 - SystemClock.uptimeMillis();
            if (uptimeMillis <= 0) {
                c();
            } else {
                postDelayed(this.D, uptimeMillis);
            }
        } else if (e()) {
            d();
        }
        j();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.S = false;
        removeCallbacks(this.C);
        removeCallbacks(this.D);
    }

    public final void p() {
        ImageView imageView;
        String str;
        if (e() && this.S && (imageView = this.t) != null) {
            y1 y1Var = this.Q;
            if (!this.f2936g0) {
                k(false, false, imageView);
            } else if (y1Var == null) {
                k(true, false, imageView);
                this.t.setImageDrawable(this.L);
                this.t.setContentDescription(this.P);
            } else {
                k(true, true, imageView);
                this.t.setImageDrawable(y1Var.M() ? this.K : this.L);
                ImageView imageView2 = this.t;
                if (y1Var.M()) {
                    str = this.O;
                } else {
                    str = this.P;
                }
                imageView2.setContentDescription(str);
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x004c  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x011e  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0128  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x0137  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void q() {
        /*
            Method dump skipped, instructions count: 366
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.PlayerControlView.q():void");
    }

    public void setPlayer(@Nullable y1 y1Var) {
        boolean z2 = true;
        b.c.a.a0.d.D(Looper.myLooper() == Looper.getMainLooper());
        if (!(y1Var == null || y1Var.L() == Looper.getMainLooper())) {
            z2 = false;
        }
        b.c.a.a0.d.j(z2);
        y1 y1Var2 = this.Q;
        if (y1Var2 != y1Var) {
            if (y1Var2 != null) {
                y1Var2.p(this.k);
            }
            this.Q = y1Var;
            if (y1Var != null) {
                y1Var.x(this.k);
            }
            j();
        }
    }

    public void setProgressUpdateListener(@Nullable d dVar) {
        this.R = dVar;
    }

    public void setRepeatToggleModes(int i) {
        this.f2931b0 = i;
        y1 y1Var = this.Q;
        if (y1Var != null) {
            int I = y1Var.I();
            if (i == 0 && I != 0) {
                this.Q.E(0);
            } else if (i == 1 && I == 2) {
                this.Q.E(1);
            } else if (i == 2 && I == 1) {
                this.Q.E(2);
            }
        }
        o();
    }

    public void setShowFastForwardButton(boolean z2) {
        this.f2933d0 = z2;
        l();
    }

    public void setShowMultiWindowTimeBar(boolean z2) {
        this.T = z2;
        q();
    }

    public void setShowNextButton(boolean z2) {
        this.f2935f0 = z2;
        l();
    }

    public void setShowPreviousButton(boolean z2) {
        this.f2934e0 = z2;
        l();
    }

    public void setShowRewindButton(boolean z2) {
        this.f2932c0 = z2;
        l();
    }

    public void setShowShuffleButton(boolean z2) {
        this.f2936g0 = z2;
        p();
    }

    public void setShowTimeoutMs(int i) {
        this.W = i;
        if (e()) {
            d();
        }
    }

    public void setShowVrButton(boolean z2) {
        View view = this.u;
        if (view != null) {
            view.setVisibility(z2 ? 0 : 8);
        }
    }

    public void setTimeBarMinUpdateInterval(int i) {
        this.f2930a0 = e0.h(i, 16, 1000);
    }

    public void setVrButtonListener(@Nullable View.OnClickListener onClickListener) {
        View view = this.u;
        if (view != null) {
            view.setOnClickListener(onClickListener);
            k(getShowVrButton(), onClickListener != null, this.u);
        }
    }

    public PlayerControlView(Context context, @Nullable AttributeSet attributeSet, int i, @Nullable AttributeSet attributeSet2) {
        super(context, attributeSet, i);
        int i2 = R.e.exo_player_control_view;
        this.W = 5000;
        this.f2931b0 = 0;
        this.f2930a0 = 200;
        this.f2937h0 = -9223372036854775807L;
        this.f2932c0 = true;
        this.f2933d0 = true;
        this.f2934e0 = true;
        this.f2935f0 = true;
        this.f2936g0 = false;
        if (attributeSet2 != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet2, R.g.PlayerControlView, i, 0);
            try {
                this.W = obtainStyledAttributes.getInt(R.g.PlayerControlView_show_timeout, this.W);
                i2 = obtainStyledAttributes.getResourceId(R.g.PlayerControlView_controller_layout_id, i2);
                this.f2931b0 = obtainStyledAttributes.getInt(R.g.PlayerControlView_repeat_toggle_modes, this.f2931b0);
                this.f2932c0 = obtainStyledAttributes.getBoolean(R.g.PlayerControlView_show_rewind_button, this.f2932c0);
                this.f2933d0 = obtainStyledAttributes.getBoolean(R.g.PlayerControlView_show_fastforward_button, this.f2933d0);
                this.f2934e0 = obtainStyledAttributes.getBoolean(R.g.PlayerControlView_show_previous_button, this.f2934e0);
                this.f2935f0 = obtainStyledAttributes.getBoolean(R.g.PlayerControlView_show_next_button, this.f2935f0);
                this.f2936g0 = obtainStyledAttributes.getBoolean(R.g.PlayerControlView_show_shuffle_button, this.f2936g0);
                setTimeBarMinUpdateInterval(obtainStyledAttributes.getInt(R.g.PlayerControlView_time_bar_min_update_interval, this.f2930a0));
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.l = new CopyOnWriteArrayList<>();
        this.A = new o2.b();
        this.B = new o2.c();
        StringBuilder sb = new StringBuilder();
        this.f2942y = sb;
        this.f2943z = new Formatter(sb, Locale.getDefault());
        this.f2938i0 = new long[0];
        this.f2939j0 = new boolean[0];
        this.k0 = new long[0];
        this.l0 = new boolean[0];
        c cVar = new c(null);
        this.k = cVar;
        this.C = new Runnable() { // from class: b.i.a.c.d3.d
            @Override // java.lang.Runnable
            public final void run() {
                PlayerControlView playerControlView = PlayerControlView.this;
                int i3 = PlayerControlView.j;
                playerControlView.n();
            }
        };
        this.D = new Runnable() { // from class: b.i.a.c.d3.a
            @Override // java.lang.Runnable
            public final void run() {
                PlayerControlView.this.c();
            }
        };
        LayoutInflater.from(context).inflate(i2, this);
        setDescendantFocusability(262144);
        int i3 = R.c.exo_progress;
        o oVar = (o) findViewById(i3);
        View findViewById = findViewById(R.c.exo_progress_placeholder);
        if (oVar != null) {
            this.f2941x = oVar;
        } else if (findViewById != null) {
            DefaultTimeBar defaultTimeBar = new DefaultTimeBar(context, null, 0, attributeSet2);
            defaultTimeBar.setId(i3);
            defaultTimeBar.setLayoutParams(findViewById.getLayoutParams());
            ViewGroup viewGroup = (ViewGroup) findViewById.getParent();
            int indexOfChild = viewGroup.indexOfChild(findViewById);
            viewGroup.removeView(findViewById);
            viewGroup.addView(defaultTimeBar, indexOfChild);
            this.f2941x = defaultTimeBar;
        } else {
            this.f2941x = null;
        }
        this.v = (TextView) findViewById(R.c.exo_duration);
        this.w = (TextView) findViewById(R.c.exo_position);
        o oVar2 = this.f2941x;
        if (oVar2 != null) {
            oVar2.b(cVar);
        }
        View findViewById2 = findViewById(R.c.exo_play);
        this.o = findViewById2;
        if (findViewById2 != null) {
            findViewById2.setOnClickListener(cVar);
        }
        View findViewById3 = findViewById(R.c.exo_pause);
        this.p = findViewById3;
        if (findViewById3 != null) {
            findViewById3.setOnClickListener(cVar);
        }
        View findViewById4 = findViewById(R.c.exo_prev);
        this.m = findViewById4;
        if (findViewById4 != null) {
            findViewById4.setOnClickListener(cVar);
        }
        View findViewById5 = findViewById(R.c.exo_next);
        this.n = findViewById5;
        if (findViewById5 != null) {
            findViewById5.setOnClickListener(cVar);
        }
        View findViewById6 = findViewById(R.c.exo_rew);
        this.r = findViewById6;
        if (findViewById6 != null) {
            findViewById6.setOnClickListener(cVar);
        }
        View findViewById7 = findViewById(R.c.exo_ffwd);
        this.q = findViewById7;
        if (findViewById7 != null) {
            findViewById7.setOnClickListener(cVar);
        }
        ImageView imageView = (ImageView) findViewById(R.c.exo_repeat_toggle);
        this.f2940s = imageView;
        if (imageView != null) {
            imageView.setOnClickListener(cVar);
        }
        ImageView imageView2 = (ImageView) findViewById(R.c.exo_shuffle);
        this.t = imageView2;
        if (imageView2 != null) {
            imageView2.setOnClickListener(cVar);
        }
        View findViewById8 = findViewById(R.c.exo_vr);
        this.u = findViewById8;
        setShowVrButton(false);
        k(false, false, findViewById8);
        Resources resources = context.getResources();
        this.M = resources.getInteger(R.d.exo_media_button_opacity_percentage_enabled) / 100.0f;
        this.N = resources.getInteger(R.d.exo_media_button_opacity_percentage_disabled) / 100.0f;
        this.E = resources.getDrawable(R.b.exo_controls_repeat_off);
        this.F = resources.getDrawable(R.b.exo_controls_repeat_one);
        this.G = resources.getDrawable(R.b.exo_controls_repeat_all);
        this.K = resources.getDrawable(R.b.exo_controls_shuffle_on);
        this.L = resources.getDrawable(R.b.exo_controls_shuffle_off);
        this.H = resources.getString(R.f.exo_controls_repeat_off_description);
        this.I = resources.getString(R.f.exo_controls_repeat_one_description);
        this.J = resources.getString(R.f.exo_controls_repeat_all_description);
        this.O = resources.getString(R.f.exo_controls_shuffle_on_description);
        this.P = resources.getString(R.f.exo_controls_shuffle_off_description);
        this.n0 = -9223372036854775807L;
        this.o0 = -9223372036854775807L;
    }
}
