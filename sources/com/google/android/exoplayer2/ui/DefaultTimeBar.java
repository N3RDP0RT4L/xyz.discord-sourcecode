package com.google.android.exoplayer2.ui;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import b.c.a.a0.d;
import b.i.a.c.d3.o;
import b.i.a.c.f3.e0;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import java.util.Collections;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;
/* loaded from: classes3.dex */
public class DefaultTimeBar extends View implements o {
    public final int A;
    public final int B;
    public final int C;
    public final StringBuilder D;
    public final Formatter E;
    public final Runnable F;
    public final CopyOnWriteArraySet<o.a> G;
    public final Point H;
    public final float I;
    public int J;
    public long K;
    public int L;
    public Rect M;
    public ValueAnimator N;
    public float O;
    public boolean P;
    public long Q;
    public long R;
    public long S;
    public long T;
    public int U;
    @Nullable
    public long[] V;
    @Nullable
    public boolean[] W;
    public final Rect j;
    public final Rect k;
    public final Rect l;
    public final Rect m;
    public final Paint n;
    public final Paint o;
    public final Paint p;
    public final Paint q;
    public final Paint r;

    /* renamed from: s  reason: collision with root package name */
    public final Paint f2926s;
    @Nullable
    public final Drawable t;
    public final int u;
    public final int v;
    public final int w;

    /* renamed from: x  reason: collision with root package name */
    public final int f2927x;

    /* renamed from: y  reason: collision with root package name */
    public final int f2928y;

    /* renamed from: z  reason: collision with root package name */
    public final int f2929z;

    public DefaultTimeBar(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0, attributeSet);
    }

    public static int c(float f, int i) {
        return (int) ((i * f) + 0.5f);
    }

    private long getPositionIncrement() {
        long j = this.K;
        if (j != -9223372036854775807L) {
            return j;
        }
        long j2 = this.R;
        if (j2 == -9223372036854775807L) {
            return 0L;
        }
        return j2 / this.J;
    }

    private String getProgressText() {
        return e0.u(this.D, this.E, this.S);
    }

    private long getScrubberPosition() {
        if (this.k.width() <= 0 || this.R == -9223372036854775807L) {
            return 0L;
        }
        return (this.m.width() * this.R) / this.k.width();
    }

    @Override // b.i.a.c.d3.o
    public void a(@Nullable long[] jArr, @Nullable boolean[] zArr, int i) {
        d.j(i == 0 || !(jArr == null || zArr == null));
        this.U = i;
        this.V = jArr;
        this.W = zArr;
        h();
    }

    @Override // b.i.a.c.d3.o
    public void b(o.a aVar) {
        this.G.add(aVar);
    }

    public final void d(float f) {
        Rect rect = this.m;
        Rect rect2 = this.k;
        rect.right = e0.h((int) f, rect2.left, rect2.right);
    }

    @Override // android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        i();
    }

    public final boolean e(long j) {
        long j2 = this.R;
        if (j2 <= 0) {
            return false;
        }
        long j3 = this.P ? this.Q : this.S;
        long i = e0.i(j3 + j, 0L, j2);
        if (i == j3) {
            return false;
        }
        if (!this.P) {
            f(i);
        } else {
            j(i);
        }
        h();
        return true;
    }

    public final void f(long j) {
        this.Q = j;
        this.P = true;
        setPressed(true);
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
        Iterator<o.a> it = this.G.iterator();
        while (it.hasNext()) {
            it.next().m(this, j);
        }
    }

    public final void g(boolean z2) {
        removeCallbacks(this.F);
        this.P = false;
        setPressed(false);
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(false);
        }
        invalidate();
        Iterator<o.a> it = this.G.iterator();
        while (it.hasNext()) {
            it.next().l(this, this.Q, z2);
        }
    }

    @Override // b.i.a.c.d3.o
    public long getPreferredUpdateDelay() {
        int width = (int) (this.k.width() / this.I);
        if (width != 0) {
            long j = this.R;
            if (!(j == 0 || j == -9223372036854775807L)) {
                return j / width;
            }
        }
        return RecyclerView.FOREVER_NS;
    }

    public final void h() {
        this.l.set(this.k);
        this.m.set(this.k);
        long j = this.P ? this.Q : this.S;
        if (this.R > 0) {
            int width = (int) ((this.k.width() * this.T) / this.R);
            Rect rect = this.l;
            Rect rect2 = this.k;
            rect.right = Math.min(rect2.left + width, rect2.right);
            int width2 = (int) ((this.k.width() * j) / this.R);
            Rect rect3 = this.m;
            Rect rect4 = this.k;
            rect3.right = Math.min(rect4.left + width2, rect4.right);
        } else {
            Rect rect5 = this.l;
            int i = this.k.left;
            rect5.right = i;
            this.m.right = i;
        }
        invalidate(this.j);
    }

    public final void i() {
        Drawable drawable = this.t;
        if (drawable != null && drawable.isStateful() && this.t.setState(getDrawableState())) {
            invalidate();
        }
    }

    public final void j(long j) {
        if (this.Q != j) {
            this.Q = j;
            Iterator<o.a> it = this.G.iterator();
            while (it.hasNext()) {
                it.next().k(this, j);
            }
        }
    }

    @Override // android.view.View
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.t;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        int i;
        canvas.save();
        int height = this.k.height();
        int centerY = this.k.centerY() - (height / 2);
        int i2 = height + centerY;
        if (this.R <= 0) {
            Rect rect = this.k;
            canvas.drawRect(rect.left, centerY, rect.right, i2, this.p);
        } else {
            Rect rect2 = this.l;
            int i3 = rect2.left;
            int i4 = rect2.right;
            int max = Math.max(Math.max(this.k.left, i4), this.m.right);
            int i5 = this.k.right;
            if (max < i5) {
                canvas.drawRect(max, centerY, i5, i2, this.p);
            }
            int max2 = Math.max(i3, this.m.right);
            if (i4 > max2) {
                canvas.drawRect(max2, centerY, i4, i2, this.o);
            }
            if (this.m.width() > 0) {
                Rect rect3 = this.m;
                canvas.drawRect(rect3.left, centerY, rect3.right, i2, this.n);
            }
            if (this.U != 0) {
                long[] jArr = this.V;
                Objects.requireNonNull(jArr);
                boolean[] zArr = this.W;
                Objects.requireNonNull(zArr);
                int i6 = this.f2927x / 2;
                for (int i7 = 0; i7 < this.U; i7++) {
                    long i8 = e0.i(jArr[i7], 0L, this.R);
                    Rect rect4 = this.k;
                    int min = Math.min(rect4.width() - this.f2927x, Math.max(0, ((int) ((this.k.width() * i8) / this.R)) - i6)) + rect4.left;
                    canvas.drawRect(min, centerY, min + this.f2927x, i2, zArr[i7] ? this.r : this.q);
                }
            }
        }
        if (this.R > 0) {
            Rect rect5 = this.m;
            int h = e0.h(rect5.right, rect5.left, this.k.right);
            int centerY2 = this.m.centerY();
            Drawable drawable = this.t;
            if (drawable == null) {
                if (this.P || isFocused()) {
                    i = this.A;
                } else {
                    i = isEnabled() ? this.f2928y : this.f2929z;
                }
                canvas.drawCircle(h, centerY2, (int) ((i * this.O) / 2.0f), this.f2926s);
            } else {
                int intrinsicWidth = ((int) (drawable.getIntrinsicWidth() * this.O)) / 2;
                int intrinsicHeight = ((int) (this.t.getIntrinsicHeight() * this.O)) / 2;
                this.t.setBounds(h - intrinsicWidth, centerY2 - intrinsicHeight, h + intrinsicWidth, centerY2 + intrinsicHeight);
                this.t.draw(canvas);
            }
        }
        canvas.restore();
    }

    @Override // android.view.View
    public void onFocusChanged(boolean z2, int i, @Nullable Rect rect) {
        super.onFocusChanged(z2, i, rect);
        if (this.P && !z2) {
            g(false);
        }
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        if (accessibilityEvent.getEventType() == 4) {
            accessibilityEvent.getText().add(getProgressText());
        }
        accessibilityEvent.setClassName("android.widget.SeekBar");
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName("android.widget.SeekBar");
        accessibilityNodeInfo.setContentDescription(getProgressText());
        if (this.R > 0) {
            if (e0.a >= 21) {
                accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
                accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
                return;
            }
            accessibilityNodeInfo.addAction(4096);
            accessibilityNodeInfo.addAction(8192);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x001a  */
    @Override // android.view.View, android.view.KeyEvent.Callback
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean onKeyDown(int r5, android.view.KeyEvent r6) {
        /*
            r4 = this;
            boolean r0 = r4.isEnabled()
            if (r0 == 0) goto L30
            long r0 = r4.getPositionIncrement()
            r2 = 66
            r3 = 1
            if (r5 == r2) goto L27
            switch(r5) {
                case 21: goto L13;
                case 22: goto L14;
                case 23: goto L27;
                default: goto L12;
            }
        L12:
            goto L30
        L13:
            long r0 = -r0
        L14:
            boolean r0 = r4.e(r0)
            if (r0 == 0) goto L30
            java.lang.Runnable r5 = r4.F
            r4.removeCallbacks(r5)
            java.lang.Runnable r5 = r4.F
            r0 = 1000(0x3e8, double:4.94E-321)
            r4.postDelayed(r5, r0)
            return r3
        L27:
            boolean r0 = r4.P
            if (r0 == 0) goto L30
            r5 = 0
            r4.g(r5)
            return r3
        L30:
            boolean r5 = super.onKeyDown(r5, r6)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.DefaultTimeBar.onKeyDown(int, android.view.KeyEvent):boolean");
    }

    @Override // android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        Rect rect;
        int i7 = i3 - i;
        int i8 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingRight = i7 - getPaddingRight();
        int i9 = this.B;
        if (this.w == 1) {
            i6 = (i8 - getPaddingBottom()) - this.v;
            int i10 = this.u;
            i5 = ((i8 - getPaddingBottom()) - i10) - Math.max(i9 - (i10 / 2), 0);
        } else {
            i6 = (i8 - this.v) / 2;
            i5 = (i8 - this.u) / 2;
        }
        this.j.set(paddingLeft, i6, paddingRight, this.v + i6);
        Rect rect2 = this.k;
        Rect rect3 = this.j;
        rect2.set(rect3.left + i9, i5, rect3.right - i9, this.u + i5);
        if (e0.a >= 29 && !((rect = this.M) != null && rect.width() == i7 && this.M.height() == i8)) {
            Rect rect4 = new Rect(0, 0, i7, i8);
            this.M = rect4;
            setSystemGestureExclusionRects(Collections.singletonList(rect4));
        }
        h();
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 0) {
            size = this.v;
        } else if (mode != 1073741824) {
            size = Math.min(this.v, size);
        }
        setMeasuredDimension(View.MeasureSpec.getSize(i), size);
        i();
    }

    @Override // android.view.View
    public void onRtlPropertiesChanged(int i) {
        Drawable drawable = this.t;
        if (drawable != null) {
            if (e0.a >= 23 && drawable.setLayoutDirection(i)) {
                invalidate();
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:12:0x0033, code lost:
        if (r3 != 3) goto L32;
     */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean onTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            boolean r0 = r7.isEnabled()
            r1 = 0
            if (r0 == 0) goto L88
            long r2 = r7.R
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 > 0) goto L11
            goto L88
        L11:
            android.graphics.Point r0 = r7.H
            float r2 = r8.getX()
            int r2 = (int) r2
            float r3 = r8.getY()
            int r3 = (int) r3
            r0.set(r2, r3)
            android.graphics.Point r0 = r7.H
            int r2 = r0.x
            int r0 = r0.y
            int r3 = r8.getAction()
            r4 = 1
            if (r3 == 0) goto L6b
            r5 = 3
            if (r3 == r4) goto L5c
            r6 = 2
            if (r3 == r6) goto L36
            if (r3 == r5) goto L5c
            goto L88
        L36:
            boolean r8 = r7.P
            if (r8 == 0) goto L88
            int r8 = r7.C
            if (r0 >= r8) goto L48
            int r8 = r7.L
            int r2 = r2 - r8
            int r2 = r2 / r5
            int r2 = r2 + r8
            float r8 = (float) r2
            r7.d(r8)
            goto L4e
        L48:
            r7.L = r2
            float r8 = (float) r2
            r7.d(r8)
        L4e:
            long r0 = r7.getScrubberPosition()
            r7.j(r0)
            r7.h()
            r7.invalidate()
            return r4
        L5c:
            boolean r0 = r7.P
            if (r0 == 0) goto L88
            int r8 = r8.getAction()
            if (r8 != r5) goto L67
            r1 = 1
        L67:
            r7.g(r1)
            return r4
        L6b:
            float r8 = (float) r2
            float r0 = (float) r0
            android.graphics.Rect r2 = r7.j
            int r3 = (int) r8
            int r0 = (int) r0
            boolean r0 = r2.contains(r3, r0)
            if (r0 == 0) goto L88
            r7.d(r8)
            long r0 = r7.getScrubberPosition()
            r7.f(r0)
            r7.h()
            r7.invalidate()
            return r4
        L88:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.DefaultTimeBar.onTouchEvent(android.view.MotionEvent):boolean");
    }

    @Override // android.view.View
    public boolean performAccessibilityAction(int i, @Nullable Bundle bundle) {
        if (super.performAccessibilityAction(i, bundle)) {
            return true;
        }
        if (this.R <= 0) {
            return false;
        }
        if (i == 8192) {
            if (e(-getPositionIncrement())) {
                g(false);
            }
        } else if (i != 4096) {
            return false;
        } else {
            if (e(getPositionIncrement())) {
                g(false);
            }
        }
        sendAccessibilityEvent(4);
        return true;
    }

    public void setAdMarkerColor(@ColorInt int i) {
        this.q.setColor(i);
        invalidate(this.j);
    }

    public void setBufferedColor(@ColorInt int i) {
        this.o.setColor(i);
        invalidate(this.j);
    }

    @Override // b.i.a.c.d3.o
    public void setBufferedPosition(long j) {
        if (this.T != j) {
            this.T = j;
            h();
        }
    }

    @Override // b.i.a.c.d3.o
    public void setDuration(long j) {
        if (this.R != j) {
            this.R = j;
            if (this.P && j == -9223372036854775807L) {
                g(true);
            }
            h();
        }
    }

    @Override // android.view.View, b.i.a.c.d3.o
    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        if (this.P && !z2) {
            g(true);
        }
    }

    public void setKeyCountIncrement(int i) {
        d.j(i > 0);
        this.J = i;
        this.K = -9223372036854775807L;
    }

    public void setKeyTimeIncrement(long j) {
        d.j(j > 0);
        this.J = -1;
        this.K = j;
    }

    public void setPlayedAdMarkerColor(@ColorInt int i) {
        this.r.setColor(i);
        invalidate(this.j);
    }

    public void setPlayedColor(@ColorInt int i) {
        this.n.setColor(i);
        invalidate(this.j);
    }

    @Override // b.i.a.c.d3.o
    public void setPosition(long j) {
        if (this.S != j) {
            this.S = j;
            setContentDescription(getProgressText());
            h();
        }
    }

    public void setScrubberColor(@ColorInt int i) {
        this.f2926s.setColor(i);
        invalidate(this.j);
    }

    public void setUnplayedColor(@ColorInt int i) {
        this.p.setColor(i);
        invalidate(this.j);
    }

    public DefaultTimeBar(Context context, @Nullable AttributeSet attributeSet, int i, @Nullable AttributeSet attributeSet2) {
        super(context, attributeSet, i);
        this.j = new Rect();
        this.k = new Rect();
        this.l = new Rect();
        this.m = new Rect();
        Paint paint = new Paint();
        this.n = paint;
        Paint paint2 = new Paint();
        this.o = paint2;
        Paint paint3 = new Paint();
        this.p = paint3;
        Paint paint4 = new Paint();
        this.q = paint4;
        Paint paint5 = new Paint();
        this.r = paint5;
        Paint paint6 = new Paint();
        this.f2926s = paint6;
        paint6.setAntiAlias(true);
        this.G = new CopyOnWriteArraySet<>();
        this.H = new Point();
        float f = context.getResources().getDisplayMetrics().density;
        this.I = f;
        this.C = c(f, -50);
        int c = c(f, 4);
        int c2 = c(f, 26);
        int c3 = c(f, 4);
        int c4 = c(f, 12);
        int c5 = c(f, 0);
        int c6 = c(f, 16);
        if (attributeSet2 != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet2, R.g.DefaultTimeBar, i, 0);
            try {
                Drawable drawable = obtainStyledAttributes.getDrawable(R.g.DefaultTimeBar_scrubber_drawable);
                this.t = drawable;
                if (drawable != null) {
                    int i2 = e0.a;
                    if (i2 >= 23) {
                        int layoutDirection = getLayoutDirection();
                        if (i2 >= 23) {
                            drawable.setLayoutDirection(layoutDirection);
                        }
                    }
                    c2 = Math.max(drawable.getMinimumHeight(), c2);
                }
                this.u = obtainStyledAttributes.getDimensionPixelSize(R.g.DefaultTimeBar_bar_height, c);
                this.v = obtainStyledAttributes.getDimensionPixelSize(R.g.DefaultTimeBar_touch_target_height, c2);
                this.w = obtainStyledAttributes.getInt(R.g.DefaultTimeBar_bar_gravity, 0);
                this.f2927x = obtainStyledAttributes.getDimensionPixelSize(R.g.DefaultTimeBar_ad_marker_width, c3);
                this.f2928y = obtainStyledAttributes.getDimensionPixelSize(R.g.DefaultTimeBar_scrubber_enabled_size, c4);
                this.f2929z = obtainStyledAttributes.getDimensionPixelSize(R.g.DefaultTimeBar_scrubber_disabled_size, c5);
                this.A = obtainStyledAttributes.getDimensionPixelSize(R.g.DefaultTimeBar_scrubber_dragged_size, c6);
                int i3 = obtainStyledAttributes.getInt(R.g.DefaultTimeBar_played_color, -1);
                int i4 = obtainStyledAttributes.getInt(R.g.DefaultTimeBar_scrubber_color, -1);
                int i5 = obtainStyledAttributes.getInt(R.g.DefaultTimeBar_buffered_color, -855638017);
                int i6 = obtainStyledAttributes.getInt(R.g.DefaultTimeBar_unplayed_color, 872415231);
                int i7 = obtainStyledAttributes.getInt(R.g.DefaultTimeBar_ad_marker_color, -1291845888);
                int i8 = obtainStyledAttributes.getInt(R.g.DefaultTimeBar_played_ad_marker_color, 872414976);
                paint.setColor(i3);
                paint6.setColor(i4);
                paint2.setColor(i5);
                paint3.setColor(i6);
                paint4.setColor(i7);
                paint5.setColor(i8);
            } finally {
                obtainStyledAttributes.recycle();
            }
        } else {
            this.u = c;
            this.v = c2;
            this.w = 0;
            this.f2927x = c3;
            this.f2928y = c4;
            this.f2929z = c5;
            this.A = c6;
            paint.setColor(-1);
            paint6.setColor(-1);
            paint2.setColor(-855638017);
            paint3.setColor(872415231);
            paint4.setColor(-1291845888);
            paint5.setColor(872414976);
            this.t = null;
        }
        StringBuilder sb = new StringBuilder();
        this.D = sb;
        this.E = new Formatter(sb, Locale.getDefault());
        this.F = new Runnable() { // from class: b.i.a.c.d3.b
            @Override // java.lang.Runnable
            public final void run() {
                DefaultTimeBar.this.g(false);
            }
        };
        Drawable drawable2 = this.t;
        if (drawable2 != null) {
            this.B = (drawable2.getMinimumWidth() + 1) / 2;
        } else {
            this.B = (Math.max(this.f2929z, Math.max(this.f2928y, this.A)) + 1) / 2;
        }
        this.O = 1.0f;
        ValueAnimator valueAnimator = new ValueAnimator();
        this.N = valueAnimator;
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: b.i.a.c.d3.c
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                DefaultTimeBar defaultTimeBar = DefaultTimeBar.this;
                Objects.requireNonNull(defaultTimeBar);
                defaultTimeBar.O = ((Float) valueAnimator2.getAnimatedValue()).floatValue();
                defaultTimeBar.invalidate(defaultTimeBar.j);
            }
        });
        this.R = -9223372036854775807L;
        this.K = -9223372036854775807L;
        this.J = 20;
        setFocusable(true);
        if (getImportantForAccessibility() == 0) {
            setImportantForAccessibility(1);
        }
    }
}
