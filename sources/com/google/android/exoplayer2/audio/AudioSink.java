package com.google.android.exoplayer2.audio;

import androidx.annotation.Nullable;
import b.i.a.c.j1;
import b.i.a.c.t2.o;
import b.i.a.c.t2.u;
import b.i.a.c.x1;
import java.nio.ByteBuffer;
/* loaded from: classes3.dex */
public interface AudioSink {

    /* loaded from: classes3.dex */
    public static final class InitializationException extends Exception {
        public final int audioTrackState;
        public final j1 format;
        public final boolean isRecoverable;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public InitializationException(int r4, int r5, int r6, int r7, b.i.a.c.j1 r8, boolean r9, @androidx.annotation.Nullable java.lang.Exception r10) {
            /*
                r3 = this;
                if (r9 == 0) goto L5
                java.lang.String r0 = " (recoverable)"
                goto L7
            L5:
                java.lang.String r0 = ""
            L7:
                int r1 = r0.length()
                int r1 = r1 + 80
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>(r1)
                java.lang.String r1 = "AudioTrack init failed "
                r2.append(r1)
                r2.append(r4)
                java.lang.String r1 = " "
                r2.append(r1)
                java.lang.String r1 = "Config("
                r2.append(r1)
                r2.append(r5)
                java.lang.String r5 = ", "
                r2.append(r5)
                r2.append(r6)
                r2.append(r5)
                r2.append(r7)
                java.lang.String r5 = ")"
                r2.append(r5)
                r2.append(r0)
                java.lang.String r5 = r2.toString()
                r3.<init>(r5, r10)
                r3.audioTrackState = r4
                r3.isRecoverable = r9
                r3.format = r8
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.AudioSink.InitializationException.<init>(int, int, int, int, b.i.a.c.j1, boolean, java.lang.Exception):void");
        }
    }

    /* loaded from: classes3.dex */
    public static final class UnexpectedDiscontinuityException extends Exception {
        public final long actualPresentationTimeUs;
        public final long expectedPresentationTimeUs;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public UnexpectedDiscontinuityException(long r4, long r6) {
            /*
                r3 = this;
                r0 = 103(0x67, float:1.44E-43)
                java.lang.String r1 = "Unexpected audio track timestamp discontinuity: expected "
                java.lang.String r2 = ", got "
                java.lang.StringBuilder r0 = b.d.b.a.a.P(r0, r1, r6, r2)
                r0.append(r4)
                java.lang.String r0 = r0.toString()
                r3.<init>(r0)
                r3.actualPresentationTimeUs = r4
                r3.expectedPresentationTimeUs = r6
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.AudioSink.UnexpectedDiscontinuityException.<init>(long, long):void");
        }
    }

    /* loaded from: classes3.dex */
    public static final class WriteException extends Exception {
        public final int errorCode;
        public final j1 format;
        public final boolean isRecoverable;

        public WriteException(int i, j1 j1Var, boolean z2) {
            super(b.d.b.a.a.f(36, "AudioTrack write failed: ", i));
            this.isRecoverable = z2;
            this.errorCode = i;
            this.format = j1Var;
        }
    }

    /* loaded from: classes3.dex */
    public interface a {
    }

    boolean a(j1 j1Var);

    boolean b();

    x1 c();

    void d();

    void e();

    void flush();

    void i(x1 x1Var);

    void j() throws WriteException;

    boolean k();

    void l(int i);

    long m(boolean z2);

    void n();

    void o(o oVar);

    void p();

    void q(float f);

    void r();

    void reset();

    boolean s(ByteBuffer byteBuffer, long j, int i) throws InitializationException, WriteException;

    void t(a aVar);

    int u(j1 j1Var);

    void v(j1 j1Var, int i, @Nullable int[] iArr) throws ConfigurationException;

    void w(boolean z2);

    void x(u uVar);

    /* loaded from: classes3.dex */
    public static final class ConfigurationException extends Exception {
        public final j1 format;

        public ConfigurationException(Throwable th, j1 j1Var) {
            super(th);
            this.format = j1Var;
        }

        public ConfigurationException(String str, j1 j1Var) {
            super(str);
            this.format = j1Var;
        }
    }
}
