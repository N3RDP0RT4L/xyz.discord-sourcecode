package com.google.android.exoplayer2.audio;

import b.i.a.c.f3.e0;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
/* loaded from: classes3.dex */
public interface AudioProcessor {
    public static final ByteBuffer a = ByteBuffer.allocateDirect(0).order(ByteOrder.nativeOrder());

    /* loaded from: classes3.dex */
    public static final class UnhandledAudioFormatException extends Exception {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public UnhandledAudioFormatException(com.google.android.exoplayer2.audio.AudioProcessor.a r3) {
            /*
                r2 = this;
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r0 = r3.length()
                int r0 = r0 + 18
                java.lang.String r1 = "Unhandled format: "
                java.lang.String r3 = b.d.b.a.a.i(r0, r1, r3)
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.AudioProcessor.UnhandledAudioFormatException.<init>(com.google.android.exoplayer2.audio.AudioProcessor$a):void");
        }
    }

    /* loaded from: classes3.dex */
    public static final class a {
        public static final a a = new a(-1, -1, -1);

        /* renamed from: b  reason: collision with root package name */
        public final int f2886b;
        public final int c;
        public final int d;
        public final int e;

        public a(int i, int i2, int i3) {
            this.f2886b = i;
            this.c = i2;
            this.d = i3;
            this.e = e0.z(i3) ? e0.s(i3, i2) : -1;
        }

        public String toString() {
            int i = this.f2886b;
            int i2 = this.c;
            int i3 = this.d;
            StringBuilder sb = new StringBuilder(83);
            sb.append("AudioFormat[sampleRate=");
            sb.append(i);
            sb.append(", channelCount=");
            sb.append(i2);
            sb.append(", encoding=");
            sb.append(i3);
            sb.append(']');
            return sb.toString();
        }
    }

    boolean a();

    boolean b();

    void c(ByteBuffer byteBuffer);

    a d(a aVar) throws UnhandledAudioFormatException;

    void e();

    void flush();

    ByteBuffer getOutput();

    void reset();
}
