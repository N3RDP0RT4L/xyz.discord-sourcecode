package com.google.android.exoplayer2.audio;

import android.annotation.SuppressLint;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.PlaybackParams;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.i.a.c.f2;
import b.i.a.c.f3.e0;
import b.i.a.c.f3.q;
import b.i.a.c.j1;
import b.i.a.c.t2.b0;
import b.i.a.c.t2.c0;
import b.i.a.c.t2.d0;
import b.i.a.c.t2.f0;
import b.i.a.c.t2.o;
import b.i.a.c.t2.p;
import b.i.a.c.t2.r;
import b.i.a.c.t2.s;
import b.i.a.c.t2.t;
import b.i.a.c.t2.u;
import b.i.a.c.t2.w;
import b.i.a.c.t2.y;
import b.i.a.c.t2.z;
import b.i.a.c.x1;
import com.discord.api.permission.Permission;
import com.discord.restapi.RestAPIAbortCodes;
import com.google.android.exoplayer2.audio.AudioProcessor;
import com.google.android.exoplayer2.audio.AudioSink;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.Executor;
import org.objectweb.asm.Opcodes;
/* loaded from: classes3.dex */
public final class DefaultAudioSink implements AudioSink {
    public long A;
    public long B;
    public long C;
    public int D;
    public boolean E;
    public boolean F;
    public long G;
    public float H;
    public AudioProcessor[] I;
    public ByteBuffer[] J;
    @Nullable
    public ByteBuffer K;
    public int L;
    @Nullable
    public ByteBuffer M;
    public byte[] N;
    public int O;
    public int P;
    public boolean Q;
    public boolean R;
    public boolean S;
    public boolean T;
    public int U;
    public u V;
    public boolean W;
    public long X;
    public boolean Y;
    public boolean Z;
    @Nullable
    public final p a;

    /* renamed from: b  reason: collision with root package name */
    public final b f2887b;
    public final boolean c;
    public final w d;
    public final f0 e;
    public final AudioProcessor[] f;
    public final AudioProcessor[] g;
    public final ConditionVariable h;
    public final t i;
    public final ArrayDeque<e> j;
    public final boolean k;
    public final int l;
    public h m;
    public final f<AudioSink.InitializationException> n;
    public final f<AudioSink.WriteException> o;
    @Nullable
    public AudioSink.a p;
    @Nullable
    public c q;
    public c r;
    @Nullable

    /* renamed from: s  reason: collision with root package name */
    public AudioTrack f2888s;
    public o t;
    @Nullable
    public e u;
    public e v;
    public x1 w;
    @Nullable

    /* renamed from: x  reason: collision with root package name */
    public ByteBuffer f2889x;

    /* renamed from: y  reason: collision with root package name */
    public int f2890y;

    /* renamed from: z  reason: collision with root package name */
    public long f2891z;

    /* loaded from: classes3.dex */
    public static final class InvalidAudioTrackTimestampException extends RuntimeException {
    }

    /* loaded from: classes3.dex */
    public class a extends Thread {
        public final /* synthetic */ AudioTrack j;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(String str, AudioTrack audioTrack) {
            super(str);
            this.j = audioTrack;
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            try {
                this.j.flush();
                this.j.release();
            } finally {
                DefaultAudioSink.this.h.open();
            }
        }
    }

    /* loaded from: classes3.dex */
    public interface b {
        x1 a(x1 x1Var);

        long b(long j);

        long c();

        boolean d(boolean z2);
    }

    /* loaded from: classes3.dex */
    public static final class c {
        public final j1 a;

        /* renamed from: b  reason: collision with root package name */
        public final int f2892b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final AudioProcessor[] i;

        public c(j1 j1Var, int i, int i2, int i3, int i4, int i5, int i6, int i7, boolean z2, AudioProcessor[] audioProcessorArr) {
            int i8;
            this.a = j1Var;
            this.f2892b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
            this.f = i5;
            this.g = i6;
            this.i = audioProcessorArr;
            if (i7 != 0) {
                i8 = i7;
            } else {
                boolean z3 = true;
                if (i2 == 0) {
                    float f = z2 ? 8.0f : 1.0f;
                    int minBufferSize = AudioTrack.getMinBufferSize(i4, i5, i6);
                    b.c.a.a0.d.D(minBufferSize == -2 ? false : z3);
                    long j = i4;
                    int h = e0.h(minBufferSize * 4, ((int) ((250000 * j) / 1000000)) * i3, Math.max(minBufferSize, ((int) ((j * 750000) / 1000000)) * i3));
                    i8 = f != 1.0f ? Math.round(h * f) : h;
                } else if (i2 == 1) {
                    i8 = e(50000000L);
                } else if (i2 == 2) {
                    i8 = e(250000L);
                } else {
                    throw new IllegalStateException();
                }
            }
            this.h = i8;
        }

        @RequiresApi(21)
        public static AudioAttributes d(o oVar, boolean z2) {
            if (z2) {
                return new AudioAttributes.Builder().setContentType(3).setFlags(16).setUsage(1).build();
            }
            return oVar.a();
        }

        public AudioTrack a(boolean z2, o oVar, int i) throws AudioSink.InitializationException {
            try {
                AudioTrack b2 = b(z2, oVar, i);
                int state = b2.getState();
                if (state == 1) {
                    return b2;
                }
                try {
                    b2.release();
                } catch (Exception unused) {
                }
                throw new AudioSink.InitializationException(state, this.e, this.f, this.h, this.a, f(), null);
            } catch (IllegalArgumentException | UnsupportedOperationException e) {
                throw new AudioSink.InitializationException(0, this.e, this.f, this.h, this.a, f(), e);
            }
        }

        public final AudioTrack b(boolean z2, o oVar, int i) {
            int i2 = e0.a;
            if (i2 >= 29) {
                AudioFormat y2 = DefaultAudioSink.y(this.e, this.f, this.g);
                AudioAttributes d = d(oVar, z2);
                boolean z3 = true;
                AudioTrack.Builder sessionId = new AudioTrack.Builder().setAudioAttributes(d).setAudioFormat(y2).setTransferMode(1).setBufferSizeInBytes(this.h).setSessionId(i);
                if (this.c != 1) {
                    z3 = false;
                }
                return sessionId.setOffloadedPlayback(z3).build();
            } else if (i2 >= 21) {
                return new AudioTrack(d(oVar, z2), DefaultAudioSink.y(this.e, this.f, this.g), this.h, 1, i);
            } else {
                int t = e0.t(oVar.m);
                if (i == 0) {
                    return new AudioTrack(t, this.e, this.f, this.g, this.h, 1);
                }
                return new AudioTrack(t, this.e, this.f, this.g, this.h, 1, i);
            }
        }

        public long c(long j) {
            return (j * 1000000) / this.e;
        }

        public final int e(long j) {
            int i;
            int i2 = this.g;
            switch (i2) {
                case 5:
                    i = RestAPIAbortCodes.RELATIONSHIP_INCOMING_DISABLED;
                    break;
                case 6:
                case 18:
                    i = 768000;
                    break;
                case 7:
                    i = 192000;
                    break;
                case 8:
                    i = 2250000;
                    break;
                case 9:
                    i = 40000;
                    break;
                case 10:
                    i = 100000;
                    break;
                case 11:
                    i = 16000;
                    break;
                case 12:
                    i = 7000;
                    break;
                case 13:
                default:
                    throw new IllegalArgumentException();
                case 14:
                    i = 3062500;
                    break;
                case 15:
                    i = 8000;
                    break;
                case 16:
                    i = 256000;
                    break;
                case 17:
                    i = 336000;
                    break;
            }
            if (i2 == 5) {
                i *= 2;
            }
            return (int) ((j * i) / 1000000);
        }

        public boolean f() {
            return this.c == 1;
        }
    }

    /* loaded from: classes3.dex */
    public static class d implements b {
        public final AudioProcessor[] a;

        /* renamed from: b  reason: collision with root package name */
        public final c0 f2893b;
        public final b.i.a.c.t2.e0 c;

        public d(AudioProcessor... audioProcessorArr) {
            c0 c0Var = new c0();
            b.i.a.c.t2.e0 e0Var = new b.i.a.c.t2.e0();
            AudioProcessor[] audioProcessorArr2 = new AudioProcessor[audioProcessorArr.length + 2];
            this.a = audioProcessorArr2;
            System.arraycopy(audioProcessorArr, 0, audioProcessorArr2, 0, audioProcessorArr.length);
            this.f2893b = c0Var;
            this.c = e0Var;
            audioProcessorArr2[audioProcessorArr.length] = c0Var;
            audioProcessorArr2[audioProcessorArr.length + 1] = e0Var;
        }

        @Override // com.google.android.exoplayer2.audio.DefaultAudioSink.b
        public x1 a(x1 x1Var) {
            b.i.a.c.t2.e0 e0Var = this.c;
            float f = x1Var.k;
            if (e0Var.c != f) {
                e0Var.c = f;
                e0Var.i = true;
            }
            float f2 = x1Var.l;
            if (e0Var.d != f2) {
                e0Var.d = f2;
                e0Var.i = true;
            }
            return x1Var;
        }

        @Override // com.google.android.exoplayer2.audio.DefaultAudioSink.b
        public long b(long j) {
            b.i.a.c.t2.e0 e0Var = this.c;
            if (e0Var.o < Permission.VIEW_CHANNEL) {
                return (long) (e0Var.c * j);
            }
            long j2 = e0Var.n;
            d0 d0Var = e0Var.j;
            Objects.requireNonNull(d0Var);
            long j3 = j2 - ((d0Var.k * d0Var.f1114b) * 2);
            int i = e0Var.h.f2886b;
            int i2 = e0Var.g.f2886b;
            if (i == i2) {
                return e0.F(j, j3, e0Var.o);
            }
            return e0.F(j, j3 * i, e0Var.o * i2);
        }

        @Override // com.google.android.exoplayer2.audio.DefaultAudioSink.b
        public long c() {
            return this.f2893b.t;
        }

        @Override // com.google.android.exoplayer2.audio.DefaultAudioSink.b
        public boolean d(boolean z2) {
            this.f2893b.m = z2;
            return z2;
        }
    }

    /* loaded from: classes3.dex */
    public static final class e {
        public final x1 a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2894b;
        public final long c;
        public final long d;

        public e(x1 x1Var, boolean z2, long j, long j2, a aVar) {
            this.a = x1Var;
            this.f2894b = z2;
            this.c = j;
            this.d = j2;
        }
    }

    /* loaded from: classes3.dex */
    public static final class f<T extends Exception> {
        @Nullable
        public T a;

        /* renamed from: b  reason: collision with root package name */
        public long f2895b;

        public f(long j) {
        }

        public void a(T t) throws Exception {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (this.a == null) {
                this.a = t;
                this.f2895b = 100 + elapsedRealtime;
            }
            if (elapsedRealtime >= this.f2895b) {
                T t2 = this.a;
                if (t2 != t) {
                    t2.addSuppressed(t);
                }
                T t3 = this.a;
                this.a = null;
                throw t3;
            }
        }
    }

    /* loaded from: classes3.dex */
    public final class g implements t.a {
        public g(a aVar) {
        }

        @Override // b.i.a.c.t2.t.a
        public void a(final long j) {
            final r.a aVar;
            Handler handler;
            AudioSink.a aVar2 = DefaultAudioSink.this.p;
            if (aVar2 != null && (handler = (aVar = z.this.O0).a) != null) {
                handler.post(new Runnable() { // from class: b.i.a.c.t2.h
                    @Override // java.lang.Runnable
                    public final void run() {
                        r.a aVar3 = r.a.this;
                        long j2 = j;
                        r rVar = aVar3.f1122b;
                        int i = e0.a;
                        rVar.R(j2);
                    }
                });
            }
        }

        @Override // b.i.a.c.t2.t.a
        public void b(final int i, final long j) {
            if (DefaultAudioSink.this.p != null) {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                DefaultAudioSink defaultAudioSink = DefaultAudioSink.this;
                final long j2 = elapsedRealtime - defaultAudioSink.X;
                final r.a aVar = z.this.O0;
                Handler handler = aVar.a;
                if (handler != null) {
                    handler.post(new Runnable() { // from class: b.i.a.c.t2.i
                        @Override // java.lang.Runnable
                        public final void run() {
                            r.a aVar2 = r.a.this;
                            int i2 = i;
                            long j3 = j;
                            long j4 = j2;
                            r rVar = aVar2.f1122b;
                            int i3 = e0.a;
                            rVar.e0(i2, j3, j4);
                        }
                    });
                }
            }
        }

        @Override // b.i.a.c.t2.t.a
        public void c(long j) {
            StringBuilder sb = new StringBuilder(61);
            sb.append("Ignoring impossibly large audio latency: ");
            sb.append(j);
            Log.w("DefaultAudioSink", sb.toString());
        }

        @Override // b.i.a.c.t2.t.a
        public void d(long j, long j2, long j3, long j4) {
            long j5;
            DefaultAudioSink defaultAudioSink = DefaultAudioSink.this;
            c cVar = defaultAudioSink.r;
            if (cVar.c == 0) {
                j5 = defaultAudioSink.f2891z / cVar.f2892b;
            } else {
                j5 = defaultAudioSink.A;
            }
            long E = defaultAudioSink.E();
            StringBuilder P = b.d.b.a.a.P(Opcodes.INVOKEVIRTUAL, "Spurious audio timestamp (frame position mismatch): ", j, ", ");
            P.append(j2);
            P.append(", ");
            P.append(j3);
            P.append(", ");
            P.append(j4);
            P.append(", ");
            P.append(j5);
            P.append(", ");
            P.append(E);
            Log.w("DefaultAudioSink", P.toString());
        }

        @Override // b.i.a.c.t2.t.a
        public void e(long j, long j2, long j3, long j4) {
            long j5;
            DefaultAudioSink defaultAudioSink = DefaultAudioSink.this;
            c cVar = defaultAudioSink.r;
            if (cVar.c == 0) {
                j5 = defaultAudioSink.f2891z / cVar.f2892b;
            } else {
                j5 = defaultAudioSink.A;
            }
            long E = defaultAudioSink.E();
            StringBuilder P = b.d.b.a.a.P(180, "Spurious audio timestamp (system clock mismatch): ", j, ", ");
            P.append(j2);
            P.append(", ");
            P.append(j3);
            P.append(", ");
            P.append(j4);
            P.append(", ");
            P.append(j5);
            P.append(", ");
            P.append(E);
            Log.w("DefaultAudioSink", P.toString());
        }
    }

    @RequiresApi(29)
    /* loaded from: classes3.dex */
    public final class h {
        public final Handler a = new Handler();

        /* renamed from: b  reason: collision with root package name */
        public final AudioTrack.StreamEventCallback f2896b;

        /* loaded from: classes3.dex */
        public class a extends AudioTrack.StreamEventCallback {
            public a(DefaultAudioSink defaultAudioSink) {
            }

            @Override // android.media.AudioTrack.StreamEventCallback
            public void onDataRequest(AudioTrack audioTrack, int i) {
                f2.a aVar;
                b.c.a.a0.d.D(audioTrack == DefaultAudioSink.this.f2888s);
                DefaultAudioSink defaultAudioSink = DefaultAudioSink.this;
                AudioSink.a aVar2 = defaultAudioSink.p;
                if (aVar2 != null && defaultAudioSink.S && (aVar = z.this.X0) != null) {
                    aVar.a();
                }
            }

            @Override // android.media.AudioTrack.StreamEventCallback
            public void onTearDown(AudioTrack audioTrack) {
                f2.a aVar;
                b.c.a.a0.d.D(audioTrack == DefaultAudioSink.this.f2888s);
                DefaultAudioSink defaultAudioSink = DefaultAudioSink.this;
                AudioSink.a aVar2 = defaultAudioSink.p;
                if (aVar2 != null && defaultAudioSink.S && (aVar = z.this.X0) != null) {
                    aVar.a();
                }
            }
        }

        public h() {
            this.f2896b = new a(DefaultAudioSink.this);
        }
    }

    public DefaultAudioSink(@Nullable p pVar, b bVar, boolean z2, boolean z3, int i) {
        this.a = pVar;
        this.f2887b = bVar;
        int i2 = e0.a;
        this.c = i2 >= 21 && z2;
        this.k = i2 >= 23 && z3;
        this.l = i2 < 29 ? 0 : i;
        this.h = new ConditionVariable(true);
        this.i = new t(new g(null));
        w wVar = new w();
        this.d = wVar;
        f0 f0Var = new f0();
        this.e = f0Var;
        ArrayList arrayList = new ArrayList();
        Collections.addAll(arrayList, new b0(), wVar, f0Var);
        Collections.addAll(arrayList, ((d) bVar).a);
        this.f = (AudioProcessor[]) arrayList.toArray(new AudioProcessor[0]);
        this.g = new AudioProcessor[]{new y()};
        this.H = 1.0f;
        this.t = o.j;
        this.U = 0;
        this.V = new u(0, 0.0f);
        x1 x1Var = x1.j;
        this.v = new e(x1Var, false, 0L, 0L, null);
        this.w = x1Var;
        this.P = -1;
        this.I = new AudioProcessor[0];
        this.J = new ByteBuffer[0];
        this.j = new ArrayDeque<>();
        this.n = new f<>(100L);
        this.o = new f<>(100L);
    }

    /* JADX WARN: Code restructure failed: missing block: B:54:0x00b2, code lost:
        if (r2 != 5) goto L55;
     */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00cd A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:65:0x00ce  */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static android.util.Pair<java.lang.Integer, java.lang.Integer> A(b.i.a.c.j1 r13, @androidx.annotation.Nullable b.i.a.c.t2.p r14) {
        /*
            Method dump skipped, instructions count: 219
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.DefaultAudioSink.A(b.i.a.c.j1, b.i.a.c.t2.p):android.util.Pair");
    }

    public static boolean H(AudioTrack audioTrack) {
        return e0.a >= 29 && audioTrack.isOffloadedPlayback();
    }

    @RequiresApi(21)
    public static AudioFormat y(int i, int i2, int i3) {
        return new AudioFormat.Builder().setSampleRate(i).setChannelMask(i2).setEncoding(i3).build();
    }

    public final e B() {
        e eVar = this.u;
        if (eVar != null) {
            return eVar;
        }
        if (!this.j.isEmpty()) {
            return this.j.getLast();
        }
        return this.v;
    }

    @RequiresApi(29)
    @SuppressLint({"WrongConstant"})
    public final int C(AudioFormat audioFormat, AudioAttributes audioAttributes) {
        int i = e0.a;
        if (i >= 31) {
            return AudioManager.getPlaybackOffloadSupport(audioFormat, audioAttributes);
        }
        if (!AudioManager.isOffloadedPlaybackSupported(audioFormat, audioAttributes)) {
            return 0;
        }
        return (i != 30 || !e0.d.startsWith("Pixel")) ? 1 : 2;
    }

    public boolean D() {
        return B().f2894b;
    }

    public final long E() {
        c cVar = this.r;
        if (cVar.c == 0) {
            return this.B / cVar.d;
        }
        return this.C;
    }

    public final void F() throws AudioSink.InitializationException {
        this.h.block();
        try {
            c cVar = this.r;
            Objects.requireNonNull(cVar);
            AudioTrack a2 = cVar.a(this.W, this.t, this.U);
            this.f2888s = a2;
            if (H(a2)) {
                AudioTrack audioTrack = this.f2888s;
                if (this.m == null) {
                    this.m = new h();
                }
                h hVar = this.m;
                final Handler handler = hVar.a;
                Objects.requireNonNull(handler);
                audioTrack.registerStreamEventCallback(new Executor() { // from class: b.i.a.c.t2.k
                    @Override // java.util.concurrent.Executor
                    public final void execute(Runnable runnable) {
                        handler.post(runnable);
                    }
                }, hVar.f2896b);
                if (this.l != 3) {
                    AudioTrack audioTrack2 = this.f2888s;
                    j1 j1Var = this.r.a;
                    audioTrack2.setOffloadDelayPadding(j1Var.M, j1Var.N);
                }
            }
            this.U = this.f2888s.getAudioSessionId();
            t tVar = this.i;
            AudioTrack audioTrack3 = this.f2888s;
            c cVar2 = this.r;
            tVar.e(audioTrack3, cVar2.c == 2, cVar2.g, cVar2.d, cVar2.h);
            N();
            int i = this.V.a;
            if (i != 0) {
                this.f2888s.attachAuxEffect(i);
                this.f2888s.setAuxEffectSendLevel(this.V.f1130b);
            }
            this.F = true;
        } catch (AudioSink.InitializationException e2) {
            if (this.r.f()) {
                this.Y = true;
            }
            AudioSink.a aVar = this.p;
            if (aVar != null) {
                ((z.b) aVar).a(e2);
            }
            throw e2;
        }
    }

    public final boolean G() {
        return this.f2888s != null;
    }

    public final void I() {
        if (!this.R) {
            this.R = true;
            t tVar = this.i;
            long E = E();
            tVar.f1129z = tVar.b();
            tVar.f1127x = SystemClock.elapsedRealtime() * 1000;
            tVar.A = E;
            this.f2888s.stop();
            this.f2890y = 0;
        }
    }

    public final void J(long j) throws AudioSink.WriteException {
        ByteBuffer byteBuffer;
        int length = this.I.length;
        int i = length;
        while (i >= 0) {
            if (i > 0) {
                byteBuffer = this.J[i - 1];
            } else {
                byteBuffer = this.K;
                if (byteBuffer == null) {
                    byteBuffer = AudioProcessor.a;
                }
            }
            if (i == length) {
                Q(byteBuffer, j);
            } else {
                AudioProcessor audioProcessor = this.I[i];
                if (i > this.P) {
                    audioProcessor.c(byteBuffer);
                }
                ByteBuffer output = audioProcessor.getOutput();
                this.J[i] = output;
                if (output.hasRemaining()) {
                    i++;
                }
            }
            if (!byteBuffer.hasRemaining()) {
                i--;
            } else {
                return;
            }
        }
    }

    public final void K() {
        this.f2891z = 0L;
        this.A = 0L;
        this.B = 0L;
        this.C = 0L;
        this.Z = false;
        this.D = 0;
        this.v = new e(z(), D(), 0L, 0L, null);
        this.G = 0L;
        this.u = null;
        this.j.clear();
        this.K = null;
        this.L = 0;
        this.M = null;
        this.R = false;
        this.Q = false;
        this.P = -1;
        this.f2889x = null;
        this.f2890y = 0;
        this.e.o = 0L;
        h();
    }

    public final void L(x1 x1Var, boolean z2) {
        e B = B();
        if (!x1Var.equals(B.a) || z2 != B.f2894b) {
            e eVar = new e(x1Var, z2, -9223372036854775807L, -9223372036854775807L, null);
            if (G()) {
                this.u = eVar;
            } else {
                this.v = eVar;
            }
        }
    }

    @RequiresApi(23)
    public final void M(x1 x1Var) {
        if (G()) {
            try {
                this.f2888s.setPlaybackParams(new PlaybackParams().allowDefaults().setSpeed(x1Var.k).setPitch(x1Var.l).setAudioFallbackMode(2));
            } catch (IllegalArgumentException e2) {
                q.c("DefaultAudioSink", "Failed to set playback params", e2);
            }
            x1Var = new x1(this.f2888s.getPlaybackParams().getSpeed(), this.f2888s.getPlaybackParams().getPitch());
            t tVar = this.i;
            tVar.j = x1Var.k;
            s sVar = tVar.f;
            if (sVar != null) {
                sVar.a();
            }
        }
        this.w = x1Var;
    }

    public final void N() {
        if (G()) {
            if (e0.a >= 21) {
                this.f2888s.setVolume(this.H);
                return;
            }
            AudioTrack audioTrack = this.f2888s;
            float f2 = this.H;
            audioTrack.setStereoVolume(f2, f2);
        }
    }

    public final boolean O() {
        if (this.W || !"audio/raw".equals(this.r.a.w)) {
            return false;
        }
        return !(this.c && e0.y(this.r.a.L));
    }

    public final boolean P(j1 j1Var, o oVar) {
        int n;
        int C;
        if (e0.a < 29 || this.l == 0) {
            return false;
        }
        String str = j1Var.w;
        Objects.requireNonNull(str);
        int b2 = b.i.a.c.f3.t.b(str, j1Var.t);
        if (b2 == 0 || (n = e0.n(j1Var.J)) == 0 || (C = C(y(j1Var.K, n, b2), oVar.a())) == 0) {
            return false;
        }
        if (C == 1) {
            return !(j1Var.M != 0 || j1Var.N != 0) || !(this.l == 1);
        } else if (C == 2) {
            return true;
        } else {
            throw new IllegalStateException();
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:46:0x00e2, code lost:
        if (r15 < r14) goto L47;
     */
    /* JADX WARN: Removed duplicated region for block: B:55:0x0103  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0139  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void Q(java.nio.ByteBuffer r13, long r14) throws com.google.android.exoplayer2.audio.AudioSink.WriteException {
        /*
            Method dump skipped, instructions count: 418
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.DefaultAudioSink.Q(java.nio.ByteBuffer, long):void");
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public boolean a(j1 j1Var) {
        return u(j1Var) != 0;
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public boolean b() {
        return !G() || (this.Q && !k());
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public x1 c() {
        if (this.k) {
            return this.w;
        }
        return z();
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void d() {
        boolean z2 = false;
        this.S = false;
        if (G()) {
            t tVar = this.i;
            tVar.l = 0L;
            tVar.w = 0;
            tVar.v = 0;
            tVar.m = 0L;
            tVar.C = 0L;
            tVar.F = 0L;
            tVar.k = false;
            if (tVar.f1127x == -9223372036854775807L) {
                s sVar = tVar.f;
                Objects.requireNonNull(sVar);
                sVar.a();
                z2 = true;
            }
            if (z2) {
                this.f2888s.pause();
            }
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void e() {
        this.S = true;
        if (G()) {
            s sVar = this.i.f;
            Objects.requireNonNull(sVar);
            sVar.a();
            this.f2888s.play();
        }
    }

    public final void f(long j) {
        x1 x1Var;
        final r.a aVar;
        Handler handler;
        if (O()) {
            x1Var = this.f2887b.a(z());
        } else {
            x1Var = x1.j;
        }
        x1 x1Var2 = x1Var;
        final boolean d2 = O() ? this.f2887b.d(D()) : false;
        this.j.add(new e(x1Var2, d2, Math.max(0L, j), this.r.c(E()), null));
        AudioProcessor[] audioProcessorArr = this.r.i;
        ArrayList arrayList = new ArrayList();
        for (AudioProcessor audioProcessor : audioProcessorArr) {
            if (audioProcessor.a()) {
                arrayList.add(audioProcessor);
            } else {
                audioProcessor.flush();
            }
        }
        int size = arrayList.size();
        this.I = (AudioProcessor[]) arrayList.toArray(new AudioProcessor[size]);
        this.J = new ByteBuffer[size];
        h();
        AudioSink.a aVar2 = this.p;
        if (!(aVar2 == null || (handler = (aVar = z.this.O0).a) == null)) {
            handler.post(new Runnable() { // from class: b.i.a.c.t2.a
                @Override // java.lang.Runnable
                public final void run() {
                    r.a aVar3 = r.a.this;
                    boolean z2 = d2;
                    r rVar = aVar3.f1122b;
                    int i = e0.a;
                    rVar.d(z2);
                }
            });
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void flush() {
        if (G()) {
            K();
            AudioTrack audioTrack = this.i.c;
            Objects.requireNonNull(audioTrack);
            if (audioTrack.getPlayState() == 3) {
                this.f2888s.pause();
            }
            if (H(this.f2888s)) {
                h hVar = this.m;
                Objects.requireNonNull(hVar);
                this.f2888s.unregisterStreamEventCallback(hVar.f2896b);
                hVar.a.removeCallbacksAndMessages(null);
            }
            AudioTrack audioTrack2 = this.f2888s;
            this.f2888s = null;
            if (e0.a < 21 && !this.T) {
                this.U = 0;
            }
            c cVar = this.q;
            if (cVar != null) {
                this.r = cVar;
                this.q = null;
            }
            this.i.d();
            this.h.close();
            new a("ExoPlayer:AudioTrackReleaseThread", audioTrack2).start();
        }
        this.o.a = null;
        this.n.a = null;
    }

    /* JADX WARN: Removed duplicated region for block: B:16:0x002f  */
    /* JADX WARN: Removed duplicated region for block: B:9:0x0018  */
    /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:15:0x0029 -> B:5:0x0009). Please submit an issue!!! */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean g() throws com.google.android.exoplayer2.audio.AudioSink.WriteException {
        /*
            r9 = this;
            int r0 = r9.P
            r1 = -1
            r2 = 1
            r3 = 0
            if (r0 != r1) goto Lb
            r9.P = r3
        L9:
            r0 = 1
            goto Lc
        Lb:
            r0 = 0
        Lc:
            int r4 = r9.P
            com.google.android.exoplayer2.audio.AudioProcessor[] r5 = r9.I
            int r6 = r5.length
            r7 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            if (r4 >= r6) goto L2f
            r4 = r5[r4]
            if (r0 == 0) goto L1f
            r4.e()
        L1f:
            r9.J(r7)
            boolean r0 = r4.b()
            if (r0 != 0) goto L29
            return r3
        L29:
            int r0 = r9.P
            int r0 = r0 + r2
            r9.P = r0
            goto L9
        L2f:
            java.nio.ByteBuffer r0 = r9.M
            if (r0 == 0) goto L3b
            r9.Q(r0, r7)
            java.nio.ByteBuffer r0 = r9.M
            if (r0 == 0) goto L3b
            return r3
        L3b:
            r9.P = r1
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.DefaultAudioSink.g():boolean");
    }

    public final void h() {
        int i = 0;
        while (true) {
            AudioProcessor[] audioProcessorArr = this.I;
            if (i < audioProcessorArr.length) {
                AudioProcessor audioProcessor = audioProcessorArr[i];
                audioProcessor.flush();
                this.J[i] = audioProcessor.getOutput();
                i++;
            } else {
                return;
            }
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void i(x1 x1Var) {
        x1 x1Var2 = new x1(e0.g(x1Var.k, 0.1f, 8.0f), e0.g(x1Var.l, 0.1f, 8.0f));
        if (!this.k || e0.a < 23) {
            L(x1Var2, D());
        } else {
            M(x1Var2);
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void j() throws AudioSink.WriteException {
        if (!this.Q && G() && g()) {
            I();
            this.Q = true;
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public boolean k() {
        return G() && this.i.c(E());
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void l(int i) {
        if (this.U != i) {
            this.U = i;
            this.T = i != 0;
            flush();
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:58:0x0116  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x01ab A[Catch: Exception -> 0x01b5, TRY_LEAVE, TryCatch #0 {Exception -> 0x01b5, blocks: (B:80:0x0181, B:82:0x01ab), top: B:135:0x0181 }] */
    @Override // com.google.android.exoplayer2.audio.AudioSink
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public long m(boolean r27) {
        /*
            Method dump skipped, instructions count: 762
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.DefaultAudioSink.m(boolean):long");
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void n() {
        if (this.W) {
            this.W = false;
            flush();
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void o(o oVar) {
        if (!this.t.equals(oVar)) {
            this.t = oVar;
            if (!this.W) {
                flush();
            }
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void p() {
        this.E = true;
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void q(float f2) {
        if (this.H != f2) {
            this.H = f2;
            N();
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void r() {
        b.c.a.a0.d.D(e0.a >= 21);
        b.c.a.a0.d.D(this.T);
        if (!this.W) {
            this.W = true;
            flush();
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void reset() {
        flush();
        for (AudioProcessor audioProcessor : this.f) {
            audioProcessor.reset();
        }
        for (AudioProcessor audioProcessor2 : this.g) {
            audioProcessor2.reset();
        }
        this.S = false;
        this.Y = false;
    }

    /* JADX WARN: Code restructure failed: missing block: B:60:0x00ea, code lost:
        if (r5.b() == 0) goto L61;
     */
    /* JADX WARN: Removed duplicated region for block: B:69:0x010c A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:70:0x010d  */
    @Override // com.google.android.exoplayer2.audio.AudioSink
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public boolean s(java.nio.ByteBuffer r19, long r20, int r22) throws com.google.android.exoplayer2.audio.AudioSink.InitializationException, com.google.android.exoplayer2.audio.AudioSink.WriteException {
        /*
            Method dump skipped, instructions count: 914
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.audio.DefaultAudioSink.s(java.nio.ByteBuffer, long, int):boolean");
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void t(AudioSink.a aVar) {
        this.p = aVar;
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public int u(j1 j1Var) {
        boolean z2 = true;
        if ("audio/raw".equals(j1Var.w)) {
            if (!e0.z(j1Var.L)) {
                b.d.b.a.a.d0(33, "Invalid PCM encoding: ", j1Var.L, "DefaultAudioSink");
                return 0;
            }
            int i = j1Var.L;
            return (i == 2 || (this.c && i == 4)) ? 2 : 1;
        } else if (!this.Y && P(j1Var, this.t)) {
            return 2;
        } else {
            if (A(j1Var, this.a) == null) {
                z2 = false;
            }
            return z2 ? 2 : 0;
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void v(j1 j1Var, int i, @Nullable int[] iArr) throws AudioSink.ConfigurationException {
        AudioProcessor[] audioProcessorArr;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        AudioProcessor[] audioProcessorArr2;
        int[] iArr2;
        int i10 = 1;
        if ("audio/raw".equals(j1Var.w)) {
            b.c.a.a0.d.j(e0.z(j1Var.L));
            i7 = e0.s(j1Var.L, j1Var.J);
            int i11 = j1Var.L;
            if (!this.c || !e0.y(i11)) {
                i10 = 0;
            }
            if (i10 != 0) {
                audioProcessorArr2 = this.g;
            } else {
                audioProcessorArr2 = this.f;
            }
            f0 f0Var = this.e;
            int i12 = j1Var.M;
            int i13 = j1Var.N;
            f0Var.i = i12;
            f0Var.j = i13;
            if (e0.a < 21 && j1Var.J == 8 && iArr == null) {
                iArr2 = new int[6];
                for (int i14 = 0; i14 < 6; i14++) {
                    iArr2[i14] = i14;
                }
            } else {
                iArr2 = iArr;
            }
            this.d.i = iArr2;
            AudioProcessor.a aVar = new AudioProcessor.a(j1Var.K, j1Var.J, j1Var.L);
            for (AudioProcessor audioProcessor : audioProcessorArr2) {
                try {
                    AudioProcessor.a d2 = audioProcessor.d(aVar);
                    if (audioProcessor.a()) {
                        aVar = d2;
                    }
                } catch (AudioProcessor.UnhandledAudioFormatException e2) {
                    throw new AudioSink.ConfigurationException(e2, j1Var);
                }
            }
            int i15 = aVar.d;
            i4 = aVar.f2886b;
            i3 = e0.n(aVar.c);
            i5 = e0.s(i15, aVar.c);
            audioProcessorArr = audioProcessorArr2;
            i2 = i15;
            i6 = 0;
        } else {
            audioProcessorArr = new AudioProcessor[0];
            int i16 = j1Var.K;
            if (P(j1Var, this.t)) {
                String str = j1Var.w;
                Objects.requireNonNull(str);
                i9 = b.i.a.c.f3.t.b(str, j1Var.t);
                i8 = e0.n(j1Var.J);
            } else {
                Pair<Integer, Integer> A = A(j1Var, this.a);
                if (A != null) {
                    i9 = ((Integer) A.first).intValue();
                    i8 = ((Integer) A.second).intValue();
                    i10 = 2;
                } else {
                    String valueOf = String.valueOf(j1Var);
                    throw new AudioSink.ConfigurationException(b.d.b.a.a.i(valueOf.length() + 37, "Unable to configure passthrough for: ", valueOf), j1Var);
                }
            }
            i2 = i9;
            i3 = i8;
            i7 = -1;
            i5 = -1;
            i4 = i16;
            i6 = i10;
        }
        if (i2 == 0) {
            String valueOf2 = String.valueOf(j1Var);
            StringBuilder sb = new StringBuilder(valueOf2.length() + 48);
            sb.append("Invalid output encoding (mode=");
            sb.append(i6);
            sb.append(") for: ");
            sb.append(valueOf2);
            throw new AudioSink.ConfigurationException(sb.toString(), j1Var);
        } else if (i3 != 0) {
            this.Y = false;
            c cVar = new c(j1Var, i7, i6, i5, i4, i3, i2, i, this.k, audioProcessorArr);
            if (G()) {
                this.q = cVar;
            } else {
                this.r = cVar;
            }
        } else {
            String valueOf3 = String.valueOf(j1Var);
            StringBuilder sb2 = new StringBuilder(valueOf3.length() + 54);
            sb2.append("Invalid output channel config (mode=");
            sb2.append(i6);
            sb2.append(") for: ");
            sb2.append(valueOf3);
            throw new AudioSink.ConfigurationException(sb2.toString(), j1Var);
        }
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void w(boolean z2) {
        L(z(), z2);
    }

    @Override // com.google.android.exoplayer2.audio.AudioSink
    public void x(u uVar) {
        if (!this.V.equals(uVar)) {
            int i = uVar.a;
            float f2 = uVar.f1130b;
            AudioTrack audioTrack = this.f2888s;
            if (audioTrack != null) {
                if (this.V.a != i) {
                    audioTrack.attachAuxEffect(i);
                }
                if (i != 0) {
                    this.f2888s.setAuxEffectSendLevel(f2);
                }
            }
            this.V = uVar;
        }
    }

    public final x1 z() {
        return B().a;
    }
}
