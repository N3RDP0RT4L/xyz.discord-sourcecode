package com.google.android.exoplayer2.mediacodec;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaCryptoException;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.c.a3.i0;
import b.i.a.c.f3.c0;
import b.i.a.c.f3.e0;
import b.i.a.c.j1;
import b.i.a.c.k1;
import b.i.a.c.t2.a0;
import b.i.a.c.v0;
import b.i.a.c.v2.b;
import b.i.a.c.v2.c;
import b.i.a.c.v2.e;
import b.i.a.c.v2.g;
import b.i.a.c.w2.b0;
import b.i.a.c.y2.p;
import b.i.a.c.y2.q;
import b.i.a.c.y2.t;
import b.i.a.c.y2.u;
import b.i.a.c.y2.v;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmSession;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/* loaded from: classes3.dex */
public abstract class MediaCodecRenderer extends v0 {
    public static final byte[] u = {0, 0, 1, 103, 66, -64, 11, -38, 37, -112, 0, 0, 1, 104, -50, 15, 19, 32, 0, 0, 1, 101, -120, -124, 13, -50, 113, 24, -96, 0, 47, -65, 28, 49, -61, 39, 93, 120};
    public boolean A0;
    public boolean B0;
    public final p C;
    public boolean E0;
    public boolean F0;
    public boolean G0;
    public boolean H0;
    @Nullable
    public ExoPlaybackException I0;
    @Nullable
    public j1 J;
    public e J0;
    @Nullable
    public j1 K;
    @Nullable
    public DrmSession L;
    @Nullable
    public DrmSession M;
    public int M0;
    @Nullable
    public MediaCrypto N;
    public boolean O;
    @Nullable
    public t S;
    @Nullable
    public j1 T;
    @Nullable
    public MediaFormat U;
    public boolean V;
    @Nullable
    public ArrayDeque<u> X;
    @Nullable
    public DecoderInitializationException Y;
    @Nullable
    public u Z;

    /* renamed from: b0  reason: collision with root package name */
    public boolean f2907b0;

    /* renamed from: c0  reason: collision with root package name */
    public boolean f2908c0;

    /* renamed from: d0  reason: collision with root package name */
    public boolean f2909d0;

    /* renamed from: e0  reason: collision with root package name */
    public boolean f2910e0;

    /* renamed from: f0  reason: collision with root package name */
    public boolean f2911f0;

    /* renamed from: g0  reason: collision with root package name */
    public boolean f2912g0;

    /* renamed from: h0  reason: collision with root package name */
    public boolean f2913h0;

    /* renamed from: i0  reason: collision with root package name */
    public boolean f2914i0;

    /* renamed from: j0  reason: collision with root package name */
    public boolean f2915j0;
    public boolean k0;
    @Nullable
    public q l0;
    @Nullable
    public ByteBuffer p0;
    public boolean q0;
    public boolean r0;
    public boolean s0;
    public boolean t0;
    public boolean u0;
    public final t.b v;
    public boolean v0;
    public final v w;

    /* renamed from: x  reason: collision with root package name */
    public final boolean f2916x;

    /* renamed from: y  reason: collision with root package name */
    public final float f2917y;
    public boolean z0;

    /* renamed from: z  reason: collision with root package name */
    public final DecoderInputBuffer f2918z = new DecoderInputBuffer(0);
    public final DecoderInputBuffer A = new DecoderInputBuffer(0);
    public final DecoderInputBuffer B = new DecoderInputBuffer(2);
    public final c0<j1> D = new c0<>();
    public final ArrayList<Long> E = new ArrayList<>();
    public final MediaCodec.BufferInfo F = new MediaCodec.BufferInfo();
    public float Q = 1.0f;
    public float R = 1.0f;
    public long P = -9223372036854775807L;
    public final long[] G = new long[10];
    public final long[] H = new long[10];
    public final long[] I = new long[10];
    public long K0 = -9223372036854775807L;
    public long L0 = -9223372036854775807L;
    public float W = -1.0f;

    /* renamed from: a0  reason: collision with root package name */
    public int f2906a0 = 0;
    public int w0 = 0;
    public int n0 = -1;
    public int o0 = -1;
    public long m0 = -9223372036854775807L;
    public long C0 = -9223372036854775807L;
    public long D0 = -9223372036854775807L;
    public int x0 = 0;
    public int y0 = 0;

    public MediaCodecRenderer(int i, t.b bVar, v vVar, boolean z2, float f) {
        super(i);
        this.v = bVar;
        Objects.requireNonNull(vVar);
        this.w = vVar;
        this.f2916x = z2;
        this.f2917y = f;
        p pVar = new p();
        this.C = pVar;
        pVar.r(0);
        pVar.l.order(ByteOrder.nativeOrder());
    }

    @RequiresApi(23)
    public final void A0() throws ExoPlaybackException {
        try {
            this.N.setMediaDrmSession(X(this.M).c);
            t0(this.M);
            this.x0 = 0;
            this.y0 = 0;
        } catch (MediaCryptoException e) {
            throw z(e, this.J, false, 6006);
        }
    }

    @Override // b.i.a.c.v0
    public void B() {
        this.J = null;
        this.K0 = -9223372036854775807L;
        this.L0 = -9223372036854775807L;
        this.M0 = 0;
        S();
    }

    public final void B0(long j) throws ExoPlaybackException {
        boolean z2;
        j1 f;
        j1 e = this.D.e(j);
        if (e == null && this.V) {
            c0<j1> c0Var = this.D;
            synchronized (c0Var) {
                f = c0Var.d == 0 ? null : c0Var.f();
            }
            e = f;
        }
        if (e != null) {
            this.K = e;
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2 || (this.V && this.K != null)) {
            h0(this.K, this.U);
            this.V = false;
        }
    }

    @Override // b.i.a.c.v0
    public void D(long j, boolean z2) throws ExoPlaybackException {
        int i;
        this.E0 = false;
        this.F0 = false;
        this.H0 = false;
        if (this.s0) {
            this.C.p();
            this.B.p();
            this.t0 = false;
        } else if (S()) {
            b0();
        }
        c0<j1> c0Var = this.D;
        synchronized (c0Var) {
            i = c0Var.d;
        }
        if (i > 0) {
            this.G0 = true;
        }
        this.D.b();
        int i2 = this.M0;
        if (i2 != 0) {
            this.L0 = this.H[i2 - 1];
            this.K0 = this.G[i2 - 1];
            this.M0 = 0;
        }
    }

    @Override // b.i.a.c.v0
    public void H(j1[] j1VarArr, long j, long j2) throws ExoPlaybackException {
        boolean z2 = true;
        if (this.L0 == -9223372036854775807L) {
            if (this.K0 != -9223372036854775807L) {
                z2 = false;
            }
            d.D(z2);
            this.K0 = j;
            this.L0 = j2;
            return;
        }
        int i = this.M0;
        long[] jArr = this.H;
        if (i == jArr.length) {
            long j3 = jArr[i - 1];
            StringBuilder sb = new StringBuilder(65);
            sb.append("Too many stream changes, so dropping offset: ");
            sb.append(j3);
            Log.w("MediaCodecRenderer", sb.toString());
        } else {
            this.M0 = i + 1;
        }
        long[] jArr2 = this.G;
        int i2 = this.M0;
        jArr2[i2 - 1] = j;
        this.H[i2 - 1] = j2;
        this.I[i2 - 1] = this.C0;
    }

    public final boolean J(long j, long j2) throws ExoPlaybackException {
        d.D(!this.F0);
        if (this.C.v()) {
            p pVar = this.C;
            if (!m0(j, j2, null, pVar.l, this.o0, 0, pVar.f1307s, pVar.n, pVar.m(), this.C.n(), this.K)) {
                return false;
            }
            i0(this.C.r);
            this.C.p();
        }
        if (this.E0) {
            this.F0 = true;
            return false;
        }
        if (this.t0) {
            d.D(this.C.u(this.B));
            this.t0 = false;
        }
        if (this.u0) {
            if (this.C.v()) {
                return true;
            }
            M();
            this.u0 = false;
            b0();
            if (!this.s0) {
                return false;
            }
        }
        d.D(!this.E0);
        k1 A = A();
        this.B.p();
        while (true) {
            this.B.p();
            int I = I(A, this.B, 0);
            if (I != -5) {
                if (I == -4) {
                    if (!this.B.n()) {
                        if (this.G0) {
                            j1 j1Var = this.J;
                            Objects.requireNonNull(j1Var);
                            this.K = j1Var;
                            h0(j1Var, null);
                            this.G0 = false;
                        }
                        this.B.s();
                        if (!this.C.u(this.B)) {
                            this.t0 = true;
                            break;
                        }
                    } else {
                        this.E0 = true;
                        break;
                    }
                } else if (I != -3) {
                    throw new IllegalStateException();
                }
            } else {
                g0(A);
                break;
            }
        }
        if (this.C.v()) {
            this.C.s();
        }
        return this.C.v() || this.E0 || this.u0;
    }

    public abstract g K(u uVar, j1 j1Var, j1 j1Var2);

    public MediaCodecDecoderException L(Throwable th, @Nullable u uVar) {
        return new MediaCodecDecoderException(th, uVar);
    }

    public final void M() {
        this.u0 = false;
        this.C.p();
        this.B.p();
        this.t0 = false;
        this.s0 = false;
    }

    public final void N() throws ExoPlaybackException {
        if (this.z0) {
            this.x0 = 1;
            this.y0 = 3;
            return;
        }
        o0();
        b0();
    }

    @TargetApi(23)
    public final boolean O() throws ExoPlaybackException {
        if (this.z0) {
            this.x0 = 1;
            if (this.f2908c0 || this.f2910e0) {
                this.y0 = 3;
                return false;
            }
            this.y0 = 2;
        } else {
            A0();
        }
        return true;
    }

    public final boolean P(long j, long j2) throws ExoPlaybackException {
        boolean z2;
        boolean z3;
        boolean z4;
        int i;
        boolean z5;
        if (!(this.o0 >= 0)) {
            if (!this.f2911f0 || !this.A0) {
                i = this.S.e(this.F);
            } else {
                try {
                    i = this.S.e(this.F);
                } catch (IllegalStateException unused) {
                    l0();
                    if (this.F0) {
                        o0();
                    }
                    return false;
                }
            }
            if (i < 0) {
                if (i == -2) {
                    this.B0 = true;
                    MediaFormat outputFormat = this.S.getOutputFormat();
                    if (this.f2906a0 != 0 && outputFormat.getInteger("width") == 32 && outputFormat.getInteger("height") == 32) {
                        this.f2915j0 = true;
                    } else {
                        if (this.f2913h0) {
                            outputFormat.setInteger("channel-count", 1);
                        }
                        this.U = outputFormat;
                        this.V = true;
                    }
                    return true;
                }
                if (this.k0 && (this.E0 || this.x0 == 2)) {
                    l0();
                }
                return false;
            } else if (this.f2915j0) {
                this.f2915j0 = false;
                this.S.releaseOutputBuffer(i, false);
                return true;
            } else {
                MediaCodec.BufferInfo bufferInfo = this.F;
                if (bufferInfo.size != 0 || (bufferInfo.flags & 4) == 0) {
                    this.o0 = i;
                    ByteBuffer j3 = this.S.j(i);
                    this.p0 = j3;
                    if (j3 != null) {
                        j3.position(this.F.offset);
                        ByteBuffer byteBuffer = this.p0;
                        MediaCodec.BufferInfo bufferInfo2 = this.F;
                        byteBuffer.limit(bufferInfo2.offset + bufferInfo2.size);
                    }
                    if (this.f2912g0) {
                        MediaCodec.BufferInfo bufferInfo3 = this.F;
                        if (bufferInfo3.presentationTimeUs == 0 && (bufferInfo3.flags & 4) != 0) {
                            long j4 = this.C0;
                            if (j4 != -9223372036854775807L) {
                                bufferInfo3.presentationTimeUs = j4;
                            }
                        }
                    }
                    long j5 = this.F.presentationTimeUs;
                    int size = this.E.size();
                    int i2 = 0;
                    while (true) {
                        if (i2 >= size) {
                            z5 = false;
                            break;
                        } else if (this.E.get(i2).longValue() == j5) {
                            this.E.remove(i2);
                            z5 = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    this.q0 = z5;
                    long j6 = this.D0;
                    long j7 = this.F.presentationTimeUs;
                    this.r0 = j6 == j7;
                    B0(j7);
                } else {
                    l0();
                    return false;
                }
            }
        }
        if (!this.f2911f0 || !this.A0) {
            z3 = true;
            z2 = false;
            t tVar = this.S;
            ByteBuffer byteBuffer2 = this.p0;
            int i3 = this.o0;
            MediaCodec.BufferInfo bufferInfo4 = this.F;
            z4 = m0(j, j2, tVar, byteBuffer2, i3, bufferInfo4.flags, 1, bufferInfo4.presentationTimeUs, this.q0, this.r0, this.K);
        } else {
            try {
                t tVar2 = this.S;
                ByteBuffer byteBuffer3 = this.p0;
                int i4 = this.o0;
                MediaCodec.BufferInfo bufferInfo5 = this.F;
                z2 = false;
                z3 = true;
                try {
                    z4 = m0(j, j2, tVar2, byteBuffer3, i4, bufferInfo5.flags, 1, bufferInfo5.presentationTimeUs, this.q0, this.r0, this.K);
                } catch (IllegalStateException unused2) {
                    l0();
                    if (this.F0) {
                        o0();
                    }
                    return z2;
                }
            } catch (IllegalStateException unused3) {
                z2 = false;
            }
        }
        if (z4) {
            i0(this.F.presentationTimeUs);
            boolean z6 = (this.F.flags & 4) != 0;
            this.o0 = -1;
            this.p0 = null;
            if (!z6) {
                return z3;
            }
            l0();
        }
        return z2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v3, types: [int] */
    public final boolean Q() throws ExoPlaybackException {
        t tVar = this.S;
        boolean z2 = 0;
        if (tVar == null || this.x0 == 2 || this.E0) {
            return false;
        }
        if (this.n0 < 0) {
            int d = tVar.d();
            this.n0 = d;
            if (d < 0) {
                return false;
            }
            this.A.l = this.S.h(d);
            this.A.p();
        }
        if (this.x0 == 1) {
            if (!this.k0) {
                this.A0 = true;
                this.S.queueInputBuffer(this.n0, 0, 0, 0L, 4);
                s0();
            }
            this.x0 = 2;
            return false;
        } else if (this.f2914i0) {
            this.f2914i0 = false;
            ByteBuffer byteBuffer = this.A.l;
            byte[] bArr = u;
            byteBuffer.put(bArr);
            this.S.queueInputBuffer(this.n0, 0, bArr.length, 0L, 0);
            s0();
            this.z0 = true;
            return true;
        } else {
            if (this.w0 == 1) {
                for (int i = 0; i < this.T.f1015y.size(); i++) {
                    this.A.l.put(this.T.f1015y.get(i));
                }
                this.w0 = 2;
            }
            int position = this.A.l.position();
            k1 A = A();
            try {
                int I = I(A, this.A, 0);
                if (j()) {
                    this.D0 = this.C0;
                }
                if (I == -3) {
                    return false;
                }
                if (I == -5) {
                    if (this.w0 == 2) {
                        this.A.p();
                        this.w0 = 1;
                    }
                    g0(A);
                    return true;
                } else if (this.A.n()) {
                    if (this.w0 == 2) {
                        this.A.p();
                        this.w0 = 1;
                    }
                    this.E0 = true;
                    if (!this.z0) {
                        l0();
                        return false;
                    }
                    try {
                        if (!this.k0) {
                            this.A0 = true;
                            this.S.queueInputBuffer(this.n0, 0, 0, 0L, 4);
                            s0();
                        }
                        return false;
                    } catch (MediaCodec.CryptoException e) {
                        throw z(e, this.J, false, e0.p(e.getErrorCode()));
                    }
                } else if (this.z0 || this.A.o()) {
                    boolean t = this.A.t();
                    if (t) {
                        c cVar = this.A.k;
                        Objects.requireNonNull(cVar);
                        if (position != 0) {
                            if (cVar.d == null) {
                                int[] iArr = new int[1];
                                cVar.d = iArr;
                                cVar.i.numBytesOfClearData = iArr;
                            }
                            int[] iArr2 = cVar.d;
                            iArr2[0] = iArr2[0] + position;
                        }
                    }
                    if (this.f2907b0 && !t) {
                        ByteBuffer byteBuffer2 = this.A.l;
                        byte[] bArr2 = b.i.a.c.f3.u.a;
                        int position2 = byteBuffer2.position();
                        int i2 = 0;
                        int i3 = 0;
                        while (true) {
                            int i4 = i2 + 1;
                            if (i4 >= position2) {
                                byteBuffer2.clear();
                                break;
                            }
                            int i5 = byteBuffer2.get(i2) & 255;
                            if (i3 == 3) {
                                if (i5 == 1 && (byteBuffer2.get(i4) & 31) == 7) {
                                    ByteBuffer duplicate = byteBuffer2.duplicate();
                                    duplicate.position(i2 - 3);
                                    duplicate.limit(position2);
                                    byteBuffer2.position(0);
                                    byteBuffer2.put(duplicate);
                                    break;
                                }
                            } else if (i5 == 0) {
                                i3++;
                            }
                            if (i5 != 0) {
                                i3 = 0;
                            }
                            i2 = i4;
                        }
                        if (this.A.l.position() == 0) {
                            return true;
                        }
                        this.f2907b0 = false;
                    }
                    DecoderInputBuffer decoderInputBuffer = this.A;
                    long j = decoderInputBuffer.n;
                    q qVar = this.l0;
                    if (qVar != null) {
                        j1 j1Var = this.J;
                        if (qVar.f1308b == 0) {
                            qVar.a = j;
                        }
                        if (!qVar.c) {
                            ByteBuffer byteBuffer3 = decoderInputBuffer.l;
                            Objects.requireNonNull(byteBuffer3);
                            int i6 = 0;
                            for (int i7 = 0; i7 < 4; i7++) {
                                i6 = (i6 << 8) | (byteBuffer3.get(i7) & 255);
                            }
                            int d2 = a0.d(i6);
                            if (d2 == -1) {
                                qVar.c = true;
                                qVar.f1308b = 0L;
                                qVar.a = decoderInputBuffer.n;
                                Log.w("C2Mp3TimestampTracker", "MPEG audio header is invalid.");
                                j = decoderInputBuffer.n;
                            } else {
                                j = qVar.a(j1Var.K);
                                qVar.f1308b += d2;
                            }
                        }
                        long j2 = this.C0;
                        q qVar2 = this.l0;
                        j1 j1Var2 = this.J;
                        Objects.requireNonNull(qVar2);
                        this.C0 = Math.max(j2, qVar2.a(j1Var2.K));
                    }
                    long j3 = j;
                    if (this.A.m()) {
                        this.E.add(Long.valueOf(j3));
                    }
                    if (this.G0) {
                        this.D.a(j3, this.J);
                        this.G0 = false;
                    }
                    this.C0 = Math.max(this.C0, j3);
                    this.A.s();
                    if (this.A.l()) {
                        Z(this.A);
                    }
                    k0(this.A);
                    try {
                        if (t) {
                            this.S.b(this.n0, 0, this.A.k, j3, 0);
                        } else {
                            this.S.queueInputBuffer(this.n0, 0, this.A.l.limit(), j3, 0);
                        }
                        s0();
                        this.z0 = true;
                        this.w0 = 0;
                        e eVar = this.J0;
                        z2 = eVar.c + 1;
                        eVar.c = z2;
                        return true;
                    } catch (MediaCodec.CryptoException e2) {
                        throw z(e2, this.J, z2, e0.p(e2.getErrorCode()));
                    }
                } else {
                    this.A.p();
                    if (this.w0 == 2) {
                        this.w0 = 1;
                    }
                    return true;
                }
            } catch (DecoderInputBuffer.InsufficientCapacityException e3) {
                d0(e3);
                n0(0);
                R();
                return true;
            }
        }
    }

    public final void R() {
        try {
            this.S.flush();
        } finally {
            q0();
        }
    }

    public boolean S() {
        if (this.S == null) {
            return false;
        }
        if (this.y0 == 3 || this.f2908c0 || ((this.f2909d0 && !this.B0) || (this.f2910e0 && this.A0))) {
            o0();
            return true;
        }
        R();
        return false;
    }

    public final List<u> T(boolean z2) throws MediaCodecUtil.DecoderQueryException {
        List<u> W = W(this.w, this.J, z2);
        if (W.isEmpty() && z2) {
            W = W(this.w, this.J, false);
            if (!W.isEmpty()) {
                String str = this.J.w;
                String valueOf = String.valueOf(W);
                a.o0(a.Q(valueOf.length() + a.b(str, 99), "Drm session requires secure decoder for ", str, ", but no secure decoder available. Trying to proceed with ", valueOf), ".", "MediaCodecRenderer");
            }
        }
        return W;
    }

    public boolean U() {
        return false;
    }

    public abstract float V(float f, j1 j1Var, j1[] j1VarArr);

    public abstract List<u> W(v vVar, j1 j1Var, boolean z2) throws MediaCodecUtil.DecoderQueryException;

    @Nullable
    public final b0 X(DrmSession drmSession) throws ExoPlaybackException {
        b g = drmSession.g();
        if (g == null || (g instanceof b0)) {
            return (b0) g;
        }
        String valueOf = String.valueOf(g);
        throw z(new IllegalArgumentException(a.i(valueOf.length() + 43, "Expecting FrameworkCryptoConfig but found: ", valueOf)), this.J, false, 6001);
    }

    public abstract t.a Y(u uVar, j1 j1Var, @Nullable MediaCrypto mediaCrypto, float f);

    public void Z(DecoderInputBuffer decoderInputBuffer) throws ExoPlaybackException {
    }

    @Override // b.i.a.c.g2
    public final int a(j1 j1Var) throws ExoPlaybackException {
        try {
            return y0(this.w, j1Var);
        } catch (MediaCodecUtil.DecoderQueryException e) {
            throw y(e, j1Var, 4002);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:80:0x014c, code lost:
        if ("stvm8".equals(r4) == false) goto L86;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x015c, code lost:
        if ("OMX.amlogic.avc.decoder.awesome.secure".equals(r1) == false) goto L86;
     */
    /* JADX WARN: Removed duplicated region for block: B:115:0x01c1  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x01d8  */
    /* JADX WARN: Removed duplicated region for block: B:143:0x021b  */
    /* JADX WARN: Removed duplicated region for block: B:149:0x022f  */
    /* JADX WARN: Removed duplicated region for block: B:156:0x0246  */
    /* JADX WARN: Removed duplicated region for block: B:159:0x0252  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00ed A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:68:0x0123  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0132  */
    /* JADX WARN: Removed duplicated region for block: B:77:0x013c  */
    /* JADX WARN: Removed duplicated region for block: B:89:0x0165  */
    /* JADX WARN: Removed duplicated region for block: B:95:0x0174  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void a0(b.i.a.c.y2.u r17, android.media.MediaCrypto r18) throws java.lang.Exception {
        /*
            Method dump skipped, instructions count: 619
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.a0(b.i.a.c.y2.u, android.media.MediaCrypto):void");
    }

    @Override // b.i.a.c.f2
    public boolean b() {
        return this.F0;
    }

    public final void b0() throws ExoPlaybackException {
        j1 j1Var;
        if (this.S == null && !this.s0 && (j1Var = this.J) != null) {
            if (this.M != null || !x0(j1Var)) {
                t0(this.M);
                String str = this.J.w;
                DrmSession drmSession = this.L;
                if (drmSession != null) {
                    if (this.N == null) {
                        b0 X = X(drmSession);
                        if (X != null) {
                            try {
                                MediaCrypto mediaCrypto = new MediaCrypto(X.f1146b, X.c);
                                this.N = mediaCrypto;
                                this.O = !X.d && mediaCrypto.requiresSecureDecoderComponent(str);
                            } catch (MediaCryptoException e) {
                                throw z(e, this.J, false, 6006);
                            }
                        } else if (this.L.f() == null) {
                            return;
                        }
                    }
                    if (b0.a) {
                        int state = this.L.getState();
                        if (state == 1) {
                            DrmSession.DrmSessionException f = this.L.f();
                            Objects.requireNonNull(f);
                            throw z(f, this.J, false, f.errorCode);
                        } else if (state != 4) {
                            return;
                        }
                    }
                }
                try {
                    c0(this.N, this.O);
                } catch (DecoderInitializationException e2) {
                    throw z(e2, this.J, false, 4001);
                }
            } else {
                j1 j1Var2 = this.J;
                M();
                String str2 = j1Var2.w;
                if ("audio/mp4a-latm".equals(str2) || "audio/mpeg".equals(str2) || "audio/opus".equals(str2)) {
                    p pVar = this.C;
                    Objects.requireNonNull(pVar);
                    d.j(true);
                    pVar.t = 32;
                } else {
                    p pVar2 = this.C;
                    Objects.requireNonNull(pVar2);
                    d.j(true);
                    pVar2.t = 1;
                }
                this.s0 = true;
            }
        }
    }

    public final void c0(MediaCrypto mediaCrypto, boolean z2) throws DecoderInitializationException {
        if (this.X == null) {
            try {
                List<u> T = T(z2);
                ArrayDeque<u> arrayDeque = new ArrayDeque<>();
                this.X = arrayDeque;
                if (this.f2916x) {
                    arrayDeque.addAll(T);
                } else if (!T.isEmpty()) {
                    this.X.add(T.get(0));
                }
                this.Y = null;
            } catch (MediaCodecUtil.DecoderQueryException e) {
                throw new DecoderInitializationException(this.J, e, z2, -49998);
            }
        }
        if (!this.X.isEmpty()) {
            while (this.S == null) {
                u peekFirst = this.X.peekFirst();
                if (w0(peekFirst)) {
                    try {
                        a0(peekFirst, mediaCrypto);
                    } catch (Exception e2) {
                        String valueOf = String.valueOf(peekFirst);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 30);
                        sb.append("Failed to initialize decoder: ");
                        sb.append(valueOf);
                        b.i.a.c.f3.q.c("MediaCodecRenderer", sb.toString(), e2);
                        this.X.removeFirst();
                        j1 j1Var = this.J;
                        String str = peekFirst.a;
                        String valueOf2 = String.valueOf(j1Var);
                        DecoderInitializationException decoderInitializationException = new DecoderInitializationException(a.k(valueOf2.length() + a.b(str, 23), "Decoder init failed: ", str, ", ", valueOf2), e2, j1Var.w, z2, peekFirst, (e0.a < 21 || !(e2 instanceof MediaCodec.CodecException)) ? null : ((MediaCodec.CodecException) e2).getDiagnosticInfo(), null);
                        d0(decoderInitializationException);
                        DecoderInitializationException decoderInitializationException2 = this.Y;
                        if (decoderInitializationException2 == null) {
                            this.Y = decoderInitializationException;
                        } else {
                            this.Y = new DecoderInitializationException(decoderInitializationException2.getMessage(), decoderInitializationException2.getCause(), decoderInitializationException2.mimeType, decoderInitializationException2.secureDecoderRequired, decoderInitializationException2.codecInfo, decoderInitializationException2.diagnosticInfo, decoderInitializationException);
                        }
                        if (this.X.isEmpty()) {
                            throw this.Y;
                        }
                    }
                } else {
                    return;
                }
            }
            this.X = null;
            return;
        }
        throw new DecoderInitializationException(this.J, null, z2, -49999);
    }

    @Override // b.i.a.c.f2
    public boolean d() {
        boolean z2;
        if (this.J == null) {
            return false;
        }
        if (j()) {
            z2 = this.f1136s;
        } else {
            i0 i0Var = this.o;
            Objects.requireNonNull(i0Var);
            z2 = i0Var.d();
        }
        if (!z2) {
            if (!(this.o0 >= 0) && (this.m0 == -9223372036854775807L || SystemClock.elapsedRealtime() >= this.m0)) {
                return false;
            }
        }
        return true;
    }

    public abstract void d0(Exception exc);

    public abstract void e0(String str, long j, long j2);

    public abstract void f0(String str);

    /* JADX WARN: Code restructure failed: missing block: B:31:0x006c, code lost:
        if (r5 != false) goto L32;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x00b7, code lost:
        if (O() == false) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x00ea, code lost:
        if (O() == false) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x00fe, code lost:
        if (O() == false) goto L90;
     */
    /* JADX WARN: Code restructure failed: missing block: B:87:0x0116, code lost:
        if (r0 == false) goto L90;
     */
    /* JADX WARN: Removed duplicated region for block: B:34:0x0071  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0080  */
    /* JADX WARN: Removed duplicated region for block: B:92:0x0121  */
    @androidx.annotation.Nullable
    @androidx.annotation.CallSuper
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public b.i.a.c.v2.g g0(b.i.a.c.k1 r12) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 321
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.g0(b.i.a.c.k1):b.i.a.c.v2.g");
    }

    public abstract void h0(j1 j1Var, @Nullable MediaFormat mediaFormat) throws ExoPlaybackException;

    @CallSuper
    public void i0(long j) {
        while (true) {
            int i = this.M0;
            if (i != 0 && j >= this.I[0]) {
                long[] jArr = this.G;
                this.K0 = jArr[0];
                this.L0 = this.H[0];
                int i2 = i - 1;
                this.M0 = i2;
                System.arraycopy(jArr, 1, jArr, 0, i2);
                long[] jArr2 = this.H;
                System.arraycopy(jArr2, 1, jArr2, 0, this.M0);
                long[] jArr3 = this.I;
                System.arraycopy(jArr3, 1, jArr3, 0, this.M0);
                j0();
            } else {
                return;
            }
        }
    }

    public abstract void j0();

    public abstract void k0(DecoderInputBuffer decoderInputBuffer) throws ExoPlaybackException;

    @TargetApi(23)
    public final void l0() throws ExoPlaybackException {
        int i = this.y0;
        if (i == 1) {
            R();
        } else if (i == 2) {
            R();
            A0();
        } else if (i != 3) {
            this.F0 = true;
            p0();
        } else {
            o0();
            b0();
        }
    }

    public abstract boolean m0(long j, long j2, @Nullable t tVar, @Nullable ByteBuffer byteBuffer, int i, int i2, int i3, long j3, boolean z2, boolean z3, j1 j1Var) throws ExoPlaybackException;

    @Override // b.i.a.c.v0, b.i.a.c.f2
    public void n(float f, float f2) throws ExoPlaybackException {
        this.Q = f;
        this.R = f2;
        z0(this.T);
    }

    public final boolean n0(int i) throws ExoPlaybackException {
        k1 A = A();
        this.f2918z.p();
        int I = I(A, this.f2918z, i | 4);
        if (I == -5) {
            g0(A);
            return true;
        } else if (I != -4 || !this.f2918z.n()) {
            return false;
        } else {
            this.E0 = true;
            l0();
            return false;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.exoplayer2.drm.DrmSession, android.media.MediaCrypto] */
    public void o0() {
        try {
            t tVar = this.S;
            if (tVar != null) {
                tVar.release();
                this.J0.f1139b++;
                f0(this.Z.a);
            }
            this.S = null;
            try {
                MediaCrypto mediaCrypto = this.N;
                if (mediaCrypto != null) {
                    mediaCrypto.release();
                }
            } finally {
            }
        } catch (Throwable th) {
            this.S = null;
            try {
                MediaCrypto mediaCrypto2 = this.N;
                if (mediaCrypto2 != null) {
                    mediaCrypto2.release();
                }
                throw th;
            } finally {
            }
        }
    }

    @Override // b.i.a.c.v0, b.i.a.c.g2
    public final int p() {
        return 8;
    }

    public void p0() throws ExoPlaybackException {
    }

    /* JADX WARN: Removed duplicated region for block: B:55:0x00a6  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00cf  */
    @Override // b.i.a.c.f2
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void q(long r6, long r8) throws com.google.android.exoplayer2.ExoPlaybackException {
        /*
            Method dump skipped, instructions count: 212
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.q(long, long):void");
    }

    @CallSuper
    public void q0() {
        s0();
        this.o0 = -1;
        this.p0 = null;
        this.m0 = -9223372036854775807L;
        this.A0 = false;
        this.z0 = false;
        this.f2914i0 = false;
        this.f2915j0 = false;
        this.q0 = false;
        this.r0 = false;
        this.E.clear();
        this.C0 = -9223372036854775807L;
        this.D0 = -9223372036854775807L;
        q qVar = this.l0;
        if (qVar != null) {
            qVar.a = 0L;
            qVar.f1308b = 0L;
            qVar.c = false;
        }
        this.x0 = 0;
        this.y0 = 0;
        this.w0 = this.v0 ? 1 : 0;
    }

    @CallSuper
    public void r0() {
        q0();
        this.I0 = null;
        this.l0 = null;
        this.X = null;
        this.Z = null;
        this.T = null;
        this.U = null;
        this.V = false;
        this.B0 = false;
        this.W = -1.0f;
        this.f2906a0 = 0;
        this.f2907b0 = false;
        this.f2908c0 = false;
        this.f2909d0 = false;
        this.f2910e0 = false;
        this.f2911f0 = false;
        this.f2912g0 = false;
        this.f2913h0 = false;
        this.k0 = false;
        this.v0 = false;
        this.w0 = 0;
        this.O = false;
    }

    public final void s0() {
        this.n0 = -1;
        this.A.l = null;
    }

    public final void t0(@Nullable DrmSession drmSession) {
        DrmSession drmSession2 = this.L;
        if (drmSession2 != drmSession) {
            if (drmSession != null) {
                drmSession.a(null);
            }
            if (drmSession2 != null) {
                drmSession2.b(null);
            }
        }
        this.L = drmSession;
    }

    public final void u0(@Nullable DrmSession drmSession) {
        DrmSession drmSession2 = this.M;
        if (drmSession2 != drmSession) {
            if (drmSession != null) {
                drmSession.a(null);
            }
            if (drmSession2 != null) {
                drmSession2.b(null);
            }
        }
        this.M = drmSession;
    }

    public final boolean v0(long j) {
        return this.P == -9223372036854775807L || SystemClock.elapsedRealtime() - j < this.P;
    }

    public boolean w0(u uVar) {
        return true;
    }

    public boolean x0(j1 j1Var) {
        return false;
    }

    public abstract int y0(v vVar, j1 j1Var) throws MediaCodecUtil.DecoderQueryException;

    public final boolean z0(j1 j1Var) throws ExoPlaybackException {
        if (!(e0.a < 23 || this.S == null || this.y0 == 3 || this.n == 0)) {
            float f = this.R;
            j1[] j1VarArr = this.p;
            Objects.requireNonNull(j1VarArr);
            float V = V(f, j1Var, j1VarArr);
            float f2 = this.W;
            if (f2 == V) {
                return true;
            }
            if (V == -1.0f) {
                N();
                return false;
            } else if (f2 == -1.0f && V <= this.f2917y) {
                return true;
            } else {
                Bundle bundle = new Bundle();
                bundle.putFloat("operating-rate", V);
                this.S.setParameters(bundle);
                this.W = V;
            }
        }
        return true;
    }

    /* loaded from: classes3.dex */
    public static class DecoderInitializationException extends Exception {
        @Nullable
        public final u codecInfo;
        @Nullable
        public final String diagnosticInfo;
        @Nullable
        public final DecoderInitializationException fallbackDecoderInitializationException;
        public final String mimeType;
        public final boolean secureDecoderRequired;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public DecoderInitializationException(b.i.a.c.j1 r12, @androidx.annotation.Nullable java.lang.Throwable r13, boolean r14, int r15) {
            /*
                r11 = this;
                java.lang.String r0 = java.lang.String.valueOf(r12)
                int r1 = r0.length()
                int r1 = r1 + 36
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>(r1)
                java.lang.String r1 = "Decoder init failed: ["
                r2.append(r1)
                r2.append(r15)
                java.lang.String r1 = "], "
                r2.append(r1)
                r2.append(r0)
                java.lang.String r4 = r2.toString()
                java.lang.String r6 = r12.w
                if (r15 >= 0) goto L2a
                java.lang.String r12 = "neg_"
                goto L2c
            L2a:
                java.lang.String r12 = ""
            L2c:
                int r15 = java.lang.Math.abs(r15)
                int r0 = r12.length()
                int r0 = r0 + 71
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>(r0)
                java.lang.String r0 = "com.google.android.exoplayer2.mediacodec.MediaCodecRenderer_"
                r1.append(r0)
                r1.append(r12)
                r1.append(r15)
                java.lang.String r9 = r1.toString()
                r10 = 0
                r8 = 0
                r3 = r11
                r5 = r13
                r7 = r14
                r3.<init>(r4, r5, r6, r7, r8, r9, r10)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException.<init>(b.i.a.c.j1, java.lang.Throwable, boolean, int):void");
        }

        public DecoderInitializationException(String str, @Nullable Throwable th, String str2, boolean z2, @Nullable u uVar, @Nullable String str3, @Nullable DecoderInitializationException decoderInitializationException) {
            super(str, th);
            this.mimeType = str2;
            this.secureDecoderRequired = z2;
            this.codecInfo = uVar;
            this.diagnosticInfo = str3;
            this.fallbackDecoderInitializationException = decoderInitializationException;
        }
    }
}
