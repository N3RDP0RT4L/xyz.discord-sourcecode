package com.google.android.exoplayer2.mediacodec;

import android.annotation.SuppressLint;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import b.i.a.c.f3.e0;
import b.i.a.c.y2.u;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
@SuppressLint({"InlinedApi"})
/* loaded from: classes3.dex */
public final class MediaCodecUtil {
    public static final Pattern a = Pattern.compile("^\\D?(\\d+)$");
    @GuardedBy("MediaCodecUtil.class")

    /* renamed from: b  reason: collision with root package name */
    public static final HashMap<b, List<u>> f2919b = new HashMap<>();
    public static int c = -1;

    /* loaded from: classes3.dex */
    public static class DecoderQueryException extends Exception {
        public DecoderQueryException(Throwable th, a aVar) {
            super("Failed to query underlying media codecs", th);
        }
    }

    /* loaded from: classes3.dex */
    public static final class b {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2920b;
        public final boolean c;

        public b(String str, boolean z2, boolean z3) {
            this.a = str;
            this.f2920b = z2;
            this.c = z3;
        }

        public boolean equals(@Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || obj.getClass() != b.class) {
                return false;
            }
            b bVar = (b) obj;
            return TextUtils.equals(this.a, bVar.a) && this.f2920b == bVar.f2920b && this.c == bVar.c;
        }

        public int hashCode() {
            int i = 1231;
            int m = (b.d.b.a.a.m(this.a, 31, 31) + (this.f2920b ? 1231 : 1237)) * 31;
            if (!this.c) {
                i = 1237;
            }
            return m + i;
        }
    }

    /* loaded from: classes3.dex */
    public interface c {
        MediaCodecInfo a(int i);

        boolean b(String str, String str2, MediaCodecInfo.CodecCapabilities codecCapabilities);

        boolean c(String str, String str2, MediaCodecInfo.CodecCapabilities codecCapabilities);

        int d();

        boolean e();
    }

    /* loaded from: classes3.dex */
    public static final class d implements c {
        public d(a aVar) {
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public MediaCodecInfo a(int i) {
            return MediaCodecList.getCodecInfoAt(i);
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public boolean b(String str, String str2, MediaCodecInfo.CodecCapabilities codecCapabilities) {
            return "secure-playback".equals(str) && "video/avc".equals(str2);
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public boolean c(String str, String str2, MediaCodecInfo.CodecCapabilities codecCapabilities) {
            return false;
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public int d() {
            return MediaCodecList.getCodecCount();
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public boolean e() {
            return false;
        }
    }

    @RequiresApi(21)
    /* loaded from: classes3.dex */
    public static final class e implements c {
        public final int a;
        @Nullable

        /* renamed from: b  reason: collision with root package name */
        public MediaCodecInfo[] f2921b;

        public e(boolean z2, boolean z3) {
            this.a = (z2 || z3) ? 1 : 0;
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public MediaCodecInfo a(int i) {
            if (this.f2921b == null) {
                this.f2921b = new MediaCodecList(this.a).getCodecInfos();
            }
            return this.f2921b[i];
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public boolean b(String str, String str2, MediaCodecInfo.CodecCapabilities codecCapabilities) {
            return codecCapabilities.isFeatureSupported(str);
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public boolean c(String str, String str2, MediaCodecInfo.CodecCapabilities codecCapabilities) {
            return codecCapabilities.isFeatureRequired(str);
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public int d() {
            if (this.f2921b == null) {
                this.f2921b = new MediaCodecList(this.a).getCodecInfos();
            }
            return this.f2921b.length;
        }

        @Override // com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c
        public boolean e() {
            return true;
        }
    }

    /* loaded from: classes3.dex */
    public interface f<T> {
        int a(T t);
    }

    public static void a(String str, List<u> list) {
        if ("audio/raw".equals(str)) {
            if (e0.a < 26 && e0.f964b.equals("R9") && list.size() == 1 && list.get(0).a.equals("OMX.MTK.AUDIO.DECODER.RAW")) {
                list.add(u.i("OMX.google.raw.decoder", "audio/raw", "audio/raw", null, false, true, false, false, false));
            }
            j(list, b.i.a.c.y2.e.a);
        }
        int i = e0.a;
        if (i < 21 && list.size() > 1) {
            String str2 = list.get(0).a;
            if ("OMX.SEC.mp3.dec".equals(str2) || "OMX.SEC.MP3.Decoder".equals(str2) || "OMX.brcm.audio.mp3.decoder".equals(str2)) {
                j(list, b.i.a.c.y2.f.a);
            }
        }
        if (i < 32 && list.size() > 1 && "OMX.qti.audio.decoder.flac".equals(list.get(0).a)) {
            list.add(list.remove(0));
        }
    }

    @Nullable
    public static String b(MediaCodecInfo mediaCodecInfo, String str, String str2) {
        String[] supportedTypes;
        for (String str3 : mediaCodecInfo.getSupportedTypes()) {
            if (str3.equalsIgnoreCase(str2)) {
                return str3;
            }
        }
        if (str2.equals("video/dolby-vision")) {
            if ("OMX.MS.HEVCDV.Decoder".equals(str)) {
                return "video/hevcdv";
            }
            if ("OMX.RTK.video.decoder".equals(str) || "OMX.realtek.video.decoder.tunneled".equals(str)) {
                return "video/dv_hevc";
            }
            return null;
        } else if (str2.equals("audio/alac") && "OMX.lge.alac.decoder".equals(str)) {
            return "audio/x-lg-alac";
        } else {
            if (!str2.equals("audio/flac") || !"OMX.lge.flac.decoder".equals(str)) {
                return null;
            }
            return "audio/x-lg-flac";
        }
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Removed duplicated region for block: B:135:0x0243  */
    /* JADX WARN: Removed duplicated region for block: B:141:0x025e  */
    /* JADX WARN: Removed duplicated region for block: B:272:0x042f A[Catch: NumberFormatException -> 0x043f, TRY_LEAVE, TryCatch #3 {NumberFormatException -> 0x043f, blocks: (B:242:0x03c4, B:244:0x03d8, B:256:0x03f7, B:272:0x042f), top: B:568:0x03c4 }] */
    /* JADX WARN: Removed duplicated region for block: B:412:0x06e0  */
    /* JADX WARN: Removed duplicated region for block: B:417:0x06fb  */
    /* JADX WARN: Removed duplicated region for block: B:576:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:69:0x012f  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x014a  */
    @androidx.annotation.Nullable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static android.util.Pair<java.lang.Integer, java.lang.Integer> c(b.i.a.c.j1 r15) {
        /*
            Method dump skipped, instructions count: 2812
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.mediacodec.MediaCodecUtil.c(b.i.a.c.j1):android.util.Pair");
    }

    @Nullable
    public static u d(String str, boolean z2, boolean z3) throws DecoderQueryException {
        List<u> e2 = e(str, z2, z3);
        if (e2.isEmpty()) {
            return null;
        }
        return e2.get(0);
    }

    public static synchronized List<u> e(String str, boolean z2, boolean z3) throws DecoderQueryException {
        c cVar;
        synchronized (MediaCodecUtil.class) {
            b bVar = new b(str, z2, z3);
            HashMap<b, List<u>> hashMap = f2919b;
            List<u> list = hashMap.get(bVar);
            if (list != null) {
                return list;
            }
            int i = e0.a;
            if (i >= 21) {
                cVar = new e(z2, z3);
            } else {
                cVar = new d(null);
            }
            ArrayList<u> f2 = f(bVar, cVar);
            if (z2 && f2.isEmpty() && 21 <= i && i <= 23) {
                f2 = f(bVar, new d(null));
                if (!f2.isEmpty()) {
                    String str2 = f2.get(0).a;
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 63 + String.valueOf(str2).length());
                    sb.append("MediaCodecList API didn't list secure decoder for: ");
                    sb.append(str);
                    sb.append(". Assuming: ");
                    sb.append(str2);
                    Log.w("MediaCodecUtil", sb.toString());
                }
            }
            a(str, f2);
            List<u> unmodifiableList = Collections.unmodifiableList(f2);
            hashMap.put(bVar, unmodifiableList);
            return unmodifiableList;
        }
    }

    public static ArrayList<u> f(b bVar, c cVar) throws DecoderQueryException {
        boolean z2;
        int i;
        int i2;
        String b2;
        String str;
        String str2;
        Exception e2;
        MediaCodecInfo.CodecCapabilities capabilitiesForType;
        boolean b3;
        boolean c2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        try {
            ArrayList<u> arrayList = new ArrayList<>();
            String str3 = bVar.a;
            int d2 = cVar.d();
            boolean e3 = cVar.e();
            int i3 = 0;
            while (i3 < d2) {
                MediaCodecInfo a2 = cVar.a(i3);
                int i4 = e0.a;
                if (!(i4 >= 29 && a2.isAlias())) {
                    String name = a2.getName();
                    if (g(a2, name, e3, str3) && (b2 = b(a2, name, str3)) != null) {
                        try {
                            capabilitiesForType = a2.getCapabilitiesForType(b2);
                            b3 = cVar.b("tunneled-playback", b2, capabilitiesForType);
                            c2 = cVar.c("tunneled-playback", b2, capabilitiesForType);
                            z3 = bVar.c;
                        } catch (Exception e4) {
                            e2 = e4;
                            str2 = b2;
                            str = name;
                            i = i3;
                            z2 = e3;
                            i2 = d2;
                        }
                        if ((z3 || !c2) && (!z3 || b3)) {
                            boolean b4 = cVar.b("secure-playback", b2, capabilitiesForType);
                            boolean c3 = cVar.c("secure-playback", b2, capabilitiesForType);
                            boolean z7 = bVar.f2920b;
                            if ((z7 || !c3) && (!z7 || b4)) {
                                if (i4 >= 29) {
                                    z4 = a2.isHardwareAccelerated();
                                    z5 = true;
                                } else {
                                    z5 = true;
                                    z4 = !h(a2);
                                }
                                boolean h = h(a2);
                                if (i4 >= 29) {
                                    z6 = a2.isVendor();
                                } else {
                                    String u1 = b.i.a.f.e.o.f.u1(a2.getName());
                                    if (u1.startsWith("omx.google.") || u1.startsWith("c2.android.") || u1.startsWith("c2.google.")) {
                                        z5 = false;
                                    }
                                    z6 = z5;
                                }
                                if ((!e3 || bVar.f2920b != b4) && (e3 || bVar.f2920b)) {
                                    str2 = b2;
                                    str = name;
                                    i = i3;
                                    z2 = e3;
                                    i2 = d2;
                                    if (!z2 && b4) {
                                        arrayList.add(u.i(String.valueOf(str).concat(".secure"), str3, str2, capabilitiesForType, z4, h, z6, false, true));
                                        return arrayList;
                                    }
                                    i3 = i + 1;
                                    d2 = i2;
                                    e3 = z2;
                                } else {
                                    str2 = b2;
                                    str = name;
                                    i = i3;
                                    z2 = e3;
                                    i2 = d2;
                                    try {
                                        arrayList.add(u.i(name, str3, b2, capabilitiesForType, z4, h, z6, false, false));
                                    } catch (Exception e5) {
                                        e2 = e5;
                                        if (e0.a > 23 || arrayList.isEmpty()) {
                                            String str4 = str;
                                            StringBuilder sb = new StringBuilder(String.valueOf(str4).length() + 25 + str2.length());
                                            sb.append("Failed to query codec ");
                                            sb.append(str4);
                                            sb.append(" (");
                                            sb.append(str2);
                                            sb.append(")");
                                            Log.e("MediaCodecUtil", sb.toString());
                                            throw e2;
                                        }
                                        StringBuilder sb2 = new StringBuilder(String.valueOf(str).length() + 46);
                                        sb2.append("Skipping codec ");
                                        sb2.append(str);
                                        sb2.append(" (failed to query capabilities)");
                                        Log.e("MediaCodecUtil", sb2.toString());
                                        i3 = i + 1;
                                        d2 = i2;
                                        e3 = z2;
                                    }
                                    i3 = i + 1;
                                    d2 = i2;
                                    e3 = z2;
                                }
                            }
                        }
                    }
                }
                i = i3;
                z2 = e3;
                i2 = d2;
                i3 = i + 1;
                d2 = i2;
                e3 = z2;
            }
            return arrayList;
        } catch (Exception e6) {
            throw new DecoderQueryException(e6, null);
        }
    }

    public static boolean g(MediaCodecInfo mediaCodecInfo, String str, boolean z2, String str2) {
        if (mediaCodecInfo.isEncoder() || (!z2 && str.endsWith(".secure"))) {
            return false;
        }
        int i = e0.a;
        if (i < 21 && ("CIPAACDecoder".equals(str) || "CIPMP3Decoder".equals(str) || "CIPVorbisDecoder".equals(str) || "CIPAMRNBDecoder".equals(str) || "AACDecoder".equals(str) || "MP3Decoder".equals(str))) {
            return false;
        }
        if (i < 18 && "OMX.MTK.AUDIO.DECODER.AAC".equals(str)) {
            String str3 = e0.f964b;
            if ("a70".equals(str3) || ("Xiaomi".equals(e0.c) && str3.startsWith("HM"))) {
                return false;
            }
        }
        if (i == 16 && "OMX.qcom.audio.decoder.mp3".equals(str)) {
            String str4 = e0.f964b;
            if ("dlxu".equals(str4) || "protou".equals(str4) || "ville".equals(str4) || "villeplus".equals(str4) || "villec2".equals(str4) || str4.startsWith("gee") || "C6602".equals(str4) || "C6603".equals(str4) || "C6606".equals(str4) || "C6616".equals(str4) || "L36h".equals(str4) || "SO-02E".equals(str4)) {
                return false;
            }
        }
        if (i == 16 && "OMX.qcom.audio.decoder.aac".equals(str)) {
            String str5 = e0.f964b;
            if ("C1504".equals(str5) || "C1505".equals(str5) || "C1604".equals(str5) || "C1605".equals(str5)) {
                return false;
            }
        }
        if (i < 24 && (("OMX.SEC.aac.dec".equals(str) || "OMX.Exynos.AAC.Decoder".equals(str)) && "samsung".equals(e0.c))) {
            String str6 = e0.f964b;
            if (str6.startsWith("zeroflte") || str6.startsWith("zerolte") || str6.startsWith("zenlte") || "SC-05G".equals(str6) || "marinelteatt".equals(str6) || "404SC".equals(str6) || "SC-04G".equals(str6) || "SCV31".equals(str6)) {
                return false;
            }
        }
        if (i <= 19 && "OMX.SEC.vp8.dec".equals(str) && "samsung".equals(e0.c)) {
            String str7 = e0.f964b;
            if (str7.startsWith("d2") || str7.startsWith("serrano") || str7.startsWith("jflte") || str7.startsWith("santos") || str7.startsWith("t0")) {
                return false;
            }
        }
        if (i > 19 || !e0.f964b.startsWith("jflte") || !"OMX.qcom.video.decoder.vp8".equals(str)) {
            return !"audio/eac3-joc".equals(str2) || !"OMX.MTK.AUDIO.DECODER.DSPAC3".equals(str);
        }
        return false;
    }

    public static boolean h(MediaCodecInfo mediaCodecInfo) {
        if (e0.a >= 29) {
            return mediaCodecInfo.isSoftwareOnly();
        }
        String u1 = b.i.a.f.e.o.f.u1(mediaCodecInfo.getName());
        if (u1.startsWith("arc.")) {
            return false;
        }
        return u1.startsWith("omx.google.") || u1.startsWith("omx.ffmpeg.") || (u1.startsWith("omx.sec.") && u1.contains(".sw.")) || u1.equals("omx.qcom.video.decoder.hevcswvdec") || u1.startsWith("c2.android.") || u1.startsWith("c2.google.") || (!u1.startsWith("omx.") && !u1.startsWith("c2."));
    }

    public static int i() throws DecoderQueryException {
        int i;
        if (c == -1) {
            int i2 = 0;
            u d2 = d("video/avc", false, false);
            if (d2 != null) {
                MediaCodecInfo.CodecProfileLevel[] d3 = d2.d();
                int length = d3.length;
                int i3 = 0;
                while (i2 < length) {
                    int i4 = d3[i2].level;
                    if (i4 == 1 || i4 == 2) {
                        i = 25344;
                    } else {
                        switch (i4) {
                            case 8:
                            case 16:
                            case 32:
                                i = 101376;
                                continue;
                            case 64:
                                i = 202752;
                                continue;
                            case 128:
                            case 256:
                                i = 414720;
                                continue;
                            case 512:
                                i = 921600;
                                continue;
                            case 1024:
                                i = 1310720;
                                continue;
                            case 2048:
                            case 4096:
                                i = 2097152;
                                continue;
                            case 8192:
                                i = 2228224;
                                continue;
                            case 16384:
                                i = 5652480;
                                continue;
                            case 32768:
                            case 65536:
                                i = 9437184;
                                continue;
                            case 131072:
                            case 262144:
                            case 524288:
                                i = 35651584;
                                continue;
                            default:
                                i = -1;
                                continue;
                        }
                    }
                    i3 = Math.max(i, i3);
                    i2++;
                }
                i2 = Math.max(i3, e0.a >= 21 ? 345600 : 172800);
            }
            c = i2;
        }
        return c;
    }

    public static <T> void j(List<T> list, final f<T> fVar) {
        Collections.sort(list, new Comparator() { // from class: b.i.a.c.y2.h
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                MediaCodecUtil.f fVar2 = MediaCodecUtil.f.this;
                return fVar2.a(obj2) - fVar2.a(obj);
            }
        });
    }
}
