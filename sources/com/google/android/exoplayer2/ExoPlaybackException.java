package com.google.android.exoplayer2;

import androidx.annotation.CheckResult;
import androidx.annotation.Nullable;
import b.c.a.a0.d;
import b.i.a.c.a3.y;
import b.i.a.c.f3.e0;
import b.i.a.c.j1;
/* loaded from: classes3.dex */
public final class ExoPlaybackException extends PlaybackException {
    public final boolean isRecoverable;
    @Nullable
    public final y mediaPeriodId;
    @Nullable
    public final j1 rendererFormat;
    public final int rendererFormatSupport;
    public final int rendererIndex;
    @Nullable
    public final String rendererName;
    public final int type;

    public ExoPlaybackException(int i, Throwable th, int i2) {
        this(i, th, null, i2, null, -1, null, 4, false);
    }

    public static ExoPlaybackException b(RuntimeException runtimeException, int i) {
        return new ExoPlaybackException(2, runtimeException, i);
    }

    @CheckResult
    public ExoPlaybackException a(@Nullable y yVar) {
        String message = getMessage();
        int i = e0.a;
        return new ExoPlaybackException(message, getCause(), this.errorCode, this.type, this.rendererName, this.rendererIndex, this.rendererFormat, this.rendererFormatSupport, yVar, this.timestampMs, this.isRecoverable);
    }

    public ExoPlaybackException(String str, @Nullable Throwable th, int i, int i2, @Nullable String str2, int i3, @Nullable j1 j1Var, int i4, @Nullable y yVar, long j, boolean z2) {
        super(str, th, i, j);
        boolean z3 = false;
        d.j(!z2 || i2 == 1);
        d.j((th != null || i2 == 3) ? true : z3);
        this.type = i2;
        this.rendererName = str2;
        this.rendererIndex = i3;
        this.rendererFormat = j1Var;
        this.rendererFormatSupport = i4;
        this.mediaPeriodId = yVar;
        this.isRecoverable = z2;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public ExoPlaybackException(int r14, @androidx.annotation.Nullable java.lang.Throwable r15, @androidx.annotation.Nullable java.lang.String r16, int r17, @androidx.annotation.Nullable java.lang.String r18, int r19, @androidx.annotation.Nullable b.i.a.c.j1 r20, int r21, boolean r22) {
        /*
            r13 = this;
            r4 = r14
            r5 = r18
            r8 = r21
            r0 = 2
            if (r4 == 0) goto L69
            r1 = 3
            r2 = 1
            if (r4 == r2) goto L16
            if (r4 == r1) goto L11
            java.lang.String r1 = "Unexpected runtime error"
            goto L13
        L11:
            java.lang.String r1 = "Remote error"
        L13:
            r7 = r19
            goto L6d
        L16:
            java.lang.String r3 = java.lang.String.valueOf(r20)
            int r6 = b.i.a.c.f3.e0.a
            if (r8 == 0) goto L39
            if (r8 == r2) goto L36
            if (r8 == r0) goto L33
            if (r8 == r1) goto L30
            r1 = 4
            if (r8 != r1) goto L2a
            java.lang.String r1 = "YES"
            goto L3b
        L2a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L30:
            java.lang.String r1 = "NO_EXCEEDS_CAPABILITIES"
            goto L3b
        L33:
            java.lang.String r1 = "NO_UNSUPPORTED_DRM"
            goto L3b
        L36:
            java.lang.String r1 = "NO_UNSUPPORTED_TYPE"
            goto L3b
        L39:
            java.lang.String r1 = "NO"
        L3b:
            r2 = 53
            int r2 = b.d.b.a.a.b(r5, r2)
            int r6 = r3.length()
            int r6 = r6 + r2
            int r2 = r1.length()
            int r2 = r2 + r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r2)
            r6.append(r5)
            java.lang.String r2 = " error, index="
            r6.append(r2)
            r7 = r19
            r6.append(r7)
            java.lang.String r2 = ", format="
            r6.append(r2)
            java.lang.String r2 = ", format_supported="
            java.lang.String r1 = b.d.b.a.a.J(r6, r3, r2, r1)
            goto L6d
        L69:
            r7 = r19
            java.lang.String r1 = "Source error"
        L6d:
            r2 = 0
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 != 0) goto L8b
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r3 = r1.length()
            int r3 = r3 + r0
            java.lang.String r0 = "null"
            int r0 = r0.length()
            int r0 = r0 + r3
            java.lang.String r3 = ": "
            java.lang.String r0 = b.d.b.a.a.j(r0, r1, r3, r2)
            r1 = r0
        L8b:
            r9 = 0
            long r10 = android.os.SystemClock.elapsedRealtime()
            r0 = r13
            r2 = r15
            r3 = r17
            r4 = r14
            r5 = r18
            r6 = r19
            r7 = r20
            r8 = r21
            r12 = r22
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ExoPlaybackException.<init>(int, java.lang.Throwable, java.lang.String, int, java.lang.String, int, b.i.a.c.j1, int, boolean):void");
    }
}
