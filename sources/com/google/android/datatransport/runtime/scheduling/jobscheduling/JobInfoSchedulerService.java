package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Base64;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import b.i.a.b.j.b;
import b.i.a.b.j.i;
import b.i.a.b.j.n;
import b.i.a.b.j.t.h.g;
import b.i.a.b.j.t.h.l;
import b.i.a.b.j.w.a;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.JobInfoSchedulerService;
@RequiresApi(api = 21)
/* loaded from: classes3.dex */
public class JobInfoSchedulerService extends JobService {
    public static final /* synthetic */ int j = 0;

    @Override // android.app.job.JobService
    public boolean onStartJob(final JobParameters jobParameters) {
        String string = jobParameters.getExtras().getString("backendName");
        String string2 = jobParameters.getExtras().getString(NotificationCompat.MessagingStyle.Message.KEY_EXTRAS_BUNDLE);
        int i = jobParameters.getExtras().getInt("priority");
        int i2 = jobParameters.getExtras().getInt("attemptNumber");
        n.b(getApplicationContext());
        i.a a = i.a();
        a.b(string);
        a.c(a.b(i));
        if (string2 != null) {
            ((b.C0086b) a).f760b = Base64.decode(string2, 0);
        }
        l lVar = n.a().e;
        lVar.e.execute(new g(lVar, a.a(), i2, new Runnable(this, jobParameters) { // from class: b.i.a.b.j.t.h.e
            public final JobInfoSchedulerService j;
            public final JobParameters k;

            {
                this.j = this;
                this.k = jobParameters;
            }

            @Override // java.lang.Runnable
            public void run() {
                JobInfoSchedulerService jobInfoSchedulerService = this.j;
                JobParameters jobParameters2 = this.k;
                int i3 = JobInfoSchedulerService.j;
                jobInfoSchedulerService.jobFinished(jobParameters2, false);
            }
        }));
        return true;
    }

    @Override // android.app.job.JobService
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }
}
