package com.google.android.flexbox;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.e.c;
import java.util.ArrayList;
import java.util.List;
/* loaded from: classes3.dex */
public class FlexboxLayoutManager extends RecyclerView.LayoutManager implements b.i.a.e.a, RecyclerView.SmoothScroller.ScrollVectorProvider {
    public static final Rect j = new Rect();
    public final Context E;
    public View F;
    public int k;
    public int l;
    public int m;
    public boolean o;
    public boolean p;

    /* renamed from: s  reason: collision with root package name */
    public RecyclerView.Recycler f2959s;
    public RecyclerView.State t;
    public c u;
    public OrientationHelper w;

    /* renamed from: x  reason: collision with root package name */
    public OrientationHelper f2960x;

    /* renamed from: y  reason: collision with root package name */
    public SavedState f2961y;
    public int n = -1;
    public List<b.i.a.e.b> q = new ArrayList();
    public final b.i.a.e.c r = new b.i.a.e.c(this);
    public b v = new b(null);

    /* renamed from: z  reason: collision with root package name */
    public int f2962z = -1;
    public int A = Integer.MIN_VALUE;
    public int B = Integer.MIN_VALUE;
    public int C = Integer.MIN_VALUE;
    public SparseArray<View> D = new SparseArray<>();
    public int G = -1;
    public c.b H = new c.b();

    /* loaded from: classes3.dex */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public int j;
        public int k;

        /* loaded from: classes3.dex */
        public static class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, (a) null);
            }

            @Override // android.os.Parcelable.Creator
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState() {
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("SavedState{mAnchorPosition=");
            R.append(this.j);
            R.append(", mAnchorOffset=");
            return b.d.b.a.a.z(R, this.k, '}');
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.j);
            parcel.writeInt(this.k);
        }

        public SavedState(Parcel parcel, a aVar) {
            this.j = parcel.readInt();
            this.k = parcel.readInt();
        }

        public SavedState(SavedState savedState, a aVar) {
            this.j = savedState.j;
            this.k = savedState.k;
        }
    }

    /* loaded from: classes3.dex */
    public class b {
        public int a;

        /* renamed from: b  reason: collision with root package name */
        public int f2963b;
        public int c;
        public int d = 0;
        public boolean e;
        public boolean f;
        public boolean g;

        public b(a aVar) {
        }

        public static void a(b bVar) {
            int i;
            int i2;
            if (!FlexboxLayoutManager.this.i()) {
                FlexboxLayoutManager flexboxLayoutManager = FlexboxLayoutManager.this;
                if (flexboxLayoutManager.o) {
                    if (bVar.e) {
                        i2 = flexboxLayoutManager.w.getEndAfterPadding();
                    } else {
                        i2 = flexboxLayoutManager.getWidth() - FlexboxLayoutManager.this.w.getStartAfterPadding();
                    }
                    bVar.c = i2;
                    return;
                }
            }
            if (bVar.e) {
                i = FlexboxLayoutManager.this.w.getEndAfterPadding();
            } else {
                i = FlexboxLayoutManager.this.w.getStartAfterPadding();
            }
            bVar.c = i;
        }

        public static void b(b bVar) {
            bVar.a = -1;
            bVar.f2963b = -1;
            bVar.c = Integer.MIN_VALUE;
            boolean z2 = false;
            bVar.f = false;
            bVar.g = false;
            if (FlexboxLayoutManager.this.i()) {
                FlexboxLayoutManager flexboxLayoutManager = FlexboxLayoutManager.this;
                int i = flexboxLayoutManager.l;
                if (i == 0) {
                    if (flexboxLayoutManager.k == 1) {
                        z2 = true;
                    }
                    bVar.e = z2;
                    return;
                }
                if (i == 2) {
                    z2 = true;
                }
                bVar.e = z2;
                return;
            }
            FlexboxLayoutManager flexboxLayoutManager2 = FlexboxLayoutManager.this;
            int i2 = flexboxLayoutManager2.l;
            if (i2 == 0) {
                if (flexboxLayoutManager2.k == 3) {
                    z2 = true;
                }
                bVar.e = z2;
                return;
            }
            if (i2 == 2) {
                z2 = true;
            }
            bVar.e = z2;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("AnchorInfo{mPosition=");
            R.append(this.a);
            R.append(", mFlexLinePosition=");
            R.append(this.f2963b);
            R.append(", mCoordinate=");
            R.append(this.c);
            R.append(", mPerpendicularCoordinate=");
            R.append(this.d);
            R.append(", mLayoutFromEnd=");
            R.append(this.e);
            R.append(", mValid=");
            R.append(this.f);
            R.append(", mAssignedFromSavedState=");
            R.append(this.g);
            R.append('}');
            return R.toString();
        }
    }

    /* loaded from: classes3.dex */
    public static class c {
        public int a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f2964b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public int h = 1;
        public int i = 1;
        public boolean j;

        public c(a aVar) {
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("LayoutState{mAvailable=");
            R.append(this.a);
            R.append(", mFlexLinePosition=");
            R.append(this.c);
            R.append(", mPosition=");
            R.append(this.d);
            R.append(", mOffset=");
            R.append(this.e);
            R.append(", mScrollingOffset=");
            R.append(this.f);
            R.append(", mLastScrollDelta=");
            R.append(this.g);
            R.append(", mItemDirection=");
            R.append(this.h);
            R.append(", mLayoutDirection=");
            return b.d.b.a.a.z(R, this.i, '}');
        }
    }

    public FlexboxLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        RecyclerView.LayoutManager.Properties properties = RecyclerView.LayoutManager.getProperties(context, attributeSet, i, i2);
        int i3 = properties.orientation;
        if (i3 != 0) {
            if (i3 == 1) {
                if (properties.reverseLayout) {
                    x(3);
                } else {
                    x(2);
                }
            }
        } else if (properties.reverseLayout) {
            x(1);
        } else {
            x(0);
        }
        int i4 = this.l;
        if (i4 != 1) {
            if (i4 == 0) {
                removeAllViews();
                k();
            }
            this.l = 1;
            this.w = null;
            this.f2960x = null;
            requestLayout();
        }
        if (this.m != 4) {
            removeAllViews();
            k();
            this.m = 4;
            requestLayout();
        }
        setAutoMeasureEnabled(true);
        this.E = context;
    }

    public static boolean isMeasurementUpToDate(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (i3 > 0 && i != i3) {
            return false;
        }
        if (mode == Integer.MIN_VALUE) {
            return size >= i;
        }
        if (mode != 0) {
            return mode == 1073741824 && size == i;
        }
        return true;
    }

    private boolean shouldMeasureChild(View view, int i, int i2, RecyclerView.LayoutParams layoutParams) {
        return view.isLayoutRequested() || !isMeasurementCacheEnabled() || !isMeasurementUpToDate(view.getWidth(), i, ((ViewGroup.MarginLayoutParams) layoutParams).width) || !isMeasurementUpToDate(view.getHeight(), i2, ((ViewGroup.MarginLayoutParams) layoutParams).height);
    }

    public final void A(b bVar, boolean z2, boolean z3) {
        c cVar;
        if (z3) {
            w();
        } else {
            this.u.f2964b = false;
        }
        if (i() || !this.o) {
            this.u.a = bVar.c - this.w.getStartAfterPadding();
        } else {
            this.u.a = (this.F.getWidth() - bVar.c) - this.w.getStartAfterPadding();
        }
        c cVar2 = this.u;
        cVar2.d = bVar.a;
        cVar2.h = 1;
        cVar2.i = -1;
        cVar2.e = bVar.c;
        cVar2.f = Integer.MIN_VALUE;
        int i = bVar.f2963b;
        cVar2.c = i;
        if (z2 && i > 0) {
            int size = this.q.size();
            int i2 = bVar.f2963b;
            if (size > i2) {
                cVar.c--;
                this.u.d -= this.q.get(i2).h;
            }
        }
    }

    @Override // b.i.a.e.a
    public void a(View view, int i, int i2, b.i.a.e.b bVar) {
        calculateItemDecorationsForChild(view, j);
        if (i()) {
            int rightDecorationWidth = getRightDecorationWidth(view) + getLeftDecorationWidth(view);
            bVar.e += rightDecorationWidth;
            bVar.f += rightDecorationWidth;
            return;
        }
        int bottomDecorationHeight = getBottomDecorationHeight(view) + getTopDecorationHeight(view);
        bVar.e += bottomDecorationHeight;
        bVar.f += bottomDecorationHeight;
    }

    @Override // b.i.a.e.a
    public void b(b.i.a.e.b bVar) {
    }

    @Override // b.i.a.e.a
    public View c(int i) {
        return f(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean canScrollHorizontally() {
        if (this.l == 0) {
            return i();
        }
        if (i()) {
            int width = getWidth();
            View view = this.F;
            if (width <= (view != null ? view.getWidth() : 0)) {
                return false;
            }
        }
        return true;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean canScrollVertically() {
        if (this.l == 0) {
            return !i();
        }
        if (i()) {
            return true;
        }
        int height = getHeight();
        View view = this.F;
        return height > (view != null ? view.getHeight() : 0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean checkLayoutParams(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeHorizontalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeHorizontalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeHorizontalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    public final int computeScrollExtent(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        int itemCount = state.getItemCount();
        l();
        View n = n(itemCount);
        View p = p(itemCount);
        if (state.getItemCount() == 0 || n == null || p == null) {
            return 0;
        }
        return Math.min(this.w.getTotalSpace(), this.w.getDecoratedEnd(p) - this.w.getDecoratedStart(n));
    }

    public final int computeScrollOffset(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        int itemCount = state.getItemCount();
        View n = n(itemCount);
        View p = p(itemCount);
        if (!(state.getItemCount() == 0 || n == null || p == null)) {
            int position = getPosition(n);
            int position2 = getPosition(p);
            int abs = Math.abs(this.w.getDecoratedEnd(p) - this.w.getDecoratedStart(n));
            int[] iArr = this.r.c;
            int i = iArr[position];
            if (!(i == 0 || i == -1)) {
                return Math.round((i * (abs / ((iArr[position2] - i) + 1))) + (this.w.getStartAfterPadding() - this.w.getDecoratedStart(n)));
            }
        }
        return 0;
    }

    public final int computeScrollRange(RecyclerView.State state) {
        if (getChildCount() == 0) {
            return 0;
        }
        int itemCount = state.getItemCount();
        View n = n(itemCount);
        View p = p(itemCount);
        if (state.getItemCount() == 0 || n == null || p == null) {
            return 0;
        }
        View r = r(0, getChildCount(), false);
        return (int) ((Math.abs(this.w.getDecoratedEnd(p) - this.w.getDecoratedStart(n)) / ((findLastVisibleItemPosition() - (r == null ? -1 : getPosition(r))) + 1)) * state.getItemCount());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.SmoothScroller.ScrollVectorProvider
    public PointF computeScrollVectorForPosition(int i) {
        if (getChildCount() == 0) {
            return null;
        }
        int i2 = i < getPosition(getChildAt(0)) ? -1 : 1;
        if (i()) {
            return new PointF(0.0f, i2);
        }
        return new PointF(i2, 0.0f);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeVerticalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeVerticalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeVerticalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    @Override // b.i.a.e.a
    public int d(int i, int i2, int i3) {
        return RecyclerView.LayoutManager.getChildMeasureSpec(getWidth(), getWidthMode(), i2, i3, canScrollHorizontally());
    }

    @Override // b.i.a.e.a
    public void e(int i, View view) {
        this.D.put(i, view);
    }

    @Override // b.i.a.e.a
    public View f(int i) {
        View view = this.D.get(i);
        return view != null ? view : this.f2959s.getViewForPosition(i);
    }

    public int findLastVisibleItemPosition() {
        View r = r(getChildCount() - 1, -1, false);
        if (r == null) {
            return -1;
        }
        return getPosition(r);
    }

    public final int fixLayoutEndGap(int i, RecyclerView.Recycler recycler, RecyclerView.State state, boolean z2) {
        int i2;
        int endAfterPadding;
        if (!i() && this.o) {
            int startAfterPadding = i - this.w.getStartAfterPadding();
            if (startAfterPadding <= 0) {
                return 0;
            }
            i2 = t(startAfterPadding, recycler, state);
        } else {
            int endAfterPadding2 = this.w.getEndAfterPadding() - i;
            if (endAfterPadding2 <= 0) {
                return 0;
            }
            i2 = -t(-endAfterPadding2, recycler, state);
        }
        int i3 = i + i2;
        if (!z2 || (endAfterPadding = this.w.getEndAfterPadding() - i3) <= 0) {
            return i2;
        }
        this.w.offsetChildren(endAfterPadding);
        return endAfterPadding + i2;
    }

    public final int fixLayoutStartGap(int i, RecyclerView.Recycler recycler, RecyclerView.State state, boolean z2) {
        int i2;
        int startAfterPadding;
        if (i() || !this.o) {
            int startAfterPadding2 = i - this.w.getStartAfterPadding();
            if (startAfterPadding2 <= 0) {
                return 0;
            }
            i2 = -t(startAfterPadding2, recycler, state);
        } else {
            int endAfterPadding = this.w.getEndAfterPadding() - i;
            if (endAfterPadding <= 0) {
                return 0;
            }
            i2 = t(-endAfterPadding, recycler, state);
        }
        int i3 = i + i2;
        if (!z2 || (startAfterPadding = i3 - this.w.getStartAfterPadding()) <= 0) {
            return i2;
        }
        this.w.offsetChildren(-startAfterPadding);
        return i2 - startAfterPadding;
    }

    @Override // b.i.a.e.a
    public int g(View view, int i, int i2) {
        int topDecorationHeight;
        int bottomDecorationHeight;
        if (i()) {
            topDecorationHeight = getLeftDecorationWidth(view);
            bottomDecorationHeight = getRightDecorationWidth(view);
        } else {
            topDecorationHeight = getTopDecorationHeight(view);
            bottomDecorationHeight = getBottomDecorationHeight(view);
        }
        return bottomDecorationHeight + topDecorationHeight;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams generateLayoutParams(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    @Override // b.i.a.e.a
    public int getAlignContent() {
        return 5;
    }

    @Override // b.i.a.e.a
    public int getAlignItems() {
        return this.m;
    }

    @Override // b.i.a.e.a
    public int getFlexDirection() {
        return this.k;
    }

    @Override // b.i.a.e.a
    public int getFlexItemCount() {
        return this.t.getItemCount();
    }

    @Override // b.i.a.e.a
    public List<b.i.a.e.b> getFlexLinesInternal() {
        return this.q;
    }

    @Override // b.i.a.e.a
    public int getFlexWrap() {
        return this.l;
    }

    @Override // b.i.a.e.a
    public int getLargestMainSize() {
        if (this.q.size() == 0) {
            return 0;
        }
        int i = Integer.MIN_VALUE;
        int size = this.q.size();
        for (int i2 = 0; i2 < size; i2++) {
            i = Math.max(i, this.q.get(i2).e);
        }
        return i;
    }

    @Override // b.i.a.e.a
    public int getMaxLine() {
        return this.n;
    }

    @Override // b.i.a.e.a
    public int getSumOfCrossSize() {
        int size = this.q.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            i += this.q.get(i2).g;
        }
        return i;
    }

    @Override // b.i.a.e.a
    public int h(int i, int i2, int i3) {
        return RecyclerView.LayoutManager.getChildMeasureSpec(getHeight(), getHeightMode(), i2, i3, canScrollVertically());
    }

    @Override // b.i.a.e.a
    public boolean i() {
        int i = this.k;
        return i == 0 || i == 1;
    }

    @Override // b.i.a.e.a
    public int j(View view) {
        int i;
        int i2;
        if (i()) {
            i2 = getTopDecorationHeight(view);
            i = getBottomDecorationHeight(view);
        } else {
            i2 = getLeftDecorationWidth(view);
            i = getRightDecorationWidth(view);
        }
        return i + i2;
    }

    public final void k() {
        this.q.clear();
        b.b(this.v);
        this.v.d = 0;
    }

    public final void l() {
        if (this.w == null) {
            if (i()) {
                if (this.l == 0) {
                    this.w = OrientationHelper.createHorizontalHelper(this);
                    this.f2960x = OrientationHelper.createVerticalHelper(this);
                    return;
                }
                this.w = OrientationHelper.createVerticalHelper(this);
                this.f2960x = OrientationHelper.createHorizontalHelper(this);
            } else if (this.l == 0) {
                this.w = OrientationHelper.createVerticalHelper(this);
                this.f2960x = OrientationHelper.createHorizontalHelper(this);
            } else {
                this.w = OrientationHelper.createHorizontalHelper(this);
                this.f2960x = OrientationHelper.createVerticalHelper(this);
            }
        }
    }

    public final int m(RecyclerView.Recycler recycler, RecyclerView.State state, c cVar) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        LayoutParams layoutParams;
        int i11;
        int i12;
        int i13;
        int i14;
        LayoutParams layoutParams2;
        int i15;
        int i16 = cVar.f;
        if (i16 != Integer.MIN_VALUE) {
            int i17 = cVar.a;
            if (i17 < 0) {
                cVar.f = i16 + i17;
            }
            v(recycler, cVar);
        }
        int i18 = cVar.a;
        boolean i19 = i();
        int i20 = i18;
        int i21 = 0;
        while (true) {
            if (i20 <= 0 && !this.u.f2964b) {
                break;
            }
            List<b.i.a.e.b> list = this.q;
            int i22 = cVar.d;
            if (!(i22 >= 0 && i22 < state.getItemCount() && (i15 = cVar.c) >= 0 && i15 < list.size())) {
                break;
            }
            b.i.a.e.b bVar = this.q.get(cVar.c);
            cVar.d = bVar.o;
            if (i()) {
                int paddingLeft = getPaddingLeft();
                int paddingRight = getPaddingRight();
                int width = getWidth();
                int i23 = cVar.e;
                if (cVar.i == -1) {
                    i23 -= bVar.g;
                }
                int i24 = cVar.d;
                float f = this.v.d;
                float f2 = paddingLeft - f;
                float f3 = (width - paddingRight) - f;
                float max = Math.max(0.0f, 0.0f);
                int i25 = bVar.h;
                int i26 = i24;
                int i27 = 0;
                while (i26 < i24 + i25) {
                    View f4 = f(i26);
                    if (f4 == null) {
                        i13 = i18;
                        i14 = i24;
                        i12 = i26;
                        i11 = i25;
                    } else {
                        i14 = i24;
                        int i28 = i25;
                        if (cVar.i == 1) {
                            calculateItemDecorationsForChild(f4, j);
                            addView(f4);
                        } else {
                            calculateItemDecorationsForChild(f4, j);
                            addView(f4, i27);
                            i27++;
                        }
                        i27 = i27;
                        b.i.a.e.c cVar2 = this.r;
                        i13 = i18;
                        long j2 = cVar2.d[i26];
                        int i29 = (int) j2;
                        int m = cVar2.m(j2);
                        if (shouldMeasureChild(f4, i29, m, (LayoutParams) f4.getLayoutParams())) {
                            f4.measure(i29, m);
                        }
                        float leftDecorationWidth = f2 + getLeftDecorationWidth(f4) + ((ViewGroup.MarginLayoutParams) layoutParams2).leftMargin;
                        float rightDecorationWidth = f3 - (getRightDecorationWidth(f4) + ((ViewGroup.MarginLayoutParams) layoutParams2).rightMargin);
                        int topDecorationHeight = getTopDecorationHeight(f4) + i23;
                        if (this.o) {
                            i12 = i26;
                            i11 = i28;
                            this.r.u(f4, bVar, Math.round(rightDecorationWidth) - f4.getMeasuredWidth(), topDecorationHeight, Math.round(rightDecorationWidth), f4.getMeasuredHeight() + topDecorationHeight);
                        } else {
                            i12 = i26;
                            i11 = i28;
                            this.r.u(f4, bVar, Math.round(leftDecorationWidth), topDecorationHeight, f4.getMeasuredWidth() + Math.round(leftDecorationWidth), f4.getMeasuredHeight() + topDecorationHeight);
                        }
                        f2 = getRightDecorationWidth(f4) + f4.getMeasuredWidth() + ((ViewGroup.MarginLayoutParams) layoutParams2).rightMargin + max + leftDecorationWidth;
                        f3 = rightDecorationWidth - ((getLeftDecorationWidth(f4) + (f4.getMeasuredWidth() + ((ViewGroup.MarginLayoutParams) layoutParams2).leftMargin)) + max);
                    }
                    i26 = i12 + 1;
                    i24 = i14;
                    i18 = i13;
                    i25 = i11;
                }
                i2 = i18;
                cVar.c += this.u.i;
                i4 = bVar.g;
                i3 = i20;
                i = i21;
            } else {
                i2 = i18;
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                int i30 = cVar.e;
                if (cVar.i == -1) {
                    int i31 = bVar.g;
                    i30 -= i31;
                    i5 = i30 + i31;
                } else {
                    i5 = i30;
                }
                int i32 = cVar.d;
                float f5 = height - paddingBottom;
                float f6 = this.v.d;
                float f7 = paddingTop - f6;
                float f8 = f5 - f6;
                float max2 = Math.max(0.0f, 0.0f);
                int i33 = bVar.h;
                int i34 = i32;
                int i35 = 0;
                while (i34 < i32 + i33) {
                    View f9 = f(i34);
                    if (f9 == null) {
                        i10 = i20;
                        i9 = i21;
                        i8 = i34;
                        i7 = i33;
                        i6 = i32;
                    } else {
                        int i36 = i33;
                        b.i.a.e.c cVar3 = this.r;
                        int i37 = i32;
                        i10 = i20;
                        i9 = i21;
                        long j3 = cVar3.d[i34];
                        int i38 = (int) j3;
                        int m2 = cVar3.m(j3);
                        if (shouldMeasureChild(f9, i38, m2, (LayoutParams) f9.getLayoutParams())) {
                            f9.measure(i38, m2);
                        }
                        float topDecorationHeight2 = f7 + getTopDecorationHeight(f9) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
                        float bottomDecorationHeight = f8 - (getBottomDecorationHeight(f9) + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin);
                        if (cVar.i == 1) {
                            calculateItemDecorationsForChild(f9, j);
                            addView(f9);
                        } else {
                            calculateItemDecorationsForChild(f9, j);
                            addView(f9, i35);
                            i35++;
                        }
                        i35 = i35;
                        int leftDecorationWidth2 = getLeftDecorationWidth(f9) + i30;
                        int rightDecorationWidth2 = i5 - getRightDecorationWidth(f9);
                        boolean z2 = this.o;
                        if (!z2) {
                            i8 = i34;
                            i7 = i36;
                            i6 = i37;
                            if (this.p) {
                                this.r.v(f9, bVar, z2, leftDecorationWidth2, Math.round(bottomDecorationHeight) - f9.getMeasuredHeight(), f9.getMeasuredWidth() + leftDecorationWidth2, Math.round(bottomDecorationHeight));
                            } else {
                                this.r.v(f9, bVar, z2, leftDecorationWidth2, Math.round(topDecorationHeight2), f9.getMeasuredWidth() + leftDecorationWidth2, f9.getMeasuredHeight() + Math.round(topDecorationHeight2));
                            }
                        } else if (this.p) {
                            i8 = i34;
                            i7 = i36;
                            i6 = i37;
                            this.r.v(f9, bVar, z2, rightDecorationWidth2 - f9.getMeasuredWidth(), Math.round(bottomDecorationHeight) - f9.getMeasuredHeight(), rightDecorationWidth2, Math.round(bottomDecorationHeight));
                        } else {
                            i8 = i34;
                            i7 = i36;
                            i6 = i37;
                            this.r.v(f9, bVar, z2, rightDecorationWidth2 - f9.getMeasuredWidth(), Math.round(topDecorationHeight2), rightDecorationWidth2, f9.getMeasuredHeight() + Math.round(topDecorationHeight2));
                        }
                        f7 = getBottomDecorationHeight(f9) + f9.getMeasuredHeight() + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin + max2 + topDecorationHeight2;
                        f8 = bottomDecorationHeight - ((getTopDecorationHeight(f9) + (f9.getMeasuredHeight() + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin)) + max2);
                    }
                    i34 = i8 + 1;
                    i20 = i10;
                    i21 = i9;
                    i33 = i7;
                    i32 = i6;
                }
                i3 = i20;
                i = i21;
                cVar.c += this.u.i;
                i4 = bVar.g;
            }
            i21 = i + i4;
            if (i19 || !this.o) {
                cVar.e = (bVar.g * cVar.i) + cVar.e;
            } else {
                cVar.e -= bVar.g * cVar.i;
            }
            i20 = i3 - bVar.g;
            i18 = i2;
        }
        int i39 = i18;
        int i40 = i21;
        int i41 = cVar.a - i40;
        cVar.a = i41;
        int i42 = cVar.f;
        if (i42 != Integer.MIN_VALUE) {
            int i43 = i42 + i40;
            cVar.f = i43;
            if (i41 < 0) {
                cVar.f = i43 + i41;
            }
            v(recycler, cVar);
        }
        return i39 - cVar.a;
    }

    public final View n(int i) {
        View s2 = s(0, getChildCount(), i);
        if (s2 == null) {
            return null;
        }
        int i2 = this.r.c[getPosition(s2)];
        if (i2 == -1) {
            return null;
        }
        return o(s2, this.q.get(i2));
    }

    public final View o(View view, b.i.a.e.b bVar) {
        boolean i = i();
        int i2 = bVar.h;
        for (int i3 = 1; i3 < i2; i3++) {
            View childAt = getChildAt(i3);
            if (!(childAt == null || childAt.getVisibility() == 8)) {
                if (!this.o || i) {
                    if (this.w.getDecoratedStart(view) <= this.w.getDecoratedStart(childAt)) {
                    }
                    view = childAt;
                } else {
                    if (this.w.getDecoratedEnd(view) >= this.w.getDecoratedEnd(childAt)) {
                    }
                    view = childAt;
                }
            }
        }
        return view;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onAdapterChanged(RecyclerView.Adapter adapter, RecyclerView.Adapter adapter2) {
        removeAllViews();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onAttachedToWindow(RecyclerView recyclerView) {
        super.onAttachedToWindow(recyclerView);
        this.F = (View) recyclerView.getParent();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onDetachedFromWindow(RecyclerView recyclerView, RecyclerView.Recycler recycler) {
        super.onDetachedFromWindow(recyclerView, recycler);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onItemsAdded(@NonNull RecyclerView recyclerView, int i, int i2) {
        super.onItemsAdded(recyclerView, i, i2);
        y(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onItemsMoved(@NonNull RecyclerView recyclerView, int i, int i2, int i3) {
        super.onItemsMoved(recyclerView, i, i2, i3);
        y(Math.min(i, i2));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onItemsRemoved(@NonNull RecyclerView recyclerView, int i, int i2) {
        super.onItemsRemoved(recyclerView, i, i2);
        y(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onItemsUpdated(@NonNull RecyclerView recyclerView, int i, int i2, Object obj) {
        super.onItemsUpdated(recyclerView, i, i2, obj);
        y(i);
    }

    /* JADX WARN: Removed duplicated region for block: B:113:0x01b6  */
    /* JADX WARN: Removed duplicated region for block: B:164:0x0294  */
    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onLayoutChildren(androidx.recyclerview.widget.RecyclerView.Recycler r20, androidx.recyclerview.widget.RecyclerView.State r21) {
        /*
            Method dump skipped, instructions count: 1146
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.FlexboxLayoutManager.onLayoutChildren(androidx.recyclerview.widget.RecyclerView$Recycler, androidx.recyclerview.widget.RecyclerView$State):void");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onLayoutCompleted(RecyclerView.State state) {
        super.onLayoutCompleted(state);
        this.f2961y = null;
        this.f2962z = -1;
        this.A = Integer.MIN_VALUE;
        this.G = -1;
        b.b(this.v);
        this.D.clear();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.f2961y = (SavedState) parcelable;
            requestLayout();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public Parcelable onSaveInstanceState() {
        SavedState savedState = this.f2961y;
        if (savedState != null) {
            return new SavedState(savedState, (a) null);
        }
        SavedState savedState2 = new SavedState();
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            savedState2.j = getPosition(childAt);
            savedState2.k = this.w.getDecoratedStart(childAt) - this.w.getStartAfterPadding();
        } else {
            savedState2.j = -1;
        }
        return savedState2;
    }

    public final View p(int i) {
        View s2 = s(getChildCount() - 1, -1, i);
        if (s2 == null) {
            return null;
        }
        return q(s2, this.q.get(this.r.c[getPosition(s2)]));
    }

    public final View q(View view, b.i.a.e.b bVar) {
        boolean i = i();
        int childCount = (getChildCount() - bVar.h) - 1;
        for (int childCount2 = getChildCount() - 2; childCount2 > childCount; childCount2--) {
            View childAt = getChildAt(childCount2);
            if (!(childAt == null || childAt.getVisibility() == 8)) {
                if (!this.o || i) {
                    if (this.w.getDecoratedEnd(view) >= this.w.getDecoratedEnd(childAt)) {
                    }
                    view = childAt;
                } else {
                    if (this.w.getDecoratedStart(view) <= this.w.getDecoratedStart(childAt)) {
                    }
                    view = childAt;
                }
            }
        }
        return view;
    }

    public final View r(int i, int i2, boolean z2) {
        int i3 = i;
        int i4 = i2 > i3 ? 1 : -1;
        while (i3 != i2) {
            View childAt = getChildAt(i3);
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            int width = getWidth() - getPaddingRight();
            int height = getHeight() - getPaddingBottom();
            int decoratedLeft = getDecoratedLeft(childAt) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) childAt.getLayoutParams())).leftMargin;
            int decoratedTop = getDecoratedTop(childAt) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) childAt.getLayoutParams())).topMargin;
            int decoratedRight = getDecoratedRight(childAt) + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) childAt.getLayoutParams())).rightMargin;
            int decoratedBottom = getDecoratedBottom(childAt) + ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) childAt.getLayoutParams())).bottomMargin;
            boolean z3 = false;
            boolean z4 = paddingLeft <= decoratedLeft && width >= decoratedRight;
            boolean z5 = decoratedLeft >= width || decoratedRight >= paddingLeft;
            boolean z6 = paddingTop <= decoratedTop && height >= decoratedBottom;
            boolean z7 = decoratedTop >= height || decoratedBottom >= paddingTop;
            if (!z2 ? !(!z5 || !z7) : !(!z4 || !z6)) {
                z3 = true;
            }
            if (z3) {
                return childAt;
            }
            i3 += i4;
        }
        return null;
    }

    public final View s(int i, int i2, int i3) {
        l();
        View view = null;
        if (this.u == null) {
            this.u = new c(null);
        }
        int startAfterPadding = this.w.getStartAfterPadding();
        int endAfterPadding = this.w.getEndAfterPadding();
        int i4 = i2 > i ? 1 : -1;
        View view2 = null;
        while (i != i2) {
            View childAt = getChildAt(i);
            int position = getPosition(childAt);
            if (position >= 0 && position < i3) {
                if (((RecyclerView.LayoutParams) childAt.getLayoutParams()).isItemRemoved()) {
                    if (view2 == null) {
                        view2 = childAt;
                    }
                } else if (this.w.getDecoratedStart(childAt) >= startAfterPadding && this.w.getDecoratedEnd(childAt) <= endAfterPadding) {
                    return childAt;
                } else {
                    if (view == null) {
                        view = childAt;
                    }
                }
            }
            i += i4;
        }
        return view != null ? view : view2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int scrollHorizontallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (!i() || (this.l == 0 && i())) {
            int t = t(i, recycler, state);
            this.D.clear();
            return t;
        }
        int u = u(i);
        this.v.d += u;
        this.f2960x.offsetChildren(-u);
        return u;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void scrollToPosition(int i) {
        this.f2962z = i;
        this.A = Integer.MIN_VALUE;
        SavedState savedState = this.f2961y;
        if (savedState != null) {
            savedState.j = -1;
        }
        requestLayout();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int scrollVerticallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (i() || (this.l == 0 && !i())) {
            int t = t(i, recycler, state);
            this.D.clear();
            return t;
        }
        int u = u(i);
        this.v.d += u;
        this.f2960x.offsetChildren(-u);
        return u;
    }

    @Override // b.i.a.e.a
    public void setFlexLines(List<b.i.a.e.b> list) {
        this.q = list;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int i) {
        LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext());
        linearSmoothScroller.setTargetPosition(i);
        startSmoothScroll(linearSmoothScroller);
    }

    public final int t(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        int i2;
        if (getChildCount() == 0 || i == 0) {
            return 0;
        }
        l();
        this.u.j = true;
        boolean z2 = !i() && this.o;
        int i3 = (!z2 ? i <= 0 : i >= 0) ? -1 : 1;
        int abs = Math.abs(i);
        this.u.i = i3;
        boolean i4 = i();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getWidth(), getWidthMode());
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getHeight(), getHeightMode());
        boolean z3 = !i4 && this.o;
        if (i3 == 1) {
            View childAt = getChildAt(getChildCount() - 1);
            this.u.e = this.w.getDecoratedEnd(childAt);
            int position = getPosition(childAt);
            View q = q(childAt, this.q.get(this.r.c[position]));
            c cVar = this.u;
            cVar.h = 1;
            int i5 = position + 1;
            cVar.d = i5;
            int[] iArr = this.r.c;
            if (iArr.length <= i5) {
                cVar.c = -1;
            } else {
                cVar.c = iArr[i5];
            }
            if (z3) {
                cVar.e = this.w.getDecoratedStart(q);
                this.u.f = this.w.getStartAfterPadding() + (-this.w.getDecoratedStart(q));
                c cVar2 = this.u;
                int i6 = cVar2.f;
                if (i6 < 0) {
                    i6 = 0;
                }
                cVar2.f = i6;
            } else {
                cVar.e = this.w.getDecoratedEnd(q);
                this.u.f = this.w.getDecoratedEnd(q) - this.w.getEndAfterPadding();
            }
            int i7 = this.u.c;
            if ((i7 == -1 || i7 > this.q.size() - 1) && this.u.d <= getFlexItemCount()) {
                int i8 = abs - this.u.f;
                this.H.a();
                if (i8 > 0) {
                    if (i4) {
                        this.r.b(this.H, makeMeasureSpec, makeMeasureSpec2, i8, this.u.d, -1, this.q);
                    } else {
                        this.r.b(this.H, makeMeasureSpec2, makeMeasureSpec, i8, this.u.d, -1, this.q);
                    }
                    this.r.h(makeMeasureSpec, makeMeasureSpec2, this.u.d);
                    this.r.A(this.u.d);
                }
            }
        } else {
            View childAt2 = getChildAt(0);
            this.u.e = this.w.getDecoratedStart(childAt2);
            int position2 = getPosition(childAt2);
            View o = o(childAt2, this.q.get(this.r.c[position2]));
            c cVar3 = this.u;
            cVar3.h = 1;
            int i9 = this.r.c[position2];
            if (i9 == -1) {
                i9 = 0;
            }
            if (i9 > 0) {
                this.u.d = position2 - this.q.get(i9 - 1).h;
            } else {
                cVar3.d = -1;
            }
            c cVar4 = this.u;
            cVar4.c = i9 > 0 ? i9 - 1 : 0;
            if (z3) {
                cVar4.e = this.w.getDecoratedEnd(o);
                this.u.f = this.w.getDecoratedEnd(o) - this.w.getEndAfterPadding();
                c cVar5 = this.u;
                int i10 = cVar5.f;
                if (i10 < 0) {
                    i10 = 0;
                }
                cVar5.f = i10;
            } else {
                cVar4.e = this.w.getDecoratedStart(o);
                this.u.f = this.w.getStartAfterPadding() + (-this.w.getDecoratedStart(o));
            }
        }
        c cVar6 = this.u;
        int i11 = cVar6.f;
        cVar6.a = abs - i11;
        int m = m(recycler, state, cVar6) + i11;
        if (m < 0) {
            return 0;
        }
        if (z2) {
            if (abs > m) {
                i2 = (-i3) * m;
                this.w.offsetChildren(-i2);
                this.u.g = i2;
                return i2;
            }
            i2 = i;
            this.w.offsetChildren(-i2);
            this.u.g = i2;
            return i2;
        }
        if (abs > m) {
            i2 = i3 * m;
            this.w.offsetChildren(-i2);
            this.u.g = i2;
            return i2;
        }
        i2 = i;
        this.w.offsetChildren(-i2);
        this.u.g = i2;
        return i2;
    }

    public final int u(int i) {
        int i2;
        boolean z2 = false;
        if (getChildCount() == 0 || i == 0) {
            return 0;
        }
        l();
        boolean i3 = i();
        View view = this.F;
        int width = i3 ? view.getWidth() : view.getHeight();
        int width2 = i3 ? getWidth() : getHeight();
        if (getLayoutDirection() == 1) {
            z2 = true;
        }
        if (z2) {
            int abs = Math.abs(i);
            if (i < 0) {
                return -Math.min((width2 + this.v.d) - width, abs);
            }
            i2 = this.v.d;
            if (i2 + i <= 0) {
                return i;
            }
        } else if (i > 0) {
            return Math.min((width2 - this.v.d) - width, i);
        } else {
            i2 = this.v.d;
            if (i2 + i >= 0) {
                return i;
            }
        }
        return -i2;
    }

    public final void v(RecyclerView.Recycler recycler, c cVar) {
        int childCount;
        if (cVar.j) {
            int i = -1;
            if (cVar.i == -1) {
                if (cVar.f >= 0) {
                    this.w.getEnd();
                    int childCount2 = getChildCount();
                    if (childCount2 != 0) {
                        int i2 = childCount2 - 1;
                        int i3 = this.r.c[getPosition(getChildAt(i2))];
                        if (i3 != -1) {
                            b.i.a.e.b bVar = this.q.get(i3);
                            int i4 = i2;
                            while (true) {
                                if (i4 < 0) {
                                    break;
                                }
                                View childAt = getChildAt(i4);
                                int i5 = cVar.f;
                                if (!(i() || !this.o ? this.w.getDecoratedStart(childAt) >= this.w.getEnd() - i5 : this.w.getDecoratedEnd(childAt) <= i5)) {
                                    break;
                                }
                                if (bVar.o == getPosition(childAt)) {
                                    if (i3 <= 0) {
                                        childCount2 = i4;
                                        break;
                                    }
                                    i3 += cVar.i;
                                    bVar = this.q.get(i3);
                                    childCount2 = i4;
                                }
                                i4--;
                            }
                            while (i2 >= childCount2) {
                                removeAndRecycleViewAt(i2, recycler);
                                i2--;
                            }
                        }
                    }
                }
            } else if (cVar.f >= 0 && (childCount = getChildCount()) != 0) {
                int i6 = this.r.c[getPosition(getChildAt(0))];
                if (i6 != -1) {
                    b.i.a.e.b bVar2 = this.q.get(i6);
                    int i7 = 0;
                    while (true) {
                        if (i7 >= childCount) {
                            break;
                        }
                        View childAt2 = getChildAt(i7);
                        int i8 = cVar.f;
                        if (!(i() || !this.o ? this.w.getDecoratedEnd(childAt2) <= i8 : this.w.getEnd() - this.w.getDecoratedStart(childAt2) <= i8)) {
                            break;
                        }
                        if (bVar2.p == getPosition(childAt2)) {
                            if (i6 >= this.q.size() - 1) {
                                i = i7;
                                break;
                            }
                            i6 += cVar.i;
                            bVar2 = this.q.get(i6);
                            i = i7;
                        }
                        i7++;
                    }
                    while (i >= 0) {
                        removeAndRecycleViewAt(i, recycler);
                        i--;
                    }
                }
            }
        }
    }

    public final void w() {
        int i;
        if (i()) {
            i = getHeightMode();
        } else {
            i = getWidthMode();
        }
        this.u.f2964b = i == 0 || i == Integer.MIN_VALUE;
    }

    public void x(int i) {
        if (this.k != i) {
            removeAllViews();
            this.k = i;
            this.w = null;
            this.f2960x = null;
            k();
            requestLayout();
        }
    }

    public final void y(int i) {
        if (i < findLastVisibleItemPosition()) {
            int childCount = getChildCount();
            this.r.j(childCount);
            this.r.k(childCount);
            this.r.i(childCount);
            if (i < this.r.c.length) {
                this.G = i;
                View childAt = getChildAt(0);
                if (childAt != null) {
                    this.f2962z = getPosition(childAt);
                    if (i() || !this.o) {
                        this.A = this.w.getDecoratedStart(childAt) - this.w.getStartAfterPadding();
                        return;
                    }
                    this.A = this.w.getEndPadding() + this.w.getDecoratedEnd(childAt);
                }
            }
        }
    }

    public final void z(b bVar, boolean z2, boolean z3) {
        int i;
        if (z3) {
            w();
        } else {
            this.u.f2964b = false;
        }
        if (i() || !this.o) {
            this.u.a = this.w.getEndAfterPadding() - bVar.c;
        } else {
            this.u.a = bVar.c - getPaddingRight();
        }
        c cVar = this.u;
        cVar.d = bVar.a;
        cVar.h = 1;
        cVar.i = 1;
        cVar.e = bVar.c;
        cVar.f = Integer.MIN_VALUE;
        cVar.c = bVar.f2963b;
        if (z2 && this.q.size() > 1 && (i = bVar.f2963b) >= 0 && i < this.q.size() - 1) {
            c cVar2 = this.u;
            cVar2.c++;
            cVar2.d += this.q.get(bVar.f2963b).h;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onItemsUpdated(@NonNull RecyclerView recyclerView, int i, int i2) {
        super.onItemsUpdated(recyclerView, i, i2);
        y(i);
    }

    /* loaded from: classes3.dex */
    public static class LayoutParams extends RecyclerView.LayoutParams implements FlexItem {
        public static final Parcelable.Creator<LayoutParams> CREATOR = new a();
        public float j;
        public float k;
        public int l;
        public float m;
        public int n;
        public int o;
        public int p;
        public int q;
        public boolean r;

        /* loaded from: classes3.dex */
        public static class a implements Parcelable.Creator<LayoutParams> {
            @Override // android.os.Parcelable.Creator
            public LayoutParams createFromParcel(Parcel parcel) {
                return new LayoutParams(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public LayoutParams[] newArray(int i) {
                return new LayoutParams[i];
            }
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.j = 0.0f;
            this.k = 1.0f;
            this.l = -1;
            this.m = -1.0f;
            this.p = ViewCompat.MEASURED_SIZE_MASK;
            this.q = ViewCompat.MEASURED_SIZE_MASK;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int D() {
            return this.n;
        }

        @Override // com.google.android.flexbox.FlexItem
        public void H(int i) {
            this.n = i;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int I() {
            return ((ViewGroup.MarginLayoutParams) this).bottomMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int J() {
            return ((ViewGroup.MarginLayoutParams) this).leftMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int N() {
            return ((ViewGroup.MarginLayoutParams) this).topMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public void O(int i) {
            this.o = i;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float S() {
            return this.j;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float W() {
            return this.m;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int d0() {
            return ((ViewGroup.MarginLayoutParams) this).rightMargin;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int f0() {
            return this.o;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getHeight() {
            return ((ViewGroup.MarginLayoutParams) this).height;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getOrder() {
            return 1;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getWidth() {
            return ((ViewGroup.MarginLayoutParams) this).width;
        }

        @Override // com.google.android.flexbox.FlexItem
        public boolean j0() {
            return this.r;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int m0() {
            return this.q;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int t0() {
            return this.p;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int u() {
            return this.l;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeFloat(this.j);
            parcel.writeFloat(this.k);
            parcel.writeInt(this.l);
            parcel.writeFloat(this.m);
            parcel.writeInt(this.n);
            parcel.writeInt(this.o);
            parcel.writeInt(this.p);
            parcel.writeInt(this.q);
            parcel.writeByte(this.r ? (byte) 1 : (byte) 0);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).bottomMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).leftMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).rightMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).topMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).height);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).width);
        }

        @Override // com.google.android.flexbox.FlexItem
        public float x() {
            return this.k;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.j = 0.0f;
            this.k = 1.0f;
            this.l = -1;
            this.m = -1.0f;
            this.p = ViewCompat.MEASURED_SIZE_MASK;
            this.q = ViewCompat.MEASURED_SIZE_MASK;
        }

        public LayoutParams(Parcel parcel) {
            super(-2, -2);
            this.j = 0.0f;
            this.k = 1.0f;
            this.l = -1;
            this.m = -1.0f;
            this.p = ViewCompat.MEASURED_SIZE_MASK;
            this.q = ViewCompat.MEASURED_SIZE_MASK;
            this.j = parcel.readFloat();
            this.k = parcel.readFloat();
            this.l = parcel.readInt();
            this.m = parcel.readFloat();
            this.n = parcel.readInt();
            this.o = parcel.readInt();
            this.p = parcel.readInt();
            this.q = parcel.readInt();
            this.r = parcel.readByte() != 0;
            ((ViewGroup.MarginLayoutParams) this).bottomMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).leftMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).rightMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).topMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).height = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).width = parcel.readInt();
        }
    }
}
