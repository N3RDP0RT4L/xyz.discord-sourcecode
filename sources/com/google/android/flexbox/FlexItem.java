package com.google.android.flexbox;

import android.os.Parcelable;
/* loaded from: classes3.dex */
public interface FlexItem extends Parcelable {
    int D();

    void H(int i);

    int I();

    int J();

    int N();

    void O(int i);

    float S();

    float W();

    int d0();

    int f0();

    int getHeight();

    int getOrder();

    int getWidth();

    boolean j0();

    int m0();

    int t0();

    int u();

    float x();
}
