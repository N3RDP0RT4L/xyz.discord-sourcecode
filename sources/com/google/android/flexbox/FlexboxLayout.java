package com.google.android.flexbox;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import b.i.a.e.a;
import b.i.a.e.b;
import b.i.a.e.c;
import java.util.ArrayList;
import java.util.List;
/* loaded from: classes3.dex */
public class FlexboxLayout extends ViewGroup implements a {
    public int j;
    public int k;
    public int l;
    public int m;
    public int n;
    public int o;
    @Nullable
    public Drawable p;
    @Nullable
    public Drawable q;
    public int r;

    /* renamed from: s  reason: collision with root package name */
    public int f2954s;
    public int t;
    public int u;
    public int[] v;
    public SparseIntArray w;

    /* renamed from: x  reason: collision with root package name */
    public c f2955x = new c(this);

    /* renamed from: y  reason: collision with root package name */
    public List<b> f2956y = new ArrayList();

    /* renamed from: z  reason: collision with root package name */
    public c.b f2957z = new c.b();

    public FlexboxLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        this.o = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.a.FlexboxLayout, 0, 0);
        this.j = obtainStyledAttributes.getInt(R.a.FlexboxLayout_flexDirection, 0);
        this.k = obtainStyledAttributes.getInt(R.a.FlexboxLayout_flexWrap, 0);
        this.l = obtainStyledAttributes.getInt(R.a.FlexboxLayout_justifyContent, 0);
        this.m = obtainStyledAttributes.getInt(R.a.FlexboxLayout_alignItems, 0);
        this.n = obtainStyledAttributes.getInt(R.a.FlexboxLayout_alignContent, 0);
        this.o = obtainStyledAttributes.getInt(R.a.FlexboxLayout_maxLine, -1);
        Drawable drawable = obtainStyledAttributes.getDrawable(R.a.FlexboxLayout_dividerDrawable);
        if (drawable != null) {
            setDividerDrawableHorizontal(drawable);
            setDividerDrawableVertical(drawable);
        }
        Drawable drawable2 = obtainStyledAttributes.getDrawable(R.a.FlexboxLayout_dividerDrawableHorizontal);
        if (drawable2 != null) {
            setDividerDrawableHorizontal(drawable2);
        }
        Drawable drawable3 = obtainStyledAttributes.getDrawable(R.a.FlexboxLayout_dividerDrawableVertical);
        if (drawable3 != null) {
            setDividerDrawableVertical(drawable3);
        }
        int i = obtainStyledAttributes.getInt(R.a.FlexboxLayout_showDivider, 0);
        if (i != 0) {
            this.f2954s = i;
            this.r = i;
        }
        int i2 = obtainStyledAttributes.getInt(R.a.FlexboxLayout_showDividerVertical, 0);
        if (i2 != 0) {
            this.f2954s = i2;
        }
        int i3 = obtainStyledAttributes.getInt(R.a.FlexboxLayout_showDividerHorizontal, 0);
        if (i3 != 0) {
            this.r = i3;
        }
        obtainStyledAttributes.recycle();
    }

    @Override // b.i.a.e.a
    public void a(View view, int i, int i2, b bVar) {
        if (!p(i, i2)) {
            return;
        }
        if (i()) {
            int i3 = bVar.e;
            int i4 = this.u;
            bVar.e = i3 + i4;
            bVar.f += i4;
            return;
        }
        int i5 = bVar.e;
        int i6 = this.t;
        bVar.e = i5 + i6;
        bVar.f += i6;
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (this.w == null) {
            this.w = new SparseIntArray(getChildCount());
        }
        c cVar = this.f2955x;
        SparseIntArray sparseIntArray = this.w;
        int flexItemCount = cVar.a.getFlexItemCount();
        List<c.C0108c> f = cVar.f(flexItemCount);
        c.C0108c cVar2 = new c.C0108c(null);
        if (view == null || !(layoutParams instanceof FlexItem)) {
            cVar2.k = 1;
        } else {
            cVar2.k = ((FlexItem) layoutParams).getOrder();
        }
        if (i == -1 || i == flexItemCount) {
            cVar2.j = flexItemCount;
        } else if (i < cVar.a.getFlexItemCount()) {
            cVar2.j = i;
            for (int i2 = i; i2 < flexItemCount; i2++) {
                ((c.C0108c) ((ArrayList) f).get(i2)).j++;
            }
        } else {
            cVar2.j = flexItemCount;
        }
        ((ArrayList) f).add(cVar2);
        this.v = cVar.x(flexItemCount + 1, f, sparseIntArray);
        super.addView(view, i, layoutParams);
    }

    @Override // b.i.a.e.a
    public void b(b bVar) {
        if (i()) {
            if ((this.f2954s & 4) > 0) {
                int i = bVar.e;
                int i2 = this.u;
                bVar.e = i + i2;
                bVar.f += i2;
            }
        } else if ((this.r & 4) > 0) {
            int i3 = bVar.e;
            int i4 = this.t;
            bVar.e = i3 + i4;
            bVar.f += i4;
        }
    }

    @Override // b.i.a.e.a
    public View c(int i) {
        return o(i);
    }

    @Override // android.view.ViewGroup
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    @Override // b.i.a.e.a
    public int d(int i, int i2, int i3) {
        return ViewGroup.getChildMeasureSpec(i, i2, i3);
    }

    @Override // b.i.a.e.a
    public void e(int i, View view) {
    }

    @Override // b.i.a.e.a
    public View f(int i) {
        return getChildAt(i);
    }

    @Override // b.i.a.e.a
    public int g(View view, int i, int i2) {
        int i3;
        int i4 = 0;
        if (i()) {
            if (p(i, i2)) {
                i4 = 0 + this.u;
            }
            if ((this.f2954s & 4) <= 0) {
                return i4;
            }
            i3 = this.u;
        } else {
            if (p(i, i2)) {
                i4 = 0 + this.t;
            }
            if ((this.r & 4) <= 0) {
                return i4;
            }
            i3 = this.t;
        }
        return i4 + i3;
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    @Override // b.i.a.e.a
    public int getAlignContent() {
        return this.n;
    }

    @Override // b.i.a.e.a
    public int getAlignItems() {
        return this.m;
    }

    @Nullable
    public Drawable getDividerDrawableHorizontal() {
        return this.p;
    }

    @Nullable
    public Drawable getDividerDrawableVertical() {
        return this.q;
    }

    @Override // b.i.a.e.a
    public int getFlexDirection() {
        return this.j;
    }

    @Override // b.i.a.e.a
    public int getFlexItemCount() {
        return getChildCount();
    }

    public List<b> getFlexLines() {
        ArrayList arrayList = new ArrayList(this.f2956y.size());
        for (b bVar : this.f2956y) {
            if (bVar.a() != 0) {
                arrayList.add(bVar);
            }
        }
        return arrayList;
    }

    @Override // b.i.a.e.a
    public List<b> getFlexLinesInternal() {
        return this.f2956y;
    }

    @Override // b.i.a.e.a
    public int getFlexWrap() {
        return this.k;
    }

    public int getJustifyContent() {
        return this.l;
    }

    @Override // b.i.a.e.a
    public int getLargestMainSize() {
        int i = Integer.MIN_VALUE;
        for (b bVar : this.f2956y) {
            i = Math.max(i, bVar.e);
        }
        return i;
    }

    @Override // b.i.a.e.a
    public int getMaxLine() {
        return this.o;
    }

    public int getShowDividerHorizontal() {
        return this.r;
    }

    public int getShowDividerVertical() {
        return this.f2954s;
    }

    @Override // b.i.a.e.a
    public int getSumOfCrossSize() {
        int i;
        int i2;
        int size = this.f2956y.size();
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            b bVar = this.f2956y.get(i4);
            if (q(i4)) {
                if (i()) {
                    i2 = this.t;
                } else {
                    i2 = this.u;
                }
                i3 += i2;
            }
            if (r(i4)) {
                if (i()) {
                    i = this.t;
                } else {
                    i = this.u;
                }
                i3 += i;
            }
            i3 += bVar.g;
        }
        return i3;
    }

    @Override // b.i.a.e.a
    public int h(int i, int i2, int i3) {
        return ViewGroup.getChildMeasureSpec(i, i2, i3);
    }

    @Override // b.i.a.e.a
    public boolean i() {
        int i = this.j;
        return i == 0 || i == 1;
    }

    @Override // b.i.a.e.a
    public int j(View view) {
        return 0;
    }

    public final void k(Canvas canvas, boolean z2, boolean z3) {
        int i;
        int i2;
        int i3;
        int i4;
        int paddingLeft = getPaddingLeft();
        int max = Math.max(0, (getWidth() - getPaddingRight()) - paddingLeft);
        int size = this.f2956y.size();
        for (int i5 = 0; i5 < size; i5++) {
            b bVar = this.f2956y.get(i5);
            for (int i6 = 0; i6 < bVar.h; i6++) {
                int i7 = bVar.o + i6;
                View o = o(i7);
                if (!(o == null || o.getVisibility() == 8)) {
                    LayoutParams layoutParams = (LayoutParams) o.getLayoutParams();
                    if (p(i7, i6)) {
                        if (z2) {
                            i4 = o.getRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
                        } else {
                            i4 = (o.getLeft() - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin) - this.u;
                        }
                        n(canvas, i4, bVar.f1323b, bVar.g);
                    }
                    if (i6 == bVar.h - 1 && (this.f2954s & 4) > 0) {
                        if (z2) {
                            i3 = (o.getLeft() - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin) - this.u;
                        } else {
                            i3 = o.getRight() + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
                        }
                        n(canvas, i3, bVar.f1323b, bVar.g);
                    }
                }
            }
            if (q(i5)) {
                if (z3) {
                    i2 = bVar.d;
                } else {
                    i2 = bVar.f1323b - this.t;
                }
                m(canvas, paddingLeft, i2, max);
            }
            if (r(i5) && (this.r & 4) > 0) {
                if (z3) {
                    i = bVar.f1323b - this.t;
                } else {
                    i = bVar.d;
                }
                m(canvas, paddingLeft, i, max);
            }
        }
    }

    public final void l(Canvas canvas, boolean z2, boolean z3) {
        int i;
        int i2;
        int i3;
        int i4;
        int paddingTop = getPaddingTop();
        int max = Math.max(0, (getHeight() - getPaddingBottom()) - paddingTop);
        int size = this.f2956y.size();
        for (int i5 = 0; i5 < size; i5++) {
            b bVar = this.f2956y.get(i5);
            for (int i6 = 0; i6 < bVar.h; i6++) {
                int i7 = bVar.o + i6;
                View o = o(i7);
                if (!(o == null || o.getVisibility() == 8)) {
                    LayoutParams layoutParams = (LayoutParams) o.getLayoutParams();
                    if (p(i7, i6)) {
                        if (z3) {
                            i4 = o.getBottom() + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                        } else {
                            i4 = (o.getTop() - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin) - this.t;
                        }
                        m(canvas, bVar.a, i4, bVar.g);
                    }
                    if (i6 == bVar.h - 1 && (this.r & 4) > 0) {
                        if (z3) {
                            i3 = (o.getTop() - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin) - this.t;
                        } else {
                            i3 = o.getBottom() + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin;
                        }
                        m(canvas, bVar.a, i3, bVar.g);
                    }
                }
            }
            if (q(i5)) {
                if (z2) {
                    i2 = bVar.c;
                } else {
                    i2 = bVar.a - this.u;
                }
                n(canvas, i2, paddingTop, max);
            }
            if (r(i5) && (this.f2954s & 4) > 0) {
                if (z2) {
                    i = bVar.a - this.u;
                } else {
                    i = bVar.c;
                }
                n(canvas, i, paddingTop, max);
            }
        }
    }

    public final void m(Canvas canvas, int i, int i2, int i3) {
        Drawable drawable = this.p;
        if (drawable != null) {
            drawable.setBounds(i, i2, i3 + i, this.t + i2);
            this.p.draw(canvas);
        }
    }

    public final void n(Canvas canvas, int i, int i2, int i3) {
        Drawable drawable = this.q;
        if (drawable != null) {
            drawable.setBounds(i, i2, this.u + i, i3 + i2);
            this.q.draw(canvas);
        }
    }

    public View o(int i) {
        if (i < 0) {
            return null;
        }
        int[] iArr = this.v;
        if (i >= iArr.length) {
            return null;
        }
        return getChildAt(iArr[i]);
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        if (this.q != null || this.p != null) {
            if (this.r != 0 || this.f2954s != 0) {
                int layoutDirection = ViewCompat.getLayoutDirection(this);
                int i = this.j;
                boolean z2 = false;
                boolean z3 = true;
                if (i == 0) {
                    boolean z4 = layoutDirection == 1;
                    if (this.k == 2) {
                        z2 = true;
                    }
                    k(canvas, z4, z2);
                } else if (i == 1) {
                    boolean z5 = layoutDirection != 1;
                    if (this.k == 2) {
                        z2 = true;
                    }
                    k(canvas, z5, z2);
                } else if (i == 2) {
                    if (layoutDirection != 1) {
                        z3 = false;
                    }
                    if (this.k == 2) {
                        z3 = !z3;
                    }
                    l(canvas, z3, false);
                } else if (i == 3) {
                    if (layoutDirection == 1) {
                        z2 = true;
                    }
                    if (this.k == 2) {
                        z2 = !z2;
                    }
                    l(canvas, z2, true);
                }
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int i5 = this.j;
        boolean z3 = false;
        if (i5 == 0) {
            s(layoutDirection == 1, i, i2, i3, i4);
        } else if (i5 == 1) {
            s(layoutDirection != 1, i, i2, i3, i4);
        } else if (i5 == 2) {
            if (layoutDirection == 1) {
                z3 = true;
            }
            if (this.k == 2) {
                z3 = !z3;
            }
            t(z3, false, i, i2, i3, i4);
        } else if (i5 == 3) {
            if (layoutDirection == 1) {
                z3 = true;
            }
            if (this.k == 2) {
                z3 = !z3;
            }
            t(z3, true, i, i2, i3, i4);
        } else {
            StringBuilder R = b.d.b.a.a.R("Invalid flex direction is set: ");
            R.append(this.j);
            throw new IllegalStateException(R.toString());
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:17:0x004a  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x00ec  */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onMeasure(int r15, int r16) {
        /*
            Method dump skipped, instructions count: 380
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.FlexboxLayout.onMeasure(int, int):void");
    }

    public final boolean p(int i, int i2) {
        boolean z2;
        int i3 = 1;
        while (true) {
            if (i3 > i2) {
                z2 = true;
                break;
            }
            View o = o(i - i3);
            if (o != null && o.getVisibility() != 8) {
                z2 = false;
                break;
            }
            i3++;
        }
        return z2 ? i() ? (this.f2954s & 1) != 0 : (this.r & 1) != 0 : i() ? (this.f2954s & 2) != 0 : (this.r & 2) != 0;
    }

    public final boolean q(int i) {
        boolean z2;
        if (i < 0 || i >= this.f2956y.size()) {
            return false;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z2 = true;
                break;
            } else if (this.f2956y.get(i2).a() > 0) {
                z2 = false;
                break;
            } else {
                i2++;
            }
        }
        return z2 ? i() ? (this.r & 1) != 0 : (this.f2954s & 1) != 0 : i() ? (this.r & 2) != 0 : (this.f2954s & 2) != 0;
    }

    public final boolean r(int i) {
        if (i < 0 || i >= this.f2956y.size()) {
            return false;
        }
        for (int i2 = i + 1; i2 < this.f2956y.size(); i2++) {
            if (this.f2956y.get(i2).a() > 0) {
                return false;
            }
        }
        return i() ? (this.r & 4) != 0 : (this.f2954s & 4) != 0;
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x00d9  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void s(boolean r26, int r27, int r28, int r29, int r30) {
        /*
            Method dump skipped, instructions count: 522
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.FlexboxLayout.s(boolean, int, int, int, int):void");
    }

    public void setAlignContent(int i) {
        if (this.n != i) {
            this.n = i;
            requestLayout();
        }
    }

    public void setAlignItems(int i) {
        if (this.m != i) {
            this.m = i;
            requestLayout();
        }
    }

    public void setDividerDrawable(Drawable drawable) {
        setDividerDrawableHorizontal(drawable);
        setDividerDrawableVertical(drawable);
    }

    public void setDividerDrawableHorizontal(@Nullable Drawable drawable) {
        if (drawable != this.p) {
            this.p = drawable;
            if (drawable != null) {
                this.t = drawable.getIntrinsicHeight();
            } else {
                this.t = 0;
            }
            if (this.p == null && this.q == null) {
                setWillNotDraw(true);
            } else {
                setWillNotDraw(false);
            }
            requestLayout();
        }
    }

    public void setDividerDrawableVertical(@Nullable Drawable drawable) {
        if (drawable != this.q) {
            this.q = drawable;
            if (drawable != null) {
                this.u = drawable.getIntrinsicWidth();
            } else {
                this.u = 0;
            }
            if (this.p == null && this.q == null) {
                setWillNotDraw(true);
            } else {
                setWillNotDraw(false);
            }
            requestLayout();
        }
    }

    public void setFlexDirection(int i) {
        if (this.j != i) {
            this.j = i;
            requestLayout();
        }
    }

    @Override // b.i.a.e.a
    public void setFlexLines(List<b> list) {
        this.f2956y = list;
    }

    public void setFlexWrap(int i) {
        if (this.k != i) {
            this.k = i;
            requestLayout();
        }
    }

    public void setJustifyContent(int i) {
        if (this.l != i) {
            this.l = i;
            requestLayout();
        }
    }

    public void setMaxLine(int i) {
        if (this.o != i) {
            this.o = i;
            requestLayout();
        }
    }

    public void setShowDivider(int i) {
        setShowDividerVertical(i);
        setShowDividerHorizontal(i);
    }

    public void setShowDividerHorizontal(int i) {
        if (i != this.r) {
            this.r = i;
            requestLayout();
        }
    }

    public void setShowDividerVertical(int i) {
        if (i != this.f2954s) {
            this.f2954s = i;
            requestLayout();
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:42:0x00d7  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void t(boolean r28, boolean r29, int r30, int r31, int r32, int r33) {
        /*
            Method dump skipped, instructions count: 514
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.flexbox.FlexboxLayout.t(boolean, boolean, int, int, int, int):void");
    }

    public final void u(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (i == 0 || i == 1) {
            i5 = getPaddingBottom() + getPaddingTop() + getSumOfCrossSize();
            i6 = getLargestMainSize();
        } else if (i == 2 || i == 3) {
            i5 = getLargestMainSize();
            i6 = getPaddingRight() + getPaddingLeft() + getSumOfCrossSize();
        } else {
            throw new IllegalArgumentException(b.d.b.a.a.p("Invalid flex direction: ", i));
        }
        if (mode == Integer.MIN_VALUE) {
            if (size < i6) {
                i4 = View.combineMeasuredStates(i4, 16777216);
            } else {
                size = i6;
            }
            i7 = View.resolveSizeAndState(size, i2, i4);
        } else if (mode == 0) {
            i7 = View.resolveSizeAndState(i6, i2, i4);
        } else if (mode == 1073741824) {
            if (size < i6) {
                i4 = View.combineMeasuredStates(i4, 16777216);
            }
            i7 = View.resolveSizeAndState(size, i2, i4);
        } else {
            throw new IllegalStateException(b.d.b.a.a.p("Unknown width mode is set: ", mode));
        }
        if (mode2 == Integer.MIN_VALUE) {
            if (size2 < i5) {
                i4 = View.combineMeasuredStates(i4, 256);
            } else {
                size2 = i5;
            }
            i8 = View.resolveSizeAndState(size2, i3, i4);
        } else if (mode2 == 0) {
            i8 = View.resolveSizeAndState(i5, i3, i4);
        } else if (mode2 == 1073741824) {
            if (size2 < i5) {
                i4 = View.combineMeasuredStates(i4, 256);
            }
            i8 = View.resolveSizeAndState(size2, i3, i4);
        } else {
            throw new IllegalStateException(b.d.b.a.a.p("Unknown height mode is set: ", mode2));
        }
        setMeasuredDimension(i7, i8);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LayoutParams) {
            return new LayoutParams((LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    /* loaded from: classes3.dex */
    public static class LayoutParams extends ViewGroup.MarginLayoutParams implements FlexItem {
        public static final Parcelable.Creator<LayoutParams> CREATOR = new a();
        public int j;
        public float k;
        public float l;
        public int m;
        public float n;
        public int o;
        public int p;
        public int q;
        public int r;

        /* renamed from: s  reason: collision with root package name */
        public boolean f2958s;

        /* loaded from: classes3.dex */
        public static class a implements Parcelable.Creator<LayoutParams> {
            @Override // android.os.Parcelable.Creator
            public LayoutParams createFromParcel(Parcel parcel) {
                return new LayoutParams(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public LayoutParams[] newArray(int i) {
                return new LayoutParams[i];
            }
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.j = 1;
            this.k = 0.0f;
            this.l = 1.0f;
            this.m = -1;
            this.n = -1.0f;
            this.o = -1;
            this.p = -1;
            this.q = ViewCompat.MEASURED_SIZE_MASK;
            this.r = ViewCompat.MEASURED_SIZE_MASK;
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.a.FlexboxLayout_Layout);
            this.j = obtainStyledAttributes.getInt(R.a.FlexboxLayout_Layout_layout_order, 1);
            this.k = obtainStyledAttributes.getFloat(R.a.FlexboxLayout_Layout_layout_flexGrow, 0.0f);
            this.l = obtainStyledAttributes.getFloat(R.a.FlexboxLayout_Layout_layout_flexShrink, 1.0f);
            this.m = obtainStyledAttributes.getInt(R.a.FlexboxLayout_Layout_layout_alignSelf, -1);
            this.n = obtainStyledAttributes.getFraction(R.a.FlexboxLayout_Layout_layout_flexBasisPercent, 1, 1, -1.0f);
            this.o = obtainStyledAttributes.getDimensionPixelSize(R.a.FlexboxLayout_Layout_layout_minWidth, -1);
            this.p = obtainStyledAttributes.getDimensionPixelSize(R.a.FlexboxLayout_Layout_layout_minHeight, -1);
            this.q = obtainStyledAttributes.getDimensionPixelSize(R.a.FlexboxLayout_Layout_layout_maxWidth, ViewCompat.MEASURED_SIZE_MASK);
            this.r = obtainStyledAttributes.getDimensionPixelSize(R.a.FlexboxLayout_Layout_layout_maxHeight, ViewCompat.MEASURED_SIZE_MASK);
            this.f2958s = obtainStyledAttributes.getBoolean(R.a.FlexboxLayout_Layout_layout_wrapBefore, false);
            obtainStyledAttributes.recycle();
        }

        @Override // com.google.android.flexbox.FlexItem
        public int D() {
            return this.o;
        }

        @Override // com.google.android.flexbox.FlexItem
        public void H(int i) {
            this.o = i;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int I() {
            return ((ViewGroup.MarginLayoutParams) this).bottomMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int J() {
            return ((ViewGroup.MarginLayoutParams) this).leftMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int N() {
            return ((ViewGroup.MarginLayoutParams) this).topMargin;
        }

        @Override // com.google.android.flexbox.FlexItem
        public void O(int i) {
            this.p = i;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float S() {
            return this.k;
        }

        @Override // com.google.android.flexbox.FlexItem
        public float W() {
            return this.n;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int d0() {
            return ((ViewGroup.MarginLayoutParams) this).rightMargin;
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int f0() {
            return this.p;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getHeight() {
            return ((ViewGroup.MarginLayoutParams) this).height;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getOrder() {
            return this.j;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int getWidth() {
            return ((ViewGroup.MarginLayoutParams) this).width;
        }

        @Override // com.google.android.flexbox.FlexItem
        public boolean j0() {
            return this.f2958s;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int m0() {
            return this.r;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int t0() {
            return this.q;
        }

        @Override // com.google.android.flexbox.FlexItem
        public int u() {
            return this.m;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.j);
            parcel.writeFloat(this.k);
            parcel.writeFloat(this.l);
            parcel.writeInt(this.m);
            parcel.writeFloat(this.n);
            parcel.writeInt(this.o);
            parcel.writeInt(this.p);
            parcel.writeInt(this.q);
            parcel.writeInt(this.r);
            parcel.writeByte(this.f2958s ? (byte) 1 : (byte) 0);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).bottomMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).leftMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).rightMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).topMargin);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).height);
            parcel.writeInt(((ViewGroup.MarginLayoutParams) this).width);
        }

        @Override // com.google.android.flexbox.FlexItem
        public float x() {
            return this.l;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            this.j = 1;
            this.k = 0.0f;
            this.l = 1.0f;
            this.m = -1;
            this.n = -1.0f;
            this.o = -1;
            this.p = -1;
            this.q = ViewCompat.MEASURED_SIZE_MASK;
            this.r = ViewCompat.MEASURED_SIZE_MASK;
            this.j = layoutParams.j;
            this.k = layoutParams.k;
            this.l = layoutParams.l;
            this.m = layoutParams.m;
            this.n = layoutParams.n;
            this.o = layoutParams.o;
            this.p = layoutParams.p;
            this.q = layoutParams.q;
            this.r = layoutParams.r;
            this.f2958s = layoutParams.f2958s;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.j = 1;
            this.k = 0.0f;
            this.l = 1.0f;
            this.m = -1;
            this.n = -1.0f;
            this.o = -1;
            this.p = -1;
            this.q = ViewCompat.MEASURED_SIZE_MASK;
            this.r = ViewCompat.MEASURED_SIZE_MASK;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.j = 1;
            this.k = 0.0f;
            this.l = 1.0f;
            this.m = -1;
            this.n = -1.0f;
            this.o = -1;
            this.p = -1;
            this.q = ViewCompat.MEASURED_SIZE_MASK;
            this.r = ViewCompat.MEASURED_SIZE_MASK;
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public LayoutParams(Parcel parcel) {
            super(0, 0);
            boolean z2 = false;
            this.j = 1;
            this.k = 0.0f;
            this.l = 1.0f;
            this.m = -1;
            this.n = -1.0f;
            this.o = -1;
            this.p = -1;
            this.q = ViewCompat.MEASURED_SIZE_MASK;
            this.r = ViewCompat.MEASURED_SIZE_MASK;
            this.j = parcel.readInt();
            this.k = parcel.readFloat();
            this.l = parcel.readFloat();
            this.m = parcel.readInt();
            this.n = parcel.readFloat();
            this.o = parcel.readInt();
            this.p = parcel.readInt();
            this.q = parcel.readInt();
            this.r = parcel.readInt();
            this.f2958s = parcel.readByte() != 0 ? true : z2;
            ((ViewGroup.MarginLayoutParams) this).bottomMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).leftMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).rightMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).topMargin = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).height = parcel.readInt();
            ((ViewGroup.MarginLayoutParams) this).width = parcel.readInt();
        }
    }
}
