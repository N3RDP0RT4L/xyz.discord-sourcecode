package com.google.android.flexbox;
/* loaded from: classes3.dex */
public final class R {

    /* loaded from: classes3.dex */
    public static final class a {
        public static final int[] FlexboxLayout = {xyz.discord.R.attr.alignContent, xyz.discord.R.attr.alignItems, xyz.discord.R.attr.dividerDrawable, xyz.discord.R.attr.dividerDrawableHorizontal, xyz.discord.R.attr.dividerDrawableVertical, xyz.discord.R.attr.flexDirection, xyz.discord.R.attr.flexWrap, xyz.discord.R.attr.justifyContent, xyz.discord.R.attr.maxLine, xyz.discord.R.attr.showDivider, xyz.discord.R.attr.showDividerHorizontal, xyz.discord.R.attr.showDividerVertical};
        public static final int[] FlexboxLayout_Layout = {xyz.discord.R.attr.layout_alignSelf, xyz.discord.R.attr.layout_flexBasisPercent, xyz.discord.R.attr.layout_flexGrow, xyz.discord.R.attr.layout_flexShrink, xyz.discord.R.attr.layout_maxHeight, xyz.discord.R.attr.layout_maxWidth, xyz.discord.R.attr.layout_minHeight, xyz.discord.R.attr.layout_minWidth, xyz.discord.R.attr.layout_order, xyz.discord.R.attr.layout_wrapBefore};
        public static final int FlexboxLayout_Layout_layout_alignSelf = 0x00000000;
        public static final int FlexboxLayout_Layout_layout_flexBasisPercent = 0x00000001;
        public static final int FlexboxLayout_Layout_layout_flexGrow = 0x00000002;
        public static final int FlexboxLayout_Layout_layout_flexShrink = 0x00000003;
        public static final int FlexboxLayout_Layout_layout_maxHeight = 0x00000004;
        public static final int FlexboxLayout_Layout_layout_maxWidth = 0x00000005;
        public static final int FlexboxLayout_Layout_layout_minHeight = 0x00000006;
        public static final int FlexboxLayout_Layout_layout_minWidth = 0x00000007;
        public static final int FlexboxLayout_Layout_layout_order = 0x00000008;
        public static final int FlexboxLayout_Layout_layout_wrapBefore = 0x00000009;
        public static final int FlexboxLayout_alignContent = 0x00000000;
        public static final int FlexboxLayout_alignItems = 0x00000001;
        public static final int FlexboxLayout_dividerDrawable = 0x00000002;
        public static final int FlexboxLayout_dividerDrawableHorizontal = 0x00000003;
        public static final int FlexboxLayout_dividerDrawableVertical = 0x00000004;
        public static final int FlexboxLayout_flexDirection = 0x00000005;
        public static final int FlexboxLayout_flexWrap = 0x00000006;
        public static final int FlexboxLayout_justifyContent = 0x00000007;
        public static final int FlexboxLayout_maxLine = 0x00000008;
        public static final int FlexboxLayout_showDivider = 0x00000009;
        public static final int FlexboxLayout_showDividerHorizontal = 0x0000000a;
        public static final int FlexboxLayout_showDividerVertical = 0x0000000b;

        private a() {
        }
    }

    private R() {
    }
}
