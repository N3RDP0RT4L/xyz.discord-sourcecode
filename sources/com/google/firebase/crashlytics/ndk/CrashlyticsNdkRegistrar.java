package com.google.firebase.crashlytics.ndk;

import android.content.Context;
import b.i.c.l.d;
import b.i.c.l.e;
import b.i.c.l.f;
import b.i.c.l.g;
import b.i.c.l.o;
import b.i.c.m.d.a;
import com.google.firebase.crashlytics.ndk.CrashlyticsNdkRegistrar;
import com.google.firebase.crashlytics.ndk.JniNativeApi;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
/* loaded from: classes3.dex */
public class CrashlyticsNdkRegistrar implements g {
    @Override // b.i.c.l.g
    public List<d<?>> getComponents() {
        d.b a = d.a(a.class);
        a.a(new o(Context.class, 1, 0));
        a.c(new f(this) { // from class: b.i.c.m.e.b
            public final CrashlyticsNdkRegistrar a;

            {
                this.a = this;
            }

            @Override // b.i.c.l.f
            public Object a(e eVar) {
                Objects.requireNonNull(this.a);
                Context context = (Context) eVar.a(Context.class);
                return new c(new a(context, new JniNativeApi(context), new f(new File(context.getFilesDir(), ".com.google.firebase.crashlytics-ndk"))));
            }
        });
        a.d(2);
        return Arrays.asList(a.b(), b.i.a.f.e.o.f.N("fire-cls-ndk", "17.3.0"));
    }
}
