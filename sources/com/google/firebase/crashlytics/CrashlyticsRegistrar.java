package com.google.firebase.crashlytics;

import b.i.c.c;
import b.i.c.j.a.a;
import b.i.c.l.d;
import b.i.c.l.f;
import b.i.c.l.g;
import b.i.c.l.o;
import com.google.firebase.crashlytics.CrashlyticsRegistrar;
import java.util.Arrays;
import java.util.List;
/* loaded from: classes3.dex */
public class CrashlyticsRegistrar implements g {
    @Override // b.i.c.l.g
    public List<d<?>> getComponents() {
        d.b a = d.a(FirebaseCrashlytics.class);
        a.a(new o(c.class, 1, 0));
        a.a(new o(b.i.c.u.g.class, 1, 0));
        a.a(new o(a.class, 0, 0));
        a.a(new o(b.i.c.m.d.a.class, 0, 0));
        a.c(new f(this) { // from class: b.i.c.m.b
            public final CrashlyticsRegistrar a;

            {
                this.a = this;
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Removed duplicated region for block: B:39:0x026b  */
            /* JADX WARN: Removed duplicated region for block: B:65:0x03a0  */
            /* JADX WARN: Type inference failed for: r4v31, types: [b.i.c.m.d.i.b, b.i.c.m.d.i.c] */
            /* JADX WARN: Type inference failed for: r5v7, types: [b.i.c.m.d.i.e] */
            @Override // b.i.c.l.f
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public java.lang.Object a(b.i.c.l.e r44) {
                /*
                    Method dump skipped, instructions count: 934
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: b.i.c.m.b.a(b.i.c.l.e):java.lang.Object");
            }
        });
        a.d(2);
        return Arrays.asList(a.b(), b.i.a.f.e.o.f.N("fire-cls", "17.3.0"));
    }
}
