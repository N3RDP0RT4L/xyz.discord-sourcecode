package com.google.firebase.iid;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import b.i.a.f.e.o.f;
import b.i.c.c;
import b.i.c.r.d;
import b.i.c.s.j;
import b.i.c.s.n;
import b.i.c.s.o;
import b.i.c.s.p;
import b.i.c.s.q;
import b.i.c.s.u;
import b.i.c.s.w;
import b.i.c.s.x;
import b.i.c.t.a;
import b.i.c.u.g;
import b.i.c.x.h;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
/* compiled from: com.google.firebase:firebase-iid@@21.0.0 */
@Deprecated
/* loaded from: classes3.dex */
public class FirebaseInstanceId {

    /* renamed from: b  reason: collision with root package name */
    public static w f3083b;
    public static ScheduledExecutorService d;
    public final Executor e;
    public final c f;
    public final q g;
    public final n h;
    public final u i;
    public final g j;
    public boolean k = false;
    public static final long a = TimeUnit.HOURS.toSeconds(8);
    public static final Pattern c = Pattern.compile("\\AA[\\w-]{38}\\z");

    public FirebaseInstanceId(c cVar, a<h> aVar, a<d> aVar2, g gVar) {
        cVar.a();
        q qVar = new q(cVar.d);
        ExecutorService a2 = b.i.c.s.h.a();
        ExecutorService a3 = b.i.c.s.h.a();
        if (q.b(cVar) != null) {
            synchronized (FirebaseInstanceId.class) {
                if (f3083b == null) {
                    cVar.a();
                    f3083b = new w(cVar.d);
                }
            }
            this.f = cVar;
            this.g = qVar;
            this.h = new n(cVar, qVar, aVar, aVar2, gVar);
            this.e = a3;
            this.i = new u(a2);
            this.j = gVar;
            return;
        }
        throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
    }

    public static <T> T a(@NonNull Task<T> task) throws InterruptedException {
        b.c.a.a0.d.z(task, "Task must not be null");
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        task.c(j.j, new b.i.a.f.n.c(countDownLatch) { // from class: b.i.c.s.k
            public final CountDownLatch a;

            {
                this.a = countDownLatch;
            }

            @Override // b.i.a.f.n.c
            public final void onComplete(Task task2) {
                CountDownLatch countDownLatch2 = this.a;
                w wVar = FirebaseInstanceId.f3083b;
                countDownLatch2.countDown();
            }
        });
        countDownLatch.await(30000L, TimeUnit.MILLISECONDS);
        if (task.p()) {
            return task.l();
        }
        if (task.n()) {
            throw new CancellationException("Task is already canceled");
        } else if (task.o()) {
            throw new IllegalStateException(task.k());
        } else {
            throw new IllegalThreadStateException("Firebase Installations getId Task has timed out.");
        }
    }

    public static void c(@NonNull c cVar) {
        cVar.a();
        b.c.a.a0.d.v(cVar.f.g, "Please set your project ID. A valid Firebase project ID is required to communicate with Firebase server APIs: It identifies your project with Google.");
        cVar.a();
        b.c.a.a0.d.v(cVar.f.f1651b, "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.");
        cVar.a();
        b.c.a.a0.d.v(cVar.f.a, "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.");
        cVar.a();
        b.c.a.a0.d.o(cVar.f.f1651b.contains(":"), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        cVar.a();
        b.c.a.a0.d.o(c.matcher(cVar.f.a).matches(), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
    }

    @NonNull
    @Keep
    public static FirebaseInstanceId getInstance(@NonNull c cVar) {
        c(cVar);
        cVar.a();
        FirebaseInstanceId firebaseInstanceId = (FirebaseInstanceId) cVar.g.a(FirebaseInstanceId.class);
        b.c.a.a0.d.z(firebaseInstanceId, "Firebase Instance ID component is not present");
        return firebaseInstanceId;
    }

    public static boolean l() {
        if (!Log.isLoggable("FirebaseInstanceId", 3)) {
            return Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseInstanceId", 3);
        }
        return true;
    }

    public String b() throws IOException {
        String b2 = q.b(this.f);
        c(this.f);
        if (Looper.getMainLooper() != Looper.myLooper()) {
            try {
                return ((o) f.k(g(b2, "*"), 30000L, TimeUnit.MILLISECONDS)).a();
            } catch (InterruptedException | TimeoutException unused) {
                throw new IOException("SERVICE_NOT_AVAILABLE");
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause instanceof IOException) {
                    if ("INSTANCE_ID_RESET".equals(cause.getMessage())) {
                        synchronized (this) {
                            f3083b.c();
                        }
                    }
                    throw ((IOException) cause);
                } else if (cause instanceof RuntimeException) {
                    throw ((RuntimeException) cause);
                } else {
                    throw new IOException(e);
                }
            }
        } else {
            throw new IOException("MAIN_THREAD");
        }
    }

    public void d(Runnable runnable, long j) {
        synchronized (FirebaseInstanceId.class) {
            if (d == null) {
                d = new ScheduledThreadPoolExecutor(1, new b.i.a.f.e.o.j.a("FirebaseInstanceId"));
            }
            d.schedule(runnable, j, TimeUnit.SECONDS);
        }
    }

    public String e() {
        try {
            w wVar = f3083b;
            String c2 = this.f.c();
            synchronized (wVar) {
                wVar.c.put(c2, Long.valueOf(wVar.d(c2)));
            }
            return (String) a(this.j.getId());
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    @NonNull
    @Deprecated
    public Task<o> f() {
        c(this.f);
        return g(q.b(this.f), "*");
    }

    public final Task<o> g(final String str, final String str2) {
        if (str2.isEmpty() || str2.equalsIgnoreCase("fcm") || str2.equalsIgnoreCase("gcm")) {
            str2 = "*";
        }
        return f.Z(null).j(this.e, new b.i.a.f.n.a(this, str, str2) { // from class: b.i.c.s.i
            public final FirebaseInstanceId a;

            /* renamed from: b  reason: collision with root package name */
            public final String f1763b;
            public final String c;

            {
                this.a = this;
                this.f1763b = str;
                this.c = str2;
            }

            @Override // b.i.a.f.n.a
            public final Object a(Task task) {
                return this.a.m(this.f1763b, this.c);
            }
        });
    }

    public final String h() {
        c cVar = this.f;
        cVar.a();
        return "[DEFAULT]".equals(cVar.e) ? "" : this.f.c();
    }

    @Nullable
    @Deprecated
    public String i() {
        c(this.f);
        w.a j = j();
        if (p(j)) {
            synchronized (this) {
                if (!this.k) {
                    o(0L);
                }
            }
        }
        int i = w.a.f1772b;
        if (j == null) {
            return null;
        }
        return j.c;
    }

    @Nullable
    public w.a j() {
        return k(q.b(this.f), "*");
    }

    @Nullable
    public w.a k(String str, String str2) {
        w.a b2;
        w wVar = f3083b;
        String h = h();
        synchronized (wVar) {
            b2 = w.a.b(wVar.a.getString(wVar.b(h, str, str2), null));
        }
        return b2;
    }

    public final Task m(final String str, final String str2) throws Exception {
        Task<o> task;
        final String e = e();
        w.a k = k(str, str2);
        if (!p(k)) {
            return f.Z(new p(e, k.c));
        }
        final u uVar = this.i;
        synchronized (uVar) {
            final Pair<String, String> pair = new Pair<>(str, str2);
            task = uVar.f1769b.get(pair);
            if (task == null) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(pair);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 24);
                    sb.append("Making new request for: ");
                    sb.append(valueOf);
                    Log.d("FirebaseInstanceId", sb.toString());
                }
                n nVar = this.h;
                Objects.requireNonNull(nVar);
                task = nVar.a(nVar.b(e, str, str2, new Bundle())).r(this.e, new b.i.a.f.n.f(this, str, str2, e) { // from class: b.i.c.s.l
                    public final FirebaseInstanceId a;

                    /* renamed from: b  reason: collision with root package name */
                    public final String f1764b;
                    public final String c;
                    public final String d;

                    {
                        this.a = this;
                        this.f1764b = str;
                        this.c = str2;
                        this.d = e;
                    }

                    @Override // b.i.a.f.n.f
                    public final Task a(Object obj) {
                        FirebaseInstanceId firebaseInstanceId = this.a;
                        String str3 = this.f1764b;
                        String str4 = this.c;
                        String str5 = this.d;
                        String str6 = (String) obj;
                        w wVar = FirebaseInstanceId.f3083b;
                        String h = firebaseInstanceId.h();
                        String a2 = firebaseInstanceId.g.a();
                        synchronized (wVar) {
                            String a3 = w.a.a(str6, a2, System.currentTimeMillis());
                            if (a3 != null) {
                                SharedPreferences.Editor edit = wVar.a.edit();
                                edit.putString(wVar.b(h, str3, str4), a3);
                                edit.commit();
                            }
                        }
                        return f.Z(new p(str5, str6));
                    }
                }).j(uVar.a, new b.i.a.f.n.a(uVar, pair) { // from class: b.i.c.s.t
                    public final u a;

                    /* renamed from: b  reason: collision with root package name */
                    public final Pair f1768b;

                    {
                        this.a = uVar;
                        this.f1768b = pair;
                    }

                    @Override // b.i.a.f.n.a
                    public final Object a(Task task2) {
                        u uVar2 = this.a;
                        Pair pair2 = this.f1768b;
                        synchronized (uVar2) {
                            uVar2.f1769b.remove(pair2);
                        }
                        return task2;
                    }
                });
                uVar.f1769b.put(pair, task);
            } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf2 = String.valueOf(pair);
                StringBuilder sb2 = new StringBuilder(valueOf2.length() + 29);
                sb2.append("Joining ongoing request for: ");
                sb2.append(valueOf2);
                Log.d("FirebaseInstanceId", sb2.toString());
            }
        }
        return task;
    }

    public synchronized void n(boolean z2) {
        this.k = z2;
    }

    public synchronized void o(long j) {
        d(new x(this, Math.min(Math.max(30L, j << 1), a)), j);
        this.k = true;
    }

    public boolean p(@Nullable w.a aVar) {
        if (aVar != null) {
            if (!(System.currentTimeMillis() > aVar.e + w.a.a || !this.g.a().equals(aVar.d))) {
                return false;
            }
        }
        return true;
    }
}
