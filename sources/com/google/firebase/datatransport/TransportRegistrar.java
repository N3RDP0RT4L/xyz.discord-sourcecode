package com.google.firebase.datatransport;

import android.content.Context;
import androidx.annotation.Keep;
import b.i.c.l.d;
import b.i.c.l.g;
import b.i.c.l.o;
import b.i.c.n.a;
import java.util.Collections;
import java.util.List;
/* compiled from: com.google.firebase:firebase-datatransport@@17.0.3 */
@Keep
/* loaded from: classes3.dex */
public class TransportRegistrar implements g {
    @Override // b.i.c.l.g
    public List<d<?>> getComponents() {
        d.b a = d.a(b.i.a.b.g.class);
        a.a(new o(Context.class, 1, 0));
        a.c(a.a);
        return Collections.singletonList(a.b());
    }
}
