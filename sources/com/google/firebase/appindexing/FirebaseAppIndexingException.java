package com.google.firebase.appindexing;

import androidx.annotation.NonNull;
import com.google.firebase.FirebaseException;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public class FirebaseAppIndexingException extends FirebaseException {
    public FirebaseAppIndexingException(@NonNull String str) {
        super(str);
    }
}
