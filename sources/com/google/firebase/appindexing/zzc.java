package com.google.firebase.appindexing;

import androidx.annotation.NonNull;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class zzc extends FirebaseAppIndexingInvalidArgumentException {
    public zzc(@NonNull String str) {
        super(str);
    }
}
