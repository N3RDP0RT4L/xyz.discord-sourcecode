package com.google.firebase.appindexing;

import androidx.annotation.NonNull;
/* compiled from: com.google.firebase:firebase-appindexing@@19.1.0 */
/* loaded from: classes3.dex */
public final class zze extends FirebaseAppIndexingInvalidArgumentException {
    public zze(@NonNull String str) {
        super(str);
    }
}
