package com.google.firebase;

import androidx.annotation.RecentlyNonNull;
import b.c.a.a0.d;
/* compiled from: com.google.android.gms:play-services-basement@@17.6.0 */
/* loaded from: classes3.dex */
public class FirebaseException extends Exception {
    @Deprecated
    public FirebaseException() {
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FirebaseException(@RecentlyNonNull String str) {
        super(str);
        d.v(str, "Detail message must not be empty");
    }
}
