package com.google.firebase.ktx;

import andhook.lib.HookHelper;
import androidx.annotation.Keep;
import b.i.a.f.e.o.f;
import b.i.c.l.d;
import b.i.c.l.g;
import d0.t.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Firebase.kt */
@Keep
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0006\u0010\u0007J\u0019\u0010\u0004\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002H\u0016¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/google/firebase/ktx/FirebaseCommonKtxRegistrar;", "Lb/i/c/l/g;", "", "Lb/i/c/l/d;", "getComponents", "()Ljava/util/List;", HookHelper.constructorName, "()V", "com.google.firebase-firebase-common-ktx"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public final class FirebaseCommonKtxRegistrar implements g {
    @Override // b.i.c.l.g
    public List<d<?>> getComponents() {
        return m.listOf(f.N("fire-core-ktx", "19.4.0"));
    }
}
