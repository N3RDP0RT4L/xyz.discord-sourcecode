package com.google.firebase.messaging;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import b.i.a.b.g;
import b.i.a.f.e.o.f;
import b.i.a.f.n.c0;
import b.i.a.f.n.e;
import b.i.a.f.n.v;
import b.i.c.c;
import b.i.c.q.a;
import b.i.c.q.b;
import b.i.c.q.d;
import b.i.c.s.n;
import b.i.c.s.q;
import b.i.c.w.y;
import b.i.c.x.h;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
/* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
/* loaded from: classes3.dex */
public class FirebaseMessaging {
    @Nullable
    @SuppressLint({"FirebaseUnknownNullness"})
    @VisibleForTesting
    public static g a;

    /* renamed from: b  reason: collision with root package name */
    public final Context f3084b;
    public final c c;
    public final FirebaseInstanceId d;
    public final a e;
    public final Executor f;
    public final Task<y> g;

    /* compiled from: com.google.firebase:firebase-messaging@@21.0.0 */
    /* loaded from: classes3.dex */
    public class a {
        public final d a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f3085b;
        @Nullable
        public b<b.i.c.a> c;
        @Nullable
        public Boolean d;

        public a(d dVar) {
            this.a = dVar;
        }

        public synchronized void a() {
            if (!this.f3085b) {
                Boolean c = c();
                this.d = c;
                if (c == null) {
                    b<b.i.c.a> jVar = new b(this) { // from class: b.i.c.w.j
                        public final FirebaseMessaging.a a;

                        {
                            this.a = this;
                        }

                        @Override // b.i.c.q.b
                        public final void a(a aVar) {
                            final FirebaseMessaging.a aVar2 = this.a;
                            if (aVar2.b()) {
                                FirebaseMessaging.this.f.execute(new Runnable(aVar2) { // from class: b.i.c.w.k
                                    public final FirebaseMessaging.a j;

                                    {
                                        this.j = aVar2;
                                    }

                                    @Override // java.lang.Runnable
                                    public final void run() {
                                        FirebaseMessaging.this.d.i();
                                    }
                                });
                            }
                        }
                    };
                    this.c = jVar;
                    this.a.a(b.i.c.a.class, jVar);
                }
                this.f3085b = true;
            }
        }

        public synchronized boolean b() {
            a();
            Boolean bool = this.d;
            if (bool != null) {
                return bool.booleanValue();
            }
            return FirebaseMessaging.this.c.g();
        }

        @Nullable
        public final Boolean c() {
            ApplicationInfo applicationInfo;
            Bundle bundle;
            c cVar = FirebaseMessaging.this.c;
            cVar.a();
            Context context = cVar.d;
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("auto_init")) {
                return Boolean.valueOf(sharedPreferences.getBoolean("auto_init", false));
            }
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager == null || (applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128)) == null || (bundle = applicationInfo.metaData) == null || !bundle.containsKey("firebase_messaging_auto_init_enabled")) {
                    return null;
                }
                return Boolean.valueOf(applicationInfo.metaData.getBoolean("firebase_messaging_auto_init_enabled"));
            } catch (PackageManager.NameNotFoundException unused) {
                return null;
            }
        }
    }

    public FirebaseMessaging(c cVar, final FirebaseInstanceId firebaseInstanceId, b.i.c.t.a<h> aVar, b.i.c.t.a<b.i.c.r.d> aVar2, b.i.c.u.g gVar, @Nullable g gVar2, d dVar) {
        try {
            Class.forName("com.google.firebase.iid.FirebaseInstanceIdReceiver");
            a = gVar2;
            this.c = cVar;
            this.d = firebaseInstanceId;
            this.e = new a(dVar);
            cVar.a();
            final Context context = cVar.d;
            this.f3084b = context;
            ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1, new b.i.a.f.e.o.j.a("Firebase-Messaging-Init"));
            this.f = scheduledThreadPoolExecutor;
            scheduledThreadPoolExecutor.execute(new Runnable(this, firebaseInstanceId) { // from class: b.i.c.w.g
                public final FirebaseMessaging j;
                public final FirebaseInstanceId k;

                {
                    this.j = this;
                    this.k = firebaseInstanceId;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    FirebaseMessaging firebaseMessaging = this.j;
                    FirebaseInstanceId firebaseInstanceId2 = this.k;
                    if (firebaseMessaging.e.b()) {
                        firebaseInstanceId2.i();
                    }
                }
            });
            final q qVar = new q(context);
            final ScheduledThreadPoolExecutor scheduledThreadPoolExecutor2 = new ScheduledThreadPoolExecutor(1, new b.i.a.f.e.o.j.a("Firebase-Messaging-Topics-Io"));
            int i = y.f1796b;
            final n nVar = new n(cVar, qVar, aVar, aVar2, gVar);
            Task<y> o = f.o(scheduledThreadPoolExecutor2, new Callable(context, scheduledThreadPoolExecutor2, firebaseInstanceId, qVar, nVar) { // from class: b.i.c.w.x
                public final Context j;
                public final ScheduledExecutorService k;
                public final FirebaseInstanceId l;
                public final q m;
                public final n n;

                {
                    this.j = context;
                    this.k = scheduledThreadPoolExecutor2;
                    this.l = firebaseInstanceId;
                    this.m = qVar;
                    this.n = nVar;
                }

                @Override // java.util.concurrent.Callable
                public final Object call() {
                    w wVar;
                    Context context2 = this.j;
                    ScheduledExecutorService scheduledExecutorService = this.k;
                    FirebaseInstanceId firebaseInstanceId2 = this.l;
                    q qVar2 = this.m;
                    n nVar2 = this.n;
                    synchronized (w.class) {
                        wVar = null;
                        WeakReference<w> weakReference = w.a;
                        if (weakReference != null) {
                            wVar = weakReference.get();
                        }
                        if (wVar == null) {
                            SharedPreferences sharedPreferences = context2.getSharedPreferences("com.google.android.gms.appid", 0);
                            w wVar2 = new w(sharedPreferences, scheduledExecutorService);
                            synchronized (wVar2) {
                                wVar2.c = u.a(sharedPreferences, "topic_operation_queue", ",", scheduledExecutorService);
                            }
                            w.a = new WeakReference<>(wVar2);
                            wVar = wVar2;
                        }
                    }
                    return new y(firebaseInstanceId2, qVar2, wVar, nVar2, context2, scheduledExecutorService);
                }
            });
            this.g = o;
            c0 c0Var = (c0) o;
            c0Var.f1588b.a(new v(new ThreadPoolExecutor(0, 1, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue(), new b.i.a.f.e.o.j.a("Firebase-Messaging-Trigger-Topics-Io")), new e(this) { // from class: b.i.c.w.h
                public final FirebaseMessaging a;

                {
                    this.a = this;
                }

                @Override // b.i.a.f.n.e
                public final void onSuccess(Object obj) {
                    boolean z2;
                    y yVar = (y) obj;
                    if (this.a.e.b()) {
                        if (yVar.j.a() != null) {
                            synchronized (yVar) {
                                z2 = yVar.i;
                            }
                            if (!z2) {
                                yVar.g(0L);
                            }
                        }
                    }
                }
            }));
            c0Var.w();
        } catch (ClassNotFoundException unused) {
            throw new IllegalStateException("FirebaseMessaging and FirebaseInstanceId versions not compatible. Update to latest version of firebase-messaging.");
        }
    }

    @NonNull
    @Keep
    public static synchronized FirebaseMessaging getInstance(@NonNull c cVar) {
        FirebaseMessaging firebaseMessaging;
        synchronized (FirebaseMessaging.class) {
            cVar.a();
            firebaseMessaging = (FirebaseMessaging) cVar.g.a(FirebaseMessaging.class);
            b.c.a.a0.d.z(firebaseMessaging, "Firebase Messaging component is not present");
        }
        return firebaseMessaging;
    }
}
