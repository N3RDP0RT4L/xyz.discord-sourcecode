package com.google.gson.internal.bind;

import b.d.b.a.a;
import b.i.d.j;
import b.i.d.k;
import b.i.d.o;
import b.i.d.q.g;
import b.i.d.q.p;
import b.i.d.q.r;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
/* loaded from: classes3.dex */
public final class MapTypeAdapterFactory implements o {
    public final g j;
    public final boolean k;

    /* loaded from: classes3.dex */
    public final class Adapter<K, V> extends TypeAdapter<Map<K, V>> {
        public final TypeAdapter<K> a;

        /* renamed from: b  reason: collision with root package name */
        public final TypeAdapter<V> f3092b;
        public final r<? extends Map<K, V>> c;

        public Adapter(Gson gson, Type type, TypeAdapter<K> typeAdapter, Type type2, TypeAdapter<V> typeAdapter2, r<? extends Map<K, V>> rVar) {
            this.a = new TypeAdapterRuntimeTypeWrapper(gson, typeAdapter, type);
            this.f3092b = new TypeAdapterRuntimeTypeWrapper(gson, typeAdapter2, type2);
            this.c = rVar;
        }

        @Override // com.google.gson.TypeAdapter
        public Object read(JsonReader jsonReader) throws IOException {
            JsonToken N = jsonReader.N();
            if (N == JsonToken.NULL) {
                jsonReader.H();
                return null;
            }
            Map<K, V> a = this.c.a();
            if (N == JsonToken.BEGIN_ARRAY) {
                jsonReader.a();
                while (jsonReader.q()) {
                    jsonReader.a();
                    K read = this.a.read(jsonReader);
                    if (a.put(read, this.f3092b.read(jsonReader)) == null) {
                        jsonReader.e();
                    } else {
                        throw new JsonSyntaxException(a.u("duplicate key: ", read));
                    }
                }
                jsonReader.e();
            } else {
                jsonReader.b();
                while (jsonReader.q()) {
                    Objects.requireNonNull((JsonReader.a) p.a);
                    if (jsonReader instanceof b.i.d.q.x.a) {
                        b.i.d.q.x.a aVar = (b.i.d.q.x.a) jsonReader;
                        aVar.W(JsonToken.NAME);
                        Map.Entry entry = (Map.Entry) ((Iterator) aVar.X()).next();
                        aVar.c0(entry.getValue());
                        aVar.c0(new k((String) entry.getKey()));
                    } else {
                        int i = jsonReader.r;
                        if (i == 0) {
                            i = jsonReader.d();
                        }
                        if (i == 13) {
                            jsonReader.r = 9;
                        } else if (i == 12) {
                            jsonReader.r = 8;
                        } else if (i == 14) {
                            jsonReader.r = 10;
                        } else {
                            StringBuilder R = a.R("Expected a name but was ");
                            R.append(jsonReader.N());
                            R.append(jsonReader.t());
                            throw new IllegalStateException(R.toString());
                        }
                    }
                    K read2 = this.a.read(jsonReader);
                    if (a.put(read2, this.f3092b.read(jsonReader)) != null) {
                        throw new JsonSyntaxException(a.u("duplicate key: ", read2));
                    }
                }
                jsonReader.f();
            }
            return a;
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, Object obj) throws IOException {
            String str;
            Map map = (Map) obj;
            if (map == null) {
                jsonWriter.s();
            } else if (!MapTypeAdapterFactory.this.k) {
                jsonWriter.c();
                for (Map.Entry<K, V> entry : map.entrySet()) {
                    jsonWriter.n(String.valueOf(entry.getKey()));
                    this.f3092b.write(jsonWriter, entry.getValue());
                }
                jsonWriter.f();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                int i = 0;
                boolean z2 = false;
                for (Map.Entry<K, V> entry2 : map.entrySet()) {
                    JsonElement jsonTree = this.a.toJsonTree(entry2.getKey());
                    arrayList.add(jsonTree);
                    arrayList2.add(entry2.getValue());
                    Objects.requireNonNull(jsonTree);
                    z2 |= (jsonTree instanceof b.i.d.g) || (jsonTree instanceof JsonObject);
                }
                if (z2) {
                    jsonWriter.b();
                    int size = arrayList.size();
                    while (i < size) {
                        jsonWriter.b();
                        TypeAdapters.X.write(jsonWriter, (JsonElement) arrayList.get(i));
                        this.f3092b.write(jsonWriter, arrayList2.get(i));
                        jsonWriter.e();
                        i++;
                    }
                    jsonWriter.e();
                    return;
                }
                jsonWriter.c();
                int size2 = arrayList.size();
                while (i < size2) {
                    JsonElement jsonElement = (JsonElement) arrayList.get(i);
                    Objects.requireNonNull(jsonElement);
                    if (jsonElement instanceof k) {
                        k e = jsonElement.e();
                        Object obj2 = e.a;
                        if (obj2 instanceof Number) {
                            str = String.valueOf(e.i());
                        } else if (obj2 instanceof Boolean) {
                            str = Boolean.toString(e.h());
                        } else if (obj2 instanceof String) {
                            str = e.g();
                        } else {
                            throw new AssertionError();
                        }
                    } else if (jsonElement instanceof j) {
                        str = "null";
                    } else {
                        throw new AssertionError();
                    }
                    jsonWriter.n(str);
                    this.f3092b.write(jsonWriter, arrayList2.get(i));
                    i++;
                }
                jsonWriter.f();
            }
        }
    }

    public MapTypeAdapterFactory(g gVar, boolean z2) {
        this.j = gVar;
        this.k = z2;
    }

    @Override // b.i.d.o
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        Type[] typeArr;
        TypeAdapter<Boolean> typeAdapter;
        Type type = typeToken.getType();
        if (!Map.class.isAssignableFrom(typeToken.getRawType())) {
            return null;
        }
        Class<?> e = b.i.d.q.a.e(type);
        if (type == Properties.class) {
            typeArr = new Type[]{String.class, String.class};
        } else {
            Type f = b.i.d.q.a.f(type, e, Map.class);
            typeArr = f instanceof ParameterizedType ? ((ParameterizedType) f).getActualTypeArguments() : new Type[]{Object.class, Object.class};
        }
        Type type2 = typeArr[0];
        if (type2 == Boolean.TYPE || type2 == Boolean.class) {
            typeAdapter = TypeAdapters.f;
        } else {
            typeAdapter = gson.h(TypeToken.get(type2));
        }
        return new Adapter(gson, typeArr[0], typeAdapter, typeArr[1], gson.h(TypeToken.get(typeArr[1])), this.j.a(typeToken));
    }
}
