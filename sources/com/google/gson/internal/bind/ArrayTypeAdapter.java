package com.google.gson.internal.bind;

import b.i.d.o;
import b.i.d.q.a;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;
/* loaded from: classes3.dex */
public final class ArrayTypeAdapter<E> extends TypeAdapter<Object> {
    public static final o a = new o() { // from class: com.google.gson.internal.bind.ArrayTypeAdapter.1
        @Override // b.i.d.o
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            Type type;
            Type type2 = typeToken.getType();
            boolean z2 = type2 instanceof GenericArrayType;
            if (!z2 && (!(type2 instanceof Class) || !((Class) type2).isArray())) {
                return null;
            }
            if (z2) {
                type = ((GenericArrayType) type2).getGenericComponentType();
            } else {
                type = ((Class) type2).getComponentType();
            }
            return new ArrayTypeAdapter(gson, gson.h(TypeToken.get(type)), a.e(type));
        }
    };

    /* renamed from: b  reason: collision with root package name */
    public final Class<E> f3089b;
    public final TypeAdapter<E> c;

    public ArrayTypeAdapter(Gson gson, TypeAdapter<E> typeAdapter, Class<E> cls) {
        this.c = new TypeAdapterRuntimeTypeWrapper(gson, typeAdapter, cls);
        this.f3089b = cls;
    }

    @Override // com.google.gson.TypeAdapter
    public Object read(JsonReader jsonReader) throws IOException {
        if (jsonReader.N() == JsonToken.NULL) {
            jsonReader.H();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        jsonReader.a();
        while (jsonReader.q()) {
            arrayList.add(this.c.read(jsonReader));
        }
        jsonReader.e();
        int size = arrayList.size();
        Object newInstance = Array.newInstance((Class<?>) this.f3089b, size);
        for (int i = 0; i < size; i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, Object obj) throws IOException {
        if (obj == null) {
            jsonWriter.s();
            return;
        }
        jsonWriter.b();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.c.write(jsonWriter, Array.get(obj, i));
        }
        jsonWriter.e();
    }
}
