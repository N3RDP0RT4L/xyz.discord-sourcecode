package com.google.gson.internal.bind;

import b.i.a.f.e.o.f;
import b.i.d.o;
import b.i.d.q.x.d.a;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
/* loaded from: classes3.dex */
public final class DateTypeAdapter extends TypeAdapter<Date> {
    public static final o a = new o() { // from class: com.google.gson.internal.bind.DateTypeAdapter.1
        @Override // b.i.d.o
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            if (typeToken.getRawType() == Date.class) {
                return new DateTypeAdapter();
            }
            return null;
        }
    };

    /* renamed from: b  reason: collision with root package name */
    public final List<DateFormat> f3091b;

    public DateTypeAdapter() {
        ArrayList arrayList = new ArrayList();
        this.f3091b = arrayList;
        Locale locale = Locale.US;
        arrayList.add(DateFormat.getDateTimeInstance(2, 2, locale));
        if (!Locale.getDefault().equals(locale)) {
            arrayList.add(DateFormat.getDateTimeInstance(2, 2));
        }
        if (b.i.d.q.o.a >= 9) {
            arrayList.add(f.r0(2, 2));
        }
    }

    @Override // com.google.gson.TypeAdapter
    public Date read(JsonReader jsonReader) throws IOException {
        if (jsonReader.N() == JsonToken.NULL) {
            jsonReader.H();
            return null;
        }
        String J = jsonReader.J();
        synchronized (this) {
            for (DateFormat dateFormat : this.f3091b) {
                try {
                    return dateFormat.parse(J);
                } catch (ParseException unused) {
                }
            }
            try {
                return a.b(J, new ParsePosition(0));
            } catch (ParseException e) {
                throw new JsonSyntaxException(J, e);
            }
        }
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, Date date) throws IOException {
        Date date2 = date;
        synchronized (this) {
            if (date2 == null) {
                jsonWriter.s();
                return;
            }
            jsonWriter.H(this.f3091b.get(0).format(date2));
        }
    }
}
