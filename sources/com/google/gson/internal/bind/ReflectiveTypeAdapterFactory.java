package com.google.gson.internal.bind;

import b.i.d.d;
import b.i.d.o;
import b.i.d.p.c;
import b.i.d.q.g;
import b.i.d.q.r;
import b.i.d.q.y.b;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Excluder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
/* loaded from: classes3.dex */
public final class ReflectiveTypeAdapterFactory implements o {
    public final g j;
    public final d k;
    public final Excluder l;
    public final JsonAdapterAnnotationTypeAdapterFactory m;
    public final b n = b.a;

    /* loaded from: classes3.dex */
    public static final class Adapter<T> extends TypeAdapter<T> {
        public final r<T> a;

        /* renamed from: b  reason: collision with root package name */
        public final Map<String, a> f3094b;

        public Adapter(r<T> rVar, Map<String, a> map) {
            this.a = rVar;
            this.f3094b = map;
        }

        @Override // com.google.gson.TypeAdapter
        public T read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() == JsonToken.NULL) {
                jsonReader.H();
                return null;
            }
            T a = this.a.a();
            try {
                jsonReader.b();
                while (jsonReader.q()) {
                    a aVar = this.f3094b.get(jsonReader.C());
                    if (aVar != null && aVar.c) {
                        aVar.a(jsonReader, a);
                    }
                    jsonReader.U();
                }
                jsonReader.f();
                return a;
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            } catch (IllegalStateException e2) {
                throw new JsonSyntaxException(e2);
            }
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, T t) throws IOException {
            if (t == null) {
                jsonWriter.s();
                return;
            }
            jsonWriter.c();
            try {
                for (a aVar : this.f3094b.values()) {
                    if (aVar.c(t)) {
                        jsonWriter.n(aVar.a);
                        aVar.b(jsonWriter, t);
                    }
                }
                jsonWriter.f();
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }

    /* loaded from: classes3.dex */
    public static abstract class a {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f3095b;
        public final boolean c;

        public a(String str, boolean z2, boolean z3) {
            this.a = str;
            this.f3095b = z2;
            this.c = z3;
        }

        public abstract void a(JsonReader jsonReader, Object obj) throws IOException, IllegalAccessException;

        public abstract void b(JsonWriter jsonWriter, Object obj) throws IOException, IllegalAccessException;

        public abstract boolean c(Object obj) throws IOException, IllegalAccessException;
    }

    public ReflectiveTypeAdapterFactory(g gVar, d dVar, Excluder excluder, JsonAdapterAnnotationTypeAdapterFactory jsonAdapterAnnotationTypeAdapterFactory) {
        this.j = gVar;
        this.k = dVar;
        this.l = excluder;
        this.m = jsonAdapterAnnotationTypeAdapterFactory;
    }

    public boolean a(Field field, boolean z2) {
        boolean z3;
        Excluder excluder = this.l;
        Class<?> type = field.getType();
        if (!(excluder.b(type) || excluder.f(type, z2))) {
            if ((excluder.l & field.getModifiers()) != 0 || (!(excluder.k == -1.0d || excluder.i((c) field.getAnnotation(c.class), (b.i.d.p.d) field.getAnnotation(b.i.d.p.d.class))) || field.isSynthetic() || ((!excluder.m && excluder.h(field.getType())) || excluder.g(field.getType())))) {
                z3 = true;
                break;
            }
            List<b.i.d.a> list = z2 ? excluder.n : excluder.o;
            if (!list.isEmpty()) {
                b.i.d.b bVar = new b.i.d.b(field);
                for (b.i.d.a aVar : list) {
                    if (aVar.a(bVar)) {
                        z3 = true;
                        break;
                    }
                }
            }
            z3 = false;
            if (!z3) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARN: Removed duplicated region for block: B:29:0x00cd  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x01a1 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:64:0x0189 A[SYNTHETIC] */
    @Override // b.i.d.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public <T> com.google.gson.TypeAdapter<T> create(com.google.gson.Gson r36, com.google.gson.reflect.TypeToken<T> r37) {
        /*
            Method dump skipped, instructions count: 492
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.internal.bind.ReflectiveTypeAdapterFactory.create(com.google.gson.Gson, com.google.gson.reflect.TypeToken):com.google.gson.TypeAdapter");
    }
}
