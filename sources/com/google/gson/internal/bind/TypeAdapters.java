package com.google.gson.internal.bind;

import b.d.b.a.a;
import b.i.d.g;
import b.i.d.j;
import b.i.d.k;
import b.i.d.o;
import b.i.d.p.b;
import b.i.d.q.q;
import com.google.android.material.badge.BadgeDrawable;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
/* loaded from: classes3.dex */
public final class TypeAdapters {
    public static final TypeAdapter<String> A;
    public static final o D;
    public static final TypeAdapter<StringBuilder> E;
    public static final o F;
    public static final TypeAdapter<StringBuffer> G;
    public static final o H;
    public static final TypeAdapter<URL> I;
    public static final o J;
    public static final TypeAdapter<URI> K;
    public static final o L;
    public static final TypeAdapter<InetAddress> M;
    public static final o N;
    public static final TypeAdapter<UUID> O;
    public static final o P;
    public static final TypeAdapter<Currency> Q;
    public static final o R;
    public static final TypeAdapter<Calendar> T;
    public static final o U;
    public static final TypeAdapter<Locale> V;
    public static final o W;
    public static final TypeAdapter<JsonElement> X;
    public static final o Y;
    public static final TypeAdapter<Class> a;

    /* renamed from: b  reason: collision with root package name */
    public static final o f3100b;
    public static final TypeAdapter<BitSet> c;
    public static final o d;
    public static final TypeAdapter<Boolean> e;
    public static final o g;
    public static final TypeAdapter<Number> h;
    public static final o i;
    public static final TypeAdapter<Number> j;
    public static final o k;
    public static final TypeAdapter<Number> l;
    public static final o m;
    public static final TypeAdapter<AtomicInteger> n;
    public static final o o;
    public static final TypeAdapter<AtomicBoolean> p;
    public static final o q;
    public static final TypeAdapter<AtomicIntegerArray> r;

    /* renamed from: s  reason: collision with root package name */
    public static final o f3101s;
    public static final TypeAdapter<Number> w;

    /* renamed from: x  reason: collision with root package name */
    public static final o f3102x;

    /* renamed from: y  reason: collision with root package name */
    public static final TypeAdapter<Character> f3103y;

    /* renamed from: z  reason: collision with root package name */
    public static final o f3104z;
    public static final TypeAdapter<Boolean> f = new TypeAdapter<Boolean>() { // from class: com.google.gson.internal.bind.TypeAdapters.4
        @Override // com.google.gson.TypeAdapter
        public Boolean read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() != JsonToken.NULL) {
                return Boolean.valueOf(jsonReader.J());
            }
            jsonReader.H();
            return null;
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, Boolean bool) throws IOException {
            Boolean bool2 = bool;
            jsonWriter.H(bool2 == null ? "null" : bool2.toString());
        }
    };
    public static final TypeAdapter<Number> t = new TypeAdapter<Number>() { // from class: com.google.gson.internal.bind.TypeAdapters.11
        @Override // com.google.gson.TypeAdapter
        public Number read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() == JsonToken.NULL) {
                jsonReader.H();
                return null;
            }
            try {
                return Long.valueOf(jsonReader.A());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, Number number) throws IOException {
            jsonWriter.D(number);
        }
    };
    public static final TypeAdapter<Number> u = new TypeAdapter<Number>() { // from class: com.google.gson.internal.bind.TypeAdapters.12
        @Override // com.google.gson.TypeAdapter
        public Number read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() != JsonToken.NULL) {
                return Float.valueOf((float) jsonReader.x());
            }
            jsonReader.H();
            return null;
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, Number number) throws IOException {
            jsonWriter.D(number);
        }
    };
    public static final TypeAdapter<Number> v = new TypeAdapter<Number>() { // from class: com.google.gson.internal.bind.TypeAdapters.13
        @Override // com.google.gson.TypeAdapter
        public Number read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() != JsonToken.NULL) {
                return Double.valueOf(jsonReader.x());
            }
            jsonReader.H();
            return null;
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, Number number) throws IOException {
            jsonWriter.D(number);
        }
    };
    public static final TypeAdapter<BigDecimal> B = new TypeAdapter<BigDecimal>() { // from class: com.google.gson.internal.bind.TypeAdapters.17
        @Override // com.google.gson.TypeAdapter
        public BigDecimal read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() == JsonToken.NULL) {
                jsonReader.H();
                return null;
            }
            try {
                return new BigDecimal(jsonReader.J());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, BigDecimal bigDecimal) throws IOException {
            jsonWriter.D(bigDecimal);
        }
    };
    public static final TypeAdapter<BigInteger> C = new TypeAdapter<BigInteger>() { // from class: com.google.gson.internal.bind.TypeAdapters.18
        @Override // com.google.gson.TypeAdapter
        public BigInteger read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() == JsonToken.NULL) {
                jsonReader.H();
                return null;
            }
            try {
                return new BigInteger(jsonReader.J());
            } catch (NumberFormatException e2) {
                throw new JsonSyntaxException(e2);
            }
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, BigInteger bigInteger) throws IOException {
            jsonWriter.D(bigInteger);
        }
    };
    public static final o S = new o() { // from class: com.google.gson.internal.bind.TypeAdapters.26
        @Override // b.i.d.o
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            if (typeToken.getRawType() != Timestamp.class) {
                return null;
            }
            final TypeAdapter<T> i2 = gson.i(Date.class);
            return (TypeAdapter<T>) new TypeAdapter<Timestamp>(this) { // from class: com.google.gson.internal.bind.TypeAdapters.26.1
                @Override // com.google.gson.TypeAdapter
                public Timestamp read(JsonReader jsonReader) throws IOException {
                    Date date = (Date) i2.read(jsonReader);
                    if (date != null) {
                        return new Timestamp(date.getTime());
                    }
                    return null;
                }

                @Override // com.google.gson.TypeAdapter
                public void write(JsonWriter jsonWriter, Timestamp timestamp) throws IOException {
                    i2.write(jsonWriter, timestamp);
                }
            };
        }
    };
    public static final o Z = new o() { // from class: com.google.gson.internal.bind.TypeAdapters.30
        @Override // b.i.d.o
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            Class rawType = typeToken.getRawType();
            if (!Enum.class.isAssignableFrom(rawType) || rawType == Enum.class) {
                return null;
            }
            if (!rawType.isEnum()) {
                rawType = (Class<? super Object>) rawType.getSuperclass();
            }
            return new EnumTypeAdapter(rawType);
        }
    };

    /* renamed from: com.google.gson.internal.bind.TypeAdapters$32  reason: invalid class name */
    /* loaded from: classes3.dex */
    public class AnonymousClass32 implements o {
        public final /* synthetic */ Class j;
        public final /* synthetic */ TypeAdapter k;

        public AnonymousClass32(Class cls, TypeAdapter typeAdapter) {
            this.j = cls;
            this.k = typeAdapter;
        }

        @Override // b.i.d.o
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            if (typeToken.getRawType() == this.j) {
                return this.k;
            }
            return null;
        }

        public String toString() {
            StringBuilder R = a.R("Factory[type=");
            a.i0(this.j, R, ",adapter=");
            R.append(this.k);
            R.append("]");
            return R.toString();
        }
    }

    /* renamed from: com.google.gson.internal.bind.TypeAdapters$33  reason: invalid class name */
    /* loaded from: classes3.dex */
    public class AnonymousClass33 implements o {
        public final /* synthetic */ Class j;
        public final /* synthetic */ Class k;
        public final /* synthetic */ TypeAdapter l;

        public AnonymousClass33(Class cls, Class cls2, TypeAdapter typeAdapter) {
            this.j = cls;
            this.k = cls2;
            this.l = typeAdapter;
        }

        @Override // b.i.d.o
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            Class<? super T> rawType = typeToken.getRawType();
            if (rawType == this.j || rawType == this.k) {
                return this.l;
            }
            return null;
        }

        public String toString() {
            StringBuilder R = a.R("Factory[type=");
            a.i0(this.k, R, BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX);
            a.i0(this.j, R, ",adapter=");
            R.append(this.l);
            R.append("]");
            return R.toString();
        }
    }

    /* loaded from: classes3.dex */
    public static final class EnumTypeAdapter<T extends Enum<T>> extends TypeAdapter<T> {
        public final Map<String, T> a = new HashMap();

        /* renamed from: b  reason: collision with root package name */
        public final Map<T, String> f3106b = new HashMap();

        public EnumTypeAdapter(Class<T> cls) {
            T[] enumConstants;
            try {
                for (T t : cls.getEnumConstants()) {
                    String name = t.name();
                    b bVar = (b) cls.getField(name).getAnnotation(b.class);
                    if (bVar != null) {
                        name = bVar.value();
                        for (String str : bVar.alternate()) {
                            this.a.put(str, t);
                        }
                    }
                    this.a.put(name, t);
                    this.f3106b.put(t, name);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError(e);
            }
        }

        @Override // com.google.gson.TypeAdapter
        public Object read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() != JsonToken.NULL) {
                return this.a.get(jsonReader.J());
            }
            jsonReader.H();
            return null;
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, Object obj) throws IOException {
            Enum r3 = (Enum) obj;
            jsonWriter.H(r3 == null ? null : this.f3106b.get(r3));
        }
    }

    static {
        TypeAdapter<Class> nullSafe = new TypeAdapter<Class>() { // from class: com.google.gson.internal.bind.TypeAdapters.1
            public Class a() throws IOException {
                throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
            }

            public void b(Class cls) throws IOException {
                throw new UnsupportedOperationException(a.n(cls, a.R("Attempted to serialize java.lang.Class: "), ". Forgot to register a type adapter?"));
            }

            @Override // com.google.gson.TypeAdapter
            public /* bridge */ /* synthetic */ Class read(JsonReader jsonReader) throws IOException {
                return a();
            }

            @Override // com.google.gson.TypeAdapter
            public /* bridge */ /* synthetic */ void write(JsonWriter jsonWriter, Class cls) throws IOException {
                b(cls);
            }
        }.nullSafe();
        a = nullSafe;
        f3100b = new AnonymousClass32(Class.class, nullSafe);
        TypeAdapter<BitSet> nullSafe2 = new TypeAdapter<BitSet>() { // from class: com.google.gson.internal.bind.TypeAdapters.2
            /* JADX WARN: Code restructure failed: missing block: B:15:0x003e, code lost:
                if (r6.y() != 0) goto L19;
             */
            /* JADX WARN: Code restructure failed: missing block: B:18:0x0049, code lost:
                if (java.lang.Integer.parseInt(r1) != 0) goto L19;
             */
            /* JADX WARN: Code restructure failed: missing block: B:19:0x004b, code lost:
                r1 = true;
             */
            /* JADX WARN: Code restructure failed: missing block: B:20:0x004d, code lost:
                r1 = false;
             */
            @Override // com.google.gson.TypeAdapter
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public java.util.BitSet read(com.google.gson.stream.JsonReader r6) throws java.io.IOException {
                /*
                    r5 = this;
                    java.util.BitSet r0 = new java.util.BitSet
                    r0.<init>()
                    r6.a()
                    com.google.gson.stream.JsonToken r1 = r6.N()
                    r2 = 0
                Ld:
                    com.google.gson.stream.JsonToken r3 = com.google.gson.stream.JsonToken.END_ARRAY
                    if (r1 == r3) goto L66
                    int r3 = r1.ordinal()
                    r4 = 5
                    if (r3 == r4) goto L41
                    r4 = 6
                    if (r3 == r4) goto L3a
                    r4 = 7
                    if (r3 != r4) goto L23
                    boolean r1 = r6.u()
                    goto L4e
                L23:
                    com.google.gson.JsonSyntaxException r6 = new com.google.gson.JsonSyntaxException
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder
                    r0.<init>()
                    java.lang.String r2 = "Invalid bitset value type: "
                    r0.append(r2)
                    r0.append(r1)
                    java.lang.String r0 = r0.toString()
                    r6.<init>(r0)
                    throw r6
                L3a:
                    int r1 = r6.y()
                    if (r1 == 0) goto L4d
                    goto L4b
                L41:
                    java.lang.String r1 = r6.J()
                    int r1 = java.lang.Integer.parseInt(r1)     // Catch: java.lang.NumberFormatException -> L5a
                    if (r1 == 0) goto L4d
                L4b:
                    r1 = 1
                    goto L4e
                L4d:
                    r1 = 0
                L4e:
                    if (r1 == 0) goto L53
                    r0.set(r2)
                L53:
                    int r2 = r2 + 1
                    com.google.gson.stream.JsonToken r1 = r6.N()
                    goto Ld
                L5a:
                    com.google.gson.JsonSyntaxException r6 = new com.google.gson.JsonSyntaxException
                    java.lang.String r0 = "Error: Expecting: bitset number value (1, 0), Found: "
                    java.lang.String r0 = b.d.b.a.a.v(r0, r1)
                    r6.<init>(r0)
                    throw r6
                L66:
                    r6.e()
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.gson.internal.bind.TypeAdapters.AnonymousClass2.read(com.google.gson.stream.JsonReader):java.lang.Object");
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, BitSet bitSet) throws IOException {
                BitSet bitSet2 = bitSet;
                jsonWriter.b();
                int length = bitSet2.length();
                for (int i2 = 0; i2 < length; i2++) {
                    jsonWriter.A(bitSet2.get(i2) ? 1L : 0L);
                }
                jsonWriter.e();
            }
        }.nullSafe();
        c = nullSafe2;
        d = new AnonymousClass32(BitSet.class, nullSafe2);
        TypeAdapter<Boolean> typeAdapter = new TypeAdapter<Boolean>() { // from class: com.google.gson.internal.bind.TypeAdapters.3
            @Override // com.google.gson.TypeAdapter
            public Boolean read(JsonReader jsonReader) throws IOException {
                JsonToken N2 = jsonReader.N();
                if (N2 == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                } else if (N2 == JsonToken.STRING) {
                    return Boolean.valueOf(Boolean.parseBoolean(jsonReader.J()));
                } else {
                    return Boolean.valueOf(jsonReader.u());
                }
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Boolean bool) throws IOException {
                jsonWriter.C(bool);
            }
        };
        e = typeAdapter;
        g = new AnonymousClass33(Boolean.TYPE, Boolean.class, typeAdapter);
        TypeAdapter<Number> typeAdapter2 = new TypeAdapter<Number>() { // from class: com.google.gson.internal.bind.TypeAdapters.5
            @Override // com.google.gson.TypeAdapter
            public Number read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                }
                try {
                    return Byte.valueOf((byte) jsonReader.y());
                } catch (NumberFormatException e2) {
                    throw new JsonSyntaxException(e2);
                }
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Number number) throws IOException {
                jsonWriter.D(number);
            }
        };
        h = typeAdapter2;
        i = new AnonymousClass33(Byte.TYPE, Byte.class, typeAdapter2);
        TypeAdapter<Number> typeAdapter3 = new TypeAdapter<Number>() { // from class: com.google.gson.internal.bind.TypeAdapters.6
            @Override // com.google.gson.TypeAdapter
            public Number read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                }
                try {
                    return Short.valueOf((short) jsonReader.y());
                } catch (NumberFormatException e2) {
                    throw new JsonSyntaxException(e2);
                }
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Number number) throws IOException {
                jsonWriter.D(number);
            }
        };
        j = typeAdapter3;
        k = new AnonymousClass33(Short.TYPE, Short.class, typeAdapter3);
        TypeAdapter<Number> typeAdapter4 = new TypeAdapter<Number>() { // from class: com.google.gson.internal.bind.TypeAdapters.7
            @Override // com.google.gson.TypeAdapter
            public Number read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                }
                try {
                    return Integer.valueOf(jsonReader.y());
                } catch (NumberFormatException e2) {
                    throw new JsonSyntaxException(e2);
                }
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Number number) throws IOException {
                jsonWriter.D(number);
            }
        };
        l = typeAdapter4;
        m = new AnonymousClass33(Integer.TYPE, Integer.class, typeAdapter4);
        TypeAdapter<AtomicInteger> nullSafe3 = new TypeAdapter<AtomicInteger>() { // from class: com.google.gson.internal.bind.TypeAdapters.8
            @Override // com.google.gson.TypeAdapter
            public AtomicInteger read(JsonReader jsonReader) throws IOException {
                try {
                    return new AtomicInteger(jsonReader.y());
                } catch (NumberFormatException e2) {
                    throw new JsonSyntaxException(e2);
                }
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, AtomicInteger atomicInteger) throws IOException {
                jsonWriter.A(atomicInteger.get());
            }
        }.nullSafe();
        n = nullSafe3;
        o = new AnonymousClass32(AtomicInteger.class, nullSafe3);
        TypeAdapter<AtomicBoolean> nullSafe4 = new TypeAdapter<AtomicBoolean>() { // from class: com.google.gson.internal.bind.TypeAdapters.9
            @Override // com.google.gson.TypeAdapter
            public AtomicBoolean read(JsonReader jsonReader) throws IOException {
                return new AtomicBoolean(jsonReader.u());
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, AtomicBoolean atomicBoolean) throws IOException {
                jsonWriter.I(atomicBoolean.get());
            }
        }.nullSafe();
        p = nullSafe4;
        q = new AnonymousClass32(AtomicBoolean.class, nullSafe4);
        TypeAdapter<AtomicIntegerArray> nullSafe5 = new TypeAdapter<AtomicIntegerArray>() { // from class: com.google.gson.internal.bind.TypeAdapters.10
            @Override // com.google.gson.TypeAdapter
            public AtomicIntegerArray read(JsonReader jsonReader) throws IOException {
                ArrayList arrayList = new ArrayList();
                jsonReader.a();
                while (jsonReader.q()) {
                    try {
                        arrayList.add(Integer.valueOf(jsonReader.y()));
                    } catch (NumberFormatException e2) {
                        throw new JsonSyntaxException(e2);
                    }
                }
                jsonReader.e();
                int size = arrayList.size();
                AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(size);
                for (int i2 = 0; i2 < size; i2++) {
                    atomicIntegerArray.set(i2, ((Integer) arrayList.get(i2)).intValue());
                }
                return atomicIntegerArray;
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, AtomicIntegerArray atomicIntegerArray) throws IOException {
                AtomicIntegerArray atomicIntegerArray2 = atomicIntegerArray;
                jsonWriter.b();
                int length = atomicIntegerArray2.length();
                for (int i2 = 0; i2 < length; i2++) {
                    jsonWriter.A(atomicIntegerArray2.get(i2));
                }
                jsonWriter.e();
            }
        }.nullSafe();
        r = nullSafe5;
        f3101s = new AnonymousClass32(AtomicIntegerArray.class, nullSafe5);
        TypeAdapter<Number> typeAdapter5 = new TypeAdapter<Number>() { // from class: com.google.gson.internal.bind.TypeAdapters.14
            @Override // com.google.gson.TypeAdapter
            public Number read(JsonReader jsonReader) throws IOException {
                JsonToken N2 = jsonReader.N();
                int ordinal = N2.ordinal();
                if (ordinal == 5 || ordinal == 6) {
                    return new q(jsonReader.J());
                }
                if (ordinal == 8) {
                    jsonReader.H();
                    return null;
                }
                throw new JsonSyntaxException("Expecting number, got: " + N2);
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Number number) throws IOException {
                jsonWriter.D(number);
            }
        };
        w = typeAdapter5;
        f3102x = new AnonymousClass32(Number.class, typeAdapter5);
        TypeAdapter<Character> typeAdapter6 = new TypeAdapter<Character>() { // from class: com.google.gson.internal.bind.TypeAdapters.15
            @Override // com.google.gson.TypeAdapter
            public Character read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                }
                String J2 = jsonReader.J();
                if (J2.length() == 1) {
                    return Character.valueOf(J2.charAt(0));
                }
                throw new JsonSyntaxException(a.v("Expecting character, got: ", J2));
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Character ch) throws IOException {
                Character ch2 = ch;
                jsonWriter.H(ch2 == null ? null : String.valueOf(ch2));
            }
        };
        f3103y = typeAdapter6;
        f3104z = new AnonymousClass33(Character.TYPE, Character.class, typeAdapter6);
        TypeAdapter<String> typeAdapter7 = new TypeAdapter<String>() { // from class: com.google.gson.internal.bind.TypeAdapters.16
            @Override // com.google.gson.TypeAdapter
            public String read(JsonReader jsonReader) throws IOException {
                JsonToken N2 = jsonReader.N();
                if (N2 == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                } else if (N2 == JsonToken.BOOLEAN) {
                    return Boolean.toString(jsonReader.u());
                } else {
                    return jsonReader.J();
                }
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, String str) throws IOException {
                jsonWriter.H(str);
            }
        };
        A = typeAdapter7;
        D = new AnonymousClass32(String.class, typeAdapter7);
        TypeAdapter<StringBuilder> typeAdapter8 = new TypeAdapter<StringBuilder>() { // from class: com.google.gson.internal.bind.TypeAdapters.19
            @Override // com.google.gson.TypeAdapter
            public StringBuilder read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() != JsonToken.NULL) {
                    return new StringBuilder(jsonReader.J());
                }
                jsonReader.H();
                return null;
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, StringBuilder sb) throws IOException {
                StringBuilder sb2 = sb;
                jsonWriter.H(sb2 == null ? null : sb2.toString());
            }
        };
        E = typeAdapter8;
        F = new AnonymousClass32(StringBuilder.class, typeAdapter8);
        TypeAdapter<StringBuffer> typeAdapter9 = new TypeAdapter<StringBuffer>() { // from class: com.google.gson.internal.bind.TypeAdapters.20
            @Override // com.google.gson.TypeAdapter
            public StringBuffer read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() != JsonToken.NULL) {
                    return new StringBuffer(jsonReader.J());
                }
                jsonReader.H();
                return null;
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, StringBuffer stringBuffer) throws IOException {
                StringBuffer stringBuffer2 = stringBuffer;
                jsonWriter.H(stringBuffer2 == null ? null : stringBuffer2.toString());
            }
        };
        G = typeAdapter9;
        H = new AnonymousClass32(StringBuffer.class, typeAdapter9);
        TypeAdapter<URL> typeAdapter10 = new TypeAdapter<URL>() { // from class: com.google.gson.internal.bind.TypeAdapters.21
            @Override // com.google.gson.TypeAdapter
            public URL read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                }
                String J2 = jsonReader.J();
                if ("null".equals(J2)) {
                    return null;
                }
                return new URL(J2);
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, URL url) throws IOException {
                URL url2 = url;
                jsonWriter.H(url2 == null ? null : url2.toExternalForm());
            }
        };
        I = typeAdapter10;
        J = new AnonymousClass32(URL.class, typeAdapter10);
        TypeAdapter<URI> typeAdapter11 = new TypeAdapter<URI>() { // from class: com.google.gson.internal.bind.TypeAdapters.22
            @Override // com.google.gson.TypeAdapter
            public URI read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                }
                try {
                    String J2 = jsonReader.J();
                    if ("null".equals(J2)) {
                        return null;
                    }
                    return new URI(J2);
                } catch (URISyntaxException e2) {
                    throw new JsonIOException(e2);
                }
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, URI uri) throws IOException {
                URI uri2 = uri;
                jsonWriter.H(uri2 == null ? null : uri2.toASCIIString());
            }
        };
        K = typeAdapter11;
        L = new AnonymousClass32(URI.class, typeAdapter11);
        final TypeAdapter<InetAddress> typeAdapter12 = new TypeAdapter<InetAddress>() { // from class: com.google.gson.internal.bind.TypeAdapters.23
            @Override // com.google.gson.TypeAdapter
            public InetAddress read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() != JsonToken.NULL) {
                    return InetAddress.getByName(jsonReader.J());
                }
                jsonReader.H();
                return null;
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, InetAddress inetAddress) throws IOException {
                InetAddress inetAddress2 = inetAddress;
                jsonWriter.H(inetAddress2 == null ? null : inetAddress2.getHostAddress());
            }
        };
        M = typeAdapter12;
        N = new o() { // from class: com.google.gson.internal.bind.TypeAdapters.35
            @Override // b.i.d.o
            public <T2> TypeAdapter<T2> create(Gson gson, TypeToken<T2> typeToken) {
                final Class<? super T2> rawType = typeToken.getRawType();
                if (!r1.isAssignableFrom(rawType)) {
                    return null;
                }
                return (TypeAdapter<T2>) new TypeAdapter<T1>() { // from class: com.google.gson.internal.bind.TypeAdapters.35.1
                    /* JADX WARN: Multi-variable type inference failed */
                    /* JADX WARN: Type inference failed for: r4v1, types: [java.lang.Object, T1] */
                    @Override // com.google.gson.TypeAdapter
                    public T1 read(JsonReader jsonReader) throws IOException {
                        ?? read = typeAdapter12.read(jsonReader);
                        if (read == 0 || rawType.isInstance(read)) {
                            return read;
                        }
                        StringBuilder R2 = a.R("Expected a ");
                        R2.append(rawType.getName());
                        R2.append(" but was ");
                        R2.append(read.getClass().getName());
                        throw new JsonSyntaxException(R2.toString());
                    }

                    @Override // com.google.gson.TypeAdapter
                    public void write(JsonWriter jsonWriter, T1 t1) throws IOException {
                        typeAdapter12.write(jsonWriter, t1);
                    }
                };
            }

            public String toString() {
                StringBuilder R2 = a.R("Factory[typeHierarchy=");
                a.i0(r1, R2, ",adapter=");
                R2.append(typeAdapter12);
                R2.append("]");
                return R2.toString();
            }
        };
        TypeAdapter<UUID> typeAdapter13 = new TypeAdapter<UUID>() { // from class: com.google.gson.internal.bind.TypeAdapters.24
            @Override // com.google.gson.TypeAdapter
            public UUID read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() != JsonToken.NULL) {
                    return UUID.fromString(jsonReader.J());
                }
                jsonReader.H();
                return null;
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, UUID uuid) throws IOException {
                UUID uuid2 = uuid;
                jsonWriter.H(uuid2 == null ? null : uuid2.toString());
            }
        };
        O = typeAdapter13;
        P = new AnonymousClass32(UUID.class, typeAdapter13);
        TypeAdapter<Currency> nullSafe6 = new TypeAdapter<Currency>() { // from class: com.google.gson.internal.bind.TypeAdapters.25
            @Override // com.google.gson.TypeAdapter
            public Currency read(JsonReader jsonReader) throws IOException {
                return Currency.getInstance(jsonReader.J());
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Currency currency) throws IOException {
                jsonWriter.H(currency.getCurrencyCode());
            }
        }.nullSafe();
        Q = nullSafe6;
        R = new AnonymousClass32(Currency.class, nullSafe6);
        final TypeAdapter<Calendar> typeAdapter14 = new TypeAdapter<Calendar>() { // from class: com.google.gson.internal.bind.TypeAdapters.27
            @Override // com.google.gson.TypeAdapter
            public Calendar read(JsonReader jsonReader) throws IOException {
                if (jsonReader.N() == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                }
                jsonReader.b();
                int i2 = 0;
                int i3 = 0;
                int i4 = 0;
                int i5 = 0;
                int i6 = 0;
                int i7 = 0;
                while (jsonReader.N() != JsonToken.END_OBJECT) {
                    String C2 = jsonReader.C();
                    int y2 = jsonReader.y();
                    if ("year".equals(C2)) {
                        i2 = y2;
                    } else if ("month".equals(C2)) {
                        i3 = y2;
                    } else if ("dayOfMonth".equals(C2)) {
                        i4 = y2;
                    } else if ("hourOfDay".equals(C2)) {
                        i5 = y2;
                    } else if ("minute".equals(C2)) {
                        i6 = y2;
                    } else if ("second".equals(C2)) {
                        i7 = y2;
                    }
                }
                jsonReader.f();
                return new GregorianCalendar(i2, i3, i4, i5, i6, i7);
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Calendar calendar) throws IOException {
                Calendar calendar2 = calendar;
                if (calendar2 == null) {
                    jsonWriter.s();
                    return;
                }
                jsonWriter.c();
                jsonWriter.n("year");
                jsonWriter.A(calendar2.get(1));
                jsonWriter.n("month");
                jsonWriter.A(calendar2.get(2));
                jsonWriter.n("dayOfMonth");
                jsonWriter.A(calendar2.get(5));
                jsonWriter.n("hourOfDay");
                jsonWriter.A(calendar2.get(11));
                jsonWriter.n("minute");
                jsonWriter.A(calendar2.get(12));
                jsonWriter.n("second");
                jsonWriter.A(calendar2.get(13));
                jsonWriter.f();
            }
        };
        T = typeAdapter14;
        U = new o() { // from class: com.google.gson.internal.bind.TypeAdapters.34
            @Override // b.i.d.o
            public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
                Class<? super T> rawType = typeToken.getRawType();
                if (rawType == r1 || rawType == r2) {
                    return typeAdapter14;
                }
                return null;
            }

            public String toString() {
                StringBuilder R2 = a.R("Factory[type=");
                a.i0(r1, R2, BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX);
                a.i0(r2, R2, ",adapter=");
                R2.append(typeAdapter14);
                R2.append("]");
                return R2.toString();
            }
        };
        TypeAdapter<Locale> typeAdapter15 = new TypeAdapter<Locale>() { // from class: com.google.gson.internal.bind.TypeAdapters.28
            @Override // com.google.gson.TypeAdapter
            public Locale read(JsonReader jsonReader) throws IOException {
                String str = null;
                if (jsonReader.N() == JsonToken.NULL) {
                    jsonReader.H();
                    return null;
                }
                StringTokenizer stringTokenizer = new StringTokenizer(jsonReader.J(), "_");
                String nextToken = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
                String nextToken2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
                if (stringTokenizer.hasMoreElements()) {
                    str = stringTokenizer.nextToken();
                }
                if (nextToken2 == null && str == null) {
                    return new Locale(nextToken);
                }
                if (str == null) {
                    return new Locale(nextToken, nextToken2);
                }
                return new Locale(nextToken, nextToken2, str);
            }

            @Override // com.google.gson.TypeAdapter
            public void write(JsonWriter jsonWriter, Locale locale) throws IOException {
                Locale locale2 = locale;
                jsonWriter.H(locale2 == null ? null : locale2.toString());
            }
        };
        V = typeAdapter15;
        W = new AnonymousClass32(Locale.class, typeAdapter15);
        final TypeAdapter<JsonElement> typeAdapter16 = new TypeAdapter<JsonElement>() { // from class: com.google.gson.internal.bind.TypeAdapters.29
            /* renamed from: a */
            public JsonElement read(JsonReader jsonReader) throws IOException {
                int ordinal = jsonReader.N().ordinal();
                if (ordinal == 0) {
                    g gVar = new g();
                    jsonReader.a();
                    while (jsonReader.q()) {
                        gVar.j.add(read(jsonReader));
                    }
                    jsonReader.e();
                    return gVar;
                } else if (ordinal == 2) {
                    JsonObject jsonObject = new JsonObject();
                    jsonReader.b();
                    while (jsonReader.q()) {
                        jsonObject.a.put(jsonReader.C(), read(jsonReader));
                    }
                    jsonReader.f();
                    return jsonObject;
                } else if (ordinal == 5) {
                    return new k(jsonReader.J());
                } else {
                    if (ordinal == 6) {
                        return new k(new q(jsonReader.J()));
                    }
                    if (ordinal == 7) {
                        return new k(Boolean.valueOf(jsonReader.u()));
                    }
                    if (ordinal == 8) {
                        jsonReader.H();
                        return j.a;
                    }
                    throw new IllegalArgumentException();
                }
            }

            /* renamed from: b */
            public void write(JsonWriter jsonWriter, JsonElement jsonElement) throws IOException {
                if (jsonElement == null || (jsonElement instanceof j)) {
                    jsonWriter.s();
                } else if (jsonElement instanceof k) {
                    k e2 = jsonElement.e();
                    Object obj = e2.a;
                    if (obj instanceof Number) {
                        jsonWriter.D(e2.i());
                    } else if (obj instanceof Boolean) {
                        jsonWriter.I(e2.h());
                    } else {
                        jsonWriter.H(e2.g());
                    }
                } else {
                    boolean z2 = jsonElement instanceof g;
                    if (z2) {
                        jsonWriter.b();
                        if (z2) {
                            Iterator<JsonElement> it = ((g) jsonElement).iterator();
                            while (it.hasNext()) {
                                write(jsonWriter, it.next());
                            }
                            jsonWriter.e();
                            return;
                        }
                        throw new IllegalStateException("Not a JSON Array: " + jsonElement);
                    } else if (jsonElement instanceof JsonObject) {
                        jsonWriter.c();
                        LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
                        LinkedTreeMap.e eVar = linkedTreeMap.header.m;
                        int i2 = linkedTreeMap.modCount;
                        while (true) {
                            if (!(eVar != linkedTreeMap.header)) {
                                jsonWriter.f();
                                return;
                            } else if (eVar == linkedTreeMap.header) {
                                throw new NoSuchElementException();
                            } else if (linkedTreeMap.modCount == i2) {
                                LinkedTreeMap.e eVar2 = eVar.m;
                                jsonWriter.n((String) eVar.getKey());
                                write(jsonWriter, (JsonElement) eVar.getValue());
                                eVar = eVar2;
                            } else {
                                throw new ConcurrentModificationException();
                            }
                        }
                    } else {
                        StringBuilder R2 = a.R("Couldn't write ");
                        R2.append(jsonElement.getClass());
                        throw new IllegalArgumentException(R2.toString());
                    }
                }
            }
        };
        X = typeAdapter16;
        Y = new o() { // from class: com.google.gson.internal.bind.TypeAdapters.35
            @Override // b.i.d.o
            public <T2> TypeAdapter<T2> create(Gson gson, TypeToken<T2> typeToken) {
                final Class rawType = typeToken.getRawType();
                if (!r1.isAssignableFrom(rawType)) {
                    return null;
                }
                return (TypeAdapter<T2>) new TypeAdapter<T1>() { // from class: com.google.gson.internal.bind.TypeAdapters.35.1
                    /* JADX WARN: Multi-variable type inference failed */
                    /* JADX WARN: Type inference failed for: r4v1, types: [java.lang.Object, T1] */
                    @Override // com.google.gson.TypeAdapter
                    public T1 read(JsonReader jsonReader) throws IOException {
                        ?? read = typeAdapter16.read(jsonReader);
                        if (read == 0 || rawType.isInstance(read)) {
                            return read;
                        }
                        StringBuilder R2 = a.R("Expected a ");
                        R2.append(rawType.getName());
                        R2.append(" but was ");
                        R2.append(read.getClass().getName());
                        throw new JsonSyntaxException(R2.toString());
                    }

                    @Override // com.google.gson.TypeAdapter
                    public void write(JsonWriter jsonWriter, T1 t1) throws IOException {
                        typeAdapter16.write(jsonWriter, t1);
                    }
                };
            }

            public String toString() {
                StringBuilder R2 = a.R("Factory[typeHierarchy=");
                a.i0(r1, R2, ",adapter=");
                R2.append(typeAdapter16);
                R2.append("]");
                return R2.toString();
            }
        };
    }
}
