package com.google.gson.internal.bind;

import b.i.d.o;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.ArrayList;
/* loaded from: classes3.dex */
public final class ObjectTypeAdapter extends TypeAdapter<Object> {
    public static final o a = new o() { // from class: com.google.gson.internal.bind.ObjectTypeAdapter.1
        @Override // b.i.d.o
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
            if (typeToken.getRawType() == Object.class) {
                return new ObjectTypeAdapter(gson);
            }
            return null;
        }
    };

    /* renamed from: b  reason: collision with root package name */
    public final Gson f3093b;

    public ObjectTypeAdapter(Gson gson) {
        this.f3093b = gson;
    }

    @Override // com.google.gson.TypeAdapter
    public Object read(JsonReader jsonReader) throws IOException {
        int ordinal = jsonReader.N().ordinal();
        if (ordinal == 0) {
            ArrayList arrayList = new ArrayList();
            jsonReader.a();
            while (jsonReader.q()) {
                arrayList.add(read(jsonReader));
            }
            jsonReader.e();
            return arrayList;
        } else if (ordinal == 2) {
            LinkedTreeMap linkedTreeMap = new LinkedTreeMap();
            jsonReader.b();
            while (jsonReader.q()) {
                linkedTreeMap.put(jsonReader.C(), read(jsonReader));
            }
            jsonReader.f();
            return linkedTreeMap;
        } else if (ordinal == 5) {
            return jsonReader.J();
        } else {
            if (ordinal == 6) {
                return Double.valueOf(jsonReader.x());
            }
            if (ordinal == 7) {
                return Boolean.valueOf(jsonReader.u());
            }
            if (ordinal == 8) {
                jsonReader.H();
                return null;
            }
            throw new IllegalStateException();
        }
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, Object obj) throws IOException {
        if (obj == null) {
            jsonWriter.s();
            return;
        }
        TypeAdapter i = this.f3093b.i(obj.getClass());
        if (i instanceof ObjectTypeAdapter) {
            jsonWriter.c();
            jsonWriter.f();
            return;
        }
        i.write(jsonWriter, obj);
    }
}
