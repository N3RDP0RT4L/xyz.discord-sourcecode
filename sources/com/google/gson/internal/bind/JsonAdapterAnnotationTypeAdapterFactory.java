package com.google.gson.internal.bind;

import b.i.d.i;
import b.i.d.m;
import b.i.d.o;
import b.i.d.p.a;
import b.i.d.q.g;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
/* loaded from: classes3.dex */
public final class JsonAdapterAnnotationTypeAdapterFactory implements o {
    public final g j;

    public JsonAdapterAnnotationTypeAdapterFactory(g gVar) {
        this.j = gVar;
    }

    public TypeAdapter<?> a(g gVar, Gson gson, TypeToken<?> typeToken, a aVar) {
        TypeAdapter<?> typeAdapter;
        Object a = gVar.a(TypeToken.get((Class) aVar.value())).a();
        if (a instanceof TypeAdapter) {
            typeAdapter = (TypeAdapter) a;
        } else if (a instanceof o) {
            typeAdapter = ((o) a).create(gson, typeToken);
        } else {
            boolean z2 = a instanceof m;
            if (z2 || (a instanceof i)) {
                i iVar = null;
                m mVar = z2 ? (m) a : null;
                if (a instanceof i) {
                    iVar = (i) a;
                }
                typeAdapter = new TreeTypeAdapter<>(mVar, iVar, gson, typeToken, null);
            } else {
                StringBuilder R = b.d.b.a.a.R("Invalid attempt to bind an instance of ");
                R.append(a.getClass().getName());
                R.append(" as a @JsonAdapter for ");
                R.append(typeToken.toString());
                R.append(". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer.");
                throw new IllegalArgumentException(R.toString());
            }
        }
        return (typeAdapter == null || !aVar.nullSafe()) ? typeAdapter : typeAdapter.nullSafe();
    }

    @Override // b.i.d.o
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        a aVar = (a) typeToken.getRawType().getAnnotation(a.class);
        if (aVar == null) {
            return null;
        }
        return (TypeAdapter<T>) a(this.j, gson, typeToken, aVar);
    }
}
