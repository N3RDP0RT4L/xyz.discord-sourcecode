package com.google.gson.internal.bind;

import b.i.d.o;
import b.i.d.q.a;
import b.i.d.q.g;
import b.i.d.q.r;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Collection;
/* loaded from: classes3.dex */
public final class CollectionTypeAdapterFactory implements o {
    public final g j;

    /* loaded from: classes3.dex */
    public static final class Adapter<E> extends TypeAdapter<Collection<E>> {
        public final TypeAdapter<E> a;

        /* renamed from: b  reason: collision with root package name */
        public final r<? extends Collection<E>> f3090b;

        public Adapter(Gson gson, Type type, TypeAdapter<E> typeAdapter, r<? extends Collection<E>> rVar) {
            this.a = new TypeAdapterRuntimeTypeWrapper(gson, typeAdapter, type);
            this.f3090b = rVar;
        }

        @Override // com.google.gson.TypeAdapter
        public Object read(JsonReader jsonReader) throws IOException {
            if (jsonReader.N() == JsonToken.NULL) {
                jsonReader.H();
                return null;
            }
            Collection<E> a = this.f3090b.a();
            jsonReader.a();
            while (jsonReader.q()) {
                a.add(this.a.read(jsonReader));
            }
            jsonReader.e();
            return a;
        }

        @Override // com.google.gson.TypeAdapter
        public void write(JsonWriter jsonWriter, Object obj) throws IOException {
            Collection<E> collection = (Collection) obj;
            if (collection == null) {
                jsonWriter.s();
                return;
            }
            jsonWriter.b();
            for (E e : collection) {
                this.a.write(jsonWriter, e);
            }
            jsonWriter.e();
        }
    }

    public CollectionTypeAdapterFactory(g gVar) {
        this.j = gVar;
    }

    @Override // b.i.d.o
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        Type type;
        Type type2 = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();
        if (!Collection.class.isAssignableFrom(rawType)) {
            return null;
        }
        Type f = a.f(type2, rawType, Collection.class);
        if (f instanceof WildcardType) {
            f = ((WildcardType) f).getUpperBounds()[0];
        }
        if (f instanceof ParameterizedType) {
            type = ((ParameterizedType) f).getActualTypeArguments()[0];
        } else {
            type = Object.class;
        }
        return new Adapter(gson, type, gson.h(TypeToken.get(type)), this.j.a(typeToken));
    }
}
