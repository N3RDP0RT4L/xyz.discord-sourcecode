package com.google.gson.internal;

import b.i.d.a;
import b.i.d.o;
import b.i.d.p.c;
import b.i.d.p.d;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.objectweb.asm.Opcodes;
/* loaded from: classes3.dex */
public final class Excluder implements o, Cloneable {
    public static final Excluder j = new Excluder();
    public double k = -1.0d;
    public int l = Opcodes.L2I;
    public boolean m = true;
    public List<a> n = Collections.emptyList();
    public List<a> o = Collections.emptyList();

    public final boolean b(Class<?> cls) {
        if (this.k == -1.0d || i((c) cls.getAnnotation(c.class), (d) cls.getAnnotation(d.class))) {
            return (!this.m && h(cls)) || g(cls);
        }
        return true;
    }

    public Object clone() throws CloneNotSupportedException {
        try {
            return (Excluder) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @Override // b.i.d.o
    public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> typeToken) {
        Class<? super T> rawType = typeToken.getRawType();
        boolean b2 = b(rawType);
        final boolean z2 = b2 || f(rawType, true);
        final boolean z3 = b2 || f(rawType, false);
        if (z2 || z3) {
            return new TypeAdapter<T>() { // from class: com.google.gson.internal.Excluder.1
                public TypeAdapter<T> a;

                /* JADX WARN: Type inference failed for: r4v1, types: [T, java.lang.Object] */
                @Override // com.google.gson.TypeAdapter
                public T read(JsonReader jsonReader) throws IOException {
                    if (z3) {
                        jsonReader.U();
                        return null;
                    }
                    TypeAdapter typeAdapter = this.a;
                    if (typeAdapter == null) {
                        typeAdapter = gson.j(Excluder.this, typeToken);
                        this.a = typeAdapter;
                    }
                    return typeAdapter.read(jsonReader);
                }

                @Override // com.google.gson.TypeAdapter
                public void write(JsonWriter jsonWriter, T t) throws IOException {
                    if (z2) {
                        jsonWriter.s();
                        return;
                    }
                    TypeAdapter typeAdapter = this.a;
                    if (typeAdapter == null) {
                        typeAdapter = gson.j(Excluder.this, typeToken);
                        this.a = typeAdapter;
                    }
                    typeAdapter.write(jsonWriter, t);
                }
            };
        }
        return null;
    }

    public final boolean f(Class<?> cls, boolean z2) {
        for (a aVar : z2 ? this.n : this.o) {
            if (aVar.b(cls)) {
                return true;
            }
        }
        return false;
    }

    public final boolean g(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    public final boolean h(Class<?> cls) {
        if (cls.isMemberClass()) {
            if (!((cls.getModifiers() & 8) != 0)) {
                return true;
            }
        }
        return false;
    }

    public final boolean i(c cVar, d dVar) {
        if (!(cVar == null || cVar.value() <= this.k)) {
            return false;
        }
        return dVar == null || (dVar.value() > this.k ? 1 : (dVar.value() == this.k ? 0 : -1)) > 0;
    }

    public Excluder j(int... iArr) {
        try {
            Excluder excluder = (Excluder) super.clone();
            excluder.l = 0;
            for (int i : iArr) {
                excluder.l = i | excluder.l;
            }
            return excluder;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }
}
