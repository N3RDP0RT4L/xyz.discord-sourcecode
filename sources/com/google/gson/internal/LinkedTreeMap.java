package com.google.gson.internal;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
/* loaded from: classes3.dex */
public final class LinkedTreeMap<K, V> extends AbstractMap<K, V> implements Serializable {
    public static final Comparator<Comparable> j = new a();
    public Comparator<? super K> comparator;
    private LinkedTreeMap<K, V>.b entrySet;
    private LinkedTreeMap<K, V>.c keySet;
    public e<K, V> root;
    public int size = 0;
    public int modCount = 0;
    public final e<K, V> header = new e<>();

    /* loaded from: classes3.dex */
    public class a implements Comparator<Comparable> {
        @Override // java.util.Comparator
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    }

    /* loaded from: classes3.dex */
    public class b extends AbstractSet<Map.Entry<K, V>> {

        /* loaded from: classes3.dex */
        public class a extends LinkedTreeMap<K, V>.d<Map.Entry<K, V>> {
            public a(b bVar) {
                super();
            }

            @Override // java.util.Iterator
            public Object next() {
                return a();
            }
        }

        public b() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            LinkedTreeMap.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && LinkedTreeMap.this.b((Map.Entry) obj) != null;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<Map.Entry<K, V>> iterator() {
            return new a(this);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            e<K, V> b2;
            if (!(obj instanceof Map.Entry) || (b2 = LinkedTreeMap.this.b((Map.Entry) obj)) == null) {
                return false;
            }
            LinkedTreeMap.this.e(b2, true);
            return true;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return LinkedTreeMap.this.size;
        }
    }

    /* loaded from: classes3.dex */
    public final class c extends AbstractSet<K> {

        /* loaded from: classes3.dex */
        public class a extends LinkedTreeMap<K, V>.d<K> {
            public a(c cVar) {
                super();
            }

            @Override // java.util.Iterator
            public K next() {
                return a().o;
            }
        }

        public c() {
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public void clear() {
            LinkedTreeMap.this.clear();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean contains(Object obj) {
            return LinkedTreeMap.this.c(obj) != null;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
        public Iterator<K> iterator() {
            return new a(this);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean remove(Object obj) {
            LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
            e<K, V> c = linkedTreeMap.c(obj);
            if (c != null) {
                linkedTreeMap.e(c, true);
            }
            return c != null;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public int size() {
            return LinkedTreeMap.this.size;
        }
    }

    /* loaded from: classes3.dex */
    public abstract class d<T> implements Iterator<T> {
        public e<K, V> j;
        public e<K, V> k = null;
        public int l;

        public d() {
            this.j = LinkedTreeMap.this.header.m;
            this.l = LinkedTreeMap.this.modCount;
        }

        public final e<K, V> a() {
            e<K, V> eVar = this.j;
            LinkedTreeMap linkedTreeMap = LinkedTreeMap.this;
            if (eVar == linkedTreeMap.header) {
                throw new NoSuchElementException();
            } else if (linkedTreeMap.modCount == this.l) {
                this.j = eVar.m;
                this.k = eVar;
                return eVar;
            } else {
                throw new ConcurrentModificationException();
            }
        }

        @Override // java.util.Iterator
        public final boolean hasNext() {
            return this.j != LinkedTreeMap.this.header;
        }

        @Override // java.util.Iterator
        public final void remove() {
            e<K, V> eVar = this.k;
            if (eVar != null) {
                LinkedTreeMap.this.e(eVar, true);
                this.k = null;
                this.l = LinkedTreeMap.this.modCount;
                return;
            }
            throw new IllegalStateException();
        }
    }

    public LinkedTreeMap() {
        Comparator<Comparable> comparator = j;
        this.comparator = comparator;
    }

    private Object writeReplace() throws ObjectStreamException {
        return new LinkedHashMap(this);
    }

    public e<K, V> a(K k, boolean z2) {
        int i;
        e<K, V> eVar;
        Comparator<? super K> comparator = this.comparator;
        e<K, V> eVar2 = this.root;
        if (eVar2 != null) {
            Comparable comparable = comparator == j ? (Comparable) k : null;
            while (true) {
                if (comparable != null) {
                    i = comparable.compareTo(eVar2.o);
                } else {
                    i = comparator.compare(k, (K) eVar2.o);
                }
                if (i == 0) {
                    return eVar2;
                }
                e<K, V> eVar3 = i < 0 ? eVar2.k : eVar2.l;
                if (eVar3 == null) {
                    break;
                }
                eVar2 = eVar3;
            }
        } else {
            i = 0;
        }
        if (!z2) {
            return null;
        }
        e<K, V> eVar4 = this.header;
        if (eVar2 != null) {
            eVar = new e<>(eVar2, k, eVar4, eVar4.n);
            if (i < 0) {
                eVar2.k = eVar;
            } else {
                eVar2.l = eVar;
            }
            d(eVar2, true);
        } else if (comparator != j || (k instanceof Comparable)) {
            eVar = new e<>(eVar2, k, eVar4, eVar4.n);
            this.root = eVar;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.size++;
        this.modCount++;
        return eVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0020, code lost:
        if ((r3 == r5 || (r3 != null && r3.equals(r5))) != false) goto L13;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.google.gson.internal.LinkedTreeMap.e<K, V> b(java.util.Map.Entry<?, ?> r5) {
        /*
            r4 = this;
            java.lang.Object r0 = r5.getKey()
            com.google.gson.internal.LinkedTreeMap$e r0 = r4.c(r0)
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L23
            V r3 = r0.p
            java.lang.Object r5 = r5.getValue()
            if (r3 == r5) goto L1f
            if (r3 == 0) goto L1d
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L1d
            goto L1f
        L1d:
            r5 = 0
            goto L20
        L1f:
            r5 = 1
        L20:
            if (r5 == 0) goto L23
            goto L24
        L23:
            r1 = 0
        L24:
            if (r1 == 0) goto L27
            goto L28
        L27:
            r0 = 0
        L28:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.internal.LinkedTreeMap.b(java.util.Map$Entry):com.google.gson.internal.LinkedTreeMap$e");
    }

    /* JADX WARN: Multi-variable type inference failed */
    public e<K, V> c(Object obj) {
        if (obj == 0) {
            return null;
        }
        try {
            return a(obj, false);
        } catch (ClassCastException unused) {
            return null;
        }
    }

    @Override // java.util.AbstractMap, java.util.Map
    public void clear() {
        this.root = null;
        this.size = 0;
        this.modCount++;
        e<K, V> eVar = this.header;
        eVar.n = eVar;
        eVar.m = eVar;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public boolean containsKey(Object obj) {
        return c(obj) != null;
    }

    public final void d(e<K, V> eVar, boolean z2) {
        while (eVar != null) {
            e<K, V> eVar2 = eVar.k;
            e<K, V> eVar3 = eVar.l;
            int i = 0;
            int i2 = eVar2 != null ? eVar2.q : 0;
            int i3 = eVar3 != null ? eVar3.q : 0;
            int i4 = i2 - i3;
            if (i4 == -2) {
                e<K, V> eVar4 = eVar3.k;
                e<K, V> eVar5 = eVar3.l;
                int i5 = eVar5 != null ? eVar5.q : 0;
                if (eVar4 != null) {
                    i = eVar4.q;
                }
                int i6 = i - i5;
                if (i6 == -1 || (i6 == 0 && !z2)) {
                    g(eVar);
                } else {
                    h(eVar3);
                    g(eVar);
                }
                if (z2) {
                    return;
                }
            } else if (i4 == 2) {
                e<K, V> eVar6 = eVar2.k;
                e<K, V> eVar7 = eVar2.l;
                int i7 = eVar7 != null ? eVar7.q : 0;
                if (eVar6 != null) {
                    i = eVar6.q;
                }
                int i8 = i - i7;
                if (i8 == 1 || (i8 == 0 && !z2)) {
                    h(eVar);
                } else {
                    g(eVar2);
                    h(eVar);
                }
                if (z2) {
                    return;
                }
            } else if (i4 == 0) {
                eVar.q = i2 + 1;
                if (z2) {
                    return;
                }
            } else {
                eVar.q = Math.max(i2, i3) + 1;
                if (!z2) {
                    return;
                }
            }
            eVar = eVar.j;
        }
    }

    public void e(e<K, V> eVar, boolean z2) {
        e<K, V> eVar2;
        int i;
        if (z2) {
            e<K, V> eVar3 = eVar.n;
            eVar3.m = eVar.m;
            eVar.m.n = eVar3;
        }
        e<K, V> eVar4 = eVar.k;
        e<K, V> eVar5 = eVar.l;
        e<K, V> eVar6 = eVar.j;
        int i2 = 0;
        if (eVar4 == null || eVar5 == null) {
            if (eVar4 != null) {
                f(eVar, eVar4);
                eVar.k = null;
            } else if (eVar5 != null) {
                f(eVar, eVar5);
                eVar.l = null;
            } else {
                f(eVar, null);
            }
            d(eVar6, false);
            this.size--;
            this.modCount++;
            return;
        }
        if (eVar4.q <= eVar5.q) {
            e<K, V> eVar7 = eVar5.k;
            while (true) {
                eVar2 = eVar5;
                eVar5 = eVar7;
                if (eVar5 == null) {
                    break;
                }
                eVar7 = eVar5.k;
            }
        } else {
            e<K, V> eVar8 = eVar4.l;
            while (true) {
                eVar4 = eVar8;
                eVar2 = eVar4;
                if (eVar4 == null) {
                    break;
                }
                eVar8 = eVar4.l;
            }
        }
        e(eVar2, false);
        e<K, V> eVar9 = eVar.k;
        if (eVar9 != null) {
            i = eVar9.q;
            eVar2.k = eVar9;
            eVar9.j = eVar2;
            eVar.k = null;
        } else {
            i = 0;
        }
        e<K, V> eVar10 = eVar.l;
        if (eVar10 != null) {
            i2 = eVar10.q;
            eVar2.l = eVar10;
            eVar10.j = eVar2;
            eVar.l = null;
        }
        eVar2.q = Math.max(i, i2) + 1;
        f(eVar, eVar2);
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        LinkedTreeMap<K, V>.b bVar = this.entrySet;
        if (bVar != null) {
            return bVar;
        }
        LinkedTreeMap<K, V>.b bVar2 = new b();
        this.entrySet = bVar2;
        return bVar2;
    }

    public final void f(e<K, V> eVar, e<K, V> eVar2) {
        e<K, V> eVar3 = eVar.j;
        eVar.j = null;
        if (eVar2 != null) {
            eVar2.j = eVar3;
        }
        if (eVar3 == null) {
            this.root = eVar2;
        } else if (eVar3.k == eVar) {
            eVar3.k = eVar2;
        } else {
            eVar3.l = eVar2;
        }
    }

    public final void g(e<K, V> eVar) {
        e<K, V> eVar2 = eVar.k;
        e<K, V> eVar3 = eVar.l;
        e<K, V> eVar4 = eVar3.k;
        e<K, V> eVar5 = eVar3.l;
        eVar.l = eVar4;
        if (eVar4 != null) {
            eVar4.j = eVar;
        }
        f(eVar, eVar3);
        eVar3.k = eVar;
        eVar.j = eVar3;
        int i = 0;
        int max = Math.max(eVar2 != null ? eVar2.q : 0, eVar4 != null ? eVar4.q : 0) + 1;
        eVar.q = max;
        if (eVar5 != null) {
            i = eVar5.q;
        }
        eVar3.q = Math.max(max, i) + 1;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        e<K, V> c2 = c(obj);
        if (c2 != null) {
            return c2.p;
        }
        return null;
    }

    public final void h(e<K, V> eVar) {
        e<K, V> eVar2 = eVar.k;
        e<K, V> eVar3 = eVar.l;
        e<K, V> eVar4 = eVar2.k;
        e<K, V> eVar5 = eVar2.l;
        eVar.k = eVar5;
        if (eVar5 != null) {
            eVar5.j = eVar;
        }
        f(eVar, eVar2);
        eVar2.l = eVar;
        eVar.j = eVar2;
        int i = 0;
        int max = Math.max(eVar3 != null ? eVar3.q : 0, eVar5 != null ? eVar5.q : 0) + 1;
        eVar.q = max;
        if (eVar4 != null) {
            i = eVar4.q;
        }
        eVar2.q = Math.max(max, i) + 1;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        LinkedTreeMap<K, V>.c cVar = this.keySet;
        if (cVar != null) {
            return cVar;
        }
        LinkedTreeMap<K, V>.c cVar2 = new c();
        this.keySet = cVar2;
        return cVar2;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V put(K k, V v) {
        Objects.requireNonNull(k, "key == null");
        e<K, V> a2 = a(k, true);
        V v2 = a2.p;
        a2.p = v;
        return v2;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public V remove(Object obj) {
        e<K, V> c2 = c(obj);
        if (c2 != null) {
            e(c2, true);
        }
        if (c2 != null) {
            return c2.p;
        }
        return null;
    }

    @Override // java.util.AbstractMap, java.util.Map
    public int size() {
        return this.size;
    }

    /* loaded from: classes3.dex */
    public static final class e<K, V> implements Map.Entry<K, V> {
        public e<K, V> j;
        public e<K, V> k;
        public e<K, V> l;
        public e<K, V> m;
        public e<K, V> n;
        public final K o;
        public V p;
        public int q;

        public e() {
            this.o = null;
            this.n = this;
            this.m = this;
        }

        @Override // java.util.Map.Entry
        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            K k = this.o;
            if (k == null) {
                if (entry.getKey() != null) {
                    return false;
                }
            } else if (!k.equals(entry.getKey())) {
                return false;
            }
            V v = this.p;
            if (v == null) {
                if (entry.getValue() != null) {
                    return false;
                }
            } else if (!v.equals(entry.getValue())) {
                return false;
            }
            return true;
        }

        @Override // java.util.Map.Entry
        public K getKey() {
            return this.o;
        }

        @Override // java.util.Map.Entry
        public V getValue() {
            return this.p;
        }

        @Override // java.util.Map.Entry
        public int hashCode() {
            K k = this.o;
            int i = 0;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.p;
            if (v != null) {
                i = v.hashCode();
            }
            return hashCode ^ i;
        }

        @Override // java.util.Map.Entry
        public V setValue(V v) {
            V v2 = this.p;
            this.p = v;
            return v2;
        }

        public String toString() {
            return this.o + "=" + this.p;
        }

        public e(e<K, V> eVar, K k, e<K, V> eVar2, e<K, V> eVar3) {
            this.j = eVar;
            this.o = k;
            this.q = 1;
            this.m = eVar2;
            this.n = eVar3;
            eVar3.m = this;
            eVar2.n = this;
        }
    }
}
