package com.google.gson;

import b.i.d.j;
import b.i.d.k;
import com.google.gson.internal.LinkedTreeMap;
import java.util.Map;
import java.util.Set;
/* loaded from: classes3.dex */
public final class JsonObject extends JsonElement {
    public final LinkedTreeMap<String, JsonElement> a = new LinkedTreeMap<>();

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof JsonObject) && ((JsonObject) obj).a.equals(this.a));
    }

    public void h(String str, JsonElement jsonElement) {
        LinkedTreeMap<String, JsonElement> linkedTreeMap = this.a;
        if (jsonElement == null) {
            jsonElement = j.a;
        }
        linkedTreeMap.put(str, jsonElement);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public void i(String str, Number number) {
        this.a.put(str, number == null ? j.a : new k(number));
    }

    public Set<Map.Entry<String, JsonElement>> j() {
        return this.a.entrySet();
    }
}
