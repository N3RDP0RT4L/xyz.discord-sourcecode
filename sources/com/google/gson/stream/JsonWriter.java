package com.google.gson.stream;

import b.d.b.a.a;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Objects;
/* loaded from: classes3.dex */
public class JsonWriter implements Closeable, Flushable {
    public static final String[] j = new String[128];
    public static final String[] k;
    public final Writer l;
    public String o;
    public boolean q;
    public boolean r;

    /* renamed from: s  reason: collision with root package name */
    public String f3111s;
    public int[] m = new int[32];
    public int n = 0;
    public String p = ":";
    public boolean t = true;

    static {
        for (int i = 0; i <= 31; i++) {
            j[i] = String.format("\\u%04x", Integer.valueOf(i));
        }
        String[] strArr = j;
        strArr[34] = "\\\"";
        strArr[92] = "\\\\";
        strArr[9] = "\\t";
        strArr[8] = "\\b";
        strArr[10] = "\\n";
        strArr[13] = "\\r";
        strArr[12] = "\\f";
        String[] strArr2 = (String[]) strArr.clone();
        k = strArr2;
        strArr2[60] = "\\u003c";
        strArr2[62] = "\\u003e";
        strArr2[38] = "\\u0026";
        strArr2[61] = "\\u003d";
        strArr2[39] = "\\u0027";
    }

    public JsonWriter(Writer writer) {
        u(6);
        Objects.requireNonNull(writer, "out == null");
        this.l = writer;
    }

    public JsonWriter A(long j2) throws IOException {
        J();
        a();
        this.l.write(Long.toString(j2));
        return this;
    }

    public JsonWriter C(Boolean bool) throws IOException {
        if (bool == null) {
            return s();
        }
        J();
        a();
        this.l.write(bool.booleanValue() ? "true" : "false");
        return this;
    }

    public JsonWriter D(Number number) throws IOException {
        if (number == null) {
            return s();
        }
        J();
        String obj = number.toString();
        if (this.q || (!obj.equals("-Infinity") && !obj.equals("Infinity") && !obj.equals("NaN"))) {
            a();
            this.l.append((CharSequence) obj);
            return this;
        }
        throw new IllegalArgumentException("Numeric values must be finite, but was " + number);
    }

    public JsonWriter H(String str) throws IOException {
        if (str == null) {
            return s();
        }
        J();
        a();
        y(str);
        return this;
    }

    public JsonWriter I(boolean z2) throws IOException {
        J();
        a();
        this.l.write(z2 ? "true" : "false");
        return this;
    }

    public final void J() throws IOException {
        if (this.f3111s != null) {
            int t = t();
            if (t == 5) {
                this.l.write(44);
            } else if (t != 3) {
                throw new IllegalStateException("Nesting problem.");
            }
            q();
            x(4);
            y(this.f3111s);
            this.f3111s = null;
        }
    }

    public final void a() throws IOException {
        int t = t();
        if (t == 1) {
            x(2);
            q();
        } else if (t == 2) {
            this.l.append(',');
            q();
        } else if (t != 4) {
            if (t != 6) {
                if (t != 7) {
                    throw new IllegalStateException("Nesting problem.");
                } else if (!this.q) {
                    throw new IllegalStateException("JSON must have only one top-level value.");
                }
            }
            x(7);
        } else {
            this.l.append((CharSequence) this.p);
            x(5);
        }
    }

    public JsonWriter b() throws IOException {
        J();
        a();
        u(1);
        this.l.write(91);
        return this;
    }

    public JsonWriter c() throws IOException {
        J();
        a();
        u(3);
        this.l.write(123);
        return this;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.l.close();
        int i = this.n;
        if (i > 1 || (i == 1 && this.m[i - 1] != 7)) {
            throw new IOException("Incomplete document");
        }
        this.n = 0;
    }

    public final JsonWriter d(int i, int i2, char c) throws IOException {
        int t = t();
        if (t != i2 && t != i) {
            throw new IllegalStateException("Nesting problem.");
        } else if (this.f3111s == null) {
            this.n--;
            if (t == i2) {
                q();
            }
            this.l.write(c);
            return this;
        } else {
            StringBuilder R = a.R("Dangling name: ");
            R.append(this.f3111s);
            throw new IllegalStateException(R.toString());
        }
    }

    public JsonWriter e() throws IOException {
        d(1, 2, ']');
        return this;
    }

    public JsonWriter f() throws IOException {
        d(3, 5, '}');
        return this;
    }

    public void flush() throws IOException {
        if (this.n != 0) {
            this.l.flush();
            return;
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    public JsonWriter n(String str) throws IOException {
        Objects.requireNonNull(str, "name == null");
        if (this.f3111s != null) {
            throw new IllegalStateException();
        } else if (this.n != 0) {
            this.f3111s = str;
            return this;
        } else {
            throw new IllegalStateException("JsonWriter is closed.");
        }
    }

    public final void q() throws IOException {
        if (this.o != null) {
            this.l.write(10);
            int i = this.n;
            for (int i2 = 1; i2 < i; i2++) {
                this.l.write(this.o);
            }
        }
    }

    public JsonWriter s() throws IOException {
        if (this.f3111s != null) {
            if (this.t) {
                J();
            } else {
                this.f3111s = null;
                return this;
            }
        }
        a();
        this.l.write("null");
        return this;
    }

    public final int t() {
        int i = this.n;
        if (i != 0) {
            return this.m[i - 1];
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    public final void u(int i) {
        int i2 = this.n;
        int[] iArr = this.m;
        if (i2 == iArr.length) {
            this.m = Arrays.copyOf(iArr, i2 * 2);
        }
        int[] iArr2 = this.m;
        int i3 = this.n;
        this.n = i3 + 1;
        iArr2[i3] = i;
    }

    public final void x(int i) {
        this.m[this.n - 1] = i;
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x0034  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void y(java.lang.String r9) throws java.io.IOException {
        /*
            r8 = this;
            boolean r0 = r8.r
            if (r0 == 0) goto L7
            java.lang.String[] r0 = com.google.gson.stream.JsonWriter.k
            goto L9
        L7:
            java.lang.String[] r0 = com.google.gson.stream.JsonWriter.j
        L9:
            java.io.Writer r1 = r8.l
            r2 = 34
            r1.write(r2)
            int r1 = r9.length()
            r3 = 0
            r4 = 0
        L16:
            if (r3 >= r1) goto L45
            char r5 = r9.charAt(r3)
            r6 = 128(0x80, float:1.794E-43)
            if (r5 >= r6) goto L25
            r5 = r0[r5]
            if (r5 != 0) goto L32
            goto L42
        L25:
            r6 = 8232(0x2028, float:1.1535E-41)
            if (r5 != r6) goto L2c
            java.lang.String r5 = "\\u2028"
            goto L32
        L2c:
            r6 = 8233(0x2029, float:1.1537E-41)
            if (r5 != r6) goto L42
            java.lang.String r5 = "\\u2029"
        L32:
            if (r4 >= r3) goto L3b
            java.io.Writer r6 = r8.l
            int r7 = r3 - r4
            r6.write(r9, r4, r7)
        L3b:
            java.io.Writer r4 = r8.l
            r4.write(r5)
            int r4 = r3 + 1
        L42:
            int r3 = r3 + 1
            goto L16
        L45:
            if (r4 >= r1) goto L4d
            java.io.Writer r0 = r8.l
            int r1 = r1 - r4
            r0.write(r9, r4, r1)
        L4d:
            java.io.Writer r9 = r8.l
            r9.write(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.stream.JsonWriter.y(java.lang.String):void");
    }
}
