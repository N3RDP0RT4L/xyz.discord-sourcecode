package com.google.gson.stream;

import andhook.lib.xposed.ClassUtils;
import b.i.d.q.p;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.Objects;
/* loaded from: classes3.dex */
public class JsonReader implements Closeable {
    public static final char[] j = ")]}'\n".toCharArray();
    public final Reader k;

    /* renamed from: s  reason: collision with root package name */
    public long f3107s;
    public int t;
    public String u;
    public int[] v;
    public int w;
    public boolean l = false;
    public final char[] m = new char[1024];
    public int n = 0;
    public int o = 0;
    public int p = 0;
    public int q = 0;
    public int r = 0;

    /* renamed from: x  reason: collision with root package name */
    public String[] f3108x = new String[32];

    /* renamed from: y  reason: collision with root package name */
    public int[] f3109y = new int[32];

    /* loaded from: classes3.dex */
    public class a extends p {
    }

    static {
        p.a = new a();
    }

    public JsonReader(Reader reader) {
        int[] iArr = new int[32];
        this.v = iArr;
        this.w = 0;
        this.w = 0 + 1;
        iArr[0] = 6;
        Objects.requireNonNull(reader, "in == null");
        this.k = reader;
    }

    public long A() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 15) {
            this.r = 0;
            int[] iArr = this.f3109y;
            int i2 = this.w - 1;
            iArr[i2] = iArr[i2] + 1;
            return this.f3107s;
        }
        if (i == 16) {
            this.u = new String(this.m, this.n, this.t);
            this.n += this.t;
        } else if (i == 8 || i == 9 || i == 10) {
            if (i == 10) {
                this.u = L();
            } else {
                this.u = I(i == 8 ? '\'' : '\"');
            }
            try {
                long parseLong = Long.parseLong(this.u);
                this.r = 0;
                int[] iArr2 = this.f3109y;
                int i3 = this.w - 1;
                iArr2[i3] = iArr2[i3] + 1;
                return parseLong;
            } catch (NumberFormatException unused) {
            }
        } else {
            StringBuilder R = b.d.b.a.a.R("Expected a long but was ");
            R.append(N());
            R.append(t());
            throw new IllegalStateException(R.toString());
        }
        this.r = 11;
        double parseDouble = Double.parseDouble(this.u);
        long j2 = (long) parseDouble;
        if (j2 == parseDouble) {
            this.u = null;
            this.r = 0;
            int[] iArr3 = this.f3109y;
            int i4 = this.w - 1;
            iArr3[i4] = iArr3[i4] + 1;
            return j2;
        }
        StringBuilder R2 = b.d.b.a.a.R("Expected a long but was ");
        R2.append(this.u);
        R2.append(t());
        throw new NumberFormatException(R2.toString());
    }

    public String C() throws IOException {
        String str;
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 14) {
            str = L();
        } else if (i == 12) {
            str = I('\'');
        } else if (i == 13) {
            str = I('\"');
        } else {
            StringBuilder R = b.d.b.a.a.R("Expected a name but was ");
            R.append(N());
            R.append(t());
            throw new IllegalStateException(R.toString());
        }
        this.r = 0;
        this.f3108x[this.w - 1] = str;
        return str;
    }

    public final int D(boolean z2) throws IOException {
        char[] cArr = this.m;
        int i = this.n;
        int i2 = this.o;
        while (true) {
            boolean z3 = true;
            if (i == i2) {
                this.n = i;
                if (n(1)) {
                    i = this.n;
                    i2 = this.o;
                } else if (!z2) {
                    return -1;
                } else {
                    StringBuilder R = b.d.b.a.a.R("End of input");
                    R.append(t());
                    throw new EOFException(R.toString());
                }
            }
            int i3 = i + 1;
            char c = cArr[i];
            if (c == '\n') {
                this.p++;
                this.q = i3;
            } else if (!(c == ' ' || c == '\r' || c == '\t')) {
                if (c == '/') {
                    this.n = i3;
                    if (i3 == i2) {
                        this.n = i3 - 1;
                        boolean n = n(2);
                        this.n++;
                        if (!n) {
                            return c;
                        }
                    }
                    c();
                    int i4 = this.n;
                    char c2 = cArr[i4];
                    if (c2 == '*') {
                        this.n = i4 + 1;
                        while (true) {
                            if (this.n + 2 > this.o && !n(2)) {
                                z3 = false;
                                break;
                            }
                            char[] cArr2 = this.m;
                            int i5 = this.n;
                            if (cArr2[i5] != '\n') {
                                for (int i6 = 0; i6 < 2; i6++) {
                                    if (this.m[this.n + i6] != "*/".charAt(i6)) {
                                        break;
                                    }
                                }
                                break;
                            }
                            this.p++;
                            this.q = i5 + 1;
                            this.n++;
                        }
                        if (z3) {
                            i = this.n + 2;
                            i2 = this.o;
                        } else {
                            V("Unterminated comment");
                            throw null;
                        }
                    } else if (c2 != '/') {
                        return c;
                    } else {
                        this.n = i4 + 1;
                        T();
                        i = this.n;
                        i2 = this.o;
                    }
                } else if (c == '#') {
                    this.n = i3;
                    c();
                    T();
                    i = this.n;
                    i2 = this.o;
                } else {
                    this.n = i3;
                    return c;
                }
            }
            i = i3;
        }
    }

    public void H() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 7) {
            this.r = 0;
            int[] iArr = this.f3109y;
            int i2 = this.w - 1;
            iArr[i2] = iArr[i2] + 1;
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Expected null but was ");
        R.append(N());
        R.append(t());
        throw new IllegalStateException(R.toString());
    }

    /* JADX WARN: Code restructure failed: missing block: B:25:0x005d, code lost:
        if (r2 != null) goto L27;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x005f, code lost:
        r2 = new java.lang.StringBuilder(java.lang.Math.max((r3 - r4) * 2, 16));
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x006d, code lost:
        r2.append(r0, r4, r3 - r4);
        r10.n = r3;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.String I(char r11) throws java.io.IOException {
        /*
            r10 = this;
            char[] r0 = r10.m
            r1 = 0
            r2 = r1
        L4:
            int r3 = r10.n
            int r4 = r10.o
        L8:
            r5 = r4
            r4 = r3
        La:
            r6 = 16
            r7 = 1
            if (r3 >= r5) goto L5d
            int r8 = r3 + 1
            char r3 = r0[r3]
            if (r3 != r11) goto L29
            r10.n = r8
            int r8 = r8 - r4
            int r8 = r8 - r7
            if (r2 != 0) goto L21
            java.lang.String r11 = new java.lang.String
            r11.<init>(r0, r4, r8)
            return r11
        L21:
            r2.append(r0, r4, r8)
            java.lang.String r11 = r2.toString()
            return r11
        L29:
            r9 = 92
            if (r3 != r9) goto L50
            r10.n = r8
            int r8 = r8 - r4
            int r8 = r8 - r7
            if (r2 != 0) goto L41
            int r2 = r8 + 1
            int r2 = r2 * 2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            int r2 = java.lang.Math.max(r2, r6)
            r3.<init>(r2)
            r2 = r3
        L41:
            r2.append(r0, r4, r8)
            char r3 = r10.R()
            r2.append(r3)
            int r3 = r10.n
            int r4 = r10.o
            goto L8
        L50:
            r6 = 10
            if (r3 != r6) goto L5b
            int r3 = r10.p
            int r3 = r3 + r7
            r10.p = r3
            r10.q = r8
        L5b:
            r3 = r8
            goto La
        L5d:
            if (r2 != 0) goto L6d
            int r2 = r3 - r4
            int r2 = r2 * 2
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            int r2 = java.lang.Math.max(r2, r6)
            r5.<init>(r2)
            r2 = r5
        L6d:
            int r5 = r3 - r4
            r2.append(r0, r4, r5)
            r10.n = r3
            boolean r3 = r10.n(r7)
            if (r3 == 0) goto L7b
            goto L4
        L7b:
            java.lang.String r11 = "Unterminated string"
            r10.V(r11)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.stream.JsonReader.I(char):java.lang.String");
    }

    public String J() throws IOException {
        String str;
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 10) {
            str = L();
        } else if (i == 8) {
            str = I('\'');
        } else if (i == 9) {
            str = I('\"');
        } else if (i == 11) {
            str = this.u;
            this.u = null;
        } else if (i == 15) {
            str = Long.toString(this.f3107s);
        } else if (i == 16) {
            str = new String(this.m, this.n, this.t);
            this.n += this.t;
        } else {
            StringBuilder R = b.d.b.a.a.R("Expected a string but was ");
            R.append(N());
            R.append(t());
            throw new IllegalStateException(R.toString());
        }
        this.r = 0;
        int[] iArr = this.f3109y;
        int i2 = this.w - 1;
        iArr[i2] = iArr[i2] + 1;
        return str;
    }

    /* JADX WARN: Code restructure failed: missing block: B:34:0x004a, code lost:
        c();
     */
    /* JADX WARN: Removed duplicated region for block: B:45:0x0080  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x008a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.String L() throws java.io.IOException {
        /*
            r6 = this;
            r0 = 0
            r1 = 0
        L2:
            r2 = 0
        L3:
            int r3 = r6.n
            int r4 = r3 + r2
            int r5 = r6.o
            if (r4 >= r5) goto L4e
            char[] r4 = r6.m
            int r3 = r3 + r2
            char r3 = r4[r3]
            r4 = 9
            if (r3 == r4) goto L5c
            r4 = 10
            if (r3 == r4) goto L5c
            r4 = 12
            if (r3 == r4) goto L5c
            r4 = 13
            if (r3 == r4) goto L5c
            r4 = 32
            if (r3 == r4) goto L5c
            r4 = 35
            if (r3 == r4) goto L4a
            r4 = 44
            if (r3 == r4) goto L5c
            r4 = 47
            if (r3 == r4) goto L4a
            r4 = 61
            if (r3 == r4) goto L4a
            r4 = 123(0x7b, float:1.72E-43)
            if (r3 == r4) goto L5c
            r4 = 125(0x7d, float:1.75E-43)
            if (r3 == r4) goto L5c
            r4 = 58
            if (r3 == r4) goto L5c
            r4 = 59
            if (r3 == r4) goto L4a
            switch(r3) {
                case 91: goto L5c;
                case 92: goto L4a;
                case 93: goto L5c;
                default: goto L47;
            }
        L47:
            int r2 = r2 + 1
            goto L3
        L4a:
            r6.c()
            goto L5c
        L4e:
            char[] r3 = r6.m
            int r3 = r3.length
            if (r2 >= r3) goto L5e
            int r3 = r2 + 1
            boolean r3 = r6.n(r3)
            if (r3 == 0) goto L5c
            goto L3
        L5c:
            r0 = r2
            goto L7e
        L5e:
            if (r1 != 0) goto L6b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r3 = 16
            int r3 = java.lang.Math.max(r2, r3)
            r1.<init>(r3)
        L6b:
            char[] r3 = r6.m
            int r4 = r6.n
            r1.append(r3, r4, r2)
            int r3 = r6.n
            int r3 = r3 + r2
            r6.n = r3
            r2 = 1
            boolean r2 = r6.n(r2)
            if (r2 != 0) goto L2
        L7e:
            if (r1 != 0) goto L8a
            java.lang.String r1 = new java.lang.String
            char[] r2 = r6.m
            int r3 = r6.n
            r1.<init>(r2, r3, r0)
            goto L95
        L8a:
            char[] r2 = r6.m
            int r3 = r6.n
            r1.append(r2, r3, r0)
            java.lang.String r1 = r1.toString()
        L95:
            int r2 = r6.n
            int r2 = r2 + r0
            r6.n = r2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.stream.JsonReader.L():java.lang.String");
    }

    public JsonToken N() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        switch (i) {
            case 1:
                return JsonToken.BEGIN_OBJECT;
            case 2:
                return JsonToken.END_OBJECT;
            case 3:
                return JsonToken.BEGIN_ARRAY;
            case 4:
                return JsonToken.END_ARRAY;
            case 5:
            case 6:
                return JsonToken.BOOLEAN;
            case 7:
                return JsonToken.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return JsonToken.STRING;
            case 12:
            case 13:
            case 14:
                return JsonToken.NAME;
            case 15:
            case 16:
                return JsonToken.NUMBER;
            case 17:
                return JsonToken.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    public final void O(int i) {
        int i2 = this.w;
        int[] iArr = this.v;
        if (i2 == iArr.length) {
            int i3 = i2 * 2;
            this.v = Arrays.copyOf(iArr, i3);
            this.f3109y = Arrays.copyOf(this.f3109y, i3);
            this.f3108x = (String[]) Arrays.copyOf(this.f3108x, i3);
        }
        int[] iArr2 = this.v;
        int i4 = this.w;
        this.w = i4 + 1;
        iArr2[i4] = i;
    }

    public final char R() throws IOException {
        int i;
        int i2;
        if (this.n != this.o || n(1)) {
            char[] cArr = this.m;
            int i3 = this.n;
            int i4 = i3 + 1;
            this.n = i4;
            char c = cArr[i3];
            if (c == '\n') {
                this.p++;
                this.q = i4;
            } else if (!(c == '\"' || c == '\'' || c == '/' || c == '\\')) {
                if (c == 'b') {
                    return '\b';
                }
                if (c == 'f') {
                    return '\f';
                }
                if (c == 'n') {
                    return '\n';
                }
                if (c == 'r') {
                    return '\r';
                }
                if (c == 't') {
                    return '\t';
                }
                if (c != 'u') {
                    V("Invalid escape sequence");
                    throw null;
                } else if (i4 + 4 <= this.o || n(4)) {
                    char c2 = 0;
                    int i5 = this.n;
                    int i6 = i5 + 4;
                    while (i5 < i6) {
                        char c3 = this.m[i5];
                        char c4 = (char) (c2 << 4);
                        if (c3 < '0' || c3 > '9') {
                            if (c3 >= 'a' && c3 <= 'f') {
                                i2 = c3 - 'a';
                            } else if (c3 < 'A' || c3 > 'F') {
                                StringBuilder R = b.d.b.a.a.R("\\u");
                                R.append(new String(this.m, this.n, 4));
                                throw new NumberFormatException(R.toString());
                            } else {
                                i2 = c3 - 'A';
                            }
                            i = i2 + 10;
                        } else {
                            i = c3 - '0';
                        }
                        c2 = (char) (i + c4);
                        i5++;
                    }
                    this.n += 4;
                    return c2;
                } else {
                    V("Unterminated escape sequence");
                    throw null;
                }
            }
            return c;
        }
        V("Unterminated escape sequence");
        throw null;
    }

    public final void S(char c) throws IOException {
        char[] cArr = this.m;
        do {
            int i = this.n;
            int i2 = this.o;
            while (i < i2) {
                int i3 = i + 1;
                char c2 = cArr[i];
                if (c2 == c) {
                    this.n = i3;
                    return;
                } else if (c2 == '\\') {
                    this.n = i3;
                    R();
                    i = this.n;
                    i2 = this.o;
                } else {
                    if (c2 == '\n') {
                        this.p++;
                        this.q = i3;
                    }
                    i = i3;
                }
            }
            this.n = i;
        } while (n(1));
        V("Unterminated string");
        throw null;
    }

    public final void T() throws IOException {
        char c;
        do {
            if (this.n < this.o || n(1)) {
                char[] cArr = this.m;
                int i = this.n;
                int i2 = i + 1;
                this.n = i2;
                c = cArr[i];
                if (c == '\n') {
                    this.p++;
                    this.q = i2;
                    return;
                }
            } else {
                return;
            }
        } while (c != '\r');
    }

    public void U() throws IOException {
        int i = 0;
        do {
            int i2 = this.r;
            if (i2 == 0) {
                i2 = d();
            }
            if (i2 == 3) {
                O(1);
            } else if (i2 == 1) {
                O(3);
            } else {
                if (i2 == 4) {
                    this.w--;
                } else if (i2 == 2) {
                    this.w--;
                } else {
                    if (i2 == 14 || i2 == 10) {
                        do {
                            int i3 = 0;
                            while (true) {
                                int i4 = this.n + i3;
                                if (i4 < this.o) {
                                    char c = this.m[i4];
                                    if (!(c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ')) {
                                        if (c != '#') {
                                            if (c != ',') {
                                                if (!(c == '/' || c == '=')) {
                                                    if (!(c == '{' || c == '}' || c == ':')) {
                                                        if (c != ';') {
                                                            switch (c) {
                                                                case '[':
                                                                case ']':
                                                                    break;
                                                                case '\\':
                                                                    break;
                                                                default:
                                                                    i3++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    this.n = i4;
                                }
                            }
                            c();
                            this.n += i3;
                        } while (n(1));
                    } else if (i2 == 8 || i2 == 12) {
                        S('\'');
                    } else if (i2 == 9 || i2 == 13) {
                        S('\"');
                    } else if (i2 == 16) {
                        this.n += this.t;
                    }
                    this.r = 0;
                }
                i--;
                this.r = 0;
            }
            i++;
            this.r = 0;
        } while (i != 0);
        int[] iArr = this.f3109y;
        int i5 = this.w;
        int i6 = i5 - 1;
        iArr[i6] = iArr[i6] + 1;
        this.f3108x[i5 - 1] = "null";
    }

    public final IOException V(String str) throws IOException {
        StringBuilder R = b.d.b.a.a.R(str);
        R.append(t());
        throw new MalformedJsonException(R.toString());
    }

    public void a() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 3) {
            O(1);
            this.f3109y[this.w - 1] = 0;
            this.r = 0;
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Expected BEGIN_ARRAY but was ");
        R.append(N());
        R.append(t());
        throw new IllegalStateException(R.toString());
    }

    public void b() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 1) {
            O(3);
            this.r = 0;
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Expected BEGIN_OBJECT but was ");
        R.append(N());
        R.append(t());
        throw new IllegalStateException(R.toString());
    }

    public final void c() throws IOException {
        if (!this.l) {
            V("Use JsonReader.setLenient(true) to accept malformed JSON");
            throw null;
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.r = 0;
        this.v[0] = 8;
        this.w = 1;
        this.k.close();
    }

    /* JADX WARN: Code restructure failed: missing block: B:146:0x0203, code lost:
        if (s(r6) != false) goto L106;
     */
    /* JADX WARN: Code restructure failed: missing block: B:147:0x0205, code lost:
        if (r13 != 2) goto L158;
     */
    /* JADX WARN: Code restructure failed: missing block: B:148:0x0207, code lost:
        if (r15 == false) goto L158;
     */
    /* JADX WARN: Code restructure failed: missing block: B:150:0x020d, code lost:
        if (r10 != Long.MIN_VALUE) goto L152;
     */
    /* JADX WARN: Code restructure failed: missing block: B:151:0x020f, code lost:
        if (r16 == false) goto L158;
     */
    /* JADX WARN: Code restructure failed: missing block: B:153:0x0215, code lost:
        if (r10 != 0) goto L155;
     */
    /* JADX WARN: Code restructure failed: missing block: B:154:0x0217, code lost:
        if (r16 != false) goto L158;
     */
    /* JADX WARN: Code restructure failed: missing block: B:155:0x0219, code lost:
        if (r16 == false) goto L156;
     */
    /* JADX WARN: Code restructure failed: missing block: B:156:0x021c, code lost:
        r10 = -r10;
     */
    /* JADX WARN: Code restructure failed: missing block: B:157:0x021d, code lost:
        r19.f3107s = r10;
        r19.n += r9;
        r6 = 15;
        r19.r = 15;
     */
    /* JADX WARN: Code restructure failed: missing block: B:158:0x0229, code lost:
        if (r13 == 2) goto L163;
     */
    /* JADX WARN: Code restructure failed: missing block: B:160:0x022c, code lost:
        if (r13 == 4) goto L163;
     */
    /* JADX WARN: Code restructure failed: missing block: B:162:0x022f, code lost:
        if (r13 != 7) goto L106;
     */
    /* JADX WARN: Code restructure failed: missing block: B:163:0x0231, code lost:
        r19.t = r9;
        r6 = 16;
        r19.r = 16;
     */
    /* JADX WARN: Removed duplicated region for block: B:100:0x0174 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:101:0x0175  */
    /* JADX WARN: Removed duplicated region for block: B:181:0x0264 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:182:0x0265  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public int d() throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 793
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.stream.JsonReader.d():int");
    }

    public void e() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 4) {
            int i2 = this.w - 1;
            this.w = i2;
            int[] iArr = this.f3109y;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.r = 0;
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Expected END_ARRAY but was ");
        R.append(N());
        R.append(t());
        throw new IllegalStateException(R.toString());
    }

    public void f() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 2) {
            int i2 = this.w - 1;
            this.w = i2;
            this.f3108x[i2] = null;
            int[] iArr = this.f3109y;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.r = 0;
            return;
        }
        StringBuilder R = b.d.b.a.a.R("Expected END_OBJECT but was ");
        R.append(N());
        R.append(t());
        throw new IllegalStateException(R.toString());
    }

    public String getPath() {
        StringBuilder O = b.d.b.a.a.O(ClassUtils.INNER_CLASS_SEPARATOR_CHAR);
        int i = this.w;
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.v[i2];
            if (i3 == 1 || i3 == 2) {
                O.append('[');
                O.append(this.f3109y[i2]);
                O.append(']');
            } else if (i3 == 3 || i3 == 4 || i3 == 5) {
                O.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                String[] strArr = this.f3108x;
                if (strArr[i2] != null) {
                    O.append(strArr[i2]);
                }
            }
        }
        return O.toString();
    }

    public final boolean n(int i) throws IOException {
        int i2;
        int i3;
        char[] cArr = this.m;
        int i4 = this.q;
        int i5 = this.n;
        this.q = i4 - i5;
        int i6 = this.o;
        if (i6 != i5) {
            int i7 = i6 - i5;
            this.o = i7;
            System.arraycopy(cArr, i5, cArr, 0, i7);
        } else {
            this.o = 0;
        }
        this.n = 0;
        do {
            Reader reader = this.k;
            int i8 = this.o;
            int read = reader.read(cArr, i8, cArr.length - i8);
            if (read == -1) {
                return false;
            }
            i2 = this.o + read;
            this.o = i2;
            if (this.p == 0 && (i3 = this.q) == 0 && i2 > 0 && cArr[0] == 65279) {
                this.n++;
                this.q = i3 + 1;
                i++;
                continue;
            }
        } while (i2 < i);
        return true;
    }

    public boolean q() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        return (i == 2 || i == 4) ? false : true;
    }

    public final boolean s(char c) throws IOException {
        if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
            return false;
        }
        if (c != '#') {
            if (c == ',') {
                return false;
            }
            if (!(c == '/' || c == '=')) {
                if (c == '{' || c == '}' || c == ':') {
                    return false;
                }
                if (c != ';') {
                    switch (c) {
                        case '[':
                        case ']':
                            return false;
                        case '\\':
                            break;
                        default:
                            return true;
                    }
                }
            }
        }
        c();
        return false;
    }

    public String t() {
        StringBuilder U = b.d.b.a.a.U(" at line ", this.p + 1, " column ", (this.n - this.q) + 1, " path ");
        U.append(getPath());
        return U.toString();
    }

    public String toString() {
        return getClass().getSimpleName() + t();
    }

    public boolean u() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 5) {
            this.r = 0;
            int[] iArr = this.f3109y;
            int i2 = this.w - 1;
            iArr[i2] = iArr[i2] + 1;
            return true;
        } else if (i == 6) {
            this.r = 0;
            int[] iArr2 = this.f3109y;
            int i3 = this.w - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return false;
        } else {
            StringBuilder R = b.d.b.a.a.R("Expected a boolean but was ");
            R.append(N());
            R.append(t());
            throw new IllegalStateException(R.toString());
        }
    }

    public double x() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 15) {
            this.r = 0;
            int[] iArr = this.f3109y;
            int i2 = this.w - 1;
            iArr[i2] = iArr[i2] + 1;
            return this.f3107s;
        }
        if (i == 16) {
            this.u = new String(this.m, this.n, this.t);
            this.n += this.t;
        } else if (i == 8 || i == 9) {
            this.u = I(i == 8 ? '\'' : '\"');
        } else if (i == 10) {
            this.u = L();
        } else if (i != 11) {
            StringBuilder R = b.d.b.a.a.R("Expected a double but was ");
            R.append(N());
            R.append(t());
            throw new IllegalStateException(R.toString());
        }
        this.r = 11;
        double parseDouble = Double.parseDouble(this.u);
        if (this.l || (!Double.isNaN(parseDouble) && !Double.isInfinite(parseDouble))) {
            this.u = null;
            this.r = 0;
            int[] iArr2 = this.f3109y;
            int i3 = this.w - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return parseDouble;
        }
        throw new MalformedJsonException("JSON forbids NaN and infinities: " + parseDouble + t());
    }

    public int y() throws IOException {
        int i = this.r;
        if (i == 0) {
            i = d();
        }
        if (i == 15) {
            long j2 = this.f3107s;
            int i2 = (int) j2;
            if (j2 == i2) {
                this.r = 0;
                int[] iArr = this.f3109y;
                int i3 = this.w - 1;
                iArr[i3] = iArr[i3] + 1;
                return i2;
            }
            StringBuilder R = b.d.b.a.a.R("Expected an int but was ");
            R.append(this.f3107s);
            R.append(t());
            throw new NumberFormatException(R.toString());
        }
        if (i == 16) {
            this.u = new String(this.m, this.n, this.t);
            this.n += this.t;
        } else if (i == 8 || i == 9 || i == 10) {
            if (i == 10) {
                this.u = L();
            } else {
                this.u = I(i == 8 ? '\'' : '\"');
            }
            try {
                int parseInt = Integer.parseInt(this.u);
                this.r = 0;
                int[] iArr2 = this.f3109y;
                int i4 = this.w - 1;
                iArr2[i4] = iArr2[i4] + 1;
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        } else {
            StringBuilder R2 = b.d.b.a.a.R("Expected an int but was ");
            R2.append(N());
            R2.append(t());
            throw new IllegalStateException(R2.toString());
        }
        this.r = 11;
        double parseDouble = Double.parseDouble(this.u);
        int i5 = (int) parseDouble;
        if (i5 == parseDouble) {
            this.u = null;
            this.r = 0;
            int[] iArr3 = this.f3109y;
            int i6 = this.w - 1;
            iArr3[i6] = iArr3[i6] + 1;
            return i5;
        }
        StringBuilder R3 = b.d.b.a.a.R("Expected an int but was ");
        R3.append(this.u);
        R3.append(t());
        throw new NumberFormatException(R3.toString());
    }
}
