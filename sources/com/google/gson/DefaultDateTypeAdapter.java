package com.google.gson;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import b.i.d.q.o;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
/* loaded from: classes3.dex */
public final class DefaultDateTypeAdapter extends TypeAdapter<Date> {
    public final Class<? extends Date> a;

    /* renamed from: b  reason: collision with root package name */
    public final List<DateFormat> f3086b;

    public DefaultDateTypeAdapter(Class<? extends Date> cls, int i, int i2) {
        ArrayList arrayList = new ArrayList();
        this.f3086b = arrayList;
        a(cls);
        this.a = cls;
        Locale locale = Locale.US;
        arrayList.add(DateFormat.getDateTimeInstance(i, i2, locale));
        if (!Locale.getDefault().equals(locale)) {
            arrayList.add(DateFormat.getDateTimeInstance(i, i2));
        }
        if (o.a >= 9) {
            arrayList.add(f.r0(i, i2));
        }
    }

    public static Class<? extends Date> a(Class<? extends Date> cls) {
        if (cls == Date.class || cls == java.sql.Date.class || cls == Timestamp.class) {
            return cls;
        }
        throw new IllegalArgumentException("Date type must be one of " + Date.class + ", " + Timestamp.class + ", or " + java.sql.Date.class + " but was " + cls);
    }

    /* JADX WARN: Code restructure failed: missing block: B:13:0x002c, code lost:
        r4 = b.i.d.q.x.d.a.b(r4, new java.text.ParsePosition(0));
     */
    @Override // com.google.gson.TypeAdapter
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.util.Date read(com.google.gson.stream.JsonReader r4) throws java.io.IOException {
        /*
            r3 = this;
            com.google.gson.stream.JsonToken r0 = r4.N()
            com.google.gson.stream.JsonToken r1 = com.google.gson.stream.JsonToken.NULL
            if (r0 != r1) goto Ld
            r4.H()
            r4 = 0
            goto L5b
        Ld:
            java.lang.String r4 = r4.J()
            java.util.List<java.text.DateFormat> r0 = r3.f3086b
            monitor-enter(r0)
            java.util.List<java.text.DateFormat> r1 = r3.f3086b     // Catch: java.lang.Throwable -> L69
            java.util.Iterator r1 = r1.iterator()     // Catch: java.lang.Throwable -> L69
        L1a:
            boolean r2 = r1.hasNext()     // Catch: java.lang.Throwable -> L69
            if (r2 == 0) goto L2c
            java.lang.Object r2 = r1.next()     // Catch: java.lang.Throwable -> L69
            java.text.DateFormat r2 = (java.text.DateFormat) r2     // Catch: java.lang.Throwable -> L69
            java.util.Date r4 = r2.parse(r4)     // Catch: java.text.ParseException -> L1a java.lang.Throwable -> L69
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L69
            goto L37
        L2c:
            java.text.ParsePosition r1 = new java.text.ParsePosition     // Catch: java.text.ParseException -> L62 java.lang.Throwable -> L69
            r2 = 0
            r1.<init>(r2)     // Catch: java.text.ParseException -> L62 java.lang.Throwable -> L69
            java.util.Date r4 = b.i.d.q.x.d.a.b(r4, r1)     // Catch: java.text.ParseException -> L62 java.lang.Throwable -> L69
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L69
        L37:
            java.lang.Class<? extends java.util.Date> r0 = r3.a
            java.lang.Class<java.util.Date> r1 = java.util.Date.class
            if (r0 != r1) goto L3e
            goto L5b
        L3e:
            java.lang.Class<java.sql.Timestamp> r1 = java.sql.Timestamp.class
            if (r0 != r1) goto L4d
            java.sql.Timestamp r0 = new java.sql.Timestamp
            long r1 = r4.getTime()
            r0.<init>(r1)
        L4b:
            r4 = r0
            goto L5b
        L4d:
            java.lang.Class<java.sql.Date> r1 = java.sql.Date.class
            if (r0 != r1) goto L5c
            java.sql.Date r0 = new java.sql.Date
            long r1 = r4.getTime()
            r0.<init>(r1)
            goto L4b
        L5b:
            return r4
        L5c:
            java.lang.AssertionError r4 = new java.lang.AssertionError
            r4.<init>()
            throw r4
        L62:
            r1 = move-exception
            com.google.gson.JsonSyntaxException r2 = new com.google.gson.JsonSyntaxException     // Catch: java.lang.Throwable -> L69
            r2.<init>(r4, r1)     // Catch: java.lang.Throwable -> L69
            throw r2     // Catch: java.lang.Throwable -> L69
        L69:
            r4 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> L69
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.DefaultDateTypeAdapter.read(com.google.gson.stream.JsonReader):java.lang.Object");
    }

    public String toString() {
        DateFormat dateFormat = this.f3086b.get(0);
        if (dateFormat instanceof SimpleDateFormat) {
            StringBuilder R = a.R("DefaultDateTypeAdapter(");
            R.append(((SimpleDateFormat) dateFormat).toPattern());
            R.append(')');
            return R.toString();
        }
        StringBuilder R2 = a.R("DefaultDateTypeAdapter(");
        R2.append(dateFormat.getClass().getSimpleName());
        R2.append(')');
        return R2.toString();
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, Date date) throws IOException {
        Date date2 = date;
        if (date2 == null) {
            jsonWriter.s();
            return;
        }
        synchronized (this.f3086b) {
            jsonWriter.H(this.f3086b.get(0).format(date2));
        }
    }
}
