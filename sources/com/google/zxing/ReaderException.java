package com.google.zxing;
/* loaded from: classes3.dex */
public abstract class ReaderException extends Exception {
    public static final boolean j;
    public static final StackTraceElement[] k;

    static {
        j = System.getProperty("surefire.test.class.path") != null;
        k = new StackTraceElement[0];
    }

    public ReaderException() {
    }

    @Override // java.lang.Throwable
    public final synchronized Throwable fillInStackTrace() {
        return null;
    }

    public ReaderException(Throwable th) {
        super(th);
    }
}
