package com.lyft.kronos;

import b.m.a.b;
import kotlin.Metadata;
/* compiled from: Clock.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"Lcom/lyft/kronos/KronosClock;", "Lb/m/a/b;", "", "a", "()J", "kronos-java"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes3.dex */
public interface KronosClock extends b {
    @Override // b.m.a.b
    long a();
}
