package com.lytefast.flexinput.model;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.CallSuper;
import androidx.core.app.NotificationCompat;
import androidx.core.view.inputmethod.InputContentInfoCompat;
import androidx.exifinterface.media.ExifInterface;
import b.c.a.a0.d;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.g0.w;
import d0.z.d.m;
import java.io.File;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Attachment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\n\b\u0016\u0018\u0000 /*\u0006\b\u0000\u0010\u0001 \u00012\u00020\u0002:\u0001/B5\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\n\b\u0002\u0010\"\u001a\u0004\u0018\u00018\u0000\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0005¢\u0006\u0004\b+\u0010,B\u0011\b\u0016\u0012\u0006\u0010-\u001a\u00020\f¢\u0006\u0004\b+\u0010.J\u001a\u0010\u0006\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0096\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\bH\u0016¢\u0006\u0004\b\u000b\u0010\nJ\u001f\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\bH\u0017¢\u0006\u0004\b\u0010\u0010\u0011R\"\u0010\u0012\u001a\u00020\u00058\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0019\u0010\u0019\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0019\u0010\u001e\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u001b\u0010\"\u001a\u0004\u0018\u00018\u00008\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u0019\u0010'\u001a\u00020&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*¨\u00060"}, d2 = {"Lcom/lytefast/flexinput/model/Attachment;", ExifInterface.GPS_DIRECTION_TRUE, "Landroid/os/Parcelable;", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "hashCode", "()I", "describeContents", "Landroid/os/Parcel;", "dest", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "spoiler", "Z", "getSpoiler", "()Z", "setSpoiler", "(Z)V", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "Landroid/net/Uri;", "getUri", "()Landroid/net/Uri;", "", "displayName", "Ljava/lang/String;", "getDisplayName", "()Ljava/lang/String;", "data", "Ljava/lang/Object;", "getData", "()Ljava/lang/Object;", "", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", HookHelper.constructorName, "(JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/Object;Z)V", "parcelIn", "(Landroid/os/Parcel;)V", "Companion", "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public class Attachment<T> implements Parcelable {
    private static final String SPOILER_PREFIX = "SPOILER_";
    private final T data;
    private final String displayName;

    /* renamed from: id  reason: collision with root package name */
    private final long f3140id;
    private boolean spoiler;
    private final Uri uri;
    public static final Companion Companion = new Companion(null);
    public static final Parcelable.Creator<Attachment<?>> CREATOR = new a();

    /* compiled from: Attachment.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0015J\u0015\u0010\u0004\u001a\u00020\u0003*\u0006\u0012\u0002\b\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J!\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002*\u00020\u00062\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\t\u0010\nJ5\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0002*\u00020\u000b2\u0006\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\r\u001a\u00020\f2\b\b\u0002\u0010\u000e\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u000f\u0010\u0010R&\u0010\u0012\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00020\u00118\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u0012\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u00020\u00038\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u0019"}, d2 = {"Lcom/lytefast/flexinput/model/Attachment$Companion;", "", "Lcom/lytefast/flexinput/model/Attachment;", "", "a", "(Lcom/lytefast/flexinput/model/Attachment;)Ljava/lang/String;", "Landroid/net/Uri;", "Landroid/content/ContentResolver;", "resolver", "b", "(Landroid/net/Uri;Landroid/content/ContentResolver;)Lcom/lytefast/flexinput/model/Attachment;", "Landroidx/core/view/inputmethod/InputContentInfoCompat;", "", "appendExtension", "defaultName", "c", "(Landroidx/core/view/inputmethod/InputContentInfoCompat;Landroid/content/ContentResolver;ZLjava/lang/String;)Lcom/lytefast/flexinput/model/Attachment;", "Landroid/os/Parcelable$Creator;", "CREATOR", "Landroid/os/Parcelable$Creator;", "getCREATOR$annotations", "()V", "SPOILER_PREFIX", "Ljava/lang/String;", HookHelper.constructorName, "flexinput_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes3.dex */
    public static final class Companion {
        public Companion() {
        }

        public final String a(Attachment<?> attachment) {
            m.checkNotNullParameter(attachment, "$this$getSendName");
            if (!attachment.getSpoiler()) {
                return attachment.getDisplayName();
            }
            StringBuilder R = b.d.b.a.a.R("SPOILER_");
            R.append(attachment.getDisplayName());
            return R.toString();
        }

        /* JADX WARN: Removed duplicated region for block: B:33:0x00aa  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final com.lytefast.flexinput.model.Attachment<android.net.Uri> b(android.net.Uri r14, android.content.ContentResolver r15) {
            /*
                r13 = this;
                java.lang.String r0 = "$this$toAttachment"
                d0.z.d.m.checkNotNullParameter(r14, r0)
                java.lang.String r0 = "resolver"
                d0.z.d.m.checkNotNullParameter(r15, r0)
                java.lang.String r0 = "$this$getFileName"
                d0.z.d.m.checkNotNullParameter(r14, r0)
                java.lang.String r0 = "contentResolver"
                d0.z.d.m.checkNotNullParameter(r15, r0)
                java.lang.String r0 = r14.getScheme()
                if (r0 != 0) goto L1c
                goto La3
            L1c:
                int r1 = r0.hashCode()
                r2 = 3143036(0x2ff57c, float:4.404332E-39)
                r3 = 0
                if (r1 == r2) goto L8b
                r2 = 951530617(0x38b73479, float:8.735894E-5)
                if (r1 == r2) goto L2d
                goto La3
            L2d:
                java.lang.String r1 = "content"
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto La3
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                r4 = r15
                r5 = r14
                android.database.Cursor r15 = r4.query(r5, r6, r7, r8, r9)     // Catch: java.lang.NullPointerException -> L6f
                if (r15 == 0) goto L6a
                boolean r0 = r15.moveToFirst()     // Catch: java.lang.Throwable -> L63
                if (r0 == 0) goto L5f
                java.lang.String r0 = "_display_name"
                int r0 = r15.getColumnIndex(r0)     // Catch: java.lang.Throwable -> L63
                if (r0 < 0) goto L5f
                java.lang.String r0 = r15.getString(r0)     // Catch: java.lang.Throwable -> L63
                if (r0 == 0) goto L56
                goto L5a
            L56:
                java.lang.String r0 = r14.getLastPathSegment()     // Catch: java.lang.Throwable -> L63
            L5a:
                d0.y.b.closeFinally(r15, r3)     // Catch: java.lang.NullPointerException -> L6f
                r3 = r0
                goto La7
            L5f:
                d0.y.b.closeFinally(r15, r3)     // Catch: java.lang.NullPointerException -> L6f
                goto L6a
            L63:
                r0 = move-exception
                throw r0     // Catch: java.lang.Throwable -> L65
            L65:
                r1 = move-exception
                d0.y.b.closeFinally(r15, r0)     // Catch: java.lang.NullPointerException -> L6f
                throw r1     // Catch: java.lang.NullPointerException -> L6f
            L6a:
                java.lang.String r3 = r14.getLastPathSegment()
                goto La7
            L6f:
                r15 = move-exception
                java.lang.String r0 = "Error getting file name for: "
                java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
                java.lang.String r1 = r14.getPath()
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                java.lang.String r1 = "FileUtils"
                android.util.Log.e(r1, r0, r15)
                java.lang.String r3 = r14.getLastPathSegment()
                goto La7
            L8b:
                java.lang.String r15 = "file"
                boolean r15 = r0.equals(r15)
                if (r15 == 0) goto La3
                java.io.File r15 = new java.io.File
                java.lang.String r0 = r14.getPath()
                if (r0 == 0) goto La7
                r15.<init>(r0)
                java.lang.String r3 = r15.getName()
                goto La7
            La3:
                java.lang.String r3 = r14.getLastPathSegment()
            La7:
                if (r3 == 0) goto Laa
                goto Lb2
            Laa:
                int r15 = r14.hashCode()
                java.lang.String r3 = java.lang.String.valueOf(r15)
            Lb2:
                r8 = r3
                com.lytefast.flexinput.model.Attachment r15 = new com.lytefast.flexinput.model.Attachment
                int r0 = r14.hashCode()
                long r5 = (long) r0
                r9 = 0
                r10 = 0
                r11 = 16
                r12 = 0
                r4 = r15
                r7 = r14
                r4.<init>(r5, r7, r8, r9, r10, r11, r12)
                return r15
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lytefast.flexinput.model.Attachment.Companion.b(android.net.Uri, android.content.ContentResolver):com.lytefast.flexinput.model.Attachment");
        }

        public final Attachment<InputContentInfoCompat> c(InputContentInfoCompat inputContentInfoCompat, ContentResolver contentResolver, boolean z2, String str) {
            String str2;
            m.checkNotNullParameter(inputContentInfoCompat, "$this$toAttachment");
            m.checkNotNullParameter(contentResolver, "resolver");
            m.checkNotNullParameter(str, "defaultName");
            String queryParameter = inputContentInfoCompat.getContentUri().getQueryParameter("fileName");
            if (queryParameter != null) {
                str = queryParameter;
            }
            m.checkNotNullExpressionValue(str, "contentUri.getQueryParam…fileName\") ?: defaultName");
            String substringAfterLast$default = w.substringAfterLast$default(str, File.separatorChar, null, 2, null);
            if (z2) {
                String mimeType = inputContentInfoCompat.getDescription().getMimeType(0);
                if (mimeType == null) {
                    mimeType = inputContentInfoCompat.getContentUri().getQueryParameter("mimeType");
                }
                if (mimeType == null) {
                    mimeType = contentResolver.getType(inputContentInfoCompat.getContentUri());
                }
                if (mimeType != null) {
                    String str3 = substringAfterLast$default + ClassUtils.PACKAGE_SEPARATOR_CHAR + w.substringAfterLast$default(mimeType, MentionUtilsKt.SLASH_CHAR, null, 2, null);
                    if (str3 != null) {
                        str2 = str3;
                        Uri contentUri = inputContentInfoCompat.getContentUri();
                        m.checkNotNullExpressionValue(contentUri, "contentUri");
                        return new Attachment<>(inputContentInfoCompat.getContentUri().hashCode(), contentUri, str2, inputContentInfoCompat, false, 16, null);
                    }
                }
            }
            str2 = substringAfterLast$default;
            Uri contentUri2 = inputContentInfoCompat.getContentUri();
            m.checkNotNullExpressionValue(contentUri2, "contentUri");
            return new Attachment<>(inputContentInfoCompat.getContentUri().hashCode(), contentUri2, str2, inputContentInfoCompat, false, 16, null);
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: Attachment.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Parcelable.Creator<Attachment<?>> {
        @Override // android.os.Parcelable.Creator
        public Attachment<?> createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "parcelIn");
            return new Attachment<>(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public Attachment<?>[] newArray(int i) {
            return new Attachment[i];
        }
    }

    public Attachment(long j, Uri uri, String str, T t, boolean z2) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "displayName");
        this.f3140id = j;
        this.uri = uri;
        this.displayName = str;
        this.data = t;
        this.spoiler = z2;
    }

    public static final Attachment<Uri> toAttachment(Uri uri, ContentResolver contentResolver) {
        return Companion.b(uri, contentResolver);
    }

    public static final Attachment<InputContentInfoCompat> toAttachment(InputContentInfoCompat inputContentInfoCompat, ContentResolver contentResolver, boolean z2, String str) {
        return Companion.c(inputContentInfoCompat, contentResolver, z2, str);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Attachment)) {
            return false;
        }
        Attachment attachment = (Attachment) obj;
        return this.f3140id == attachment.f3140id && m.areEqual(this.uri, attachment.uri) && this.spoiler == attachment.spoiler;
    }

    public final T getData() {
        return this.data;
    }

    public final String getDisplayName() {
        return this.displayName;
    }

    public final long getId() {
        return this.f3140id;
    }

    public final boolean getSpoiler() {
        return this.spoiler;
    }

    public final Uri getUri() {
        return this.uri;
    }

    public int hashCode() {
        return d.K0(Long.valueOf(this.f3140id), this.uri);
    }

    public final void setSpoiler(boolean z2) {
        this.spoiler = z2;
    }

    @Override // android.os.Parcelable
    @CallSuper
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "dest");
        parcel.writeLong(this.f3140id);
        parcel.writeParcelable(this.uri, i);
        parcel.writeString(this.displayName);
        parcel.writeInt(this.spoiler ? 1 : 0);
    }

    public /* synthetic */ Attachment(long j, Uri uri, String str, Object obj, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, uri, str, (i & 8) != 0 ? null : obj, (i & 16) != 0 ? false : z2);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public Attachment(android.os.Parcel r9) {
        /*
            r8 = this;
            java.lang.String r0 = "parcelIn"
            d0.z.d.m.checkNotNullParameter(r9, r0)
            long r2 = r9.readLong()
            java.lang.Class<android.net.Uri> r0 = android.net.Uri.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r9.readParcelable(r0)
            android.net.Uri r0 = (android.net.Uri) r0
            if (r0 == 0) goto L18
            goto L1f
        L18:
            android.net.Uri r0 = android.net.Uri.EMPTY
            java.lang.String r1 = "Uri.EMPTY"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
        L1f:
            r4 = r0
            java.lang.String r0 = r9.readString()
            if (r0 == 0) goto L27
            goto L29
        L27:
            java.lang.String r0 = ""
        L29:
            r5 = r0
            java.lang.String r0 = "parcelIn.readString() ?: \"\""
            d0.z.d.m.checkNotNullExpressionValue(r5, r0)
            r6 = 0
            int r9 = r9.readInt()
            r0 = 1
            if (r9 != r0) goto L39
            r7 = 1
            goto L3b
        L39:
            r9 = 0
            r7 = 0
        L3b:
            r1 = r8
            r1.<init>(r2, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lytefast.flexinput.model.Attachment.<init>(android.os.Parcel):void");
    }
}
