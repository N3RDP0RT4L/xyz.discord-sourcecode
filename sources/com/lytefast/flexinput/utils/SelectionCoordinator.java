package com.lytefast.flexinput.utils;

import andhook.lib.HookHelper;
import androidx.collection.ArrayMap;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import b.b.a.g.b;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.e0;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: SelectionCoordinator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u0001*\b\b\u0001\u0010\u0002*\u00028\u00002\u00020\u0003:\u0003'(\bJ\u001d\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00028\u00012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0004\u001a\u00028\u00012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\r\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00028\u0000¢\u0006\u0004\b\r\u0010\u000eJ)\u0010\u0012\u001a\u00020\n2\u001a\u0010\u0011\u001a\u0016\u0012\u0006\b\u0001\u0012\u00028\u00000\u000fj\n\u0012\u0006\b\u0001\u0012\u00028\u0000`\u0010¢\u0006\u0004\b\u0012\u0010\u0013R(\u0010\u001a\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u00148\u0004@\u0004X\u0084\u000e¢\u0006\u0012\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R(\u0010!\u001a\b\u0012\u0004\u0012\u00028\u00000\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R(\u0010&\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u00050\"8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010%¨\u0006)"}, d2 = {"Lcom/lytefast/flexinput/utils/SelectionCoordinator;", "I", ExifInterface.GPS_DIRECTION_TRUE, "", "item", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "a", "(Ljava/lang/Object;I)Z", "", "c", "(Ljava/lang/Object;I)V", "d", "(Ljava/lang/Object;)Z", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "selectedItems", "b", "(Ljava/util/ArrayList;)V", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "getAdapter", "()Landroidx/recyclerview/widget/RecyclerView$Adapter;", "setAdapter", "(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V", "adapter", "Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;", "Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;", "getItemSelectionListener", "()Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;", "setItemSelectionListener", "(Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;)V", "itemSelectionListener", "Landroidx/collection/ArrayMap;", "Landroidx/collection/ArrayMap;", "getSelectedItemPositionMap", "()Landroidx/collection/ArrayMap;", "selectedItemPositionMap", "ItemSelectionListener", "RestorationException", "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public class SelectionCoordinator<I, T extends I> {
    public RecyclerView.Adapter<?> a;

    /* renamed from: b  reason: collision with root package name */
    public final ArrayMap<T, Integer> f3142b;
    public ItemSelectionListener<? super I> c;

    /* compiled from: SelectionCoordinator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\bf\u0018\u0000*\u0006\b\u0002\u0010\u0001 \u00002\u00020\u0002J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00028\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00028\u0002H\u0016¢\u0006\u0004\b\u0007\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\b\u0010\t¨\u0006\n"}, d2 = {"Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;", "I", "", "item", "", "onItemSelected", "(Ljava/lang/Object;)V", "onItemUnselected", "unregister", "()V", "flexinput_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes3.dex */
    public interface ItemSelectionListener<I> {
        void onItemSelected(I i);

        void onItemUnselected(I i);

        void unregister();
    }

    /* compiled from: SelectionCoordinator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0011\b\u0000\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/lytefast/flexinput/utils/SelectionCoordinator$RestorationException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "", NotificationCompat.CATEGORY_MESSAGE, HookHelper.constructorName, "(Ljava/lang/String;)V", "flexinput_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes3.dex */
    public static final class RestorationException extends Exception {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public RestorationException(String str) {
            super(str);
            m.checkNotNullParameter(str, NotificationCompat.CATEGORY_MESSAGE);
        }
    }

    /* compiled from: SelectionCoordinator.kt */
    /* loaded from: classes3.dex */
    public static final class a<T> {
        public final T a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f3143b;

        public a(T t, boolean z2) {
            this.a = t;
            this.f3143b = z2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return m.areEqual(this.a, aVar.a) && this.f3143b == aVar.f3143b;
        }

        public int hashCode() {
            T t = this.a;
            int hashCode = (t != null ? t.hashCode() : 0) * 31;
            boolean z2 = this.f3143b;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("SelectionEvent(item=");
            R.append(this.a);
            R.append(", isSelected=");
            return b.d.b.a.a.M(R, this.f3143b, ")");
        }
    }

    public SelectionCoordinator() {
        this(null, null, 3);
    }

    public SelectionCoordinator(ArrayMap arrayMap, ItemSelectionListener itemSelectionListener, int i) {
        b bVar = null;
        ArrayMap<T, Integer> arrayMap2 = (i & 1) != 0 ? new ArrayMap<>(4) : null;
        bVar = (i & 2) != 0 ? new b() : bVar;
        m.checkNotNullParameter(arrayMap2, "selectedItemPositionMap");
        m.checkNotNullParameter(bVar, "itemSelectionListener");
        this.f3142b = arrayMap2;
        this.c = bVar;
    }

    public final boolean a(T t, int i) {
        Integer num = this.f3142b.get(t);
        if (num == null) {
            return false;
        }
        if (i == num.intValue()) {
            return true;
        }
        this.f3142b.put(t, Integer.valueOf(i));
        return true;
    }

    public final void b(ArrayList<? extends I> arrayList) throws RestorationException {
        m.checkNotNullParameter(arrayList, "selectedItems");
        if (this.a == null) {
            Iterator<? extends I> it = arrayList.iterator();
            while (it.hasNext()) {
                I next = it.next();
                if (!(next instanceof Object)) {
                    next = null;
                }
                if (next != null) {
                    this.f3142b.put(next, -1);
                }
            }
            return;
        }
        throw new RestorationException("cannot restoreSelections after adapter set: prevents mismatches");
    }

    public final void c(T t, int i) {
        this.f3142b.put(t, Integer.valueOf(i));
        RecyclerView.Adapter<?> adapter = this.a;
        if (adapter != null) {
            adapter.notifyItemChanged(i, new a(t, true));
        }
        this.c.onItemSelected(t);
    }

    public final boolean d(I i) {
        ArrayMap<T, Integer> arrayMap = this.f3142b;
        Objects.requireNonNull(arrayMap, "null cannot be cast to non-null type kotlin.collections.MutableMap<K, V>");
        Integer num = (Integer) e0.asMutableMap(arrayMap).remove(i);
        if (num == null) {
            return false;
        }
        int intValue = num.intValue();
        RecyclerView.Adapter<?> adapter = this.a;
        if (adapter != null) {
            adapter.notifyItemChanged(intValue, new a(i, false));
        }
        this.c.onItemUnselected(i);
        return true;
    }
}
