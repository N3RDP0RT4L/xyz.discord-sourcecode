package com.lytefast.flexinput.fragment;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import b.b.a.a.g;
import b.b.a.a.h;
import b.b.a.a.i;
import b.b.a.a.p;
import b.b.a.d.d;
import b.b.a.h.a;
import com.discord.utilities.display.DisplayUtils;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.TextWatcher;
import com.discord.utilities.view.text.TextWatcherKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.lytefast.flexinput.FlexInputListener;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.adapters.AttachmentPreviewAdapter;
import com.lytefast.flexinput.managers.FileManager;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.utils.SelectionAggregator;
import com.lytefast.flexinput.utils.SelectionCoordinator;
import com.lytefast.flexinput.viewmodel.FlexInputState;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
import com.lytefast.flexinput.widget.FlexEditText;
import d0.t.n;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KProperty;
import rx.Subscription;
/* compiled from: FlexInputFragment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¦\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0016\u0018\u00002\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002B\u0007¢\u0006\u0004\bb\u0010\u0015J\u000f\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J-\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ!\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u001b\u0010\u0018\u001a\u00020\u00112\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00110\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\r\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001e\u001a\u00020\u00112\u0006\u0010\u001d\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010 \u001a\u00020\u0011H\u0016¢\u0006\u0004\b \u0010\u0015J\u000f\u0010!\u001a\u00020\u0011H\u0016¢\u0006\u0004\b!\u0010\u0015J\u001d\u0010$\u001a\u00020\u00112\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00030\"H\u0016¢\u0006\u0004\b$\u0010%J\u001d\u0010'\u001a\u00020\u00112\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00110\u0016H\u0016¢\u0006\u0004\b'\u0010\u0019J\u000f\u0010(\u001a\u00020\u0004H\u0016¢\u0006\u0004\b(\u0010\u0006R\u0019\u0010-\u001a\b\u0012\u0004\u0012\u00020*0)8F@\u0006¢\u0006\u0006\u001a\u0004\b+\u0010,R\u0018\u00101\u001a\u0004\u0018\u00010.8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100R\u0018\u00105\u001a\u0004\u0018\u0001028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u00104R\u0018\u00109\u001a\u0004\u0018\u0001068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R\u0018\u0010;\u001a\u0004\u0018\u0001028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u00104R\"\u0010?\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00160<8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b=\u0010>R\u001d\u0010D\u001a\u00020@8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001b\u0010A\u001a\u0004\bB\u0010CR\"\u0010L\u001a\u00020E8\u0016@\u0016X\u0096.¢\u0006\u0012\n\u0004\bF\u0010G\u001a\u0004\bH\u0010I\"\u0004\bJ\u0010KR\u0018\u0010N\u001a\u0004\u0018\u0001028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bM\u00104R\"\u0010R\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\"0O8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bP\u0010QR\"\u0010T\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00160<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010>R\u001c\u0010W\u001a\b\u0012\u0004\u0012\u00020*0)8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bU\u0010VR\u0018\u0010[\u001a\u0004\u0018\u00010X8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bY\u0010ZR\u0016\u0010]\u001a\u00020\u001a8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0005\u0010\\R\"\u0010a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\"0^8V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b_\u0010`¨\u0006c"}, d2 = {"Lcom/lytefast/flexinput/fragment/FlexInputFragment;", "Landroidx/fragment/app/Fragment;", "Lb/b/a/b;", "", "", "m", "()Z", "Landroid/view/LayoutInflater;", "inflater", "Landroid/view/ViewGroup;", "container", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/view/View;", "onCreateView", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;", "view", "", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "()V", "Lkotlin/Function0;", "onViewCreatedUpdate", "i", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/lytefast/flexinput/widget/FlexEditText;", "l", "()Lcom/lytefast/flexinput/widget/FlexEditText;", "outState", "onSaveInstanceState", "(Landroid/os/Bundle;)V", "onPause", "onDestroyView", "Lcom/lytefast/flexinput/model/Attachment;", "attachment", "f", "(Lcom/lytefast/flexinput/model/Attachment;)V", "onSuccess", "requestMediaPermissions", "hasMediaPermissions", "", "Lb/b/a/d/d$a;", "k", "()[Lcom/lytefast/flexinput/adapters/AddContentPagerAdapter$PageSupplier;", "contentPages", "Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;", "s", "Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;", "viewModel", "Lrx/Subscription;", "v", "Lrx/Subscription;", "showExpressionKeyboardSubscription", "Lcom/lytefast/flexinput/FlexInputListener;", "o", "Lcom/lytefast/flexinput/FlexInputListener;", "inputListener", "t", "stateSubscription", "", "x", "Ljava/util/List;", "onContentPagesInitializedUpdates", "Lb/b/a/e/a;", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "j", "()Lb/b/a/e/a;", "binding", "Lcom/lytefast/flexinput/managers/FileManager;", "p", "Lcom/lytefast/flexinput/managers/FileManager;", "getFileManager", "()Lcom/lytefast/flexinput/managers/FileManager;", "setFileManager", "(Lcom/lytefast/flexinput/managers/FileManager;)V", "fileManager", "u", "eventSubscription", "Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;", "q", "Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;", "attachmentPreviewAdapter", "w", "onViewCreatedUpdates", "r", "[Lcom/lytefast/flexinput/adapters/AddContentPagerAdapter$PageSupplier;", "pageSuppliers", "Lb/b/a/f/a;", "n", "Lb/b/a/f/a;", "keyboardManager", "Lcom/lytefast/flexinput/widget/FlexEditText;", "inputEt", "Lcom/lytefast/flexinput/utils/SelectionAggregator;", "b", "()Lcom/lytefast/flexinput/utils/SelectionAggregator;", "selectionAggregator", HookHelper.constructorName, "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public class FlexInputFragment extends Fragment implements b.b.a.b<Object> {
    public static final /* synthetic */ KProperty[] j = {b.d.b.a.a.b0(FlexInputFragment.class, "binding", "getBinding()Lcom/lytefast/flexinput/databinding/FlexInputWidgetBinding;", 0)};
    public static final String k = FlexInputFragment.class.getName();
    public FlexEditText m;
    public b.b.a.f.a n;
    public FlexInputListener o;
    public FileManager p;
    public AttachmentPreviewAdapter<Attachment<Object>> q;
    public d.a[] r;

    /* renamed from: s  reason: collision with root package name */
    public FlexInputViewModel f3137s;
    public Subscription t;
    public Subscription u;
    public Subscription v;
    public final FragmentViewBindingDelegate l = FragmentViewBindingDelegateKt.viewBinding$default(this, c.j, null, 2, null);
    public final List<Function0<Unit>> w = new ArrayList();

    /* renamed from: x  reason: collision with root package name */
    public List<Function0<Unit>> f3138x = new ArrayList();

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            int i = this.j;
            if (i == 0) {
                FlexInputViewModel flexInputViewModel = ((FlexInputFragment) this.k).f3137s;
                if (flexInputViewModel != null) {
                    flexInputViewModel.onInputTextClicked();
                }
            } else if (i == 1) {
                AttachmentPreviewAdapter<Attachment<Object>> attachmentPreviewAdapter = ((FlexInputFragment) this.k).q;
                if (attachmentPreviewAdapter == null) {
                    m.throwUninitializedPropertyAccessException("attachmentPreviewAdapter");
                }
                attachmentPreviewAdapter.a.clear();
                attachmentPreviewAdapter.notifyDataSetChanged();
                FlexInputViewModel flexInputViewModel2 = ((FlexInputFragment) this.k).f3137s;
                if (flexInputViewModel2 != null) {
                    flexInputViewModel2.onAttachmentsUpdated(n.emptyList());
                }
            } else {
                throw null;
            }
        }
    }

    /* compiled from: FlexInputFragment.kt */
    /* loaded from: classes3.dex */
    public static final class b implements Runnable {
        public final /* synthetic */ DialogFragment j;

        public b(DialogFragment dialogFragment) {
            this.j = dialogFragment;
        }

        @Override // java.lang.Runnable
        public final void run() {
            DialogFragment dialogFragment = this.j;
            if (dialogFragment != null && dialogFragment.isAdded() && !this.j.isRemoving() && !this.j.isDetached()) {
                try {
                    this.j.dismiss();
                } catch (IllegalStateException e) {
                    Log.w(FlexInputFragment.k, "could not dismiss add content dialog", e);
                }
            }
        }
    }

    /* compiled from: FlexInputFragment.kt */
    /* loaded from: classes3.dex */
    public static final /* synthetic */ class c extends k implements Function1<View, b.b.a.e.a> {
        public static final c j = new c();

        public c() {
            super(1, b.b.a.e.a.class, "bind", "bind(Landroid/view/View;)Lcom/lytefast/flexinput/databinding/FlexInputWidgetBinding;", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public b.b.a.e.a invoke(View view) {
            View view2 = view;
            m.checkNotNullParameter(view2, "p1");
            int i = R.f.attachment_clear_btn;
            AppCompatImageButton appCompatImageButton = (AppCompatImageButton) view2.findViewById(i);
            if (appCompatImageButton != null) {
                i = R.f.attachment_preview_container;
                LinearLayout linearLayout = (LinearLayout) view2.findViewById(i);
                if (linearLayout != null) {
                    i = R.f.attachment_preview_list;
                    RecyclerView recyclerView = (RecyclerView) view2.findViewById(i);
                    if (recyclerView != null) {
                        i = R.f.cannot_send_text;
                        TextView textView = (TextView) view2.findViewById(i);
                        if (textView != null) {
                            LinearLayout linearLayout2 = (LinearLayout) view2;
                            i = R.f.default_window_insets_handler;
                            FrameLayout frameLayout = (FrameLayout) view2.findViewById(i);
                            if (frameLayout != null) {
                                i = R.f.expand_btn;
                                AppCompatImageButton appCompatImageButton2 = (AppCompatImageButton) view2.findViewById(i);
                                if (appCompatImageButton2 != null) {
                                    i = R.f.expression_btn;
                                    AppCompatImageButton appCompatImageButton3 = (AppCompatImageButton) view2.findViewById(i);
                                    if (appCompatImageButton3 != null) {
                                        i = R.f.expression_btn_badge;
                                        ImageView imageView = (ImageView) view2.findViewById(i);
                                        if (imageView != null) {
                                            i = R.f.expression_btn_container;
                                            FrameLayout frameLayout2 = (FrameLayout) view2.findViewById(i);
                                            if (frameLayout2 != null) {
                                                i = R.f.expression_tray_container;
                                                FrameLayout frameLayout3 = (FrameLayout) view2.findViewById(i);
                                                if (frameLayout3 != null) {
                                                    i = R.f.gallery_btn;
                                                    AppCompatImageButton appCompatImageButton4 = (AppCompatImageButton) view2.findViewById(i);
                                                    if (appCompatImageButton4 != null) {
                                                        i = R.f.gift_btn;
                                                        AppCompatImageButton appCompatImageButton5 = (AppCompatImageButton) view2.findViewById(i);
                                                        if (appCompatImageButton5 != null) {
                                                            i = R.f.left_btns_container;
                                                            LinearLayout linearLayout3 = (LinearLayout) view2.findViewById(i);
                                                            if (linearLayout3 != null) {
                                                                i = R.f.main_input_container;
                                                                LinearLayout linearLayout4 = (LinearLayout) view2.findViewById(i);
                                                                if (linearLayout4 != null) {
                                                                    i = R.f.send_btn_container;
                                                                    FrameLayout frameLayout4 = (FrameLayout) view2.findViewById(i);
                                                                    if (frameLayout4 != null) {
                                                                        i = R.f.send_btn_image;
                                                                        ImageView imageView2 = (ImageView) view2.findViewById(i);
                                                                        if (imageView2 != null) {
                                                                            i = R.f.text_input;
                                                                            FlexEditText flexEditText = (FlexEditText) view2.findViewById(i);
                                                                            if (flexEditText != null) {
                                                                                return new b.b.a.e.a(linearLayout2, appCompatImageButton, linearLayout, recyclerView, textView, linearLayout2, frameLayout, appCompatImageButton2, appCompatImageButton3, imageView, frameLayout2, frameLayout3, appCompatImageButton4, appCompatImageButton5, linearLayout3, linearLayout4, frameLayout4, imageView2, flexEditText);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
        }
    }

    /* compiled from: FlexInputFragment.kt */
    /* loaded from: classes3.dex */
    public static final /* synthetic */ class d extends k implements Function1<FlexInputState, Unit> {
        public d(FlexInputFragment flexInputFragment) {
            super(1, flexInputFragment, FlexInputFragment.class, "configureUI", "configureUI(Lcom/lytefast/flexinput/viewmodel/FlexInputState;)V", 0);
        }

        /* JADX WARN: Removed duplicated region for block: B:41:0x00ec  */
        /* JADX WARN: Removed duplicated region for block: B:42:0x00ee  */
        /* JADX WARN: Removed duplicated region for block: B:50:0x0108  */
        /* JADX WARN: Removed duplicated region for block: B:51:0x010a  */
        /* JADX WARN: Removed duplicated region for block: B:54:0x0127  */
        /* JADX WARN: Removed duplicated region for block: B:55:0x012c  */
        /* JADX WARN: Removed duplicated region for block: B:58:0x0143  */
        /* JADX WARN: Removed duplicated region for block: B:76:0x019c  */
        /* JADX WARN: Removed duplicated region for block: B:84:0x01bd  */
        /* JADX WARN: Removed duplicated region for block: B:92:0x01dd  */
        /* JADX WARN: Removed duplicated region for block: B:95:0x01e5  */
        /* JADX WARN: Removed duplicated region for block: B:96:0x01e9  */
        @Override // kotlin.jvm.functions.Function1
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public kotlin.Unit invoke(com.lytefast.flexinput.viewmodel.FlexInputState r13) {
            /*
                Method dump skipped, instructions count: 497
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lytefast.flexinput.fragment.FlexInputFragment.d.invoke(java.lang.Object):java.lang.Object");
        }
    }

    /* compiled from: FlexInputFragment.kt */
    /* loaded from: classes3.dex */
    public static final /* synthetic */ class e extends k implements Function1<b.b.a.h.a, Unit> {
        public e(FlexInputFragment flexInputFragment) {
            super(1, flexInputFragment, FlexInputFragment.class, "handleEvent", "handleEvent(Lcom/lytefast/flexinput/viewmodel/FlexInputEvent;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(b.b.a.h.a aVar) {
            b.b.a.h.a aVar2 = aVar;
            m.checkNotNullParameter(aVar2, "p1");
            FlexInputFragment flexInputFragment = (FlexInputFragment) this.receiver;
            KProperty[] kPropertyArr = FlexInputFragment.j;
            Objects.requireNonNull(flexInputFragment);
            if (aVar2 instanceof a.d) {
                Toast.makeText(flexInputFragment.requireContext(), ((a.d) aVar2).a, 0).show();
            } else if (aVar2 instanceof a.e) {
                Toast.makeText(flexInputFragment.requireContext(), ((a.e) aVar2).a, 0).show();
            } else if (aVar2 instanceof a.c) {
                b.b.a.f.a aVar3 = flexInputFragment.n;
                if (aVar3 != null) {
                    FlexEditText flexEditText = flexInputFragment.m;
                    if (flexEditText == null) {
                        m.throwUninitializedPropertyAccessException("inputEt");
                    }
                    aVar3.requestDisplay(flexEditText);
                }
            } else if (aVar2 instanceof a.b) {
                b.b.a.f.a aVar4 = flexInputFragment.n;
                if (aVar4 != null) {
                    aVar4.requestHide();
                }
            } else if (aVar2 instanceof a.C0059a) {
                flexInputFragment.j().q.performClick();
            }
            return Unit.a;
        }
    }

    /* compiled from: FlexInputFragment.kt */
    /* loaded from: classes3.dex */
    public static final class f extends o implements Function1<Editable, Unit> {
        public f() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Editable editable) {
            Editable editable2 = editable;
            m.checkNotNullParameter(editable2, "it");
            FlexInputViewModel flexInputViewModel = FlexInputFragment.this.f3137s;
            if (flexInputViewModel != null) {
                b.i.a.f.e.o.f.P0(flexInputViewModel, editable2.toString(), null, 2, null);
            }
            return Unit.a;
        }
    }

    public static final void g(FlexInputFragment flexInputFragment, int i) {
        ViewPager viewPager;
        FragmentTransaction beginTransaction = flexInputFragment.getChildFragmentManager().beginTransaction();
        m.checkNotNullExpressionValue(beginTransaction, "childFragmentManager.beginTransaction()");
        b.b.a.a.a aVar = new b.b.a.a.a();
        aVar.show(beginTransaction, "Add Content");
        flexInputFragment.getChildFragmentManager().executePendingTransactions();
        if (aVar.getDialog() != null) {
            ViewPager viewPager2 = aVar.k;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i);
            }
            FlexInputViewModel flexInputViewModel = flexInputFragment.f3137s;
            if (flexInputViewModel != null) {
                flexInputViewModel.onContentDialogPageChanged(i);
            }
            b.b.a.a.f fVar = new b.b.a.a.f(flexInputFragment);
            m.checkNotNullParameter(fVar, "listener");
            ViewPager.OnPageChangeListener onPageChangeListener = aVar.q;
            if (!(onPageChangeListener == null || (viewPager = aVar.k) == null)) {
                viewPager.removeOnPageChangeListener(onPageChangeListener);
            }
            aVar.q = fVar;
            ViewPager viewPager3 = aVar.k;
            if (viewPager3 != null) {
                viewPager3.addOnPageChangeListener(fVar);
            }
            Dialog dialog = aVar.getDialog();
            if (dialog != null) {
                dialog.setOnDismissListener(new g(flexInputFragment));
            }
            h hVar = new h(flexInputFragment);
            m.checkNotNullParameter(hVar, "onKeyboardSelectedListener");
            aVar.p = hVar;
        }
    }

    public static final void h(FlexInputFragment flexInputFragment, boolean z2) {
        Fragment findFragmentById = flexInputFragment.getChildFragmentManager().findFragmentById(R.f.expression_tray_container);
        if (findFragmentById != null && findFragmentById.isAdded() && findFragmentById.isResumed()) {
            if (!(findFragmentById instanceof b.b.a.c)) {
                findFragmentById = null;
            }
            b.b.a.c cVar = (b.b.a.c) findFragmentById;
            if (cVar != null) {
                cVar.isShown(z2);
            }
        }
    }

    @Override // b.b.a.b
    public SelectionAggregator<Attachment<Object>> b() {
        AttachmentPreviewAdapter<Attachment<Object>> attachmentPreviewAdapter = this.q;
        if (attachmentPreviewAdapter == null) {
            m.throwUninitializedPropertyAccessException("attachmentPreviewAdapter");
        }
        return attachmentPreviewAdapter.a;
    }

    @Override // b.b.a.b
    public void f(Attachment<? extends Object> attachment) {
        m.checkNotNullParameter(attachment, "attachment");
        DialogFragment dialogFragment = (DialogFragment) getChildFragmentManager().findFragmentByTag("Add Content");
        SelectionCoordinator<Attachment<Object>, ?> selectionCoordinator = new SelectionCoordinator<>(null, null, 3);
        b().registerSelectionCoordinator(selectionCoordinator);
        selectionCoordinator.c(attachment, 0);
        selectionCoordinator.c.unregister();
        FlexInputViewModel flexInputViewModel = this.f3137s;
        if (flexInputViewModel != null) {
            flexInputViewModel.onAttachmentsUpdated(b().getAttachments());
        }
        j().d.post(new b(dialogFragment));
    }

    @Override // b.b.a.b
    public FileManager getFileManager() {
        FileManager fileManager = this.p;
        if (fileManager == null) {
            m.throwUninitializedPropertyAccessException("fileManager");
        }
        return fileManager;
    }

    @Override // b.b.a.b
    public boolean hasMediaPermissions() {
        FlexInputViewModel flexInputViewModel = this.f3137s;
        if (flexInputViewModel != null) {
            return flexInputViewModel.hasMediaPermissions();
        }
        return false;
    }

    public final void i(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onViewCreatedUpdate");
        try {
            LinearLayout linearLayout = j().f;
            function0.invoke();
        } catch (IllegalStateException unused) {
            this.w.add(function0);
        }
    }

    public final b.b.a.e.a j() {
        return (b.b.a.e.a) this.l.getValue((Fragment) this, j[0]);
    }

    public final d.a[] k() {
        d.a[] aVarArr = this.r;
        if (aVarArr == null) {
            m.throwUninitializedPropertyAccessException("pageSuppliers");
        }
        if (aVarArr.length == 0) {
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            m.checkNotNullParameter(requireContext, "context");
            return new d.a[]{new b.b.a.d.a(requireContext, DrawableCompat.getThemedDrawableRes$default(requireContext, R.b.ic_flex_input_image, 0, 2, (Object) null), R.h.attachment_media), new b.b.a.d.b(requireContext, DrawableCompat.getThemedDrawableRes$default(requireContext, R.b.ic_flex_input_file, 0, 2, (Object) null), R.h.attachment_files), new b.b.a.d.c(requireContext, DrawableCompat.getThemedDrawableRes$default(requireContext, R.b.ic_flex_input_add_a_photo, 0, 2, (Object) null), R.h.camera)};
        }
        d.a[] aVarArr2 = this.r;
        if (aVarArr2 == null) {
            m.throwUninitializedPropertyAccessException("pageSuppliers");
        }
        return aVarArr2;
    }

    public final FlexEditText l() {
        FlexEditText flexEditText = this.m;
        if (flexEditText == null) {
            m.throwUninitializedPropertyAccessException("inputEt");
        }
        return flexEditText;
    }

    public final boolean m() {
        View view;
        if (!isAdded() || isHidden() || (view = getView()) == null) {
            return false;
        }
        return view.getVisibility() == 0;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        m.checkNotNullParameter(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.g.flex_input_widget, viewGroup, false);
        View findViewById = inflate.findViewById(R.f.text_input);
        m.checkNotNullExpressionValue(findViewById, "root.findViewById(R.id.text_input)");
        this.m = (FlexEditText) findViewById;
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        TextWatcher.Companion.reset(this);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        FlexInputViewModel flexInputViewModel = this.f3137s;
        if (flexInputViewModel != null) {
            flexInputViewModel.onFlexInputFragmentPause();
        }
        Subscription subscription = this.t;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        Subscription subscription2 = this.u;
        if (subscription2 != null) {
            subscription2.unsubscribe();
        }
        Subscription subscription3 = this.v;
        if (subscription3 != null) {
            subscription3.unsubscribe();
        }
        super.onPause();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FlexInputViewModel flexInputViewModel = this.f3137s;
        if (flexInputViewModel != null) {
            this.t = flexInputViewModel.observeState().q().V(new b.b.a.a.n(new d(this)));
            FlexInputViewModel flexInputViewModel2 = this.f3137s;
            if (flexInputViewModel2 != null) {
                this.u = flexInputViewModel2.observeEvents().V(new b.b.a.a.n(new e(this)));
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        m.checkNotNullParameter(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putParcelableArrayList("FlexInput.ATTACHMENTS", b().getAttachments());
        FlexEditText flexEditText = this.m;
        if (flexEditText == null) {
            m.throwUninitializedPropertyAccessException("inputEt");
        }
        bundle.putString("FlexInput.TEXT", String.valueOf(flexEditText.getText()));
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        FlexInputViewModel flexInputViewModel;
        m.checkNotNullParameter(view, "view");
        AttachmentPreviewAdapter<Attachment<Object>> attachmentPreviewAdapter = new AttachmentPreviewAdapter<>(false, null, null, 7);
        attachmentPreviewAdapter.a.addItemSelectionListener(new b.b.a.a.m(this));
        this.q = attachmentPreviewAdapter;
        FlexEditText flexEditText = this.m;
        if (flexEditText == null) {
            m.throwUninitializedPropertyAccessException("inputEt");
        }
        TextWatcherKt.addBindedTextWatcher(flexEditText, this, new f());
        FlexEditText flexEditText2 = this.m;
        if (flexEditText2 == null) {
            m.throwUninitializedPropertyAccessException("inputEt");
        }
        flexEditText2.setOnClickListener(new a(0, this));
        j().f318b.setOnClickListener(new a(1, this));
        b.b.a.e.a j2 = j();
        j().i.setOnClickListener(new h(0, this));
        j().o.setOnClickListener(new h(1, this));
        j2.l.setOnClickListener(new h(2, this));
        j2.m.setOnClickListener(new h(3, this));
        j2.h.setOnClickListener(new h(4, this));
        AppCompatImageButton appCompatImageButton = j2.f318b;
        m.checkNotNullExpressionValue(appCompatImageButton, "attachmentClearBtn");
        AppCompatImageButton appCompatImageButton2 = j2.l;
        m.checkNotNullExpressionValue(appCompatImageButton2, "galleryBtn");
        AppCompatImageButton appCompatImageButton3 = j2.m;
        m.checkNotNullExpressionValue(appCompatImageButton3, "giftBtn");
        AppCompatImageButton appCompatImageButton4 = j2.i;
        m.checkNotNullExpressionValue(appCompatImageButton4, "expressionBtn");
        FrameLayout frameLayout = j2.o;
        m.checkNotNullExpressionValue(frameLayout, "sendBtnContainer");
        AppCompatImageButton appCompatImageButton5 = j2.h;
        m.checkNotNullExpressionValue(appCompatImageButton5, "expandBtn");
        for (View view2 : n.listOf((Object[]) new View[]{appCompatImageButton, appCompatImageButton2, appCompatImageButton3, appCompatImageButton4, frameLayout, appCompatImageButton5})) {
            view2.setOnLongClickListener(new i(this));
        }
        FrameLayout frameLayout2 = j().k;
        m.checkNotNullExpressionValue(frameLayout2, "binding.expressionTrayContainer");
        ViewGroup.LayoutParams layoutParams = frameLayout2.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) layoutParams;
        FrameLayout frameLayout3 = j().k;
        m.checkNotNullExpressionValue(frameLayout3, "binding.expressionTrayContainer");
        Context context = frameLayout3.getContext();
        m.checkNotNullExpressionValue(context, "binding.expressionTrayContainer.context");
        layoutParams2.height = (int) (DisplayUtils.getScreenSize(context).height() * 0.5f);
        FrameLayout frameLayout4 = j().k;
        m.checkNotNullExpressionValue(frameLayout4, "binding.expressionTrayContainer");
        frameLayout4.setLayoutParams(layoutParams2);
        if (bundle != null) {
            ArrayList<? super Parcelable> parcelableArrayList = bundle.getParcelableArrayList("FlexInput.ATTACHMENTS");
            if (parcelableArrayList != null && parcelableArrayList.size() > 0) {
                b().initFrom(parcelableArrayList);
            }
            FlexInputViewModel flexInputViewModel2 = this.f3137s;
            if (flexInputViewModel2 != null) {
                flexInputViewModel2.onAttachmentsUpdated(b().getAttachments());
            }
            String string = bundle.getString("FlexInput.TEXT");
            if (!(string == null || (flexInputViewModel = this.f3137s) == null)) {
                flexInputViewModel.onInputTextChanged(string, null);
            }
        }
        FrameLayout frameLayout5 = j().k;
        m.checkNotNullExpressionValue(frameLayout5, "binding.expressionTrayContainer");
        ViewExtensions.setForwardingWindowInsetsListener(frameLayout5);
        ViewCompat.setOnApplyWindowInsetsListener(j().g, b.b.a.a.o.a);
        ViewCompat.setOnApplyWindowInsetsListener(j().f, new p(this));
        for (Function0<Unit> function0 : this.w) {
            function0.invoke();
        }
        this.w.clear();
    }

    @Override // b.b.a.b
    public void requestMediaPermissions(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        FlexInputViewModel flexInputViewModel = this.f3137s;
        if (flexInputViewModel != null) {
            flexInputViewModel.requestMediaPermissions(function0);
        }
    }
}
