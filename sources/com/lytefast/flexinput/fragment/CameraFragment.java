package com.lytefast.flexinput.fragment;

import andhook.lib.HookHelper;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import b.a.k.b;
import b.o.a.k;
import b.o.a.l;
import b.o.a.m.e;
import b.o.a.m.f;
import b.o.a.r.g;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.managers.FileManager;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraView;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: CameraFragment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0016\u0018\u0000 ?2\u00020\u0001:\u0001@B\u0007¢\u0006\u0004\b>\u0010\u0012J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J-\u0010\f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\f\u0010\rJ!\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000b2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J)\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u00132\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001a\u0010\u0012J\u000f\u0010\u001b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001b\u0010\u0012J#\u0010\u001c\u001a\u00020\u00042\b\b\u0002\u0010\u0015\u001a\u00020\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\n\u0012\u0004\u0012\u00020\u001f\u0018\u00010\u001eH\u0002¢\u0006\u0004\b \u0010!R\u0016\u0010%\u001a\u00020\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010)\u001a\u00020&8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010+\u001a\u00020&8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b*\u0010(R\u0018\u0010/\u001a\u0004\u0018\u00010,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u00103\u001a\u0002008\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00105\u001a\u00020&8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b4\u0010(R\u0016\u00109\u001a\u0002068\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u0010;\u001a\u00020&8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b:\u0010(R\u0016\u0010=\u001a\u00020\u000b8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b \u0010<¨\u0006A"}, d2 = {"Lcom/lytefast/flexinput/fragment/CameraFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/os/Bundle;", "savedInstanceState", "", "onCreate", "(Landroid/os/Bundle;)V", "Landroid/view/LayoutInflater;", "inflater", "Landroid/view/ViewGroup;", "container", "Landroid/view/View;", "onCreateView", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;", "view", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "()V", "", "requestCode", "resultCode", "Landroid/content/Intent;", "data", "onActivityResult", "(IILandroid/content/Intent;)V", "h", "g", "i", "(II)V", "Lb/b/a/b;", "", "k", "()Lb/b/a/b;", "", "s", "Z", "photoFilePending", "Landroid/widget/ImageView;", "o", "Landroid/widget/ImageView;", "cameraFlashBtn", "n", "cameraFacingBtn", "Ljava/io/File;", "r", "Ljava/io/File;", "photoFile", "Lcom/otaliastudios/cameraview/CameraView;", "l", "Lcom/otaliastudios/cameraview/CameraView;", "cameraView", "p", "takePhotoBtn", "Landroid/view/ViewStub;", "m", "Landroid/view/ViewStub;", "permissionsViewStub", "q", "launchCameraBtn", "Landroid/view/View;", "cameraContainer", HookHelper.constructorName, "j", "b", "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public class CameraFragment extends Fragment {
    public static final b j = new b(null);
    public View k;
    public CameraView l;
    public ViewStub m;
    public ImageView n;
    public ImageView o;
    public ImageView p;
    public ImageView q;
    public File r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f3135s;

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        public a(int i, Object obj) {
            this.j = i;
            this.k = obj;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            List list;
            boolean z2;
            Collection<f> b2;
            List list2;
            boolean z3;
            Collection<e> a;
            int i = this.j;
            if (i != 0) {
                Integer num = null;
                if (i != 1) {
                    int i2 = 0;
                    if (i == 2) {
                        CameraFragment cameraFragment = (CameraFragment) this.k;
                        CameraView cameraView = cameraFragment.l;
                        if (cameraView == null) {
                            m.throwUninitializedPropertyAccessException("cameraView");
                        }
                        b.o.a.c cameraOptions = cameraView.getCameraOptions();
                        if (cameraOptions == null || (b2 = cameraOptions.b()) == null || (list = u.toList(b2)) == null) {
                            list = n.emptyList();
                        }
                        if (list.size() > 1) {
                            CameraView cameraView2 = cameraFragment.l;
                            if (cameraView2 == null) {
                                m.throwUninitializedPropertyAccessException("cameraView");
                            }
                            f flash = cameraView2.getFlash();
                            m.checkNotNullExpressionValue(flash, "cameraView.flash");
                            Iterator<Integer> it = n.getIndices(list).iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                Integer next = it.next();
                                if (flash == ((f) list.get(next.intValue()))) {
                                    z2 = true;
                                    continue;
                                } else {
                                    z2 = false;
                                    continue;
                                }
                                if (z2) {
                                    num = next;
                                    break;
                                }
                            }
                            Integer num2 = num;
                            if (num2 != null) {
                                i2 = num2.intValue();
                            }
                            f fVar = (f) u.getOrNull(list, (i2 + 1) % list.size());
                            if (fVar != null) {
                                try {
                                    CameraView cameraView3 = cameraFragment.l;
                                    if (cameraView3 == null) {
                                        m.throwUninitializedPropertyAccessException("cameraView");
                                    }
                                    cameraView3.setFlash(fVar);
                                    cameraFragment.h();
                                } catch (Exception e) {
                                    String string = cameraFragment.getString(R.h.camera_unknown_error);
                                    m.checkNotNullExpressionValue(string, "getString(R.string.camera_unknown_error)");
                                    CameraFragment.l(cameraFragment, string, e, true, null, 8, null);
                                }
                            }
                        }
                    } else if (i == 3) {
                        CameraFragment cameraFragment2 = (CameraFragment) this.k;
                        CameraView cameraView4 = cameraFragment2.l;
                        if (cameraView4 == null) {
                            m.throwUninitializedPropertyAccessException("cameraView");
                        }
                        b.o.a.c cameraOptions2 = cameraView4.getCameraOptions();
                        if (cameraOptions2 == null || (a = cameraOptions2.a()) == null || (list2 = u.toList(a)) == null) {
                            list2 = n.emptyList();
                        }
                        if (list2.size() > 1) {
                            CameraView cameraView5 = cameraFragment2.l;
                            if (cameraView5 == null) {
                                m.throwUninitializedPropertyAccessException("cameraView");
                            }
                            e facing = cameraView5.getFacing();
                            m.checkNotNullExpressionValue(facing, "cameraView.facing");
                            Iterator<Integer> it2 = n.getIndices(list2).iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    break;
                                }
                                Integer next2 = it2.next();
                                if (facing == ((e) list2.get(next2.intValue()))) {
                                    z3 = true;
                                    continue;
                                } else {
                                    z3 = false;
                                    continue;
                                }
                                if (z3) {
                                    num = next2;
                                    break;
                                }
                            }
                            Integer num3 = num;
                            if (num3 != null) {
                                i2 = num3.intValue();
                            }
                            e eVar = (e) u.getOrNull(list2, (i2 + 1) % list2.size());
                            if (eVar != null) {
                                cameraFragment2.g();
                                CameraView cameraView6 = cameraFragment2.l;
                                if (cameraView6 == null) {
                                    m.throwUninitializedPropertyAccessException("cameraView");
                                }
                                cameraView6.setFacing(eVar);
                            }
                        }
                    } else {
                        throw null;
                    }
                } else {
                    CameraFragment cameraFragment3 = (CameraFragment) this.k;
                    cameraFragment3.r = null;
                    cameraFragment3.f3135s = true;
                    CameraView cameraView7 = cameraFragment3.l;
                    if (cameraView7 == null) {
                        m.throwUninitializedPropertyAccessException("cameraView");
                    }
                    cameraView7.close();
                }
            } else {
                CameraView cameraView8 = ((CameraFragment) this.k).l;
                if (cameraView8 == null) {
                    m.throwUninitializedPropertyAccessException("cameraView");
                }
                cameraView8.f3148z.O0(new l.a());
            }
        }
    }

    /* compiled from: CameraFragment.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: CameraFragment.kt */
    /* loaded from: classes3.dex */
    public static final class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            CameraFragment cameraFragment = CameraFragment.this;
            b bVar = CameraFragment.j;
            b.b.a.b<Object> k = cameraFragment.k();
            if (k != null) {
                k.requestMediaPermissions(q.k);
            }
        }
    }

    /* compiled from: CameraFragment.kt */
    /* loaded from: classes3.dex */
    public static final class d extends b.o.a.a {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f3136b;

        /* compiled from: CameraFragment.kt */
        /* loaded from: classes3.dex */
        public static final class a implements k {
            public a() {
            }

            @Override // b.o.a.k
            public final void a(File file) {
                if (file != null) {
                    b bVar = CameraFragment.j;
                    Context context = d.this.f3136b.getContext();
                    m.checkNotNullExpressionValue(context, "view.context");
                    context.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
                    b.b.a.b<Object> k = CameraFragment.this.k();
                    if (k != null) {
                        k.f(b.b.a.g.a.a(file));
                    }
                }
            }
        }

        public d(View view) {
            this.f3136b = view;
        }

        @Override // b.o.a.a
        public void a() {
            CameraFragment cameraFragment = CameraFragment.this;
            if (cameraFragment.f3135s) {
                b.b.a.b<Object> k = cameraFragment.k();
                FileManager fileManager = k != null ? k.getFileManager() : null;
                File b2 = fileManager != null ? fileManager.b() : null;
                if (b2 != null) {
                    Context requireContext = cameraFragment.requireContext();
                    m.checkNotNullExpressionValue(requireContext, "requireContext()");
                    Uri a2 = fileManager.a(requireContext, b2);
                    Intent addFlags = new Intent("android.media.action.IMAGE_CAPTURE").putExtra("output", a2).addFlags(2);
                    Context requireContext2 = cameraFragment.requireContext();
                    m.checkNotNullExpressionValue(requireContext2, "requireContext()");
                    m.checkNotNullExpressionValue(addFlags, "intent");
                    List<ResolveInfo> queryIntentActivities = requireContext2.getPackageManager().queryIntentActivities(addFlags, 65536);
                    m.checkNotNullExpressionValue(queryIntentActivities, "packageManager.queryInte…nager.MATCH_DEFAULT_ONLY)");
                    for (ResolveInfo resolveInfo : queryIntentActivities) {
                        requireContext2.grantUriPermission(resolveInfo.activityInfo.packageName, a2, 3);
                    }
                    m.checkNotNullExpressionValue(addFlags, "Intent(MediaStore.ACTION…nt, photoUri)\n          }");
                    cameraFragment.r = b2;
                    try {
                        cameraFragment.startActivityForResult(addFlags, 4567);
                    } catch (ActivityNotFoundException unused) {
                        CameraFragment.j(cameraFragment, 0, 0, 3, null);
                    }
                } else {
                    CameraFragment.j(cameraFragment, 0, 0, 3, null);
                }
            }
        }

        @Override // b.o.a.a
        public void b(CameraException cameraException) {
            m.checkNotNullParameter(cameraException, "exception");
            CameraFragment cameraFragment = CameraFragment.this;
            String string = cameraFragment.getString(R.h.camera_unknown_error);
            m.checkNotNullExpressionValue(string, "getString(R.string.camera_unknown_error)");
            CameraFragment.l(cameraFragment, string, cameraException, true, null, 8, null);
        }

        @Override // b.o.a.a
        public void c(b.o.a.c cVar) {
            m.checkNotNullParameter(cVar, "cameraOptions");
            CameraFragment cameraFragment = CameraFragment.this;
            b bVar = CameraFragment.j;
            cameraFragment.h();
            CameraFragment.this.g();
            ImageView imageView = CameraFragment.this.n;
            if (imageView == null) {
                m.throwUninitializedPropertyAccessException("cameraFacingBtn");
            }
            int i = 0;
            boolean z2 = true;
            imageView.setVisibility(cVar.a().size() > 1 ? 0 : 8);
            ImageView imageView2 = CameraFragment.this.o;
            if (imageView2 == null) {
                m.throwUninitializedPropertyAccessException("cameraFlashBtn");
            }
            if (cVar.b().size() <= 1) {
                z2 = false;
            }
            if (!z2) {
                i = 8;
            }
            imageView2.setVisibility(i);
        }

        @Override // b.o.a.a
        public void d(l lVar) {
            FileManager fileManager;
            File b2;
            m.checkNotNullParameter(lVar, "result");
            CameraFragment cameraFragment = CameraFragment.this;
            b bVar = CameraFragment.j;
            b.b.a.b<Object> k = cameraFragment.k();
            if (k != null && (fileManager = k.getFileManager()) != null && (b2 = fileManager.b()) != null) {
                a aVar = new a();
                byte[] bArr = lVar.a;
                b.o.a.b bVar2 = b.o.a.e.a;
                g.a(new b.o.a.d(bArr, b2, new Handler(), aVar));
            }
        }
    }

    public static /* synthetic */ void j(CameraFragment cameraFragment, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = 0;
        }
        if ((i3 & 2) != 0) {
            i2 = 4567;
        }
        cameraFragment.i(i, i2);
    }

    public static void l(CameraFragment cameraFragment, String str, Exception exc, boolean z2, String str2, int i, Object obj) {
        String str3 = null;
        if ((i & 2) != 0) {
            exc = null;
        }
        if ((i & 4) != 0) {
            z2 = false;
        }
        if ((i & 8) != 0) {
            str3 = str;
        }
        Objects.requireNonNull(cameraFragment);
        Log.e("Discord", str, exc);
        if (z2) {
            Toast.makeText(cameraFragment.getContext(), str3, 0).show();
        }
    }

    public final void g() {
        int i;
        ImageView imageView = this.n;
        if (imageView == null) {
            m.throwUninitializedPropertyAccessException("cameraFacingBtn");
        }
        CameraView cameraView = this.l;
        if (cameraView == null) {
            m.throwUninitializedPropertyAccessException("cameraView");
        }
        int ordinal = cameraView.getFacing().ordinal();
        if (ordinal == 0) {
            i = R.e.ic_camera_front_white_24dp;
        } else if (ordinal != 1) {
            i = R.e.ic_camera_front_white_24dp;
        } else {
            i = R.e.ic_camera_rear_white_24dp;
        }
        imageView.setImageResource(i);
    }

    public final void h() {
        int i;
        String str;
        ImageView imageView = this.o;
        if (imageView == null) {
            m.throwUninitializedPropertyAccessException("cameraFlashBtn");
        }
        CameraView cameraView = this.l;
        if (cameraView == null) {
            m.throwUninitializedPropertyAccessException("cameraView");
        }
        int ordinal = cameraView.getFlash().ordinal();
        if (ordinal == 0) {
            i = R.e.ic_flash_off_24dp;
        } else if (ordinal == 1) {
            i = R.e.ic_flash_on_24dp;
        } else if (ordinal != 3) {
            i = R.e.ic_flash_auto_24dp;
        } else {
            i = R.e.ic_flash_torch_24dp;
        }
        imageView.setImageResource(i);
        ImageView imageView2 = this.o;
        if (imageView2 == null) {
            m.throwUninitializedPropertyAccessException("cameraFlashBtn");
        }
        CameraView cameraView2 = this.l;
        if (cameraView2 == null) {
            m.throwUninitializedPropertyAccessException("cameraView");
        }
        int ordinal2 = cameraView2.getFlash().ordinal();
        if (ordinal2 == 0) {
            str = getString(R.h.flash_off);
        } else if (ordinal2 == 1) {
            str = getString(R.h.flash_on);
        } else if (ordinal2 == 2) {
            str = getString(R.h.flash_auto);
        } else if (ordinal2 != 3) {
            str = getString(R.h.flash_auto);
        } else {
            str = getString(R.h.flash_torch);
        }
        imageView2.setContentDescription(str);
    }

    public final void i(int i, int i2) {
        File file;
        if (i2 == 4567) {
            if (i == -1) {
                File file2 = this.r;
                if (file2 != null) {
                    Context requireContext = requireContext();
                    m.checkNotNullExpressionValue(requireContext, "requireContext()");
                    requireContext.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file2)));
                    b.b.a.b<Object> k = k();
                    if (k != null) {
                        k.f(b.b.a.g.a.a(file2));
                    }
                }
            } else if (!(i == 0 || (file = this.r) == null)) {
                String string = getString(R.h.camera_intent_result_error);
                m.checkNotNullExpressionValue(string, "getString(R.string.camera_intent_result_error)");
                Toast.makeText(getContext(), string, 0).show();
                file.delete();
            }
            CameraView cameraView = this.l;
            if (cameraView == null) {
                m.throwUninitializedPropertyAccessException("cameraView");
            }
            cameraView.open();
            this.r = null;
            this.f3135s = false;
        }
    }

    public final b.b.a.b<Object> k() {
        Fragment parentFragment = getParentFragment();
        b.b.a.b<Object> bVar = null;
        Fragment parentFragment2 = parentFragment != null ? parentFragment.getParentFragment() : null;
        if (parentFragment2 instanceof b.b.a.b) {
            bVar = parentFragment2;
        }
        return bVar;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        i(i2, i);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        m.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(R.g.fragment_camera, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        Context context;
        PackageManager packageManager;
        super.onResume();
        View view = getView();
        boolean z2 = true;
        boolean z3 = (view == null || (context = view.getContext()) == null || (packageManager = context.getPackageManager()) == null || !packageManager.hasSystemFeature("android.hardware.camera.any")) ? false : true;
        b.b.a.b<Object> k = k();
        if (k == null || !k.hasMediaPermissions()) {
            z2 = false;
        }
        if (!z3 || !z2) {
            View view2 = this.k;
            if (view2 == null) {
                m.throwUninitializedPropertyAccessException("cameraContainer");
            }
            view2.setVisibility(8);
            ViewStub viewStub = this.m;
            if (viewStub == null) {
                m.throwUninitializedPropertyAccessException("permissionsViewStub");
            }
            if (viewStub.getTag() == null) {
                ViewStub viewStub2 = this.m;
                if (viewStub2 == null) {
                    m.throwUninitializedPropertyAccessException("permissionsViewStub");
                }
                viewStub2.setTag(Boolean.TRUE);
                ViewStub viewStub3 = this.m;
                if (viewStub3 == null) {
                    m.throwUninitializedPropertyAccessException("permissionsViewStub");
                }
                View inflate = viewStub3.inflate();
                inflate.findViewById(R.f.permissions_required_action_btn).setOnClickListener(new c());
                View findViewById = inflate.findViewById(R.f.permissions_required_text);
                m.checkNotNullExpressionValue(findViewById, "view.findViewById<TextVi…ermissions_required_text)");
                b.a.k.b.m((TextView) findViewById, R.h.system_permission_request_camera, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                return;
            }
            ViewStub viewStub4 = this.m;
            if (viewStub4 == null) {
                m.throwUninitializedPropertyAccessException("permissionsViewStub");
            }
            viewStub4.setVisibility(0);
            return;
        }
        View view3 = this.k;
        if (view3 == null) {
            m.throwUninitializedPropertyAccessException("cameraContainer");
        }
        view3.setVisibility(0);
        ViewStub viewStub5 = this.m;
        if (viewStub5 == null) {
            m.throwUninitializedPropertyAccessException("permissionsViewStub");
        }
        if (m.areEqual(viewStub5.getTag(), Boolean.TRUE)) {
            ViewStub viewStub6 = this.m;
            if (viewStub6 == null) {
                m.throwUninitializedPropertyAccessException("permissionsViewStub");
            }
            viewStub6.setVisibility(8);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.f.permissions_view_stub);
        m.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.permissions_view_stub)");
        this.m = (ViewStub) findViewById;
        View findViewById2 = view.findViewById(R.f.camera_container);
        m.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.camera_container)");
        this.k = findViewById2;
        View findViewById3 = view.findViewById(R.f.camera_view);
        m.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.camera_view)");
        CameraView cameraView = (CameraView) findViewById3;
        this.l = cameraView;
        if (cameraView == null) {
            m.throwUninitializedPropertyAccessException("cameraView");
        }
        cameraView.setLifecycleOwner(getViewLifecycleOwner());
        CameraView cameraView2 = this.l;
        if (cameraView2 == null) {
            m.throwUninitializedPropertyAccessException("cameraView");
        }
        cameraView2.setPlaySounds(false);
        CameraView cameraView3 = this.l;
        if (cameraView3 == null) {
            m.throwUninitializedPropertyAccessException("cameraView");
        }
        cameraView3.D.add(new d(view));
        View findViewById4 = view.findViewById(R.f.take_photo_btn);
        m.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.take_photo_btn)");
        ImageView imageView = (ImageView) findViewById4;
        this.p = imageView;
        if (imageView == null) {
            m.throwUninitializedPropertyAccessException("takePhotoBtn");
        }
        imageView.setOnClickListener(new a(0, this));
        ImageView imageView2 = this.p;
        if (imageView2 == null) {
            m.throwUninitializedPropertyAccessException("takePhotoBtn");
        }
        imageView2.setImageTintList(null);
        View findViewById5 = view.findViewById(R.f.launch_camera_btn);
        m.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.launch_camera_btn)");
        ImageView imageView3 = (ImageView) findViewById5;
        this.q = imageView3;
        if (imageView3 == null) {
            m.throwUninitializedPropertyAccessException("launchCameraBtn");
        }
        imageView3.setOnClickListener(new a(1, this));
        ImageView imageView4 = this.q;
        if (imageView4 == null) {
            m.throwUninitializedPropertyAccessException("launchCameraBtn");
        }
        imageView4.setImageTintList(ColorStateList.valueOf(-1));
        ImageView imageView5 = this.q;
        if (imageView5 == null) {
            m.throwUninitializedPropertyAccessException("launchCameraBtn");
        }
        imageView5.setImageResource(R.e.ic_launch_24dp);
        View findViewById6 = view.findViewById(R.f.camera_flash_btn);
        m.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.camera_flash_btn)");
        ImageView imageView6 = (ImageView) findViewById6;
        this.o = imageView6;
        if (imageView6 == null) {
            m.throwUninitializedPropertyAccessException("cameraFlashBtn");
        }
        imageView6.setOnClickListener(new a(2, this));
        ImageView imageView7 = this.o;
        if (imageView7 == null) {
            m.throwUninitializedPropertyAccessException("cameraFlashBtn");
        }
        imageView7.setImageTintList(ColorStateList.valueOf(-1));
        View findViewById7 = view.findViewById(R.f.camera_facing_btn);
        m.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.camera_facing_btn)");
        ImageView imageView8 = (ImageView) findViewById7;
        this.n = imageView8;
        if (imageView8 == null) {
            m.throwUninitializedPropertyAccessException("cameraFacingBtn");
        }
        imageView8.setOnClickListener(new a(3, this));
        ImageView imageView9 = this.n;
        if (imageView9 == null) {
            m.throwUninitializedPropertyAccessException("cameraFacingBtn");
        }
        imageView9.setImageTintList(ColorStateList.valueOf(-1));
    }
}
