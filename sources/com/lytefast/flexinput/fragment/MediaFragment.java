package com.lytefast.flexinput.fragment;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import b.b.a.d.h;
import com.discord.utilities.display.DisplayUtils;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.adapters.EmptyListAdapter;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.model.Media;
import com.lytefast.flexinput.utils.SelectionAggregator;
import com.lytefast.flexinput.utils.SelectionCoordinator;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MediaFragment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0016\u0018\u0000 ,2\u00020\u0001:\u0001,B\u0007¢\u0006\u0004\b+\u0010\u0013J#\u0010\u0006\u001a\u00020\u00052\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0002\"\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J-\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\t\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0014\u0010\u0013J\u0017\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0016\u001a\u00020\u0015H\u0014¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u001a\u0010\u0013R\u0018\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR*\u0010\"\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0\u001f\u0012\u0004\u0012\u00020!\u0018\u00010\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#R$\u0010%\u001a\u0004\u0018\u00010$8\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*¨\u0006-"}, d2 = {"Lcom/lytefast/flexinput/fragment/MediaFragment;", "Landroidx/fragment/app/Fragment;", "", "", "requiredPermissionList", "", "hasPermissions", "([Ljava/lang/String;)Z", "Landroid/view/LayoutInflater;", "inflater", "Landroid/view/ViewGroup;", "container", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/view/View;", "onCreateView", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;", "", "onResume", "()V", "onPause", "Landroid/view/View$OnClickListener;", "onClickListener", "Lcom/lytefast/flexinput/adapters/EmptyListAdapter;", "newPermissionsRequestAdapter", "(Landroid/view/View$OnClickListener;)Lcom/lytefast/flexinput/adapters/EmptyListAdapter;", "onDestroyView", "Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;", "swipeRefreshLayout", "Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;", "Lcom/lytefast/flexinput/utils/SelectionCoordinator;", "Lcom/lytefast/flexinput/model/Attachment;", "", "Lcom/lytefast/flexinput/model/Media;", "selectionCoordinator", "Lcom/lytefast/flexinput/utils/SelectionCoordinator;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "getRecyclerView$flexinput_release", "()Landroidx/recyclerview/widget/RecyclerView;", "setRecyclerView$flexinput_release", "(Landroidx/recyclerview/widget/RecyclerView;)V", HookHelper.constructorName, "Companion", "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public class MediaFragment extends Fragment {
    public static final Companion Companion = new Companion(null);
    private static final int GRID_LAYOUT_SPAN_COUNT = 3;
    private static final String REQUIRED_PERMISSION = "android.permission.READ_EXTERNAL_STORAGE";
    private RecyclerView recyclerView;
    private SelectionCoordinator<Attachment<Object>, Media> selectionCoordinator;
    private SwipeRefreshLayout swipeRefreshLayout;

    /* compiled from: MediaFragment.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/lytefast/flexinput/fragment/MediaFragment$Companion;", "", "", "GRID_LAYOUT_SPAN_COUNT", "I", "", "REQUIRED_PERMISSION", "Ljava/lang/String;", HookHelper.constructorName, "()V", "flexinput_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes3.dex */
    public static final class Companion {
        public Companion() {
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: MediaFragment.kt */
    /* loaded from: classes3.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ View j;
        public final /* synthetic */ h k;
        public final /* synthetic */ MediaFragment l;
        public final /* synthetic */ b.b.a.b m;

        /* compiled from: MediaFragment.kt */
        /* renamed from: com.lytefast.flexinput.fragment.MediaFragment$a$a  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0269a extends o implements Function0<Unit> {
            public C0269a() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public Unit invoke() {
                RecyclerView recyclerView$flexinput_release = a.this.l.getRecyclerView$flexinput_release();
                m.checkNotNull(recyclerView$flexinput_release);
                recyclerView$flexinput_release.setLayoutManager(new GridLayoutManager(a.this.j.getContext(), 3));
                RecyclerView recyclerView$flexinput_release2 = a.this.l.getRecyclerView$flexinput_release();
                m.checkNotNull(recyclerView$flexinput_release2);
                recyclerView$flexinput_release2.setAdapter(a.this.k);
                RecyclerView recyclerView$flexinput_release3 = a.this.l.getRecyclerView$flexinput_release();
                m.checkNotNull(recyclerView$flexinput_release3);
                recyclerView$flexinput_release3.invalidateItemDecorations();
                return Unit.a;
            }
        }

        public a(View view, h hVar, MediaFragment mediaFragment, b.b.a.b bVar) {
            this.j = view;
            this.k = hVar;
            this.l = mediaFragment;
            this.m = bVar;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            b.b.a.b bVar = this.m;
            if (bVar != null) {
                bVar.requestMediaPermissions(new C0269a());
            }
        }
    }

    /* compiled from: MediaFragment.kt */
    /* loaded from: classes3.dex */
    public static final class b implements SwipeRefreshLayout.OnRefreshListener {
        public final /* synthetic */ View a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ h f3139b;
        public final /* synthetic */ MediaFragment c;
        public final /* synthetic */ b.b.a.b d;

        public b(View view, h hVar, MediaFragment mediaFragment, b.b.a.b bVar) {
            this.a = view;
            this.f3139b = hVar;
            this.c = mediaFragment;
            this.d = bVar;
        }

        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
        public final void onRefresh() {
            if (this.c.hasPermissions(MediaFragment.REQUIRED_PERMISSION)) {
                h hVar = this.f3139b;
                Context context = this.a.getContext();
                m.checkNotNullExpressionValue(context, "context");
                ContentResolver contentResolver = context.getContentResolver();
                m.checkNotNullExpressionValue(contentResolver, "context.contentResolver");
                hVar.b(contentResolver);
            }
            SwipeRefreshLayout swipeRefreshLayout = this.c.swipeRefreshLayout;
            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean hasPermissions(String... strArr) {
        int length = strArr.length;
        int i = 0;
        while (true) {
            boolean z2 = true;
            if (i >= length) {
                return true;
            }
            if (ContextCompat.checkSelfPermission(requireContext(), strArr[i]) != 0) {
                z2 = false;
            }
            if (!z2) {
                return false;
            }
            i++;
        }
    }

    public final RecyclerView getRecyclerView$flexinput_release() {
        return this.recyclerView;
    }

    public EmptyListAdapter newPermissionsRequestAdapter(View.OnClickListener onClickListener) {
        m.checkNotNullParameter(onClickListener, "onClickListener");
        return new EmptyListAdapter(R.g.item_permission_storage, R.f.permissions_req_btn, onClickListener);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Context context;
        m.checkNotNullParameter(layoutInflater, "inflater");
        this.selectionCoordinator = new SelectionCoordinator<>(null, null, 3);
        Fragment parentFragment = getParentFragment();
        Fragment parentFragment2 = parentFragment != null ? parentFragment.getParentFragment() : null;
        if (!(parentFragment2 instanceof b.b.a.b)) {
            parentFragment2 = null;
        }
        b.b.a.b bVar = (b.b.a.b) parentFragment2;
        if (bVar != null) {
            SelectionAggregator b2 = bVar.b();
            SelectionCoordinator<Attachment<Object>, Media> selectionCoordinator = this.selectionCoordinator;
            m.checkNotNull(selectionCoordinator);
            b2.registerSelectionCoordinator(selectionCoordinator);
        }
        View inflate = layoutInflater.inflate(R.g.fragment_recycler_view, viewGroup, false);
        if (inflate == null) {
            return null;
        }
        this.recyclerView = (RecyclerView) inflate.findViewById(R.f.list);
        m.checkNotNullExpressionValue(inflate.getContext(), "context");
        int width = (int) (DisplayUtils.getScreenSize(context).width() / 3);
        SelectionCoordinator<Attachment<Object>, Media> selectionCoordinator2 = this.selectionCoordinator;
        m.checkNotNull(selectionCoordinator2);
        h hVar = new h(selectionCoordinator2, width, width);
        if (hasPermissions(REQUIRED_PERMISSION)) {
            RecyclerView recyclerView = this.recyclerView;
            if (recyclerView != null) {
                recyclerView.setLayoutManager(new GridLayoutManager(inflate.getContext(), 3));
            }
            RecyclerView recyclerView2 = this.recyclerView;
            if (recyclerView2 != null) {
                recyclerView2.setAdapter(hVar);
            }
        } else {
            RecyclerView recyclerView3 = this.recyclerView;
            if (recyclerView3 != null) {
                recyclerView3.setAdapter(newPermissionsRequestAdapter(new a(inflate, hVar, this, bVar)));
            }
        }
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) inflate.findViewById(R.f.swipeRefreshLayout);
        this.swipeRefreshLayout = swipeRefreshLayout;
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(new b(inflate, hVar, this, bVar));
        }
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        SelectionCoordinator<Attachment<Object>, Media> selectionCoordinator = this.selectionCoordinator;
        m.checkNotNull(selectionCoordinator);
        selectionCoordinator.c.unregister();
        super.onDestroyView();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        SwipeRefreshLayout swipeRefreshLayout = this.swipeRefreshLayout;
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setEnabled(false);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        SwipeRefreshLayout swipeRefreshLayout = this.swipeRefreshLayout;
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setEnabled(true);
        }
    }

    public final void setRecyclerView$flexinput_release(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }
}
