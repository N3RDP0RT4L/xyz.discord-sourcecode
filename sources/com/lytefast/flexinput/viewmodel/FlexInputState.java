package com.lytefast.flexinput.viewmodel;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.lytefast.flexinput.model.Attachment;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: FlexInputState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\u0013\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001B\u0087\u0001\u0012\b\b\u0002\u00102\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u0019\u001a\u00020\t\u0012\u0014\b\u0002\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010&0%\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0010\u001a\u00020\t\u0012\b\b\u0002\u00105\u001a\u00020\t\u0012\b\b\u0002\u00108\u001a\u00020\t\u0012\b\b\u0002\u0010.\u001a\u00020\t\u0012\b\b\u0002\u0010$\u001a\u00020\t\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0011\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b9\u0010:J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0019\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\r\u001a\u0004\b\u0018\u0010\u000fR\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0013\u001a\u0004\b\u001b\u0010\u0015R\u001b\u0010!\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0019\u0010$\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\r\u001a\u0004\b#\u0010\u000fR%\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010&0%8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u0019\u0010.\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010\r\u001a\u0004\b-\u0010\u000fR\u0019\u00102\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010\u0004R\u0019\u00105\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010\r\u001a\u0004\b4\u0010\u000fR\u0019\u00108\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010\r\u001a\u0004\b7\u0010\u000f¨\u0006;"}, d2 = {"Lcom/lytefast/flexinput/viewmodel/FlexInputState;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "e", "Z", "getAbleToSendMessages", "()Z", "ableToSendMessages", "", "k", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "guildId", "b", "getShowExpandedButtons", "showExpandedButtons", "j", "getChannelId", "channelId", "d", "Ljava/lang/Integer;", "getShowContentDialogIndex", "()Ljava/lang/Integer;", "showContentDialogIndex", "i", "getExpressionSuggestionsEnabled", "expressionSuggestionsEnabled", "", "Lcom/lytefast/flexinput/model/Attachment;", "c", "Ljava/util/List;", "getAttachments", "()Ljava/util/List;", "attachments", "h", "getShowExpressionTrayButtonBadge", "showExpressionTrayButtonBadge", "a", "Ljava/lang/String;", "getInputText", "inputText", "f", "getAbleToAttachFiles", "ableToAttachFiles", "g", "getShowExpressionTray", "showExpressionTray", HookHelper.constructorName, "(Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZZLjava/lang/Long;Ljava/lang/Long;)V", "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public final class FlexInputState {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3144b;
    public final List<Attachment<Object>> c;
    public final Integer d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final Long j;
    public final Long k;

    public FlexInputState() {
        this(null, false, null, null, false, false, false, false, false, null, null, 2047);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public FlexInputState(String str, boolean z2, List<? extends Attachment<? extends Object>> list, Integer num, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, Long l, Long l2) {
        m.checkNotNullParameter(str, "inputText");
        m.checkNotNullParameter(list, "attachments");
        this.a = str;
        this.f3144b = z2;
        this.c = list;
        this.d = num;
        this.e = z3;
        this.f = z4;
        this.g = z5;
        this.h = z6;
        this.i = z7;
        this.j = l;
        this.k = l2;
    }

    public static FlexInputState a(FlexInputState flexInputState, String str, boolean z2, List list, Integer num, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, Long l, Long l2, int i) {
        String str2 = (i & 1) != 0 ? flexInputState.a : str;
        boolean z8 = (i & 2) != 0 ? flexInputState.f3144b : z2;
        List list2 = (i & 4) != 0 ? flexInputState.c : list;
        Integer num2 = (i & 8) != 0 ? flexInputState.d : num;
        boolean z9 = (i & 16) != 0 ? flexInputState.e : z3;
        boolean z10 = (i & 32) != 0 ? flexInputState.f : z4;
        boolean z11 = (i & 64) != 0 ? flexInputState.g : z5;
        boolean z12 = (i & 128) != 0 ? flexInputState.h : z6;
        boolean z13 = (i & 256) != 0 ? flexInputState.i : z7;
        Long l3 = (i & 512) != 0 ? flexInputState.j : l;
        Long l4 = (i & 1024) != 0 ? flexInputState.k : l2;
        Objects.requireNonNull(flexInputState);
        m.checkNotNullParameter(str2, "inputText");
        m.checkNotNullParameter(list2, "attachments");
        return new FlexInputState(str2, z8, list2, num2, z9, z10, z11, z12, z13, l3, l4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FlexInputState)) {
            return false;
        }
        FlexInputState flexInputState = (FlexInputState) obj;
        return m.areEqual(this.a, flexInputState.a) && this.f3144b == flexInputState.f3144b && m.areEqual(this.c, flexInputState.c) && m.areEqual(this.d, flexInputState.d) && this.e == flexInputState.e && this.f == flexInputState.f && this.g == flexInputState.g && this.h == flexInputState.h && this.i == flexInputState.i && m.areEqual(this.j, flexInputState.j) && m.areEqual(this.k, flexInputState.k);
    }

    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        boolean z2 = this.f3144b;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode + i3) * 31;
        List<Attachment<Object>> list = this.c;
        int hashCode2 = (i5 + (list != null ? list.hashCode() : 0)) * 31;
        Integer num = this.d;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        boolean z3 = this.e;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (hashCode3 + i6) * 31;
        boolean z4 = this.f;
        if (z4) {
            z4 = true;
        }
        int i9 = z4 ? 1 : 0;
        int i10 = z4 ? 1 : 0;
        int i11 = (i8 + i9) * 31;
        boolean z5 = this.g;
        if (z5) {
            z5 = true;
        }
        int i12 = z5 ? 1 : 0;
        int i13 = z5 ? 1 : 0;
        int i14 = (i11 + i12) * 31;
        boolean z6 = this.h;
        if (z6) {
            z6 = true;
        }
        int i15 = z6 ? 1 : 0;
        int i16 = z6 ? 1 : 0;
        int i17 = (i14 + i15) * 31;
        boolean z7 = this.i;
        if (!z7) {
            i2 = z7 ? 1 : 0;
        }
        int i18 = (i17 + i2) * 31;
        Long l = this.j;
        int hashCode4 = (i18 + (l != null ? l.hashCode() : 0)) * 31;
        Long l2 = this.k;
        if (l2 != null) {
            i = l2.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("FlexInputState(inputText=");
        R.append(this.a);
        R.append(", showExpandedButtons=");
        R.append(this.f3144b);
        R.append(", attachments=");
        R.append(this.c);
        R.append(", showContentDialogIndex=");
        R.append(this.d);
        R.append(", ableToSendMessages=");
        R.append(this.e);
        R.append(", ableToAttachFiles=");
        R.append(this.f);
        R.append(", showExpressionTray=");
        R.append(this.g);
        R.append(", showExpressionTrayButtonBadge=");
        R.append(this.h);
        R.append(", expressionSuggestionsEnabled=");
        R.append(this.i);
        R.append(", channelId=");
        R.append(this.j);
        R.append(", guildId=");
        return a.F(R, this.k, ")");
    }

    /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FlexInputState(String str, boolean z2, List list, Integer num, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, Long l, Long l2, int i) {
        this((i & 1) != 0 ? "" : null, (i & 2) != 0 ? true : z2, (i & 4) != 0 ? n.emptyList() : null, null, (i & 16) != 0 ? true : z3, (i & 32) != 0 ? true : z4, (i & 64) != 0 ? false : z5, (i & 128) == 0 ? z6 : false, (i & 256) == 0 ? z7 : true, null, null);
        int i2 = i & 8;
        int i3 = i & 512;
        int i4 = i & 1024;
    }
}
