package com.lytefast.flexinput.adapters;

import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.b.a.d.e;
import b.b.a.d.f;
import b.b.a.d.g;
import b.f.g.a.a.d;
import com.discord.utilities.analytics.ChatInputComponentTypes;
import com.facebook.drawee.view.SimpleDraweeView;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.utils.SelectionCoordinator;
import d0.t.n;
import d0.t.q;
import d0.t.r;
import d0.z.d.m;
import d0.z.d.o;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: FileListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0002\u0011\rB'\u0012\u0006\u0010\t\u001a\u00020\u0006\u0012\u0016\u0010\u0013\u001a\u0012\u0012\u0002\b\u0003\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u0010¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0004\u0010\u0005R\u0016\u0010\t\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\"\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010\u000eR(\u0010\u0013\u001a\u0014\u0012\u0002\b\u0003\u0012\f\b\u0000\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/lytefast/flexinput/adapters/FileListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/lytefast/flexinput/adapters/FileListAdapter$b;", "", "getItemCount", "()I", "Landroid/content/ContentResolver;", "c", "Landroid/content/ContentResolver;", "contentResolver", "", "Lcom/lytefast/flexinput/model/Attachment;", "Ljava/io/File;", "b", "Ljava/util/List;", ChatInputComponentTypes.FILES, "Lcom/lytefast/flexinput/utils/SelectionCoordinator;", "a", "Lcom/lytefast/flexinput/utils/SelectionCoordinator;", "selectionCoordinator", HookHelper.constructorName, "(Landroid/content/ContentResolver;Lcom/lytefast/flexinput/utils/SelectionCoordinator;)V", "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public final class FileListAdapter extends RecyclerView.Adapter<b> {
    public final SelectionCoordinator<?, ? super Attachment<? extends File>> a;

    /* renamed from: b  reason: collision with root package name */
    public List<? extends Attachment<? extends File>> f3133b = n.emptyList();
    public final ContentResolver c;

    /* compiled from: FileListAdapter.kt */
    /* loaded from: classes3.dex */
    public static final class a extends AsyncTask<File, Boolean, List<? extends Attachment<? extends File>>> {
        public final FileListAdapter a;

        public a(FileListAdapter fileListAdapter) {
            m.checkNotNullParameter(fileListAdapter, "adapter");
            this.a = fileListAdapter;
        }

        @Override // android.os.AsyncTask
        public List<? extends Attachment<? extends File>> doInBackground(File[] fileArr) {
            File[] fileArr2 = fileArr;
            m.checkNotNullParameter(fileArr2, "rootFiles");
            File file = fileArr2[0];
            g gVar = g.j;
            ArrayList arrayList = new ArrayList();
            LinkedList linkedList = new LinkedList();
            r.addAll(linkedList, gVar.invoke(file));
            while (!linkedList.isEmpty()) {
                File file2 = (File) linkedList.remove();
                m.checkNotNullExpressionValue(file2, "file");
                if (!file2.isHidden()) {
                    if (file2.isDirectory()) {
                        r.addAll(linkedList, gVar.invoke(file2));
                    } else {
                        arrayList.add(b.b.a.g.a.a(file2));
                    }
                }
            }
            q.sortWith(arrayList, d0.u.a.then(new f(this), new e()));
            return arrayList;
        }

        @Override // android.os.AsyncTask
        public void onPostExecute(List<? extends Attachment<? extends File>> list) {
            List<? extends Attachment<? extends File>> list2 = list;
            m.checkNotNullParameter(list2, ChatInputComponentTypes.FILES);
            FileListAdapter fileListAdapter = this.a;
            fileListAdapter.f3133b = list2;
            fileListAdapter.notifyDataSetChanged();
        }
    }

    /* compiled from: FileListAdapter.kt */
    /* loaded from: classes3.dex */
    public class b extends RecyclerView.ViewHolder {
        public final AnimatorSet a;

        /* renamed from: b  reason: collision with root package name */
        public final AnimatorSet f3134b;
        public SimpleDraweeView c;
        public ImageView d;
        public TextView e;
        public TextView f;
        public Attachment<? extends File> g;
        public final /* synthetic */ FileListAdapter h;

        /* compiled from: FileListAdapter.kt */
        /* loaded from: classes3.dex */
        public static final class a implements View.OnClickListener {
            public a() {
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                boolean z2;
                b bVar = b.this;
                SelectionCoordinator<?, ? super Attachment<? extends File>> selectionCoordinator = bVar.h.a;
                Attachment<? extends File> attachment = bVar.g;
                int adapterPosition = bVar.getAdapterPosition();
                Objects.requireNonNull(selectionCoordinator);
                if (attachment != null && !selectionCoordinator.d(attachment)) {
                    selectionCoordinator.c(attachment, adapterPosition);
                    z2 = true;
                } else {
                    z2 = false;
                }
                bVar.b(z2, true);
            }
        }

        /* compiled from: FileListAdapter.kt */
        /* renamed from: com.lytefast.flexinput.adapters.FileListAdapter$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public static final class C0267b extends o implements Function1<AnimatorSet, Unit> {
            public final /* synthetic */ boolean $isAnimationRequested;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public C0267b(boolean z2) {
                super(1);
                this.$isAnimationRequested = z2;
            }

            public final void a(AnimatorSet animatorSet) {
                m.checkNotNullParameter(animatorSet, "animation");
                animatorSet.start();
                if (!this.$isAnimationRequested) {
                    animatorSet.end();
                }
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(AnimatorSet animatorSet) {
                a(animatorSet);
                return Unit.a;
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(FileListAdapter fileListAdapter, View view) {
            super(view);
            m.checkNotNullParameter(view, "itemView");
            this.h = fileListAdapter;
            View findViewById = view.findViewById(R.f.thumb_iv);
            m.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.thumb_iv)");
            this.c = (SimpleDraweeView) findViewById;
            View findViewById2 = view.findViewById(R.f.type_iv);
            m.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.type_iv)");
            this.d = (ImageView) findViewById2;
            View findViewById3 = view.findViewById(R.f.file_name_tv);
            m.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.file_name_tv)");
            this.e = (TextView) findViewById3;
            View findViewById4 = view.findViewById(R.f.file_subtitle_tv);
            m.checkNotNullExpressionValue(findViewById4, "itemView.findViewById(R.id.file_subtitle_tv)");
            this.f = (TextView) findViewById4;
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "this.itemView");
            view2.setClickable(true);
            this.itemView.setOnClickListener(new a());
            Animator loadAnimator = AnimatorInflater.loadAnimator(view.getContext(), R.a.selection_shrink);
            Objects.requireNonNull(loadAnimator, "null cannot be cast to non-null type android.animation.AnimatorSet");
            AnimatorSet animatorSet = (AnimatorSet) loadAnimator;
            this.a = animatorSet;
            animatorSet.setTarget(this.c);
            Animator loadAnimator2 = AnimatorInflater.loadAnimator(view.getContext(), R.a.selection_grow);
            Objects.requireNonNull(loadAnimator2, "null cannot be cast to non-null type android.animation.AnimatorSet");
            AnimatorSet animatorSet2 = (AnimatorSet) loadAnimator2;
            this.f3134b = animatorSet2;
            animatorSet2.setTarget(this.c);
        }

        /* JADX WARN: Type inference failed for: r4v3, types: [java.lang.Throwable, android.graphics.BitmapFactory$Options] */
        public final void a(File file) {
            Bitmap thumbnail;
            Cursor query = this.h.c.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "mini_thumb_magic"}, "_data=?", new String[]{file.getPath()}, null);
            if (query != null) {
                try {
                    th = 0;
                    if (query.moveToFirst()) {
                        long j = query.getLong(0);
                        if (query.getLong(1) == 0 && (thumbnail = MediaStore.Images.Thumbnails.getThumbnail(this.h.c, j, 1, th)) != null) {
                            thumbnail.recycle();
                        }
                        Cursor query2 = this.h.c.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, new String[]{"_id"}, "image_id=?", new String[]{String.valueOf(j)}, null);
                        if (query2 != null) {
                            if (!query2.moveToFirst()) {
                                d0.y.b.closeFinally(query2, th);
                                return;
                            }
                            String string = query2.getString(0);
                            SimpleDraweeView simpleDraweeView = this.c;
                            d a2 = b.f.g.a.a.b.a();
                            a2.n = this.c.getController();
                            d f = a2.f(Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, string));
                            f.l = true;
                            simpleDraweeView.setController(f.a());
                            d0.y.b.closeFinally(query2, th);
                        }
                    }
                } catch (Throwable th) {
                    try {
                        throw th;
                    } finally {
                        d0.y.b.closeFinally(query, th);
                    }
                }
            }
        }

        public final void b(boolean z2, boolean z3) {
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            view.setSelected(z2);
            C0267b bVar = new C0267b(z3);
            if (z2) {
                if (this.c.getScaleX() == 1.0f) {
                    bVar.a(this.a);
                }
            } else if (this.c.getScaleX() != 1.0f) {
                bVar.a(this.f3134b);
            }
        }
    }

    public FileListAdapter(ContentResolver contentResolver, SelectionCoordinator<?, Attachment<File>> selectionCoordinator) {
        m.checkNotNullParameter(contentResolver, "contentResolver");
        m.checkNotNullParameter(selectionCoordinator, "selectionCoordinator");
        this.c = contentResolver;
        Objects.requireNonNull(selectionCoordinator);
        m.checkNotNullParameter(this, "adapter");
        selectionCoordinator.a = this;
        this.a = selectionCoordinator;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.f3133b.size();
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x00ef  */
    /* JADX WARN: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onBindViewHolder(com.lytefast.flexinput.adapters.FileListAdapter.b r11, int r12) {
        /*
            Method dump skipped, instructions count: 339
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lytefast.flexinput.adapters.FileListAdapter.onBindViewHolder(androidx.recyclerview.widget.RecyclerView$ViewHolder, int):void");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.g.view_file_item, viewGroup, false);
        m.checkNotNullExpressionValue(inflate, "view");
        return new b(this, inflate);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(b bVar, int i, List list) {
        SelectionCoordinator.a aVar;
        Object obj;
        b bVar2 = bVar;
        m.checkNotNullParameter(bVar2, "holder");
        m.checkNotNullParameter(list, "payloads");
        Iterator it = list.iterator();
        while (true) {
            aVar = null;
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (obj instanceof SelectionCoordinator.a) {
                break;
            }
        }
        if (obj != null) {
            if (obj instanceof SelectionCoordinator.a) {
                aVar = obj;
            }
            SelectionCoordinator.a aVar2 = aVar;
            if (aVar2 != null) {
                bVar2.b(aVar2.f3143b, true);
                return;
            }
        }
        super.onBindViewHolder(bVar2, i, list);
    }
}
