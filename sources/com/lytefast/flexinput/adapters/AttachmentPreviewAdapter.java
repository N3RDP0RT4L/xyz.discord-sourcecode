package com.lytefast.flexinput.adapters;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import b.b.a.d.i;
import b.f.g.a.a.d;
import b.f.j.d.e;
import b.f.j.d.f;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.model.Media;
import com.lytefast.flexinput.utils.SelectionAggregator;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
/* compiled from: AttachmentPreviewAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000*\u000e\b\u0000\u0010\u0003*\b\u0012\u0004\u0012\u00020\u00020\u00012\u0012\u0012\u000e\u0012\f0\u0005R\b\u0012\u0004\u0012\u00028\u00000\u00000\u0004:\u0001\u0017Be\b\u0007\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u0016\u0012 \b\u0002\u0010\u0015\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\t\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00100\u000f\u0012.\b\u0002\u0010\u001e\u001a(\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\t\u0018\u00010\u001cj\n\u0012\u0004\u0012\u00028\u0000\u0018\u0001`\u001d¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\t8\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR1\u0010\u0015\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\t\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00100\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0019\u0010\u001b\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a¨\u0006!"}, d2 = {"Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;", "Lcom/lytefast/flexinput/model/Attachment;", "", ExifInterface.GPS_DIRECTION_TRUE, "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter$b;", "", "getItemCount", "()I", "Lcom/lytefast/flexinput/utils/SelectionAggregator;", "a", "Lcom/lytefast/flexinput/utils/SelectionAggregator;", "getSelectionAggregator", "()Lcom/lytefast/flexinput/utils/SelectionAggregator;", "selectionAggregator", "Lkotlin/Function2;", "", "c", "Lkotlin/jvm/functions/Function2;", "getOnAttachmentSelected", "()Lkotlin/jvm/functions/Function2;", "onAttachmentSelected", "", "b", "Z", "getUseBottomSheet", "()Z", "useBottomSheet", "Lkotlin/Function1;", "Lcom/lytefast/flexinput/adapters/SelectionAggregatorProvider;", "selectionAggregatorProvider", HookHelper.constructorName, "(ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)V", "flexinput_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public final class AttachmentPreviewAdapter<T extends Attachment<? extends Object>> extends RecyclerView.Adapter<AttachmentPreviewAdapter<T>.b> {
    public final SelectionAggregator<T> a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f3132b;
    public final Function2<SelectionAggregator<T>, T, Unit> c;

    /* compiled from: AttachmentPreviewAdapter.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function2<SelectionAggregator<T>, T, Unit> {
        public static final a j = new a();

        public a() {
            super(2);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // kotlin.jvm.functions.Function2
        public Unit invoke(Object obj, Object obj2) {
            SelectionAggregator selectionAggregator = (SelectionAggregator) obj;
            Attachment attachment = (Attachment) obj2;
            m.checkNotNullParameter(selectionAggregator, "aggregator");
            m.checkNotNullParameter(attachment, "attachment");
            selectionAggregator.unselectItem(attachment);
            return Unit.a;
        }
    }

    /* compiled from: AttachmentPreviewAdapter.kt */
    /* loaded from: classes3.dex */
    public final class b extends i {
        public final b.b.a.e.b p;
        public final Function2<SelectionAggregator<T>, T, Unit> q;
        public final /* synthetic */ AttachmentPreviewAdapter r;

        /* JADX WARN: Illegal instructions before constructor call */
        /* JADX WARN: Multi-variable type inference failed */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public b(com.lytefast.flexinput.adapters.AttachmentPreviewAdapter r4, b.b.a.e.b r5, kotlin.jvm.functions.Function2<? super com.lytefast.flexinput.utils.SelectionAggregator<T>, ? super T, kotlin.Unit> r6) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                java.lang.String r0 = "onAttachmentSelected"
                d0.z.d.m.checkNotNullParameter(r6, r0)
                r3.r = r4
                androidx.constraintlayout.widget.ConstraintLayout r4 = r5.a
                java.lang.String r0 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r4, r0)
                r3.<init>(r4)
                r3.p = r5
                r3.q = r6
                com.facebook.drawee.view.SimpleDraweeView r4 = r3.a()
                com.facebook.drawee.interfaces.DraweeHierarchy r4 = r4.getHierarchy()
                com.facebook.drawee.generic.GenericDraweeHierarchy r4 = (com.facebook.drawee.generic.GenericDraweeHierarchy) r4
                com.facebook.drawee.view.SimpleDraweeView r5 = r3.a()
                android.content.Context r5 = r5.getContext()
                java.lang.String r6 = "imageView.context"
                d0.z.d.m.checkNotNullExpressionValue(r5, r6)
                int r6 = com.lytefast.flexinput.R.b.ic_flex_input_file
                r0 = 0
                r1 = 2
                r2 = 0
                int r5 = com.discord.utilities.drawable.DrawableCompat.getThemedDrawableRes$default(r5, r6, r0, r1, r2)
                r4.p(r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lytefast.flexinput.adapters.AttachmentPreviewAdapter.b.<init>(com.lytefast.flexinput.adapters.AttachmentPreviewAdapter, b.b.a.e.b, kotlin.jvm.functions.Function2):void");
        }

        @Override // b.b.a.d.i
        public SimpleDraweeView a() {
            SimpleDraweeView simpleDraweeView = this.p.f319b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.attachmentItem");
            return simpleDraweeView;
        }
    }

    public AttachmentPreviewAdapter() {
        this(false, null, null, 7);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ AttachmentPreviewAdapter(boolean r1, kotlin.jvm.functions.Function2 r2, kotlin.jvm.functions.Function1 r3, int r4) {
        /*
            r0 = this;
            r2 = r4 & 1
            if (r2 == 0) goto L5
            r1 = 0
        L5:
            r2 = r4 & 2
            r3 = 0
            if (r2 == 0) goto Ld
            com.lytefast.flexinput.adapters.AttachmentPreviewAdapter$a r2 = com.lytefast.flexinput.adapters.AttachmentPreviewAdapter.a.j
            goto Le
        Ld:
            r2 = r3
        Le:
            r4 = r4 & 4
            r0.<init>(r1, r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lytefast.flexinput.adapters.AttachmentPreviewAdapter.<init>(boolean, kotlin.jvm.functions.Function2, kotlin.jvm.functions.Function1, int):void");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.a.getSize();
    }

    /* JADX WARN: Type inference failed for: r2v9, types: [REQUEST, com.facebook.imagepipeline.request.ImageRequest] */
    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        b bVar = (b) viewHolder;
        m.checkNotNullParameter(bVar, "holder");
        T t = this.a.get(i);
        Objects.requireNonNull(bVar);
        m.checkNotNullParameter(t, "item");
        if (t instanceof Media) {
            bVar.a().setController(null);
            bVar.c((Media) t, null, null);
        } else {
            int dimensionPixelSize = bVar.a().getResources().getDimensionPixelSize(R.d.attachment_preview_dimen);
            ImageRequestBuilder b2 = ImageRequestBuilder.b(t.getUri());
            b2.e = f.a;
            b2.d = new e(dimensionPixelSize, dimensionPixelSize);
            d a2 = b.f.g.a.a.b.a();
            a2.n = bVar.a().getController();
            a2.m = true;
            a2.h = b2.a();
            bVar.a().setController(a2.a());
        }
        int i2 = 8;
        if (bVar.r.f3132b) {
            boolean spoiler = t.getSpoiler();
            View view = bVar.p.d;
            m.checkNotNullExpressionValue(view, "binding.attachmentSpoilerCover");
            view.setVisibility(spoiler ? 0 : 8);
            SimpleDraweeView simpleDraweeView = bVar.p.e;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.attachmentSpoilerIcon");
            if (spoiler) {
                i2 = 0;
            }
            simpleDraweeView.setVisibility(i2);
            bVar.p.c.setOnClickListener(new e(0, bVar, t));
            bVar.p.f319b.setOnClickListener(new e(1, bVar, t));
            return;
        }
        SimpleDraweeView simpleDraweeView2 = bVar.p.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.attachmentRemove");
        simpleDraweeView2.setVisibility(8);
        bVar.p.f319b.setOnClickListener(new e(2, bVar, t));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View findViewById;
        m.checkNotNullParameter(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.g.view_attachment_preview_item, viewGroup, false);
        int i2 = R.f.attachment_item;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(i2);
        if (simpleDraweeView != null) {
            i2 = R.f.attachment_remove;
            SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) inflate.findViewById(i2);
            if (!(simpleDraweeView2 == null || (findViewById = inflate.findViewById((i2 = R.f.attachment_spoiler_cover))) == null)) {
                i2 = R.f.attachment_spoiler_icon;
                SimpleDraweeView simpleDraweeView3 = (SimpleDraweeView) inflate.findViewById(i2);
                if (simpleDraweeView3 != null) {
                    b.b.a.e.b bVar = new b.b.a.e.b((ConstraintLayout) inflate, simpleDraweeView, simpleDraweeView2, findViewById, simpleDraweeView3);
                    m.checkNotNullExpressionValue(bVar, "ViewAttachmentPreviewIte….context), parent, false)");
                    return new b(this, bVar, this.c);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        b bVar = (b) viewHolder;
        m.checkNotNullParameter(bVar, "holder");
        super.onViewRecycled(bVar);
        bVar.b();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public AttachmentPreviewAdapter(boolean z2, Function2<? super SelectionAggregator<T>, ? super T, Unit> function2, Function1<? super AttachmentPreviewAdapter<T>, ? extends SelectionAggregator<T>> function1) {
        SelectionAggregator<T> selectionAggregator;
        m.checkNotNullParameter(function2, "onAttachmentSelected");
        this.f3132b = z2;
        this.c = function2;
        this.a = (function1 == null || (selectionAggregator = function1.invoke(this)) == null) ? new SelectionAggregator<>(this, null, null, null, 14, null) : selectionAggregator;
    }
}
