package com.discord.widgets.home;

import andhook.lib.HookHelper;
import android.content.SharedPreferences;
import androidx.annotation.MainThread;
import androidx.core.app.FrameMetricsAggregator;
import b.d.b.a.a;
import com.discord.api.permission.Permission;
import com.discord.api.user.NsfwAllowance;
import com.discord.app.AppViewModel;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.user.MeUser;
import com.discord.panels.PanelState;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreGuildSelected;
import com.discord.stores.StoreGuildWelcomeScreens;
import com.discord.stores.StoreLurking;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserConnections;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.time.Clock;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.channels.ChannelOnboardingManager;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventUpsellBottomSheet;
import com.discord.widgets.home.WidgetHomeViewModel;
import com.discord.widgets.playstation.PlaystationUpsellManager;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func3;
import rx.subjects.PublishSubject;
/* compiled from: WidgetHomeViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 X2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0005XYZ[\\Bm\u0012\b\b\u0002\u0010L\u001a\u00020K\u0012\b\b\u0002\u0010O\u001a\u00020N\u0012\b\b\u0002\u0010R\u001a\u00020Q\u0012\b\b\u0002\u0010;\u001a\u00020:\u0012\u000e\b\u0002\u0010T\u001a\b\u0012\u0004\u0012\u00020\u00030%\u0012\u000e\b\u0002\u0010U\u001a\b\u0012\u0004\u0012\u00020\b0%\u0012\b\b\u0002\u0010>\u001a\u00020=\u0012\b\b\u0002\u0010A\u001a\u00020@\u0012\b\b\u0002\u0010D\u001a\u00020C¢\u0006\u0004\bV\u0010WJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ5\u0010\u0014\u001a\u00020\u00122\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\u000e\u001a\u00020\f2\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u00102\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J'\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u001d\u0010\u001cJ\u000f\u0010\u001e\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u001e\u0010\u001cJ\u001b\u0010\u001f\u001a\u00020\u00052\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010H\u0003¢\u0006\u0004\b\u001f\u0010 J\u001b\u0010!\u001a\u00020\u00052\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010H\u0003¢\u0006\u0004\b!\u0010 J\u000f\u0010\"\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\"\u0010\u001cJ\u000f\u0010#\u001a\u00020\u0005H\u0003¢\u0006\u0004\b#\u0010\u001cJ\u000f\u0010$\u001a\u00020\u0005H\u0003¢\u0006\u0004\b$\u0010\u001cJ\u0013\u0010'\u001a\b\u0012\u0004\u0012\u00020&0%¢\u0006\u0004\b'\u0010(J\u0015\u0010*\u001a\u00020\u00052\u0006\u0010)\u001a\u00020\f¢\u0006\u0004\b*\u0010+J\u0015\u0010,\u001a\u00020\u00052\u0006\u0010)\u001a\u00020\f¢\u0006\u0004\b,\u0010+J\u000f\u0010-\u001a\u00020\u0005H\u0007¢\u0006\u0004\b-\u0010\u001cR\u0016\u0010.\u001a\u00020\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010/R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R$\u00104\u001a\u0004\u0018\u0001038\u0000@\u0000X\u0080\u000e¢\u0006\u0012\n\u0004\b4\u00105\u001a\u0004\b6\u00107\"\u0004\b8\u00109R\u0016\u0010;\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010A\u001a\u00020@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010ER:\u0010H\u001a&\u0012\f\u0012\n G*\u0004\u0018\u00010&0& G*\u0012\u0012\f\u0012\n G*\u0004\u0018\u00010&0&\u0018\u00010F0F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bH\u0010IR\u0018\u0010\r\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010JR\u0016\u0010L\u001a\u00020K8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010MR\u0016\u0010O\u001a\u00020N8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bO\u0010PR\u0016\u0010R\u001a\u00020Q8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bR\u0010S¨\u0006]"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;", "Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;)V", "Lcom/discord/stores/StoreNavigation$PanelAction;", "navPanelAction", "handleNavDrawerAction", "(Lcom/discord/stores/StoreNavigation$PanelAction;)V", "Lcom/discord/panels/PanelState;", "previousLeftPanelState", "leftPanelState", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "isNewUser", "shouldShowChannelOnboardingSheet", "(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;JZ)Z", "canManageEvents", "canGuildShowEvents", "isInEventsUpsellExperiment", "shouldShowGuildEventUpsell", "(ZZZ)Z", "emitClosePanelsEvent", "()V", "emitUnlockLeftPanelEvent", "emitShowChannelOnboardingSheet", "emitShowWelcomeSheet", "(J)V", "emitShowGuildEventUpsell", "emitShowPlaystationUpsell", "emitAnimatePeekIn", "emitAnimatePeekOut", "Lrx/Observable;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", "observeEvents", "()Lrx/Observable;", "panelState", "onStartPanelStateChange", "(Lcom/discord/panels/PanelState;)V", "onEndPanelStateChange", "emitOpenLeftPanelEvent", "wasThreadPeek", "Z", "Lcom/discord/widgets/playstation/PlaystationUpsellManager;", "playstationUpsellManager", "Lcom/discord/widgets/playstation/PlaystationUpsellManager;", "Lcom/discord/widgets/home/WidgetHomeModel;", "widgetHomeModel", "Lcom/discord/widgets/home/WidgetHomeModel;", "getWidgetHomeModel$app_productionGoogleRelease", "()Lcom/discord/widgets/home/WidgetHomeModel;", "setWidgetHomeModel$app_productionGoogleRelease", "(Lcom/discord/widgets/home/WidgetHomeModel;)V", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/widgets/channels/ChannelOnboardingManager;", "channelOnboardingManager", "Lcom/discord/widgets/channels/ChannelOnboardingManager;", "Landroid/content/SharedPreferences;", "sharedPreferences", "Landroid/content/SharedPreferences;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/panels/PanelState;", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreNavigation;", "Lcom/discord/stores/StoreGuildWelcomeScreens;", "storeGuildWelcomeScreens", "Lcom/discord/stores/StoreGuildWelcomeScreens;", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/stores/StoreExperiments;", "storeStateObservable", "navPanelActionObservable", HookHelper.constructorName, "(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreGuildWelcomeScreens;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUser;Lrx/Observable;Lrx/Observable;Lcom/discord/widgets/channels/ChannelOnboardingManager;Landroid/content/SharedPreferences;Lcom/discord/utilities/time/Clock;)V", "Companion", "Event", "GuildInfo", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHomeViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final ChannelOnboardingManager channelOnboardingManager;
    private final Clock clock;
    private final PublishSubject<Event> eventSubject;
    private final PlaystationUpsellManager playstationUpsellManager;
    private PanelState previousLeftPanelState;
    private final SharedPreferences sharedPreferences;
    private final StoreExperiments storeExperiments;
    private final StoreGuildWelcomeScreens storeGuildWelcomeScreens;
    private final StoreNavigation storeNavigation;
    private final StoreUser storeUser;
    private boolean wasThreadPeek;
    private WidgetHomeModel widgetHomeModel;

    /* compiled from: WidgetHomeViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.home.WidgetHomeViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            WidgetHomeViewModel widgetHomeViewModel = WidgetHomeViewModel.this;
            m.checkNotNullExpressionValue(storeState, "storeState");
            widgetHomeViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetHomeViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreNavigation$PanelAction;", "panelAction", "", "invoke", "(Lcom/discord/stores/StoreNavigation$PanelAction;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.home.WidgetHomeViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreNavigation.PanelAction, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreNavigation.PanelAction panelAction) {
            invoke2(panelAction);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreNavigation.PanelAction panelAction) {
            m.checkNotNullParameter(panelAction, "panelAction");
            WidgetHomeViewModel.this.handleNavDrawerAction(panelAction);
        }
    }

    /* compiled from: WidgetHomeViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J]\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Companion;", "", "Lcom/discord/stores/StoreGuildSelected;", "storeGuildSelected", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreLurking;", "storeLurking", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreGuildWelcomeScreens;", "storeGuildWelcomeScreens", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StoreUserConnections;", "storeUserConnections", "Lrx/Observable;", "Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuildWelcomeScreens;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserConnections;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(StoreGuildSelected storeGuildSelected, StoreNavigation storeNavigation, final StoreLurking storeLurking, StoreUser storeUser, final StoreGuildWelcomeScreens storeGuildWelcomeScreens, StoreChannelsSelected storeChannelsSelected, final StoreExperiments storeExperiments, final StorePermissions storePermissions, StoreUserConnections storeUserConnections) {
            Observable<StoreState> d = Observable.d(storeNavigation.observeLeftPanelState(), storeNavigation.observeRightPanelState(), storeGuildSelected.observeSelectedGuildId().Y(new b<Long, Observable<? extends GuildInfo>>() { // from class: com.discord.widgets.home.WidgetHomeViewModel$Companion$observeStoreState$guildInfoObservable$1
                public final Observable<? extends WidgetHomeViewModel.GuildInfo> call(final Long l) {
                    StoreLurking storeLurking2 = StoreLurking.this;
                    m.checkNotNullExpressionValue(l, "selectedGuildId");
                    return Observable.i(storeLurking2.isLurkingObs(l.longValue()), storeGuildWelcomeScreens.observeGuildWelcomeScreen(l.longValue()), storePermissions.observePermissionsForGuild(l.longValue()), new Func3<Boolean, StoreGuildWelcomeScreens.State, Long, WidgetHomeViewModel.GuildInfo>() { // from class: com.discord.widgets.home.WidgetHomeViewModel$Companion$observeStoreState$guildInfoObservable$1.1
                        public final WidgetHomeViewModel.GuildInfo call(Boolean bool, StoreGuildWelcomeScreens.State state, Long l2) {
                            Long l3 = l;
                            m.checkNotNullExpressionValue(l3, "selectedGuildId");
                            long longValue = l3.longValue();
                            m.checkNotNullExpressionValue(bool, "isLurking");
                            boolean booleanValue = bool.booleanValue();
                            boolean can = l2 != null ? PermissionUtils.can(Permission.MANAGE_EVENTS, Long.valueOf(l2.longValue())) : false;
                            StoreExperiments storeExperiments2 = storeExperiments;
                            Long l4 = l;
                            m.checkNotNullExpressionValue(l4, "selectedGuildId");
                            Experiment guildExperiment = storeExperiments2.getGuildExperiment("2021-06_stage_events", l4.longValue(), false);
                            return new WidgetHomeViewModel.GuildInfo(longValue, booleanValue, state, can, guildExperiment != null && guildExperiment.getBucket() == 1);
                        }
                    });
                }
            }), StoreUser.observeMe$default(storeUser, false, 1, null), storeChannelsSelected.observeResolvedSelectedChannel(), storeExperiments.observeUserExperiment("2021-09_events_upsell", false), storeUserConnections.observeConnectedAccounts(), storeExperiments.observeUserExperiment("2021-12_connected_accounts_playstation", false), WidgetHomeViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(d, "Observable.combineLatest…counts,\n        )\n      }");
            return d;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetHomeViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\t\u0004\u0005\u0006\u0007\b\t\n\u000b\fB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\t\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", "", HookHelper.constructorName, "()V", "AnimatePeekIn", "AnimatePeekOut", "ClosePanels", "OpenLeftPanel", "ShowChannelOnboardingSheet", "ShowGuildEventUpsell", "ShowGuildWelcomeSheet", "ShowPlaystationUpsell", "UnlockLeftPanel", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ClosePanels;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$OpenLeftPanel;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$UnlockLeftPanel;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowChannelOnboardingSheet;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$AnimatePeekIn;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$AnimatePeekOut;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildEventUpsell;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowPlaystationUpsell;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$AnimatePeekIn;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AnimatePeekIn extends Event {
            public static final AnimatePeekIn INSTANCE = new AnimatePeekIn();

            private AnimatePeekIn() {
                super(null);
            }
        }

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$AnimatePeekOut;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AnimatePeekOut extends Event {
            public static final AnimatePeekOut INSTANCE = new AnimatePeekOut();

            private AnimatePeekOut() {
                super(null);
            }
        }

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ClosePanels;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ClosePanels extends Event {
            public static final ClosePanels INSTANCE = new ClosePanels();

            private ClosePanels() {
                super(null);
            }
        }

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$OpenLeftPanel;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class OpenLeftPanel extends Event {
            public static final OpenLeftPanel INSTANCE = new OpenLeftPanel();

            private OpenLeftPanel() {
                super(null);
            }
        }

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowChannelOnboardingSheet;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowChannelOnboardingSheet extends Event {
            public static final ShowChannelOnboardingSheet INSTANCE = new ShowChannelOnboardingSheet();

            private ShowChannelOnboardingSheet() {
                super(null);
            }
        }

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildEventUpsell;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "guildId", "copy", "(J)Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildEventUpsell;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowGuildEventUpsell extends Event {
            private final long guildId;

            public ShowGuildEventUpsell(long j) {
                super(null);
                this.guildId = j;
            }

            public static /* synthetic */ ShowGuildEventUpsell copy$default(ShowGuildEventUpsell showGuildEventUpsell, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = showGuildEventUpsell.guildId;
                }
                return showGuildEventUpsell.copy(j);
            }

            public final long component1() {
                return this.guildId;
            }

            public final ShowGuildEventUpsell copy(long j) {
                return new ShowGuildEventUpsell(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowGuildEventUpsell) && this.guildId == ((ShowGuildEventUpsell) obj).guildId;
                }
                return true;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public int hashCode() {
                return a0.a.a.b.a(this.guildId);
            }

            public String toString() {
                return a.B(a.R("ShowGuildEventUpsell(guildId="), this.guildId, ")");
            }
        }

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "guildId", "copy", "(J)Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowGuildWelcomeSheet extends Event {
            private final long guildId;

            public ShowGuildWelcomeSheet(long j) {
                super(null);
                this.guildId = j;
            }

            public static /* synthetic */ ShowGuildWelcomeSheet copy$default(ShowGuildWelcomeSheet showGuildWelcomeSheet, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = showGuildWelcomeSheet.guildId;
                }
                return showGuildWelcomeSheet.copy(j);
            }

            public final long component1() {
                return this.guildId;
            }

            public final ShowGuildWelcomeSheet copy(long j) {
                return new ShowGuildWelcomeSheet(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowGuildWelcomeSheet) && this.guildId == ((ShowGuildWelcomeSheet) obj).guildId;
                }
                return true;
            }

            public final long getGuildId() {
                return this.guildId;
            }

            public int hashCode() {
                return a0.a.a.b.a(this.guildId);
            }

            public String toString() {
                return a.B(a.R("ShowGuildWelcomeSheet(guildId="), this.guildId, ")");
            }
        }

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowPlaystationUpsell;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowPlaystationUpsell extends Event {
            public static final ShowPlaystationUpsell INSTANCE = new ShowPlaystationUpsell();

            private ShowPlaystationUpsell() {
                super(null);
            }
        }

        /* compiled from: WidgetHomeViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$Event$UnlockLeftPanel;", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UnlockLeftPanel extends Event {
            public static final UnlockLeftPanel INSTANCE = new UnlockLeftPanel();

            private UnlockLeftPanel() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetHomeViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\n\u0010\u000e\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0006\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\u0011\u001a\u00020\u0006\u0012\u0006\u0010\u0012\u001a\u00020\u0006¢\u0006\u0004\b%\u0010&J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ\u0010\u0010\r\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\r\u0010\bJH\u0010\u0013\u001a\u00020\u00002\f\b\u0002\u0010\u000e\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u000f\u001a\u00020\u00062\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\u0011\u001a\u00020\u00062\b\b\u0002\u0010\u0012\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\u00062\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u000bR\u001d\u0010\u000e\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010 \u001a\u0004\b!\u0010\u0005R\u0019\u0010\u000f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b\u000f\u0010\bR\u0019\u0010\u0011\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\"\u001a\u0004\b#\u0010\bR\u0019\u0010\u0012\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b$\u0010\b¨\u0006'"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Z", "Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "component3", "()Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "component4", "component5", "guildId", "isLurking", "welcomeScreenState", "canManageEvents", "canGuildShowEvents", "copy", "(JZLcom/discord/stores/StoreGuildWelcomeScreens$State;ZZ)Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreGuildWelcomeScreens$State;", "getWelcomeScreenState", "J", "getGuildId", "Z", "getCanManageEvents", "getCanGuildShowEvents", HookHelper.constructorName, "(JZLcom/discord/stores/StoreGuildWelcomeScreens$State;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GuildInfo {
        private final boolean canGuildShowEvents;
        private final boolean canManageEvents;
        private final long guildId;
        private final boolean isLurking;
        private final StoreGuildWelcomeScreens.State welcomeScreenState;

        public GuildInfo(long j, boolean z2, StoreGuildWelcomeScreens.State state, boolean z3, boolean z4) {
            this.guildId = j;
            this.isLurking = z2;
            this.welcomeScreenState = state;
            this.canManageEvents = z3;
            this.canGuildShowEvents = z4;
        }

        public static /* synthetic */ GuildInfo copy$default(GuildInfo guildInfo, long j, boolean z2, StoreGuildWelcomeScreens.State state, boolean z3, boolean z4, int i, Object obj) {
            if ((i & 1) != 0) {
                j = guildInfo.guildId;
            }
            long j2 = j;
            if ((i & 2) != 0) {
                z2 = guildInfo.isLurking;
            }
            boolean z5 = z2;
            if ((i & 4) != 0) {
                state = guildInfo.welcomeScreenState;
            }
            StoreGuildWelcomeScreens.State state2 = state;
            if ((i & 8) != 0) {
                z3 = guildInfo.canManageEvents;
            }
            boolean z6 = z3;
            if ((i & 16) != 0) {
                z4 = guildInfo.canGuildShowEvents;
            }
            return guildInfo.copy(j2, z5, state2, z6, z4);
        }

        public final long component1() {
            return this.guildId;
        }

        public final boolean component2() {
            return this.isLurking;
        }

        public final StoreGuildWelcomeScreens.State component3() {
            return this.welcomeScreenState;
        }

        public final boolean component4() {
            return this.canManageEvents;
        }

        public final boolean component5() {
            return this.canGuildShowEvents;
        }

        public final GuildInfo copy(long j, boolean z2, StoreGuildWelcomeScreens.State state, boolean z3, boolean z4) {
            return new GuildInfo(j, z2, state, z3, z4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GuildInfo)) {
                return false;
            }
            GuildInfo guildInfo = (GuildInfo) obj;
            return this.guildId == guildInfo.guildId && this.isLurking == guildInfo.isLurking && m.areEqual(this.welcomeScreenState, guildInfo.welcomeScreenState) && this.canManageEvents == guildInfo.canManageEvents && this.canGuildShowEvents == guildInfo.canGuildShowEvents;
        }

        public final boolean getCanGuildShowEvents() {
            return this.canGuildShowEvents;
        }

        public final boolean getCanManageEvents() {
            return this.canManageEvents;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final StoreGuildWelcomeScreens.State getWelcomeScreenState() {
            return this.welcomeScreenState;
        }

        public int hashCode() {
            int a = a0.a.a.b.a(this.guildId) * 31;
            boolean z2 = this.isLurking;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (a + i2) * 31;
            StoreGuildWelcomeScreens.State state = this.welcomeScreenState;
            int hashCode = (i4 + (state != null ? state.hashCode() : 0)) * 31;
            boolean z3 = this.canManageEvents;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (hashCode + i5) * 31;
            boolean z4 = this.canGuildShowEvents;
            if (!z4) {
                i = z4 ? 1 : 0;
            }
            return i7 + i;
        }

        public final boolean isLurking() {
            return this.isLurking;
        }

        public String toString() {
            StringBuilder R = a.R("GuildInfo(guildId=");
            R.append(this.guildId);
            R.append(", isLurking=");
            R.append(this.isLurking);
            R.append(", welcomeScreenState=");
            R.append(this.welcomeScreenState);
            R.append(", canManageEvents=");
            R.append(this.canManageEvents);
            R.append(", canGuildShowEvents=");
            return a.M(R, this.canGuildShowEvents, ")");
        }
    }

    /* compiled from: WidgetHomeViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001Be\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0002\u0012\u0006\u0010\u001e\u001a\u00020\u0006\u0012\u0006\u0010\u001f\u001a\u00020\t\u0012\u0006\u0010 \u001a\u00020\f\u0012\u0006\u0010!\u001a\u00020\f\u0012\u0006\u0010\"\u001a\u00020\f\u0012\u0006\u0010#\u001a\u00020\f\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0012\u0012\n\u0010%\u001a\u00060\u0015j\u0002`\u0016\u0012\u0006\u0010&\u001a\u00020\u0019¢\u0006\u0004\b@\u0010AJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000eJ\u0010\u0010\u0011\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000eJ\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0014\u0010\u0017\u001a\u00060\u0015j\u0002`\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0084\u0001\u0010'\u001a\u00020\u00002\b\b\u0002\u0010\u001c\u001a\u00020\u00022\b\b\u0002\u0010\u001d\u001a\u00020\u00022\b\b\u0002\u0010\u001e\u001a\u00020\u00062\b\b\u0002\u0010\u001f\u001a\u00020\t2\b\b\u0002\u0010 \u001a\u00020\f2\b\b\u0002\u0010!\u001a\u00020\f2\b\b\u0002\u0010\"\u001a\u00020\f2\b\b\u0002\u0010#\u001a\u00020\f2\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00122\f\b\u0002\u0010%\u001a\u00060\u0015j\u0002`\u00162\b\b\u0002\u0010&\u001a\u00020\u0019HÆ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010*\u001a\u00020)HÖ\u0001¢\u0006\u0004\b*\u0010+J\u0010\u0010-\u001a\u00020,HÖ\u0001¢\u0006\u0004\b-\u0010.J\u001a\u00100\u001a\u00020\f2\b\u0010/\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b0\u00101R\u001d\u0010%\u001a\u00060\u0015j\u0002`\u00168\u0006@\u0006¢\u0006\f\n\u0004\b%\u00102\u001a\u0004\b3\u0010\u0018R\u0019\u0010#\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\b#\u0010\u000eR\u0019\u0010&\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b&\u00105\u001a\u0004\b6\u0010\u001bR\u0019\u0010!\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00104\u001a\u0004\b!\u0010\u000eR\u001b\u0010$\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b$\u00107\u001a\u0004\b8\u0010\u0014R\u0019\u0010\u001d\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00109\u001a\u0004\b:\u0010\u0004R\u0019\u0010\"\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00104\u001a\u0004\b\"\u0010\u000eR\u0019\u0010 \u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b \u00104\u001a\u0004\b \u0010\u000eR\u0019\u0010\u001e\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010;\u001a\u0004\b<\u0010\bR\u0019\u0010\u001f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010=\u001a\u0004\b>\u0010\u000bR\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00109\u001a\u0004\b?\u0010\u0004¨\u0006B"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;", "", "Lcom/discord/panels/PanelState;", "component1", "()Lcom/discord/panels/PanelState;", "component2", "Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;", "component3", "()Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;", "Lcom/discord/models/user/MeUser;", "component4", "()Lcom/discord/models/user/MeUser;", "", "component5", "()Z", "component6", "component7", "component8", "Lcom/discord/api/user/NsfwAllowance;", "component9", "()Lcom/discord/api/user/NsfwAllowance;", "", "Lcom/discord/primitives/GuildId;", "component10", "()J", "Lcom/discord/stores/StoreUserConnections$State;", "component11", "()Lcom/discord/stores/StoreUserConnections$State;", "leftPanelState", "rightPanelState", "guildInfo", "me", "isThreadPeek", "isInEventsUpsellExperiment", "isNsfwUnconsented", "isChannelNsfw", "nsfwAllowed", "guildId", "connectedAccountsState", "copy", "(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;Lcom/discord/models/user/MeUser;ZZZZLcom/discord/api/user/NsfwAllowance;JLcom/discord/stores/StoreUserConnections$State;)Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getGuildId", "Z", "Lcom/discord/stores/StoreUserConnections$State;", "getConnectedAccountsState", "Lcom/discord/api/user/NsfwAllowance;", "getNsfwAllowed", "Lcom/discord/panels/PanelState;", "getRightPanelState", "Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;", "getGuildInfo", "Lcom/discord/models/user/MeUser;", "getMe", "getLeftPanelState", HookHelper.constructorName, "(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;Lcom/discord/models/user/MeUser;ZZZZLcom/discord/api/user/NsfwAllowance;JLcom/discord/stores/StoreUserConnections$State;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final StoreUserConnections.State connectedAccountsState;
        private final long guildId;
        private final GuildInfo guildInfo;
        private final boolean isChannelNsfw;
        private final boolean isInEventsUpsellExperiment;
        private final boolean isNsfwUnconsented;
        private final boolean isThreadPeek;
        private final PanelState leftPanelState;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2831me;
        private final NsfwAllowance nsfwAllowed;
        private final PanelState rightPanelState;

        public StoreState(PanelState panelState, PanelState panelState2, GuildInfo guildInfo, MeUser meUser, boolean z2, boolean z3, boolean z4, boolean z5, NsfwAllowance nsfwAllowance, long j, StoreUserConnections.State state) {
            m.checkNotNullParameter(panelState, "leftPanelState");
            m.checkNotNullParameter(panelState2, "rightPanelState");
            m.checkNotNullParameter(guildInfo, "guildInfo");
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(state, "connectedAccountsState");
            this.leftPanelState = panelState;
            this.rightPanelState = panelState2;
            this.guildInfo = guildInfo;
            this.f2831me = meUser;
            this.isThreadPeek = z2;
            this.isInEventsUpsellExperiment = z3;
            this.isNsfwUnconsented = z4;
            this.isChannelNsfw = z5;
            this.nsfwAllowed = nsfwAllowance;
            this.guildId = j;
            this.connectedAccountsState = state;
        }

        public final PanelState component1() {
            return this.leftPanelState;
        }

        public final long component10() {
            return this.guildId;
        }

        public final StoreUserConnections.State component11() {
            return this.connectedAccountsState;
        }

        public final PanelState component2() {
            return this.rightPanelState;
        }

        public final GuildInfo component3() {
            return this.guildInfo;
        }

        public final MeUser component4() {
            return this.f2831me;
        }

        public final boolean component5() {
            return this.isThreadPeek;
        }

        public final boolean component6() {
            return this.isInEventsUpsellExperiment;
        }

        public final boolean component7() {
            return this.isNsfwUnconsented;
        }

        public final boolean component8() {
            return this.isChannelNsfw;
        }

        public final NsfwAllowance component9() {
            return this.nsfwAllowed;
        }

        public final StoreState copy(PanelState panelState, PanelState panelState2, GuildInfo guildInfo, MeUser meUser, boolean z2, boolean z3, boolean z4, boolean z5, NsfwAllowance nsfwAllowance, long j, StoreUserConnections.State state) {
            m.checkNotNullParameter(panelState, "leftPanelState");
            m.checkNotNullParameter(panelState2, "rightPanelState");
            m.checkNotNullParameter(guildInfo, "guildInfo");
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(state, "connectedAccountsState");
            return new StoreState(panelState, panelState2, guildInfo, meUser, z2, z3, z4, z5, nsfwAllowance, j, state);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.leftPanelState, storeState.leftPanelState) && m.areEqual(this.rightPanelState, storeState.rightPanelState) && m.areEqual(this.guildInfo, storeState.guildInfo) && m.areEqual(this.f2831me, storeState.f2831me) && this.isThreadPeek == storeState.isThreadPeek && this.isInEventsUpsellExperiment == storeState.isInEventsUpsellExperiment && this.isNsfwUnconsented == storeState.isNsfwUnconsented && this.isChannelNsfw == storeState.isChannelNsfw && m.areEqual(this.nsfwAllowed, storeState.nsfwAllowed) && this.guildId == storeState.guildId && m.areEqual(this.connectedAccountsState, storeState.connectedAccountsState);
        }

        public final StoreUserConnections.State getConnectedAccountsState() {
            return this.connectedAccountsState;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final GuildInfo getGuildInfo() {
            return this.guildInfo;
        }

        public final PanelState getLeftPanelState() {
            return this.leftPanelState;
        }

        public final MeUser getMe() {
            return this.f2831me;
        }

        public final NsfwAllowance getNsfwAllowed() {
            return this.nsfwAllowed;
        }

        public final PanelState getRightPanelState() {
            return this.rightPanelState;
        }

        public int hashCode() {
            PanelState panelState = this.leftPanelState;
            int i = 0;
            int hashCode = (panelState != null ? panelState.hashCode() : 0) * 31;
            PanelState panelState2 = this.rightPanelState;
            int hashCode2 = (hashCode + (panelState2 != null ? panelState2.hashCode() : 0)) * 31;
            GuildInfo guildInfo = this.guildInfo;
            int hashCode3 = (hashCode2 + (guildInfo != null ? guildInfo.hashCode() : 0)) * 31;
            MeUser meUser = this.f2831me;
            int hashCode4 = (hashCode3 + (meUser != null ? meUser.hashCode() : 0)) * 31;
            boolean z2 = this.isThreadPeek;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode4 + i3) * 31;
            boolean z3 = this.isInEventsUpsellExperiment;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            boolean z4 = this.isNsfwUnconsented;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int i11 = (i8 + i9) * 31;
            boolean z5 = this.isChannelNsfw;
            if (!z5) {
                i2 = z5 ? 1 : 0;
            }
            int i12 = (i11 + i2) * 31;
            NsfwAllowance nsfwAllowance = this.nsfwAllowed;
            int a = (a0.a.a.b.a(this.guildId) + ((i12 + (nsfwAllowance != null ? nsfwAllowance.hashCode() : 0)) * 31)) * 31;
            StoreUserConnections.State state = this.connectedAccountsState;
            if (state != null) {
                i = state.hashCode();
            }
            return a + i;
        }

        public final boolean isChannelNsfw() {
            return this.isChannelNsfw;
        }

        public final boolean isInEventsUpsellExperiment() {
            return this.isInEventsUpsellExperiment;
        }

        public final boolean isNsfwUnconsented() {
            return this.isNsfwUnconsented;
        }

        public final boolean isThreadPeek() {
            return this.isThreadPeek;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(leftPanelState=");
            R.append(this.leftPanelState);
            R.append(", rightPanelState=");
            R.append(this.rightPanelState);
            R.append(", guildInfo=");
            R.append(this.guildInfo);
            R.append(", me=");
            R.append(this.f2831me);
            R.append(", isThreadPeek=");
            R.append(this.isThreadPeek);
            R.append(", isInEventsUpsellExperiment=");
            R.append(this.isInEventsUpsellExperiment);
            R.append(", isNsfwUnconsented=");
            R.append(this.isNsfwUnconsented);
            R.append(", isChannelNsfw=");
            R.append(this.isChannelNsfw);
            R.append(", nsfwAllowed=");
            R.append(this.nsfwAllowed);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", connectedAccountsState=");
            R.append(this.connectedAccountsState);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: WidgetHomeViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0006\u0012\u0006\u0010\u0014\u001a\u00020\u0006\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\n\u0012\n\u0010\u0016\u001a\u00060\rj\u0002`\u000e¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0014\u0010\u000f\u001a\u00060\rj\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JR\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00062\b\b\u0002\u0010\u0014\u001a\u00020\u00062\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\n2\f\b\u0002\u0010\u0016\u001a\u00060\rj\u0002`\u000eHÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010 \u001a\u00020\u00062\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u0013\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b\u0013\u0010\bR\u0019\u0010\u0014\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\"\u001a\u0004\b\u0014\u0010\bR\u001d\u0010\u0016\u001a\u00060\rj\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b$\u0010\u0010R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010%\u001a\u0004\b&\u0010\u0004R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010'\u001a\u0004\b(\u0010\fR\u0019\u0010\u0011\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010%\u001a\u0004\b)\u0010\u0004¨\u0006,"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;", "", "Lcom/discord/panels/PanelState;", "component1", "()Lcom/discord/panels/PanelState;", "component2", "", "component3", "()Z", "component4", "Lcom/discord/api/user/NsfwAllowance;", "component5", "()Lcom/discord/api/user/NsfwAllowance;", "", "Lcom/discord/primitives/GuildId;", "component6", "()J", "leftPanelState", "rightPanelState", "isNsfwUnconsented", "isChannelNsfw", "nsfwAllowed", "guildId", "copy", "(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;ZZLcom/discord/api/user/NsfwAllowance;J)Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "J", "getGuildId", "Lcom/discord/panels/PanelState;", "getRightPanelState", "Lcom/discord/api/user/NsfwAllowance;", "getNsfwAllowed", "getLeftPanelState", HookHelper.constructorName, "(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;ZZLcom/discord/api/user/NsfwAllowance;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final long guildId;
        private final boolean isChannelNsfw;
        private final boolean isNsfwUnconsented;
        private final PanelState leftPanelState;
        private final NsfwAllowance nsfwAllowed;
        private final PanelState rightPanelState;

        public ViewState(PanelState panelState, PanelState panelState2, boolean z2, boolean z3, NsfwAllowance nsfwAllowance, long j) {
            m.checkNotNullParameter(panelState, "leftPanelState");
            m.checkNotNullParameter(panelState2, "rightPanelState");
            this.leftPanelState = panelState;
            this.rightPanelState = panelState2;
            this.isNsfwUnconsented = z2;
            this.isChannelNsfw = z3;
            this.nsfwAllowed = nsfwAllowance;
            this.guildId = j;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, PanelState panelState, PanelState panelState2, boolean z2, boolean z3, NsfwAllowance nsfwAllowance, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                panelState = viewState.leftPanelState;
            }
            if ((i & 2) != 0) {
                panelState2 = viewState.rightPanelState;
            }
            PanelState panelState3 = panelState2;
            if ((i & 4) != 0) {
                z2 = viewState.isNsfwUnconsented;
            }
            boolean z4 = z2;
            if ((i & 8) != 0) {
                z3 = viewState.isChannelNsfw;
            }
            boolean z5 = z3;
            if ((i & 16) != 0) {
                nsfwAllowance = viewState.nsfwAllowed;
            }
            NsfwAllowance nsfwAllowance2 = nsfwAllowance;
            if ((i & 32) != 0) {
                j = viewState.guildId;
            }
            return viewState.copy(panelState, panelState3, z4, z5, nsfwAllowance2, j);
        }

        public final PanelState component1() {
            return this.leftPanelState;
        }

        public final PanelState component2() {
            return this.rightPanelState;
        }

        public final boolean component3() {
            return this.isNsfwUnconsented;
        }

        public final boolean component4() {
            return this.isChannelNsfw;
        }

        public final NsfwAllowance component5() {
            return this.nsfwAllowed;
        }

        public final long component6() {
            return this.guildId;
        }

        public final ViewState copy(PanelState panelState, PanelState panelState2, boolean z2, boolean z3, NsfwAllowance nsfwAllowance, long j) {
            m.checkNotNullParameter(panelState, "leftPanelState");
            m.checkNotNullParameter(panelState2, "rightPanelState");
            return new ViewState(panelState, panelState2, z2, z3, nsfwAllowance, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.leftPanelState, viewState.leftPanelState) && m.areEqual(this.rightPanelState, viewState.rightPanelState) && this.isNsfwUnconsented == viewState.isNsfwUnconsented && this.isChannelNsfw == viewState.isChannelNsfw && m.areEqual(this.nsfwAllowed, viewState.nsfwAllowed) && this.guildId == viewState.guildId;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final PanelState getLeftPanelState() {
            return this.leftPanelState;
        }

        public final NsfwAllowance getNsfwAllowed() {
            return this.nsfwAllowed;
        }

        public final PanelState getRightPanelState() {
            return this.rightPanelState;
        }

        public int hashCode() {
            PanelState panelState = this.leftPanelState;
            int i = 0;
            int hashCode = (panelState != null ? panelState.hashCode() : 0) * 31;
            PanelState panelState2 = this.rightPanelState;
            int hashCode2 = (hashCode + (panelState2 != null ? panelState2.hashCode() : 0)) * 31;
            boolean z2 = this.isNsfwUnconsented;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode2 + i3) * 31;
            boolean z3 = this.isChannelNsfw;
            if (!z3) {
                i2 = z3 ? 1 : 0;
            }
            int i6 = (i5 + i2) * 31;
            NsfwAllowance nsfwAllowance = this.nsfwAllowed;
            if (nsfwAllowance != null) {
                i = nsfwAllowance.hashCode();
            }
            return a0.a.a.b.a(this.guildId) + ((i6 + i) * 31);
        }

        public final boolean isChannelNsfw() {
            return this.isChannelNsfw;
        }

        public final boolean isNsfwUnconsented() {
            return this.isNsfwUnconsented;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(leftPanelState=");
            R.append(this.leftPanelState);
            R.append(", rightPanelState=");
            R.append(this.rightPanelState);
            R.append(", isNsfwUnconsented=");
            R.append(this.isNsfwUnconsented);
            R.append(", isChannelNsfw=");
            R.append(this.isChannelNsfw);
            R.append(", nsfwAllowed=");
            R.append(this.nsfwAllowed);
            R.append(", guildId=");
            return a.B(R, this.guildId, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StoreNavigation.PanelAction.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreNavigation.PanelAction.OPEN.ordinal()] = 1;
            iArr[StoreNavigation.PanelAction.CLOSE.ordinal()] = 2;
            iArr[StoreNavigation.PanelAction.UNLOCK_LEFT.ordinal()] = 3;
            iArr[StoreNavigation.PanelAction.NOOP.ordinal()] = 4;
        }
    }

    public WidgetHomeViewModel() {
        this(null, null, null, null, null, null, null, null, null, FrameMetricsAggregator.EVERY_DURATION, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetHomeViewModel(com.discord.stores.StoreNavigation r16, com.discord.stores.StoreGuildWelcomeScreens r17, com.discord.stores.StoreExperiments r18, com.discord.stores.StoreUser r19, rx.Observable r20, rx.Observable r21, com.discord.widgets.channels.ChannelOnboardingManager r22, android.content.SharedPreferences r23, com.discord.utilities.time.Clock r24, int r25, kotlin.jvm.internal.DefaultConstructorMarker r26) {
        /*
            r15 = this;
            r0 = r25
            r1 = r0 & 1
            if (r1 == 0) goto Ld
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreNavigation r1 = r1.getNavigation()
            goto Lf
        Ld:
            r1 = r16
        Lf:
            r2 = r0 & 2
            if (r2 == 0) goto L1b
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildWelcomeScreens r2 = r2.getGuildWelcomeScreens()
            r12 = r2
            goto L1d
        L1b:
            r12 = r17
        L1d:
            r2 = r0 & 4
            if (r2 == 0) goto L29
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreExperiments r2 = r2.getExperiments()
            r13 = r2
            goto L2b
        L29:
            r13 = r18
        L2b:
            r2 = r0 & 8
            if (r2 == 0) goto L37
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r2 = r2.getUsers()
            r14 = r2
            goto L39
        L37:
            r14 = r19
        L39:
            r2 = r0 & 16
            if (r2 == 0) goto L5f
            com.discord.widgets.home.WidgetHomeViewModel$Companion r2 = com.discord.widgets.home.WidgetHomeViewModel.Companion
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildSelected r4 = r3.getGuildSelected()
            com.discord.stores.StoreLurking r5 = r3.getLurking()
            com.discord.stores.StoreChannelsSelected r8 = r3.getChannelsSelected()
            com.discord.stores.StorePermissions r10 = r3.getPermissions()
            com.discord.stores.StoreUserConnections r11 = r3.getUserConnections()
            r3 = r4
            r4 = r1
            r6 = r14
            r7 = r12
            r9 = r13
            rx.Observable r2 = com.discord.widgets.home.WidgetHomeViewModel.Companion.access$observeStoreState(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            goto L61
        L5f:
            r2 = r20
        L61:
            r3 = r0 & 32
            if (r3 == 0) goto L6a
            rx.Observable r3 = r1.getNavigationPanelAction()
            goto L6c
        L6a:
            r3 = r21
        L6c:
            r4 = r0 & 64
            if (r4 == 0) goto L76
            com.discord.widgets.channels.ChannelOnboardingManager r4 = new com.discord.widgets.channels.ChannelOnboardingManager
            r4.<init>()
            goto L78
        L76:
            r4 = r22
        L78:
            r5 = r0 & 128(0x80, float:1.794E-43)
            if (r5 == 0) goto L83
            com.discord.utilities.cache.SharedPreferencesProvider r5 = com.discord.utilities.cache.SharedPreferencesProvider.INSTANCE
            android.content.SharedPreferences r5 = r5.get()
            goto L85
        L83:
            r5 = r23
        L85:
            r0 = r0 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L8e
            com.discord.utilities.time.Clock r0 = com.discord.utilities.time.ClockFactory.get()
            goto L90
        L8e:
            r0 = r24
        L90:
            r16 = r15
            r17 = r1
            r18 = r12
            r19 = r13
            r20 = r14
            r21 = r2
            r22 = r3
            r23 = r4
            r24 = r5
            r25 = r0
            r16.<init>(r17, r18, r19, r20, r21, r22, r23, r24, r25)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.home.WidgetHomeViewModel.<init>(com.discord.stores.StoreNavigation, com.discord.stores.StoreGuildWelcomeScreens, com.discord.stores.StoreExperiments, com.discord.stores.StoreUser, rx.Observable, rx.Observable, com.discord.widgets.channels.ChannelOnboardingManager, android.content.SharedPreferences, com.discord.utilities.time.Clock, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    @MainThread
    private final void emitAnimatePeekIn() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.AnimatePeekIn.INSTANCE);
    }

    @MainThread
    private final void emitAnimatePeekOut() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.AnimatePeekOut.INSTANCE);
    }

    @MainThread
    private final void emitClosePanelsEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ClosePanels.INSTANCE);
    }

    @MainThread
    private final void emitShowChannelOnboardingSheet() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowChannelOnboardingSheet.INSTANCE);
    }

    @MainThread
    private final void emitShowGuildEventUpsell(long j) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowGuildEventUpsell(j));
    }

    @MainThread
    private final void emitShowPlaystationUpsell() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowPlaystationUpsell.INSTANCE);
    }

    @MainThread
    private final void emitShowWelcomeSheet(long j) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowGuildWelcomeSheet(j));
    }

    @MainThread
    private final void emitUnlockLeftPanelEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.UnlockLeftPanel.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleNavDrawerAction(StoreNavigation.PanelAction panelAction) {
        int ordinal = panelAction.ordinal();
        if (ordinal == 1) {
            emitOpenLeftPanelEvent();
        } else if (ordinal == 2) {
            emitClosePanelsEvent();
        } else if (ordinal == 3) {
            emitUnlockLeftPanelEvent();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        PanelState leftPanelState = storeState.getLeftPanelState();
        PanelState rightPanelState = storeState.getRightPanelState();
        long guildId = storeState.getGuildInfo().getGuildId();
        boolean isLurking = storeState.getGuildInfo().isLurking();
        StoreGuildWelcomeScreens.State welcomeScreenState = storeState.getGuildInfo().getWelcomeScreenState();
        boolean isNewUser = UserUtils.INSTANCE.isNewUser(storeState.getMe(), this.clock);
        updateViewState(new ViewState(leftPanelState, rightPanelState, storeState.isNsfwUnconsented(), storeState.isChannelNsfw(), storeState.getNsfwAllowed(), storeState.getGuildId()));
        if (isLurking && !this.storeGuildWelcomeScreens.hasWelcomeScreenBeenSeen(guildId)) {
            if (welcomeScreenState instanceof StoreGuildWelcomeScreens.State.Loaded) {
                emitShowWelcomeSheet(guildId);
            } else if (welcomeScreenState == null) {
                this.storeGuildWelcomeScreens.fetchIfNonexisting(guildId);
            }
        }
        if (shouldShowChannelOnboardingSheet(this.previousLeftPanelState, leftPanelState, guildId, isNewUser)) {
            emitShowChannelOnboardingSheet();
        } else if (shouldShowGuildEventUpsell(storeState.getGuildInfo().getCanManageEvents(), storeState.getGuildInfo().getCanGuildShowEvents(), storeState.isInEventsUpsellExperiment())) {
            SharedPreferences.Editor edit = this.sharedPreferences.edit();
            m.checkNotNullExpressionValue(edit, "editor");
            edit.putBoolean(WidgetGuildScheduledEventUpsellBottomSheet.GUILD_EVENT_UPSELL_CACHE_KEY, true);
            edit.apply();
            emitShowGuildEventUpsell(guildId);
        } else if (this.playstationUpsellManager.canShow(storeState.getMe(), storeState.getConnectedAccountsState())) {
            emitShowPlaystationUpsell();
            this.playstationUpsellManager.setHasBeenShown();
        }
        this.previousLeftPanelState = leftPanelState;
        if (storeState.isThreadPeek() && !this.wasThreadPeek) {
            emitAnimatePeekIn();
        }
        if (!storeState.isThreadPeek() && this.wasThreadPeek) {
            emitAnimatePeekOut();
        }
        this.wasThreadPeek = storeState.isThreadPeek();
    }

    private final boolean shouldShowChannelOnboardingSheet(PanelState panelState, PanelState panelState2, long j, boolean z2) {
        return ((panelState instanceof PanelState.d) && (panelState2 instanceof PanelState.c)) && ((j > 0L ? 1 : (j == 0L ? 0 : -1)) != 0) && z2 && !this.channelOnboardingManager.hasUserSeenChannelOnboarding();
    }

    private final boolean shouldShowGuildEventUpsell(boolean z2, boolean z3, boolean z4) {
        return z2 && !this.sharedPreferences.getBoolean(WidgetGuildScheduledEventUpsellBottomSheet.GUILD_EVENT_UPSELL_CACHE_KEY, false) && z4 && z3;
    }

    @MainThread
    public final void emitOpenLeftPanelEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.OpenLeftPanel.INSTANCE);
    }

    public final WidgetHomeModel getWidgetHomeModel$app_productionGoogleRelease() {
        return this.widgetHomeModel;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void onEndPanelStateChange(PanelState panelState) {
        m.checkNotNullParameter(panelState, "panelState");
        this.storeNavigation.setRightPanelState(panelState);
    }

    public final void onStartPanelStateChange(PanelState panelState) {
        m.checkNotNullParameter(panelState, "panelState");
        this.storeNavigation.setLeftPanelState(panelState);
    }

    public final void setWidgetHomeModel$app_productionGoogleRelease(WidgetHomeModel widgetHomeModel) {
        this.widgetHomeModel = widgetHomeModel;
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public WidgetHomeViewModel(com.discord.stores.StoreNavigation r27, com.discord.stores.StoreGuildWelcomeScreens r28, com.discord.stores.StoreExperiments r29, com.discord.stores.StoreUser r30, rx.Observable<com.discord.widgets.home.WidgetHomeViewModel.StoreState> r31, rx.Observable<com.discord.stores.StoreNavigation.PanelAction> r32, com.discord.widgets.channels.ChannelOnboardingManager r33, android.content.SharedPreferences r34, com.discord.utilities.time.Clock r35) {
        /*
            r26 = this;
            r0 = r26
            r1 = r27
            r2 = r28
            r3 = r29
            r4 = r30
            r5 = r32
            r6 = r33
            r7 = r34
            r8 = r35
            java.lang.String r9 = "storeNavigation"
            d0.z.d.m.checkNotNullParameter(r1, r9)
            java.lang.String r9 = "storeGuildWelcomeScreens"
            d0.z.d.m.checkNotNullParameter(r2, r9)
            java.lang.String r9 = "storeExperiments"
            d0.z.d.m.checkNotNullParameter(r3, r9)
            java.lang.String r9 = "storeUser"
            d0.z.d.m.checkNotNullParameter(r4, r9)
            java.lang.String r9 = "storeStateObservable"
            r10 = r31
            d0.z.d.m.checkNotNullParameter(r10, r9)
            java.lang.String r9 = "navPanelActionObservable"
            d0.z.d.m.checkNotNullParameter(r5, r9)
            java.lang.String r9 = "channelOnboardingManager"
            d0.z.d.m.checkNotNullParameter(r6, r9)
            java.lang.String r9 = "sharedPreferences"
            d0.z.d.m.checkNotNullParameter(r7, r9)
            java.lang.String r9 = "clock"
            d0.z.d.m.checkNotNullParameter(r8, r9)
            com.discord.widgets.home.WidgetHomeViewModel$ViewState r9 = new com.discord.widgets.home.WidgetHomeViewModel$ViewState
            com.discord.panels.PanelState$a r13 = com.discord.panels.PanelState.a.a
            com.discord.api.user.NsfwAllowance r16 = com.discord.api.user.NsfwAllowance.UNKNOWN
            r14 = 0
            r15 = 0
            r17 = 0
            r11 = r9
            r12 = r13
            r11.<init>(r12, r13, r14, r15, r16, r17)
            r0.<init>(r9)
            r0.storeNavigation = r1
            r0.storeGuildWelcomeScreens = r2
            r0.storeExperiments = r3
            r0.storeUser = r4
            r0.channelOnboardingManager = r6
            r0.sharedPreferences = r7
            r0.clock = r8
            rx.subjects.PublishSubject r1 = rx.subjects.PublishSubject.k0()
            r0.eventSubject = r1
            com.discord.widgets.playstation.PlaystationUpsellManager r1 = new com.discord.widgets.playstation.PlaystationUpsellManager
            r1.<init>(r7, r3, r8)
            r0.playstationUpsellManager = r1
            rx.Observable r1 = r31.q()
            java.lang.String r2 = "storeStateObservable\n   …  .distinctUntilChanged()"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)
            rx.Observable r1 = com.discord.utilities.rx.ObservableExtensionsKt.computationLatest(r1)
            r2 = 0
            r3 = 2
            rx.Observable r6 = com.discord.utilities.rx.ObservableExtensionsKt.ui$default(r1, r0, r2, r3, r2)
            java.lang.Class<com.discord.widgets.home.WidgetHomeViewModel> r7 = com.discord.widgets.home.WidgetHomeViewModel.class
            com.discord.widgets.home.WidgetHomeViewModel$1 r13 = new com.discord.widgets.home.WidgetHomeViewModel$1
            r13.<init>()
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r14 = 62
            r15 = 0
            com.discord.utilities.rx.ObservableExtensionsKt.appSubscribe$default(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            rx.Observable r16 = com.discord.utilities.rx.ObservableExtensionsKt.ui$default(r5, r0, r2, r3, r2)
            java.lang.Class<com.discord.widgets.home.WidgetHomeViewModel> r17 = com.discord.widgets.home.WidgetHomeViewModel.class
            com.discord.widgets.home.WidgetHomeViewModel$2 r1 = new com.discord.widgets.home.WidgetHomeViewModel$2
            r1.<init>()
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            r24 = 62
            r25 = 0
            r23 = r1
            com.discord.utilities.rx.ObservableExtensionsKt.appSubscribe$default(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.home.WidgetHomeViewModel.<init>(com.discord.stores.StoreNavigation, com.discord.stores.StoreGuildWelcomeScreens, com.discord.stores.StoreExperiments, com.discord.stores.StoreUser, rx.Observable, rx.Observable, com.discord.widgets.channels.ChannelOnboardingManager, android.content.SharedPreferences, com.discord.utilities.time.Clock):void");
    }
}
