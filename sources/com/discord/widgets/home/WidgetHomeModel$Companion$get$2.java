package com.discord.widgets.home;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.presence.Presence;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadsActive;
import com.discord.stores.StoreUser;
import com.discord.utilities.rx.ObservableWithLeadingEdgeThrottle;
import com.discord.widgets.chat.list.CreateThreadsFeatureFlag;
import d0.z.d.m;
import j0.k.b;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import rx.Observable;
import rx.functions.Func8;
/* compiled from: WidgetHomeModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001a*\u0012\u000e\b\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00070\u0007 \u0003*\u0014\u0012\u000e\b\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00060\u00062Z\u0010\u0005\u001aV\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00040\u0004 \u0003**\u0012\u0016\u0012\u0014 \u0003*\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00020\u0001j\u0002`\u0002\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lkotlin/Pair;", "", "Lcom/discord/primitives/GuildId;", "kotlin.jvm.PlatformType", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "<name for destructuring parameter 0>", "Lrx/Observable;", "Lcom/discord/widgets/home/WidgetHomeModel;", NotificationCompat.CATEGORY_CALL, "(Lkotlin/Pair;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHomeModel$Companion$get$2<T, R> implements b<Pair<? extends Long, ? extends StoreChannelsSelected.ResolvedSelectedChannel>, Observable<? extends WidgetHomeModel>> {
    public static final WidgetHomeModel$Companion$get$2 INSTANCE = new WidgetHomeModel$Companion$get$2();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Observable<? extends WidgetHomeModel> call(Pair<? extends Long, ? extends StoreChannelsSelected.ResolvedSelectedChannel> pair) {
        return call2((Pair<Long, ? extends StoreChannelsSelected.ResolvedSelectedChannel>) pair);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Observable<? extends WidgetHomeModel> call2(Pair<Long, ? extends StoreChannelsSelected.ResolvedSelectedChannel> pair) {
        Long l;
        long j;
        Long component1 = pair.component1();
        final StoreChannelsSelected.ResolvedSelectedChannel component2 = pair.component2();
        final Channel maybeChannel = component2.getMaybeChannel();
        StoreStream.Companion companion = StoreStream.Companion;
        Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
        Observable<Map<Long, Presence>> observeAllPresences = companion.getPresences().observeAllPresences();
        Observable<Channel> observeSelectedChannel = companion.getVoiceChannelSelected().observeSelectedChannel();
        Observable<Integer> observeTotalMentions = companion.getMentions().observeTotalMentions();
        Observable<Map<Long, Integer>> observe = companion.getUserRelationships().observe();
        StoreThreadsActive threadsActive = companion.getThreadsActive();
        m.checkNotNullExpressionValue(component1, "selectedGuildId");
        long longValue = component1.longValue();
        if (maybeChannel != null && ChannelUtils.C(maybeChannel)) {
            j = maybeChannel.r();
        } else if (maybeChannel != null) {
            j = maybeChannel.h();
        } else {
            l = null;
            return ObservableWithLeadingEdgeThrottle.combineLatest(observeMe$default, observeAllPresences, observeSelectedChannel, observeTotalMentions, observe, threadsActive.observeActiveThreadsForChannel(longValue, l), WidgetHomeModel.Companion.getParentChannelObservable(maybeChannel), new CreateThreadsFeatureFlag(null, null, 3, null).observeEnabled(component1.longValue()), new Func8<MeUser, Map<Long, ? extends Presence>, Channel, Integer, Map<Long, ? extends Integer>, Map<Long, ? extends Channel>, Channel, Boolean, WidgetHomeModel>() { // from class: com.discord.widgets.home.WidgetHomeModel$Companion$get$2.1
                @Override // rx.functions.Func8
                public /* bridge */ /* synthetic */ WidgetHomeModel call(MeUser meUser, Map<Long, ? extends Presence> map, Channel channel, Integer num, Map<Long, ? extends Integer> map2, Map<Long, ? extends Channel> map3, Channel channel2, Boolean bool) {
                    return call2(meUser, (Map<Long, Presence>) map, channel, num, (Map<Long, Integer>) map2, (Map<Long, Channel>) map3, channel2, bool);
                }

                /* JADX WARN: Removed duplicated region for block: B:35:0x0086  */
                /* renamed from: call  reason: avoid collision after fix types in other method */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public final com.discord.widgets.home.WidgetHomeModel call2(com.discord.models.user.MeUser r17, java.util.Map<java.lang.Long, com.discord.models.presence.Presence> r18, com.discord.api.channel.Channel r19, java.lang.Integer r20, java.util.Map<java.lang.Long, java.lang.Integer> r21, java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r22, com.discord.api.channel.Channel r23, java.lang.Boolean r24) {
                    /*
                        r16 = this;
                        r0 = r16
                        r1 = r21
                        com.discord.api.channel.Channel r2 = com.discord.api.channel.Channel.this
                        r3 = 1
                        if (r2 == 0) goto L11
                        boolean r2 = com.discord.api.channel.ChannelUtils.x(r2)
                        if (r2 != r3) goto L11
                        r2 = 1
                        goto L12
                    L11:
                        r2 = 0
                    L12:
                        r4 = 0
                        if (r2 == 0) goto L38
                        if (r19 == 0) goto L20
                        long r5 = r19.h()
                        java.lang.Long r2 = java.lang.Long.valueOf(r5)
                        goto L21
                    L20:
                        r2 = r4
                    L21:
                        com.discord.api.channel.Channel r5 = com.discord.api.channel.Channel.this
                        if (r5 == 0) goto L2e
                        long r5 = r5.h()
                        java.lang.Long r5 = java.lang.Long.valueOf(r5)
                        goto L2f
                    L2e:
                        r5 = r4
                    L2f:
                        boolean r2 = d0.z.d.m.areEqual(r2, r5)
                        if (r2 == 0) goto L38
                        r2 = 1
                        r12 = 1
                        goto L3a
                    L38:
                        r2 = 0
                        r12 = 0
                    L3a:
                        com.discord.stores.StoreChannelsSelected$ResolvedSelectedChannel r6 = r2
                        java.lang.String r2 = "selectedChannel"
                        d0.z.d.m.checkNotNullExpressionValue(r6, r2)
                        com.discord.api.channel.Channel r2 = com.discord.api.channel.Channel.this
                        if (r2 == 0) goto L55
                        com.discord.models.user.User r2 = com.discord.api.channel.ChannelUtils.a(r2)
                        if (r2 == 0) goto L55
                        r5 = r18
                        java.lang.Object r2 = b.d.b.a.a.e(r2, r5)
                        com.discord.models.presence.Presence r2 = (com.discord.models.presence.Presence) r2
                        r9 = r2
                        goto L56
                    L55:
                        r9 = r4
                    L56:
                        java.lang.String r2 = "unreadCount"
                        r5 = r20
                        d0.z.d.m.checkNotNullExpressionValue(r5, r2)
                        int r10 = r20.intValue()
                        java.lang.String r2 = "userRelationships"
                        d0.z.d.m.checkNotNullExpressionValue(r1, r2)
                        com.discord.api.channel.Channel r2 = com.discord.api.channel.Channel.this
                        if (r2 == 0) goto L79
                        com.discord.models.user.User r2 = com.discord.api.channel.ChannelUtils.a(r2)
                        if (r2 == 0) goto L79
                        long r7 = r2.getId()
                        java.lang.Long r2 = java.lang.Long.valueOf(r7)
                        goto L7a
                    L79:
                        r2 = r4
                    L7a:
                        java.lang.Object r1 = r1.get(r2)
                        java.lang.Integer r1 = (java.lang.Integer) r1
                        boolean r11 = com.discord.models.domain.ModelUserRelationship.isType(r1, r3)
                        if (r17 == 0) goto L8a
                        com.discord.api.user.NsfwAllowance r4 = r17.getNsfwAllowance()
                    L8a:
                        r13 = r4
                        int r14 = r22.size()
                        java.lang.String r1 = "threadExperimentEnabled"
                        r2 = r24
                        d0.z.d.m.checkNotNullExpressionValue(r2, r1)
                        boolean r15 = r24.booleanValue()
                        com.discord.widgets.home.WidgetHomeModel r1 = new com.discord.widgets.home.WidgetHomeModel
                        r5 = r1
                        r7 = r19
                        r8 = r23
                        r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
                        return r1
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.home.WidgetHomeModel$Companion$get$2.AnonymousClass1.call2(com.discord.models.user.MeUser, java.util.Map, com.discord.api.channel.Channel, java.lang.Integer, java.util.Map, java.util.Map, com.discord.api.channel.Channel, java.lang.Boolean):com.discord.widgets.home.WidgetHomeModel");
                }
            }, 350L, TimeUnit.MILLISECONDS);
        }
        l = Long.valueOf(j);
        return ObservableWithLeadingEdgeThrottle.combineLatest(observeMe$default, observeAllPresences, observeSelectedChannel, observeTotalMentions, observe, threadsActive.observeActiveThreadsForChannel(longValue, l), WidgetHomeModel.Companion.getParentChannelObservable(maybeChannel), new CreateThreadsFeatureFlag(null, null, 3, null).observeEnabled(component1.longValue()), new Func8<MeUser, Map<Long, ? extends Presence>, Channel, Integer, Map<Long, ? extends Integer>, Map<Long, ? extends Channel>, Channel, Boolean, WidgetHomeModel>() { // from class: com.discord.widgets.home.WidgetHomeModel$Companion$get$2.1
            @Override // rx.functions.Func8
            public /* bridge */ /* synthetic */ WidgetHomeModel call(MeUser meUser, Map<Long, ? extends Presence> map, Channel channel, Integer num, Map<Long, ? extends Integer> map2, Map<Long, ? extends Channel> map3, Channel channel2, Boolean bool) {
                return call2(meUser, (Map<Long, Presence>) map, channel, num, (Map<Long, Integer>) map2, (Map<Long, Channel>) map3, channel2, bool);
            }

            /* renamed from: call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final com.discord.widgets.home.WidgetHomeModel call2(com.discord.models.user.MeUser r17, java.util.Map<java.lang.Long, com.discord.models.presence.Presence> r18, com.discord.api.channel.Channel r19, java.lang.Integer r20, java.util.Map<java.lang.Long, java.lang.Integer> r21, java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r22, com.discord.api.channel.Channel r23, java.lang.Boolean r24) {
                /*
                    r16 = this;
                    r0 = r16
                    r1 = r21
                    com.discord.api.channel.Channel r2 = com.discord.api.channel.Channel.this
                    r3 = 1
                    if (r2 == 0) goto L11
                    boolean r2 = com.discord.api.channel.ChannelUtils.x(r2)
                    if (r2 != r3) goto L11
                    r2 = 1
                    goto L12
                L11:
                    r2 = 0
                L12:
                    r4 = 0
                    if (r2 == 0) goto L38
                    if (r19 == 0) goto L20
                    long r5 = r19.h()
                    java.lang.Long r2 = java.lang.Long.valueOf(r5)
                    goto L21
                L20:
                    r2 = r4
                L21:
                    com.discord.api.channel.Channel r5 = com.discord.api.channel.Channel.this
                    if (r5 == 0) goto L2e
                    long r5 = r5.h()
                    java.lang.Long r5 = java.lang.Long.valueOf(r5)
                    goto L2f
                L2e:
                    r5 = r4
                L2f:
                    boolean r2 = d0.z.d.m.areEqual(r2, r5)
                    if (r2 == 0) goto L38
                    r2 = 1
                    r12 = 1
                    goto L3a
                L38:
                    r2 = 0
                    r12 = 0
                L3a:
                    com.discord.stores.StoreChannelsSelected$ResolvedSelectedChannel r6 = r2
                    java.lang.String r2 = "selectedChannel"
                    d0.z.d.m.checkNotNullExpressionValue(r6, r2)
                    com.discord.api.channel.Channel r2 = com.discord.api.channel.Channel.this
                    if (r2 == 0) goto L55
                    com.discord.models.user.User r2 = com.discord.api.channel.ChannelUtils.a(r2)
                    if (r2 == 0) goto L55
                    r5 = r18
                    java.lang.Object r2 = b.d.b.a.a.e(r2, r5)
                    com.discord.models.presence.Presence r2 = (com.discord.models.presence.Presence) r2
                    r9 = r2
                    goto L56
                L55:
                    r9 = r4
                L56:
                    java.lang.String r2 = "unreadCount"
                    r5 = r20
                    d0.z.d.m.checkNotNullExpressionValue(r5, r2)
                    int r10 = r20.intValue()
                    java.lang.String r2 = "userRelationships"
                    d0.z.d.m.checkNotNullExpressionValue(r1, r2)
                    com.discord.api.channel.Channel r2 = com.discord.api.channel.Channel.this
                    if (r2 == 0) goto L79
                    com.discord.models.user.User r2 = com.discord.api.channel.ChannelUtils.a(r2)
                    if (r2 == 0) goto L79
                    long r7 = r2.getId()
                    java.lang.Long r2 = java.lang.Long.valueOf(r7)
                    goto L7a
                L79:
                    r2 = r4
                L7a:
                    java.lang.Object r1 = r1.get(r2)
                    java.lang.Integer r1 = (java.lang.Integer) r1
                    boolean r11 = com.discord.models.domain.ModelUserRelationship.isType(r1, r3)
                    if (r17 == 0) goto L8a
                    com.discord.api.user.NsfwAllowance r4 = r17.getNsfwAllowance()
                L8a:
                    r13 = r4
                    int r14 = r22.size()
                    java.lang.String r1 = "threadExperimentEnabled"
                    r2 = r24
                    d0.z.d.m.checkNotNullExpressionValue(r2, r1)
                    boolean r15 = r24.booleanValue()
                    com.discord.widgets.home.WidgetHomeModel r1 = new com.discord.widgets.home.WidgetHomeModel
                    r5 = r1
                    r7 = r19
                    r8 = r23
                    r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
                    return r1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.home.WidgetHomeModel$Companion$get$2.AnonymousClass1.call2(com.discord.models.user.MeUser, java.util.Map, com.discord.api.channel.Channel, java.lang.Integer, java.util.Map, java.util.Map, com.discord.api.channel.Channel, java.lang.Boolean):com.discord.widgets.home.WidgetHomeModel");
            }
        }, 350L, TimeUnit.MILLISECONDS);
    }
}
