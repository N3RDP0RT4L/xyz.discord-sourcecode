package com.discord.widgets.home;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewKt;
import androidx.fragment.app.FragmentManager;
import b.a.i.e5;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.user.NsfwAllowance;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetHomeBinding;
import com.discord.models.presence.Presence;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreStream;
import com.discord.utilities.channel.ChannelInviteLaunchUtils;
import com.discord.utilities.channel.GuildChannelIconUtilsKt;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.toolbar.ToolbarUtilsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.StatusView;
import com.discord.views.ToolbarTitleLayout;
import com.discord.widgets.channels.threads.browser.WidgetThreadBrowser;
import com.discord.widgets.friends.WidgetFriendsAdd;
import com.discord.widgets.search.WidgetSearch;
import com.discord.widgets.user.calls.PrivateCallLauncher;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.functions.Action1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetHomeHeaderManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000{\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0006*\u0001\u0011\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001/B\t\b\u0002¢\u0006\u0004\b-\u0010.J5\u0010\u000b\u001a\u00020\n2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\t\u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ#\u0010\u0012\u001a\u00020\u0011*\u00020\u00042\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J)\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00142\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ!\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d*\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001f\u0010 J\u001b\u0010\"\u001a\u00020!*\u00020\u00042\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\"\u0010#J%\u0010(\u001a\u00020\n2\u0006\u0010%\u001a\u00020$2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010'\u001a\u00020&¢\u0006\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b+\u0010,¨\u00060"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeHeaderManager;", "", "Landroidx/appcompat/widget/Toolbar;", "toolbar", "Lcom/discord/widgets/home/WidgetHomeModel;", "model", "Lcom/discord/api/channel/Channel;", "channel", "Landroid/content/Context;", "context", "", "configureThreadBrowserIconBehavior", "(Landroidx/appcompat/widget/Toolbar;Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/api/channel/Channel;Landroid/content/Context;)V", "Lcom/discord/app/AppFragment;", "appFragment", "Lcom/discord/widgets/home/PanelLayout;", "panelLayout", "com/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1", "getOnSelectedAction", "(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/app/AppFragment;Lcom/discord/widgets/home/PanelLayout;)Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;", "", "isChannelNsfw", "isNsfwUnConsented", "Lcom/discord/api/user/NsfwAllowance;", "nsfwAllowed", "isChannelNSFWGated", "(ZZLcom/discord/api/user/NsfwAllowance;)Z", "Landroid/content/res/Resources;", "resources", "Lrx/functions/Action1;", "Landroid/view/Menu;", "getOnConfigureAction", "(Lcom/discord/widgets/home/WidgetHomeModel;Landroid/content/res/Resources;)Lrx/functions/Action1;", "Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;", "getHeaderData", "(Lcom/discord/widgets/home/WidgetHomeModel;Landroid/content/Context;)Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;", "Lcom/discord/widgets/home/WidgetHome;", "widgetHome", "Lcom/discord/databinding/WidgetHomeBinding;", "binding", "configure", "(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/databinding/WidgetHomeBinding;)V", "", "ANALYTICS_SOURCE", "Ljava/lang/String;", HookHelper.constructorName, "()V", "HeaderData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHomeHeaderManager {
    private static final String ANALYTICS_SOURCE = "Toolbar";
    public static final WidgetHomeHeaderManager INSTANCE = new WidgetHomeHeaderManager();

    /* compiled from: WidgetHomeHeaderManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0003\u0010\u000f\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0010\u001a\u00020\t\u0012\n\b\u0003\u0010\u0011\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b$\u0010%J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\f\u0010\bJJ\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00022\n\b\u0003\u0010\u000f\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0010\u001a\u00020\t2\n\b\u0003\u0010\u0011\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001a\u001a\u00020\t2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001c\u001a\u0004\b\u001d\u0010\bR\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010 \u001a\u0004\b!\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\"\u0010\bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010 \u001a\u0004\b#\u0010\u0004¨\u0006&"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;", "", "", "component1", "()Ljava/lang/CharSequence;", "component2", "", "component3", "()Ljava/lang/Integer;", "", "component4", "()Z", "component5", "title", "subtitle", "drawableRes", "leftButtonIsBack", "trailingDrawable", "copy", "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Integer;ZLjava/lang/Integer;)Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getTrailingDrawable", "Z", "getLeftButtonIsBack", "Ljava/lang/CharSequence;", "getTitle", "getDrawableRes", "getSubtitle", HookHelper.constructorName, "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Integer;ZLjava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class HeaderData {
        private final Integer drawableRes;
        private final boolean leftButtonIsBack;
        private final CharSequence subtitle;
        private final CharSequence title;
        private final Integer trailingDrawable;

        public HeaderData(CharSequence charSequence, CharSequence charSequence2, @DrawableRes Integer num, boolean z2, @DrawableRes Integer num2) {
            this.title = charSequence;
            this.subtitle = charSequence2;
            this.drawableRes = num;
            this.leftButtonIsBack = z2;
            this.trailingDrawable = num2;
        }

        public static /* synthetic */ HeaderData copy$default(HeaderData headerData, CharSequence charSequence, CharSequence charSequence2, Integer num, boolean z2, Integer num2, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = headerData.title;
            }
            if ((i & 2) != 0) {
                charSequence2 = headerData.subtitle;
            }
            CharSequence charSequence3 = charSequence2;
            if ((i & 4) != 0) {
                num = headerData.drawableRes;
            }
            Integer num3 = num;
            if ((i & 8) != 0) {
                z2 = headerData.leftButtonIsBack;
            }
            boolean z3 = z2;
            if ((i & 16) != 0) {
                num2 = headerData.trailingDrawable;
            }
            return headerData.copy(charSequence, charSequence3, num3, z3, num2);
        }

        public final CharSequence component1() {
            return this.title;
        }

        public final CharSequence component2() {
            return this.subtitle;
        }

        public final Integer component3() {
            return this.drawableRes;
        }

        public final boolean component4() {
            return this.leftButtonIsBack;
        }

        public final Integer component5() {
            return this.trailingDrawable;
        }

        public final HeaderData copy(CharSequence charSequence, CharSequence charSequence2, @DrawableRes Integer num, boolean z2, @DrawableRes Integer num2) {
            return new HeaderData(charSequence, charSequence2, num, z2, num2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof HeaderData)) {
                return false;
            }
            HeaderData headerData = (HeaderData) obj;
            return m.areEqual(this.title, headerData.title) && m.areEqual(this.subtitle, headerData.subtitle) && m.areEqual(this.drawableRes, headerData.drawableRes) && this.leftButtonIsBack == headerData.leftButtonIsBack && m.areEqual(this.trailingDrawable, headerData.trailingDrawable);
        }

        public final Integer getDrawableRes() {
            return this.drawableRes;
        }

        public final boolean getLeftButtonIsBack() {
            return this.leftButtonIsBack;
        }

        public final CharSequence getSubtitle() {
            return this.subtitle;
        }

        public final CharSequence getTitle() {
            return this.title;
        }

        public final Integer getTrailingDrawable() {
            return this.trailingDrawable;
        }

        public int hashCode() {
            CharSequence charSequence = this.title;
            int i = 0;
            int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
            CharSequence charSequence2 = this.subtitle;
            int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
            Integer num = this.drawableRes;
            int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
            boolean z2 = this.leftButtonIsBack;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode3 + i2) * 31;
            Integer num2 = this.trailingDrawable;
            if (num2 != null) {
                i = num2.hashCode();
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("HeaderData(title=");
            R.append(this.title);
            R.append(", subtitle=");
            R.append(this.subtitle);
            R.append(", drawableRes=");
            R.append(this.drawableRes);
            R.append(", leftButtonIsBack=");
            R.append(this.leftButtonIsBack);
            R.append(", trailingDrawable=");
            return a.E(R, this.trailingDrawable, ")");
        }

        public /* synthetic */ HeaderData(CharSequence charSequence, CharSequence charSequence2, Integer num, boolean z2, Integer num2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(charSequence, (i & 2) != 0 ? null : charSequence2, (i & 4) != 0 ? null : num, (i & 8) != 0 ? false : z2, (i & 16) != 0 ? null : num2);
        }
    }

    private WidgetHomeHeaderManager() {
    }

    private final void configureThreadBrowserIconBehavior(Toolbar toolbar, WidgetHomeModel widgetHomeModel, final Channel channel, final Context context) {
        View actionView;
        View actionView2;
        Menu menu;
        if (channel != null) {
            int threadCount = widgetHomeModel.getThreadCount();
            View view = null;
            MenuItem findItem = (toolbar == null || (menu = toolbar.getMenu()) == null) ? null : menu.findItem(R.id.menu_chat_thread_browser);
            TextView textView = (findItem == null || (actionView2 = findItem.getActionView()) == null) ? null : (TextView) actionView2.findViewById(R.id.thread_browser_count);
            if (textView != null) {
                textView.setText(String.valueOf(Math.min(99, threadCount)));
            }
            if (textView != null) {
                ViewKt.setVisible(textView, threadCount != 0);
            }
            if (!(findItem == null || (actionView = findItem.getActionView()) == null)) {
                view = actionView.findViewById(R.id.thread_browser_root);
            }
            if (view != null) {
                view.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.home.WidgetHomeHeaderManager$configureThreadBrowserIconBehavior$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        WidgetThreadBrowser.Companion.show(context, channel.f(), ChannelUtils.C(channel) ? channel.r() : channel.h(), "Chat List Header");
                    }
                });
            }
        }
    }

    private final HeaderData getHeaderData(WidgetHomeModel widgetHomeModel, Context context) {
        StoreChannelsSelected.ResolvedSelectedChannel selectedChannel = widgetHomeModel.getSelectedChannel();
        if (selectedChannel instanceof StoreChannelsSelected.ResolvedSelectedChannel.Channel) {
            Channel channel = widgetHomeModel.getChannel();
            String str = null;
            Integer valueOf = channel != null ? Integer.valueOf(channel.A()) : null;
            if (valueOf == null) {
                return new HeaderData(context.getString(R.string.channels_unavailable_title), null, null, false, null, 26, null);
            }
            if (valueOf.intValue() == 3) {
                return new HeaderData(ChannelUtils.d(widgetHomeModel.getChannel(), context, false), null, Integer.valueOf((int) R.drawable.ic_group_message_header), false, null, 26, null);
            }
            if (valueOf.intValue() == 1) {
                return new HeaderData(ChannelUtils.d(widgetHomeModel.getChannel(), context, false), null, Integer.valueOf((int) R.drawable.ic_direct_message_header), false, null, 26, null);
            }
            if (valueOf.intValue() == 14) {
                return new HeaderData(ChannelUtils.d(widgetHomeModel.getChannel(), context, false), null, Integer.valueOf((int) R.drawable.ic_hub_24dp), false, null, 26, null);
            }
            if (widgetHomeModel.getChannel().h() <= 0) {
                return new HeaderData(null, null, null, false, null, 26, null);
            }
            String d = ChannelUtils.d(widgetHomeModel.getChannel(), context, false);
            Channel parentChannel = widgetHomeModel.getParentChannel();
            if (parentChannel != null) {
                str = ChannelUtils.d(parentChannel, context, false);
            }
            return new HeaderData(d, str, Integer.valueOf(GuildChannelIconUtilsKt.guildChannelIcon(widgetHomeModel.getChannel())), ((StoreChannelsSelected.ResolvedSelectedChannel.Channel) widgetHomeModel.getSelectedChannel()).getPeekParent() != null || ChannelUtils.E(widgetHomeModel.getChannel()), null, 16, null);
        } else if (selectedChannel instanceof StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft) {
            return new HeaderData(context.getString(R.string.create_thread), ChannelUtils.d(((StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft) widgetHomeModel.getSelectedChannel()).getParentChannel(), context, false), null, true, null, 16, null);
        } else {
            return new HeaderData(null, null, null, false, null, 30, null);
        }
    }

    private final Action1<Menu> getOnConfigureAction(final WidgetHomeModel widgetHomeModel, final Resources resources) {
        return new Action1<Menu>() { // from class: com.discord.widgets.home.WidgetHomeHeaderManager$getOnConfigureAction$1
            /* JADX WARN: Removed duplicated region for block: B:18:0x0056  */
            /* JADX WARN: Removed duplicated region for block: B:19:0x005f  */
            /* JADX WARN: Removed duplicated region for block: B:63:0x0181  */
            /* JADX WARN: Removed duplicated region for block: B:69:0x01a4 A[ADDED_TO_REGION] */
            /* JADX WARN: Removed duplicated region for block: B:75:0x01c2 A[ADDED_TO_REGION] */
            /* JADX WARN: Removed duplicated region for block: B:81:0x01e0 A[ADDED_TO_REGION] */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final void call(android.view.Menu r15) {
                /*
                    Method dump skipped, instructions count: 487
                    To view this dump add '--comments-level debug' option
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.home.WidgetHomeHeaderManager$getOnConfigureAction$1.call(android.view.Menu):void");
            }
        };
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.discord.widgets.home.WidgetHomeHeaderManager$getOnSelectedAction$1] */
    private final WidgetHomeHeaderManager$getOnSelectedAction$1 getOnSelectedAction(final WidgetHomeModel widgetHomeModel, final AppFragment appFragment, final PanelLayout panelLayout) {
        return new Action2<MenuItem, Context>() { // from class: com.discord.widgets.home.WidgetHomeHeaderManager$getOnSelectedAction$1
            private final void launchForSearch(Context context) {
                Channel channel = WidgetHomeModel.this.getChannel();
                if (channel == null || !ChannelUtils.x(channel)) {
                    Channel channel2 = WidgetHomeModel.this.getChannel();
                    if (channel2 != null && ChannelUtils.s(channel2)) {
                        WidgetSearch.Companion.launchForGuild(WidgetHomeModel.this.getChannel().f(), context);
                        return;
                    }
                    return;
                }
                WidgetSearch.Companion.launchForChannel(WidgetHomeModel.this.getChannelId(), context);
            }

            public void call(MenuItem menuItem, Context context) {
                m.checkNotNullParameter(menuItem, "menuItem");
                m.checkNotNullParameter(context, "context");
                AppFragment appFragment2 = appFragment;
                FragmentManager parentFragmentManager = appFragment2.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "appFragment.parentFragmentManager");
                PrivateCallLauncher privateCallLauncher = new PrivateCallLauncher(appFragment2, appFragment2, context, parentFragmentManager);
                switch (menuItem.getItemId()) {
                    case R.id.menu_chat_add_friend /* 2131364304 */:
                        WidgetFriendsAdd.Companion.show$default(WidgetFriendsAdd.Companion, context, null, "Toolbar", 2, null);
                        break;
                    case R.id.menu_chat_search /* 2131364305 */:
                        launchForSearch(context);
                        break;
                    case R.id.menu_chat_side_panel /* 2131364306 */:
                        panelLayout.openEndPanel();
                        break;
                    case R.id.menu_chat_start_call /* 2131364307 */:
                        privateCallLauncher.launchVoiceCall(WidgetHomeModel.this.getChannelId());
                        break;
                    case R.id.menu_chat_start_group /* 2131364308 */:
                        ChannelInviteLaunchUtils.INSTANCE.inviteToChannel(appFragment, WidgetHomeModel.this.getChannel(), "Toolbar", (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? null : null);
                        break;
                    case R.id.menu_chat_start_video_call /* 2131364309 */:
                        privateCallLauncher.launchVideoCall(WidgetHomeModel.this.getChannelId());
                        break;
                    case R.id.menu_chat_stop_call /* 2131364310 */:
                        StoreStream.Companion.getVoiceChannelSelected().clear();
                        break;
                }
                AppFragment.hideKeyboard$default(appFragment, null, 1, null);
            }
        };
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean isChannelNSFWGated(boolean z2, boolean z3, NsfwAllowance nsfwAllowance) {
        return z2 && (z3 || (nsfwAllowance == NsfwAllowance.DISALLOWED));
    }

    public final void configure(final WidgetHome widgetHome, final WidgetHomeModel widgetHomeModel, final WidgetHomeBinding widgetHomeBinding) {
        View view;
        int i;
        int i2;
        m.checkNotNullParameter(widgetHome, "widgetHome");
        m.checkNotNullParameter(widgetHomeModel, "model");
        m.checkNotNullParameter(widgetHomeBinding, "binding");
        boolean z2 = false;
        widgetHome.lockCloseRightPanel(widgetHomeModel.getChannel() == null || (!ChannelUtils.x(widgetHomeModel.getChannel()) && !ChannelUtils.s(widgetHomeModel.getChannel())) || ChannelUtils.j(widgetHomeModel.getChannel()));
        Channel channel = widgetHomeModel.getChannel();
        if (channel == null || channel.A() != 14) {
            e5 e5Var = widgetHomeBinding.d.f100b;
            m.checkNotNullExpressionValue(e5Var, "binding.panelCenter.widgetHomePanelCenterChat");
            view = e5Var.a;
            m.checkNotNullExpressionValue(view, "binding.panelCenter.widgetHomePanelCenterChat.root");
        } else {
            view = widgetHomeBinding.d.c;
            m.checkNotNullExpressionValue(view, "binding.panelCenter.widgetHomePanelDirectory");
        }
        widgetHome.bindToolbar(view);
        widgetHome.setActionBarTitleLayoutExpandedTappableArea();
        Context context = widgetHome.getContext();
        String str = null;
        if (context != null) {
            WidgetHomeHeaderManager widgetHomeHeaderManager = INSTANCE;
            m.checkNotNullExpressionValue(context, "context");
            HeaderData headerData = widgetHomeHeaderManager.getHeaderData(widgetHomeModel, context);
            CharSequence component1 = headerData.component1();
            CharSequence component2 = headerData.component2();
            Integer component3 = headerData.component3();
            boolean component4 = headerData.component4();
            Integer component5 = headerData.component5();
            if (component4) {
                i = DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.ic_action_bar_back, 0, 2, (Object) null);
            } else if (!component4) {
                i = R.drawable.ic_menu_24dp;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            Integer valueOf = Integer.valueOf(i);
            if (component4) {
                i2 = R.string.back;
            } else if (!component4) {
                i2 = R.string.toggle_drawer;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            widgetHome.setActionBarDisplayHomeAsUpEnabled(true, valueOf, Integer.valueOf(i2));
            widgetHome.setActionBarTitle(component1, component3, component5);
            widgetHome.setActionBarSubtitle(component2);
            Toolbar toolbar = widgetHome.getToolbar();
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.home.WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        widgetHome.handleCenterPanelBack$app_productionGoogleRelease();
                    }
                });
            }
        }
        widgetHome.setActionBarTitleClick(new View.OnClickListener() { // from class: com.discord.widgets.home.WidgetHomeHeaderManager$configure$$inlined$apply$lambda$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Channel channel2 = WidgetHomeModel.this.getChannel();
                Integer valueOf2 = channel2 != null ? Integer.valueOf(channel2.A()) : null;
                if ((valueOf2 != null && valueOf2.intValue() == 1) || ((valueOf2 != null && valueOf2.intValue() == 3) || ((valueOf2 != null && valueOf2.intValue() == 5) || ((valueOf2 != null && valueOf2.intValue() == 15) || (valueOf2 != null && valueOf2.intValue() == 0))))) {
                    widgetHome.getPanelLayout().openEndPanel();
                }
            }
        });
        WidgetHomeHeaderManager widgetHomeHeaderManager2 = INSTANCE;
        WidgetHomeHeaderManager$getOnSelectedAction$1 onSelectedAction = widgetHomeHeaderManager2.getOnSelectedAction(widgetHomeModel, widgetHome, widgetHome.getPanelLayout());
        Resources resources = widgetHome.getResources();
        m.checkNotNullExpressionValue(resources, "widgetHome.resources");
        Toolbar actionBarOptionsMenu = widgetHome.setActionBarOptionsMenu(R.menu.menu_chat_toolbar, onSelectedAction, widgetHomeHeaderManager2.getOnConfigureAction(widgetHomeModel, resources));
        Channel channel2 = widgetHomeModel.getChannel();
        boolean n = channel2 != null ? ChannelUtils.n(channel2, widgetHomeModel.getDmPresence()) : false;
        ToolbarTitleLayout actionBarTitleLayout = widgetHome.getActionBarTitleLayout();
        if (actionBarTitleLayout != null) {
            Presence dmPresence = widgetHomeModel.getDmPresence();
            StatusView statusView = actionBarTitleLayout.k.c;
            m.checkNotNullExpressionValue(statusView, "binding.toolbarPresence");
            statusView.setVisibility(n ? 0 : 8);
            actionBarTitleLayout.k.c.setPresence(dmPresence);
        }
        TextView unreadCountView = widgetHome.getUnreadCountView();
        Integer valueOf2 = Integer.valueOf(widgetHomeModel.getUnreadCount());
        if (valueOf2.intValue() > 0) {
            z2 = true;
        }
        if (!z2) {
            valueOf2 = null;
        }
        if (valueOf2 != null) {
            str = String.valueOf(valueOf2.intValue());
        }
        ViewExtensions.setTextAndVisibilityBy(unreadCountView, str);
        Toolbar toolbar2 = widgetHome.getToolbar();
        if (toolbar2 != null) {
            ToolbarUtilsKt.positionUnreadCountView(toolbar2, unreadCountView);
        }
        widgetHomeHeaderManager2.configureThreadBrowserIconBehavior(actionBarOptionsMenu, widgetHomeModel, widgetHomeModel.getChannel(), widgetHome.getContext());
    }
}
