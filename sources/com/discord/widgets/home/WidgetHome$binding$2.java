package com.discord.widgets.home;

import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentContainerView;
import b.a.i.d5;
import b.a.i.e5;
import b.a.i.f5;
import b.a.i.g5;
import b.a.i.h5;
import b.a.i.i5;
import com.discord.databinding.WidgetHomeBinding;
import com.discord.utilities.view.rounded.RoundedRelativeLayout;
import com.google.android.material.appbar.AppBarLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetHomeBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetHomeBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetHome$binding$2 extends k implements Function1<View, WidgetHomeBinding> {
    public static final WidgetHome$binding$2 INSTANCE = new WidgetHome$binding$2();

    public WidgetHome$binding$2() {
        super(1, WidgetHomeBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetHomeBinding;", 0);
    }

    public final WidgetHomeBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.loading;
        View findViewById = view.findViewById(R.id.loading);
        if (findViewById != null) {
            ImageView imageView = (ImageView) findViewById.findViewById(R.id.logo);
            if (imageView != null) {
                h5 h5Var = new h5((FrameLayout) findViewById, imageView);
                i = R.id.overlapping_panels;
                HomePanelsLayout homePanelsLayout = (HomePanelsLayout) view.findViewById(R.id.overlapping_panels);
                if (homePanelsLayout != null) {
                    i = R.id.panel_center;
                    View findViewById2 = view.findViewById(R.id.panel_center);
                    if (findViewById2 != null) {
                        int i2 = R.id.widget_home_panel_center_chat;
                        View findViewById3 = findViewById2.findViewById(R.id.widget_home_panel_center_chat);
                        if (findViewById3 != null) {
                            int i3 = R.id.action_bar_toolbar_layout;
                            AppBarLayout appBarLayout = (AppBarLayout) findViewById3.findViewById(R.id.action_bar_toolbar_layout);
                            if (appBarLayout != null) {
                                i3 = R.id.home_panel_center_channel_less;
                                ViewStub viewStub = (ViewStub) findViewById3.findViewById(R.id.home_panel_center_channel_less);
                                if (viewStub != null) {
                                    i3 = R.id.home_panel_center_nsfw;
                                    ViewStub viewStub2 = (ViewStub) findViewById3.findViewById(R.id.home_panel_center_nsfw);
                                    if (viewStub2 != null) {
                                        i3 = R.id.unread;
                                        View findViewById4 = findViewById3.findViewById(R.id.unread);
                                        if (findViewById4 != null) {
                                            TextView textView = (TextView) findViewById4;
                                            f5 f5Var = new f5(textView, textView);
                                            View findViewById5 = findViewById3.findViewById(R.id.widget_chat_bottom_space);
                                            if (findViewById5 != null) {
                                                FragmentContainerView fragmentContainerView = (FragmentContainerView) findViewById3.findViewById(R.id.widget_chat_input);
                                                if (fragmentContainerView != null) {
                                                    FragmentContainerView fragmentContainerView2 = (FragmentContainerView) findViewById3.findViewById(R.id.widget_chat_list);
                                                    if (fragmentContainerView2 != null) {
                                                        FragmentContainerView fragmentContainerView3 = (FragmentContainerView) findViewById3.findViewById(R.id.widget_chat_overlay_actions);
                                                        if (fragmentContainerView3 != null) {
                                                            FragmentContainerView fragmentContainerView4 = (FragmentContainerView) findViewById3.findViewById(R.id.widget_chat_voice_inline);
                                                            if (fragmentContainerView4 != null) {
                                                                ConstraintLayout constraintLayout = (ConstraintLayout) findViewById3;
                                                                FragmentContainerView fragmentContainerView5 = (FragmentContainerView) findViewById3.findViewById(R.id.widget_status);
                                                                if (fragmentContainerView5 != null) {
                                                                    FragmentContainerView fragmentContainerView6 = (FragmentContainerView) findViewById3.findViewById(R.id.widget_thread_status);
                                                                    if (fragmentContainerView6 != null) {
                                                                        e5 e5Var = new e5(constraintLayout, appBarLayout, viewStub, viewStub2, f5Var, findViewById5, fragmentContainerView, fragmentContainerView2, fragmentContainerView3, fragmentContainerView4, constraintLayout, fragmentContainerView5, fragmentContainerView6);
                                                                        FragmentContainerView fragmentContainerView7 = (FragmentContainerView) findViewById2.findViewById(R.id.widget_home_panel_directory);
                                                                        if (fragmentContainerView7 != null) {
                                                                            d5 d5Var = new d5((RoundedRelativeLayout) findViewById2, e5Var, fragmentContainerView7);
                                                                            i = R.id.panel_left;
                                                                            View findViewById6 = view.findViewById(R.id.panel_left);
                                                                            if (findViewById6 != null) {
                                                                                int i4 = R.id.guild_list_add_hint;
                                                                                TextView textView2 = (TextView) findViewById6.findViewById(R.id.guild_list_add_hint);
                                                                                if (textView2 != null) {
                                                                                    i4 = R.id.widget_channels;
                                                                                    FragmentContainerView fragmentContainerView8 = (FragmentContainerView) findViewById6.findViewById(R.id.widget_channels);
                                                                                    if (fragmentContainerView8 != null) {
                                                                                        i4 = R.id.widget_guilds;
                                                                                        FragmentContainerView fragmentContainerView9 = (FragmentContainerView) findViewById6.findViewById(R.id.widget_guilds);
                                                                                        if (fragmentContainerView9 != null) {
                                                                                            i4 = R.id.widget_profile_strip;
                                                                                            FrameLayout frameLayout = (FrameLayout) findViewById6.findViewById(R.id.widget_profile_strip);
                                                                                            if (frameLayout != null) {
                                                                                                g5 g5Var = new g5((RelativeLayout) findViewById6, textView2, fragmentContainerView8, fragmentContainerView9, frameLayout);
                                                                                                View findViewById7 = view.findViewById(R.id.panel_right);
                                                                                                if (findViewById7 != null) {
                                                                                                    int i5 = R.id.main_panel_right_rounded_container;
                                                                                                    RoundedRelativeLayout roundedRelativeLayout = (RoundedRelativeLayout) findViewById7.findViewById(R.id.main_panel_right_rounded_container);
                                                                                                    if (roundedRelativeLayout != null) {
                                                                                                        i5 = R.id.widget_channel_action_bar;
                                                                                                        FragmentContainerView fragmentContainerView10 = (FragmentContainerView) findViewById7.findViewById(R.id.widget_channel_action_bar);
                                                                                                        if (fragmentContainerView10 != null) {
                                                                                                            i5 = R.id.widget_channel_topic;
                                                                                                            FragmentContainerView fragmentContainerView11 = (FragmentContainerView) findViewById7.findViewById(R.id.widget_channel_topic);
                                                                                                            if (fragmentContainerView11 != null) {
                                                                                                                i5 = R.id.widget_connected_list;
                                                                                                                FragmentContainerView fragmentContainerView12 = (FragmentContainerView) findViewById7.findViewById(R.id.widget_connected_list);
                                                                                                                if (fragmentContainerView12 != null) {
                                                                                                                    i5 i5Var = new i5((FrameLayout) findViewById7, roundedRelativeLayout, fragmentContainerView10, fragmentContainerView11, fragmentContainerView12);
                                                                                                                    ImageView imageView2 = (ImageView) view.findViewById(R.id.peek_transition_bitmap);
                                                                                                                    if (imageView2 != null) {
                                                                                                                        FrameLayout frameLayout2 = (FrameLayout) view;
                                                                                                                        return new WidgetHomeBinding(frameLayout2, h5Var, homePanelsLayout, d5Var, g5Var, i5Var, imageView2, frameLayout2);
                                                                                                                    }
                                                                                                                    i = R.id.peek_transition_bitmap;
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    throw new NullPointerException("Missing required view with ID: ".concat(findViewById7.getResources().getResourceName(i5)));
                                                                                                }
                                                                                                i = R.id.panel_right;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                throw new NullPointerException("Missing required view with ID: ".concat(findViewById6.getResources().getResourceName(i4)));
                                                                            }
                                                                        } else {
                                                                            i2 = R.id.widget_home_panel_directory;
                                                                        }
                                                                    } else {
                                                                        i3 = R.id.widget_thread_status;
                                                                    }
                                                                } else {
                                                                    i3 = R.id.widget_status;
                                                                }
                                                            } else {
                                                                i3 = R.id.widget_chat_voice_inline;
                                                            }
                                                        } else {
                                                            i3 = R.id.widget_chat_overlay_actions;
                                                        }
                                                    } else {
                                                        i3 = R.id.widget_chat_list;
                                                    }
                                                } else {
                                                    i3 = R.id.widget_chat_input;
                                                }
                                            } else {
                                                i3 = R.id.widget_chat_bottom_space;
                                            }
                                        }
                                    }
                                }
                            }
                            throw new NullPointerException("Missing required view with ID: ".concat(findViewById3.getResources().getResourceName(i3)));
                        }
                        throw new NullPointerException("Missing required view with ID: ".concat(findViewById2.getResources().getResourceName(i2)));
                    }
                }
            } else {
                throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(R.id.logo)));
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
