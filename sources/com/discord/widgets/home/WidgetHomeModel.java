package com.discord.widgets.home;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.user.NsfwAllowance;
import com.discord.models.presence.Presence;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import d0.z.d.m;
import j0.l.e.k;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetHomeModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000e\n\u0002\b\r\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0013\b\u0086\b\u0018\u0000 E2\u00020\u0001:\u0001EB_\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\u001c\u001a\u00020\f\u0012\u0006\u0010\u001d\u001a\u00020\u000f\u0012\u0006\u0010\u001e\u001a\u00020\u000f\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010 \u001a\u00020\f\u0012\u0006\u0010!\u001a\u00020\u000f¢\u0006\u0004\bC\u0010DJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u000eJ\u0010\u0010\u0017\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0011J|\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u00022\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\u001c\u001a\u00020\f2\b\b\u0002\u0010\u001d\u001a\u00020\u000f2\b\b\u0002\u0010\u001e\u001a\u00020\u000f2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00132\b\b\u0002\u0010 \u001a\u00020\f2\b\b\u0002\u0010!\u001a\u00020\u000fHÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b'\u0010\u000eJ\u001a\u0010)\u001a\u00020\u000f2\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b)\u0010*R\u001b\u0010\u001b\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010+\u001a\u0004\b,\u0010\u000bR\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010-\u001a\u0004\b.\u0010\u0004R\u001b\u0010/\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u0010\u0007R\u001d\u00104\u001a\u000602j\u0002`38\u0006@\u0006¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00100\u001a\u0004\b8\u0010\u0007R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00109\u001a\u0004\b:\u0010\u0015R\u0019\u0010\u001d\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010;\u001a\u0004\b\u001d\u0010\u0011R\u0019\u0010 \u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010<\u001a\u0004\b=\u0010\u000eR\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00100\u001a\u0004\b>\u0010\u0007R\u0019\u0010\u001e\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010;\u001a\u0004\b\u001e\u0010\u0011R\u0013\u0010?\u001a\u00020\u000f8F@\u0006¢\u0006\u0006\u001a\u0004\b?\u0010\u0011R\u0019\u0010\u001c\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010<\u001a\u0004\b@\u0010\u000eR\u0013\u0010A\u001a\u00020\u000f8F@\u0006¢\u0006\u0006\u001a\u0004\bA\u0010\u0011R\u0019\u0010!\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010;\u001a\u0004\bB\u0010\u0011¨\u0006F"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeModel;", "", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "component1", "()Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "component3", "Lcom/discord/models/presence/Presence;", "component4", "()Lcom/discord/models/presence/Presence;", "", "component5", "()I", "", "component6", "()Z", "component7", "Lcom/discord/api/user/NsfwAllowance;", "component8", "()Lcom/discord/api/user/NsfwAllowance;", "component9", "component10", "selectedChannel", "selectedVoiceChannel", "parentChannel", "dmPresence", "unreadCount", "isFriend", "isCallConnected", "nsfwAllowed", "threadCount", "threadExperimentEnabled", "copy", "(Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/models/presence/Presence;IZZLcom/discord/api/user/NsfwAllowance;IZ)Lcom/discord/widgets/home/WidgetHomeModel;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/presence/Presence;", "getDmPresence", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "getSelectedChannel", "channel", "Lcom/discord/api/channel/Channel;", "getChannel", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "getChannelId", "()J", "getSelectedVoiceChannel", "Lcom/discord/api/user/NsfwAllowance;", "getNsfwAllowed", "Z", "I", "getThreadCount", "getParentChannel", "isChannelNsfw", "getUnreadCount", "isNsfwUnConsented", "getThreadExperimentEnabled", HookHelper.constructorName, "(Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/models/presence/Presence;IZZLcom/discord/api/user/NsfwAllowance;IZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHomeModel {
    public static final Companion Companion = new Companion(null);
    private final Channel channel;
    private final long channelId;
    private final Presence dmPresence;
    private final boolean isCallConnected;
    private final boolean isFriend;
    private final NsfwAllowance nsfwAllowed;
    private final Channel parentChannel;
    private final StoreChannelsSelected.ResolvedSelectedChannel selectedChannel;
    private final Channel selectedVoiceChannel;
    private final int threadCount;
    private final boolean threadExperimentEnabled;
    private final int unreadCount;

    /* compiled from: WidgetHomeModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0013\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004¢\u0006\u0004\b\b\u0010\tR\u0019\u0010\f\u001a\b\u0012\u0004\u0012\u00020\n0\u00048F@\u0006¢\u0006\u0006\u001a\u0004\b\u000b\u0010\t¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeModel$Companion;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "getParentChannelObservable", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "Lcom/discord/widgets/home/WidgetHomeModel;", "get", "()Lrx/Observable;", "", "getInitialized", "initialized", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<WidgetHomeModel> get() {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable Y = Observable.j(companion.getGuildSelected().observeSelectedGuildId(), companion.getChannelsSelected().observeResolvedSelectedChannel(), WidgetHomeModel$Companion$get$1.INSTANCE).Y(WidgetHomeModel$Companion$get$2.INSTANCE);
            m.checkNotNullExpressionValue(Y, "Observable.combineLatest…            )\n          }");
            Observable<WidgetHomeModel> q = ObservableExtensionsKt.computationLatest(Y).q();
            m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
            return q;
        }

        public final Observable<Boolean> getInitialized() {
            Observable<Boolean> p = StoreStream.Companion.isInitializedObservable().p(150L, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(p, "StoreStream\n          .i…0, TimeUnit.MILLISECONDS)");
            return p;
        }

        public final Observable<Channel> getParentChannelObservable(Channel channel) {
            if (channel == null || ChannelUtils.C(channel)) {
                if ((channel != null ? Long.valueOf(channel.r()) : null) != null) {
                    return StoreStream.Companion.getChannels().observeChannel(channel.r());
                }
            }
            k kVar = new k(null);
            m.checkNotNullExpressionValue(kVar, "Observable.just(null)");
            return kVar;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetHomeModel(StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel, Channel channel, Channel channel2, Presence presence, int i, boolean z2, boolean z3, NsfwAllowance nsfwAllowance, int i2, boolean z4) {
        m.checkNotNullParameter(resolvedSelectedChannel, "selectedChannel");
        this.selectedChannel = resolvedSelectedChannel;
        this.selectedVoiceChannel = channel;
        this.parentChannel = channel2;
        this.dmPresence = presence;
        this.unreadCount = i;
        this.isFriend = z2;
        this.isCallConnected = z3;
        this.nsfwAllowed = nsfwAllowance;
        this.threadCount = i2;
        this.threadExperimentEnabled = z4;
        this.channel = resolvedSelectedChannel.getMaybeChannel();
        Channel maybeChannel = resolvedSelectedChannel.getMaybeChannel();
        this.channelId = maybeChannel != null ? maybeChannel.h() : 0L;
    }

    public final StoreChannelsSelected.ResolvedSelectedChannel component1() {
        return this.selectedChannel;
    }

    public final boolean component10() {
        return this.threadExperimentEnabled;
    }

    public final Channel component2() {
        return this.selectedVoiceChannel;
    }

    public final Channel component3() {
        return this.parentChannel;
    }

    public final Presence component4() {
        return this.dmPresence;
    }

    public final int component5() {
        return this.unreadCount;
    }

    public final boolean component6() {
        return this.isFriend;
    }

    public final boolean component7() {
        return this.isCallConnected;
    }

    public final NsfwAllowance component8() {
        return this.nsfwAllowed;
    }

    public final int component9() {
        return this.threadCount;
    }

    public final WidgetHomeModel copy(StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel, Channel channel, Channel channel2, Presence presence, int i, boolean z2, boolean z3, NsfwAllowance nsfwAllowance, int i2, boolean z4) {
        m.checkNotNullParameter(resolvedSelectedChannel, "selectedChannel");
        return new WidgetHomeModel(resolvedSelectedChannel, channel, channel2, presence, i, z2, z3, nsfwAllowance, i2, z4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetHomeModel)) {
            return false;
        }
        WidgetHomeModel widgetHomeModel = (WidgetHomeModel) obj;
        return m.areEqual(this.selectedChannel, widgetHomeModel.selectedChannel) && m.areEqual(this.selectedVoiceChannel, widgetHomeModel.selectedVoiceChannel) && m.areEqual(this.parentChannel, widgetHomeModel.parentChannel) && m.areEqual(this.dmPresence, widgetHomeModel.dmPresence) && this.unreadCount == widgetHomeModel.unreadCount && this.isFriend == widgetHomeModel.isFriend && this.isCallConnected == widgetHomeModel.isCallConnected && m.areEqual(this.nsfwAllowed, widgetHomeModel.nsfwAllowed) && this.threadCount == widgetHomeModel.threadCount && this.threadExperimentEnabled == widgetHomeModel.threadExperimentEnabled;
    }

    public final Channel getChannel() {
        return this.channel;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final Presence getDmPresence() {
        return this.dmPresence;
    }

    public final NsfwAllowance getNsfwAllowed() {
        return this.nsfwAllowed;
    }

    public final Channel getParentChannel() {
        return this.parentChannel;
    }

    public final StoreChannelsSelected.ResolvedSelectedChannel getSelectedChannel() {
        return this.selectedChannel;
    }

    public final Channel getSelectedVoiceChannel() {
        return this.selectedVoiceChannel;
    }

    public final int getThreadCount() {
        return this.threadCount;
    }

    public final boolean getThreadExperimentEnabled() {
        return this.threadExperimentEnabled;
    }

    public final int getUnreadCount() {
        return this.unreadCount;
    }

    public int hashCode() {
        StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel = this.selectedChannel;
        int i = 0;
        int hashCode = (resolvedSelectedChannel != null ? resolvedSelectedChannel.hashCode() : 0) * 31;
        Channel channel = this.selectedVoiceChannel;
        int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
        Channel channel2 = this.parentChannel;
        int hashCode3 = (hashCode2 + (channel2 != null ? channel2.hashCode() : 0)) * 31;
        Presence presence = this.dmPresence;
        int hashCode4 = (((hashCode3 + (presence != null ? presence.hashCode() : 0)) * 31) + this.unreadCount) * 31;
        boolean z2 = this.isFriend;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode4 + i3) * 31;
        boolean z3 = this.isCallConnected;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        NsfwAllowance nsfwAllowance = this.nsfwAllowed;
        if (nsfwAllowance != null) {
            i = nsfwAllowance.hashCode();
        }
        int i9 = (((i8 + i) * 31) + this.threadCount) * 31;
        boolean z4 = this.threadExperimentEnabled;
        if (!z4) {
            i2 = z4 ? 1 : 0;
        }
        return i9 + i2;
    }

    public final boolean isCallConnected() {
        return this.isCallConnected;
    }

    public final boolean isChannelNsfw() {
        Channel channel = this.channel;
        return channel != null && channel.o();
    }

    public final boolean isFriend() {
        return this.isFriend;
    }

    public final boolean isNsfwUnConsented() {
        return this.channel != null && !StoreStream.Companion.getGuildsNsfw().isGuildNsfwGateAgreed(this.channel.f());
    }

    public String toString() {
        StringBuilder R = a.R("WidgetHomeModel(selectedChannel=");
        R.append(this.selectedChannel);
        R.append(", selectedVoiceChannel=");
        R.append(this.selectedVoiceChannel);
        R.append(", parentChannel=");
        R.append(this.parentChannel);
        R.append(", dmPresence=");
        R.append(this.dmPresence);
        R.append(", unreadCount=");
        R.append(this.unreadCount);
        R.append(", isFriend=");
        R.append(this.isFriend);
        R.append(", isCallConnected=");
        R.append(this.isCallConnected);
        R.append(", nsfwAllowed=");
        R.append(this.nsfwAllowed);
        R.append(", threadCount=");
        R.append(this.threadCount);
        R.append(", threadExperimentEnabled=");
        return a.M(R, this.threadExperimentEnabled, ")");
    }
}
