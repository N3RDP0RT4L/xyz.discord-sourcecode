package com.discord.widgets.home;

import com.discord.databinding.WidgetHomeBinding;
import com.discord.panels.OverlappingPanelsLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/panels/OverlappingPanelsLayout$LockState;", "lockState", "", "invoke", "(Lcom/discord/panels/OverlappingPanelsLayout$LockState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHome$configureLeftPanel$1 extends o implements Function1<OverlappingPanelsLayout.LockState, Unit> {
    public final /* synthetic */ WidgetHome this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHome$configureLeftPanel$1(WidgetHome widgetHome) {
        super(1);
        this.this$0 = widgetHome;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(OverlappingPanelsLayout.LockState lockState) {
        invoke2(lockState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(OverlappingPanelsLayout.LockState lockState) {
        WidgetHomeBinding binding;
        WidgetHomeBinding binding2;
        m.checkNotNullParameter(lockState, "lockState");
        binding = this.this$0.getBinding();
        binding.c.setStartPanelUseFullPortraitWidth(lockState == OverlappingPanelsLayout.LockState.OPEN);
        binding2 = this.this$0.getBinding();
        binding2.c.setStartPanelLockState(lockState);
    }
}
