package com.discord.widgets.home;

import android.content.Context;
import com.discord.utilities.locale.LocaleManager;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/content/Context;", "context", "Ljava/util/Locale;", "invoke", "(Landroid/content/Context;)Ljava/util/Locale;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHome$onCreate$1 extends o implements Function1<Context, Locale> {
    public final /* synthetic */ WidgetHome this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHome$onCreate$1(WidgetHome widgetHome) {
        super(1);
        this.this$0 = widgetHome;
    }

    public final Locale invoke(Context context) {
        LocaleManager localeManager;
        m.checkNotNullParameter(context, "context");
        localeManager = this.this$0.localeManager;
        return localeManager.getPrimaryLocale(context);
    }
}
