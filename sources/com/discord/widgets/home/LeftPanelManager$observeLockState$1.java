package com.discord.widgets.home;

import androidx.core.app.NotificationCompat;
import com.discord.panels.OverlappingPanelsLayout;
import com.discord.stores.StoreChannelsSelected;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: LeftPanelManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\n \u0001*\u0004\u0018\u00010\u00060\u00062\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u0018\u0010\u0005\u001a\u0014 \u0001*\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00040\u0003j\u0002`\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "kotlin.jvm.PlatformType", "resolvedSelectedChannel", "", "Lcom/discord/primitives/GuildId;", "selectedGuildId", "Lcom/discord/panels/OverlappingPanelsLayout$LockState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;Ljava/lang/Long;)Lcom/discord/panels/OverlappingPanelsLayout$LockState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class LeftPanelManager$observeLockState$1<T1, T2, R> implements Func2<StoreChannelsSelected.ResolvedSelectedChannel, Long, OverlappingPanelsLayout.LockState> {
    public static final LeftPanelManager$observeLockState$1 INSTANCE = new LeftPanelManager$observeLockState$1();

    /* JADX WARN: Code restructure failed: missing block: B:21:0x003e, code lost:
        if (com.discord.api.channel.ChannelUtils.E(r10.getChannel()) == false) goto L22;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.panels.OverlappingPanelsLayout.LockState call(com.discord.stores.StoreChannelsSelected.ResolvedSelectedChannel r10, java.lang.Long r11) {
        /*
            r9 = this;
            long r0 = r10.getId()
            r2 = 0
            r4 = 0
            r5 = 1
            int r6 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r6 == 0) goto L14
            r6 = -1
            int r8 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r8 == 0) goto L14
            r0 = 1
            goto L15
        L14:
            r0 = 0
        L15:
            if (r11 != 0) goto L18
            goto L20
        L18:
            long r6 = r11.longValue()
            int r11 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r11 == 0) goto L22
        L20:
            r11 = 1
            goto L23
        L22:
            r11 = 0
        L23:
            r0 = r0 ^ r5
            boolean r1 = r10 instanceof com.discord.stores.StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft
            if (r1 == 0) goto L2a
        L28:
            r4 = 1
            goto L41
        L2a:
            boolean r1 = r10 instanceof com.discord.stores.StoreChannelsSelected.ResolvedSelectedChannel.Channel
            if (r1 == 0) goto L41
            com.discord.stores.StoreChannelsSelected$ResolvedSelectedChannel$Channel r10 = (com.discord.stores.StoreChannelsSelected.ResolvedSelectedChannel.Channel) r10
            java.lang.Long r1 = r10.getPeekParent()
            if (r1 != 0) goto L28
            com.discord.api.channel.Channel r10 = r10.getChannel()
            boolean r10 = com.discord.api.channel.ChannelUtils.E(r10)
            if (r10 == 0) goto L41
            goto L28
        L41:
            if (r11 == 0) goto L48
            if (r4 == 0) goto L48
            com.discord.panels.OverlappingPanelsLayout$LockState r10 = com.discord.panels.OverlappingPanelsLayout.LockState.CLOSE
            goto L51
        L48:
            if (r11 != 0) goto L4f
            if (r0 == 0) goto L4f
            com.discord.panels.OverlappingPanelsLayout$LockState r10 = com.discord.panels.OverlappingPanelsLayout.LockState.OPEN
            goto L51
        L4f:
            com.discord.panels.OverlappingPanelsLayout$LockState r10 = com.discord.panels.OverlappingPanelsLayout.LockState.UNLOCKED
        L51:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.home.LeftPanelManager$observeLockState$1.call(com.discord.stores.StoreChannelsSelected$ResolvedSelectedChannel, java.lang.Long):com.discord.panels.OverlappingPanelsLayout$LockState");
    }
}
