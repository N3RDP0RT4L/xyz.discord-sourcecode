package com.discord.widgets.home;

import andhook.lib.HookHelper;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import b.a.i.d5;
import b.a.i.h5;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetHomeBinding;
import com.discord.stores.StoreStream;
import com.discord.utilities.display.DisplayUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.rounded.RoundedRelativeLayout;
import d0.a0.a;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHomePanelLoading.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u000f\u0012\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/home/WidgetHomePanelLoading;", "", "", "initialized", "animate", "", "setLoadingPanelVisibility", "(ZZ)V", "centerLogoRelativeToLoadingScreen", "()V", "Lcom/discord/app/AppFragment;", "fragment", "configure", "(Lcom/discord/app/AppFragment;)V", "Lcom/discord/databinding/WidgetHomeBinding;", "binding", "Lcom/discord/databinding/WidgetHomeBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetHomeBinding;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHomePanelLoading {
    public static final Companion Companion = new Companion(null);
    private static boolean panelInitialized;
    private final WidgetHomeBinding binding;

    /* compiled from: WidgetHomePanelLoading.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/home/WidgetHomePanelLoading$Companion;", "", "", "panelInitialized", "Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetHomePanelLoading(WidgetHomeBinding widgetHomeBinding) {
        m.checkNotNullParameter(widgetHomeBinding, "binding");
        this.binding = widgetHomeBinding;
        centerLogoRelativeToLoadingScreen();
    }

    private final void centerLogoRelativeToLoadingScreen() {
        ImageView imageView = this.binding.f2442b.f125b;
        m.checkNotNullExpressionValue(imageView, "binding.loading.logo");
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
        ImageView imageView2 = this.binding.f2442b.f125b;
        m.checkNotNullExpressionValue(imageView2, "binding.loading.logo");
        Resources resources = imageView2.getResources();
        m.checkNotNullExpressionValue(resources, "binding.loading.logo.resources");
        layoutParams2.setMargins(((ViewGroup.MarginLayoutParams) layoutParams2).leftMargin, a.roundToInt(DisplayUtils.getStatusBarHeight(resources) / (-2.0f)), ((ViewGroup.MarginLayoutParams) layoutParams2).rightMargin, ((ViewGroup.MarginLayoutParams) layoutParams2).bottomMargin);
        ImageView imageView3 = this.binding.f2442b.f125b;
        m.checkNotNullExpressionValue(imageView3, "binding.loading.logo");
        imageView3.setLayoutParams(layoutParams2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setLoadingPanelVisibility(boolean z2, boolean z3) {
        h5 h5Var = this.binding.f2442b;
        m.checkNotNullExpressionValue(h5Var, "binding.loading");
        FrameLayout frameLayout = h5Var.a;
        m.checkNotNullExpressionValue(frameLayout, "binding.loading.root");
        if ((frameLayout.getVisibility() == 0) || !z2) {
            panelInitialized = z2;
            if (z2) {
                if (z3) {
                    h5 h5Var2 = this.binding.f2442b;
                    m.checkNotNullExpressionValue(h5Var2, "binding.loading");
                    ViewExtensions.fadeOut$default(h5Var2.a, 0L, null, null, 7, null);
                    d5 d5Var = this.binding.d;
                    m.checkNotNullExpressionValue(d5Var, "binding.panelCenter");
                    ViewExtensions.fadeIn$default(d5Var.a, 0L, null, null, null, 15, null);
                } else {
                    h5 h5Var3 = this.binding.f2442b;
                    m.checkNotNullExpressionValue(h5Var3, "binding.loading");
                    FrameLayout frameLayout2 = h5Var3.a;
                    m.checkNotNullExpressionValue(frameLayout2, "binding.loading.root");
                    frameLayout2.setVisibility(8);
                    d5 d5Var2 = this.binding.d;
                    m.checkNotNullExpressionValue(d5Var2, "binding.panelCenter");
                    RoundedRelativeLayout roundedRelativeLayout = d5Var2.a;
                    m.checkNotNullExpressionValue(roundedRelativeLayout, "binding.panelCenter.root");
                    roundedRelativeLayout.setVisibility(0);
                }
                StoreStream.Companion.getAnalytics().appUiViewed(WidgetHome.class);
                return;
            }
            d5 d5Var3 = this.binding.d;
            m.checkNotNullExpressionValue(d5Var3, "binding.panelCenter");
            RoundedRelativeLayout roundedRelativeLayout2 = d5Var3.a;
            m.checkNotNullExpressionValue(roundedRelativeLayout2, "binding.panelCenter.root");
            roundedRelativeLayout2.setVisibility(8);
            h5 h5Var4 = this.binding.f2442b;
            m.checkNotNullExpressionValue(h5Var4, "binding.loading");
            FrameLayout frameLayout3 = h5Var4.a;
            m.checkNotNullExpressionValue(frameLayout3, "binding.loading.root");
            frameLayout3.setVisibility(0);
        }
    }

    public final void configure(AppFragment appFragment) {
        m.checkNotNullParameter(appFragment, "fragment");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(WidgetHomeModel.Companion.getInitialized(), appFragment, null, 2, null), WidgetHomePanelLoading.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHomePanelLoading$configure$1(this));
        setLoadingPanelVisibility(panelInitialized, false);
    }
}
