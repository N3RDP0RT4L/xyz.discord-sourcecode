package com.discord.widgets.home;

import com.discord.models.guild.Guild;
import com.discord.stores.StoreNux;
import com.discord.stores.StoreStream;
import com.discord.utilities.features.GrowthTeamFeatures;
import d0.z.d.o;
import java.util.Collection;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/StoreNux$NuxState;", "kotlin.jvm.PlatformType", "state", "", "invoke", "(Lcom/discord/stores/StoreNux$NuxState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHome$onViewBound$3 extends o implements Function1<StoreNux.NuxState, Unit> {
    public final /* synthetic */ WidgetHome this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHome$onViewBound$3(WidgetHome widgetHome) {
        super(1);
        this.this$0 = widgetHome;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreNux.NuxState nuxState) {
        invoke2(nuxState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreNux.NuxState nuxState) {
        boolean z2;
        boolean z3 = false;
        if (!nuxState.getFirstOpen()) {
            Collection<Guild> values = StoreStream.Companion.getGuilds().getGuilds().values();
            if (!(values instanceof Collection) || !values.isEmpty()) {
                for (Guild guild : values) {
                    if (!(!guild.isHub())) {
                        z2 = false;
                        break;
                    }
                }
            }
            z2 = true;
            if (z2 && GrowthTeamFeatures.INSTANCE.isHubEmailConnectionEnabled()) {
                z3 = true;
            }
        }
        if (z3) {
            this.this$0.maybeShowHubEmailUpsell();
        }
    }
}
