package com.discord.widgets.home;

import androidx.core.app.NotificationCompat;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0007\u001a\n \u0001*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0003\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/widgets/home/WidgetHomeModel;", "kotlin.jvm.PlatformType", "model1", "model2", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/widgets/home/WidgetHomeModel;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHome$onViewBoundOrOnResume$4<T1, T2, R> implements Func2<WidgetHomeModel, WidgetHomeModel, Boolean> {
    public static final WidgetHome$onViewBoundOrOnResume$4 INSTANCE = new WidgetHome$onViewBoundOrOnResume$4();

    public final Boolean call(WidgetHomeModel widgetHomeModel, WidgetHomeModel widgetHomeModel2) {
        return Boolean.valueOf(widgetHomeModel.getChannelId() == widgetHomeModel2.getChannelId());
    }
}
