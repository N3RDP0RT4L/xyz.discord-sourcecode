package com.discord.widgets.home;

import androidx.core.app.NotificationCompat;
import com.discord.utilities.surveys.SurveyUtils;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/utilities/surveys/SurveyUtils$Survey;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHome$onViewBoundOrOnResume$6<T, R> implements b<SurveyUtils.Survey, Boolean> {
    public static final WidgetHome$onViewBoundOrOnResume$6 INSTANCE = new WidgetHome$onViewBoundOrOnResume$6();

    public final Boolean call(SurveyUtils.Survey survey) {
        return Boolean.valueOf(!m.areEqual(survey, SurveyUtils.Survey.None.INSTANCE));
    }
}
