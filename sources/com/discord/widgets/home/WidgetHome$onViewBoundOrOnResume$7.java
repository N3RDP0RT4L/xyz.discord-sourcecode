package com.discord.widgets.home;

import com.discord.utilities.surveys.SurveyUtils;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/surveys/SurveyUtils$Survey;", "p1", "", "invoke", "(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetHome$onViewBoundOrOnResume$7 extends k implements Function1<SurveyUtils.Survey, Unit> {
    public WidgetHome$onViewBoundOrOnResume$7(WidgetHome widgetHome) {
        super(1, widgetHome, WidgetHome.class, "showSurvey", "showSurvey(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(SurveyUtils.Survey survey) {
        invoke2(survey);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(SurveyUtils.Survey survey) {
        m.checkNotNullParameter(survey, "p1");
        ((WidgetHome) this.receiver).showSurvey(survey);
    }
}
