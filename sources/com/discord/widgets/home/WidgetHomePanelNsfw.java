package com.discord.widgets.home;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewKt;
import androidx.core.view.WindowInsetsCompat;
import b.a.d.f;
import b.a.k.b;
import com.discord.api.user.NsfwAllowance;
import com.discord.app.AppComponent;
import com.discord.databinding.WidgetHomePanelCenterNsfwBinding;
import com.discord.stores.StoreGuildsNsfw;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetHomePanelNsfw.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 -2\u00020\u0001:\u0001-B\u000f\u0012\u0006\u0010!\u001a\u00020 ¢\u0006\u0004\b+\u0010,JG\u0010\f\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00072\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\f\u0010\rJ+\u0010\u000f\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u00022\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\n0\tH\u0002¢\u0006\u0004\b\u000f\u0010\u0010Je\u0010\u0017\u001a\u00020\n2\n\u0010\u0013\u001a\u00060\u0011j\u0002`\u00122\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\b\u0010\u0014\u001a\u0004\u0018\u00010\u00072\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\n0\t2\u0010\b\u0002\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u0015\u0010\u001b\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0018\u0010$\u001a\u0004\u0018\u00010#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u0018\u0010'\u001a\u0004\u0018\u00010&8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010)\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010*¨\u0006."}, d2 = {"Lcom/discord/widgets/home/WidgetHomePanelNsfw;", "", "", "isChannelNsfw", "isNsfwUnconsented", "Lcom/discord/api/user/NsfwAllowance;", "nsfwAllowed", "Landroid/view/ViewStub;", "stub", "Lkotlin/Function1;", "", "onToggleNsfw", "toggleContainerVisibility", "(ZZLcom/discord/api/user/NsfwAllowance;Landroid/view/ViewStub;Lkotlin/jvm/functions/Function1;)V", "isHidden", "setContainerViewHidden", "(ZLkotlin/jvm/functions/Function1;)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "viewStub", "Lkotlin/Function0;", "onDenyNsfw", "configureUI", "(JZZLcom/discord/api/user/NsfwAllowance;Landroid/view/ViewStub;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V", "Landroidx/core/view/WindowInsetsCompat;", "insets", "dispatchApplyWindowInsets", "(Landroidx/core/view/WindowInsetsCompat;)V", "Lcom/discord/stores/StoreGuildsNsfw;", "guildsNsfwStore", "Lcom/discord/stores/StoreGuildsNsfw;", "Lcom/discord/app/AppComponent;", "appComponent", "Lcom/discord/app/AppComponent;", "Lrx/Subscription;", "hidePanelSubscription", "Lrx/Subscription;", "Lcom/discord/databinding/WidgetHomePanelCenterNsfwBinding;", "binding", "Lcom/discord/databinding/WidgetHomePanelCenterNsfwBinding;", "stubInflated", "Z", HookHelper.constructorName, "(Lcom/discord/app/AppComponent;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHomePanelNsfw {
    private static final Companion Companion = new Companion(null);
    @Deprecated
    private static final long HIDE_DELAY_MILLIS = 500;
    private final AppComponent appComponent;
    private WidgetHomePanelCenterNsfwBinding binding;
    private final StoreGuildsNsfw guildsNsfwStore = StoreStream.Companion.getGuildsNsfw();
    private Subscription hidePanelSubscription;
    private boolean stubInflated;

    /* compiled from: WidgetHomePanelNsfw.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/home/WidgetHomePanelNsfw$Companion;", "", "", "HIDE_DELAY_MILLIS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetHomePanelNsfw(AppComponent appComponent) {
        m.checkNotNullParameter(appComponent, "appComponent");
        this.appComponent = appComponent;
    }

    public static /* synthetic */ void configureUI$default(WidgetHomePanelNsfw widgetHomePanelNsfw, long j, boolean z2, boolean z3, NsfwAllowance nsfwAllowance, ViewStub viewStub, Function1 function1, Function0 function0, int i, Object obj) {
        widgetHomePanelNsfw.configureUI(j, z2, z3, nsfwAllowance, viewStub, function1, (i & 64) != 0 ? null : function0);
    }

    public final void setContainerViewHidden(boolean z2, Function1<? super Boolean, Unit> function1) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        LinearLayout linearLayout3;
        LinearLayout linearLayout4;
        WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding = this.binding;
        if ((widgetHomePanelCenterNsfwBinding == null || (linearLayout4 = widgetHomePanelCenterNsfwBinding.a) == null || linearLayout4.getVisibility() != 8) && z2) {
            WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding2 = this.binding;
            if (!(widgetHomePanelCenterNsfwBinding2 == null || (linearLayout3 = widgetHomePanelCenterNsfwBinding2.a) == null)) {
                linearLayout3.setVisibility(8);
            }
            function1.invoke(Boolean.FALSE);
            return;
        }
        WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding3 = this.binding;
        if ((widgetHomePanelCenterNsfwBinding3 == null || (linearLayout2 = widgetHomePanelCenterNsfwBinding3.a) == null || linearLayout2.getVisibility() != 0) && !z2) {
            WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding4 = this.binding;
            if (!(widgetHomePanelCenterNsfwBinding4 == null || (linearLayout = widgetHomePanelCenterNsfwBinding4.a) == null)) {
                linearLayout.setVisibility(0);
            }
            function1.invoke(Boolean.TRUE);
        }
    }

    private final void toggleContainerVisibility(boolean z2, boolean z3, NsfwAllowance nsfwAllowance, ViewStub viewStub, Function1<? super Boolean, Unit> function1) {
        LinkifiedTextView linkifiedTextView;
        LinkifiedTextView linkifiedTextView2;
        TextView textView;
        MaterialButton materialButton;
        ImageView imageView;
        MaterialButton materialButton2;
        View inflate;
        boolean z4 = nsfwAllowance == NsfwAllowance.DISALLOWED;
        if (!z2 || (!z3 && !z4)) {
            Observable<Long> d02 = Observable.d0(HIDE_DELAY_MILLIS, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable.timer(HIDE_DE…S, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this.appComponent, null, 2, null), WidgetHomePanelNsfw.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetHomePanelNsfw$toggleContainerVisibility$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHomePanelNsfw$toggleContainerVisibility$3(this, function1));
            return;
        }
        if (!(this.stubInflated || viewStub == null || (inflate = viewStub.inflate()) == null)) {
            this.stubInflated = true;
            int i = R.id.home_panel_center_nsfw_anchor_wrap;
            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.home_panel_center_nsfw_anchor_wrap);
            if (linearLayout != null) {
                i = R.id.home_panel_center_nsfw_art;
                ImageView imageView2 = (ImageView) inflate.findViewById(R.id.home_panel_center_nsfw_art);
                if (imageView2 != null) {
                    i = R.id.home_panel_center_nsfw_confirm;
                    MaterialButton materialButton3 = (MaterialButton) inflate.findViewById(R.id.home_panel_center_nsfw_confirm);
                    if (materialButton3 != null) {
                        i = R.id.home_panel_center_nsfw_deny;
                        MaterialButton materialButton4 = (MaterialButton) inflate.findViewById(R.id.home_panel_center_nsfw_deny);
                        if (materialButton4 != null) {
                            i = R.id.home_panel_center_nsfw_description;
                            LinkifiedTextView linkifiedTextView3 = (LinkifiedTextView) inflate.findViewById(R.id.home_panel_center_nsfw_description);
                            if (linkifiedTextView3 != null) {
                                i = R.id.home_panel_center_nsfw_title;
                                TextView textView2 = (TextView) inflate.findViewById(R.id.home_panel_center_nsfw_title);
                                if (textView2 != null) {
                                    this.binding = new WidgetHomePanelCenterNsfwBinding((LinearLayout) inflate, linearLayout, imageView2, materialButton3, materialButton4, linkifiedTextView3, textView2);
                                    ViewCompat.setOnApplyWindowInsetsListener(inflate, WidgetHomePanelNsfw$toggleContainerVisibility$1$1.INSTANCE);
                                    inflate.requestApplyInsets();
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
        }
        if (z4) {
            WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding = this.binding;
            if (!(widgetHomePanelCenterNsfwBinding == null || (materialButton2 = widgetHomePanelCenterNsfwBinding.c) == null)) {
                ViewKt.setVisible(materialButton2, false);
            }
            WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding2 = this.binding;
            if (!(widgetHomePanelCenterNsfwBinding2 == null || (imageView = widgetHomePanelCenterNsfwBinding2.f2443b) == null)) {
                imageView.setImageResource(R.drawable.img_age_gate_failure);
            }
            WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding3 = this.binding;
            if (!(widgetHomePanelCenterNsfwBinding3 == null || (materialButton = widgetHomePanelCenterNsfwBinding3.d) == null)) {
                b.m(materialButton, R.string.back, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            }
            WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding4 = this.binding;
            if (!(widgetHomePanelCenterNsfwBinding4 == null || (textView = widgetHomePanelCenterNsfwBinding4.f) == null)) {
                b.m(textView, R.string.age_gate_nsfw_underage_header, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            }
            WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding5 = this.binding;
            if (!(widgetHomePanelCenterNsfwBinding5 == null || (linkifiedTextView2 = widgetHomePanelCenterNsfwBinding5.e) == null)) {
                b.m(linkifiedTextView2, R.string.age_gate_nsfw_underage_body, new Object[]{f.a.a(115000084051L, "h_5206f3f2-0ee4-4380-b50a-25319e45bc7c")}, (r4 & 4) != 0 ? b.g.j : null);
            }
        } else {
            WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding6 = this.binding;
            if (!(widgetHomePanelCenterNsfwBinding6 == null || (linkifiedTextView = widgetHomePanelCenterNsfwBinding6.e) == null)) {
                b.m(linkifiedTextView, R.string.age_gate_nsfw_description, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            }
        }
        setContainerViewHidden(false, function1);
    }

    @MainThread
    public final void configureUI(final long j, boolean z2, boolean z3, NsfwAllowance nsfwAllowance, ViewStub viewStub, final Function1<? super Boolean, Unit> function1, final Function0<Unit> function0) {
        MaterialButton materialButton;
        MaterialButton materialButton2;
        m.checkNotNullParameter(function1, "onToggleNsfw");
        Subscription subscription = this.hidePanelSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        toggleContainerVisibility(z2, z3, nsfwAllowance, viewStub, function1);
        WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding = this.binding;
        if (!(widgetHomePanelCenterNsfwBinding == null || (materialButton2 = widgetHomePanelCenterNsfwBinding.d) == null)) {
            materialButton2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.home.WidgetHomePanelNsfw$configureUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StoreGuildsNsfw storeGuildsNsfw;
                    storeGuildsNsfw = WidgetHomePanelNsfw.this.guildsNsfwStore;
                    storeGuildsNsfw.deny(j);
                    Function0 function02 = function0;
                    if (function02 != null) {
                        Unit unit = (Unit) function02.invoke();
                    }
                }
            });
        }
        WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding2 = this.binding;
        if (widgetHomePanelCenterNsfwBinding2 != null && (materialButton = widgetHomePanelCenterNsfwBinding2.c) != null) {
            materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.home.WidgetHomePanelNsfw$configureUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StoreGuildsNsfw storeGuildsNsfw;
                    storeGuildsNsfw = WidgetHomePanelNsfw.this.guildsNsfwStore;
                    storeGuildsNsfw.allow(j);
                    WidgetHomePanelNsfw.this.setContainerViewHidden(true, function1);
                }
            });
        }
    }

    public final void dispatchApplyWindowInsets(WindowInsetsCompat windowInsetsCompat) {
        LinearLayout linearLayout;
        m.checkNotNullParameter(windowInsetsCompat, "insets");
        WidgetHomePanelCenterNsfwBinding widgetHomePanelCenterNsfwBinding = this.binding;
        if (widgetHomePanelCenterNsfwBinding != null && (linearLayout = widgetHomePanelCenterNsfwBinding.a) != null) {
            ViewCompat.dispatchApplyWindowInsets(linearLayout, windowInsetsCompat);
        }
    }
}
