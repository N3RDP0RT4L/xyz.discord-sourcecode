package com.discord.widgets.home;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\n\u001a\u0004\u0018\u00010\u00002\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002.\u0010\u0007\u001a*\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0006 \u0001*\u0014\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "kotlin.jvm.PlatformType", "connectionOpen", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "channels", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;Ljava/util/Map;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHome$configureFirstOpen$1<T1, T2, R> implements Func2<Boolean, Map<Long, ? extends Channel>, Boolean> {
    public static final WidgetHome$configureFirstOpen$1 INSTANCE = new WidgetHome$configureFirstOpen$1();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ Boolean call(Boolean bool, Map<Long, ? extends Channel> map) {
        return call2(bool, (Map<Long, Channel>) map);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final Boolean call2(Boolean bool, Map<Long, Channel> map) {
        m.checkNotNullExpressionValue(bool, "connectionOpen");
        if (bool.booleanValue()) {
            return Boolean.valueOf(map.isEmpty());
        }
        return null;
    }
}
