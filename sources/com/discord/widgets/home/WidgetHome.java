package com.discord.widgets.home;

import andhook.lib.HookHelper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.graphics.Insets;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewGroupKt;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.d5;
import b.a.i.e5;
import b.a.i.f5;
import b.a.i.g5;
import b.a.i.i5;
import b.a.o.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetHomeBinding;
import com.discord.panels.OverlappingPanelsLayout;
import com.discord.panels.PanelState;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreConnectionOpen;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreNux;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreTabsNavigation;
import com.discord.stores.StoreUser;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.locale.LocaleManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.surveys.SurveyUtils;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.rounded.RoundedRelativeLayout;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.accessibility.AccessibilityDetectionNavigator;
import com.discord.widgets.channels.WidgetChannelOnboarding;
import com.discord.widgets.chat.input.SmoothKeyboardReactionHelper;
import com.discord.widgets.chat.list.WidgetChatList;
import com.discord.widgets.directories.WidgetDirectoryChannel;
import com.discord.widgets.guilds.join.WidgetGuildWelcomeSheet;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventUpsellBottomSheet;
import com.discord.widgets.home.WidgetHomeModel;
import com.discord.widgets.home.WidgetHomeViewModel;
import com.discord.widgets.hubs.WidgetHubEmailFlow;
import com.discord.widgets.notice.WidgetNoticeNuxSamsungLink;
import com.discord.widgets.playstation.WidgetPlaystationIntegrationUpsellBottomSheet;
import com.discord.widgets.status.WidgetGlobalStatusIndicatorState;
import com.discord.widgets.tabs.NavigationTab;
import com.discord.widgets.tabs.OnTabSelectedListener;
import com.discord.widgets.tabs.WidgetTabsHost;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen;
import com.google.android.material.appbar.AppBarLayout;
import d0.t.m0;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetHome.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ø\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u0000 \u0088\u00012\u00020\u00012\u00020\u00022\u00020\u0003:\u0002\u0088\u0001B\b¢\u0006\u0005\b\u0087\u0001\u0010\nJ\u0019\u0010\u0007\u001a\u00020\u00062\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ-\u0010\u0010\u001a\u00020\u00062\n\u0010\r\u001a\u00060\u000bj\u0002`\f2\u0010\b\u0002\u0010\u000f\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0012\u001a\u00020\u00062\n\u0010\r\u001a\u00060\u000bj\u0002`\fH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\u0014\u001a\u00020\u00062\n\u0010\u000f\u001a\u00060\u000bj\u0002`\u000eH\u0002¢\u0006\u0004\b\u0014\u0010\u0013J\u0017\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0019\u0010\nJ\u000f\u0010\u001a\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u001a\u0010\nJ\u0017\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010!\u001a\u00020\u00062\u0006\u0010 \u001a\u00020\u001fH\u0002¢\u0006\u0004\b!\u0010\"J\u0017\u0010%\u001a\u00020\u00062\u0006\u0010$\u001a\u00020#H\u0002¢\u0006\u0004\b%\u0010&J\u0017\u0010)\u001a\u00020\u00062\u0006\u0010(\u001a\u00020'H\u0002¢\u0006\u0004\b)\u0010*J\u000f\u0010+\u001a\u00020\u0006H\u0002¢\u0006\u0004\b+\u0010\nJ\u000f\u0010,\u001a\u00020\u0006H\u0002¢\u0006\u0004\b,\u0010\nJ\u0013\u0010.\u001a\u00020\u0006*\u00020-H\u0002¢\u0006\u0004\b.\u0010/J\u0013\u00100\u001a\u00020\u0006*\u00020-H\u0002¢\u0006\u0004\b0\u0010/J\u0013\u00102\u001a\u00020\u0006*\u000201H\u0002¢\u0006\u0004\b2\u00103J\u000f\u00104\u001a\u00020#H\u0002¢\u0006\u0004\b4\u00105J\u000f\u00106\u001a\u00020\u0006H\u0002¢\u0006\u0004\b6\u0010\nJ\u000f\u00107\u001a\u00020\u0006H\u0002¢\u0006\u0004\b7\u0010\nJ\u000f\u00108\u001a\u00020#H\u0002¢\u0006\u0004\b8\u00105J\u0017\u0010;\u001a\u00020\u00062\u0006\u0010:\u001a\u000209H\u0002¢\u0006\u0004\b;\u0010<J\u000f\u0010=\u001a\u00020\u0006H\u0002¢\u0006\u0004\b=\u0010\nJ\u000f\u0010>\u001a\u00020\u0006H\u0002¢\u0006\u0004\b>\u0010\nJ\u000f\u0010?\u001a\u00020\u0006H\u0002¢\u0006\u0004\b?\u0010\nJ\u0017\u0010@\u001a\u00020\u00062\u0006\u0010(\u001a\u00020'H\u0002¢\u0006\u0004\b@\u0010*J\u000f\u0010A\u001a\u00020\u0006H\u0002¢\u0006\u0004\bA\u0010\nJ\u0019\u0010D\u001a\u00020\u00062\b\u0010C\u001a\u0004\u0018\u00010BH\u0016¢\u0006\u0004\bD\u0010EJ\u0017\u0010H\u001a\u00020\u00062\u0006\u0010G\u001a\u00020FH\u0016¢\u0006\u0004\bH\u0010IJ\u000f\u0010J\u001a\u00020\u0006H\u0016¢\u0006\u0004\bJ\u0010\nJ\u000f\u0010K\u001a\u00020\u0006H\u0016¢\u0006\u0004\bK\u0010\nJ\u000f\u0010L\u001a\u00020\u0006H\u0016¢\u0006\u0004\bL\u0010\nJ\u000f\u0010M\u001a\u00020\u0006H\u0016¢\u0006\u0004\bM\u0010\nJ\u001d\u0010Q\u001a\u00020\u00062\f\u0010P\u001a\b\u0012\u0004\u0012\u00020O0NH\u0016¢\u0006\u0004\bQ\u0010RJ\u0015\u0010T\u001a\u00020\u00062\u0006\u0010S\u001a\u00020#¢\u0006\u0004\bT\u0010&J\u000f\u0010V\u001a\u0004\u0018\u00010U¢\u0006\u0004\bV\u0010WJ\r\u0010Y\u001a\u00020X¢\u0006\u0004\bY\u0010ZJ\r\u0010\\\u001a\u00020[¢\u0006\u0004\b\\\u0010]J!\u0010`\u001a\u00020\u00062\u0012\u0010_\u001a\u000e\u0012\u0004\u0012\u00020F\u0012\u0004\u0012\u00020\u00060^¢\u0006\u0004\b`\u0010aJ\u000f\u0010c\u001a\u00020\u0006H\u0000¢\u0006\u0004\bb\u0010\nR\u001d\u0010i\u001a\u00020d8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\be\u0010f\u001a\u0004\bg\u0010hR\u001d\u0010o\u001a\u00020j8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bk\u0010l\u001a\u0004\bm\u0010nR\u0016\u0010q\u001a\u00020p8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bq\u0010rR\u0016\u0010t\u001a\u00020s8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bt\u0010uR\u0018\u0010w\u001a\u0004\u0018\u00010v8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bw\u0010xR\"\u0010_\u001a\u000e\u0012\u0004\u0012\u00020F\u0012\u0004\u0012\u00020\u00060^8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b_\u0010yR\u0016\u0010{\u001a\u00020z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b{\u0010|R\u0018\u0010~\u001a\u0004\u0018\u00010}8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b~\u0010\u007fR\u001a\u0010\u0081\u0001\u001a\u00030\u0080\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0081\u0001\u0010\u0082\u0001R!\u0010\u0085\u0001\u001a\n\u0012\u0005\u0012\u00030\u0084\u00010\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0085\u0001\u0010\u0086\u0001¨\u0006\u0089\u0001"}, d2 = {"Lcom/discord/widgets/home/WidgetHome;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/tabs/OnTabSelectedListener;", "Lb/a/o/b$a;", "Lcom/discord/widgets/home/HomeConfig;", "homeConfig", "", "handleHomeConfig", "(Lcom/discord/widgets/home/HomeConfig;)V", "showChannelOnboardingSheet", "()V", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/GuildScheduledEventId;", "guildScheduledEventId", "showWelcomeSheet", "(JLjava/lang/Long;)V", "showGuildEventUpsell", "(J)V", "enqueueEventDetails", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;", "state", "handleGlobalStatusIndicatorState", "(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V", "roundPanelCorners", "unroundPanelCorners", "", "radius", "setPanelCorners", "(F)V", "Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;", "viewState", "handleViewState", "(Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;)V", "", "isHidden", "onNsfwToggle", "(Z)V", "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/home/WidgetHomeViewModel$Event;)V", "showPlaystationUpsell", "configureOverlappingPanels", "Lcom/discord/widgets/home/WidgetHomeModel;", "configureUI", "(Lcom/discord/widgets/home/WidgetHomeModel;)V", "maybeToggleChannel", "Lcom/discord/stores/StoreNavigation;", "configureNavigationDrawerAction", "(Lcom/discord/stores/StoreNavigation;)V", "isOnHomeTab", "()Z", "configureFirstOpen", "configureLeftPanel", "handleBackPressed", "Lcom/discord/utilities/surveys/SurveyUtils$Survey;", "survey", "showSurvey", "(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)V", "showUrgentMessageDialog", "maybeShowHubEmailUpsell", "setPanelWindowInsetsListeners", "animatePeek", "setupSmoothKeyboardReaction", "Landroid/os/Bundle;", "savedInstanceState", "onCreate", "(Landroid/os/Bundle;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onResume", "onPause", "onTabSelected", "", "Landroid/graphics/Rect;", "gestureRegions", "onGestureRegionsUpdate", "(Ljava/util/List;)V", "lock", "lockCloseRightPanel", "Landroidx/appcompat/widget/Toolbar;", "getToolbar", "()Landroidx/appcompat/widget/Toolbar;", "Landroid/widget/TextView;", "getUnreadCountView", "()Landroid/widget/TextView;", "Lcom/discord/widgets/home/PanelLayout;", "getPanelLayout", "()Lcom/discord/widgets/home/PanelLayout;", "Lkotlin/Function1;", "onGuildListAddHintCreate", "setOnGuildListAddHintCreate", "(Lkotlin/jvm/functions/Function1;)V", "handleCenterPanelBack$app_productionGoogleRelease", "handleCenterPanelBack", "Lcom/discord/databinding/WidgetHomeBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetHomeBinding;", "binding", "Lcom/discord/widgets/home/WidgetHomeViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/home/WidgetHomeViewModel;", "viewModel", "Lcom/discord/stores/StoreTabsNavigation;", "storeTabsNavigation", "Lcom/discord/stores/StoreTabsNavigation;", "Lcom/discord/utilities/locale/LocaleManager;", "localeManager", "Lcom/discord/utilities/locale/LocaleManager;", "Lcom/discord/widgets/home/WidgetHomePanelLoading;", "panelLoading", "Lcom/discord/widgets/home/WidgetHomePanelLoading;", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/widgets/home/LeftPanelManager;", "leftPanelManager", "Lcom/discord/widgets/home/LeftPanelManager;", "Lcom/discord/widgets/home/WidgetHomePanelNsfw;", "panelNsfw", "Lcom/discord/widgets/home/WidgetHomePanelNsfw;", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;", "globalStatusIndicatorStateObserver", "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;", "", "", "fixedPositionViewIds", "Ljava/util/Set;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHome extends AppFragment implements OnTabSelectedListener, b.a {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetHome.class, "binding", "getBinding()Lcom/discord/databinding/WidgetHomeBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final long DELAY_DRAWER_OPEN_FINISH = 2000;
    private static final long DELAY_DRAWER_OPEN_START = 1000;
    private WidgetHomePanelLoading panelLoading;
    private WidgetHomePanelNsfw panelNsfw;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetHome$binding$2.INSTANCE, null, 2, null);
    private final Set<Integer> fixedPositionViewIds = m0.setOf(Integer.valueOf((int) R.id.unread));
    private final LeftPanelManager leftPanelManager = new LeftPanelManager(null, null, 3, null);
    private final LocaleManager localeManager = new LocaleManager();
    private Function1<? super View, Unit> onGuildListAddHintCreate = WidgetHome$onGuildListAddHintCreate$1.INSTANCE;
    private final StoreTabsNavigation storeTabsNavigation = StoreStream.Companion.getTabsNavigation();
    private final WidgetGlobalStatusIndicatorState globalStatusIndicatorStateObserver = WidgetGlobalStatusIndicatorState.Provider.get();

    /* compiled from: WidgetHome.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/home/WidgetHome$Companion;", "", "", "DELAY_DRAWER_OPEN_FINISH", "J", "DELAY_DRAWER_OPEN_START", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            OverlappingPanelsLayout.Panel.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[OverlappingPanelsLayout.Panel.END.ordinal()] = 1;
            iArr[OverlappingPanelsLayout.Panel.CENTER.ordinal()] = 2;
            iArr[OverlappingPanelsLayout.Panel.START.ordinal()] = 3;
        }
    }

    public WidgetHome() {
        super(R.layout.widget_home);
        WidgetHome$viewModel$2 widgetHome$viewModel$2 = WidgetHome$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetHomeViewModel.class), new WidgetHome$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetHome$viewModel$2));
    }

    private final void animatePeek(WidgetHomeViewModel.Event event) {
        View view;
        long j;
        float f;
        float f2;
        View view2;
        if (!AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
            Fragment findFragmentById = getChildFragmentManager().findFragmentById(R.id.widget_chat_list);
            if (!(findFragmentById instanceof WidgetChatList)) {
                findFragmentById = null;
            }
            final WidgetChatList widgetChatList = (WidgetChatList) findFragmentById;
            if (widgetChatList != null) {
                d5 d5Var = getBinding().d;
                m.checkNotNullExpressionValue(d5Var, "binding.panelCenter");
                RoundedRelativeLayout roundedRelativeLayout = d5Var.a;
                m.checkNotNullExpressionValue(roundedRelativeLayout, "binding.panelCenter.root");
                Bitmap createBitmap = Bitmap.createBitmap(roundedRelativeLayout.getMeasuredWidth(), roundedRelativeLayout.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
                roundedRelativeLayout.draw(new Canvas(createBitmap));
                getBinding().g.setImageBitmap(createBitmap);
                if (event instanceof WidgetHomeViewModel.Event.AnimatePeekIn) {
                    j = 250;
                    View view3 = getBinding().g;
                    m.checkNotNullExpressionValue(view3, "binding.peekTransitionBitmap");
                    f = roundedRelativeLayout.getRight();
                    view2 = view3;
                    view = roundedRelativeLayout;
                    f2 = 0.0f;
                } else {
                    j = 200;
                    view = getBinding().g;
                    m.checkNotNullExpressionValue(view, "binding.peekTransitionBitmap");
                    f2 = roundedRelativeLayout.getRight();
                    view2 = roundedRelativeLayout;
                    f = 0.0f;
                }
                view.setTranslationX(f);
                view.animate().setDuration(j).translationX(f2).withEndAction(new Runnable() { // from class: com.discord.widgets.home.WidgetHome$animatePeek$1
                    @Override // java.lang.Runnable
                    public final void run() {
                        WidgetHomeBinding binding;
                        binding = WidgetHome.this.getBinding();
                        ImageView imageView = binding.g;
                        m.checkNotNullExpressionValue(imageView, "binding.peekTransitionBitmap");
                        imageView.setVisibility(8);
                        widgetChatList.enableItemAnimations();
                    }
                }).start();
                view2.setTranslationX(f - roundedRelativeLayout.getMeasuredWidth());
                view2.animate().setDuration(j).translationX(f2 - roundedRelativeLayout.getMeasuredWidth()).start();
                widgetChatList.disableItemAnimations();
                ImageView imageView = getBinding().g;
                m.checkNotNullExpressionValue(imageView, "binding.peekTransitionBitmap");
                imageView.setVisibility(0);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureFirstOpen() {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable y2 = Observable.j(StoreConnectionOpen.observeConnectionOpen$default(companion.getConnectionOpen(), false, 1, null), companion.getChannels().observeGuildAndPrivateChannels(), WidgetHome$configureFirstOpen$1.INSTANCE).y();
        m.checkNotNullExpressionValue(y2, "Observable\n        .comb…       }\n        .first()");
        ObservableExtensionsKt.appSubscribe(y2, WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$configureFirstOpen$2(this));
    }

    private final void configureLeftPanel() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(this.leftPanelManager.observeLockState()), this, null, 2, null), WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$configureLeftPanel$1(this));
    }

    private final void configureNavigationDrawerAction(StoreNavigation storeNavigation) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(storeNavigation.getNavigationPanelAction(), this, null, 2, null), StoreNavigation.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$configureNavigationDrawerAction$1(this, storeNavigation));
    }

    private final void configureOverlappingPanels() {
        getBinding().c.registerStartPanelStateListeners(new OverlappingPanelsLayout.PanelStateListener() { // from class: com.discord.widgets.home.WidgetHome$configureOverlappingPanels$1
            @Override // com.discord.panels.OverlappingPanelsLayout.PanelStateListener
            public void onPanelStateChange(PanelState panelState) {
                WidgetHomeViewModel viewModel;
                m.checkNotNullParameter(panelState, "panelState");
                viewModel = WidgetHome.this.getViewModel();
                viewModel.onStartPanelStateChange(panelState);
            }
        });
        getBinding().c.registerEndPanelStateListeners(new OverlappingPanelsLayout.PanelStateListener() { // from class: com.discord.widgets.home.WidgetHome$configureOverlappingPanels$2
            @Override // com.discord.panels.OverlappingPanelsLayout.PanelStateListener
            public void onPanelStateChange(PanelState panelState) {
                WidgetHomeViewModel viewModel;
                m.checkNotNullParameter(panelState, "panelState");
                viewModel = WidgetHome.this.getViewModel();
                viewModel.onEndPanelStateChange(panelState);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetHomeModel widgetHomeModel) {
        getViewModel().setWidgetHomeModel$app_productionGoogleRelease(widgetHomeModel);
        if (isOnHomeTab()) {
            WidgetHomeHeaderManager widgetHomeHeaderManager = WidgetHomeHeaderManager.INSTANCE;
            WidgetHomeBinding binding = getBinding();
            m.checkNotNullExpressionValue(binding, "binding");
            widgetHomeHeaderManager.configure(this, widgetHomeModel, binding);
        }
    }

    private final void enqueueEventDetails(long j) {
        WidgetGuildScheduledEventDetailsBottomSheet.Companion.enqueue(j);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetHomeBinding getBinding() {
        return (WidgetHomeBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetHomeViewModel getViewModel() {
        return (WidgetHomeViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean handleBackPressed() {
        int ordinal = getBinding().c.getSelectedPanel().ordinal();
        if (ordinal == 0) {
            requireActivity().moveTaskToBack(true);
        } else if (ordinal == 1) {
            handleCenterPanelBack$app_productionGoogleRelease();
        } else if (ordinal == 2) {
            getBinding().c.closePanels();
        }
        return true;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(WidgetHomeViewModel.Event event) {
        if (m.areEqual(event, WidgetHomeViewModel.Event.OpenLeftPanel.INSTANCE)) {
            getBinding().c.openStartPanel();
        } else if (m.areEqual(event, WidgetHomeViewModel.Event.ClosePanels.INSTANCE)) {
            getBinding().c.closePanels();
        } else if (m.areEqual(event, WidgetHomeViewModel.Event.UnlockLeftPanel.INSTANCE)) {
            getBinding().c.setStartPanelLockState(OverlappingPanelsLayout.LockState.UNLOCKED);
        } else if (m.areEqual(event, WidgetHomeViewModel.Event.ShowChannelOnboardingSheet.INSTANCE)) {
            showChannelOnboardingSheet();
        } else if (m.areEqual(event, WidgetHomeViewModel.Event.AnimatePeekIn.INSTANCE) || m.areEqual(event, WidgetHomeViewModel.Event.AnimatePeekOut.INSTANCE)) {
            animatePeek(event);
        } else if (event instanceof WidgetHomeViewModel.Event.ShowGuildWelcomeSheet) {
            showWelcomeSheet$default(this, ((WidgetHomeViewModel.Event.ShowGuildWelcomeSheet) event).getGuildId(), null, 2, null);
        } else if (event instanceof WidgetHomeViewModel.Event.ShowGuildEventUpsell) {
            showGuildEventUpsell(((WidgetHomeViewModel.Event.ShowGuildEventUpsell) event).getGuildId());
        } else if (event instanceof WidgetHomeViewModel.Event.ShowPlaystationUpsell) {
            showPlaystationUpsell();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleGlobalStatusIndicatorState(WidgetGlobalStatusIndicatorState.State state) {
        if (state.isCustomBackground()) {
            unroundPanelCorners();
        } else {
            roundPanelCorners();
        }
    }

    private final void handleHomeConfig(HomeConfig homeConfig) {
        Long l;
        Long l2 = null;
        if ((homeConfig != null ? homeConfig.getGuildWelcomeSheetId() : null) == null || homeConfig.getGuildScheduledEventId() == null) {
            if (homeConfig != null) {
                l = homeConfig.getGuildWelcomeSheetId();
            } else {
                l = null;
            }
            if (l != null) {
                showWelcomeSheet$default(this, homeConfig.getGuildWelcomeSheetId().longValue(), null, 2, null);
                return;
            }
            if (homeConfig != null) {
                l2 = homeConfig.getGuildScheduledEventId();
            }
            if (l2 != null) {
                enqueueEventDetails(homeConfig.getGuildScheduledEventId().longValue());
                return;
            }
            return;
        }
        showWelcomeSheet(homeConfig.getGuildWelcomeSheetId().longValue(), homeConfig.getGuildScheduledEventId());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleViewState(WidgetHomeViewModel.ViewState viewState) {
        getBinding().c.handleStartPanelState(viewState.getLeftPanelState());
        getBinding().c.handleEndPanelState(viewState.getRightPanelState());
        WidgetHomePanelNsfw widgetHomePanelNsfw = this.panelNsfw;
        if (widgetHomePanelNsfw != null) {
            WidgetHomePanelNsfw.configureUI$default(widgetHomePanelNsfw, viewState.getGuildId(), viewState.isChannelNsfw(), viewState.isNsfwUnconsented(), viewState.getNsfwAllowed(), getBinding().d.f100b.f106b, new WidgetHome$handleViewState$1(this), null, 64, null);
        }
    }

    private final boolean isOnHomeTab() {
        return this.storeTabsNavigation.getSelectedTab() == NavigationTab.HOME;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void maybeShowHubEmailUpsell() {
        StoreNotices notices = StoreStream.Companion.getNotices();
        String name = WidgetHubEmailFlow.Companion.getNAME();
        m.checkNotNullExpressionValue(name, "WidgetHubEmailFlow.NAME");
        notices.requestToShow(new StoreNotices.Notice(name, null, 0L, 0, true, d0.t.m.listOf(a0.getOrCreateKotlinClass(WidgetHome.class)), 0L, true, RecyclerView.FOREVER_NS, WidgetHome$maybeShowHubEmailUpsell$1.INSTANCE, 6, null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void maybeToggleChannel(WidgetHomeModel widgetHomeModel) {
        Channel channel = widgetHomeModel.getChannel();
        int i = 0;
        boolean z2 = channel != null && ChannelUtils.o(channel);
        FragmentContainerView fragmentContainerView = getBinding().d.c;
        m.checkNotNullExpressionValue(fragmentContainerView, "binding.panelCenter.widgetHomePanelDirectory");
        fragmentContainerView.setVisibility(z2 ? 0 : 8);
        Fragment findFragmentById = getChildFragmentManager().findFragmentById(R.id.widget_home_panel_directory);
        if (!(findFragmentById instanceof WidgetDirectoryChannel)) {
            findFragmentById = null;
        }
        WidgetDirectoryChannel widgetDirectoryChannel = (WidgetDirectoryChannel) findFragmentById;
        if (widgetDirectoryChannel != null) {
            WidgetDirectoryChannel.bindGestureObservers$default(widgetDirectoryChannel, z2, null, 2, null);
        }
        e5 e5Var = getBinding().d.f100b;
        m.checkNotNullExpressionValue(e5Var, "binding.panelCenter.widgetHomePanelCenterChat");
        ConstraintLayout constraintLayout = e5Var.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.panelCenter.widgetHomePanelCenterChat.root");
        if (!(!z2)) {
            i = 8;
        }
        constraintLayout.setVisibility(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onNsfwToggle(boolean z2) {
        FragmentManager childFragmentManager = getChildFragmentManager();
        m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        Fragment findFragmentById = childFragmentManager.findFragmentById(R.id.widget_chat_list);
        if (findFragmentById != null) {
            m.checkNotNullExpressionValue(findFragmentById, "fragmentManager.findFrag…dget_chat_list) ?: return");
            FragmentTransaction beginTransaction = childFragmentManager.beginTransaction();
            m.checkNotNullExpressionValue(beginTransaction, "fragmentManager.beginTransaction()");
            if (z2) {
                beginTransaction.hide(findFragmentById);
            } else {
                beginTransaction.show(findFragmentById);
            }
            beginTransaction.commit();
        }
    }

    private final void roundPanelCorners() {
        setPanelCorners(DimenUtils.dpToPixels(8));
    }

    private final void setPanelCorners(float f) {
        d5 d5Var = getBinding().d;
        m.checkNotNullExpressionValue(d5Var, "binding.panelCenter");
        d5Var.a.updateTopLeftRadius(f);
        d5 d5Var2 = getBinding().d;
        m.checkNotNullExpressionValue(d5Var2, "binding.panelCenter");
        d5Var2.a.updateTopRightRadius(f);
        getBinding().f.f132b.updateTopLeftRadius(f);
        getBinding().f.f132b.updateTopRightRadius(f);
    }

    private final void setPanelWindowInsetsListeners() {
        FrameLayout frameLayout = getBinding().h;
        m.checkNotNullExpressionValue(frameLayout, "binding.widgetHomeContainer");
        ViewExtensions.setForwardingWindowInsetsListener(frameLayout);
        HomePanelsLayout homePanelsLayout = getBinding().c;
        m.checkNotNullExpressionValue(homePanelsLayout, "binding.overlappingPanels");
        ViewExtensions.setForwardingWindowInsetsListener(homePanelsLayout);
        g5 g5Var = getBinding().e;
        m.checkNotNullExpressionValue(g5Var, "binding.panelLeft");
        ViewCompat.setOnApplyWindowInsetsListener(g5Var.a, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.home.WidgetHome$setPanelWindowInsetsListeners$1
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetHomeBinding binding;
                m.checkNotNullParameter(view, "<anonymous parameter 0>");
                m.checkNotNullParameter(windowInsetsCompat, "insets");
                binding = WidgetHome.this.getBinding();
                ViewGroup.LayoutParams layoutParams = binding.e.c.getLayoutParams();
                Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin = windowInsetsCompat.getSystemWindowInsetLeft();
                return windowInsetsCompat.consumeSystemWindowInsets();
            }
        });
        d5 d5Var = getBinding().d;
        m.checkNotNullExpressionValue(d5Var, "binding.panelCenter");
        ViewCompat.setOnApplyWindowInsetsListener(d5Var.a, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.home.WidgetHome$setPanelWindowInsetsListeners$2
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetHomeBinding binding;
                WidgetHomeBinding binding2;
                WidgetHomePanelNsfw widgetHomePanelNsfw;
                m.checkNotNullParameter(view, "<anonymous parameter 0>");
                m.checkNotNullParameter(windowInsetsCompat, "insets");
                WindowInsetsCompat build = new WindowInsetsCompat.Builder().setSystemWindowInsets(Insets.of(0, 0, 0, windowInsetsCompat.getSystemWindowInsetBottom())).build();
                m.checkNotNullExpressionValue(build, "WindowInsetsCompat.Build…        )\n      ).build()");
                binding = WidgetHome.this.getBinding();
                ViewCompat.dispatchApplyWindowInsets(binding.d.f100b.d, build);
                binding2 = WidgetHome.this.getBinding();
                ViewCompat.dispatchApplyWindowInsets(binding2.d.f100b.e, build);
                widgetHomePanelNsfw = WidgetHome.this.panelNsfw;
                if (widgetHomePanelNsfw != null) {
                    widgetHomePanelNsfw.dispatchApplyWindowInsets(windowInsetsCompat);
                }
                return windowInsetsCompat.consumeSystemWindowInsets();
            }
        });
        i5 i5Var = getBinding().f;
        m.checkNotNullExpressionValue(i5Var, "binding.panelRight");
        ViewCompat.setOnApplyWindowInsetsListener(i5Var.a, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.home.WidgetHome$setPanelWindowInsetsListeners$3
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetHomeBinding binding;
                m.checkNotNullParameter(view, "<anonymous parameter 0>");
                m.checkNotNullParameter(windowInsetsCompat, "insets");
                binding = WidgetHome.this.getBinding();
                FragmentContainerView fragmentContainerView = binding.f.c;
                m.checkNotNullExpressionValue(fragmentContainerView, "binding.panelRight.widgetConnectedList");
                fragmentContainerView.setPadding(fragmentContainerView.getPaddingLeft(), fragmentContainerView.getPaddingTop(), fragmentContainerView.getPaddingRight(), windowInsetsCompat.getSystemWindowInsetBottom());
                return windowInsetsCompat.consumeSystemWindowInsets();
            }
        });
    }

    private final void setupSmoothKeyboardReaction() {
        d5 d5Var = getBinding().d;
        m.checkNotNullExpressionValue(d5Var, "binding.panelCenter");
        RoundedRelativeLayout roundedRelativeLayout = d5Var.a;
        m.checkNotNullExpressionValue(roundedRelativeLayout, "binding.panelCenter.root");
        Iterator<View> it = ViewGroupKt.iterator(roundedRelativeLayout);
        while (it.hasNext()) {
            View next = it.next();
            if (!(next instanceof AppBarLayout) && !this.fixedPositionViewIds.contains(Integer.valueOf(next.getId()))) {
                SmoothKeyboardReactionHelper.INSTANCE.install(next);
            }
        }
    }

    private final void showChannelOnboardingSheet() {
        WidgetChannelOnboarding.Companion companion = WidgetChannelOnboarding.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.show(parentFragmentManager);
    }

    private final void showGuildEventUpsell(long j) {
        WidgetGuildScheduledEventUpsellBottomSheet.Companion companion = WidgetGuildScheduledEventUpsellBottomSheet.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.launch(parentFragmentManager, j);
    }

    private final void showPlaystationUpsell() {
        WidgetPlaystationIntegrationUpsellBottomSheet.Companion companion = WidgetPlaystationIntegrationUpsellBottomSheet.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.show(parentFragmentManager);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showSurvey(SurveyUtils.Survey survey) {
        StoreStream.Companion.getNotices().requestToShow(new StoreNotices.Notice(survey.getNoticeKey(), null, 0L, 5, true, null, 0L, false, 0L, new WidgetHome$showSurvey$1(survey), 486, null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showUrgentMessageDialog() {
        StoreStream.Companion.getNotices().requestToShow(new StoreNotices.Notice("URGENT_MESSAGE_DIALOG", null, 0L, 0, false, null, 0L, false, 0L, WidgetHome$showUrgentMessageDialog$1.INSTANCE, Opcodes.IF_ACMPNE, null));
    }

    private final void showWelcomeSheet(long j, Long l) {
        if (!StoreStream.Companion.getGuildWelcomeScreens().hasWelcomeScreenBeenSeen(j)) {
            WidgetGuildWelcomeSheet.Companion companion = WidgetGuildWelcomeSheet.Companion;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.show(parentFragmentManager, j, l);
        }
    }

    public static /* synthetic */ void showWelcomeSheet$default(WidgetHome widgetHome, long j, Long l, int i, Object obj) {
        if ((i & 2) != 0) {
            l = null;
        }
        widgetHome.showWelcomeSheet(j, l);
    }

    private final void unroundPanelCorners() {
        setPanelCorners(0.0f);
    }

    public final PanelLayout getPanelLayout() {
        HomePanelsLayout homePanelsLayout = getBinding().c;
        m.checkNotNullExpressionValue(homePanelsLayout, "binding.overlappingPanels");
        return homePanelsLayout;
    }

    public final Toolbar getToolbar() {
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            return appActivity.u;
        }
        return null;
    }

    public final TextView getUnreadCountView() {
        f5 f5Var = getBinding().d.f100b.c;
        m.checkNotNullExpressionValue(f5Var, "binding.panelCenter.widg…omePanelCenterChat.unread");
        TextView textView = f5Var.a;
        m.checkNotNullExpressionValue(textView, "binding.panelCenter.widg…nelCenterChat.unread.root");
        return textView;
    }

    public final void handleCenterPanelBack$app_productionGoogleRelease() {
        WidgetHomeModel widgetHomeModel$app_productionGoogleRelease = getViewModel().getWidgetHomeModel$app_productionGoogleRelease();
        Channel channel = null;
        StoreChannelsSelected.ResolvedSelectedChannel selectedChannel = widgetHomeModel$app_productionGoogleRelease != null ? widgetHomeModel$app_productionGoogleRelease.getSelectedChannel() : null;
        WidgetHomeModel widgetHomeModel$app_productionGoogleRelease2 = getViewModel().getWidgetHomeModel$app_productionGoogleRelease();
        if (widgetHomeModel$app_productionGoogleRelease2 != null) {
            channel = widgetHomeModel$app_productionGoogleRelease2.getSelectedVoiceChannel();
        }
        if (selectedChannel instanceof StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft) {
            ChannelSelector.Companion.getInstance().dismissCreateThread();
            return;
        }
        boolean z2 = selectedChannel instanceof StoreChannelsSelected.ResolvedSelectedChannel.Channel;
        if (z2) {
            StoreChannelsSelected.ResolvedSelectedChannel.Channel channel2 = (StoreChannelsSelected.ResolvedSelectedChannel.Channel) selectedChannel;
            if (channel2.getPeekParent() != null) {
                ChannelSelector.selectChannel$default(ChannelSelector.Companion.getInstance(), channel2.getChannel().f(), channel2.getPeekParent().longValue(), null, null, 12, null);
                return;
            }
        }
        if (z2) {
            StoreChannelsSelected.ResolvedSelectedChannel.Channel channel3 = (StoreChannelsSelected.ResolvedSelectedChannel.Channel) selectedChannel;
            if (ChannelUtils.E(channel3.getChannel())) {
                if (channel == null || channel.h() == 0 || ChannelUtils.x(channel)) {
                    WidgetCallPreviewFullscreen.Companion.launch$default(WidgetCallPreviewFullscreen.Companion, requireContext(), channel3.getChannel().h(), null, 4, null);
                    requireAppActivity().overridePendingTransition(R.anim.activity_slide_horizontal_close_in, R.anim.activity_slide_horizontal_close_out);
                    return;
                }
                WidgetCallFullscreen.Companion.launch$default(WidgetCallFullscreen.Companion, requireContext(), channel3.getChannel().h(), false, null, null, 28, null);
                requireAppActivity().overridePendingTransition(R.anim.activity_slide_horizontal_close_in, R.anim.activity_slide_horizontal_close_out);
                return;
            }
        }
        getBinding().c.openStartPanel();
    }

    public final void lockCloseRightPanel(boolean z2) {
        OverlappingPanelsLayout.LockState lockState;
        if (z2) {
            lockState = OverlappingPanelsLayout.LockState.CLOSE;
        } else {
            lockState = OverlappingPanelsLayout.LockState.UNLOCKED;
        }
        getBinding().c.setEndPanelLockState(lockState);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b.a.o.a aVar = b.a.o.a.f247b;
        WidgetHome$onCreate$1 widgetHome$onCreate$1 = new WidgetHome$onCreate$1(this);
        m.checkParameterIsNotNull(widgetHome$onCreate$1, "provider");
        b.a.o.a.a = widgetHome$onCreate$1;
    }

    @Override // b.a.o.b.a
    public void onGestureRegionsUpdate(List<Rect> list) {
        m.checkNotNullParameter(list, "gestureRegions");
        getBinding().c.setChildGestureRegions(list);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        b a = b.C0038b.a();
        m.checkParameterIsNotNull(this, "gestureRegionsListener");
        a.l.remove(this);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        b a = b.C0038b.a();
        m.checkParameterIsNotNull(this, "gestureRegionsListener");
        onGestureRegionsUpdate(u.toList(a.j.values()));
        a.l.add(this);
    }

    @Override // com.discord.widgets.tabs.OnTabSelectedListener
    public void onTabSelected() {
        WidgetHomeModel widgetHomeModel$app_productionGoogleRelease = getViewModel().getWidgetHomeModel$app_productionGoogleRelease();
        if (widgetHomeModel$app_productionGoogleRelease != null) {
            WidgetHomeHeaderManager widgetHomeHeaderManager = WidgetHomeHeaderManager.INSTANCE;
            WidgetHomeBinding binding = getBinding();
            m.checkNotNullExpressionValue(binding, "binding");
            widgetHomeHeaderManager.configure(this, widgetHomeModel$app_productionGoogleRelease, binding);
        }
        if (getBinding().c.getSelectedPanel() == OverlappingPanelsLayout.Panel.CENTER) {
            setActionBarTitleAccessibilityViewFocused();
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        boolean z2 = TextUtils.getLayoutDirectionFromLocale(new LocaleManager().getPrimaryLocale(requireContext())) == 0;
        RoundedRelativeLayout roundedRelativeLayout = getBinding().f.f132b;
        m.checkNotNullExpressionValue(roundedRelativeLayout, "binding.panelRight.mainPanelRightRoundedContainer");
        ViewGroup.LayoutParams layoutParams = roundedRelativeLayout.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        int dpToPixels = DimenUtils.dpToPixels(8);
        int i = z2 ? marginLayoutParams.leftMargin : dpToPixels;
        if (!z2) {
            dpToPixels = marginLayoutParams.rightMargin;
        }
        marginLayoutParams.setMargins(i, marginLayoutParams.topMargin, dpToPixels, marginLayoutParams.bottomMargin);
        RoundedRelativeLayout roundedRelativeLayout2 = getBinding().f.f132b;
        m.checkNotNullExpressionValue(roundedRelativeLayout2, "binding.panelRight.mainPanelRightRoundedContainer");
        roundedRelativeLayout2.setLayoutParams(marginLayoutParams);
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof WidgetTabsHost)) {
            parentFragment = null;
        }
        WidgetTabsHost widgetTabsHost = (WidgetTabsHost) parentFragment;
        if (widgetTabsHost != null) {
            widgetTabsHost.registerTabSelectionListener(NavigationTab.HOME, this);
        }
        this.panelNsfw = new WidgetHomePanelNsfw(this);
        WidgetHomeBinding binding = getBinding();
        m.checkNotNullExpressionValue(binding, "binding");
        this.panelLoading = new WidgetHomePanelLoading(binding);
        Function1<? super View, Unit> function1 = this.onGuildListAddHintCreate;
        TextView textView = getBinding().e.f118b;
        m.checkNotNullExpressionValue(textView, "binding.panelLeft.guildListAddHint");
        function1.invoke(textView);
        AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.home.WidgetHome$onViewBound$1
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                boolean handleBackPressed;
                handleBackPressed = WidgetHome.this.handleBackPressed();
                return Boolean.valueOf(handleBackPressed);
            }
        }, 0, 2, null);
        WidgetNoticeNuxSamsungLink.Companion.enqueue(requireContext(), ClockFactory.get());
        setPanelWindowInsetsListeners();
        setupSmoothKeyboardReaction();
        Observable<StoreNux.NuxState> y2 = StoreStream.Companion.getNux().getNuxState().x(new j0.k.b<StoreNux.NuxState, Boolean>() { // from class: com.discord.widgets.home.WidgetHome$onViewBound$2
            public final Boolean call(StoreNux.NuxState nuxState) {
                FragmentActivity activity = WidgetHome.this.e();
                if (!(activity instanceof AppActivity)) {
                    activity = null;
                }
                AppActivity appActivity = (AppActivity) activity;
                boolean z3 = true;
                if (appActivity == null || !appActivity.h(a0.getOrCreateKotlinClass(WidgetTabsHost.class))) {
                    z3 = false;
                }
                return Boolean.valueOf(z3);
            }
        }).y();
        m.checkNotNullExpressionValue(y2, "StoreStream\n        .get…       }\n        .first()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout$default(y2, 0L, false, 1, null), this, null, 2, null), WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBound$3(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Serializable serializableExtra = getMostRecentIntent().getSerializableExtra("com.discord.intent.extra.EXTRA_HOME_CONFIG");
        if (!(serializableExtra instanceof HomeConfig)) {
            serializableExtra = null;
        }
        handleHomeConfig((HomeConfig) serializableExtra);
        WidgetHomePanelLoading widgetHomePanelLoading = this.panelLoading;
        if (widgetHomePanelLoading != null) {
            widgetHomePanelLoading.configure(this);
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBoundOrOnResume$2(this));
        WidgetHomeModel.Companion companion = WidgetHomeModel.Companion;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.get(), this, null, 2, null), WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBoundOrOnResume$3(this));
        Observable r = ObservableExtensionsKt.ui$default(companion.get(), this, null, 2, null).r(WidgetHome$onViewBoundOrOnResume$4.INSTANCE);
        m.checkNotNullExpressionValue(r, "WidgetHomeModel\n        …lId == model2.channelId }");
        ObservableExtensionsKt.appSubscribe(r, WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBoundOrOnResume$5(this));
        AccessibilityDetectionNavigator.INSTANCE.enqueueNoticeWhenEnabled(this);
        StoreStream.Companion companion2 = StoreStream.Companion;
        configureNavigationDrawerAction(companion2.getNavigation());
        configureOverlappingPanels();
        Observable F = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout$default(SurveyUtils.INSTANCE.getSurveyToShow(), 0L, false, 3, null), this, null, 2, null).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable x2 = F.x(WidgetHome$onViewBoundOrOnResume$6.INSTANCE);
        m.checkNotNullExpressionValue(x2, "SurveyUtils\n        .get…SurveyUtils.Survey.None }");
        ObservableExtensionsKt.appSubscribe(x2, WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBoundOrOnResume$7(this));
        Observable<StoreNux.NuxState> x3 = companion2.getNux().getNuxState().x(WidgetHome$onViewBoundOrOnResume$8.INSTANCE);
        m.checkNotNullExpressionValue(x3, "StoreStream\n        .get… .filter { it.firstOpen }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout$default(x3, 0L, false, 1, null), this, null, 2, null), WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBoundOrOnResume$9(this));
        Observable x4 = StoreUser.observeMe$default(companion2.getUsers(), false, 1, null).x(WidgetHome$onViewBoundOrOnResume$10.INSTANCE);
        m.checkNotNullExpressionValue(x4, "StoreStream\n        .get…hasUnreadUrgentMessages }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(x4, this, null, 2, null), WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBoundOrOnResume$11(this));
        configureLeftPanel();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(this.globalStatusIndicatorStateObserver.observeState(), this, null, 2, null), WidgetHome.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHome$onViewBoundOrOnResume$12(this));
    }

    public final void setOnGuildListAddHintCreate(Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(function1, "onGuildListAddHintCreate");
        this.onGuildListAddHintCreate = function1;
    }
}
