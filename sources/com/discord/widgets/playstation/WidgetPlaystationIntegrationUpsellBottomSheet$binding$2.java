package com.discord.widgets.playstation;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import com.discord.databinding.WidgetPlaystationUpsellBottomSheetBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetPlaystationIntegrationUpsellBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetPlaystationUpsellBottomSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetPlaystationUpsellBottomSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetPlaystationIntegrationUpsellBottomSheet$binding$2 extends k implements Function1<View, WidgetPlaystationUpsellBottomSheetBinding> {
    public static final WidgetPlaystationIntegrationUpsellBottomSheet$binding$2 INSTANCE = new WidgetPlaystationIntegrationUpsellBottomSheet$binding$2();

    public WidgetPlaystationIntegrationUpsellBottomSheet$binding$2() {
        super(1, WidgetPlaystationUpsellBottomSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetPlaystationUpsellBottomSheetBinding;", 0);
    }

    public final WidgetPlaystationUpsellBottomSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.cancel_button;
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.cancel_button);
        if (materialButton != null) {
            i = R.id.confirm_button;
            MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.confirm_button);
            if (materialButton2 != null) {
                i = R.id.upsell_body;
                TextView textView = (TextView) view.findViewById(R.id.upsell_body);
                if (textView != null) {
                    i = R.id.upsell_header;
                    TextView textView2 = (TextView) view.findViewById(R.id.upsell_header);
                    if (textView2 != null) {
                        i = R.id.upsell_image;
                        ImageView imageView = (ImageView) view.findViewById(R.id.upsell_image);
                        if (imageView != null) {
                            return new WidgetPlaystationUpsellBottomSheetBinding((NestedScrollView) view, materialButton, materialButton2, textView, textView2, imageView);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
