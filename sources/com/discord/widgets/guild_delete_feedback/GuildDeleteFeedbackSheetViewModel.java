package com.discord.widgets.guild_delete_feedback;

import andhook.lib.HookHelper;
import com.discord.app.AppViewModel;
import com.discord.widgets.feedback.FeedbackSheetViewModel;
import com.discord.widgets.voice.feedback.FeedbackIssue;
import com.discord.widgets.voice.feedback.FeedbackRating;
import com.discord.widgets.voice.feedback.PendingFeedback;
import d0.t.m;
import d0.t.n;
import d0.t.u;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: GuildDeleteFeedbackSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 !2\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001!B\u0013\u0012\n\u0010\u001e\u001a\u00060\u001cj\u0002`\u001d¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0005\u0010\u0006J#\u0010\u000b\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u00072\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\r\u0010\u0006J\u0015\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R:\u0010\u0017\u001a&\u0012\f\u0012\n \u0016*\u0004\u0018\u00010\u000f0\u000f \u0016*\u0012\u0012\f\u0012\n \u0016*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u00150\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006\""}, d2 = {"Lcom/discord/widgets/guild_delete_feedback/GuildDeleteFeedbackSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel;", "", "onCleared", "()V", "Lcom/discord/widgets/voice/feedback/FeedbackIssue;", "feedbackIssue", "", "reasonDescription", "selectIssue", "(Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;)V", "submitForm", "Lrx/Observable;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Lcom/discord/widgets/voice/feedback/PendingFeedback$GuildDeleteFeedback;", "pendingFeedback", "Lcom/discord/widgets/voice/feedback/PendingFeedback$GuildDeleteFeedback;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "", "submitted", "Z", "", "Lcom/discord/primitives/GuildId;", "guildId", HookHelper.constructorName, "(J)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildDeleteFeedbackSheetViewModel extends AppViewModel<FeedbackSheetViewModel.ViewState> implements FeedbackSheetViewModel {
    public static final Companion Companion = new Companion(null);
    private static final List<FeedbackIssue> REASONS = n.listOf((Object[]) new FeedbackIssue[]{FeedbackIssue.GUILD_DELETE_TOO_HARD, FeedbackIssue.GUILD_DELETE_TEST, FeedbackIssue.GUILD_DELETE_ACCIDENT, FeedbackIssue.GUILD_DELETE_TEMPLATE, FeedbackIssue.GUILD_DELETE_LONELY, FeedbackIssue.GUILD_DELETE_INACTIVE});
    private final PublishSubject<FeedbackSheetViewModel.Event> eventSubject = PublishSubject.k0();
    private PendingFeedback.GuildDeleteFeedback pendingFeedback;
    private boolean submitted;

    /* compiled from: GuildDeleteFeedbackSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/guild_delete_feedback/GuildDeleteFeedbackSheetViewModel$Companion;", "", "", "Lcom/discord/widgets/voice/feedback/FeedbackIssue;", "REASONS", "Ljava/util/List;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public GuildDeleteFeedbackSheetViewModel(long j) {
        super(new FeedbackSheetViewModel.ViewState(FeedbackRating.NO_RESPONSE, u.plus((Collection<? extends FeedbackIssue>) m.shuffled(REASONS), FeedbackIssue.GUILD_DELETE_OTHER), R.string.guild_delete_feedback_header, null, R.string.guild_delete_feedback_body, 8, null));
        this.pendingFeedback = new PendingFeedback.GuildDeleteFeedback(j, null, null, 6, null);
    }

    @Override // com.discord.widgets.feedback.FeedbackSheetViewModel
    public Observable<FeedbackSheetViewModel.Event> observeEvents() {
        PublishSubject<FeedbackSheetViewModel.Event> publishSubject = this.eventSubject;
        d0.z.d.m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @Override // com.discord.app.AppViewModel, androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        submitForm();
    }

    @Override // com.discord.widgets.feedback.FeedbackSheetViewModel
    public void selectIssue(FeedbackIssue feedbackIssue, String str) {
        PendingFeedback.GuildDeleteFeedback copy$default = PendingFeedback.GuildDeleteFeedback.copy$default(this.pendingFeedback, 0L, feedbackIssue, null, 5, null);
        this.pendingFeedback = copy$default;
        if (feedbackIssue == FeedbackIssue.GUILD_DELETE_OTHER) {
            PublishSubject<FeedbackSheetViewModel.Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new FeedbackSheetViewModel.Event.NavigateToIssueDetails(copy$default, false));
            return;
        }
        submitForm();
    }

    @Override // com.discord.widgets.feedback.FeedbackSheetViewModel
    public void selectRating(FeedbackRating feedbackRating) {
        d0.z.d.m.checkNotNullParameter(feedbackRating, "feedbackRating");
        FeedbackSheetViewModel.DefaultImpls.selectRating(this, feedbackRating);
    }

    @Override // com.discord.widgets.feedback.FeedbackSheetViewModel
    public void submitForm() {
        if (!this.submitted) {
            boolean z2 = true;
            this.submitted = true;
            if (this.pendingFeedback.getReason() == null) {
                z2 = false;
            }
            if (z2) {
                new GuildDeleteFeedbackSubmitter(this.pendingFeedback).submit(null);
            } else {
                new GuildDeleteFeedbackSubmitter(this.pendingFeedback).skip();
            }
            PublishSubject<FeedbackSheetViewModel.Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new FeedbackSheetViewModel.Event.Submitted(z2));
        }
    }
}
