package com.discord.widgets.roles;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import b.a.k.b;
import com.discord.api.role.GuildRole;
import com.discord.databinding.RoleIconViewBinding;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreStream;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.guilds.RoleIconUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.textprocessing.AstRenderer;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.utilities.view.ToastManager;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.z.d.m;
import java.util.Collections;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: RoleIconView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010!\u001a\u00020 ¢\u0006\u0004\b\"\u0010#J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\n\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0013\u0010\r\u001a\u00020\f*\u00020\u0004H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0013\u0010\u000f\u001a\u00020\f*\u00020\u0004H\u0002¢\u0006\u0004\b\u000f\u0010\u000eJ\u0013\u0010\u0010\u001a\u00020\f*\u00020\u0004H\u0002¢\u0006\u0004\b\u0010\u0010\u000eJ'\u0010\u0014\u001a\u00020\u00062\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u000e\u0010\u0013\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`\u0012¢\u0006\u0004\b\u0014\u0010\u0015J-\u0010\u0014\u001a\u00020\u00062\u000e\u0010\u0017\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`\u00162\u000e\u0010\u0013\u001a\n\u0018\u00010\u0011j\u0004\u0018\u0001`\u0012¢\u0006\u0004\b\u0014\u0010\u0018J\u0017\u0010\u0019\u001a\u00020\u00062\b\u0010\t\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0019\u0010\u000bJ\u0017\u0010\u0019\u001a\u00020\u00062\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a¢\u0006\u0004\b\u0019\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001f¨\u0006$"}, d2 = {"Lcom/discord/widgets/roles/RoleIconView;", "Landroid/widget/FrameLayout;", "Landroid/content/Context;", "context", "Lcom/discord/api/role/GuildRole;", "role", "", "showRoleIconToast", "(Landroid/content/Context;Lcom/discord/api/role/GuildRole;)V", "guildRole", "setIcon", "(Lcom/discord/api/role/GuildRole;)V", "", "hasIcon", "(Lcom/discord/api/role/GuildRole;)Z", "hasUnicodeEmoji", "hasIconOrUnicodeEmoji", "", "Lcom/discord/primitives/GuildId;", "guildId", "setRole", "(Lcom/discord/api/role/GuildRole;Ljava/lang/Long;)V", "Lcom/discord/primitives/RoleId;", "roleId", "(Ljava/lang/Long;Ljava/lang/Long;)V", "setRoleIconPreview", "", "icon", "(Ljava/lang/String;)V", "Lcom/discord/databinding/RoleIconViewBinding;", "binding", "Lcom/discord/databinding/RoleIconViewBinding;", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RoleIconView extends FrameLayout {
    private final RoleIconViewBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RoleIconView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(attributeSet, "attrs");
        LayoutInflater.from(context).inflate(R.layout.role_icon_view, this);
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.role_icon_iv);
        if (simpleDraweeView != null) {
            RoleIconViewBinding roleIconViewBinding = new RoleIconViewBinding(this, simpleDraweeView);
            m.checkNotNullExpressionValue(roleIconViewBinding, "RoleIconViewBinding.infl…ater.from(context), this)");
            this.binding = roleIconViewBinding;
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(R.id.role_icon_iv)));
    }

    private final boolean hasIcon(GuildRole guildRole) {
        if (guildRole.d() != null) {
            String d = guildRole.d();
            m.checkNotNull(d);
            if (d.length() > 0) {
                return true;
            }
        }
        return false;
    }

    private final boolean hasIconOrUnicodeEmoji(GuildRole guildRole) {
        return hasIcon(guildRole) || hasUnicodeEmoji(guildRole);
    }

    private final boolean hasUnicodeEmoji(GuildRole guildRole) {
        if (guildRole.k() != null) {
            String k = guildRole.k();
            m.checkNotNull(k);
            if (k.length() > 0) {
                return true;
            }
        }
        return false;
    }

    private final void setIcon(GuildRole guildRole) {
        if (hasIcon(guildRole)) {
            SimpleDraweeView simpleDraweeView = this.binding.f2122b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.roleIconIv");
            IconUtils.setIcon$default(simpleDraweeView, guildRole, (int) R.dimen.role_icon_size, (MGImages.ChangeDetector) null, 8, (Object) null);
        } else if (hasUnicodeEmoji(guildRole)) {
            ModelEmojiUnicode modelEmojiUnicode = StoreStream.Companion.getEmojis().getUnicodeEmojiSurrogateMap().get(guildRole.k());
            String imageUri = ModelEmojiUnicode.getImageUri(modelEmojiUnicode != null ? modelEmojiUnicode.getCodePoints() : null, getContext());
            SimpleDraweeView simpleDraweeView2 = this.binding.f2122b;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.roleIconIv");
            IconUtils.setIcon$default(simpleDraweeView2, imageUri, 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showRoleIconToast(final Context context, GuildRole guildRole) {
        EmojiNode emojiNode;
        CharSequence b2;
        ModelEmojiUnicode modelEmojiUnicode;
        if (hasIcon(guildRole)) {
            emojiNode = new EmojiNode(guildRole.g(), new RoleIconView$showRoleIconToast$1(guildRole), new EmojiNode.EmojiIdAndType.Unicode(""), DimenUtils.dpToPixels(24), DimenUtils.dpToPixels(24));
        } else {
            emojiNode = (!hasUnicodeEmoji(guildRole) || (modelEmojiUnicode = StoreStream.Companion.getEmojis().getUnicodeEmojiSurrogateMap().get(guildRole.k())) == null) ? null : EmojiNode.Companion.from(modelEmojiUnicode, DimenUtils.dpToPixels(24));
        }
        if (emojiNode != null) {
            EmojiNode.RenderContext roleIconView$showRoleIconToast$renderContext$1 = new EmojiNode.RenderContext(context) { // from class: com.discord.widgets.roles.RoleIconView$showRoleIconToast$renderContext$1
                public final /* synthetic */ Context $context;
                @SuppressLint({"StaticFieldLeak"})
                private final Context context;
                private final boolean isAnimationEnabled;

                {
                    this.$context = context;
                    this.context = context;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public Context getContext() {
                    return this.context;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public boolean isAnimationEnabled() {
                    return this.isAnimationEnabled;
                }

                @Override // com.discord.utilities.textprocessing.node.EmojiNode.RenderContext
                public void onEmojiClicked(EmojiNode.EmojiIdAndType emojiIdAndType) {
                    m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
                    EmojiNode.RenderContext.DefaultImpls.onEmojiClicked(this, emojiIdAndType);
                }
            };
            Set singleton = Collections.singleton(emojiNode);
            m.checkNotNullExpressionValue(singleton, "Collections.singleton(iconNode)");
            DraweeSpanStringBuilder render = AstRenderer.render(singleton, roleIconView$showRoleIconToast$renderContext$1);
            ToastManager toastManager = new ToastManager();
            SpannableStringBuilder append = render.append((CharSequence) " ");
            b2 = b.b(context, R.string.role_icon_toast_message, new Object[]{guildRole.g()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            ToastManager.show$default(toastManager, context, append.append(b2), 0, 4, (Object) null);
        }
    }

    public final void setRole(final GuildRole guildRole, Long l) {
        CharSequence b2;
        Guild guild = l != null ? StoreStream.Companion.getGuilds().getGuild(l.longValue()) : null;
        if (guildRole == null || !hasIconOrUnicodeEmoji(guildRole) || guild == null || !RoleIconUtils.INSTANCE.canUseRoleIcons(guild, guildRole)) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        setIcon(guildRole);
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        b2 = b.b(context, R.string.role_icon_alt_text, new Object[]{guildRole.g()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        setContentDescription(b2);
        setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.roles.RoleIconView$setRole$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RoleIconView roleIconView = RoleIconView.this;
                Context context2 = roleIconView.getContext();
                m.checkNotNullExpressionValue(context2, "context");
                roleIconView.showRoleIconToast(context2, guildRole);
            }
        });
    }

    public final void setRoleIconPreview(GuildRole guildRole) {
        if (guildRole == null || !hasIconOrUnicodeEmoji(guildRole)) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        setIcon(guildRole);
    }

    public final void setRoleIconPreview(String str) {
        if (str == null || t.isBlank(str)) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        SimpleDraweeView simpleDraweeView = this.binding.f2122b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.roleIconIv");
        IconUtils.setIcon$default(simpleDraweeView, str, (int) R.dimen.role_icon_size, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
    }

    public final void setRole(Long l, Long l2) {
        setRole(GuildUtilsKt.getGuildRole(l), l2);
    }
}
