package com.discord.widgets.roles;

import android.content.Context;
import com.discord.api.role.GuildRole;
import com.discord.utilities.icon.IconUtils;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
/* compiled from: RoleIconView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "<anonymous parameter 0>", "", "<anonymous parameter 1>", "Landroid/content/Context;", "<anonymous parameter 2>", "", "invoke", "(ZILandroid/content/Context;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RoleIconView$showRoleIconToast$1 extends o implements Function3<Boolean, Integer, Context, String> {
    public final /* synthetic */ GuildRole $role;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public RoleIconView$showRoleIconToast$1(GuildRole guildRole) {
        super(3);
        this.$role = guildRole;
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ String invoke(Boolean bool, Integer num, Context context) {
        return invoke(bool.booleanValue(), num.intValue(), context);
    }

    public final String invoke(boolean z2, int i, Context context) {
        m.checkNotNullParameter(context, "<anonymous parameter 2>");
        IconUtils iconUtils = IconUtils.INSTANCE;
        long id2 = this.$role.getId();
        String d = this.$role.d();
        m.checkNotNull(d);
        return IconUtils.getRoleIconUrl$default(iconUtils, id2, d, null, 4, null);
    }
}
