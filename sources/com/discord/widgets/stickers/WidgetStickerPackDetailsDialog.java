package com.discord.widgets.stickers;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.StickerPackDetailsDialogBinding;
import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.stores.StoreStickers;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stickers.StickerUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetStickerPackDetailsDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u0007¢\u0006\u0004\b\u001c\u0010\u0011J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\fH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u001e\u0010\u0014\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001d\u0010\u001b\u001a\u00020\u00168B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001a¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog;", "Lcom/discord/app/AppDialog;", "Lcom/discord/stores/StoreStickers$StickerPackState;", "stickerPackState", "", "configureUI", "(Lcom/discord/stores/StoreStickers$StickerPackState;)V", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/app/Dialog;", "onCreateDialog", "(Landroid/os/Bundle;)Landroid/app/Dialog;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "", "Lcom/discord/primitives/StickerPackId;", "stickerPackId", "Ljava/lang/Long;", "Lcom/discord/databinding/StickerPackDetailsDialogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/StickerPackDetailsDialogBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerPackDetailsDialog extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetStickerPackDetailsDialog.class, "binding", "getBinding()Lcom/discord/databinding/StickerPackDetailsDialogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_Y_POSITION = "EXTRA_Y_POSITION";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetStickerPackDetailsDialog$binding$2.INSTANCE, null, 2, null);
    private Long stickerPackId;

    /* compiled from: WidgetStickerPackDetailsDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J-\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/stickers/WidgetStickerPackDetailsDialog$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/StickerPackId;", "stickerPackId", "", "y", "", "show", "(Landroidx/fragment/app/FragmentManager;JLjava/lang/Integer;)V", "", WidgetStickerPackDetailsDialog.EXTRA_Y_POSITION, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, FragmentManager fragmentManager, long j, Integer num, int i, Object obj) {
            if ((i & 4) != 0) {
                num = null;
            }
            companion.show(fragmentManager, j, num);
        }

        public final void show(FragmentManager fragmentManager, long j, Integer num) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            Bundle bundle = new Bundle();
            bundle.putLong("com.discord.intent.EXTRA_STICKER_PACK_ID", j);
            if (num != null) {
                num.intValue();
                bundle.putInt(WidgetStickerPackDetailsDialog.EXTRA_Y_POSITION, num.intValue());
            }
            WidgetStickerPackDetailsDialog widgetStickerPackDetailsDialog = new WidgetStickerPackDetailsDialog();
            widgetStickerPackDetailsDialog.setArguments(bundle);
            widgetStickerPackDetailsDialog.show(fragmentManager, WidgetStickerPackDetailsDialog.class.getSimpleName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetStickerPackDetailsDialog() {
        super(R.layout.sticker_pack_details_dialog);
        setStyle(1, R.style.AppTheme_Dialog_Popup_Tooltip);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @SuppressLint({"SetTextI18n"})
    public final void configureUI(StoreStickers.StickerPackState stickerPackState) {
        CharSequence e;
        CharSequence e2;
        if (stickerPackState instanceof StoreStickers.StickerPackState.Loaded) {
            ModelStickerPack stickerPack = ((StoreStickers.StickerPackState.Loaded) stickerPackState).getStickerPack();
            TextView textView = getBinding().e;
            m.checkNotNullExpressionValue(textView, "binding.stickerPackDetailsPack");
            e = b.e(this, R.string.sticker_picker_pack_details, new Object[]{stickerPack.getName()}, (r4 & 4) != 0 ? b.a.j : null);
            textView.setText(e);
            TextView textView2 = getBinding().f2135b;
            m.checkNotNullExpressionValue(textView2, "binding.stickerPackDetailsAnimated");
            int i = 8;
            textView2.setVisibility(stickerPack.isAnimatedPack() ? 0 : 8);
            TextView textView3 = getBinding().f2135b;
            m.checkNotNullExpressionValue(textView3, "binding.stickerPackDetailsAnimated");
            textView3.setText("・ " + getString(R.string.sticker_picker_pack_details_animated));
            TextView textView4 = getBinding().f;
            m.checkNotNullExpressionValue(textView4, "binding.stickerPackDetailsPremium");
            textView4.setVisibility(stickerPack.isPremiumPack() ? 0 : 8);
            TextView textView5 = getBinding().f;
            m.checkNotNullExpressionValue(textView5, "binding.stickerPackDetailsPremium");
            textView5.setText("・ " + getString(R.string.sticker_picker_pack_details_premium));
            TextView textView6 = getBinding().d;
            m.checkNotNullExpressionValue(textView6, "binding.stickerPackDetailsLimited");
            if (stickerPack.isLimitedPack()) {
                i = 0;
            }
            textView6.setVisibility(i);
            TextView textView7 = getBinding().d;
            m.checkNotNullExpressionValue(textView7, "binding.stickerPackDetailsLimited");
            StringBuilder sb = new StringBuilder();
            sb.append("・ ");
            StickerUtils stickerUtils = StickerUtils.INSTANCE;
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            e2 = b.e(this, R.string.sticker_picker_pack_details_limited_time_left, new Object[]{stickerUtils.getLimitedTimeLeftString(requireContext, stickerPack.getStoreListing())}, (r4 & 4) != 0 ? b.a.j : null);
            sb.append(e2);
            textView7.setText(sb.toString());
        }
    }

    private final StickerPackDetailsDialogBinding getBinding() {
        return (StickerPackDetailsDialogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        WindowManager.LayoutParams attributes;
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setGravity(81);
        }
        Bundle arguments = getArguments();
        Integer valueOf = arguments != null ? Integer.valueOf(arguments.getInt(EXTRA_Y_POSITION, 0)) : null;
        Window window2 = onCreateDialog.getWindow();
        if (!(window2 == null || (attributes = window2.getAttributes()) == null)) {
            if (valueOf != null && valueOf.intValue() == 0) {
                Context context = onCreateDialog.getContext();
                m.checkNotNullExpressionValue(context, "context");
                valueOf = Integer.valueOf((int) (context.getResources().getDimension(R.dimen.default_keyboard_height) / 2));
            }
            attributes.y = valueOf.intValue();
        }
        return onCreateDialog;
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.WidgetStickerPackDetailsDialog$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetStickerPackDetailsDialog.this.dismiss();
            }
        });
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Bundle arguments = getArguments();
        Long valueOf = arguments != null ? Long.valueOf(arguments.getLong("com.discord.intent.EXTRA_STICKER_PACK_ID")) : null;
        this.stickerPackId = valueOf;
        if (valueOf != null) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreStream.Companion.getStickers().observeStickerPack(valueOf.longValue()), this, null, 2, null), WidgetStickerPackDetailsDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetStickerPackDetailsDialog$onViewBoundOrOnResume$1(this));
        }
    }
}
