package com.discord.widgets.stickers;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetUnknownStickerSheetBinding;
import com.discord.views.sticker.StickerView;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetUnknownStickerSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetUnknownStickerSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetUnknownStickerSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetUnknownStickerSheet$binding$2 extends k implements Function1<View, WidgetUnknownStickerSheetBinding> {
    public static final WidgetUnknownStickerSheet$binding$2 INSTANCE = new WidgetUnknownStickerSheet$binding$2();

    public WidgetUnknownStickerSheet$binding$2() {
        super(1, WidgetUnknownStickerSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetUnknownStickerSheetBinding;", 0);
    }

    public final WidgetUnknownStickerSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.unknown_sticker_sheet_sticker;
        StickerView stickerView = (StickerView) view.findViewById(R.id.unknown_sticker_sheet_sticker);
        if (stickerView != null) {
            i = R.id.unknown_sticker_sheet_sticker_name;
            TextView textView = (TextView) view.findViewById(R.id.unknown_sticker_sheet_sticker_name);
            if (textView != null) {
                return new WidgetUnknownStickerSheetBinding((LinearLayout) view, stickerView, textView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
