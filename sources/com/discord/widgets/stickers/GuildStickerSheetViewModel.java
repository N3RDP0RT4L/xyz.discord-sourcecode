package com.discord.widgets.stickers;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import com.discord.api.guild.GuildFeature;
import com.discord.api.premium.PremiumTier;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreUser;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guilds.join.GuildJoinHelperKt;
import com.discord.widgets.stickers.GuildStickerSheetViewModel;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.Map;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import retrofit2.HttpException;
import rx.Observable;
/* compiled from: GuildStickerSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001f2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001f !BG\u0012\u0006\u0010\u0015\u001a\u00020\u0014\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u0018\u001a\u00020\u0017\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0019\u0012\u000e\b\u0002\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00030\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001d\u0010\f\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\""}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$StoreState;)V", "Lcom/discord/models/guild/Guild;", "guild", "Landroidx/fragment/app/Fragment;", "fragment", "joinGuild", "(Lcom/discord/models/guild/Guild;Landroidx/fragment/app/Fragment;)V", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/api/sticker/Sticker;", "sticker", "Lcom/discord/api/sticker/Sticker;", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Lcom/discord/api/sticker/Sticker;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreGuilds;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildStickerSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final RestAPI restAPI;
    private final Sticker sticker;
    private final StoreUser storeUser;

    /* compiled from: GuildStickerSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.stickers.GuildStickerSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GuildStickerSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GuildStickerSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u0015B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J=\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ-\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion;", "", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/api/sticker/Sticker;", "sticker", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lrx/Observable;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$StoreState;", "observeStoreState", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/api/sticker/Sticker;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "getGuildForGuildSticker", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/api/sticker/Sticker;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;", HookHelper.constructorName, "()V", "GuildStickerGuildInfo", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: GuildStickerSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\b\tB\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0004\u0082\u0001\u0002\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "", "", "isPublic", "()Z", "isUserInGuild", HookHelper.constructorName, "()V", "Known", "Unknown", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo$Known;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo$Unknown;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static abstract class GuildStickerGuildInfo {

            /* compiled from: GuildStickerSheetViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ:\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00052\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0019\u001a\u00020\u00052\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001c\u0010\u000e\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u000e\u0010\u0007R\u001c\u0010\r\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\r\u0010\u0007R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004¨\u0006\""}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo$Known;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "component2", "()Z", "component3", "", "component4", "()Ljava/lang/Integer;", "guild", "isPublic", "isUserInGuild", "approximateOnline", "copy", "(Lcom/discord/models/guild/Guild;ZZLjava/lang/Integer;)Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo$Known;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/Integer;", "getApproximateOnline", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;ZZLjava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Known extends GuildStickerGuildInfo {
                private final Integer approximateOnline;
                private final Guild guild;
                private final boolean isPublic;
                private final boolean isUserInGuild;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public Known(Guild guild, boolean z2, boolean z3, Integer num) {
                    super(null);
                    m.checkNotNullParameter(guild, "guild");
                    this.guild = guild;
                    this.isPublic = z2;
                    this.isUserInGuild = z3;
                    this.approximateOnline = num;
                }

                public static /* synthetic */ Known copy$default(Known known, Guild guild, boolean z2, boolean z3, Integer num, int i, Object obj) {
                    if ((i & 1) != 0) {
                        guild = known.guild;
                    }
                    if ((i & 2) != 0) {
                        z2 = known.isPublic();
                    }
                    if ((i & 4) != 0) {
                        z3 = known.isUserInGuild();
                    }
                    if ((i & 8) != 0) {
                        num = known.approximateOnline;
                    }
                    return known.copy(guild, z2, z3, num);
                }

                public final Guild component1() {
                    return this.guild;
                }

                public final boolean component2() {
                    return isPublic();
                }

                public final boolean component3() {
                    return isUserInGuild();
                }

                public final Integer component4() {
                    return this.approximateOnline;
                }

                public final Known copy(Guild guild, boolean z2, boolean z3, Integer num) {
                    m.checkNotNullParameter(guild, "guild");
                    return new Known(guild, z2, z3, num);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof Known)) {
                        return false;
                    }
                    Known known = (Known) obj;
                    return m.areEqual(this.guild, known.guild) && isPublic() == known.isPublic() && isUserInGuild() == known.isUserInGuild() && m.areEqual(this.approximateOnline, known.approximateOnline);
                }

                public final Integer getApproximateOnline() {
                    return this.approximateOnline;
                }

                public final Guild getGuild() {
                    return this.guild;
                }

                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r3v0 */
                /* JADX WARN: Type inference failed for: r3v1 */
                /* JADX WARN: Type inference failed for: r3v2 */
                public int hashCode() {
                    Guild guild = this.guild;
                    int i = 0;
                    int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
                    boolean isPublic = isPublic();
                    ?? r3 = 1;
                    if (isPublic) {
                        isPublic = true;
                    }
                    int i2 = isPublic ? 1 : 0;
                    int i3 = isPublic ? 1 : 0;
                    int i4 = (hashCode + i2) * 31;
                    boolean isUserInGuild = isUserInGuild();
                    if (!isUserInGuild) {
                        r3 = isUserInGuild;
                    }
                    int i5 = (i4 + (r3 == true ? 1 : 0)) * 31;
                    Integer num = this.approximateOnline;
                    if (num != null) {
                        i = num.hashCode();
                    }
                    return i5 + i;
                }

                @Override // com.discord.widgets.stickers.GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo
                public boolean isPublic() {
                    return this.isPublic;
                }

                @Override // com.discord.widgets.stickers.GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo
                public boolean isUserInGuild() {
                    return this.isUserInGuild;
                }

                public String toString() {
                    StringBuilder R = a.R("Known(guild=");
                    R.append(this.guild);
                    R.append(", isPublic=");
                    R.append(isPublic());
                    R.append(", isUserInGuild=");
                    R.append(isUserInGuild());
                    R.append(", approximateOnline=");
                    return a.E(R, this.approximateOnline, ")");
                }
            }

            /* compiled from: GuildStickerSheetViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0003\u0010\u0005R\u001c\u0010\u0006\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0006\u0010\u0004\u001a\u0004\b\u0006\u0010\u0005¨\u0006\t"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo$Unknown;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "", "isPublic", "Z", "()Z", "isUserInGuild", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Unknown extends GuildStickerGuildInfo {
                public static final Unknown INSTANCE = new Unknown();
                private static final boolean isPublic = false;
                private static final boolean isUserInGuild = false;

                private Unknown() {
                    super(null);
                }

                @Override // com.discord.widgets.stickers.GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo
                public boolean isPublic() {
                    return isPublic;
                }

                @Override // com.discord.widgets.stickers.GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo
                public boolean isUserInGuild() {
                    return isUserInGuild;
                }
            }

            private GuildStickerGuildInfo() {
            }

            public abstract boolean isPublic();

            public abstract boolean isUserInGuild();

            public /* synthetic */ GuildStickerGuildInfo(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        private Companion() {
        }

        private final Observable<GuildStickerGuildInfo> getGuildForGuildSticker(final RestAPI restAPI, final Sticker sticker, final StoreGuilds storeGuilds) {
            Observable<GuildStickerGuildInfo> z2 = Observable.C(new Callable<Guild>() { // from class: com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion$getGuildForGuildSticker$1
                /* JADX WARN: Can't rename method to resolve collision */
                @Override // java.util.concurrent.Callable
                public final Guild call() {
                    return StoreGuilds.this.getGuilds().get(sticker.g());
                }
            }).z(new b<Guild, Observable<? extends GuildStickerGuildInfo>>() { // from class: com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion$getGuildForGuildSticker$2

                /* compiled from: GuildStickerSheetViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/guild/Guild;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/models/guild/Guild;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/guild/Guild;)Lcom/discord/models/guild/Guild;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion$getGuildForGuildSticker$2$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1<T, R> implements b<com.discord.api.guild.Guild, Guild> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    public final Guild call(com.discord.api.guild.Guild guild) {
                        m.checkNotNullExpressionValue(guild, "it");
                        return new Guild(guild);
                    }
                }

                /* compiled from: GuildStickerSheetViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/guild/Guild;", "kotlin.jvm.PlatformType", "responseGuild", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion$getGuildForGuildSticker$2$2  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass2<T, R> implements b<Guild, GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo> {
                    public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                    public final GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo call(Guild guild) {
                        m.checkNotNullExpressionValue(guild, "responseGuild");
                        return new GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Known(guild, true, true, Integer.valueOf(guild.getApproximatePresenceCount()));
                    }
                }

                /* compiled from: GuildStickerSheetViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/guild/Guild;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/models/guild/Guild;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/guild/Guild;)Lcom/discord/models/guild/Guild;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion$getGuildForGuildSticker$2$4  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass4<T, R> implements b<com.discord.api.guild.Guild, Guild> {
                    public static final AnonymousClass4 INSTANCE = new AnonymousClass4();

                    public final Guild call(com.discord.api.guild.Guild guild) {
                        m.checkNotNullExpressionValue(guild, "it");
                        return new Guild(guild);
                    }
                }

                /* compiled from: GuildStickerSheetViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/guild/Guild;", "kotlin.jvm.PlatformType", "responseGuild", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion$getGuildForGuildSticker$2$5  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass5<T, R> implements b<Guild, GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo> {
                    public static final AnonymousClass5 INSTANCE = new AnonymousClass5();

                    public final GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo call(Guild guild) {
                        m.checkNotNullExpressionValue(guild, "responseGuild");
                        return new GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Known(guild, true, false, Integer.valueOf(guild.getApproximatePresenceCount()));
                    }
                }

                /* compiled from: GuildStickerSheetViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion$getGuildForGuildSticker$2$6  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass6<T, R> implements b<Throwable, GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo> {
                    public static final AnonymousClass6 INSTANCE = new AnonymousClass6();

                    public final GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo call(Throwable th) {
                        return GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Unknown.INSTANCE;
                    }
                }

                public final Observable<? extends GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo> call(final Guild guild) {
                    if (guild == null) {
                        return (Observable<R>) RestAPI.this.getStickerGuild(sticker.getId()).F(AnonymousClass4.INSTANCE).F(AnonymousClass5.INSTANCE).L(AnonymousClass6.INSTANCE);
                    }
                    if (guild.getFeatures().contains(GuildFeature.DISCOVERABLE)) {
                        return (Observable<R>) RestAPI.this.getStickerGuild(sticker.getId()).F(AnonymousClass1.INSTANCE).F(AnonymousClass2.INSTANCE).L(new b<Throwable, GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo>() { // from class: com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion$getGuildForGuildSticker$2.3
                            public final GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo call(Throwable th) {
                                if (!(th instanceof HttpException) || ((HttpException) th).a() != 404) {
                                    return GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Unknown.INSTANCE;
                                }
                                return new GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Known(Guild.this, false, true, null);
                            }
                        });
                    }
                    return new k(new GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Known(guild, false, true, null));
                }
            });
            m.checkNotNullExpressionValue(z2, "Observable.fromCallable …      }\n        }\n      }");
            return z2;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(RestAPI restAPI, Sticker sticker, StoreUser storeUser, StoreChannelsSelected storeChannelsSelected, StoreGuilds storeGuilds) {
            Observable<StoreState> h = Observable.h(getGuildForGuildSticker(restAPI, sticker, storeGuilds), StoreUser.observeMe$default(storeUser, false, 1, null), storeChannelsSelected.observeSelectedChannel(), storeGuilds.observeGuilds(), GuildStickerSheetViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(h, "Observable.combineLatest…erGuildInfo\n      )\n    }");
            return h;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GuildStickerSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u000e\u0010\u0011\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u0013\u001a\u00020\r¢\u0006\u0004\b(\u0010)J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ \u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\n0\tHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJP\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0010\u001a\u00020\u00022\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\u0018\b\u0002\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\n0\t2\b\b\u0002\u0010\u0013\u001a\u00020\rHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001e\u001a\u00020\u001d2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\u0004R)\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\n0\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\fR!\u0010\u0011\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b%\u0010\bR\u0019\u0010\u0013\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b'\u0010\u000f¨\u0006*"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$StoreState;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/lang/Long;", "", "Lcom/discord/models/guild/Guild;", "component3", "()Ljava/util/Map;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "component4", "()Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "meUser", "currentGuildId", "userGuilds", "guildStickerGuildInfo", "copy", "(Lcom/discord/models/user/MeUser;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;)Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/user/MeUser;", "getMeUser", "Ljava/util/Map;", "getUserGuilds", "Ljava/lang/Long;", "getCurrentGuildId", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "getGuildStickerGuildInfo", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Long currentGuildId;
        private final Companion.GuildStickerGuildInfo guildStickerGuildInfo;
        private final MeUser meUser;
        private final Map<Long, Guild> userGuilds;

        public StoreState(MeUser meUser, Long l, Map<Long, Guild> map, Companion.GuildStickerGuildInfo guildStickerGuildInfo) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(map, "userGuilds");
            m.checkNotNullParameter(guildStickerGuildInfo, "guildStickerGuildInfo");
            this.meUser = meUser;
            this.currentGuildId = l;
            this.userGuilds = map;
            this.guildStickerGuildInfo = guildStickerGuildInfo;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, MeUser meUser, Long l, Map map, Companion.GuildStickerGuildInfo guildStickerGuildInfo, int i, Object obj) {
            if ((i & 1) != 0) {
                meUser = storeState.meUser;
            }
            if ((i & 2) != 0) {
                l = storeState.currentGuildId;
            }
            if ((i & 4) != 0) {
                map = storeState.userGuilds;
            }
            if ((i & 8) != 0) {
                guildStickerGuildInfo = storeState.guildStickerGuildInfo;
            }
            return storeState.copy(meUser, l, map, guildStickerGuildInfo);
        }

        public final MeUser component1() {
            return this.meUser;
        }

        public final Long component2() {
            return this.currentGuildId;
        }

        public final Map<Long, Guild> component3() {
            return this.userGuilds;
        }

        public final Companion.GuildStickerGuildInfo component4() {
            return this.guildStickerGuildInfo;
        }

        public final StoreState copy(MeUser meUser, Long l, Map<Long, Guild> map, Companion.GuildStickerGuildInfo guildStickerGuildInfo) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(map, "userGuilds");
            m.checkNotNullParameter(guildStickerGuildInfo, "guildStickerGuildInfo");
            return new StoreState(meUser, l, map, guildStickerGuildInfo);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.meUser, storeState.meUser) && m.areEqual(this.currentGuildId, storeState.currentGuildId) && m.areEqual(this.userGuilds, storeState.userGuilds) && m.areEqual(this.guildStickerGuildInfo, storeState.guildStickerGuildInfo);
        }

        public final Long getCurrentGuildId() {
            return this.currentGuildId;
        }

        public final Companion.GuildStickerGuildInfo getGuildStickerGuildInfo() {
            return this.guildStickerGuildInfo;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final Map<Long, Guild> getUserGuilds() {
            return this.userGuilds;
        }

        public int hashCode() {
            MeUser meUser = this.meUser;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            Long l = this.currentGuildId;
            int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
            Map<Long, Guild> map = this.userGuilds;
            int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
            Companion.GuildStickerGuildInfo guildStickerGuildInfo = this.guildStickerGuildInfo;
            if (guildStickerGuildInfo != null) {
                i = guildStickerGuildInfo.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(meUser=");
            R.append(this.meUser);
            R.append(", currentGuildId=");
            R.append(this.currentGuildId);
            R.append(", userGuilds=");
            R.append(this.userGuilds);
            R.append(", guildStickerGuildInfo=");
            R.append(this.guildStickerGuildInfo);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildStickerSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Dismiss", "Loaded", "Loading", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Dismiss;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: GuildStickerSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Dismiss;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Dismiss extends ViewState {
            public static final Dismiss INSTANCE = new Dismiss();

            private Dismiss() {
                super(null);
            }
        }

        /* compiled from: GuildStickerSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\u0006\u0010\u000f\u001a\u00020\t¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ8\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00052\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\r\u0010\u0007R\u0019\u0010\u000f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b\u001e\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001f\u001a\u0004\b \u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u000e\u0010\u0007¨\u0006#"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState;", "Lcom/discord/api/sticker/Sticker;", "component1", "()Lcom/discord/api/sticker/Sticker;", "", "component2", "()Z", "component3", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "component4", "()Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "sticker", "isUserPremium", "isCurrentGuild", "guildStickerGuildInfo", "copy", "(Lcom/discord/api/sticker/Sticker;ZZLcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;)Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;", "getGuildStickerGuildInfo", "Lcom/discord/api/sticker/Sticker;", "getSticker", HookHelper.constructorName, "(Lcom/discord/api/sticker/Sticker;ZZLcom/discord/widgets/stickers/GuildStickerSheetViewModel$Companion$GuildStickerGuildInfo;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final Companion.GuildStickerGuildInfo guildStickerGuildInfo;
            private final boolean isCurrentGuild;
            private final boolean isUserPremium;
            private final Sticker sticker;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(Sticker sticker, boolean z2, boolean z3, Companion.GuildStickerGuildInfo guildStickerGuildInfo) {
                super(null);
                m.checkNotNullParameter(sticker, "sticker");
                m.checkNotNullParameter(guildStickerGuildInfo, "guildStickerGuildInfo");
                this.sticker = sticker;
                this.isUserPremium = z2;
                this.isCurrentGuild = z3;
                this.guildStickerGuildInfo = guildStickerGuildInfo;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, Sticker sticker, boolean z2, boolean z3, Companion.GuildStickerGuildInfo guildStickerGuildInfo, int i, Object obj) {
                if ((i & 1) != 0) {
                    sticker = loaded.sticker;
                }
                if ((i & 2) != 0) {
                    z2 = loaded.isUserPremium;
                }
                if ((i & 4) != 0) {
                    z3 = loaded.isCurrentGuild;
                }
                if ((i & 8) != 0) {
                    guildStickerGuildInfo = loaded.guildStickerGuildInfo;
                }
                return loaded.copy(sticker, z2, z3, guildStickerGuildInfo);
            }

            public final Sticker component1() {
                return this.sticker;
            }

            public final boolean component2() {
                return this.isUserPremium;
            }

            public final boolean component3() {
                return this.isCurrentGuild;
            }

            public final Companion.GuildStickerGuildInfo component4() {
                return this.guildStickerGuildInfo;
            }

            public final Loaded copy(Sticker sticker, boolean z2, boolean z3, Companion.GuildStickerGuildInfo guildStickerGuildInfo) {
                m.checkNotNullParameter(sticker, "sticker");
                m.checkNotNullParameter(guildStickerGuildInfo, "guildStickerGuildInfo");
                return new Loaded(sticker, z2, z3, guildStickerGuildInfo);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.sticker, loaded.sticker) && this.isUserPremium == loaded.isUserPremium && this.isCurrentGuild == loaded.isCurrentGuild && m.areEqual(this.guildStickerGuildInfo, loaded.guildStickerGuildInfo);
            }

            public final Companion.GuildStickerGuildInfo getGuildStickerGuildInfo() {
                return this.guildStickerGuildInfo;
            }

            public final Sticker getSticker() {
                return this.sticker;
            }

            public int hashCode() {
                Sticker sticker = this.sticker;
                int i = 0;
                int hashCode = (sticker != null ? sticker.hashCode() : 0) * 31;
                boolean z2 = this.isUserPremium;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode + i3) * 31;
                boolean z3 = this.isCurrentGuild;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                int i6 = (i5 + i2) * 31;
                Companion.GuildStickerGuildInfo guildStickerGuildInfo = this.guildStickerGuildInfo;
                if (guildStickerGuildInfo != null) {
                    i = guildStickerGuildInfo.hashCode();
                }
                return i6 + i;
            }

            public final boolean isCurrentGuild() {
                return this.isCurrentGuild;
            }

            public final boolean isUserPremium() {
                return this.isUserPremium;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(sticker=");
                R.append(this.sticker);
                R.append(", isUserPremium=");
                R.append(this.isUserPremium);
                R.append(", isCurrentGuild=");
                R.append(this.isCurrentGuild);
                R.append(", guildStickerGuildInfo=");
                R.append(this.guildStickerGuildInfo);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: GuildStickerSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GuildStickerSheetViewModel(com.discord.api.sticker.Sticker r8, com.discord.utilities.rest.RestAPI r9, com.discord.stores.StoreUser r10, com.discord.stores.StoreChannelsSelected r11, com.discord.stores.StoreGuilds r12, rx.Observable r13, int r14, kotlin.jvm.internal.DefaultConstructorMarker r15) {
        /*
            r7 = this;
            r15 = r14 & 2
            if (r15 == 0) goto La
            com.discord.utilities.rest.RestAPI$Companion r9 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r9 = r9.getApi()
        La:
            r2 = r9
            r9 = r14 & 4
            if (r9 == 0) goto L15
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r10 = r9.getUsers()
        L15:
            r3 = r10
            r9 = r14 & 8
            if (r9 == 0) goto L20
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannelsSelected r11 = r9.getChannelsSelected()
        L20:
            r4 = r11
            r9 = r14 & 16
            if (r9 == 0) goto L2b
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r12 = r9.getGuilds()
        L2b:
            r5 = r12
            r9 = r14 & 32
            if (r9 == 0) goto L3b
            com.discord.widgets.stickers.GuildStickerSheetViewModel$Companion r10 = com.discord.widgets.stickers.GuildStickerSheetViewModel.Companion
            r11 = r2
            r12 = r8
            r13 = r3
            r14 = r4
            r15 = r5
            rx.Observable r13 = com.discord.widgets.stickers.GuildStickerSheetViewModel.Companion.access$observeStoreState(r10, r11, r12, r13, r14, r15)
        L3b:
            r6 = r13
            r0 = r7
            r1 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stickers.GuildStickerSheetViewModel.<init>(com.discord.api.sticker.Sticker, com.discord.utilities.rest.RestAPI, com.discord.stores.StoreUser, com.discord.stores.StoreChannelsSelected, com.discord.stores.StoreGuilds, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        updateViewState(new ViewState.Loaded(this.sticker, storeState.getMeUser().getPremiumTier() == PremiumTier.TIER_2, m.areEqual(storeState.getCurrentGuildId(), this.sticker.g()), storeState.getGuildStickerGuildInfo()));
    }

    public final void joinGuild(Guild guild, Fragment fragment) {
        m.checkNotNullParameter(guild, "guild");
        m.checkNotNullParameter(fragment, "fragment");
        Context requireContext = fragment.requireContext();
        m.checkNotNullExpressionValue(requireContext, "fragment.requireContext()");
        GuildJoinHelperKt.joinGuild(requireContext, guild.getId(), false, (r27 & 8) != 0 ? null : null, (r27 & 16) != 0 ? null : null, (r27 & 32) != 0 ? null : null, GuildStickerSheetViewModel.class, (r27 & 128) != 0 ? null : null, (r27 & 256) != 0 ? null : null, (r27 & 512) != 0 ? null : null, new GuildStickerSheetViewModel$joinGuild$1(this));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildStickerSheetViewModel(Sticker sticker, RestAPI restAPI, StoreUser storeUser, StoreChannelsSelected storeChannelsSelected, StoreGuilds storeGuilds, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(sticker, "sticker");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeChannelsSelected, "storeChannelsSelected");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.sticker = sticker;
        this.restAPI = restAPI;
        this.storeUser = storeUser;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), GuildStickerSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
