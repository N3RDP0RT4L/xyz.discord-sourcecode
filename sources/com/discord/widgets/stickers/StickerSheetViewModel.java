package com.discord.widgets.stickers;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.premium.PremiumTier;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStickers;
import com.discord.stores.StoreUser;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StickerSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001e2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001e\u001f B=\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\b\b\u0002\u0010\f\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00060\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\n\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\tR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006!"}, d2 = {"Lcom/discord/widgets/stickers/StickerSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;", "", "fetchStickersData", "()V", "Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;)V", "handleLoadedStoreState", "Lcom/discord/stores/StoreStickers;", "storeStickers", "Lcom/discord/stores/StoreStickers;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "Ljava/lang/String;", "", "hasFiredAnalytics", "Z", "Lcom/discord/api/sticker/Sticker;", "sticker", "Lcom/discord/api/sticker/Sticker;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Lcom/discord/api/sticker/Sticker;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUser;Ljava/lang/String;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private boolean hasFiredAnalytics;
    private final String location;
    private final Sticker sticker;
    private final StoreStickers storeStickers;
    private final StoreUser storeUser;

    /* compiled from: StickerSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.stickers.StickerSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            StickerSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: StickerSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ-\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/stickers/StickerSheetViewModel$Companion;", "", "Lcom/discord/stores/StoreStickers;", "storeStickers", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/api/sticker/Sticker;", "sticker", "Lrx/Observable;", "Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUser;Lcom/discord/api/sticker/Sticker;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(StoreStickers storeStickers, StoreUser storeUser, Sticker sticker) {
            Long i = sticker.i();
            m.checkNotNull(i);
            Observable<StoreState> j = Observable.j(storeStickers.observeStickerPack(i.longValue()), StoreUser.observeMe$default(storeUser, false, 1, null), StickerSheetViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(j, "Observable.combineLatest…ser\n          )\n        }");
            return j;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StickerSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;", "", "Lcom/discord/stores/StoreStickers$StickerPackState;", "component1", "()Lcom/discord/stores/StoreStickers$StickerPackState;", "Lcom/discord/models/user/MeUser;", "component2", "()Lcom/discord/models/user/MeUser;", "stickerPack", "meUser", "copy", "(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/models/user/MeUser;)Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreStickers$StickerPackState;", "getStickerPack", "Lcom/discord/models/user/MeUser;", "getMeUser", HookHelper.constructorName, "(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/models/user/MeUser;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final MeUser meUser;
        private final StoreStickers.StickerPackState stickerPack;

        public StoreState(StoreStickers.StickerPackState stickerPackState, MeUser meUser) {
            m.checkNotNullParameter(stickerPackState, "stickerPack");
            m.checkNotNullParameter(meUser, "meUser");
            this.stickerPack = stickerPackState;
            this.meUser = meUser;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, StoreStickers.StickerPackState stickerPackState, MeUser meUser, int i, Object obj) {
            if ((i & 1) != 0) {
                stickerPackState = storeState.stickerPack;
            }
            if ((i & 2) != 0) {
                meUser = storeState.meUser;
            }
            return storeState.copy(stickerPackState, meUser);
        }

        public final StoreStickers.StickerPackState component1() {
            return this.stickerPack;
        }

        public final MeUser component2() {
            return this.meUser;
        }

        public final StoreState copy(StoreStickers.StickerPackState stickerPackState, MeUser meUser) {
            m.checkNotNullParameter(stickerPackState, "stickerPack");
            m.checkNotNullParameter(meUser, "meUser");
            return new StoreState(stickerPackState, meUser);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.stickerPack, storeState.stickerPack) && m.areEqual(this.meUser, storeState.meUser);
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final StoreStickers.StickerPackState getStickerPack() {
            return this.stickerPack;
        }

        public int hashCode() {
            StoreStickers.StickerPackState stickerPackState = this.stickerPack;
            int i = 0;
            int hashCode = (stickerPackState != null ? stickerPackState.hashCode() : 0) * 31;
            MeUser meUser = this.meUser;
            if (meUser != null) {
                i = meUser.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(stickerPack=");
            R.append(this.stickerPack);
            R.append(", meUser=");
            R.append(this.meUser);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: StickerSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\u0006\u0010\u0011\u001a\u00020\u000b\u0012\u0006\u0010\u0012\u001a\u00020\u0002¢\u0006\u0004\b%\u0010&J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004J8\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000b2\b\b\u0002\u0010\u0012\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\u00022\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\rR\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b\u0012\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010#\u001a\u0004\b$\u0010\n¨\u0006'"}, d2 = {"Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;", "", "", "isUserPremiumTier2", "()Z", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "component1", "()Lcom/discord/models/sticker/dto/ModelStickerPack;", "Lcom/discord/api/sticker/Sticker;", "component2", "()Lcom/discord/api/sticker/Sticker;", "Lcom/discord/api/premium/PremiumTier;", "component3", "()Lcom/discord/api/premium/PremiumTier;", "component4", "stickerPack", "sticker", "meUserPremiumTier", "isStickerPackEnabled", "copy", "(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/api/sticker/Sticker;Lcom/discord/api/premium/PremiumTier;Z)Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "getStickerPack", "Lcom/discord/api/premium/PremiumTier;", "getMeUserPremiumTier", "Z", "Lcom/discord/api/sticker/Sticker;", "getSticker", HookHelper.constructorName, "(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/api/sticker/Sticker;Lcom/discord/api/premium/PremiumTier;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean isStickerPackEnabled;
        private final PremiumTier meUserPremiumTier;
        private final Sticker sticker;
        private final ModelStickerPack stickerPack;

        public ViewState(ModelStickerPack modelStickerPack, Sticker sticker, PremiumTier premiumTier, boolean z2) {
            m.checkNotNullParameter(modelStickerPack, "stickerPack");
            m.checkNotNullParameter(sticker, "sticker");
            m.checkNotNullParameter(premiumTier, "meUserPremiumTier");
            this.stickerPack = modelStickerPack;
            this.sticker = sticker;
            this.meUserPremiumTier = premiumTier;
            this.isStickerPackEnabled = z2;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, ModelStickerPack modelStickerPack, Sticker sticker, PremiumTier premiumTier, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                modelStickerPack = viewState.stickerPack;
            }
            if ((i & 2) != 0) {
                sticker = viewState.sticker;
            }
            if ((i & 4) != 0) {
                premiumTier = viewState.meUserPremiumTier;
            }
            if ((i & 8) != 0) {
                z2 = viewState.isStickerPackEnabled;
            }
            return viewState.copy(modelStickerPack, sticker, premiumTier, z2);
        }

        public final ModelStickerPack component1() {
            return this.stickerPack;
        }

        public final Sticker component2() {
            return this.sticker;
        }

        public final PremiumTier component3() {
            return this.meUserPremiumTier;
        }

        public final boolean component4() {
            return this.isStickerPackEnabled;
        }

        public final ViewState copy(ModelStickerPack modelStickerPack, Sticker sticker, PremiumTier premiumTier, boolean z2) {
            m.checkNotNullParameter(modelStickerPack, "stickerPack");
            m.checkNotNullParameter(sticker, "sticker");
            m.checkNotNullParameter(premiumTier, "meUserPremiumTier");
            return new ViewState(modelStickerPack, sticker, premiumTier, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.stickerPack, viewState.stickerPack) && m.areEqual(this.sticker, viewState.sticker) && m.areEqual(this.meUserPremiumTier, viewState.meUserPremiumTier) && this.isStickerPackEnabled == viewState.isStickerPackEnabled;
        }

        public final PremiumTier getMeUserPremiumTier() {
            return this.meUserPremiumTier;
        }

        public final Sticker getSticker() {
            return this.sticker;
        }

        public final ModelStickerPack getStickerPack() {
            return this.stickerPack;
        }

        public int hashCode() {
            ModelStickerPack modelStickerPack = this.stickerPack;
            int i = 0;
            int hashCode = (modelStickerPack != null ? modelStickerPack.hashCode() : 0) * 31;
            Sticker sticker = this.sticker;
            int hashCode2 = (hashCode + (sticker != null ? sticker.hashCode() : 0)) * 31;
            PremiumTier premiumTier = this.meUserPremiumTier;
            if (premiumTier != null) {
                i = premiumTier.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.isStickerPackEnabled;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isStickerPackEnabled() {
            return this.isStickerPackEnabled;
        }

        public final boolean isUserPremiumTier2() {
            return this.meUserPremiumTier == PremiumTier.TIER_2;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(stickerPack=");
            R.append(this.stickerPack);
            R.append(", sticker=");
            R.append(this.sticker);
            R.append(", meUserPremiumTier=");
            R.append(this.meUserPremiumTier);
            R.append(", isStickerPackEnabled=");
            return a.M(R, this.isStickerPackEnabled, ")");
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ StickerSheetViewModel(com.discord.api.sticker.Sticker r7, com.discord.stores.StoreStickers r8, com.discord.stores.StoreUser r9, java.lang.String r10, rx.Observable r11, int r12, kotlin.jvm.internal.DefaultConstructorMarker r13) {
        /*
            r6 = this;
            r13 = r12 & 2
            if (r13 == 0) goto La
            com.discord.stores.StoreStream$Companion r8 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreStickers r8 = r8.getStickers()
        La:
            r2 = r8
            r8 = r12 & 4
            if (r8 == 0) goto L15
            com.discord.stores.StoreStream$Companion r8 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r9 = r8.getUsers()
        L15:
            r3 = r9
            r8 = r12 & 16
            if (r8 == 0) goto L20
            com.discord.widgets.stickers.StickerSheetViewModel$Companion r8 = com.discord.widgets.stickers.StickerSheetViewModel.Companion
            rx.Observable r11 = com.discord.widgets.stickers.StickerSheetViewModel.Companion.access$observeStoreState(r8, r2, r3, r7)
        L20:
            r5 = r11
            r0 = r6
            r1 = r7
            r4 = r10
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.stickers.StickerSheetViewModel.<init>(com.discord.api.sticker.Sticker, com.discord.stores.StoreStickers, com.discord.stores.StoreUser, java.lang.String, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final void fetchStickersData() {
        StoreStickers storeStickers = this.storeStickers;
        Long i = this.sticker.i();
        m.checkNotNull(i);
        storeStickers.fetchStickerPack(i.longValue());
        this.storeStickers.fetchEnabledStickerDirectory();
    }

    private final void handleLoadedStoreState(StoreState storeState) {
        boolean z2;
        boolean z3;
        MeUser meUser = storeState.getMeUser();
        StoreStickers.StickerPackState stickerPack = storeState.getStickerPack();
        Objects.requireNonNull(stickerPack, "null cannot be cast to non-null type com.discord.stores.StoreStickers.StickerPackState.Loaded");
        ModelStickerPack stickerPack2 = ((StoreStickers.StickerPackState.Loaded) stickerPack).getStickerPack();
        if (!this.hasFiredAnalytics && this.location != null) {
            StickerPurchaseLocation popoutPurchaseLocation = StickerPurchaseLocation.Companion.getPopoutPurchaseLocation(stickerPack2.canBePurchased());
            AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
            Long i = this.sticker.i();
            m.checkNotNull(i);
            analyticsTracker.stickerPopoutOpened(i.longValue(), this.location, new Traits.Location(null, popoutPurchaseLocation.getAnalyticsValue(), null, null, null, 29, null));
            this.hasFiredAnalytics = true;
        }
        Sticker sticker = this.sticker;
        PremiumTier premiumTier = meUser.getPremiumTier();
        List<ModelStickerPack> enabledStickerPacks = this.storeStickers.getEnabledStickerPacks();
        boolean z4 = false;
        if (!(enabledStickerPacks instanceof Collection) || !enabledStickerPacks.isEmpty()) {
            Iterator<T> it = enabledStickerPacks.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                List<Sticker> stickers = ((ModelStickerPack) it.next()).getStickers();
                if (!(stickers instanceof Collection) || !stickers.isEmpty()) {
                    for (Sticker sticker2 : stickers) {
                        if (sticker2.getId() == this.sticker.getId()) {
                            z3 = true;
                            continue;
                        } else {
                            z3 = false;
                            continue;
                        }
                        if (z3) {
                            z2 = true;
                            continue;
                            break;
                        }
                    }
                }
                z2 = false;
                continue;
                if (z2) {
                    z4 = true;
                    break;
                }
            }
        }
        updateViewState(new ViewState(stickerPack2, sticker, premiumTier, z4));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        if (storeState.getStickerPack() instanceof StoreStickers.StickerPackState.Loaded) {
            handleLoadedStoreState(storeState);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StickerSheetViewModel(Sticker sticker, StoreStickers storeStickers, StoreUser storeUser, String str, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(sticker, "sticker");
        m.checkNotNullParameter(storeStickers, "storeStickers");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.sticker = sticker;
        this.storeStickers = storeStickers;
        this.storeUser = storeUser;
        this.location = str;
        fetchStickersData();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), StickerSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
