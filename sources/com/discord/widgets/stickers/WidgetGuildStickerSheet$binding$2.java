package com.discord.widgets.stickers;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetGuildStickerSheetBinding;
import com.discord.views.sticker.StickerView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildStickerSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetGuildStickerSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildStickerSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildStickerSheet$binding$2 extends k implements Function1<View, WidgetGuildStickerSheetBinding> {
    public static final WidgetGuildStickerSheet$binding$2 INSTANCE = new WidgetGuildStickerSheet$binding$2();

    public WidgetGuildStickerSheet$binding$2() {
        super(1, WidgetGuildStickerSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetGuildStickerSheetBinding;", 0);
    }

    public final WidgetGuildStickerSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.divider;
        View findViewById = view.findViewById(R.id.divider);
        if (findViewById != null) {
            i = R.id.guild_sticker_sheet_button_container;
            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.guild_sticker_sheet_button_container);
            if (frameLayout != null) {
                i = R.id.guild_sticker_sheet_description;
                TextView textView = (TextView) view.findViewById(R.id.guild_sticker_sheet_description);
                if (textView != null) {
                    AppViewFlipper appViewFlipper = (AppViewFlipper) view;
                    i = R.id.guild_sticker_sheet_guild_container;
                    LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.guild_sticker_sheet_guild_container);
                    if (linearLayout != null) {
                        i = R.id.guild_sticker_sheet_guild_header;
                        TextView textView2 = (TextView) view.findViewById(R.id.guild_sticker_sheet_guild_header);
                        if (textView2 != null) {
                            i = R.id.guild_sticker_sheet_guild_icon;
                            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.guild_sticker_sheet_guild_icon);
                            if (simpleDraweeView != null) {
                                i = R.id.guild_sticker_sheet_guild_icon_text;
                                TextView textView3 = (TextView) view.findViewById(R.id.guild_sticker_sheet_guild_icon_text);
                                if (textView3 != null) {
                                    i = R.id.guild_sticker_sheet_guild_icon_wrapper;
                                    FrameLayout frameLayout2 = (FrameLayout) view.findViewById(R.id.guild_sticker_sheet_guild_icon_wrapper);
                                    if (frameLayout2 != null) {
                                        i = R.id.guild_sticker_sheet_guild_info;
                                        TextView textView4 = (TextView) view.findViewById(R.id.guild_sticker_sheet_guild_info);
                                        if (textView4 != null) {
                                            i = R.id.guild_sticker_sheet_guild_name;
                                            TextView textView5 = (TextView) view.findViewById(R.id.guild_sticker_sheet_guild_name);
                                            if (textView5 != null) {
                                                i = R.id.guild_sticker_sheet_join_btn;
                                                MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.guild_sticker_sheet_join_btn);
                                                if (materialButton != null) {
                                                    i = R.id.guild_sticker_sheet_premium_btn;
                                                    MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.guild_sticker_sheet_premium_btn);
                                                    if (materialButton2 != null) {
                                                        i = R.id.guild_sticker_sheet_sticker;
                                                        StickerView stickerView = (StickerView) view.findViewById(R.id.guild_sticker_sheet_sticker);
                                                        if (stickerView != null) {
                                                            i = R.id.guild_sticker_sheet_sticker_name;
                                                            TextView textView6 = (TextView) view.findViewById(R.id.guild_sticker_sheet_sticker_name);
                                                            if (textView6 != null) {
                                                                return new WidgetGuildStickerSheetBinding(appViewFlipper, findViewById, frameLayout, textView, appViewFlipper, linearLayout, textView2, simpleDraweeView, textView3, frameLayout2, textView4, textView5, materialButton, materialButton2, stickerView, textView6);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
