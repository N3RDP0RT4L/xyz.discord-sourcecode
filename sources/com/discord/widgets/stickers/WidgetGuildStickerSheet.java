package com.discord.widgets.stickers;

import andhook.lib.HookHelper;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.guild.GuildFeature;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerType;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetGuildStickerSheetBinding;
import com.discord.models.guild.Guild;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.locale.LocaleManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.stickers.GuildStickerSheetViewModel;
import com.discord.widgets.stickers.StickerPremiumUpsellDialog;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.a0;
import d0.z.d.m;
import java.text.NumberFormat;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetGuildStickerSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 +2\u00020\u0001:\u0001+B\u0007¢\u0006\u0004\b*\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0007\u0010\bJ/\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\u0013\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\t2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J3\u0010\u0016\u001a\u00020\u00022\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\t2\b\u0010\u0015\u001a\u0004\u0018\u00010\u000eH\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dR\u001d\u0010#\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001d\u0010)\u001a\u00020$8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(¨\u0006,"}, d2 = {"Lcom/discord/widgets/stickers/WidgetGuildStickerSheet;", "Lcom/discord/app/AppBottomSheet;", "", "showLoading", "()V", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Loaded;", "viewState", "configureUI", "(Lcom/discord/widgets/stickers/GuildStickerSheetViewModel$ViewState$Loaded;)V", "", "isCurrentGuild", "isUserInGuild", "isGuildPublic", "isUserPremium", "", "getCustomStickerInfoText", "(ZZZZ)I", "Lcom/discord/models/guild/Guild;", "guild", "configureButtons", "(ZZLcom/discord/models/guild/Guild;)V", "approximateOnline", "configureGuildSection", "(Lcom/discord/models/guild/Guild;ZZLjava/lang/Integer;)V", "getContentViewResId", "()I", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "Lcom/discord/widgets/stickers/GuildStickerSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/stickers/GuildStickerSheetViewModel;", "viewModel", "Lcom/discord/databinding/WidgetGuildStickerSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildStickerSheetBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildStickerSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildStickerSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildStickerSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int VIEW_CONTENT = 0;
    private static final int VIEW_LOADING = 1;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildStickerSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetGuildStickerSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\u000b¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/stickers/WidgetGuildStickerSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/api/sticker/Sticker;", "sticker", "", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/api/sticker/Sticker;)V", "", "VIEW_CONTENT", "I", "VIEW_LOADING", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, Sticker sticker) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(sticker, "sticker");
            if (sticker.k() == StickerType.GUILD) {
                WidgetGuildStickerSheet widgetGuildStickerSheet = new WidgetGuildStickerSheet();
                Bundle bundle = new Bundle();
                bundle.putSerializable("com.discord.intent.extra.EXTRA_STICKER", sticker);
                widgetGuildStickerSheet.setArguments(bundle);
                widgetGuildStickerSheet.show(fragmentManager, WidgetGuildStickerSheet.class.getName());
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildStickerSheet() {
        super(false, 1, null);
        WidgetGuildStickerSheet$viewModel$2 widgetGuildStickerSheet$viewModel$2 = new WidgetGuildStickerSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildStickerSheetViewModel.class), new WidgetGuildStickerSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildStickerSheet$viewModel$2));
    }

    private final void configureButtons(final boolean z2, final boolean z3, final Guild guild) {
        WidgetGuildStickerSheetBinding binding = getBinding();
        if (guild == null) {
            FrameLayout frameLayout = binding.f2430b;
            m.checkNotNullExpressionValue(frameLayout, "guildStickerSheetButtonContainer");
            frameLayout.setVisibility(8);
        } else if (!z2) {
            FrameLayout frameLayout2 = binding.f2430b;
            m.checkNotNullExpressionValue(frameLayout2, "guildStickerSheetButtonContainer");
            frameLayout2.setVisibility(0);
            MaterialButton materialButton = binding.l;
            m.checkNotNullExpressionValue(materialButton, "guildStickerSheetPremiumBtn");
            materialButton.setVisibility(0);
            MaterialButton materialButton2 = binding.k;
            m.checkNotNullExpressionValue(materialButton2, "guildStickerSheetJoinBtn");
            materialButton2.setVisibility(8);
            binding.l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.WidgetGuildStickerSheet$configureButtons$$inlined$with$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetGuildStickerSheet.this.dismiss();
                    StickerPremiumUpsellDialog.Companion companion = StickerPremiumUpsellDialog.Companion;
                    FragmentManager parentFragmentManager = WidgetGuildStickerSheet.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion.show(parentFragmentManager, new Traits.Location(null, Traits.Location.Section.STICKER_POPOUT, null, null, null, 29, null));
                }
            });
        } else if (!z3) {
            FrameLayout frameLayout3 = binding.f2430b;
            m.checkNotNullExpressionValue(frameLayout3, "guildStickerSheetButtonContainer");
            frameLayout3.setVisibility(0);
            MaterialButton materialButton3 = binding.l;
            m.checkNotNullExpressionValue(materialButton3, "guildStickerSheetPremiumBtn");
            materialButton3.setVisibility(8);
            MaterialButton materialButton4 = binding.k;
            m.checkNotNullExpressionValue(materialButton4, "guildStickerSheetJoinBtn");
            materialButton4.setVisibility(0);
            binding.k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.WidgetGuildStickerSheet$configureButtons$$inlined$with$lambda$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GuildStickerSheetViewModel viewModel;
                    Guild guild2 = guild;
                    viewModel = WidgetGuildStickerSheet.this.getViewModel();
                    viewModel.joinGuild(guild2, WidgetGuildStickerSheet.this);
                }
            });
        } else {
            FrameLayout frameLayout4 = binding.f2430b;
            m.checkNotNullExpressionValue(frameLayout4, "guildStickerSheetButtonContainer");
            frameLayout4.setVisibility(8);
        }
    }

    private final void configureGuildSection(Guild guild, boolean z2, boolean z3, Integer num) {
        int i;
        CharSequence e;
        CharSequence e2;
        WidgetGuildStickerSheetBinding binding = getBinding();
        if (guild == null || z2) {
            LinearLayout linearLayout = binding.e;
            m.checkNotNullExpressionValue(linearLayout, "guildStickerSheetGuildContainer");
            linearLayout.setVisibility(8);
            return;
        }
        LinearLayout linearLayout2 = binding.e;
        m.checkNotNullExpressionValue(linearLayout2, "guildStickerSheetGuildContainer");
        linearLayout2.setVisibility(0);
        if (guild.hasIcon()) {
            SimpleDraweeView simpleDraweeView = binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView, "guildStickerSheetGuildIcon");
            IconUtils.setIcon$default((ImageView) simpleDraweeView, guild, 0, (MGImages.ChangeDetector) null, true, 12, (Object) null);
        } else {
            binding.h.setBackgroundResource(R.drawable.drawable_circle_black);
            int themedColor = ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorBackgroundSecondary);
            FrameLayout frameLayout = binding.h;
            m.checkNotNullExpressionValue(frameLayout, "guildStickerSheetGuildIconWrapper");
            frameLayout.setBackgroundTintList(ColorStateList.valueOf(themedColor));
            TextView textView = binding.g;
            m.checkNotNullExpressionValue(textView, "guildStickerSheetGuildIconText");
            textView.setText(guild.getShortName());
        }
        if (guild.getFeatures().contains(GuildFeature.PARTNERED)) {
            i = R.drawable.ic_partnered_badge_banner;
        } else {
            i = guild.getFeatures().contains(GuildFeature.VERIFIED) ? R.drawable.ic_verified_badge_banner : 0;
        }
        TextView textView2 = binding.j;
        m.checkNotNullExpressionValue(textView2, "guildStickerSheetGuildName");
        DrawableCompat.setCompoundDrawablesCompat(textView2, i, 0, 0, 0);
        TextView textView3 = binding.j;
        m.checkNotNullExpressionValue(textView3, "guildStickerSheetGuildName");
        textView3.setText(guild.getName());
        int i2 = z3 ? R.string.sticker_popout_public_server : R.string.sticker_popout_private_server;
        String str = null;
        e = b.e(this, i2, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        if (num != null) {
            String format = NumberFormat.getNumberInstance(new LocaleManager().getPrimaryLocale(requireContext())).format(Integer.valueOf(num.intValue()));
            StringBuilder sb = new StringBuilder();
            e2 = b.e(this, R.string.instant_invite_guild_members_online, new Object[]{format}, (r4 & 4) != 0 ? b.a.j : null);
            sb.append(e2.toString());
            sb.append(" • ");
            str = sb.toString();
        }
        if (str == null) {
            str = "";
        }
        TextView textView4 = binding.i;
        m.checkNotNullExpressionValue(textView4, "guildStickerSheetGuildInfo");
        textView4.setText(str + e);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(GuildStickerSheetViewModel.ViewState.Loaded loaded) {
        Sticker component1 = loaded.component1();
        boolean component2 = loaded.component2();
        boolean component3 = loaded.component3();
        boolean isPublic = loaded.getGuildStickerGuildInfo().isPublic();
        boolean isUserInGuild = loaded.getGuildStickerGuildInfo().isUserInGuild();
        GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo guildStickerGuildInfo = loaded.getGuildStickerGuildInfo();
        Integer num = null;
        if (!(guildStickerGuildInfo instanceof GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Known)) {
            guildStickerGuildInfo = null;
        }
        GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Known known = (GuildStickerSheetViewModel.Companion.GuildStickerGuildInfo.Known) guildStickerGuildInfo;
        getBinding().m.d(component1, 0);
        TextView textView = getBinding().n;
        m.checkNotNullExpressionValue(textView, "binding.guildStickerSheetStickerName");
        textView.setText(component1.h());
        TextView textView2 = getBinding().c;
        m.checkNotNullExpressionValue(textView2, "binding.guildStickerSheetDescription");
        textView2.setText(getString(getCustomStickerInfoText(component3, isUserInGuild, isPublic, component2)));
        configureButtons(component2, isUserInGuild, known != null ? known.getGuild() : null);
        Guild guild = known != null ? known.getGuild() : null;
        if (known != null) {
            num = known.getApproximateOnline();
        }
        configureGuildSection(guild, component3, isPublic, num);
        AppViewFlipper appViewFlipper = getBinding().d;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.guildStickerSheetFlipper");
        appViewFlipper.setDisplayedChild(0);
    }

    private final WidgetGuildStickerSheetBinding getBinding() {
        return (WidgetGuildStickerSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final int getCustomStickerInfoText(boolean z2, boolean z3, boolean z4, boolean z5) {
        return (!z2 || z5) ? (!z2 || !z5) ? (!z3 || z5) ? (!z3 || !z5) ? (!z4 || z5) ? (!z4 || !z5) ? R.string.sticker_popout_premium_unjoined_private_guild_description : R.string.sticker_popout_premium_unjoined_discoverable_guild_description : R.string.sticker_popout_unjoined_discoverable_guild_description : R.string.sticker_popout_premium_joined_guild_description : R.string.sticker_popout_joined_guild_description : R.string.sticker_popout_premium_current_guild_description : R.string.sticker_popout_current_guild_description;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildStickerSheetViewModel getViewModel() {
        return (GuildStickerSheetViewModel) this.viewModel$delegate.getValue();
    }

    public static final void show(FragmentManager fragmentManager, Sticker sticker) {
        Companion.show(fragmentManager, sticker);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showLoading() {
        AppViewFlipper appViewFlipper = getBinding().d;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.guildStickerSheetFlipper");
        appViewFlipper.setDisplayedChild(1);
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        Observable q = ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, WidgetGuildStickerSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildStickerSheet$bindSubscriptions$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_guild_sticker_sheet;
    }
}
