package com.discord.widgets.stickers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.premium.PremiumTier;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerType;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetStickerSheetBinding;
import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stickers.StickerUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.discord.widgets.chat.input.expression.ExpressionPickerEvent;
import com.discord.widgets.chat.input.expression.ExpressionPickerEventBus;
import com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewType;
import com.discord.widgets.chat.input.sticker.WidgetStickerPackStoreSheet;
import com.discord.widgets.settings.premium.WidgetSettingsPremium;
import com.discord.widgets.stickers.StickerPremiumUpsellDialog;
import com.discord.widgets.stickers.StickerSheetViewModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetStickerSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\f\u0010\rR\u001d\u0010\u0013\u001a\u00020\u000e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001d\u0010\u0019\u001a\u00020\u00148B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/stickers/WidgetStickerSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;)V", "", "getContentViewResId", "()I", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "Lcom/discord/databinding/WidgetStickerSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetStickerSheetBinding;", "binding", "Lcom/discord/widgets/stickers/StickerSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/stickers/StickerSheetViewModel;", "viewModel", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerSheet extends AppBottomSheet {
    private static final String ANALYTICS_LOCATION = "widget_sticker_sheet_analytics_location";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetStickerSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetStickerSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetStickerSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetStickerSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J+\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/stickers/WidgetStickerSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/api/sticker/Sticker;", "sticker", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/api/sticker/Sticker;J)V", "", "ANALYTICS_LOCATION", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, Sticker sticker, long j) {
            Channel findChannelById;
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(sticker, "sticker");
            if (sticker.k() == StickerType.STANDARD && (findChannelById = StoreStream.Companion.getChannels().findChannelById(j)) != null) {
                String str = findChannelById.f() == 0 ? "DM Channel" : Traits.Location.Page.GUILD_CHANNEL;
                WidgetStickerSheet widgetStickerSheet = new WidgetStickerSheet();
                Bundle bundle = new Bundle();
                bundle.putSerializable("com.discord.intent.extra.EXTRA_STICKER", sticker);
                bundle.putString(WidgetStickerSheet.ANALYTICS_LOCATION, str);
                widgetStickerSheet.setArguments(bundle);
                widgetStickerSheet.show(fragmentManager, WidgetStickerSheet.class.getName());
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetStickerSheet() {
        super(false, 1, null);
        WidgetStickerSheet$viewModel$2 widgetStickerSheet$viewModel$2 = new WidgetStickerSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(StickerSheetViewModel.class), new WidgetStickerSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetStickerSheet$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(StickerSheetViewModel.ViewState viewState) {
        final ModelStickerPack component1 = viewState.component1();
        final Sticker component2 = viewState.component2();
        final PremiumTier component3 = viewState.component3();
        final boolean component4 = viewState.component4();
        TextView textView = getBinding().d;
        m.checkNotNullExpressionValue(textView, "binding.stickerSheetStickerName");
        textView.setText(component2.h());
        if (!viewState.isUserPremiumTier2()) {
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.WidgetStickerSheet$configureUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetSettingsPremium.Companion companion = WidgetSettingsPremium.Companion;
                    FragmentActivity requireActivity = WidgetStickerSheet.this.requireActivity();
                    m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
                    WidgetSettingsPremium.Companion.launch$default(companion, requireActivity, null, "Sticker Nitro Upsell Popout", 2, null);
                }
            });
        }
        TextView textView2 = getBinding().c;
        m.checkNotNullExpressionValue(textView2, "binding.stickerSheetStickerInfo");
        boolean z2 = true;
        int i = 0;
        textView2.setText((component4 || viewState.isUserPremiumTier2() || component1.isPremiumPack()) ? b.e(this, R.string.sticker_popout_pack_info_premium, new Object[]{component1.getName()}, (r4 & 4) != 0 ? b.a.j : null) : !component1.canBePurchased() ? b.e(this, R.string.sticker_popout_pack_info_unavailable, new Object[]{component1.getName()}, (r4 & 4) != 0 ? b.a.j : null) : b.e(this, R.string.sticker_popout_pack_info, new Object[]{component1.getName(), String.valueOf(StickerUtils.INSTANCE.calculatePremiumStickerPackDiscount())}, WidgetStickerSheet$configureUI$2.INSTANCE));
        try {
            getBinding().h.d(component1.getStickers().get(0), 0);
            getBinding().i.d(component1.getStickers().get(1), 0);
            getBinding().j.d(component1.getStickers().get(2), 0);
            getBinding().k.d(component1.getStickers().get(3), 0);
        } catch (IndexOutOfBoundsException unused) {
        }
        Bundle arguments = getArguments();
        final String string = arguments != null ? arguments.getString(ANALYTICS_LOCATION) : null;
        StickerUtils stickerUtils = StickerUtils.INSTANCE;
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        getBinding().f2637b.setText(stickerUtils.getStickerPackPremiumPriceLabel(requireContext, component1, component3, component4));
        getBinding().f2637b.setIsLoading(false);
        getBinding().f2637b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.WidgetStickerSheet$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (component4 && component3 != PremiumTier.TIER_2) {
                    WidgetStickerSheet.this.dismiss();
                    StickerPremiumUpsellDialog.Companion companion = StickerPremiumUpsellDialog.Companion;
                    FragmentManager parentFragmentManager = WidgetStickerSheet.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion.show(parentFragmentManager, new Traits.Location(null, Traits.Location.Section.STICKER_POPOUT, null, null, null, 29, null));
                }
            }
        });
        LoadingButton loadingButton = getBinding().f2637b;
        m.checkNotNullExpressionValue(loadingButton, "binding.stickerSheetBuyButton");
        if (!component4 || component3 == PremiumTier.TIER_2) {
            z2 = false;
        }
        loadingButton.setVisibility(z2 ? 0 : 8);
        MaterialButton materialButton = getBinding().e;
        m.checkNotNullExpressionValue(materialButton, "binding.stickerSheetViewButton");
        materialButton.setVisibility(component4 ? 0 : 8);
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.WidgetStickerSheet$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (!component4 || component3 != PremiumTier.TIER_2) {
                    WidgetStickerPackStoreSheet.Companion companion = WidgetStickerPackStoreSheet.Companion;
                    FragmentManager parentFragmentManager = WidgetStickerSheet.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion.show(parentFragmentManager, component2, StickerPackStoreSheetViewType.STICKER_POPOUT_VIEW_ALL, string, StickerPurchaseLocation.Companion.getPopoutPurchaseLocation(component1.canBePurchased()));
                } else {
                    ExpressionPickerEventBus.Companion.getINSTANCE().emitEvent(new ExpressionPickerEvent.OpenStickerPicker(component2.i(), null, false, 2, null));
                }
                WidgetStickerSheet.this.dismiss();
            }
        });
        RelativeLayout relativeLayout = getBinding().g;
        m.checkNotNullExpressionValue(relativeLayout, "binding.stickerSheetViewLimitedContainer");
        if (!component1.isLimitedPack()) {
            i = 8;
        }
        relativeLayout.setVisibility(i);
        Chip chip = getBinding().f;
        m.checkNotNullExpressionValue(chip, "binding.stickerSheetViewLimitedChip");
        Context requireContext2 = requireContext();
        m.checkNotNullExpressionValue(requireContext2, "requireContext()");
        chip.setText(stickerUtils.getLimitedTimeLeftString(requireContext2, component1.getStoreListing()));
    }

    private final WidgetStickerSheetBinding getBinding() {
        return (WidgetStickerSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final StickerSheetViewModel getViewModel() {
        return (StickerSheetViewModel) this.viewModel$delegate.getValue();
    }

    public static final void show(FragmentManager fragmentManager, Sticker sticker, long j) {
        Companion.show(fragmentManager, sticker, j);
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        Observable q = ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, WidgetStickerSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetStickerSheet$bindSubscriptions$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_sticker_sheet;
    }
}
