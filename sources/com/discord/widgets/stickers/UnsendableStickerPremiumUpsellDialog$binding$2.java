package com.discord.widgets.stickers;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.discord.databinding.StickerPremiumUpsellDialogBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: UnsendableStickerPremiumUpsellDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/StickerPremiumUpsellDialogBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/StickerPremiumUpsellDialogBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class UnsendableStickerPremiumUpsellDialog$binding$2 extends k implements Function1<View, StickerPremiumUpsellDialogBinding> {
    public static final UnsendableStickerPremiumUpsellDialog$binding$2 INSTANCE = new UnsendableStickerPremiumUpsellDialog$binding$2();

    public UnsendableStickerPremiumUpsellDialog$binding$2() {
        super(1, StickerPremiumUpsellDialogBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/StickerPremiumUpsellDialogBinding;", 0);
    }

    public final StickerPremiumUpsellDialogBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.sticker_premium_upsell_close_button;
        TextView textView = (TextView) view.findViewById(R.id.sticker_premium_upsell_close_button);
        if (textView != null) {
            i = R.id.sticker_premium_upsell_description;
            TextView textView2 = (TextView) view.findViewById(R.id.sticker_premium_upsell_description);
            if (textView2 != null) {
                i = R.id.sticker_premium_upsell_subscribe_button;
                MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.sticker_premium_upsell_subscribe_button);
                if (materialButton != null) {
                    return new StickerPremiumUpsellDialogBinding((RelativeLayout) view, textView, textView2, materialButton);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
