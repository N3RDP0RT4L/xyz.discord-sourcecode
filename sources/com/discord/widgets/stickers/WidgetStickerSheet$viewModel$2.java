package com.discord.widgets.stickers;

import android.os.Bundle;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppViewModel;
import com.discord.widgets.stickers.StickerSheetViewModel;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetStickerSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerSheet$viewModel$2 extends o implements Function0<AppViewModel<StickerSheetViewModel.ViewState>> {
    public final /* synthetic */ WidgetStickerSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStickerSheet$viewModel$2(WidgetStickerSheet widgetStickerSheet) {
        super(0);
        this.this$0 = widgetStickerSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<StickerSheetViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        Serializable serializable = argumentsOrDefault.getSerializable("com.discord.intent.extra.EXTRA_STICKER");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type com.discord.api.sticker.Sticker");
        Sticker sticker = (Sticker) serializable;
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        return new StickerSheetViewModel(sticker, null, null, argumentsOrDefault2.getString("widget_sticker_sheet_analytics_location"), null, 22, null);
    }
}
