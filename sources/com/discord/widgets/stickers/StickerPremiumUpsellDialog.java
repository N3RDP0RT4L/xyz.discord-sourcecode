package com.discord.widgets.stickers;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.PremiumStickerUpsellDialogBinding;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlaySku;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.settings.premium.WidgetSettingsPremium;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: StickerPremiumUpsellDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0007¢\u0006\u0004\b\f\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004R\u001d\u0010\u000b\u001a\u00020\u00068B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\n¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/stickers/StickerPremiumUpsellDialog;", "Lcom/discord/app/AppDialog;", "", "onStart", "()V", "onViewBoundOrOnResume", "Lcom/discord/databinding/PremiumStickerUpsellDialogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/PremiumStickerUpsellDialogBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerPremiumUpsellDialog extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(StickerPremiumUpsellDialog.class, "binding", "getBinding()Lcom/discord/databinding/PremiumStickerUpsellDialogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, StickerPremiumUpsellDialog$binding$2.INSTANCE, null, 2, null);

    /* compiled from: StickerPremiumUpsellDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/stickers/StickerPremiumUpsellDialog$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/utilities/analytics/Traits$Location;", "analyticsLocation", "", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/analytics/Traits$Location;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, Traits.Location location) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(location, "analyticsLocation");
            AnalyticsTracker.INSTANCE.openModal(Traits.Location.Section.STICKER_PREMIUM_TIER_2_UPSELL_MODAL, location);
            new StickerPremiumUpsellDialog().show(fragmentManager, StickerPremiumUpsellDialog.class.getSimpleName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public StickerPremiumUpsellDialog() {
        super(R.layout.premium_sticker_upsell_dialog);
    }

    private final PremiumStickerUpsellDialogBinding getBinding() {
        return (PremiumStickerUpsellDialogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        Dialog requireDialog = requireDialog();
        m.checkNotNullExpressionValue(requireDialog, "requireDialog()");
        Window window = requireDialog.getWindow();
        if (window != null) {
            window.setLayout(-1, -2);
        }
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        requireDialog().setCanceledOnTouchOutside(true);
        String skuPrice = PremiumUtils.INSTANCE.getSkuPrice(GooglePlaySku.PREMIUM_TIER_2_MONTHLY.getSkuName());
        if (skuPrice != null) {
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.premiumUpsellDescription");
            b.m(textView, R.string.premium_upsell_feature_pretext, new Object[]{skuPrice}, (r4 & 4) != 0 ? b.g.j : null);
        } else {
            TextView textView2 = getBinding().d;
            m.checkNotNullExpressionValue(textView2, "binding.premiumUpsellDescription");
            b.m(textView2, R.string.premium_upsell_feature_pretext_without_price, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        }
        TextView textView3 = getBinding().e;
        m.checkNotNullExpressionValue(textView3, "binding.premiumUpsellPerkBoosts");
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        b.m(textView3, R.string.premium_upsell_feature_free_guild_subscription, new Object[]{StringResourceUtilsKt.getI18nPluralString(requireContext, R.plurals.premium_upsell_feature_free_guild_subscription_numFreeGuildSubscriptions, 2, 2)}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.StickerPremiumUpsellDialog$onViewBoundOrOnResume$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StickerPremiumUpsellDialog.this.dismiss();
            }
        });
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.StickerPremiumUpsellDialog$onViewBoundOrOnResume$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StickerPremiumUpsellDialog.this.dismiss();
                WidgetSettingsPremium.Companion companion = WidgetSettingsPremium.Companion;
                Context requireContext2 = StickerPremiumUpsellDialog.this.requireContext();
                m.checkNotNullExpressionValue(requireContext2, "requireContext()");
                companion.launch(requireContext2, 1, Traits.Location.Section.STICKER_PREMIUM_TIER_2_UPSELL_MODAL);
            }
        });
        getBinding().f2119b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.stickers.StickerPremiumUpsellDialog$onViewBoundOrOnResume$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StickerPremiumUpsellDialog.this.dismiss();
            }
        });
    }
}
