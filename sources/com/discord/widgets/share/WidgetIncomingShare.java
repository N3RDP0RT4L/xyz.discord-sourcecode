package com.discord.widgets.share;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.a.c;
import b.a.d.j;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.api.activity.Activity;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.ViewImageBinding;
import com.discord.databinding.WidgetIncomingShareBinding;
import com.discord.models.deserialization.gson.InboundGatewayGsonParser;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreMessages;
import com.discord.stores.StoreSlowMode;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.ShareUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.attachments.AttachmentUtilsKt;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.rest.SendUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.recycler.PaddedItemDecorator;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.widgets.chat.list.ViewEmbedGameInvite;
import com.discord.widgets.share.WidgetIncomingShare;
import com.discord.widgets.user.search.WidgetGlobalSearchAdapter;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.textfield.TextInputLayout;
import com.lytefast.flexinput.model.Attachment;
import d0.g0.t;
import d0.l;
import d0.t.n;
import d0.t.u;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlinx.coroutines.CoroutineScope;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.functions.Action2;
import rx.functions.Func6;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetIncomingShare.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 ;2\u00020\u0001:\u0004<;=>B\u0007¢\u0006\u0004\b:\u0010\tJ\u001b\u0010\u0006\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJU\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00122\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u001b\u0010\tJ\u0013\u0010\u001c\u001a\u00020\u0005*\u00020\u0010H\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0013\u0010\u0006\u001a\u00020\u0005*\u00020\u0010H\u0003¢\u0006\u0004\b\u0006\u0010\u001dJ\u001b\u0010 \u001a\u00020\u00052\n\u0010\u001f\u001a\u00060\u001eR\u00020\u0000H\u0002¢\u0006\u0004\b \u0010!J\u0017\u0010$\u001a\u00020\u00052\u0006\u0010#\u001a\u00020\"H\u0016¢\u0006\u0004\b$\u0010%J\u000f\u0010&\u001a\u00020\u0005H\u0016¢\u0006\u0004\b&\u0010\tR\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b(\u0010)R\u001a\u0010\u001f\u001a\u00060\u001eR\u00020\u00008\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001f\u0010*R\u001d\u00100\u001a\u00020+8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R:\u00103\u001a&\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00100\u0010 2*\u0012\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00100\u0010\u0018\u000101018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R:\u00105\u001a&\u0012\f\u0012\n 2*\u0004\u0018\u00010\f0\f 2*\u0012\u0012\f\u0012\n 2*\u0004\u0018\u00010\f0\f\u0018\u000101018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00104R:\u00106\u001a&\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00170\u0017 2*\u0012\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00170\u0017\u0018\u000101018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00104R:\u00107\u001a&\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00170\u0017 2*\u0012\u0012\f\u0012\n 2*\u0004\u0018\u00010\u00170\u0017\u0018\u000101018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00104R\u0018\u00108\u001a\u0004\u0018\u00010\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00109¨\u0006?"}, d2 = {"Lcom/discord/widgets/share/WidgetIncomingShare;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/share/WidgetIncomingShare$Model;", "Lcom/discord/utilities/time/Clock;", "clock", "", "configureUi", "(Lcom/discord/widgets/share/WidgetIncomingShare$Model;Lcom/discord/utilities/time/Clock;)V", "finish", "()V", "Landroid/content/Context;", "context", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "receiver", "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "gameInviteModel", "Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "contentModel", "", "isOnCooldown", "", "maxFileSizeMB", "isUserPremium", "", "captchaKey", "onSendClicked", "(Landroid/content/Context;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;ZIZLjava/lang/String;)V", "onSendCompleted", "initialize", "(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)V", "Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;", "previewAdapter", "configureAdapter", "(Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;", "resultsAdapter", "Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;", "Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;", "Lcom/discord/databinding/WidgetIncomingShareBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetIncomingShareBinding;", "binding", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "contentPublisher", "Lrx/subjects/BehaviorSubject;", "selectedReceiverPublisher", "searchQueryPublisher", "commentPublisher", "queryString", "Ljava/lang/String;", HookHelper.constructorName, "Companion", "Adapter", "ContentModel", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetIncomingShare extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetIncomingShare.class, "binding", "getBinding()Lcom/discord/databinding/WidgetIncomingShareBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_RECIPIENT = "EXTRA_RECIPIENT";
    private static final int FLIPPER_RESULTS = 0;
    private static final int FLIPPER_RESULTS_EMPTY = 1;
    private static final int FLIPPER_SEARCH = 0;
    private static final int FLIPPER_SELECTED = 1;
    private Adapter previewAdapter;
    private String queryString;
    private WidgetGlobalSearchAdapter resultsAdapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetIncomingShare$binding$2.INSTANCE, null, 2, null);
    private final BehaviorSubject<ContentModel> contentPublisher = BehaviorSubject.k0();
    private final BehaviorSubject<String> commentPublisher = BehaviorSubject.l0("");
    private final BehaviorSubject<String> searchQueryPublisher = BehaviorSubject.l0("");
    private final BehaviorSubject<WidgetGlobalSearchModel.ItemDataPayload> selectedReceiverPublisher = BehaviorSubject.k0();

    /* compiled from: WidgetIncomingShare.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\n\b\u0086\u0004\u0018\u00002\u0010\u0012\f\u0012\n0\u0002R\u00060\u0000R\u00020\u00030\u0001:\u0001\u001cB'\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0016\b\u0002\u0010\t\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ+\u0010\n\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u00042\u0014\u0010\t\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\r\u001a\u00020\fH\u0016¢\u0006\u0004\b\r\u0010\u000eJ'\u0010\u0011\u001a\u00020\b2\u000e\u0010\u000f\u001a\n0\u0002R\u00060\u0000R\u00020\u00032\u0006\u0010\u0010\u001a\u00020\fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J'\u0010\u0016\u001a\n0\u0002R\u00060\u0000R\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\fH\u0016¢\u0006\u0004\b\u0016\u0010\u0017R$\u0010\t\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0012\u0004\u0012\u00020\b0\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\t\u0010\u0018R\u0016\u0010\u0005\u001a\u00020\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0005\u0010\u0019¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/share/WidgetIncomingShare$Adapter$ViewHolder;", "Lcom/discord/widgets/share/WidgetIncomingShare;", "Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "inputModel", "Lkotlin/Function1;", "Landroid/net/Uri;", "", "onItemClickListener", "setData", "(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Lkotlin/jvm/functions/Function1;)V", "", "getItemCount", "()I", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onBindViewHolder", "(Lcom/discord/widgets/share/WidgetIncomingShare$Adapter$ViewHolder;I)V", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/share/WidgetIncomingShare$Adapter$ViewHolder;", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", HookHelper.constructorName, "(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Lkotlin/jvm/functions/Function1;)V", "ViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class Adapter extends RecyclerView.Adapter<ViewHolder> {
        private ContentModel inputModel;
        private Function1<? super Uri, Unit> onItemClickListener;
        public final /* synthetic */ WidgetIncomingShare this$0;

        /* compiled from: WidgetIncomingShare.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/net/Uri;", "it", "", "invoke", "(Landroid/net/Uri;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.share.WidgetIncomingShare$Adapter$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends o implements Function1<Uri, Unit> {
            public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

            public AnonymousClass1() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Uri uri) {
                invoke2(uri);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(Uri uri) {
            }
        }

        /* compiled from: WidgetIncomingShare.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/share/WidgetIncomingShare$Adapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "", "bind", "(Landroid/net/Uri;)V", "Lcom/discord/databinding/ViewImageBinding;", "binding", "Lcom/discord/databinding/ViewImageBinding;", HookHelper.constructorName, "(Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;Lcom/discord/databinding/ViewImageBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final class ViewHolder extends RecyclerView.ViewHolder {
            private final ViewImageBinding binding;
            public final /* synthetic */ Adapter this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ViewHolder(Adapter adapter, ViewImageBinding viewImageBinding) {
                super(viewImageBinding.a);
                m.checkNotNullParameter(viewImageBinding, "binding");
                this.this$0 = adapter;
                this.binding = viewImageBinding;
            }

            public final void bind(final Uri uri) {
                this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.share.WidgetIncomingShare$Adapter$ViewHolder$bind$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        Function1 function1;
                        function1 = WidgetIncomingShare.Adapter.ViewHolder.this.this$0.onItemClickListener;
                        function1.invoke(uri);
                    }
                });
                this.binding.a.setImageURI(uri);
            }
        }

        public Adapter(WidgetIncomingShare widgetIncomingShare, ContentModel contentModel, Function1<? super Uri, Unit> function1) {
            m.checkNotNullParameter(contentModel, "inputModel");
            m.checkNotNullParameter(function1, "onItemClickListener");
            this.this$0 = widgetIncomingShare;
            this.inputModel = contentModel;
            this.onItemClickListener = function1;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            List<Uri> uris = this.inputModel.getUris();
            if (uris != null) {
                return uris.size();
            }
            return 0;
        }

        public final void setData(ContentModel contentModel, Function1<? super Uri, Unit> function1) {
            m.checkNotNullParameter(contentModel, "inputModel");
            m.checkNotNullParameter(function1, "onItemClickListener");
            this.inputModel = contentModel;
            this.onItemClickListener = function1;
        }

        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            m.checkNotNullParameter(viewHolder, "holder");
            List<Uri> uris = this.inputModel.getUris();
            viewHolder.bind(uris != null ? uris.get(i) : null);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_image, viewGroup, false);
            Objects.requireNonNull(inflate, "rootView");
            ViewImageBinding viewImageBinding = new ViewImageBinding((SimpleDraweeView) inflate);
            m.checkNotNullExpressionValue(viewImageBinding, "ViewImageBinding.inflate….context), parent, false)");
            return new ViewHolder(this, viewImageBinding);
        }

        public /* synthetic */ Adapter(WidgetIncomingShare widgetIncomingShare, ContentModel contentModel, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(widgetIncomingShare, contentModel, (i & 2) != 0 ? AnonymousClass1.INSTANCE : function1);
        }
    }

    /* compiled from: WidgetIncomingShare.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J-\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000eR\u0016\u0010\u0011\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000e¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/share/WidgetIncomingShare$Companion;", "", "Landroid/content/Context;", "context", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "recipient", "", "launch", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V", WidgetIncomingShare.EXTRA_RECIPIENT, "Ljava/lang/String;", "", "FLIPPER_RESULTS", "I", "FLIPPER_RESULTS_EMPTY", "FLIPPER_SEARCH", "FLIPPER_SELECTED", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, String str, String str2, int i, Object obj) {
            if ((i & 2) != 0) {
                str = null;
            }
            if ((i & 4) != 0) {
                str2 = null;
            }
            companion.launch(context, str, str2);
        }

        public final void launch(Context context, String str, String str2) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra("android.intent.extra.TEXT", str).putExtra(WidgetIncomingShare.EXTRA_RECIPIENT, str2);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…TRA_RECIPIENT, recipient)");
            j.d(context, WidgetIncomingShare.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetIncomingShare.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\b\u0018\u0000 22\u00020\u0001:\u00012BU\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b0\u00101J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J^\u0010\u001a\u001a\u00020\u00002\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00052\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0011HÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u000eJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010\"\u001a\u00020!2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010$\u001a\u0004\b%\u0010\u0010R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\u000eR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010(\u001a\u0004\b)\u0010\u0013R!\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010*\u001a\u0004\b+\u0010\bR\u001b\u0010\u0016\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010,\u001a\u0004\b-\u0010\u000bR\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010.\u001a\u0004\b/\u0010\u0004¨\u00063"}, d2 = {"Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "", "", "component1", "()Ljava/lang/CharSequence;", "", "Landroid/net/Uri;", "component2", "()Ljava/util/List;", "", "component3", "()Ljava/lang/Long;", "", "component4", "()Ljava/lang/String;", "component5", "()Landroid/net/Uri;", "Lcom/discord/api/activity/Activity;", "component6", "()Lcom/discord/api/activity/Activity;", "sharedText", "uris", "preselectedRecipientChannel", "recipient", "activityActionUri", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "copy", "(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/api/activity/Activity;)Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Landroid/net/Uri;", "getActivityActionUri", "Ljava/lang/String;", "getRecipient", "Lcom/discord/api/activity/Activity;", "getActivity", "Ljava/util/List;", "getUris", "Ljava/lang/Long;", "getPreselectedRecipientChannel", "Ljava/lang/CharSequence;", "getSharedText", HookHelper.constructorName, "(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/api/activity/Activity;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ContentModel {
        public static final Companion Companion = new Companion(null);
        private final Activity activity;
        private final Uri activityActionUri;
        private final Long preselectedRecipientChannel;
        private final String recipient;
        private final CharSequence sharedText;
        private final List<Uri> uris;

        /* compiled from: WidgetIncomingShare.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;", "", "Landroid/content/Intent;", "recentIntent", "Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "get", "(Landroid/content/Intent;)Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final ContentModel get(Intent intent) {
                m.checkNotNullParameter(intent, "recentIntent");
                ShareUtils.SharedContent sharedContent = ShareUtils.INSTANCE.getSharedContent(intent, false);
                Long directShareId = IntentUtils.INSTANCE.getDirectShareId(intent);
                String stringExtra = intent.getStringExtra(WidgetIncomingShare.EXTRA_RECIPIENT);
                String action = intent.getAction();
                Uri data = (action != null && action.hashCode() == -1103390587 && action.equals("com.discord.intent.action.SDK")) ? intent.getData() : null;
                String stringExtra2 = intent.getStringExtra("com.discord.intent.extra.EXTRA_ACTIVITY");
                return new ContentModel(sharedContent.getText(), sharedContent.getUris(), directShareId, stringExtra, data, stringExtra2 != null ? (Activity) InboundGatewayGsonParser.fromJson(new Model.JsonReader(new StringReader(stringExtra2)), Activity.class) : null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public ContentModel() {
            this(null, null, null, null, null, null, 63, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public ContentModel(CharSequence charSequence, List<? extends Uri> list, Long l, String str, Uri uri, Activity activity) {
            this.sharedText = charSequence;
            this.uris = list;
            this.preselectedRecipientChannel = l;
            this.recipient = str;
            this.activityActionUri = uri;
            this.activity = activity;
        }

        public static /* synthetic */ ContentModel copy$default(ContentModel contentModel, CharSequence charSequence, List list, Long l, String str, Uri uri, Activity activity, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = contentModel.sharedText;
            }
            List<Uri> list2 = list;
            if ((i & 2) != 0) {
                list2 = contentModel.uris;
            }
            List list3 = list2;
            if ((i & 4) != 0) {
                l = contentModel.preselectedRecipientChannel;
            }
            Long l2 = l;
            if ((i & 8) != 0) {
                str = contentModel.recipient;
            }
            String str2 = str;
            if ((i & 16) != 0) {
                uri = contentModel.activityActionUri;
            }
            Uri uri2 = uri;
            if ((i & 32) != 0) {
                activity = contentModel.activity;
            }
            return contentModel.copy(charSequence, list3, l2, str2, uri2, activity);
        }

        public final CharSequence component1() {
            return this.sharedText;
        }

        public final List<Uri> component2() {
            return this.uris;
        }

        public final Long component3() {
            return this.preselectedRecipientChannel;
        }

        public final String component4() {
            return this.recipient;
        }

        public final Uri component5() {
            return this.activityActionUri;
        }

        public final Activity component6() {
            return this.activity;
        }

        public final ContentModel copy(CharSequence charSequence, List<? extends Uri> list, Long l, String str, Uri uri, Activity activity) {
            return new ContentModel(charSequence, list, l, str, uri, activity);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ContentModel)) {
                return false;
            }
            ContentModel contentModel = (ContentModel) obj;
            return m.areEqual(this.sharedText, contentModel.sharedText) && m.areEqual(this.uris, contentModel.uris) && m.areEqual(this.preselectedRecipientChannel, contentModel.preselectedRecipientChannel) && m.areEqual(this.recipient, contentModel.recipient) && m.areEqual(this.activityActionUri, contentModel.activityActionUri) && m.areEqual(this.activity, contentModel.activity);
        }

        public final Activity getActivity() {
            return this.activity;
        }

        public final Uri getActivityActionUri() {
            return this.activityActionUri;
        }

        public final Long getPreselectedRecipientChannel() {
            return this.preselectedRecipientChannel;
        }

        public final String getRecipient() {
            return this.recipient;
        }

        public final CharSequence getSharedText() {
            return this.sharedText;
        }

        public final List<Uri> getUris() {
            return this.uris;
        }

        public int hashCode() {
            CharSequence charSequence = this.sharedText;
            int i = 0;
            int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
            List<Uri> list = this.uris;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            Long l = this.preselectedRecipientChannel;
            int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
            String str = this.recipient;
            int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
            Uri uri = this.activityActionUri;
            int hashCode5 = (hashCode4 + (uri != null ? uri.hashCode() : 0)) * 31;
            Activity activity = this.activity;
            if (activity != null) {
                i = activity.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ContentModel(sharedText=");
            R.append(this.sharedText);
            R.append(", uris=");
            R.append(this.uris);
            R.append(", preselectedRecipientChannel=");
            R.append(this.preselectedRecipientChannel);
            R.append(", recipient=");
            R.append(this.recipient);
            R.append(", activityActionUri=");
            R.append(this.activityActionUri);
            R.append(", activity=");
            R.append(this.activity);
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ ContentModel(String str, List list, Long l, String str2, Uri uri, Activity activity, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? null : list, (i & 4) != 0 ? null : l, (i & 8) != 0 ? null : str2, (i & 16) != 0 ? null : uri, (i & 32) == 0 ? activity : null);
        }
    }

    /* compiled from: WidgetIncomingShare.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\f\u001a\u00020\u000b\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\b\u0010!\u001a\u0004\u0018\u00010 \u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016\u0012\u0006\u0010\u0010\u001a\u00020\u0007\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b%\u0010&R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\b\u0010\nR\u0019\u0010\f\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0010\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\t\u001a\u0004\b\u0010\u0010\nR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u0019\u0010\u001c\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u001b\u0010!\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$¨\u0006'"}, d2 = {"Lcom/discord/widgets/share/WidgetIncomingShare$Model;", "", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;", "searchModel", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;", "getSearchModel", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;", "", "isUserPremium", "Z", "()Z", "Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "contentModel", "Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "getContentModel", "()Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "isOnCooldown", "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "gameInviteModel", "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "getGameInviteModel", "()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "receiver", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "getReceiver", "()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;", "", "maxFileSizeMB", "I", "getMaxFileSizeMB", "()I", "", "comment", "Ljava/lang/String;", "getComment", "()Ljava/lang/String;", HookHelper.constructorName, "(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Ljava/lang/String;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;ZIZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        private final String comment;
        private final ContentModel contentModel;
        private final ViewEmbedGameInvite.Model gameInviteModel;
        private final boolean isOnCooldown;
        private final boolean isUserPremium;
        private final int maxFileSizeMB;
        private final WidgetGlobalSearchModel.ItemDataPayload receiver;
        private final WidgetGlobalSearchModel searchModel;

        public Model(ContentModel contentModel, ViewEmbedGameInvite.Model model, String str, WidgetGlobalSearchModel widgetGlobalSearchModel, WidgetGlobalSearchModel.ItemDataPayload itemDataPayload, boolean z2, int i, boolean z3) {
            m.checkNotNullParameter(contentModel, "contentModel");
            m.checkNotNullParameter(widgetGlobalSearchModel, "searchModel");
            this.contentModel = contentModel;
            this.gameInviteModel = model;
            this.comment = str;
            this.searchModel = widgetGlobalSearchModel;
            this.receiver = itemDataPayload;
            this.isOnCooldown = z2;
            this.maxFileSizeMB = i;
            this.isUserPremium = z3;
        }

        public final String getComment() {
            return this.comment;
        }

        public final ContentModel getContentModel() {
            return this.contentModel;
        }

        public final ViewEmbedGameInvite.Model getGameInviteModel() {
            return this.gameInviteModel;
        }

        public final int getMaxFileSizeMB() {
            return this.maxFileSizeMB;
        }

        public final WidgetGlobalSearchModel.ItemDataPayload getReceiver() {
            return this.receiver;
        }

        public final WidgetGlobalSearchModel getSearchModel() {
            return this.searchModel;
        }

        public final boolean isOnCooldown() {
            return this.isOnCooldown;
        }

        public final boolean isUserPremium() {
            return this.isUserPremium;
        }
    }

    public WidgetIncomingShare() {
        super(R.layout.widget_incoming_share);
    }

    private final void configureAdapter(Adapter adapter) {
        RecyclerView recyclerView = getBinding().e;
        m.checkNotNullExpressionValue(recyclerView, "binding.externalShareList");
        recyclerView.setAdapter(adapter);
        ViewCompat.setNestedScrollingEnabled(getBinding().e, false);
        getBinding().e.setHasFixedSize(true);
        int dpToPixels = DimenUtils.dpToPixels(8);
        getBinding().e.addItemDecoration(new PaddedItemDecorator(0, dpToPixels, dpToPixels, false, 8, null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUi(final Model model, Clock clock) {
        List<Uri> uris;
        configureUi(model.getContentModel());
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.externalShareComment");
        ViewExtensions.setOnEditorActionListener(textInputLayout, new WidgetIncomingShare$configureUi$1(this, model));
        boolean z2 = true;
        int i = 0;
        if (model.getReceiver() != null) {
            AppViewFlipper appViewFlipper = getBinding().k;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.searchFlipper");
            appViewFlipper.setDisplayedChild(1);
            TextInputLayout textInputLayout2 = getBinding().g;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.externalShareSearch");
            textInputLayout2.setFocusable(false);
            TextInputLayout textInputLayout3 = getBinding().d;
            m.checkNotNullExpressionValue(textInputLayout3, "binding.externalShareComment");
            EditText editText = textInputLayout3.getEditText();
            if (editText != null) {
                editText.setImeOptions(4);
            }
            String comment = model.getComment();
            setActionBarOptionsMenu(!(comment == null || t.isBlank(comment)) || (((uris = model.getContentModel().getUris()) != null && !uris.isEmpty()) || model.getGameInviteModel() != null) ? R.menu.menu_external_share : R.menu.menu_empty, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.share.WidgetIncomingShare$configureUi$2
                public final void call(MenuItem menuItem, Context context) {
                    m.checkNotNullExpressionValue(menuItem, "menuItem");
                    if (menuItem.getItemId() == R.id.menu_send) {
                        WidgetIncomingShare widgetIncomingShare = WidgetIncomingShare.this;
                        m.checkNotNullExpressionValue(context, "ctx");
                        widgetIncomingShare.onSendClicked(context, model.getReceiver(), model.getGameInviteModel(), model.getContentModel(), model.isOnCooldown(), model.getMaxFileSizeMB(), model.isUserPremium(), (r19 & 128) != 0 ? null : null);
                    }
                }
            }, null);
            WidgetGlobalSearchModel.ItemDataPayload receiver = model.getReceiver();
            if (receiver instanceof WidgetGlobalSearchModel.ItemUser) {
                getBinding().l.onConfigure((WidgetGlobalSearchModel.ItemUser) model.getReceiver());
            } else if (receiver instanceof WidgetGlobalSearchModel.ItemChannel) {
                getBinding().l.onConfigure((WidgetGlobalSearchModel.ItemChannel) model.getReceiver());
            } else if (receiver instanceof WidgetGlobalSearchModel.ItemGuild) {
                getBinding().l.onConfigure((WidgetGlobalSearchModel.ItemGuild) model.getReceiver());
            }
        } else {
            AppViewFlipper appViewFlipper2 = getBinding().k;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.searchFlipper");
            appViewFlipper2.setDisplayedChild(0);
            TextInputLayout textInputLayout4 = getBinding().g;
            m.checkNotNullExpressionValue(textInputLayout4, "binding.externalShareSearch");
            textInputLayout4.setFocusable(true);
            TextInputLayout textInputLayout5 = getBinding().g;
            m.checkNotNullExpressionValue(textInputLayout5, "binding.externalShareSearch");
            textInputLayout5.setFocusableInTouchMode(true);
            TextInputLayout textInputLayout6 = getBinding().d;
            m.checkNotNullExpressionValue(textInputLayout6, "binding.externalShareComment");
            EditText editText2 = textInputLayout6.getEditText();
            if (editText2 != null) {
                editText2.setImeOptions(5);
            }
            AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_empty, null, null, 4, null);
        }
        AppViewFlipper appViewFlipper3 = getBinding().i;
        m.checkNotNullExpressionValue(appViewFlipper3, "binding.incomingShareResultsFlipper");
        appViewFlipper3.setDisplayedChild(model.getSearchModel().getData().isEmpty() ? 1 : 0);
        WidgetGlobalSearchAdapter widgetGlobalSearchAdapter = this.resultsAdapter;
        if (widgetGlobalSearchAdapter == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        widgetGlobalSearchAdapter.setOnUpdated(new WidgetIncomingShare$configureUi$$inlined$apply$lambda$1(this, model));
        List<WidgetGlobalSearchModel.ItemDataPayload> data = model.getSearchModel().getData();
        ArrayList arrayList = new ArrayList();
        for (Object obj : data) {
            String key = ((WidgetGlobalSearchModel.ItemDataPayload) obj).getKey();
            WidgetGlobalSearchModel.ItemDataPayload receiver2 = model.getReceiver();
            if (!m.areEqual(key, receiver2 != null ? receiver2.getKey() : null)) {
                arrayList.add(obj);
            }
        }
        widgetGlobalSearchAdapter.setData(arrayList);
        widgetGlobalSearchAdapter.setOnSelectedListener(new WidgetIncomingShare$configureUi$$inlined$apply$lambda$2(this, model));
        ViewEmbedGameInvite viewEmbedGameInvite = getBinding().c;
        m.checkNotNullExpressionValue(viewEmbedGameInvite, "binding.externalShareActivityActionPreview");
        if (model.getGameInviteModel() == null) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        viewEmbedGameInvite.setVisibility(i);
        ViewEmbedGameInvite.Model gameInviteModel = model.getGameInviteModel();
        if (gameInviteModel != null) {
            getBinding().c.bind(gameInviteModel, clock);
        }
    }

    private final void finish() {
        FragmentActivity activity = e();
        if (activity != null) {
            activity.finish();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetIncomingShareBinding getBinding() {
        return (WidgetIncomingShareBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void initialize(ContentModel contentModel) {
        Context context;
        int i = 0;
        if (!(contentModel.getPreselectedRecipientChannel() == null || (context = getContext()) == null)) {
            ChannelSelector.Companion.getInstance().findAndSet(context, contentModel.getPreselectedRecipientChannel().longValue());
            m.checkNotNullExpressionValue(context, "context");
            j.c(context, false, getMostRecentIntent().setFlags(268468225), 2);
            finish();
        }
        TextInputLayout textInputLayout = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout, "binding.externalShareSearch");
        ViewExtensions.setText(textInputLayout, contentModel.getRecipient());
        TextInputLayout textInputLayout2 = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.externalShareSearch");
        ViewExtensions.setSelectionEnd(textInputLayout2);
        TextInputLayout textInputLayout3 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.externalShareComment");
        ViewExtensions.setText(textInputLayout3, contentModel.getSharedText());
        this.contentPublisher.onNext(contentModel);
        List<Uri> uris = contentModel.getUris();
        if (uris != null) {
            for (Object obj : uris) {
                i++;
                if (i < 0) {
                    n.throwIndexOverflow();
                }
                Uri uri = (Uri) obj;
                Context context2 = getContext();
                AnalyticsTracker.addAttachment(AnalyticsTracker.ATTACHMENT_SOURCE_SHARE, AttachmentUtilsKt.getMimeType$default(context2 != null ? context2.getContentResolver() : null, uri, null, 4, null), i);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onSendClicked(Context context, WidgetGlobalSearchModel.ItemDataPayload itemDataPayload, final ViewEmbedGameInvite.Model model, ContentModel contentModel, boolean z2, int i, boolean z3, final String str) {
        Observable.c cVar;
        final List<Attachment> list;
        boolean z4;
        boolean z5;
        boolean z6;
        int i2;
        if (z2) {
            b.a.d.m.g(context, R.string.channel_slowmode_desc_short, 0, null, 12);
            return;
        }
        if (itemDataPayload instanceof WidgetGlobalSearchModel.ItemChannel) {
            WidgetGlobalSearchModel.ItemChannel itemChannel = (WidgetGlobalSearchModel.ItemChannel) itemDataPayload;
            ChannelSelector.selectChannel$default(ChannelSelector.Companion.getInstance(), itemChannel.getChannel(), null, null, 6, null);
            cVar = b.a.d.o.d(new WidgetIncomingShare$onSendClicked$filter$1(itemDataPayload), itemChannel.getChannel(), 0L, null, 12);
        } else if (itemDataPayload instanceof WidgetGlobalSearchModel.ItemUser) {
            ChannelSelector.Companion.getInstance().findAndSetDirectMessage(context, ((WidgetGlobalSearchModel.ItemUser) itemDataPayload).getUser().getId());
            cVar = b.a.d.o.d(new WidgetIncomingShare$onSendClicked$filter$2(itemDataPayload), itemDataPayload.getChannel(), 0L, null, 12);
        } else if (itemDataPayload instanceof WidgetGlobalSearchModel.ItemGuild) {
            StoreStream.Companion.getGuildSelected().set(((WidgetGlobalSearchModel.ItemGuild) itemDataPayload).getGuild().getId());
            cVar = b.a.d.o.d(new WidgetIncomingShare$onSendClicked$filter$3(itemDataPayload), itemDataPayload.getChannel(), 0L, null, 12);
        } else {
            return;
        }
        List<Uri> uris = contentModel.getUris();
        if (uris != null) {
            list = new ArrayList(d0.t.o.collectionSizeOrDefault(uris, 10));
            for (Uri uri : uris) {
                Attachment.Companion companion = Attachment.Companion;
                ContentResolver contentResolver = context.getContentResolver();
                m.checkNotNullExpressionValue(contentResolver, "context.contentResolver");
                list.add(companion.b(uri, contentResolver));
            }
        } else {
            list = n.emptyList();
        }
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        for (Attachment attachment : list) {
            Uri uri2 = attachment.getUri();
            ContentResolver contentResolver2 = context.getContentResolver();
            m.checkNotNullExpressionValue(contentResolver2, "context.contentResolver");
            arrayList.add(Float.valueOf(SendUtilsKt.computeFileSizeMegabytes(uri2, contentResolver2)));
        }
        Float maxOrNull = u.m87maxOrNull((Iterable<Float>) arrayList);
        float floatValue = maxOrNull != null ? maxOrNull.floatValue() : 0.0f;
        float sumOfFloat = u.sumOfFloat(arrayList);
        boolean z7 = list instanceof Collection;
        if (!z7 || !list.isEmpty()) {
            for (Attachment attachment2 : list) {
                if (AttachmentUtilsKt.isImage(attachment2, context.getContentResolver())) {
                    z4 = true;
                    break;
                }
            }
        }
        z4 = false;
        if (!z7 || !list.isEmpty()) {
            for (Attachment attachment3 : list) {
                if (AttachmentUtilsKt.isVideo(attachment3, context.getContentResolver())) {
                    z5 = true;
                    break;
                }
            }
        }
        z5 = false;
        if (!z7 || !list.isEmpty()) {
            for (Attachment attachment4 : list) {
                if (AttachmentUtilsKt.isGif(attachment4, context.getContentResolver())) {
                    i2 = i;
                    z6 = true;
                    break;
                }
            }
        }
        i2 = i;
        z6 = false;
        if (sumOfFloat > ((float) i2)) {
            c.b bVar = c.k;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            bVar.a(parentFragmentManager, z3, i, floatValue, sumOfFloat, null, list.size(), z4, z5, z6);
            return;
        }
        StoreStream.Companion companion2 = StoreStream.Companion;
        Observable observeMe$default = StoreUser.observeMe$default(companion2.getUsers(), false, 1, null);
        Observable<R> k = companion2.getChannelsSelected().observeSelectedChannel().k(cVar);
        m.checkNotNullExpressionValue(k, "StoreStream\n            …         .compose(filter)");
        Observable z8 = Observable.j(observeMe$default, ObservableExtensionsKt.takeSingleUntilTimeout$default(k, 1000L, false, 2, null), WidgetIncomingShare$onSendClicked$1.INSTANCE).Z(1).z(new b<Pair<? extends MeUser, ? extends Channel>, Observable<? extends Pair<? extends MeUser, ? extends MessageResult>>>() { // from class: com.discord.widgets.share.WidgetIncomingShare$onSendClicked$2
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends Pair<? extends MeUser, ? extends MessageResult>> call(Pair<? extends MeUser, ? extends Channel> pair) {
                return call2((Pair<MeUser, Channel>) pair);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends Pair<MeUser, MessageResult>> call2(Pair<MeUser, Channel> pair) {
                WidgetIncomingShareBinding binding;
                final MeUser component1 = pair.component1();
                Channel component2 = pair.component2();
                if (component2 == null) {
                    return j0.l.a.c.k;
                }
                StoreMessages messages = StoreStream.Companion.getMessages();
                long h = component2.h();
                m.checkNotNullExpressionValue(component1, "meUser");
                binding = WidgetIncomingShare.this.getBinding();
                TextInputLayout textInputLayout = binding.d;
                m.checkNotNullExpressionValue(textInputLayout, "binding.externalShareComment");
                String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
                List list2 = list;
                ViewEmbedGameInvite.Model model2 = model;
                Application application = model2 != null ? model2.getApplication() : null;
                ViewEmbedGameInvite.Model model3 = model;
                Activity activity = model3 != null ? model3.getActivity() : null;
                ViewEmbedGameInvite.Model model4 = model;
                return StoreMessages.sendMessage$default(messages, h, component1, textOrEmpty, null, list2, null, null, null, application, activity, model4 != null ? model4.getMessageActivity() : null, null, null, null, str, 14464, null).F(new b<MessageResult, Pair<? extends MeUser, ? extends MessageResult>>() { // from class: com.discord.widgets.share.WidgetIncomingShare$onSendClicked$2$$special$$inlined$let$lambda$1
                    public final Pair<MeUser, MessageResult> call(MessageResult messageResult) {
                        return d0.o.to(component1, messageResult);
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(z8, "Observable\n        .comb…ervable.empty()\n        }");
        WidgetGlobalSearchAdapter widgetGlobalSearchAdapter = this.resultsAdapter;
        if (widgetGlobalSearchAdapter == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.withDimmer(ObservableExtensionsKt.ui(z8, this, widgetGlobalSearchAdapter), getBinding().f2456b, 0L), WidgetIncomingShare.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetIncomingShare$onSendClicked$3(this, list, context, itemDataPayload, model, contentModel, z2, i, z3, z4, z5, z6));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onSendCompleted() {
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "this.context ?: return");
            if (!m.areEqual(getMostRecentIntent().getAction(), "com.discord.intent.action.SDK") && getMostRecentIntent().getBooleanExtra("com.discord.intent.extra.EXTRA_CONTINUE_IN_APP", true)) {
                j.c(context, false, new Intent().addFlags(268468224), 2);
            }
            finish();
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        Context context = getContext();
        if (context == null) {
            finish();
            return;
        }
        Adapter adapter = new Adapter(this, new ContentModel(null, null, null, null, null, null, 63, null), null, 2, null);
        this.previewAdapter = adapter;
        if (adapter == null) {
            m.throwUninitializedPropertyAccessException("previewAdapter");
        }
        configureAdapter(adapter);
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.externalShareComment");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetIncomingShare$onViewBound$1(this));
        RecyclerView recyclerView = getBinding().h;
        m.checkNotNullExpressionValue(recyclerView, "binding.externalShareSearchResults");
        this.resultsAdapter = (WidgetGlobalSearchAdapter) MGRecyclerAdapter.Companion.configure(new WidgetGlobalSearchAdapter(recyclerView));
        ViewCompat.setNestedScrollingEnabled(getBinding().h, false);
        getBinding().h.setHasFixedSize(false);
        TextInputLayout textInputLayout2 = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.externalShareSearch");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, this, new WidgetIncomingShare$onViewBound$2(this));
        TextInputLayout textInputLayout3 = getBinding().g;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.externalShareSearch");
        ViewExtensions.setOnEditTextFocusChangeListener(textInputLayout3, new View.OnFocusChangeListener() { // from class: com.discord.widgets.share.WidgetIncomingShare$onViewBound$3
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view2, boolean z2) {
                WidgetIncomingShareBinding binding;
                WidgetIncomingShareBinding binding2;
                if (z2) {
                    binding = WidgetIncomingShare.this.getBinding();
                    NestedScrollView nestedScrollView = binding.j;
                    binding2 = WidgetIncomingShare.this.getBinding();
                    TextInputLayout textInputLayout4 = binding2.d;
                    m.checkNotNullExpressionValue(textInputLayout4, "binding.externalShareComment");
                    nestedScrollView.smoothScrollTo(0, textInputLayout4.getBottom());
                }
            }
        });
        getBinding().m.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.share.WidgetIncomingShare$onViewBound$4

            /* compiled from: WidgetIncomingShare.kt */
            @e(c = "com.discord.widgets.share.WidgetIncomingShare$onViewBound$4$1", f = "WidgetIncomingShare.kt", l = {Opcodes.L2F}, m = "invokeSuspend")
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.share.WidgetIncomingShare$onViewBound$4$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
                public int label;

                public AnonymousClass1(Continuation continuation) {
                    super(2, continuation);
                }

                @Override // d0.w.i.a.a
                public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
                    m.checkNotNullParameter(continuation, "completion");
                    return new AnonymousClass1(continuation);
                }

                @Override // kotlin.jvm.functions.Function2
                public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
                    return ((AnonymousClass1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
                }

                @Override // d0.w.i.a.a
                public final Object invokeSuspend(Object obj) {
                    WidgetIncomingShareBinding binding;
                    Object coroutine_suspended = d0.w.h.c.getCOROUTINE_SUSPENDED();
                    int i = this.label;
                    if (i == 0) {
                        l.throwOnFailure(obj);
                        this.label = 1;
                        if (f.P(200L, this) == coroutine_suspended) {
                            return coroutine_suspended;
                        }
                    } else if (i == 1) {
                        l.throwOnFailure(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    binding = WidgetIncomingShare.this.getBinding();
                    binding.g.requestFocus();
                    return Unit.a;
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetIncomingShareBinding binding;
                BehaviorSubject behaviorSubject;
                binding = WidgetIncomingShare.this.getBinding();
                TextInputLayout textInputLayout4 = binding.g;
                m.checkNotNullExpressionValue(textInputLayout4, "binding.externalShareSearch");
                CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(textInputLayout4);
                if (coroutineScope != null) {
                    f.H0(coroutineScope, null, null, new AnonymousClass1(null), 3, null);
                }
                behaviorSubject = WidgetIncomingShare.this.selectedReceiverPublisher;
                behaviorSubject.onNext(null);
            }
        });
        this.selectedReceiverPublisher.onNext(null);
        AppViewFlipper appViewFlipper = getBinding().k;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.searchFlipper");
        appViewFlipper.setInAnimation(AnimationUtils.loadAnimation(context, R.anim.abc_grow_fade_in_from_bottom));
        AppViewFlipper appViewFlipper2 = getBinding().k;
        m.checkNotNullExpressionValue(appViewFlipper2, "binding.searchFlipper");
        appViewFlipper2.setOutAnimation(AnimationUtils.loadAnimation(context, R.anim.abc_shrink_fade_out_from_bottom));
        setOnNewIntentListener(new WidgetIncomingShare$onViewBound$5(this));
        initialize(ContentModel.Companion.get(getMostRecentIntent()));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        BehaviorSubject<String> behaviorSubject = this.searchQueryPublisher;
        m.checkNotNullExpressionValue(behaviorSubject, "searchQueryPublisher");
        StoreGuilds.Actions.requestMembers(this, behaviorSubject, false);
        Clock clock = ClockFactory.get();
        Observable<R> Y = this.selectedReceiverPublisher.q().Y(new b<WidgetGlobalSearchModel.ItemDataPayload, Observable<? extends Model>>() { // from class: com.discord.widgets.share.WidgetIncomingShare$onViewBoundOrOnResume$1

            /* compiled from: WidgetIncomingShare.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004 \u0001*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;", "kotlin.jvm.PlatformType", "it", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.share.WidgetIncomingShare$onViewBoundOrOnResume$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1<T, R> implements b<WidgetIncomingShare.ContentModel, Observable<? extends ViewEmbedGameInvite.Model>> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public final Observable<? extends ViewEmbedGameInvite.Model> call(WidgetIncomingShare.ContentModel contentModel) {
                    return ViewEmbedGameInvite.Model.Companion.getForShare(ClockFactory.get(), contentModel.getActivityActionUri(), contentModel.getActivity());
                }
            }

            /* compiled from: WidgetIncomingShare.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Integer;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.share.WidgetIncomingShare$onViewBoundOrOnResume$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2<T, R> implements b<Integer, Boolean> {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                public final Boolean call(Integer num) {
                    return Boolean.valueOf(num.intValue() > 0);
                }
            }

            public final Observable<? extends WidgetIncomingShare.Model> call(final WidgetGlobalSearchModel.ItemDataPayload itemDataPayload) {
                BehaviorSubject behaviorSubject2;
                BehaviorSubject behaviorSubject3;
                BehaviorSubject behaviorSubject4;
                BehaviorSubject behaviorSubject5;
                Channel channel;
                behaviorSubject2 = WidgetIncomingShare.this.contentPublisher;
                behaviorSubject3 = WidgetIncomingShare.this.contentPublisher;
                Observable<R> Y2 = behaviorSubject3.Y(AnonymousClass1.INSTANCE);
                behaviorSubject4 = WidgetIncomingShare.this.commentPublisher;
                m.checkNotNullExpressionValue(behaviorSubject4, "commentPublisher");
                Observable leadingEdgeThrottle = ObservableExtensionsKt.leadingEdgeThrottle(behaviorSubject4, 500L, TimeUnit.MILLISECONDS);
                WidgetGlobalSearchModel.Companion companion = WidgetGlobalSearchModel.Companion;
                behaviorSubject5 = WidgetIncomingShare.this.searchQueryPublisher;
                m.checkNotNullExpressionValue(behaviorSubject5, "searchQueryPublisher");
                Observable forSend$default = WidgetGlobalSearchModel.Companion.getForSend$default(companion, behaviorSubject5, null, 2, null);
                StoreStream.Companion companion2 = StoreStream.Companion;
                return Observable.f(behaviorSubject2, Y2, leadingEdgeThrottle, forSend$default, companion2.getSlowMode().observeCooldownSecs((itemDataPayload == null || (channel = itemDataPayload.getChannel()) == null) ? null : Long.valueOf(channel.h()), StoreSlowMode.Type.MessageSend.INSTANCE).F(AnonymousClass2.INSTANCE).q(), StoreUser.observeMe$default(companion2.getUsers(), false, 1, null), new Func6<WidgetIncomingShare.ContentModel, ViewEmbedGameInvite.Model, String, WidgetGlobalSearchModel, Boolean, MeUser, WidgetIncomingShare.Model>() { // from class: com.discord.widgets.share.WidgetIncomingShare$onViewBoundOrOnResume$1.3
                    public final WidgetIncomingShare.Model call(WidgetIncomingShare.ContentModel contentModel, ViewEmbedGameInvite.Model model, String str, WidgetGlobalSearchModel widgetGlobalSearchModel, Boolean bool, MeUser meUser) {
                        Guild guild;
                        WidgetGlobalSearchModel.ItemDataPayload itemDataPayload2 = WidgetGlobalSearchModel.ItemDataPayload.this;
                        int i = 0;
                        if (itemDataPayload2 instanceof WidgetGlobalSearchModel.ItemGuild) {
                            i = GuildUtilsKt.getMaxFileSizeMB(((WidgetGlobalSearchModel.ItemGuild) itemDataPayload2).getGuild());
                        } else if ((itemDataPayload2 instanceof WidgetGlobalSearchModel.ItemChannel) && (guild = ((WidgetGlobalSearchModel.ItemChannel) itemDataPayload2).getGuild()) != null) {
                            i = GuildUtilsKt.getMaxFileSizeMB(guild);
                        }
                        m.checkNotNullExpressionValue(contentModel, "contentModel");
                        m.checkNotNullExpressionValue(widgetGlobalSearchModel, "searchModel");
                        WidgetGlobalSearchModel.ItemDataPayload itemDataPayload3 = WidgetGlobalSearchModel.ItemDataPayload.this;
                        m.checkNotNullExpressionValue(bool, "isOnCooldown");
                        boolean booleanValue = bool.booleanValue();
                        int max = Math.max(i, PremiumUtils.INSTANCE.getMaxFileSizeMB(meUser));
                        UserUtils userUtils = UserUtils.INSTANCE;
                        m.checkNotNullExpressionValue(meUser, "meUser");
                        return new WidgetIncomingShare.Model(contentModel, model, str, widgetGlobalSearchModel, itemDataPayload3, booleanValue, max, userUtils.isPremium(meUser));
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "selectedReceiverPublishe…  )\n          }\n        }");
        Observable computationLatest = ObservableExtensionsKt.computationLatest(Y);
        WidgetGlobalSearchAdapter widgetGlobalSearchAdapter = this.resultsAdapter;
        if (widgetGlobalSearchAdapter == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(computationLatest, this, widgetGlobalSearchAdapter), WidgetIncomingShare.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetIncomingShare$onViewBoundOrOnResume$2(this, clock));
    }

    @SuppressLint({"NotifyDataSetChanged"})
    private final void configureUi(ContentModel contentModel) {
        Adapter adapter = this.previewAdapter;
        if (adapter == null) {
            m.throwUninitializedPropertyAccessException("previewAdapter");
        }
        adapter.setData(contentModel, new WidgetIncomingShare$configureUi$5(this, contentModel));
        Adapter adapter2 = this.previewAdapter;
        if (adapter2 == null) {
            m.throwUninitializedPropertyAccessException("previewAdapter");
        }
        adapter2.notifyDataSetChanged();
        List<Uri> uris = contentModel.getUris();
        int i = 0;
        boolean z2 = uris != null && !uris.isEmpty();
        CardView cardView = getBinding().f;
        m.checkNotNullExpressionValue(cardView, "binding.externalShareListWrap");
        if (!z2) {
            i = 8;
        }
        cardView.setVisibility(i);
        getBinding().d.setHint(z2 ? R.string.add_a_comment_optional : R.string.upload_area_leave_a_comment);
    }
}
