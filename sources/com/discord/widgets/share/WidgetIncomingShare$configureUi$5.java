package com.discord.widgets.share;

import android.net.Uri;
import com.discord.widgets.share.WidgetIncomingShare;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.BehaviorSubject;
/* compiled from: WidgetIncomingShare.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/net/Uri;", "selectedUri", "", "invoke", "(Landroid/net/Uri;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetIncomingShare$configureUi$5 extends o implements Function1<Uri, Unit> {
    public final /* synthetic */ WidgetIncomingShare.ContentModel $this_configureUi;
    public final /* synthetic */ WidgetIncomingShare this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetIncomingShare$configureUi$5(WidgetIncomingShare widgetIncomingShare, WidgetIncomingShare.ContentModel contentModel) {
        super(1);
        this.this$0 = widgetIncomingShare;
        this.$this_configureUi = contentModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Uri uri) {
        invoke2(uri);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Uri uri) {
        ArrayList arrayList;
        BehaviorSubject behaviorSubject;
        WidgetIncomingShare.ContentModel contentModel = this.$this_configureUi;
        List<Uri> uris = contentModel.getUris();
        if (uris != null) {
            arrayList = new ArrayList();
            for (Object obj : uris) {
                if (!m.areEqual((Uri) obj, uri)) {
                    arrayList.add(obj);
                }
            }
        } else {
            arrayList = null;
        }
        WidgetIncomingShare.ContentModel copy$default = WidgetIncomingShare.ContentModel.copy$default(contentModel, null, arrayList, null, null, null, null, 61, null);
        behaviorSubject = this.this$0.contentPublisher;
        behaviorSubject.onNext(copy$default);
    }
}
