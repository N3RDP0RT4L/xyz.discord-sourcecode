package com.discord.widgets.share;

import android.content.Context;
import android.content.res.Resources;
import androidx.fragment.app.FragmentManager;
import b.a.a.c;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.captcha.CaptchaErrorBody;
import com.discord.utilities.error.Error;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.SendUtils;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.captcha.WidgetCaptchaBottomSheet;
import com.discord.widgets.chat.list.ViewEmbedGameInvite;
import com.discord.widgets.share.WidgetIncomingShare;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import com.lytefast.flexinput.model.Attachment;
import d0.d0.f;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetIncomingShare.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052F\u0010\u0004\u001aB\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00030\u0003 \u0002* \u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lkotlin/Pair;", "Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "Lcom/discord/utilities/messagesend/MessageResult;", "<name for destructuring parameter 0>", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetIncomingShare$onSendClicked$3 extends o implements Function1<Pair<? extends MeUser, ? extends MessageResult>, Unit> {
    public final /* synthetic */ WidgetIncomingShare.ContentModel $contentModel;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ List $data;
    public final /* synthetic */ ViewEmbedGameInvite.Model $gameInviteModel;
    public final /* synthetic */ boolean $hasGif;
    public final /* synthetic */ boolean $hasImage;
    public final /* synthetic */ boolean $hasVideo;
    public final /* synthetic */ boolean $isOnCooldown;
    public final /* synthetic */ boolean $isUserPremium;
    public final /* synthetic */ int $maxFileSizeMB;
    public final /* synthetic */ WidgetGlobalSearchModel.ItemDataPayload $receiver;
    public final /* synthetic */ WidgetIncomingShare this$0;

    /* compiled from: WidgetIncomingShare.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/app/AppFragment;", "<anonymous parameter 0>", "", "captchaToken", "", "invoke", "(Lcom/discord/app/AppFragment;Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.share.WidgetIncomingShare$onSendClicked$3$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function2<AppFragment, String, Unit> {
        public AnonymousClass2() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public /* bridge */ /* synthetic */ Unit invoke(AppFragment appFragment, String str) {
            invoke2(appFragment, str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(AppFragment appFragment, String str) {
            m.checkNotNullParameter(appFragment, "<anonymous parameter 0>");
            m.checkNotNullParameter(str, "captchaToken");
            WidgetIncomingShare$onSendClicked$3 widgetIncomingShare$onSendClicked$3 = WidgetIncomingShare$onSendClicked$3.this;
            widgetIncomingShare$onSendClicked$3.this$0.onSendClicked(widgetIncomingShare$onSendClicked$3.$context, widgetIncomingShare$onSendClicked$3.$receiver, widgetIncomingShare$onSendClicked$3.$gameInviteModel, widgetIncomingShare$onSendClicked$3.$contentModel, widgetIncomingShare$onSendClicked$3.$isOnCooldown, widgetIncomingShare$onSendClicked$3.$maxFileSizeMB, widgetIncomingShare$onSendClicked$3.$isUserPremium, str);
        }
    }

    /* compiled from: WidgetIncomingShare.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.share.WidgetIncomingShare$onSendClicked$3$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends o implements Function0<Unit> {
        public final /* synthetic */ MeUser $me;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass3(MeUser meUser) {
            super(0);
            this.$me = meUser;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Guild guild;
            WidgetGlobalSearchModel.ItemDataPayload itemDataPayload = WidgetIncomingShare$onSendClicked$3.this.$receiver;
            int i = 0;
            if (itemDataPayload instanceof WidgetGlobalSearchModel.ItemGuild) {
                i = GuildUtilsKt.getMaxFileSizeMB(((WidgetGlobalSearchModel.ItemGuild) itemDataPayload).getGuild());
            } else if ((itemDataPayload instanceof WidgetGlobalSearchModel.ItemChannel) && (guild = ((WidgetGlobalSearchModel.ItemChannel) itemDataPayload).getGuild()) != null) {
                i = GuildUtilsKt.getMaxFileSizeMB(guild);
            }
            int max = Math.max(i, PremiumUtils.INSTANCE.getMaxFileSizeMB(this.$me));
            c.b bVar = c.k;
            FragmentManager parentFragmentManager = WidgetIncomingShare$onSendClicked$3.this.this$0.getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            UserUtils userUtils = UserUtils.INSTANCE;
            MeUser meUser = this.$me;
            m.checkNotNullExpressionValue(meUser, "me");
            boolean isPremium = userUtils.isPremium(meUser);
            int size = WidgetIncomingShare$onSendClicked$3.this.$data.size();
            WidgetIncomingShare$onSendClicked$3 widgetIncomingShare$onSendClicked$3 = WidgetIncomingShare$onSendClicked$3.this;
            bVar.a(parentFragmentManager, isPremium, max, Float.MAX_VALUE, Float.MAX_VALUE, null, size, widgetIncomingShare$onSendClicked$3.$hasImage, widgetIncomingShare$onSendClicked$3.$hasVideo, widgetIncomingShare$onSendClicked$3.$hasGif);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetIncomingShare$onSendClicked$3(WidgetIncomingShare widgetIncomingShare, List list, Context context, WidgetGlobalSearchModel.ItemDataPayload itemDataPayload, ViewEmbedGameInvite.Model model, WidgetIncomingShare.ContentModel contentModel, boolean z2, int i, boolean z3, boolean z4, boolean z5, boolean z6) {
        super(1);
        this.this$0 = widgetIncomingShare;
        this.$data = list;
        this.$context = context;
        this.$receiver = itemDataPayload;
        this.$gameInviteModel = model;
        this.$contentModel = contentModel;
        this.$isOnCooldown = z2;
        this.$maxFileSizeMB = i;
        this.$isUserPremium = z3;
        this.$hasImage = z4;
        this.$hasVideo = z5;
        this.$hasGif = z6;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends MeUser, ? extends MessageResult> pair) {
        invoke2((Pair<MeUser, ? extends MessageResult>) pair);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Pair<MeUser, ? extends MessageResult> pair) {
        MeUser component1 = pair.component1();
        MessageResult component2 = pair.component2();
        if (component2 instanceof MessageResult.Success) {
            this.this$0.onSendCompleted();
            Attachment attachment = (Attachment) u.firstOrNull((List<? extends Object>) this.$data);
            if (attachment != null) {
                AnalyticsTracker.INSTANCE.externalShare(attachment.getUri());
            }
        } else if (component2 instanceof MessageResult.Slowmode) {
            int coerceAtLeast = f.coerceAtLeast((int) (((MessageResult.Slowmode) component2).getCooldownMs() / 1000), 1);
            Resources resources = this.$context.getResources();
            m.checkNotNullExpressionValue(resources, "context.resources");
            b.a.d.m.h(this.$context, StringResourceUtilsKt.getQuantityString(resources, this.$context, (int) R.plurals.channel_slowmode_cooldown_seconds, coerceAtLeast, Integer.valueOf(coerceAtLeast)), 0, null, 12);
        } else if (component2 instanceof MessageResult.CaptchaRequired) {
            MessageResult.CaptchaRequired captchaRequired = (MessageResult.CaptchaRequired) component2;
            if (captchaRequired.getNonce() != null) {
                StoreStream.Companion.getMessages().deleteLocalMessage(captchaRequired.getChannelId(), captchaRequired.getNonce());
            }
            WidgetCaptchaBottomSheet.Companion.enqueue$default(WidgetCaptchaBottomSheet.Companion, "Message Captcha", new AnonymousClass2(), null, CaptchaErrorBody.Companion.createFromError(captchaRequired.getError()), 4, null);
        } else if (component2 instanceof MessageResult.UnknownFailure) {
            SendUtils sendUtils = SendUtils.INSTANCE;
            Error error = ((MessageResult.UnknownFailure) component2).getError();
            AppActivity appActivity = this.this$0.getAppActivity();
            if (appActivity != null) {
                SendUtils.handleSendError$default(sendUtils, error, appActivity, new AnonymousClass3(component1), null, 8, null);
            }
        }
    }
}
