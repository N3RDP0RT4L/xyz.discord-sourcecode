package com.discord.widgets.share;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.TextView;
import com.discord.databinding.WidgetIncomingShareBinding;
import com.discord.widgets.share.WidgetIncomingShare;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
/* compiled from: WidgetIncomingShare.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Landroid/widget/TextView;", "<anonymous parameter 0>", "", "actionId", "Landroid/view/KeyEvent;", "<anonymous parameter 2>", "", "invoke", "(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetIncomingShare$configureUi$1 extends o implements Function3<TextView, Integer, KeyEvent, Boolean> {
    public final /* synthetic */ WidgetIncomingShare.Model $this_configureUi;
    public final /* synthetic */ WidgetIncomingShare this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetIncomingShare$configureUi$1(WidgetIncomingShare widgetIncomingShare, WidgetIncomingShare.Model model) {
        super(3);
        this.this$0 = widgetIncomingShare;
        this.$this_configureUi = model;
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Boolean invoke(TextView textView, Integer num, KeyEvent keyEvent) {
        return Boolean.valueOf(invoke(textView, num.intValue(), keyEvent));
    }

    public final boolean invoke(TextView textView, int i, KeyEvent keyEvent) {
        WidgetIncomingShareBinding binding;
        m.checkNotNullParameter(textView, "<anonymous parameter 0>");
        if (i != 4 || this.$this_configureUi.getReceiver() == null) {
            return false;
        }
        WidgetIncomingShare widgetIncomingShare = this.this$0;
        binding = widgetIncomingShare.getBinding();
        TextInputLayout textInputLayout = binding.d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.externalShareComment");
        Context context = textInputLayout.getContext();
        m.checkNotNullExpressionValue(context, "binding.externalShareComment.context");
        widgetIncomingShare.onSendClicked(context, this.$this_configureUi.getReceiver(), this.$this_configureUi.getGameInviteModel(), this.$this_configureUi.getContentModel(), this.$this_configureUi.isOnCooldown(), this.$this_configureUi.getMaxFileSizeMB(), this.$this_configureUi.isUserPremium(), (r19 & 128) != 0 ? null : null);
        return true;
    }
}
