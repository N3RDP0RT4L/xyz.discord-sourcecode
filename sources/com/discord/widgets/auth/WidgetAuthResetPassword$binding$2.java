package com.discord.widgets.auth;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetAuthResetPasswordBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAuthResetPassword.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetAuthResetPasswordBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthResetPasswordBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetAuthResetPassword$binding$2 extends k implements Function1<View, WidgetAuthResetPasswordBinding> {
    public static final WidgetAuthResetPassword$binding$2 INSTANCE = new WidgetAuthResetPassword$binding$2();

    public WidgetAuthResetPassword$binding$2() {
        super(1, WidgetAuthResetPasswordBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthResetPasswordBinding;", 0);
    }

    public final WidgetAuthResetPasswordBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.auth_reset_password_back;
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.auth_reset_password_back);
        if (materialButton != null) {
            i = R.id.auth_reset_password_submit;
            MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.auth_reset_password_submit);
            if (materialButton2 != null) {
                return new WidgetAuthResetPasswordBinding((CoordinatorLayout) view, materialButton, materialButton2);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
