package com.discord.widgets.auth;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelGuildTemplate;
import com.discord.models.domain.ModelInvite;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreAuthentication;
import com.discord.stores.StoreGuildTemplates;
import com.discord.stores.StoreInstantInvites;
import com.discord.stores.StoreInviteSettings;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action0;
import rx.subjects.PublishSubject;
/* compiled from: WidgetAuthLandingViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 *2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004*+,-BG\u0012\u0006\u0010\"\u001a\u00020!\u0012\b\b\u0002\u0010$\u001a\u00020#\u0012\b\b\u0002\u0010&\u001a\u00020%\u0012\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00070\u0003\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u001e\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b(\u0010)J\u0013\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0018\u0010\f\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\f\u0010\rR:\u0010\u0010\u001a&\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u00040\u0004 \u000f*\u0012\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u000e0\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\"\u0010\u0018\u001a\u00020\u00178\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006."}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;", "Lrx/Observable;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;)V", "mostRecentStoreState", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/utilities/auth/GoogleSmartLockManager;", "googleSmartLockManager", "Lcom/discord/utilities/auth/GoogleSmartLockManager;", "getGoogleSmartLockManager", "()Lcom/discord/utilities/auth/GoogleSmartLockManager;", "", "smartLockCredentialRequestDisabled", "Z", "getSmartLockCredentialRequestDisabled", "()Z", "setSmartLockCredentialRequestDisabled", "(Z)V", "Lcom/discord/stores/StoreInstantInvites;", "storeInstantInvites", "Lcom/discord/stores/StoreInstantInvites;", "Landroid/content/Context;", "context", "Lcom/discord/stores/StoreAuthentication;", "storeAuthentication", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "storeObservable", HookHelper.constructorName, "(Landroid/content/Context;Lcom/discord/stores/StoreAuthentication;Lcom/discord/stores/StoreAnalytics;Lrx/Observable;Lcom/discord/stores/StoreInstantInvites;Lcom/discord/utilities/auth/GoogleSmartLockManager;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthLandingViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final GoogleSmartLockManager googleSmartLockManager;
    private StoreState mostRecentStoreState;
    private boolean smartLockCredentialRequestDisabled;
    private final StoreInstantInvites storeInstantInvites;

    /* compiled from: WidgetAuthLandingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;", "it", "", "invoke", "(Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.auth.WidgetAuthLandingViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "it");
            WidgetAuthLandingViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetAuthLandingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "it", "", "invoke", "(Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.auth.WidgetAuthLandingViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<GoogleSmartLockManager.SmartLockCredentials, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(GoogleSmartLockManager.SmartLockCredentials smartLockCredentials) {
            invoke2(smartLockCredentials);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(GoogleSmartLockManager.SmartLockCredentials smartLockCredentials) {
            m.checkNotNullParameter(smartLockCredentials, "it");
            PublishSubject publishSubject = WidgetAuthLandingViewModel.this.eventSubject;
            publishSubject.k.onNext(new Event.SmartLockLogin(smartLockCredentials));
        }
    }

    /* compiled from: WidgetAuthLandingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ-\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Companion;", "", "Lcom/discord/stores/StoreInviteSettings;", "storeInviteSettings", "Lcom/discord/stores/StoreGuildTemplates;", "storeGuildTemplates", "Lcom/discord/stores/StoreAuthentication;", "storeAuthentication", "Lrx/Observable;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreInviteSettings;Lcom/discord/stores/StoreGuildTemplates;Lcom/discord/stores/StoreAuthentication;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(StoreInviteSettings storeInviteSettings, final StoreGuildTemplates storeGuildTemplates, StoreAuthentication storeAuthentication) {
            Observable<StoreState> h = Observable.h(storeInviteSettings.getInviteCode(), storeInviteSettings.getInvite(), storeGuildTemplates.observeDynamicLinkGuildTemplateCode().Y(new b<String, Observable<? extends StoreGuildTemplates.GuildTemplateState>>() { // from class: com.discord.widgets.auth.WidgetAuthLandingViewModel$Companion$observeStoreState$1
                public final Observable<? extends StoreGuildTemplates.GuildTemplateState> call(final String str) {
                    if (str != null) {
                        return StoreGuildTemplates.this.observeGuildTemplate(str).u(new Action0() { // from class: com.discord.widgets.auth.WidgetAuthLandingViewModel$Companion$observeStoreState$1.1
                            @Override // rx.functions.Action0
                            public final void call() {
                                StoreGuildTemplates.this.maybeInitTemplateState(str);
                            }
                        });
                    }
                    return new k(StoreGuildTemplates.GuildTemplateState.None.INSTANCE);
                }
            }), storeAuthentication.getAgeGateError(), WidgetAuthLandingViewModel$Companion$observeStoreState$2.INSTANCE);
            m.checkNotNullExpressionValue(h, "Observable.combineLatest…eError,\n        )\n      }");
            return h;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetAuthLandingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Event;", "", HookHelper.constructorName, "()V", "SmartLockLogin", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Event$SmartLockLogin;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetAuthLandingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Event$SmartLockLogin;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Event;", "Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "component1", "()Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "smartLockCredentials", "copy", "(Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$Event$SmartLockLogin;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "getSmartLockCredentials", HookHelper.constructorName, "(Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SmartLockLogin extends Event {
            private final GoogleSmartLockManager.SmartLockCredentials smartLockCredentials;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SmartLockLogin(GoogleSmartLockManager.SmartLockCredentials smartLockCredentials) {
                super(null);
                m.checkNotNullParameter(smartLockCredentials, "smartLockCredentials");
                this.smartLockCredentials = smartLockCredentials;
            }

            public static /* synthetic */ SmartLockLogin copy$default(SmartLockLogin smartLockLogin, GoogleSmartLockManager.SmartLockCredentials smartLockCredentials, int i, Object obj) {
                if ((i & 1) != 0) {
                    smartLockCredentials = smartLockLogin.smartLockCredentials;
                }
                return smartLockLogin.copy(smartLockCredentials);
            }

            public final GoogleSmartLockManager.SmartLockCredentials component1() {
                return this.smartLockCredentials;
            }

            public final SmartLockLogin copy(GoogleSmartLockManager.SmartLockCredentials smartLockCredentials) {
                m.checkNotNullParameter(smartLockCredentials, "smartLockCredentials");
                return new SmartLockLogin(smartLockCredentials);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SmartLockLogin) && m.areEqual(this.smartLockCredentials, ((SmartLockLogin) obj).smartLockCredentials);
                }
                return true;
            }

            public final GoogleSmartLockManager.SmartLockCredentials getSmartLockCredentials() {
                return this.smartLockCredentials;
            }

            public int hashCode() {
                GoogleSmartLockManager.SmartLockCredentials smartLockCredentials = this.smartLockCredentials;
                if (smartLockCredentials != null) {
                    return smartLockCredentials.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("SmartLockLogin(smartLockCredentials=");
                R.append(this.smartLockCredentials);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetAuthLandingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b$\u0010%J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ>\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u0014\u0010\rJ\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001c\u001a\u0004\b\u001d\u0010\rR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b#\u0010\n¨\u0006&"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;", "", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "component1", "()Lcom/discord/stores/StoreInviteSettings$InviteCode;", "Lcom/discord/models/domain/ModelInvite;", "component2", "()Lcom/discord/models/domain/ModelInvite;", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "component3", "()Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "", "component4", "()Ljava/lang/String;", "inviteCode", "invite", "guildTemplateState", "ageGateError", "copy", "(Lcom/discord/stores/StoreInviteSettings$InviteCode;Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$StoreState;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getAgeGateError", "Lcom/discord/stores/StoreInviteSettings$InviteCode;", "getInviteCode", "Lcom/discord/models/domain/ModelInvite;", "getInvite", "Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;", "getGuildTemplateState", HookHelper.constructorName, "(Lcom/discord/stores/StoreInviteSettings$InviteCode;Lcom/discord/models/domain/ModelInvite;Lcom/discord/stores/StoreGuildTemplates$GuildTemplateState;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final String ageGateError;
        private final StoreGuildTemplates.GuildTemplateState guildTemplateState;
        private final ModelInvite invite;
        private final StoreInviteSettings.InviteCode inviteCode;

        public StoreState(StoreInviteSettings.InviteCode inviteCode, ModelInvite modelInvite, StoreGuildTemplates.GuildTemplateState guildTemplateState, String str) {
            m.checkNotNullParameter(guildTemplateState, "guildTemplateState");
            this.inviteCode = inviteCode;
            this.invite = modelInvite;
            this.guildTemplateState = guildTemplateState;
            this.ageGateError = str;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, StoreInviteSettings.InviteCode inviteCode, ModelInvite modelInvite, StoreGuildTemplates.GuildTemplateState guildTemplateState, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                inviteCode = storeState.inviteCode;
            }
            if ((i & 2) != 0) {
                modelInvite = storeState.invite;
            }
            if ((i & 4) != 0) {
                guildTemplateState = storeState.guildTemplateState;
            }
            if ((i & 8) != 0) {
                str = storeState.ageGateError;
            }
            return storeState.copy(inviteCode, modelInvite, guildTemplateState, str);
        }

        public final StoreInviteSettings.InviteCode component1() {
            return this.inviteCode;
        }

        public final ModelInvite component2() {
            return this.invite;
        }

        public final StoreGuildTemplates.GuildTemplateState component3() {
            return this.guildTemplateState;
        }

        public final String component4() {
            return this.ageGateError;
        }

        public final StoreState copy(StoreInviteSettings.InviteCode inviteCode, ModelInvite modelInvite, StoreGuildTemplates.GuildTemplateState guildTemplateState, String str) {
            m.checkNotNullParameter(guildTemplateState, "guildTemplateState");
            return new StoreState(inviteCode, modelInvite, guildTemplateState, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.inviteCode, storeState.inviteCode) && m.areEqual(this.invite, storeState.invite) && m.areEqual(this.guildTemplateState, storeState.guildTemplateState) && m.areEqual(this.ageGateError, storeState.ageGateError);
        }

        public final String getAgeGateError() {
            return this.ageGateError;
        }

        public final StoreGuildTemplates.GuildTemplateState getGuildTemplateState() {
            return this.guildTemplateState;
        }

        public final ModelInvite getInvite() {
            return this.invite;
        }

        public final StoreInviteSettings.InviteCode getInviteCode() {
            return this.inviteCode;
        }

        public int hashCode() {
            StoreInviteSettings.InviteCode inviteCode = this.inviteCode;
            int i = 0;
            int hashCode = (inviteCode != null ? inviteCode.hashCode() : 0) * 31;
            ModelInvite modelInvite = this.invite;
            int hashCode2 = (hashCode + (modelInvite != null ? modelInvite.hashCode() : 0)) * 31;
            StoreGuildTemplates.GuildTemplateState guildTemplateState = this.guildTemplateState;
            int hashCode3 = (hashCode2 + (guildTemplateState != null ? guildTemplateState.hashCode() : 0)) * 31;
            String str = this.ageGateError;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(inviteCode=");
            R.append(this.inviteCode);
            R.append(", invite=");
            R.append(this.invite);
            R.append(", guildTemplateState=");
            R.append(this.guildTemplateState);
            R.append(", ageGateError=");
            return a.H(R, this.ageGateError, ")");
        }
    }

    /* compiled from: WidgetAuthLandingViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0013\b\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001e\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0003\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;", "", "", "ageGateError", "Ljava/lang/String;", "getAgeGateError", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "Empty", "GuildTemplate", "Invite", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {
        private final String ageGateError;

        /* compiled from: WidgetAuthLandingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001e\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;", "", "component1", "()Ljava/lang/String;", "ageGateError", "copy", "(Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Empty;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getAgeGateError", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Empty extends ViewState {
            private final String ageGateError;

            public Empty(String str) {
                super(str, null);
                this.ageGateError = str;
            }

            public static /* synthetic */ Empty copy$default(Empty empty, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = empty.getAgeGateError();
                }
                return empty.copy(str);
            }

            public final String component1() {
                return getAgeGateError();
            }

            public final Empty copy(String str) {
                return new Empty(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Empty) && m.areEqual(getAgeGateError(), ((Empty) obj).getAgeGateError());
                }
                return true;
            }

            @Override // com.discord.widgets.auth.WidgetAuthLandingViewModel.ViewState
            public String getAgeGateError() {
                return this.ageGateError;
            }

            public int hashCode() {
                String ageGateError = getAgeGateError();
                if (ageGateError != null) {
                    return ageGateError.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Empty(ageGateError=");
                R.append(getAgeGateError());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetAuthLandingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001e\u0010\t\u001a\u0004\u0018\u00010\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;", "Lcom/discord/models/domain/ModelGuildTemplate;", "component1", "()Lcom/discord/models/domain/ModelGuildTemplate;", "", "component2", "()Ljava/lang/String;", "guildTemplate", "ageGateError", "copy", "(Lcom/discord/models/domain/ModelGuildTemplate;Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$GuildTemplate;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getAgeGateError", "Lcom/discord/models/domain/ModelGuildTemplate;", "getGuildTemplate", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGuildTemplate;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class GuildTemplate extends ViewState {
            private final String ageGateError;
            private final ModelGuildTemplate guildTemplate;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public GuildTemplate(ModelGuildTemplate modelGuildTemplate, String str) {
                super(str, null);
                m.checkNotNullParameter(modelGuildTemplate, "guildTemplate");
                this.guildTemplate = modelGuildTemplate;
                this.ageGateError = str;
            }

            public static /* synthetic */ GuildTemplate copy$default(GuildTemplate guildTemplate, ModelGuildTemplate modelGuildTemplate, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGuildTemplate = guildTemplate.guildTemplate;
                }
                if ((i & 2) != 0) {
                    str = guildTemplate.getAgeGateError();
                }
                return guildTemplate.copy(modelGuildTemplate, str);
            }

            public final ModelGuildTemplate component1() {
                return this.guildTemplate;
            }

            public final String component2() {
                return getAgeGateError();
            }

            public final GuildTemplate copy(ModelGuildTemplate modelGuildTemplate, String str) {
                m.checkNotNullParameter(modelGuildTemplate, "guildTemplate");
                return new GuildTemplate(modelGuildTemplate, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof GuildTemplate)) {
                    return false;
                }
                GuildTemplate guildTemplate = (GuildTemplate) obj;
                return m.areEqual(this.guildTemplate, guildTemplate.guildTemplate) && m.areEqual(getAgeGateError(), guildTemplate.getAgeGateError());
            }

            @Override // com.discord.widgets.auth.WidgetAuthLandingViewModel.ViewState
            public String getAgeGateError() {
                return this.ageGateError;
            }

            public final ModelGuildTemplate getGuildTemplate() {
                return this.guildTemplate;
            }

            public int hashCode() {
                ModelGuildTemplate modelGuildTemplate = this.guildTemplate;
                int i = 0;
                int hashCode = (modelGuildTemplate != null ? modelGuildTemplate.hashCode() : 0) * 31;
                String ageGateError = getAgeGateError();
                if (ageGateError != null) {
                    i = ageGateError.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("GuildTemplate(guildTemplate=");
                R.append(this.guildTemplate);
                R.append(", ageGateError=");
                R.append(getAgeGateError());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetAuthLandingViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001e\u0010\t\u001a\u0004\u0018\u00010\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;", "Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState;", "Lcom/discord/models/domain/ModelInvite;", "component1", "()Lcom/discord/models/domain/ModelInvite;", "", "component2", "()Ljava/lang/String;", "invite", "ageGateError", "copy", "(Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetAuthLandingViewModel$ViewState$Invite;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelInvite;", "getInvite", "Ljava/lang/String;", "getAgeGateError", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invite extends ViewState {
            private final String ageGateError;
            private final ModelInvite invite;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Invite(ModelInvite modelInvite, String str) {
                super(str, null);
                m.checkNotNullParameter(modelInvite, "invite");
                this.invite = modelInvite;
                this.ageGateError = str;
            }

            public static /* synthetic */ Invite copy$default(Invite invite, ModelInvite modelInvite, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelInvite = invite.invite;
                }
                if ((i & 2) != 0) {
                    str = invite.getAgeGateError();
                }
                return invite.copy(modelInvite, str);
            }

            public final ModelInvite component1() {
                return this.invite;
            }

            public final String component2() {
                return getAgeGateError();
            }

            public final Invite copy(ModelInvite modelInvite, String str) {
                m.checkNotNullParameter(modelInvite, "invite");
                return new Invite(modelInvite, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Invite)) {
                    return false;
                }
                Invite invite = (Invite) obj;
                return m.areEqual(this.invite, invite.invite) && m.areEqual(getAgeGateError(), invite.getAgeGateError());
            }

            @Override // com.discord.widgets.auth.WidgetAuthLandingViewModel.ViewState
            public String getAgeGateError() {
                return this.ageGateError;
            }

            public final ModelInvite getInvite() {
                return this.invite;
            }

            public int hashCode() {
                ModelInvite modelInvite = this.invite;
                int i = 0;
                int hashCode = (modelInvite != null ? modelInvite.hashCode() : 0) * 31;
                String ageGateError = getAgeGateError();
                if (ageGateError != null) {
                    i = ageGateError.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("Invite(invite=");
                R.append(this.invite);
                R.append(", ageGateError=");
                R.append(getAgeGateError());
                R.append(")");
                return R.toString();
            }
        }

        private ViewState(String str) {
            this.ageGateError = str;
        }

        public String getAgeGateError() {
            return this.ageGateError;
        }

        public /* synthetic */ ViewState(String str, DefaultConstructorMarker defaultConstructorMarker) {
            this(str);
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetAuthLandingViewModel(android.content.Context r8, com.discord.stores.StoreAuthentication r9, com.discord.stores.StoreAnalytics r10, rx.Observable r11, com.discord.stores.StoreInstantInvites r12, com.discord.utilities.auth.GoogleSmartLockManager r13, int r14, kotlin.jvm.internal.DefaultConstructorMarker r15) {
        /*
            r7 = this;
            r15 = r14 & 2
            if (r15 == 0) goto La
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreAuthentication r9 = r9.getAuthentication()
        La:
            r2 = r9
            r9 = r14 & 4
            if (r9 == 0) goto L15
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreAnalytics r10 = r9.getAnalytics()
        L15:
            r3 = r10
            r9 = r14 & 8
            if (r9 == 0) goto L2e
            com.discord.widgets.auth.WidgetAuthLandingViewModel$Companion r9 = com.discord.widgets.auth.WidgetAuthLandingViewModel.Companion
            com.discord.stores.StoreStream$Companion r10 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreInviteSettings r11 = r10.getInviteSettings()
            com.discord.stores.StoreGuildTemplates r15 = r10.getGuildTemplates()
            com.discord.stores.StoreAuthentication r10 = r10.getAuthentication()
            rx.Observable r11 = com.discord.widgets.auth.WidgetAuthLandingViewModel.Companion.access$observeStoreState(r9, r11, r15, r10)
        L2e:
            r4 = r11
            r9 = r14 & 16
            if (r9 == 0) goto L39
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreInstantInvites r12 = r9.getInstantInvites()
        L39:
            r5 = r12
            r9 = r14 & 32
            if (r9 == 0) goto L45
            com.discord.utilities.auth.GoogleSmartLockManager r13 = new com.discord.utilities.auth.GoogleSmartLockManager
            r9 = 0
            r10 = 2
            r13.<init>(r8, r9, r10, r9)
        L45:
            r6 = r13
            r0 = r7
            r1 = r8
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.auth.WidgetAuthLandingViewModel.<init>(android.content.Context, com.discord.stores.StoreAuthentication, com.discord.stores.StoreAnalytics, rx.Observable, com.discord.stores.StoreInstantInvites, com.discord.utilities.auth.GoogleSmartLockManager, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final GoogleSmartLockManager getGoogleSmartLockManager() {
        return this.googleSmartLockManager;
    }

    public final boolean getSmartLockCredentialRequestDisabled() {
        return this.smartLockCredentialRequestDisabled;
    }

    @MainThread
    public final void handleStoreState(StoreState storeState) {
        m.checkNotNullParameter(storeState, "storeState");
        StoreInviteSettings.InviteCode inviteCode = storeState.getInviteCode();
        StoreState storeState2 = this.mostRecentStoreState;
        if ((!m.areEqual(storeState2 != null ? storeState2.getInviteCode() : null, inviteCode)) && inviteCode != null) {
            this.storeInstantInvites.fetchInviteIfNotLoaded(inviteCode.getInviteCode(), (r13 & 2) != 0 ? null : null, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? null : null);
        }
        ModelInvite invite = storeState.getInvite();
        StoreGuildTemplates.GuildTemplateState guildTemplateState = storeState.getGuildTemplateState();
        String ageGateError = storeState.getAgeGateError();
        if (invite != null) {
            updateViewState(new ViewState.Invite(invite, ageGateError));
        } else if (guildTemplateState instanceof StoreGuildTemplates.GuildTemplateState.Resolved) {
            updateViewState(new ViewState.GuildTemplate(((StoreGuildTemplates.GuildTemplateState.Resolved) guildTemplateState).getGuildTemplate(), ageGateError));
        } else {
            updateViewState(new ViewState.Empty(ageGateError));
        }
        this.mostRecentStoreState = storeState;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void setSmartLockCredentialRequestDisabled(boolean z2) {
        this.smartLockCredentialRequestDisabled = z2;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAuthLandingViewModel(Context context, StoreAuthentication storeAuthentication, StoreAnalytics storeAnalytics, Observable<StoreState> observable, StoreInstantInvites storeInstantInvites, GoogleSmartLockManager googleSmartLockManager) {
        super(new ViewState.Empty(null));
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(storeAuthentication, "storeAuthentication");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(observable, "storeObservable");
        m.checkNotNullParameter(storeInstantInvites, "storeInstantInvites");
        m.checkNotNullParameter(googleSmartLockManager, "googleSmartLockManager");
        this.storeInstantInvites = storeInstantInvites;
        this.googleSmartLockManager = googleSmartLockManager;
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), WidgetAuthLandingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(googleSmartLockManager.getSmartLockRepo().getSmartLockLoginObservable(), this, null, 2, null), WidgetAuthLandingViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
        storeAuthentication.requestConsentRequired();
        storeAnalytics.appLandingViewed();
    }
}
