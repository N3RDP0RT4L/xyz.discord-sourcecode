package com.discord.widgets.auth;

import com.discord.models.phone.PhoneCountryCode;
import com.discord.stores.StorePhone;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetAuthLogin.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/models/phone/PhoneCountryCode;", "invoke", "()Lcom/discord/models/phone/PhoneCountryCode;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthLogin$onViewBound$8 extends o implements Function0<PhoneCountryCode> {
    public final /* synthetic */ StorePhone $phoneStore;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAuthLogin$onViewBound$8(StorePhone storePhone) {
        super(0);
        this.$phoneStore = storePhone;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final PhoneCountryCode invoke() {
        return this.$phoneStore.getCountryCode();
    }
}
