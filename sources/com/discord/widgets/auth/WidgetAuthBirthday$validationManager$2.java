package com.discord.widgets.auth;

import android.widget.CheckBox;
import com.discord.databinding.WidgetAuthBirthdayBinding;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.validators.BasicTextInputValidator;
import com.discord.utilities.view.validators.Input;
import com.discord.utilities.view.validators.InputValidator;
import com.discord.utilities.view.validators.ValidationManager;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetAuthBirthday.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/view/validators/ValidationManager;", "invoke", "()Lcom/discord/utilities/view/validators/ValidationManager;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthBirthday$validationManager$2 extends o implements Function0<ValidationManager> {
    public final /* synthetic */ WidgetAuthBirthday this$0;

    /* compiled from: WidgetAuthBirthday.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/google/android/material/checkbox/MaterialCheckBox;", "checkBox", "", "errorMessage", "", "invoke", "(Lcom/google/android/material/checkbox/MaterialCheckBox;Ljava/lang/CharSequence;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.auth.WidgetAuthBirthday$validationManager$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function2<MaterialCheckBox, CharSequence, Boolean> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public AnonymousClass2() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public /* bridge */ /* synthetic */ Boolean invoke(MaterialCheckBox materialCheckBox, CharSequence charSequence) {
            return Boolean.valueOf(invoke2(materialCheckBox, charSequence));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(MaterialCheckBox materialCheckBox, CharSequence charSequence) {
            m.checkNotNullParameter(materialCheckBox, "checkBox");
            m.checkNotNullParameter(charSequence, "errorMessage");
            b.a.d.m.h(materialCheckBox.getContext(), charSequence, 0, null, 12);
            ViewExtensions.hintWithRipple$default(materialCheckBox, 0L, 1, null);
            return true;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAuthBirthday$validationManager$2(WidgetAuthBirthday widgetAuthBirthday) {
        super(0);
        this.this$0 = widgetAuthBirthday;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ValidationManager invoke() {
        WidgetAuthBirthdayBinding binding;
        WidgetAuthBirthdayBinding binding2;
        binding = this.this$0.getBinding();
        TextInputLayout textInputLayout = binding.c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.authRegisterBirthday");
        InputValidator[] inputValidatorArr = {BasicTextInputValidator.Companion.createRequiredInputValidator(R.string.age_gate_invalid_birthday)};
        binding2 = this.this$0.getBinding();
        return new ValidationManager(new Input.TextInputLayoutInput("dateOfBirth", textInputLayout, inputValidatorArr), new Input.GenericInput("tos", binding2.e, new InputValidator<CheckBox>() { // from class: com.discord.widgets.auth.WidgetAuthBirthday$validationManager$2.1
            /* JADX WARN: Removed duplicated region for block: B:11:0x0024 A[RETURN, SYNTHETIC] */
            /* JADX WARN: Removed duplicated region for block: B:12:0x0026  */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public java.lang.CharSequence getErrorMessage(android.widget.CheckBox r2) {
                /*
                    r1 = this;
                    java.lang.String r0 = "view"
                    d0.z.d.m.checkNotNullParameter(r2, r0)
                    com.discord.widgets.auth.WidgetAuthBirthday$validationManager$2 r0 = com.discord.widgets.auth.WidgetAuthBirthday$validationManager$2.this
                    com.discord.widgets.auth.WidgetAuthBirthday r0 = r0.this$0
                    boolean r0 = com.discord.widgets.auth.WidgetAuthBirthday.access$isConsentRequired$p(r0)
                    if (r0 == 0) goto L15
                    boolean r0 = r2.isChecked()
                    if (r0 != 0) goto L1f
                L15:
                    com.discord.widgets.auth.WidgetAuthBirthday$validationManager$2 r0 = com.discord.widgets.auth.WidgetAuthBirthday$validationManager$2.this
                    com.discord.widgets.auth.WidgetAuthBirthday r0 = r0.this$0
                    boolean r0 = com.discord.widgets.auth.WidgetAuthBirthday.access$isConsentRequired$p(r0)
                    if (r0 != 0) goto L21
                L1f:
                    r0 = 1
                    goto L22
                L21:
                    r0 = 0
                L22:
                    if (r0 == 0) goto L26
                    r2 = 0
                    goto L31
                L26:
                    android.content.Context r2 = r2.getContext()
                    r0 = 2131894946(0x7f1222a2, float:1.9424711E38)
                    java.lang.String r2 = r2.getString(r0)
                L31:
                    return r2
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.auth.WidgetAuthBirthday$validationManager$2.AnonymousClass1.getErrorMessage(android.widget.CheckBox):java.lang.CharSequence");
            }
        }, AnonymousClass2.INSTANCE));
    }
}
