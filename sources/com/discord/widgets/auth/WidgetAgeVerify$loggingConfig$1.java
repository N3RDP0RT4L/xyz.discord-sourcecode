package com.discord.widgets.auth;

import com.discord.analytics.generated.events.impression.TrackImpressionUserAgeGate;
import com.discord.analytics.generated.events.impression.TrackImpressionUserAgeGateUnderage;
import com.discord.api.science.AnalyticsSchema;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetAgeVerifyBinding;
import com.discord.stores.StoreStream;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetAgeVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/api/science/AnalyticsSchema;", "invoke", "()Lcom/discord/api/science/AnalyticsSchema;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAgeVerify$loggingConfig$1 extends o implements Function0<AnalyticsSchema> {
    public final /* synthetic */ WidgetAgeVerify this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAgeVerify$loggingConfig$1(WidgetAgeVerify widgetAgeVerify) {
        super(0);
        this.this$0 = widgetAgeVerify;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AnalyticsSchema invoke() {
        WidgetAgeVerifyBinding binding;
        AnalyticsSchema analyticsSchema;
        boolean z2 = StoreStream.Companion.getUsers().getMe().getId() > 0;
        binding = this.this$0.getBinding();
        AppViewFlipper appViewFlipper = binding.f2207b;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.ageVerifyViewFlipper");
        int displayedChild = appViewFlipper.getDisplayedChild();
        if (displayedChild == 0) {
            analyticsSchema = new TrackImpressionUserAgeGate(Boolean.valueOf(z2));
        } else if (displayedChild != 2) {
            return null;
        } else {
            analyticsSchema = new TrackImpressionUserAgeGateUnderage(Boolean.valueOf(z2));
        }
        return analyticsSchema;
    }
}
