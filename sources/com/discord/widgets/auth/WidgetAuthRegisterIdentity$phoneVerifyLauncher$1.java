package com.discord.widgets.auth;

import b.a.d.j;
import com.discord.utilities.auth.RegistrationFlowRepo;
import com.discord.views.phone.PhoneOrEmailInputView;
import com.discord.widgets.auth.WidgetAuthPhoneVerify;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetAuthRegisterIdentity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthPhoneVerify$Result;", "result", "", "invoke", "(Lcom/discord/widgets/auth/WidgetAuthPhoneVerify$Result;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthRegisterIdentity$phoneVerifyLauncher$1 extends o implements Function1<WidgetAuthPhoneVerify.Result, Unit> {
    public final /* synthetic */ WidgetAuthRegisterIdentity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAuthRegisterIdentity$phoneVerifyLauncher$1(WidgetAuthRegisterIdentity widgetAuthRegisterIdentity) {
        super(1);
        this.this$0 = widgetAuthRegisterIdentity;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetAuthPhoneVerify.Result result) {
        invoke2(result);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetAuthPhoneVerify.Result result) {
        m.checkNotNullParameter(result, "result");
        if (result instanceof WidgetAuthPhoneVerify.Result.Token) {
            RegistrationFlowRepo.Companion companion = RegistrationFlowRepo.Companion;
            RegistrationFlowRepo.trackTransition$default(companion.getINSTANCE(), "Phone Verification", "success", null, 4, null);
            companion.getINSTANCE().setPhoneToken(((WidgetAuthPhoneVerify.Result.Token) result).getToken());
            j.e(this.this$0.requireContext(), WidgetAuthRegisterAccountInformation.class, null, 4);
        } else if (result instanceof WidgetAuthPhoneVerify.Result.Cancelled) {
            this.this$0.setInputMode(PhoneOrEmailInputView.Mode.EMAIL);
        }
    }
}
