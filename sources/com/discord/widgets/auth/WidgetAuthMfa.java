package com.discord.widgets.auth;

import andhook.lib.HookHelper;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.d.o;
import b.a.i.j4;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetAuthMfaBackupCodesBinding;
import com.discord.databinding.WidgetAuthMfaBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreAuthentication;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.auth.AuthUtils;
import com.discord.utilities.error.Error;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CodeVerificationView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetAuthMfa.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 +2\u00020\u0001:\u0001+B\u0007¢\u0006\u0004\b*\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0017\u0010\t\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\r\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J)\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000f2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u001f\u0010\u0004R\u0016\u0010 \u001a\u00020\u00078\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010\"\u001a\u00020\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010#R\u001d\u0010)\u001a\u00020$8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(¨\u0006,"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthMfa;", "Lcom/discord/app/AppFragment;", "", "tryPasteCodeFromClipboard", "()V", "showBackupCodesDialog", "showInfoDialog", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "evaluateCode", "(Ljava/lang/String;)V", "Landroidx/appcompat/app/AlertDialog;", "dialog", "evaluateBackupCode", "(Landroidx/appcompat/app/AlertDialog;Ljava/lang/String;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Landroid/view/ContextMenu;", "menu", "v", "Landroid/view/ContextMenu$ContextMenuInfo;", "menuInfo", "onCreateContextMenu", "(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V", "Landroid/view/MenuItem;", "item", "", "onContextItemSelected", "(Landroid/view/MenuItem;)Z", "onResume", "ticket", "Ljava/lang/String;", "ignoreAutopaste", "Z", "Lcom/discord/databinding/WidgetAuthMfaBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetAuthMfaBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthMfa extends AppFragment {
    private static final int BACKUP_CODE_DIGITS = 8;
    private static final String INTENT_TICKET = "INTENT_TICKET";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetAuthMfa$binding$2.INSTANCE, null, 2, null);
    private boolean ignoreAutopaste = true;
    private String ticket;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetAuthMfa.class, "binding", "getBinding()Lcom/discord/databinding/WidgetAuthMfaBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetAuthMfa.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthMfa$Companion;", "", "Landroid/content/Context;", "context", "", "ticket", "", "start", "(Landroid/content/Context;Ljava/lang/String;)V", "", "BACKUP_CODE_DIGITS", "I", WidgetAuthMfa.INTENT_TICKET, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void start(Context context, String str) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, "ticket");
            Intent intent = new Intent();
            intent.putExtra(WidgetAuthMfa.INTENT_TICKET, str);
            j.d(context, WidgetAuthMfa.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetAuthMfa() {
        super(R.layout.widget_auth_mfa);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void evaluateBackupCode(AlertDialog alertDialog, String str) {
        if (str.length() < 8) {
            b.a.d.m.i(this, R.string.two_fa_backup_code_enter_wrong, 0, 4);
            return;
        }
        alertDialog.hide();
        evaluateCode(str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void evaluateCode(String str) {
        if (TextUtils.isEmpty(str)) {
            b.a.d.m.i(this, R.string.two_fa_token_required, 0, 4);
            return;
        }
        StoreAuthentication authentication = StoreStream.Companion.getAuthentication();
        String str2 = this.ticket;
        if (str2 == null) {
            m.throwUninitializedPropertyAccessException("ticket");
        }
        ObservableExtensionsKt.withDimmer(ObservableExtensionsKt.ui$default(authentication.authMFA(str, str2), this, null, 2, null), getBinding().c, 0L).k(o.a.g(getContext(), WidgetAuthMfa$evaluateCode$1.INSTANCE, new Action1<Error>() { // from class: com.discord.widgets.auth.WidgetAuthMfa$evaluateCode$2
            public final void call(Error error) {
                WidgetAuthMfaBinding binding;
                binding = WidgetAuthMfa.this.getBinding();
                binding.f2217b.b();
                AnalyticsTracker.INSTANCE.loginAttempt(false);
            }
        }));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetAuthMfaBinding getBinding() {
        return (WidgetAuthMfaBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void showBackupCodesDialog() {
        View inflate = getLayoutInflater().inflate(R.layout.widget_auth_mfa_backup_codes, (ViewGroup) null, false);
        int i = R.id.server_settings_delete_server_header;
        TextView textView = (TextView) inflate.findViewById(R.id.server_settings_delete_server_header);
        if (textView != null) {
            i = R.id.widget_auth_mfa_backup_codes_cancel;
            MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.widget_auth_mfa_backup_codes_cancel);
            if (materialButton != null) {
                i = R.id.widget_auth_mfa_backup_codes_edittext;
                TextInputLayout textInputLayout = (TextInputLayout) inflate.findViewById(R.id.widget_auth_mfa_backup_codes_edittext);
                if (textInputLayout != null) {
                    i = R.id.widget_auth_mfa_backup_codes_send;
                    MaterialButton materialButton2 = (MaterialButton) inflate.findViewById(R.id.widget_auth_mfa_backup_codes_send);
                    if (materialButton2 != null) {
                        LinearLayout linearLayout = (LinearLayout) inflate;
                        final WidgetAuthMfaBackupCodesBinding widgetAuthMfaBackupCodesBinding = new WidgetAuthMfaBackupCodesBinding(linearLayout, textView, materialButton, textInputLayout, materialButton2);
                        m.checkNotNullExpressionValue(widgetAuthMfaBackupCodesBinding, "WidgetAuthMfaBackupCodes…outInflater, null, false)");
                        m.checkNotNullExpressionValue(linearLayout, "binding.root");
                        final AlertDialog show = new AlertDialog.Builder(linearLayout.getContext()).setView(linearLayout).show();
                        materialButton2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthMfa$showBackupCodesDialog$1
                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view) {
                                WidgetAuthMfa widgetAuthMfa = WidgetAuthMfa.this;
                                AlertDialog alertDialog = show;
                                m.checkNotNullExpressionValue(alertDialog, "dialog");
                                TextInputLayout textInputLayout2 = widgetAuthMfaBackupCodesBinding.c;
                                m.checkNotNullExpressionValue(textInputLayout2, "binding.widgetAuthMfaBackupCodesEdittext");
                                widgetAuthMfa.evaluateBackupCode(alertDialog, ViewExtensions.getTextOrEmpty(textInputLayout2));
                            }
                        });
                        materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthMfa$showBackupCodesDialog$2
                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view) {
                                AlertDialog.this.hide();
                            }
                        });
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void showInfoDialog() {
        View inflate = getLayoutInflater().inflate(R.layout.widget_auth_mfa_info, (ViewGroup) null, false);
        int i = R.id.server_settings_delete_server_header;
        TextView textView = (TextView) inflate.findViewById(R.id.server_settings_delete_server_header);
        if (textView != null) {
            i = R.id.widget_auth_mfa_info_okay;
            MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.widget_auth_mfa_info_okay);
            if (materialButton != null) {
                i = R.id.widget_auth_mfa_info_text;
                LinkifiedTextView linkifiedTextView = (LinkifiedTextView) inflate.findViewById(R.id.widget_auth_mfa_info_text);
                if (linkifiedTextView != null) {
                    LinearLayout linearLayout = (LinearLayout) inflate;
                    m.checkNotNullExpressionValue(new j4(linearLayout, textView, materialButton, linkifiedTextView), "WidgetAuthMfaInfoBinding…outInflater, null, false)");
                    m.checkNotNullExpressionValue(linearLayout, "binding.root");
                    final AlertDialog show = new AlertDialog.Builder(linearLayout.getContext()).setView(linearLayout).show();
                    m.checkNotNullExpressionValue(linkifiedTextView, "binding.widgetAuthMfaInfoText");
                    b.m(linkifiedTextView, R.string.two_fa_download_app_body, new Object[]{AuthUtils.URL_AUTHY, AuthUtils.URL_GOOGLE_AUTHENTICATOR}, (r4 & 4) != 0 ? b.g.j : null);
                    materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthMfa$showInfoDialog$1
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            AlertDialog.this.dismiss();
                        }
                    });
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void tryPasteCodeFromClipboard() {
        ClipData primaryClip;
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return");
            Object systemService = context.getSystemService("clipboard");
            if (!(systemService instanceof ClipboardManager)) {
                systemService = null;
            }
            ClipboardManager clipboardManager = (ClipboardManager) systemService;
            if (clipboardManager != null && (primaryClip = clipboardManager.getPrimaryClip()) != null) {
                m.checkNotNullExpressionValue(primaryClip, "clipboard.primaryClip ?: return");
                if (primaryClip.getItemCount() >= 1) {
                    CharSequence coerceToText = primaryClip.getItemAt(0).coerceToText(context);
                    if (coerceToText.length() == 6 && TextUtils.isDigitsOnly(coerceToText)) {
                        getBinding().f2217b.setOnCodeEntered(WidgetAuthMfa$tryPasteCodeFromClipboard$1.INSTANCE);
                        CodeVerificationView codeVerificationView = getBinding().f2217b;
                        m.checkNotNullExpressionValue(coerceToText, "clipboardText");
                        codeVerificationView.setCode(coerceToText);
                        getBinding().f2217b.setOnCodeEntered(new WidgetAuthMfa$tryPasteCodeFromClipboard$2(this));
                        Observable<Long> d02 = Observable.d0(500L, TimeUnit.MILLISECONDS);
                        m.checkNotNullExpressionValue(d02, "Observable\n            .…L, TimeUnit.MILLISECONDS)");
                        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), WidgetAuthMfa.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetAuthMfa$tryPasteCodeFromClipboard$3(this));
                    }
                }
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onContextItemSelected(MenuItem menuItem) {
        m.checkNotNullParameter(menuItem, "item");
        if (menuItem.getItemId() != R.id.menu_code_verification_paste) {
            return super.onContextItemSelected(menuItem);
        }
        tryPasteCodeFromClipboard();
        return true;
    }

    @Override // androidx.fragment.app.Fragment, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        m.checkNotNullParameter(contextMenu, "menu");
        m.checkNotNullParameter(view, "v");
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        new MenuInflater(requireContext()).inflate(R.menu.menu_code_verification, contextMenu);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (!this.ignoreAutopaste) {
            Observable<Long> d02 = Observable.d0(250L, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable\n          .ti…L, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), WidgetAuthMfa.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetAuthMfa$onResume$1(this));
            return;
        }
        this.ignoreAutopaste = false;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        AppActivity appActivity;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_auth_mfa, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.auth.WidgetAuthMfa$onViewBound$1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                switch (menuItem.getItemId()) {
                    case R.id.menu_auth_mfa_backup_codes /* 2131364296 */:
                        WidgetAuthMfa.this.showBackupCodesDialog();
                        return;
                    case R.id.menu_auth_mfa_info /* 2131364297 */:
                        WidgetAuthMfa.this.showInfoDialog();
                        return;
                    default:
                        return;
                }
            }
        }, null, 4, null);
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_TICKET);
        if (stringExtra == null) {
            stringExtra = "";
        }
        this.ticket = stringExtra;
        if (stringExtra == null) {
            m.throwUninitializedPropertyAccessException("ticket");
        }
        if ((stringExtra.length() == 0) && (appActivity = getAppActivity()) != null) {
            appActivity.finish();
        }
        getBinding().f2217b.setOnCodeEntered(new WidgetAuthMfa$onViewBound$2(this));
        registerForContextMenu(getBinding().f2217b);
    }
}
