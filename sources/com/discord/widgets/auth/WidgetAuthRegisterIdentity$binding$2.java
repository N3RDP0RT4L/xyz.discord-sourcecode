package com.discord.widgets.auth;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetAuthRegisterIdentityBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.phone.PhoneOrEmailInputView;
import com.discord.views.segmentedcontrol.CardSegment;
import com.discord.views.segmentedcontrol.SegmentedControlContainer;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAuthRegisterIdentity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetAuthRegisterIdentityBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthRegisterIdentityBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetAuthRegisterIdentity$binding$2 extends k implements Function1<View, WidgetAuthRegisterIdentityBinding> {
    public static final WidgetAuthRegisterIdentity$binding$2 INSTANCE = new WidgetAuthRegisterIdentity$binding$2();

    public WidgetAuthRegisterIdentity$binding$2() {
        super(1, WidgetAuthRegisterIdentityBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthRegisterIdentityBinding;", 0);
    }

    public final WidgetAuthRegisterIdentityBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.auth_register_identity_button;
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.auth_register_identity_button);
        if (materialButton != null) {
            i = R.id.auth_register_identity_first_segment_card;
            CardSegment cardSegment = (CardSegment) view.findViewById(R.id.auth_register_identity_first_segment_card);
            if (cardSegment != null) {
                i = R.id.auth_register_identity_input;
                PhoneOrEmailInputView phoneOrEmailInputView = (PhoneOrEmailInputView) view.findViewById(R.id.auth_register_identity_input);
                if (phoneOrEmailInputView != null) {
                    i = R.id.auth_register_identity_policy_link;
                    LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.auth_register_identity_policy_link);
                    if (linkifiedTextView != null) {
                        i = R.id.auth_register_identity_second_segment_card;
                        CardSegment cardSegment2 = (CardSegment) view.findViewById(R.id.auth_register_identity_second_segment_card);
                        if (cardSegment2 != null) {
                            i = R.id.auth_register_identity_segmented_control;
                            SegmentedControlContainer segmentedControlContainer = (SegmentedControlContainer) view.findViewById(R.id.auth_register_identity_segmented_control);
                            if (segmentedControlContainer != null) {
                                i = R.id.dimmer_view;
                                DimmerView dimmerView = (DimmerView) view.findViewById(R.id.dimmer_view);
                                if (dimmerView != null) {
                                    return new WidgetAuthRegisterIdentityBinding((CoordinatorLayout) view, materialButton, cardSegment, phoneOrEmailInputView, linkifiedTextView, cardSegment2, segmentedControlContainer, dimmerView);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
