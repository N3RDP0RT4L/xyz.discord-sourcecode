package com.discord.widgets.auth;

import com.discord.widgets.auth.WidgetRemoteAuthViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetRemoteAuth.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/auth/WidgetRemoteAuthViewModel$ViewState;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/widgets/auth/WidgetRemoteAuthViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetRemoteAuth$onViewBound$1 extends o implements Function1<WidgetRemoteAuthViewModel.ViewState, Unit> {
    public final /* synthetic */ WidgetRemoteAuth this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetRemoteAuth$onViewBound$1(WidgetRemoteAuth widgetRemoteAuth) {
        super(1);
        this.this$0 = widgetRemoteAuth;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetRemoteAuthViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetRemoteAuthViewModel.ViewState viewState) {
        WidgetRemoteAuth widgetRemoteAuth = this.this$0;
        m.checkNotNullExpressionValue(viewState, "it");
        widgetRemoteAuth.configureUI(viewState);
    }
}
