package com.discord.widgets.auth;

import android.view.View;
import android.widget.LinearLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetAuthBirthdayBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAuthBirthday.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetAuthBirthdayBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthBirthdayBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetAuthBirthday$binding$2 extends k implements Function1<View, WidgetAuthBirthdayBinding> {
    public static final WidgetAuthBirthday$binding$2 INSTANCE = new WidgetAuthBirthday$binding$2();

    public WidgetAuthBirthday$binding$2() {
        super(1, WidgetAuthBirthdayBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthBirthdayBinding;", 0);
    }

    public final WidgetAuthBirthdayBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.auth_policy_links;
        LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.auth_policy_links);
        if (linkifiedTextView != null) {
            i = R.id.auth_register_birthday;
            TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.auth_register_birthday);
            if (textInputLayout != null) {
                i = R.id.auth_register_button;
                MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.auth_register_button);
                if (materialButton != null) {
                    i = R.id.auth_register_container;
                    LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.auth_register_container);
                    if (linearLayout != null) {
                        i = R.id.auth_tos_opt_in;
                        MaterialCheckBox materialCheckBox = (MaterialCheckBox) view.findViewById(R.id.auth_tos_opt_in);
                        if (materialCheckBox != null) {
                            i = R.id.dimmer_view;
                            DimmerView dimmerView = (DimmerView) view.findViewById(R.id.dimmer_view);
                            if (dimmerView != null) {
                                return new WidgetAuthBirthdayBinding((CoordinatorLayout) view, linkifiedTextView, textInputLayout, materialButton, linearLayout, materialCheckBox, dimmerView);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
