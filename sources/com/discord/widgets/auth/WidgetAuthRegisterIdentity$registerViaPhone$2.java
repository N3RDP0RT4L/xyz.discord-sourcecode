package com.discord.widgets.auth;

import android.content.Context;
import android.content.Intent;
import androidx.activity.result.ActivityResultLauncher;
import b.a.k.b;
import com.discord.databinding.WidgetAuthRegisterIdentityBinding;
import com.discord.utilities.auth.RegistrationFlowRepo;
import com.discord.widgets.auth.WidgetAuthPhoneVerify;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAuthRegisterIdentity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthRegisterIdentity$registerViaPhone$2 extends o implements Function1<Void, Unit> {
    public final /* synthetic */ String $phone;
    public final /* synthetic */ WidgetAuthRegisterIdentity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAuthRegisterIdentity$registerViaPhone$2(WidgetAuthRegisterIdentity widgetAuthRegisterIdentity, String str) {
        super(1);
        this.this$0 = widgetAuthRegisterIdentity;
        this.$phone = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
        invoke2(r1);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Void r13) {
        ActivityResultLauncher<Intent> activityResultLauncher;
        WidgetAuthRegisterIdentityBinding binding;
        CharSequence b2;
        CharSequence b3;
        RegistrationFlowRepo.trackTransition$default(RegistrationFlowRepo.Companion.getINSTANCE(), "Account Identity", "success", null, 4, null);
        WidgetAuthPhoneVerify.Companion companion = WidgetAuthPhoneVerify.Companion;
        Context requireContext = this.this$0.requireContext();
        activityResultLauncher = this.this$0.phoneVerifyLauncher;
        binding = this.this$0.getBinding();
        String textOrEmpty = binding.d.getTextOrEmpty();
        b2 = b.b(this.this$0.requireContext(), R.string.sms_confirmation_title, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b3 = b.b(this.this$0.requireContext(), R.string.sms_confirmation_description, new Object[]{this.$phone}, (r4 & 4) != 0 ? b.C0034b.j : null);
        companion.launch(requireContext, activityResultLauncher, textOrEmpty, b2, b3);
    }
}
