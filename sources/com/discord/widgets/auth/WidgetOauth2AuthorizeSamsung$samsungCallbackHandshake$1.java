package com.discord.widgets.auth;

import com.discord.models.domain.ModelAuditLogEntry;
import d0.w.i.a.d;
import d0.w.i.a.e;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import org.objectweb.asm.Opcodes;
/* compiled from: WidgetOauth2AuthorizeSamsung.kt */
@e(c = "com.discord.widgets.auth.WidgetOauth2AuthorizeSamsung", f = "WidgetOauth2AuthorizeSamsung.kt", l = {Opcodes.RETURN, Opcodes.NEWARRAY, Opcodes.CHECKCAST}, m = "samsungCallbackHandshake")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00002\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0082@¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "samsungAuthCode", "Lkotlin/coroutines/Continuation;", "", "continuation", "", "samsungCallbackHandshake", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1 extends d {
    public Object L$0;
    public Object L$1;
    public Object L$2;
    public int label;
    public /* synthetic */ Object result;
    public final /* synthetic */ WidgetOauth2AuthorizeSamsung this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetOauth2AuthorizeSamsung$samsungCallbackHandshake$1(WidgetOauth2AuthorizeSamsung widgetOauth2AuthorizeSamsung, Continuation continuation) {
        super(continuation);
        this.this$0 = widgetOauth2AuthorizeSamsung;
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.samsungCallbackHandshake(null, null, this);
    }
}
