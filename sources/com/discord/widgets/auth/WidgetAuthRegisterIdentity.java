package com.discord.widgets.auth;

import andhook.lib.HookHelper;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.activity.result.ActivityResultLauncher;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetAuthRegisterIdentityBinding;
import com.discord.models.experiments.domain.Experiment;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StorePhone;
import com.discord.stores.StoreStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.auth.RegistrationFlowRepo;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.phone.PhoneOrEmailInputView;
import com.discord.views.segmentedcontrol.CardSegment;
import com.discord.views.segmentedcontrol.SegmentedControlContainer;
import d0.g;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetAuthRegisterIdentity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 >2\u00020\u0001:\u0001>B\u0007¢\u0006\u0004\b=\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\u0004J\u0017\u0010\r\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000f\u0010\u0004J\u001b\u0010\u0012\u001a\u00020\u00022\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0014\u0010\u0004J\u0017\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0015H\u0016¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u00192\b\u0010\u001b\u001a\u0004\u0018\u00010\u0015H\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u001e\u0010\u0004R\u001d\u0010$\u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001c\u0010'\u001a\b\u0012\u0004\u0012\u00020&0%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001d\u0010+\u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b)\u0010!\u001a\u0004\b*\u0010#R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u001c\u00100\u001a\u00020/8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R\u001d\u00109\u001a\u0002048B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b5\u00106\u001a\u0004\b7\u00108R\u0016\u0010:\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u0010;R\u001c\u0010<\u001a\b\u0012\u0004\u0012\u00020&0%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010(¨\u0006?"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthRegisterIdentity;", "Lcom/discord/app/AppFragment;", "", "configureUI", "()V", "configurePhoneInput", "Lcom/discord/views/phone/PhoneOrEmailInputView$Mode;", "mode", "Lcom/discord/views/segmentedcontrol/CardSegment;", "getCardSegmentForMode", "(Lcom/discord/views/phone/PhoneOrEmailInputView$Mode;)Lcom/discord/views/segmentedcontrol/CardSegment;", "configureSegmentControl", "newInputMode", "setInputMode", "(Lcom/discord/views/phone/PhoneOrEmailInputView$Mode;)V", "handleNext", "", "captchaKey", "registerViaPhone", "(Ljava/lang/String;)V", "registerViaEmail", "Landroid/os/Bundle;", "outState", "onSaveInstanceState", "(Landroid/os/Bundle;)V", "Landroid/view/View;", "view", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "Lcom/discord/utilities/view/validators/ValidationManager;", "emailValidationManager$delegate", "Lkotlin/Lazy;", "getEmailValidationManager", "()Lcom/discord/utilities/view/validators/ValidationManager;", "emailValidationManager", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "captchaLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "phoneValidationManager$delegate", "getPhoneValidationManager", "phoneValidationManager", "", "shouldValidateInputs", "Z", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/databinding/WidgetAuthRegisterIdentityBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetAuthRegisterIdentityBinding;", "binding", "inputMode", "Lcom/discord/views/phone/PhoneOrEmailInputView$Mode;", "phoneVerifyLauncher", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthRegisterIdentity extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetAuthRegisterIdentity.class, "binding", "getBinding()Lcom/discord/databinding/WidgetAuthRegisterIdentityBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String IS_PHONE_MODE = "IS_PHONE_MODE";
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetAuthRegisterIdentity$loggingConfig$1.INSTANCE, 3);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetAuthRegisterIdentity$binding$2.INSTANCE, null, 2, null);
    private PhoneOrEmailInputView.Mode inputMode = PhoneOrEmailInputView.Mode.PHONE;
    private boolean shouldValidateInputs = true;
    private final ActivityResultLauncher<Intent> phoneVerifyLauncher = WidgetAuthPhoneVerify.Companion.registerForResult(this, new WidgetAuthRegisterIdentity$phoneVerifyLauncher$1(this));
    private final ActivityResultLauncher<Intent> captchaLauncher = WidgetAuthCaptcha.Companion.registerForResult(this, new WidgetAuthRegisterIdentity$captchaLauncher$1(this));
    private final Lazy phoneValidationManager$delegate = g.lazy(new WidgetAuthRegisterIdentity$phoneValidationManager$2(this));
    private final Lazy emailValidationManager$delegate = g.lazy(new WidgetAuthRegisterIdentity$emailValidationManager$2(this));

    /* compiled from: WidgetAuthRegisterIdentity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthRegisterIdentity$Companion;", "", "", WidgetAuthRegisterIdentity.IS_PHONE_MODE, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetAuthRegisterIdentity() {
        super(R.layout.widget_auth_register_identity);
    }

    private final void configurePhoneInput() {
        getBinding().d.setMode(this.inputMode);
        if (this.inputMode == PhoneOrEmailInputView.Mode.PHONE) {
            getBinding().d.setHint(getString(R.string.form_label_phone_number));
        } else {
            getBinding().d.setHint(getString(R.string.form_label_email));
        }
        getBinding().d.getMainEditText().setImeOptions(6);
        ViewExtensions.setOnImeActionDone$default(getBinding().d.getMainTextInputLayout(), false, new WidgetAuthRegisterIdentity$configurePhoneInput$1(this), 1, null);
    }

    private final void configureSegmentControl() {
        int i = 0;
        SegmentedControlContainer.b(getBinding().g, 0, 1);
        getBinding().g.setOnSegmentSelectedChangeListener(new WidgetAuthRegisterIdentity$configureSegmentControl$1(this));
        PhoneOrEmailInputView.Mode mode = this.inputMode;
        PhoneOrEmailInputView.Mode mode2 = PhoneOrEmailInputView.Mode.PHONE;
        if (mode != mode2) {
            i = 1;
        }
        getBinding().g.setSelectedIndex(i);
        CardSegment cardSegmentForMode = getCardSegmentForMode(mode2);
        cardSegmentForMode.setText(getString(R.string.phone));
        cardSegmentForMode.setContentDescription(getString(R.string.use_phone));
        CardSegment cardSegmentForMode2 = getCardSegmentForMode(PhoneOrEmailInputView.Mode.EMAIL);
        cardSegmentForMode2.setText(getString(R.string.email));
        cardSegmentForMode2.setContentDescription(getString(R.string.use_email));
    }

    private final void configureUI() {
        getBinding().d.b(this);
        StorePhone phone = StoreStream.Companion.getPhone();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{phone}, false, null, null, new WidgetAuthRegisterIdentity$configureUI$1(phone), 14, null), this, null, 2, null), WidgetAuthRegisterIdentity.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetAuthRegisterIdentity$configureUI$2(this));
        configurePhoneInput();
        configureSegmentControl();
        getBinding().d.requestFocus();
        LinkifiedTextView linkifiedTextView = getBinding().e;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.authRegisterIdentityPolicyLink");
        b.m(linkifiedTextView, R.string.register_login_privacy_notice, new Object[]{getString(R.string.privacy_policy_url)}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().f2220b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthRegisterIdentity$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetAuthRegisterIdentity.this.handleNext();
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetAuthRegisterIdentityBinding getBinding() {
        return (WidgetAuthRegisterIdentityBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final CardSegment getCardSegmentForMode(PhoneOrEmailInputView.Mode mode) {
        if (mode == PhoneOrEmailInputView.Mode.PHONE) {
            CardSegment cardSegment = getBinding().c;
            m.checkNotNullExpressionValue(cardSegment, "binding.authRegisterIdentityFirstSegmentCard");
            return cardSegment;
        }
        CardSegment cardSegment2 = getBinding().f;
        m.checkNotNullExpressionValue(cardSegment2, "binding.authRegisterIdentitySecondSegmentCard");
        return cardSegment2;
    }

    private final ValidationManager getEmailValidationManager() {
        return (ValidationManager) this.emailValidationManager$delegate.getValue();
    }

    private final ValidationManager getPhoneValidationManager() {
        return (ValidationManager) this.phoneValidationManager$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleNext() {
        if (this.inputMode == PhoneOrEmailInputView.Mode.EMAIL) {
            if (!this.shouldValidateInputs || ValidationManager.validate$default(getEmailValidationManager(), false, 1, null)) {
                registerViaEmail();
            } else {
                RegistrationFlowRepo.trackTransition$default(RegistrationFlowRepo.Companion.getINSTANCE(), "Account Identity", "input_error", null, 4, null);
            }
        } else if (!this.shouldValidateInputs || ValidationManager.validate$default(getPhoneValidationManager(), false, 1, null)) {
            registerViaPhone$default(this, null, 1, null);
        } else {
            RegistrationFlowRepo.trackTransition$default(RegistrationFlowRepo.Companion.getINSTANCE(), "Account Identity", "input_error", null, 4, null);
        }
    }

    private final void registerViaEmail() {
        RegistrationFlowRepo.Companion companion = RegistrationFlowRepo.Companion;
        companion.getINSTANCE().setEmail(getBinding().d.getTextOrEmpty());
        RegistrationFlowRepo.trackTransition$default(companion.getINSTANCE(), "Account Identity", "success", null, 4, null);
        j.e(requireContext(), WidgetAuthRegisterAccountInformation.class, null, 4);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void registerViaPhone(String str) {
        String textOrEmpty = getBinding().d.getTextOrEmpty();
        RegistrationFlowRepo.Companion.getINSTANCE().setPhone(textOrEmpty);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.ui$default(RestCallStateKt.logNetworkAction(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().postAuthRegisterPhone(new RestAPIParams.AuthRegisterPhone(textOrEmpty, str)), false, 1, null), WidgetAuthRegisterIdentity$registerViaPhone$1.INSTANCE), this, null, 2, null), getBinding().h, 0L, 2, null), WidgetAuthRegisterIdentity.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetAuthRegisterIdentity$registerViaPhone$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetAuthRegisterIdentity$registerViaPhone$2(this, textOrEmpty));
    }

    public static /* synthetic */ void registerViaPhone$default(WidgetAuthRegisterIdentity widgetAuthRegisterIdentity, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        widgetAuthRegisterIdentity.registerViaPhone(str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setInputMode(PhoneOrEmailInputView.Mode mode) {
        if (this.inputMode != mode) {
            this.inputMode = mode;
            getBinding().d.getMainTextInputLayout().setError(null);
            configurePhoneInput();
        }
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        configureUI();
        Map<String, List<String>> errors = RegistrationFlowRepo.Companion.getINSTANCE().getErrors();
        if (errors != null) {
            getEmailValidationManager().setErrors(errors);
            Set<String> keySet = errors.keySet();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = keySet.iterator();
            while (true) {
                boolean z2 = true;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                String str = (String) next;
                if (!m.areEqual(str, NotificationCompat.CATEGORY_EMAIL) && !m.areEqual(str, "phone")) {
                    z2 = false;
                }
                if (z2) {
                    arrayList.add(next);
                }
            }
            List<String> list = u.toList(arrayList);
            if (!list.isEmpty()) {
                RegistrationFlowRepo.Companion.getINSTANCE().trackTransition("Account Identity", "response_error", list);
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        m.checkNotNullParameter(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putBoolean(IS_PHONE_MODE, this.inputMode == PhoneOrEmailInputView.Mode.PHONE);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        PhoneOrEmailInputView.Mode mode;
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        if (bundle == null || bundle.getBoolean(IS_PHONE_MODE)) {
            mode = PhoneOrEmailInputView.Mode.PHONE;
        } else {
            mode = PhoneOrEmailInputView.Mode.EMAIL;
        }
        this.inputMode = mode;
        RegistrationFlowRepo.trackTransition$default(RegistrationFlowRepo.Companion.getINSTANCE(), "Account Identity", "submitted", null, 4, null);
        StoreStream.Companion companion = StoreStream.Companion;
        boolean z2 = true;
        Experiment userExperiment = companion.getExperiments().getUserExperiment("2021-01_android_registration_flow", true);
        if ((userExperiment != null && userExperiment.getBucket() == 1) || (userExperiment != null && userExperiment.getBucket() == 3)) {
            z2 = false;
        }
        this.shouldValidateInputs = z2;
        configureUI();
        companion.getInviteSettings().trackWithInvite$app_productionGoogleRelease(WidgetAuthRegisterIdentity.class, WidgetAuthRegisterIdentity$onViewCreated$1.INSTANCE);
    }
}
