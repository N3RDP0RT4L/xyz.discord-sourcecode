package com.discord.widgets.auth;

import android.view.View;
import androidx.fragment.app.FragmentManager;
import b.a.a.k;
import com.discord.utilities.birthday.BirthdayHelper;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAuthBirthday.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthBirthday$onViewBoundOrOnResume$1 extends o implements Function1<View, Unit> {
    public final /* synthetic */ WidgetAuthBirthday this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAuthBirthday$onViewBoundOrOnResume$1(WidgetAuthBirthday widgetAuthBirthday) {
        super(1);
        this.this$0 = widgetAuthBirthday;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        Long birthday;
        m.checkNotNullParameter(view, "it");
        k.a aVar = k.k;
        FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        String string = this.this$0.getString(R.string.age_gate_date_of_birth);
        m.checkNotNullExpressionValue(string, "getString(R.string.age_gate_date_of_birth)");
        birthday = this.this$0.getBirthday();
        aVar.a(parentFragmentManager, string, birthday != null ? birthday.longValue() : BirthdayHelper.INSTANCE.defaultInputAge(), BirthdayHelper.INSTANCE.getMaxDateOfBirth()).l = new WidgetAuthBirthday$onViewBoundOrOnResume$1$$special$$inlined$apply$lambda$1(this);
    }
}
