package com.discord.widgets.auth;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetAgeVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004¨\u0006\u0005"}, d2 = {"", "dateOfBirth", "", "invoke", "(J)V", "com/discord/widgets/auth/WidgetAgeVerify$configureBirthdayInput$1$1$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAgeVerify$configureBirthdayInput$1$$special$$inlined$apply$lambda$1 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ WidgetAgeVerify$configureBirthdayInput$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAgeVerify$configureBirthdayInput$1$$special$$inlined$apply$lambda$1(WidgetAgeVerify$configureBirthdayInput$1 widgetAgeVerify$configureBirthdayInput$1) {
        super(1);
        this.this$0 = widgetAgeVerify$configureBirthdayInput$1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    public final void invoke(long j) {
        WidgetAgeVerifyViewModel viewModel;
        viewModel = this.this$0.this$0.getViewModel();
        viewModel.setDateOfBirth(j);
    }
}
