package com.discord.widgets.auth;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import b.a.k.b;
import com.discord.utilities.view.validators.Input;
import com.discord.utilities.view.validators.InputValidator;
import com.discord.utilities.view.validators.ValidationManager;
import d0.g0.s;
import d0.g0.w;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetOauth2Authorize.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/view/validators/ValidationManager;", "invoke", "()Lcom/discord/utilities/view/validators/ValidationManager;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetOauth2Authorize$validationManager$2 extends o implements Function0<ValidationManager> {
    public final /* synthetic */ WidgetOauth2Authorize this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetOauth2Authorize$validationManager$2(WidgetOauth2Authorize widgetOauth2Authorize) {
        super(0);
        this.this$0 = widgetOauth2Authorize;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ValidationManager invoke() {
        LinearLayout linearLayout = this.this$0.getBinding().h;
        m.checkNotNullExpressionValue(linearLayout, "binding.oauthAuthorizeLoading");
        LinearLayout linearLayout2 = this.this$0.getBinding().h;
        m.checkNotNullExpressionValue(linearLayout2, "binding.oauthAuthorizeLoading");
        return new ValidationManager(new Input<View>("_root", linearLayout, new InputValidator[0]) { // from class: com.discord.widgets.auth.WidgetOauth2Authorize$validationManager$2.1
            @Override // com.discord.utilities.view.validators.Input
            public boolean setErrorMessage(CharSequence charSequence) {
                CharSequence g;
                if (charSequence == null) {
                    return false;
                }
                Context context = getView().getContext();
                g = b.g(charSequence, new Object[0], (r3 & 2) != 0 ? b.e.j : null);
                b.a.d.m.h(context, g, 1, null, 8);
                return true;
            }
        }, new Input<View>("scope", linearLayout2, new InputValidator[0]) { // from class: com.discord.widgets.auth.WidgetOauth2Authorize$validationManager$2.2
            @Override // com.discord.utilities.view.validators.Input
            public boolean setErrorMessage(CharSequence charSequence) {
                CharSequence d;
                if (charSequence == null) {
                    return false;
                }
                Integer intOrNull = s.toIntOrNull(charSequence.toString());
                String str = (String) u.getOrNull(w.split$default((CharSequence) WidgetOauth2Authorize$validationManager$2.this.this$0.getOauth2ViewModel().getOauthAuthorize().getScope(), new char[]{' '}, false, 0, 6, (Object) null), intOrNull != null ? intOrNull.intValue() : -1);
                if (str == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append('[');
                    sb.append(charSequence);
                    sb.append(']');
                    str = sb.toString();
                }
                Context context = getView().getContext();
                d = b.d(getView(), R.string.oauth2_request_invalid_scope, new Object[]{str}, (r4 & 4) != 0 ? b.c.j : null);
                b.a.d.m.h(context, d, 1, null, 8);
                return true;
            }
        });
    }
}
