package com.discord.widgets.auth;

import com.discord.restapi.RestAPIParams;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetOauth2Authorize.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;", "response", "", "invoke", "(Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetOauth2Authorize$onViewBoundOrOnResume$2 extends o implements Function1<RestAPIParams.OAuth2Authorize.ResponseGet, Unit> {
    public final /* synthetic */ WidgetOauth2Authorize this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetOauth2Authorize$onViewBoundOrOnResume$2(WidgetOauth2Authorize widgetOauth2Authorize) {
        super(1);
        this.this$0 = widgetOauth2Authorize;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RestAPIParams.OAuth2Authorize.ResponseGet responseGet) {
        invoke2(responseGet);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RestAPIParams.OAuth2Authorize.ResponseGet responseGet) {
        m.checkNotNullParameter(responseGet, "response");
        this.this$0.getOauth2ViewModel().setOauthGetResponse(responseGet);
        this.this$0.configureUI(responseGet);
    }
}
