package com.discord.widgets.auth;

import com.discord.databinding.WidgetAuthRegisterIdentityBinding;
import com.discord.utilities.auth.AuthUtils;
import com.discord.utilities.view.validators.BasicTextInputValidator;
import com.discord.utilities.view.validators.Input;
import com.discord.utilities.view.validators.ValidationManager;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetAuthRegisterIdentity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/view/validators/ValidationManager;", "invoke", "()Lcom/discord/utilities/view/validators/ValidationManager;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthRegisterIdentity$phoneValidationManager$2 extends o implements Function0<ValidationManager> {
    public final /* synthetic */ WidgetAuthRegisterIdentity this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAuthRegisterIdentity$phoneValidationManager$2(WidgetAuthRegisterIdentity widgetAuthRegisterIdentity) {
        super(0);
        this.this$0 = widgetAuthRegisterIdentity;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ValidationManager invoke() {
        WidgetAuthRegisterIdentityBinding binding;
        binding = this.this$0.getBinding();
        return new ValidationManager(new Input.TextInputLayoutInput("phone", binding.d.getMainTextInputLayout(), BasicTextInputValidator.Companion.createRequiredInputValidator(R.string.phone_required), AuthUtils.INSTANCE.createPhoneInputValidator(R.string.phone_invalid)));
    }
}
