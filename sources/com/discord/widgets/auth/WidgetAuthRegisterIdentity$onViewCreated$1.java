package com.discord.widgets.auth;

import com.discord.models.domain.ModelInvite;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.auth.RegistrationFlowRepo;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetAuthRegisterIdentity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelInvite;", "invite", "", "invoke", "(Lcom/discord/models/domain/ModelInvite;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthRegisterIdentity$onViewCreated$1 extends o implements Function1<ModelInvite, Unit> {
    public static final WidgetAuthRegisterIdentity$onViewCreated$1 INSTANCE = new WidgetAuthRegisterIdentity$onViewCreated$1();

    public WidgetAuthRegisterIdentity$onViewCreated$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelInvite modelInvite) {
        invoke2(modelInvite);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelInvite modelInvite) {
        AnalyticsTracker.INSTANCE.registerViewed(modelInvite);
        RegistrationFlowRepo.Companion companion = RegistrationFlowRepo.Companion;
        companion.getINSTANCE().setInvite(modelInvite);
        RegistrationFlowRepo.trackTransition$default(companion.getINSTANCE(), "Account Identity", "viewed", null, 4, null);
    }
}
