package com.discord.widgets.auth;

import andhook.lib.HookHelper;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import androidx.activity.result.ActivityResultLauncher;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetAuthRegisterAccountInformationBinding;
import com.discord.models.experiments.domain.Experiment;
import com.discord.stores.StoreAuthentication;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.auth.RegistrationFlowRepo;
import com.discord.utilities.birthday.BirthdayHelper;
import com.discord.utilities.error.Error;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.home.HomeConfig;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetAuthRegisterAccountInformation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b/\u0010\fJ\u001b\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\fR\u001c\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u0018R\u001c\u0010\u001b\u001a\u00020\u001a8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u00168B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\u0016\u0010!\u001a\u00020\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\u0018R\u001c\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010\u0015R\u001d\u0010(\u001a\u00020#8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001d\u0010.\u001a\u00020)8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-¨\u00060"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthRegisterAccountInformation;", "Lcom/discord/app/AppFragment;", "", "captchaKey", "", "register", "(Ljava/lang/String;)V", "Lcom/discord/utilities/error/Error;", "error", "handleError", "(Lcom/discord/utilities/error/Error;)V", "configureUI", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "birthdayLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "", "shouldShowAgeGate", "Z", "shouldValidateInputs", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "isConsented", "()Z", "isConsentRequired", "captchaLauncher", "Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager$delegate", "Lkotlin/Lazy;", "getValidationManager", "()Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager", "Lcom/discord/databinding/WidgetAuthRegisterAccountInformationBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetAuthRegisterAccountInformationBinding;", "binding", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthRegisterAccountInformation extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetAuthRegisterAccountInformation.class, "binding", "getBinding()Lcom/discord/databinding/WidgetAuthRegisterAccountInformationBinding;", 0)};
    private boolean isConsentRequired;
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetAuthRegisterAccountInformation$loggingConfig$1.INSTANCE, 3);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetAuthRegisterAccountInformation$binding$2.INSTANCE, null, 2, null);
    private boolean shouldValidateInputs = true;
    private boolean shouldShowAgeGate = true;
    private final ActivityResultLauncher<Intent> captchaLauncher = WidgetAuthCaptcha.Companion.registerForResult(this, new WidgetAuthRegisterAccountInformation$captchaLauncher$1(this));
    private final ActivityResultLauncher<Intent> birthdayLauncher = WidgetAuthBirthday.Companion.registerForResult(this, new WidgetAuthRegisterAccountInformation$birthdayLauncher$1(this));
    private final Lazy validationManager$delegate = g.lazy(new WidgetAuthRegisterAccountInformation$validationManager$2(this));

    public WidgetAuthRegisterAccountInformation() {
        super(R.layout.widget_auth_register_account_information);
    }

    private final void configureUI() {
        getBinding().e.requestFocus();
        if (this.isConsentRequired) {
            TextInputLayout textInputLayout = getBinding().d;
            m.checkNotNullExpressionValue(textInputLayout, "binding.authRegisterAccountInformationPasswordWrap");
            EditText editText = textInputLayout.getEditText();
            if (editText != null) {
                editText.setImeOptions(6);
            }
        } else {
            TextInputLayout textInputLayout2 = getBinding().d;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.authRegisterAccountInformationPasswordWrap");
            ViewExtensions.setOnImeActionDone$default(textInputLayout2, false, new WidgetAuthRegisterAccountInformation$configureUI$1(this), 1, null);
        }
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthRegisterAccountInformation$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetAuthRegisterAccountInformation.register$default(WidgetAuthRegisterAccountInformation.this, null, 1, null);
            }
        });
        if (RegistrationFlowRepo.Companion.getINSTANCE().getBirthday() != null || !this.shouldShowAgeGate) {
            getBinding().c.setText(getText(R.string.register));
        } else {
            getBinding().c.setText(getText(R.string.next));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetAuthRegisterAccountInformationBinding getBinding() {
        return (WidgetAuthRegisterAccountInformationBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final ValidationManager getValidationManager() {
        return (ValidationManager) this.validationManager$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleError(Error error) {
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        Map<String, List<String>> messages = response.getMessages();
        m.checkNotNullExpressionValue(messages, "error.response.messages");
        if (messages.containsKey(BirthdayHelper.DATE_OF_BIRTH_KEY)) {
            RegistrationFlowRepo.Companion.getINSTANCE().trackTransition("Account Information", "response_error", d0.t.m.listOf(BirthdayHelper.DATE_OF_BIRTH_KEY));
            error.setShowErrorToasts(false);
            Error.Response response2 = error.getResponse();
            m.checkNotNullExpressionValue(response2, "error.response");
            Map<String, List<String>> messages2 = response2.getMessages();
            m.checkNotNullExpressionValue(messages2, "error.response.messages");
            List<String> list = messages2.get(BirthdayHelper.DATE_OF_BIRTH_KEY);
            if (list == null) {
                list = n.emptyList();
            }
            m.checkNotNullExpressionValue(list, "error.response.messages\n…IRTH_KEY) { emptyList() }");
            StoreStream.Companion.getAuthentication().setAgeGateError(u.joinToString$default(list, "\n", null, null, 0, null, null, 62, null));
            j.b(requireContext(), false, new Intent().putExtra("com.discord.intent.extra.EXTRA_HOME_CONFIG", new HomeConfig(null, null, true, 3, null)));
            return;
        }
        Error.Response response3 = error.getResponse();
        m.checkNotNullExpressionValue(response3, "error.response");
        if (response3.getMessages().isEmpty()) {
            RegistrationFlowRepo instance = RegistrationFlowRepo.Companion.getINSTANCE();
            Error.Response response4 = error.getResponse();
            m.checkNotNullExpressionValue(response4, "error.response");
            instance.trackTransition("Register", "response_error", n.listOf((Object[]) new String[]{"connection_error", error.getType().toString(), String.valueOf(response4.getCode())}));
            return;
        }
        ValidationManager validationManager = getValidationManager();
        Error.Response response5 = error.getResponse();
        m.checkNotNullExpressionValue(response5, "error.response");
        Map<String, List<String>> messages3 = response5.getMessages();
        m.checkNotNullExpressionValue(messages3, "error.response.messages");
        List<String> mutableList = u.toMutableList((Collection) validationManager.setErrors(messages3));
        Error.Response response6 = error.getResponse();
        m.checkNotNullExpressionValue(response6, "error.response");
        List<String> mutableList2 = u.toMutableList((Collection) response6.getMessages().keySet());
        RegistrationFlowRepo.Companion companion = RegistrationFlowRepo.Companion;
        RegistrationFlowRepo instance2 = companion.getINSTANCE();
        Error.Response response7 = error.getResponse();
        m.checkNotNullExpressionValue(response7, "error.response");
        instance2.setErrors(response7.getMessages());
        WidgetAuthCaptcha.Companion.processErrorsForCaptcha(requireContext(), this.captchaLauncher, mutableList, error);
        if (mutableList.contains(NotificationCompat.CATEGORY_EMAIL)) {
            requireActivity().onBackPressed();
            mutableList.remove(NotificationCompat.CATEGORY_EMAIL);
            mutableList2.remove(NotificationCompat.CATEGORY_EMAIL);
        }
        companion.getINSTANCE().trackTransition("Account Information", "response_error", mutableList2);
        error.setShowErrorToasts(!mutableList.isEmpty());
    }

    private final boolean isConsented() {
        if (this.isConsentRequired) {
            MaterialCheckBox materialCheckBox = getBinding().f;
            m.checkNotNullExpressionValue(materialCheckBox, "binding.authTosOptIn");
            if (!materialCheckBox.isChecked()) {
                return false;
            }
        }
        return true;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void register(String str) {
        if (!this.shouldValidateInputs || ValidationManager.validate$default(getValidationManager(), false, 1, null)) {
            RegistrationFlowRepo.Companion companion = RegistrationFlowRepo.Companion;
            if (companion.getINSTANCE().getBirthday() != null || !this.shouldShowAgeGate) {
                String uTCDateTime = companion.getINSTANCE().getBirthday() != null ? TimeUtils.toUTCDateTime(companion.getINSTANCE().getBirthday(), TimeUtils.UTCFormat.SHORT) : null;
                getBinding().c.setIsLoading(true);
                StoreAuthentication authentication = StoreStream.Companion.getAuthentication();
                TextInputLayout textInputLayout = getBinding().e;
                m.checkNotNullExpressionValue(textInputLayout, "binding.authRegisterAccountInformationUsernameWrap");
                String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
                String email = companion.getINSTANCE().getEmail();
                String phoneToken = companion.getINSTANCE().getPhoneToken();
                TextInputLayout textInputLayout2 = getBinding().d;
                m.checkNotNullExpressionValue(textInputLayout2, "binding.authRegisterAccountInformationPasswordWrap");
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(authentication.register(textOrEmpty, email, phoneToken, ViewExtensions.getTextOrEmpty(textInputLayout2), str, isConsented(), uTCDateTime), this, null, 2, null), WidgetAuthRegisterAccountInformation.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetAuthRegisterAccountInformation$register$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetAuthRegisterAccountInformation$register$1.INSTANCE);
                return;
            }
            RegistrationFlowRepo.trackTransition$default(companion.getINSTANCE(), "Account Information", "success", null, 4, null);
            AnalyticsTracker.openModal$default("Enter Your Birthday", "Register", null, 4, null);
            WidgetAuthBirthday.Companion.launch(requireContext(), this.birthdayLauncher);
            return;
        }
        RegistrationFlowRepo.trackTransition$default(RegistrationFlowRepo.Companion.getINSTANCE(), "Account Information", "input_error", null, 4, null);
    }

    public static /* synthetic */ void register$default(WidgetAuthRegisterAccountInformation widgetAuthRegisterAccountInformation, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        widgetAuthRegisterAccountInformation.register(str);
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        RegistrationFlowRepo.Companion companion = RegistrationFlowRepo.Companion;
        RegistrationFlowRepo.trackTransition$default(companion.getINSTANCE(), "Account Information", "submitted", null, 4, null);
        StoreStream.Companion companion2 = StoreStream.Companion;
        Experiment userExperiment = companion2.getExperiments().getUserExperiment("2021-01_android_registration_flow", true);
        this.shouldValidateInputs = (userExperiment == null || userExperiment.getBucket() != 1) && (userExperiment == null || userExperiment.getBucket() != 3);
        boolean z2 = (userExperiment == null || userExperiment.getBucket() != 2) && (userExperiment == null || userExperiment.getBucket() != 3);
        this.shouldShowAgeGate = z2;
        int i = 8;
        if (!z2) {
            this.isConsentRequired = companion2.getAuthentication().isConsentRequired();
        } else {
            this.isConsentRequired = false;
            LinkifiedTextView linkifiedTextView = getBinding().f2219b;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.authPolicyLinks");
            linkifiedTextView.setVisibility(8);
        }
        MaterialCheckBox materialCheckBox = getBinding().f;
        m.checkNotNullExpressionValue(materialCheckBox, "binding.authTosOptIn");
        if (this.isConsentRequired) {
            i = 0;
        }
        materialCheckBox.setVisibility(i);
        LinkifiedTextView linkifiedTextView2 = getBinding().f2219b;
        m.checkNotNullExpressionValue(linkifiedTextView2, "binding.authPolicyLinks");
        b.m(linkifiedTextView2, this.isConsentRequired ? R.string.terms_privacy_opt_in : R.string.terms_privacy, new Object[]{getString(R.string.terms_of_service_url), getString(R.string.privacy_policy_url)}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().c.setIsLoading(false);
        RegistrationFlowRepo.trackTransition$default(companion.getINSTANCE(), "Account Information", "viewed", null, 4, null);
        TextInputLayout textInputLayout = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout, "binding.authRegisterAccountInformationUsernameWrap");
        ViewExtensions.setText(textInputLayout, companion.getINSTANCE().getUsername());
        TextInputLayout textInputLayout2 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.authRegisterAccountInformationPasswordWrap");
        ViewExtensions.setText(textInputLayout2, companion.getINSTANCE().getPassword());
        TextInputLayout textInputLayout3 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.authRegisterAccountInformationUsernameWrap");
        ViewExtensions.addBindedTextWatcher(textInputLayout3, this, new WidgetAuthRegisterAccountInformation$onViewBound$1(this));
        TextInputLayout textInputLayout4 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.authRegisterAccountInformationPasswordWrap");
        ViewExtensions.addBindedTextWatcher(textInputLayout4, this, new WidgetAuthRegisterAccountInformation$onViewBound$2(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Map<String, List<String>> errors = RegistrationFlowRepo.Companion.getINSTANCE().getErrors();
        if (errors != null) {
            getValidationManager().setErrors(errors);
            Set<String> keySet = errors.keySet();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = keySet.iterator();
            while (true) {
                boolean z2 = true;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                String str = (String) next;
                if (!m.areEqual(str, "username") && !m.areEqual(str, "password")) {
                    z2 = false;
                }
                if (z2) {
                    arrayList.add(next);
                }
            }
            List<String> list = u.toList(arrayList);
            if (!list.isEmpty()) {
                RegistrationFlowRepo.Companion.getINSTANCE().trackTransition("Account Information", "response_error", list);
            }
        }
        configureUI();
    }
}
