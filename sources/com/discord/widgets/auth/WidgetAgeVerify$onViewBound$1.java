package com.discord.widgets.auth;

import com.discord.widgets.auth.WidgetAgeVerifyViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetAgeVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;", "it", "", "invoke", "(Lcom/discord/widgets/auth/WidgetAgeVerifyViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAgeVerify$onViewBound$1 extends o implements Function1<WidgetAgeVerifyViewModel.ViewState, Unit> {
    public final /* synthetic */ WidgetAgeVerify this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAgeVerify$onViewBound$1(WidgetAgeVerify widgetAgeVerify) {
        super(1);
        this.this$0 = widgetAgeVerify;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetAgeVerifyViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetAgeVerifyViewModel.ViewState viewState) {
        m.checkNotNullParameter(viewState, "it");
        this.this$0.configureUI(viewState);
    }
}
