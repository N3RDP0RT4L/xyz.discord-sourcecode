package com.discord.widgets.auth;

import com.discord.models.domain.auth.ModelLoginResult;
import com.discord.utilities.analytics.AnalyticsTracker;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetAuthMfa.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/domain/auth/ModelLoginResult;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/models/domain/auth/ModelLoginResult;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthMfa$evaluateCode$1 extends o implements Function1<ModelLoginResult, Unit> {
    public static final WidgetAuthMfa$evaluateCode$1 INSTANCE = new WidgetAuthMfa$evaluateCode$1();

    public WidgetAuthMfa$evaluateCode$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelLoginResult modelLoginResult) {
        invoke2(modelLoginResult);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelLoginResult modelLoginResult) {
        AnalyticsTracker.INSTANCE.loginAttempt(true);
    }
}
