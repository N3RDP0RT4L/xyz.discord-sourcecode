package com.discord.widgets.auth;

import andhook.lib.HookHelper;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.activity.result.ActivityResultLauncher;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import b.a.d.m;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetAuthLoginBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StorePhone;
import com.discord.stores.StoreStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.auth.GoogleSmartLockManager;
import com.discord.utilities.auth.GoogleSmartLockManagerKt;
import com.discord.utilities.auth.RegistrationFlowRepo;
import com.discord.utilities.error.Error;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.auth.WidgetAuthCaptcha;
import com.discord.widgets.auth.WidgetAuthPhoneVerify;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.g0.t;
import d0.t.u;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetAuthLogin.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 H2\u00020\u0001:\u0001HB\u0007¢\u0006\u0004\bG\u0010\u0017J\u001b\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J%\u0010\u000b\u001a\u00020\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ5\u0010\u000b\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u00022\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\u000fJ\u001f\u0010\u0010\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J#\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00122\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0019\u0010\u001a\u001a\u00020\u00042\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018H\u0016¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001cH\u0016¢\u0006\u0004\b\u001e\u0010\u001fR\u001c\u0010\"\u001a\b\u0012\u0004\u0012\u00020!0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u001d\u0010)\u001a\u00020$8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\"\u0010+\u001a\u00020*8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R\u001c\u00102\u001a\u0002018\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105R$\u00107\u001a\u0004\u0018\u0001068\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b7\u00108\u001a\u0004\b9\u0010:\"\u0004\b;\u0010<R\u001c\u0010=\u001a\b\u0012\u0004\u0012\u00020!0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010#R\u001c\u0010>\u001a\b\u0012\u0004\u0012\u00020!0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010#R\u001c\u0010?\u001a\b\u0012\u0004\u0012\u00020!0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010#R\u001c\u0010@\u001a\b\u0012\u0004\u0012\u00020!0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010#R\u001d\u0010F\u001a\u00020A8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bB\u0010C\u001a\u0004\bD\u0010E¨\u0006I"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLogin;", "Lcom/discord/app/AppFragment;", "", "captchaKey", "", "forgotPassword", "(Ljava/lang/String;)V", NotificationCompat.CATEGORY_EMAIL, "showEmailSentToast", "", "undelete", "login", "(Ljava/lang/String;Z)V", ModelAuditLogEntry.CHANGE_KEY_ID, "password", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V", "onLoginSuccess", "(Ljava/lang/String;Ljava/lang/String;)V", "Lcom/discord/utilities/error/Error;", "error", "handleError", "(Lcom/discord/utilities/error/Error;Ljava/lang/String;)V", "loadCachedLogin", "()V", "Landroid/os/Bundle;", "savedInstanceState", "onCreate", "(Landroid/os/Bundle;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "captchaLoginLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/databinding/WidgetAuthLoginBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetAuthLoginBinding;", "binding", "Lcom/discord/utilities/auth/GoogleSmartLockManager;", "googleSmartLockManager", "Lcom/discord/utilities/auth/GoogleSmartLockManager;", "getGoogleSmartLockManager", "()Lcom/discord/utilities/auth/GoogleSmartLockManager;", "setGoogleSmartLockManager", "(Lcom/discord/utilities/auth/GoogleSmartLockManager;)V", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "smartLockCredentials", "Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "getSmartLockCredentials", "()Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;", "setSmartLockCredentials", "(Lcom/discord/utilities/auth/GoogleSmartLockManager$SmartLockCredentials;)V", "undeleteAccountLauncher", "phoneVerifyPasswordLauncher", "captchaForgotPasswordLauncher", "phoneVerifyLoginLauncher", "Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager$delegate", "Lkotlin/Lazy;", "getValidationManager", "()Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthLogin extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetAuthLogin.class, "binding", "getBinding()Lcom/discord/databinding/WidgetAuthLoginBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final String GOOGLE_SMARTLOCK_LOGIN_EXTRA_ID = "smartlock_extra_id";
    public static final String GOOGLE_SMARTLOCK_LOGIN_EXTRA_PASSWORD = "smartlock_extra_password";
    private final ActivityResultLauncher<Intent> captchaForgotPasswordLauncher;
    private final ActivityResultLauncher<Intent> captchaLoginLauncher;
    public GoogleSmartLockManager googleSmartLockManager;
    private final ActivityResultLauncher<Intent> phoneVerifyLoginLauncher;
    private final ActivityResultLauncher<Intent> phoneVerifyPasswordLauncher;
    private GoogleSmartLockManager.SmartLockCredentials smartLockCredentials;
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetAuthLogin$loggingConfig$1.INSTANCE, 3);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetAuthLogin$binding$2.INSTANCE, null, 2, null);
    private final ActivityResultLauncher<Intent> undeleteAccountLauncher = WidgetAuthUndeleteAccount.Companion.registerForResult(this, new WidgetAuthLogin$undeleteAccountLauncher$1(this));
    private final Lazy validationManager$delegate = g.lazy(new WidgetAuthLogin$validationManager$2(this));

    /* compiled from: WidgetAuthLogin.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/auth/WidgetAuthLogin$Companion;", "", "", "GOOGLE_SMARTLOCK_LOGIN_EXTRA_ID", "Ljava/lang/String;", "GOOGLE_SMARTLOCK_LOGIN_EXTRA_PASSWORD", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetAuthLogin() {
        super(R.layout.widget_auth_login);
        WidgetAuthCaptcha.Companion companion = WidgetAuthCaptcha.Companion;
        this.captchaForgotPasswordLauncher = companion.registerForResult(this, new WidgetAuthLogin$captchaForgotPasswordLauncher$1(this));
        this.captchaLoginLauncher = companion.registerForResult(this, new WidgetAuthLogin$captchaLoginLauncher$1(this));
        WidgetAuthPhoneVerify.Companion companion2 = WidgetAuthPhoneVerify.Companion;
        this.phoneVerifyLoginLauncher = companion2.registerForResult(this, new WidgetAuthLogin$phoneVerifyLoginLauncher$1(this));
        this.phoneVerifyPasswordLauncher = companion2.registerForResult(this, new WidgetAuthLogin$phoneVerifyPasswordLauncher$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void forgotPassword(String str) {
        String textOrEmpty = getBinding().e.getTextOrEmpty();
        if (textOrEmpty.length() == 0) {
            m.g(getContext(), R.string.login_required, 0, null, 12);
        } else {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.ui(StoreStream.Companion.getAuthentication().forgotPassword(textOrEmpty, str)), getBinding().h, 0L, 2, null), (r18 & 1) != 0 ? null : getContext(), "REST: forgotPassword", (r18 & 4) != 0 ? null : null, new WidgetAuthLogin$forgotPassword$1(this, textOrEmpty), (r18 & 16) != 0 ? null : new WidgetAuthLogin$forgotPassword$2(this, textOrEmpty), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
        }
    }

    public static /* synthetic */ void forgotPassword$default(WidgetAuthLogin widgetAuthLogin, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        widgetAuthLogin.forgotPassword(str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetAuthLoginBinding getBinding() {
        return (WidgetAuthLoginBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ValidationManager getValidationManager() {
        return (ValidationManager) this.validationManager$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleError(Error error, String str) {
        Error.Response response = error.getResponse();
        d0.z.d.m.checkNotNullExpressionValue(response, "error.response");
        boolean z2 = response.getCode() == 20011;
        boolean z3 = response.getCode() == 20013;
        boolean z4 = response.getCode() == 70007;
        if (response.getCode() == 50035) {
            if (str != null && this.smartLockCredentials != null) {
                GoogleSmartLockManager googleSmartLockManager = this.googleSmartLockManager;
                if (googleSmartLockManager == null) {
                    d0.z.d.m.throwUninitializedPropertyAccessException("googleSmartLockManager");
                }
                googleSmartLockManager.getSmartLockRepo().onSmartLockCredentialsFailed();
                GoogleSmartLockManager googleSmartLockManager2 = this.googleSmartLockManager;
                if (googleSmartLockManager2 == null) {
                    d0.z.d.m.throwUninitializedPropertyAccessException("googleSmartLockManager");
                }
                googleSmartLockManager2.deleteCredentials(str);
                this.smartLockCredentials = null;
                GoogleSmartLockManagerKt.clearSmartLockCredentials(getMostRecentIntent());
            }
        } else if (z2) {
            WidgetAuthUndeleteAccount.Companion.launch(requireContext(), this.undeleteAccountLauncher, false);
        } else if (z3) {
            WidgetAuthUndeleteAccount.Companion.launch(requireContext(), this.undeleteAccountLauncher, true);
        } else if (z4) {
            WidgetAuthPhoneVerify.Companion companion = WidgetAuthPhoneVerify.Companion;
            Context requireContext = requireContext();
            ActivityResultLauncher<Intent> activityResultLauncher = this.phoneVerifyLoginLauncher;
            String textOrEmpty = getBinding().e.getTextOrEmpty();
            String string = getString(R.string.phone_ip_authorization_title);
            d0.z.d.m.checkNotNullExpressionValue(string, "getString(R.string.phone_ip_authorization_title)");
            String string2 = getString(R.string.phone_ip_authorization_subtitle);
            d0.z.d.m.checkNotNullExpressionValue(string2, "getString(R.string.phone…p_authorization_subtitle)");
            companion.launch(requireContext, activityResultLauncher, textOrEmpty, string, string2);
        } else {
            Error.Response response2 = error.getResponse();
            d0.z.d.m.checkNotNullExpressionValue(response2, "error.response");
            if (!response2.getMessages().isEmpty()) {
                ValidationManager validationManager = getValidationManager();
                Map<String, List<String>> messages = response.getMessages();
                d0.z.d.m.checkNotNullExpressionValue(messages, "errorResponse.messages");
                List<String> mutableList = u.toMutableList((Collection) validationManager.setErrors(messages));
                WidgetAuthCaptcha.Companion.processErrorsForCaptcha(requireContext(), this.captchaLoginLauncher, mutableList, error);
                error.setShowErrorToasts(!mutableList.isEmpty());
            }
        }
    }

    public static /* synthetic */ void handleError$default(WidgetAuthLogin widgetAuthLogin, Error error, String str, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        widgetAuthLogin.handleError(error, str);
    }

    private final void loadCachedLogin() {
        String savedLogin = StoreStream.Companion.getAuthentication().getSavedLogin();
        if (savedLogin == null || !(!t.isBlank(savedLogin))) {
            getBinding().e.requestFocus();
            return;
        }
        getBinding().e.setText(savedLogin);
        getBinding().g.requestFocus();
    }

    private final void login(String str, boolean z2) {
        if (ValidationManager.validate$default(getValidationManager(), false, 1, null)) {
            String textOrEmpty = getBinding().e.getTextOrEmpty();
            TextInputLayout textInputLayout = getBinding().g;
            d0.z.d.m.checkNotNullExpressionValue(textInputLayout, "binding.authLoginPasswordWrap");
            login(textOrEmpty, ViewExtensions.getTextOrEmpty(textInputLayout), str, z2);
        }
    }

    public static /* synthetic */ void login$default(WidgetAuthLogin widgetAuthLogin, String str, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        if ((i & 2) != 0) {
            z2 = false;
        }
        widgetAuthLogin.login(str, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onLoginSuccess(String str, String str2) {
        if (GoogleSmartLockManagerKt.hasSmartLockCredentials(getMostRecentIntent())) {
            GoogleSmartLockManager googleSmartLockManager = this.googleSmartLockManager;
            if (googleSmartLockManager == null) {
                d0.z.d.m.throwUninitializedPropertyAccessException("googleSmartLockManager");
            }
            googleSmartLockManager.getSmartLockRepo().onLoginWithSmartLockSuccess();
        }
        GoogleSmartLockManager googleSmartLockManager2 = this.googleSmartLockManager;
        if (googleSmartLockManager2 == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("googleSmartLockManager");
        }
        googleSmartLockManager2.saveCredentials(str, str2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showEmailSentToast(String str) {
        CharSequence b2;
        b2 = b.b(requireContext(), R.string.email_verification_instructions_body, new Object[]{str}, (r4 & 4) != 0 ? b.C0034b.j : null);
        m.h(getContext(), b2, 0, null, 12);
    }

    public final GoogleSmartLockManager getGoogleSmartLockManager() {
        GoogleSmartLockManager googleSmartLockManager = this.googleSmartLockManager;
        if (googleSmartLockManager == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("googleSmartLockManager");
        }
        return googleSmartLockManager;
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    public final GoogleSmartLockManager.SmartLockCredentials getSmartLockCredentials() {
        return this.smartLockCredentials;
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        RegistrationFlowRepo.Companion.getINSTANCE().setRegistering(false);
        FragmentActivity requireActivity = requireActivity();
        d0.z.d.m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        this.googleSmartLockManager = new GoogleSmartLockManager(requireActivity, null, 2, null);
        this.smartLockCredentials = GoogleSmartLockManagerKt.toSmartLockCredentials(getMostRecentIntent());
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        d0.z.d.m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getInviteSettings().trackWithInvite$app_productionGoogleRelease(WidgetAuthLogin.class, WidgetAuthLogin$onViewBound$1.INSTANCE);
        loadCachedLogin();
        getBinding().f2215b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthLogin$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetAuthLogin.login$default(WidgetAuthLogin.this, null, false, 3, null);
            }
        });
        TextInputLayout textInputLayout = getBinding().g;
        d0.z.d.m.checkNotNullExpressionValue(textInputLayout, "binding.authLoginPasswordWrap");
        ViewExtensions.setOnImeActionDone$default(textInputLayout, false, new WidgetAuthLogin$onViewBound$3(this), 1, null);
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthLogin$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetAuthLogin.forgotPassword$default(WidgetAuthLogin.this, null, 1, null);
            }
        });
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthLogin$onViewBound$5

            /* compiled from: WidgetAuthLogin.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.auth.WidgetAuthLogin$onViewBound$5$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<View, Unit> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(View view) {
                    invoke2(view);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(View view) {
                    d0.z.d.m.checkNotNullParameter(view, "it");
                    try {
                        view.getContext().startActivity(new Intent("android.settings.ACCESSIBILITY_SETTINGS"));
                    } catch (ActivityNotFoundException unused) {
                        m.g(view.getContext(), R.string.password_manager_open_settings_error, 0, null, 12);
                    }
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetNoticeDialog.Builder negativeButton$default = WidgetNoticeDialog.Builder.setNegativeButton$default(new WidgetNoticeDialog.Builder(WidgetAuthLogin.this.requireContext()).setTitle(R.string.password_manager).setMessage(R.string.password_manager_info_android).setPositiveButton(R.string.password_manager_open_settings, AnonymousClass1.INSTANCE), (int) R.string.cancel, (Function1) null, 2, (Object) null);
                FragmentManager parentFragmentManager = WidgetAuthLogin.this.getParentFragmentManager();
                d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                negativeButton$default.show(parentFragmentManager);
            }
        });
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetAuthLogin$onViewBound$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AppFragment.hideKeyboard$default(WidgetAuthLogin.this, null, 1, null);
            }
        });
        getBinding().e.b(this);
        GoogleSmartLockManager.SmartLockCredentials smartLockCredentials = this.smartLockCredentials;
        if (smartLockCredentials != null) {
            getBinding().e.setText(smartLockCredentials.getId());
            TextInputLayout textInputLayout2 = getBinding().g;
            d0.z.d.m.checkNotNullExpressionValue(textInputLayout2, "binding.authLoginPasswordWrap");
            ViewExtensions.setText(textInputLayout2, smartLockCredentials.getPassword());
            login$default(this, null, false, 3, null);
        }
        StorePhone phone = companion.getPhone();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{phone}, false, null, null, new WidgetAuthLogin$onViewBound$8(phone), 14, null), this, null, 2, null), WidgetAuthLogin.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetAuthLogin$onViewBound$9(this));
    }

    public final void setGoogleSmartLockManager(GoogleSmartLockManager googleSmartLockManager) {
        d0.z.d.m.checkNotNullParameter(googleSmartLockManager, "<set-?>");
        this.googleSmartLockManager = googleSmartLockManager;
    }

    public final void setSmartLockCredentials(GoogleSmartLockManager.SmartLockCredentials smartLockCredentials) {
        this.smartLockCredentials = smartLockCredentials;
    }

    public static /* synthetic */ void login$default(WidgetAuthLogin widgetAuthLogin, String str, String str2, String str3, boolean z2, int i, Object obj) {
        if ((i & 4) != 0) {
            str3 = null;
        }
        if ((i & 8) != 0) {
            z2 = false;
        }
        widgetAuthLogin.login(str, str2, str3, z2);
    }

    private final void login(String str, String str2, String str3, boolean z2) {
        Context context = getContext();
        if (context != null) {
            d0.z.d.m.checkNotNullExpressionValue(context, "context ?: return");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.ui$default(StoreStream.Companion.getAuthentication().login(str, str2, str3, z2, GoogleSmartLockManagerKt.hasSmartLockCredentials(getMostRecentIntent()) ? RestAPIParams.AuthLogin.LoginSource.LOGIN_SOURCE_KEYCHAIN_AUTO : null), this, null, 2, null), getBinding().h, 0L, 2, null), (r18 & 1) != 0 ? null : context, "REST: login", (r18 & 4) != 0 ? null : null, new WidgetAuthLogin$login$1(this, str, str2, context), (r18 & 16) != 0 ? null : new WidgetAuthLogin$login$2(this, str), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
        }
    }
}
