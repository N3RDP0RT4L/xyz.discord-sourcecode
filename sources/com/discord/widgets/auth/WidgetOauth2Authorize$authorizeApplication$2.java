package com.discord.widgets.auth;

import android.content.Intent;
import android.net.Uri;
import com.discord.app.AppActivity;
import com.discord.app.AppLog;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.logging.Logger;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetOauth2Authorize.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponsePost;", "it", "", "invoke", "(Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponsePost;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetOauth2Authorize$authorizeApplication$2 extends o implements Function1<RestAPIParams.OAuth2Authorize.ResponsePost, Unit> {
    public final /* synthetic */ WidgetOauth2Authorize this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetOauth2Authorize$authorizeApplication$2(WidgetOauth2Authorize widgetOauth2Authorize) {
        super(1);
        this.this$0 = widgetOauth2Authorize;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RestAPIParams.OAuth2Authorize.ResponsePost responsePost) {
        invoke2(responsePost);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RestAPIParams.OAuth2Authorize.ResponsePost responsePost) {
        m.checkNotNullParameter(responsePost, "it");
        Uri parse = Uri.parse(responsePost.getLocation());
        AppLog appLog = AppLog.g;
        Logger.d$default(appLog, "Redirect OAuth flow to: " + parse, null, 2, null);
        Intent addFlags = new Intent("android.intent.action.VIEW", parse).addFlags(268435456);
        m.checkNotNullExpressionValue(addFlags, "Intent(Intent.ACTION_VIE…t.FLAG_ACTIVITY_NEW_TASK)");
        AppActivity appActivity = this.this$0.getAppActivity();
        if (appActivity != null) {
            appActivity.startActivity(addFlags);
        }
        AppActivity appActivity2 = this.this$0.getAppActivity();
        if (appActivity2 != null) {
            appActivity2.setResult(-1);
        }
        AppActivity appActivity3 = this.this$0.getAppActivity();
        if (appActivity3 != null) {
            appActivity3.onBackPressed();
        }
    }
}
