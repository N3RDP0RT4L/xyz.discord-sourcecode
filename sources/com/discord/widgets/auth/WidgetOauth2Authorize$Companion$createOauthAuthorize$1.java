package com.discord.widgets.auth;

import android.net.Uri;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetOauth2Authorize.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\u0010\u0005\u001a\u00020\u0001*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/net/Uri;", "", "parameterName", "invoke", "(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;", "getQueryParameterOrThrow"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetOauth2Authorize$Companion$createOauthAuthorize$1 extends o implements Function2<Uri, String, String> {
    public static final WidgetOauth2Authorize$Companion$createOauthAuthorize$1 INSTANCE = new WidgetOauth2Authorize$Companion$createOauthAuthorize$1();

    public WidgetOauth2Authorize$Companion$createOauthAuthorize$1() {
        super(2);
    }

    public final String invoke(Uri uri, String str) {
        m.checkNotNullParameter(uri, "$this$getQueryParameterOrThrow");
        m.checkNotNullParameter(str, "parameterName");
        String queryParameter = uri.getQueryParameter(str);
        if (queryParameter != null) {
            return queryParameter;
        }
        throw new IllegalArgumentException(str);
    }
}
