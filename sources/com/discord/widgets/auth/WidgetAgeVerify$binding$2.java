package com.discord.widgets.auth;

import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import b.a.i.e0;
import b.a.i.f0;
import b.a.i.g0;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetAgeVerifyBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAgeVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetAgeVerifyBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetAgeVerifyBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetAgeVerify$binding$2 extends k implements Function1<View, WidgetAgeVerifyBinding> {
    public static final WidgetAgeVerify$binding$2 INSTANCE = new WidgetAgeVerify$binding$2();

    public WidgetAgeVerify$binding$2() {
        super(1, WidgetAgeVerifyBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetAgeVerifyBinding;", 0);
    }

    public final WidgetAgeVerifyBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.age_verify_view_flipper;
        AppViewFlipper appViewFlipper = (AppViewFlipper) view.findViewById(R.id.age_verify_view_flipper);
        if (appViewFlipper != null) {
            i = R.id.confirm;
            View findViewById = view.findViewById(R.id.confirm);
            if (findViewById != null) {
                int i2 = R.id.age_verify_confirm_back_button;
                MaterialButton materialButton = (MaterialButton) findViewById.findViewById(R.id.age_verify_confirm_back_button);
                if (materialButton != null) {
                    i2 = R.id.age_verify_confirm_button;
                    LoadingButton loadingButton = (LoadingButton) findViewById.findViewById(R.id.age_verify_confirm_button);
                    if (loadingButton != null) {
                        i2 = R.id.age_verify_confirm_description;
                        LinkifiedTextView linkifiedTextView = (LinkifiedTextView) findViewById.findViewById(R.id.age_verify_confirm_description);
                        if (linkifiedTextView != null) {
                            i2 = R.id.age_verify_confirm_title;
                            TextView textView = (TextView) findViewById.findViewById(R.id.age_verify_confirm_title);
                            if (textView != null) {
                                f0 f0Var = new f0((ConstraintLayout) findViewById, materialButton, loadingButton, linkifiedTextView, textView);
                                View findViewById2 = view.findViewById(R.id.underage);
                                if (findViewById2 != null) {
                                    int i3 = R.id.age_verify_return_to_login_button;
                                    MaterialButton materialButton2 = (MaterialButton) findViewById2.findViewById(R.id.age_verify_return_to_login_button);
                                    if (materialButton2 != null) {
                                        i3 = R.id.age_verify_underage_description;
                                        LinkifiedTextView linkifiedTextView2 = (LinkifiedTextView) findViewById2.findViewById(R.id.age_verify_underage_description);
                                        if (linkifiedTextView2 != null) {
                                            i3 = R.id.age_verify_underage_title;
                                            TextView textView2 = (TextView) findViewById2.findViewById(R.id.age_verify_underage_title);
                                            if (textView2 != null) {
                                                i3 = R.id.underage_warning;
                                                TextView textView3 = (TextView) findViewById2.findViewById(R.id.underage_warning);
                                                if (textView3 != null) {
                                                    g0 g0Var = new g0((ConstraintLayout) findViewById2, materialButton2, linkifiedTextView2, textView2, textView3);
                                                    View findViewById3 = view.findViewById(R.id.verify);
                                                    if (findViewById3 != null) {
                                                        int i4 = R.id.age_verify_description;
                                                        LinkifiedTextView linkifiedTextView3 = (LinkifiedTextView) findViewById3.findViewById(R.id.age_verify_description);
                                                        if (linkifiedTextView3 != null) {
                                                            i4 = R.id.age_verify_input_wrapper;
                                                            TextInputLayout textInputLayout = (TextInputLayout) findViewById3.findViewById(R.id.age_verify_input_wrapper);
                                                            if (textInputLayout != null) {
                                                                i4 = R.id.age_verify_next_button;
                                                                LoadingButton loadingButton2 = (LoadingButton) findViewById3.findViewById(R.id.age_verify_next_button);
                                                                if (loadingButton2 != null) {
                                                                    i4 = R.id.age_verify_title;
                                                                    TextView textView4 = (TextView) findViewById3.findViewById(R.id.age_verify_title);
                                                                    if (textView4 != null) {
                                                                        return new WidgetAgeVerifyBinding((CoordinatorLayout) view, appViewFlipper, f0Var, g0Var, new e0((ConstraintLayout) findViewById3, linkifiedTextView3, textInputLayout, loadingButton2, textView4));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        throw new NullPointerException("Missing required view with ID: ".concat(findViewById3.getResources().getResourceName(i4)));
                                                    }
                                                    i = R.id.verify;
                                                }
                                            }
                                        }
                                    }
                                    throw new NullPointerException("Missing required view with ID: ".concat(findViewById2.getResources().getResourceName(i3)));
                                }
                                i = R.id.underage;
                            }
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(i2)));
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
