package com.discord.widgets.auth;

import android.view.View;
import androidx.fragment.app.FragmentManager;
import b.a.a.k;
import b.a.k.b;
import com.discord.utilities.birthday.BirthdayHelper;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAgeVerify.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAgeVerify$configureBirthdayInput$1 extends o implements Function1<View, Unit> {
    public final /* synthetic */ Long $timeOfBirth;
    public final /* synthetic */ WidgetAgeVerify this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAgeVerify$configureBirthdayInput$1(WidgetAgeVerify widgetAgeVerify, Long l) {
        super(1);
        this.this$0 = widgetAgeVerify;
        this.$timeOfBirth = l;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        CharSequence e;
        m.checkNotNullParameter(view, "it");
        k.a aVar = k.k;
        FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        e = b.e(this.this$0, R.string.age_gate_date_of_birth, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        Long l = this.$timeOfBirth;
        aVar.a(parentFragmentManager, e, l != null ? l.longValue() : BirthdayHelper.INSTANCE.defaultInputAge(), BirthdayHelper.INSTANCE.getMaxDateOfBirth()).l = new WidgetAgeVerify$configureBirthdayInput$1$$special$$inlined$apply$lambda$1(this);
    }
}
