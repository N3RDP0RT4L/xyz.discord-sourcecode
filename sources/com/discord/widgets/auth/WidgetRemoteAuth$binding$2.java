package com.discord.widgets.auth;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import b.a.i.q5;
import b.a.i.r5;
import b.a.i.s5;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetRemoteAuthBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetRemoteAuth.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetRemoteAuthBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetRemoteAuthBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetRemoteAuth$binding$2 extends k implements Function1<View, WidgetRemoteAuthBinding> {
    public static final WidgetRemoteAuth$binding$2 INSTANCE = new WidgetRemoteAuth$binding$2();

    public WidgetRemoteAuth$binding$2() {
        super(1, WidgetRemoteAuthBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetRemoteAuthBinding;", 0);
    }

    public final WidgetRemoteAuthBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.auth_success;
        View findViewById = view.findViewById(R.id.auth_success);
        if (findViewById != null) {
            MaterialButton materialButton = (MaterialButton) findViewById.findViewById(R.id.remote_auth_finish_button);
            if (materialButton != null) {
                s5 s5Var = new s5((LinearLayout) findViewById, materialButton);
                i = R.id.not_found;
                View findViewById2 = view.findViewById(R.id.not_found);
                if (findViewById2 != null) {
                    MaterialButton materialButton2 = (MaterialButton) findViewById2.findViewById(R.id.remote_auth_not_found_cancel_button);
                    if (materialButton2 != null) {
                        q5 q5Var = new q5((LinearLayout) findViewById2, materialButton2);
                        i = R.id.pending_login;
                        View findViewById3 = view.findViewById(R.id.pending_login);
                        if (findViewById3 != null) {
                            int i2 = R.id.remote_auth_cancel_button;
                            MaterialButton materialButton3 = (MaterialButton) findViewById3.findViewById(R.id.remote_auth_cancel_button);
                            if (materialButton3 != null) {
                                i2 = R.id.remote_auth_login_button;
                                MaterialButton materialButton4 = (MaterialButton) findViewById3.findViewById(R.id.remote_auth_login_button);
                                if (materialButton4 != null) {
                                    i2 = R.id.remote_auth_temporary_switch;
                                    SwitchMaterial switchMaterial = (SwitchMaterial) findViewById3.findViewById(R.id.remote_auth_temporary_switch);
                                    if (switchMaterial != null) {
                                        r5 r5Var = new r5((LinearLayout) findViewById3, materialButton3, materialButton4, switchMaterial);
                                        i = R.id.remote_auth_view_flipper;
                                        AppViewFlipper appViewFlipper = (AppViewFlipper) view.findViewById(R.id.remote_auth_view_flipper);
                                        if (appViewFlipper != null) {
                                            return new WidgetRemoteAuthBinding((RelativeLayout) view, s5Var, q5Var, r5Var, appViewFlipper);
                                        }
                                    }
                                }
                            }
                            throw new NullPointerException("Missing required view with ID: ".concat(findViewById3.getResources().getResourceName(i2)));
                        }
                    } else {
                        throw new NullPointerException("Missing required view with ID: ".concat(findViewById2.getResources().getResourceName(R.id.remote_auth_not_found_cancel_button)));
                    }
                }
            } else {
                throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(R.id.remote_auth_finish_button)));
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
