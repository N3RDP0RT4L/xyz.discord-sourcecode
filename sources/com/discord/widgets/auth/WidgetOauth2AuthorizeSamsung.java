package com.discord.widgets.auth;

import andhook.lib.HookHelper;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import b.a.r.c;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelUserSettings;
import com.discord.restapi.RestAPIParams;
import com.discord.samsung.SamsungConnectActivity;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.auth.WidgetOauth2Authorize;
import com.discord.widgets.auth.WidgetOauth2AuthorizeSamsung;
import d0.g0.y;
import d0.z.d.m;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetOauth2AuthorizeSamsung.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 )2\u00020\u0001:\u0001)B\u0007¢\u0006\u0004\b'\u0010(J\u001f\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J!\u0010\t\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00022\b\u0010\b\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\t\u0010\u0007J\u0017\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u0011H\u0014¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u0016H\u0014¢\u0006\u0004\b\u0018\u0010\u0019J#\u0010\u001b\u001a\u00020\u00052\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0002H\u0082@ø\u0001\u0000¢\u0006\u0004\b\u001b\u0010\u001cR\"\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d8\u0014@\u0014X\u0094\u0004¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u001c\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010 R\u0018\u0010\u0004\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0004\u0010$R$\u0010&\u001a\u0010\u0012\f\u0012\n %*\u0004\u0018\u00010\u001e0\u001e0\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010 \u0082\u0002\u0004\n\u0002\b\u0019¨\u0006*"}, d2 = {"Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung;", "Lcom/discord/widgets/auth/WidgetOauth2Authorize;", "", "saUrl", "samsungAuthCode", "", "startSamsungAccountLink", "(Ljava/lang/String;Ljava/lang/String;)V", "captchaKey", "authorizeForSamsung", "Lcom/discord/samsung/SamsungConnectActivity$Result$Failure;", "result", "handleConnectActivityFailure", "(Lcom/discord/samsung/SamsungConnectActivity$Result$Failure;)V", "Lcom/discord/samsung/SamsungConnectActivity$Result$Success;", "handleConnectActivitySuccess", "(Lcom/discord/samsung/SamsungConnectActivity$Result$Success;)V", "Landroid/net/Uri;", "requestUrl", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "createOauthAuthorize", "(Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;", "data", "configureUI", "(Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;)V", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "samsungCallbackHandshake", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "captchaLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "getCaptchaLauncher", "()Landroidx/activity/result/ActivityResultLauncher;", "samsungConnectLauncher", "Ljava/lang/String;", "kotlin.jvm.PlatformType", "samsungDisclaimerLauncher", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetOauth2AuthorizeSamsung extends WidgetOauth2Authorize {
    public static final Companion Companion = new Companion(null);
    private static final int REQ_CODE_SAMSUNG = 5459;
    private final ActivityResultLauncher<Intent> captchaLauncher = WidgetAuthCaptcha.Companion.registerForResult(this, new WidgetOauth2AuthorizeSamsung$captchaLauncher$1(this));
    private String samsungAuthCode;
    private final ActivityResultLauncher<Intent> samsungConnectLauncher;
    private final ActivityResultLauncher<Intent> samsungDisclaimerLauncher;

    /* compiled from: WidgetOauth2AuthorizeSamsung.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J#\u0010\t\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\fJ!\u0010\u0011\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u000f*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/auth/WidgetOauth2AuthorizeSamsung$Companion;", "", "", "message", "", "logI", "(Ljava/lang/String;)V", "", "throwable", "logW", "(Ljava/lang/String;Ljava/lang/Throwable;)V", "createSAStateId", "()Ljava/lang/String;", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "accountUri", "Lrx/Observable;", "Ljava/lang/Void;", "getForSamsung", "(Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;Ljava/lang/String;)Lrx/Observable;", "", "REQ_CODE_SAMSUNG", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final String createSAStateId() {
            StringBuilder R = a.R("SA");
            R.append(UUID.randomUUID());
            return y.take(R.toString(), 115);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void logI(String str) {
            AppLog appLog = AppLog.g;
            appLog.c(str, "Samsung", null, new WidgetOauth2AuthorizeSamsung$Companion$logI$1(appLog));
        }

        private final void logW(String str, Throwable th) {
            AppLog appLog = AppLog.g;
            appLog.c(str, "Samsung", th, new WidgetOauth2AuthorizeSamsung$Companion$logW$1(appLog));
        }

        public static /* synthetic */ void logW$default(Companion companion, String str, Throwable th, int i, Object obj) {
            if ((i & 2) != 0) {
                th = null;
            }
            companion.logW(str, th);
        }

        public final Observable<Void> getForSamsung(WidgetOauth2Authorize.OAuth2Authorize oAuth2Authorize, String str) {
            m.checkNotNullParameter(oAuth2Authorize, "$this$getForSamsung");
            m.checkNotNullParameter(str, "accountUri");
            return RestAPI.Companion.getApi().getOauth2SamsungAuthorize(String.valueOf(oAuth2Authorize.getClientId()), oAuth2Authorize.getState(), oAuth2Authorize.getResponseType(), str, oAuth2Authorize.getPrompt(), oAuth2Authorize.getScope());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetOauth2AuthorizeSamsung() {
        WidgetOauth2AuthorizeSamsung$samsungConnectLauncher$1 widgetOauth2AuthorizeSamsung$samsungConnectLauncher$1 = new WidgetOauth2AuthorizeSamsung$samsungConnectLauncher$1(this);
        m.checkNotNullParameter(this, "fragment");
        m.checkNotNullParameter(widgetOauth2AuthorizeSamsung$samsungConnectLauncher$1, "callback");
        ActivityResultLauncher<Intent> registerForActivityResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new c(widgetOauth2AuthorizeSamsung$samsungConnectLauncher$1));
        m.checkNotNullExpressionValue(registerForActivityResult, "fragment.registerForActi…  }\n          }\n        }");
        this.samsungConnectLauncher = registerForActivityResult;
        ActivityResultLauncher<Intent> registerForActivityResult2 = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.auth.WidgetOauth2AuthorizeSamsung$samsungDisclaimerLauncher$1
            public final void onActivityResult(ActivityResult activityResult) {
                Bundle extras;
                Bundle extras2;
                ActivityResultLauncher activityResultLauncher;
                m.checkNotNullExpressionValue(activityResult, "activityResult");
                if (activityResult.getResultCode() == -1) {
                    Context requireContext = WidgetOauth2AuthorizeSamsung.this.requireContext();
                    activityResultLauncher = WidgetOauth2AuthorizeSamsung.this.samsungConnectLauncher;
                    SamsungConnectActivity.b(requireContext, activityResultLauncher, 5459);
                    return;
                }
                Intent data = activityResult.getData();
                Object obj = (data == null || (extras2 = data.getExtras()) == null) ? null : extras2.get("error_message");
                Intent data2 = activityResult.getData();
                Object obj2 = (data2 == null || (extras = data2.getExtras()) == null) ? null : extras.get("error_code");
                WidgetOauth2AuthorizeSamsung.Companion companion = WidgetOauth2AuthorizeSamsung.Companion;
                WidgetOauth2AuthorizeSamsung.Companion.logW$default(companion, "Connection requires disclaimer acceptance. [" + obj2 + "] " + obj, null, 2, null);
                b.a.d.m.i(WidgetOauth2AuthorizeSamsung.this, R.string.failed, 0, 4);
                AppActivity appActivity = WidgetOauth2AuthorizeSamsung.this.getAppActivity();
                if (appActivity != null) {
                    appActivity.finish();
                }
            }
        });
        m.checkNotNullExpressionValue(registerForActivityResult2, "registerForActivityResul…inish()\n        }\n      }");
        this.samsungDisclaimerLauncher = registerForActivityResult2;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void authorizeForSamsung(String str, String str2) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(getOauth2ViewModel().getOauthAuthorize().post(str2), false, 1, null), this, null, 2, null), WidgetOauth2AuthorizeSamsung.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetOauth2AuthorizeSamsung$authorizeForSamsung$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetOauth2AuthorizeSamsung$authorizeForSamsung$2(this, str));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleConnectActivityFailure(SamsungConnectActivity.Result.Failure failure) {
        if (failure.a && failure.f2774b < 2) {
            Companion.logW$default(Companion, "Retrying SA connection.\nBecause sometimes it just doesn't bind the first time.", null, 2, null);
            SamsungConnectActivity.b(requireContext(), this.samsungConnectLauncher, failure.f2774b);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleConnectActivitySuccess(SamsungConnectActivity.Result.Success success) {
        String str = success.a;
        this.samsungAuthCode = str;
        startSamsungAccountLink(success.f2775b, str);
    }

    private final void startSamsungAccountLink(String str, String str2) {
        String str3;
        m.checkNotNullParameter(str, "authServerUrl");
        String take = y.take(str, 2);
        int hashCode = take.hashCode();
        if (hashCode != 3179) {
            if (hashCode == 3248) {
                take.equals("eu");
            } else if (hashCode == 3742 && take.equals("us")) {
                str3 = "https://us.account.samsung.com";
            }
            str3 = "https://account.samsung.com";
        } else {
            if (take.equals("cn")) {
                str3 = "https://account.samsung.cn";
            }
            str3 = "https://account.samsung.com";
        }
        Companion companion = Companion;
        companion.logI("GET /authorize " + str3);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(companion.getForSamsung(getOauth2ViewModel().getOauthAuthorize(), str3), false, 1, null), this, null, 2, null), WidgetOauth2AuthorizeSamsung.class, (r18 & 2) != 0 ? null : requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetOauth2AuthorizeSamsung$startSamsungAccountLink$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetOauth2AuthorizeSamsung$startSamsungAccountLink$2(this, str2));
    }

    @Override // com.discord.widgets.auth.WidgetOauth2Authorize
    public void configureUI(RestAPIParams.OAuth2Authorize.ResponseGet responseGet) {
        m.checkNotNullParameter(responseGet, "data");
        super.configureUI(responseGet);
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetOauth2AuthorizeSamsung$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ActivityResultLauncher activityResultLauncher;
                m.checkNotNullExpressionValue(view, "it");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                m.checkNotNullParameter(context, "context");
                Account[] accountsByType = AccountManager.get(context).getAccountsByType("com.osp.app.signin");
                m.checkNotNullExpressionValue(accountsByType, "accountManager.getAccoun…yType(SA_ACCOUNT_SERVICE)");
                if (!(!(accountsByType.length == 0))) {
                    WidgetOauth2AuthorizeSamsung.Companion.logI("Not Logged into Samsung Account");
                }
                try {
                    activityResultLauncher = WidgetOauth2AuthorizeSamsung.this.samsungDisclaimerLauncher;
                    Intent intent = new Intent("com.msc.action.samsungaccount.REQUEST_NEW_THIRD_PARTY_INTEGRATION_WITH_SAMSUNG_ACCOUNT");
                    intent.putExtra("client_id", "97t47j218f");
                    intent.putExtra("progress_theme", ModelUserSettings.THEME_DARK);
                    activityResultLauncher.launch(intent);
                } catch (ActivityNotFoundException unused) {
                    AppActivity appActivity = WidgetOauth2AuthorizeSamsung.this.getAppActivity();
                    if (appActivity != null) {
                        appActivity.finish();
                    }
                }
            }
        });
    }

    @Override // com.discord.widgets.auth.WidgetOauth2Authorize
    public WidgetOauth2Authorize.OAuth2Authorize createOauthAuthorize(Uri uri) {
        WidgetOauth2Authorize.OAuth2Authorize copy;
        m.checkNotNullParameter(uri, "requestUrl");
        WidgetOauth2Authorize.OAuth2Authorize createOauthAuthorize = super.createOauthAuthorize(uri);
        String state = createOauthAuthorize.getState();
        if (state == null) {
            state = Companion.createSAStateId();
        }
        copy = createOauthAuthorize.copy((r24 & 1) != 0 ? createOauthAuthorize.clientId : 0L, (r24 & 2) != 0 ? createOauthAuthorize.state : state, (r24 & 4) != 0 ? createOauthAuthorize.responseType : null, (r24 & 8) != 0 ? createOauthAuthorize.redirectUrl : "https://discord.com/api/v6/oauth2/samsung/authorize/callback", (r24 & 16) != 0 ? createOauthAuthorize.prompt : null, (r24 & 32) != 0 ? createOauthAuthorize.scope : null, (r24 & 64) != 0 ? createOauthAuthorize.permissions : null, (r24 & 128) != 0 ? createOauthAuthorize.codeChallenge : null, (r24 & 256) != 0 ? createOauthAuthorize.codeChallengeMethod : null, (r24 & 512) != 0 ? createOauthAuthorize.internalReferrer : null);
        return copy;
    }

    @Override // com.discord.widgets.auth.WidgetOauth2Authorize
    public ActivityResultLauncher<Intent> getCaptchaLauncher() {
        return this.captchaLauncher;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARN: Removed duplicated region for block: B:18:0x0058  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0106 A[RETURN] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object samsungCallbackHandshake(java.lang.String r16, java.lang.String r17, kotlin.coroutines.Continuation<? super kotlin.Unit> r18) {
        /*
            Method dump skipped, instructions count: 276
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.auth.WidgetOauth2AuthorizeSamsung.samsungCallbackHandshake(java.lang.String, java.lang.String, kotlin.coroutines.Continuation):java.lang.Object");
    }
}
