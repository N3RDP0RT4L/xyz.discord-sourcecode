package com.discord.widgets.auth;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetAuthRegisterAccountInformationBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.LoadingButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAuthRegisterAccountInformation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetAuthRegisterAccountInformationBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthRegisterAccountInformationBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetAuthRegisterAccountInformation$binding$2 extends k implements Function1<View, WidgetAuthRegisterAccountInformationBinding> {
    public static final WidgetAuthRegisterAccountInformation$binding$2 INSTANCE = new WidgetAuthRegisterAccountInformation$binding$2();

    public WidgetAuthRegisterAccountInformation$binding$2() {
        super(1, WidgetAuthRegisterAccountInformationBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthRegisterAccountInformationBinding;", 0);
    }

    public final WidgetAuthRegisterAccountInformationBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.auth_policy_links;
        LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.auth_policy_links);
        if (linkifiedTextView != null) {
            i = R.id.auth_register_account_information_button;
            LoadingButton loadingButton = (LoadingButton) view.findViewById(R.id.auth_register_account_information_button);
            if (loadingButton != null) {
                i = R.id.auth_register_account_information_password_wrap;
                TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.auth_register_account_information_password_wrap);
                if (textInputLayout != null) {
                    i = R.id.auth_register_account_information_username_wrap;
                    TextInputLayout textInputLayout2 = (TextInputLayout) view.findViewById(R.id.auth_register_account_information_username_wrap);
                    if (textInputLayout2 != null) {
                        i = R.id.auth_tos_opt_in;
                        MaterialCheckBox materialCheckBox = (MaterialCheckBox) view.findViewById(R.id.auth_tos_opt_in);
                        if (materialCheckBox != null) {
                            return new WidgetAuthRegisterAccountInformationBinding((CoordinatorLayout) view, linkifiedTextView, loadingButton, textInputLayout, textInputLayout2, materialCheckBox);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
