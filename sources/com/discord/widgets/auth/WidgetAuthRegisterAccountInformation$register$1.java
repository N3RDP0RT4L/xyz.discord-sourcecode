package com.discord.widgets.auth;

import com.discord.api.auth.RegisterResponse;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.auth.RegistrationFlowRepo;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetAuthRegisterAccountInformation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/auth/RegisterResponse;", "it", "", "invoke", "(Lcom/discord/api/auth/RegisterResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthRegisterAccountInformation$register$1 extends o implements Function1<RegisterResponse, Unit> {
    public static final WidgetAuthRegisterAccountInformation$register$1 INSTANCE = new WidgetAuthRegisterAccountInformation$register$1();

    public WidgetAuthRegisterAccountInformation$register$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RegisterResponse registerResponse) {
        invoke2(registerResponse);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RegisterResponse registerResponse) {
        m.checkNotNullParameter(registerResponse, "it");
        AnalyticsTracker.INSTANCE.registered(true);
        RegistrationFlowRepo.trackTransition$default(RegistrationFlowRepo.Companion.getINSTANCE(), "Account Information", "success", null, 4, null);
    }
}
