package com.discord.widgets.auth;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetAuthLoginBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.views.phone.PhoneOrEmailInputView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAuthLogin.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetAuthLoginBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthLoginBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetAuthLogin$binding$2 extends k implements Function1<View, WidgetAuthLoginBinding> {
    public static final WidgetAuthLogin$binding$2 INSTANCE = new WidgetAuthLogin$binding$2();

    public WidgetAuthLogin$binding$2() {
        super(1, WidgetAuthLoginBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetAuthLoginBinding;", 0);
    }

    public final WidgetAuthLoginBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.auth_login;
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.auth_login);
        if (materialButton != null) {
            i = R.id.auth_login_container;
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.auth_login_container);
            if (linearLayout != null) {
                i = R.id.auth_login_forgot_password;
                TextView textView = (TextView) view.findViewById(R.id.auth_login_forgot_password);
                if (textView != null) {
                    i = R.id.auth_login_login_input;
                    PhoneOrEmailInputView phoneOrEmailInputView = (PhoneOrEmailInputView) view.findViewById(R.id.auth_login_login_input);
                    if (phoneOrEmailInputView != null) {
                        i = R.id.auth_login_password_manager_link;
                        TextView textView2 = (TextView) view.findViewById(R.id.auth_login_password_manager_link);
                        if (textView2 != null) {
                            i = R.id.auth_login_password_wrap;
                            TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.auth_login_password_wrap);
                            if (textInputLayout != null) {
                                i = R.id.dimmer_view;
                                DimmerView dimmerView = (DimmerView) view.findViewById(R.id.dimmer_view);
                                if (dimmerView != null) {
                                    return new WidgetAuthLoginBinding((CoordinatorLayout) view, materialButton, linearLayout, textView, phoneOrEmailInputView, textView2, textInputLayout, dimmerView);
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
