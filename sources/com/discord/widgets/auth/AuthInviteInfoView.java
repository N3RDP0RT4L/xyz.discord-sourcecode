package com.discord.widgets.auth;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.Guild;
import com.discord.api.user.User;
import com.discord.databinding.ViewAuthInviteInfoBinding;
import com.discord.models.domain.ModelGuildTemplate;
import com.discord.models.domain.ModelInvite;
import com.discord.models.user.CoreUser;
import com.discord.nullserializable.NullSerializable;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: AuthInviteInfoView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u0012\u001a\u00020\u0011\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0013\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018J\u0013\u0010\u0004\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0015\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\f\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/auth/AuthInviteInfoView;", "Landroid/widget/RelativeLayout;", "Lcom/discord/models/domain/ModelInvite;", "", "getIntroText", "(Lcom/discord/models/domain/ModelInvite;)Ljava/lang/CharSequence;", "invite", "", "configureInvite", "(Lcom/discord/models/domain/ModelInvite;)V", "Lcom/discord/models/domain/ModelGuildTemplate;", "guildTemplate", "configureGuildTemplate", "(Lcom/discord/models/domain/ModelGuildTemplate;)V", "Lcom/discord/databinding/ViewAuthInviteInfoBinding;", "binding", "Lcom/discord/databinding/ViewAuthInviteInfoBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyleAttr", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AuthInviteInfoView extends RelativeLayout {
    private final ViewAuthInviteInfoBinding binding;

    public AuthInviteInfoView(Context context) {
        this(context, null, 0, 6, null);
    }

    public AuthInviteInfoView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ AuthInviteInfoView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX WARN: Code restructure failed: missing block: B:28:0x005e, code lost:
        if ((com.discord.api.channel.ChannelUtils.c(r0).length() == 0) != true) goto L29;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final java.lang.CharSequence getIntroText(com.discord.models.domain.ModelInvite r10) {
        /*
            r9 = this;
            com.discord.api.user.User r0 = r10.getInviter()
            r1 = 0
            if (r0 == 0) goto Lc
            java.lang.String r0 = r0.r()
            goto Ld
        Lc:
            r0 = r1
        Ld:
            r2 = 1
            r3 = 0
            if (r0 == 0) goto L1a
            boolean r4 = d0.g0.t.isBlank(r0)
            r4 = r4 ^ r2
            if (r4 == 0) goto L1a
            r4 = 1
            goto L1b
        L1a:
            r4 = 0
        L1b:
            com.discord.api.channel.Channel r5 = r10.getChannel()
            r6 = 4
            java.lang.String r7 = "context"
            if (r5 != 0) goto L3c
            com.discord.api.guild.Guild r5 = r10.guild
            if (r5 != 0) goto L3c
            if (r4 == 0) goto L3c
            android.content.Context r10 = r9.getContext()
            d0.z.d.m.checkNotNullExpressionValue(r10, r7)
            r0 = 2131891155(0x7f1213d3, float:1.9417022E38)
            java.lang.Object[] r2 = new java.lang.Object[r3]
            java.lang.CharSequence r10 = b.a.k.b.h(r10, r0, r2, r1, r6)
            goto Lb9
        L3c:
            com.discord.api.channel.Channel r5 = r10.getChannel()
            r8 = 2131886835(0x7f1202f3, float:1.940826E38)
            if (r5 == 0) goto L8f
            boolean r5 = com.discord.api.channel.ChannelUtils.w(r5)
            if (r5 != r2) goto L8f
            com.discord.api.channel.Channel r0 = r10.getChannel()
            if (r0 == 0) goto L60
            java.lang.String r0 = com.discord.api.channel.ChannelUtils.c(r0)
            int r0 = r0.length()
            if (r0 != 0) goto L5d
            r0 = 1
            goto L5e
        L5d:
            r0 = 0
        L5e:
            if (r0 == r2) goto L62
        L60:
            if (r4 != 0) goto L73
        L62:
            android.content.Context r10 = r9.getContext()
            d0.z.d.m.checkNotNullExpressionValue(r10, r7)
            r0 = 2131891158(0x7f1213d6, float:1.9417028E38)
            java.lang.Object[] r2 = new java.lang.Object[r3]
            java.lang.CharSequence r10 = b.a.k.b.h(r10, r0, r2, r1, r6)
            goto Lb9
        L73:
            android.content.Context r0 = r9.getContext()
            d0.z.d.m.checkNotNullExpressionValue(r0, r7)
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.discord.api.user.User r10 = r10.getInviter()
            if (r10 == 0) goto L87
            java.lang.String r10 = r10.r()
            goto L88
        L87:
            r10 = r1
        L88:
            r2[r3] = r10
            java.lang.CharSequence r10 = b.a.k.b.h(r0, r8, r2, r1, r6)
            goto Lb9
        L8f:
            int r10 = r10.getApproximateMemberCount()
            r5 = 200(0xc8, float:2.8E-43)
            if (r10 >= r5) goto La9
            if (r4 == 0) goto La9
            android.content.Context r10 = r9.getContext()
            d0.z.d.m.checkNotNullExpressionValue(r10, r7)
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r3] = r0
            java.lang.CharSequence r10 = b.a.k.b.h(r10, r8, r2, r1, r6)
            goto Lb9
        La9:
            android.content.Context r10 = r9.getContext()
            d0.z.d.m.checkNotNullExpressionValue(r10, r7)
            r0 = 2131891156(0x7f1213d4, float:1.9417024E38)
            java.lang.Object[] r2 = new java.lang.Object[r3]
            java.lang.CharSequence r10 = b.a.k.b.h(r10, r0, r2, r1, r6)
        Lb9:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.auth.AuthInviteInfoView.getIntroText(com.discord.models.domain.ModelInvite):java.lang.CharSequence");
    }

    public final void configureGuildTemplate(ModelGuildTemplate modelGuildTemplate) {
        m.checkNotNullParameter(modelGuildTemplate, "guildTemplate");
        SimpleDraweeView simpleDraweeView = this.binding.f2157b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.authInviteInfoGuildAvatar");
        simpleDraweeView.setVisibility(8);
        ImageView imageView = this.binding.e;
        m.checkNotNullExpressionValue(imageView, "binding.authInviteInfoTemplateIcon");
        imageView.setVisibility(0);
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.authInviteInfoInvitedText");
        b.m(textView, R.string.guild_template_modal_title, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        TextView textView2 = this.binding.c;
        m.checkNotNullExpressionValue(textView2, "binding.authInviteInfoGuildName");
        textView2.setText(modelGuildTemplate.getName());
    }

    public final void configureInvite(ModelInvite modelInvite) {
        CharSequence g;
        String str;
        CharSequence g2;
        String f;
        CharSequence g3;
        NullSerializable<String> a;
        String r;
        m.checkNotNullParameter(modelInvite, "invite");
        SimpleDraweeView simpleDraweeView = this.binding.f2157b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.authInviteInfoGuildAvatar");
        boolean z2 = false;
        simpleDraweeView.setVisibility(0);
        ImageView imageView = this.binding.e;
        m.checkNotNullExpressionValue(imageView, "binding.authInviteInfoTemplateIcon");
        imageView.setVisibility(8);
        Channel channel = modelInvite.getChannel();
        String str2 = null;
        Integer valueOf = channel != null ? Integer.valueOf(channel.A()) : null;
        if ((valueOf != null && valueOf.intValue() == 5) || ((valueOf != null && valueOf.intValue() == 0) || ((valueOf != null && valueOf.intValue() == 2) || (valueOf != null && valueOf.intValue() == 13)))) {
            Guild guild = modelInvite.guild;
            if (guild != null) {
                SimpleDraweeView simpleDraweeView2 = this.binding.f2157b;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.authInviteInfoGuildAvatar");
                m.checkNotNullExpressionValue(guild, "inviteGuild");
                IconUtils.setIcon$default(simpleDraweeView2, IconUtils.getForGuild$default(new com.discord.models.guild.Guild(guild), IconUtils.DEFAULT_ICON_BLURPLE, true, null, 8, null), (int) R.dimen.avatar_size_large, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
                TextView textView = this.binding.d;
                m.checkNotNullExpressionValue(textView, "binding.authInviteInfoInvitedText");
                g = b.g(getIntroText(modelInvite), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
                textView.setText(g);
                TextView textView2 = this.binding.c;
                m.checkNotNullExpressionValue(textView2, "binding.authInviteInfoGuildName");
                textView2.setText(guild.x());
                return;
            }
            return;
        }
        String str3 = "";
        if (valueOf != null && valueOf.intValue() == 3) {
            User inviter = modelInvite.getInviter();
            if (!(inviter == null || (r = inviter.r()) == null)) {
                str3 = r;
            }
            SimpleDraweeView simpleDraweeView3 = this.binding.f2157b;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.authInviteInfoGuildAvatar");
            User inviter2 = modelInvite.getInviter();
            IconUtils.setIcon$default(simpleDraweeView3, (inviter2 == null || (a = inviter2.a()) == null) ? null : a.a(), (int) R.dimen.avatar_size_large, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
            TextView textView3 = this.binding.d;
            m.checkNotNullExpressionValue(textView3, "binding.authInviteInfoInvitedText");
            g3 = b.g(getIntroText(modelInvite), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
            textView3.setText(g3);
            TextView textView4 = this.binding.c;
            m.checkNotNullExpressionValue(textView4, "binding.authInviteInfoGuildName");
            Channel channel2 = modelInvite.getChannel();
            if (channel2 != null) {
                if (ChannelUtils.c(channel2).length() > 0) {
                    z2 = true;
                }
                if (z2) {
                    Channel channel3 = modelInvite.getChannel();
                    if (channel3 != null) {
                        str2 = ChannelUtils.c(channel3);
                    }
                    str3 = str2;
                }
            }
            textView4.setText(str3);
        } else if (valueOf == null) {
            User inviter3 = modelInvite.getInviter();
            if (inviter3 == null || (str = inviter3.r()) == null) {
                str = str3;
            }
            StringBuilder V = a.V(str, "#");
            User inviter4 = modelInvite.getInviter();
            if (!(inviter4 == null || (f = inviter4.f()) == null)) {
                str3 = f;
            }
            V.append(str3);
            String sb = V.toString();
            SimpleDraweeView simpleDraweeView4 = this.binding.f2157b;
            m.checkNotNullExpressionValue(simpleDraweeView4, "binding.authInviteInfoGuildAvatar");
            User inviter5 = modelInvite.getInviter();
            IconUtils.setIcon$default(simpleDraweeView4, inviter5 != null ? new CoreUser(inviter5) : null, R.dimen.avatar_size_large, null, null, null, 56, null);
            TextView textView5 = this.binding.d;
            m.checkNotNullExpressionValue(textView5, "binding.authInviteInfoInvitedText");
            g2 = b.g(getIntroText(modelInvite), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
            textView5.setText(g2);
            TextView textView6 = this.binding.c;
            m.checkNotNullExpressionValue(textView6, "binding.authInviteInfoGuildName");
            textView6.setText(sb);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AuthInviteInfoView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_auth_invite_info, (ViewGroup) this, false);
        addView(inflate);
        int i2 = R.id.auth_invite_info_content;
        LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.auth_invite_info_content);
        if (linearLayout != null) {
            i2 = R.id.auth_invite_info_guild_avatar;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.auth_invite_info_guild_avatar);
            if (simpleDraweeView != null) {
                i2 = R.id.auth_invite_info_guild_name;
                TextView textView = (TextView) inflate.findViewById(R.id.auth_invite_info_guild_name);
                if (textView != null) {
                    i2 = R.id.auth_invite_info_invited_text;
                    TextView textView2 = (TextView) inflate.findViewById(R.id.auth_invite_info_invited_text);
                    if (textView2 != null) {
                        i2 = R.id.auth_invite_info_template_icon;
                        ImageView imageView = (ImageView) inflate.findViewById(R.id.auth_invite_info_template_icon);
                        if (imageView != null) {
                            ViewAuthInviteInfoBinding viewAuthInviteInfoBinding = new ViewAuthInviteInfoBinding((RelativeLayout) inflate, linearLayout, simpleDraweeView, textView, textView2, imageView);
                            m.checkNotNullExpressionValue(viewAuthInviteInfoBinding, "ViewAuthInviteInfoBindin…rom(context), this, true)");
                            this.binding = viewAuthInviteInfoBinding;
                            return;
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }
}
