package com.discord.widgets.auth;

import android.content.Context;
import android.content.Intent;
import androidx.activity.result.ActivityResultLauncher;
import com.discord.utilities.error.Error;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.widgets.auth.WidgetAuthCaptcha;
import com.discord.widgets.auth.WidgetAuthPhoneVerify;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetAuthLogin.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetAuthLogin$forgotPassword$2 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ String $login;
    public final /* synthetic */ WidgetAuthLogin this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetAuthLogin$forgotPassword$2(WidgetAuthLogin widgetAuthLogin, String str) {
        super(1);
        this.this$0 = widgetAuthLogin;
        this.$login = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        ValidationManager validationManager;
        ActivityResultLauncher<Intent> activityResultLauncher;
        ActivityResultLauncher<Intent> activityResultLauncher2;
        m.checkNotNullParameter(error, "error");
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        if (response.getCode() == 70007) {
            WidgetAuthPhoneVerify.Companion companion = WidgetAuthPhoneVerify.Companion;
            Context requireContext = this.this$0.requireContext();
            activityResultLauncher2 = this.this$0.phoneVerifyPasswordLauncher;
            String str = this.$login;
            String string = this.this$0.getString(R.string.password_recovery_verify_phone_title);
            m.checkNotNullExpressionValue(string, "getString(R.string.passw…overy_verify_phone_title)");
            String string2 = this.this$0.getString(R.string.password_recovery_verify_phone_subtitle);
            m.checkNotNullExpressionValue(string2, "getString(R.string.passw…ry_verify_phone_subtitle)");
            companion.launch(requireContext, activityResultLauncher2, str, string, string2);
            error.setShowErrorToasts(false);
            return;
        }
        validationManager = this.this$0.getValidationManager();
        Error.Response response2 = error.getResponse();
        m.checkNotNullExpressionValue(response2, "error.response");
        Map<String, List<String>> messages = response2.getMessages();
        m.checkNotNullExpressionValue(messages, "error.response.messages");
        List<String> mutableList = u.toMutableList((Collection) validationManager.setErrors(messages));
        WidgetAuthCaptcha.Companion companion2 = WidgetAuthCaptcha.Companion;
        Context requireContext2 = this.this$0.requireContext();
        activityResultLauncher = this.this$0.captchaForgotPasswordLauncher;
        companion2.processErrorsForCaptcha(requireContext2, activityResultLauncher, mutableList, error);
        error.setShowErrorToasts(!mutableList.isEmpty());
    }
}
