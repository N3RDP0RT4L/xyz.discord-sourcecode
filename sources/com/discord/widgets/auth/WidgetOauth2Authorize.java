package com.discord.widgets.auth;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.m;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.application.Application;
import com.discord.api.auth.OAuthScope;
import com.discord.api.auth.OAuthScopeTypeAdapter;
import com.discord.api.user.User;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.app.AppViewModel;
import com.discord.databinding.OauthTokenPermissionDetailedListItemBinding;
import com.discord.databinding.WidgetOauthAuthorizeBinding;
import com.discord.nullserializable.NullSerializable;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.captcha.CaptchaHelper;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import com.discord.views.OAuthPermissionViews;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.c0.c;
import d0.g;
import d0.g0.t;
import d0.g0.w;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.a0;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetOauth2Authorize.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\b\u0016\u0018\u0000 F2\u00020\u0001:\u0005FGHIJB\u0007¢\u0006\u0004\bE\u0010#J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001d\u0010\u0012\u001a\u00020\u00042\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0019\u0010\u0018\u001a\u00020\u00042\b\u0010\u0017\u001a\u0004\u0018\u00010\u0010H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u0007H\u0014¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\"\u0010#J\u0017\u0010&\u001a\u00020\u00042\u0006\u0010%\u001a\u00020$H\u0014¢\u0006\u0004\b&\u0010'R\u001d\u0010-\u001a\u00020(8D@\u0004X\u0084\u0084\u0002¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\"\u00101\u001a\u000e\u0012\u0004\u0012\u00020/\u0012\u0004\u0012\u0002000.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R\u001d\u00108\u001a\u0002038B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u001d\u0010=\u001a\u0002098D@\u0004X\u0084\u0084\u0002¢\u0006\f\n\u0004\b:\u00105\u001a\u0004\b;\u0010<R(\u0010@\u001a\b\u0012\u0004\u0012\u00020?0>8\u0014@\u0014X\u0094\u0004¢\u0006\u0012\n\u0004\b@\u0010A\u0012\u0004\bD\u0010#\u001a\u0004\bB\u0010C¨\u0006K"}, d2 = {"Lcom/discord/widgets/auth/WidgetOauth2Authorize;", "Lcom/discord/app/AppFragment;", "Lcom/discord/views/OAuthPermissionViews$InvalidScopeException;", "e", "", "onUnknownScope", "(Lcom/discord/views/OAuthPermissionViews$InvalidScopeException;)V", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "configureNotSupportedUI", "(Landroid/net/Uri;)V", "", "clientId", "configureAgeNoticeUI", "(J)V", "", "", "scopeNames", "configureSecurityNoticeUI", "(Ljava/util/List;)V", "", "getRandomFakeScopeText", "()I", "captchaKey", "authorizeApplication", "(Ljava/lang/String;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "requestUrl", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "createOauthAuthorize", "(Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "onViewBoundOrOnResume", "()V", "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;", "data", "configureUI", "(Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;)V", "Lcom/discord/databinding/WidgetOauthAuthorizeBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetOauthAuthorizeBinding;", "binding", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuthPermissionViewHolder;", "adapter", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager$delegate", "Lkotlin/Lazy;", "getValidationManager", "()Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;", "oauth2ViewModel$delegate", "getOauth2ViewModel", "()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;", "oauth2ViewModel", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "captchaLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "getCaptchaLauncher", "()Landroidx/activity/result/ActivityResultLauncher;", "getCaptchaLauncher$annotations", HookHelper.constructorName, "Companion", "OAuth2Authorize", "OAuth2ViewModel", "OAuthPermissionViewHolder", "PermissionModel", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class WidgetOauth2Authorize extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetOauth2Authorize.class, "binding", "getBinding()Lcom/discord/databinding/WidgetOauthAuthorizeBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_PARAM_URI = "REQ_URI";
    private static final String QUERY_INTERNAL_REFERRER = "internal_referrer";
    private static final String QUERY_PARAM_CLIENT_ID = "client_id";
    private static final String QUERY_PARAM_CODE_CHALLENGE = "code_challenge";
    private static final String QUERY_PARAM_CODE_CHALLENGE_METHOD = "code_challenge_method";
    private static final String QUERY_PARAM_PERMISSIONS = "permissions";
    private static final String QUERY_PARAM_REDIRECT = "redirect_uri";
    private static final String QUERY_PARAM_RESPONSE_TYPE = "response_type";
    private static final String QUERY_PARAM_SCOPE = "scope";
    private static final String QUERY_PARAM_STATE = "state";
    private static final String ROOT_ERROR_KEY = "_root";
    private final Lazy oauth2ViewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetOauth2Authorize$binding$2.INSTANCE, null, 2, null);
    private final ActivityResultLauncher<Intent> captchaLauncher = WidgetAuthCaptcha.Companion.registerForResult(this, new WidgetOauth2Authorize$captchaLauncher$1(this));
    private final Lazy validationManager$delegate = g.lazy(new WidgetOauth2Authorize$validationManager$2(this));
    private final SimpleRecyclerAdapter<PermissionModel, OAuthPermissionViewHolder> adapter = new SimpleRecyclerAdapter<>(null, WidgetOauth2Authorize$adapter$1.INSTANCE, 1, null);

    /* compiled from: WidgetOauth2Authorize.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0013\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b)\u0010*J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\n\u0010\u000bJ3\u0010\u0014\u001a\u00020\u00122\u0006\u0010\r\u001a\u00020\f2\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f2\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ\u001d\u0010\u001b\u001a\u00020\u00122\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001f\u0010\u001eR\u0016\u0010 \u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b \u0010\u001eR\u0016\u0010!\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b!\u0010\u001eR\u0016\u0010\"\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\"\u0010\u001eR\u0016\u0010#\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010\u001eR\u0016\u0010$\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b$\u0010\u001eR\u0016\u0010%\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010\u001eR\u0016\u0010&\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b&\u0010\u001eR\u0016\u0010'\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b'\u0010\u001eR\u0016\u0010(\u001a\u00020\u00188\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b(\u0010\u001e¨\u0006+"}, d2 = {"Lcom/discord/widgets/auth/WidgetOauth2Authorize$Companion;", "", "Landroid/net/Uri;", "requestUri", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "oAuth2Authorize", "Landroid/content/Intent;", "createLaunchIntent", "(Landroid/net/Uri;Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;)Landroid/content/Intent;", "requestUrl", "createOauthAuthorize", "(Landroid/net/Uri;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "Landroid/content/Context;", "context", "Ljava/lang/Exception;", "Lkotlin/Exception;", "e", "Lkotlin/Function0;", "", "onComplete", "handleError", "(Landroid/content/Context;Ljava/lang/Exception;Lkotlin/jvm/functions/Function0;)V", "", "clientId", "", "getNoticeName", "(J)Ljava/lang/String;", "launch", "(Landroid/content/Context;Landroid/net/Uri;)V", "INTENT_PARAM_URI", "Ljava/lang/String;", "QUERY_INTERNAL_REFERRER", "QUERY_PARAM_CLIENT_ID", "QUERY_PARAM_CODE_CHALLENGE", "QUERY_PARAM_CODE_CHALLENGE_METHOD", "QUERY_PARAM_PERMISSIONS", "QUERY_PARAM_REDIRECT", "QUERY_PARAM_RESPONSE_TYPE", "QUERY_PARAM_SCOPE", "QUERY_PARAM_STATE", "ROOT_ERROR_KEY", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Intent createLaunchIntent(Uri uri, OAuth2Authorize oAuth2Authorize) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.putExtra(WidgetOauth2Authorize.INTENT_PARAM_URI, uri);
            String internalReferrer = oAuth2Authorize.getInternalReferrer();
            if ((internalReferrer == null || !Boolean.parseBoolean(internalReferrer)) && !oAuth2Authorize.isUnsupported()) {
                intent.addFlags(268468224);
            }
            return intent;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final OAuth2Authorize createOauthAuthorize(Uri uri) {
            WidgetOauth2Authorize$Companion$createOauthAuthorize$1 widgetOauth2Authorize$Companion$createOauthAuthorize$1 = WidgetOauth2Authorize$Companion$createOauthAuthorize$1.INSTANCE;
            long parseLong = Long.parseLong(widgetOauth2Authorize$Companion$createOauthAuthorize$1.invoke(uri, WidgetOauth2Authorize.QUERY_PARAM_CLIENT_ID));
            String queryParameter = uri.getQueryParameter(WidgetOauth2Authorize.QUERY_PARAM_REDIRECT);
            return new OAuth2Authorize(parseLong, uri.getQueryParameter(WidgetOauth2Authorize.QUERY_PARAM_STATE), uri.getQueryParameter(WidgetOauth2Authorize.QUERY_PARAM_RESPONSE_TYPE), queryParameter, null, widgetOauth2Authorize$Companion$createOauthAuthorize$1.invoke(uri, WidgetOauth2Authorize.QUERY_PARAM_SCOPE), uri.getQueryParameter("permissions"), uri.getQueryParameter(WidgetOauth2Authorize.QUERY_PARAM_CODE_CHALLENGE), uri.getQueryParameter(WidgetOauth2Authorize.QUERY_PARAM_CODE_CHALLENGE_METHOD), uri.getQueryParameter(WidgetOauth2Authorize.QUERY_INTERNAL_REFERRER), 16, null);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void handleError(Context context, Exception exc, Function0<Unit> function0) {
            CharSequence b2;
            b2 = b.b(context, R.string.oauth2_request_missing_param, new Object[]{exc.getMessage()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            String obj = b2.toString();
            AppLog.g.w(obj, exc);
            m.h(context, obj, 1, null, 8);
            function0.invoke();
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ void handleError$default(Companion companion, Context context, Exception exc, Function0 function0, int i, Object obj) {
            if ((i & 4) != 0) {
                function0 = WidgetOauth2Authorize$Companion$handleError$1.INSTANCE;
            }
            companion.handleError(context, exc, function0);
        }

        public final String getNoticeName(long j) {
            return a.s("OAUTH_REQUEST:", j);
        }

        /* JADX WARN: Code restructure failed: missing block: B:15:0x0045, code lost:
            if ((android.os.Build.VERSION.SDK_INT >= 28 && d0.g0.t.equals(android.os.Build.MANUFACTURER, "samsung", true)) != false) goto L17;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final void launch(android.content.Context r8, android.net.Uri r9) {
            /*
                r7 = this;
                java.lang.String r0 = "context"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                java.lang.String r0 = "requestUri"
                d0.z.d.m.checkNotNullParameter(r9, r0)
                com.discord.widgets.auth.WidgetOauth2Authorize$OAuth2Authorize r0 = r7.createOauthAuthorize(r9)     // Catch: java.lang.Exception -> Lf
                goto L19
            Lf:
                r3 = move-exception
                r4 = 0
                r5 = 4
                r6 = 0
                r1 = r7
                r2 = r8
                handleError$default(r1, r2, r3, r4, r5, r6)
                r0 = 0
            L19:
                if (r0 == 0) goto L57
                long r1 = r0.getClientId()
                com.discord.utilities.analytics.AnalyticsTracker r3 = com.discord.utilities.analytics.AnalyticsTracker.INSTANCE
                r3.oauth2AuthorizedViewed(r1)
                java.lang.String r1 = java.lang.String.valueOf(r1)
                java.lang.String r2 = "591317049637339146"
                boolean r1 = d0.z.d.m.areEqual(r1, r2)
                r2 = 1
                r3 = 0
                if (r1 == 0) goto L48
                int r1 = android.os.Build.VERSION.SDK_INT
                r4 = 28
                if (r1 < r4) goto L44
                java.lang.String r1 = android.os.Build.MANUFACTURER
                java.lang.String r4 = "samsung"
                boolean r1 = d0.g0.t.equals(r1, r4, r2)
                if (r1 == 0) goto L44
                r1 = 1
                goto L45
            L44:
                r1 = 0
            L45:
                if (r1 == 0) goto L48
                goto L49
            L48:
                r2 = 0
            L49:
                if (r2 == 0) goto L4e
                java.lang.Class<com.discord.widgets.auth.WidgetOauth2AuthorizeSamsung> r1 = com.discord.widgets.auth.WidgetOauth2AuthorizeSamsung.class
                goto L50
            L4e:
                java.lang.Class<com.discord.widgets.auth.WidgetOauth2Authorize> r1 = com.discord.widgets.auth.WidgetOauth2Authorize.class
            L50:
                android.content.Intent r9 = r7.createLaunchIntent(r9, r0)
                b.a.d.j.d(r8, r1, r9)
            L57:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.auth.WidgetOauth2Authorize.Companion.launch(android.content.Context, android.net.Uri):void");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetOauth2Authorize.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0019\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001Bu\u0012\u0006\u0010\u0018\u001a\u00020\u000b\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u0006\u0012\u0006\u0010\u001d\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b<\u0010=J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00022\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000fJ\u0010\u0010\u0012\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000fJ\u0010\u0010\u0013\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000fJ\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u000fJ\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u000fJ\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u000fJ\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u000fJ\u0082\u0001\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u000b2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u001c\u001a\u00020\u00062\b\b\u0002\u0010\u001d\u001a\u00020\u00062\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010$\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b$\u0010\u000fJ\u0010\u0010&\u001a\u00020%HÖ\u0001¢\u0006\u0004\b&\u0010'J\u001a\u0010*\u001a\u00020)2\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b*\u0010+R\u001b\u0010!\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010,\u001a\u0004\b-\u0010\u000fR\u0019\u0010\u001c\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010,\u001a\u0004\b.\u0010\u000fR\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010,\u001a\u0004\b/\u0010\u000fR\u0019\u0010\u0018\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00100\u001a\u0004\b1\u0010\rR\u0019\u0010\u001d\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010,\u001a\u0004\b2\u0010\u000fR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010,\u001a\u0004\b3\u0010\u000fR\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010,\u001a\u0004\b4\u0010\u000fR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010,\u001a\u0004\b5\u0010\u000fR\u001b\u0010 \u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b \u0010,\u001a\u0004\b6\u0010\u000fR\u0013\u00107\u001a\u00020)8F@\u0006¢\u0006\u0006\u001a\u0004\b7\u00108R\u0016\u0010:\u001a\u00020)8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b9\u00108R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010,\u001a\u0004\b;\u0010\u000f¨\u0006>"}, d2 = {"Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "", "Lrx/Observable;", "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;", "get", "()Lrx/Observable;", "", "captchaKey", "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponsePost;", "post", "(Ljava/lang/String;)Lrx/Observable;", "", "component1", "()J", "component2", "()Ljava/lang/String;", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "component10", "clientId", WidgetOauth2Authorize.QUERY_PARAM_STATE, "responseType", "redirectUrl", "prompt", WidgetOauth2Authorize.QUERY_PARAM_SCOPE, "permissions", "codeChallenge", "codeChallengeMethod", "internalReferrer", "copy", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getInternalReferrer", "getPrompt", "getResponseType", "J", "getClientId", "getScope", "getRedirectUrl", "getState", "getPermissions", "getCodeChallengeMethod", "isUnsupported", "()Z", "getHasBotPermission", "hasBotPermission", "getCodeChallenge", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class OAuth2Authorize {
        private final long clientId;
        private final String codeChallenge;
        private final String codeChallengeMethod;
        private final String internalReferrer;
        private final String permissions;
        private final String prompt;
        private final String redirectUrl;
        private final String responseType;
        private final String scope;
        private final String state;

        public OAuth2Authorize(long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
            d0.z.d.m.checkNotNullParameter(str4, "prompt");
            d0.z.d.m.checkNotNullParameter(str5, WidgetOauth2Authorize.QUERY_PARAM_SCOPE);
            this.clientId = j;
            this.state = str;
            this.responseType = str2;
            this.redirectUrl = str3;
            this.prompt = str4;
            this.scope = str5;
            this.permissions = str6;
            this.codeChallenge = str7;
            this.codeChallengeMethod = str8;
            this.internalReferrer = str9;
        }

        private final boolean getHasBotPermission() {
            boolean z2;
            List<String> split$default = w.split$default((CharSequence) this.scope, new String[]{" "}, false, 0, 6, (Object) null);
            if ((split$default instanceof Collection) && split$default.isEmpty()) {
                return false;
            }
            for (String str : split$default) {
                if (t.equals(str, OAuthScope.Bot.INSTANCE.a(), true) || t.equals(str, OAuthScope.WebhookIncoming.INSTANCE.a(), true)) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    return true;
                }
            }
            return false;
        }

        public static /* synthetic */ Observable post$default(OAuth2Authorize oAuth2Authorize, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = null;
            }
            return oAuth2Authorize.post(str);
        }

        public final long component1() {
            return this.clientId;
        }

        public final String component10() {
            return this.internalReferrer;
        }

        public final String component2() {
            return this.state;
        }

        public final String component3() {
            return this.responseType;
        }

        public final String component4() {
            return this.redirectUrl;
        }

        public final String component5() {
            return this.prompt;
        }

        public final String component6() {
            return this.scope;
        }

        public final String component7() {
            return this.permissions;
        }

        public final String component8() {
            return this.codeChallenge;
        }

        public final String component9() {
            return this.codeChallengeMethod;
        }

        public final OAuth2Authorize copy(long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
            d0.z.d.m.checkNotNullParameter(str4, "prompt");
            d0.z.d.m.checkNotNullParameter(str5, WidgetOauth2Authorize.QUERY_PARAM_SCOPE);
            return new OAuth2Authorize(j, str, str2, str3, str4, str5, str6, str7, str8, str9);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OAuth2Authorize)) {
                return false;
            }
            OAuth2Authorize oAuth2Authorize = (OAuth2Authorize) obj;
            return this.clientId == oAuth2Authorize.clientId && d0.z.d.m.areEqual(this.state, oAuth2Authorize.state) && d0.z.d.m.areEqual(this.responseType, oAuth2Authorize.responseType) && d0.z.d.m.areEqual(this.redirectUrl, oAuth2Authorize.redirectUrl) && d0.z.d.m.areEqual(this.prompt, oAuth2Authorize.prompt) && d0.z.d.m.areEqual(this.scope, oAuth2Authorize.scope) && d0.z.d.m.areEqual(this.permissions, oAuth2Authorize.permissions) && d0.z.d.m.areEqual(this.codeChallenge, oAuth2Authorize.codeChallenge) && d0.z.d.m.areEqual(this.codeChallengeMethod, oAuth2Authorize.codeChallengeMethod) && d0.z.d.m.areEqual(this.internalReferrer, oAuth2Authorize.internalReferrer);
        }

        public final Observable<RestAPIParams.OAuth2Authorize.ResponseGet> get() {
            return RestAPI.Companion.getApi().getOauth2Authorize(String.valueOf(this.clientId), this.state, this.responseType, this.redirectUrl, this.prompt, this.scope, this.permissions);
        }

        public final long getClientId() {
            return this.clientId;
        }

        public final String getCodeChallenge() {
            return this.codeChallenge;
        }

        public final String getCodeChallengeMethod() {
            return this.codeChallengeMethod;
        }

        public final String getInternalReferrer() {
            return this.internalReferrer;
        }

        public final String getPermissions() {
            return this.permissions;
        }

        public final String getPrompt() {
            return this.prompt;
        }

        public final String getRedirectUrl() {
            return this.redirectUrl;
        }

        public final String getResponseType() {
            return this.responseType;
        }

        public final String getScope() {
            return this.scope;
        }

        public final String getState() {
            return this.state;
        }

        public int hashCode() {
            int a = a0.a.a.b.a(this.clientId) * 31;
            String str = this.state;
            int i = 0;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.responseType;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.redirectUrl;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.prompt;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.scope;
            int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
            String str6 = this.permissions;
            int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
            String str7 = this.codeChallenge;
            int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
            String str8 = this.codeChallengeMethod;
            int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
            String str9 = this.internalReferrer;
            if (str9 != null) {
                i = str9.hashCode();
            }
            return hashCode8 + i;
        }

        public final boolean isUnsupported() {
            return getHasBotPermission();
        }

        public final Observable<RestAPIParams.OAuth2Authorize.ResponsePost> post(String str) {
            Map map;
            Map mapOf = g0.mapOf(o.to("authorize", "true"));
            if (str != null) {
                map = g0.mapOf(o.to(CaptchaHelper.CAPTCHA_KEY, str));
            } else {
                map = h0.emptyMap();
            }
            return RestAPI.Companion.getApi().postOauth2Authorize(String.valueOf(this.clientId), this.state, this.responseType, this.redirectUrl, this.prompt, this.scope, this.permissions, this.codeChallenge, this.codeChallengeMethod, h0.plus(mapOf, map));
        }

        public String toString() {
            StringBuilder R = a.R("OAuth2Authorize(clientId=");
            R.append(this.clientId);
            R.append(", state=");
            R.append(this.state);
            R.append(", responseType=");
            R.append(this.responseType);
            R.append(", redirectUrl=");
            R.append(this.redirectUrl);
            R.append(", prompt=");
            R.append(this.prompt);
            R.append(", scope=");
            R.append(this.scope);
            R.append(", permissions=");
            R.append(this.permissions);
            R.append(", codeChallenge=");
            R.append(this.codeChallenge);
            R.append(", codeChallengeMethod=");
            R.append(this.codeChallengeMethod);
            R.append(", internalReferrer=");
            return a.H(R, this.internalReferrer, ")");
        }

        public /* synthetic */ OAuth2Authorize(long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(j, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : str3, (i & 16) != 0 ? "consent" : str4, str5, (i & 64) != 0 ? null : str6, (i & 128) != 0 ? null : str7, (i & 256) != 0 ? null : str8, (i & 512) != 0 ? null : str9);
        }
    }

    /* compiled from: WidgetOauth2Authorize.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0011\u0010\u0012R$\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\"\u0010\u000b\u001a\u00020\n8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2ViewModel;", "Lcom/discord/app/AppViewModel;", "", "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;", "oauthGetResponse", "Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;", "getOauthGetResponse", "()Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;", "setOauthGetResponse", "(Lcom/discord/restapi/RestAPIParams$OAuth2Authorize$ResponseGet;)V", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "oauthAuthorize", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "getOauthAuthorize", "()Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;", "setOauthAuthorize", "(Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuth2Authorize;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class OAuth2ViewModel extends AppViewModel<Unit> {
        public OAuth2Authorize oauthAuthorize;
        private RestAPIParams.OAuth2Authorize.ResponseGet oauthGetResponse;

        public OAuth2ViewModel() {
            super(null, 1, null);
        }

        public final OAuth2Authorize getOauthAuthorize() {
            OAuth2Authorize oAuth2Authorize = this.oauthAuthorize;
            if (oAuth2Authorize == null) {
                d0.z.d.m.throwUninitializedPropertyAccessException("oauthAuthorize");
            }
            return oAuth2Authorize;
        }

        public final RestAPIParams.OAuth2Authorize.ResponseGet getOauthGetResponse() {
            return this.oauthGetResponse;
        }

        public final void setOauthAuthorize(OAuth2Authorize oAuth2Authorize) {
            d0.z.d.m.checkNotNullParameter(oAuth2Authorize, "<set-?>");
            this.oauthAuthorize = oAuth2Authorize;
        }

        public final void setOauthGetResponse(RestAPIParams.OAuth2Authorize.ResponseGet responseGet) {
            this.oauthGetResponse = responseGet;
        }
    }

    /* compiled from: WidgetOauth2Authorize.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/auth/WidgetOauth2Authorize$OAuthPermissionViewHolder;", "Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder;", "Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;", "data", "", "bind", "(Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;)V", "Lcom/discord/databinding/OauthTokenPermissionDetailedListItemBinding;", "binding", "Lcom/discord/databinding/OauthTokenPermissionDetailedListItemBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/OauthTokenPermissionDetailedListItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class OAuthPermissionViewHolder extends SimpleRecyclerAdapter.ViewHolder<PermissionModel> {
        private final OauthTokenPermissionDetailedListItemBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public OAuthPermissionViewHolder(com.discord.databinding.OauthTokenPermissionDetailedListItemBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                androidx.constraintlayout.widget.ConstraintLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r0)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.auth.WidgetOauth2Authorize.OAuthPermissionViewHolder.<init>(com.discord.databinding.OauthTokenPermissionDetailedListItemBinding):void");
        }

        public void bind(PermissionModel permissionModel) {
            d0.z.d.m.checkNotNullParameter(permissionModel, "data");
            if (permissionModel.getScope() != null) {
                this.binding.f2116b.setImageResource(R.drawable.ic_check_circle_green_24dp);
                TextView textView = this.binding.c;
                d0.z.d.m.checkNotNullExpressionValue(textView, "binding.oauthTokenPermissionDetailedName");
                OAuthPermissionViews.a(textView, permissionModel.getScope());
            } else if (permissionModel.getFakeText() != null) {
                this.binding.f2116b.setImageResource(R.drawable.ic_close_circle_grey_24dp);
                this.binding.c.setText(permissionModel.getFakeText().intValue());
            }
        }
    }

    /* compiled from: WidgetOauth2Authorize.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/auth/WidgetOauth2Authorize$PermissionModel;", "", "", "fakeText", "Ljava/lang/Integer;", "getFakeText", "()Ljava/lang/Integer;", "Lcom/discord/api/auth/OAuthScope;", WidgetOauth2Authorize.QUERY_PARAM_SCOPE, "Lcom/discord/api/auth/OAuthScope;", "getScope", "()Lcom/discord/api/auth/OAuthScope;", HookHelper.constructorName, "(Lcom/discord/api/auth/OAuthScope;Ljava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PermissionModel {
        private final Integer fakeText;
        private final OAuthScope scope;

        public PermissionModel(OAuthScope oAuthScope, Integer num) {
            this.scope = oAuthScope;
            this.fakeText = num;
        }

        public final Integer getFakeText() {
            return this.fakeText;
        }

        public final OAuthScope getScope() {
            return this.scope;
        }
    }

    public WidgetOauth2Authorize() {
        super(R.layout.widget_oauth_authorize);
        WidgetOauth2Authorize$oauth2ViewModel$2 widgetOauth2Authorize$oauth2ViewModel$2 = WidgetOauth2Authorize$oauth2ViewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.oauth2ViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(OAuth2ViewModel.class), new WidgetOauth2Authorize$appViewModels$$inlined$viewModels$1(f0Var), new b.a.d.h0(widgetOauth2Authorize$oauth2ViewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void authorizeApplication(String str) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(getOauth2ViewModel().getOauthAuthorize().post(str), false, 1, null), this, null, 2, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetOauth2Authorize$authorizeApplication$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetOauth2Authorize$authorizeApplication$2(this));
    }

    private final void configureAgeNoticeUI(long j) {
        long parseSnowflake = TimeUtils.parseSnowflake(Long.valueOf(j));
        TextView textView = getBinding().f2483b;
        d0.z.d.m.checkNotNullExpressionValue(textView, "binding.oauthAuthorizeAgeNotice");
        b.m(textView, R.string.oauth2_details_creation_date, new Object[]{DateUtils.formatDateTime(requireContext(), parseSnowflake, 65536)}, (r4 & 4) != 0 ? b.g.j : null);
    }

    private final void configureNotSupportedUI(final Uri uri) {
        LinearLayout linearLayout = getBinding().j;
        d0.z.d.m.checkNotNullExpressionValue(linearLayout, "binding.oauthAuthorizeNotSupported");
        linearLayout.setVisibility(0);
        LinearLayout linearLayout2 = getBinding().h;
        d0.z.d.m.checkNotNullExpressionValue(linearLayout2, "binding.oauthAuthorizeLoading");
        linearLayout2.setVisibility(8);
        LinearLayout linearLayout3 = getBinding().e;
        d0.z.d.m.checkNotNullExpressionValue(linearLayout3, "binding.oauthAuthorizeContent");
        linearLayout3.setVisibility(8);
        getBinding().n.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetOauth2Authorize$configureNotSupportedUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                m.c(a.x(view, "it", "it.context"), String.valueOf(uri), 0, 4);
                AppActivity appActivity = WidgetOauth2Authorize.this.getAppActivity();
                if (appActivity != null) {
                    appActivity.onBackPressed();
                }
            }
        });
    }

    private final void configureSecurityNoticeUI(List<String> list) {
        int i = list.contains(OAuthScope.MessagesRead.INSTANCE.a()) ? R.string.oauth2_can_read_notice : R.string.oauth2_cannot_read_send_notice;
        TextView textView = getBinding().m;
        d0.z.d.m.checkNotNullExpressionValue(textView, "binding.oauthAuthorizeSecurityNotice");
        b.m(textView, i, new Object[0], new WidgetOauth2Authorize$configureSecurityNoticeUI$1(this));
    }

    public static /* synthetic */ void getCaptchaLauncher$annotations() {
    }

    private final int getRandomFakeScopeText() {
        switch (c.k.nextInt(8)) {
            case 0:
                return R.string.oauth2_fake_scope_1;
            case 1:
                return R.string.oauth2_fake_scope_2;
            case 2:
                return R.string.oauth2_fake_scope_3;
            case 3:
                return R.string.oauth2_fake_scope_4;
            case 4:
                return R.string.oauth2_fake_scope_5;
            case 5:
                return R.string.oauth2_fake_scope_6;
            case 6:
                return R.string.oauth2_fake_scope_7;
            default:
                return R.string.oauth2_fake_scope_8;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ValidationManager getValidationManager() {
        return (ValidationManager) this.validationManager$delegate.getValue();
    }

    private final void onUnknownScope(OAuthPermissionViews.InvalidScopeException invalidScopeException) {
        CharSequence b2;
        AppLog appLog = AppLog.g;
        StringBuilder R = a.R("invalid scope ");
        R.append(invalidScopeException.a());
        R.append(" in ");
        R.append(getClass().getSimpleName());
        Logger.e$default(appLog, R.toString(), invalidScopeException, null, 4, null);
        Context requireContext = requireContext();
        b2 = b.b(requireContext(), R.string.oauth2_request_invalid_scope, new Object[]{invalidScopeException.a()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        m.h(requireContext, b2, 1, null, 8);
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.setResult(0);
            appActivity.finish();
        }
    }

    public void configureUI(RestAPIParams.OAuth2Authorize.ResponseGet responseGet) {
        HashMap hashMap;
        d0.z.d.m.checkNotNullParameter(responseGet, "data");
        LinearLayout linearLayout = getBinding().j;
        d0.z.d.m.checkNotNullExpressionValue(linearLayout, "binding.oauthAuthorizeNotSupported");
        linearLayout.setVisibility(8);
        LinearLayout linearLayout2 = getBinding().h;
        d0.z.d.m.checkNotNullExpressionValue(linearLayout2, "binding.oauthAuthorizeLoading");
        linearLayout2.setVisibility(8);
        LinearLayout linearLayout3 = getBinding().e;
        d0.z.d.m.checkNotNullExpressionValue(linearLayout3, "binding.oauthAuthorizeContent");
        linearLayout3.setVisibility(0);
        User user = responseGet.getUser();
        SimpleDraweeView simpleDraweeView = getBinding().p;
        d0.z.d.m.checkNotNullExpressionValue(simpleDraweeView, "binding.oauthAuthorizeUserIcon");
        Long valueOf = Long.valueOf(user.i());
        NullSerializable<String> a = user.a();
        MGImages.setImage$default(simpleDraweeView, IconUtils.getForUser$default(valueOf, a != null ? a.a() : null, null, false, null, 28, null), 0, 0, false, null, null, 124, null);
        Application application = responseGet.getApplication();
        TextView textView = getBinding().d;
        d0.z.d.m.checkNotNullExpressionValue(textView, "binding.oauthAuthorizeApplicationName");
        textView.setText(application.h());
        TextView textView2 = getBinding().k;
        d0.z.d.m.checkNotNullExpressionValue(textView2, "binding.oauthAuthorizePermissionsLabelTv");
        b.m(textView2, R.string.oauth2_scopes_label, new Object[]{application.h()}, (r4 & 4) != 0 ? b.g.j : null);
        SimpleDraweeView simpleDraweeView2 = getBinding().c;
        d0.z.d.m.checkNotNullExpressionValue(simpleDraweeView2, "binding.oauthAuthorizeApplicationIcon");
        String f = application.f();
        MGImages.setImage$default(simpleDraweeView2, f != null ? IconUtils.getApplicationIcon$default(application.g(), f, 0, 4, (Object) null) : null, 0, 0, false, null, null, 124, null);
        try {
            List split$default = w.split$default((CharSequence) getOauth2ViewModel().getOauthAuthorize().getScope(), new String[]{" "}, false, 0, 6, (Object) null);
            ArrayList<String> arrayList = new ArrayList();
            for (Object obj : split$default) {
                if (((String) obj).length() > 0) {
                    arrayList.add(obj);
                }
            }
            ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
            for (String str : arrayList) {
                Objects.requireNonNull(OAuthScopeTypeAdapter.Companion);
                hashMap = OAuthScopeTypeAdapter.nameToScopeMap;
                arrayList2.add(new PermissionModel((OAuthScope) hashMap.get(str), null));
            }
            List mutableList = u.toMutableList((Collection) arrayList2);
            this.adapter.setData(u.plus((Collection) mutableList, (Iterable) (mutableList.isEmpty() ^ true ? d0.t.m.listOf(new PermissionModel(null, Integer.valueOf(getRandomFakeScopeText()))) : n.emptyList())));
            getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetOauth2Authorize$configureUI$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetOauth2Authorize.this.authorizeApplication(null);
                }
            });
        } catch (OAuthPermissionViews.InvalidScopeException e) {
            onUnknownScope(e);
        }
    }

    public OAuth2Authorize createOauthAuthorize(Uri uri) {
        d0.z.d.m.checkNotNullParameter(uri, "requestUrl");
        return Companion.createOauthAuthorize(uri);
    }

    public final WidgetOauthAuthorizeBinding getBinding() {
        return (WidgetOauthAuthorizeBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public ActivityResultLauncher<Intent> getCaptchaLauncher() {
        return this.captchaLauncher;
    }

    public final OAuth2ViewModel getOauth2ViewModel() {
        return (OAuth2ViewModel) this.oauth2ViewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        d0.z.d.m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        RecyclerView recyclerView = getBinding().l;
        d0.z.d.m.checkNotNullExpressionValue(recyclerView, "binding.oauthAuthorizePermissionsList");
        recyclerView.setAdapter(this.adapter);
        Uri uri = (Uri) getMostRecentIntent().getParcelableExtra(INTENT_PARAM_URI);
        if (uri == null) {
            uri = Uri.EMPTY;
        }
        try {
            OAuth2ViewModel oauth2ViewModel = getOauth2ViewModel();
            d0.z.d.m.checkNotNullExpressionValue(uri, "requestUrl");
            oauth2ViewModel.setOauthAuthorize(createOauthAuthorize(uri));
            for (MaterialButton materialButton : n.listOf((Object[]) new MaterialButton[]{getBinding().f, getBinding().i, getBinding().o})) {
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.auth.WidgetOauth2Authorize$onViewBound$$inlined$forEach$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        AppActivity appActivity = WidgetOauth2Authorize.this.getAppActivity();
                        if (appActivity != null) {
                            appActivity.onBackPressed();
                        }
                    }
                });
            }
            AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.auth.WidgetOauth2Authorize$onViewBound$3
                @Override // rx.functions.Func0, java.util.concurrent.Callable
                public final Boolean call() {
                    AppActivity appActivity = WidgetOauth2Authorize.this.getAppActivity();
                    if (appActivity != null) {
                        appActivity.finishAndRemoveTask();
                    }
                    return Boolean.TRUE;
                }
            }, 0, 2, null);
            configureAgeNoticeUI(getOauth2ViewModel().getOauthAuthorize().getClientId());
            configureSecurityNoticeUI(w.split$default((CharSequence) getOauth2ViewModel().getOauthAuthorize().getScope(), new String[]{" "}, false, 0, 6, (Object) null));
            StoreNotices.markSeen$default(StoreStream.Companion.getNotices(), Companion.getNoticeName(getOauth2ViewModel().getOauthAuthorize().getClientId()), 0L, 2, null);
        } catch (IllegalArgumentException e) {
            Companion.handleError(requireContext(), e, new WidgetOauth2Authorize$onViewBound$1(this));
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        AppActivity appActivity = getAppActivity();
        if (appActivity != null && appActivity.isFinishing()) {
            return;
        }
        if (getOauth2ViewModel().getOauthAuthorize().isUnsupported()) {
            Uri uri = (Uri) getMostRecentIntent().getParcelableExtra(INTENT_PARAM_URI);
            if (uri == null) {
                uri = Uri.EMPTY;
            }
            d0.z.d.m.checkNotNullExpressionValue(uri, "requestUrl");
            configureNotSupportedUI(uri);
            return;
        }
        RestAPIParams.OAuth2Authorize.ResponseGet oauthGetResponse = getOauth2ViewModel().getOauthGetResponse();
        if (oauthGetResponse == null) {
            LinearLayout linearLayout = getBinding().j;
            d0.z.d.m.checkNotNullExpressionValue(linearLayout, "binding.oauthAuthorizeNotSupported");
            linearLayout.setVisibility(8);
            LinearLayout linearLayout2 = getBinding().e;
            d0.z.d.m.checkNotNullExpressionValue(linearLayout2, "binding.oauthAuthorizeContent");
            linearLayout2.setVisibility(8);
            LinearLayout linearLayout3 = getBinding().h;
            d0.z.d.m.checkNotNullExpressionValue(linearLayout3, "binding.oauthAuthorizeLoading");
            linearLayout3.setVisibility(0);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(getOauth2ViewModel().getOauthAuthorize().get(), false, 1, null), this, null, 2, null), getClass(), (r18 & 2) != 0 ? null : requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetOauth2Authorize$onViewBoundOrOnResume$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetOauth2Authorize$onViewBoundOrOnResume$2(this));
            return;
        }
        configureUI(oauthGetResponse);
    }
}
