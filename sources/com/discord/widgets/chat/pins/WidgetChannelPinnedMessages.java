package com.discord.widgets.chat.pins;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.role.GuildRole;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChannelPinnedMessagesBinding;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import com.discord.stores.StoreChat;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.embed.InviteEmbedModel;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.WidgetUrlActions;
import com.discord.widgets.chat.list.ThreadSpineItemDecoration;
import com.discord.widgets.chat.list.actions.WidgetChatListActions;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemCallMessage;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.DividerEntry;
import com.discord.widgets.chat.list.entries.EmptyPinsEntry;
import com.discord.widgets.chat.list.entries.LoadingEntry;
import com.discord.widgets.chat.list.model.WidgetChatListModelMessages;
import com.discord.widgets.chat.pins.WidgetChannelPinnedMessages;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func9;
import xyz.discord.R;
/* compiled from: WidgetChannelPinnedMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 \u001b2\u00020\u0001:\u0003\u001c\u001b\u001dB\u0007¢\u0006\u0004\b\u001a\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\u0010J\u000f\u0010\u0012\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0012\u0010\u0010R\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010\u0013R\u001d\u0010\u0019\u001a\u00020\u00148B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", "", "addThreadSpineItemDecoration", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$Model;", "model", "configureUI", "(Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$Model;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "onPause", "onDestroy", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "Lcom/discord/databinding/WidgetChannelPinnedMessagesBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChannelPinnedMessagesBinding;", "binding", HookHelper.constructorName, "Companion", "ChannelPinnedMessagesAdapterEventHandler", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelPinnedMessages extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChannelPinnedMessages.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChannelPinnedMessagesBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private WidgetChatListAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChannelPinnedMessages$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetChannelPinnedMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u001a\u001a\u00020\u0019\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\"\u0010#J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ'\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0013\u0010\u0014J\u001f\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0015H\u0016¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!¨\u0006$"}, d2 = {"Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$ChannelPinnedMessagesAdapterEventHandler;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "Lcom/discord/models/message/Message;", "message", "", "isThreadStarterMessage", "", "onMessageClicked", "(Lcom/discord/models/message/Message;Z)V", "", "formattedMessage", "onMessageLongClicked", "(Lcom/discord/models/message/Message;Ljava/lang/CharSequence;Z)V", "Lcom/discord/api/channel/Channel;", "channel", "onThreadClicked", "(Lcom/discord/api/channel/Channel;)V", "", "url", "onUrlLongClicked", "(Ljava/lang/String;)V", "Lcom/discord/api/sticker/BaseSticker;", "sticker", "onStickerClicked", "(Lcom/discord/models/message/Message;Lcom/discord/api/sticker/BaseSticker;)V", "Landroid/content/Context;", "context", "Landroid/content/Context;", "Lcom/discord/utilities/channel/ChannelSelector;", "channelSelector", "Lcom/discord/utilities/channel/ChannelSelector;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", HookHelper.constructorName, "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/channel/ChannelSelector;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelPinnedMessagesAdapterEventHandler implements WidgetChatListAdapter.EventHandler {
        private final ChannelSelector channelSelector;
        private final Context context;
        private final FragmentManager fragmentManager;

        public ChannelPinnedMessagesAdapterEventHandler(Context context, FragmentManager fragmentManager, ChannelSelector channelSelector) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(channelSelector, "channelSelector");
            this.context = context;
            this.fragmentManager = fragmentManager;
            this.channelSelector = channelSelector;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onBotUiComponentClicked(long j, Long l, long j2, long j3, Long l2, int i, RestAPIParams.ComponentInteractionData componentInteractionData) {
            m.checkNotNullParameter(componentInteractionData, "componentSendData");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onBotUiComponentClicked(this, j, l, j2, j3, l2, i, componentInteractionData);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onCallMessageClicked(long j, WidgetChatListAdapterItemCallMessage.CallStatus callStatus) {
            m.checkNotNullParameter(callStatus, "callStatus");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onCallMessageClicked(this, j, callStatus);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onCommandClicked(long j, Long l, long j2, long j3, long j4, long j5, String str) {
            WidgetChatListAdapter.EventHandler.DefaultImpls.onCommandClicked(this, j, l, j2, j3, j4, j5, str);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onDismissClicked(Message message) {
            m.checkNotNullParameter(message, "message");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onDismissClicked(this, message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onInteractionStateUpdated(StoreChat.InteractionState interactionState) {
            m.checkNotNullParameter(interactionState, "interactionState");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onInteractionStateUpdated(this, interactionState);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onListClicked() {
            WidgetChatListAdapter.EventHandler.DefaultImpls.onListClicked(this);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageAuthorAvatarClicked(Message message, long j) {
            m.checkNotNullParameter(message, "message");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onMessageAuthorAvatarClicked(this, message, j);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageAuthorLongClicked(Message message, Long l) {
            m.checkNotNullParameter(message, "message");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onMessageAuthorLongClicked(this, message, l);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageAuthorNameClicked(Message message, long j) {
            m.checkNotNullParameter(message, "message");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onMessageAuthorNameClicked(this, message, j);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageBlockedGroupClicked(Message message) {
            m.checkNotNullParameter(message, "message");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onMessageBlockedGroupClicked(this, message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageClicked(Message message, boolean z2) {
            m.checkNotNullParameter(message, "message");
            StoreStream.Companion.getMessagesLoader().jumpToMessage(message.getChannelId(), message.getId());
            j.c(this.context, false, null, 6);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onMessageLongClicked(Message message, CharSequence charSequence, boolean z2) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(charSequence, "formattedMessage");
            WidgetChatListActions.Companion.showForPin(this.fragmentManager, message.getChannelId(), message.getId(), charSequence);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onOldestMessageId(long j, long j2) {
            WidgetChatListAdapter.EventHandler.DefaultImpls.onOldestMessageId(this, j, j2);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onOpenPinsClicked(Message message) {
            m.checkNotNullParameter(message, "message");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onOpenPinsClicked(this, message);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onQuickAddReactionClicked(long j, long j2, long j3, long j4) {
            WidgetChatListAdapter.EventHandler.DefaultImpls.onQuickAddReactionClicked(this, j, j2, j3, j4);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public boolean onQuickDownloadClicked(Uri uri, String str) {
            m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
            m.checkNotNullParameter(str, "fileName");
            return WidgetChatListAdapter.EventHandler.DefaultImpls.onQuickDownloadClicked(this, uri, str);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onReactionClicked(long j, long j2, long j3, long j4, MessageReaction messageReaction, boolean z2) {
            m.checkNotNullParameter(messageReaction, "reaction");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onReactionClicked(this, j, j2, j3, j4, messageReaction, z2);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onReactionLongClicked(long j, long j2, long j3, MessageReaction messageReaction) {
            m.checkNotNullParameter(messageReaction, "reaction");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onReactionLongClicked(this, j, j2, j3, messageReaction);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onSendGreetMessageClicked(long j, int i, Sticker sticker) {
            m.checkNotNullParameter(sticker, "sticker");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onSendGreetMessageClicked(this, j, i, sticker);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onShareButtonClick(GuildScheduledEvent guildScheduledEvent, WeakReference<Context> weakReference, WeakReference<AppFragment> weakReference2) {
            m.checkNotNullParameter(guildScheduledEvent, "guildEvent");
            m.checkNotNullParameter(weakReference, "weakContext");
            m.checkNotNullParameter(weakReference2, "weakFragment");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onShareButtonClick(this, guildScheduledEvent, weakReference, weakReference2);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onStickerClicked(Message message, BaseSticker baseSticker) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(baseSticker, "sticker");
            StoreStream.Companion.getMessagesLoader().jumpToMessage(message.getChannelId(), message.getId());
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onThreadClicked(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            ChannelSelector.selectChannel$default(this.channelSelector, channel, null, SelectedChannelAnalyticsLocation.EMBED, 2, null);
            j.c(this.context, false, null, 6);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onThreadLongClicked(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onThreadLongClicked(this, channel);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onUrlLongClicked(String str) {
            m.checkNotNullParameter(str, "url");
            WidgetUrlActions.Companion.launch(this.fragmentManager, str);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onUserActivityAction(long j, long j2, long j3, MessageActivityType messageActivityType, Activity activity, Application application) {
            m.checkNotNullParameter(messageActivityType, "messageActivityType");
            m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            m.checkNotNullParameter(application, "application");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onUserActivityAction(this, j, j2, j3, messageActivityType, activity, application);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onUserMentionClicked(long j, long j2, long j3) {
            WidgetChatListAdapter.EventHandler.DefaultImpls.onUserMentionClicked(this, j, j2, j3);
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
        public void onWelcomeCtaClicked(Message message, Channel channel, BaseSticker baseSticker) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(baseSticker, "sticker");
            WidgetChatListAdapter.EventHandler.DefaultImpls.onWelcomeCtaClicked(this, message, channel, baseSticker);
        }
    }

    /* compiled from: WidgetChannelPinnedMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$Companion;", "", "Landroid/content/Context;", "context", "", "channelId", "", "show", "(Landroid/content/Context;J)V", "", WidgetChannelPinnedMessages.INTENT_EXTRA_CHANNEL_ID, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra(WidgetChannelPinnedMessages.INTENT_EXTRA_CHANNEL_ID, j);
            j.d(context, WidgetChannelPinnedMessages.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelPinnedMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0018\b\u0082\b\u0018\u0000 F2\u00020\u0001:\u0001FB\u0083\u0001\u0012\u0006\u0010\u001d\u001a\u00020\u0002\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u001f\u001a\u00020\b\u0012\u0012\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f\u0012\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\b0\u0013\u0012\b\b\u0002\u0010#\u001a\u00020\b\u0012\b\b\u0002\u0010$\u001a\u00020\b\u0012\b\b\u0002\u0010%\u001a\u00020\b\u0012\b\b\u0002\u0010&\u001a\u00020\b\u0012\b\b\u0002\u0010'\u001a\u00020\u001a¢\u0006\u0004\bD\u0010EJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u001c\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\b0\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0016\u0010\nJ\u0010\u0010\u0017\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0017\u0010\nJ\u0010\u0010\u0018\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0018\u0010\nJ\u0010\u0010\u0019\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u0019\u0010\nJ\u0010\u0010\u001b\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0098\u0001\u0010(\u001a\u00020\u00002\b\b\u0002\u0010\u001d\u001a\u00020\u00022\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u001f\u001a\u00020\b2\u0014\b\u0002\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\f0\u000b2\u000e\b\u0002\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\b0\u00132\b\b\u0002\u0010#\u001a\u00020\b2\b\b\u0002\u0010$\u001a\u00020\b2\b\b\u0002\u0010%\u001a\u00020\b2\b\b\u0002\u0010&\u001a\u00020\b2\b\b\u0002\u0010'\u001a\u00020\u001aHÆ\u0001¢\u0006\u0004\b(\u0010)J\u0010\u0010*\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b*\u0010+J\u0010\u0010-\u001a\u00020,HÖ\u0001¢\u0006\u0004\b-\u0010.J\u001a\u00101\u001a\u00020\u001a2\b\u00100\u001a\u0004\u0018\u00010/HÖ\u0003¢\u0006\u0004\b1\u00102R\"\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b!\u00103\u001a\u0004\b4\u0010\u0012R\u001c\u0010%\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b%\u00105\u001a\u0004\b6\u0010\nR\u001e\u0010\u001e\u001a\u0004\u0018\u00010\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001e\u00107\u001a\u0004\b8\u0010\u0007R\u001c\u0010\u001f\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001f\u00105\u001a\u0004\b9\u0010\nR\u001c\u0010'\u001a\u00020\u001a8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b'\u0010:\u001a\u0004\b'\u0010\u001cR\u001c\u0010&\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u00105\u001a\u0004\b;\u0010\nR\u001c\u0010$\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u00105\u001a\u0004\b<\u0010\nR\u0019\u0010\u001d\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010=\u001a\u0004\b>\u0010\u0004R(\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\f0\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b \u0010?\u001a\u0004\b@\u0010\u000eR\"\u0010\"\u001a\b\u0012\u0004\u0012\u00020\b0\u00138\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\"\u0010A\u001a\u0004\bB\u0010\u0015R\u001c\u0010#\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b#\u00105\u001a\u0004\bC\u0010\n¨\u0006G"}, d2 = {"Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$Model;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "", "component3", "()J", "", "", "component4", "()Ljava/util/Map;", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "component5", "()Ljava/util/List;", "", "component6", "()Ljava/util/Set;", "component7", "component8", "component9", "component10", "", "component11", "()Z", "channel", "guild", "userId", "channelNames", "list", "myRoleIds", "channelId", "guildId", "oldestMessageId", "newMessagesMarkerMessageId", "isSpoilerClickAllowed", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;JLjava/util/Map;Ljava/util/List;Ljava/util/Set;JJJJZ)Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$Model;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getList", "J", "getOldestMessageId", "Lcom/discord/models/guild/Guild;", "getGuild", "getUserId", "Z", "getNewMessagesMarkerMessageId", "getGuildId", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/Map;", "getChannelNames", "Ljava/util/Set;", "getMyRoleIds", "getChannelId", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;JLjava/util/Map;Ljava/util/List;Ljava/util/Set;JJJJZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model implements WidgetChatListAdapter.Data {
        public static final Companion Companion = new Companion(null);
        private final Channel channel;
        private final long channelId;
        private final Map<Long, String> channelNames;
        private final Guild guild;
        private final long guildId;
        private final boolean isSpoilerClickAllowed;
        private final List<ChatListEntry> list;
        private final Set<Long> myRoleIds;
        private final long newMessagesMarkerMessageId;
        private final long oldestMessageId;
        private final long userId;

        /* compiled from: WidgetChannelPinnedMessages.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ)\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$Model$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "Lcom/discord/widgets/chat/pins/WidgetChannelPinnedMessages$Model;", "get", "(Landroid/content/Context;J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(final Context context, long j) {
                m.checkNotNullParameter(context, "context");
                StoreStream.Companion companion = StoreStream.Companion;
                final Channel findChannelById = companion.getChannels().findChannelById(j);
                if (findChannelById != null) {
                    Observable Y = companion.getPinnedMessages().observeForChannel(j).Y(new b<List<? extends Message>, Observable<? extends Model>>() { // from class: com.discord.widgets.chat.pins.WidgetChannelPinnedMessages$Model$Companion$get$1
                        @Override // j0.k.b
                        public /* bridge */ /* synthetic */ Observable<? extends WidgetChannelPinnedMessages.Model> call(List<? extends Message> list) {
                            return call2((List<Message>) list);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final Observable<? extends WidgetChannelPinnedMessages.Model> call2(final List<Message> list) {
                            StoreStream.Companion companion2 = StoreStream.Companion;
                            Observable<Long> observeMeId = companion2.getUsers().observeMeId();
                            WidgetChatListModelMessages.MessagesWithMetadata.Companion companion3 = WidgetChatListModelMessages.MessagesWithMetadata.Companion;
                            m.checkNotNullExpressionValue(list, "pinnedMessages");
                            return Observable.c(observeMeId, companion3.get(list), companion2.getGuilds().observeRoles(Channel.this.f()), companion2.getGuilds().observeComputed(Channel.this.f()), companion2.getChannels().observeNames(), StoreUserSettings.observeIsAnimatedEmojisEnabled$default(companion2.getUserSettings(), false, 1, null), StoreUserSettings.observeIsAutoPlayGifsEnabled$default(companion2.getUserSettings(), false, 1, null), companion2.getGuilds().observeGuild(Channel.this.f()), InviteEmbedModel.Companion.observe$default(InviteEmbedModel.Companion, null, null, null, null, 15, null), new Func9<Long, WidgetChatListModelMessages.MessagesWithMetadata, Map<Long, ? extends GuildRole>, Map<Long, ? extends GuildMember>, Map<Long, ? extends String>, Boolean, Boolean, Guild, InviteEmbedModel, WidgetChannelPinnedMessages.Model>() { // from class: com.discord.widgets.chat.pins.WidgetChannelPinnedMessages$Model$Companion$get$1.1
                                @Override // rx.functions.Func9
                                public /* bridge */ /* synthetic */ WidgetChannelPinnedMessages.Model call(Long l, WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata, Map<Long, ? extends GuildRole> map, Map<Long, ? extends GuildMember> map2, Map<Long, ? extends String> map3, Boolean bool, Boolean bool2, Guild guild, InviteEmbedModel inviteEmbedModel) {
                                    return call2(l, messagesWithMetadata, (Map<Long, GuildRole>) map, (Map<Long, GuildMember>) map2, (Map<Long, String>) map3, bool, bool2, guild, inviteEmbedModel);
                                }

                                /* renamed from: call  reason: avoid collision after fix types in other method */
                                public final WidgetChannelPinnedMessages.Model call2(Long l, WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata, Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Map<Long, String> map3, Boolean bool, Boolean bool2, Guild guild, InviteEmbedModel inviteEmbedModel) {
                                    ArrayList arrayList;
                                    String str;
                                    Map<Long, GuildMember> map4;
                                    GuildMember guildMember;
                                    List<Long> roles;
                                    List listOf;
                                    Map<Long, GuildMember> map5 = map2;
                                    List list2 = list;
                                    String str2 = "meId";
                                    if (list2 == null) {
                                        listOf = d0.t.m.listOf(new LoadingEntry());
                                    } else if (list2.isEmpty()) {
                                        WidgetChannelPinnedMessages$Model$Companion$get$1 widgetChannelPinnedMessages$Model$Companion$get$1 = WidgetChannelPinnedMessages$Model$Companion$get$1.this;
                                        String string = context.getString(ChannelUtils.x(Channel.this) ? R.string.no_pins_in_dm : R.string.no_pins_in_channel);
                                        m.checkNotNullExpressionValue(string, "context.getString(\n     …                        )");
                                        listOf = d0.t.m.listOf(new EmptyPinsEntry(string));
                                    } else {
                                        ArrayList arrayList2 = new ArrayList();
                                        int size = list.size();
                                        int i = 0;
                                        while (i < size) {
                                            Message message = (Message) list.get(i);
                                            WidgetChatListModelMessages.Companion companion4 = WidgetChatListModelMessages.Companion;
                                            Channel channel = Channel.this;
                                            m.checkNotNullExpressionValue(map5, "guildMembers");
                                            m.checkNotNullExpressionValue(map, "guildRoles");
                                            HashMap hashMap = new HashMap();
                                            int i2 = i;
                                            size = size;
                                            m.checkNotNullExpressionValue(bool, "allowAnimateEmojis");
                                            boolean booleanValue = bool.booleanValue();
                                            ArrayList arrayList3 = arrayList2;
                                            m.checkNotNullExpressionValue(bool2, "autoPlayGifs");
                                            boolean booleanValue2 = bool2.booleanValue();
                                            boolean isRenderEmbedsEnabled = StoreStream.Companion.getUserSettings().getIsRenderEmbedsEnabled();
                                            m.checkNotNullExpressionValue(l, str2);
                                            long longValue = l.longValue();
                                            HashMap hashMap2 = new HashMap();
                                            m.checkNotNullExpressionValue(inviteEmbedModel, "inviteEmbedModel");
                                            str2 = str2;
                                            arrayList3.addAll(WidgetChatListModelMessages.Companion.getMessageItems$default(companion4, channel, map2, map, new HashMap(), messagesWithMetadata.getMessageThreads().get(Long.valueOf(message.getId())), messagesWithMetadata.getThreadCountsAndLatestMessages().get(Long.valueOf(message.getId())), message, messagesWithMetadata.getMessageState().get(Long.valueOf(message.getId())), hashMap, false, false, null, booleanValue, booleanValue2, isRenderEmbedsEnabled, longValue, false, hashMap2, inviteEmbedModel, false, 524288, null));
                                            if (i2 < list.size() - 1) {
                                                arrayList3.add(new DividerEntry());
                                            }
                                            i = i2 + 1;
                                            map5 = map2;
                                            arrayList2 = arrayList3;
                                        }
                                        str = str2;
                                        arrayList = arrayList2;
                                        map4 = map2;
                                        guildMember = map4.get(l);
                                        if (guildMember != null || (roles = guildMember.getRoles()) == null || (r2 = u.toHashSet(roles)) == null) {
                                            Set set = n0.emptySet();
                                        }
                                        Channel channel2 = Channel.this;
                                        m.checkNotNullExpressionValue(l, str);
                                        long longValue2 = l.longValue();
                                        m.checkNotNullExpressionValue(map3, "channelNames");
                                        return new WidgetChannelPinnedMessages.Model(channel2, guild, longValue2, map3, arrayList, set, 0L, 0L, 0L, 0L, false, 1984, null);
                                    }
                                    arrayList = listOf;
                                    str = str2;
                                    map4 = map5;
                                    guildMember = map4.get(l);
                                    if (guildMember != null) {
                                    }
                                    Set set2 = n0.emptySet();
                                    Channel channel22 = Channel.this;
                                    m.checkNotNullExpressionValue(l, str);
                                    long longValue22 = l.longValue();
                                    m.checkNotNullExpressionValue(map3, "channelNames");
                                    return new WidgetChannelPinnedMessages.Model(channel22, guild, longValue22, map3, arrayList, set2, 0L, 0L, 0L, 0L, false, 1984, null);
                                }
                            });
                        }
                    });
                    m.checkNotNullExpressionValue(Y, "getPinnedMessages().obse…              }\n        }");
                    return Y;
                }
                k kVar = new k(null);
                m.checkNotNullExpressionValue(kVar, "Observable.just(null)");
                return kVar;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(Channel channel, Guild guild, long j, Map<Long, String> map, List<? extends ChatListEntry> list, Set<Long> set, long j2, long j3, long j4, long j5, boolean z2) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "channelNames");
            m.checkNotNullParameter(list, "list");
            m.checkNotNullParameter(set, "myRoleIds");
            this.channel = channel;
            this.guild = guild;
            this.userId = j;
            this.channelNames = map;
            this.list = list;
            this.myRoleIds = set;
            this.channelId = j2;
            this.guildId = j3;
            this.oldestMessageId = j4;
            this.newMessagesMarkerMessageId = j5;
            this.isSpoilerClickAllowed = z2;
        }

        public final Channel component1() {
            return this.channel;
        }

        public final long component10() {
            return getNewMessagesMarkerMessageId();
        }

        public final boolean component11() {
            return isSpoilerClickAllowed();
        }

        public final Guild component2() {
            return getGuild();
        }

        public final long component3() {
            return getUserId();
        }

        public final Map<Long, String> component4() {
            return getChannelNames();
        }

        public final List<ChatListEntry> component5() {
            return getList();
        }

        public final Set<Long> component6() {
            return getMyRoleIds();
        }

        public final long component7() {
            return getChannelId();
        }

        public final long component8() {
            return getGuildId();
        }

        public final long component9() {
            return getOldestMessageId();
        }

        public final Model copy(Channel channel, Guild guild, long j, Map<Long, String> map, List<? extends ChatListEntry> list, Set<Long> set, long j2, long j3, long j4, long j5, boolean z2) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "channelNames");
            m.checkNotNullParameter(list, "list");
            m.checkNotNullParameter(set, "myRoleIds");
            return new Model(channel, guild, j, map, list, set, j2, j3, j4, j5, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.channel, model.channel) && m.areEqual(getGuild(), model.getGuild()) && getUserId() == model.getUserId() && m.areEqual(getChannelNames(), model.getChannelNames()) && m.areEqual(getList(), model.getList()) && m.areEqual(getMyRoleIds(), model.getMyRoleIds()) && getChannelId() == model.getChannelId() && getGuildId() == model.getGuildId() && getOldestMessageId() == model.getOldestMessageId() && getNewMessagesMarkerMessageId() == model.getNewMessagesMarkerMessageId() && isSpoilerClickAllowed() == model.isSpoilerClickAllowed();
        }

        public final Channel getChannel() {
            return this.channel;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getChannelId() {
            return this.channelId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Map<Long, String> getChannelNames() {
            return this.channelNames;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Guild getGuild() {
            return this.guild;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getGuildId() {
            return this.guildId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public List<ChatListEntry> getList() {
            return this.list;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Set<Long> getMyRoleIds() {
            return this.myRoleIds;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getNewMessagesMarkerMessageId() {
            return this.newMessagesMarkerMessageId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getOldestMessageId() {
            return this.oldestMessageId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getUserId() {
            return this.userId;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            Guild guild = getGuild();
            int a = (a0.a.a.b.a(getUserId()) + ((hashCode + (guild != null ? guild.hashCode() : 0)) * 31)) * 31;
            Map<Long, String> channelNames = getChannelNames();
            int hashCode2 = (a + (channelNames != null ? channelNames.hashCode() : 0)) * 31;
            List<ChatListEntry> list = getList();
            int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
            Set<Long> myRoleIds = getMyRoleIds();
            if (myRoleIds != null) {
                i = myRoleIds.hashCode();
            }
            int a2 = a0.a.a.b.a(getChannelId());
            int a3 = a0.a.a.b.a(getGuildId());
            int a4 = (a0.a.a.b.a(getNewMessagesMarkerMessageId()) + ((a0.a.a.b.a(getOldestMessageId()) + ((a3 + ((a2 + ((hashCode3 + i) * 31)) * 31)) * 31)) * 31)) * 31;
            boolean isSpoilerClickAllowed = isSpoilerClickAllowed();
            if (isSpoilerClickAllowed) {
                isSpoilerClickAllowed = true;
            }
            int i2 = isSpoilerClickAllowed ? 1 : 0;
            int i3 = isSpoilerClickAllowed ? 1 : 0;
            return a4 + i2;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public boolean isSpoilerClickAllowed() {
            return this.isSpoilerClickAllowed;
        }

        public String toString() {
            StringBuilder R = a.R("Model(channel=");
            R.append(this.channel);
            R.append(", guild=");
            R.append(getGuild());
            R.append(", userId=");
            R.append(getUserId());
            R.append(", channelNames=");
            R.append(getChannelNames());
            R.append(", list=");
            R.append(getList());
            R.append(", myRoleIds=");
            R.append(getMyRoleIds());
            R.append(", channelId=");
            R.append(getChannelId());
            R.append(", guildId=");
            R.append(getGuildId());
            R.append(", oldestMessageId=");
            R.append(getOldestMessageId());
            R.append(", newMessagesMarkerMessageId=");
            R.append(getNewMessagesMarkerMessageId());
            R.append(", isSpoilerClickAllowed=");
            R.append(isSpoilerClickAllowed());
            R.append(")");
            return R.toString();
        }

        public /* synthetic */ Model(Channel channel, Guild guild, long j, Map map, List list, Set set, long j2, long j3, long j4, long j5, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(channel, guild, j, map, list, set, (i & 64) != 0 ? channel.h() : j2, (i & 128) != 0 ? channel.f() : j3, (i & 256) != 0 ? 0L : j4, (i & 512) != 0 ? 0L : j5, (i & 1024) != 0 ? false : z2);
        }
    }

    public WidgetChannelPinnedMessages() {
        super(R.layout.widget_channel_pinned_messages);
    }

    private final void addThreadSpineItemDecoration(WidgetChatListAdapter widgetChatListAdapter) {
        getBinding().f2248b.addItemDecoration(new ThreadSpineItemDecoration(requireContext(), widgetChatListAdapter));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        if (model == null) {
            requireActivity().finish();
            return;
        }
        setActionBarSubtitle(ChannelUtils.e(model.getChannel(), requireContext(), false, 2));
        WidgetChatListAdapter widgetChatListAdapter = this.adapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.setData(model);
        }
    }

    private final WidgetChannelPinnedMessagesBinding getBinding() {
        return (WidgetChannelPinnedMessagesBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        WidgetChatListAdapter widgetChatListAdapter = this.adapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.dispose();
        }
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        WidgetChatListAdapter widgetChatListAdapter = this.adapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.disposeHandlers();
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.pinned_messages);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2248b;
        m.checkNotNullExpressionValue(recyclerView, "binding.channelPinnedMessagesRecycler");
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        FragmentManager parentFragmentManager2 = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
        WidgetChatListAdapter widgetChatListAdapter = (WidgetChatListAdapter) companion.configure(new WidgetChatListAdapter(recyclerView, this, parentFragmentManager, new ChannelPinnedMessagesAdapterEventHandler(requireContext, parentFragmentManager2, ChannelSelector.Companion.getInstance()), null, null, 48, null));
        addThreadSpineItemDecoration(widgetChatListAdapter);
        this.adapter = widgetChatListAdapter;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<Model> q = Model.Companion.get(requireContext(), getMostRecentIntent().getLongExtra(INTENT_EXTRA_CHANNEL_ID, 0L)).q();
        m.checkNotNullExpressionValue(q, "Model\n        .get(requi…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), WidgetChannelPinnedMessages.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelPinnedMessages$onViewBoundOrOnResume$1(this));
        WidgetChatListAdapter widgetChatListAdapter = this.adapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.setHandlers();
        }
    }
}
