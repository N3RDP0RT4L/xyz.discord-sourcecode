package com.discord.widgets.chat;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.pm.ShortcutManager;
import android.os.Build;
import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.core.content.pm.ShortcutManagerCompat;
import b.d.b.a.a;
import com.discord.api.message.MessageReference;
import com.discord.api.message.allowedmentions.MessageAllowedMentions;
import com.discord.api.message.allowedmentions.MessageAllowedMentionsTypes;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.user.User;
import com.discord.models.guild.Guild;
import com.discord.models.message.Message;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreMessageReplies;
import com.discord.stores.StoreMessages;
import com.discord.stores.StorePendingReplies;
import com.discord.stores.StoreSlowMode;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.error.Error;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.rest.SendUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.lytefast.flexinput.model.Attachment;
import d0.t.k;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: MessageManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Þ\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0004VWXYB_\u0012\u0006\u0010>\u001a\u00020=\u0012\b\b\u0002\u0010L\u001a\u00020K\u0012\b\b\u0002\u0010A\u001a\u00020@\u0012\b\b\u0002\u0010D\u001a\u00020C\u0012\b\b\u0002\u0010O\u001a\u00020N\u0012\b\b\u0002\u0010;\u001a\u00020:\u0012\b\b\u0002\u0010I\u001a\u00020H\u0012\b\b\u0002\u00108\u001a\u000207\u0012\b\b\u0002\u0010R\u001a\u00020Q¢\u0006\u0004\bT\u0010UJA\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J)\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\n\u0010\u0017\u001a\u00060\u0015j\u0002`\u00162\n\u0010\u0019\u001a\u00060\u0015j\u0002`\u0018H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ³\u0001\u0010'\u001a\u00020\t2\b\b\u0002\u0010\u0003\u001a\u00020\u00022\u0010\b\u0002\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u00042\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0010\b\u0002\u0010\u0017\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u00162\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\b\b\u0002\u0010\u001f\u001a\u00020\t2\u001c\b\u0002\u0010\"\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020!\u0018\u00010 2\u001c\b\u0002\u0010#\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020!\u0018\u00010 2\u0014\b\u0002\u0010&\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020!0$¢\u0006\u0004\b'\u0010(JW\u0010)\u001a\u00020\t2\n\u0010\u0019\u001a\u00060\u0015j\u0002`\u00182\n\u0010\u0017\u001a\u00060\u0015j\u0002`\u00162\u0006\u0010\u0003\u001a\u00020\u00022\u001c\b\u0002\u0010\"\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020!\u0018\u00010 2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b)\u0010*J\u0087\u0001\u00105\u001a\u00020\t2\n\u0010\u0017\u001a\u00060\u0015j\u0002`\u00162\u000e\u0010,\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`+2\b\u0010-\u001a\u0004\u0018\u00010\u00022\u0006\u0010/\u001a\u00020.2\n\b\u0002\u00100\u001a\u0004\u0018\u00010\u00102\f\u00102\u001a\b\u0012\u0004\u0012\u00020!012\u0012\u00104\u001a\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020!0$2\u001c\b\u0002\u0010#\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020!\u0018\u00010 ¢\u0006\u0004\b5\u00106R\u0016\u00108\u001a\u0002078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109R\u0016\u0010;\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010A\u001a\u00020@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010ER\"\u0010F\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020!0$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u0016\u0010I\u001a\u00020H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010JR\u0016\u0010L\u001a\u00020K8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010MR\u0016\u0010O\u001a\u00020N8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bO\u0010PR\u0016\u0010R\u001a\u00020Q8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bR\u0010S¨\u0006Z"}, d2 = {"Lcom/discord/widgets/chat/MessageManager;", "", "", "content", "", "Lcom/discord/api/sticker/BaseSticker;", "stickers", "Lcom/discord/models/user/MeUser;", "meUser", "", "isEditing", "", "previousMessageLength", "Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;", "validateMessageContent", "(Ljava/lang/String;Ljava/util/List;Lcom/discord/models/user/MeUser;ZLjava/lang/Integer;)Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;", "Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;", "attachmentsRequest", "Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;", "validateAttachments", "(Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;)Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "messageId", "Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "getAllowedMentionsForMessageEdit", "(JJ)Lcom/discord/api/message/allowedmentions/MessageAllowedMentions;", "Lcom/discord/models/user/User;", "mentions", "consumePendingReply", "Lkotlin/Function2;", "", "onMessageTooLong", "onFilesTooLarge", "Lkotlin/Function1;", "Lcom/discord/widgets/chat/MessageManager$MessageSendResult;", "messageSendResultHandler", "sendMessage", "(Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Ljava/lang/Long;Ljava/util/List;ZLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)Z", "editMessage", "(JJLjava/lang/String;Lkotlin/jvm/functions/Function2;Ljava/lang/Integer;)Z", "Lcom/discord/primitives/GuildId;", "guildId", "version", "Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "data", "attachmentRequest", "Lkotlin/Function0;", "onSuccess", "Lcom/discord/utilities/error/Error;", "onFail", "sendCommand", "(JLjava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/chat/input/models/ApplicationCommandData;Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)Z", "Lcom/discord/stores/StorePendingReplies;", "storePendingReplies", "Lcom/discord/stores/StorePendingReplies;", "Lcom/discord/stores/StoreSlowMode;", "storeSlowMode", "Lcom/discord/stores/StoreSlowMode;", "Landroid/content/Context;", "context", "Landroid/content/Context;", "Lcom/discord/stores/StoreApplicationInteractions;", "storeApplicationInteractions", "Lcom/discord/stores/StoreApplicationInteractions;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "defaultMessageResultHandler", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreMessages;", "storeMessages", "Lcom/discord/stores/StoreMessages;", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreChannelsSelected;", "Lcom/discord/stores/StoreMessageReplies;", "storeMessageReplies", "Lcom/discord/stores/StoreMessageReplies;", HookHelper.constructorName, "(Landroid/content/Context;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreApplicationInteractions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/stores/StoreMessageReplies;)V", "AttachmentValidationResult", "AttachmentsRequest", "ContentValidationResult", "MessageSendResult", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageManager {
    private final Context context;
    private final Function1<MessageSendResult, Unit> defaultMessageResultHandler;
    private final StoreApplicationInteractions storeApplicationInteractions;
    private final StoreChannelsSelected storeChannelsSelected;
    private final StoreGuilds storeGuilds;
    private final StoreMessageReplies storeMessageReplies;
    private final StoreMessages storeMessages;
    private final StorePendingReplies storePendingReplies;
    private final StoreSlowMode storeSlowMode;
    private final StoreUser storeUser;

    /* compiled from: MessageManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;", "", HookHelper.constructorName, "()V", "EmptyAttachments", "FilesTooLarge", "Success", "Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$EmptyAttachments;", "Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$FilesTooLarge;", "Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$Success;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class AttachmentValidationResult {

        /* compiled from: MessageManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$EmptyAttachments;", "Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmptyAttachments extends AttachmentValidationResult {
            public static final EmptyAttachments INSTANCE = new EmptyAttachments();

            private EmptyAttachments() {
                super(null);
            }
        }

        /* compiled from: MessageManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$FilesTooLarge;", "Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;", "Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;", "component1", "()Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;", "attachmentsRequest", "copy", "(Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;)Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$FilesTooLarge;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;", "getAttachmentsRequest", HookHelper.constructorName, "(Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class FilesTooLarge extends AttachmentValidationResult {
            private final AttachmentsRequest attachmentsRequest;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public FilesTooLarge(AttachmentsRequest attachmentsRequest) {
                super(null);
                m.checkNotNullParameter(attachmentsRequest, "attachmentsRequest");
                this.attachmentsRequest = attachmentsRequest;
            }

            public static /* synthetic */ FilesTooLarge copy$default(FilesTooLarge filesTooLarge, AttachmentsRequest attachmentsRequest, int i, Object obj) {
                if ((i & 1) != 0) {
                    attachmentsRequest = filesTooLarge.attachmentsRequest;
                }
                return filesTooLarge.copy(attachmentsRequest);
            }

            public final AttachmentsRequest component1() {
                return this.attachmentsRequest;
            }

            public final FilesTooLarge copy(AttachmentsRequest attachmentsRequest) {
                m.checkNotNullParameter(attachmentsRequest, "attachmentsRequest");
                return new FilesTooLarge(attachmentsRequest);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof FilesTooLarge) && m.areEqual(this.attachmentsRequest, ((FilesTooLarge) obj).attachmentsRequest);
                }
                return true;
            }

            public final AttachmentsRequest getAttachmentsRequest() {
                return this.attachmentsRequest;
            }

            public int hashCode() {
                AttachmentsRequest attachmentsRequest = this.attachmentsRequest;
                if (attachmentsRequest != null) {
                    return attachmentsRequest.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("FilesTooLarge(attachmentsRequest=");
                R.append(this.attachmentsRequest);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: MessageManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult$Success;", "Lcom/discord/widgets/chat/MessageManager$AttachmentValidationResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Success extends AttachmentValidationResult {
            public static final Success INSTANCE = new Success();

            private Success() {
                super(null);
            }
        }

        private AttachmentValidationResult() {
        }

        public /* synthetic */ AttachmentValidationResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: MessageManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0014\b\u0002\u0010\t\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b\u0018\u00010\u0007¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R%\u0010\t\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\u000e\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$AttachmentsRequest;", "", "", "currentFileSizeMB", "F", "getCurrentFileSizeMB", "()F", "", "Lcom/lytefast/flexinput/model/Attachment;", "attachments", "Ljava/util/List;", "getAttachments", "()Ljava/util/List;", "", "maxFileSizeMB", "I", "getMaxFileSizeMB", "()I", HookHelper.constructorName, "(FILjava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AttachmentsRequest {
        private final List<Attachment<?>> attachments;
        private final float currentFileSizeMB;
        private final int maxFileSizeMB;

        /* JADX WARN: Multi-variable type inference failed */
        public AttachmentsRequest(float f, int i, List<? extends Attachment<?>> list) {
            this.currentFileSizeMB = f;
            this.maxFileSizeMB = i;
            this.attachments = list;
        }

        public final List<Attachment<?>> getAttachments() {
            return this.attachments;
        }

        public final float getCurrentFileSizeMB() {
            return this.currentFileSizeMB;
        }

        public final int getMaxFileSizeMB() {
            return this.maxFileSizeMB;
        }

        public /* synthetic */ AttachmentsRequest(float f, int i, List list, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(f, i, (i2 & 4) != 0 ? null : list);
        }
    }

    /* compiled from: MessageManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;", "", HookHelper.constructorName, "()V", "EmptyContent", "MessageTooLong", "Success", "Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$EmptyContent;", "Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;", "Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$Success;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ContentValidationResult {

        /* compiled from: MessageManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$EmptyContent;", "Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmptyContent extends ContentValidationResult {
            public static final EmptyContent INSTANCE = new EmptyContent();

            private EmptyContent() {
                super(null);
            }
        }

        /* compiled from: MessageManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;", "Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;", "", "component1", "()I", "maxMessageLength", "copy", "(I)Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$MessageTooLong;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMaxMessageLength", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class MessageTooLong extends ContentValidationResult {
            private final int maxMessageLength;

            public MessageTooLong(int i) {
                super(null);
                this.maxMessageLength = i;
            }

            public static /* synthetic */ MessageTooLong copy$default(MessageTooLong messageTooLong, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = messageTooLong.maxMessageLength;
                }
                return messageTooLong.copy(i);
            }

            public final int component1() {
                return this.maxMessageLength;
            }

            public final MessageTooLong copy(int i) {
                return new MessageTooLong(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof MessageTooLong) && this.maxMessageLength == ((MessageTooLong) obj).maxMessageLength;
                }
                return true;
            }

            public final int getMaxMessageLength() {
                return this.maxMessageLength;
            }

            public int hashCode() {
                return this.maxMessageLength;
            }

            public String toString() {
                return a.A(a.R("MessageTooLong(maxMessageLength="), this.maxMessageLength, ")");
            }
        }

        /* compiled from: MessageManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$ContentValidationResult$Success;", "Lcom/discord/widgets/chat/MessageManager$ContentValidationResult;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Success extends ContentValidationResult {
            public static final Success INSTANCE = new Success();

            private Success() {
                super(null);
            }
        }

        private ContentValidationResult() {
        }

        public /* synthetic */ ContentValidationResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: MessageManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$MessageSendResult;", "", "Lcom/discord/utilities/messagesend/MessageResult;", "component1", "()Lcom/discord/utilities/messagesend/MessageResult;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "messageResult", "guild", "copy", "(Lcom/discord/utilities/messagesend/MessageResult;Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/chat/MessageManager$MessageSendResult;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/messagesend/MessageResult;", "getMessageResult", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/utilities/messagesend/MessageResult;Lcom/discord/models/guild/Guild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MessageSendResult {
        private final Guild guild;
        private final MessageResult messageResult;

        public MessageSendResult(MessageResult messageResult, Guild guild) {
            m.checkNotNullParameter(messageResult, "messageResult");
            this.messageResult = messageResult;
            this.guild = guild;
        }

        public static /* synthetic */ MessageSendResult copy$default(MessageSendResult messageSendResult, MessageResult messageResult, Guild guild, int i, Object obj) {
            if ((i & 1) != 0) {
                messageResult = messageSendResult.messageResult;
            }
            if ((i & 2) != 0) {
                guild = messageSendResult.guild;
            }
            return messageSendResult.copy(messageResult, guild);
        }

        public final MessageResult component1() {
            return this.messageResult;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final MessageSendResult copy(MessageResult messageResult, Guild guild) {
            m.checkNotNullParameter(messageResult, "messageResult");
            return new MessageSendResult(messageResult, guild);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MessageSendResult)) {
                return false;
            }
            MessageSendResult messageSendResult = (MessageSendResult) obj;
            return m.areEqual(this.messageResult, messageSendResult.messageResult) && m.areEqual(this.guild, messageSendResult.guild);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final MessageResult getMessageResult() {
            return this.messageResult;
        }

        public int hashCode() {
            MessageResult messageResult = this.messageResult;
            int i = 0;
            int hashCode = (messageResult != null ? messageResult.hashCode() : 0) * 31;
            Guild guild = this.guild;
            if (guild != null) {
                i = guild.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("MessageSendResult(messageResult=");
            R.append(this.messageResult);
            R.append(", guild=");
            R.append(this.guild);
            R.append(")");
            return R.toString();
        }
    }

    public MessageManager(Context context, StoreMessages storeMessages, StoreApplicationInteractions storeApplicationInteractions, StoreUser storeUser, StoreChannelsSelected storeChannelsSelected, StoreSlowMode storeSlowMode, StoreGuilds storeGuilds, StorePendingReplies storePendingReplies, StoreMessageReplies storeMessageReplies) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(storeMessages, "storeMessages");
        m.checkNotNullParameter(storeApplicationInteractions, "storeApplicationInteractions");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeChannelsSelected, "storeChannelsSelected");
        m.checkNotNullParameter(storeSlowMode, "storeSlowMode");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storePendingReplies, "storePendingReplies");
        m.checkNotNullParameter(storeMessageReplies, "storeMessageReplies");
        this.context = context;
        this.storeMessages = storeMessages;
        this.storeApplicationInteractions = storeApplicationInteractions;
        this.storeUser = storeUser;
        this.storeChannelsSelected = storeChannelsSelected;
        this.storeSlowMode = storeSlowMode;
        this.storeGuilds = storeGuilds;
        this.storePendingReplies = storePendingReplies;
        this.storeMessageReplies = storeMessageReplies;
        this.defaultMessageResultHandler = new MessageManager$defaultMessageResultHandler$1(this);
    }

    private final MessageAllowedMentions getAllowedMentionsForMessageEdit(long j, long j2) {
        Integer type;
        MessageReference messageReference;
        Long c;
        boolean z2;
        Message message = this.storeMessages.getMessage(j, j2);
        if (!(message == null || (type = message.getType()) == null || type.intValue() != 19 || (messageReference = message.getMessageReference()) == null || (c = messageReference.c()) == null)) {
            StoreMessageReplies.MessageState messageState = this.storeMessageReplies.getAllMessageReferences().get(Long.valueOf(c.longValue()));
            if (messageState == null || !(messageState instanceof StoreMessageReplies.MessageState.Loaded)) {
                return null;
            }
            List<User> mentions = message.getMentions();
            if (mentions != null) {
                boolean z3 = false;
                if (!mentions.isEmpty()) {
                    Iterator<T> it = mentions.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        long i = ((User) it.next()).i();
                        User author = ((StoreMessageReplies.MessageState.Loaded) messageState).getMessage().getAuthor();
                        if (author == null || i != author.i()) {
                            z2 = false;
                            continue;
                        } else {
                            z2 = true;
                            continue;
                        }
                        if (z2) {
                            z3 = true;
                            break;
                        }
                    }
                }
                if (z3) {
                    return null;
                }
            }
            return new MessageAllowedMentions(k.toList(MessageAllowedMentionsTypes.values()), null, null, Boolean.FALSE, 6);
        }
        return null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ boolean sendMessage$default(MessageManager messageManager, String str, List list, AttachmentsRequest attachmentsRequest, Long l, List list2, boolean z2, Function2 function2, Function2 function22, Function1 function1, int i, Object obj) {
        String str2 = (i & 1) != 0 ? "" : str;
        Function2<? super Integer, ? super Boolean, Unit> function23 = null;
        List list3 = (i & 2) != 0 ? null : list;
        AttachmentsRequest attachmentsRequest2 = (i & 4) != 0 ? null : attachmentsRequest;
        Long l2 = (i & 8) != 0 ? null : l;
        List<? extends BaseSticker> emptyList = (i & 16) != 0 ? n.emptyList() : list2;
        boolean z3 = (i & 32) != 0 ? true : z2;
        Function2 function24 = (i & 64) != 0 ? null : function2;
        if ((i & 128) == 0) {
            function23 = function22;
        }
        return messageManager.sendMessage(str2, list3, attachmentsRequest2, l2, emptyList, z3, function24, function23, (i & 256) != 0 ? messageManager.defaultMessageResultHandler : function1);
    }

    private final AttachmentValidationResult validateAttachments(AttachmentsRequest attachmentsRequest) {
        if (attachmentsRequest == null) {
            return AttachmentValidationResult.EmptyAttachments.INSTANCE;
        }
        List<Attachment<?>> attachments = attachmentsRequest.getAttachments();
        if (attachments == null || attachments.isEmpty()) {
            return AttachmentValidationResult.EmptyAttachments.INSTANCE;
        }
        if (attachmentsRequest.getCurrentFileSizeMB() >= attachmentsRequest.getMaxFileSizeMB()) {
            return new AttachmentValidationResult.FilesTooLarge(attachmentsRequest);
        }
        return AttachmentValidationResult.Success.INSTANCE;
    }

    private final ContentValidationResult validateMessageContent(String str, List<? extends BaseSticker> list, MeUser meUser, boolean z2, Integer num) {
        int i = 2000;
        boolean z3 = true;
        boolean z4 = z2 && num != null && num.intValue() > 2000;
        if (UserUtils.INSTANCE.isPremiumTier2(meUser) || z4) {
            i = SendUtils.MAX_MESSAGE_CHARACTER_COUNT_PREMIUM;
        }
        if (str.length() == 0) {
            if (list != null && !list.isEmpty()) {
                z3 = false;
            }
            if (z3) {
                return ContentValidationResult.EmptyContent.INSTANCE;
            }
        }
        return str.length() > i ? new ContentValidationResult.MessageTooLong(i) : ContentValidationResult.Success.INSTANCE;
    }

    public static /* synthetic */ ContentValidationResult validateMessageContent$default(MessageManager messageManager, String str, List list, MeUser meUser, boolean z2, Integer num, int i, Object obj) {
        if ((i & 16) != 0) {
            num = null;
        }
        return messageManager.validateMessageContent(str, list, meUser, z2, num);
    }

    public final boolean editMessage(long j, long j2, String str, Function2<? super Integer, ? super Integer, Unit> function2, Integer num) {
        m.checkNotNullParameter(str, "content");
        ContentValidationResult validateMessageContent = validateMessageContent(str, n.emptyList(), this.storeUser.getMe(), true, num);
        if (validateMessageContent instanceof ContentValidationResult.MessageTooLong) {
            if (function2 != null) {
                function2.invoke(Integer.valueOf(str.length()), Integer.valueOf(((ContentValidationResult.MessageTooLong) validateMessageContent).getMaxMessageLength()));
            }
            return false;
        } else if (m.areEqual(validateMessageContent, ContentValidationResult.EmptyContent.INSTANCE)) {
            return false;
        } else {
            m.areEqual(validateMessageContent, ContentValidationResult.Success.INSTANCE);
            this.storeMessages.editMessage(j, j2, str, getAllowedMentionsForMessageEdit(j2, j));
            return true;
        }
    }

    public final boolean sendCommand(long j, Long l, String str, ApplicationCommandData applicationCommandData, AttachmentsRequest attachmentsRequest, Function0<Unit> function0, Function1<? super Error, Unit> function1, Function2<? super Integer, ? super Boolean, Unit> function2) {
        m.checkNotNullParameter(applicationCommandData, "data");
        m.checkNotNullParameter(function0, "onSuccess");
        m.checkNotNullParameter(function1, "onFail");
        MeUser me2 = this.storeUser.getMe();
        AttachmentValidationResult validateAttachments = validateAttachments(attachmentsRequest);
        if (validateAttachments instanceof AttachmentValidationResult.FilesTooLarge) {
            AttachmentsRequest attachmentsRequest2 = ((AttachmentValidationResult.FilesTooLarge) validateAttachments).getAttachmentsRequest();
            if (function2 == null) {
                return false;
            }
            function2.invoke(Integer.valueOf(attachmentsRequest2.getMaxFileSizeMB()), Boolean.valueOf(UserUtils.INSTANCE.isPremium(me2)));
            return false;
        }
        this.storeApplicationInteractions.sendApplicationCommand(j, l, str, applicationCommandData, attachmentsRequest != null ? attachmentsRequest.getAttachments() : null, MessageManager$sendCommand$1.INSTANCE, MessageManager$sendCommand$2.INSTANCE);
        return true;
    }

    public final boolean sendMessage(final String str, final List<? extends com.discord.models.user.User> list, AttachmentsRequest attachmentsRequest, Long l, final List<? extends BaseSticker> list2, boolean z2, Function2<? super Integer, ? super Integer, Unit> function2, Function2<? super Integer, ? super Boolean, Unit> function22, Function1<? super MessageSendResult, Unit> function1) {
        long j;
        ShortcutManager shortcutManager;
        m.checkNotNullParameter(str, "content");
        m.checkNotNullParameter(list2, "stickers");
        m.checkNotNullParameter(function1, "messageSendResultHandler");
        final MeUser me2 = this.storeUser.getMe();
        ContentValidationResult validateMessageContent$default = validateMessageContent$default(this, str, list2, me2, false, null, 16, null);
        if (validateMessageContent$default instanceof ContentValidationResult.MessageTooLong) {
            if (function2 != null) {
                function2.invoke(Integer.valueOf(str.length()), Integer.valueOf(((ContentValidationResult.MessageTooLong) validateMessageContent$default).getMaxMessageLength()));
            }
            return false;
        }
        AttachmentValidationResult validateAttachments = validateAttachments(attachmentsRequest);
        if (validateAttachments instanceof AttachmentValidationResult.FilesTooLarge) {
            AttachmentsRequest attachmentsRequest2 = ((AttachmentValidationResult.FilesTooLarge) validateAttachments).getAttachmentsRequest();
            if (function22 != null) {
                function22.invoke(Integer.valueOf(attachmentsRequest2.getMaxFileSizeMB()), Boolean.valueOf(UserUtils.INSTANCE.isPremium(me2)));
            }
            return false;
        }
        final List<Attachment<?>> attachments = attachmentsRequest != null ? attachmentsRequest.getAttachments() : null;
        if (l != null) {
            j = l.longValue();
        } else {
            j = this.storeChannelsSelected.getId();
        }
        final long j2 = j;
        if (Build.VERSION.SDK_INT >= 25) {
            List<ShortcutInfoCompat> dynamicShortcuts = ShortcutManagerCompat.getDynamicShortcuts(this.context);
            m.checkNotNullExpressionValue(dynamicShortcuts, "ShortcutManagerCompat.getDynamicShortcuts(context)");
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(dynamicShortcuts, 10));
            for (ShortcutInfoCompat shortcutInfoCompat : dynamicShortcuts) {
                m.checkNotNullExpressionValue(shortcutInfoCompat, "it");
                arrayList.add(shortcutInfoCompat.getId());
            }
            if (u.toSet(arrayList).contains(String.valueOf(j2)) && (shortcutManager = (ShortcutManager) this.context.getSystemService(ShortcutManager.class)) != null) {
                shortcutManager.reportShortcutUsed(String.valueOf(j2));
            }
        }
        final StorePendingReplies.PendingReply pendingReply = z2 ? this.storePendingReplies.getPendingReply(j2) : null;
        final MessageAllowedMentions messageAllowedMentions = (pendingReply == null || pendingReply.getShouldMention()) ? null : new MessageAllowedMentions(k.toList(MessageAllowedMentionsTypes.values()), null, null, Boolean.FALSE, 6);
        Observable Z = Observable.j(this.storeSlowMode.observeCooldownSecs(l, StoreSlowMode.Type.MessageSend.INSTANCE).Z(1).Y(new b<Integer, Observable<? extends MessageResult>>() { // from class: com.discord.widgets.chat.MessageManager$sendMessage$messageResultObservable$1
            public final Observable<? extends MessageResult> call(Integer num) {
                StoreMessages storeMessages;
                StorePendingReplies storePendingReplies;
                if (num.intValue() > 0) {
                    return new j0.l.e.k(new MessageResult.Slowmode(num.intValue() * 1000));
                }
                if (pendingReply != null) {
                    storePendingReplies = MessageManager.this.storePendingReplies;
                    storePendingReplies.onDeletePendingReply(j2);
                }
                storeMessages = MessageManager.this.storeMessages;
                long j3 = j2;
                MeUser meUser = me2;
                String str2 = str;
                List list3 = list;
                List list4 = attachments;
                List list5 = list2;
                StorePendingReplies.PendingReply pendingReply2 = pendingReply;
                return StoreMessages.sendMessage$default(storeMessages, j3, meUser, str2, list3, list4, list5, pendingReply2 != null ? pendingReply2.getMessageReference() : null, messageAllowedMentions, null, null, null, null, null, null, null, 32512, null);
            }
        }), this.storeGuilds.observeFromChannelId(j2), MessageManager$sendMessage$1.INSTANCE).Z(1);
        m.checkNotNullExpressionValue(Z, "Observable.combineLatest…d)\n    }\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(Z, false, 1, null)), MessageManager.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new MessageManager$sendMessage$2(function1));
        return true;
    }

    public /* synthetic */ MessageManager(Context context, StoreMessages storeMessages, StoreApplicationInteractions storeApplicationInteractions, StoreUser storeUser, StoreChannelsSelected storeChannelsSelected, StoreSlowMode storeSlowMode, StoreGuilds storeGuilds, StorePendingReplies storePendingReplies, StoreMessageReplies storeMessageReplies, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? StoreStream.Companion.getMessages() : storeMessages, (i & 4) != 0 ? StoreStream.Companion.getInteractions() : storeApplicationInteractions, (i & 8) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 16) != 0 ? StoreStream.Companion.getChannelsSelected() : storeChannelsSelected, (i & 32) != 0 ? StoreStream.Companion.getSlowMode() : storeSlowMode, (i & 64) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 128) != 0 ? StoreStream.Companion.getPendingReplies() : storePendingReplies, (i & 256) != 0 ? StoreStream.Companion.getRepliedMessages() : storeMessageReplies);
    }
}
