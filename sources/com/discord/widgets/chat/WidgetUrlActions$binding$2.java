package com.discord.widgets.chat;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetUrlActionsBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetUrlActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetUrlActionsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetUrlActionsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetUrlActions$binding$2 extends k implements Function1<View, WidgetUrlActionsBinding> {
    public static final WidgetUrlActions$binding$2 INSTANCE = new WidgetUrlActions$binding$2();

    public WidgetUrlActions$binding$2() {
        super(1, WidgetUrlActionsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetUrlActionsBinding;", 0);
    }

    public final WidgetUrlActionsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.dialog_url_actions_copy;
        TextView textView = (TextView) view.findViewById(R.id.dialog_url_actions_copy);
        if (textView != null) {
            i = R.id.dialog_url_actions_open;
            TextView textView2 = (TextView) view.findViewById(R.id.dialog_url_actions_open);
            if (textView2 != null) {
                i = R.id.dialog_url_actions_share;
                TextView textView3 = (TextView) view.findViewById(R.id.dialog_url_actions_share);
                if (textView3 != null) {
                    i = R.id.dialog_url_actions_url;
                    TextView textView4 = (TextView) view.findViewById(R.id.dialog_url_actions_url);
                    if (textView4 != null) {
                        return new WidgetUrlActionsBinding((LinearLayout) view, textView, textView2, textView3, textView4);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
