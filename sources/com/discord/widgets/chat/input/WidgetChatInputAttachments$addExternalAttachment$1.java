package com.discord.widgets.chat.input;

import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.model.Attachment;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatInputAttachments.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInputAttachments$addExternalAttachment$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Attachment $attachment;
    public final /* synthetic */ WidgetChatInputAttachments this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInputAttachments$addExternalAttachment$1(WidgetChatInputAttachments widgetChatInputAttachments, Attachment attachment) {
        super(0);
        this.this$0 = widgetChatInputAttachments;
        this.$attachment = attachment;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        FlexInputFragment flexInputFragment;
        flexInputFragment = this.this$0.flexInputFragment;
        flexInputFragment.f(this.$attachment);
    }
}
