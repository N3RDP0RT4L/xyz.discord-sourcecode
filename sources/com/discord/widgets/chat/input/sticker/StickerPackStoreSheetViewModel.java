package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.premium.PremiumTier;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppViewModel;
import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.models.user.User;
import com.discord.stores.StoreStickers;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stickers.StickerUtils;
import com.discord.widgets.chat.input.sticker.StickerItem;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: StickerPackStoreSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001a2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001a\u001b\u001cB5\u0012\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010\u0012\b\b\u0002\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00060\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0003¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\n\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\tJ\r\u0010\u000b\u001a\u00020\u0003¢\u0006\u0004\b\u000b\u0010\u0005R\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u001a\u0010\u0011\u001a\u00060\u000fj\u0002`\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;", "", "trackStickerPackStoreSheetViewed", "()V", "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;)V", "handleLoadedStoreState", "fetchStickersData", "Lcom/discord/stores/StoreStickers;", "stickersStore", "Lcom/discord/stores/StoreStickers;", "", "Lcom/discord/primitives/StickerPackId;", "stickerPackId", "J", "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;", "analytics", "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(JLcom/discord/stores/StoreStickers;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerPackStoreSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final StickerPackStoreSheetAnalytics analytics;
    private final long stickerPackId;
    private final StoreStickers stickersStore;

    /* compiled from: StickerPackStoreSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            StickerPackStoreSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: StickerPackStoreSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ-\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\u000bJ9\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\n\u0010\u000e\u001a\u00060\fj\u0002`\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$Companion;", "", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "stickerPack", "", "stickerAnimationSettings", "Lcom/discord/models/user/User;", "meUser", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "buildStoreStickerListItems", "(Lcom/discord/models/sticker/dto/ModelStickerPack;ILcom/discord/models/user/User;)Ljava/util/List;", "", "Lcom/discord/primitives/StickerPackId;", "stickerPackId", "Lcom/discord/stores/StoreStickers;", "storeStickers", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreUser;", "storeUser", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final List<MGRecyclerDataPayload> buildStoreStickerListItems(ModelStickerPack modelStickerPack, int i, User user) {
            ArrayList arrayList = new ArrayList();
            if (modelStickerPack.getStickers().isEmpty()) {
                return n.emptyList();
            }
            arrayList.add(new StoreHeaderItem(modelStickerPack, false));
            for (Sticker sticker : modelStickerPack.getStickers()) {
                arrayList.add(new StickerItem(sticker, i, StickerItem.Mode.STORE, StickerUtils.getStickerSendability$default(StickerUtils.INSTANCE, sticker, user, null, null, 12, null)));
            }
            return arrayList;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(long j, StoreStickers storeStickers, StoreUserSettings storeUserSettings, StoreUser storeUser) {
            Observable<StoreState> i = Observable.i(storeStickers.observeStickerPack(j), StoreUserSettings.observeStickerAnimationSettings$default(storeUserSettings, false, 1, null), StoreUser.observeMe$default(storeUser, false, 1, null), StickerPackStoreSheetViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(i, "Observable.combineLatest…ser\n          )\n        }");
            return i;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StickerPackStoreSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0007J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\n¨\u0006 "}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;", "", "Lcom/discord/stores/StoreStickers$StickerPackState;", "component1", "()Lcom/discord/stores/StoreStickers$StickerPackState;", "", "component2", "()I", "Lcom/discord/models/user/User;", "component3", "()Lcom/discord/models/user/User;", "stickerPack", "stickerAnimationSettings", "meUser", "copy", "(Lcom/discord/stores/StoreStickers$StickerPackState;ILcom/discord/models/user/User;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getStickerAnimationSettings", "Lcom/discord/stores/StoreStickers$StickerPackState;", "getStickerPack", "Lcom/discord/models/user/User;", "getMeUser", HookHelper.constructorName, "(Lcom/discord/stores/StoreStickers$StickerPackState;ILcom/discord/models/user/User;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final User meUser;
        private final int stickerAnimationSettings;
        private final StoreStickers.StickerPackState stickerPack;

        public StoreState(StoreStickers.StickerPackState stickerPackState, int i, User user) {
            m.checkNotNullParameter(stickerPackState, "stickerPack");
            m.checkNotNullParameter(user, "meUser");
            this.stickerPack = stickerPackState;
            this.stickerAnimationSettings = i;
            this.meUser = user;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, StoreStickers.StickerPackState stickerPackState, int i, User user, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                stickerPackState = storeState.stickerPack;
            }
            if ((i2 & 2) != 0) {
                i = storeState.stickerAnimationSettings;
            }
            if ((i2 & 4) != 0) {
                user = storeState.meUser;
            }
            return storeState.copy(stickerPackState, i, user);
        }

        public final StoreStickers.StickerPackState component1() {
            return this.stickerPack;
        }

        public final int component2() {
            return this.stickerAnimationSettings;
        }

        public final User component3() {
            return this.meUser;
        }

        public final StoreState copy(StoreStickers.StickerPackState stickerPackState, int i, User user) {
            m.checkNotNullParameter(stickerPackState, "stickerPack");
            m.checkNotNullParameter(user, "meUser");
            return new StoreState(stickerPackState, i, user);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.stickerPack, storeState.stickerPack) && this.stickerAnimationSettings == storeState.stickerAnimationSettings && m.areEqual(this.meUser, storeState.meUser);
        }

        public final User getMeUser() {
            return this.meUser;
        }

        public final int getStickerAnimationSettings() {
            return this.stickerAnimationSettings;
        }

        public final StoreStickers.StickerPackState getStickerPack() {
            return this.stickerPack;
        }

        public int hashCode() {
            StoreStickers.StickerPackState stickerPackState = this.stickerPack;
            int i = 0;
            int hashCode = (((stickerPackState != null ? stickerPackState.hashCode() : 0) * 31) + this.stickerAnimationSettings) * 31;
            User user = this.meUser;
            if (user != null) {
                i = user.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(stickerPack=");
            R.append(this.stickerPack);
            R.append(", stickerAnimationSettings=");
            R.append(this.stickerAnimationSettings);
            R.append(", meUser=");
            R.append(this.meUser);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: StickerPackStoreSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\u0012\u001a\u00020\f\u0012\u0006\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b&\u0010'J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0004J>\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0010\u001a\u00020\u00052\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\u0012\u001a\u00020\f2\b\b\u0002\u0010\u0013\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001d\u001a\u00020\u00022\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u0019\u0010\u0012\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001f\u001a\u0004\b \u0010\u000eR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\"\u0010\u000bR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010#\u001a\u0004\b\u0013\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010$\u001a\u0004\b%\u0010\u0007¨\u0006("}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;", "", "", "isPremiumTier2", "()Z", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "component1", "()Lcom/discord/models/sticker/dto/ModelStickerPack;", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "component2", "()Ljava/util/List;", "Lcom/discord/api/premium/PremiumTier;", "component3", "()Lcom/discord/api/premium/PremiumTier;", "component4", "stickerPack", "stickerItems", "meUserPremiumTier", "isPackEnabled", "copy", "(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/util/List;Lcom/discord/api/premium/PremiumTier;Z)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/premium/PremiumTier;", "getMeUserPremiumTier", "Ljava/util/List;", "getStickerItems", "Z", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "getStickerPack", HookHelper.constructorName, "(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/util/List;Lcom/discord/api/premium/PremiumTier;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean isPackEnabled;
        private final PremiumTier meUserPremiumTier;
        private final List<MGRecyclerDataPayload> stickerItems;
        private final ModelStickerPack stickerPack;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(ModelStickerPack modelStickerPack, List<? extends MGRecyclerDataPayload> list, PremiumTier premiumTier, boolean z2) {
            m.checkNotNullParameter(modelStickerPack, "stickerPack");
            m.checkNotNullParameter(list, "stickerItems");
            m.checkNotNullParameter(premiumTier, "meUserPremiumTier");
            this.stickerPack = modelStickerPack;
            this.stickerItems = list;
            this.meUserPremiumTier = premiumTier;
            this.isPackEnabled = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, ModelStickerPack modelStickerPack, List list, PremiumTier premiumTier, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                modelStickerPack = viewState.stickerPack;
            }
            if ((i & 2) != 0) {
                list = viewState.stickerItems;
            }
            if ((i & 4) != 0) {
                premiumTier = viewState.meUserPremiumTier;
            }
            if ((i & 8) != 0) {
                z2 = viewState.isPackEnabled;
            }
            return viewState.copy(modelStickerPack, list, premiumTier, z2);
        }

        public final ModelStickerPack component1() {
            return this.stickerPack;
        }

        public final List<MGRecyclerDataPayload> component2() {
            return this.stickerItems;
        }

        public final PremiumTier component3() {
            return this.meUserPremiumTier;
        }

        public final boolean component4() {
            return this.isPackEnabled;
        }

        public final ViewState copy(ModelStickerPack modelStickerPack, List<? extends MGRecyclerDataPayload> list, PremiumTier premiumTier, boolean z2) {
            m.checkNotNullParameter(modelStickerPack, "stickerPack");
            m.checkNotNullParameter(list, "stickerItems");
            m.checkNotNullParameter(premiumTier, "meUserPremiumTier");
            return new ViewState(modelStickerPack, list, premiumTier, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.stickerPack, viewState.stickerPack) && m.areEqual(this.stickerItems, viewState.stickerItems) && m.areEqual(this.meUserPremiumTier, viewState.meUserPremiumTier) && this.isPackEnabled == viewState.isPackEnabled;
        }

        public final PremiumTier getMeUserPremiumTier() {
            return this.meUserPremiumTier;
        }

        public final List<MGRecyclerDataPayload> getStickerItems() {
            return this.stickerItems;
        }

        public final ModelStickerPack getStickerPack() {
            return this.stickerPack;
        }

        public int hashCode() {
            ModelStickerPack modelStickerPack = this.stickerPack;
            int i = 0;
            int hashCode = (modelStickerPack != null ? modelStickerPack.hashCode() : 0) * 31;
            List<MGRecyclerDataPayload> list = this.stickerItems;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            PremiumTier premiumTier = this.meUserPremiumTier;
            if (premiumTier != null) {
                i = premiumTier.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.isPackEnabled;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isPackEnabled() {
            return this.isPackEnabled;
        }

        public final boolean isPremiumTier2() {
            return this.meUserPremiumTier == PremiumTier.TIER_2;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(stickerPack=");
            R.append(this.stickerPack);
            R.append(", stickerItems=");
            R.append(this.stickerItems);
            R.append(", meUserPremiumTier=");
            R.append(this.meUserPremiumTier);
            R.append(", isPackEnabled=");
            return a.M(R, this.isPackEnabled, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StickerPackStoreSheetViewType.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[StickerPackStoreSheetViewType.STICKER_POPOUT_VIEW_ALL.ordinal()] = 1;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ StickerPackStoreSheetViewModel(long r7, com.discord.stores.StoreStickers r9, com.discord.widgets.chat.input.sticker.StickerPackStoreSheetAnalytics r10, rx.Observable r11, int r12, kotlin.jvm.internal.DefaultConstructorMarker r13) {
        /*
            r6 = this;
            r13 = r12 & 2
            if (r13 == 0) goto La
            com.discord.stores.StoreStream$Companion r9 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreStickers r9 = r9.getStickers()
        La:
            r12 = r12 & 8
            if (r12 == 0) goto L20
            com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewModel$Companion r0 = com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewModel.Companion
            com.discord.stores.StoreStream$Companion r11 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUserSettings r4 = r11.getUserSettings()
            com.discord.stores.StoreUser r5 = r11.getUsers()
            r1 = r7
            r3 = r9
            rx.Observable r11 = com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewModel.Companion.access$observeStoreState(r0, r1, r3, r4, r5)
        L20:
            r5 = r11
            r0 = r6
            r1 = r7
            r3 = r9
            r4 = r10
            r0.<init>(r1, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewModel.<init>(long, com.discord.stores.StoreStickers, com.discord.widgets.chat.input.sticker.StickerPackStoreSheetAnalytics, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final void handleLoadedStoreState(StoreState storeState) {
        boolean z2;
        int stickerAnimationSettings = storeState.getStickerAnimationSettings();
        User meUser = storeState.getMeUser();
        ArrayList arrayList = new ArrayList();
        StoreStickers.StickerPackState stickerPack = storeState.getStickerPack();
        Objects.requireNonNull(stickerPack, "null cannot be cast to non-null type com.discord.stores.StoreStickers.StickerPackState.Loaded");
        ModelStickerPack stickerPack2 = ((StoreStickers.StickerPackState.Loaded) stickerPack).getStickerPack();
        arrayList.addAll(Companion.buildStoreStickerListItems(stickerPack2, stickerAnimationSettings, storeState.getMeUser()));
        PremiumTier premiumTier = meUser.getPremiumTier();
        List<ModelStickerPack> enabledStickerPacks = this.stickersStore.getEnabledStickerPacks();
        boolean z3 = true;
        if (!(enabledStickerPacks instanceof Collection) || !enabledStickerPacks.isEmpty()) {
            for (ModelStickerPack modelStickerPack : enabledStickerPacks) {
                if (modelStickerPack.getId() == this.stickerPackId) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
        }
        z3 = false;
        updateViewState(new ViewState(stickerPack2, arrayList, premiumTier, z3));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        if (storeState.getStickerPack() instanceof StoreStickers.StickerPackState.Loaded) {
            handleLoadedStoreState(storeState);
        }
    }

    private final void trackStickerPackStoreSheetViewed() {
        AnalyticsTracker.INSTANCE.stickerPackViewAllViewed(this.analytics.getSticker(), this.analytics.getType().ordinal() != 0 ? "Sticker Pack Detail Sheet" : "Sticker Pack Detail Sheet (Sticker Upsell Popout)", this.analytics.getLocation(), new Traits.Location(null, this.analytics.getLocation(), null, null, null, 29, null));
    }

    public final void fetchStickersData() {
        this.stickersStore.fetchStickerPack(this.stickerPackId);
        this.stickersStore.fetchEnabledStickerDirectory();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StickerPackStoreSheetViewModel(long j, StoreStickers storeStickers, StickerPackStoreSheetAnalytics stickerPackStoreSheetAnalytics, Observable<StoreState> observable) {
        super(null);
        m.checkNotNullParameter(storeStickers, "stickersStore");
        m.checkNotNullParameter(stickerPackStoreSheetAnalytics, "analytics");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.stickerPackId = j;
        this.stickersStore = storeStickers;
        this.analytics = stickerPackStoreSheetAnalytics;
        fetchStickersData();
        trackStickerPackStoreSheetViewed();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), StickerPackStoreSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
