package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.e0;
import b.b.a.c;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetStickerPickerBinding;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.recycler.SelfHealingLinearLayoutManager;
import com.discord.utilities.recycler.SpeedOnScrollListener;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.StickyHeaderItemDecoration;
import com.discord.views.SearchInputView;
import com.discord.views.sticker.StickerView;
import com.discord.widgets.chat.input.OnBackspacePressedListener;
import com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter;
import com.discord.widgets.chat.input.sticker.StickerCategoryItem;
import com.discord.widgets.chat.input.sticker.StickerPickerViewModel;
import com.discord.widgets.chat.input.sticker.WidgetStickerPickerSheet;
import com.discord.widgets.stickers.StickerPremiumUpsellDialog;
import com.discord.widgets.stickers.UnsendableStickerPremiumUpsellDialog;
import com.discord.widgets.stickers.WidgetStickerPackDetailsDialog;
import com.google.android.material.appbar.AppBarLayout;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import j0.l.e.k;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetStickerPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ð\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u0099\u00012\u00020\u00012\u00020\u0002:\u0002\u0099\u0001B\b¢\u0006\u0005\b\u0098\u0001\u0010\u001dJ\u0019\u0010\u0006\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u001a\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\u00020\u00052\u0006\u0010\u001f\u001a\u00020\u001eH\u0002¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\"\u0010\u001dJ\u000f\u0010#\u001a\u00020\u0005H\u0002¢\u0006\u0004\b#\u0010\u001dJ\u000f\u0010$\u001a\u00020\u0005H\u0002¢\u0006\u0004\b$\u0010\u001dJ%\u0010)\u001a\u00020\u00052\u0006\u0010%\u001a\u00020\u001e2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020'0&H\u0002¢\u0006\u0004\b)\u0010*J\u000f\u0010+\u001a\u00020\u0005H\u0002¢\u0006\u0004\b+\u0010\u001dJ\u000f\u0010-\u001a\u00020,H\u0002¢\u0006\u0004\b-\u0010.J\u0017\u00101\u001a\u00020\u00052\u0006\u00100\u001a\u00020/H\u0002¢\u0006\u0004\b1\u00102J\u000f\u00103\u001a\u00020\u0005H\u0002¢\u0006\u0004\b3\u0010\u001dJ\u000f\u00104\u001a\u00020\u001eH\u0002¢\u0006\u0004\b4\u00105J\u0017\u00108\u001a\u00020\u00052\b\u00107\u001a\u0004\u0018\u000106¢\u0006\u0004\b8\u00109J\u0017\u0010<\u001a\u00020\u00052\b\u0010;\u001a\u0004\u0018\u00010:¢\u0006\u0004\b<\u0010=J#\u0010@\u001a\u00020\u00052\u0014\u0010?\u001a\u0010\u0012\u0004\u0012\u00020/\u0012\u0004\u0012\u00020\u0005\u0018\u00010>¢\u0006\u0004\b@\u0010AJ\u001b\u0010D\u001a\u00020\u00052\f\u0010C\u001a\b\u0012\u0004\u0012\u00020\u00050B¢\u0006\u0004\bD\u0010EJ\r\u0010F\u001a\u00020/¢\u0006\u0004\bF\u0010GJ\u0019\u0010J\u001a\u00020\u00052\b\u0010I\u001a\u0004\u0018\u00010HH\u0016¢\u0006\u0004\bJ\u0010KJ\u0017\u0010N\u001a\u00020\u00052\u0006\u0010M\u001a\u00020LH\u0016¢\u0006\u0004\bN\u0010OJ\u000f\u0010P\u001a\u00020\u0005H\u0016¢\u0006\u0004\bP\u0010\u001dJ\r\u0010Q\u001a\u00020\u0005¢\u0006\u0004\bQ\u0010\u001dJ#\u0010W\u001a\u00020\u00052\b\b\u0002\u0010S\u001a\u00020R2\n\u0010V\u001a\u00060Tj\u0002`U¢\u0006\u0004\bW\u0010XJ\u0015\u0010Z\u001a\u00020\u00052\u0006\u0010Y\u001a\u00020T¢\u0006\u0004\bZ\u0010[J\u001d\u0010\\\u001a\u00020\u00052\u000e\u0010V\u001a\n\u0018\u00010Tj\u0004\u0018\u0001`U¢\u0006\u0004\b\\\u0010]J\u0017\u0010_\u001a\u00020\u00052\u0006\u0010^\u001a\u00020/H\u0016¢\u0006\u0004\b_\u00102J\r\u0010`\u001a\u00020\u0005¢\u0006\u0004\b`\u0010\u001dR\u0018\u0010a\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\ba\u0010bR\u0016\u0010c\u001a\u00020/8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bc\u0010dR\u001c\u0010C\u001a\b\u0012\u0004\u0012\u00020\u00050B8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010eR\u001d\u0010k\u001a\u00020f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bg\u0010h\u001a\u0004\bi\u0010jR\u001d\u0010q\u001a\u00020l8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bm\u0010n\u001a\u0004\bo\u0010pR\u001f\u0010u\u001a\u0004\u0018\u00010T8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\br\u0010n\u001a\u0004\bs\u0010tR$\u0010?\u001a\u0010\u0012\u0004\u0012\u00020/\u0012\u0004\u0012\u00020\u0005\u0018\u00010>8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b?\u0010vR\u0018\u00107\u001a\u0004\u0018\u0001068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u0010wR\u0016\u0010y\u001a\u00020x8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\by\u0010zR:\u0010}\u001a&\u0012\f\u0012\n |*\u0004\u0018\u00010\u00050\u0005 |*\u0012\u0012\f\u0012\n |*\u0004\u0018\u00010\u00050\u0005\u0018\u00010{0{8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b}\u0010~R\u0019\u0010\u0082\u0001\u001a\u00020\u007f8B@\u0002X\u0082\u0004¢\u0006\b\u001a\u0006\b\u0080\u0001\u0010\u0081\u0001R\u0018\u0010\u0083\u0001\u001a\u00020/8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0083\u0001\u0010dR?\u0010\u0085\u0001\u001a(\u0012\f\u0012\n |*\u0004\u0018\u00010/0/ |*\u0013\u0012\f\u0012\n |*\u0004\u0018\u00010/0/\u0018\u00010\u0084\u00010\u0084\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0085\u0001\u0010\u0086\u0001R\u001a\u0010\u0088\u0001\u001a\u00030\u0087\u00018\u0002@\u0002X\u0082.¢\u0006\b\n\u0006\b\u0088\u0001\u0010\u0089\u0001R\u0019\u0010;\u001a\u0004\u0018\u00010:8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b;\u0010\u008a\u0001R\u001b\u0010\u008b\u0001\u001a\u0004\u0018\u00010,8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u008b\u0001\u0010\u008c\u0001R\u0018\u0010\u008d\u0001\u001a\u00020/8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u008d\u0001\u0010dR\"\u0010\u0092\u0001\u001a\u00030\u008e\u00018B@\u0002X\u0082\u0084\u0002¢\u0006\u000f\n\u0005\b\u008f\u0001\u0010n\u001a\u0006\b\u0090\u0001\u0010\u0091\u0001R!\u0010\u0093\u0001\u001a\n\u0018\u00010Tj\u0004\u0018\u0001`U8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0093\u0001\u0010\u0094\u0001R\u001a\u0010\u0096\u0001\u001a\u00030\u0095\u00018\u0002@\u0002X\u0082.¢\u0006\b\n\u0006\b\u0096\u0001\u0010\u0097\u0001¨\u0006\u009a\u0001"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;", "Lb/b/a/c;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;)V", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;)V", "Lcom/discord/widgets/chat/input/sticker/StickerItem;", "stickerItem", "onStickerItemSelected", "(Lcom/discord/widgets/chat/input/sticker/StickerItem;)V", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$GuildItem;", "guildItem", "onGuildClicked", "(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$GuildItem;)V", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;", "stickerPackItem", "onPackClicked", "(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;)V", "Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;", "storeHeaderItem", "onStickerHeaderItemsClicked", "(Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;)V", "onRecentClicked", "()V", "", "selectedCategoryPosition", "onSelectedCategoryAdapterPositionUpdated", "(I)V", "setUpStickerRecycler", "setUpCategoryRecycler", "initializeSearchBar", "stickerRecyclerScrollPosition", "", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "stickerCategoryItems", "handleNewStickerRecyclerScrollPosition", "(ILjava/util/List;)V", "setWindowInsetsListeners", "Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;", "getMode", "()Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;", "", "showBottomBar", "showCategoryBottomBar", "(Z)V", "launchBottomSheet", "getAdditionalBottomPaddingPx", "()I", "Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;", "stickerPickerListener", "setListener", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V", "Lcom/discord/widgets/chat/input/OnBackspacePressedListener;", "onBackspacePressedListener", "setOnBackspacePressedListener", "(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V", "Lkotlin/Function1;", "showSearchBar", "setShowSearchBar", "(Lkotlin/jvm/functions/Function1;)V", "Lkotlin/Function0;", "scrollExpressionPickerToTop", "setScrollExpressionPickerToTop", "(Lkotlin/jvm/functions/Function0;)V", "getCanHandleIsShown", "()Z", "Landroid/os/Bundle;", "savedInstanceState", "onCreate", "(Landroid/os/Bundle;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "clearSearchInput", "", "searchText", "", "Lcom/discord/primitives/StickerPackId;", "packId", "setupForInlineSearchAndScroll", "(Ljava/lang/String;J)V", "itemId", "selectCategoryById", "(J)V", "scrollToPack", "(Ljava/lang/Long;)V", "isActive", "isShown", "scrollToTop", "previousViewState", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;", "canHandleIsShown", "Z", "Lkotlin/jvm/functions/Function0;", "Lcom/discord/databinding/WidgetStickerPickerBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetStickerPickerBinding;", "binding", "Lcom/discord/widgets/chat/input/sticker/StickerPickerInlineViewModel;", "viewModelForInline$delegate", "Lkotlin/Lazy;", "getViewModelForInline", "()Lcom/discord/widgets/chat/input/sticker/StickerPickerInlineViewModel;", "viewModelForInline", "initialStickerPackId$delegate", "getInitialStickerPackId", "()Ljava/lang/Long;", "initialStickerPackId", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;", "Landroidx/recyclerview/widget/LinearLayoutManager;", "categoryLayoutManager", "Landroidx/recyclerview/widget/LinearLayoutManager;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "stickerCategoryScrollSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;", "getViewModel", "()Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;", "viewModel", "wasActive", "Lrx/subjects/BehaviorSubject;", "recyclerScrollingWithinThresholdSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;", "stickerAdapter", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;", "Lcom/discord/widgets/chat/input/OnBackspacePressedListener;", "stickerPickerMode", "Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;", "restoredSearchQueryFromViewModel", "Lcom/discord/widgets/chat/input/sticker/StickerPickerSheetViewModel;", "viewModelForSheet$delegate", "getViewModelForSheet", "()Lcom/discord/widgets/chat/input/sticker/StickerPickerSheetViewModel;", "viewModelForSheet", "autoscrollToPackId", "Ljava/lang/Long;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;", "categoryAdapter", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerPicker extends AppFragment implements c {
    public static final String ARG_MODE = "MODE";
    private static final int STICKER_PICKER_VIEW_FLIPPER_EMPTY_STATE = 1;
    private static final int STICKER_PICKER_VIEW_FLIPPER_EMPTY_STATE_PHASE_1 = 2;
    private static final int STICKER_PICKER_VIEW_FLIPPER_RESULTS = 0;
    public static final String VIEW_TYPE = "VIEW_TYPE";
    private Long autoscrollToPackId;
    private boolean canHandleIsShown;
    private StickerCategoryAdapter categoryAdapter;
    private LinearLayoutManager categoryLayoutManager;
    private OnBackspacePressedListener onBackspacePressedListener;
    private StickerPickerViewModel.ViewState previousViewState;
    private boolean restoredSearchQueryFromViewModel;
    private Function1<? super Boolean, Unit> showSearchBar;
    private WidgetStickerAdapter stickerAdapter;
    private StickerPickerListener stickerPickerListener;
    private StickerPickerMode stickerPickerMode;
    private boolean wasActive;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetStickerPicker.class, "binding", "getBinding()Lcom/discord/databinding/WidgetStickerPickerBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetStickerPicker$binding$2.INSTANCE, null, 2, null);
    private final Lazy initialStickerPackId$delegate = g.lazy(new WidgetStickerPicker$initialStickerPackId$2(this));
    private final Lazy viewModelForInline$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(StickerPickerInlineViewModel.class), new WidgetStickerPicker$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(new WidgetStickerPicker$viewModelForInline$2(this)));
    private final Lazy viewModelForSheet$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(StickerPickerSheetViewModel.class), new WidgetStickerPicker$appActivityViewModels$$inlined$activityViewModels$3(this), new e0(new WidgetStickerPicker$viewModelForSheet$2(this)));
    private Function0<Unit> scrollExpressionPickerToTop = WidgetStickerPicker$scrollExpressionPickerToTop$1.INSTANCE;
    private final PublishSubject<Unit> stickerCategoryScrollSubject = PublishSubject.k0();
    private final BehaviorSubject<Boolean> recyclerScrollingWithinThresholdSubject = BehaviorSubject.l0(Boolean.TRUE);

    /* compiled from: WidgetStickerPicker.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\u0007R\u0016\u0010\t\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\u0007R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004¨\u0006\r"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker$Companion;", "", "", "ARG_MODE", "Ljava/lang/String;", "", "STICKER_PICKER_VIEW_FLIPPER_EMPTY_STATE", "I", "STICKER_PICKER_VIEW_FLIPPER_EMPTY_STATE_PHASE_1", "STICKER_PICKER_VIEW_FLIPPER_RESULTS", WidgetStickerPicker.VIEW_TYPE, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetStickerPicker() {
        super(R.layout.widget_sticker_picker);
    }

    public static final /* synthetic */ WidgetStickerAdapter access$getStickerAdapter$p(WidgetStickerPicker widgetStickerPicker) {
        WidgetStickerAdapter widgetStickerAdapter = widgetStickerPicker.stickerAdapter;
        if (widgetStickerAdapter == null) {
            m.throwUninitializedPropertyAccessException("stickerAdapter");
        }
        return widgetStickerAdapter;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(StickerPickerViewModel.ViewState viewState) {
        String searchQuery = viewState != null ? viewState.getSearchQuery() : null;
        if (!this.restoredSearchQueryFromViewModel && searchQuery != null) {
            this.restoredSearchQueryFromViewModel = true;
            getBinding().r.setText(searchQuery);
        }
        if (viewState != null) {
            int i = 0;
            if (viewState instanceof StickerPickerViewModel.ViewState.EmptyNonPremium) {
                if (this.wasActive && ((StickerPickerViewModel.ViewState.EmptyNonPremium) viewState).isStickersSelectedTab()) {
                    AnalyticsTracker.premiumUpsellViewed$default(AnalyticsTracker.INSTANCE, AnalyticsTracker.PremiumUpsellType.EmptyStickerPickerUpsell, new Traits.Location(null, Traits.Location.Section.EMPTY_STICKER_PICKER_UPSELL, null, null, null, 29, null), null, null, 12, null);
                }
                AppViewFlipper appViewFlipper = getBinding().l;
                m.checkNotNullExpressionValue(appViewFlipper, "binding.chatInputStickerPickerViewFlipper");
                appViewFlipper.setDisplayedChild(2);
                Function1<? super Boolean, Unit> function1 = this.showSearchBar;
                if (function1 != null) {
                    function1.invoke(Boolean.FALSE);
                }
                this.scrollExpressionPickerToTop.invoke();
                StickerPickerViewModel.ViewState.EmptyNonPremium emptyNonPremium = (StickerPickerViewModel.ViewState.EmptyNonPremium) viewState;
                if (emptyNonPremium.getEmptyStateStickers().size() >= 4) {
                    StickerView.e(getBinding().d, emptyNonPremium.getEmptyStateStickers().get(0), null, 2);
                    StickerView.e(getBinding().e, emptyNonPremium.getEmptyStateStickers().get(1), null, 2);
                    StickerView.e(getBinding().f, emptyNonPremium.getEmptyStateStickers().get(2), null, 2);
                    StickerView.e(getBinding().g, emptyNonPremium.getEmptyStateStickers().get(3), null, 2);
                }
                showCategoryBottomBar(false);
            } else if (viewState instanceof StickerPickerViewModel.ViewState.EmptySearchResults) {
                AppViewFlipper appViewFlipper2 = getBinding().l;
                m.checkNotNullExpressionValue(appViewFlipper2, "binding.chatInputStickerPickerViewFlipper");
                appViewFlipper2.setDisplayedChild(1);
                Function1<? super Boolean, Unit> function12 = this.showSearchBar;
                if (function12 != null) {
                    function12.invoke(Boolean.TRUE);
                }
                this.scrollExpressionPickerToTop.invoke();
                getBinding().f2635b.setImageResource(R.drawable.img_stickers_search_empty_90dp);
                TextView textView = getBinding().j;
                m.checkNotNullExpressionValue(textView, "binding.chatInputStickerPickerEmptyTitle");
                textView.setVisibility(8);
                TextView textView2 = getBinding().i;
                m.checkNotNullExpressionValue(textView2, "binding.chatInputStickerPickerEmptySubtitle");
                textView2.setText(getString(R.string.no_sticker_search_results));
                LinkifiedTextView linkifiedTextView = getBinding().c;
                m.checkNotNullExpressionValue(linkifiedTextView, "binding.chatInputStickerPickerEmptyLink");
                linkifiedTextView.setVisibility(8);
                showCategoryBottomBar(true);
                StickerCategoryAdapter stickerCategoryAdapter = this.categoryAdapter;
                if (stickerCategoryAdapter == null) {
                    m.throwUninitializedPropertyAccessException("categoryAdapter");
                }
                stickerCategoryAdapter.setItems(((StickerPickerViewModel.ViewState.EmptySearchResults) viewState).getCategoryItems());
            } else if (viewState instanceof StickerPickerViewModel.ViewState.Stickers) {
                AppViewFlipper appViewFlipper3 = getBinding().l;
                m.checkNotNullExpressionValue(appViewFlipper3, "binding.chatInputStickerPickerViewFlipper");
                appViewFlipper3.setDisplayedChild(0);
                Function1<? super Boolean, Unit> function13 = this.showSearchBar;
                if (function13 != null) {
                    function13.invoke(Boolean.TRUE);
                }
                WidgetStickerAdapter widgetStickerAdapter = this.stickerAdapter;
                if (widgetStickerAdapter == null) {
                    m.throwUninitializedPropertyAccessException("stickerAdapter");
                }
                StickerPickerViewModel.ViewState.Stickers stickers = (StickerPickerViewModel.ViewState.Stickers) viewState;
                widgetStickerAdapter.setData(stickers.getStickerItems());
                SearchInputView searchInputView = getBinding().r;
                m.checkNotNullExpressionValue(searchInputView, "binding.stickerSearchInput");
                if (!(this.stickerPickerMode != StickerPickerMode.INLINE)) {
                    i = 8;
                }
                searchInputView.setVisibility(i);
                WidgetStickerAdapter widgetStickerAdapter2 = this.stickerAdapter;
                if (widgetStickerAdapter2 == null) {
                    m.throwUninitializedPropertyAccessException("stickerAdapter");
                }
                widgetStickerAdapter2.setOnScrollPositionListener(new WidgetStickerPicker$configureUI$1(this, viewState));
                WidgetStickerAdapter widgetStickerAdapter3 = this.stickerAdapter;
                if (widgetStickerAdapter3 == null) {
                    m.throwUninitializedPropertyAccessException("stickerAdapter");
                }
                widgetStickerAdapter3.setOnScrollListener(null);
                showCategoryBottomBar(true);
                StickerCategoryAdapter stickerCategoryAdapter2 = this.categoryAdapter;
                if (stickerCategoryAdapter2 == null) {
                    m.throwUninitializedPropertyAccessException("categoryAdapter");
                }
                stickerCategoryAdapter2.setItems(stickers.getCategoryItems());
                if (this.stickerPickerMode == StickerPickerMode.BOTTOM_SHEET) {
                    RecyclerView recyclerView = getBinding().k;
                    m.checkNotNullExpressionValue(recyclerView, "binding.chatInputStickerPickerRecycler");
                    ViewGroup.LayoutParams layoutParams = recyclerView.getLayoutParams();
                    Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
                    FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                    layoutParams2.topMargin = DimenUtils.dpToPixels(72);
                    RecyclerView recyclerView2 = getBinding().k;
                    m.checkNotNullExpressionValue(recyclerView2, "binding.chatInputStickerPickerRecycler");
                    recyclerView2.setLayoutParams(layoutParams2);
                }
            }
            this.previousViewState = viewState;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final int getAdditionalBottomPaddingPx() {
        if (Build.VERSION.SDK_INT >= 29) {
            return DimenUtils.dpToPixels(8);
        }
        return 0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetStickerPickerBinding getBinding() {
        return (WidgetStickerPickerBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Long getInitialStickerPackId() {
        return (Long) this.initialStickerPackId$delegate.getValue();
    }

    private final StickerPickerMode getMode() {
        Bundle arguments = getArguments();
        StickerPickerMode stickerPickerMode = null;
        Serializable serializable = arguments != null ? arguments.getSerializable("MODE") : null;
        if (serializable instanceof StickerPickerMode) {
            stickerPickerMode = serializable;
        }
        StickerPickerMode stickerPickerMode2 = stickerPickerMode;
        return stickerPickerMode2 != null ? stickerPickerMode2 : StickerPickerMode.INLINE;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final StickerPickerViewModel getViewModel() {
        if (getMode() == StickerPickerMode.INLINE) {
            return getViewModelForInline();
        }
        return getViewModelForSheet();
    }

    private final StickerPickerInlineViewModel getViewModelForInline() {
        return (StickerPickerInlineViewModel) this.viewModelForInline$delegate.getValue();
    }

    private final StickerPickerSheetViewModel getViewModelForSheet() {
        return (StickerPickerSheetViewModel) this.viewModelForSheet$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(StickerPickerViewModel.Event event) {
        if (event instanceof StickerPickerViewModel.Event.ScrollToStickerItemPosition) {
            WidgetStickerAdapter widgetStickerAdapter = this.stickerAdapter;
            if (widgetStickerAdapter == null) {
                m.throwUninitializedPropertyAccessException("stickerAdapter");
            }
            widgetStickerAdapter.scrollToPosition(((StickerPickerViewModel.Event.ScrollToStickerItemPosition) event).getPosition());
        } else if (event instanceof StickerPickerViewModel.Event.ShowStickerPremiumUpsell) {
            UnsendableStickerPremiumUpsellDialog.Companion companion = UnsendableStickerPremiumUpsellDialog.Companion;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            companion.show(parentFragmentManager);
        } else if (event instanceof StickerPickerViewModel.Event.SlowMode) {
            b.a.d.m.i(getParentFragment(), R.string.channel_slowmode_desc_short, 0, 4);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleNewStickerRecyclerScrollPosition(int i, List<? extends StickerCategoryItem> list) {
        Long l;
        for (StickerCategoryItem stickerCategoryItem : list) {
            Pair<Integer, Integer> categoryRange = stickerCategoryItem.getCategoryRange();
            int intValue = categoryRange.getFirst().intValue();
            int intValue2 = categoryRange.getSecond().intValue();
            long categoryId = stickerCategoryItem.getCategoryId();
            if (intValue <= i && intValue2 > i && ((l = this.autoscrollToPackId) == null || (l != null && l.longValue() == categoryId))) {
                this.autoscrollToPackId = null;
                if (!stickerCategoryItem.isSelected()) {
                    selectCategoryById(categoryId);
                }
            }
        }
    }

    private final void initializeSearchBar() {
        AppBarLayout appBarLayout = getBinding().m;
        m.checkNotNullExpressionValue(appBarLayout, "binding.stickerAppBar");
        StickerPickerMode stickerPickerMode = this.stickerPickerMode;
        StickerPickerMode stickerPickerMode2 = StickerPickerMode.INLINE;
        int i = 0;
        appBarLayout.setVisibility(stickerPickerMode != stickerPickerMode2 ? 0 : 8);
        SearchInputView searchInputView = getBinding().r;
        m.checkNotNullExpressionValue(searchInputView, "binding.stickerSearchInput");
        if (!(this.stickerPickerMode != stickerPickerMode2)) {
            i = 8;
        }
        searchInputView.setVisibility(i);
        getBinding().r.setOnClearClicked(new WidgetStickerPicker$initializeSearchBar$1(this));
        if (this.stickerPickerMode == StickerPickerMode.BOTTOM_SHEET) {
            getBinding().r.k.c.requestFocus();
            showKeyboard(getBinding().r.getEditText());
        }
        Bundle arguments = getArguments();
        String string = arguments != null ? arguments.getString("com.discord.intent.extra.EXTRA_TEXT") : null;
        if (string != null) {
            getBinding().r.setText(string);
            getViewModel().setSearchText(string);
            this.restoredSearchQueryFromViewModel = true;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchBottomSheet() {
        WidgetStickerPickerSheet.Companion companion = WidgetStickerPickerSheet.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.show(parentFragmentManager, this.stickerPickerListener, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? null : null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onGuildClicked(StickerCategoryItem.GuildItem guildItem) {
        AnalyticsTracker.INSTANCE.guildCategorySelected(guildItem.getGuild().getId());
        this.autoscrollToPackId = Long.valueOf(guildItem.getGuild().getId());
        selectCategoryById(guildItem.getGuild().getId());
        Pair<Integer, Integer> categoryRange = guildItem.getCategoryRange();
        Observable<T> p = new k(Unit.a).p(200L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(p, "Observable.just(Unit)\n  …0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(p, this, null, 2, null), WidgetStickerPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetStickerPicker$onGuildClicked$1(this, categoryRange));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onPackClicked(StickerCategoryItem.PackItem packItem) {
        AnalyticsTracker.INSTANCE.stickerPackCategorySelected(packItem.getPack().getId());
        this.autoscrollToPackId = Long.valueOf(packItem.getPack().getId());
        selectCategoryById(packItem.getPack().getId());
        Pair<Integer, Integer> categoryRange = packItem.getCategoryRange();
        Observable<T> p = new k(Unit.a).p(200L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(p, "Observable.just(Unit)\n  …0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(p, this, null, 2, null), WidgetStickerPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetStickerPicker$onPackClicked$1(this, categoryRange));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onRecentClicked() {
        selectCategoryById(-1L);
        WidgetStickerAdapter widgetStickerAdapter = this.stickerAdapter;
        if (widgetStickerAdapter == null) {
            m.throwUninitializedPropertyAccessException("stickerAdapter");
        }
        widgetStickerAdapter.scrollToPosition(0);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onSelectedCategoryAdapterPositionUpdated(int i) {
        LinearLayoutManager linearLayoutManager = this.categoryLayoutManager;
        if (linearLayoutManager == null) {
            m.throwUninitializedPropertyAccessException("categoryLayoutManager");
        }
        int findFirstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
        LinearLayoutManager linearLayoutManager2 = this.categoryLayoutManager;
        if (linearLayoutManager2 == null) {
            m.throwUninitializedPropertyAccessException("categoryLayoutManager");
        }
        int findLastCompletelyVisibleItemPosition = linearLayoutManager2.findLastCompletelyVisibleItemPosition();
        int i2 = findLastCompletelyVisibleItemPosition - findFirstCompletelyVisibleItemPosition;
        if (!new IntRange(findFirstCompletelyVisibleItemPosition, findLastCompletelyVisibleItemPosition).contains(i)) {
            int max = Math.max(i < findFirstCompletelyVisibleItemPosition ? i - i2 : i + i2, 0);
            StickerCategoryAdapter stickerCategoryAdapter = this.categoryAdapter;
            if (stickerCategoryAdapter == null) {
                m.throwUninitializedPropertyAccessException("categoryAdapter");
            }
            getBinding().p.scrollToPosition(Math.min(max, stickerCategoryAdapter.getItemCount() - 1));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onStickerHeaderItemsClicked(StoreHeaderItem storeHeaderItem) {
        WidgetStickerPackDetailsDialog.Companion companion = WidgetStickerPackDetailsDialog.Companion;
        FragmentManager childFragmentManager = getChildFragmentManager();
        m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        WidgetStickerPackDetailsDialog.Companion.show$default(companion, childFragmentManager, storeHeaderItem.getPack().getId(), null, 4, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onStickerItemSelected(StickerItem stickerItem) {
        StickerPickerListener stickerPickerListener;
        Sticker sticker = stickerItem.getSticker();
        if (getViewModel().onStickerSelected(sticker) && (stickerPickerListener = this.stickerPickerListener) != null) {
            stickerPickerListener.onStickerPicked(sticker);
        }
    }

    private final void setUpCategoryRecycler() {
        RecyclerView recyclerView = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView, "binding.stickerPickerCategoryRecycler");
        recyclerView.setItemAnimator(null);
        StickerCategoryAdapter stickerCategoryAdapter = new StickerCategoryAdapter(new WidgetStickerPicker$setUpCategoryRecycler$1(this), new WidgetStickerPicker$setUpCategoryRecycler$2(this), new WidgetStickerPicker$setUpCategoryRecycler$3(this), new WidgetStickerPicker$setUpCategoryRecycler$4(this), this, null, 32, null);
        this.categoryAdapter = stickerCategoryAdapter;
        if (stickerCategoryAdapter == null) {
            m.throwUninitializedPropertyAccessException("categoryAdapter");
        }
        stickerCategoryAdapter.setHasStableIds(true);
        RecyclerView recyclerView2 = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView2, "binding.stickerPickerCategoryRecycler");
        StickerCategoryAdapter stickerCategoryAdapter2 = this.categoryAdapter;
        if (stickerCategoryAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("categoryAdapter");
        }
        recyclerView2.setAdapter(stickerCategoryAdapter2);
        RecyclerView recyclerView3 = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView3, "binding.stickerPickerCategoryRecycler");
        StickerCategoryAdapter stickerCategoryAdapter3 = this.categoryAdapter;
        if (stickerCategoryAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("categoryAdapter");
        }
        this.categoryLayoutManager = new SelfHealingLinearLayoutManager(recyclerView3, stickerCategoryAdapter3, 0, false, 8, null);
        RecyclerView recyclerView4 = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView4, "binding.stickerPickerCategoryRecycler");
        LinearLayoutManager linearLayoutManager = this.categoryLayoutManager;
        if (linearLayoutManager == null) {
            m.throwUninitializedPropertyAccessException("categoryLayoutManager");
        }
        recyclerView4.setLayoutManager(linearLayoutManager);
        getBinding().p.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.chat.input.sticker.WidgetStickerPicker$setUpCategoryRecycler$5
            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrolled(RecyclerView recyclerView5, int i, int i2) {
                PublishSubject publishSubject;
                m.checkNotNullParameter(recyclerView5, "recyclerView");
                super.onScrolled(recyclerView5, i, i2);
                publishSubject = WidgetStickerPicker.this.stickerCategoryScrollSubject;
                publishSubject.k.onNext(Unit.a);
            }
        });
    }

    private final void setUpStickerRecycler() {
        RecyclerView recyclerView = getBinding().k;
        m.checkNotNullExpressionValue(recyclerView, "binding.chatInputStickerPickerRecycler");
        recyclerView.setItemAnimator(null);
        RecyclerView recyclerView2 = getBinding().k;
        m.checkNotNullExpressionValue(recyclerView2, "binding.chatInputStickerPickerRecycler");
        WidgetStickerAdapter widgetStickerAdapter = new WidgetStickerAdapter(recyclerView2, new WidgetStickerPicker$setUpStickerRecycler$1(this), new WidgetStickerPicker$setUpStickerRecycler$2(this), this.recyclerScrollingWithinThresholdSubject, this, false, 32, null);
        this.stickerAdapter = widgetStickerAdapter;
        if (widgetStickerAdapter == null) {
            m.throwUninitializedPropertyAccessException("stickerAdapter");
        }
        StickyHeaderItemDecoration stickyHeaderItemDecoration = new StickyHeaderItemDecoration(widgetStickerAdapter);
        getBinding().k.addItemDecoration(stickyHeaderItemDecoration);
        RecyclerView recyclerView3 = getBinding().k;
        m.checkNotNullExpressionValue(recyclerView3, "binding.chatInputStickerPickerRecycler");
        stickyHeaderItemDecoration.blockClicks(recyclerView3);
        getBinding().k.setHasFixedSize(true);
        getBinding().k.setRecyclerListener(WidgetStickerPicker$setUpStickerRecycler$3.INSTANCE);
        getBinding().k.addOnScrollListener(new SpeedOnScrollListener(0L, new WidgetStickerPicker$setUpStickerRecycler$4(this), 0, null, 13, null));
    }

    private final void setWindowInsetsListeners() {
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().q, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.chat.input.sticker.WidgetStickerPicker$setWindowInsetsListeners$1
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                int additionalBottomPaddingPx;
                m.checkNotNullExpressionValue(view, "view");
                m.checkNotNullExpressionValue(windowInsetsCompat, "insets");
                int systemWindowInsetBottom = windowInsetsCompat.getSystemWindowInsetBottom();
                additionalBottomPaddingPx = WidgetStickerPicker.this.getAdditionalBottomPaddingPx();
                view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), additionalBottomPaddingPx + systemWindowInsetBottom);
                return windowInsetsCompat.consumeSystemWindowInsets();
            }
        });
    }

    public static /* synthetic */ void setupForInlineSearchAndScroll$default(WidgetStickerPicker widgetStickerPicker, String str, long j, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "";
        }
        widgetStickerPicker.setupForInlineSearchAndScroll(str, j);
    }

    private final void showCategoryBottomBar(boolean z2) {
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-1, -1);
        int i = 0;
        marginLayoutParams.bottomMargin = z2 ? DimenUtils.dpToPixels(48) : 0;
        AppViewFlipper appViewFlipper = getBinding().l;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.chatInputStickerPickerViewFlipper");
        appViewFlipper.setLayoutParams(new CoordinatorLayout.LayoutParams(marginLayoutParams));
        ConstraintLayout constraintLayout = getBinding().n;
        m.checkNotNullExpressionValue(constraintLayout, "binding.stickerPickerBottomBar");
        constraintLayout.setVisibility(z2 ? 0 : 8);
        View view = getBinding().o;
        m.checkNotNullExpressionValue(view, "binding.stickerPickerBottomBarDivider");
        if (!z2) {
            i = 8;
        }
        view.setVisibility(i);
    }

    public final void clearSearchInput() {
        getViewModel().setSearchText("");
    }

    public final boolean getCanHandleIsShown() {
        return this.canHandleIsShown;
    }

    @Override // b.b.a.c
    public void isShown(boolean z2) {
        if (z2 && !this.wasActive) {
            getBinding().m.setExpanded(true);
            scrollToTop();
        }
        try {
            getViewModel().setSelectedCategoryId(-1L);
            clearSearchInput();
        } catch (Exception unused) {
        }
        this.wasActive = z2;
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        System.gc();
        this.stickerPickerMode = getMode();
        this.canHandleIsShown = true;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        CoordinatorLayout coordinatorLayout = getBinding().q;
        m.checkNotNullExpressionValue(coordinatorLayout, "binding.stickerPickerContainer");
        coordinatorLayout.setPadding(coordinatorLayout.getPaddingLeft(), coordinatorLayout.getPaddingTop(), coordinatorLayout.getPaddingRight(), getAdditionalBottomPaddingPx());
        StickerPickerMode mode = getMode();
        StickerPickerMode stickerPickerMode = StickerPickerMode.INLINE;
        if (mode == stickerPickerMode) {
            setWindowInsetsListeners();
        }
        initializeSearchBar();
        Toolbar toolbar = getBinding().f2636s;
        m.checkNotNullExpressionValue(toolbar, "binding.stickerToolbar");
        ViewGroup.LayoutParams layoutParams = toolbar.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type com.google.android.material.appbar.AppBarLayout.LayoutParams");
        ((AppBarLayout.LayoutParams) layoutParams).setScrollFlags(this.stickerPickerMode == stickerPickerMode ? 5 : 0);
        setUpStickerRecycler();
        setUpCategoryRecycler();
        getBinding().r.a(this, new WidgetStickerPicker$onViewBound$1(this));
        Bundle arguments = getArguments();
        Long valueOf = arguments != null ? Long.valueOf(arguments.getLong("com.discord.intent.EXTRA_STICKER_PACK_ID")) : null;
        getViewModel().setSelectedCategoryId(valueOf != null ? valueOf.longValue() : -1L);
        RecyclerView recyclerView = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView, "binding.stickerPickerCategoryRecycler");
        recyclerView.setLayoutParams(new ConstraintLayout.LayoutParams(-1, -1));
        getBinding().h.setIsLoading(false);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.sticker.WidgetStickerPicker$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                StickerPremiumUpsellDialog.Companion companion = StickerPremiumUpsellDialog.Companion;
                FragmentManager parentFragmentManager = WidgetStickerPicker.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                companion.show(parentFragmentManager, new Traits.Location(null, Traits.Location.Section.EXPRESSION_PICKER, null, null, null, 29, null));
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        StickerPickerViewModel viewModel = getViewModel();
        WidgetExpressionPickerAdapter.Companion companion = WidgetExpressionPickerAdapter.Companion;
        RecyclerView recyclerView = getBinding().k;
        m.checkNotNullExpressionValue(recyclerView, "binding.chatInputStickerPickerRecycler");
        viewModel.setStickerCountToDisplayForStore(companion.calculateNumOfColumns(recyclerView, getResources().getDimension(R.dimen.chat_input_sticker_size), 4));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetStickerPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetStickerPicker$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetStickerPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetStickerPicker$onViewBoundOrOnResume$2(this));
    }

    public final void scrollToPack(Long l) {
        getViewModel().scrollToPackId(l);
    }

    public final void scrollToTop() {
        RecyclerView recyclerView = getBinding().k;
        m.checkNotNullExpressionValue(recyclerView, "binding.chatInputStickerPickerRecycler");
        if (!ViewCompat.isLaidOut(recyclerView) || recyclerView.isLayoutRequested()) {
            recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.widgets.chat.input.sticker.WidgetStickerPicker$scrollToTop$$inlined$doOnLayout$1
                @Override // android.view.View.OnLayoutChangeListener
                public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    m.checkNotNullParameter(view, "view");
                    view.removeOnLayoutChangeListener(this);
                    WidgetStickerPicker.this.getBinding().k.scrollToPosition(0);
                }
            });
        } else {
            getBinding().k.scrollToPosition(0);
        }
    }

    public final void selectCategoryById(long j) {
        getViewModel().setSelectedCategoryId(j);
    }

    public final void setListener(StickerPickerListener stickerPickerListener) {
        this.stickerPickerListener = stickerPickerListener;
    }

    public final void setOnBackspacePressedListener(OnBackspacePressedListener onBackspacePressedListener) {
        this.onBackspacePressedListener = onBackspacePressedListener;
    }

    public final void setScrollExpressionPickerToTop(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "scrollExpressionPickerToTop");
        this.scrollExpressionPickerToTop = function0;
    }

    public final void setShowSearchBar(Function1<? super Boolean, Unit> function1) {
        this.showSearchBar = function1;
    }

    public final void setupForInlineSearchAndScroll(final String str, final long j) {
        m.checkNotNullParameter(str, "searchText");
        RecyclerView recyclerView = getBinding().k;
        m.checkNotNullExpressionValue(recyclerView, "binding.chatInputStickerPickerRecycler");
        if (!ViewCompat.isLaidOut(recyclerView) || recyclerView.isLayoutRequested()) {
            recyclerView.addOnLayoutChangeListener(new WidgetStickerPicker$setupForInlineSearchAndScroll$$inlined$doOnLayout$1(this, str, j));
            return;
        }
        getViewModel().setSearchText(str);
        getViewModel().setSelectedCategoryId(j);
        RecyclerView recyclerView2 = getBinding().k;
        m.checkNotNullExpressionValue(recyclerView2, "binding.chatInputStickerPickerRecycler");
        if (!ViewCompat.isLaidOut(recyclerView2) || recyclerView2.isLayoutRequested()) {
            recyclerView2.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.widgets.chat.input.sticker.WidgetStickerPicker$setupForInlineSearchAndScroll$$inlined$doOnLayout$lambda$1
                @Override // android.view.View.OnLayoutChangeListener
                public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    m.checkNotNullParameter(view, "view");
                    view.removeOnLayoutChangeListener(this);
                    WidgetStickerPicker.this.scrollToPack(Long.valueOf(j));
                }
            });
        } else {
            scrollToPack(Long.valueOf(j));
        }
    }
}
