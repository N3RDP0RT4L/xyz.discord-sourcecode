package com.discord.widgets.chat.input.sticker;

import com.discord.widgets.chat.input.sticker.StickerPickerViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetStickerPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "newPosition", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerPicker$configureUI$1 extends o implements Function1<Integer, Unit> {
    public final /* synthetic */ StickerPickerViewModel.ViewState $viewState;
    public final /* synthetic */ WidgetStickerPicker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStickerPicker$configureUI$1(WidgetStickerPicker widgetStickerPicker, StickerPickerViewModel.ViewState viewState) {
        super(1);
        this.this$0 = widgetStickerPicker;
        this.$viewState = viewState;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke(num.intValue());
        return Unit.a;
    }

    public final void invoke(int i) {
        this.this$0.handleNewStickerRecyclerScrollPosition(i, ((StickerPickerViewModel.ViewState.Stickers) this.$viewState).getCategoryItems());
    }
}
