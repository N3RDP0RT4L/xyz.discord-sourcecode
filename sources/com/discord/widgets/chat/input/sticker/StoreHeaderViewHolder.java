package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.d.b.a.a;
import com.discord.databinding.StickerStoreHeaderItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter;
import com.google.android.material.chip.Chip;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: StickerAdapterViewHolders.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B\u000f\u0012\u0006\u0010\u0012\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u000e\u0010\nR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StoreHeaderViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "bind", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Landroid/view/View;", "getItemView", "()Landroid/view/View;", "onConfigure", "Lcom/discord/databinding/StickerStoreHeaderItemBinding;", "binding", "Lcom/discord/databinding/StickerStoreHeaderItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StoreHeaderViewHolder extends MGRecyclerViewHolder<WidgetStickerAdapter, MGRecyclerDataPayload> implements WidgetExpressionPickerAdapter.StickyHeaderViewHolder {
    private final StickerStoreHeaderItemBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StoreHeaderViewHolder(WidgetStickerAdapter widgetStickerAdapter) {
        super((int) R.layout.sticker_store_header_item, widgetStickerAdapter);
        m.checkNotNullParameter(widgetStickerAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.sticker_store_header_item_animated;
        FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.sticker_store_header_item_animated);
        if (frameLayout != null) {
            i = R.id.sticker_store_header_item_container;
            RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.sticker_store_header_item_container);
            if (relativeLayout != null) {
                i = R.id.sticker_store_header_item_description;
                TextView textView = (TextView) view.findViewById(R.id.sticker_store_header_item_description);
                if (textView != null) {
                    i = R.id.sticker_store_header_item_new;
                    Chip chip = (Chip) view.findViewById(R.id.sticker_store_header_item_new);
                    if (chip != null) {
                        i = R.id.sticker_store_header_item_premium;
                        FrameLayout frameLayout2 = (FrameLayout) view.findViewById(R.id.sticker_store_header_item_premium);
                        if (frameLayout2 != null) {
                            i = R.id.sticker_store_header_item_subtitle;
                            TextView textView2 = (TextView) view.findViewById(R.id.sticker_store_header_item_subtitle);
                            if (textView2 != null) {
                                i = R.id.sticker_store_header_item_title;
                                TextView textView3 = (TextView) view.findViewById(R.id.sticker_store_header_item_title);
                                if (textView3 != null) {
                                    StickerStoreHeaderItemBinding stickerStoreHeaderItemBinding = new StickerStoreHeaderItemBinding((LinearLayout) view, frameLayout, relativeLayout, textView, chip, frameLayout2, textView2, textView3);
                                    m.checkNotNullExpressionValue(stickerStoreHeaderItemBinding, "StickerStoreHeaderItemBinding.bind(itemView)");
                                    this.binding = stickerStoreHeaderItemBinding;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetStickerAdapter access$getAdapter$p(StoreHeaderViewHolder storeHeaderViewHolder) {
        return (WidgetStickerAdapter) storeHeaderViewHolder.adapter;
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter.StickyHeaderViewHolder
    public void bind(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
        m.checkNotNullParameter(mGRecyclerDataPayload, "data");
        onConfigure(i, mGRecyclerDataPayload);
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter.StickyHeaderViewHolder
    public View getItemView() {
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        return view;
    }

    public void onConfigure(int i, final MGRecyclerDataPayload mGRecyclerDataPayload) {
        m.checkNotNullParameter(mGRecyclerDataPayload, "data");
        super.onConfigure(i, (int) mGRecyclerDataPayload);
        if (mGRecyclerDataPayload instanceof StoreHeaderItem) {
            TextView textView = this.binding.g;
            m.checkNotNullExpressionValue(textView, "binding.stickerStoreHeaderItemTitle");
            StoreHeaderItem storeHeaderItem = (StoreHeaderItem) mGRecyclerDataPayload;
            textView.setText(storeHeaderItem.getPack().getName());
            int i2 = 0;
            CharSequence i18nPluralString = StringResourceUtilsKt.getI18nPluralString(a.x(this.itemView, "itemView", "itemView.context"), R.plurals.sticker_pack_sticker_count_numStickers, storeHeaderItem.getPack().getStickers().size(), Integer.valueOf(storeHeaderItem.getPack().getStickers().size()));
            TextView textView2 = this.binding.f;
            m.checkNotNullExpressionValue(textView2, "binding.stickerStoreHeaderItemSubtitle");
            textView2.setText(i18nPluralString);
            FrameLayout frameLayout = this.binding.f2138b;
            m.checkNotNullExpressionValue(frameLayout, "binding.stickerStoreHeaderItemAnimated");
            frameLayout.setVisibility(storeHeaderItem.getPack().isAnimatedPack() ? 0 : 8);
            FrameLayout frameLayout2 = this.binding.e;
            m.checkNotNullExpressionValue(frameLayout2, "binding.stickerStoreHeaderItemPremium");
            if (!storeHeaderItem.getPack().isPremiumPack()) {
                i2 = 8;
            }
            frameLayout2.setVisibility(i2);
            if (((WidgetStickerAdapter) this.adapter).getShowStickerPackDescriptions()) {
                TextView textView3 = this.binding.d;
                m.checkNotNullExpressionValue(textView3, "binding.stickerStoreHeaderItemDescription");
                ViewExtensions.setTextAndVisibilityBy(textView3, storeHeaderItem.getPack().getDescription());
            } else {
                TextView textView4 = this.binding.d;
                m.checkNotNullExpressionValue(textView4, "binding.stickerStoreHeaderItemDescription");
                textView4.setVisibility(8);
            }
            if (storeHeaderItem.getPack().isAnimatedPack() || storeHeaderItem.getPack().isPremiumPack() || storeHeaderItem.getPack().isLimitedPack()) {
                this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.sticker.StoreHeaderViewHolder$onConfigure$1
                    /* JADX WARN: Type inference failed for: r0v0, types: [com.discord.utilities.mg_recycler.MGRecyclerDataPayload, java.lang.Object] */
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        Function1<StoreHeaderItem, Unit> onStickerHeaderItemsClicked = StoreHeaderViewHolder.access$getAdapter$p(StoreHeaderViewHolder.this).getOnStickerHeaderItemsClicked();
                        if (onStickerHeaderItemsClicked != null) {
                            onStickerHeaderItemsClicked.invoke(mGRecyclerDataPayload);
                        }
                    }
                });
            }
        }
    }
}
