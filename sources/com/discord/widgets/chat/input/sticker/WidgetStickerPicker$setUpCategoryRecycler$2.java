package com.discord.widgets.chat.input.sticker;

import com.discord.widgets.chat.input.sticker.StickerCategoryItem;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetStickerPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetStickerPicker$setUpCategoryRecycler$2 extends k implements Function1<StickerCategoryItem.PackItem, Unit> {
    public WidgetStickerPicker$setUpCategoryRecycler$2(WidgetStickerPicker widgetStickerPicker) {
        super(1, widgetStickerPicker, WidgetStickerPicker.class, "onPackClicked", "onPackClicked(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StickerCategoryItem.PackItem packItem) {
        invoke2(packItem);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StickerCategoryItem.PackItem packItem) {
        m.checkNotNullParameter(packItem, "p1");
        ((WidgetStickerPicker) this.receiver).onPackClicked(packItem);
    }
}
