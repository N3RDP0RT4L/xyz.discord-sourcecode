package com.discord.widgets.chat.input.sticker;

import androidx.recyclerview.widget.RecyclerView;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetStickerPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "viewHolder", "", "onViewRecycled", "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerPicker$setUpStickerRecycler$3 implements RecyclerView.RecyclerListener {
    public static final WidgetStickerPicker$setUpStickerRecycler$3 INSTANCE = new WidgetStickerPicker$setUpStickerRecycler$3();

    @Override // androidx.recyclerview.widget.RecyclerView.RecyclerListener
    public final void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        m.checkNotNullParameter(viewHolder, "viewHolder");
        if (viewHolder instanceof StickerViewHolder) {
            ((StickerViewHolder) viewHolder).cancelLoading();
        }
    }
}
