package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StickerAdapterItems.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001c\u0010\u0015\u001a\u00020\u000b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\rR\u001c\u0010\u0018\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\n¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/HeaderItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/widgets/chat/input/sticker/HeaderType;", "component1", "()Lcom/discord/widgets/chat/input/sticker/HeaderType;", "headerType", "copy", "(Lcom/discord/widgets/chat/input/sticker/HeaderType;)Lcom/discord/widgets/chat/input/sticker/HeaderItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/input/sticker/HeaderType;", "getHeaderType", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/sticker/HeaderType;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HeaderItem implements MGRecyclerDataPayload {
    private final HeaderType headerType;
    private final String key;
    private final int type;

    public HeaderItem(HeaderType headerType) {
        m.checkNotNullParameter(headerType, "headerType");
        this.headerType = headerType;
        this.key = headerType.getId();
    }

    public static /* synthetic */ HeaderItem copy$default(HeaderItem headerItem, HeaderType headerType, int i, Object obj) {
        if ((i & 1) != 0) {
            headerType = headerItem.headerType;
        }
        return headerItem.copy(headerType);
    }

    public final HeaderType component1() {
        return this.headerType;
    }

    public final HeaderItem copy(HeaderType headerType) {
        m.checkNotNullParameter(headerType, "headerType");
        return new HeaderItem(headerType);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof HeaderItem) && m.areEqual(this.headerType, ((HeaderItem) obj).headerType);
        }
        return true;
    }

    public final HeaderType getHeaderType() {
        return this.headerType;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        HeaderType headerType = this.headerType;
        if (headerType != null) {
            return headerType.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder R = a.R("HeaderItem(headerType=");
        R.append(this.headerType);
        R.append(")");
        return R.toString();
    }
}
