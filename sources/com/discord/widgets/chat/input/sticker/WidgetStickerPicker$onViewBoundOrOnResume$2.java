package com.discord.widgets.chat.input.sticker;

import com.discord.widgets.chat.input.sticker.StickerPickerViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetStickerPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;", "event", "", "invoke", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerPicker$onViewBoundOrOnResume$2 extends o implements Function1<StickerPickerViewModel.Event, Unit> {
    public final /* synthetic */ WidgetStickerPicker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStickerPicker$onViewBoundOrOnResume$2(WidgetStickerPicker widgetStickerPicker) {
        super(1);
        this.this$0 = widgetStickerPicker;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StickerPickerViewModel.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StickerPickerViewModel.Event event) {
        m.checkNotNullParameter(event, "event");
        this.this$0.handleEvent(event);
    }
}
