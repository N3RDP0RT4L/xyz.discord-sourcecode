package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppComponent;
import com.discord.databinding.StickerCategoryItemGuildBinding;
import com.discord.databinding.StickerCategoryItemPackBinding;
import com.discord.databinding.StickerCategoryItemRecentBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.recycler.DiffCreator;
import com.discord.views.sticker.StickerView;
import com.discord.widgets.chat.input.emoji.GuildIcon;
import com.discord.widgets.chat.input.sticker.StickerCategoryItem;
import com.discord.widgets.chat.input.sticker.StickerCategoryViewHolder;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: StickerCategoryAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001Bu\u0012\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\r0\u001a\u0012\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\r0\u001a\u0012\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\r0\u001f\u0012\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\r0\u001a\u0012\u0006\u0010*\u001a\u00020)\u0012\u001a\b\u0002\u0010%\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u0015\u0012\u0004\u0012\u00020\u00020$¢\u0006\u0004\b+\u0010,J\u001f\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0011\u001a\u00020\u00102\u0006\u0010\f\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0013\u0010\u0014J\u001b\u0010\u0018\u001a\u00020\r2\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015¢\u0006\u0004\b\u0018\u0010\u0019R\"\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\r0\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\"\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\r0\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001cR\u001c\u0010 \u001a\b\u0012\u0004\u0012\u00020\r0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\"\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\r0\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010\u001cR(\u0010%\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u0015\u0012\u0004\u0012\u00020\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u001c\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b'\u0010(¨\u0006-"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;", "getItemCount", "()I", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onBindViewHolder", "(Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;I)V", "", "getItemId", "(I)J", "getItemViewType", "(I)I", "", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "newItems", "setItems", "(Ljava/util/List;)V", "Lkotlin/Function1;", "onSelectedItemAdapterPositionUpdated", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$GuildItem;", "onGuildClicked", "Lkotlin/Function0;", "onRecentClicked", "Lkotlin/jvm/functions/Function0;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;", "onPackClicked", "Lcom/discord/utilities/recycler/DiffCreator;", "diffCreator", "Lcom/discord/utilities/recycler/DiffCreator;", "items", "Ljava/util/List;", "Lcom/discord/app/AppComponent;", "appComponent", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/app/AppComponent;Lcom/discord/utilities/recycler/DiffCreator;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerCategoryAdapter extends RecyclerView.Adapter<StickerCategoryViewHolder> {
    private final DiffCreator<List<StickerCategoryItem>, StickerCategoryViewHolder> diffCreator;
    private List<? extends StickerCategoryItem> items;
    private final Function1<StickerCategoryItem.GuildItem, Unit> onGuildClicked;
    private final Function1<StickerCategoryItem.PackItem, Unit> onPackClicked;
    private final Function0<Unit> onRecentClicked;
    private final Function1<Integer, Unit> onSelectedItemAdapterPositionUpdated;

    public /* synthetic */ StickerCategoryAdapter(Function1 function1, Function1 function12, Function0 function0, Function1 function13, AppComponent appComponent, DiffCreator diffCreator, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(function1, function12, function0, function13, appComponent, (i & 32) != 0 ? new DiffCreator(appComponent) : diffCreator);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        StickerCategoryItem stickerCategoryItem = this.items.get(i);
        if (stickerCategoryItem instanceof StickerCategoryItem.RecentItem) {
            return -1L;
        }
        if (stickerCategoryItem instanceof StickerCategoryItem.PackItem) {
            return ((StickerCategoryItem.PackItem) stickerCategoryItem).getPack().getId();
        }
        if (stickerCategoryItem instanceof StickerCategoryItem.GuildItem) {
            return ((StickerCategoryItem.GuildItem) stickerCategoryItem).getGuild().getId();
        }
        throw new NoWhenBranchMatchedException();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        StickerCategoryItem stickerCategoryItem = this.items.get(i);
        if (stickerCategoryItem instanceof StickerCategoryItem.RecentItem) {
            return 0;
        }
        if (stickerCategoryItem instanceof StickerCategoryItem.PackItem) {
            return 1;
        }
        if (stickerCategoryItem instanceof StickerCategoryItem.GuildItem) {
            return 2;
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void setItems(List<? extends StickerCategoryItem> list) {
        m.checkNotNullParameter(list, "newItems");
        this.diffCreator.dispatchDiffUpdatesAsync(this, new StickerCategoryAdapter$setItems$1(this), this.items, list);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public StickerCategoryAdapter(Function1<? super StickerCategoryItem.GuildItem, Unit> function1, Function1<? super StickerCategoryItem.PackItem, Unit> function12, Function0<Unit> function0, Function1<? super Integer, Unit> function13, AppComponent appComponent, DiffCreator<List<StickerCategoryItem>, StickerCategoryViewHolder> diffCreator) {
        m.checkNotNullParameter(function1, "onGuildClicked");
        m.checkNotNullParameter(function12, "onPackClicked");
        m.checkNotNullParameter(function0, "onRecentClicked");
        m.checkNotNullParameter(function13, "onSelectedItemAdapterPositionUpdated");
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(diffCreator, "diffCreator");
        this.onGuildClicked = function1;
        this.onPackClicked = function12;
        this.onRecentClicked = function0;
        this.onSelectedItemAdapterPositionUpdated = function13;
        this.diffCreator = diffCreator;
        this.items = n.emptyList();
    }

    public void onBindViewHolder(StickerCategoryViewHolder stickerCategoryViewHolder, int i) {
        m.checkNotNullParameter(stickerCategoryViewHolder, "holder");
        StickerCategoryItem stickerCategoryItem = this.items.get(i);
        if (stickerCategoryItem instanceof StickerCategoryItem.RecentItem) {
            ((StickerCategoryViewHolder.Recent) stickerCategoryViewHolder).configure((StickerCategoryItem.RecentItem) stickerCategoryItem, this.onRecentClicked);
        } else if (stickerCategoryItem instanceof StickerCategoryItem.PackItem) {
            ((StickerCategoryViewHolder.Pack) stickerCategoryViewHolder).configure((StickerCategoryItem.PackItem) stickerCategoryItem, this.onPackClicked);
        } else if (stickerCategoryItem instanceof StickerCategoryItem.GuildItem) {
            ((StickerCategoryViewHolder.Guild) stickerCategoryViewHolder).configure((StickerCategoryItem.GuildItem) stickerCategoryItem, this.onGuildClicked);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public StickerCategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        int i2 = R.id.overline;
        if (i == 0) {
            View inflate = from.inflate(R.layout.sticker_category_item_recent, viewGroup, false);
            View findViewById = inflate.findViewById(R.id.overline);
            if (findViewById != null) {
                StickerCategoryItemRecentBinding stickerCategoryItemRecentBinding = new StickerCategoryItemRecentBinding((FrameLayout) inflate, new b.a.i.n(findViewById, findViewById));
                m.checkNotNullExpressionValue(stickerCategoryItemRecentBinding, "StickerCategoryItemRecen…(inflater, parent, false)");
                return new StickerCategoryViewHolder.Recent(stickerCategoryItemRecentBinding);
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(R.id.overline)));
        } else if (i == 1) {
            View inflate2 = from.inflate(R.layout.sticker_category_item_pack, viewGroup, false);
            View findViewById2 = inflate2.findViewById(R.id.overline);
            if (findViewById2 != null) {
                b.a.i.n nVar = new b.a.i.n(findViewById2, findViewById2);
                StickerView stickerView = (StickerView) inflate2.findViewById(R.id.sticker_category_item_pack_avatar);
                if (stickerView != null) {
                    StickerCategoryItemPackBinding stickerCategoryItemPackBinding = new StickerCategoryItemPackBinding((FrameLayout) inflate2, nVar, stickerView);
                    m.checkNotNullExpressionValue(stickerCategoryItemPackBinding, "StickerCategoryItemPackB…(inflater, parent, false)");
                    return new StickerCategoryViewHolder.Pack(stickerCategoryItemPackBinding);
                }
                i2 = R.id.sticker_category_item_pack_avatar;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(i2)));
        } else if (i == 2) {
            View inflate3 = from.inflate(R.layout.sticker_category_item_guild, viewGroup, false);
            View findViewById3 = inflate3.findViewById(R.id.overline);
            if (findViewById3 != null) {
                b.a.i.n nVar2 = new b.a.i.n(findViewById3, findViewById3);
                GuildIcon guildIcon = (GuildIcon) inflate3.findViewById(R.id.sticker_category_item_guild_icon);
                if (guildIcon != null) {
                    StickerCategoryItemGuildBinding stickerCategoryItemGuildBinding = new StickerCategoryItemGuildBinding((FrameLayout) inflate3, nVar2, guildIcon);
                    m.checkNotNullExpressionValue(stickerCategoryItemGuildBinding, "StickerCategoryItemGuild…(inflater, parent, false)");
                    return new StickerCategoryViewHolder.Guild(stickerCategoryItemGuildBinding);
                }
                i2 = R.id.sticker_category_item_guild_icon;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate3.getResources().getResourceName(i2)));
        } else {
            throw new IllegalStateException(a.p("Invalid Sticker Category Type: ", i));
        }
    }
}
