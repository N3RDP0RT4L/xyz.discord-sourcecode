package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.ViewGroup;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.app.AppComponent;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter;
import d0.t.m0;
import d0.t.u;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetStickerAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 72\u00020\u0001:\u00017B_\u0012\u0006\u00104\u001a\u000203\u0012\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u000200\u0012\u0004\u0012\u00020\"0 \u0012\u0016\b\u0002\u0010#\u001a\u0010\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\"\u0018\u00010 \u0012\u0010\b\u0002\u0010(\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010'\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\b\b\u0002\u0010,\u001a\u00020\u000b¢\u0006\u0004\b5\u00106J+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u0001H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0013\u001a\u00020\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0019\u0010\u0018\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001c\u001a\u00020\u00048\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR'\u0010#\u001a\u0010\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\"\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R!\u0010(\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010'8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b*\u0010+R\u0019\u0010,\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R%\u00101\u001a\u000e\u0012\u0004\u0012\u000200\u0012\u0004\u0012\u00020\"0 8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010$\u001a\u0004\b2\u0010&¨\u00068"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "isHeader", "(I)Z", "adapter", "Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;", "createStickyHeaderViewHolder", "(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;", "Landroidx/recyclerview/widget/GridLayoutManager;", "layoutManager", "Landroidx/recyclerview/widget/GridLayoutManager;", "getLayoutManager", "()Landroidx/recyclerview/widget/GridLayoutManager;", "Lcom/discord/app/AppComponent;", "appComponent", "Lcom/discord/app/AppComponent;", "getAppComponent", "()Lcom/discord/app/AppComponent;", "numColumns", "I", "getNumColumns", "()I", "Lkotlin/Function1;", "Lcom/discord/widgets/chat/input/sticker/StoreHeaderItem;", "", "onStickerHeaderItemsClicked", "Lkotlin/jvm/functions/Function1;", "getOnStickerHeaderItemsClicked", "()Lkotlin/jvm/functions/Function1;", "Lrx/subjects/BehaviorSubject;", "recyclerScrollingWithinThresholdSubject", "Lrx/subjects/BehaviorSubject;", "getRecyclerScrollingWithinThresholdSubject", "()Lrx/subjects/BehaviorSubject;", "showStickerPackDescriptions", "Z", "getShowStickerPackDescriptions", "()Z", "Lcom/discord/widgets/chat/input/sticker/StickerItem;", "onStickerItemSelected", "getOnStickerItemSelected", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lrx/subjects/BehaviorSubject;Lcom/discord/app/AppComponent;Z)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerAdapter extends WidgetExpressionPickerAdapter {
    public static final Companion Companion = new Companion(null);
    public static final int DEFAULT_NUM_COLUMNS = 4;
    public static final int ITEM_TYPE_OWNED_HEADER = 0;
    public static final int ITEM_TYPE_STICKER = 1;
    public static final int ITEM_TYPE_STORE_HEADER = 2;
    private final AppComponent appComponent;
    private final GridLayoutManager layoutManager;
    private final int numColumns;
    private final Function1<StoreHeaderItem, Unit> onStickerHeaderItemsClicked;
    private final Function1<StickerItem, Unit> onStickerItemSelected;
    private final BehaviorSubject<Boolean> recyclerScrollingWithinThresholdSubject;
    private final boolean showStickerPackDescriptions;

    /* compiled from: WidgetStickerAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter$Companion;", "", "", "DEFAULT_NUM_COLUMNS", "I", "ITEM_TYPE_OWNED_HEADER", "ITEM_TYPE_STICKER", "ITEM_TYPE_STORE_HEADER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetStickerAdapter(RecyclerView recyclerView, Function1 function1, Function1 function12, BehaviorSubject behaviorSubject, AppComponent appComponent, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(recyclerView, function1, (i & 4) != 0 ? null : function12, (i & 8) != 0 ? null : behaviorSubject, appComponent, (i & 32) != 0 ? false : z2);
    }

    public final AppComponent getAppComponent() {
        return this.appComponent;
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter
    public GridLayoutManager getLayoutManager() {
        return this.layoutManager;
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter
    public int getNumColumns() {
        return this.numColumns;
    }

    public final Function1<StoreHeaderItem, Unit> getOnStickerHeaderItemsClicked() {
        return this.onStickerHeaderItemsClicked;
    }

    public final Function1<StickerItem, Unit> getOnStickerItemSelected() {
        return this.onStickerItemSelected;
    }

    public final BehaviorSubject<Boolean> getRecyclerScrollingWithinThresholdSubject() {
        return this.recyclerScrollingWithinThresholdSubject;
    }

    public final boolean getShowStickerPackDescriptions() {
        return this.showStickerPackDescriptions;
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public boolean isHeader(int i) {
        return ((MGRecyclerDataPayload) u.getOrNull(getInternalData(), i)) instanceof HeaderItem;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public WidgetStickerAdapter(RecyclerView recyclerView, Function1<? super StickerItem, Unit> function1, Function1<? super StoreHeaderItem, Unit> function12, BehaviorSubject<Boolean> behaviorSubject, AppComponent appComponent, boolean z2) {
        super(recyclerView, m0.setOf(0));
        m.checkNotNullParameter(recyclerView, "recycler");
        m.checkNotNullParameter(function1, "onStickerItemSelected");
        m.checkNotNullParameter(appComponent, "appComponent");
        this.onStickerItemSelected = function1;
        this.onStickerHeaderItemsClicked = function12;
        this.recyclerScrollingWithinThresholdSubject = behaviorSubject;
        this.appComponent = appComponent;
        this.showStickerPackDescriptions = z2;
        Context context = recyclerView.getContext();
        m.checkNotNullExpressionValue(context, "recycler.context");
        this.numColumns = WidgetExpressionPickerAdapter.Companion.calculateNumOfColumns(recyclerView, context.getResources().getDimension(R.dimen.chat_input_sticker_size), 4);
        this.layoutManager = new GridLayoutManager(recyclerView.getContext(), getNumColumns());
        getLayoutManager().setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() { // from class: com.discord.widgets.chat.input.sticker.WidgetStickerAdapter.1
            @Override // androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
            public int getSpanSize(int i) {
                int itemViewType = WidgetStickerAdapter.this.getItemViewType(i);
                if (itemViewType == 0 || itemViewType == 2) {
                    return WidgetStickerAdapter.this.getNumColumns();
                }
                return 1;
            }
        });
        recyclerView.setLayoutManager(getLayoutManager());
        recyclerView.setAdapter(this);
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter
    public OwnedHeaderViewHolder createStickyHeaderViewHolder(WidgetExpressionPickerAdapter widgetExpressionPickerAdapter) {
        m.checkNotNullParameter(widgetExpressionPickerAdapter, "adapter");
        return new OwnedHeaderViewHolder((WidgetStickerAdapter) widgetExpressionPickerAdapter);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<WidgetStickerAdapter, MGRecyclerDataPayload> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new OwnedHeaderViewHolder(this);
        }
        if (i == 1) {
            return new StickerViewHolder(i, this, this.recyclerScrollingWithinThresholdSubject, this.appComponent);
        }
        if (i == 2) {
            return new StoreHeaderViewHolder(this);
        }
        throw invalidViewTypeException(i);
    }
}
