package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.models.guild.Guild;
import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.utilities.recycler.DiffKeyProvider;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: StickerCategoryItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00132\u00020\u0001:\u0004\u0013\u0014\u0015\u0016B-\b\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R(\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b0\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u001c\u0010\u000e\u001a\u00020\r8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u000e\u0010\u0010\u0082\u0001\u0003\u0017\u0018\u0019¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "Lcom/discord/utilities/recycler/DiffKeyProvider;", "", "categoryId", "J", "getCategoryId", "()J", "Lkotlin/Pair;", "", "categoryRange", "Lkotlin/Pair;", "getCategoryRange", "()Lkotlin/Pair;", "", "isSelected", "Z", "()Z", HookHelper.constructorName, "(ZLkotlin/Pair;J)V", "Companion", "GuildItem", "PackItem", "RecentItem", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$GuildItem;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class StickerCategoryItem implements DiffKeyProvider {
    public static final Companion Companion = new Companion(null);
    public static final int TYPE_GUILD = 2;
    public static final int TYPE_PACK = 1;
    public static final int TYPE_RECENT = 0;
    private final long categoryId;
    private final Pair<Integer, Integer> categoryRange;
    private final boolean isSelected;

    /* compiled from: StickerCategoryItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$Companion;", "", "", "TYPE_GUILD", "I", "TYPE_PACK", "TYPE_RECENT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StickerCategoryItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u0013\u001a\u00020\r¢\u0006\u0004\b)\u0010*J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u001c\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\tHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJJ\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0010\u001a\u00020\u00022\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0014\b\u0002\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t2\b\b\u0002\u0010\u0013\u001a\u00020\rHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001d\u001a\u00020\r2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001f\u001a\u0004\b \u0010\u0004R(\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0012\u0010!\u001a\u0004\b\"\u0010\fR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b$\u0010\bR\u001c\u0010%\u001a\u00020\u00168\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010\u0018R\u001c\u0010\u0013\u001a\u00020\r8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010(\u001a\u0004\b\u0013\u0010\u000f¨\u0006+"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$GuildItem;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "Lcom/discord/api/sticker/Sticker;", "component2", "()Ljava/util/List;", "Lkotlin/Pair;", "", "component3", "()Lkotlin/Pair;", "", "component4", "()Z", "guild", "stickers", "categoryRange", "isSelected", "copy", "(Lcom/discord/models/guild/Guild;Ljava/util/List;Lkotlin/Pair;Z)Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$GuildItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lkotlin/Pair;", "getCategoryRange", "Ljava/util/List;", "getStickers", "key", "Ljava/lang/String;", "getKey", "Z", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Ljava/util/List;Lkotlin/Pair;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GuildItem extends StickerCategoryItem {
        private final Pair<Integer, Integer> categoryRange;
        private final Guild guild;
        private final boolean isSelected;
        private final String key;
        private final List<Sticker> stickers;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public GuildItem(Guild guild, List<Sticker> list, Pair<Integer, Integer> pair, boolean z2) {
            super(z2, pair, guild.getId(), null);
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(list, "stickers");
            m.checkNotNullParameter(pair, "categoryRange");
            this.guild = guild;
            this.stickers = list;
            this.categoryRange = pair;
            this.isSelected = z2;
            this.key = String.valueOf(guild.getId());
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ GuildItem copy$default(GuildItem guildItem, Guild guild, List list, Pair pair, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = guildItem.guild;
            }
            if ((i & 2) != 0) {
                list = guildItem.stickers;
            }
            if ((i & 4) != 0) {
                pair = guildItem.getCategoryRange();
            }
            if ((i & 8) != 0) {
                z2 = guildItem.isSelected();
            }
            return guildItem.copy(guild, list, pair, z2);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final List<Sticker> component2() {
            return this.stickers;
        }

        public final Pair<Integer, Integer> component3() {
            return getCategoryRange();
        }

        public final boolean component4() {
            return isSelected();
        }

        public final GuildItem copy(Guild guild, List<Sticker> list, Pair<Integer, Integer> pair, boolean z2) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(list, "stickers");
            m.checkNotNullParameter(pair, "categoryRange");
            return new GuildItem(guild, list, pair, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GuildItem)) {
                return false;
            }
            GuildItem guildItem = (GuildItem) obj;
            return m.areEqual(this.guild, guildItem.guild) && m.areEqual(this.stickers, guildItem.stickers) && m.areEqual(getCategoryRange(), guildItem.getCategoryRange()) && isSelected() == guildItem.isSelected();
        }

        @Override // com.discord.widgets.chat.input.sticker.StickerCategoryItem
        public Pair<Integer, Integer> getCategoryRange() {
            return this.categoryRange;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final List<Sticker> getStickers() {
            return this.stickers;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            List<Sticker> list = this.stickers;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            Pair<Integer, Integer> categoryRange = getCategoryRange();
            if (categoryRange != null) {
                i = categoryRange.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean isSelected = isSelected();
            if (isSelected) {
                isSelected = true;
            }
            int i3 = isSelected ? 1 : 0;
            int i4 = isSelected ? 1 : 0;
            return i2 + i3;
        }

        @Override // com.discord.widgets.chat.input.sticker.StickerCategoryItem
        public boolean isSelected() {
            return this.isSelected;
        }

        public String toString() {
            StringBuilder R = a.R("GuildItem(guild=");
            R.append(this.guild);
            R.append(", stickers=");
            R.append(this.stickers);
            R.append(", categoryRange=");
            R.append(getCategoryRange());
            R.append(", isSelected=");
            R.append(isSelected());
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: StickerCategoryItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b\"\u0010#J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ:\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\u0014\b\u0002\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\t2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u00020\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0013R\u001c\u0010\u000e\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u000e\u0010\u000bR(\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010 \u001a\u0004\b!\u0010\u0004¨\u0006$"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "component1", "()Lcom/discord/models/sticker/dto/ModelStickerPack;", "Lkotlin/Pair;", "", "component2", "()Lkotlin/Pair;", "", "component3", "()Z", "pack", "categoryRange", "isSelected", "copy", "(Lcom/discord/models/sticker/dto/ModelStickerPack;Lkotlin/Pair;Z)Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Z", "Lkotlin/Pair;", "getCategoryRange", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "getPack", HookHelper.constructorName, "(Lcom/discord/models/sticker/dto/ModelStickerPack;Lkotlin/Pair;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PackItem extends StickerCategoryItem {
        private final Pair<Integer, Integer> categoryRange;
        private final boolean isSelected;
        private final String key;
        private final ModelStickerPack pack;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PackItem(ModelStickerPack modelStickerPack, Pair<Integer, Integer> pair, boolean z2) {
            super(z2, pair, modelStickerPack.getId(), null);
            m.checkNotNullParameter(modelStickerPack, "pack");
            m.checkNotNullParameter(pair, "categoryRange");
            this.pack = modelStickerPack;
            this.categoryRange = pair;
            this.isSelected = z2;
            this.key = String.valueOf(modelStickerPack.getId());
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ PackItem copy$default(PackItem packItem, ModelStickerPack modelStickerPack, Pair pair, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                modelStickerPack = packItem.pack;
            }
            if ((i & 2) != 0) {
                pair = packItem.getCategoryRange();
            }
            if ((i & 4) != 0) {
                z2 = packItem.isSelected();
            }
            return packItem.copy(modelStickerPack, pair, z2);
        }

        public final ModelStickerPack component1() {
            return this.pack;
        }

        public final Pair<Integer, Integer> component2() {
            return getCategoryRange();
        }

        public final boolean component3() {
            return isSelected();
        }

        public final PackItem copy(ModelStickerPack modelStickerPack, Pair<Integer, Integer> pair, boolean z2) {
            m.checkNotNullParameter(modelStickerPack, "pack");
            m.checkNotNullParameter(pair, "categoryRange");
            return new PackItem(modelStickerPack, pair, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PackItem)) {
                return false;
            }
            PackItem packItem = (PackItem) obj;
            return m.areEqual(this.pack, packItem.pack) && m.areEqual(getCategoryRange(), packItem.getCategoryRange()) && isSelected() == packItem.isSelected();
        }

        @Override // com.discord.widgets.chat.input.sticker.StickerCategoryItem
        public Pair<Integer, Integer> getCategoryRange() {
            return this.categoryRange;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final ModelStickerPack getPack() {
            return this.pack;
        }

        public int hashCode() {
            ModelStickerPack modelStickerPack = this.pack;
            int i = 0;
            int hashCode = (modelStickerPack != null ? modelStickerPack.hashCode() : 0) * 31;
            Pair<Integer, Integer> categoryRange = getCategoryRange();
            if (categoryRange != null) {
                i = categoryRange.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean isSelected = isSelected();
            if (isSelected) {
                isSelected = true;
            }
            int i3 = isSelected ? 1 : 0;
            int i4 = isSelected ? 1 : 0;
            return i2 + i3;
        }

        @Override // com.discord.widgets.chat.input.sticker.StickerCategoryItem
        public boolean isSelected() {
            return this.isSelected;
        }

        public String toString() {
            StringBuilder R = a.R("PackItem(pack=");
            R.append(this.pack);
            R.append(", categoryRange=");
            R.append(getCategoryRange());
            R.append(", isSelected=");
            R.append(isSelected());
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: StickerCategoryItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ0\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u0014\b\u0002\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00022\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R(\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\bR\u001c\u0010\t\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\t\u0010\u0004R\u001c\u0010\u0019\u001a\u00020\r8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u000f¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "", "component1", "()Z", "Lkotlin/Pair;", "", "component2", "()Lkotlin/Pair;", "isSelected", "categoryRange", "copy", "(ZLkotlin/Pair;)Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/Pair;", "getCategoryRange", "Z", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(ZLkotlin/Pair;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RecentItem extends StickerCategoryItem {
        private final Pair<Integer, Integer> categoryRange;
        private final boolean isSelected;
        private final String key = "recent";

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public RecentItem(boolean z2, Pair<Integer, Integer> pair) {
            super(z2, pair, -1L, null);
            m.checkNotNullParameter(pair, "categoryRange");
            this.isSelected = z2;
            this.categoryRange = pair;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ RecentItem copy$default(RecentItem recentItem, boolean z2, Pair pair, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = recentItem.isSelected();
            }
            if ((i & 2) != 0) {
                pair = recentItem.getCategoryRange();
            }
            return recentItem.copy(z2, pair);
        }

        public final boolean component1() {
            return isSelected();
        }

        public final Pair<Integer, Integer> component2() {
            return getCategoryRange();
        }

        public final RecentItem copy(boolean z2, Pair<Integer, Integer> pair) {
            m.checkNotNullParameter(pair, "categoryRange");
            return new RecentItem(z2, pair);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RecentItem)) {
                return false;
            }
            RecentItem recentItem = (RecentItem) obj;
            return isSelected() == recentItem.isSelected() && m.areEqual(getCategoryRange(), recentItem.getCategoryRange());
        }

        @Override // com.discord.widgets.chat.input.sticker.StickerCategoryItem
        public Pair<Integer, Integer> getCategoryRange() {
            return this.categoryRange;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public int hashCode() {
            boolean isSelected = isSelected();
            if (isSelected) {
                isSelected = true;
            }
            int i = isSelected ? 1 : 0;
            int i2 = isSelected ? 1 : 0;
            int i3 = i * 31;
            Pair<Integer, Integer> categoryRange = getCategoryRange();
            return i3 + (categoryRange != null ? categoryRange.hashCode() : 0);
        }

        @Override // com.discord.widgets.chat.input.sticker.StickerCategoryItem
        public boolean isSelected() {
            return this.isSelected;
        }

        public String toString() {
            StringBuilder R = a.R("RecentItem(isSelected=");
            R.append(isSelected());
            R.append(", categoryRange=");
            R.append(getCategoryRange());
            R.append(")");
            return R.toString();
        }
    }

    private StickerCategoryItem(boolean z2, Pair<Integer, Integer> pair, long j) {
        this.isSelected = z2;
        this.categoryRange = pair;
        this.categoryId = j;
    }

    public long getCategoryId() {
        return this.categoryId;
    }

    public Pair<Integer, Integer> getCategoryRange() {
        return this.categoryRange;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public /* synthetic */ StickerCategoryItem(boolean z2, Pair pair, long j, DefaultConstructorMarker defaultConstructorMarker) {
        this(z2, pair, j);
    }
}
