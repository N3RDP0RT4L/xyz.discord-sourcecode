package com.discord.widgets.chat.input.sticker;

import com.discord.app.AppViewModel;
import com.discord.utilities.locale.LocaleManager;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.sticker.StickerPickerViewModel;
import d0.z.d.o;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetStickerPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerPicker$viewModelForSheet$2 extends o implements Function0<AppViewModel<StickerPickerViewModel.ViewState>> {
    public final /* synthetic */ WidgetStickerPicker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStickerPicker$viewModelForSheet$2(WidgetStickerPicker widgetStickerPicker) {
        super(0);
        this.this$0 = widgetStickerPicker;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<StickerPickerViewModel.ViewState> invoke() {
        Long initialStickerPackId;
        Locale primaryLocale = new LocaleManager().getPrimaryLocale(this.this$0.requireContext());
        MessageManager messageManager = new MessageManager(this.this$0.requireContext(), null, null, null, null, null, null, null, null, 510, null);
        initialStickerPackId = this.this$0.getInitialStickerPackId();
        return new StickerPickerSheetViewModel(primaryLocale, messageManager, initialStickerPackId != null ? initialStickerPackId.longValue() : -1L);
    }
}
