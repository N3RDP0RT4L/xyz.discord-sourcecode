package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: StickerPickerMode.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerMode;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "INLINE", "BOTTOM_SHEET", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public enum StickerPickerMode {
    INLINE,
    BOTTOM_SHEET
}
