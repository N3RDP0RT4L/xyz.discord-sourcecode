package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.StickerCategoryItemGuildBinding;
import com.discord.databinding.StickerCategoryItemPackBinding;
import com.discord.databinding.StickerCategoryItemRecentBinding;
import com.discord.rlottie.RLottieImageView;
import com.discord.views.sticker.StickerView;
import com.discord.widgets.chat.input.sticker.StickerCategoryItem;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: StickerCategoryViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0006\u0007\bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005\u0082\u0001\u0003\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;)V", "Guild", "Pack", "Recent", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Recent;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Guild;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class StickerCategoryViewHolder extends RecyclerView.ViewHolder {

    /* compiled from: StickerCategoryViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ)\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Guild;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$GuildItem;", "guildItem", "Lkotlin/Function1;", "", "onGuildClicked", "configure", "(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$GuildItem;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/databinding/StickerCategoryItemGuildBinding;", "binding", "Lcom/discord/databinding/StickerCategoryItemGuildBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/StickerCategoryItemGuildBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Guild extends StickerCategoryViewHolder {
        private final StickerCategoryItemGuildBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public Guild(com.discord.databinding.StickerCategoryItemGuildBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.FrameLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.sticker.StickerCategoryViewHolder.Guild.<init>(com.discord.databinding.StickerCategoryItemGuildBinding):void");
        }

        public final void configure(final StickerCategoryItem.GuildItem guildItem, final Function1<? super StickerCategoryItem.GuildItem, Unit> function1) {
            m.checkNotNullParameter(guildItem, "guildItem");
            m.checkNotNullParameter(function1, "onGuildClicked");
            this.binding.c.updateView(guildItem.getGuild());
            View view = this.binding.f2131b.f160b;
            m.checkNotNullExpressionValue(view, "binding.overline.express…CategorySelectionOverline");
            view.setVisibility(guildItem.isSelected() ? 0 : 8);
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.sticker.StickerCategoryViewHolder$Guild$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    Function1.this.invoke(guildItem);
                }
            });
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            view2.setContentDescription(guildItem.getGuild().getName());
        }
    }

    /* compiled from: StickerCategoryViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ)\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Pack;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;", "packItem", "Lkotlin/Function1;", "", "onPackClicked", "configure", "(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$PackItem;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/databinding/StickerCategoryItemPackBinding;", "binding", "Lcom/discord/databinding/StickerCategoryItemPackBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/StickerCategoryItemPackBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Pack extends StickerCategoryViewHolder {
        private final StickerCategoryItemPackBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public Pack(com.discord.databinding.StickerCategoryItemPackBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.FrameLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.sticker.StickerCategoryViewHolder.Pack.<init>(com.discord.databinding.StickerCategoryItemPackBinding):void");
        }

        public final void configure(final StickerCategoryItem.PackItem packItem, final Function1<? super StickerCategoryItem.PackItem, Unit> function1) {
            m.checkNotNullParameter(packItem, "packItem");
            m.checkNotNullParameter(function1, "onPackClicked");
            this.binding.c.d(packItem.getPack().getCoverSticker(), 2);
            this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.sticker.StickerCategoryViewHolder$Pack$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1.this.invoke(packItem);
                }
            });
            StickerView stickerView = this.binding.c;
            SimpleDraweeView simpleDraweeView = stickerView.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.stickerViewImageview");
            simpleDraweeView.setImportantForAccessibility(4);
            RLottieImageView rLottieImageView = stickerView.j.c;
            m.checkNotNullExpressionValue(rLottieImageView, "binding.stickerViewLottie");
            rLottieImageView.setImportantForAccessibility(4);
            ImageView imageView = stickerView.j.d;
            m.checkNotNullExpressionValue(imageView, "binding.stickerViewPlaceholder");
            imageView.setImportantForAccessibility(4);
            StickerView stickerView2 = this.binding.c;
            m.checkNotNullExpressionValue(stickerView2, "binding.stickerCategoryItemPackAvatar");
            stickerView2.setContentDescription(packItem.getPack().getName());
            View view = this.binding.f2132b.f160b;
            m.checkNotNullExpressionValue(view, "binding.overline.express…CategorySelectionOverline");
            view.setVisibility(packItem.isSelected() ? 0 : 8);
        }
    }

    /* compiled from: StickerCategoryViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ#\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder$Recent;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryViewHolder;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;", "categoryItem", "Lkotlin/Function0;", "", "onRecentClicked", "configure", "(Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem$RecentItem;Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/databinding/StickerCategoryItemRecentBinding;", "binding", "Lcom/discord/databinding/StickerCategoryItemRecentBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/StickerCategoryItemRecentBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Recent extends StickerCategoryViewHolder {
        private final StickerCategoryItemRecentBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public Recent(com.discord.databinding.StickerCategoryItemRecentBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.FrameLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.sticker.StickerCategoryViewHolder.Recent.<init>(com.discord.databinding.StickerCategoryItemRecentBinding):void");
        }

        public final void configure(StickerCategoryItem.RecentItem recentItem, final Function0<Unit> function0) {
            m.checkNotNullParameter(recentItem, "categoryItem");
            m.checkNotNullParameter(function0, "onRecentClicked");
            View view = this.binding.f2133b.f160b;
            m.checkNotNullExpressionValue(view, "binding.overline.express…CategorySelectionOverline");
            view.setVisibility(recentItem.isSelected() ? 0 : 8);
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.sticker.StickerCategoryViewHolder$Recent$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    Function0.this.invoke();
                }
            });
        }
    }

    private StickerCategoryViewHolder(View view) {
        super(view);
    }

    public /* synthetic */ StickerCategoryViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }
}
