package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.stickers.StickerUtils;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StickerAdapterItems.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001:\u0001,B'\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\u0006\u0010\u0011\u001a\u00020\u000b¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ8\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0007J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\nR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b \u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010!\u001a\u0004\b\"\u0010\u0007R\u001c\u0010#\u001a\u00020\u00148\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010\u0016R\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010&\u001a\u0004\b'\u0010\rR\u001c\u0010(\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b(\u0010!\u001a\u0004\b)\u0010\u0007¨\u0006-"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/api/sticker/Sticker;", "component1", "()Lcom/discord/api/sticker/Sticker;", "", "component2", "()I", "Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;", "component3", "()Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;", "Lcom/discord/utilities/stickers/StickerUtils$StickerSendability;", "component4", "()Lcom/discord/utilities/stickers/StickerUtils$StickerSendability;", "sticker", "stickerAnimationSettings", "mode", "sendability", "copy", "(Lcom/discord/api/sticker/Sticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;Lcom/discord/utilities/stickers/StickerUtils$StickerSendability;)Lcom/discord/widgets/chat/input/sticker/StickerItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;", "getMode", "Lcom/discord/api/sticker/Sticker;", "getSticker", "I", "getStickerAnimationSettings", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/utilities/stickers/StickerUtils$StickerSendability;", "getSendability", "type", "getType", HookHelper.constructorName, "(Lcom/discord/api/sticker/Sticker;ILcom/discord/widgets/chat/input/sticker/StickerItem$Mode;Lcom/discord/utilities/stickers/StickerUtils$StickerSendability;)V", "Mode", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerItem implements MGRecyclerDataPayload {
    private final String key;
    private final Mode mode;
    private final StickerUtils.StickerSendability sendability;
    private final Sticker sticker;
    private final int stickerAnimationSettings;
    private final int type = 1;

    /* compiled from: StickerAdapterItems.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerItem$Mode;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "OWNED", "STORE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum Mode {
        OWNED,
        STORE
    }

    public StickerItem(Sticker sticker, int i, Mode mode, StickerUtils.StickerSendability stickerSendability) {
        m.checkNotNullParameter(sticker, "sticker");
        m.checkNotNullParameter(mode, "mode");
        m.checkNotNullParameter(stickerSendability, "sendability");
        this.sticker = sticker;
        this.stickerAnimationSettings = i;
        this.mode = mode;
        this.sendability = stickerSendability;
        StringBuilder R = a.R("sticker:");
        R.append(sticker.getId());
        this.key = R.toString();
    }

    public static /* synthetic */ StickerItem copy$default(StickerItem stickerItem, Sticker sticker, int i, Mode mode, StickerUtils.StickerSendability stickerSendability, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            sticker = stickerItem.sticker;
        }
        if ((i2 & 2) != 0) {
            i = stickerItem.stickerAnimationSettings;
        }
        if ((i2 & 4) != 0) {
            mode = stickerItem.mode;
        }
        if ((i2 & 8) != 0) {
            stickerSendability = stickerItem.sendability;
        }
        return stickerItem.copy(sticker, i, mode, stickerSendability);
    }

    public final Sticker component1() {
        return this.sticker;
    }

    public final int component2() {
        return this.stickerAnimationSettings;
    }

    public final Mode component3() {
        return this.mode;
    }

    public final StickerUtils.StickerSendability component4() {
        return this.sendability;
    }

    public final StickerItem copy(Sticker sticker, int i, Mode mode, StickerUtils.StickerSendability stickerSendability) {
        m.checkNotNullParameter(sticker, "sticker");
        m.checkNotNullParameter(mode, "mode");
        m.checkNotNullParameter(stickerSendability, "sendability");
        return new StickerItem(sticker, i, mode, stickerSendability);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StickerItem)) {
            return false;
        }
        StickerItem stickerItem = (StickerItem) obj;
        return m.areEqual(this.sticker, stickerItem.sticker) && this.stickerAnimationSettings == stickerItem.stickerAnimationSettings && m.areEqual(this.mode, stickerItem.mode) && m.areEqual(this.sendability, stickerItem.sendability);
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final Mode getMode() {
        return this.mode;
    }

    public final StickerUtils.StickerSendability getSendability() {
        return this.sendability;
    }

    public final Sticker getSticker() {
        return this.sticker;
    }

    public final int getStickerAnimationSettings() {
        return this.stickerAnimationSettings;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        Sticker sticker = this.sticker;
        int i = 0;
        int hashCode = (((sticker != null ? sticker.hashCode() : 0) * 31) + this.stickerAnimationSettings) * 31;
        Mode mode = this.mode;
        int hashCode2 = (hashCode + (mode != null ? mode.hashCode() : 0)) * 31;
        StickerUtils.StickerSendability stickerSendability = this.sendability;
        if (stickerSendability != null) {
            i = stickerSendability.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("StickerItem(sticker=");
        R.append(this.sticker);
        R.append(", stickerAnimationSettings=");
        R.append(this.stickerAnimationSettings);
        R.append(", mode=");
        R.append(this.mode);
        R.append(", sendability=");
        R.append(this.sendability);
        R.append(")");
        return R.toString();
    }
}
