package com.discord.widgets.chat.input.sticker;

import android.os.Bundle;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppViewModel;
import com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewModel;
import com.discord.widgets.stickers.StickerPurchaseLocation;
import d0.z.d.m;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetStickerPackStoreSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerPackStoreSheet$viewModel$2 extends o implements Function0<AppViewModel<StickerPackStoreSheetViewModel.ViewState>> {
    public final /* synthetic */ WidgetStickerPackStoreSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetStickerPackStoreSheet$viewModel$2(WidgetStickerPackStoreSheet widgetStickerPackStoreSheet) {
        super(0);
        this.this$0 = widgetStickerPackStoreSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<StickerPackStoreSheetViewModel.ViewState> invoke() {
        Bundle arguments = this.this$0.getArguments();
        Serializable serializable = null;
        Serializable serializable2 = arguments != null ? arguments.getSerializable("com.discord.intent.extra.EXTRA_STICKER") : null;
        Objects.requireNonNull(serializable2, "null cannot be cast to non-null type com.discord.api.sticker.Sticker");
        Sticker sticker = (Sticker) serializable2;
        Bundle arguments2 = this.this$0.getArguments();
        Serializable serializable3 = arguments2 != null ? arguments2.getSerializable("widget_sticker_pack_store_sheet_view_type") : null;
        Objects.requireNonNull(serializable3, "null cannot be cast to non-null type com.discord.widgets.chat.input.sticker.StickerPackStoreSheetViewType");
        StickerPackStoreSheetViewType stickerPackStoreSheetViewType = (StickerPackStoreSheetViewType) serializable3;
        Bundle arguments3 = this.this$0.getArguments();
        String string = arguments3 != null ? arguments3.getString("widget_sticker_pack_Store_sheet_analytics_location") : null;
        Bundle arguments4 = this.this$0.getArguments();
        if (arguments4 != null) {
            serializable = arguments4.getSerializable("widget_sticker_pack_Store_sheet_analytics_location_section");
        }
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type com.discord.widgets.stickers.StickerPurchaseLocation");
        StickerPackStoreSheetAnalytics stickerPackStoreSheetAnalytics = new StickerPackStoreSheetAnalytics(sticker, stickerPackStoreSheetViewType, string, ((StickerPurchaseLocation) serializable).getAnalyticsValue());
        Long i = sticker.i();
        m.checkNotNull(i);
        return new StickerPackStoreSheetViewModel(i.longValue(), null, stickerPackStoreSheetAnalytics, null, 10, null);
    }
}
