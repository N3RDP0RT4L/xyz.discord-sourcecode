package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.discord.api.sticker.Sticker;
import com.discord.widgets.chat.input.expression.WidgetExpressionPickerSheet;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetStickerPickerSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 '2\u00020\u00012\u00020\u0002:\u0001'B\u0007¢\u0006\u0004\b%\u0010&J\u000f\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0004\u0010\u0005J!\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\u00062\b\u0010\t\u001a\u0004\u0018\u00010\bH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0015\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\n2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u001d\u0010\u001c\u001a\u00020\n2\u000e\u0010\u001b\u001a\n\u0018\u00010\u0019j\u0004\u0018\u0001`\u001a¢\u0006\u0004\b\u001c\u0010\u001dR\u0018\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010 \u001a\u00020\u00068\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006("}, d2 = {"Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerSheet;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "Lcom/discord/api/sticker/Sticker;", "sticker", "onStickerPicked", "(Lcom/discord/api/sticker/Sticker;)V", "Landroid/content/DialogInterface;", "dialog", "onCancel", "(Landroid/content/DialogInterface;)V", "onDismiss", "stickerPickerListener", "setStickerPickerListener", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V", "", "Lcom/discord/primitives/StickerPackId;", "packId", "scrollToPack", "(Ljava/lang/Long;)V", "stickerPickerListenerDelegate", "Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;", "container", "Landroid/view/View;", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;", "stickerPickerFragment", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetStickerPickerSheet extends WidgetExpressionPickerSheet implements StickerPickerListener {
    public static final Companion Companion = new Companion(null);
    private View container;
    private WidgetStickerPicker stickerPickerFragment;
    private StickerPickerListener stickerPickerListenerDelegate;

    /* compiled from: WidgetStickerPickerSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012JO\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0010\b\u0002\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t2\u0010\b\u0002\u0010\r\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000b¢\u0006\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;", "stickerPickerListener", "", "Lcom/discord/primitives/StickerPackId;", "initialStickerPackId", "", "searchText", "Lkotlin/Function0;", "", "onCancel", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;Ljava/lang/Long;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Lcom/discord/widgets/chat/input/sticker/WidgetStickerPickerSheet;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final WidgetStickerPickerSheet show(FragmentManager fragmentManager, StickerPickerListener stickerPickerListener, Long l, String str, Function0<Unit> function0) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            System.gc();
            Bundle bundle = new Bundle();
            if (l != null) {
                l.longValue();
                bundle.putLong("com.discord.intent.EXTRA_STICKER_PACK_ID", l.longValue());
            }
            bundle.putString("com.discord.intent.extra.EXTRA_TEXT", str);
            WidgetStickerPickerSheet widgetStickerPickerSheet = new WidgetStickerPickerSheet();
            widgetStickerPickerSheet.setStickerPickerListener(stickerPickerListener);
            widgetStickerPickerSheet.setOnCancel(function0);
            widgetStickerPickerSheet.setArguments(bundle);
            widgetStickerPickerSheet.show(fragmentManager, WidgetStickerPickerSheet.class.getSimpleName());
            return widgetStickerPickerSheet;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public static final /* synthetic */ View access$getContainer$p(WidgetStickerPickerSheet widgetStickerPickerSheet) {
        View view = widgetStickerPickerSheet.container;
        if (view == null) {
            m.throwUninitializedPropertyAccessException("container");
        }
        return view;
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_sticker_picker_sheet;
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerSheet, androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        super.onCancel(dialogInterface);
        WidgetStickerPicker widgetStickerPicker = this.stickerPickerFragment;
        if (widgetStickerPicker == null) {
            m.throwUninitializedPropertyAccessException("stickerPickerFragment");
        }
        widgetStickerPicker.clearSearchInput();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        WidgetStickerPicker widgetStickerPicker = this.stickerPickerFragment;
        if (widgetStickerPicker == null) {
            m.throwUninitializedPropertyAccessException("stickerPickerFragment");
        }
        widgetStickerPicker.selectCategoryById(-1L);
    }

    @Override // com.discord.widgets.chat.input.sticker.StickerPickerListener
    public void onStickerPicked(Sticker sticker) {
        m.checkNotNullParameter(sticker, "sticker");
        StickerPickerListener stickerPickerListener = this.stickerPickerListenerDelegate;
        if (stickerPickerListener != null) {
            stickerPickerListener.onStickerPicked(sticker);
        }
        dismiss();
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerSheet, com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        this.container = view;
        Bundle arguments = getArguments();
        String str = null;
        Long valueOf = arguments != null ? Long.valueOf(arguments.getLong("com.discord.intent.EXTRA_STICKER_PACK_ID")) : null;
        Bundle arguments2 = getArguments();
        Serializable serializable = arguments2 != null ? arguments2.getSerializable("com.discord.intent.EXTRA_STICKER_PICKER_SCREEN") : null;
        Bundle arguments3 = getArguments();
        if (arguments3 != null) {
            str = arguments3.getString("com.discord.intent.extra.EXTRA_TEXT");
        }
        WidgetStickerPicker widgetStickerPicker = new WidgetStickerPicker();
        this.stickerPickerFragment = widgetStickerPicker;
        if (widgetStickerPicker == null) {
            m.throwUninitializedPropertyAccessException("stickerPickerFragment");
        }
        Bundle bundle2 = new Bundle();
        bundle2.putSerializable("MODE", StickerPickerMode.BOTTOM_SHEET);
        bundle2.putSerializable(WidgetStickerPicker.VIEW_TYPE, StickerPackStoreSheetViewType.STICKER_SEARCH_VIEW_ALL);
        bundle2.putString("com.discord.intent.extra.EXTRA_TEXT", str);
        if (valueOf != null) {
            bundle2.putLong("com.discord.intent.EXTRA_STICKER_PACK_ID", valueOf.longValue());
        }
        if (serializable != null) {
            bundle2.putSerializable("com.discord.intent.EXTRA_STICKER_PICKER_SCREEN", serializable);
        }
        widgetStickerPicker.setArguments(bundle2);
        WidgetStickerPicker widgetStickerPicker2 = this.stickerPickerFragment;
        if (widgetStickerPicker2 == null) {
            m.throwUninitializedPropertyAccessException("stickerPickerFragment");
        }
        widgetStickerPicker2.setListener(this);
        FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
        WidgetStickerPicker widgetStickerPicker3 = this.stickerPickerFragment;
        if (widgetStickerPicker3 == null) {
            m.throwUninitializedPropertyAccessException("stickerPickerFragment");
        }
        WidgetStickerPicker widgetStickerPicker4 = this.stickerPickerFragment;
        if (widgetStickerPicker4 == null) {
            m.throwUninitializedPropertyAccessException("stickerPickerFragment");
        }
        beginTransaction.replace(R.id.sticker_sheet_sticker_picker_content, widgetStickerPicker3, widgetStickerPicker4.getClass().getSimpleName()).runOnCommit(new Runnable() { // from class: com.discord.widgets.chat.input.sticker.WidgetStickerPickerSheet$onViewCreated$2
            @Override // java.lang.Runnable
            public final void run() {
                ViewGroup.LayoutParams layoutParams = WidgetStickerPickerSheet.access$getContainer$p(WidgetStickerPickerSheet.this).getLayoutParams();
                if (layoutParams != null) {
                    Resources resources = WidgetStickerPickerSheet.this.getResources();
                    m.checkNotNullExpressionValue(resources, "resources");
                    layoutParams.height = (int) (resources.getDisplayMetrics().heightPixels * 0.9d);
                }
            }
        }).commit();
    }

    public final void scrollToPack(Long l) {
        WidgetStickerPicker widgetStickerPicker = this.stickerPickerFragment;
        if (widgetStickerPicker == null) {
            m.throwUninitializedPropertyAccessException("stickerPickerFragment");
        }
        widgetStickerPicker.scrollToPack(l);
    }

    public final void setStickerPickerListener(StickerPickerListener stickerPickerListener) {
        this.stickerPickerListenerDelegate = stickerPickerListener;
    }
}
