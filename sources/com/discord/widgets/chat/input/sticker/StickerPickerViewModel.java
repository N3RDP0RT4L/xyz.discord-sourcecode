package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.premium.PremiumTier;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.sticker.dto.ModelStickerPack;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreExpressionPickerNavigation;
import com.discord.stores.StoreGuildStickers;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreGuildsSorted;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreSlowMode;
import com.discord.stores.StoreStickers;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stickers.StickerUtils;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.expression.ExpressionTrayTab;
import com.discord.widgets.chat.input.sticker.HeaderType;
import com.discord.widgets.chat.input.sticker.StickerCategoryItem;
import com.discord.widgets.chat.input.sticker.StickerItem;
import com.discord.widgets.chat.input.sticker.StickerPickerViewModel;
import d0.g0.w;
import d0.t.h0;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function11;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
/* compiled from: StickerPickerViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000º\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0016\u0018\u0000 R2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004RSTUB\u0087\u0001\u0012\u000e\b\u0002\u00109\u001a\b\u0012\u0004\u0012\u00020\u001c08\u0012\u0010\b\u0002\u0010H\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\n08\u0012\u0006\u0010<\u001a\u00020;\u0012\u0006\u00106\u001a\u000205\u0012\b\b\u0002\u0010M\u001a\u00020L\u0012\n\u0010A\u001a\u00060\nj\u0002`+\u0012\b\b\u0002\u00103\u001a\u000202\u0012\b\b\u0002\u0010J\u001a\u00020I\u0012\b\b\u0002\u00100\u001a\u00020/\u0012\b\b\u0002\u0010?\u001a\u00020>\u0012\u000e\b\u0002\u0010O\u001a\b\u0012\u0004\u0012\u00020\u00030\u0018¢\u0006\u0004\bP\u0010QJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J'\u0010\r\u001a\u0012\u0012\b\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\f0\t2\u0006\u0010\u0004\u001a\u00020\bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J;\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00112\u0006\u0010\u0004\u001a\u00020\b2\u000e\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00112\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\f0\u0011H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0013\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00190\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ\u0015\u0010!\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\n¢\u0006\u0004\b!\u0010\"J\u0015\u0010%\u001a\u00020$2\u0006\u0010#\u001a\u00020\f¢\u0006\u0004\b%\u0010&J\u0015\u0010)\u001a\u00020\u00052\u0006\u0010(\u001a\u00020'¢\u0006\u0004\b)\u0010*J\u001d\u0010-\u001a\u00020\u00052\u000e\u0010,\u001a\n\u0018\u00010\nj\u0004\u0018\u0001`+¢\u0006\u0004\b-\u0010.R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\u001c\u00109\u001a\b\u0012\u0004\u0012\u00020\u001c088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:R\u0016\u0010<\u001a\u00020;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R\u0016\u0010?\u001a\u00020>8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010@R\u001a\u0010A\u001a\u00060\nj\u0002`+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010C\u001a\u00020'8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010DR\u001c\u0010F\u001a\b\u0012\u0004\u0012\u00020\u00190E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u001e\u0010H\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\n088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bH\u0010:R\u0016\u0010J\u001a\u00020I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bJ\u0010KR\u0016\u0010M\u001a\u00020L8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bM\u0010N¨\u0006V"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;)V", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;", "", "", "Lcom/discord/primitives/StickerId;", "Lcom/discord/api/sticker/Sticker;", "getAllStickersById", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;)Ljava/util/Map;", "handleStoreStateForPage", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;)V", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "listItems", "frequentlyUsedStickers", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "createCategoryItems", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;Ljava/util/List;Ljava/util/List;)Ljava/util/List;", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;", "observeEvents", "()Lrx/Observable;", "", "searchText", "setSearchText", "(Ljava/lang/String;)V", "categoryId", "setSelectedCategoryId", "(J)V", "sticker", "", "onStickerSelected", "(Lcom/discord/api/sticker/Sticker;)Z", "", "count", "setStickerCountToDisplayForStore", "(I)V", "Lcom/discord/primitives/StickerPackId;", "packId", "scrollToPackId", "(Ljava/lang/Long;)V", "Lcom/discord/stores/StorePermissions;", "permissionStore", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/widgets/chat/MessageManager;", "messageManager", "Lcom/discord/widgets/chat/MessageManager;", "Lrx/subjects/BehaviorSubject;", "searchSubject", "Lrx/subjects/BehaviorSubject;", "Ljava/util/Locale;", "locale", "Ljava/util/Locale;", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreUser;", "initialStickerPackId", "J", "stickerCountToDisplayForStore", "I", "Lrx/subjects/PublishSubject;", "eventSubject", "Lrx/subjects/PublishSubject;", "selectedCategoryIdSubject", "Lcom/discord/stores/StoreChannelsSelected;", "channelSelectedStore", "Lcom/discord/stores/StoreChannelsSelected;", "Lcom/discord/stores/StoreStickers;", "stickersStore", "Lcom/discord/stores/StoreStickers;", "storeStateObservable", HookHelper.constructorName, "(Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Ljava/util/Locale;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreStickers;JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class StickerPickerViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    public static final long RECENT_SELECTED_ID = -1;
    private final StoreChannelsSelected channelSelectedStore;
    private final StoreChannels channelStore;
    private final PublishSubject<Event> eventSubject;
    private final long initialStickerPackId;
    private final Locale locale;
    private final MessageManager messageManager;
    private final StorePermissions permissionStore;
    private final BehaviorSubject<String> searchSubject;
    private final BehaviorSubject<Long> selectedCategoryIdSubject;
    private int stickerCountToDisplayForStore;
    private final StoreStickers stickersStore;
    private final StoreUser userStore;

    /* compiled from: StickerPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.sticker.StickerPickerViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            StickerPickerViewModel stickerPickerViewModel = StickerPickerViewModel.this;
            m.checkNotNullExpressionValue(storeState, "storeState");
            stickerPickerViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: StickerPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b8\u00109J=\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJa\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\r0\f2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\f2\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u00142\n\u0010\u0018\u001a\u00060\u0016j\u0002`\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0097\u0001\u00104\u001a\b\u0012\u0004\u0012\u000203022\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00040\u001b2\u000e\u0010\u001d\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00160\u001b2\b\b\u0002\u0010\u001f\u001a\u00020\u001e2\b\b\u0002\u0010!\u001a\u00020 2\b\b\u0002\u0010#\u001a\u00020\"2\b\b\u0002\u0010%\u001a\u00020$2\b\b\u0002\u0010'\u001a\u00020&2\b\b\u0002\u0010)\u001a\u00020(2\b\b\u0002\u0010+\u001a\u00020*2\b\b\u0002\u0010-\u001a\u00020,2\b\b\u0002\u0010/\u001a\u00020.2\b\b\u0002\u00101\u001a\u000200H\u0002¢\u0006\u0004\b4\u00105R\u0016\u00106\u001a\u00020\u00168\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b6\u00107¨\u0006:"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Companion;", "", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "stickerPack", "", "searchInputLower", "", "stickerAnimationSettings", "Ljava/util/Locale;", "locale", "Lcom/discord/models/user/MeUser;", "meUser", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "buildStickerListItems", "(Lcom/discord/models/sticker/dto/ModelStickerPack;Ljava/lang/String;ILjava/util/Locale;Lcom/discord/models/user/MeUser;)Ljava/util/List;", "Lcom/discord/api/sticker/Sticker;", "stickers", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/channel/Channel;", "currentChannel", "", "Lcom/discord/api/permission/PermissionBit;", "currentChannelPermissions", "buildGuildStickersListItems", "(Ljava/util/List;Lcom/discord/models/guild/Guild;ILjava/lang/String;Lcom/discord/models/user/MeUser;Ljava/util/Locale;Lcom/discord/api/channel/Channel;J)Ljava/util/List;", "Lrx/subjects/BehaviorSubject;", "searchSubject", "selectedPackIdSubject", "Lcom/discord/stores/StoreGuildsSorted;", "storeGuildsSorted", "Lcom/discord/stores/StoreGuildStickers;", "storeGuildStickers", "Lcom/discord/stores/StoreStickers;", "storeStickers", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreExpressionPickerNavigation;", "storeExpressionPickerNavigation", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelSelected", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StoreSlowMode;", "storeSlowMode", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;", "observeStoreState", "(Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreGuildsSorted;Lcom/discord/stores/StoreGuildStickers;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;", "RECENT_SELECTED_ID", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final List<MGRecyclerDataPayload> buildGuildStickersListItems(List<Sticker> list, Guild guild, int i, String str, MeUser meUser, Locale locale, Channel channel, long j) {
            ArrayList arrayList = new ArrayList();
            for (Sticker sticker : list) {
                String h = sticker.h();
                Objects.requireNonNull(h, "null cannot be cast to non-null type java.lang.String");
                String lowerCase = h.toLowerCase(locale);
                m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                if (!w.contains$default((CharSequence) lowerCase, (CharSequence) str, false, 2, (Object) null)) {
                    String j2 = sticker.j();
                    Objects.requireNonNull(j2, "null cannot be cast to non-null type java.lang.String");
                    String lowerCase2 = j2.toLowerCase(locale);
                    m.checkNotNullExpressionValue(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
                    if (w.contains$default((CharSequence) lowerCase2, (CharSequence) str, false, 2, (Object) null)) {
                    }
                }
                arrayList.add(new StickerItem(sticker, i, StickerItem.Mode.OWNED, StickerUtils.INSTANCE.getStickerSendability(sticker, meUser, channel, Long.valueOf(j))));
            }
            if ((str.length() > 0) && arrayList.isEmpty()) {
                return n.emptyList();
            }
            arrayList.add(0, new HeaderItem(new HeaderType.GuildItem(guild)));
            return arrayList;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final List<MGRecyclerDataPayload> buildStickerListItems(ModelStickerPack modelStickerPack, String str, int i, Locale locale, MeUser meUser) {
            ArrayList arrayList = new ArrayList();
            if (modelStickerPack.getStickers().isEmpty()) {
                return n.emptyList();
            }
            for (Sticker sticker : modelStickerPack.getStickers()) {
                String h = sticker.h();
                Objects.requireNonNull(h, "null cannot be cast to non-null type java.lang.String");
                String lowerCase = h.toLowerCase(locale);
                m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                if (!w.contains$default((CharSequence) lowerCase, (CharSequence) str, false, 2, (Object) null)) {
                    String j = sticker.j();
                    Objects.requireNonNull(j, "null cannot be cast to non-null type java.lang.String");
                    String lowerCase2 = j.toLowerCase(locale);
                    m.checkNotNullExpressionValue(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
                    if (w.contains$default((CharSequence) lowerCase2, (CharSequence) str, false, 2, (Object) null)) {
                    }
                }
                arrayList.add(new StickerItem(sticker, i, StickerItem.Mode.OWNED, StickerUtils.getStickerSendability$default(StickerUtils.INSTANCE, sticker, meUser, null, null, 12, null)));
            }
            if ((str.length() > 0) && arrayList.isEmpty()) {
                return n.emptyList();
            }
            arrayList.add(0, new HeaderItem(new HeaderType.PackItem(modelStickerPack)));
            return arrayList;
        }

        private final Observable<StoreState> observeStoreState(final BehaviorSubject<String> behaviorSubject, final BehaviorSubject<Long> behaviorSubject2, final StoreGuildsSorted storeGuildsSorted, final StoreGuildStickers storeGuildStickers, final StoreStickers storeStickers, final StoreUserSettings storeUserSettings, final StoreUser storeUser, final StoreExpressionPickerNavigation storeExpressionPickerNavigation, StoreChannelsSelected storeChannelsSelected, final StorePermissions storePermissions, final StoreSlowMode storeSlowMode, final StoreGuilds storeGuilds) {
            Observable Y = storeChannelsSelected.observeResolvedSelectedChannel().Y(new b<StoreChannelsSelected.ResolvedSelectedChannel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.chat.input.sticker.StickerPickerViewModel$Companion$observeStoreState$1

                /* compiled from: StickerPickerViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010 \u001a\u00020\u001d2X\u0010\u0006\u001aT\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0005**\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0000j\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u0001`\u00040\u0000j\u0012\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003`\u00042*\u0010\u000b\u001a&\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0018\u0012\u0016\u0012\b\u0012\u00060\u0001j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0007j\u0002`\n0\u00072\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0010\u0010\u000f\u001a\f\u0012\b\u0012\u00060\u0001j\u0002`\b0\f2\u000e\u0010\u0011\u001a\n \u0005*\u0004\u0018\u00010\u00100\u00102\u0006\u0010\u0013\u001a\u00020\u00122\b\u0010\u0014\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u00172\u001a\u0010\u001b\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0019\u0012\b\u0012\u00060\u0001j\u0002`\u001a0\u00072\u0006\u0010\u001c\u001a\u00020\u0012H\n¢\u0006\u0004\b\u001e\u0010\u001f"}, d2 = {"Ljava/util/LinkedHashMap;", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "Lkotlin/collections/LinkedHashMap;", "kotlin.jvm.PlatformType", "allGuilds", "", "Lcom/discord/primitives/StickerId;", "Lcom/discord/api/sticker/Sticker;", "Lcom/discord/stores/StickerMap;", "guildStickers", "", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "enabledStickerPacks", "frequentlyUsedStickerIds", "", "searchInputString", "", "stickerAnimationSettings", "selectedCategoryId", "Lcom/discord/models/user/MeUser;", "meUser", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "selectedExpressionPickerTab", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/permission/PermissionBit;", "allChannelPermissions", "cooldownSecs", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;", "invoke", "(Ljava/util/LinkedHashMap;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/user/MeUser;Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Ljava/util/Map;I)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.input.sticker.StickerPickerViewModel$Companion$observeStoreState$1$2  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass2 extends o implements Function11<LinkedHashMap<Long, Guild>, Map<Long, ? extends Map<Long, ? extends Sticker>>, List<? extends ModelStickerPack>, List<? extends Long>, String, Integer, Long, MeUser, ExpressionTrayTab, Map<Long, ? extends Long>, Integer, StickerPickerViewModel.StoreState.Loaded> {
                    public final /* synthetic */ Channel $channel;

                    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                    public AnonymousClass2(Channel channel) {
                        super(11);
                        this.$channel = channel;
                    }

                    @Override // kotlin.jvm.functions.Function11
                    public /* bridge */ /* synthetic */ StickerPickerViewModel.StoreState.Loaded invoke(LinkedHashMap<Long, Guild> linkedHashMap, Map<Long, ? extends Map<Long, ? extends Sticker>> map, List<? extends ModelStickerPack> list, List<? extends Long> list2, String str, Integer num, Long l, MeUser meUser, ExpressionTrayTab expressionTrayTab, Map<Long, ? extends Long> map2, Integer num2) {
                        return invoke(linkedHashMap, (Map<Long, ? extends Map<Long, Sticker>>) map, (List<ModelStickerPack>) list, (List<Long>) list2, str, num.intValue(), l, meUser, expressionTrayTab, (Map<Long, Long>) map2, num2.intValue());
                    }

                    public final StickerPickerViewModel.StoreState.Loaded invoke(LinkedHashMap<Long, Guild> linkedHashMap, Map<Long, ? extends Map<Long, Sticker>> map, List<ModelStickerPack> list, List<Long> list2, String str, int i, Long l, MeUser meUser, ExpressionTrayTab expressionTrayTab, Map<Long, Long> map2, int i2) {
                        m.checkNotNullParameter(map, "guildStickers");
                        m.checkNotNullParameter(list, "enabledStickerPacks");
                        m.checkNotNullParameter(list2, "frequentlyUsedStickerIds");
                        m.checkNotNullParameter(meUser, "meUser");
                        m.checkNotNullParameter(expressionTrayTab, "selectedExpressionPickerTab");
                        m.checkNotNullParameter(map2, "allChannelPermissions");
                        m.checkNotNullExpressionValue(linkedHashMap, "allGuilds");
                        m.checkNotNullExpressionValue(str, "searchInputString");
                        boolean z2 = expressionTrayTab == ExpressionTrayTab.STICKER;
                        Channel channel = this.$channel;
                        long j = 0;
                        Long l2 = map2.get(Long.valueOf(channel != null ? channel.h() : 0L));
                        if (l2 != null) {
                            j = l2.longValue();
                        }
                        return new StickerPickerViewModel.StoreState.Loaded(linkedHashMap, map, list, list2, str, i, l, meUser, z2, channel, j, i2 > 0);
                    }
                }

                /* JADX WARN: Code restructure failed: missing block: B:5:0x0013, code lost:
                    if (r3 != null) goto L7;
                 */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public final rx.Observable<? extends com.discord.widgets.chat.input.sticker.StickerPickerViewModel.StoreState> call(com.discord.stores.StoreChannelsSelected.ResolvedSelectedChannel r17) {
                    /*
                        r16 = this;
                        r0 = r16
                        com.discord.api.channel.Channel r1 = r17.getChannelOrParent()
                        r2 = 0
                        if (r1 == 0) goto L16
                        long r3 = r1.f()
                        com.discord.stores.StoreGuilds r5 = com.discord.stores.StoreGuilds.this
                        com.discord.models.guild.Guild r3 = r5.getGuild(r3)
                        if (r3 == 0) goto L16
                        goto L17
                    L16:
                        r3 = r2
                    L17:
                        com.discord.stores.StoreGuildsSorted r4 = r2
                        rx.Observable r4 = r4.observeOrderedGuilds()
                        com.discord.widgets.chat.input.sticker.StickerPickerViewModel$Companion$observeStoreState$1$1 r5 = new com.discord.widgets.chat.input.sticker.StickerPickerViewModel$Companion$observeStoreState$1$1
                        r5.<init>()
                        rx.Observable r3 = r4.F(r5)
                        rx.Observable r4 = r3.q()
                        java.lang.String r3 = "storeGuildsSorted.observ… }.distinctUntilChanged()"
                        d0.z.d.m.checkNotNullExpressionValue(r4, r3)
                        com.discord.stores.StoreGuildStickers r3 = r3
                        rx.Observable r5 = r3.observeGuildStickers()
                        com.discord.stores.StoreStickers r3 = r4
                        rx.Observable r6 = r3.observeEnabledStickerPacks()
                        com.discord.stores.StoreStickers r3 = r4
                        rx.Observable r7 = r3.observeFrequentlyUsedStickerIds()
                        rx.subjects.BehaviorSubject r8 = r5
                        com.discord.stores.StoreUserSettings r3 = r6
                        r9 = 0
                        r10 = 1
                        rx.Observable r3 = com.discord.stores.StoreUserSettings.observeStickerAnimationSettings$default(r3, r9, r10, r2)
                        rx.subjects.BehaviorSubject r11 = r7
                        com.discord.stores.StoreUser r12 = r8
                        rx.Observable r2 = com.discord.stores.StoreUser.observeMe$default(r12, r9, r10, r2)
                        com.discord.stores.StoreExpressionPickerNavigation r9 = r9
                        rx.Observable r12 = r9.observeSelectedTab()
                        com.discord.stores.StorePermissions r9 = r10
                        rx.Observable r13 = r9.observePermissionsForAllChannels()
                        com.discord.stores.StoreSlowMode r9 = r11
                        long r14 = r17.getId()
                        java.lang.Long r10 = java.lang.Long.valueOf(r14)
                        com.discord.stores.StoreSlowMode$Type$MessageSend r14 = com.discord.stores.StoreSlowMode.Type.MessageSend.INSTANCE
                        rx.Observable r14 = r9.observeCooldownSecs(r10, r14)
                        com.discord.widgets.chat.input.sticker.StickerPickerViewModel$Companion$observeStoreState$1$2 r15 = new com.discord.widgets.chat.input.sticker.StickerPickerViewModel$Companion$observeStoreState$1$2
                        r15.<init>(r1)
                        r9 = r3
                        r10 = r11
                        r11 = r2
                        rx.Observable r1 = com.discord.utilities.rx.ObservableCombineLatestOverloadsKt.combineLatest(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
                        return r1
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.sticker.StickerPickerViewModel$Companion$observeStoreState$1.call(com.discord.stores.StoreChannelsSelected$ResolvedSelectedChannel):rx.Observable");
                }
            });
            m.checkNotNullExpressionValue(Y, "storeChannelSelected.obs…      )\n        }\n      }");
            return Y;
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, BehaviorSubject behaviorSubject, BehaviorSubject behaviorSubject2, StoreGuildsSorted storeGuildsSorted, StoreGuildStickers storeGuildStickers, StoreStickers storeStickers, StoreUserSettings storeUserSettings, StoreUser storeUser, StoreExpressionPickerNavigation storeExpressionPickerNavigation, StoreChannelsSelected storeChannelsSelected, StorePermissions storePermissions, StoreSlowMode storeSlowMode, StoreGuilds storeGuilds, int i, Object obj) {
            return companion.observeStoreState(behaviorSubject, behaviorSubject2, (i & 4) != 0 ? StoreStream.Companion.getGuildsSorted() : storeGuildsSorted, (i & 8) != 0 ? StoreStream.Companion.getGuildStickers() : storeGuildStickers, (i & 16) != 0 ? StoreStream.Companion.getStickers() : storeStickers, (i & 32) != 0 ? StoreStream.Companion.getUserSettings() : storeUserSettings, (i & 64) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 128) != 0 ? StoreStream.Companion.getExpressionPickerNavigation() : storeExpressionPickerNavigation, (i & 256) != 0 ? StoreStream.Companion.getChannelsSelected() : storeChannelsSelected, (i & 512) != 0 ? StoreStream.Companion.getPermissions() : storePermissions, (i & 1024) != 0 ? StoreStream.Companion.getSlowMode() : storeSlowMode, (i & 2048) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StickerPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;", "", HookHelper.constructorName, "()V", "ScrollToStickerItemPosition", "ShowStickerPremiumUpsell", "SlowMode", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ScrollToStickerItemPosition;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ShowStickerPremiumUpsell;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$SlowMode;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: StickerPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ScrollToStickerItemPosition;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;", "", "component1", "()I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "copy", "(I)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ScrollToStickerItemPosition;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getPosition", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ScrollToStickerItemPosition extends Event {
            private final int position;

            public ScrollToStickerItemPosition(int i) {
                super(null);
                this.position = i;
            }

            public static /* synthetic */ ScrollToStickerItemPosition copy$default(ScrollToStickerItemPosition scrollToStickerItemPosition, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = scrollToStickerItemPosition.position;
                }
                return scrollToStickerItemPosition.copy(i);
            }

            public final int component1() {
                return this.position;
            }

            public final ScrollToStickerItemPosition copy(int i) {
                return new ScrollToStickerItemPosition(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ScrollToStickerItemPosition) && this.position == ((ScrollToStickerItemPosition) obj).position;
                }
                return true;
            }

            public final int getPosition() {
                return this.position;
            }

            public int hashCode() {
                return this.position;
            }

            public String toString() {
                return a.A(a.R("ScrollToStickerItemPosition(position="), this.position, ")");
            }
        }

        /* compiled from: StickerPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$ShowStickerPremiumUpsell;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowStickerPremiumUpsell extends Event {
            public static final ShowStickerPremiumUpsell INSTANCE = new ShowStickerPremiumUpsell();

            private ShowStickerPremiumUpsell() {
                super(null);
            }
        }

        /* compiled from: StickerPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event$SlowMode;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SlowMode extends Event {
            public static final SlowMode INSTANCE = new SlowMode();

            private SlowMode() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StickerPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Uninitialized;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: StickerPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u0000\n\u0002\b\u001a\b\u0086\b\u0018\u00002\u00020\u0001BÃ\u0001\u0012*\u0010+\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\bj\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t`\n\u0012&\u0010,\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\r0\r\u0012\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0010\u0010.\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u000e0\u0012\u0012\u0006\u0010/\u001a\u00020\u0017\u0012\u0006\u00100\u001a\u00020\u001a\u0012\b\u00101\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u00102\u001a\u00020\u001f\u0012\u0006\u00103\u001a\u00020\u0005\u0012\b\u00104\u001a\u0004\u0018\u00010$\u0012\n\u00105\u001a\u00060\u0002j\u0002`'\u0012\u0006\u00106\u001a\u00020\u0005¢\u0006\u0004\bS\u0010TJ\u0019\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007J4\u0010\u000b\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\bj\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ0\u0010\u0010\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\r0\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0016\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u000e0\u0012HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0015J\u0010\u0010\u0018\u001a\u00020\u0017HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0012\u0010\u001d\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÆ\u0003¢\u0006\u0004\b \u0010!J\u0010\u0010\"\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\"\u0010#J\u0012\u0010%\u001a\u0004\u0018\u00010$HÆ\u0003¢\u0006\u0004\b%\u0010&J\u0014\u0010(\u001a\u00060\u0002j\u0002`'HÆ\u0003¢\u0006\u0004\b(\u0010)J\u0010\u0010*\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b*\u0010#Jä\u0001\u00107\u001a\u00020\u00002,\b\u0002\u0010+\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\bj\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t`\n2(\b\u0002\u0010,\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\r0\r2\u000e\b\u0002\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\u0012\b\u0002\u0010.\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u000e0\u00122\b\b\u0002\u0010/\u001a\u00020\u00172\b\b\u0002\u00100\u001a\u00020\u001a2\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u00102\u001a\u00020\u001f2\b\b\u0002\u00103\u001a\u00020\u00052\n\b\u0002\u00104\u001a\u0004\u0018\u00010$2\f\b\u0002\u00105\u001a\u00060\u0002j\u0002`'2\b\b\u0002\u00106\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b7\u00108J\u0010\u00109\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b9\u0010\u0019J\u0010\u0010:\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b:\u0010\u001cJ\u001a\u0010=\u001a\u00020\u00052\b\u0010<\u001a\u0004\u0018\u00010;HÖ\u0003¢\u0006\u0004\b=\u0010>R\u001b\u00101\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010?\u001a\u0004\b@\u0010\u001eR\u0019\u00106\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010A\u001a\u0004\b6\u0010#R\u0019\u00100\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010B\u001a\u0004\bC\u0010\u001cR\u001f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010D\u001a\u0004\bE\u0010\u0015R9\u0010,\u001a\"\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\r0\r8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010F\u001a\u0004\bG\u0010\u0011R#\u0010.\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u000e0\u00128\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010D\u001a\u0004\bH\u0010\u0015R\u0019\u00102\u001a\u00020\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010I\u001a\u0004\bJ\u0010!R\u0019\u0010/\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010K\u001a\u0004\bL\u0010\u0019R\u0019\u00103\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010A\u001a\u0004\b3\u0010#R=\u0010+\u001a&\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t0\bj\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\t`\n8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010M\u001a\u0004\bN\u0010\fR\u001b\u00104\u001a\u0004\u0018\u00010$8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010O\u001a\u0004\bP\u0010&R\u001d\u00105\u001a\u00060\u0002j\u0002`'8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010Q\u001a\u0004\bR\u0010)¨\u0006U"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;", "", "Lcom/discord/primitives/GuildId;", "externalStickerGuildId", "", "canUseExternalStickersInCurrentChannel", "(J)Z", "Ljava/util/LinkedHashMap;", "Lcom/discord/models/guild/Guild;", "Lkotlin/collections/LinkedHashMap;", "component1", "()Ljava/util/LinkedHashMap;", "", "Lcom/discord/primitives/StickerId;", "Lcom/discord/api/sticker/Sticker;", "component2", "()Ljava/util/Map;", "", "Lcom/discord/models/sticker/dto/ModelStickerPack;", "component3", "()Ljava/util/List;", "component4", "", "component5", "()Ljava/lang/String;", "", "component6", "()I", "component7", "()Ljava/lang/Long;", "Lcom/discord/models/user/MeUser;", "component8", "()Lcom/discord/models/user/MeUser;", "component9", "()Z", "Lcom/discord/api/channel/Channel;", "component10", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/api/permission/PermissionBit;", "component11", "()J", "component12", "allGuilds", "guildStickers", "enabledStickerPacks", "frequentlyUsedStickerIds", "searchInputStringUpper", "stickerAnimationSettings", "selectedCategoryId", "meUser", "isStickersSelectedTab", "currentChannel", "currentChannelPermissions", "isOnCooldown", "copy", "(Ljava/util/LinkedHashMap;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/user/MeUser;ZLcom/discord/api/channel/Channel;JZ)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Loaded;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getSelectedCategoryId", "Z", "I", "getStickerAnimationSettings", "Ljava/util/List;", "getEnabledStickerPacks", "Ljava/util/Map;", "getGuildStickers", "getFrequentlyUsedStickerIds", "Lcom/discord/models/user/MeUser;", "getMeUser", "Ljava/lang/String;", "getSearchInputStringUpper", "Ljava/util/LinkedHashMap;", "getAllGuilds", "Lcom/discord/api/channel/Channel;", "getCurrentChannel", "J", "getCurrentChannelPermissions", HookHelper.constructorName, "(Ljava/util/LinkedHashMap;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ILjava/lang/Long;Lcom/discord/models/user/MeUser;ZLcom/discord/api/channel/Channel;JZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends StoreState {
            private final LinkedHashMap<Long, Guild> allGuilds;
            private final Channel currentChannel;
            private final long currentChannelPermissions;
            private final List<ModelStickerPack> enabledStickerPacks;
            private final List<Long> frequentlyUsedStickerIds;
            private final Map<Long, Map<Long, Sticker>> guildStickers;
            private final boolean isOnCooldown;
            private final boolean isStickersSelectedTab;
            private final MeUser meUser;
            private final String searchInputStringUpper;
            private final Long selectedCategoryId;
            private final int stickerAnimationSettings;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(LinkedHashMap<Long, Guild> linkedHashMap, Map<Long, ? extends Map<Long, Sticker>> map, List<ModelStickerPack> list, List<Long> list2, String str, int i, Long l, MeUser meUser, boolean z2, Channel channel, long j, boolean z3) {
                super(null);
                m.checkNotNullParameter(linkedHashMap, "allGuilds");
                m.checkNotNullParameter(map, "guildStickers");
                m.checkNotNullParameter(list, "enabledStickerPacks");
                m.checkNotNullParameter(list2, "frequentlyUsedStickerIds");
                m.checkNotNullParameter(str, "searchInputStringUpper");
                m.checkNotNullParameter(meUser, "meUser");
                this.allGuilds = linkedHashMap;
                this.guildStickers = map;
                this.enabledStickerPacks = list;
                this.frequentlyUsedStickerIds = list2;
                this.searchInputStringUpper = str;
                this.stickerAnimationSettings = i;
                this.selectedCategoryId = l;
                this.meUser = meUser;
                this.isStickersSelectedTab = z2;
                this.currentChannel = channel;
                this.currentChannelPermissions = j;
                this.isOnCooldown = z3;
            }

            public final boolean canUseExternalStickersInCurrentChannel(long j) {
                Channel channel = this.currentChannel;
                if ((channel != null && ChannelUtils.x(channel)) || PermissionUtils.can(Permission.USE_EXTERNAL_STICKERS, Long.valueOf(this.currentChannelPermissions))) {
                    return true;
                }
                Channel channel2 = this.currentChannel;
                return channel2 != null && j == channel2.f();
            }

            public final LinkedHashMap<Long, Guild> component1() {
                return this.allGuilds;
            }

            public final Channel component10() {
                return this.currentChannel;
            }

            public final long component11() {
                return this.currentChannelPermissions;
            }

            public final boolean component12() {
                return this.isOnCooldown;
            }

            public final Map<Long, Map<Long, Sticker>> component2() {
                return this.guildStickers;
            }

            public final List<ModelStickerPack> component3() {
                return this.enabledStickerPacks;
            }

            public final List<Long> component4() {
                return this.frequentlyUsedStickerIds;
            }

            public final String component5() {
                return this.searchInputStringUpper;
            }

            public final int component6() {
                return this.stickerAnimationSettings;
            }

            public final Long component7() {
                return this.selectedCategoryId;
            }

            public final MeUser component8() {
                return this.meUser;
            }

            public final boolean component9() {
                return this.isStickersSelectedTab;
            }

            public final Loaded copy(LinkedHashMap<Long, Guild> linkedHashMap, Map<Long, ? extends Map<Long, Sticker>> map, List<ModelStickerPack> list, List<Long> list2, String str, int i, Long l, MeUser meUser, boolean z2, Channel channel, long j, boolean z3) {
                m.checkNotNullParameter(linkedHashMap, "allGuilds");
                m.checkNotNullParameter(map, "guildStickers");
                m.checkNotNullParameter(list, "enabledStickerPacks");
                m.checkNotNullParameter(list2, "frequentlyUsedStickerIds");
                m.checkNotNullParameter(str, "searchInputStringUpper");
                m.checkNotNullParameter(meUser, "meUser");
                return new Loaded(linkedHashMap, map, list, list2, str, i, l, meUser, z2, channel, j, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.allGuilds, loaded.allGuilds) && m.areEqual(this.guildStickers, loaded.guildStickers) && m.areEqual(this.enabledStickerPacks, loaded.enabledStickerPacks) && m.areEqual(this.frequentlyUsedStickerIds, loaded.frequentlyUsedStickerIds) && m.areEqual(this.searchInputStringUpper, loaded.searchInputStringUpper) && this.stickerAnimationSettings == loaded.stickerAnimationSettings && m.areEqual(this.selectedCategoryId, loaded.selectedCategoryId) && m.areEqual(this.meUser, loaded.meUser) && this.isStickersSelectedTab == loaded.isStickersSelectedTab && m.areEqual(this.currentChannel, loaded.currentChannel) && this.currentChannelPermissions == loaded.currentChannelPermissions && this.isOnCooldown == loaded.isOnCooldown;
            }

            public final LinkedHashMap<Long, Guild> getAllGuilds() {
                return this.allGuilds;
            }

            public final Channel getCurrentChannel() {
                return this.currentChannel;
            }

            public final long getCurrentChannelPermissions() {
                return this.currentChannelPermissions;
            }

            public final List<ModelStickerPack> getEnabledStickerPacks() {
                return this.enabledStickerPacks;
            }

            public final List<Long> getFrequentlyUsedStickerIds() {
                return this.frequentlyUsedStickerIds;
            }

            public final Map<Long, Map<Long, Sticker>> getGuildStickers() {
                return this.guildStickers;
            }

            public final MeUser getMeUser() {
                return this.meUser;
            }

            public final String getSearchInputStringUpper() {
                return this.searchInputStringUpper;
            }

            public final Long getSelectedCategoryId() {
                return this.selectedCategoryId;
            }

            public final int getStickerAnimationSettings() {
                return this.stickerAnimationSettings;
            }

            public int hashCode() {
                LinkedHashMap<Long, Guild> linkedHashMap = this.allGuilds;
                int i = 0;
                int hashCode = (linkedHashMap != null ? linkedHashMap.hashCode() : 0) * 31;
                Map<Long, Map<Long, Sticker>> map = this.guildStickers;
                int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
                List<ModelStickerPack> list = this.enabledStickerPacks;
                int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
                List<Long> list2 = this.frequentlyUsedStickerIds;
                int hashCode4 = (hashCode3 + (list2 != null ? list2.hashCode() : 0)) * 31;
                String str = this.searchInputStringUpper;
                int hashCode5 = (((hashCode4 + (str != null ? str.hashCode() : 0)) * 31) + this.stickerAnimationSettings) * 31;
                Long l = this.selectedCategoryId;
                int hashCode6 = (hashCode5 + (l != null ? l.hashCode() : 0)) * 31;
                MeUser meUser = this.meUser;
                int hashCode7 = (hashCode6 + (meUser != null ? meUser.hashCode() : 0)) * 31;
                boolean z2 = this.isStickersSelectedTab;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode7 + i3) * 31;
                Channel channel = this.currentChannel;
                if (channel != null) {
                    i = channel.hashCode();
                }
                int a = (a0.a.a.b.a(this.currentChannelPermissions) + ((i5 + i) * 31)) * 31;
                boolean z3 = this.isOnCooldown;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                return a + i2;
            }

            public final boolean isOnCooldown() {
                return this.isOnCooldown;
            }

            public final boolean isStickersSelectedTab() {
                return this.isStickersSelectedTab;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(allGuilds=");
                R.append(this.allGuilds);
                R.append(", guildStickers=");
                R.append(this.guildStickers);
                R.append(", enabledStickerPacks=");
                R.append(this.enabledStickerPacks);
                R.append(", frequentlyUsedStickerIds=");
                R.append(this.frequentlyUsedStickerIds);
                R.append(", searchInputStringUpper=");
                R.append(this.searchInputStringUpper);
                R.append(", stickerAnimationSettings=");
                R.append(this.stickerAnimationSettings);
                R.append(", selectedCategoryId=");
                R.append(this.selectedCategoryId);
                R.append(", meUser=");
                R.append(this.meUser);
                R.append(", isStickersSelectedTab=");
                R.append(this.isStickersSelectedTab);
                R.append(", currentChannel=");
                R.append(this.currentChannel);
                R.append(", currentChannelPermissions=");
                R.append(this.currentChannelPermissions);
                R.append(", isOnCooldown=");
                return a.M(R, this.isOnCooldown, ")");
            }
        }

        /* compiled from: StickerPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState$Uninitialized;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends StoreState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: StickerPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0003\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;", "", "", "searchQuery", "Ljava/lang/String;", "getSearchQuery", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "EmptyNonPremium", "EmptySearchResults", "Stickers", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptyNonPremium;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {
        private final String searchQuery;

        /* compiled from: StickerPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00062\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\n\u0010\bR\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptyNonPremium;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;", "", "Lcom/discord/api/sticker/Sticker;", "component1", "()Ljava/util/List;", "", "component2", "()Z", "emptyStateStickers", "isStickersSelectedTab", "copy", "(Ljava/util/List;Z)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptyNonPremium;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/List;", "getEmptyStateStickers", HookHelper.constructorName, "(Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmptyNonPremium extends ViewState {
            private final List<Sticker> emptyStateStickers;
            private final boolean isStickersSelectedTab;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmptyNonPremium(List<Sticker> list, boolean z2) {
                super("", null);
                m.checkNotNullParameter(list, "emptyStateStickers");
                this.emptyStateStickers = list;
                this.isStickersSelectedTab = z2;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ EmptyNonPremium copy$default(EmptyNonPremium emptyNonPremium, List list, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = emptyNonPremium.emptyStateStickers;
                }
                if ((i & 2) != 0) {
                    z2 = emptyNonPremium.isStickersSelectedTab;
                }
                return emptyNonPremium.copy(list, z2);
            }

            public final List<Sticker> component1() {
                return this.emptyStateStickers;
            }

            public final boolean component2() {
                return this.isStickersSelectedTab;
            }

            public final EmptyNonPremium copy(List<Sticker> list, boolean z2) {
                m.checkNotNullParameter(list, "emptyStateStickers");
                return new EmptyNonPremium(list, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EmptyNonPremium)) {
                    return false;
                }
                EmptyNonPremium emptyNonPremium = (EmptyNonPremium) obj;
                return m.areEqual(this.emptyStateStickers, emptyNonPremium.emptyStateStickers) && this.isStickersSelectedTab == emptyNonPremium.isStickersSelectedTab;
            }

            public final List<Sticker> getEmptyStateStickers() {
                return this.emptyStateStickers;
            }

            public int hashCode() {
                List<Sticker> list = this.emptyStateStickers;
                int hashCode = (list != null ? list.hashCode() : 0) * 31;
                boolean z2 = this.isStickersSelectedTab;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public final boolean isStickersSelectedTab() {
                return this.isStickersSelectedTab;
            }

            public String toString() {
                StringBuilder R = a.R("EmptyNonPremium(emptyStateStickers=");
                R.append(this.emptyStateStickers);
                R.append(", isStickersSelectedTab=");
                return a.M(R, this.isStickersSelectedTab, ")");
            }
        }

        /* compiled from: StickerPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\bR\u001c\u0010\t\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "component2", "()Ljava/util/List;", "searchQuery", "categoryItems", "copy", "(Ljava/lang/String;Ljava/util/List;)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$EmptySearchResults;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getCategoryItems", "Ljava/lang/String;", "getSearchQuery", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmptySearchResults extends ViewState {
            private final List<StickerCategoryItem> categoryItems;
            private final String searchQuery;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public EmptySearchResults(String str, List<? extends StickerCategoryItem> list) {
                super(str, null);
                m.checkNotNullParameter(str, "searchQuery");
                m.checkNotNullParameter(list, "categoryItems");
                this.searchQuery = str;
                this.categoryItems = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ EmptySearchResults copy$default(EmptySearchResults emptySearchResults, String str, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = emptySearchResults.getSearchQuery();
                }
                if ((i & 2) != 0) {
                    list = emptySearchResults.categoryItems;
                }
                return emptySearchResults.copy(str, list);
            }

            public final String component1() {
                return getSearchQuery();
            }

            public final List<StickerCategoryItem> component2() {
                return this.categoryItems;
            }

            public final EmptySearchResults copy(String str, List<? extends StickerCategoryItem> list) {
                m.checkNotNullParameter(str, "searchQuery");
                m.checkNotNullParameter(list, "categoryItems");
                return new EmptySearchResults(str, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EmptySearchResults)) {
                    return false;
                }
                EmptySearchResults emptySearchResults = (EmptySearchResults) obj;
                return m.areEqual(getSearchQuery(), emptySearchResults.getSearchQuery()) && m.areEqual(this.categoryItems, emptySearchResults.categoryItems);
            }

            public final List<StickerCategoryItem> getCategoryItems() {
                return this.categoryItems;
            }

            @Override // com.discord.widgets.chat.input.sticker.StickerPickerViewModel.ViewState
            public String getSearchQuery() {
                return this.searchQuery;
            }

            public int hashCode() {
                String searchQuery = getSearchQuery();
                int i = 0;
                int hashCode = (searchQuery != null ? searchQuery.hashCode() : 0) * 31;
                List<StickerCategoryItem> list = this.categoryItems;
                if (list != null) {
                    i = list.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("EmptySearchResults(searchQuery=");
                R.append(getSearchQuery());
                R.append(", categoryItems=");
                return a.K(R, this.categoryItems, ")");
            }
        }

        /* compiled from: StickerPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\u0005\u0012\u0006\u0010\u0012\u001a\u00020\u000b\u0012\u0006\u0010\u0013\u001a\u00020\u000b¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0005HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJN\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\u00052\b\b\u0002\u0010\u0012\u001a\u00020\u000b2\b\b\u0002\u0010\u0013\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0004J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u000b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0012\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001e\u001a\u0004\b\u0012\u0010\rR\u0019\u0010\u0013\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001e\u001a\u0004\b\u0013\u0010\rR\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001f\u001a\u0004\b \u0010\bR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001f\u001a\u0004\b!\u0010\bR\u001c\u0010\u000f\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0004¨\u0006&"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;", "Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState;", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "component2", "()Ljava/util/List;", "Lcom/discord/widgets/chat/input/sticker/StickerCategoryItem;", "component3", "", "component4", "()Z", "component5", "searchQuery", "stickerItems", "categoryItems", "isStickersSelectedTab", "isOnCooldown", "copy", "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZ)Lcom/discord/widgets/chat/input/sticker/StickerPickerViewModel$ViewState$Stickers;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/List;", "getStickerItems", "getCategoryItems", "Ljava/lang/String;", "getSearchQuery", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Stickers extends ViewState {
            private final List<StickerCategoryItem> categoryItems;
            private final boolean isOnCooldown;
            private final boolean isStickersSelectedTab;
            private final String searchQuery;
            private final List<MGRecyclerDataPayload> stickerItems;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Stickers(String str, List<? extends MGRecyclerDataPayload> list, List<? extends StickerCategoryItem> list2, boolean z2, boolean z3) {
                super(str, null);
                m.checkNotNullParameter(str, "searchQuery");
                m.checkNotNullParameter(list, "stickerItems");
                m.checkNotNullParameter(list2, "categoryItems");
                this.searchQuery = str;
                this.stickerItems = list;
                this.categoryItems = list2;
                this.isStickersSelectedTab = z2;
                this.isOnCooldown = z3;
            }

            public static /* synthetic */ Stickers copy$default(Stickers stickers, String str, List list, List list2, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = stickers.getSearchQuery();
                }
                List<MGRecyclerDataPayload> list3 = list;
                if ((i & 2) != 0) {
                    list3 = stickers.stickerItems;
                }
                List list4 = list3;
                List<StickerCategoryItem> list5 = list2;
                if ((i & 4) != 0) {
                    list5 = stickers.categoryItems;
                }
                List list6 = list5;
                if ((i & 8) != 0) {
                    z2 = stickers.isStickersSelectedTab;
                }
                boolean z4 = z2;
                if ((i & 16) != 0) {
                    z3 = stickers.isOnCooldown;
                }
                return stickers.copy(str, list4, list6, z4, z3);
            }

            public final String component1() {
                return getSearchQuery();
            }

            public final List<MGRecyclerDataPayload> component2() {
                return this.stickerItems;
            }

            public final List<StickerCategoryItem> component3() {
                return this.categoryItems;
            }

            public final boolean component4() {
                return this.isStickersSelectedTab;
            }

            public final boolean component5() {
                return this.isOnCooldown;
            }

            public final Stickers copy(String str, List<? extends MGRecyclerDataPayload> list, List<? extends StickerCategoryItem> list2, boolean z2, boolean z3) {
                m.checkNotNullParameter(str, "searchQuery");
                m.checkNotNullParameter(list, "stickerItems");
                m.checkNotNullParameter(list2, "categoryItems");
                return new Stickers(str, list, list2, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Stickers)) {
                    return false;
                }
                Stickers stickers = (Stickers) obj;
                return m.areEqual(getSearchQuery(), stickers.getSearchQuery()) && m.areEqual(this.stickerItems, stickers.stickerItems) && m.areEqual(this.categoryItems, stickers.categoryItems) && this.isStickersSelectedTab == stickers.isStickersSelectedTab && this.isOnCooldown == stickers.isOnCooldown;
            }

            public final List<StickerCategoryItem> getCategoryItems() {
                return this.categoryItems;
            }

            @Override // com.discord.widgets.chat.input.sticker.StickerPickerViewModel.ViewState
            public String getSearchQuery() {
                return this.searchQuery;
            }

            public final List<MGRecyclerDataPayload> getStickerItems() {
                return this.stickerItems;
            }

            public int hashCode() {
                String searchQuery = getSearchQuery();
                int i = 0;
                int hashCode = (searchQuery != null ? searchQuery.hashCode() : 0) * 31;
                List<MGRecyclerDataPayload> list = this.stickerItems;
                int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
                List<StickerCategoryItem> list2 = this.categoryItems;
                if (list2 != null) {
                    i = list2.hashCode();
                }
                int i2 = (hashCode2 + i) * 31;
                boolean z2 = this.isStickersSelectedTab;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.isOnCooldown;
                if (!z3) {
                    i3 = z3 ? 1 : 0;
                }
                return i6 + i3;
            }

            public final boolean isOnCooldown() {
                return this.isOnCooldown;
            }

            public final boolean isStickersSelectedTab() {
                return this.isStickersSelectedTab;
            }

            public String toString() {
                StringBuilder R = a.R("Stickers(searchQuery=");
                R.append(getSearchQuery());
                R.append(", stickerItems=");
                R.append(this.stickerItems);
                R.append(", categoryItems=");
                R.append(this.categoryItems);
                R.append(", isStickersSelectedTab=");
                R.append(this.isStickersSelectedTab);
                R.append(", isOnCooldown=");
                return a.M(R, this.isOnCooldown, ")");
            }
        }

        private ViewState(String str) {
            this.searchQuery = str;
        }

        public String getSearchQuery() {
            return this.searchQuery;
        }

        public /* synthetic */ ViewState(String str, DefaultConstructorMarker defaultConstructorMarker) {
            this(str);
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ StickerPickerViewModel(rx.subjects.BehaviorSubject r24, rx.subjects.BehaviorSubject r25, java.util.Locale r26, com.discord.widgets.chat.MessageManager r27, com.discord.stores.StoreStickers r28, long r29, com.discord.stores.StoreChannels r31, com.discord.stores.StoreChannelsSelected r32, com.discord.stores.StorePermissions r33, com.discord.stores.StoreUser r34, rx.Observable r35, int r36, kotlin.jvm.internal.DefaultConstructorMarker r37) {
        /*
            r23 = this;
            r0 = r36
            r1 = r0 & 1
            if (r1 == 0) goto L12
            java.lang.String r1 = ""
            rx.subjects.BehaviorSubject r1 = rx.subjects.BehaviorSubject.l0(r1)
            java.lang.String r2 = "BehaviorSubject.create(\"\")"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)
            goto L14
        L12:
            r1 = r24
        L14:
            r2 = r0 & 2
            if (r2 == 0) goto L23
            r2 = 0
            rx.subjects.BehaviorSubject r2 = rx.subjects.BehaviorSubject.l0(r2)
            java.lang.String r3 = "BehaviorSubject.create(null as Long?)"
            d0.z.d.m.checkNotNullExpressionValue(r2, r3)
            goto L25
        L23:
            r2 = r25
        L25:
            r3 = r0 & 16
            if (r3 == 0) goto L32
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreStickers r3 = r3.getStickers()
            r18 = r3
            goto L34
        L32:
            r18 = r28
        L34:
            r3 = r0 & 64
            if (r3 == 0) goto L41
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r3 = r3.getChannels()
            r19 = r3
            goto L43
        L41:
            r19 = r31
        L43:
            r3 = r0 & 128(0x80, float:1.794E-43)
            if (r3 == 0) goto L50
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannelsSelected r3 = r3.getChannelsSelected()
            r20 = r3
            goto L52
        L50:
            r20 = r32
        L52:
            r3 = r0 & 256(0x100, float:3.59E-43)
            if (r3 == 0) goto L5f
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePermissions r3 = r3.getPermissions()
            r21 = r3
            goto L61
        L5f:
            r21 = r33
        L61:
            r3 = r0 & 512(0x200, float:7.175E-43)
            if (r3 == 0) goto L6e
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r3 = r3.getUsers()
            r22 = r3
            goto L70
        L6e:
            r22 = r34
        L70:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L8c
            com.discord.widgets.chat.input.sticker.StickerPickerViewModel$Companion r3 = com.discord.widgets.chat.input.sticker.StickerPickerViewModel.Companion
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 4092(0xffc, float:5.734E-42)
            r17 = 0
            r4 = r1
            r5 = r2
            rx.Observable r0 = com.discord.widgets.chat.input.sticker.StickerPickerViewModel.Companion.observeStoreState$default(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            r15 = r0
            goto L8e
        L8c:
            r15 = r35
        L8e:
            r3 = r23
            r4 = r1
            r5 = r2
            r6 = r26
            r7 = r27
            r8 = r18
            r9 = r29
            r11 = r19
            r12 = r20
            r13 = r21
            r14 = r22
            r3.<init>(r4, r5, r6, r7, r8, r9, r11, r12, r13, r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.sticker.StickerPickerViewModel.<init>(rx.subjects.BehaviorSubject, rx.subjects.BehaviorSubject, java.util.Locale, com.discord.widgets.chat.MessageManager, com.discord.stores.StoreStickers, long, com.discord.stores.StoreChannels, com.discord.stores.StoreChannelsSelected, com.discord.stores.StorePermissions, com.discord.stores.StoreUser, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final List<StickerCategoryItem> createCategoryItems(StoreState.Loaded loaded, List<? extends MGRecyclerDataPayload> list, List<Sticker> list2) {
        int i;
        Collection<Sticker> values;
        List list3;
        boolean z2;
        StoreState.Loaded loaded2;
        boolean z3;
        boolean z4;
        ArrayList arrayList = new ArrayList();
        Long selectedCategoryId = loaded.getSelectedCategoryId();
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        LinkedHashSet linkedHashSet2 = new LinkedHashSet();
        if ((loaded.getMeUser().getPremiumTier() != PremiumTier.TIER_2 && loaded.getGuildStickers().isEmpty()) || loaded.getEnabledStickerPacks().isEmpty()) {
            return n.emptyList();
        }
        if (list == null) {
            linkedHashSet.add(-1L);
            for (ModelStickerPack modelStickerPack : loaded.getEnabledStickerPacks()) {
                linkedHashSet.add(Long.valueOf(modelStickerPack.getId()));
            }
        } else {
            for (MGRecyclerDataPayload mGRecyclerDataPayload : list) {
                if (mGRecyclerDataPayload instanceof StickerItem) {
                    Sticker sticker = ((StickerItem) mGRecyclerDataPayload).getSticker();
                    if (!(list2 instanceof Collection) || !list2.isEmpty()) {
                        for (Sticker sticker2 : list2) {
                            if (sticker2.getId() == sticker.getId()) {
                                z4 = true;
                                continue;
                            } else {
                                z4 = false;
                                continue;
                            }
                            if (z4) {
                                z3 = true;
                                break;
                            }
                        }
                    }
                    z3 = false;
                    if (z3) {
                        linkedHashSet.add(-1L);
                    }
                    if (sticker.i() != null) {
                        Long i2 = sticker.i();
                        Objects.requireNonNull(i2, "null cannot be cast to non-null type kotlin.Long");
                        linkedHashSet.add(i2);
                    }
                    if (sticker.g() != null) {
                        Long g = sticker.g();
                        Objects.requireNonNull(g, "null cannot be cast to non-null type kotlin.Long");
                        linkedHashSet2.add(g);
                    }
                }
            }
        }
        if (!list2.isEmpty()) {
            i = list2.size() + 1 + 0;
            if (linkedHashSet.contains(-1L)) {
                arrayList.add(new StickerCategoryItem.RecentItem(selectedCategoryId != null && selectedCategoryId.longValue() == -1, new Pair(0, Integer.valueOf(i))));
            }
        } else {
            i = 0;
        }
        for (Map.Entry<Long, Guild> entry : loaded.getAllGuilds().entrySet()) {
            long longValue = entry.getKey().longValue();
            Guild value = entry.getValue();
            Map<Long, Sticker> map = loaded.getGuildStickers().get(Long.valueOf(longValue));
            if (map != null && (values = map.values()) != null && (list3 = u.toList(values)) != null) {
                long id2 = value.getId();
                if (selectedCategoryId != null && selectedCategoryId.longValue() == id2) {
                    loaded2 = loaded;
                    z2 = true;
                } else {
                    loaded2 = loaded;
                    z2 = false;
                }
                if (loaded2.canUseExternalStickersInCurrentChannel(longValue)) {
                    int size = list3.size() + 1 + i;
                    if (linkedHashSet2.contains(Long.valueOf(longValue))) {
                        arrayList.add(new StickerCategoryItem.GuildItem(value, list3, new Pair(Integer.valueOf(i), Integer.valueOf(size)), z2));
                    }
                    i = size;
                }
            }
        }
        if (loaded.getMeUser().getPremiumTier() == PremiumTier.TIER_2) {
            for (ModelStickerPack modelStickerPack2 : loaded.getEnabledStickerPacks()) {
                boolean z5 = selectedCategoryId != null && selectedCategoryId.longValue() == modelStickerPack2.getId();
                int size2 = modelStickerPack2.getStickers().size() + 1 + i;
                if (linkedHashSet.contains(Long.valueOf(modelStickerPack2.getId()))) {
                    arrayList.add(new StickerCategoryItem.PackItem(modelStickerPack2, new Pair(Integer.valueOf(i), Integer.valueOf(size2)), z5));
                }
                i = size2;
            }
        }
        return arrayList;
    }

    private final Map<Long, Sticker> getAllStickersById(StoreState.Loaded loaded) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (ModelStickerPack modelStickerPack : loaded.getEnabledStickerPacks()) {
            for (Sticker sticker : modelStickerPack.getStickers()) {
                linkedHashMap.put(Long.valueOf(sticker.getId()), sticker);
            }
        }
        Iterator<T> it = loaded.getGuildStickers().values().iterator();
        while (it.hasNext()) {
            for (Sticker sticker2 : ((Map) it.next()).values()) {
                linkedHashMap.put(Long.valueOf(sticker2.getId()), sticker2);
            }
        }
        return h0.toMap(linkedHashMap);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        if (storeState instanceof StoreState.Loaded) {
            StoreState.Loaded loaded = (StoreState.Loaded) storeState;
            Long valueOf = loaded.getSelectedCategoryId() == null ? Long.valueOf(this.initialStickerPackId) : null;
            handleStoreStateForPage(loaded);
            scrollToPackId(valueOf);
        }
    }

    private final void handleStoreStateForPage(StoreState.Loaded loaded) {
        boolean z2;
        boolean z3;
        ArrayList arrayList;
        Collection<Sticker> values;
        List list;
        String str;
        StoreState.Loaded loaded2 = loaded;
        Map<Long, Sticker> allStickersById = getAllStickersById(loaded);
        List<ModelStickerPack> enabledStickerPacks = loaded.getEnabledStickerPacks();
        int stickerAnimationSettings = loaded.getStickerAnimationSettings();
        String searchInputStringUpper = loaded.getSearchInputStringUpper();
        Locale locale = this.locale;
        Objects.requireNonNull(searchInputStringUpper, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = searchInputStringUpper.toLowerCase(locale);
        String str2 = "(this as java.lang.String).toLowerCase(locale)";
        m.checkNotNullExpressionValue(lowerCase, str2);
        List<Long> frequentlyUsedStickerIds = loaded.getFrequentlyUsedStickerIds();
        ArrayList arrayList2 = new ArrayList();
        for (Number number : frequentlyUsedStickerIds) {
            Sticker sticker = allStickersById.get(Long.valueOf(number.longValue()));
            if (sticker != null) {
                arrayList2.add(sticker);
            }
        }
        List<Sticker> arrayList3 = new ArrayList<>();
        Iterator it = arrayList2.iterator();
        while (true) {
            z2 = true;
            z3 = false;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (StickerUtils.INSTANCE.getStickerSendability((Sticker) next, loaded.getMeUser(), loaded.getCurrentChannel(), Long.valueOf(loaded.getCurrentChannelPermissions())) == StickerUtils.StickerSendability.NONSENDABLE) {
                z2 = false;
            }
            if (z2) {
                arrayList3.add(next);
            }
        }
        List<? extends MGRecyclerDataPayload> arrayList4 = new ArrayList<>();
        int i = 2;
        if (!arrayList3.isEmpty()) {
            for (Sticker sticker2 : arrayList3) {
                String h = sticker2.h();
                Locale locale2 = this.locale;
                Objects.requireNonNull(h, "null cannot be cast to non-null type java.lang.String");
                String lowerCase2 = h.toLowerCase(locale2);
                m.checkNotNullExpressionValue(lowerCase2, str2);
                if (!w.contains$default(lowerCase2, lowerCase, z3, i, (Object) null)) {
                    String j = sticker2.j();
                    Locale locale3 = this.locale;
                    Objects.requireNonNull(j, "null cannot be cast to non-null type java.lang.String");
                    String lowerCase3 = j.toLowerCase(locale3);
                    m.checkNotNullExpressionValue(lowerCase3, str2);
                    if (!w.contains$default(lowerCase3, lowerCase, z3, i, (Object) null)) {
                        str = str2;
                        str2 = str;
                        i = 2;
                        z3 = false;
                    }
                }
                str = str2;
                arrayList4.add(new StickerItem(sticker2, stickerAnimationSettings, StickerItem.Mode.OWNED, StickerUtils.INSTANCE.getStickerSendability(sticker2, loaded.getMeUser(), loaded.getCurrentChannel(), Long.valueOf(loaded.getCurrentChannelPermissions()))));
                str2 = str;
                i = 2;
                z3 = false;
            }
            if (!arrayList4.isEmpty()) {
                arrayList4.add(0, new HeaderItem(HeaderType.Recent.INSTANCE));
            }
        }
        for (Map.Entry<Long, Guild> entry : loaded.getAllGuilds().entrySet()) {
            long longValue = entry.getKey().longValue();
            Guild value = entry.getValue();
            Map<Long, Sticker> map = loaded.getGuildStickers().get(Long.valueOf(longValue));
            if (map == null || (values = map.values()) == null || (list = u.toList(values)) == null || !loaded2.canUseExternalStickersInCurrentChannel(longValue)) {
                arrayList = arrayList4;
            } else {
                arrayList = arrayList4;
                arrayList.addAll(Companion.buildGuildStickersListItems(list, value, stickerAnimationSettings, lowerCase, loaded.getMeUser(), this.locale, loaded.getCurrentChannel(), loaded.getCurrentChannelPermissions()));
            }
            arrayList4 = arrayList;
            loaded2 = loaded;
        }
        List<? extends MGRecyclerDataPayload> list2 = arrayList4;
        if (loaded.getMeUser().getPremiumTier() == PremiumTier.TIER_2) {
            for (ModelStickerPack modelStickerPack : enabledStickerPacks) {
                list2.addAll(Companion.buildStickerListItems(modelStickerPack, lowerCase, stickerAnimationSettings, this.locale, loaded.getMeUser()));
            }
        }
        List<StickerCategoryItem> createCategoryItems = createCategoryItems(loaded, list2, arrayList3);
        if (loaded.getMeUser().getPremiumTier() == PremiumTier.TIER_2 || !loaded.getGuildStickers().isEmpty()) {
            if (list2.isEmpty()) {
                if (lowerCase.length() <= 0) {
                    z2 = false;
                }
                if (z2) {
                    updateViewState(new ViewState.EmptySearchResults(loaded.getSearchInputStringUpper(), createCategoryItems));
                    return;
                }
            }
            updateViewState(new ViewState.Stickers(loaded.getSearchInputStringUpper(), list2, createCategoryItems, loaded.isStickersSelectedTab(), loaded.isOnCooldown()));
            return;
        }
        updateViewState(new ViewState.EmptyNonPremium(n.listOfNotNull((Object[]) new Sticker[]{this.stickersStore.getStickers().get(781323769960202280L), this.stickersStore.getStickers().get(809209266556764241L), this.stickersStore.getStickers().get(818597810047680532L), this.stickersStore.getStickers().get(819129296374595614L)}), loaded.isStickersSelectedTab()));
    }

    public final Observable<Event> observeEvents() {
        return this.eventSubject;
    }

    public final boolean onStickerSelected(Sticker sticker) {
        long id2;
        Channel findChannelById;
        m.checkNotNullParameter(sticker, "sticker");
        if ((getViewState() instanceof ViewState.Stickers) && (findChannelById = this.channelStore.findChannelById((id2 = this.channelSelectedStore.getId()))) != null) {
            Long l = this.permissionStore.getPermissionsByChannel().get(Long.valueOf(id2));
            boolean hasAccessWrite = PermissionUtils.INSTANCE.hasAccessWrite(findChannelById, l);
            StickerUtils.StickerSendability stickerSendability = StickerUtils.INSTANCE.getStickerSendability(sticker, this.userStore.getMe(), findChannelById, l);
            if (stickerSendability == StickerUtils.StickerSendability.SENDABLE_WITH_PREMIUM) {
                this.eventSubject.k.onNext(Event.ShowStickerPremiumUpsell.INSTANCE);
                return false;
            }
            ViewState viewState = getViewState();
            Objects.requireNonNull(viewState, "null cannot be cast to non-null type com.discord.widgets.chat.input.sticker.StickerPickerViewModel.ViewState.Stickers");
            if (((ViewState.Stickers) viewState).isOnCooldown()) {
                this.eventSubject.k.onNext(Event.SlowMode.INSTANCE);
                return false;
            } else if (hasAccessWrite && stickerSendability == StickerUtils.StickerSendability.SENDABLE) {
                this.stickersStore.onStickerUsed(sticker);
                MessageManager.sendMessage$default(this.messageManager, null, null, null, null, d0.t.m.listOf(sticker), false, null, null, null, 495, null);
                this.searchSubject.onNext("");
                return true;
            }
        }
        return false;
    }

    public final void scrollToPackId(Long l) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Stickers)) {
            viewState = null;
        }
        ViewState.Stickers stickers = (ViewState.Stickers) viewState;
        if (stickers != null && l != null) {
            l.longValue();
            int size = stickers.getStickerItems().size();
            int i = 0;
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    break;
                }
                MGRecyclerDataPayload mGRecyclerDataPayload = stickers.getStickerItems().get(i2);
                if (mGRecyclerDataPayload instanceof HeaderItem) {
                    HeaderItem headerItem = (HeaderItem) mGRecyclerDataPayload;
                    if ((headerItem.getHeaderType() instanceof HeaderType.PackItem) && ((HeaderType.PackItem) headerItem.getHeaderType()).getPack().getId() == l.longValue()) {
                        i = i2;
                        break;
                    }
                }
                i2++;
            }
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new Event.ScrollToStickerItemPosition(i));
        }
    }

    public final void setSearchText(String str) {
        m.checkNotNullParameter(str, "searchText");
        this.searchSubject.onNext(str);
    }

    public final void setSelectedCategoryId(long j) {
        this.selectedCategoryIdSubject.onNext(Long.valueOf(j));
    }

    public final void setStickerCountToDisplayForStore(int i) {
        this.stickerCountToDisplayForStore = i;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StickerPickerViewModel(BehaviorSubject<String> behaviorSubject, BehaviorSubject<Long> behaviorSubject2, Locale locale, MessageManager messageManager, StoreStickers storeStickers, long j, StoreChannels storeChannels, StoreChannelsSelected storeChannelsSelected, StorePermissions storePermissions, StoreUser storeUser, Observable<StoreState> observable) {
        super(null);
        m.checkNotNullParameter(behaviorSubject, "searchSubject");
        m.checkNotNullParameter(behaviorSubject2, "selectedCategoryIdSubject");
        m.checkNotNullParameter(locale, "locale");
        m.checkNotNullParameter(messageManager, "messageManager");
        m.checkNotNullParameter(storeStickers, "stickersStore");
        m.checkNotNullParameter(storeChannels, "channelStore");
        m.checkNotNullParameter(storeChannelsSelected, "channelSelectedStore");
        m.checkNotNullParameter(storePermissions, "permissionStore");
        m.checkNotNullParameter(storeUser, "userStore");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.searchSubject = behaviorSubject;
        this.selectedCategoryIdSubject = behaviorSubject2;
        this.locale = locale;
        this.messageManager = messageManager;
        this.stickersStore = storeStickers;
        this.initialStickerPackId = j;
        this.channelStore = storeChannels;
        this.channelSelectedStore = storeChannelsSelected;
        this.permissionStore = storePermissions;
        this.userStore = storeUser;
        PublishSubject<Event> k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.eventSubject = k0;
        this.stickerCountToDisplayForStore = 4;
        storeStickers.fetchEnabledStickerDirectory();
        Observable q = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "storeStateObservable\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
