package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.models.domain.ModelAuditLogEntry;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StickerPackStoreSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ:\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u000f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0012\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\u0007R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\nR\u0019\u0010\u000f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b \u0010\n¨\u0006#"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;", "", "Lcom/discord/api/sticker/Sticker;", "component1", "()Lcom/discord/api/sticker/Sticker;", "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;", "component2", "()Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;", "", "component3", "()Ljava/lang/String;", "component4", "sticker", "type", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "section", "copy", "(Lcom/discord/api/sticker/Sticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetAnalytics;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;", "getType", "Lcom/discord/api/sticker/Sticker;", "getSticker", "Ljava/lang/String;", "getLocation", "getSection", HookHelper.constructorName, "(Lcom/discord/api/sticker/Sticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerPackStoreSheetAnalytics {
    private final String location;
    private final String section;
    private final Sticker sticker;
    private final StickerPackStoreSheetViewType type;

    public StickerPackStoreSheetAnalytics(Sticker sticker, StickerPackStoreSheetViewType stickerPackStoreSheetViewType, String str, String str2) {
        m.checkNotNullParameter(sticker, "sticker");
        m.checkNotNullParameter(stickerPackStoreSheetViewType, "type");
        m.checkNotNullParameter(str2, "section");
        this.sticker = sticker;
        this.type = stickerPackStoreSheetViewType;
        this.location = str;
        this.section = str2;
    }

    public static /* synthetic */ StickerPackStoreSheetAnalytics copy$default(StickerPackStoreSheetAnalytics stickerPackStoreSheetAnalytics, Sticker sticker, StickerPackStoreSheetViewType stickerPackStoreSheetViewType, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            sticker = stickerPackStoreSheetAnalytics.sticker;
        }
        if ((i & 2) != 0) {
            stickerPackStoreSheetViewType = stickerPackStoreSheetAnalytics.type;
        }
        if ((i & 4) != 0) {
            str = stickerPackStoreSheetAnalytics.location;
        }
        if ((i & 8) != 0) {
            str2 = stickerPackStoreSheetAnalytics.section;
        }
        return stickerPackStoreSheetAnalytics.copy(sticker, stickerPackStoreSheetViewType, str, str2);
    }

    public final Sticker component1() {
        return this.sticker;
    }

    public final StickerPackStoreSheetViewType component2() {
        return this.type;
    }

    public final String component3() {
        return this.location;
    }

    public final String component4() {
        return this.section;
    }

    public final StickerPackStoreSheetAnalytics copy(Sticker sticker, StickerPackStoreSheetViewType stickerPackStoreSheetViewType, String str, String str2) {
        m.checkNotNullParameter(sticker, "sticker");
        m.checkNotNullParameter(stickerPackStoreSheetViewType, "type");
        m.checkNotNullParameter(str2, "section");
        return new StickerPackStoreSheetAnalytics(sticker, stickerPackStoreSheetViewType, str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StickerPackStoreSheetAnalytics)) {
            return false;
        }
        StickerPackStoreSheetAnalytics stickerPackStoreSheetAnalytics = (StickerPackStoreSheetAnalytics) obj;
        return m.areEqual(this.sticker, stickerPackStoreSheetAnalytics.sticker) && m.areEqual(this.type, stickerPackStoreSheetAnalytics.type) && m.areEqual(this.location, stickerPackStoreSheetAnalytics.location) && m.areEqual(this.section, stickerPackStoreSheetAnalytics.section);
    }

    public final String getLocation() {
        return this.location;
    }

    public final String getSection() {
        return this.section;
    }

    public final Sticker getSticker() {
        return this.sticker;
    }

    public final StickerPackStoreSheetViewType getType() {
        return this.type;
    }

    public int hashCode() {
        Sticker sticker = this.sticker;
        int i = 0;
        int hashCode = (sticker != null ? sticker.hashCode() : 0) * 31;
        StickerPackStoreSheetViewType stickerPackStoreSheetViewType = this.type;
        int hashCode2 = (hashCode + (stickerPackStoreSheetViewType != null ? stickerPackStoreSheetViewType.hashCode() : 0)) * 31;
        String str = this.location;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.section;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("StickerPackStoreSheetAnalytics(sticker=");
        R.append(this.sticker);
        R.append(", type=");
        R.append(this.type);
        R.append(", location=");
        R.append(this.location);
        R.append(", section=");
        return a.H(R, this.section, ")");
    }
}
