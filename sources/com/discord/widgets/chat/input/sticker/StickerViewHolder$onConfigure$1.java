package com.discord.widgets.chat.input.sticker;

import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Subscription;
/* compiled from: StickerAdapterViewHolders.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "scrollingWithinThreshold", "", "invoke", "(Ljava/lang/Boolean;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerViewHolder$onConfigure$1 extends o implements Function1<Boolean, Unit> {
    public final /* synthetic */ MGRecyclerDataPayload $data;
    public final /* synthetic */ StickerViewHolder this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StickerViewHolder$onConfigure$1(StickerViewHolder stickerViewHolder, MGRecyclerDataPayload mGRecyclerDataPayload) {
        super(1);
        this.this$0 = stickerViewHolder;
        this.$data = mGRecyclerDataPayload;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
        invoke2(bool);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Boolean bool) {
        Subscription subscription;
        m.checkNotNullExpressionValue(bool, "scrollingWithinThreshold");
        if (bool.booleanValue()) {
            this.this$0.configureSticker(this.$data);
            subscription = this.this$0.scrollingSpeedSubscription;
            if (subscription != null) {
                subscription.unsubscribe();
            }
            this.this$0.scrollingSpeedSubscription = null;
        }
    }
}
