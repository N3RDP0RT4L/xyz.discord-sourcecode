package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import b.i.a.f.e.o.f;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppComponent;
import com.discord.databinding.StickerPickerStickerItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.rlottie.RLottieImageView;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stickers.StickerUtils;
import com.discord.views.sticker.StickerView;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.Job;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: StickerAdapterViewHolders.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\n\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B1\u0012\u0006\u0010\u0011\u001a\u00020\b\u0012\u0006\u0010%\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u001d\u0012\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b&\u0010'J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001f\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\n\u0010\u000bJ\u0011\u0010\r\u001a\u0004\u0018\u00010\fH\u0014¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u000f\u001a\u00020\u0005¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0019\u0010\u0016\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR!\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"R\u0018\u0010#\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006("}, d2 = {"Lcom/discord/widgets/chat/input/sticker/StickerViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "data", "", "configureSticker", "(Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onConfigure", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "cancelLoading", "()V", "type", "I", "getType", "()I", "Lcom/discord/app/AppComponent;", "appComponent", "Lcom/discord/app/AppComponent;", "getAppComponent", "()Lcom/discord/app/AppComponent;", "Lcom/discord/databinding/StickerPickerStickerItemBinding;", "binding", "Lcom/discord/databinding/StickerPickerStickerItemBinding;", "Lrx/subjects/BehaviorSubject;", "", "recyclerScrollingWithinThresholdSubject", "Lrx/subjects/BehaviorSubject;", "getRecyclerScrollingWithinThresholdSubject", "()Lrx/subjects/BehaviorSubject;", "scrollingSpeedSubscription", "Lrx/Subscription;", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;Lrx/subjects/BehaviorSubject;Lcom/discord/app/AppComponent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerViewHolder extends MGRecyclerViewHolder<WidgetStickerAdapter, MGRecyclerDataPayload> {
    private final AppComponent appComponent;
    private final StickerPickerStickerItemBinding binding;
    private final BehaviorSubject<Boolean> recyclerScrollingWithinThresholdSubject;
    private Subscription scrollingSpeedSubscription;
    private final int type;

    public /* synthetic */ StickerViewHolder(int i, WidgetStickerAdapter widgetStickerAdapter, BehaviorSubject behaviorSubject, AppComponent appComponent, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, widgetStickerAdapter, (i2 & 4) != 0 ? null : behaviorSubject, appComponent);
    }

    public static final /* synthetic */ WidgetStickerAdapter access$getAdapter$p(StickerViewHolder stickerViewHolder) {
        return (WidgetStickerAdapter) stickerViewHolder.adapter;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureSticker(final MGRecyclerDataPayload mGRecyclerDataPayload) {
        Sticker sticker;
        StickerItem stickerItem = (StickerItem) (!(mGRecyclerDataPayload instanceof StickerItem) ? null : mGRecyclerDataPayload);
        if (stickerItem != null && (sticker = stickerItem.getSticker()) != null) {
            this.binding.f2136b.d(sticker, 0);
            StickerView stickerView = this.binding.f2136b;
            m.checkNotNullExpressionValue(stickerView, "binding.stickerPickerSticker");
            stickerView.setAlpha(((StickerItem) mGRecyclerDataPayload).getSendability() == StickerUtils.StickerSendability.SENDABLE ? 1.0f : 0.25f);
            this.binding.f2136b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.sticker.StickerViewHolder$configureSticker$1
                /* JADX WARN: Type inference failed for: r0v0, types: [com.discord.utilities.mg_recycler.MGRecyclerDataPayload, java.lang.Object] */
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StickerPickerStickerItemBinding stickerPickerStickerItemBinding;
                    StickerViewHolder.access$getAdapter$p(StickerViewHolder.this).getOnStickerItemSelected().invoke(mGRecyclerDataPayload);
                    try {
                        stickerPickerStickerItemBinding = StickerViewHolder.this.binding;
                        stickerPickerStickerItemBinding.f2136b.performHapticFeedback(3);
                    } catch (Throwable unused) {
                    }
                }
            });
        }
    }

    public final void cancelLoading() {
        StickerView stickerView = this.binding.f2136b;
        Job job = stickerView.m;
        if (job != null) {
            f.t(job, null, 1, null);
        }
        stickerView.j.c.clearAnimation();
        Subscription subscription = this.scrollingSpeedSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.scrollingSpeedSubscription = null;
    }

    public final AppComponent getAppComponent() {
        return this.appComponent;
    }

    public final BehaviorSubject<Boolean> getRecyclerScrollingWithinThresholdSubject() {
        return this.recyclerScrollingWithinThresholdSubject;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.binding.f2136b.getSubscription();
    }

    public final int getType() {
        return this.type;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StickerViewHolder(int i, WidgetStickerAdapter widgetStickerAdapter, BehaviorSubject<Boolean> behaviorSubject, AppComponent appComponent) {
        super((int) R.layout.sticker_picker_sticker_item, widgetStickerAdapter);
        m.checkNotNullParameter(widgetStickerAdapter, "adapter");
        m.checkNotNullParameter(appComponent, "appComponent");
        this.type = i;
        this.recyclerScrollingWithinThresholdSubject = behaviorSubject;
        this.appComponent = appComponent;
        View view = this.itemView;
        StickerView stickerView = (StickerView) view.findViewById(R.id.sticker_picker_sticker);
        if (stickerView != null) {
            StickerPickerStickerItemBinding stickerPickerStickerItemBinding = new StickerPickerStickerItemBinding((FrameLayout) view, stickerView);
            m.checkNotNullExpressionValue(stickerPickerStickerItemBinding, "StickerPickerStickerItemBinding.bind(itemView)");
            this.binding = stickerPickerStickerItemBinding;
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.sticker_picker_sticker)));
    }

    public void onConfigure(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
        m.checkNotNullParameter(mGRecyclerDataPayload, "data");
        super.onConfigure(i, (int) mGRecyclerDataPayload);
        if (this.recyclerScrollingWithinThresholdSubject != null) {
            StickerView stickerView = this.binding.f2136b;
            RLottieImageView rLottieImageView = stickerView.j.c;
            m.checkNotNullExpressionValue(rLottieImageView, "binding.stickerViewLottie");
            rLottieImageView.setVisibility(8);
            SimpleDraweeView simpleDraweeView = stickerView.j.f173b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.stickerViewImageview");
            simpleDraweeView.setVisibility(8);
            stickerView.j.f173b.setImageDrawable(null);
            ImageView imageView = stickerView.j.d;
            m.checkNotNullExpressionValue(imageView, "binding.stickerViewPlaceholder");
            imageView.setVisibility(0);
            Observable<Boolean> q = this.recyclerScrollingWithinThresholdSubject.q();
            m.checkNotNullExpressionValue(q, "recyclerScrollingWithinT…  .distinctUntilChanged()");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this.appComponent, null, 2, null), StickerViewHolder.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new StickerViewHolder$onConfigure$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new StickerViewHolder$onConfigure$1(this, mGRecyclerDataPayload));
            return;
        }
        configureSticker(mGRecyclerDataPayload);
    }
}
