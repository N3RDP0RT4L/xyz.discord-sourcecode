package com.discord.widgets.chat.input.sticker;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.databinding.ExpressionPickerHeaderItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter;
import com.discord.widgets.chat.input.sticker.HeaderType;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: StickerAdapterViewHolders.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B\u000f\u0012\u0006\u0010\u0012\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u000e\u0010\nR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/sticker/OwnedHeaderViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "bind", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Landroid/view/View;", "getItemView", "()Landroid/view/View;", "onConfigure", "Lcom/discord/databinding/ExpressionPickerHeaderItemBinding;", "binding", "Lcom/discord/databinding/ExpressionPickerHeaderItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/sticker/WidgetStickerAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class OwnedHeaderViewHolder extends MGRecyclerViewHolder<WidgetStickerAdapter, MGRecyclerDataPayload> implements WidgetExpressionPickerAdapter.StickyHeaderViewHolder {
    private final ExpressionPickerHeaderItemBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public OwnedHeaderViewHolder(WidgetStickerAdapter widgetStickerAdapter) {
        super((int) R.layout.expression_picker_header_item, widgetStickerAdapter);
        m.checkNotNullParameter(widgetStickerAdapter, "adapter");
        View view = this.itemView;
        Objects.requireNonNull(view, "rootView");
        TextView textView = (TextView) view;
        ExpressionPickerHeaderItemBinding expressionPickerHeaderItemBinding = new ExpressionPickerHeaderItemBinding(textView, textView);
        m.checkNotNullExpressionValue(expressionPickerHeaderItemBinding, "ExpressionPickerHeaderItemBinding.bind(itemView)");
        this.binding = expressionPickerHeaderItemBinding;
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter.StickyHeaderViewHolder
    public void bind(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
        m.checkNotNullParameter(mGRecyclerDataPayload, "data");
        onConfigure(i, mGRecyclerDataPayload);
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter.StickyHeaderViewHolder
    public View getItemView() {
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        return view;
    }

    public void onConfigure(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
        HeaderType headerType;
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        m.checkNotNullParameter(mGRecyclerDataPayload, "data");
        super.onConfigure(i, (int) mGRecyclerDataPayload);
        if (!(mGRecyclerDataPayload instanceof HeaderItem)) {
            mGRecyclerDataPayload = null;
        }
        HeaderItem headerItem = (HeaderItem) mGRecyclerDataPayload;
        if (headerItem != null && (headerType = headerItem.getHeaderType()) != null) {
            if (headerType instanceof HeaderType.PackItem) {
                TextView textView = this.binding.f2093b;
                m.checkNotNullExpressionValue(textView, "binding.headerItemText");
                HeaderType.PackItem packItem = (HeaderType.PackItem) headerType;
                textView.setText(packItem.getPack().getName());
                TextView textView2 = this.binding.f2093b;
                m.checkNotNullExpressionValue(textView2, "binding.headerItemText");
                b4 = b.b(a.x(this.itemView, "itemView", "itemView.context"), R.string.sticker_category_a11y_label, new Object[]{packItem.getPack().getName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                textView2.setContentDescription(b4);
            } else if (headerType instanceof HeaderType.Recent) {
                this.binding.f2093b.setText(R.string.sticker_picker_categories_recent);
                TextView textView3 = this.binding.f2093b;
                m.checkNotNullExpressionValue(textView3, "binding.headerItemText");
                TextView textView4 = this.binding.f2093b;
                m.checkNotNullExpressionValue(textView4, "binding.headerItemText");
                Context context = textView4.getContext();
                m.checkNotNullExpressionValue(context, "binding.headerItemText.context");
                TextView textView5 = this.binding.f2093b;
                m.checkNotNullExpressionValue(textView5, "binding.headerItemText");
                b3 = b.b(context, R.string.sticker_category_a11y_label, new Object[]{textView5.getText()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                textView3.setContentDescription(b3);
            } else if (headerType instanceof HeaderType.GuildItem) {
                TextView textView6 = this.binding.f2093b;
                m.checkNotNullExpressionValue(textView6, "binding.headerItemText");
                HeaderType.GuildItem guildItem = (HeaderType.GuildItem) headerType;
                textView6.setText(guildItem.getGuild().getName());
                TextView textView7 = this.binding.f2093b;
                m.checkNotNullExpressionValue(textView7, "binding.headerItemText");
                TextView textView8 = this.binding.f2093b;
                m.checkNotNullExpressionValue(textView8, "binding.headerItemText");
                Context context2 = textView8.getContext();
                m.checkNotNullExpressionValue(context2, "binding.headerItemText.context");
                b2 = b.b(context2, R.string.sticker_category_a11y_label, new Object[]{guildItem.getGuild().getName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                textView7.setContentDescription(b2);
            }
        }
    }
}
