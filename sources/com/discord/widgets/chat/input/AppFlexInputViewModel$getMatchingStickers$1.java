package com.discord.widgets.chat.input;

import com.discord.api.sticker.Sticker;
import com.discord.utilities.stickers.StickerUtils;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: AppFlexInputViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "inputText", "", "Lcom/discord/api/sticker/Sticker;", "invoke", "(Ljava/lang/String;)Ljava/util/Set;", "getStickerMatchesForInput"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AppFlexInputViewModel$getMatchingStickers$1 extends o implements Function1<String, Set<? extends Sticker>> {
    public static final AppFlexInputViewModel$getMatchingStickers$1 INSTANCE = new AppFlexInputViewModel$getMatchingStickers$1();

    public AppFlexInputViewModel$getMatchingStickers$1() {
        super(1);
    }

    public final Set<Sticker> invoke(String str) {
        m.checkNotNullParameter(str, "inputText");
        StickerUtils stickerUtils = StickerUtils.INSTANCE;
        return StickerUtils.findStickerMatches$default(stickerUtils, str, StickerUtils.getStickersForAutocomplete$default(stickerUtils, null, null, null, null, null, 31, null), false, 4, null);
    }
}
