package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.commands.Application;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetChatInputCategoriesAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\t\u0010\u0007¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/chat/input/CommandCategoryItem;", "", "Lcom/discord/models/commands/Application;", "component1", "()Lcom/discord/models/commands/Application;", "", "component2", "()Z", "application", "isSelected", "copy", "(Lcom/discord/models/commands/Application;Z)Lcom/discord/widgets/chat/input/CommandCategoryItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/commands/Application;", "getApplication", "Z", HookHelper.constructorName, "(Lcom/discord/models/commands/Application;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CommandCategoryItem {
    private final Application application;
    private final boolean isSelected;

    public CommandCategoryItem(Application application, boolean z2) {
        m.checkNotNullParameter(application, "application");
        this.application = application;
        this.isSelected = z2;
    }

    public static /* synthetic */ CommandCategoryItem copy$default(CommandCategoryItem commandCategoryItem, Application application, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            application = commandCategoryItem.application;
        }
        if ((i & 2) != 0) {
            z2 = commandCategoryItem.isSelected;
        }
        return commandCategoryItem.copy(application, z2);
    }

    public final Application component1() {
        return this.application;
    }

    public final boolean component2() {
        return this.isSelected;
    }

    public final CommandCategoryItem copy(Application application, boolean z2) {
        m.checkNotNullParameter(application, "application");
        return new CommandCategoryItem(application, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CommandCategoryItem)) {
            return false;
        }
        CommandCategoryItem commandCategoryItem = (CommandCategoryItem) obj;
        return m.areEqual(this.application, commandCategoryItem.application) && this.isSelected == commandCategoryItem.isSelected;
    }

    public final Application getApplication() {
        return this.application;
    }

    public int hashCode() {
        Application application = this.application;
        int hashCode = (application != null ? application.hashCode() : 0) * 31;
        boolean z2 = this.isSelected;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        return hashCode + i;
    }

    public final boolean isSelected() {
        return this.isSelected;
    }

    public String toString() {
        StringBuilder R = a.R("CommandCategoryItem(application=");
        R.append(this.application);
        R.append(", isSelected=");
        return a.M(R, this.isSelected, ")");
    }
}
