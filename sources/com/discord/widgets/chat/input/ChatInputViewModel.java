package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.api.guildjoinrequest.GuildJoinRequest;
import com.discord.api.message.MessageReference;
import com.discord.api.permission.Permission;
import com.discord.api.sticker.Sticker;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreApplicationCommandFrecency;
import com.discord.stores.StoreApplicationCommands;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreChat;
import com.discord.stores.StoreGuildJoinRequest;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreLurking;
import com.discord.stores.StoreMessagesLoader;
import com.discord.stores.StorePendingReplies;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreSlowMode;
import com.discord.stores.StoreStickers;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadDraft;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserRelationships;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.attachments.AttachmentUtilsKt;
import com.discord.utilities.guilds.GuildVerificationLevelUtils;
import com.discord.utilities.guilds.MemberVerificationUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.rest.SendUtilsKt;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.utilities.time.ClockFactory;
import com.discord.widgets.chat.MessageContent;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.ChatInputViewModel;
import com.discord.widgets.chat.input.autocomplete.Autocompletable;
import com.discord.widgets.chat.input.autocomplete.EmojiUpsellPlaceholder;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.discord.widgets.chat.input.models.ApplicationCommandDataKt;
import com.discord.widgets.user.account.WidgetUserAccountVerifyBase;
import com.discord.widgets.user.email.WidgetUserEmailVerify;
import com.discord.widgets.user.phone.WidgetUserPhoneManage;
import com.lytefast.flexinput.model.Attachment;
import d0.t.n;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function12;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: ChatInputViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 \u0080\u00012\b\u0012\u0004\u0012\u00020\u00020\u0001:\n\u0081\u0001\u0080\u0001\u0082\u0001\u0083\u0001\u0084\u0001B£\u0001\u0012\b\b\u0002\u0010u\u001a\u00020t\u0012\b\b\u0002\u0010f\u001a\u00020e\u0012\b\b\u0002\u0010i\u001a\u00020h\u0012\b\b\u0002\u0010]\u001a\u00020\\\u0012\b\b\u0002\u0010Z\u001a\u00020Y\u0012\b\b\u0002\u0010c\u001a\u00020b\u0012\b\b\u0002\u0010`\u001a\u00020_\u0012\b\b\u0002\u0010x\u001a\u00020w\u0012\b\b\u0002\u0010V\u001a\u00020U\u0012\b\b\u0002\u0010o\u001a\u00020n\u0012\b\b\u0002\u0010l\u001a\u00020k\u0012\b\b\u0002\u0010{\u001a\u00020z\u0012\b\b\u0002\u0010Q\u001a\u00020P\u0012\b\b\u0002\u0010S\u001a\u00020/\u0012\u000e\b\u0002\u0010}\u001a\b\u0012\u0004\u0012\u00020\u00030\u0011¢\u0006\u0004\b~\u0010\u007fJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0019\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0013\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011¢\u0006\u0004\b\u0013\u0010\u0014J\u0013\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00020\u0011¢\u0006\u0004\b\u0015\u0010\u0014J\u0015\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u0018\u0010\u0019J#\u0010\u001d\u001a\u00020\u001c*\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u001b0\u001a2\u0006\u0010\u0017\u001a\u00020\u0016¢\u0006\u0004\b\u001d\u0010\u001eJO\u0010(\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u00162\u000e\u0010 \u001a\n\u0018\u00010\u000ej\u0004\u0018\u0001`\u001f2\b\u0010\"\u001a\u0004\u0018\u00010!2\u0006\u0010$\u001a\u00020#2\u0016\b\u0002\u0010'\u001a\u0010\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u00020\u0005\u0018\u00010%¢\u0006\u0004\b(\u0010)JU\u00102\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010+\u001a\u00020*2\u0006\u0010-\u001a\u00020,2\u0010\u0010.\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u001b0\u001a2\b\b\u0002\u00100\u001a\u00020/2\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00020/\u0012\u0004\u0012\u00020\u00050%¢\u0006\u0004\b2\u00103J\u0015\u00106\u001a\u00020\u00052\u0006\u00105\u001a\u000204¢\u0006\u0004\b6\u00107Ja\u00109\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010+\u001a\u00020*2\u0006\u00105\u001a\u0002042\u0010\u0010.\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u001b0\u001a2\b\b\u0002\u00108\u001a\u00020/2\b\b\u0002\u00100\u001a\u00020/2\u0014\b\u0002\u00101\u001a\u000e\u0012\u0004\u0012\u00020/\u0012\u0004\u0012\u00020\u00050%¢\u0006\u0004\b9\u0010:J\r\u0010;\u001a\u00020\u0005¢\u0006\u0004\b;\u0010<J\u001d\u0010?\u001a\u00020\u00052\u0006\u0010>\u001a\u00020=2\u0006\u0010+\u001a\u00020*¢\u0006\u0004\b?\u0010@J\u0015\u0010C\u001a\u00020\u00052\u0006\u0010B\u001a\u00020A¢\u0006\u0004\bC\u0010DJ\r\u0010E\u001a\u00020\u0005¢\u0006\u0004\bE\u0010<J\r\u0010F\u001a\u00020\u0005¢\u0006\u0004\bF\u0010<J\r\u0010G\u001a\u00020\u0005¢\u0006\u0004\bG\u0010<J\u0015\u0010J\u001a\u00020\u00052\u0006\u0010I\u001a\u00020H¢\u0006\u0004\bJ\u0010KJ\u0015\u0010N\u001a\u00020/2\u0006\u0010M\u001a\u00020L¢\u0006\u0004\bN\u0010OR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u0016\u0010S\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010TR\u0016\u0010V\u001a\u00020U8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010WR\u0016\u0010X\u001a\u00020/8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bX\u0010TR\u0016\u0010Z\u001a\u00020Y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[R\u0016\u0010]\u001a\u00020\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR\u0016\u0010c\u001a\u00020b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bc\u0010dR\u0016\u0010f\u001a\u00020e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010gR\u0016\u0010i\u001a\u00020h8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010jR\u0016\u0010l\u001a\u00020k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010mR\u0016\u0010o\u001a\u00020n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bo\u0010pR\u001c\u0010r\u001a\b\u0012\u0004\u0012\u00020\u00120q8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\br\u0010sR\u0016\u0010u\u001a\u00020t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bu\u0010vR\u0016\u0010x\u001a\u00020w8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bx\u0010yR\u0016\u0010{\u001a\u00020z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b{\u0010|¨\u0006\u0085\u0001"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;)V", "Lcom/discord/stores/StoreChat$Event;", "event", "handleStoreChatEvent", "(Lcom/discord/stores/StoreChat$Event;)V", "Lcom/discord/models/member/GuildMember;", "guildMember", "", "calculateTimeoutLeftMs", "(Lcom/discord/models/member/GuildMember;)J", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", "observeEvents", "()Lrx/Observable;", "observeChatInputViewState", "Landroid/content/Context;", "context", "verifyAccount", "(Landroid/content/Context;)V", "", "Lcom/lytefast/flexinput/model/Attachment;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$AttachmentContext;", "toAttachmentContext", "(Ljava/util/List;Landroid/content/Context;)Lcom/discord/widgets/chat/input/ChatInputViewModel$AttachmentContext;", "Lcom/discord/primitives/MessageId;", "parentMessageId", "", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded;", "loadedViewState", "Lkotlin/Function1;", "Lcom/discord/api/channel/Channel;", "onThreadCreated", "createAndGotoThread", "(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/widgets/chat/MessageManager;", "messageManager", "Lcom/discord/widgets/chat/MessageContent;", "messageContent", "attachmentsRaw", "", "compressedImages", "onValidationResult", "sendMessage", "(Landroid/content/Context;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/widgets/chat/MessageContent;Ljava/util/List;ZLkotlin/jvm/functions/Function1;)V", "Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "applicationCommandData", "onCommandUsed", "(Lcom/discord/widgets/chat/input/models/ApplicationCommandData;)V", "autocomplete", "sendCommand", "(Landroid/content/Context;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/widgets/chat/input/models/ApplicationCommandData;Ljava/util/List;ZZLkotlin/jvm/functions/Function1;)V", "onCommandInputsInvalid", "()V", "Lcom/discord/api/sticker/Sticker;", "sticker", "sendSticker", "(Lcom/discord/api/sticker/Sticker;Lcom/discord/widgets/chat/MessageManager;)V", "Lcom/discord/api/message/MessageReference;", "messageReference", "jumpToMessageReference", "(Lcom/discord/api/message/MessageReference;)V", "togglePendingReplyShouldMention", "deletePendingReply", "deleteEditingMessage", "Landroidx/fragment/app/Fragment;", "fragment", "lurkGuild", "(Landroidx/fragment/app/Fragment;)V", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "item", "handleEmojiAutocompleteUpsellClicked", "(Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;)Z", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "isEmojiAutocompleteUpsellEnabled", "Z", "Lcom/discord/stores/StoreApplicationCommandFrecency;", "storeApplicationCommandsFrecency", "Lcom/discord/stores/StoreApplicationCommandFrecency;", "useTimeoutUpdateInterval", "Lcom/discord/stores/StoreStickers;", "storeStickers", "Lcom/discord/stores/StoreStickers;", "Lcom/discord/stores/StoreLurking;", "storeLurking", "Lcom/discord/stores/StoreLurking;", "Lcom/discord/stores/StoreApplicationInteractions;", "storeApplicationInteractions", "Lcom/discord/stores/StoreApplicationInteractions;", "Lcom/discord/stores/StorePendingReplies;", "storePendingReplies", "Lcom/discord/stores/StorePendingReplies;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreMessagesLoader;", "storeMessagesLoader", "Lcom/discord/stores/StoreMessagesLoader;", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreUserSettings;", "Lrx/subjects/PublishSubject;", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/StoreChat;", "storeChat", "Lcom/discord/stores/StoreChat;", "Lcom/discord/stores/StoreApplicationCommands;", "storeApplicationCommands", "Lcom/discord/stores/StoreApplicationCommands;", "Lcom/discord/stores/StoreThreadDraft;", "storeThreadDraft", "Lcom/discord/stores/StoreThreadDraft;", "storeStateObservable", HookHelper.constructorName, "(Lcom/discord/stores/StoreChat;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreMessagesLoader;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StorePendingReplies;Lcom/discord/stores/StoreApplicationInteractions;Lcom/discord/stores/StoreApplicationCommands;Lcom/discord/stores/StoreApplicationCommandFrecency;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreThreadDraft;Lcom/discord/stores/StoreGuilds;ZLrx/Observable;)V", "Companion", "AttachmentContext", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatInputViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final boolean isEmojiAutocompleteUpsellEnabled;
    private final StoreAnalytics storeAnalytics;
    private final StoreApplicationCommands storeApplicationCommands;
    private final StoreApplicationCommandFrecency storeApplicationCommandsFrecency;
    private final StoreApplicationInteractions storeApplicationInteractions;
    private final StoreChannels storeChannels;
    private final StoreChat storeChat;
    private final StoreGuilds storeGuilds;
    private final StoreLurking storeLurking;
    private final StoreMessagesLoader storeMessagesLoader;
    private final StorePendingReplies storePendingReplies;
    private final StoreStickers storeStickers;
    private final StoreThreadDraft storeThreadDraft;
    private final StoreUserSettings storeUserSettings;
    private boolean useTimeoutUpdateInterval;

    /* compiled from: ChatInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.ChatInputViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(ChatInputViewModel chatInputViewModel) {
            super(1, chatInputViewModel, ChatInputViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((ChatInputViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: ChatInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreChat$Event;", "p1", "", "invoke", "(Lcom/discord/stores/StoreChat$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.ChatInputViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<StoreChat.Event, Unit> {
        public AnonymousClass2(ChatInputViewModel chatInputViewModel) {
            super(1, chatInputViewModel, ChatInputViewModel.class, "handleStoreChatEvent", "handleStoreChatEvent(Lcom/discord/stores/StoreChat$Event;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreChat.Event event) {
            invoke2(event);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreChat.Event event) {
            m.checkNotNullParameter(event, "p1");
            ((ChatInputViewModel) this.receiver).handleStoreChatEvent(event);
        }
    }

    /* compiled from: ChatInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0010\u0007\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001B]\u0012\u001e\u0010\u0013\u001a\u001a\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002j\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0003`\u0004\u0012\u0006\u0010\u0014\u001a\u00020\u0007\u0012\u0006\u0010\u0015\u001a\u00020\u0007\u0012\u0006\u0010\u0016\u001a\u00020\u0007\u0012\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f\u0012\u0006\u0010\u0018\u001a\u00020\r\u0012\u0006\u0010\u0019\u001a\u00020\r¢\u0006\u0004\b0\u00101J(\u0010\u0005\u001a\u001a\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002j\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0003`\u0004HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\n\u0010\tJ\u0010\u0010\u000b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011Jt\u0010\u001a\u001a\u00020\u00002 \b\u0002\u0010\u0013\u001a\u001a\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002j\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0003`\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u00072\b\b\u0002\u0010\u0015\u001a\u00020\u00072\b\b\u0002\u0010\u0016\u001a\u00020\u00072\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f2\b\b\u0002\u0010\u0018\u001a\u00020\r2\b\b\u0002\u0010\u0019\u001a\u00020\rHÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010#\u001a\u00020\u00072\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u0019\u0010\u0015\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010%\u001a\u0004\b&\u0010\tR\u0019\u0010\u0014\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010%\u001a\u0004\b'\u0010\tR\u0019\u0010\u0019\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010(\u001a\u0004\b)\u0010\u0011R\u0019\u0010\u0018\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b*\u0010\u0011R1\u0010\u0013\u001a\u001a\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u0002j\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0003`\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010+\u001a\u0004\b,\u0010\u0006R\u0019\u0010\u0016\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010%\u001a\u0004\b-\u0010\tR\u001f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010.\u001a\u0004\b/\u0010\u000f¨\u00062"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$AttachmentContext;", "", "Ljava/util/ArrayList;", "Lcom/lytefast/flexinput/model/Attachment;", "Lkotlin/collections/ArrayList;", "component1", "()Ljava/util/ArrayList;", "", "component2", "()Z", "component3", "component4", "", "", "component5", "()Ljava/util/List;", "component6", "()F", "component7", "attachments", "hasImage", "hasVideo", "hasGif", "attachmentSizes", "currentFileSizeMB", "maxAttachmentSizeMB", "copy", "(Ljava/util/ArrayList;ZZZLjava/util/List;FF)Lcom/discord/widgets/chat/input/ChatInputViewModel$AttachmentContext;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getHasVideo", "getHasImage", "F", "getMaxAttachmentSizeMB", "getCurrentFileSizeMB", "Ljava/util/ArrayList;", "getAttachments", "getHasGif", "Ljava/util/List;", "getAttachmentSizes", HookHelper.constructorName, "(Ljava/util/ArrayList;ZZZLjava/util/List;FF)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AttachmentContext {
        private final List<Float> attachmentSizes;
        private final ArrayList<Attachment<?>> attachments;
        private final float currentFileSizeMB;
        private final boolean hasGif;
        private final boolean hasImage;
        private final boolean hasVideo;
        private final float maxAttachmentSizeMB;

        public AttachmentContext(ArrayList<Attachment<?>> arrayList, boolean z2, boolean z3, boolean z4, List<Float> list, float f, float f2) {
            m.checkNotNullParameter(arrayList, "attachments");
            m.checkNotNullParameter(list, "attachmentSizes");
            this.attachments = arrayList;
            this.hasImage = z2;
            this.hasVideo = z3;
            this.hasGif = z4;
            this.attachmentSizes = list;
            this.currentFileSizeMB = f;
            this.maxAttachmentSizeMB = f2;
        }

        public static /* synthetic */ AttachmentContext copy$default(AttachmentContext attachmentContext, ArrayList arrayList, boolean z2, boolean z3, boolean z4, List list, float f, float f2, int i, Object obj) {
            ArrayList<Attachment<?>> arrayList2 = arrayList;
            if ((i & 1) != 0) {
                arrayList2 = attachmentContext.attachments;
            }
            if ((i & 2) != 0) {
                z2 = attachmentContext.hasImage;
            }
            boolean z5 = z2;
            if ((i & 4) != 0) {
                z3 = attachmentContext.hasVideo;
            }
            boolean z6 = z3;
            if ((i & 8) != 0) {
                z4 = attachmentContext.hasGif;
            }
            boolean z7 = z4;
            List<Float> list2 = list;
            if ((i & 16) != 0) {
                list2 = attachmentContext.attachmentSizes;
            }
            List list3 = list2;
            if ((i & 32) != 0) {
                f = attachmentContext.currentFileSizeMB;
            }
            float f3 = f;
            if ((i & 64) != 0) {
                f2 = attachmentContext.maxAttachmentSizeMB;
            }
            return attachmentContext.copy(arrayList2, z5, z6, z7, list3, f3, f2);
        }

        public final ArrayList<Attachment<?>> component1() {
            return this.attachments;
        }

        public final boolean component2() {
            return this.hasImage;
        }

        public final boolean component3() {
            return this.hasVideo;
        }

        public final boolean component4() {
            return this.hasGif;
        }

        public final List<Float> component5() {
            return this.attachmentSizes;
        }

        public final float component6() {
            return this.currentFileSizeMB;
        }

        public final float component7() {
            return this.maxAttachmentSizeMB;
        }

        public final AttachmentContext copy(ArrayList<Attachment<?>> arrayList, boolean z2, boolean z3, boolean z4, List<Float> list, float f, float f2) {
            m.checkNotNullParameter(arrayList, "attachments");
            m.checkNotNullParameter(list, "attachmentSizes");
            return new AttachmentContext(arrayList, z2, z3, z4, list, f, f2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AttachmentContext)) {
                return false;
            }
            AttachmentContext attachmentContext = (AttachmentContext) obj;
            return m.areEqual(this.attachments, attachmentContext.attachments) && this.hasImage == attachmentContext.hasImage && this.hasVideo == attachmentContext.hasVideo && this.hasGif == attachmentContext.hasGif && m.areEqual(this.attachmentSizes, attachmentContext.attachmentSizes) && Float.compare(this.currentFileSizeMB, attachmentContext.currentFileSizeMB) == 0 && Float.compare(this.maxAttachmentSizeMB, attachmentContext.maxAttachmentSizeMB) == 0;
        }

        public final List<Float> getAttachmentSizes() {
            return this.attachmentSizes;
        }

        public final ArrayList<Attachment<?>> getAttachments() {
            return this.attachments;
        }

        public final float getCurrentFileSizeMB() {
            return this.currentFileSizeMB;
        }

        public final boolean getHasGif() {
            return this.hasGif;
        }

        public final boolean getHasImage() {
            return this.hasImage;
        }

        public final boolean getHasVideo() {
            return this.hasVideo;
        }

        public final float getMaxAttachmentSizeMB() {
            return this.maxAttachmentSizeMB;
        }

        public int hashCode() {
            ArrayList<Attachment<?>> arrayList = this.attachments;
            int i = 0;
            int hashCode = (arrayList != null ? arrayList.hashCode() : 0) * 31;
            boolean z2 = this.hasImage;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            boolean z3 = this.hasVideo;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            boolean z4 = this.hasGif;
            if (!z4) {
                i2 = z4 ? 1 : 0;
            }
            int i9 = (i8 + i2) * 31;
            List<Float> list = this.attachmentSizes;
            if (list != null) {
                i = list.hashCode();
            }
            return Float.floatToIntBits(this.maxAttachmentSizeMB) + ((Float.floatToIntBits(this.currentFileSizeMB) + ((i9 + i) * 31)) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("AttachmentContext(attachments=");
            R.append(this.attachments);
            R.append(", hasImage=");
            R.append(this.hasImage);
            R.append(", hasVideo=");
            R.append(this.hasVideo);
            R.append(", hasGif=");
            R.append(this.hasGif);
            R.append(", attachmentSizes=");
            R.append(this.attachmentSizes);
            R.append(", currentFileSizeMB=");
            R.append(this.currentFileSizeMB);
            R.append(", maxAttachmentSizeMB=");
            R.append(this.maxAttachmentSizeMB);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: ChatInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b+\u0010,J1\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\fJG\u0010\u0013\u001a&\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011 \u0012*\u0012\u0012\f\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011\u0018\u00010\t0\t2\n\u0010\u000e\u001a\u00060\u0002j\u0002`\r2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J+\u0010\u0018\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00170\t2\n\u0010\u000e\u001a\u00060\u0002j\u0002`\r2\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0018\u0010\u0019Jm\u0010)\u001a\b\u0012\u0004\u0012\u00020(0\t2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020 2\u0006\u0010#\u001a\u00020\"2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010%\u001a\u00020$2\u0006\u0010'\u001a\u00020&H\u0002¢\u0006\u0004\b)\u0010*¨\u0006-"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lrx/Observable;", "Lcom/discord/api/guild/GuildVerificationLevel;", "getVerificationLevelTriggeredObservable", "(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;)Lrx/Observable;", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreSlowMode;", "storeSlowMode", "", "kotlin.jvm.PlatformType", "getIsOnCooldownObservable", "(JLcom/discord/stores/StoreSlowMode;)Lrx/Observable;", "Lcom/discord/stores/StorePendingReplies;", "storePendingReplies", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;", "getPendingReplyStateObservable", "(JLcom/discord/stores/StorePendingReplies;)Lrx/Observable;", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreChat;", "storeChat", "Lcom/discord/stores/StoreUserRelationships;", "storeUserRelationships", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StoreLurking;", "storeLurking", "Lcom/discord/stores/StoreGuildJoinRequest;", "storeGuildJoinRequest", "Lcom/discord/stores/StoreThreadDraft;", "storeThreadDraft", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChat;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreSlowMode;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePendingReplies;Lcom/discord/stores/StoreGuildJoinRequest;Lcom/discord/stores/StoreThreadDraft;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Boolean> getIsOnCooldownObservable(long j, StoreSlowMode storeSlowMode) {
            return storeSlowMode.observeCooldownSecs(Long.valueOf(j), StoreSlowMode.Type.MessageSend.INSTANCE).F(ChatInputViewModel$Companion$getIsOnCooldownObservable$1.INSTANCE).q();
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState.Loaded.PendingReply> getPendingReplyStateObservable(long j, StorePendingReplies storePendingReplies) {
            Observable<StoreState.Loaded.PendingReply> q = storePendingReplies.observePendingReply(j).Y(ChatInputViewModel$Companion$getPendingReplyStateObservable$1.INSTANCE).q();
            m.checkNotNullExpressionValue(q, "storePendingReplies\n    …  .distinctUntilChanged()");
            return q;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<GuildVerificationLevel> getVerificationLevelTriggeredObservable(long j, StoreGuilds storeGuilds, StoreUser storeUser) {
            return GuildVerificationLevelUtils.observeVerificationLevelTriggered$default(GuildVerificationLevelUtils.INSTANCE, j, storeGuilds, storeUser, null, 8, null);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(StoreChannelsSelected storeChannelsSelected, final StoreUser storeUser, final StoreChat storeChat, final StoreUserRelationships storeUserRelationships, final StorePermissions storePermissions, final StoreLurking storeLurking, final StoreSlowMode storeSlowMode, final StoreGuilds storeGuilds, final StorePendingReplies storePendingReplies, final StoreGuildJoinRequest storeGuildJoinRequest, final StoreThreadDraft storeThreadDraft) {
            Observable Y = storeChannelsSelected.observeResolvedSelectedChannel().Y(new b<StoreChannelsSelected.ResolvedSelectedChannel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.chat.input.ChatInputViewModel$Companion$observeStoreState$1

                /* compiled from: ChatInputViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u000e\u0010\u0006\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00052\u000e\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u000e\u0010\u000f\u001a\n \u000e*\u0004\u0018\u00010\f0\f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\b\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u000e\u0010\u0016\u001a\n\u0018\u00010\u0014j\u0004\u0018\u0001`\u00152\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u001a\u001a\u00020\u0019H\n¢\u0006\u0004\b\u001c\u0010\u001d"}, d2 = {"Lcom/discord/models/user/MeUser;", "me", "Lcom/discord/stores/StoreChat$EditingMessage;", "editing", "", "Lcom/discord/primitives/RelationshipType;", "relationshipType", "", "Lcom/discord/api/permission/PermissionBit;", "channelPermissions", "Lcom/discord/api/guild/GuildVerificationLevel;", "verificationLevelTriggered", "", "isLurking", "kotlin.jvm.PlatformType", "isOnCooldown", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;", "pendingReply", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "meGuildMember", "Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "guildJoinRequest", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "threadDraftState", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded;", "invoke", "(Lcom/discord/models/user/MeUser;Lcom/discord/stores/StoreChat$EditingMessage;Ljava/lang/Integer;Ljava/lang/Long;Lcom/discord/api/guild/GuildVerificationLevel;ZLjava/lang/Boolean;Lcom/discord/models/guild/Guild;Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;Lcom/discord/models/member/GuildMember;Lcom/discord/api/guildjoinrequest/GuildJoinRequest;Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;)Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.input.ChatInputViewModel$Companion$observeStoreState$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function12<MeUser, StoreChat.EditingMessage, Integer, Long, GuildVerificationLevel, Boolean, Boolean, Guild, ChatInputViewModel.StoreState.Loaded.PendingReply, GuildMember, GuildJoinRequest, StoreThreadDraft.ThreadDraftState, ChatInputViewModel.StoreState.Loaded> {
                    public final /* synthetic */ Channel $channel;
                    public final /* synthetic */ StoreChannelsSelected.ResolvedSelectedChannel $selectedChannel;

                    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                    public AnonymousClass1(Channel channel, StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel) {
                        super(12);
                        this.$channel = channel;
                        this.$selectedChannel = resolvedSelectedChannel;
                    }

                    @Override // kotlin.jvm.functions.Function12
                    public /* bridge */ /* synthetic */ ChatInputViewModel.StoreState.Loaded invoke(MeUser meUser, StoreChat.EditingMessage editingMessage, Integer num, Long l, GuildVerificationLevel guildVerificationLevel, Boolean bool, Boolean bool2, Guild guild, ChatInputViewModel.StoreState.Loaded.PendingReply pendingReply, GuildMember guildMember, GuildJoinRequest guildJoinRequest, StoreThreadDraft.ThreadDraftState threadDraftState) {
                        return invoke(meUser, editingMessage, num, l, guildVerificationLevel, bool.booleanValue(), bool2, guild, pendingReply, guildMember, guildJoinRequest, threadDraftState);
                    }

                    public final ChatInputViewModel.StoreState.Loaded invoke(MeUser meUser, StoreChat.EditingMessage editingMessage, Integer num, Long l, GuildVerificationLevel guildVerificationLevel, boolean z2, Boolean bool, Guild guild, ChatInputViewModel.StoreState.Loaded.PendingReply pendingReply, GuildMember guildMember, GuildJoinRequest guildJoinRequest, StoreThreadDraft.ThreadDraftState threadDraftState) {
                        m.checkNotNullParameter(meUser, "me");
                        m.checkNotNullParameter(guildVerificationLevel, "verificationLevelTriggered");
                        m.checkNotNullParameter(threadDraftState, "threadDraftState");
                        Channel channel = this.$channel;
                        m.checkNotNullExpressionValue(bool, "isOnCooldown");
                        boolean booleanValue = bool.booleanValue();
                        StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel = this.$selectedChannel;
                        if (!(resolvedSelectedChannel instanceof StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft)) {
                            resolvedSelectedChannel = null;
                        }
                        return new ChatInputViewModel.StoreState.Loaded(channel, meUser, editingMessage, num, l, guildVerificationLevel, z2, booleanValue, guild, pendingReply, (StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft) resolvedSelectedChannel, guildMember, guildJoinRequest, threadDraftState);
                    }
                }

                public final Observable<? extends ChatInputViewModel.StoreState> call(StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel) {
                    Observable verificationLevelTriggeredObservable;
                    Observable isOnCooldownObservable;
                    Observable pendingReplyStateObservable;
                    Channel channelOrParent = resolvedSelectedChannel.getChannelOrParent();
                    if (channelOrParent == null) {
                        return new j0.l.e.k(ChatInputViewModel.StoreState.Loading.INSTANCE);
                    }
                    Observable<MeUser> observeMe = StoreUser.this.observeMe(true);
                    Observable computationBuffered = ObservableExtensionsKt.computationBuffered(storeChat.observeEditingMessage());
                    StoreUserRelationships storeUserRelationships2 = storeUserRelationships;
                    User a = ChannelUtils.a(channelOrParent);
                    Observable<Integer> observe = storeUserRelationships2.observe(a != null ? a.getId() : 0L);
                    Observable<Long> observePermissionsForChannel = storePermissions.observePermissionsForChannel(channelOrParent.h());
                    ChatInputViewModel.Companion companion = ChatInputViewModel.Companion;
                    verificationLevelTriggeredObservable = companion.getVerificationLevelTriggeredObservable(channelOrParent.f(), storeGuilds, StoreUser.this);
                    Observable<Boolean> isLurkingObs = storeLurking.isLurkingObs(channelOrParent.f());
                    isOnCooldownObservable = companion.getIsOnCooldownObservable(channelOrParent.h(), storeSlowMode);
                    m.checkNotNullExpressionValue(isOnCooldownObservable, "getIsOnCooldownObservabl…hannel.id, storeSlowMode)");
                    Observable<Guild> observeFromChannelId = storeGuilds.observeFromChannelId(channelOrParent.h());
                    pendingReplyStateObservable = companion.getPendingReplyStateObservable(channelOrParent.h(), storePendingReplies);
                    return ObservableCombineLatestOverloadsKt.combineLatest(observeMe, computationBuffered, observe, observePermissionsForChannel, verificationLevelTriggeredObservable, isLurkingObs, isOnCooldownObservable, observeFromChannelId, pendingReplyStateObservable, storeGuilds.observeComputedMember(channelOrParent.f(), StoreUser.this.getMe().getId()), storeGuildJoinRequest.observeGuildJoinRequest(channelOrParent.f()), storeThreadDraft.observeDraftState(), new AnonymousClass1(channelOrParent, resolvedSelectedChannel));
                }
            });
            m.checkNotNullExpressionValue(Y, "storeChannelsSelected.ob…      }\n        }\n      }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ChatInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\t\u0004\u0005\u0006\u0007\b\t\n\u000b\fB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\t\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", "", HookHelper.constructorName, "()V", "AppendChatText", "CommandInputsInvalid", "EmptyThreadName", "FailedDeliveryToRecipient", "FilesTooLarge", "MessageTooLong", "SetChatText", "ShowPremiumUpsell", "ThreadDraftClosed", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$FilesTooLarge;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$MessageTooLong;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$EmptyThreadName;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$AppendChatText;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$SetChatText;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$CommandInputsInvalid;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$FailedDeliveryToRecipient;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$ShowPremiumUpsell;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$ThreadDraftClosed;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$AppendChatText;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", "", "component1", "()Ljava/lang/String;", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "copy", "(Ljava/lang/String;)Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$AppendChatText;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getText", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AppendChatText extends Event {
            private final String text;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public AppendChatText(String str) {
                super(null);
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                this.text = str;
            }

            public static /* synthetic */ AppendChatText copy$default(AppendChatText appendChatText, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = appendChatText.text;
                }
                return appendChatText.copy(str);
            }

            public final String component1() {
                return this.text;
            }

            public final AppendChatText copy(String str) {
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                return new AppendChatText(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof AppendChatText) && m.areEqual(this.text, ((AppendChatText) obj).text);
                }
                return true;
            }

            public final String getText() {
                return this.text;
            }

            public int hashCode() {
                String str = this.text;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("AppendChatText(text="), this.text, ")");
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$CommandInputsInvalid;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CommandInputsInvalid extends Event {
            public static final CommandInputsInvalid INSTANCE = new CommandInputsInvalid();

            private CommandInputsInvalid() {
                super(null);
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$EmptyThreadName;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmptyThreadName extends Event {
            public static final EmptyThreadName INSTANCE = new EmptyThreadName();

            private EmptyThreadName() {
                super(null);
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$FailedDeliveryToRecipient;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class FailedDeliveryToRecipient extends Event {
            public static final FailedDeliveryToRecipient INSTANCE = new FailedDeliveryToRecipient();

            private FailedDeliveryToRecipient() {
                super(null);
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001Ba\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0006\u0010\u0018\u001a\u00020\u0005\u0012\u0006\u0010\u0019\u001a\u00020\u0005\u0012\u0006\u0010\u001a\u001a\u00020\t\u0012\u0010\u0010\u001b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\r0\f\u0012\u0006\u0010\u001c\u001a\u00020\t\u0012\u0006\u0010\u001d\u001a\u00020\t\u0012\u0006\u0010\u001e\u001a\u00020\t\u0012\u000e\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013¢\u0006\u0004\b7\u00108J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\r0\fHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000bJ\u0010\u0010\u0011\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000bJ\u0010\u0010\u0012\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000bJ\u0018\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J|\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0017\u001a\u00020\u00022\b\b\u0002\u0010\u0018\u001a\u00020\u00052\b\b\u0002\u0010\u0019\u001a\u00020\u00052\b\b\u0002\u0010\u001a\u001a\u00020\t2\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\r0\f2\b\b\u0002\u0010\u001c\u001a\u00020\t2\b\b\u0002\u0010\u001d\u001a\u00020\t2\b\b\u0002\u0010\u001e\u001a\u00020\t2\u0010\b\u0002\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013HÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010%\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b%\u0010\u0004J\u001a\u0010(\u001a\u00020\t2\b\u0010'\u001a\u0004\u0018\u00010&HÖ\u0003¢\u0006\u0004\b(\u0010)R#\u0010\u001b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010*\u001a\u0004\b+\u0010\u000fR\u0019\u0010\u001d\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010,\u001a\u0004\b-\u0010\u000bR\u0019\u0010\u001a\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010,\u001a\u0004\b\u001a\u0010\u000bR\u0019\u0010\u001e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010,\u001a\u0004\b.\u0010\u000bR\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010/\u001a\u0004\b0\u0010\u0004R\u0019\u0010\u0019\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00101\u001a\u0004\b2\u0010\u0007R\u0019\u0010\u001c\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010,\u001a\u0004\b3\u0010\u000bR!\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00104\u001a\u0004\b5\u0010\u0016R\u0019\u0010\u0018\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00101\u001a\u0004\b6\u0010\u0007¨\u00069"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$FilesTooLarge;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", "", "component1", "()I", "", "component2", "()F", "component3", "", "component4", "()Z", "", "Lcom/lytefast/flexinput/model/Attachment;", "component5", "()Ljava/util/List;", "component6", "component7", "component8", "Lkotlin/Function0;", "", "component9", "()Lkotlin/jvm/functions/Function0;", "maxFileSizeMB", "currentFileSizeMB", "maxAttachmentSizeMB", "isUserPremium", "attachments", "hasImage", "hasVideo", "hasGif", "onResendCompressed", "copy", "(IFFZLjava/util/List;ZZZLkotlin/jvm/functions/Function0;)Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$FilesTooLarge;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getAttachments", "Z", "getHasVideo", "getHasGif", "I", "getMaxFileSizeMB", "F", "getMaxAttachmentSizeMB", "getHasImage", "Lkotlin/jvm/functions/Function0;", "getOnResendCompressed", "getCurrentFileSizeMB", HookHelper.constructorName, "(IFFZLjava/util/List;ZZZLkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class FilesTooLarge extends Event {
            private final List<Attachment<?>> attachments;
            private final float currentFileSizeMB;
            private final boolean hasGif;
            private final boolean hasImage;
            private final boolean hasVideo;
            private final boolean isUserPremium;
            private final float maxAttachmentSizeMB;
            private final int maxFileSizeMB;
            private final Function0<Unit> onResendCompressed;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public FilesTooLarge(int i, float f, float f2, boolean z2, List<? extends Attachment<?>> list, boolean z3, boolean z4, boolean z5, Function0<Unit> function0) {
                super(null);
                m.checkNotNullParameter(list, "attachments");
                this.maxFileSizeMB = i;
                this.currentFileSizeMB = f;
                this.maxAttachmentSizeMB = f2;
                this.isUserPremium = z2;
                this.attachments = list;
                this.hasImage = z3;
                this.hasVideo = z4;
                this.hasGif = z5;
                this.onResendCompressed = function0;
            }

            public final int component1() {
                return this.maxFileSizeMB;
            }

            public final float component2() {
                return this.currentFileSizeMB;
            }

            public final float component3() {
                return this.maxAttachmentSizeMB;
            }

            public final boolean component4() {
                return this.isUserPremium;
            }

            public final List<Attachment<?>> component5() {
                return this.attachments;
            }

            public final boolean component6() {
                return this.hasImage;
            }

            public final boolean component7() {
                return this.hasVideo;
            }

            public final boolean component8() {
                return this.hasGif;
            }

            public final Function0<Unit> component9() {
                return this.onResendCompressed;
            }

            public final FilesTooLarge copy(int i, float f, float f2, boolean z2, List<? extends Attachment<?>> list, boolean z3, boolean z4, boolean z5, Function0<Unit> function0) {
                m.checkNotNullParameter(list, "attachments");
                return new FilesTooLarge(i, f, f2, z2, list, z3, z4, z5, function0);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof FilesTooLarge)) {
                    return false;
                }
                FilesTooLarge filesTooLarge = (FilesTooLarge) obj;
                return this.maxFileSizeMB == filesTooLarge.maxFileSizeMB && Float.compare(this.currentFileSizeMB, filesTooLarge.currentFileSizeMB) == 0 && Float.compare(this.maxAttachmentSizeMB, filesTooLarge.maxAttachmentSizeMB) == 0 && this.isUserPremium == filesTooLarge.isUserPremium && m.areEqual(this.attachments, filesTooLarge.attachments) && this.hasImage == filesTooLarge.hasImage && this.hasVideo == filesTooLarge.hasVideo && this.hasGif == filesTooLarge.hasGif && m.areEqual(this.onResendCompressed, filesTooLarge.onResendCompressed);
            }

            public final List<Attachment<?>> getAttachments() {
                return this.attachments;
            }

            public final float getCurrentFileSizeMB() {
                return this.currentFileSizeMB;
            }

            public final boolean getHasGif() {
                return this.hasGif;
            }

            public final boolean getHasImage() {
                return this.hasImage;
            }

            public final boolean getHasVideo() {
                return this.hasVideo;
            }

            public final float getMaxAttachmentSizeMB() {
                return this.maxAttachmentSizeMB;
            }

            public final int getMaxFileSizeMB() {
                return this.maxFileSizeMB;
            }

            public final Function0<Unit> getOnResendCompressed() {
                return this.onResendCompressed;
            }

            public int hashCode() {
                int floatToIntBits = (Float.floatToIntBits(this.maxAttachmentSizeMB) + ((Float.floatToIntBits(this.currentFileSizeMB) + (this.maxFileSizeMB * 31)) * 31)) * 31;
                boolean z2 = this.isUserPremium;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (floatToIntBits + i2) * 31;
                List<Attachment<?>> list = this.attachments;
                int i5 = 0;
                int hashCode = (i4 + (list != null ? list.hashCode() : 0)) * 31;
                boolean z3 = this.hasImage;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (hashCode + i6) * 31;
                boolean z4 = this.hasVideo;
                if (z4) {
                    z4 = true;
                }
                int i9 = z4 ? 1 : 0;
                int i10 = z4 ? 1 : 0;
                int i11 = (i8 + i9) * 31;
                boolean z5 = this.hasGif;
                if (!z5) {
                    i = z5 ? 1 : 0;
                }
                int i12 = (i11 + i) * 31;
                Function0<Unit> function0 = this.onResendCompressed;
                if (function0 != null) {
                    i5 = function0.hashCode();
                }
                return i12 + i5;
            }

            public final boolean isUserPremium() {
                return this.isUserPremium;
            }

            public String toString() {
                StringBuilder R = a.R("FilesTooLarge(maxFileSizeMB=");
                R.append(this.maxFileSizeMB);
                R.append(", currentFileSizeMB=");
                R.append(this.currentFileSizeMB);
                R.append(", maxAttachmentSizeMB=");
                R.append(this.maxAttachmentSizeMB);
                R.append(", isUserPremium=");
                R.append(this.isUserPremium);
                R.append(", attachments=");
                R.append(this.attachments);
                R.append(", hasImage=");
                R.append(this.hasImage);
                R.append(", hasVideo=");
                R.append(this.hasVideo);
                R.append(", hasGif=");
                R.append(this.hasGif);
                R.append(", onResendCompressed=");
                R.append(this.onResendCompressed);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$MessageTooLong;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", "", "component1", "()I", "component2", "currentCharacterCount", "maxCharacterCount", "copy", "(II)Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$MessageTooLong;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getCurrentCharacterCount", "getMaxCharacterCount", HookHelper.constructorName, "(II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class MessageTooLong extends Event {
            private final int currentCharacterCount;
            private final int maxCharacterCount;

            public MessageTooLong(int i, int i2) {
                super(null);
                this.currentCharacterCount = i;
                this.maxCharacterCount = i2;
            }

            public static /* synthetic */ MessageTooLong copy$default(MessageTooLong messageTooLong, int i, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    i = messageTooLong.currentCharacterCount;
                }
                if ((i3 & 2) != 0) {
                    i2 = messageTooLong.maxCharacterCount;
                }
                return messageTooLong.copy(i, i2);
            }

            public final int component1() {
                return this.currentCharacterCount;
            }

            public final int component2() {
                return this.maxCharacterCount;
            }

            public final MessageTooLong copy(int i, int i2) {
                return new MessageTooLong(i, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MessageTooLong)) {
                    return false;
                }
                MessageTooLong messageTooLong = (MessageTooLong) obj;
                return this.currentCharacterCount == messageTooLong.currentCharacterCount && this.maxCharacterCount == messageTooLong.maxCharacterCount;
            }

            public final int getCurrentCharacterCount() {
                return this.currentCharacterCount;
            }

            public final int getMaxCharacterCount() {
                return this.maxCharacterCount;
            }

            public int hashCode() {
                return (this.currentCharacterCount * 31) + this.maxCharacterCount;
            }

            public String toString() {
                StringBuilder R = a.R("MessageTooLong(currentCharacterCount=");
                R.append(this.currentCharacterCount);
                R.append(", maxCharacterCount=");
                return a.A(R, this.maxCharacterCount, ")");
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$SetChatText;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", "", "component1", "()Ljava/lang/String;", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "copy", "(Ljava/lang/String;)Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$SetChatText;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getText", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SetChatText extends Event {
            private final String text;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SetChatText(String str) {
                super(null);
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                this.text = str;
            }

            public static /* synthetic */ SetChatText copy$default(SetChatText setChatText, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = setChatText.text;
                }
                return setChatText.copy(str);
            }

            public final String component1() {
                return this.text;
            }

            public final SetChatText copy(String str) {
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                return new SetChatText(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SetChatText) && m.areEqual(this.text, ((SetChatText) obj).text);
                }
                return true;
            }

            public final String getText() {
                return this.text;
            }

            public int hashCode() {
                String str = this.text;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.H(a.R("SetChatText(text="), this.text, ")");
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\b\b\u0001\u0010\f\u001a\u00020\u0002\u0012\b\b\u0001\u0010\r\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0007\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0007¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\n\u0010\tJB\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0003\u0010\f\u001a\u00020\u00022\b\b\u0003\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00072\b\b\u0002\u0010\u000f\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0004J\u001a\u0010\u0018\u001a\u00020\u00072\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u000f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001a\u001a\u0004\b\u001b\u0010\tR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001a\u001a\u0004\b\u001f\u0010\tR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001c\u001a\u0004\b \u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$ShowPremiumUpsell;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", "", "component1", "()I", "component2", "component3", "", "component4", "()Z", "component5", "page", "headerResId", "bodyResId", "showOtherPages", "showLearnMore", "copy", "(IIIZZ)Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$ShowPremiumUpsell;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowLearnMore", "I", "getBodyResId", "getHeaderResId", "getShowOtherPages", "getPage", HookHelper.constructorName, "(IIIZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowPremiumUpsell extends Event {
            private final int bodyResId;
            private final int headerResId;
            private final int page;
            private final boolean showLearnMore;
            private final boolean showOtherPages;

            public /* synthetic */ ShowPremiumUpsell(int i, int i2, int i3, boolean z2, boolean z3, int i4, DefaultConstructorMarker defaultConstructorMarker) {
                this(i, i2, i3, (i4 & 8) != 0 ? false : z2, (i4 & 16) != 0 ? false : z3);
            }

            public static /* synthetic */ ShowPremiumUpsell copy$default(ShowPremiumUpsell showPremiumUpsell, int i, int i2, int i3, boolean z2, boolean z3, int i4, Object obj) {
                if ((i4 & 1) != 0) {
                    i = showPremiumUpsell.page;
                }
                if ((i4 & 2) != 0) {
                    i2 = showPremiumUpsell.headerResId;
                }
                int i5 = i2;
                if ((i4 & 4) != 0) {
                    i3 = showPremiumUpsell.bodyResId;
                }
                int i6 = i3;
                if ((i4 & 8) != 0) {
                    z2 = showPremiumUpsell.showOtherPages;
                }
                boolean z4 = z2;
                if ((i4 & 16) != 0) {
                    z3 = showPremiumUpsell.showLearnMore;
                }
                return showPremiumUpsell.copy(i, i5, i6, z4, z3);
            }

            public final int component1() {
                return this.page;
            }

            public final int component2() {
                return this.headerResId;
            }

            public final int component3() {
                return this.bodyResId;
            }

            public final boolean component4() {
                return this.showOtherPages;
            }

            public final boolean component5() {
                return this.showLearnMore;
            }

            public final ShowPremiumUpsell copy(int i, @StringRes int i2, @StringRes int i3, boolean z2, boolean z3) {
                return new ShowPremiumUpsell(i, i2, i3, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ShowPremiumUpsell)) {
                    return false;
                }
                ShowPremiumUpsell showPremiumUpsell = (ShowPremiumUpsell) obj;
                return this.page == showPremiumUpsell.page && this.headerResId == showPremiumUpsell.headerResId && this.bodyResId == showPremiumUpsell.bodyResId && this.showOtherPages == showPremiumUpsell.showOtherPages && this.showLearnMore == showPremiumUpsell.showLearnMore;
            }

            public final int getBodyResId() {
                return this.bodyResId;
            }

            public final int getHeaderResId() {
                return this.headerResId;
            }

            public final int getPage() {
                return this.page;
            }

            public final boolean getShowLearnMore() {
                return this.showLearnMore;
            }

            public final boolean getShowOtherPages() {
                return this.showOtherPages;
            }

            public int hashCode() {
                int i = ((((this.page * 31) + this.headerResId) * 31) + this.bodyResId) * 31;
                boolean z2 = this.showOtherPages;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (i + i3) * 31;
                boolean z3 = this.showLearnMore;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                return i5 + i2;
            }

            public String toString() {
                StringBuilder R = a.R("ShowPremiumUpsell(page=");
                R.append(this.page);
                R.append(", headerResId=");
                R.append(this.headerResId);
                R.append(", bodyResId=");
                R.append(this.bodyResId);
                R.append(", showOtherPages=");
                R.append(this.showOtherPages);
                R.append(", showLearnMore=");
                return a.M(R, this.showLearnMore, ")");
            }

            public ShowPremiumUpsell(int i, @StringRes int i2, @StringRes int i3, boolean z2, boolean z3) {
                super(null);
                this.page = i;
                this.headerResId = i2;
                this.bodyResId = i3;
                this.showOtherPages = z2;
                this.showLearnMore = z3;
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$Event$ThreadDraftClosed;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ThreadDraftClosed extends Event {
            public static final ThreadDraftClosed INSTANCE = new ThreadDraftClosed();

            private ThreadDraftClosed() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ChatInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Loaded", "Loading", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loading;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b \b\u0086\b\u0018\u00002\u00020\u0001:\u0001`B\u0093\u0001\u0012\u0006\u0010,\u001a\u00020\u0002\u0012\u0006\u0010-\u001a\u00020\u0005\u0012\b\u0010.\u001a\u0004\u0018\u00010\b\u0012\u000e\u0010/\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f\u0012\u000e\u00100\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u0010\u0012\u0006\u00101\u001a\u00020\u0013\u0012\u0006\u00102\u001a\u00020\u0016\u0012\u0006\u00103\u001a\u00020\u0016\u0012\b\u00104\u001a\u0004\u0018\u00010\u001a\u0012\b\u00105\u001a\u0004\u0018\u00010\u001d\u0012\b\u00106\u001a\u0004\u0018\u00010 \u0012\b\u00107\u001a\u0004\u0018\u00010#\u0012\b\u00108\u001a\u0004\u0018\u00010&\u0012\u0006\u00109\u001a\u00020)¢\u0006\u0004\b^\u0010_J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0018\u0010\r\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0018\u0010\u0011\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0018J\u0012\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u001dHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ\u0012\u0010!\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b!\u0010\"J\u0012\u0010$\u001a\u0004\u0018\u00010#HÆ\u0003¢\u0006\u0004\b$\u0010%J\u0012\u0010'\u001a\u0004\u0018\u00010&HÆ\u0003¢\u0006\u0004\b'\u0010(J\u0010\u0010*\u001a\u00020)HÆ\u0003¢\u0006\u0004\b*\u0010+J¸\u0001\u0010:\u001a\u00020\u00002\b\b\u0002\u0010,\u001a\u00020\u00022\b\b\u0002\u0010-\u001a\u00020\u00052\n\b\u0002\u0010.\u001a\u0004\u0018\u00010\b2\u0010\b\u0002\u0010/\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f2\u0010\b\u0002\u00100\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u00102\b\b\u0002\u00101\u001a\u00020\u00132\b\b\u0002\u00102\u001a\u00020\u00162\b\b\u0002\u00103\u001a\u00020\u00162\n\b\u0002\u00104\u001a\u0004\u0018\u00010\u001a2\n\b\u0002\u00105\u001a\u0004\u0018\u00010\u001d2\n\b\u0002\u00106\u001a\u0004\u0018\u00010 2\n\b\u0002\u00107\u001a\u0004\u0018\u00010#2\n\b\u0002\u00108\u001a\u0004\u0018\u00010&2\b\b\u0002\u00109\u001a\u00020)HÆ\u0001¢\u0006\u0004\b:\u0010;J\u0010\u0010=\u001a\u00020<HÖ\u0001¢\u0006\u0004\b=\u0010>J\u0010\u0010?\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b?\u0010@J\u001a\u0010C\u001a\u00020\u00162\b\u0010B\u001a\u0004\u0018\u00010AHÖ\u0003¢\u0006\u0004\bC\u0010DR\u001b\u00105\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010E\u001a\u0004\bF\u0010\u001fR!\u0010/\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010G\u001a\u0004\bH\u0010\u000eR\u001b\u0010.\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010I\u001a\u0004\bJ\u0010\nR!\u00100\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u00108\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010K\u001a\u0004\bL\u0010\u0012R\u0019\u00103\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010M\u001a\u0004\b3\u0010\u0018R\u001b\u00104\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010N\u001a\u0004\bO\u0010\u001cR\u001b\u00106\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010P\u001a\u0004\bQ\u0010\"R\u0019\u00109\u001a\u00020)8\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010R\u001a\u0004\bS\u0010+R\u001b\u00107\u001a\u0004\u0018\u00010#8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010T\u001a\u0004\bU\u0010%R\u0019\u00102\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010M\u001a\u0004\b2\u0010\u0018R\u001b\u00108\u001a\u0004\u0018\u00010&8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010V\u001a\u0004\bW\u0010(R\u0019\u00101\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010X\u001a\u0004\bY\u0010\u0015R\u0019\u0010,\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010Z\u001a\u0004\b[\u0010\u0004R\u0019\u0010-\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\\\u001a\u0004\b]\u0010\u0007¨\u0006a"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/user/MeUser;", "component2", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/stores/StoreChat$EditingMessage;", "component3", "()Lcom/discord/stores/StoreChat$EditingMessage;", "", "Lcom/discord/primitives/RelationshipType;", "component4", "()Ljava/lang/Integer;", "", "Lcom/discord/api/permission/PermissionBit;", "component5", "()Ljava/lang/Long;", "Lcom/discord/api/guild/GuildVerificationLevel;", "component6", "()Lcom/discord/api/guild/GuildVerificationLevel;", "", "component7", "()Z", "component8", "Lcom/discord/models/guild/Guild;", "component9", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;", "component10", "()Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "component11", "()Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "Lcom/discord/models/member/GuildMember;", "component12", "()Lcom/discord/models/member/GuildMember;", "Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "component13", "()Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "component14", "()Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "channel", "me", "editingMessage", "relationshipType", "channelPermissions", "verificationLevelTriggered", "isLurking", "isOnCooldown", "guild", "pendingReply", "selectedThreadDraft", "meGuildMember", "guildJoinRequest", "threadDraftState", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/user/MeUser;Lcom/discord/stores/StoreChat$EditingMessage;Ljava/lang/Integer;Ljava/lang/Long;Lcom/discord/api/guild/GuildVerificationLevel;ZZLcom/discord/models/guild/Guild;Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;Lcom/discord/models/member/GuildMember;Lcom/discord/api/guildjoinrequest/GuildJoinRequest;Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;)Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;", "getPendingReply", "Ljava/lang/Integer;", "getRelationshipType", "Lcom/discord/stores/StoreChat$EditingMessage;", "getEditingMessage", "Ljava/lang/Long;", "getChannelPermissions", "Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "getSelectedThreadDraft", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "getThreadDraftState", "Lcom/discord/models/member/GuildMember;", "getMeGuildMember", "Lcom/discord/api/guildjoinrequest/GuildJoinRequest;", "getGuildJoinRequest", "Lcom/discord/api/guild/GuildVerificationLevel;", "getVerificationLevelTriggered", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/models/user/MeUser;", "getMe", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/models/user/MeUser;Lcom/discord/stores/StoreChat$EditingMessage;Ljava/lang/Integer;Ljava/lang/Long;Lcom/discord/api/guild/GuildVerificationLevel;ZZLcom/discord/models/guild/Guild;Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;Lcom/discord/models/member/GuildMember;Lcom/discord/api/guildjoinrequest/GuildJoinRequest;Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;)V", "PendingReply", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends StoreState {
            private final Channel channel;
            private final Long channelPermissions;
            private final StoreChat.EditingMessage editingMessage;
            private final Guild guild;
            private final GuildJoinRequest guildJoinRequest;
            private final boolean isLurking;
            private final boolean isOnCooldown;

            /* renamed from: me  reason: collision with root package name */
            private final MeUser f2823me;
            private final GuildMember meGuildMember;
            private final PendingReply pendingReply;
            private final Integer relationshipType;
            private final StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft selectedThreadDraft;
            private final StoreThreadDraft.ThreadDraftState threadDraftState;
            private final GuildVerificationLevel verificationLevelTriggered;

            /* compiled from: ChatInputViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ2\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001b\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007¨\u0006\""}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;", "", "Lcom/discord/stores/StorePendingReplies$PendingReply;", "component1", "()Lcom/discord/stores/StorePendingReplies$PendingReply;", "Lcom/discord/models/user/User;", "component2", "()Lcom/discord/models/user/User;", "Lcom/discord/models/member/GuildMember;", "component3", "()Lcom/discord/models/member/GuildMember;", "pendingReply", "repliedAuthor", "repliedAuthorGuildMember", "copy", "(Lcom/discord/stores/StorePendingReplies$PendingReply;Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/member/GuildMember;", "getRepliedAuthorGuildMember", "Lcom/discord/stores/StorePendingReplies$PendingReply;", "getPendingReply", "Lcom/discord/models/user/User;", "getRepliedAuthor", HookHelper.constructorName, "(Lcom/discord/stores/StorePendingReplies$PendingReply;Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class PendingReply {
                private final StorePendingReplies.PendingReply pendingReply;
                private final User repliedAuthor;
                private final GuildMember repliedAuthorGuildMember;

                public PendingReply(StorePendingReplies.PendingReply pendingReply, User user, GuildMember guildMember) {
                    m.checkNotNullParameter(pendingReply, "pendingReply");
                    this.pendingReply = pendingReply;
                    this.repliedAuthor = user;
                    this.repliedAuthorGuildMember = guildMember;
                }

                public static /* synthetic */ PendingReply copy$default(PendingReply pendingReply, StorePendingReplies.PendingReply pendingReply2, User user, GuildMember guildMember, int i, Object obj) {
                    if ((i & 1) != 0) {
                        pendingReply2 = pendingReply.pendingReply;
                    }
                    if ((i & 2) != 0) {
                        user = pendingReply.repliedAuthor;
                    }
                    if ((i & 4) != 0) {
                        guildMember = pendingReply.repliedAuthorGuildMember;
                    }
                    return pendingReply.copy(pendingReply2, user, guildMember);
                }

                public final StorePendingReplies.PendingReply component1() {
                    return this.pendingReply;
                }

                public final User component2() {
                    return this.repliedAuthor;
                }

                public final GuildMember component3() {
                    return this.repliedAuthorGuildMember;
                }

                public final PendingReply copy(StorePendingReplies.PendingReply pendingReply, User user, GuildMember guildMember) {
                    m.checkNotNullParameter(pendingReply, "pendingReply");
                    return new PendingReply(pendingReply, user, guildMember);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof PendingReply)) {
                        return false;
                    }
                    PendingReply pendingReply = (PendingReply) obj;
                    return m.areEqual(this.pendingReply, pendingReply.pendingReply) && m.areEqual(this.repliedAuthor, pendingReply.repliedAuthor) && m.areEqual(this.repliedAuthorGuildMember, pendingReply.repliedAuthorGuildMember);
                }

                public final StorePendingReplies.PendingReply getPendingReply() {
                    return this.pendingReply;
                }

                public final User getRepliedAuthor() {
                    return this.repliedAuthor;
                }

                public final GuildMember getRepliedAuthorGuildMember() {
                    return this.repliedAuthorGuildMember;
                }

                public int hashCode() {
                    StorePendingReplies.PendingReply pendingReply = this.pendingReply;
                    int i = 0;
                    int hashCode = (pendingReply != null ? pendingReply.hashCode() : 0) * 31;
                    User user = this.repliedAuthor;
                    int hashCode2 = (hashCode + (user != null ? user.hashCode() : 0)) * 31;
                    GuildMember guildMember = this.repliedAuthorGuildMember;
                    if (guildMember != null) {
                        i = guildMember.hashCode();
                    }
                    return hashCode2 + i;
                }

                public String toString() {
                    StringBuilder R = a.R("PendingReply(pendingReply=");
                    R.append(this.pendingReply);
                    R.append(", repliedAuthor=");
                    R.append(this.repliedAuthor);
                    R.append(", repliedAuthorGuildMember=");
                    R.append(this.repliedAuthorGuildMember);
                    R.append(")");
                    return R.toString();
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(Channel channel, MeUser meUser, StoreChat.EditingMessage editingMessage, Integer num, Long l, GuildVerificationLevel guildVerificationLevel, boolean z2, boolean z3, Guild guild, PendingReply pendingReply, StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft threadDraft, GuildMember guildMember, GuildJoinRequest guildJoinRequest, StoreThreadDraft.ThreadDraftState threadDraftState) {
                super(null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(meUser, "me");
                m.checkNotNullParameter(guildVerificationLevel, "verificationLevelTriggered");
                m.checkNotNullParameter(threadDraftState, "threadDraftState");
                this.channel = channel;
                this.f2823me = meUser;
                this.editingMessage = editingMessage;
                this.relationshipType = num;
                this.channelPermissions = l;
                this.verificationLevelTriggered = guildVerificationLevel;
                this.isLurking = z2;
                this.isOnCooldown = z3;
                this.guild = guild;
                this.pendingReply = pendingReply;
                this.selectedThreadDraft = threadDraft;
                this.meGuildMember = guildMember;
                this.guildJoinRequest = guildJoinRequest;
                this.threadDraftState = threadDraftState;
            }

            public final Channel component1() {
                return this.channel;
            }

            public final PendingReply component10() {
                return this.pendingReply;
            }

            public final StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft component11() {
                return this.selectedThreadDraft;
            }

            public final GuildMember component12() {
                return this.meGuildMember;
            }

            public final GuildJoinRequest component13() {
                return this.guildJoinRequest;
            }

            public final StoreThreadDraft.ThreadDraftState component14() {
                return this.threadDraftState;
            }

            public final MeUser component2() {
                return this.f2823me;
            }

            public final StoreChat.EditingMessage component3() {
                return this.editingMessage;
            }

            public final Integer component4() {
                return this.relationshipType;
            }

            public final Long component5() {
                return this.channelPermissions;
            }

            public final GuildVerificationLevel component6() {
                return this.verificationLevelTriggered;
            }

            public final boolean component7() {
                return this.isLurking;
            }

            public final boolean component8() {
                return this.isOnCooldown;
            }

            public final Guild component9() {
                return this.guild;
            }

            public final Loaded copy(Channel channel, MeUser meUser, StoreChat.EditingMessage editingMessage, Integer num, Long l, GuildVerificationLevel guildVerificationLevel, boolean z2, boolean z3, Guild guild, PendingReply pendingReply, StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft threadDraft, GuildMember guildMember, GuildJoinRequest guildJoinRequest, StoreThreadDraft.ThreadDraftState threadDraftState) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(meUser, "me");
                m.checkNotNullParameter(guildVerificationLevel, "verificationLevelTriggered");
                m.checkNotNullParameter(threadDraftState, "threadDraftState");
                return new Loaded(channel, meUser, editingMessage, num, l, guildVerificationLevel, z2, z3, guild, pendingReply, threadDraft, guildMember, guildJoinRequest, threadDraftState);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.channel, loaded.channel) && m.areEqual(this.f2823me, loaded.f2823me) && m.areEqual(this.editingMessage, loaded.editingMessage) && m.areEqual(this.relationshipType, loaded.relationshipType) && m.areEqual(this.channelPermissions, loaded.channelPermissions) && m.areEqual(this.verificationLevelTriggered, loaded.verificationLevelTriggered) && this.isLurking == loaded.isLurking && this.isOnCooldown == loaded.isOnCooldown && m.areEqual(this.guild, loaded.guild) && m.areEqual(this.pendingReply, loaded.pendingReply) && m.areEqual(this.selectedThreadDraft, loaded.selectedThreadDraft) && m.areEqual(this.meGuildMember, loaded.meGuildMember) && m.areEqual(this.guildJoinRequest, loaded.guildJoinRequest) && m.areEqual(this.threadDraftState, loaded.threadDraftState);
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final Long getChannelPermissions() {
                return this.channelPermissions;
            }

            public final StoreChat.EditingMessage getEditingMessage() {
                return this.editingMessage;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final GuildJoinRequest getGuildJoinRequest() {
                return this.guildJoinRequest;
            }

            public final MeUser getMe() {
                return this.f2823me;
            }

            public final GuildMember getMeGuildMember() {
                return this.meGuildMember;
            }

            public final PendingReply getPendingReply() {
                return this.pendingReply;
            }

            public final Integer getRelationshipType() {
                return this.relationshipType;
            }

            public final StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft getSelectedThreadDraft() {
                return this.selectedThreadDraft;
            }

            public final StoreThreadDraft.ThreadDraftState getThreadDraftState() {
                return this.threadDraftState;
            }

            public final GuildVerificationLevel getVerificationLevelTriggered() {
                return this.verificationLevelTriggered;
            }

            public int hashCode() {
                Channel channel = this.channel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                MeUser meUser = this.f2823me;
                int hashCode2 = (hashCode + (meUser != null ? meUser.hashCode() : 0)) * 31;
                StoreChat.EditingMessage editingMessage = this.editingMessage;
                int hashCode3 = (hashCode2 + (editingMessage != null ? editingMessage.hashCode() : 0)) * 31;
                Integer num = this.relationshipType;
                int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
                Long l = this.channelPermissions;
                int hashCode5 = (hashCode4 + (l != null ? l.hashCode() : 0)) * 31;
                GuildVerificationLevel guildVerificationLevel = this.verificationLevelTriggered;
                int hashCode6 = (hashCode5 + (guildVerificationLevel != null ? guildVerificationLevel.hashCode() : 0)) * 31;
                boolean z2 = this.isLurking;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode6 + i3) * 31;
                boolean z3 = this.isOnCooldown;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                int i6 = (i5 + i2) * 31;
                Guild guild = this.guild;
                int hashCode7 = (i6 + (guild != null ? guild.hashCode() : 0)) * 31;
                PendingReply pendingReply = this.pendingReply;
                int hashCode8 = (hashCode7 + (pendingReply != null ? pendingReply.hashCode() : 0)) * 31;
                StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft threadDraft = this.selectedThreadDraft;
                int hashCode9 = (hashCode8 + (threadDraft != null ? threadDraft.hashCode() : 0)) * 31;
                GuildMember guildMember = this.meGuildMember;
                int hashCode10 = (hashCode9 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
                GuildJoinRequest guildJoinRequest = this.guildJoinRequest;
                int hashCode11 = (hashCode10 + (guildJoinRequest != null ? guildJoinRequest.hashCode() : 0)) * 31;
                StoreThreadDraft.ThreadDraftState threadDraftState = this.threadDraftState;
                if (threadDraftState != null) {
                    i = threadDraftState.hashCode();
                }
                return hashCode11 + i;
            }

            public final boolean isLurking() {
                return this.isLurking;
            }

            public final boolean isOnCooldown() {
                return this.isOnCooldown;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(channel=");
                R.append(this.channel);
                R.append(", me=");
                R.append(this.f2823me);
                R.append(", editingMessage=");
                R.append(this.editingMessage);
                R.append(", relationshipType=");
                R.append(this.relationshipType);
                R.append(", channelPermissions=");
                R.append(this.channelPermissions);
                R.append(", verificationLevelTriggered=");
                R.append(this.verificationLevelTriggered);
                R.append(", isLurking=");
                R.append(this.isLurking);
                R.append(", isOnCooldown=");
                R.append(this.isOnCooldown);
                R.append(", guild=");
                R.append(this.guild);
                R.append(", pendingReply=");
                R.append(this.pendingReply);
                R.append(", selectedThreadDraft=");
                R.append(this.selectedThreadDraft);
                R.append(", meGuildMember=");
                R.append(this.meGuildMember);
                R.append(", guildJoinRequest=");
                R.append(this.guildJoinRequest);
                R.append(", threadDraftState=");
                R.append(this.threadDraftState);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loading;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends StoreState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ChatInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Loading", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loading;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001f\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b$\b\u0086\b\u0018\u00002\u00020\u0001:\u0001wBá\u0001\u0012\u0006\u00104\u001a\u00020\u0002\u0012\u0006\u00105\u001a\u00020\u0005\u0012\b\u00106\u001a\u0004\u0018\u00010\b\u0012\b\u00107\u001a\u0004\u0018\u00010\u000b\u0012\b\u00108\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u00109\u001a\u00020\u0011\u0012\u0006\u0010:\u001a\u00020\u0014\u0012\u0006\u0010;\u001a\u00020\u0011\u0012\u0006\u0010<\u001a\u00020\u0011\u0012\u0006\u0010=\u001a\u00020\u0011\u0012\u0006\u0010>\u001a\u00020\u001a\u0012\u0006\u0010?\u001a\u00020\u0011\u0012\u0006\u0010@\u001a\u00020\u001e\u0012\u0006\u0010A\u001a\u00020\u0011\u0012\u0006\u0010B\u001a\u00020\u0011\u0012\u0006\u0010C\u001a\u00020\u0011\u0012\u0006\u0010D\u001a\u00020\u0011\u0012\u0006\u0010E\u001a\u00020\u0011\u0012\u0006\u0010F\u001a\u00020\u0011\u0012\u0006\u0010G\u001a\u00020\u0011\u0012\u0006\u0010H\u001a\u00020\u0005\u0012\b\u0010I\u001a\u0004\u0018\u00010)\u0012\u0006\u0010J\u001a\u00020\u0011\u0012\b\u0010K\u001a\u0004\u0018\u00010-\u0012\u0006\u0010L\u001a\u000200\u0012\u0006\u0010M\u001a\u00020\u0011¢\u0006\u0004\bu\u0010vJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0013J\u0010\u0010\u0018\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0013J\u0010\u0010\u0019\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0013J\u0010\u0010\u001b\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0013J\u0010\u0010\u001f\u001a\u00020\u001eHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b!\u0010\u0013J\u0010\u0010\"\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\"\u0010\u0013J\u0010\u0010#\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b#\u0010\u0013J\u0010\u0010$\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b$\u0010\u0013J\u0010\u0010%\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b%\u0010\u0013J\u0010\u0010&\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b&\u0010\u0013J\u0010\u0010'\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b'\u0010\u0013J\u0010\u0010(\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b(\u0010\u0007J\u0012\u0010*\u001a\u0004\u0018\u00010)HÆ\u0003¢\u0006\u0004\b*\u0010+J\u0010\u0010,\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b,\u0010\u0013J\u0012\u0010.\u001a\u0004\u0018\u00010-HÆ\u0003¢\u0006\u0004\b.\u0010/J\u0010\u00101\u001a\u000200HÆ\u0003¢\u0006\u0004\b1\u00102J\u0010\u00103\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b3\u0010\u0013J\u009e\u0002\u0010N\u001a\u00020\u00002\b\b\u0002\u00104\u001a\u00020\u00022\b\b\u0002\u00105\u001a\u00020\u00052\n\b\u0002\u00106\u001a\u0004\u0018\u00010\b2\n\b\u0002\u00107\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u00108\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u00109\u001a\u00020\u00112\b\b\u0002\u0010:\u001a\u00020\u00142\b\b\u0002\u0010;\u001a\u00020\u00112\b\b\u0002\u0010<\u001a\u00020\u00112\b\b\u0002\u0010=\u001a\u00020\u00112\b\b\u0002\u0010>\u001a\u00020\u001a2\b\b\u0002\u0010?\u001a\u00020\u00112\b\b\u0002\u0010@\u001a\u00020\u001e2\b\b\u0002\u0010A\u001a\u00020\u00112\b\b\u0002\u0010B\u001a\u00020\u00112\b\b\u0002\u0010C\u001a\u00020\u00112\b\b\u0002\u0010D\u001a\u00020\u00112\b\b\u0002\u0010E\u001a\u00020\u00112\b\b\u0002\u0010F\u001a\u00020\u00112\b\b\u0002\u0010G\u001a\u00020\u00112\b\b\u0002\u0010H\u001a\u00020\u00052\n\b\u0002\u0010I\u001a\u0004\u0018\u00010)2\b\b\u0002\u0010J\u001a\u00020\u00112\n\b\u0002\u0010K\u001a\u0004\u0018\u00010-2\b\b\u0002\u0010L\u001a\u0002002\b\b\u0002\u0010M\u001a\u00020\u0011HÆ\u0001¢\u0006\u0004\bN\u0010OJ\u0010\u0010Q\u001a\u00020PHÖ\u0001¢\u0006\u0004\bQ\u0010RJ\u0010\u0010S\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\bS\u0010\u001cJ\u001a\u0010V\u001a\u00020\u00112\b\u0010U\u001a\u0004\u0018\u00010THÖ\u0003¢\u0006\u0004\bV\u0010WR\u0019\u0010?\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b?\u0010X\u001a\u0004\bY\u0010\u0013R\u0019\u0010@\u001a\u00020\u001e8\u0006@\u0006¢\u0006\f\n\u0004\b@\u0010Z\u001a\u0004\b[\u0010 R\u0019\u00104\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\\\u001a\u0004\b]\u0010\u0004R\u001b\u00108\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010^\u001a\u0004\b_\u0010\u0010R\u0019\u0010=\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b=\u0010X\u001a\u0004\b=\u0010\u0013R\u0019\u0010C\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bC\u0010X\u001a\u0004\bC\u0010\u0013R\u0019\u0010M\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bM\u0010X\u001a\u0004\b`\u0010\u0013R\u0019\u0010E\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bE\u0010X\u001a\u0004\bE\u0010\u0013R\u001b\u00106\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010a\u001a\u0004\bb\u0010\nR\u0019\u0010>\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010c\u001a\u0004\bd\u0010\u001cR\u001b\u0010I\u001a\u0004\u0018\u00010)8\u0006@\u0006¢\u0006\f\n\u0004\bI\u0010e\u001a\u0004\bf\u0010+R\u0019\u0010G\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bG\u0010X\u001a\u0004\bG\u0010\u0013R\u0019\u0010J\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010X\u001a\u0004\bg\u0010\u0013R\u0019\u0010A\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bA\u0010X\u001a\u0004\bh\u0010\u0013R\u0019\u0010D\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010X\u001a\u0004\bD\u0010\u0013R\u0019\u0010H\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\bH\u0010i\u001a\u0004\bj\u0010\u0007R\u0019\u0010F\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bF\u0010X\u001a\u0004\bF\u0010\u0013R\u001b\u0010K\u001a\u0004\u0018\u00010-8\u0006@\u0006¢\u0006\f\n\u0004\bK\u0010k\u001a\u0004\bl\u0010/R\u0019\u0010:\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010m\u001a\u0004\bn\u0010\u0016R\u0019\u0010;\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010X\u001a\u0004\b;\u0010\u0013R\u0019\u00105\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010i\u001a\u0004\bo\u0010\u0007R\u001b\u00107\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010p\u001a\u0004\bq\u0010\rR\u0019\u0010L\u001a\u0002008\u0006@\u0006¢\u0006\f\n\u0004\bL\u0010r\u001a\u0004\bs\u00102R\u0019\u0010<\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010X\u001a\u0004\b<\u0010\u0013R\u0019\u00109\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b9\u0010X\u001a\u0004\bt\u0010\u0013R\u0019\u0010B\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\bB\u0010X\u001a\u0004\bB\u0010\u0013¨\u0006x"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()J", "Lcom/discord/models/user/MeUser;", "component3", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/member/GuildMember;", "component4", "()Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/StoreChat$EditingMessage;", "component5", "()Lcom/discord/stores/StoreChat$EditingMessage;", "", "component6", "()Z", "Lcom/discord/api/guild/GuildVerificationLevel;", "component7", "()Lcom/discord/api/guild/GuildVerificationLevel;", "component8", "component9", "component10", "", "component11", "()I", "component12", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState;", "component13", "()Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState;", "component14", "component15", "component16", "component17", "component18", "component19", "component20", "component21", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "component22", "()Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "component23", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "component24", "()Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "component25", "()Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "component26", "channel", "channelId", "me", "meGuildMember", "editingMessage", "ableToSendMessage", "verificationLevelTriggered", "isLurking", "isSystemDM", "isOnCooldown", "maxFileSizeMB", "shouldShowFollow", "pendingReplyState", "shouldBadgeChatInput", "isBlocked", "isInputShowing", "isVerificationLevelTriggered", "isEditing", "isReplying", "isCommunicationDisabled", "timeoutLeftMs", "selectedThreadDraft", "shouldShowVerificationGate", "joinRequestStatus", "threadDraftState", "showCreateThreadOption", "copy", "(Lcom/discord/api/channel/Channel;JLcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;Lcom/discord/stores/StoreChat$EditingMessage;ZLcom/discord/api/guild/GuildVerificationLevel;ZZZIZLcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState;ZZZZZZZJLcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;ZLcom/discord/api/guildjoinrequest/ApplicationStatus;Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;Z)Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShouldShowFollow", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState;", "getPendingReplyState", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/stores/StoreChat$EditingMessage;", "getEditingMessage", "getShowCreateThreadOption", "Lcom/discord/models/user/MeUser;", "getMe", "I", "getMaxFileSizeMB", "Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;", "getSelectedThreadDraft", "getShouldShowVerificationGate", "getShouldBadgeChatInput", "J", "getTimeoutLeftMs", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "getJoinRequestStatus", "Lcom/discord/api/guild/GuildVerificationLevel;", "getVerificationLevelTriggered", "getChannelId", "Lcom/discord/models/member/GuildMember;", "getMeGuildMember", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "getThreadDraftState", "getAbleToSendMessage", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;JLcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;Lcom/discord/stores/StoreChat$EditingMessage;ZLcom/discord/api/guild/GuildVerificationLevel;ZZZIZLcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState;ZZZZZZZJLcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel$ThreadDraft;ZLcom/discord/api/guildjoinrequest/ApplicationStatus;Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;Z)V", "PendingReplyState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean ableToSendMessage;
            private final Channel channel;
            private final long channelId;
            private final StoreChat.EditingMessage editingMessage;
            private final boolean isBlocked;
            private final boolean isCommunicationDisabled;
            private final boolean isEditing;
            private final boolean isInputShowing;
            private final boolean isLurking;
            private final boolean isOnCooldown;
            private final boolean isReplying;
            private final boolean isSystemDM;
            private final boolean isVerificationLevelTriggered;
            private final ApplicationStatus joinRequestStatus;
            private final int maxFileSizeMB;

            /* renamed from: me  reason: collision with root package name */
            private final MeUser f2824me;
            private final GuildMember meGuildMember;
            private final PendingReplyState pendingReplyState;
            private final StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft selectedThreadDraft;
            private final boolean shouldBadgeChatInput;
            private final boolean shouldShowFollow;
            private final boolean shouldShowVerificationGate;
            private final boolean showCreateThreadOption;
            private final StoreThreadDraft.ThreadDraftState threadDraftState;
            private final long timeoutLeftMs;
            private final GuildVerificationLevel verificationLevelTriggered;

            /* compiled from: ChatInputViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState;", "", HookHelper.constructorName, "()V", "Hide", "Replying", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState$Hide;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState$Replying;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static abstract class PendingReplyState {

                /* compiled from: ChatInputViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState$Hide;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                /* loaded from: classes2.dex */
                public static final class Hide extends PendingReplyState {
                    public static final Hide INSTANCE = new Hide();

                    private Hide() {
                        super(null);
                    }
                }

                /* compiled from: ChatInputViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\u0006\u0010\u0012\u001a\u00020\t\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b)\u0010*J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJD\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u00052\b\b\u0002\u0010\u0012\u001a\u00020\t2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001e\u001a\u00020\u00052\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b%\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010$\u001a\u0004\b&\u0010\u0007R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010'\u001a\u0004\b(\u0010\u000e¨\u0006+"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState$Replying;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState;", "Lcom/discord/api/message/MessageReference;", "component1", "()Lcom/discord/api/message/MessageReference;", "", "component2", "()Z", "component3", "Lcom/discord/models/user/User;", "component4", "()Lcom/discord/models/user/User;", "Lcom/discord/models/member/GuildMember;", "component5", "()Lcom/discord/models/member/GuildMember;", "messageReference", "shouldMention", "showMentionToggle", "repliedAuthor", "repliedAuthorGuildMember", "copy", "(Lcom/discord/api/message/MessageReference;ZZLcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState$Replying;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/message/MessageReference;", "getMessageReference", "Lcom/discord/models/user/User;", "getRepliedAuthor", "Z", "getShowMentionToggle", "getShouldMention", "Lcom/discord/models/member/GuildMember;", "getRepliedAuthorGuildMember", HookHelper.constructorName, "(Lcom/discord/api/message/MessageReference;ZZLcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                /* loaded from: classes2.dex */
                public static final class Replying extends PendingReplyState {
                    private final MessageReference messageReference;
                    private final User repliedAuthor;
                    private final GuildMember repliedAuthorGuildMember;
                    private final boolean shouldMention;
                    private final boolean showMentionToggle;

                    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                    public Replying(MessageReference messageReference, boolean z2, boolean z3, User user, GuildMember guildMember) {
                        super(null);
                        m.checkNotNullParameter(messageReference, "messageReference");
                        m.checkNotNullParameter(user, "repliedAuthor");
                        this.messageReference = messageReference;
                        this.shouldMention = z2;
                        this.showMentionToggle = z3;
                        this.repliedAuthor = user;
                        this.repliedAuthorGuildMember = guildMember;
                    }

                    public static /* synthetic */ Replying copy$default(Replying replying, MessageReference messageReference, boolean z2, boolean z3, User user, GuildMember guildMember, int i, Object obj) {
                        if ((i & 1) != 0) {
                            messageReference = replying.messageReference;
                        }
                        if ((i & 2) != 0) {
                            z2 = replying.shouldMention;
                        }
                        boolean z4 = z2;
                        if ((i & 4) != 0) {
                            z3 = replying.showMentionToggle;
                        }
                        boolean z5 = z3;
                        if ((i & 8) != 0) {
                            user = replying.repliedAuthor;
                        }
                        User user2 = user;
                        if ((i & 16) != 0) {
                            guildMember = replying.repliedAuthorGuildMember;
                        }
                        return replying.copy(messageReference, z4, z5, user2, guildMember);
                    }

                    public final MessageReference component1() {
                        return this.messageReference;
                    }

                    public final boolean component2() {
                        return this.shouldMention;
                    }

                    public final boolean component3() {
                        return this.showMentionToggle;
                    }

                    public final User component4() {
                        return this.repliedAuthor;
                    }

                    public final GuildMember component5() {
                        return this.repliedAuthorGuildMember;
                    }

                    public final Replying copy(MessageReference messageReference, boolean z2, boolean z3, User user, GuildMember guildMember) {
                        m.checkNotNullParameter(messageReference, "messageReference");
                        m.checkNotNullParameter(user, "repliedAuthor");
                        return new Replying(messageReference, z2, z3, user, guildMember);
                    }

                    public boolean equals(Object obj) {
                        if (this == obj) {
                            return true;
                        }
                        if (!(obj instanceof Replying)) {
                            return false;
                        }
                        Replying replying = (Replying) obj;
                        return m.areEqual(this.messageReference, replying.messageReference) && this.shouldMention == replying.shouldMention && this.showMentionToggle == replying.showMentionToggle && m.areEqual(this.repliedAuthor, replying.repliedAuthor) && m.areEqual(this.repliedAuthorGuildMember, replying.repliedAuthorGuildMember);
                    }

                    public final MessageReference getMessageReference() {
                        return this.messageReference;
                    }

                    public final User getRepliedAuthor() {
                        return this.repliedAuthor;
                    }

                    public final GuildMember getRepliedAuthorGuildMember() {
                        return this.repliedAuthorGuildMember;
                    }

                    public final boolean getShouldMention() {
                        return this.shouldMention;
                    }

                    public final boolean getShowMentionToggle() {
                        return this.showMentionToggle;
                    }

                    public int hashCode() {
                        MessageReference messageReference = this.messageReference;
                        int i = 0;
                        int hashCode = (messageReference != null ? messageReference.hashCode() : 0) * 31;
                        boolean z2 = this.shouldMention;
                        int i2 = 1;
                        if (z2) {
                            z2 = true;
                        }
                        int i3 = z2 ? 1 : 0;
                        int i4 = z2 ? 1 : 0;
                        int i5 = (hashCode + i3) * 31;
                        boolean z3 = this.showMentionToggle;
                        if (!z3) {
                            i2 = z3 ? 1 : 0;
                        }
                        int i6 = (i5 + i2) * 31;
                        User user = this.repliedAuthor;
                        int hashCode2 = (i6 + (user != null ? user.hashCode() : 0)) * 31;
                        GuildMember guildMember = this.repliedAuthorGuildMember;
                        if (guildMember != null) {
                            i = guildMember.hashCode();
                        }
                        return hashCode2 + i;
                    }

                    public String toString() {
                        StringBuilder R = a.R("Replying(messageReference=");
                        R.append(this.messageReference);
                        R.append(", shouldMention=");
                        R.append(this.shouldMention);
                        R.append(", showMentionToggle=");
                        R.append(this.showMentionToggle);
                        R.append(", repliedAuthor=");
                        R.append(this.repliedAuthor);
                        R.append(", repliedAuthorGuildMember=");
                        R.append(this.repliedAuthorGuildMember);
                        R.append(")");
                        return R.toString();
                    }
                }

                private PendingReplyState() {
                }

                public /* synthetic */ PendingReplyState(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(Channel channel, long j, MeUser meUser, GuildMember guildMember, StoreChat.EditingMessage editingMessage, boolean z2, GuildVerificationLevel guildVerificationLevel, boolean z3, boolean z4, boolean z5, int i, boolean z6, PendingReplyState pendingReplyState, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, boolean z13, long j2, StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft threadDraft, boolean z14, ApplicationStatus applicationStatus, StoreThreadDraft.ThreadDraftState threadDraftState, boolean z15) {
                super(null);
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(guildVerificationLevel, "verificationLevelTriggered");
                m.checkNotNullParameter(pendingReplyState, "pendingReplyState");
                m.checkNotNullParameter(threadDraftState, "threadDraftState");
                this.channel = channel;
                this.channelId = j;
                this.f2824me = meUser;
                this.meGuildMember = guildMember;
                this.editingMessage = editingMessage;
                this.ableToSendMessage = z2;
                this.verificationLevelTriggered = guildVerificationLevel;
                this.isLurking = z3;
                this.isSystemDM = z4;
                this.isOnCooldown = z5;
                this.maxFileSizeMB = i;
                this.shouldShowFollow = z6;
                this.pendingReplyState = pendingReplyState;
                this.shouldBadgeChatInput = z7;
                this.isBlocked = z8;
                this.isInputShowing = z9;
                this.isVerificationLevelTriggered = z10;
                this.isEditing = z11;
                this.isReplying = z12;
                this.isCommunicationDisabled = z13;
                this.timeoutLeftMs = j2;
                this.selectedThreadDraft = threadDraft;
                this.shouldShowVerificationGate = z14;
                this.joinRequestStatus = applicationStatus;
                this.threadDraftState = threadDraftState;
                this.showCreateThreadOption = z15;
            }

            public final Channel component1() {
                return this.channel;
            }

            public final boolean component10() {
                return this.isOnCooldown;
            }

            public final int component11() {
                return this.maxFileSizeMB;
            }

            public final boolean component12() {
                return this.shouldShowFollow;
            }

            public final PendingReplyState component13() {
                return this.pendingReplyState;
            }

            public final boolean component14() {
                return this.shouldBadgeChatInput;
            }

            public final boolean component15() {
                return this.isBlocked;
            }

            public final boolean component16() {
                return this.isInputShowing;
            }

            public final boolean component17() {
                return this.isVerificationLevelTriggered;
            }

            public final boolean component18() {
                return this.isEditing;
            }

            public final boolean component19() {
                return this.isReplying;
            }

            public final long component2() {
                return this.channelId;
            }

            public final boolean component20() {
                return this.isCommunicationDisabled;
            }

            public final long component21() {
                return this.timeoutLeftMs;
            }

            public final StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft component22() {
                return this.selectedThreadDraft;
            }

            public final boolean component23() {
                return this.shouldShowVerificationGate;
            }

            public final ApplicationStatus component24() {
                return this.joinRequestStatus;
            }

            public final StoreThreadDraft.ThreadDraftState component25() {
                return this.threadDraftState;
            }

            public final boolean component26() {
                return this.showCreateThreadOption;
            }

            public final MeUser component3() {
                return this.f2824me;
            }

            public final GuildMember component4() {
                return this.meGuildMember;
            }

            public final StoreChat.EditingMessage component5() {
                return this.editingMessage;
            }

            public final boolean component6() {
                return this.ableToSendMessage;
            }

            public final GuildVerificationLevel component7() {
                return this.verificationLevelTriggered;
            }

            public final boolean component8() {
                return this.isLurking;
            }

            public final boolean component9() {
                return this.isSystemDM;
            }

            public final Loaded copy(Channel channel, long j, MeUser meUser, GuildMember guildMember, StoreChat.EditingMessage editingMessage, boolean z2, GuildVerificationLevel guildVerificationLevel, boolean z3, boolean z4, boolean z5, int i, boolean z6, PendingReplyState pendingReplyState, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, boolean z13, long j2, StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft threadDraft, boolean z14, ApplicationStatus applicationStatus, StoreThreadDraft.ThreadDraftState threadDraftState, boolean z15) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(guildVerificationLevel, "verificationLevelTriggered");
                m.checkNotNullParameter(pendingReplyState, "pendingReplyState");
                m.checkNotNullParameter(threadDraftState, "threadDraftState");
                return new Loaded(channel, j, meUser, guildMember, editingMessage, z2, guildVerificationLevel, z3, z4, z5, i, z6, pendingReplyState, z7, z8, z9, z10, z11, z12, z13, j2, threadDraft, z14, applicationStatus, threadDraftState, z15);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.channel, loaded.channel) && this.channelId == loaded.channelId && m.areEqual(this.f2824me, loaded.f2824me) && m.areEqual(this.meGuildMember, loaded.meGuildMember) && m.areEqual(this.editingMessage, loaded.editingMessage) && this.ableToSendMessage == loaded.ableToSendMessage && m.areEqual(this.verificationLevelTriggered, loaded.verificationLevelTriggered) && this.isLurking == loaded.isLurking && this.isSystemDM == loaded.isSystemDM && this.isOnCooldown == loaded.isOnCooldown && this.maxFileSizeMB == loaded.maxFileSizeMB && this.shouldShowFollow == loaded.shouldShowFollow && m.areEqual(this.pendingReplyState, loaded.pendingReplyState) && this.shouldBadgeChatInput == loaded.shouldBadgeChatInput && this.isBlocked == loaded.isBlocked && this.isInputShowing == loaded.isInputShowing && this.isVerificationLevelTriggered == loaded.isVerificationLevelTriggered && this.isEditing == loaded.isEditing && this.isReplying == loaded.isReplying && this.isCommunicationDisabled == loaded.isCommunicationDisabled && this.timeoutLeftMs == loaded.timeoutLeftMs && m.areEqual(this.selectedThreadDraft, loaded.selectedThreadDraft) && this.shouldShowVerificationGate == loaded.shouldShowVerificationGate && m.areEqual(this.joinRequestStatus, loaded.joinRequestStatus) && m.areEqual(this.threadDraftState, loaded.threadDraftState) && this.showCreateThreadOption == loaded.showCreateThreadOption;
            }

            public final boolean getAbleToSendMessage() {
                return this.ableToSendMessage;
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final StoreChat.EditingMessage getEditingMessage() {
                return this.editingMessage;
            }

            public final ApplicationStatus getJoinRequestStatus() {
                return this.joinRequestStatus;
            }

            public final int getMaxFileSizeMB() {
                return this.maxFileSizeMB;
            }

            public final MeUser getMe() {
                return this.f2824me;
            }

            public final GuildMember getMeGuildMember() {
                return this.meGuildMember;
            }

            public final PendingReplyState getPendingReplyState() {
                return this.pendingReplyState;
            }

            public final StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft getSelectedThreadDraft() {
                return this.selectedThreadDraft;
            }

            public final boolean getShouldBadgeChatInput() {
                return this.shouldBadgeChatInput;
            }

            public final boolean getShouldShowFollow() {
                return this.shouldShowFollow;
            }

            public final boolean getShouldShowVerificationGate() {
                return this.shouldShowVerificationGate;
            }

            public final boolean getShowCreateThreadOption() {
                return this.showCreateThreadOption;
            }

            public final StoreThreadDraft.ThreadDraftState getThreadDraftState() {
                return this.threadDraftState;
            }

            public final long getTimeoutLeftMs() {
                return this.timeoutLeftMs;
            }

            public final GuildVerificationLevel getVerificationLevelTriggered() {
                return this.verificationLevelTriggered;
            }

            public int hashCode() {
                Channel channel = this.channel;
                int i = 0;
                int a = (a0.a.a.b.a(this.channelId) + ((channel != null ? channel.hashCode() : 0) * 31)) * 31;
                MeUser meUser = this.f2824me;
                int hashCode = (a + (meUser != null ? meUser.hashCode() : 0)) * 31;
                GuildMember guildMember = this.meGuildMember;
                int hashCode2 = (hashCode + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
                StoreChat.EditingMessage editingMessage = this.editingMessage;
                int hashCode3 = (hashCode2 + (editingMessage != null ? editingMessage.hashCode() : 0)) * 31;
                boolean z2 = this.ableToSendMessage;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode3 + i3) * 31;
                GuildVerificationLevel guildVerificationLevel = this.verificationLevelTriggered;
                int hashCode4 = (i5 + (guildVerificationLevel != null ? guildVerificationLevel.hashCode() : 0)) * 31;
                boolean z3 = this.isLurking;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (hashCode4 + i6) * 31;
                boolean z4 = this.isSystemDM;
                if (z4) {
                    z4 = true;
                }
                int i9 = z4 ? 1 : 0;
                int i10 = z4 ? 1 : 0;
                int i11 = (i8 + i9) * 31;
                boolean z5 = this.isOnCooldown;
                if (z5) {
                    z5 = true;
                }
                int i12 = z5 ? 1 : 0;
                int i13 = z5 ? 1 : 0;
                int i14 = (((i11 + i12) * 31) + this.maxFileSizeMB) * 31;
                boolean z6 = this.shouldShowFollow;
                if (z6) {
                    z6 = true;
                }
                int i15 = z6 ? 1 : 0;
                int i16 = z6 ? 1 : 0;
                int i17 = (i14 + i15) * 31;
                PendingReplyState pendingReplyState = this.pendingReplyState;
                int hashCode5 = (i17 + (pendingReplyState != null ? pendingReplyState.hashCode() : 0)) * 31;
                boolean z7 = this.shouldBadgeChatInput;
                if (z7) {
                    z7 = true;
                }
                int i18 = z7 ? 1 : 0;
                int i19 = z7 ? 1 : 0;
                int i20 = (hashCode5 + i18) * 31;
                boolean z8 = this.isBlocked;
                if (z8) {
                    z8 = true;
                }
                int i21 = z8 ? 1 : 0;
                int i22 = z8 ? 1 : 0;
                int i23 = (i20 + i21) * 31;
                boolean z9 = this.isInputShowing;
                if (z9) {
                    z9 = true;
                }
                int i24 = z9 ? 1 : 0;
                int i25 = z9 ? 1 : 0;
                int i26 = (i23 + i24) * 31;
                boolean z10 = this.isVerificationLevelTriggered;
                if (z10) {
                    z10 = true;
                }
                int i27 = z10 ? 1 : 0;
                int i28 = z10 ? 1 : 0;
                int i29 = (i26 + i27) * 31;
                boolean z11 = this.isEditing;
                if (z11) {
                    z11 = true;
                }
                int i30 = z11 ? 1 : 0;
                int i31 = z11 ? 1 : 0;
                int i32 = (i29 + i30) * 31;
                boolean z12 = this.isReplying;
                if (z12) {
                    z12 = true;
                }
                int i33 = z12 ? 1 : 0;
                int i34 = z12 ? 1 : 0;
                int i35 = (i32 + i33) * 31;
                boolean z13 = this.isCommunicationDisabled;
                if (z13) {
                    z13 = true;
                }
                int i36 = z13 ? 1 : 0;
                int i37 = z13 ? 1 : 0;
                int a2 = (a0.a.a.b.a(this.timeoutLeftMs) + ((i35 + i36) * 31)) * 31;
                StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft threadDraft = this.selectedThreadDraft;
                int hashCode6 = (a2 + (threadDraft != null ? threadDraft.hashCode() : 0)) * 31;
                boolean z14 = this.shouldShowVerificationGate;
                if (z14) {
                    z14 = true;
                }
                int i38 = z14 ? 1 : 0;
                int i39 = z14 ? 1 : 0;
                int i40 = (hashCode6 + i38) * 31;
                ApplicationStatus applicationStatus = this.joinRequestStatus;
                int hashCode7 = (i40 + (applicationStatus != null ? applicationStatus.hashCode() : 0)) * 31;
                StoreThreadDraft.ThreadDraftState threadDraftState = this.threadDraftState;
                if (threadDraftState != null) {
                    i = threadDraftState.hashCode();
                }
                int i41 = (hashCode7 + i) * 31;
                boolean z15 = this.showCreateThreadOption;
                if (!z15) {
                    i2 = z15 ? 1 : 0;
                }
                return i41 + i2;
            }

            public final boolean isBlocked() {
                return this.isBlocked;
            }

            public final boolean isCommunicationDisabled() {
                return this.isCommunicationDisabled;
            }

            public final boolean isEditing() {
                return this.isEditing;
            }

            public final boolean isInputShowing() {
                return this.isInputShowing;
            }

            public final boolean isLurking() {
                return this.isLurking;
            }

            public final boolean isOnCooldown() {
                return this.isOnCooldown;
            }

            public final boolean isReplying() {
                return this.isReplying;
            }

            public final boolean isSystemDM() {
                return this.isSystemDM;
            }

            public final boolean isVerificationLevelTriggered() {
                return this.isVerificationLevelTriggered;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(channel=");
                R.append(this.channel);
                R.append(", channelId=");
                R.append(this.channelId);
                R.append(", me=");
                R.append(this.f2824me);
                R.append(", meGuildMember=");
                R.append(this.meGuildMember);
                R.append(", editingMessage=");
                R.append(this.editingMessage);
                R.append(", ableToSendMessage=");
                R.append(this.ableToSendMessage);
                R.append(", verificationLevelTriggered=");
                R.append(this.verificationLevelTriggered);
                R.append(", isLurking=");
                R.append(this.isLurking);
                R.append(", isSystemDM=");
                R.append(this.isSystemDM);
                R.append(", isOnCooldown=");
                R.append(this.isOnCooldown);
                R.append(", maxFileSizeMB=");
                R.append(this.maxFileSizeMB);
                R.append(", shouldShowFollow=");
                R.append(this.shouldShowFollow);
                R.append(", pendingReplyState=");
                R.append(this.pendingReplyState);
                R.append(", shouldBadgeChatInput=");
                R.append(this.shouldBadgeChatInput);
                R.append(", isBlocked=");
                R.append(this.isBlocked);
                R.append(", isInputShowing=");
                R.append(this.isInputShowing);
                R.append(", isVerificationLevelTriggered=");
                R.append(this.isVerificationLevelTriggered);
                R.append(", isEditing=");
                R.append(this.isEditing);
                R.append(", isReplying=");
                R.append(this.isReplying);
                R.append(", isCommunicationDisabled=");
                R.append(this.isCommunicationDisabled);
                R.append(", timeoutLeftMs=");
                R.append(this.timeoutLeftMs);
                R.append(", selectedThreadDraft=");
                R.append(this.selectedThreadDraft);
                R.append(", shouldShowVerificationGate=");
                R.append(this.shouldShowVerificationGate);
                R.append(", joinRequestStatus=");
                R.append(this.joinRequestStatus);
                R.append(", threadDraftState=");
                R.append(this.threadDraftState);
                R.append(", showCreateThreadOption=");
                return a.M(R, this.showCreateThreadOption, ")");
            }
        }

        /* compiled from: ChatInputViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loading;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            GuildVerificationLevel.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[GuildVerificationLevel.LOW.ordinal()] = 1;
            iArr[GuildVerificationLevel.HIGHEST.ordinal()] = 2;
            iArr[GuildVerificationLevel.NONE.ordinal()] = 3;
            iArr[GuildVerificationLevel.MEDIUM.ordinal()] = 4;
            iArr[GuildVerificationLevel.HIGH.ordinal()] = 5;
        }
    }

    public ChatInputViewModel() {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, 32767, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ ChatInputViewModel(com.discord.stores.StoreChat r22, com.discord.stores.StoreChannels r23, com.discord.stores.StoreMessagesLoader r24, com.discord.stores.StoreLurking r25, com.discord.stores.StoreStickers r26, com.discord.stores.StorePendingReplies r27, com.discord.stores.StoreApplicationInteractions r28, com.discord.stores.StoreApplicationCommands r29, com.discord.stores.StoreApplicationCommandFrecency r30, com.discord.stores.StoreUserSettings r31, com.discord.stores.StoreAnalytics r32, com.discord.stores.StoreThreadDraft r33, com.discord.stores.StoreGuilds r34, boolean r35, rx.Observable r36, int r37, kotlin.jvm.internal.DefaultConstructorMarker r38) {
        /*
            Method dump skipped, instructions count: 287
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.ChatInputViewModel.<init>(com.discord.stores.StoreChat, com.discord.stores.StoreChannels, com.discord.stores.StoreMessagesLoader, com.discord.stores.StoreLurking, com.discord.stores.StoreStickers, com.discord.stores.StorePendingReplies, com.discord.stores.StoreApplicationInteractions, com.discord.stores.StoreApplicationCommands, com.discord.stores.StoreApplicationCommandFrecency, com.discord.stores.StoreUserSettings, com.discord.stores.StoreAnalytics, com.discord.stores.StoreThreadDraft, com.discord.stores.StoreGuilds, boolean, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long calculateTimeoutLeftMs(GuildMember guildMember) {
        UtcDateTime communicationDisabledUntil;
        if (guildMember == null || (communicationDisabledUntil = guildMember.getCommunicationDisabledUntil()) == null) {
            return 0L;
        }
        return communicationDisabledUntil.g() - ClockFactory.get().currentTimeMillis();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void createAndGotoThread$default(ChatInputViewModel chatInputViewModel, Context context, Long l, String str, ViewState.Loaded loaded, Function1 function1, int i, Object obj) {
        Function1<? super Channel, Unit> function12 = function1;
        if ((i & 16) != 0) {
            function12 = null;
        }
        chatInputViewModel.createAndGotoThread(context, l, str, loaded, function12);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreChatEvent(StoreChat.Event event) {
        if (event instanceof StoreChat.Event.AppendChatText) {
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new Event.AppendChatText(((StoreChat.Event.AppendChatText) event).getText()));
        } else if (event instanceof StoreChat.Event.ReplaceChatText) {
            PublishSubject<Event> publishSubject2 = this.eventSubject;
            publishSubject2.k.onNext(new Event.SetChatText(((StoreChat.Event.ReplaceChatText) event).getText()));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        boolean z2;
        ViewState.Loaded.PendingReplyState pendingReplyState;
        String str;
        CharSequence content;
        if (storeState instanceof StoreState.Loaded) {
            StoreState.Loaded loaded = (StoreState.Loaded) storeState;
            if (loaded.getSelectedThreadDraft() == null) {
                z2 = PermissionUtils.INSTANCE.hasAccessWrite(loaded.getChannel(), loaded.getChannelPermissions());
            } else {
                z2 = PermissionUtils.can(Permission.SEND_MESSAGES_IN_THREADS, loaded.getChannelPermissions());
            }
            boolean isType = ModelUserRelationship.isType(loaded.getRelationshipType(), 2);
            ThreadUtils threadUtils = ThreadUtils.INSTANCE;
            boolean z3 = true;
            boolean z4 = !isType && z2 && !loaded.isLurking() && (threadUtils.canUnarchiveThread(loaded.getChannel(), loaded.getChannelPermissions()) || !ChannelUtils.C(loaded.getChannel()));
            boolean z5 = loaded.getChannel().A() == 5 && !z4;
            PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
            int maxFileSizeMB = premiumUtils.getMaxFileSizeMB(loaded.getMe());
            Guild guild = loaded.getGuild();
            int max = Math.max(premiumUtils.getGuildMaxFileSizeMB(guild != null ? guild.getPremiumTier() : 0), maxFileSizeMB);
            boolean z6 = loaded.getVerificationLevelTriggered() != GuildVerificationLevel.NONE;
            StoreState.Loaded.PendingReply pendingReply = loaded.getPendingReply();
            if ((pendingReply != null ? pendingReply.getRepliedAuthor() : null) != null) {
                pendingReplyState = new ViewState.Loaded.PendingReplyState.Replying(loaded.getPendingReply().getPendingReply().getMessageReference(), loaded.getPendingReply().getPendingReply().getShouldMention(), loaded.getPendingReply().getPendingReply().getShowMentionToggle(), loaded.getPendingReply().getRepliedAuthor(), loaded.getPendingReply().getRepliedAuthorGuildMember());
            } else {
                pendingReplyState = ViewState.Loaded.PendingReplyState.Hide.INSTANCE;
            }
            boolean hasVerificationGate$default = MemberVerificationUtils.hasVerificationGate$default(MemberVerificationUtils.INSTANCE, loaded.getGuild(), null, 2, null);
            boolean z7 = loaded.getMeGuildMember() != null && !loaded.getMeGuildMember().getPending();
            GuildMember meGuildMember = loaded.getMeGuildMember();
            boolean isCommunicationDisabled = meGuildMember != null ? meGuildMember.isCommunicationDisabled() : false;
            boolean z8 = hasVerificationGate$default && !z7;
            boolean z9 = !ChannelUtils.A(loaded.getChannel()) && !loaded.isLurking() && !z6 && !z5 && !z8 && !isCommunicationDisabled;
            boolean z10 = z4 && loaded.getEditingMessage() != null && loaded.getEditingMessage().getMessage().getChannelId() == loaded.getChannel().h();
            boolean z11 = pendingReplyState instanceof ViewState.Loaded.PendingReplyState.Replying;
            boolean canCreateThread = threadUtils.canCreateThread(loaded.getChannelPermissions(), loaded.getChannel(), null, loaded.getGuild());
            long calculateTimeoutLeftMs = calculateTimeoutLeftMs(loaded.getMeGuildMember());
            Channel channel = loaded.getChannel();
            long h = loaded.getChannel().h();
            MeUser me2 = loaded.getMe();
            GuildMember meGuildMember2 = loaded.getMeGuildMember();
            StoreChat.EditingMessage editingMessage = loaded.getEditingMessage();
            GuildVerificationLevel verificationLevelTriggered = loaded.getVerificationLevelTriggered();
            boolean isLurking = loaded.isLurking();
            boolean A = ChannelUtils.A(loaded.getChannel());
            boolean isOnCooldown = loaded.isOnCooldown();
            StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft selectedThreadDraft = loaded.getSelectedThreadDraft();
            GuildJoinRequest guildJoinRequest = loaded.getGuildJoinRequest();
            ApplicationStatus a = guildJoinRequest != null ? guildJoinRequest.a() : null;
            StoreThreadDraft.ThreadDraftState threadDraftState = loaded.getThreadDraftState();
            boolean z12 = isCommunicationDisabled;
            ViewState.Loaded loaded2 = new ViewState.Loaded(channel, h, me2, meGuildMember2, editingMessage, z4, verificationLevelTriggered, isLurking, A, isOnCooldown, max, z5, pendingReplyState, false, isType, z9, z6, z10, z11, z12, calculateTimeoutLeftMs, selectedThreadDraft, z8, a, threadDraftState, canCreateThread);
            ViewState viewState = getViewState();
            if (!(viewState instanceof ViewState.Loaded)) {
                viewState = null;
            }
            ViewState.Loaded loaded3 = (ViewState.Loaded) viewState;
            boolean z13 = !m.areEqual(loaded3 != null ? loaded3.getEditingMessage() : null, loaded2.getEditingMessage());
            ViewState viewState2 = getViewState();
            if (!(viewState2 instanceof ViewState.Loaded)) {
                viewState2 = null;
            }
            ViewState.Loaded loaded4 = (ViewState.Loaded) viewState2;
            if ((loaded4 != null ? loaded4.getSelectedThreadDraft() : null) == null || loaded2.getSelectedThreadDraft() != null) {
                z3 = false;
            }
            this.useTimeoutUpdateInterval = z12;
            updateViewState(loaded2);
            if (z13) {
                StoreChat.EditingMessage editingMessage2 = loaded2.getEditingMessage();
                if (editingMessage2 == null || (content = editingMessage2.getContent()) == null || (str = content.toString()) == null) {
                    str = "";
                }
                this.eventSubject.k.onNext(new Event.SetChatText(str));
            }
            if (z3) {
                this.eventSubject.k.onNext(Event.ThreadDraftClosed.INSTANCE);
            }
        }
    }

    public final void createAndGotoThread(Context context, Long l, String str, ViewState.Loaded loaded, Function1<? super Channel, Unit> function1) {
        int i;
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(loaded, "loadedViewState");
        ThreadUtils threadUtils = ThreadUtils.INSTANCE;
        long channelId = loaded.getChannelId();
        if (ChannelUtils.i(loaded.getChannel())) {
            i = 10;
        } else {
            i = loaded.getThreadDraftState().isPrivate() ? 12 : 11;
        }
        String threadName = loaded.getThreadDraftState().getThreadName();
        if (threadName == null) {
            threadName = "";
        }
        String str2 = threadName;
        Integer autoArchiveDuration = loaded.getThreadDraftState().getAutoArchiveDuration();
        if (autoArchiveDuration == null) {
            autoArchiveDuration = loaded.getChannel().d();
        }
        Observable z2 = ObservableExtensionsKt.restSubscribeOn$default(threadUtils.createThread(channelId, l, i, str2, Integer.valueOf(autoArchiveDuration != null ? autoArchiveDuration.intValue() : 1440), str), false, 1, null).z(new b<Channel, Observable<? extends Channel>>() { // from class: com.discord.widgets.chat.input.ChatInputViewModel$createAndGotoThread$1
            public final Observable<? extends Channel> call(Channel channel) {
                StoreChannels storeChannels;
                storeChannels = ChatInputViewModel.this.storeChannels;
                Observable<R> F = storeChannels.observeChannel(channel.h()).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
                m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
                return ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.takeSingleUntilTimeout$default(F, 0L, false, 3, null));
            }
        });
        m.checkNotNullExpressionValue(z2, "ThreadUtils.createThread…utationLatest()\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(z2, this, null, 2, null), ChatInputViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new ChatInputViewModel$createAndGotoThread$3(loaded, context), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ChatInputViewModel$createAndGotoThread$2(loaded, function1));
    }

    public final void deleteEditingMessage() {
        this.storeChat.setEditingMessage(null);
    }

    public final void deletePendingReply() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            this.storePendingReplies.onDeletePendingReply(loaded.getChannelId());
        }
    }

    public final boolean handleEmojiAutocompleteUpsellClicked(Autocompletable autocompletable) {
        m.checkNotNullParameter(autocompletable, "item");
        if (!this.isEmojiAutocompleteUpsellEnabled || !(autocompletable instanceof EmojiUpsellPlaceholder)) {
            return false;
        }
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowPremiumUpsell(1, R.string.autocomplete_emoji_upsell_modal_header, R.string.autocomplete_emoji_upsell_modal_blurb_mobile, false, false, 24, null));
        this.storeAnalytics.emojiAutocompleteUpsellModalViewed();
        return true;
    }

    public final void jumpToMessageReference(MessageReference messageReference) {
        m.checkNotNullParameter(messageReference, "messageReference");
        StoreMessagesLoader storeMessagesLoader = this.storeMessagesLoader;
        Long a = messageReference.a();
        m.checkNotNull(a);
        long longValue = a.longValue();
        Long c = messageReference.c();
        m.checkNotNull(c);
        storeMessagesLoader.jumpToMessage(longValue, c.longValue());
    }

    public final void lurkGuild(Fragment fragment) {
        m.checkNotNullParameter(fragment, "fragment");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            StoreLurking storeLurking = this.storeLurking;
            long f = loaded.getChannel().f();
            Context requireContext = fragment.requireContext();
            m.checkNotNullExpressionValue(requireContext, "fragment.requireContext()");
            storeLurking.postJoinGuildAsMember(f, requireContext);
        }
    }

    public final Observable<ViewState> observeChatInputViewState() {
        Observable<Long> D = Observable.D(0L, 1L, TimeUnit.SECONDS);
        m.checkNotNullExpressionValue(D, "Observable.interval(0L, 1L, TimeUnit.SECONDS)");
        Observable<ViewState> j = Observable.j(ObservableExtensionsKt.ui(D).F(new b<Long, Long>() { // from class: com.discord.widgets.chat.input.ChatInputViewModel$observeChatInputViewState$1
            public final Long call(Long l) {
                boolean z2;
                z2 = ChatInputViewModel.this.useTimeoutUpdateInterval;
                if (z2) {
                    return l;
                }
                return 0L;
            }
        }).q(), observeViewState(), new Func2<Long, ViewState, ViewState>() { // from class: com.discord.widgets.chat.input.ChatInputViewModel$observeChatInputViewState$2
            public final ChatInputViewModel.ViewState call(Long l, ChatInputViewModel.ViewState viewState) {
                boolean z2;
                long calculateTimeoutLeftMs;
                ChatInputViewModel.ViewState.Loaded copy;
                StoreGuilds storeGuilds;
                ChatInputViewModel.ViewState.Loaded loaded = (ChatInputViewModel.ViewState.Loaded) (!(viewState instanceof ChatInputViewModel.ViewState.Loaded) ? null : viewState);
                if (loaded == null) {
                    return viewState;
                }
                z2 = ChatInputViewModel.this.useTimeoutUpdateInterval;
                if (z2) {
                    calculateTimeoutLeftMs = ChatInputViewModel.this.calculateTimeoutLeftMs(loaded.getMeGuildMember());
                    GuildMember meGuildMember = loaded.getMeGuildMember();
                    copy = loaded.copy((r46 & 1) != 0 ? loaded.channel : null, (r46 & 2) != 0 ? loaded.channelId : 0L, (r46 & 4) != 0 ? loaded.f2824me : null, (r46 & 8) != 0 ? loaded.meGuildMember : null, (r46 & 16) != 0 ? loaded.editingMessage : null, (r46 & 32) != 0 ? loaded.ableToSendMessage : false, (r46 & 64) != 0 ? loaded.verificationLevelTriggered : null, (r46 & 128) != 0 ? loaded.isLurking : false, (r46 & 256) != 0 ? loaded.isSystemDM : false, (r46 & 512) != 0 ? loaded.isOnCooldown : false, (r46 & 1024) != 0 ? loaded.maxFileSizeMB : 0, (r46 & 2048) != 0 ? loaded.shouldShowFollow : false, (r46 & 4096) != 0 ? loaded.pendingReplyState : null, (r46 & 8192) != 0 ? loaded.shouldBadgeChatInput : false, (r46 & 16384) != 0 ? loaded.isBlocked : false, (r46 & 32768) != 0 ? loaded.isInputShowing : false, (r46 & 65536) != 0 ? loaded.isVerificationLevelTriggered : false, (r46 & 131072) != 0 ? loaded.isEditing : false, (r46 & 262144) != 0 ? loaded.isReplying : false, (r46 & 524288) != 0 ? loaded.isCommunicationDisabled : meGuildMember != null && meGuildMember.isCommunicationDisabled(), (r46 & 1048576) != 0 ? loaded.timeoutLeftMs : calculateTimeoutLeftMs, (r46 & 2097152) != 0 ? loaded.selectedThreadDraft : null, (4194304 & r46) != 0 ? loaded.shouldShowVerificationGate : false, (r46 & 8388608) != 0 ? loaded.joinRequestStatus : null, (r46 & 16777216) != 0 ? loaded.threadDraftState : null, (r46 & 33554432) != 0 ? loaded.showCreateThreadOption : false);
                    if (loaded.getMeGuildMember() != null && loaded.isCommunicationDisabled() && !copy.isCommunicationDisabled()) {
                        storeGuilds = ChatInputViewModel.this.storeGuilds;
                        storeGuilds.handleGuildMemberCommunicationEnabled(loaded.getMeGuildMember().getGuildId(), loaded.getMeGuildMember().getUserId());
                    }
                    loaded = copy;
                }
                return loaded != null ? loaded : viewState;
            }
        });
        m.checkNotNullExpressionValue(j, "Observable.combineLatest…     } ?: viewState\n    }");
        return j;
    }

    public final Observable<Event> observeEvents() {
        return this.eventSubject;
    }

    public final void onCommandInputsInvalid() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.CommandInputsInvalid.INSTANCE);
    }

    public final void onCommandUsed(ApplicationCommandData applicationCommandData) {
        m.checkNotNullParameter(applicationCommandData, "applicationCommandData");
        ViewState viewState = getViewState();
        Long l = null;
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            if (loaded.getChannel().f() != 0) {
                l = Long.valueOf(loaded.getChannel().f());
            }
            this.storeApplicationCommandsFrecency.onCommandUsed(l, ApplicationCommandDataKt.getCommandId(applicationCommandData));
        }
    }

    public final void sendCommand(Context context, MessageManager messageManager, ApplicationCommandData applicationCommandData, List<? extends Attachment<?>> list, boolean z2, boolean z3, Function1<? super Boolean, Unit> function1) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(messageManager, "messageManager");
        m.checkNotNullParameter(applicationCommandData, "applicationCommandData");
        m.checkNotNullParameter(list, "attachmentsRaw");
        m.checkNotNullParameter(function1, "onValidationResult");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            AttachmentContext attachmentContext = toAttachmentContext(n.emptyList(), context);
            ChatInputViewModel$sendCommand$commandResendCompressedHandler$1 chatInputViewModel$sendCommand$commandResendCompressedHandler$1 = (z3 || !attachmentContext.getHasImage()) ? null : new ChatInputViewModel$sendCommand$commandResendCompressedHandler$1(this, context, attachmentContext, messageManager, applicationCommandData, z2);
            if (applicationCommandData.getValidInputs() || z2) {
                Long valueOf = loaded.getChannel().f() != 0 ? Long.valueOf(loaded.getChannel().f()) : null;
                if (z2) {
                    this.storeApplicationCommands.requestApplicationCommandAutocompleteData(valueOf, loaded.getChannelId(), applicationCommandData);
                    function1.invoke(Boolean.TRUE);
                } else if (chatInputViewModel$sendCommand$commandResendCompressedHandler$1 == null || !this.storeUserSettings.getIsAutoImageCompressionEnabled()) {
                    onCommandUsed(applicationCommandData);
                    function1.invoke(Boolean.valueOf(messageManager.sendCommand(loaded.getChannelId(), valueOf, applicationCommandData.getApplicationCommand().getVersion(), applicationCommandData, new MessageManager.AttachmentsRequest(attachmentContext.getCurrentFileSizeMB(), loaded.getMaxFileSizeMB(), attachmentContext.getAttachments()), ChatInputViewModel$sendCommand$validated$1.INSTANCE, ChatInputViewModel$sendCommand$validated$2.INSTANCE, new ChatInputViewModel$sendCommand$validated$3(this, attachmentContext, chatInputViewModel$sendCommand$commandResendCompressedHandler$1))));
                } else {
                    chatInputViewModel$sendCommand$commandResendCompressedHandler$1.invoke();
                }
            } else {
                onCommandInputsInvalid();
                function1.invoke(Boolean.FALSE);
            }
        } else {
            function1.invoke(Boolean.FALSE);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v5, types: [java.lang.Object] */
    public final void sendMessage(Context context, MessageManager messageManager, MessageContent messageContent, List<? extends Attachment<?>> list, boolean z2, Function1<? super Boolean, Unit> function1) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(messageManager, "messageManager");
        m.checkNotNullParameter(messageContent, "messageContent");
        m.checkNotNullParameter(list, "attachmentsRaw");
        m.checkNotNullParameter(function1, "onValidationResult");
        ?? viewState = getViewState();
        boolean z3 = viewState instanceof ViewState.Loaded;
        ViewState.Loaded loaded = viewState;
        if (!z3) {
            loaded = null;
        }
        ViewState.Loaded loaded2 = loaded;
        if (loaded2 != null) {
            AttachmentContext attachmentContext = toAttachmentContext(list, context);
            boolean z4 = true;
            int i = 0;
            boolean z5 = loaded2.getSelectedThreadDraft() != null;
            ChatInputViewModel$sendMessage$messageResendCompressedHandler$1 chatInputViewModel$sendMessage$messageResendCompressedHandler$1 = (z2 || !attachmentContext.getHasImage()) ? null : new ChatInputViewModel$sendMessage$messageResendCompressedHandler$1(this, context, attachmentContext, messageManager, messageContent, function1);
            ChatInputViewModel$sendMessage$messageSendResultHandler$1 chatInputViewModel$sendMessage$messageSendResultHandler$1 = new ChatInputViewModel$sendMessage$messageSendResultHandler$1(this, context, loaded2, attachmentContext, chatInputViewModel$sendMessage$messageResendCompressedHandler$1, z5);
            ChatInputViewModel$sendMessage$onMessageTooLong$1 chatInputViewModel$sendMessage$onMessageTooLong$1 = new ChatInputViewModel$sendMessage$onMessageTooLong$1(this);
            if (chatInputViewModel$sendMessage$messageResendCompressedHandler$1 == null || !this.storeUserSettings.getIsAutoImageCompressionEnabled()) {
                ChatInputViewModel$sendMessage$sendMessage$1 chatInputViewModel$sendMessage$sendMessage$1 = new ChatInputViewModel$sendMessage$sendMessage$1(this, messageManager, messageContent, attachmentContext, loaded2, chatInputViewModel$sendMessage$messageSendResultHandler$1, chatInputViewModel$sendMessage$onMessageTooLong$1, chatInputViewModel$sendMessage$messageResendCompressedHandler$1, function1);
                if (loaded2.isEditing() && loaded2.getEditingMessage() != null) {
                    Message message = loaded2.getEditingMessage().getMessage();
                    long id2 = message.getId();
                    long channelId = message.getChannelId();
                    String textContent = messageContent.getTextContent();
                    String content = message.getContent();
                    if (content != null) {
                        i = content.length();
                    }
                    function1.invoke(Boolean.valueOf(messageManager.editMessage(id2, channelId, textContent, chatInputViewModel$sendMessage$onMessageTooLong$1, Integer.valueOf(i))));
                } else if (z5) {
                    String threadName = loaded2.getThreadDraftState().getThreadName();
                    if (!(threadName == null || threadName.length() == 0)) {
                        z4 = false;
                    }
                    if (z4) {
                        StoreStream.Companion.getThreadDraft().setDraftState(StoreThreadDraft.ThreadDraftState.copy$default(loaded2.getThreadDraftState(), false, null, null, false, true, 15, null));
                        return;
                    }
                    StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft selectedThreadDraft = loaded2.getSelectedThreadDraft();
                    Long starterMessageId = selectedThreadDraft != null ? selectedThreadDraft.getStarterMessageId() : null;
                    StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft selectedThreadDraft2 = loaded2.getSelectedThreadDraft();
                    createAndGotoThread(context, starterMessageId, selectedThreadDraft2 != null ? selectedThreadDraft2.getThreadStartLocation() : null, loaded2, new ChatInputViewModel$sendMessage$1(chatInputViewModel$sendMessage$sendMessage$1));
                } else {
                    chatInputViewModel$sendMessage$sendMessage$1.invoke((ChatInputViewModel$sendMessage$sendMessage$1) Long.valueOf(loaded2.getChannelId()));
                }
            } else {
                chatInputViewModel$sendMessage$messageResendCompressedHandler$1.invoke();
            }
        } else {
            function1.invoke(Boolean.FALSE);
        }
    }

    public final void sendSticker(Sticker sticker, MessageManager messageManager) {
        m.checkNotNullParameter(sticker, "sticker");
        m.checkNotNullParameter(messageManager, "messageManager");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && loaded.getAbleToSendMessage()) {
            this.storeStickers.onStickerUsed(sticker);
            MessageManager.sendMessage$default(messageManager, null, null, null, null, d0.t.m.listOf(sticker), false, null, null, null, 495, null);
        }
    }

    public final AttachmentContext toAttachmentContext(List<? extends Attachment<?>> list, Context context) {
        boolean z2;
        boolean z3;
        boolean z4;
        m.checkNotNullParameter(list, "$this$toAttachmentContext");
        m.checkNotNullParameter(context, "context");
        ArrayList<Attachment> arrayList = new ArrayList(list);
        if (!arrayList.isEmpty()) {
            for (Attachment attachment : arrayList) {
                m.checkNotNullExpressionValue(attachment, "attachment");
                if (AttachmentUtilsKt.isImage(attachment, context.getContentResolver())) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        if (!arrayList.isEmpty()) {
            for (Attachment attachment2 : arrayList) {
                m.checkNotNullExpressionValue(attachment2, "attachment");
                if (AttachmentUtilsKt.isVideo(attachment2, context.getContentResolver())) {
                    z3 = true;
                    break;
                }
            }
        }
        z3 = false;
        if (!arrayList.isEmpty()) {
            for (Attachment attachment3 : arrayList) {
                m.checkNotNullExpressionValue(attachment3, "attachment");
                if (AttachmentUtilsKt.isGif(attachment3, context.getContentResolver())) {
                    z4 = true;
                    break;
                }
            }
        }
        z4 = false;
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
        for (Attachment attachment4 : arrayList) {
            Uri uri = attachment4.getUri();
            ContentResolver contentResolver = context.getContentResolver();
            m.checkNotNullExpressionValue(contentResolver, "context.contentResolver");
            arrayList2.add(Float.valueOf(SendUtilsKt.computeFileSizeMegabytes(uri, contentResolver)));
        }
        float sumOfFloat = u.sumOfFloat(arrayList2);
        Float maxOrNull = u.m87maxOrNull((Iterable<Float>) arrayList2);
        return new AttachmentContext(arrayList, z2, z3, z4, arrayList2, sumOfFloat, maxOrNull != null ? maxOrNull.floatValue() : 0.0f);
    }

    public final void togglePendingReplyShouldMention() {
        ViewState viewState = getViewState();
        ViewState.Loaded.PendingReplyState.Replying replying = null;
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            ViewState.Loaded.PendingReplyState pendingReplyState = loaded.getPendingReplyState();
            if (pendingReplyState instanceof ViewState.Loaded.PendingReplyState.Replying) {
                replying = pendingReplyState;
            }
            ViewState.Loaded.PendingReplyState.Replying replying2 = replying;
            if (replying2 != null) {
                StorePendingReplies storePendingReplies = this.storePendingReplies;
                Long a = replying2.getMessageReference().a();
                m.checkNotNull(a);
                storePendingReplies.onSetPendingReplyShouldMention(a.longValue(), !replying2.getShouldMention());
            }
        }
    }

    public final void verifyAccount(Context context) {
        Unit unit;
        m.checkNotNullParameter(context, "context");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            int ordinal = loaded.getVerificationLevelTriggered().ordinal();
            if (ordinal != 0) {
                if (ordinal == 1) {
                    WidgetUserEmailVerify.Companion.launch(context, WidgetUserAccountVerifyBase.Mode.UNFORCED);
                    unit = Unit.a;
                } else if (!(ordinal == 2 || ordinal == 3)) {
                    if (ordinal == 4) {
                        WidgetUserPhoneManage.Companion.launch(context, WidgetUserAccountVerifyBase.Mode.UNFORCED, WidgetUserPhoneManage.Companion.Source.GUILD_PHONE_REQUIRED);
                        unit = Unit.a;
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                }
                KotlinExtensionsKt.getExhaustive(unit);
            }
            unit = Unit.a;
            KotlinExtensionsKt.getExhaustive(unit);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChatInputViewModel(StoreChat storeChat, StoreChannels storeChannels, StoreMessagesLoader storeMessagesLoader, StoreLurking storeLurking, StoreStickers storeStickers, StorePendingReplies storePendingReplies, StoreApplicationInteractions storeApplicationInteractions, StoreApplicationCommands storeApplicationCommands, StoreApplicationCommandFrecency storeApplicationCommandFrecency, StoreUserSettings storeUserSettings, StoreAnalytics storeAnalytics, StoreThreadDraft storeThreadDraft, StoreGuilds storeGuilds, boolean z2, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(storeChat, "storeChat");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeMessagesLoader, "storeMessagesLoader");
        m.checkNotNullParameter(storeLurking, "storeLurking");
        m.checkNotNullParameter(storeStickers, "storeStickers");
        m.checkNotNullParameter(storePendingReplies, "storePendingReplies");
        m.checkNotNullParameter(storeApplicationInteractions, "storeApplicationInteractions");
        m.checkNotNullParameter(storeApplicationCommands, "storeApplicationCommands");
        m.checkNotNullParameter(storeApplicationCommandFrecency, "storeApplicationCommandsFrecency");
        m.checkNotNullParameter(storeUserSettings, "storeUserSettings");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(storeThreadDraft, "storeThreadDraft");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.storeChat = storeChat;
        this.storeChannels = storeChannels;
        this.storeMessagesLoader = storeMessagesLoader;
        this.storeLurking = storeLurking;
        this.storeStickers = storeStickers;
        this.storePendingReplies = storePendingReplies;
        this.storeApplicationInteractions = storeApplicationInteractions;
        this.storeApplicationCommands = storeApplicationCommands;
        this.storeApplicationCommandsFrecency = storeApplicationCommandFrecency;
        this.storeUserSettings = storeUserSettings;
        this.storeAnalytics = storeAnalytics;
        this.storeThreadDraft = storeThreadDraft;
        this.storeGuilds = storeGuilds;
        this.isEmojiAutocompleteUpsellEnabled = z2;
        PublishSubject<Event> k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.eventSubject = k0;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), ChatInputViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(storeChat.observeEvents(), this, null, 2, null), ChatInputViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(this));
    }
}
