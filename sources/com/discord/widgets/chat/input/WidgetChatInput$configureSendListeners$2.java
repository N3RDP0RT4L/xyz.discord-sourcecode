package com.discord.widgets.chat.input;

import android.content.Context;
import com.discord.models.commands.ApplicationCommand;
import com.discord.widgets.chat.MessageContent;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.autocomplete.InputAutocomplete;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.discord.widgets.chat.input.models.ApplicationCommandValue;
import com.lytefast.flexinput.model.Attachment;
import d0.d0.f;
import d0.t.g0;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
/* compiled from: WidgetChatInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u000b\u001a\u00020\u00072\u0010\u0010\u0002\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00010\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\n¢\u0006\u0004\b\t\u0010\n"}, d2 = {"", "Lcom/lytefast/flexinput/model/Attachment;", "attachmentsRaw", "Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "applicationCommandData", "Lkotlin/Function1;", "", "", "onValidationResult", "invoke", "(Ljava/util/List;Lcom/discord/widgets/chat/input/models/ApplicationCommandData;Lkotlin/jvm/functions/Function1;)V", "sendMessage"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInput$configureSendListeners$2 extends o implements Function3<List<? extends Attachment<?>>, ApplicationCommandData, Function1<? super Boolean, ? extends Unit>, Unit> {
    public final /* synthetic */ WidgetChatInputEditText $chatInput;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ MessageManager $messageManager;
    public final /* synthetic */ WidgetChatInput this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInput$configureSendListeners$2(WidgetChatInput widgetChatInput, WidgetChatInputEditText widgetChatInputEditText, Context context, MessageManager messageManager) {
        super(3);
        this.this$0 = widgetChatInput;
        this.$chatInput = widgetChatInputEditText;
        this.$context = context;
        this.$messageManager = messageManager;
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends Attachment<?>> list, ApplicationCommandData applicationCommandData, Function1<? super Boolean, ? extends Unit> function1) {
        invoke2(list, applicationCommandData, (Function1<? super Boolean, Unit>) function1);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<? extends Attachment<?>> list, ApplicationCommandData applicationCommandData, Function1<? super Boolean, Unit> function1) {
        InputAutocomplete inputAutocomplete;
        MessageContent messageContent;
        MessageContent messageContent2;
        ChatInputViewModel viewModel;
        ApplicationCommand applicationCommand;
        ChatInputViewModel viewModel2;
        String str;
        m.checkNotNullParameter(list, "attachmentsRaw");
        m.checkNotNullParameter(function1, "onValidationResult");
        inputAutocomplete = this.this$0.autocomplete;
        if (inputAutocomplete == null || (messageContent = inputAutocomplete.getInputContent()) == null) {
            messageContent = new MessageContent(this.$chatInput.getText(), n.emptyList());
        }
        if (applicationCommandData == null || (applicationCommand = applicationCommandData.getApplicationCommand()) == null || !applicationCommand.getBuiltIn()) {
            messageContent2 = messageContent;
        } else {
            ApplicationCommand applicationCommand2 = applicationCommandData.getApplicationCommand();
            List<ApplicationCommandValue> values = applicationCommandData.getValues();
            LinkedHashMap linkedHashMap = new LinkedHashMap(f.coerceAtLeast(g0.mapCapacity(d0.t.o.collectionSizeOrDefault(values, 10)), 16));
            for (Object obj : values) {
                linkedHashMap.put(((ApplicationCommandValue) obj).getName(), obj);
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(linkedHashMap.size()));
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                linkedHashMap2.put(entry.getKey(), ((ApplicationCommandValue) entry.getValue()).getValue());
            }
            viewModel2 = this.this$0.getViewModel();
            viewModel2.onCommandUsed(applicationCommandData);
            Function1<Map<String, ? extends Object>, String> execute = applicationCommand2.getExecute();
            if (execute == null || (str = execute.invoke(linkedHashMap2)) == null) {
                str = "";
            }
            messageContent2 = new MessageContent(str, messageContent.getMentionedUsers());
        }
        viewModel = this.this$0.getViewModel();
        viewModel.sendMessage(this.$context, this.$messageManager, messageContent2, list, (r14 & 16) != 0 ? false : false, function1);
    }
}
