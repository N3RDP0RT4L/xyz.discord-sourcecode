package com.discord.widgets.chat.input;

import android.content.Context;
import androidx.fragment.app.Fragment;
import b.b.a.d.d;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.widgets.chat.input.WidgetChatInputAttachments;
import com.lytefast.flexinput.fragment.CameraFragment;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetChatInputAttachments.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInputAttachments$configureFlexInputContentPages$1 extends o implements Function0<Unit> {
    public final /* synthetic */ boolean $canCreateThread;
    public final /* synthetic */ WidgetChatInputAttachments this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInputAttachments$configureFlexInputContentPages$1(WidgetChatInputAttachments widgetChatInputAttachments, boolean z2) {
        super(0);
        this.this$0 = widgetChatInputAttachments;
        this.$canCreateThread = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        FlexInputFragment flexInputFragment;
        FlexInputFragment flexInputFragment2;
        flexInputFragment = this.this$0.flexInputFragment;
        final Context requireContext = flexInputFragment.requireContext();
        m.checkNotNullExpressionValue(requireContext, "flexInputFragment.requireContext()");
        final int themedDrawableRes$default = DrawableCompat.getThemedDrawableRes$default(requireContext, (int) R.attr.ic_flex_input_image, 0, 2, (Object) null);
        final int themedDrawableRes$default2 = DrawableCompat.getThemedDrawableRes$default(requireContext, (int) R.attr.ic_flex_input_file, 0, 2, (Object) null);
        final int themedDrawableRes$default3 = DrawableCompat.getThemedDrawableRes$default(requireContext, (int) R.attr.ic_flex_input_add_a_photo, 0, 2, (Object) null);
        List mutableListOf = n.mutableListOf(new d.a(themedDrawableRes$default, R.string.attachment_media) { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$configureFlexInputContentPages$1$pageArray$1
            @Override // b.b.a.d.d.a
            public WidgetChatInputAttachments.DiscordMediaFragment createFragment() {
                return new WidgetChatInputAttachments.DiscordMediaFragment();
            }
        }, new d.a(themedDrawableRes$default2, R.string.attachment_files) { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$configureFlexInputContentPages$1$pageArray$2
            @Override // b.b.a.d.d.a
            public WidgetChatInputAttachments.DiscordFilesFragment createFragment() {
                return new WidgetChatInputAttachments.DiscordFilesFragment();
            }
        }, new d.a(themedDrawableRes$default3, R.string.camera) { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$configureFlexInputContentPages$1$pageArray$3
            @Override // b.b.a.d.d.a
            public CameraFragment createFragment() {
                return new CameraFragment();
            }
        });
        if (this.$canCreateThread) {
            final int themedDrawableRes$default4 = DrawableCompat.getThemedDrawableRes$default(requireContext, (int) R.attr.ic_flex_input_create_thread, 0, 2, (Object) null);
            mutableListOf.add(new d.a(themedDrawableRes$default4, R.string.create_thread) { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$configureFlexInputContentPages$1$page$1
                @Override // b.b.a.d.d.a
                public Fragment createFragment() {
                    return new Fragment();
                }
            });
        }
        flexInputFragment2 = this.this$0.flexInputFragment;
        Object[] array = mutableListOf.toArray(new d.a[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        d.a[] aVarArr = (d.a[]) array;
        Objects.requireNonNull(flexInputFragment2);
        m.checkNotNullParameter(aVarArr, "pageSuppliers");
        flexInputFragment2.r = aVarArr;
        for (Function0<Unit> function0 : flexInputFragment2.f3138x) {
            function0.invoke();
        }
        flexInputFragment2.f3138x.clear();
    }
}
