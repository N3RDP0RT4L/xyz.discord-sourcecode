package com.discord.widgets.chat.input;

import android.content.Context;
import androidx.exifinterface.media.ExifInterface;
import com.discord.app.AppLog;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.attachments.AttachmentUtilsKt;
import com.discord.widgets.chat.input.WidgetChatInputAttachments$createPreviewAdapter$1;
import com.lytefast.flexinput.adapters.AttachmentPreviewAdapter;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.utils.SelectionAggregator;
import com.lytefast.flexinput.utils.SelectionCoordinator;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatInputAttachments.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u00020\u00010\u00002\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/lytefast/flexinput/model/Attachment;", "", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;", "previewAdapter", "Lcom/lytefast/flexinput/utils/SelectionAggregator;", "invoke", "(Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;)Lcom/lytefast/flexinput/utils/SelectionAggregator;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInputAttachments$createPreviewAdapter$1 extends o implements Function1<AttachmentPreviewAdapter<T>, SelectionAggregator<T>> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ WidgetChatInputAttachments this$0;

    /* compiled from: WidgetChatInputAttachments.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001J\u0013\u0010\u0003\u001a\u00020\u0002*\u00028\u0000H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J!\u0010\u0007\u001a\u00020\u00022\u0010\u0010\u0006\u001a\f\u0012\u0004\u0012\u00028\u0000\u0012\u0002\b\u00030\u0005H\u0014¢\u0006\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"com/discord/widgets/chat/input/WidgetChatInputAttachments$createPreviewAdapter$1$1", "Lcom/lytefast/flexinput/utils/SelectionAggregator;", "", "track", "(Lcom/lytefast/flexinput/model/Attachment;)V", "Lcom/lytefast/flexinput/utils/SelectionCoordinator;", "selectionCoordinator", "registerSelectionCoordinatorInternal", "(Lcom/lytefast/flexinput/utils/SelectionCoordinator;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.WidgetChatInputAttachments$createPreviewAdapter$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends SelectionAggregator<T> {
        public final /* synthetic */ AttachmentPreviewAdapter $previewAdapter;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(AttachmentPreviewAdapter attachmentPreviewAdapter, AttachmentPreviewAdapter attachmentPreviewAdapter2) {
            super(attachmentPreviewAdapter2, null, null, null, 14, null);
            this.$previewAdapter = attachmentPreviewAdapter;
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* JADX WARN: Incorrect types in method signature: (TT;)V */
        public final void track(Attachment attachment) {
            FlexInputFragment flexInputFragment;
            String source = attachment instanceof SourcedAttachment ? ((SourcedAttachment) attachment).getSource() : AnalyticsTracker.ATTACHMENT_SOURCE_PICKER;
            try {
                flexInputFragment = WidgetChatInputAttachments$createPreviewAdapter$1.this.this$0.flexInputFragment;
                int size = flexInputFragment.b().getSize();
                Context context = WidgetChatInputAttachments$createPreviewAdapter$1.this.$context;
                AnalyticsTracker.addAttachment(source, AttachmentUtilsKt.getMimeType(attachment, context != null ? context.getContentResolver() : null), size);
            } catch (Throwable th) {
                AppLog.g.i("Analytic error on attachment update", th);
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.lytefast.flexinput.utils.SelectionAggregator
        public void registerSelectionCoordinatorInternal(SelectionCoordinator<T, ?> selectionCoordinator) {
            m.checkNotNullParameter(selectionCoordinator, "selectionCoordinator");
            super.registerSelectionCoordinatorInternal(selectionCoordinator);
            final SelectionCoordinator.ItemSelectionListener<? super I> itemSelectionListener = selectionCoordinator.c;
            SelectionCoordinator.ItemSelectionListener widgetChatInputAttachments$createPreviewAdapter$1$1$registerSelectionCoordinatorInternal$1 = new SelectionCoordinator.ItemSelectionListener<T>(itemSelectionListener) { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$createPreviewAdapter$1$1$registerSelectionCoordinatorInternal$1
                private final /* synthetic */ SelectionCoordinator.ItemSelectionListener<? super T> $$delegate_0;
                public final /* synthetic */ SelectionCoordinator.ItemSelectionListener $oldItemSelectionListener;

                {
                    this.$oldItemSelectionListener = itemSelectionListener;
                    this.$$delegate_0 = itemSelectionListener;
                }

                /* JADX WARN: Incorrect types in method signature: (TT;)V */
                public void onItemUnselected(Attachment attachment) {
                    m.checkNotNullParameter(attachment, "item");
                    this.$$delegate_0.onItemUnselected(attachment);
                }

                @Override // com.lytefast.flexinput.utils.SelectionCoordinator.ItemSelectionListener
                public void unregister() {
                    this.$$delegate_0.unregister();
                }

                /* JADX WARN: Incorrect types in method signature: (TT;)V */
                public void onItemSelected(Attachment attachment) {
                    m.checkNotNullParameter(attachment, "item");
                    WidgetChatInputAttachments$createPreviewAdapter$1.AnonymousClass1.this.track(attachment);
                    this.$oldItemSelectionListener.onItemSelected(attachment);
                }
            };
            m.checkNotNullParameter(widgetChatInputAttachments$createPreviewAdapter$1$1$registerSelectionCoordinatorInternal$1, "<set-?>");
            selectionCoordinator.c = widgetChatInputAttachments$createPreviewAdapter$1$1$registerSelectionCoordinatorInternal$1;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInputAttachments$createPreviewAdapter$1(WidgetChatInputAttachments widgetChatInputAttachments, Context context) {
        super(1);
        this.this$0 = widgetChatInputAttachments;
        this.$context = context;
    }

    public final SelectionAggregator<T> invoke(AttachmentPreviewAdapter<T> attachmentPreviewAdapter) {
        m.checkNotNullParameter(attachmentPreviewAdapter, "previewAdapter");
        return new AnonymousClass1(attachmentPreviewAdapter, attachmentPreviewAdapter);
    }
}
