package com.discord.widgets.chat.input;

import com.discord.models.commands.ApplicationCommandOption;
import com.discord.widgets.chat.input.autocomplete.InputAutocomplete;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/commands/ApplicationCommandOption;", "it", "", "invoke", "(Lcom/discord/models/commands/ApplicationCommandOption;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInput$configureSendListeners$5 extends o implements Function1<ApplicationCommandOption, Unit> {
    public final /* synthetic */ WidgetChatInput$configureSendListeners$1 $sendCommand$1;
    public final /* synthetic */ WidgetChatInput this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInput$configureSendListeners$5(WidgetChatInput widgetChatInput, WidgetChatInput$configureSendListeners$1 widgetChatInput$configureSendListeners$1) {
        super(1);
        this.this$0 = widgetChatInput;
        this.$sendCommand$1 = widgetChatInput$configureSendListeners$1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ApplicationCommandOption applicationCommandOption) {
        invoke2(applicationCommandOption);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ApplicationCommandOption applicationCommandOption) {
        InputAutocomplete inputAutocomplete;
        m.checkNotNullParameter(applicationCommandOption, "it");
        inputAutocomplete = this.this$0.autocomplete;
        ApplicationCommandData applicationCommandData = inputAutocomplete != null ? inputAutocomplete.getApplicationCommandData(applicationCommandOption) : null;
        if (applicationCommandData != null) {
            WidgetChatInput$configureSendListeners$1.invoke$default(this.$sendCommand$1, applicationCommandData, n.emptyList(), true, null, 8, null);
        }
    }
}
