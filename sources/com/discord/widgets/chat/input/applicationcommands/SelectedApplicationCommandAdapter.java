package com.discord.widgets.chat.input.applicationcommands;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.objectweb.asm.Opcodes;
import xyz.discord.R;
/* compiled from: SelectedApplicationCommandAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B#\u0012\u0006\u00103\u001a\u000202\u0012\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00030%¢\u0006\u0004\b4\u00105J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001d\u0010\n\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\f\u001a\u00020\u0003¢\u0006\u0004\b\f\u0010\u0005J\u0015\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\r¢\u0006\u0004\b\u0012\u0010\u0010J+\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00172\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0015H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\r\u0010\u001a\u001a\u00020\u0003¢\u0006\u0004\b\u001a\u0010\u0005J/\u0010 \u001a\u00020\u00032\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u001c0\u001b2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\r0\u001e¢\u0006\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R%\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00030%8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u0018\u0010*\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b*\u0010+R\"\u0010.\u001a\u000e\u0012\u0004\u0012\u00020-\u0012\u0004\u0012\u00020\u00150,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010/R\u0018\u00100\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u00101¨\u00066"}, d2 = {"Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;", "", "configureVerified", "()V", "Lcom/discord/models/commands/ApplicationCommand;", "command", "Lcom/discord/models/commands/Application;", "application", "setApplicationCommand", "(Lcom/discord/models/commands/ApplicationCommand;Lcom/discord/models/commands/Application;)V", "clear", "Lcom/discord/models/commands/ApplicationCommandOption;", "commandOption", "highlightOption", "(Lcom/discord/models/commands/ApplicationCommandOption;)V", "option", "onParamClicked", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "clearParamOptionHighlight", "", "", "verifiedInputs", "", "showOptionErrorSet", "setVerified", "(Ljava/util/Map;Ljava/util/Set;)V", "Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;", "scroller", "Landroidx/recyclerview/widget/RecyclerView$SmoothScroller;", "Lkotlin/Function1;", "onClickApplicationCommandOption", "Lkotlin/jvm/functions/Function1;", "getOnClickApplicationCommandOption", "()Lkotlin/jvm/functions/Function1;", "currentCommand", "Lcom/discord/models/commands/ApplicationCommand;", "", "", "paramPositions", "Ljava/util/Map;", "highlightedCommandOption", "Lcom/discord/models/commands/ApplicationCommandOption;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SelectedApplicationCommandAdapter extends MGRecyclerAdapterSimple<SelectedApplicationCommandItem> {
    private ApplicationCommand currentCommand;
    private ApplicationCommandOption highlightedCommandOption;
    private final Function1<ApplicationCommandOption, Unit> onClickApplicationCommandOption;
    private final Map<String, Integer> paramPositions = new LinkedHashMap();
    private RecyclerView.SmoothScroller scroller;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public SelectedApplicationCommandAdapter(RecyclerView recyclerView, Function1<? super ApplicationCommandOption, Unit> function1) {
        super(recyclerView, false);
        m.checkNotNullParameter(recyclerView, "recyclerView");
        m.checkNotNullParameter(function1, "onClickApplicationCommandOption");
        this.onClickApplicationCommandOption = function1;
        final Context context = getContext();
        this.scroller = new LinearSmoothScroller(context) { // from class: com.discord.widgets.chat.input.applicationcommands.SelectedApplicationCommandAdapter$scroller$1
            @Override // androidx.recyclerview.widget.LinearSmoothScroller
            public int getHorizontalSnapPreference() {
                return -1;
            }
        };
    }

    private final void configureVerified() {
    }

    public final void clear() {
        setData(n.emptyList());
        this.paramPositions.clear();
        this.currentCommand = null;
        this.highlightedCommandOption = null;
    }

    public final void clearParamOptionHighlight() {
        if (getItemCount() != 0) {
            Map<String, Integer> map = this.paramPositions;
            ApplicationCommandOption applicationCommandOption = this.highlightedCommandOption;
            Integer num = map.get(applicationCommandOption != null ? applicationCommandOption.getName() : null);
            if (num != null) {
                int intValue = num.intValue();
                getItem(intValue).setHighlighted(false);
                notifyItemChanged(intValue);
            }
        }
    }

    public final Function1<ApplicationCommandOption, Unit> getOnClickApplicationCommandOption() {
        return this.onClickApplicationCommandOption;
    }

    public final void highlightOption(final ApplicationCommandOption applicationCommandOption) {
        Integer num;
        m.checkNotNullParameter(applicationCommandOption, "commandOption");
        if (!m.areEqual(this.highlightedCommandOption, applicationCommandOption)) {
            clearParamOptionHighlight();
            this.highlightedCommandOption = applicationCommandOption;
            if (getItemCount() != 0 && (num = this.paramPositions.get(applicationCommandOption.getName())) != null) {
                final int intValue = num.intValue();
                this.highlightedCommandOption = applicationCommandOption;
                getItem(intValue).setHighlighted(true);
                notifyItemChanged(intValue);
                getRecycler().post(new Runnable() { // from class: com.discord.widgets.chat.input.applicationcommands.SelectedApplicationCommandAdapter$highlightOption$$inlined$let$lambda$1
                    @Override // java.lang.Runnable
                    public final void run() {
                        RecyclerView.SmoothScroller smoothScroller;
                        RecyclerView.SmoothScroller smoothScroller2;
                        int i = intValue;
                        if (i == 1) {
                            i = 0;
                        }
                        smoothScroller = this.scroller;
                        smoothScroller.setTargetPosition(i);
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) this.getRecycler().getLayoutManager();
                        if (linearLayoutManager != null) {
                            smoothScroller2 = this.scroller;
                            linearLayoutManager.startSmoothScroll(smoothScroller2);
                        }
                    }
                });
            }
        }
    }

    public final void onParamClicked(ApplicationCommandOption applicationCommandOption) {
        m.checkNotNullParameter(applicationCommandOption, "option");
        this.onClickApplicationCommandOption.invoke(applicationCommandOption);
    }

    public final void setApplicationCommand(ApplicationCommand applicationCommand, Application application) {
        m.checkNotNullParameter(applicationCommand, "command");
        m.checkNotNullParameter(application, "application");
        ApplicationCommand applicationCommand2 = this.currentCommand;
        if (!m.areEqual(applicationCommand2 != null ? applicationCommand2.getId() : null, applicationCommand.getId())) {
            this.currentCommand = applicationCommand;
            ArrayList arrayList = new ArrayList();
            arrayList.add(new SelectedApplicationCommandItem(application, null, false, false, false, applicationCommand.getName(), null, 94, null));
            List<ApplicationCommandOption> options = applicationCommand.getOptions();
            ArrayList arrayList2 = new ArrayList();
            for (Object obj : options) {
                if (((ApplicationCommandOption) obj).getRequired()) {
                    arrayList2.add(obj);
                }
            }
            int i = 0;
            int i2 = 0;
            for (Object obj2 : arrayList2) {
                i2++;
                if (i2 < 0) {
                    n.throwIndexOverflow();
                }
                ApplicationCommandOption applicationCommandOption = (ApplicationCommandOption) obj2;
                this.paramPositions.put(applicationCommandOption.getName(), Integer.valueOf(arrayList.size()));
                arrayList.add(new SelectedApplicationCommandItem(null, applicationCommandOption, false, false, false, null, null, Opcodes.LUSHR, null));
            }
            List<ApplicationCommandOption> options2 = applicationCommand.getOptions();
            if (!(options2 instanceof Collection) || !options2.isEmpty()) {
                for (ApplicationCommandOption applicationCommandOption2 : options2) {
                    if ((!applicationCommandOption2.getRequired()) && (i = i + 1) < 0) {
                        n.throwCountOverflow();
                    }
                }
            }
            if (i != 0) {
                arrayList.add(new SelectedApplicationCommandItem(null, null, false, false, false, null, getRecycler().getResources().getString(R.string.commands_optional_header), 63, null));
            }
            List<ApplicationCommandOption> options3 = applicationCommand.getOptions();
            ArrayList<ApplicationCommandOption> arrayList3 = new ArrayList();
            for (Object obj3 : options3) {
                if (!((ApplicationCommandOption) obj3).getRequired()) {
                    arrayList3.add(obj3);
                }
            }
            for (ApplicationCommandOption applicationCommandOption3 : arrayList3) {
                this.paramPositions.put(applicationCommandOption3.getName(), Integer.valueOf(arrayList.size()));
                arrayList.add(new SelectedApplicationCommandItem(null, applicationCommandOption3, false, false, false, null, null, Opcodes.LUSHR, null));
            }
            setData(arrayList);
        }
    }

    public final void setVerified(Map<ApplicationCommandOption, Boolean> map, Set<ApplicationCommandOption> set) {
        m.checkNotNullParameter(map, "verifiedInputs");
        m.checkNotNullParameter(set, "showOptionErrorSet");
        if (getItemCount() != 0) {
            for (ApplicationCommandOption applicationCommandOption : map.keySet()) {
                Integer num = this.paramPositions.get(applicationCommandOption.getName());
                if (num != null) {
                    int intValue = num.intValue();
                    getItem(intValue).setCompleted(m.areEqual(map.get(applicationCommandOption), Boolean.TRUE));
                    getItem(intValue).setError(set.contains(applicationCommandOption));
                }
            }
            configureVerified();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<SelectedApplicationCommandAdapter, SelectedApplicationCommandItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new SelectedAppCommandTitleAdapterItem(this);
        }
        if (i == 1) {
            return new SelectedAppCommandOptionAdapterItem(this);
        }
        if (i == 2) {
            return new SelectedAppCommandSectionHeadingAdapterItem(this);
        }
        throw invalidViewTypeException(i);
    }
}
