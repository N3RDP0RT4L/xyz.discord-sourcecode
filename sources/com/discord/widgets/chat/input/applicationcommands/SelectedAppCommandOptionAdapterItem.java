package com.discord.widgets.chat.input.applicationcommands;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.discord.databinding.ViewAppcommandsOptionparamListitemBinding;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: SelectedApplicationCommandAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/chat/input/applicationcommands/SelectedAppCommandOptionAdapterItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;", "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandItem;)V", "Lcom/discord/databinding/ViewAppcommandsOptionparamListitemBinding;", "binding", "Lcom/discord/databinding/ViewAppcommandsOptionparamListitemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SelectedAppCommandOptionAdapterItem extends MGRecyclerViewHolder<SelectedApplicationCommandAdapter, SelectedApplicationCommandItem> {
    private final ViewAppcommandsOptionparamListitemBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SelectedAppCommandOptionAdapterItem(SelectedApplicationCommandAdapter selectedApplicationCommandAdapter) {
        super((int) R.layout.view_appcommands_optionparam_listitem, selectedApplicationCommandAdapter);
        m.checkNotNullParameter(selectedApplicationCommandAdapter, "adapter");
        View view = this.itemView;
        TextView textView = (TextView) view.findViewById(R.id.appcommands_optiontitle_title);
        if (textView != null) {
            ViewAppcommandsOptionparamListitemBinding viewAppcommandsOptionparamListitemBinding = new ViewAppcommandsOptionparamListitemBinding((FrameLayout) view, textView);
            m.checkNotNullExpressionValue(viewAppcommandsOptionparamListitemBinding, "ViewAppcommandsOptionpar…temBinding.bind(itemView)");
            this.binding = viewAppcommandsOptionparamListitemBinding;
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.appcommands_optiontitle_title)));
    }

    public static final /* synthetic */ SelectedApplicationCommandAdapter access$getAdapter$p(SelectedAppCommandOptionAdapterItem selectedAppCommandOptionAdapterItem) {
        return (SelectedApplicationCommandAdapter) selectedAppCommandOptionAdapterItem.adapter;
    }

    public void onConfigure(int i, final SelectedApplicationCommandItem selectedApplicationCommandItem) {
        int i2;
        m.checkNotNullParameter(selectedApplicationCommandItem, "data");
        super.onConfigure(i, (int) selectedApplicationCommandItem);
        final ApplicationCommandOption option = selectedApplicationCommandItem.getOption();
        if (option != null) {
            TextView textView = this.binding.f2155b;
            m.checkNotNullExpressionValue(textView, "binding.appcommandsOptiontitleTitle");
            textView.setText(option.getName());
            int i3 = selectedApplicationCommandItem.getHighlighted() ? R.drawable.drawable_bg_command_param_highlight : R.drawable.drawable_bg_command_param_normal;
            if (selectedApplicationCommandItem.getHighlighted()) {
                TextView textView2 = this.binding.f2155b;
                m.checkNotNullExpressionValue(textView2, "binding.appcommandsOptiontitleTitle");
                i2 = ColorCompat.getThemedColor(textView2, (int) R.attr.colorHeaderPrimary);
            } else if (selectedApplicationCommandItem.getError()) {
                TextView textView3 = this.binding.f2155b;
                m.checkNotNullExpressionValue(textView3, "binding.appcommandsOptiontitleTitle");
                i2 = ColorCompat.getColor(textView3, (int) R.color.status_red_500);
            } else {
                TextView textView4 = this.binding.f2155b;
                m.checkNotNullExpressionValue(textView4, "binding.appcommandsOptiontitleTitle");
                i2 = ColorCompat.getThemedColor(textView4, (int) R.attr.colorHeaderPrimary);
            }
            if (!selectedApplicationCommandItem.getCompleted() || selectedApplicationCommandItem.getHighlighted()) {
                TextView textView5 = this.binding.f2155b;
                m.checkNotNullExpressionValue(textView5, "binding.appcommandsOptiontitleTitle");
                textView5.setAlpha(1.0f);
            } else {
                TextView textView6 = this.binding.f2155b;
                m.checkNotNullExpressionValue(textView6, "binding.appcommandsOptiontitleTitle");
                textView6.setAlpha(0.5f);
            }
            this.binding.f2155b.setBackgroundResource(i3);
            this.binding.f2155b.setTextColor(i2);
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.applicationcommands.SelectedAppCommandOptionAdapterItem$onConfigure$$inlined$let$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SelectedAppCommandOptionAdapterItem.access$getAdapter$p(this).onParamClicked(ApplicationCommandOption.this);
                }
            });
        }
    }
}
