package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.databinding.CommandCategoryItemBinding;
import com.discord.models.commands.Application;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.icon.IconUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatInputCategoriesAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00060\f¢\u0006\u0004\b\u000f\u0010\u0010J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\"\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00060\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/chat/input/ApplicationCategoryViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/models/commands/Application;", "application", "", "isSelected", "", "bind", "(Lcom/discord/models/commands/Application;Z)V", "Lcom/discord/databinding/CommandCategoryItemBinding;", "binding", "Lcom/discord/databinding/CommandCategoryItemBinding;", "Lkotlin/Function1;", "onItemSelected", "Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(Lcom/discord/databinding/CommandCategoryItemBinding;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ApplicationCategoryViewHolder extends RecyclerView.ViewHolder {
    private final CommandCategoryItemBinding binding;
    private final Function1<Application, Unit> onItemSelected;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public ApplicationCategoryViewHolder(CommandCategoryItemBinding commandCategoryItemBinding, Function1<? super Application, Unit> function1) {
        super(commandCategoryItemBinding.a);
        m.checkNotNullParameter(commandCategoryItemBinding, "binding");
        m.checkNotNullParameter(function1, "onItemSelected");
        this.binding = commandCategoryItemBinding;
        this.onItemSelected = function1;
    }

    public final void bind(final Application application, boolean z2) {
        int i;
        CharSequence b2;
        m.checkNotNullParameter(application, "application");
        this.binding.f2078b.setImageURI((String) null);
        SimpleDraweeView simpleDraweeView = this.binding.f2078b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.commandCategoryItemIcon");
        IconUtils.setApplicationIcon(simpleDraweeView, application);
        SimpleDraweeView simpleDraweeView2 = this.binding.f2078b;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.commandCategoryItemIcon");
        simpleDraweeView2.setSelected(z2);
        View view = this.binding.c.f160b;
        m.checkNotNullExpressionValue(view, "binding.overline.express…CategorySelectionOverline");
        view.setVisibility(z2 ? 0 : 8);
        if (z2) {
            SimpleDraweeView simpleDraweeView3 = this.binding.f2078b;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.commandCategoryItemIcon");
            i = ColorCompat.getThemedColor(simpleDraweeView3, (int) R.attr.colorInteractiveActive);
        } else {
            SimpleDraweeView simpleDraweeView4 = this.binding.f2078b;
            m.checkNotNullExpressionValue(simpleDraweeView4, "binding.commandCategoryItemIcon");
            i = ColorCompat.getThemedColor(simpleDraweeView4, (int) R.attr.colorInteractiveNormal);
        }
        SimpleDraweeView simpleDraweeView5 = this.binding.f2078b;
        m.checkNotNullExpressionValue(simpleDraweeView5, "binding.commandCategoryItemIcon");
        ColorCompatKt.tintWithColor(simpleDraweeView5, i);
        SimpleDraweeView simpleDraweeView6 = this.binding.f2078b;
        m.checkNotNullExpressionValue(simpleDraweeView6, "binding.commandCategoryItemIcon");
        simpleDraweeView6.setAlpha(z2 ? 1.0f : 0.5f);
        this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.ApplicationCategoryViewHolder$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Function1 function1;
                function1 = ApplicationCategoryViewHolder.this.onItemSelected;
                function1.invoke(application);
            }
        });
        View view2 = this.itemView;
        m.checkNotNullExpressionValue(view2, "itemView");
        b2 = b.b(a.x(this.itemView, "itemView", "itemView.context"), R.string.command_accessibility_desc_app_item, new Object[]{application.getName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        view2.setContentDescription(b2);
    }
}
