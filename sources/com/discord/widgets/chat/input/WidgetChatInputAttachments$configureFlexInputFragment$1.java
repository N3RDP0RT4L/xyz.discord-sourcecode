package com.discord.widgets.chat.input;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.b.a.f.a;
import com.discord.app.AppFragment;
import com.lytefast.flexinput.R;
import com.lytefast.flexinput.adapters.AttachmentPreviewAdapter;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.managers.FileManager;
import com.lytefast.flexinput.model.Attachment;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatInputAttachments.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInputAttachments$configureFlexInputFragment$1 extends o implements Function0<Unit> {
    public final /* synthetic */ AppFragment $fragment;
    public final /* synthetic */ WidgetChatInputAttachments this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInputAttachments$configureFlexInputFragment$1(WidgetChatInputAttachments widgetChatInputAttachments, AppFragment appFragment) {
        super(0);
        this.this$0 = widgetChatInputAttachments;
        this.$fragment = appFragment;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        FlexInputFragment flexInputFragment;
        FlexInputFragment flexInputFragment2;
        AttachmentPreviewAdapter<Attachment<Object>> createPreviewAdapter;
        FlexInputFragment flexInputFragment3;
        Fragment createAndConfigureExpressionFragment;
        this.this$0.configureFlexInputContentPages(false);
        flexInputFragment = this.this$0.flexInputFragment;
        FileManager fileManager = this.$fragment.getFileManager();
        Objects.requireNonNull(flexInputFragment);
        m.checkNotNullParameter(fileManager, "<set-?>");
        flexInputFragment.p = fileManager;
        flexInputFragment.n = new a() { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$configureFlexInputFragment$1$$special$$inlined$apply$lambda$1
            @Override // b.b.a.f.a
            public void requestDisplay(View view) {
                m.checkNotNullParameter(view, "view");
                WidgetChatInputAttachments$configureFlexInputFragment$1.this.$fragment.showKeyboard(view);
            }

            @Override // b.b.a.f.a
            public void requestHide() {
                FlexInputFragment flexInputFragment4;
                WidgetChatInputAttachments$configureFlexInputFragment$1 widgetChatInputAttachments$configureFlexInputFragment$1 = WidgetChatInputAttachments$configureFlexInputFragment$1.this;
                AppFragment appFragment = widgetChatInputAttachments$configureFlexInputFragment$1.$fragment;
                flexInputFragment4 = widgetChatInputAttachments$configureFlexInputFragment$1.this$0.flexInputFragment;
                appFragment.hideKeyboard(flexInputFragment4.l());
            }
        };
        flexInputFragment2 = this.this$0.flexInputFragment;
        flexInputFragment2.l().setInputContentHandler(new WidgetChatInputAttachments$configureFlexInputFragment$1$$special$$inlined$apply$lambda$2(this));
        createPreviewAdapter = this.this$0.createPreviewAdapter(this.$fragment.getContext());
        m.checkNotNullParameter(createPreviewAdapter, "previewAdapter");
        createPreviewAdapter.a.initFrom(flexInputFragment.b());
        flexInputFragment.q = createPreviewAdapter;
        RecyclerView recyclerView = flexInputFragment.j().d;
        m.checkNotNullExpressionValue(recyclerView, "binding.attachmentPreviewList");
        AttachmentPreviewAdapter<Attachment<Object>> attachmentPreviewAdapter = flexInputFragment.q;
        if (attachmentPreviewAdapter == null) {
            m.throwUninitializedPropertyAccessException("attachmentPreviewAdapter");
        }
        recyclerView.setAdapter(attachmentPreviewAdapter);
        WidgetChatInputAttachments widgetChatInputAttachments = this.this$0;
        FragmentManager childFragmentManager = this.$fragment.getChildFragmentManager();
        m.checkNotNullExpressionValue(childFragmentManager, "fragment.childFragmentManager");
        flexInputFragment3 = this.this$0.flexInputFragment;
        createAndConfigureExpressionFragment = widgetChatInputAttachments.createAndConfigureExpressionFragment(childFragmentManager, flexInputFragment3.l());
        if (createAndConfigureExpressionFragment != null) {
            flexInputFragment.getChildFragmentManager().beginTransaction().replace(R.f.expression_tray_container, createAndConfigureExpressionFragment, createAndConfigureExpressionFragment.getClass().getSimpleName()).commit();
            AppCompatImageButton appCompatImageButton = flexInputFragment.j().i;
            m.checkNotNullExpressionValue(appCompatImageButton, "binding.expressionBtn");
            appCompatImageButton.setVisibility(0);
        }
    }
}
