package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandKt;
import com.discord.models.commands.ModelApplicationComparator;
import com.discord.stores.DiscoverCommands;
import com.discord.stores.LoadState;
import com.discord.widgets.chat.input.autocomplete.ApplicationCommandAutocompletable;
import com.discord.widgets.chat.input.autocomplete.ApplicationCommandLoadingPlaceholder;
import com.discord.widgets.chat.input.autocomplete.ApplicationPlaceholder;
import com.discord.widgets.chat.input.autocomplete.Autocompletable;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetChatInputDiscoveryCommandsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0015\b\u0086\b\u0018\u0000 22\u00020\u0001:\u00012BQ\u0012\u001e\u0010\u0016\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u00060\u0002\u0012\u0006\u0010\u0017\u001a\u00020\t\u0012\u0006\u0010\u0018\u001a\u00020\t\u0012\u0006\u0010\u0019\u001a\u00020\r\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0010\u0012\u0006\u0010\u001b\u001a\u00020\u0013¢\u0006\u0004\b0\u00101J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005J(\u0010\b\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u00060\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0005J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015Jf\u0010\u001c\u001a\u00020\u00002 \b\u0002\u0010\u0016\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u00060\u00022\b\b\u0002\u0010\u0017\u001a\u00020\t2\b\b\u0002\u0010\u0018\u001a\u00020\t2\b\b\u0002\u0010\u0019\u001a\u00020\r2\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00102\b\b\u0002\u0010\u001b\u001a\u00020\u0013HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b!\u0010\u000fJ\u001a\u0010#\u001a\u00020\t2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u0019\u0010\u001b\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010%\u001a\u0004\b&\u0010\u0015R1\u0010\u0016\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u00060\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010'\u001a\u0004\b(\u0010\u0005R\u0019\u0010\u0018\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010)\u001a\u0004\b*\u0010\u000bR\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010+\u001a\u0004\b,\u0010\u0012R\u0019\u0010\u0017\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b-\u0010\u000bR\u0019\u0010\u0019\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010.\u001a\u0004\b/\u0010\u000f¨\u00063"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", "", "", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "getFlattenCommandsModel", "()Ljava/util/List;", "Lkotlin/Pair;", "Lcom/discord/models/commands/Application;", "component1", "", "component2", "()Z", "component3", "", "component4", "()I", "", "component5", "()Ljava/lang/Long;", "Lcom/discord/stores/LoadState;", "component6", "()Lcom/discord/stores/LoadState;", "commandsByApplication", "hasMoreBefore", "hasMoreAfter", "jumpedSequenceId", "jumpedApplicationId", "loadState", "copy", "(Ljava/util/List;ZZILjava/lang/Long;Lcom/discord/stores/LoadState;)Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/LoadState;", "getLoadState", "Ljava/util/List;", "getCommandsByApplication", "Z", "getHasMoreAfter", "Ljava/lang/Long;", "getJumpedApplicationId", "getHasMoreBefore", "I", "getJumpedSequenceId", HookHelper.constructorName, "(Ljava/util/List;ZZILjava/lang/Long;Lcom/discord/stores/LoadState;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInputDiscoveryCommandsModel {
    public static final Companion Companion = new Companion(null);
    private final List<Pair<Application, List<Autocompletable>>> commandsByApplication;
    private final boolean hasMoreAfter;
    private final boolean hasMoreBefore;
    private final Long jumpedApplicationId;
    private final int jumpedSequenceId;
    private final LoadState loadState;

    /* compiled from: WidgetChatInputDiscoveryCommandsModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ%\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJa\u0010\u0017\u001a\u00020\u00162\n\u0010\f\u001a\u00060\nj\u0002`\u000b2\u0010\u0010\u000e\u001a\f\u0012\b\u0012\u00060\nj\u0002`\r0\u00062\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u00062\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u0006H\u0007¢\u0006\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel$Companion;", "", "", "placeholdersCount", "Lcom/discord/models/commands/Application;", "app", "", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "createPlaceholderModels", "(ILcom/discord/models/commands/Application;)Ljava/util/List;", "", "Lcom/discord/primitives/UserId;", "myId", "Lcom/discord/primitives/RoleId;", "myRoles", "applications", "Lcom/discord/stores/DiscoverCommands;", "discoveryCommands", "", "includeHeaders", "Lcom/discord/models/commands/ApplicationCommand;", "frecencyCommands", "Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", "parseModelDiscoveryCommands", "(JLjava/util/List;Ljava/util/List;Lcom/discord/stores/DiscoverCommands;ZILjava/util/List;)Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final List<Autocompletable> createPlaceholderModels(int i, Application application) {
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < i; i2++) {
                arrayList.add(i2, new ApplicationCommandLoadingPlaceholder(application));
            }
            return arrayList;
        }

        public final WidgetChatInputDiscoveryCommandsModel parseModelDiscoveryCommands(long j, List<Long> list, List<Application> list2, DiscoverCommands discoverCommands, boolean z2, int i, List<? extends ApplicationCommand> list3) {
            m.checkNotNullParameter(list, "myRoles");
            m.checkNotNullParameter(list2, "applications");
            m.checkNotNullParameter(discoverCommands, "discoveryCommands");
            m.checkNotNullParameter(list3, "frecencyCommands");
            HashMap hashMap = new HashMap();
            for (Application application : list2) {
                hashMap.put(Long.valueOf(application.getId()), application);
            }
            TreeSet<Application> treeSet = new TreeSet(ModelApplicationComparator.Companion);
            HashMap hashMap2 = new HashMap();
            for (ApplicationCommand applicationCommand : discoverCommands.getCommands()) {
                long applicationId = applicationCommand.getApplicationId();
                Application application2 = (Application) hashMap.get(Long.valueOf(applicationId));
                if (application2 != null) {
                    m.checkNotNullExpressionValue(application2, "appMap[appId] ?: continue");
                    treeSet.add(application2);
                    ApplicationCommandAutocompletable applicationCommandAutocompletable = new ApplicationCommandAutocompletable(application2, applicationCommand, ApplicationCommandKt.hasPermission(applicationCommand, j, list), false);
                    if (!hashMap2.containsKey(Long.valueOf(applicationId))) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(applicationCommandAutocompletable);
                        hashMap2.put(Long.valueOf(applicationId), arrayList);
                    } else {
                        List list4 = (List) hashMap2.get(Long.valueOf(applicationId));
                        if (list4 != null) {
                            list4.add(applicationCommandAutocompletable);
                        }
                    }
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Application application3 : treeSet) {
                List list5 = (List) hashMap2.get(Long.valueOf(application3.getId()));
                if (list5 != null && (true ^ list5.isEmpty())) {
                    if (z2) {
                        list5.add(0, new ApplicationPlaceholder(application3));
                    }
                    arrayList2.add(new Pair(application3, list5));
                }
            }
            if (i > 0 && (!arrayList2.isEmpty())) {
                if (discoverCommands.getHasMoreBefore()) {
                    Pair pair = (Pair) u.first((List<? extends Object>) arrayList2);
                    Application application4 = (Application) pair.component1();
                    arrayList2.set(0, new Pair(application4, u.plus((Collection) createPlaceholderModels(i, application4), (Iterable) ((List) pair.component2()))));
                }
                if (discoverCommands.getHasMoreAfter()) {
                    Pair pair2 = (Pair) u.last((List<? extends Object>) arrayList2);
                    Application application5 = (Application) pair2.component1();
                    arrayList2.set(arrayList2.size() - 1, new Pair(application5, u.plus((Collection) ((List) pair2.component2()), (Iterable) createPlaceholderModels(i, application5))));
                }
            }
            Application application6 = (Application) hashMap.get(-2L);
            if (!discoverCommands.getHasMoreBefore() && application6 != null && (!list3.isEmpty())) {
                ArrayList arrayList3 = new ArrayList();
                arrayList3.add(new ApplicationPlaceholder(application6));
                for (ApplicationCommand applicationCommand2 : list3) {
                    Application application7 = (Application) hashMap.get(Long.valueOf(applicationCommand2.getApplicationId()));
                    if (application7 != null) {
                        m.checkNotNullExpressionValue(application7, "appMap[command.applicationId] ?: return@forEach");
                        arrayList3.add(new ApplicationCommandAutocompletable(application7, applicationCommand2, ApplicationCommandKt.hasPermission(applicationCommand2, j, list), true));
                    }
                }
                arrayList2.add(0, new Pair(application6, arrayList3));
            }
            return new WidgetChatInputDiscoveryCommandsModel(arrayList2, discoverCommands.getHasMoreBefore(), discoverCommands.getHasMoreAfter(), discoverCommands.getJumpedSequenceId(), discoverCommands.getJumpedApplicationId(), discoverCommands.getLoadState());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public WidgetChatInputDiscoveryCommandsModel(List<? extends Pair<Application, ? extends List<? extends Autocompletable>>> list, boolean z2, boolean z3, int i, Long l, LoadState loadState) {
        m.checkNotNullParameter(list, "commandsByApplication");
        m.checkNotNullParameter(loadState, "loadState");
        this.commandsByApplication = list;
        this.hasMoreBefore = z2;
        this.hasMoreAfter = z3;
        this.jumpedSequenceId = i;
        this.jumpedApplicationId = l;
        this.loadState = loadState;
    }

    public static /* synthetic */ WidgetChatInputDiscoveryCommandsModel copy$default(WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel, List list, boolean z2, boolean z3, int i, Long l, LoadState loadState, int i2, Object obj) {
        List<Pair<Application, List<Autocompletable>>> list2 = list;
        if ((i2 & 1) != 0) {
            list2 = widgetChatInputDiscoveryCommandsModel.commandsByApplication;
        }
        if ((i2 & 2) != 0) {
            z2 = widgetChatInputDiscoveryCommandsModel.hasMoreBefore;
        }
        boolean z4 = z2;
        if ((i2 & 4) != 0) {
            z3 = widgetChatInputDiscoveryCommandsModel.hasMoreAfter;
        }
        boolean z5 = z3;
        if ((i2 & 8) != 0) {
            i = widgetChatInputDiscoveryCommandsModel.jumpedSequenceId;
        }
        int i3 = i;
        if ((i2 & 16) != 0) {
            l = widgetChatInputDiscoveryCommandsModel.jumpedApplicationId;
        }
        Long l2 = l;
        if ((i2 & 32) != 0) {
            loadState = widgetChatInputDiscoveryCommandsModel.loadState;
        }
        return widgetChatInputDiscoveryCommandsModel.copy(list2, z4, z5, i3, l2, loadState);
    }

    public static final WidgetChatInputDiscoveryCommandsModel parseModelDiscoveryCommands(long j, List<Long> list, List<Application> list2, DiscoverCommands discoverCommands, boolean z2, int i, List<? extends ApplicationCommand> list3) {
        return Companion.parseModelDiscoveryCommands(j, list, list2, discoverCommands, z2, i, list3);
    }

    public final List<Pair<Application, List<Autocompletable>>> component1() {
        return this.commandsByApplication;
    }

    public final boolean component2() {
        return this.hasMoreBefore;
    }

    public final boolean component3() {
        return this.hasMoreAfter;
    }

    public final int component4() {
        return this.jumpedSequenceId;
    }

    public final Long component5() {
        return this.jumpedApplicationId;
    }

    public final LoadState component6() {
        return this.loadState;
    }

    public final WidgetChatInputDiscoveryCommandsModel copy(List<? extends Pair<Application, ? extends List<? extends Autocompletable>>> list, boolean z2, boolean z3, int i, Long l, LoadState loadState) {
        m.checkNotNullParameter(list, "commandsByApplication");
        m.checkNotNullParameter(loadState, "loadState");
        return new WidgetChatInputDiscoveryCommandsModel(list, z2, z3, i, l, loadState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetChatInputDiscoveryCommandsModel)) {
            return false;
        }
        WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel = (WidgetChatInputDiscoveryCommandsModel) obj;
        return m.areEqual(this.commandsByApplication, widgetChatInputDiscoveryCommandsModel.commandsByApplication) && this.hasMoreBefore == widgetChatInputDiscoveryCommandsModel.hasMoreBefore && this.hasMoreAfter == widgetChatInputDiscoveryCommandsModel.hasMoreAfter && this.jumpedSequenceId == widgetChatInputDiscoveryCommandsModel.jumpedSequenceId && m.areEqual(this.jumpedApplicationId, widgetChatInputDiscoveryCommandsModel.jumpedApplicationId) && m.areEqual(this.loadState, widgetChatInputDiscoveryCommandsModel.loadState);
    }

    public final List<Pair<Application, List<Autocompletable>>> getCommandsByApplication() {
        return this.commandsByApplication;
    }

    public final List<Autocompletable> getFlattenCommandsModel() {
        ArrayList arrayList = new ArrayList();
        for (Pair<Application, List<Autocompletable>> pair : this.commandsByApplication) {
            arrayList.addAll(pair.component2());
        }
        return arrayList;
    }

    public final boolean getHasMoreAfter() {
        return this.hasMoreAfter;
    }

    public final boolean getHasMoreBefore() {
        return this.hasMoreBefore;
    }

    public final Long getJumpedApplicationId() {
        return this.jumpedApplicationId;
    }

    public final int getJumpedSequenceId() {
        return this.jumpedSequenceId;
    }

    public final LoadState getLoadState() {
        return this.loadState;
    }

    public int hashCode() {
        List<Pair<Application, List<Autocompletable>>> list = this.commandsByApplication;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        boolean z2 = this.hasMoreBefore;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode + i3) * 31;
        boolean z3 = this.hasMoreAfter;
        if (!z3) {
            i2 = z3 ? 1 : 0;
        }
        int i6 = (((i5 + i2) * 31) + this.jumpedSequenceId) * 31;
        Long l = this.jumpedApplicationId;
        int hashCode2 = (i6 + (l != null ? l.hashCode() : 0)) * 31;
        LoadState loadState = this.loadState;
        if (loadState != null) {
            i = loadState.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetChatInputDiscoveryCommandsModel(commandsByApplication=");
        R.append(this.commandsByApplication);
        R.append(", hasMoreBefore=");
        R.append(this.hasMoreBefore);
        R.append(", hasMoreAfter=");
        R.append(this.hasMoreAfter);
        R.append(", jumpedSequenceId=");
        R.append(this.jumpedSequenceId);
        R.append(", jumpedApplicationId=");
        R.append(this.jumpedApplicationId);
        R.append(", loadState=");
        R.append(this.loadState);
        R.append(")");
        return R.toString();
    }
}
