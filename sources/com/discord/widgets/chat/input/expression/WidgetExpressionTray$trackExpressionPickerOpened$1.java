package com.discord.widgets.chat.input.expression;

import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.icon.IconUtils;
import com.discord.widgets.chat.input.expression.ExpressionTrayViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetExpressionTray.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;", "kotlin.jvm.PlatformType", "viewState", "", "invoke", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetExpressionTray$trackExpressionPickerOpened$1 extends o implements Function1<ExpressionTrayViewModel.ViewState, Unit> {
    public static final WidgetExpressionTray$trackExpressionPickerOpened$1 INSTANCE = new WidgetExpressionTray$trackExpressionPickerOpened$1();

    public WidgetExpressionTray$trackExpressionPickerOpened$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ExpressionTrayViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ExpressionTrayViewModel.ViewState viewState) {
        String str;
        int ordinal = viewState.getSelectedExpressionTab().ordinal();
        if (ordinal == 0) {
            str = "emoji";
        } else if (ordinal == 1) {
            str = IconUtils.ANIMATED_IMAGE_EXTENSION;
        } else if (ordinal == 2) {
            str = "sticker";
        } else {
            throw new NoWhenBranchMatchedException();
        }
        AnalyticsTracker.INSTANCE.expressionPickerOpened(str, false);
    }
}
