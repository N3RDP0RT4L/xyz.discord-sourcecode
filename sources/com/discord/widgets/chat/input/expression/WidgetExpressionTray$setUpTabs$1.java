package com.discord.widgets.chat.input.expression;

import android.widget.TextView;
import com.discord.databinding.WidgetExpressionTrayBinding;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetExpressionTray.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "index", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetExpressionTray$setUpTabs$1 extends o implements Function1<Integer, Unit> {
    public final /* synthetic */ WidgetExpressionTray this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetExpressionTray$setUpTabs$1(WidgetExpressionTray widgetExpressionTray) {
        super(1);
        this.this$0 = widgetExpressionTray;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke(num.intValue());
        return Unit.a;
    }

    public final void invoke(int i) {
        WidgetExpressionTrayBinding binding;
        boolean z2;
        ExpressionTrayViewModel expressionTrayViewModel;
        ExpressionTrayTab expressionTrayTab = ExpressionTrayTab.values()[i];
        binding = this.this$0.getBinding();
        TextView textView = binding.j;
        m.checkNotNullExpressionValue(textView, "binding.expressionTraySearchButton");
        textView.setText("");
        z2 = this.this$0.stickerPickerInitialized;
        if (z2 && WidgetExpressionTray.access$getStickerPickerFragment$p(this.this$0).isVisible()) {
            WidgetExpressionTray.access$getStickerPickerFragment$p(this.this$0).clearSearchInput();
        }
        expressionTrayViewModel = this.this$0.getExpressionTrayViewModel();
        expressionTrayViewModel.selectTab(expressionTrayTab);
        this.this$0.trackExpressionPickerTabClicked(expressionTrayTab);
    }
}
