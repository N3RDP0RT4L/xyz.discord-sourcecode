package com.discord.widgets.chat.input.expression;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.e0;
import b.b.a.c;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetExpressionTrayBinding;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.ChatInputComponentTypes;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.rounded.RoundedRelativeLayout;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.ContentResizingCoordinatorLayout;
import com.discord.views.segmentedcontrol.CardSegment;
import com.discord.views.segmentedcontrol.SegmentedControlContainer;
import com.discord.widgets.chat.input.AppFlexInputViewModel;
import com.discord.widgets.chat.input.OnBackspacePressedListener;
import com.discord.widgets.chat.input.emoji.EmojiPickerContextType;
import com.discord.widgets.chat.input.emoji.EmojiPickerListener;
import com.discord.widgets.chat.input.emoji.EmojiPickerMode;
import com.discord.widgets.chat.input.emoji.EmojiPickerNavigator;
import com.discord.widgets.chat.input.emoji.WidgetEmojiPicker;
import com.discord.widgets.chat.input.expression.ExpressionDetailPage;
import com.discord.widgets.chat.input.expression.ExpressionTrayViewModel;
import com.discord.widgets.chat.input.gifpicker.WidgetGifCategory;
import com.discord.widgets.chat.input.gifpicker.WidgetGifPicker;
import com.discord.widgets.chat.input.gifpicker.WidgetGifPickerSheet;
import com.discord.widgets.chat.input.sticker.StickerPickerListener;
import com.discord.widgets.chat.input.sticker.StickerPickerMode;
import com.discord.widgets.chat.input.sticker.WidgetStickerPicker;
import com.discord.widgets.chat.input.sticker.WidgetStickerPickerSheet;
import com.google.android.material.appbar.AppBarLayout;
import d0.o;
import d0.t.h0;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import j0.l.e.k;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetExpressionTray.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ª\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 n2\u00020\u00012\u00020\u0002:\u0001nB\u0007¢\u0006\u0004\bm\u0010\u0005J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u000e\u0010\tJ\u0017\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u000f\u0010\tJ\u0017\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0015\u0010\u0005J\u000f\u0010\u0016\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0016\u0010\u0005J\u000f\u0010\u0017\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0017\u0010\u0005J\u000f\u0010\u0018\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0018\u0010\u0005J\u000f\u0010\u0019\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0019\u0010\u0005J'\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u001cH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010 \u001a\u00020\u0003H\u0002¢\u0006\u0004\b \u0010\u0005J\u000f\u0010!\u001a\u00020\u0003H\u0002¢\u0006\u0004\b!\u0010\u0005J\u000f\u0010\"\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\"\u0010\u0005J\u0017\u0010%\u001a\u00020\u00032\u0006\u0010$\u001a\u00020#H\u0002¢\u0006\u0004\b%\u0010&J\u000f\u0010'\u001a\u00020\u0003H\u0002¢\u0006\u0004\b'\u0010\u0005J\u0017\u0010)\u001a\u00020\u00032\u0006\u0010(\u001a\u00020\u0010H\u0002¢\u0006\u0004\b)\u0010*J\u0017\u0010-\u001a\u00020\u00032\u0006\u0010,\u001a\u00020+H\u0016¢\u0006\u0004\b-\u0010.J\u000f\u0010/\u001a\u00020\u0003H\u0016¢\u0006\u0004\b/\u0010\u0005J\u0017\u00101\u001a\u00020\u00032\u0006\u00100\u001a\u00020#H\u0016¢\u0006\u0004\b1\u0010&J\u001b\u00104\u001a\u00020\u00032\f\u00103\u001a\b\u0012\u0004\u0012\u00020\u000302¢\u0006\u0004\b4\u00105J\u0015\u00108\u001a\u00020\u00032\u0006\u00107\u001a\u000206¢\u0006\u0004\b8\u00109J\u0015\u0010<\u001a\u00020\u00032\u0006\u0010;\u001a\u00020:¢\u0006\u0004\b<\u0010=J\u0017\u0010@\u001a\u00020\u00032\b\u0010?\u001a\u0004\u0018\u00010>¢\u0006\u0004\b@\u0010AR\u0018\u0010;\u001a\u0004\u0018\u00010:8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b;\u0010BR\u001d\u0010H\u001a\u00020C8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bD\u0010E\u001a\u0004\bF\u0010GR\u0016\u0010I\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bI\u0010JR\u0016\u0010L\u001a\u00020K8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bL\u0010MR\"\u0010O\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020+0N8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bO\u0010PR\u0016\u0010R\u001a\u00020Q8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bR\u0010SR\u0016\u0010T\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bT\u0010JR\u001d\u0010Z\u001a\u00020U8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bV\u0010W\u001a\u0004\bX\u0010YR\u001d\u0010_\u001a\u00020[8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\\\u0010E\u001a\u0004\b]\u0010^R\u0018\u0010?\u001a\u0004\u0018\u00010>8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b?\u0010`R\u001e\u0010a\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u0001028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\ba\u0010bR\u0018\u00107\u001a\u0004\u0018\u0001068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u0010cR:\u0010f\u001a&\u0012\f\u0012\n e*\u0004\u0018\u00010#0# e*\u0012\u0012\f\u0012\n e*\u0004\u0018\u00010#0#\u0018\u00010d0d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010gR\u0016\u0010i\u001a\u00020h8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bi\u0010jR\u0016\u0010k\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bk\u0010JR\u0016\u0010l\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bl\u0010J¨\u0006o"}, d2 = {"Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray;", "Lb/b/a/c;", "Lcom/discord/app/AppFragment;", "", "fetchDataForTrayOpen", "()V", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;", "viewState", "handleViewState", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;)V", "configureLandingPage", "configureDetailPage", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "tab", "", "getTabIndexFromTabType", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)I", "setUpTabs", "initializeExpressionTabToViewsMap", "setUpEmojiPicker", "setUpGifPicker", "setUpStickerPicker", "fragmentId", "fragment", "", "fragmentName", "setupTabFragment", "(ILcom/discord/app/AppFragment;Ljava/lang/String;)V", "setWindowInsetsListeners", "onGifSelected", "onGifSearchSheetCanceled", "", "show", "showStickersSearchBar", "(Z)V", "trackExpressionPickerOpened", "clickedTab", "trackExpressionPickerTabClicked", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "isActive", "isShown", "Lkotlin/Function0;", "callback", "setOnEmojiSearchOpenedListener", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;", "emojiPickerListener", "setEmojiPickerListener", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V", "Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;", "stickerPickerListener", "setStickerPickerListener", "(Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;)V", "Lcom/discord/widgets/chat/input/OnBackspacePressedListener;", "onBackspacePressedListener", "setOnBackspacePressedListener", "(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V", "Lcom/discord/widgets/chat/input/sticker/StickerPickerListener;", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel$delegate", "Lkotlin/Lazy;", "getFlexInputViewModel", "()Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel", "stickerPickerInitialized", "Z", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;", "stickerPickerFragment", "Lcom/discord/widgets/chat/input/sticker/WidgetStickerPicker;", "", "expressionTabToContentViewsMap", "Ljava/util/Map;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;", "emojiPickerFragment", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;", "emojiPickerInitialized", "Lcom/discord/databinding/WidgetExpressionTrayBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetExpressionTrayBinding;", "binding", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;", "expressionTrayViewModel$delegate", "getExpressionTrayViewModel", "()Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;", "expressionTrayViewModel", "Lcom/discord/widgets/chat/input/OnBackspacePressedListener;", "onEmojiSearchOpenedListener", "Lkotlin/jvm/functions/Function0;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "isAtInitialScrollPositionSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;", "gifPickerFragment", "Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPicker;", "gifPickerInitialized", "wasActive", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetExpressionTray extends AppFragment implements c {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetExpressionTray.class, "binding", "getBinding()Lcom/discord/databinding/WidgetExpressionTrayBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final BehaviorSubject<Boolean> isExpressionTrayActiveSubject;
    private WidgetEmojiPicker emojiPickerFragment;
    private boolean emojiPickerInitialized;
    private EmojiPickerListener emojiPickerListener;
    private Map<ExpressionTrayTab, ? extends View> expressionTabToContentViewsMap;
    private WidgetGifPicker gifPickerFragment;
    private boolean gifPickerInitialized;
    private OnBackspacePressedListener onBackspacePressedListener;
    private Function0<Unit> onEmojiSearchOpenedListener;
    private WidgetStickerPicker stickerPickerFragment;
    private boolean stickerPickerInitialized;
    private StickerPickerListener stickerPickerListener;
    private boolean wasActive;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetExpressionTray$binding$2.INSTANCE, null, 2, null);
    private final Lazy expressionTrayViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(ExpressionTrayViewModel.class), new WidgetExpressionTray$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetExpressionTray$expressionTrayViewModel$2.INSTANCE));
    private final Lazy flexInputViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(AppFlexInputViewModel.class), new WidgetExpressionTray$appActivityViewModels$$inlined$activityViewModels$3(this), new e0(new WidgetExpressionTray$flexInputViewModel$2(this)));
    private final BehaviorSubject<Boolean> isAtInitialScrollPositionSubject = BehaviorSubject.l0(Boolean.TRUE);

    /* compiled from: WidgetExpressionTray.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0004\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/expression/WidgetExpressionTray$Companion;", "", "Lrx/subjects/BehaviorSubject;", "", "isExpressionTrayActiveSubject", "Lrx/subjects/BehaviorSubject;", "()Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final BehaviorSubject<Boolean> isExpressionTrayActiveSubject() {
            return WidgetExpressionTray.isExpressionTrayActiveSubject;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;
        public static final /* synthetic */ int[] $EnumSwitchMapping$3;

        static {
            ExpressionTrayTab.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            ExpressionTrayTab expressionTrayTab = ExpressionTrayTab.EMOJI;
            iArr[expressionTrayTab.ordinal()] = 1;
            ExpressionTrayTab expressionTrayTab2 = ExpressionTrayTab.GIF;
            iArr[expressionTrayTab2.ordinal()] = 2;
            ExpressionTrayTab expressionTrayTab3 = ExpressionTrayTab.STICKER;
            iArr[expressionTrayTab3.ordinal()] = 3;
            ExpressionTrayTab.values();
            int[] iArr2 = new int[3];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[expressionTrayTab.ordinal()] = 1;
            iArr2[expressionTrayTab2.ordinal()] = 2;
            iArr2[expressionTrayTab3.ordinal()] = 3;
            ExpressionTrayTab.values();
            int[] iArr3 = new int[3];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[expressionTrayTab.ordinal()] = 1;
            iArr3[expressionTrayTab2.ordinal()] = 2;
            iArr3[expressionTrayTab3.ordinal()] = 3;
            ExpressionTrayTab.values();
            int[] iArr4 = new int[3];
            $EnumSwitchMapping$3 = iArr4;
            iArr4[expressionTrayTab.ordinal()] = 1;
            iArr4[expressionTrayTab2.ordinal()] = 2;
            iArr4[expressionTrayTab3.ordinal()] = 3;
        }
    }

    static {
        BehaviorSubject<Boolean> l0 = BehaviorSubject.l0(Boolean.FALSE);
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(false)");
        isExpressionTrayActiveSubject = l0;
    }

    public WidgetExpressionTray() {
        super(R.layout.widget_expression_tray);
    }

    public static final /* synthetic */ WidgetStickerPicker access$getStickerPickerFragment$p(WidgetExpressionTray widgetExpressionTray) {
        WidgetStickerPicker widgetStickerPicker = widgetExpressionTray.stickerPickerFragment;
        if (widgetStickerPicker == null) {
            m.throwUninitializedPropertyAccessException("stickerPickerFragment");
        }
        return widgetStickerPicker;
    }

    private final void configureDetailPage(ExpressionTrayViewModel.ViewState viewState) {
        Fragment findFragmentById;
        ExpressionDetailPage expressionDetailPage = viewState.getExpressionDetailPage();
        if (expressionDetailPage instanceof ExpressionDetailPage.GifCategoryPage) {
            WidgetGifCategory widgetGifCategory = new WidgetGifCategory();
            Bundle bundle = new Bundle();
            bundle.putSerializable(WidgetGifCategory.ARG_GIF_CATEGORY_ITEM, ((ExpressionDetailPage.GifCategoryPage) expressionDetailPage).getGifCategoryItem());
            widgetGifCategory.setArguments(bundle);
            widgetGifCategory.setOnGifSelected(new WidgetExpressionTray$configureDetailPage$gifCategoryFragment$1$2(this));
            String simpleName = WidgetGifCategory.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName, "gifCategoryFragment.javaClass.simpleName");
            setupTabFragment(R.id.expression_tray_detail_page, widgetGifCategory, simpleName);
        } else if (expressionDetailPage == null && (findFragmentById = getChildFragmentManager().findFragmentById(R.id.expression_tray_detail_page)) != null) {
            getChildFragmentManager().beginTransaction().remove(findFragmentById).commit();
        }
    }

    private final void configureLandingPage(ExpressionTrayViewModel.ViewState viewState) {
        int i;
        ExpressionTrayTab selectedExpressionTab = viewState.getSelectedExpressionTab();
        int ordinal = selectedExpressionTab.ordinal();
        if (ordinal == 0) {
            setUpEmojiPicker();
        } else if (ordinal == 1) {
            setUpGifPicker();
        } else if (ordinal == 2) {
            setUpStickerPicker();
        }
        getBinding().k.setSelectedIndex(getTabIndexFromTabType(selectedExpressionTab));
        Map<ExpressionTrayTab, ? extends View> map = this.expressionTabToContentViewsMap;
        if (map == null) {
            m.throwUninitializedPropertyAccessException("expressionTabToContentViewsMap");
        }
        for (Map.Entry<ExpressionTrayTab, ? extends View> entry : map.entrySet()) {
            ExpressionTrayTab key = entry.getKey();
            View value = entry.getValue();
            int i2 = 0;
            if (!(key == selectedExpressionTab)) {
                i2 = 8;
            }
            value.setVisibility(i2);
        }
        int ordinal2 = selectedExpressionTab.ordinal();
        if (ordinal2 == 0) {
            i = R.string.search_for_emoji;
        } else if (ordinal2 == 1) {
            i = R.string.search_tenor;
        } else if (ordinal2 == 2) {
            i = R.string.search_for_stickers;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        String string = getResources().getString(i);
        m.checkNotNullExpressionValue(string, "resources.getString(searchTextStringRes)");
        TextView textView = getBinding().j;
        m.checkNotNullExpressionValue(textView, "binding.expressionTraySearchButton");
        textView.setHint(string);
        RoundedRelativeLayout roundedRelativeLayout = getBinding().i;
        m.checkNotNullExpressionValue(roundedRelativeLayout, "binding.expressionTraySearchBar");
        roundedRelativeLayout.setContentDescription(string);
    }

    private final void fetchDataForTrayOpen() {
        StoreStream.Companion.getStickers().fetchEnabledStickerDirectory();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetExpressionTrayBinding getBinding() {
        return (WidgetExpressionTrayBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ExpressionTrayViewModel getExpressionTrayViewModel() {
        return (ExpressionTrayViewModel) this.expressionTrayViewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final AppFlexInputViewModel getFlexInputViewModel() {
        return (AppFlexInputViewModel) this.flexInputViewModel$delegate.getValue();
    }

    private final int getTabIndexFromTabType(ExpressionTrayTab expressionTrayTab) {
        Map<ExpressionTrayTab, ? extends View> map = this.expressionTabToContentViewsMap;
        if (map == null) {
            m.throwUninitializedPropertyAccessException("expressionTabToContentViewsMap");
        }
        return u.indexOf(map.keySet(), expressionTrayTab);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(ExpressionTrayViewModel.Event event) {
        if (m.areEqual(event, ExpressionTrayViewModel.Event.HideExpressionTray.INSTANCE)) {
            getFlexInputViewModel().hideExpressionTray();
        } else if (m.areEqual(event, ExpressionTrayViewModel.Event.ShowEmojiPickerSheet.INSTANCE)) {
            Function0<Unit> function0 = this.onEmojiSearchOpenedListener;
            if (function0 != null) {
                function0.invoke();
            }
            AnalyticsTracker.INSTANCE.chatInputComponentViewed(ChatInputComponentTypes.EMOJI_SEARCH);
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            EmojiPickerNavigator.launchBottomSheet$default(parentFragmentManager, this.emojiPickerListener, EmojiPickerContextType.Chat.INSTANCE, null, 8, null);
        } else if (m.areEqual(event, ExpressionTrayViewModel.Event.ShowGifPickerSheet.INSTANCE)) {
            AnalyticsTracker.INSTANCE.chatInputComponentViewed(ChatInputComponentTypes.GIF_SEARCH);
            WidgetGifPickerSheet.Companion companion = WidgetGifPickerSheet.Companion;
            FragmentManager parentFragmentManager2 = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
            companion.show(parentFragmentManager2, new WidgetExpressionTray$handleEvent$1(this), new WidgetExpressionTray$handleEvent$2(this));
        } else if (event instanceof ExpressionTrayViewModel.Event.ShowStickerPicker) {
            ExpressionTrayViewModel.Event.ShowStickerPicker showStickerPicker = (ExpressionTrayViewModel.Event.ShowStickerPicker) event;
            if (!showStickerPicker.getInline()) {
                AnalyticsTracker.INSTANCE.chatInputComponentViewed(ChatInputComponentTypes.STICKER_SEARCH);
                WidgetStickerPickerSheet.Companion companion2 = WidgetStickerPickerSheet.Companion;
                FragmentManager parentFragmentManager3 = getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager3, "parentFragmentManager");
                WidgetStickerPickerSheet show = companion2.show(parentFragmentManager3, this.stickerPickerListener, showStickerPicker.getStickerPackId(), showStickerPicker.getSearchText(), new WidgetExpressionTray$handleEvent$pickerSheet$1(this));
                Observable<T> p = new k(Unit.a).p(500L, TimeUnit.MILLISECONDS);
                m.checkNotNullExpressionValue(p, "Observable.just(Unit)\n  …0, TimeUnit.MILLISECONDS)");
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(p, this, null, 2, null), WidgetExpressionTray.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetExpressionTray$handleEvent$3(show, event));
                return;
            }
            AnalyticsTracker.INSTANCE.chatInputComponentViewed("sticker");
            setUpStickerPicker();
            getFlexInputViewModel().showExpressionTray();
            TextView textView = getBinding().j;
            m.checkNotNullExpressionValue(textView, "binding.expressionTraySearchButton");
            textView.setText(showStickerPicker.getSearchText());
            hideKeyboard(getView());
            WidgetStickerPicker widgetStickerPicker = this.stickerPickerFragment;
            if (widgetStickerPicker == null) {
                m.throwUninitializedPropertyAccessException("stickerPickerFragment");
            }
            String searchText = showStickerPicker.getSearchText();
            if (searchText == null) {
                searchText = "";
            }
            Long stickerPackId = showStickerPicker.getStickerPackId();
            widgetStickerPicker.setupForInlineSearchAndScroll(searchText, stickerPackId != null ? stickerPackId.longValue() : -1L);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleViewState(ExpressionTrayViewModel.ViewState viewState) {
        configureLandingPage(viewState);
        configureDetailPage(viewState);
        ContentResizingCoordinatorLayout contentResizingCoordinatorLayout = getBinding().h;
        m.checkNotNullExpressionValue(contentResizingCoordinatorLayout, "binding.expressionTrayLandingPage");
        int i = 8;
        contentResizingCoordinatorLayout.setVisibility(viewState.getShowLandingPage() ? 0 : 8);
        FragmentContainerView fragmentContainerView = getBinding().d;
        m.checkNotNullExpressionValue(fragmentContainerView, "binding.expressionTrayDetailPage");
        boolean z2 = true;
        fragmentContainerView.setVisibility(viewState.getShowLandingPage() ^ true ? 0 : 8);
        RoundedRelativeLayout roundedRelativeLayout = getBinding().i;
        m.checkNotNullExpressionValue(roundedRelativeLayout, "binding.expressionTraySearchBar");
        roundedRelativeLayout.setVisibility(viewState.getShowSearchBar() ? 0 : 8);
        boolean showGifsAndStickers = viewState.getShowGifsAndStickers();
        boolean showGifsAndStickers2 = viewState.getShowGifsAndStickers();
        CardSegment cardSegment = getBinding().l;
        m.checkNotNullExpressionValue(cardSegment, "binding.expressionTrayStickerCard");
        if ((cardSegment.getVisibility() == 0) && !showGifsAndStickers && viewState.getSelectedExpressionTab() == ExpressionTrayTab.STICKER) {
            getExpressionTrayViewModel().selectTab(ExpressionTrayTab.EMOJI);
        }
        CardSegment cardSegment2 = getBinding().f;
        m.checkNotNullExpressionValue(cardSegment2, "binding.expressionTrayGifCard");
        if ((cardSegment2.getVisibility() == 0) && !showGifsAndStickers2 && viewState.getSelectedExpressionTab() == ExpressionTrayTab.GIF) {
            getExpressionTrayViewModel().selectTab(ExpressionTrayTab.EMOJI);
        }
        CardSegment cardSegment3 = getBinding().l;
        m.checkNotNullExpressionValue(cardSegment3, "binding.expressionTrayStickerCard");
        cardSegment3.setVisibility(showGifsAndStickers ? 0 : 8);
        CardSegment cardSegment4 = getBinding().f;
        m.checkNotNullExpressionValue(cardSegment4, "binding.expressionTrayGifCard");
        cardSegment4.setVisibility(showGifsAndStickers2 ? 0 : 8);
        SegmentedControlContainer segmentedControlContainer = getBinding().k;
        m.checkNotNullExpressionValue(segmentedControlContainer, "binding.expressionTraySegmentedControl");
        if (!showGifsAndStickers2 && !showGifsAndStickers) {
            z2 = false;
        }
        if (z2) {
            i = 0;
        }
        segmentedControlContainer.setVisibility(i);
    }

    private final void initializeExpressionTabToViewsMap() {
        this.expressionTabToContentViewsMap = h0.linkedMapOf(o.to(ExpressionTrayTab.EMOJI, getBinding().e), o.to(ExpressionTrayTab.GIF, getBinding().g), o.to(ExpressionTrayTab.STICKER, getBinding().m));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onGifSearchSheetCanceled() {
        if (isAdded()) {
            getFlexInputViewModel().showKeyboardAndHideExpressionTray();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onGifSelected() {
        if (isAdded()) {
            getFlexInputViewModel().showKeyboardAndHideExpressionTray();
        }
    }

    private final void setUpEmojiPicker() {
        if (!this.emojiPickerInitialized) {
            this.emojiPickerInitialized = true;
            WidgetEmojiPicker widgetEmojiPicker = new WidgetEmojiPicker();
            widgetEmojiPicker.setListener(this.emojiPickerListener);
            widgetEmojiPicker.setOnBackspacePressedListener(this.onBackspacePressedListener);
            Bundle bundle = new Bundle();
            bundle.putSerializable("MODE", EmojiPickerMode.INLINE);
            bundle.putSerializable(EmojiPickerNavigator.ARG_EMOJI_PICKER_CONTEXT_TYPE, EmojiPickerContextType.Chat.INSTANCE);
            widgetEmojiPicker.setArguments(bundle);
            String simpleName = WidgetEmojiPicker.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName, "emojiPickerFragment.javaClass.simpleName");
            setupTabFragment(R.id.expression_tray_emoji_picker_content, widgetEmojiPicker, simpleName);
            this.emojiPickerFragment = widgetEmojiPicker;
        }
    }

    private final void setUpGifPicker() {
        if (!this.gifPickerInitialized) {
            this.gifPickerInitialized = true;
            WidgetGifPicker widgetGifPicker = new WidgetGifPicker();
            widgetGifPicker.setOnSelectGifCategory(new WidgetExpressionTray$setUpGifPicker$1(getExpressionTrayViewModel()));
            String simpleName = WidgetGifPicker.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName, "gifPickerFragment.javaClass.simpleName");
            setupTabFragment(R.id.expression_tray_gif_picker_content, widgetGifPicker, simpleName);
            this.gifPickerFragment = widgetGifPicker;
        }
    }

    private final void setUpStickerPicker() {
        if (!this.stickerPickerInitialized) {
            this.stickerPickerInitialized = true;
            WidgetStickerPicker widgetStickerPicker = new WidgetStickerPicker();
            widgetStickerPicker.setListener(this.stickerPickerListener);
            widgetStickerPicker.setOnBackspacePressedListener(this.onBackspacePressedListener);
            widgetStickerPicker.setShowSearchBar(new WidgetExpressionTray$setUpStickerPicker$1(this));
            widgetStickerPicker.setScrollExpressionPickerToTop(new WidgetExpressionTray$setUpStickerPicker$2(this));
            Bundle bundle = new Bundle();
            bundle.putSerializable("MODE", StickerPickerMode.INLINE);
            widgetStickerPicker.setArguments(bundle);
            String simpleName = WidgetStickerPicker.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName, "stickerPickerFragment.javaClass.simpleName");
            setupTabFragment(R.id.expression_tray_sticker_picker_content, widgetStickerPicker, simpleName);
            this.stickerPickerFragment = widgetStickerPicker;
        }
    }

    private final void setUpTabs() {
        getBinding().k.a(getTabIndexFromTabType(ExpressionTrayTab.EMOJI));
        getBinding().k.setOnSegmentSelectedChangeListener(new WidgetExpressionTray$setUpTabs$1(this));
    }

    private final void setWindowInsetsListeners() {
        FragmentContainerView fragmentContainerView = getBinding().e;
        m.checkNotNullExpressionValue(fragmentContainerView, "binding.expressionTrayEmojiPickerContent");
        ViewExtensions.setForwardingWindowInsetsListener(fragmentContainerView);
        FragmentContainerView fragmentContainerView2 = getBinding().g;
        m.checkNotNullExpressionValue(fragmentContainerView2, "binding.expressionTrayGifPickerContent");
        ViewExtensions.setForwardingWindowInsetsListener(fragmentContainerView2);
        FragmentContainerView fragmentContainerView3 = getBinding().m;
        m.checkNotNullExpressionValue(fragmentContainerView3, "binding.expressionTrayStickerPickerContent");
        ViewExtensions.setForwardingWindowInsetsListener(fragmentContainerView3);
        FragmentContainerView fragmentContainerView4 = getBinding().d;
        m.checkNotNullExpressionValue(fragmentContainerView4, "binding.expressionTrayDetailPage");
        ViewExtensions.setForwardingWindowInsetsListener(fragmentContainerView4);
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().c, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.chat.input.expression.WidgetExpressionTray$setWindowInsetsListeners$1
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetExpressionTrayBinding binding;
                WidgetExpressionTrayBinding binding2;
                WidgetExpressionTrayBinding binding3;
                binding = WidgetExpressionTray.this.getBinding();
                ViewCompat.dispatchApplyWindowInsets(binding.e, windowInsetsCompat);
                binding2 = WidgetExpressionTray.this.getBinding();
                ViewCompat.dispatchApplyWindowInsets(binding2.g, windowInsetsCompat);
                binding3 = WidgetExpressionTray.this.getBinding();
                return ViewCompat.dispatchApplyWindowInsets(binding3.m, windowInsetsCompat);
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().h, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.chat.input.expression.WidgetExpressionTray$setWindowInsetsListeners$2
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetExpressionTrayBinding binding;
                binding = WidgetExpressionTray.this.getBinding();
                return ViewCompat.dispatchApplyWindowInsets(binding.c, windowInsetsCompat);
            }
        });
        FrameLayout frameLayout = getBinding().f2364b;
        m.checkNotNullExpressionValue(frameLayout, "binding.expressionTrayContainer");
        ViewExtensions.setForwardingWindowInsetsListener(frameLayout);
    }

    private final void setupTabFragment(int i, AppFragment appFragment, String str) {
        if (!isStateSaved()) {
            getChildFragmentManager().beginTransaction().replace(i, appFragment, str).commit();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showStickersSearchBar(boolean z2) {
        getExpressionTrayViewModel().showStickersSearchBar(z2);
    }

    private final void trackExpressionPickerOpened() {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(getExpressionTrayViewModel().observeViewState(), 0L, false, 3, null), WidgetExpressionTray.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetExpressionTray$trackExpressionPickerOpened$1.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void trackExpressionPickerTabClicked(ExpressionTrayTab expressionTrayTab) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(getExpressionTrayViewModel().observeViewState(), 0L, false, 3, null), WidgetExpressionTray.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetExpressionTray$trackExpressionPickerTabClicked$1(expressionTrayTab));
    }

    @Override // b.b.a.c
    public void isShown(boolean z2) {
        getExpressionTrayViewModel().handleIsActive(z2);
        isExpressionTrayActiveSubject.onNext(Boolean.valueOf(z2));
        if (z2 && !this.wasActive) {
            getBinding().n.setExpanded(true);
            if (this.emojiPickerInitialized) {
                WidgetEmojiPicker widgetEmojiPicker = this.emojiPickerFragment;
                if (widgetEmojiPicker == null) {
                    m.throwUninitializedPropertyAccessException("emojiPickerFragment");
                }
                widgetEmojiPicker.scrollToTop();
            }
            if (this.gifPickerInitialized) {
                WidgetGifPicker widgetGifPicker = this.gifPickerFragment;
                if (widgetGifPicker == null) {
                    m.throwUninitializedPropertyAccessException("gifPickerFragment");
                }
                widgetGifPicker.scrollToTop();
            }
            if (this.stickerPickerInitialized) {
                WidgetStickerPicker widgetStickerPicker = this.stickerPickerFragment;
                if (widgetStickerPicker == null) {
                    m.throwUninitializedPropertyAccessException("stickerPickerFragment");
                }
                widgetStickerPicker.scrollToTop();
            }
            fetchDataForTrayOpen();
            trackExpressionPickerOpened();
        } else if (!z2 && this.wasActive) {
            getExpressionTrayViewModel().clickBack();
        }
        if (!z2) {
            TextView textView = getBinding().j;
            m.checkNotNullExpressionValue(textView, "binding.expressionTraySearchButton");
            textView.setText("");
            if (this.stickerPickerInitialized) {
                WidgetStickerPicker widgetStickerPicker2 = this.stickerPickerFragment;
                if (widgetStickerPicker2 == null) {
                    m.throwUninitializedPropertyAccessException("stickerPickerFragment");
                }
                if (widgetStickerPicker2.isVisible()) {
                    WidgetStickerPicker widgetStickerPicker3 = this.stickerPickerFragment;
                    if (widgetStickerPicker3 == null) {
                        m.throwUninitializedPropertyAccessException("stickerPickerFragment");
                    }
                    widgetStickerPicker3.clearSearchInput();
                }
            }
        }
        if (this.stickerPickerInitialized) {
            WidgetStickerPicker widgetStickerPicker4 = this.stickerPickerFragment;
            if (widgetStickerPicker4 == null) {
                m.throwUninitializedPropertyAccessException("stickerPickerFragment");
            }
            if (widgetStickerPicker4.getCanHandleIsShown()) {
                WidgetStickerPicker widgetStickerPicker5 = this.stickerPickerFragment;
                if (widgetStickerPicker5 == null) {
                    m.throwUninitializedPropertyAccessException("stickerPickerFragment");
                }
                widgetStickerPicker5.isShown(z2);
            }
        }
        this.wasActive = z2;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        initializeExpressionTabToViewsMap();
        setUpTabs();
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.expression.WidgetExpressionTray$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ExpressionTrayViewModel expressionTrayViewModel;
                WidgetExpressionTrayBinding binding;
                expressionTrayViewModel = WidgetExpressionTray.this.getExpressionTrayViewModel();
                binding = WidgetExpressionTray.this.getBinding();
                TextView textView = binding.j;
                m.checkNotNullExpressionValue(textView, "binding.expressionTraySearchButton");
                expressionTrayViewModel.clickSearch(textView.getText().toString());
            }
        });
        setWindowInsetsListeners();
        getBinding().n.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() { // from class: com.discord.widgets.chat.input.expression.WidgetExpressionTray$onViewBound$2
            @Override // com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener, com.google.android.material.appbar.AppBarLayout.BaseOnOffsetChangedListener
            public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                BehaviorSubject behaviorSubject;
                behaviorSubject = WidgetExpressionTray.this.isAtInitialScrollPositionSubject;
                behaviorSubject.onNext(Boolean.valueOf(i == 0));
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getExpressionTrayViewModel().observeViewState(), this, null, 2, null), WidgetExpressionTray.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetExpressionTray$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getExpressionTrayViewModel().observeEvents(), this, null, 2, null), WidgetExpressionTray.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetExpressionTray$onViewBoundOrOnResume$2(this));
    }

    public final void setEmojiPickerListener(EmojiPickerListener emojiPickerListener) {
        m.checkNotNullParameter(emojiPickerListener, "emojiPickerListener");
        this.emojiPickerListener = emojiPickerListener;
    }

    public final void setOnBackspacePressedListener(OnBackspacePressedListener onBackspacePressedListener) {
        this.onBackspacePressedListener = onBackspacePressedListener;
    }

    public final void setOnEmojiSearchOpenedListener(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "callback");
        this.onEmojiSearchOpenedListener = function0;
    }

    public final void setStickerPickerListener(StickerPickerListener stickerPickerListener) {
        m.checkNotNullParameter(stickerPickerListener, "stickerPickerListener");
        this.stickerPickerListener = stickerPickerListener;
    }
}
