package com.discord.widgets.chat.input.expression;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppViewModel;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreExpressionPickerNavigation;
import com.discord.stores.StoreGuildStickers;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreUser;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.expression.ExpressionDetailPage;
import com.discord.widgets.chat.input.expression.ExpressionPickerEvent;
import com.discord.widgets.chat.input.gifpicker.GifCategoryItem;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: ExpressionTrayViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 O2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004OPQRBc\u0012\b\b\u0002\u00104\u001a\u000203\u0012\b\b\u0002\u0010=\u001a\u00020<\u0012\b\b\u0002\u0010C\u001a\u00020B\u0012\b\b\u0002\u0010I\u001a\u00020H\u0012\b\b\u0002\u0010F\u001a\u00020E\u0012\b\b\u0002\u0010@\u001a\u00020?\u0012\u000e\b\u0002\u0010K\u001a\b\u0012\u0004\u0012\u00020\u00030\u001f\u0012\u000e\b\u0002\u0010L\u001a\b\u0012\u0004\u0012\u00020\b0\u001f¢\u0006\u0004\bM\u0010NJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000e\u0010\rJ-\u0010\u0014\u001a\u00020\u00052\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u00102\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J-\u0010\u0016\u001a\u00020\u00052\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\u000fj\u0004\u0018\u0001`\u00102\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0016\u0010\u0015J\u000f\u0010\u0017\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0017\u0010\rJ\u0017\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u001c\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u001d\u0010\u001eJ\u0013\u0010!\u001a\b\u0012\u0004\u0012\u00020 0\u001f¢\u0006\u0004\b!\u0010\"J\u0015\u0010$\u001a\u00020\u00052\u0006\u0010#\u001a\u00020\u0018¢\u0006\u0004\b$\u0010%J\u0019\u0010&\u001a\u00020\u00052\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\b&\u0010'J\r\u0010(\u001a\u00020\u0005¢\u0006\u0004\b(\u0010\rJ\u0015\u0010+\u001a\u00020\u00052\u0006\u0010*\u001a\u00020)¢\u0006\u0004\b+\u0010,J\u0015\u0010/\u001a\u00020\u00052\u0006\u0010.\u001a\u00020-¢\u0006\u0004\b/\u00100J\u0015\u00102\u001a\u00020\u00052\u0006\u00101\u001a\u00020-¢\u0006\u0004\b2\u00100R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R:\u00108\u001a&\u0012\f\u0012\n 7*\u0004\u0018\u00010 0  7*\u0012\u0012\f\u0012\n 7*\u0004\u0018\u00010 0 \u0018\u000106068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109R\u0016\u0010:\u001a\u00020-8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u0010;R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>R\u0016\u0010@\u001a\u00020?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010AR\u0016\u0010C\u001a\u00020B8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bC\u0010DR\u0016\u0010F\u001a\u00020E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u0016\u0010I\u001a\u00020H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010J¨\u0006S"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;)V", "Lcom/discord/widgets/chat/input/expression/ExpressionPickerEvent;", "expressionPickerEvent", "handleExpressionPickerEvents", "(Lcom/discord/widgets/chat/input/expression/ExpressionPickerEvent;)V", "showEmojiPickerSheet", "()V", "showGifPickerSheet", "", "Lcom/discord/primitives/StickerPackId;", "stickerPackId", "", "searchText", "showStickerPickerSheet", "(Ljava/lang/Long;Ljava/lang/String;)V", "showStickerPickerInline", "hideExpressionPicker", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "expressionTab", "getChatInputComponentType", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)Ljava/lang/String;", "viewState", "updateViewState", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;)V", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;", "observeEvents", "()Lrx/Observable;", "expressionTrayTab", "selectTab", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;)V", "clickSearch", "(Ljava/lang/String;)V", "clickBack", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;", "gifCategoryItem", "selectGifCategory", "(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;)V", "", "show", "showStickersSearchBar", "(Z)V", "isActive", "handleIsActive", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "wasActive", "Z", "Lcom/discord/stores/StoreExpressionPickerNavigation;", "storeExpressionPickerNavigation", "Lcom/discord/stores/StoreExpressionPickerNavigation;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreChannelsSelected;", "Lcom/discord/stores/StoreGuildStickers;", "storeGuildStickers", "Lcom/discord/stores/StoreGuildStickers;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "storeStateObservable", "expressionPickerNavigationObservable", HookHelper.constructorName, "(Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuildStickers;Lcom/discord/stores/StoreGuilds;Lrx/Observable;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ExpressionTrayViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final StoreAnalytics storeAnalytics;
    private final StoreChannelsSelected storeChannelsSelected;
    private final StoreExpressionPickerNavigation storeExpressionPickerNavigation;
    private final StoreGuildStickers storeGuildStickers;
    private final StoreGuilds storeGuilds;
    private final StoreUser storeUser;
    private boolean wasActive;

    /* compiled from: ExpressionTrayViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.expression.ExpressionTrayViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(ExpressionTrayViewModel expressionTrayViewModel) {
            super(1, expressionTrayViewModel, ExpressionTrayViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((ExpressionTrayViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: ExpressionTrayViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionPickerEvent;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/expression/ExpressionPickerEvent;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.expression.ExpressionTrayViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<ExpressionPickerEvent, Unit> {
        public AnonymousClass2(ExpressionTrayViewModel expressionTrayViewModel) {
            super(1, expressionTrayViewModel, ExpressionTrayViewModel.class, "handleExpressionPickerEvents", "handleExpressionPickerEvents(Lcom/discord/widgets/chat/input/expression/ExpressionPickerEvent;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ExpressionPickerEvent expressionPickerEvent) {
            invoke2(expressionPickerEvent);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ExpressionPickerEvent expressionPickerEvent) {
            m.checkNotNullParameter(expressionPickerEvent, "p1");
            ((ExpressionTrayViewModel) this.receiver).handleExpressionPickerEvents(expressionPickerEvent);
        }
    }

    /* compiled from: ExpressionTrayViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J=\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Companion;", "", "Lcom/discord/stores/StoreExpressionPickerNavigation;", "storeExpressionPickerNavigation", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreGuildStickers;", "storeGuildStickers", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;", "observeStoreState", "(Lcom/discord/stores/StoreExpressionPickerNavigation;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuildStickers;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(StoreExpressionPickerNavigation storeExpressionPickerNavigation, StoreChannelsSelected storeChannelsSelected, StoreUser storeUser, StoreGuildStickers storeGuildStickers, StoreGuilds storeGuilds) {
            Observable<StoreState> g = Observable.g(storeExpressionPickerNavigation.observeSelectedTab(), storeChannelsSelected.observeResolvedSelectedChannel(), StoreUser.observeMe$default(storeUser, false, 1, null), storeGuildStickers.observeGuildStickers(), storeGuilds.observeGuilds(), ExpressionTrayViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(g, "Observable.combineLatest…aft\n          )\n        }");
            return g;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ExpressionTrayViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;", "", HookHelper.constructorName, "()V", "HideExpressionTray", "ShowEmojiPickerSheet", "ShowGifPickerSheet", "ShowStickerPicker", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$HideExpressionTray;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowEmojiPickerSheet;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowGifPickerSheet;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPicker;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: ExpressionTrayViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$HideExpressionTray;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class HideExpressionTray extends Event {
            public static final HideExpressionTray INSTANCE = new HideExpressionTray();

            private HideExpressionTray() {
                super(null);
            }
        }

        /* compiled from: ExpressionTrayViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowEmojiPickerSheet;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowEmojiPickerSheet extends Event {
            public static final ShowEmojiPickerSheet INSTANCE = new ShowEmojiPickerSheet();

            private ShowEmojiPickerSheet() {
                super(null);
            }
        }

        /* compiled from: ExpressionTrayViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowGifPickerSheet;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowGifPickerSheet extends Event {
            public static final ShowGifPickerSheet INSTANCE = new ShowGifPickerSheet();

            private ShowGifPickerSheet() {
                super(null);
            }
        }

        /* compiled from: ExpressionTrayViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u000e\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b\u001f\u0010 J\u0018\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ8\u0010\u000f\u001a\u00020\u00002\u0010\b\u0002\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\bJ\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\t2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\bR!\u0010\f\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005R\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\u000b¨\u0006!"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPicker;", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event;", "", "Lcom/discord/primitives/StickerPackId;", "component1", "()Ljava/lang/Long;", "", "component2", "()Ljava/lang/String;", "", "component3", "()Z", "stickerPackId", "searchText", "inline", "copy", "(Ljava/lang/Long;Ljava/lang/String;Z)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$Event$ShowStickerPicker;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getSearchText", "Ljava/lang/Long;", "getStickerPackId", "Z", "getInline", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowStickerPicker extends Event {
            private final boolean inline;
            private final String searchText;
            private final Long stickerPackId;

            public /* synthetic */ ShowStickerPicker(Long l, String str, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(l, str, (i & 4) != 0 ? false : z2);
            }

            public static /* synthetic */ ShowStickerPicker copy$default(ShowStickerPicker showStickerPicker, Long l, String str, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    l = showStickerPicker.stickerPackId;
                }
                if ((i & 2) != 0) {
                    str = showStickerPicker.searchText;
                }
                if ((i & 4) != 0) {
                    z2 = showStickerPicker.inline;
                }
                return showStickerPicker.copy(l, str, z2);
            }

            public final Long component1() {
                return this.stickerPackId;
            }

            public final String component2() {
                return this.searchText;
            }

            public final boolean component3() {
                return this.inline;
            }

            public final ShowStickerPicker copy(Long l, String str, boolean z2) {
                return new ShowStickerPicker(l, str, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ShowStickerPicker)) {
                    return false;
                }
                ShowStickerPicker showStickerPicker = (ShowStickerPicker) obj;
                return m.areEqual(this.stickerPackId, showStickerPicker.stickerPackId) && m.areEqual(this.searchText, showStickerPicker.searchText) && this.inline == showStickerPicker.inline;
            }

            public final boolean getInline() {
                return this.inline;
            }

            public final String getSearchText() {
                return this.searchText;
            }

            public final Long getStickerPackId() {
                return this.stickerPackId;
            }

            public int hashCode() {
                Long l = this.stickerPackId;
                int i = 0;
                int hashCode = (l != null ? l.hashCode() : 0) * 31;
                String str = this.searchText;
                if (str != null) {
                    i = str.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.inline;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("ShowStickerPicker(stickerPackId=");
                R.append(this.stickerPackId);
                R.append(", searchText=");
                R.append(this.searchText);
                R.append(", inline=");
                return a.M(R, this.inline, ")");
            }

            public ShowStickerPicker(Long l, String str, boolean z2) {
                super(null);
                this.stickerPackId = l;
                this.searchText = str;
                this.inline = z2;
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ExpressionTrayViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001Bo\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0010\b\u0002\u0010\u0018\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\t\u0012&\u0010\u001a\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u00020\u000e0\f0\f\u0012\u0010\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u0011\u0012\u0006\u0010\u001c\u001a\u00020\u0014¢\u0006\u0004\b3\u00104J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ0\u0010\u000f\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u00020\u000e0\f0\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0012\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0080\u0001\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0017\u001a\u00020\u00022\u0010\b\u0002\u0010\u0018\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\t2(\b\u0002\u0010\u001a\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u00020\u000e0\f0\f2\u0012\b\u0002\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00112\b\b\u0002\u0010\u001c\u001a\u00020\u0014HÆ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u001a\u0010&\u001a\u00020\u00142\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b&\u0010'R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010(\u001a\u0004\b)\u0010\u0004R#\u0010\u001b\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u00060\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010*\u001a\u0004\b+\u0010\u0013R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010,\u001a\u0004\b-\u0010\u000bR\u0019\u0010\u001c\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010.\u001a\u0004\b\u001c\u0010\u0016R!\u0010\u0018\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010/\u001a\u0004\b0\u0010\bR9\u0010\u001a\u001a\"\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0005j\u0002`\r\u0012\u0004\u0012\u00020\u000e0\f0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00101\u001a\u0004\b2\u0010\u0010¨\u00065"}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;", "", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "component1", "()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/lang/Long;", "Lcom/discord/models/user/MeUser;", "component3", "()Lcom/discord/models/user/MeUser;", "", "Lcom/discord/primitives/StickerId;", "Lcom/discord/api/sticker/Sticker;", "component4", "()Ljava/util/Map;", "", "component5", "()Ljava/util/Set;", "", "component6", "()Z", "selectedExpressionTab", "guildId", "meUser", "guildStickers", "userGuildIds", "isThreadDraft", "copy", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Ljava/lang/Long;Lcom/discord/models/user/MeUser;Ljava/util/Map;Ljava/util/Set;Z)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "getSelectedExpressionTab", "Ljava/util/Set;", "getUserGuildIds", "Lcom/discord/models/user/MeUser;", "getMeUser", "Z", "Ljava/lang/Long;", "getGuildId", "Ljava/util/Map;", "getGuildStickers", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Ljava/lang/Long;Lcom/discord/models/user/MeUser;Ljava/util/Map;Ljava/util/Set;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Long guildId;
        private final Map<Long, Map<Long, Sticker>> guildStickers;
        private final boolean isThreadDraft;
        private final MeUser meUser;
        private final ExpressionTrayTab selectedExpressionTab;
        private final Set<Long> userGuildIds;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(ExpressionTrayTab expressionTrayTab, Long l, MeUser meUser, Map<Long, ? extends Map<Long, Sticker>> map, Set<Long> set, boolean z2) {
            m.checkNotNullParameter(expressionTrayTab, "selectedExpressionTab");
            m.checkNotNullParameter(map, "guildStickers");
            m.checkNotNullParameter(set, "userGuildIds");
            this.selectedExpressionTab = expressionTrayTab;
            this.guildId = l;
            this.meUser = meUser;
            this.guildStickers = map;
            this.userGuildIds = set;
            this.isThreadDraft = z2;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, ExpressionTrayTab expressionTrayTab, Long l, MeUser meUser, Map map, Set set, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                expressionTrayTab = storeState.selectedExpressionTab;
            }
            if ((i & 2) != 0) {
                l = storeState.guildId;
            }
            Long l2 = l;
            if ((i & 4) != 0) {
                meUser = storeState.meUser;
            }
            MeUser meUser2 = meUser;
            Map<Long, Map<Long, Sticker>> map2 = map;
            if ((i & 8) != 0) {
                map2 = storeState.guildStickers;
            }
            Map map3 = map2;
            Set<Long> set2 = set;
            if ((i & 16) != 0) {
                set2 = storeState.userGuildIds;
            }
            Set set3 = set2;
            if ((i & 32) != 0) {
                z2 = storeState.isThreadDraft;
            }
            return storeState.copy(expressionTrayTab, l2, meUser2, map3, set3, z2);
        }

        public final ExpressionTrayTab component1() {
            return this.selectedExpressionTab;
        }

        public final Long component2() {
            return this.guildId;
        }

        public final MeUser component3() {
            return this.meUser;
        }

        public final Map<Long, Map<Long, Sticker>> component4() {
            return this.guildStickers;
        }

        public final Set<Long> component5() {
            return this.userGuildIds;
        }

        public final boolean component6() {
            return this.isThreadDraft;
        }

        public final StoreState copy(ExpressionTrayTab expressionTrayTab, Long l, MeUser meUser, Map<Long, ? extends Map<Long, Sticker>> map, Set<Long> set, boolean z2) {
            m.checkNotNullParameter(expressionTrayTab, "selectedExpressionTab");
            m.checkNotNullParameter(map, "guildStickers");
            m.checkNotNullParameter(set, "userGuildIds");
            return new StoreState(expressionTrayTab, l, meUser, map, set, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.selectedExpressionTab, storeState.selectedExpressionTab) && m.areEqual(this.guildId, storeState.guildId) && m.areEqual(this.meUser, storeState.meUser) && m.areEqual(this.guildStickers, storeState.guildStickers) && m.areEqual(this.userGuildIds, storeState.userGuildIds) && this.isThreadDraft == storeState.isThreadDraft;
        }

        public final Long getGuildId() {
            return this.guildId;
        }

        public final Map<Long, Map<Long, Sticker>> getGuildStickers() {
            return this.guildStickers;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final ExpressionTrayTab getSelectedExpressionTab() {
            return this.selectedExpressionTab;
        }

        public final Set<Long> getUserGuildIds() {
            return this.userGuildIds;
        }

        public int hashCode() {
            ExpressionTrayTab expressionTrayTab = this.selectedExpressionTab;
            int i = 0;
            int hashCode = (expressionTrayTab != null ? expressionTrayTab.hashCode() : 0) * 31;
            Long l = this.guildId;
            int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
            MeUser meUser = this.meUser;
            int hashCode3 = (hashCode2 + (meUser != null ? meUser.hashCode() : 0)) * 31;
            Map<Long, Map<Long, Sticker>> map = this.guildStickers;
            int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
            Set<Long> set = this.userGuildIds;
            if (set != null) {
                i = set.hashCode();
            }
            int i2 = (hashCode4 + i) * 31;
            boolean z2 = this.isThreadDraft;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isThreadDraft() {
            return this.isThreadDraft;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(selectedExpressionTab=");
            R.append(this.selectedExpressionTab);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", meUser=");
            R.append(this.meUser);
            R.append(", guildStickers=");
            R.append(this.guildStickers);
            R.append(", userGuildIds=");
            R.append(this.userGuildIds);
            R.append(", isThreadDraft=");
            return a.M(R, this.isThreadDraft, ")");
        }

        public /* synthetic */ StoreState(ExpressionTrayTab expressionTrayTab, Long l, MeUser meUser, Map map, Set set, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(expressionTrayTab, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : meUser, map, set, z2);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            ExpressionTrayTab.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            ExpressionTrayTab expressionTrayTab = ExpressionTrayTab.EMOJI;
            iArr[expressionTrayTab.ordinal()] = 1;
            ExpressionTrayTab expressionTrayTab2 = ExpressionTrayTab.GIF;
            iArr[expressionTrayTab2.ordinal()] = 2;
            ExpressionTrayTab expressionTrayTab3 = ExpressionTrayTab.STICKER;
            iArr[expressionTrayTab3.ordinal()] = 3;
            ExpressionTrayTab.values();
            int[] iArr2 = new int[3];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[expressionTrayTab.ordinal()] = 1;
            iArr2[expressionTrayTab2.ordinal()] = 2;
            iArr2[expressionTrayTab3.ordinal()] = 3;
        }
    }

    public ExpressionTrayViewModel() {
        this(null, null, null, null, null, null, null, null, 255, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ ExpressionTrayViewModel(com.discord.stores.StoreAnalytics r9, com.discord.stores.StoreExpressionPickerNavigation r10, com.discord.stores.StoreChannelsSelected r11, com.discord.stores.StoreUser r12, com.discord.stores.StoreGuildStickers r13, com.discord.stores.StoreGuilds r14, rx.Observable r15, rx.Observable r16, int r17, kotlin.jvm.internal.DefaultConstructorMarker r18) {
        /*
            r8 = this;
            r0 = r17
            r1 = r0 & 1
            if (r1 == 0) goto Ld
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreAnalytics r1 = r1.getAnalytics()
            goto Le
        Ld:
            r1 = r9
        Le:
            r2 = r0 & 2
            if (r2 == 0) goto L19
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreExpressionPickerNavigation r2 = r2.getExpressionPickerNavigation()
            goto L1a
        L19:
            r2 = r10
        L1a:
            r3 = r0 & 4
            if (r3 == 0) goto L25
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannelsSelected r3 = r3.getChannelsSelected()
            goto L26
        L25:
            r3 = r11
        L26:
            r4 = r0 & 8
            if (r4 == 0) goto L31
            com.discord.stores.StoreStream$Companion r4 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r4 = r4.getUsers()
            goto L32
        L31:
            r4 = r12
        L32:
            r5 = r0 & 16
            if (r5 == 0) goto L3d
            com.discord.stores.StoreStream$Companion r5 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildStickers r5 = r5.getGuildStickers()
            goto L3e
        L3d:
            r5 = r13
        L3e:
            r6 = r0 & 32
            if (r6 == 0) goto L49
            com.discord.stores.StoreStream$Companion r6 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r6 = r6.getGuilds()
            goto L4a
        L49:
            r6 = r14
        L4a:
            r7 = r0 & 64
            if (r7 == 0) goto L5b
            com.discord.widgets.chat.input.expression.ExpressionTrayViewModel$Companion r7 = com.discord.widgets.chat.input.expression.ExpressionTrayViewModel.Companion
            r9 = r7
            r10 = r2
            r11 = r3
            r12 = r4
            r13 = r5
            r14 = r6
            rx.Observable r7 = com.discord.widgets.chat.input.expression.ExpressionTrayViewModel.Companion.access$observeStoreState(r9, r10, r11, r12, r13, r14)
            goto L5c
        L5b:
            r7 = r15
        L5c:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L6b
            com.discord.widgets.chat.input.expression.ExpressionPickerEventBus$Companion r0 = com.discord.widgets.chat.input.expression.ExpressionPickerEventBus.Companion
            com.discord.widgets.chat.input.expression.ExpressionPickerEventBus r0 = r0.getINSTANCE()
            rx.Observable r0 = r0.observeExpressionPickerEvents()
            goto L6d
        L6b:
            r0 = r16
        L6d:
            r9 = r8
            r10 = r1
            r11 = r2
            r12 = r3
            r13 = r4
            r14 = r5
            r15 = r6
            r16 = r7
            r17 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.expression.ExpressionTrayViewModel.<init>(com.discord.stores.StoreAnalytics, com.discord.stores.StoreExpressionPickerNavigation, com.discord.stores.StoreChannelsSelected, com.discord.stores.StoreUser, com.discord.stores.StoreGuildStickers, com.discord.stores.StoreGuilds, rx.Observable, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public static /* synthetic */ void clickSearch$default(ExpressionTrayViewModel expressionTrayViewModel, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        expressionTrayViewModel.clickSearch(str);
    }

    private final String getChatInputComponentType(ExpressionTrayTab expressionTrayTab) {
        int ordinal = expressionTrayTab.ordinal();
        if (ordinal == 0) {
            return "emoji";
        }
        if (ordinal == 1) {
            return "GIF";
        }
        if (ordinal == 2) {
            return "sticker";
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleExpressionPickerEvents(ExpressionPickerEvent expressionPickerEvent) {
        if (m.areEqual(expressionPickerEvent, ExpressionPickerEvent.CloseExpressionPicker.INSTANCE)) {
            hideExpressionPicker();
        } else if (expressionPickerEvent instanceof ExpressionPickerEvent.OpenStickerPicker) {
            ExpressionPickerEvent.OpenStickerPicker openStickerPicker = (ExpressionPickerEvent.OpenStickerPicker) expressionPickerEvent;
            if (!openStickerPicker.getInline()) {
                showStickerPickerSheet(openStickerPicker.getStickerPackId(), openStickerPicker.getSearchText());
            }
            Long stickerPackId = openStickerPicker.getStickerPackId();
            long longValue = stickerPackId != null ? stickerPackId.longValue() : -1L;
            selectTab(ExpressionTrayTab.STICKER);
            showStickerPickerInline(Long.valueOf(longValue), openStickerPicker.getSearchText());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        updateViewState(ViewState.copy$default(requireViewState(), storeState.getSelectedExpressionTab(), null, false, !storeState.isThreadDraft(), 6, null));
    }

    private final void hideExpressionPicker() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.HideExpressionTray.INSTANCE);
    }

    private final void showEmojiPickerSheet() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowEmojiPickerSheet.INSTANCE);
    }

    private final void showGifPickerSheet() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowGifPickerSheet.INSTANCE);
    }

    private final void showStickerPickerInline(Long l, String str) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowStickerPicker(l, str, true));
    }

    public static /* synthetic */ void showStickerPickerInline$default(ExpressionTrayViewModel expressionTrayViewModel, Long l, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            l = null;
        }
        if ((i & 2) != 0) {
            str = null;
        }
        expressionTrayViewModel.showStickerPickerInline(l, str);
    }

    private final void showStickerPickerSheet(Long l, String str) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowStickerPicker(l, str, false, 4, null));
    }

    public static /* synthetic */ void showStickerPickerSheet$default(ExpressionTrayViewModel expressionTrayViewModel, Long l, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            l = null;
        }
        if ((i & 2) != 0) {
            str = null;
        }
        expressionTrayViewModel.showStickerPickerSheet(l, str);
    }

    public final void clickBack() {
        ViewState requireViewState = requireViewState();
        if (requireViewState.getExpressionDetailPage() != null) {
            updateViewState(ViewState.copy$default(requireViewState, null, null, false, false, 13, null));
        }
    }

    public final void clickSearch(String str) {
        int ordinal = requireViewState().getSelectedExpressionTab().ordinal();
        if (ordinal == 0) {
            showEmojiPickerSheet();
        } else if (ordinal == 1) {
            showGifPickerSheet();
        } else if (ordinal == 2) {
            showStickerPickerSheet$default(this, null, str, 1, null);
        }
    }

    public final void handleIsActive(boolean z2) {
        ViewState viewState;
        ExpressionTrayTab selectedExpressionTab;
        if (z2 && !this.wasActive && (viewState = getViewState()) != null && (selectedExpressionTab = viewState.getSelectedExpressionTab()) != null) {
            this.storeAnalytics.trackChatInputComponentViewed(getChatInputComponentType(selectedExpressionTab));
        }
        this.wasActive = z2;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void selectGifCategory(GifCategoryItem gifCategoryItem) {
        m.checkNotNullParameter(gifCategoryItem, "gifCategoryItem");
        updateViewState(ViewState.copy$default(requireViewState(), null, new ExpressionDetailPage.GifCategoryPage(gifCategoryItem), false, false, 13, null));
    }

    public final void selectTab(ExpressionTrayTab expressionTrayTab) {
        m.checkNotNullParameter(expressionTrayTab, "expressionTrayTab");
        this.storeExpressionPickerNavigation.onSelectTab(expressionTrayTab);
    }

    public final void showStickersSearchBar(boolean z2) {
        updateViewState(ViewState.copy$default(requireViewState(), null, null, z2, false, 11, null));
    }

    public void updateViewState(ViewState viewState) {
        m.checkNotNullParameter(viewState, "viewState");
        ViewState viewState2 = getViewState();
        ExpressionTrayTab selectedExpressionTab = viewState2 != null ? viewState2.getSelectedExpressionTab() : null;
        ExpressionTrayTab selectedExpressionTab2 = viewState.getSelectedExpressionTab();
        if (viewState.getShowLandingPage() && selectedExpressionTab != selectedExpressionTab2 && this.wasActive) {
            this.storeAnalytics.trackChatInputComponentViewed(getChatInputComponentType(selectedExpressionTab2));
        }
        super.updateViewState((ExpressionTrayViewModel) viewState);
    }

    /* compiled from: ExpressionTrayViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u000e\u001a\u00020\b\u0012\u0006\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ:\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u000e\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0019\u001a\u00020\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\u000f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001b\u001a\u0004\b\u001c\u0010\nR\u0019\u0010\u001d\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001b\u001a\u0004\b\u001e\u0010\nR\u0019\u0010\u000e\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001f\u0010\nR\u0019\u0010 \u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u001b\u001a\u0004\b!\u0010\nR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\"\u001a\u0004\b#\u0010\u0004R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010$\u001a\u0004\b%\u0010\u0007¨\u0006("}, d2 = {"Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;", "", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "component1", "()Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;", "component2", "()Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;", "", "component3", "()Z", "component4", "selectedExpressionTab", "expressionDetailPage", "showStickersSearchBar", "showGifsAndStickers", "copy", "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZZ)Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowGifsAndStickers", "showSearchBar", "getShowSearchBar", "getShowStickersSearchBar", "showLandingPage", "getShowLandingPage", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;", "getSelectedExpressionTab", "Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;", "getExpressionDetailPage", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/expression/ExpressionTrayTab;Lcom/discord/widgets/chat/input/expression/ExpressionDetailPage;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final ExpressionDetailPage expressionDetailPage;
        private final ExpressionTrayTab selectedExpressionTab;
        private final boolean showGifsAndStickers;
        private final boolean showLandingPage;
        private final boolean showSearchBar;
        private final boolean showStickersSearchBar;

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                ExpressionTrayTab.values();
                int[] iArr = new int[3];
                $EnumSwitchMapping$0 = iArr;
                iArr[ExpressionTrayTab.EMOJI.ordinal()] = 1;
                iArr[ExpressionTrayTab.GIF.ordinal()] = 2;
                iArr[ExpressionTrayTab.STICKER.ordinal()] = 3;
            }
        }

        public ViewState(ExpressionTrayTab expressionTrayTab, ExpressionDetailPage expressionDetailPage, boolean z2, boolean z3) {
            m.checkNotNullParameter(expressionTrayTab, "selectedExpressionTab");
            this.selectedExpressionTab = expressionTrayTab;
            this.expressionDetailPage = expressionDetailPage;
            this.showStickersSearchBar = z2;
            this.showGifsAndStickers = z3;
            this.showLandingPage = expressionDetailPage == null;
            int ordinal = expressionTrayTab.ordinal();
            if (ordinal == 0 || ordinal == 1) {
                z2 = true;
            } else if (ordinal != 2) {
                throw new NoWhenBranchMatchedException();
            }
            this.showSearchBar = z2;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, ExpressionTrayTab expressionTrayTab, ExpressionDetailPage expressionDetailPage, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                expressionTrayTab = viewState.selectedExpressionTab;
            }
            if ((i & 2) != 0) {
                expressionDetailPage = viewState.expressionDetailPage;
            }
            if ((i & 4) != 0) {
                z2 = viewState.showStickersSearchBar;
            }
            if ((i & 8) != 0) {
                z3 = viewState.showGifsAndStickers;
            }
            return viewState.copy(expressionTrayTab, expressionDetailPage, z2, z3);
        }

        public final ExpressionTrayTab component1() {
            return this.selectedExpressionTab;
        }

        public final ExpressionDetailPage component2() {
            return this.expressionDetailPage;
        }

        public final boolean component3() {
            return this.showStickersSearchBar;
        }

        public final boolean component4() {
            return this.showGifsAndStickers;
        }

        public final ViewState copy(ExpressionTrayTab expressionTrayTab, ExpressionDetailPage expressionDetailPage, boolean z2, boolean z3) {
            m.checkNotNullParameter(expressionTrayTab, "selectedExpressionTab");
            return new ViewState(expressionTrayTab, expressionDetailPage, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.selectedExpressionTab, viewState.selectedExpressionTab) && m.areEqual(this.expressionDetailPage, viewState.expressionDetailPage) && this.showStickersSearchBar == viewState.showStickersSearchBar && this.showGifsAndStickers == viewState.showGifsAndStickers;
        }

        public final ExpressionDetailPage getExpressionDetailPage() {
            return this.expressionDetailPage;
        }

        public final ExpressionTrayTab getSelectedExpressionTab() {
            return this.selectedExpressionTab;
        }

        public final boolean getShowGifsAndStickers() {
            return this.showGifsAndStickers;
        }

        public final boolean getShowLandingPage() {
            return this.showLandingPage;
        }

        public final boolean getShowSearchBar() {
            return this.showSearchBar;
        }

        public final boolean getShowStickersSearchBar() {
            return this.showStickersSearchBar;
        }

        public int hashCode() {
            ExpressionTrayTab expressionTrayTab = this.selectedExpressionTab;
            int i = 0;
            int hashCode = (expressionTrayTab != null ? expressionTrayTab.hashCode() : 0) * 31;
            ExpressionDetailPage expressionDetailPage = this.expressionDetailPage;
            if (expressionDetailPage != null) {
                i = expressionDetailPage.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.showStickersSearchBar;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.showGifsAndStickers;
            if (!z3) {
                i3 = z3 ? 1 : 0;
            }
            return i6 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(selectedExpressionTab=");
            R.append(this.selectedExpressionTab);
            R.append(", expressionDetailPage=");
            R.append(this.expressionDetailPage);
            R.append(", showStickersSearchBar=");
            R.append(this.showStickersSearchBar);
            R.append(", showGifsAndStickers=");
            return a.M(R, this.showGifsAndStickers, ")");
        }

        public /* synthetic */ ViewState(ExpressionTrayTab expressionTrayTab, ExpressionDetailPage expressionDetailPage, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(expressionTrayTab, (i & 2) != 0 ? null : expressionDetailPage, z2, z3);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ExpressionTrayViewModel(StoreAnalytics storeAnalytics, StoreExpressionPickerNavigation storeExpressionPickerNavigation, StoreChannelsSelected storeChannelsSelected, StoreUser storeUser, StoreGuildStickers storeGuildStickers, StoreGuilds storeGuilds, Observable<StoreState> observable, Observable<ExpressionPickerEvent> observable2) {
        super(new ViewState(ExpressionTrayTab.EMOJI, null, true, true, 2, null));
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(storeExpressionPickerNavigation, "storeExpressionPickerNavigation");
        m.checkNotNullParameter(storeChannelsSelected, "storeChannelsSelected");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeGuildStickers, "storeGuildStickers");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(observable, "storeStateObservable");
        m.checkNotNullParameter(observable2, "expressionPickerNavigationObservable");
        this.storeAnalytics = storeAnalytics;
        this.storeExpressionPickerNavigation = storeExpressionPickerNavigation;
        this.storeChannelsSelected = storeChannelsSelected;
        this.storeUser = storeUser;
        this.storeGuildStickers = storeGuildStickers;
        this.storeGuilds = storeGuilds;
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), ExpressionTrayViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable2, this, null, 2, null), ExpressionTrayViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(this));
    }
}
