package com.discord.widgets.chat.input.expression;

import andhook.lib.HookHelper;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.MarginLayoutParamsCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rx.LeadingEdgeThrottle;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.views.StickyHeaderItemDecoration;
import com.discord.widgets.chat.input.ExpressionPickerItemDecoration;
import d0.t.m0;
import d0.t.n;
import d0.z.d.m;
import j0.l.a.r;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;
/* compiled from: WidgetExpressionPickerAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\"\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\r\b&\u0018\u0000 M2\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0003MNOB\u001f\u0012\u0006\u0010J\u001a\u00020\u0007\u0012\u000e\b\u0002\u00107\u001a\b\u0012\u0004\u0012\u00020\u000f06¢\u0006\u0004\bK\u0010LJ\u000f\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\r\u001a\u00020\u00042\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0013\u001a\u00020\u0000H&¢\u0006\u0004\b\u0015\u0010\u0016J\u0019\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0019\u0010\u001b\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u001a\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u001b\u0010\u001cR$\u0010\u001e\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u0016\u0010'\u001a\u00020$8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b%\u0010&R\"\u0010)\u001a\u00020(8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R$\u00100\u001a\u0004\u0018\u00010/8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b0\u00101\u001a\u0004\b2\u00103\"\u0004\b4\u00105R\u001f\u00107\u001a\b\u0012\u0004\u0012\u00020\u000f068\u0006@\u0006¢\u0006\f\n\u0004\b7\u00108\u001a\u0004\b9\u0010:R\u0016\u0010=\u001a\u00020\u000f8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b;\u0010<R(\u0010>\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000b8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b>\u0010?\u001a\u0004\b@\u0010A\"\u0004\bB\u0010\u000eR.\u0010D\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00040C8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bD\u0010E\u001a\u0004\bF\u0010G\"\u0004\bH\u0010I¨\u0006P"}, d2 = {"Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;", "", "setupScrollObservables", "()V", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "onAttachedToRecyclerView", "(Landroidx/recyclerview/widget/RecyclerView;)V", "", "data", "setData", "(Ljava/util/List;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "scrollToPosition", "(I)V", "adapter", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "createStickyHeaderViewHolder", "(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "Landroid/view/View;", "getAndBindHeaderView", "(I)Landroid/view/View;", "itemPosition", "getHeaderPositionForItem", "(I)Ljava/lang/Integer;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "onScrollListener", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "getOnScrollListener", "()Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "setOnScrollListener", "(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V", "Landroidx/recyclerview/widget/GridLayoutManager;", "getLayoutManager", "()Landroidx/recyclerview/widget/GridLayoutManager;", "layoutManager", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeadersManager;", "stickyHeaderManager", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeadersManager;", "getStickyHeaderManager", "()Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeadersManager;", "setStickyHeaderManager", "(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeadersManager;)V", "Lcom/discord/widgets/chat/input/ExpressionPickerItemDecoration;", "itemDecoration", "Lcom/discord/widgets/chat/input/ExpressionPickerItemDecoration;", "getItemDecoration", "()Lcom/discord/widgets/chat/input/ExpressionPickerItemDecoration;", "setItemDecoration", "(Lcom/discord/widgets/chat/input/ExpressionPickerItemDecoration;)V", "", "headerTypes", "Ljava/util/Set;", "getHeaderTypes", "()Ljava/util/Set;", "getNumColumns", "()I", "numColumns", "headerIndices", "Ljava/util/List;", "getHeaderIndices", "()Ljava/util/List;", "setHeaderIndices", "Lkotlin/Function1;", "onScrollPositionListener", "Lkotlin/jvm/functions/Function1;", "getOnScrollPositionListener", "()Lkotlin/jvm/functions/Function1;", "setOnScrollPositionListener", "(Lkotlin/jvm/functions/Function1;)V", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Ljava/util/Set;)V", "Companion", "StickyHeaderViewHolder", "StickyHeadersManager", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class WidgetExpressionPickerAdapter extends MGRecyclerAdapterSimple<MGRecyclerDataPayload> implements StickyHeaderItemDecoration.StickyHeaderAdapter {
    public static final Companion Companion = new Companion(null);
    public static final int ITEM_TYPE_HEADER = 0;
    public List<Integer> headerIndices;
    private final Set<Integer> headerTypes;
    private ExpressionPickerItemDecoration itemDecoration;
    private RecyclerView.OnScrollListener onScrollListener;
    private Function1<? super Integer, Unit> onScrollPositionListener;
    public StickyHeadersManager stickyHeaderManager;

    /* compiled from: WidgetExpressionPickerAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J%\u0010\b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\b\u0010\tJ-\u0010\b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\b\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$Companion;", "", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "", "itemSize", "", "defaultNumColumns", "calculateNumOfColumns", "(Landroidx/recyclerview/widget/RecyclerView;FI)I", "margin", "Landroid/content/res/Resources;", "resources", "(IFILandroid/content/res/Resources;)I", "ITEM_TYPE_HEADER", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final int calculateNumOfColumns(int i, float f, int i2, Resources resources) {
            m.checkNotNullParameter(resources, "resources");
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            int i3 = (int) ((displayMetrics.widthPixels - i) / f);
            if (i3 != 0) {
                return i3;
            }
            AppLog appLog = AppLog.g;
            StringBuilder R = a.R("\n          invalid dimensions while calculating numColumns\n          displayMetrics.widthPixels: ");
            R.append(displayMetrics.widthPixels);
            R.append("\n          total margin marginStart: ");
            R.append(i);
            R.append("\n          itemSize: ");
            R.append(f);
            R.append("\n        ");
            Logger.e$default(appLog, d0.g0.m.trimIndent(R.toString()), null, null, 6, null);
            return i2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final int calculateNumOfColumns(RecyclerView recyclerView, float f, int i) {
            m.checkNotNullParameter(recyclerView, "recyclerView");
            ViewGroup.LayoutParams layoutParams = recyclerView.getLayoutParams();
            int i2 = 0;
            int marginStart = layoutParams instanceof ViewGroup.MarginLayoutParams ? MarginLayoutParamsCompat.getMarginStart((ViewGroup.MarginLayoutParams) layoutParams) : 0;
            ViewGroup.LayoutParams layoutParams2 = recyclerView.getLayoutParams();
            if (layoutParams2 instanceof ViewGroup.MarginLayoutParams) {
                i2 = MarginLayoutParamsCompat.getMarginEnd((ViewGroup.MarginLayoutParams) layoutParams2);
            }
            Resources resources = recyclerView.getResources();
            m.checkNotNullExpressionValue(resources, "recyclerView.resources");
            return calculateNumOfColumns(marginStart + i2, f, i, resources);
        }
    }

    /* compiled from: WidgetExpressionPickerAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H&¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH&¢\u0006\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "data", "", "bind", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Landroid/view/View;", "getItemView", "()Landroid/view/View;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface StickyHeaderViewHolder {
        void bind(int i, MGRecyclerDataPayload mGRecyclerDataPayload);

        View getItemView();
    }

    /* compiled from: WidgetExpressionPickerAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeadersManager;", "", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "", "layoutViews", "(Landroidx/recyclerview/widget/RecyclerView;)V", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "stickyHeaderHolder", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "getStickyHeaderHolder", "()Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "Landroid/view/View;", "currentStickyHeaderView", "Landroid/view/View;", "getCurrentStickyHeaderView", "()Landroid/view/View;", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StickyHeadersManager {
        private final View currentStickyHeaderView;
        private final StickyHeaderViewHolder stickyHeaderHolder;

        public StickyHeadersManager(WidgetExpressionPickerAdapter widgetExpressionPickerAdapter) {
            m.checkNotNullParameter(widgetExpressionPickerAdapter, "adapter");
            StickyHeaderViewHolder createStickyHeaderViewHolder = widgetExpressionPickerAdapter.createStickyHeaderViewHolder(widgetExpressionPickerAdapter);
            this.stickyHeaderHolder = createStickyHeaderViewHolder;
            this.currentStickyHeaderView = createStickyHeaderViewHolder.getItemView();
        }

        public final View getCurrentStickyHeaderView() {
            return this.currentStickyHeaderView;
        }

        public final StickyHeaderViewHolder getStickyHeaderHolder() {
            return this.stickyHeaderHolder;
        }

        public final void layoutViews(RecyclerView recyclerView) {
            m.checkNotNullParameter(recyclerView, "recyclerView");
            View view = this.currentStickyHeaderView;
            if (view != null) {
                StickyHeaderItemDecoration.LayoutManager.layoutHeaderView(recyclerView, view);
            }
        }
    }

    public /* synthetic */ WidgetExpressionPickerAdapter(RecyclerView recyclerView, Set set, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(recyclerView, (i & 2) != 0 ? m0.setOf(0) : set);
    }

    private final void setupScrollObservables() {
        Observable n = Observable.n(new Action1<Emitter<Unit>>() { // from class: com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter$setupScrollObservables$1
            public final void call(final Emitter<Unit> emitter) {
                m.checkNotNullParameter(emitter, "emitter");
                WidgetExpressionPickerAdapter.this.setOnScrollListener(new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter$setupScrollObservables$1.1
                    @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
                    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                        m.checkNotNullParameter(recyclerView, "recyclerView");
                        super.onScrolled(recyclerView, i, i2);
                        Emitter.this.onNext(Unit.a);
                    }
                });
            }
        }, Emitter.BackpressureMode.LATEST);
        Observable h02 = Observable.h0(new r(n.j, new LeadingEdgeThrottle(250L, TimeUnit.MILLISECONDS)));
        m.checkNotNullExpressionValue(h02, "Observable\n        .crea…, TimeUnit.MILLISECONDS))");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(h02), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetExpressionPickerAdapter$setupScrollObservables$2(this));
        RecyclerView.OnScrollListener onScrollListener = this.onScrollListener;
        if (onScrollListener != null) {
            getRecycler().addOnScrollListener(onScrollListener);
        }
    }

    public abstract StickyHeaderViewHolder createStickyHeaderViewHolder(WidgetExpressionPickerAdapter widgetExpressionPickerAdapter);

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public View getAndBindHeaderView(int i) {
        MGRecyclerDataPayload mGRecyclerDataPayload = getInternalData().get(i);
        StickyHeadersManager stickyHeadersManager = this.stickyHeaderManager;
        if (stickyHeadersManager == null) {
            m.throwUninitializedPropertyAccessException("stickyHeaderManager");
        }
        stickyHeadersManager.getStickyHeaderHolder().bind(i, mGRecyclerDataPayload);
        StickyHeadersManager stickyHeadersManager2 = this.stickyHeaderManager;
        if (stickyHeadersManager2 == null) {
            m.throwUninitializedPropertyAccessException("stickyHeaderManager");
        }
        return stickyHeadersManager2.getCurrentStickyHeaderView();
    }

    public final List<Integer> getHeaderIndices() {
        List<Integer> list = this.headerIndices;
        if (list == null) {
            m.throwUninitializedPropertyAccessException("headerIndices");
        }
        return list;
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public Integer getHeaderPositionForItem(int i) {
        Integer num;
        boolean z2;
        List<Integer> list = this.headerIndices;
        if (list == null) {
            m.throwUninitializedPropertyAccessException("headerIndices");
        }
        ListIterator<Integer> listIterator = list.listIterator(list.size());
        while (true) {
            if (!listIterator.hasPrevious()) {
                num = null;
                break;
            }
            num = listIterator.previous();
            if (i >= num.intValue()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        Integer num2 = num;
        if (num2 == null) {
            AppLog appLog = AppLog.g;
            StringBuilder R = a.R("failed to find header position for item in ");
            R.append(getClass().getName());
            Logger.w$default(appLog, R.toString(), null, 2, null);
        }
        return num2;
    }

    public final Set<Integer> getHeaderTypes() {
        return this.headerTypes;
    }

    public final ExpressionPickerItemDecoration getItemDecoration() {
        return this.itemDecoration;
    }

    public abstract GridLayoutManager getLayoutManager();

    public abstract int getNumColumns();

    public final RecyclerView.OnScrollListener getOnScrollListener() {
        return this.onScrollListener;
    }

    public final Function1<Integer, Unit> getOnScrollPositionListener() {
        return this.onScrollPositionListener;
    }

    public final StickyHeadersManager getStickyHeaderManager() {
        StickyHeadersManager stickyHeadersManager = this.stickyHeaderManager;
        if (stickyHeadersManager == null) {
            m.throwUninitializedPropertyAccessException("stickyHeaderManager");
        }
        return stickyHeadersManager;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        super.onAttachedToRecyclerView(recyclerView);
        this.stickyHeaderManager = new StickyHeadersManager(this);
        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter$onAttachedToRecyclerView$1
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                WidgetExpressionPickerAdapter.this.getStickyHeaderManager().layoutViews(recyclerView);
            }
        });
    }

    public final void scrollToPosition(int i) {
        RecyclerView.OnScrollListener onScrollListener = this.onScrollListener;
        if (onScrollListener != null) {
            getRecycler().removeOnScrollListener(onScrollListener);
        }
        getLayoutManager().scrollToPositionWithOffset(i, 0);
        RecyclerView.OnScrollListener onScrollListener2 = this.onScrollListener;
        if (onScrollListener2 != null) {
            getRecycler().addOnScrollListener(onScrollListener2);
        }
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple
    public void setData(List<? extends MGRecyclerDataPayload> list) {
        m.checkNotNullParameter(list, "data");
        super.setData(list);
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (Object obj : list) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            Integer valueOf = this.headerTypes.contains(Integer.valueOf(((MGRecyclerDataPayload) obj).getType())) ? Integer.valueOf(i) : null;
            if (valueOf != null) {
                arrayList.add(valueOf);
            }
        }
        this.headerIndices = arrayList;
        ExpressionPickerItemDecoration expressionPickerItemDecoration = this.itemDecoration;
        if (expressionPickerItemDecoration != null) {
            getRecycler().removeItemDecoration(expressionPickerItemDecoration);
        }
        int numColumns = getNumColumns();
        List<Integer> list2 = this.headerIndices;
        if (list2 == null) {
            m.throwUninitializedPropertyAccessException("headerIndices");
        }
        ExpressionPickerItemDecoration expressionPickerItemDecoration2 = new ExpressionPickerItemDecoration(numColumns, list2, DimenUtils.dpToPixels(8));
        getRecycler().addItemDecoration(expressionPickerItemDecoration2);
        this.itemDecoration = expressionPickerItemDecoration2;
    }

    public final void setHeaderIndices(List<Integer> list) {
        m.checkNotNullParameter(list, "<set-?>");
        this.headerIndices = list;
    }

    public final void setItemDecoration(ExpressionPickerItemDecoration expressionPickerItemDecoration) {
        this.itemDecoration = expressionPickerItemDecoration;
    }

    public final void setOnScrollListener(RecyclerView.OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    public final void setOnScrollPositionListener(Function1<? super Integer, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onScrollPositionListener = function1;
    }

    public final void setStickyHeaderManager(StickyHeadersManager stickyHeadersManager) {
        m.checkNotNullParameter(stickyHeadersManager, "<set-?>");
        this.stickyHeaderManager = stickyHeadersManager;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetExpressionPickerAdapter(RecyclerView recyclerView, Set<Integer> set) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
        m.checkNotNullParameter(set, "headerTypes");
        this.headerTypes = set;
        this.onScrollPositionListener = WidgetExpressionPickerAdapter$onScrollPositionListener$1.INSTANCE;
        setupScrollObservables();
    }
}
