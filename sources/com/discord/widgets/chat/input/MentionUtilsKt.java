package com.discord.widgets.chat.input;

import com.swift.sandhook.annotation.MethodReflectParams;
import d0.g0.a;
import d0.t.n0;
import d0.z.d.m;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
/* compiled from: MentionUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\f\n\u0002\b\u0007\n\u0002\u0010\"\n\u0002\b\u0007\u001a+\u0010\u0006\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0012\u0004\u0012\u00020\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007\u001a#\u0010\u000b\u001a\u00020\u0002*\u00020\u00002\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\f\"\u0016\u0010\r\u001a\u00020\b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u000e\"\u0016\u0010\u000f\u001a\u00020\b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e\"\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\b0\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014\"\u0016\u0010\u0015\u001a\u00020\b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0015\u0010\u000e\"\u0016\u0010\u0016\u001a\u00020\b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0016\u0010\u000e¨\u0006\u0017"}, d2 = {"", "content", "", "cursorPosition", "Lkotlin/Pair;", "", "getSelectedToken", "(Ljava/lang/CharSequence;I)Lkotlin/Pair;", "", MethodReflectParams.CHAR, "endPosition", "reverseIndexOf", "(Ljava/lang/CharSequence;CI)I", "CHANNELS_CHAR", "C", "MENTIONS_CHAR", "", "DEFAULT_LEADING_IDENTIFIERS", "Ljava/util/Set;", "getDEFAULT_LEADING_IDENTIFIERS", "()Ljava/util/Set;", "SLASH_CHAR", "EMOJIS_AND_STICKERS_CHAR", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MentionUtilsKt {
    public static final char MENTIONS_CHAR = '@';
    public static final char EMOJIS_AND_STICKERS_CHAR = ':';
    public static final char CHANNELS_CHAR = '#';
    public static final char SLASH_CHAR = '/';
    private static final Set<Character> DEFAULT_LEADING_IDENTIFIERS = n0.hashSetOf(Character.valueOf(MENTIONS_CHAR), Character.valueOf(EMOJIS_AND_STICKERS_CHAR), Character.valueOf(CHANNELS_CHAR), Character.valueOf(SLASH_CHAR));

    public static final Set<Character> getDEFAULT_LEADING_IDENTIFIERS() {
        return DEFAULT_LEADING_IDENTIFIERS;
    }

    public static final Pair<String, Integer> getSelectedToken(CharSequence charSequence, int i) {
        m.checkNotNullParameter(charSequence, "content");
        boolean z2 = false;
        if (charSequence.length() == 0) {
            return new Pair<>(null, -1);
        }
        boolean z3 = i == charSequence.length();
        boolean z4 = i == 0 || a.isWhitespace(charSequence.charAt(i + (-1)));
        if (z3 && z4) {
            return new Pair<>(null, -1);
        }
        int max = Math.max(reverseIndexOf(charSequence, ' ', i), reverseIndexOf(charSequence, '\n', i)) + 1;
        String obj = charSequence.subSequence(max, i).toString();
        if (obj.length() > 0) {
            z2 = true;
        }
        if (z2) {
            return new Pair<>(obj, Integer.valueOf(max));
        }
        return new Pair<>(null, -1);
    }

    private static final int reverseIndexOf(CharSequence charSequence, char c, int i) {
        for (int i2 = i - 1; i2 >= 0; i2--) {
            if (charSequence.charAt(i2) == c) {
                return i2;
            }
        }
        return -1;
    }
}
