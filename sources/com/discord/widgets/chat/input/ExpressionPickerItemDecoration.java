package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import kotlin.Metadata;
/* compiled from: ExpressionPickerItemDecoration.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u0013\u0012\u0006\u0010\u0018\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0019\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J/\u0010\u0011\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0017¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/chat/input/ExpressionPickerItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "", "itemPosition", "getNextHeaderPositionForItem", "(I)Ljava/lang/Integer;", "getHeaderPositionForItem", "(I)I", "Landroid/graphics/Rect;", "outRect", "Landroid/view/View;", "view", "Landroidx/recyclerview/widget/RecyclerView;", "parent", "Landroidx/recyclerview/widget/RecyclerView$State;", "state", "", "getItemOffsets", "(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V", "", "headerIndices", "Ljava/util/List;", "numOfColumns", "I", "bottomPaddingForLastRow", HookHelper.constructorName, "(ILjava/util/List;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ExpressionPickerItemDecoration extends RecyclerView.ItemDecoration {
    private final int bottomPaddingForLastRow;
    private final List<Integer> headerIndices;
    private final int numOfColumns;

    public ExpressionPickerItemDecoration(int i, List<Integer> list, int i2) {
        m.checkNotNullParameter(list, "headerIndices");
        this.numOfColumns = i;
        this.headerIndices = list;
        this.bottomPaddingForLastRow = i2;
    }

    private final int getHeaderPositionForItem(int i) {
        Integer num;
        boolean z2;
        List<Integer> list = this.headerIndices;
        ListIterator<Integer> listIterator = list.listIterator(list.size());
        while (true) {
            if (!listIterator.hasPrevious()) {
                num = null;
                break;
            }
            num = listIterator.previous();
            if (i >= num.intValue()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        Integer num2 = num;
        if (num2 != null) {
            return num2.intValue();
        }
        return 0;
    }

    private final Integer getNextHeaderPositionForItem(int i) {
        Object obj;
        boolean z2;
        Iterator<T> it = this.headerIndices.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (i <= ((Number) obj).intValue()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        return (Integer) obj;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        m.checkNotNullParameter(rect, "outRect");
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(recyclerView, "parent");
        m.checkNotNullParameter(state, "state");
        super.getItemOffsets(rect, view, recyclerView, state);
        if (!this.headerIndices.isEmpty()) {
            int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
            if (!this.headerIndices.contains(Integer.valueOf(childAdapterPosition))) {
                int headerPositionForItem = getHeaderPositionForItem(childAdapterPosition);
                Integer nextHeaderPositionForItem = getNextHeaderPositionForItem(childAdapterPosition);
                int intValue = nextHeaderPositionForItem != null ? nextHeaderPositionForItem.intValue() : state.getItemCount();
                boolean z2 = true;
                int i = (intValue - headerPositionForItem) - 1;
                int i2 = (childAdapterPosition - headerPositionForItem) - 1;
                int i3 = this.numOfColumns;
                int i4 = 0;
                if (i2 < ((i / i3) - (i % i3 == 0 ? 1 : 0)) * i3) {
                    z2 = false;
                }
                if (z2) {
                    i4 = this.bottomPaddingForLastRow;
                }
                rect.bottom = i4;
            }
        }
    }
}
