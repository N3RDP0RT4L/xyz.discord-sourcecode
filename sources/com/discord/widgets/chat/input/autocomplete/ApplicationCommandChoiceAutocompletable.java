package com.discord.widgets.chat.input.autocomplete;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.commands.CommandChoice;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Autocompletable.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\b¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0015\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005H\u0016¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\u000b\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u0018\u0010\nR\u001f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u0007¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/ApplicationCommandChoiceAutocompletable;", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "", "getInputReplacement", "()Ljava/lang/String;", "", "getInputTextMatchers", "()Ljava/util/List;", "Lcom/discord/api/commands/CommandChoice;", "component1", "()Lcom/discord/api/commands/CommandChoice;", "choice", "copy", "(Lcom/discord/api/commands/CommandChoice;)Lcom/discord/widgets/chat/input/autocomplete/ApplicationCommandChoiceAutocompletable;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/commands/CommandChoice;", "getChoice", "textMatchers", "Ljava/util/List;", "getTextMatchers", HookHelper.constructorName, "(Lcom/discord/api/commands/CommandChoice;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ApplicationCommandChoiceAutocompletable extends Autocompletable {
    private final CommandChoice choice;
    private final List<String> textMatchers;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ApplicationCommandChoiceAutocompletable(CommandChoice commandChoice) {
        super(null);
        m.checkNotNullParameter(commandChoice, "choice");
        this.choice = commandChoice;
        this.textMatchers = d0.t.m.listOf(commandChoice.a());
    }

    public static /* synthetic */ ApplicationCommandChoiceAutocompletable copy$default(ApplicationCommandChoiceAutocompletable applicationCommandChoiceAutocompletable, CommandChoice commandChoice, int i, Object obj) {
        if ((i & 1) != 0) {
            commandChoice = applicationCommandChoiceAutocompletable.choice;
        }
        return applicationCommandChoiceAutocompletable.copy(commandChoice);
    }

    public final CommandChoice component1() {
        return this.choice;
    }

    public final ApplicationCommandChoiceAutocompletable copy(CommandChoice commandChoice) {
        m.checkNotNullParameter(commandChoice, "choice");
        return new ApplicationCommandChoiceAutocompletable(commandChoice);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof ApplicationCommandChoiceAutocompletable) && m.areEqual(this.choice, ((ApplicationCommandChoiceAutocompletable) obj).choice);
        }
        return true;
    }

    public final CommandChoice getChoice() {
        return this.choice;
    }

    @Override // com.discord.widgets.chat.input.autocomplete.Autocompletable
    public String getInputReplacement() {
        return this.choice.b();
    }

    @Override // com.discord.widgets.chat.input.autocomplete.Autocompletable
    public List<String> getInputTextMatchers() {
        return this.textMatchers;
    }

    public final List<String> getTextMatchers() {
        return this.textMatchers;
    }

    public int hashCode() {
        CommandChoice commandChoice = this.choice;
        if (commandChoice != null) {
            return commandChoice.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommandChoiceAutocompletable(choice=");
        R.append(this.choice);
        R.append(")");
        return R.toString();
    }
}
