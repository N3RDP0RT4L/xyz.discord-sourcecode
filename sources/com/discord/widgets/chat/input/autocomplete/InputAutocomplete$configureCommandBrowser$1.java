package com.discord.widgets.chat.input.autocomplete;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: InputAutocomplete.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "i", "i2", "", "invoke", "(II)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InputAutocomplete$configureCommandBrowser$1 extends o implements Function2<Integer, Integer, Unit> {
    public final /* synthetic */ InputAutocomplete this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InputAutocomplete$configureCommandBrowser$1(InputAutocomplete inputAutocomplete) {
        super(2);
        this.this$0 = inputAutocomplete;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Integer num, Integer num2) {
        invoke(num.intValue(), num2.intValue());
        return Unit.a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:3:0x0002, code lost:
        r0 = r3.this$0.getCurrentlySelectedCategory(r4);
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void invoke(int r4, int r5) {
        /*
            r3 = this;
            if (r4 < 0) goto L2c
            com.discord.widgets.chat.input.autocomplete.InputAutocomplete r0 = r3.this$0
            java.lang.Long r0 = com.discord.widgets.chat.input.autocomplete.InputAutocomplete.access$getCurrentlySelectedCategory(r0, r4)
            if (r0 == 0) goto L2c
            long r0 = r0.longValue()
            com.discord.widgets.chat.input.autocomplete.InputAutocomplete r2 = r3.this$0
            com.discord.widgets.chat.input.ChatInputApplicationsAdapter r2 = com.discord.widgets.chat.input.autocomplete.InputAutocomplete.access$getCategoriesAdapter$p(r2)
            r2.selectApplication(r0)
            com.discord.widgets.chat.input.autocomplete.InputAutocomplete r2 = r3.this$0
            com.discord.widgets.chat.input.ChatInputApplicationsAdapter r2 = com.discord.widgets.chat.input.autocomplete.InputAutocomplete.access$getCategoriesAdapter$p(r2)
            int r0 = r2.getPositionOfApplication(r0)
            if (r0 < 0) goto L2c
            com.discord.widgets.chat.input.autocomplete.InputAutocomplete r1 = r3.this$0
            androidx.recyclerview.widget.RecyclerView r1 = com.discord.widgets.chat.input.autocomplete.InputAutocomplete.access$getCommandBrowserAppsRecyclerView$p(r1)
            r1.smoothScrollToPosition(r0)
        L2c:
            com.discord.widgets.chat.input.autocomplete.InputAutocomplete r0 = r3.this$0
            com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel r0 = com.discord.widgets.chat.input.autocomplete.InputAutocomplete.access$getViewModel$p(r0)
            com.discord.widgets.chat.input.autocomplete.InputAutocomplete r1 = r3.this$0
            com.discord.widgets.chat.input.autocomplete.adapter.ChatInputAutocompleteAdapter r1 = com.discord.widgets.chat.input.autocomplete.InputAutocomplete.access$getAutocompleteAdapter$p(r1)
            int r1 = r1.getItemCount()
            r0.onCommandsBrowserScroll(r4, r5, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.autocomplete.InputAutocomplete$configureCommandBrowser$1.invoke(int, int):void");
    }
}
