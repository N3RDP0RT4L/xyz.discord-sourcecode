package com.discord.widgets.chat.input.autocomplete;

import andhook.lib.HookHelper;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.commands.Application;
import com.discord.utilities.user.UserUtils;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Comparator;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: Autocompletable.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u0003B\u0007¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/AutocompletableComparator;", "Ljava/util/Comparator;", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "Lkotlin/Comparator;", "o1", "o2", "", "compare", "(Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AutocompletableComparator implements Comparator<Autocompletable> {
    public int compare(Autocompletable autocompletable, Autocompletable autocompletable2) {
        String lower;
        String lower2;
        String lower3;
        String lower4;
        String lower5;
        String lower6;
        String lower7;
        String lower8;
        String lower9;
        String lower10;
        String lower11;
        String lower12;
        String lower13;
        String lower14;
        String lower15;
        String lower16;
        String lower17;
        String lower18;
        m.checkNotNullParameter(autocompletable, "o1");
        m.checkNotNullParameter(autocompletable2, "o2");
        if (!m.areEqual(a0.getOrCreateKotlinClass(autocompletable.getClass()), a0.getOrCreateKotlinClass(autocompletable2.getClass()))) {
            return m.compare(AutocompletableKt.getSortIndex(autocompletable), AutocompletableKt.getSortIndex(autocompletable2));
        }
        if (autocompletable instanceof ApplicationCommandChoiceAutocompletable) {
            lower17 = AutocompletableKt.lower(((ApplicationCommandChoiceAutocompletable) autocompletable).getChoice().a());
            lower18 = AutocompletableKt.lower(((ApplicationCommandChoiceAutocompletable) autocompletable2).getChoice().a());
            return lower17.compareTo(lower18);
        } else if (autocompletable instanceof ApplicationCommandLoadingPlaceholder) {
            return 0;
        } else {
            if (autocompletable instanceof ApplicationCommandAutocompletable) {
                ApplicationCommandAutocompletable applicationCommandAutocompletable = (ApplicationCommandAutocompletable) autocompletable2;
                ApplicationCommandAutocompletable applicationCommandAutocompletable2 = (ApplicationCommandAutocompletable) autocompletable;
                Application application = applicationCommandAutocompletable2.getApplication();
                String str = null;
                Long valueOf = application != null ? Long.valueOf(application.getId()) : null;
                Application application2 = applicationCommandAutocompletable.getApplication();
                if (m.areEqual(valueOf, application2 != null ? Long.valueOf(application2.getId()) : null)) {
                    lower15 = AutocompletableKt.lower(applicationCommandAutocompletable2.getCommand().getName());
                    lower16 = AutocompletableKt.lower(applicationCommandAutocompletable.getCommand().getName());
                    return lower15.compareTo(lower16);
                }
                Application application3 = applicationCommandAutocompletable2.getApplication();
                lower13 = AutocompletableKt.lower(String.valueOf(application3 != null ? application3.getName() : null));
                Application application4 = applicationCommandAutocompletable.getApplication();
                if (application4 != null) {
                    str = application4.getName();
                }
                lower14 = AutocompletableKt.lower(String.valueOf(str));
                return lower13.compareTo(lower14);
            } else if (autocompletable instanceof ApplicationPlaceholder) {
                lower11 = AutocompletableKt.lower(((ApplicationPlaceholder) autocompletable).getApplication().getName());
                lower12 = AutocompletableKt.lower(((ApplicationPlaceholder) autocompletable2).getApplication().getName());
                return lower11.compareTo(lower12);
            } else if (autocompletable instanceof ChannelAutocompletable) {
                ChannelAutocompletable channelAutocompletable = (ChannelAutocompletable) autocompletable2;
                ChannelAutocompletable channelAutocompletable2 = (ChannelAutocompletable) autocompletable;
                lower9 = AutocompletableKt.lower(ChannelUtils.c(channelAutocompletable2.getChannel()));
                lower10 = AutocompletableKt.lower(ChannelUtils.c(channelAutocompletable.getChannel()));
                if (!m.areEqual(lower9, lower10)) {
                    return lower9.compareTo(lower10);
                }
                return (channelAutocompletable2.getChannel().h() > channelAutocompletable.getChannel().h() ? 1 : (channelAutocompletable2.getChannel().h() == channelAutocompletable.getChannel().h() ? 0 : -1));
            } else if (autocompletable instanceof EmojiAutocompletable) {
                String firstName = ((EmojiAutocompletable) autocompletable).getEmoji().getFirstName();
                m.checkNotNullExpressionValue(firstName, "o1.emoji.firstName");
                lower7 = AutocompletableKt.lower(firstName);
                String firstName2 = ((EmojiAutocompletable) autocompletable2).getEmoji().getFirstName();
                m.checkNotNullExpressionValue(firstName2, "o2c.emoji.firstName");
                lower8 = AutocompletableKt.lower(firstName2);
                return lower7.compareTo(lower8);
            } else if (autocompletable instanceof EmojiUpsellPlaceholder) {
                return 0;
            } else {
                if (autocompletable instanceof GlobalRoleAutocompletable) {
                    lower5 = AutocompletableKt.lower(((GlobalRoleAutocompletable) autocompletable).getText());
                    lower6 = AutocompletableKt.lower(((GlobalRoleAutocompletable) autocompletable2).getText());
                    return lower5.compareTo(lower6);
                } else if (autocompletable instanceof RoleAutocompletable) {
                    lower3 = AutocompletableKt.lower(((RoleAutocompletable) autocompletable).getRole().g());
                    lower4 = AutocompletableKt.lower(((RoleAutocompletable) autocompletable2).getRole().g());
                    return lower3.compareTo(lower4);
                } else if (autocompletable instanceof UserAutocompletable) {
                    UserAutocompletable userAutocompletable = (UserAutocompletable) autocompletable;
                    String nickname = userAutocompletable.getNickname();
                    if (nickname == null) {
                        nickname = UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, userAutocompletable.getUser(), null, null, 3, null).toString();
                    }
                    UserAutocompletable userAutocompletable2 = (UserAutocompletable) autocompletable2;
                    String nickname2 = userAutocompletable2.getNickname();
                    if (nickname2 == null) {
                        nickname2 = UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, userAutocompletable2.getUser(), null, null, 3, null).toString();
                    }
                    lower = AutocompletableKt.lower(nickname);
                    lower2 = AutocompletableKt.lower(nickname2);
                    return lower.compareTo(lower2);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
        }
    }
}
