package com.discord.widgets.chat.input.autocomplete;

import andhook.lib.HookHelper;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.commands.ApplicationCommandAutocompleteChoice;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.api.commands.CommandChoice;
import com.discord.api.sticker.Sticker;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.models.commands.ApplicationSubCommand;
import com.discord.stores.CommandAutocompleteState;
import com.discord.utilities.stickers.StickerUtils;
import com.discord.utilities.string.StringUtilsKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.input.autocomplete.sources.ApplicationCommandsAutocompletableSource;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.discord.widgets.chat.input.models.ApplicationCommandValue;
import com.discord.widgets.chat.input.models.AutocompleteInputSelectionModel;
import com.discord.widgets.chat.input.models.ChatInputMentionsMap;
import com.discord.widgets.chat.input.models.CommandOptionValue;
import com.discord.widgets.chat.input.models.InputSelectionModel;
import com.discord.widgets.chat.input.models.InputSelectionModelKt;
import com.discord.widgets.chat.input.models.MentionInputModel;
import com.discord.widgets.chat.input.models.MentionToken;
import com.discord.widgets.chat.input.models.OptionRange;
import com.discord.widgets.chat.input.models.StringOptionValue;
import d0.g0.t;
import d0.g0.w;
import d0.t.g0;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.ranges.IntRange;
/* compiled from: AutocompleteModelUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\bG\u0010HJ\u001b\u0010\u0005\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J)\u0010\u000b\u001a\u0004\u0018\u00010\n*\u0004\u0018\u00010\u00022\u0006\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\t\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001b\u0010\u000e\u001a\u00020\u0004*\u00020\r2\u0006\u0010\u0003\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ=\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0010\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u00112\u0018\u0010\u0017\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\u0013¢\u0006\u0004\b\u0018\u0010\u0019JK\u0010 \u001a\u00020\u001f2\u0006\u0010\u001a\u001a\u00020\u00022\u0018\u0010\u001c\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u001b0\u00132\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00160\u00132\u0006\u0010\u001e\u001a\u00020\u0004¢\u0006\u0004\b \u0010!J!\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015¢\u0006\u0004\b\"\u0010#J'\u0010%\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\u00132\u0006\u0010$\u001a\u00020\u0011¢\u0006\u0004\b%\u0010&JW\u00100\u001a\u0004\u0018\u00010\n2\u0006\u0010\u001a\u001a\u00020'2\u0006\u0010(\u001a\u00020\r2\u0006\u0010*\u001a\u00020)2\u0006\u0010+\u001a\u00020\u00042\u0012\u0010-\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020,0\u00132\u0012\u0010/\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020.0\u0013¢\u0006\u0004\b0\u00101J\u001f\u00102\u001a\u0004\u0018\u00010\n2\u0006\u0010\u001a\u001a\u00020'2\u0006\u0010(\u001a\u00020\r¢\u0006\u0004\b2\u00103JA\u0010;\u001a\u0004\u0018\u00010:2\b\u0010\u0012\u001a\u0004\u0018\u0001042\n\b\u0002\u00105\u001a\u0004\u0018\u00010)2\f\u00107\u001a\b\u0012\u0004\u0012\u0002060\u00152\f\u00109\u001a\b\u0012\u0004\u0012\u0002080\u0015¢\u0006\u0004\b;\u0010<J\u001d\u0010>\u001a\b\u0012\u0004\u0012\u00020=0\u00152\b\u0010\u0010\u001a\u0004\u0018\u00010\n¢\u0006\u0004\b>\u0010?J\u0013\u0010@\u001a\u00020\u0004*\u0004\u0018\u00010\u0002¢\u0006\u0004\b@\u0010AJ+\u0010E\u001a\u0004\u0018\u00010\r*\u00020\r2\u0006\u0010B\u001a\u00020\u00072\u0006\u0010C\u001a\u00020\u00072\u0006\u0010D\u001a\u00020\u0007¢\u0006\u0004\bE\u0010F¨\u0006I"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/AutocompleteModelUtils;", "", "", "other", "", "lowerCaseContains", "(Ljava/lang/String;Ljava/lang/String;)Z", "", "tokenPosition", "useNullLeadingIdentifier", "Lcom/discord/widgets/chat/input/models/MentionToken;", "asMentionToken", "(Ljava/lang/String;IZ)Lcom/discord/widgets/chat/input/models/MentionToken;", "Lkotlin/ranges/IntRange;", "isSubRangeOf", "(Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;)Z", "token", "Lcom/discord/widgets/chat/input/models/InputSelectionModel;", "inputSelectionModel", "", "Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "mentions", "filterMentionsFromToken", "(Lcom/discord/widgets/chat/input/models/MentionToken;Lcom/discord/widgets/chat/input/models/InputSelectionModel;Ljava/util/Map;)Ljava/util/List;", "input", "", "autocompletables", "currentInputMentionMap", "isCommand", "Lcom/discord/widgets/chat/input/models/ChatInputMentionsMap;", "mapInputToMentions", "(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Z)Lcom/discord/widgets/chat/input/models/ChatInputMentionsMap;", "filterAutocompletablesForMessageContext", "(Ljava/util/List;)Ljava/util/List;", "model", "filterAutocompletablesForInputContext", "(Lcom/discord/widgets/chat/input/models/InputSelectionModel;)Ljava/util/Map;", "", "selection", "Lcom/discord/models/commands/ApplicationCommandOption;", "selectedCommandOption", "hasSelectedFreeformInputOption", "Lcom/discord/widgets/chat/input/models/OptionRange;", "inputCommandOptionsRanges", "Lcom/discord/widgets/chat/input/models/CommandOptionValue;", "inputCommandOptionValues", "getCommandAutocompleteToken", "(Ljava/lang/CharSequence;Lkotlin/ranges/IntRange;Lcom/discord/models/commands/ApplicationCommandOption;ZLjava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/chat/input/models/MentionToken;", "getMessageAutocompleteToken", "(Ljava/lang/CharSequence;Lkotlin/ranges/IntRange;)Lcom/discord/widgets/chat/input/models/MentionToken;", "Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;", "focusedOption", "Lcom/discord/models/commands/Application;", "applications", "Lcom/discord/models/commands/ApplicationCommand;", "queryCommands", "Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "getApplicationSendData", "(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;Lcom/discord/models/commands/ApplicationCommandOption;Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "Lcom/discord/api/sticker/Sticker;", "getStickerMatches", "(Lcom/discord/widgets/chat/input/models/MentionToken;)Ljava/util/List;", "isBoolean", "(Ljava/lang/String;)Z", "start", "before", "count", "shiftOrRemove", "(Lkotlin/ranges/IntRange;III)Lkotlin/ranges/IntRange;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AutocompleteModelUtils {
    public static final AutocompleteModelUtils INSTANCE = new AutocompleteModelUtils();

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ApplicationCommandType.values();
            int[] iArr = new int[12];
            $EnumSwitchMapping$0 = iArr;
            iArr[ApplicationCommandType.CHANNEL.ordinal()] = 1;
            iArr[ApplicationCommandType.USER.ordinal()] = 2;
            iArr[ApplicationCommandType.ROLE.ordinal()] = 3;
            iArr[ApplicationCommandType.MENTIONABLE.ordinal()] = 4;
            iArr[ApplicationCommandType.BOOLEAN.ordinal()] = 5;
        }
    }

    private AutocompleteModelUtils() {
    }

    private final MentionToken asMentionToken(String str, int i, boolean z2) {
        Character ch;
        if (str != null) {
            boolean z3 = true;
            if (!(!t.isBlank(str)) || !MentionUtilsKt.getDEFAULT_LEADING_IDENTIFIERS().contains(Character.valueOf(str.charAt(0)))) {
                ch = null;
            } else {
                ch = Character.valueOf(str.charAt(0));
            }
            if (ch != null || z2) {
                LeadingIdentifier fromChar = LeadingIdentifier.Companion.fromChar(ch);
                if (i != 0) {
                    z3 = false;
                }
                return new MentionToken(fromChar, str, z3, i);
            }
        }
        return null;
    }

    public static /* synthetic */ MentionToken asMentionToken$default(AutocompleteModelUtils autocompleteModelUtils, String str, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        return autocompleteModelUtils.asMentionToken(str, i, z2);
    }

    public static /* synthetic */ ApplicationCommandData getApplicationSendData$default(AutocompleteModelUtils autocompleteModelUtils, AutocompleteInputSelectionModel autocompleteInputSelectionModel, ApplicationCommandOption applicationCommandOption, List list, List list2, int i, Object obj) {
        if ((i & 2) != 0) {
            applicationCommandOption = null;
        }
        return autocompleteModelUtils.getApplicationSendData(autocompleteInputSelectionModel, applicationCommandOption, list, list2);
    }

    private final boolean isSubRangeOf(IntRange intRange, IntRange intRange2) {
        return !intRange.equals(intRange2) && intRange2.contains(intRange.getFirst()) && intRange2.contains(intRange.getLast());
    }

    private final boolean lowerCaseContains(String str, String str2) {
        Locale locale = Locale.getDefault();
        m.checkNotNullExpressionValue(locale, "Locale.getDefault()");
        Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = str.toLowerCase(locale);
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        Locale locale2 = Locale.getDefault();
        m.checkNotNullExpressionValue(locale2, "Locale.getDefault()");
        Objects.requireNonNull(str2, "null cannot be cast to non-null type java.lang.String");
        String lowerCase2 = str2.toLowerCase(locale2);
        m.checkNotNullExpressionValue(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
        return w.contains$default((CharSequence) lowerCase, (CharSequence) lowerCase2, false, 2, (Object) null);
    }

    public final Map<LeadingIdentifier, List<Autocompletable>> filterAutocompletablesForInputContext(InputSelectionModel inputSelectionModel) {
        List list;
        List list2;
        boolean z2;
        m.checkNotNullParameter(inputSelectionModel, "model");
        List<? extends Autocompletable> flatten = o.flatten(inputSelectionModel.getInputModel().getAutocompletables().values());
        if (inputSelectionModel instanceof InputSelectionModel.MessageInputSelectionModel) {
            List<Autocompletable> filterAutocompletablesForMessageContext = filterAutocompletablesForMessageContext(flatten);
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Object obj : filterAutocompletablesForMessageContext) {
                LeadingIdentifier leadingIdentifier = ((Autocompletable) obj).leadingIdentifier();
                Object obj2 = linkedHashMap.get(leadingIdentifier);
                if (obj2 == null) {
                    obj2 = new ArrayList();
                    linkedHashMap.put(leadingIdentifier, obj2);
                }
                ((List) obj2).add(obj);
            }
            return linkedHashMap;
        } else if (inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel) {
            InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
            ApplicationCommandOption selectedCommandOption = commandInputSelectionModel.getSelectedCommandOption();
            if (selectedCommandOption != null) {
                List<CommandChoice> choices = selectedCommandOption.getChoices();
                boolean z3 = true;
                if (choices == null || !(!choices.isEmpty())) {
                    switch (selectedCommandOption.getType().ordinal()) {
                        case 4:
                            list = ApplicationCommandsAutocompletableSource.Companion.createFromCommandOption(selectedCommandOption);
                            break;
                        case 5:
                            list = new ArrayList();
                            for (Object obj3 : flatten) {
                                if (obj3 instanceof UserAutocompletable) {
                                    list.add(obj3);
                                }
                            }
                            break;
                        case 6:
                            list = new ArrayList();
                            for (Object obj4 : flatten) {
                                if (obj4 instanceof ChannelAutocompletable) {
                                    list.add(obj4);
                                }
                            }
                            List<Integer> channelTypes = selectedCommandOption.getChannelTypes();
                            if (channelTypes != null && !channelTypes.isEmpty()) {
                                z3 = false;
                            }
                            if (!z3) {
                                list2 = new ArrayList();
                                for (Object obj5 : list) {
                                    if (selectedCommandOption.getChannelTypes().contains(Integer.valueOf(((ChannelAutocompletable) obj5).getChannel().A()))) {
                                        list2.add(obj5);
                                    }
                                }
                                list = list2;
                                break;
                            }
                            break;
                        case 7:
                            list = new ArrayList();
                            for (Object obj6 : flatten) {
                                if (obj6 instanceof RoleAutocompletable) {
                                    list.add(obj6);
                                }
                            }
                            break;
                        case 8:
                            list = new ArrayList();
                            for (Object obj7 : flatten) {
                                Autocompletable autocompletable = (Autocompletable) obj7;
                                if ((autocompletable instanceof UserAutocompletable) || (autocompletable instanceof RoleAutocompletable)) {
                                    list.add(obj7);
                                }
                            }
                            break;
                        default:
                            if (selectedCommandOption.getAutocomplete()) {
                                Map<String, CommandAutocompleteState> map = commandInputSelectionModel.getInputModel().getCommandOptionAutocompleteItems().get(selectedCommandOption.getName());
                                CommandAutocompleteState commandAutocompleteState = null;
                                if (map != null) {
                                    CommandOptionValue commandOptionValue = commandInputSelectionModel.getInputModel().getInputCommandOptionValues().get(selectedCommandOption);
                                    if (commandOptionValue != null) {
                                        commandAutocompleteState = commandOptionValue.getValue();
                                    }
                                    commandAutocompleteState = map.get(String.valueOf(commandAutocompleteState));
                                }
                                if (!(commandAutocompleteState instanceof CommandAutocompleteState.Choices)) {
                                    list = n.emptyList();
                                    break;
                                } else {
                                    List<ApplicationCommandAutocompleteChoice> choices2 = ((CommandAutocompleteState.Choices) commandAutocompleteState).getChoices();
                                    list2 = new ArrayList(o.collectionSizeOrDefault(choices2, 10));
                                    for (ApplicationCommandAutocompleteChoice applicationCommandAutocompleteChoice : choices2) {
                                        list2.add(new ApplicationCommandChoiceAutocompletable(new CommandChoice(applicationCommandAutocompleteChoice.a(), applicationCommandAutocompleteChoice.b())));
                                    }
                                }
                            } else {
                                List plus = u.plus((Collection) ApplicationCommandsAutocompletableSource.Companion.createFromCommandOption(selectedCommandOption), (Iterable) flatten);
                                list2 = new ArrayList();
                                for (Object obj8 : plus) {
                                    Autocompletable autocompletable2 = (Autocompletable) obj8;
                                    if (autocompletable2 instanceof ChannelAutocompletable) {
                                        z2 = ChannelUtils.s(((ChannelAutocompletable) autocompletable2).getChannel());
                                    } else if (autocompletable2 instanceof UserAutocompletable) {
                                        z2 = ((UserAutocompletable) autocompletable2).getCanUserReadChannel();
                                    } else {
                                        z2 = !(autocompletable2 instanceof ApplicationCommandAutocompletable);
                                    }
                                    if (z2) {
                                        list2.add(obj8);
                                    }
                                }
                            }
                            list = list2;
                            break;
                    }
                    LinkedHashMap linkedHashMap2 = new LinkedHashMap();
                    for (Object obj9 : list) {
                        LeadingIdentifier leadingIdentifier2 = ((Autocompletable) obj9).leadingIdentifier();
                        Object obj10 = linkedHashMap2.get(leadingIdentifier2);
                        if (obj10 == null) {
                            obj10 = new ArrayList();
                            linkedHashMap2.put(leadingIdentifier2, obj10);
                        }
                        ((List) obj10).add(obj9);
                    }
                    return linkedHashMap2;
                }
                List<Autocompletable> createFromCommandOption = ApplicationCommandsAutocompletableSource.Companion.createFromCommandOption(selectedCommandOption);
                LinkedHashMap linkedHashMap3 = new LinkedHashMap();
                for (Object obj11 : createFromCommandOption) {
                    LeadingIdentifier leadingIdentifier3 = ((Autocompletable) obj11).leadingIdentifier();
                    Object obj12 = linkedHashMap3.get(leadingIdentifier3);
                    if (obj12 == null) {
                        obj12 = new ArrayList();
                        linkedHashMap3.put(leadingIdentifier3, obj12);
                    }
                    ((List) obj12).add(obj11);
                }
                return linkedHashMap3;
            }
            ArrayList arrayList = new ArrayList();
            for (Object obj13 : flatten) {
                if (!(((Autocompletable) obj13) instanceof ApplicationCommandAutocompletable)) {
                    arrayList.add(obj13);
                }
            }
            LinkedHashMap linkedHashMap4 = new LinkedHashMap();
            for (Object obj14 : arrayList) {
                LeadingIdentifier leadingIdentifier4 = ((Autocompletable) obj14).leadingIdentifier();
                Object obj15 = linkedHashMap4.get(leadingIdentifier4);
                if (obj15 == null) {
                    obj15 = new ArrayList();
                    linkedHashMap4.put(leadingIdentifier4, obj15);
                }
                ((List) obj15).add(obj14);
            }
            return linkedHashMap4;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final List<Autocompletable> filterAutocompletablesForMessageContext(List<? extends Autocompletable> list) {
        boolean z2;
        m.checkNotNullParameter(list, "autocompletables");
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            Autocompletable autocompletable = (Autocompletable) obj;
            if (autocompletable instanceof ChannelAutocompletable) {
                z2 = ChannelUtils.s(((ChannelAutocompletable) autocompletable).getChannel());
            } else if (autocompletable instanceof UserAutocompletable) {
                z2 = ((UserAutocompletable) autocompletable).getCanUserReadChannel();
            } else {
                z2 = autocompletable instanceof RoleAutocompletable ? ((RoleAutocompletable) autocompletable).getCanMention() : true;
            }
            if (z2) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public final List<Autocompletable> filterMentionsFromToken(MentionToken mentionToken, InputSelectionModel inputSelectionModel, Map<LeadingIdentifier, ? extends List<? extends Autocompletable>> map) {
        ArrayList arrayList;
        ApplicationCommandOption selectedCommandOption;
        m.checkNotNullParameter(mentionToken, "token");
        m.checkNotNullParameter(inputSelectionModel, "inputSelectionModel");
        m.checkNotNullParameter(map, "mentions");
        boolean z2 = true;
        if ((inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel) && (selectedCommandOption = ((InputSelectionModel.CommandInputSelectionModel) inputSelectionModel).getSelectedCommandOption()) != null && selectedCommandOption.getAutocomplete()) {
            return InputAutocompletablesKt.flatten(map);
        }
        if (!t.isBlank(mentionToken.getFormattedToken()) || mentionToken.getLeadingIdentifier() != LeadingIdentifier.NONE) {
            z2 = false;
        }
        String formattedToken = mentionToken.getFormattedToken();
        Locale locale = Locale.getDefault();
        m.checkNotNullExpressionValue(locale, "Locale.getDefault()");
        Objects.requireNonNull(formattedToken, "null cannot be cast to non-null type java.lang.String");
        String lowerCase = formattedToken.toLowerCase(locale);
        m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
        String replace$default = t.replace$default(StringUtilsKt.stripAccents(lowerCase), " ", "", false, 4, (Object) null);
        if (z2 && InputSelectionModelKt.hasSelectedCommandOptionWithChoices(inputSelectionModel)) {
            List<? extends Autocompletable> list = map.get(LeadingIdentifier.NONE);
            if (list != null) {
                arrayList = new ArrayList();
                for (Object obj : list) {
                    if (obj instanceof ApplicationCommandChoiceAutocompletable) {
                        arrayList.add(obj);
                    }
                }
            } else {
                arrayList = null;
            }
            return arrayList != null ? arrayList : n.emptyList();
        } else if (z2) {
            return n.emptyList();
        } else {
            ArrayList arrayList2 = new ArrayList();
            List<? extends Autocompletable> list2 = map.get(mentionToken.getLeadingIdentifier());
            if (list2 != null) {
                for (Autocompletable autocompletable : list2) {
                    Iterator<String> iterateAutocompleteMatchers = autocompletable.iterateAutocompleteMatchers();
                    while (true) {
                        if (iterateAutocompleteMatchers.hasNext()) {
                            String next = iterateAutocompleteMatchers.next();
                            Locale locale2 = Locale.getDefault();
                            m.checkNotNullExpressionValue(locale2, "Locale.getDefault()");
                            Objects.requireNonNull(next, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase2 = next.toLowerCase(locale2);
                            m.checkNotNullExpressionValue(lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
                            if (INSTANCE.lowerCaseContains(t.replace$default(StringUtilsKt.stripAccents(lowerCase2), " ", "", false, 4, (Object) null), replace$default)) {
                                arrayList2.add(autocompletable);
                                break;
                            }
                        }
                    }
                }
            }
            return arrayList2;
        }
    }

    public final ApplicationCommandData getApplicationSendData(AutocompleteInputSelectionModel autocompleteInputSelectionModel, ApplicationCommandOption applicationCommandOption, List<Application> list, List<? extends ApplicationCommand> list2) {
        boolean z2;
        ApplicationCommandValue applicationCommandValue;
        boolean z3;
        ApplicationCommandValue applicationCommandValue2;
        OptionRange optionRange;
        IntRange value;
        boolean z4;
        m.checkNotNullParameter(list, "applications");
        m.checkNotNullParameter(list2, "queryCommands");
        InputSelectionModel inputSelectionModel = autocompleteInputSelectionModel != null ? autocompleteInputSelectionModel.getInputSelectionModel() : null;
        if (!(inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel)) {
            return null;
        }
        InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
        ApplicationCommand selectedCommand = commandInputSelectionModel.getInputModel().getInputCommandContext().getSelectedCommand();
        Application selectedApplication = commandInputSelectionModel.getInputModel().getInputCommandContext().getSelectedApplication();
        MentionInputModel.VerifiedCommandInputModel inputModel = commandInputSelectionModel.getInputModel();
        if (!(inputModel instanceof MentionInputModel.VerifiedCommandInputModel)) {
            inputModel = null;
        }
        if (inputModel == null && selectedCommand != null && selectedApplication != null) {
            List emptyList = n.emptyList();
            List<ApplicationCommandOption> options = selectedCommand.getOptions();
            if (!(options instanceof Collection) || !options.isEmpty()) {
                for (ApplicationCommandOption applicationCommandOption2 : options) {
                    if (!(!applicationCommandOption2.getRequired())) {
                        z4 = false;
                        break;
                    }
                }
            }
            z4 = true;
            return new ApplicationCommandData(selectedApplication, selectedCommand, emptyList, z4);
        } else if (inputModel == null || selectedCommand == null || selectedApplication == null) {
            return null;
        } else {
            Map<ApplicationCommandOption, CommandOptionValue> inputCommandOptionValues = inputModel.getInputCommandOptionValues();
            LinkedHashMap linkedHashMap = new LinkedHashMap(g0.mapCapacity(inputCommandOptionValues.size()));
            Iterator<T> it = inputCommandOptionValues.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                Object value2 = ((CommandOptionValue) entry.getValue()).getValue();
                if (entry.getValue() instanceof StringOptionValue) {
                    List<CommandChoice> choices = ((ApplicationCommandOption) entry.getKey()).getChoices();
                    if (!(!(choices == null || choices.isEmpty()) || (optionRange = inputModel.getInputCommandOptionsRanges().get(entry.getKey())) == null || (value = optionRange.getValue()) == null)) {
                        int first = value.getFirst();
                        String obj = ((CommandOptionValue) entry.getValue()).getValue().toString();
                        Map<IntRange, Autocompletable> inputMentionsMap = inputModel.getInputMentionsMap();
                        LinkedHashMap linkedHashMap2 = new LinkedHashMap(g0.mapCapacity(inputMentionsMap.size()));
                        Iterator<T> it2 = inputMentionsMap.entrySet().iterator();
                        while (it2.hasNext()) {
                            Map.Entry entry2 = (Map.Entry) it2.next();
                            linkedHashMap2.put(new IntRange(((IntRange) entry2.getKey()).getFirst() - first, ((IntRange) entry2.getKey()).getLast() - first), entry2.getValue());
                        }
                        LinkedHashMap linkedHashMap3 = new LinkedHashMap();
                        for (Map.Entry entry3 : linkedHashMap2.entrySet()) {
                            if (((IntRange) entry3.getKey()).getFirst() >= 0 && ((IntRange) entry3.getKey()).getLast() <= obj.length()) {
                                linkedHashMap3.put(entry3.getKey(), entry3.getValue());
                            }
                        }
                        value2 = AutocompleteExtensionsKt.replaceAutocompleteDataWithServerValues(obj, linkedHashMap3);
                    }
                }
                linkedHashMap.put(key, value2);
            }
            Map<ApplicationCommandOption, Boolean> inputCommandOptionValidity = inputModel.getInputCommandOptionValidity();
            if (selectedCommand instanceof ApplicationSubCommand) {
                Collection<Boolean> values = inputCommandOptionValidity.values();
                if (!(values instanceof Collection) || !values.isEmpty()) {
                    for (Boolean bool : values) {
                        if (!bool.booleanValue()) {
                            z3 = false;
                            break;
                        }
                    }
                }
                z3 = true;
                Set<ApplicationCommandOption> keySet = linkedHashMap.keySet();
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(keySet, 10));
                for (ApplicationCommandOption applicationCommandOption3 : keySet) {
                    boolean areEqual = m.areEqual(applicationCommandOption != null ? applicationCommandOption.getName() : null, applicationCommandOption3.getName());
                    boolean z5 = (applicationCommandOption3.getRequired() && linkedHashMap.get(applicationCommandOption3) == null && !applicationCommandOption3.getAutocomplete()) || m.areEqual(inputCommandOptionValidity.get(applicationCommandOption3), Boolean.FALSE);
                    z3 = z3 && !z5;
                    Object obj2 = !z5 ? linkedHashMap.get(applicationCommandOption3) : null;
                    if (obj2 != null || applicationCommandOption3.getAutocomplete()) {
                        applicationCommandValue2 = new ApplicationCommandValue(applicationCommandOption3.getName(), obj2 != null ? obj2 : "", applicationCommandOption3.getType().getType(), null, areEqual ? Boolean.TRUE : null, 8, null);
                    } else {
                        applicationCommandValue2 = null;
                    }
                    arrayList.add(applicationCommandValue2);
                }
                ApplicationSubCommand applicationSubCommand = (ApplicationSubCommand) selectedCommand;
                ApplicationCommandValue applicationCommandValue3 = new ApplicationCommandValue(applicationSubCommand.getSubCommandName(), null, ApplicationCommandType.SUBCOMMAND.getType(), u.filterNotNull(arrayList), null, 18, null);
                String parentGroupName = applicationSubCommand.getParentGroupName();
                if (parentGroupName != null) {
                    return new ApplicationCommandData(selectedApplication, applicationSubCommand.getRootCommand(), d0.t.m.listOf(new ApplicationCommandValue(parentGroupName, null, ApplicationCommandType.SUBCOMMAND_GROUP.getType(), d0.t.m.listOf(applicationCommandValue3), null, 18, null)), z3);
                }
                return new ApplicationCommandData(selectedApplication, applicationSubCommand.getRootCommand(), d0.t.m.listOf(applicationCommandValue3), z3);
            }
            Collection<Boolean> values2 = inputCommandOptionValidity.values();
            if (!(values2 instanceof Collection) || !values2.isEmpty()) {
                for (Boolean bool2 : values2) {
                    if (!bool2.booleanValue()) {
                        z2 = false;
                        break;
                    }
                }
            }
            z2 = true;
            Set<ApplicationCommandOption> keySet2 = linkedHashMap.keySet();
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(keySet2, 10));
            for (ApplicationCommandOption applicationCommandOption4 : keySet2) {
                boolean areEqual2 = m.areEqual(applicationCommandOption != null ? applicationCommandOption.getName() : null, applicationCommandOption4.getName());
                boolean z6 = (applicationCommandOption4.getRequired() && linkedHashMap.get(applicationCommandOption4) == null && !applicationCommandOption4.getAutocomplete()) || m.areEqual(inputCommandOptionValidity.get(applicationCommandOption4), Boolean.FALSE);
                z2 = z2 && !z6;
                Object obj3 = !z6 ? linkedHashMap.get(applicationCommandOption4) : null;
                if (obj3 != null || applicationCommandOption4.getAutocomplete()) {
                    applicationCommandValue = new ApplicationCommandValue(applicationCommandOption4.getName(), obj3 != null ? obj3 : "", applicationCommandOption4.getType().getType(), null, areEqual2 ? Boolean.TRUE : null, 8, null);
                } else {
                    applicationCommandValue = null;
                }
                arrayList2.add(applicationCommandValue);
            }
            return new ApplicationCommandData(selectedApplication, selectedCommand, u.filterNotNull(arrayList2), z2);
        }
    }

    public final MentionToken getCommandAutocompleteToken(CharSequence charSequence, IntRange intRange, ApplicationCommandOption applicationCommandOption, boolean z2, Map<ApplicationCommandOption, OptionRange> map, Map<ApplicationCommandOption, ? extends CommandOptionValue> map2) {
        String str;
        m.checkNotNullParameter(charSequence, "input");
        m.checkNotNullParameter(intRange, "selection");
        m.checkNotNullParameter(applicationCommandOption, "selectedCommandOption");
        m.checkNotNullParameter(map, "inputCommandOptionsRanges");
        m.checkNotNullParameter(map2, "inputCommandOptionValues");
        Pair<String, Integer> selectedToken = MentionUtilsKt.getSelectedToken(charSequence.toString(), intRange.getFirst());
        String component1 = selectedToken.component1();
        int intValue = selectedToken.component2().intValue();
        IntRange intRange2 = null;
        if (component1 != null) {
            str = w.removePrefix(component1, applicationCommandOption.getName() + ":");
        } else {
            str = null;
        }
        if (z2) {
            return asMentionToken$default(this, str, intValue, false, 2, null);
        }
        OptionRange optionRange = map.get(applicationCommandOption);
        if (optionRange != null) {
            intRange2 = optionRange.getValue();
        }
        CommandOptionValue commandOptionValue = map2.get(applicationCommandOption);
        if (!(intRange2 == null || commandOptionValue == null || intRange.getFirst() <= intRange2.getFirst())) {
            String obj = commandOptionValue.getValue().toString();
            int min = Math.min(intRange.getFirst() - intRange2.getFirst(), commandOptionValue.getValue().toString().length());
            Objects.requireNonNull(obj, "null cannot be cast to non-null type java.lang.String");
            str = obj.substring(0, min);
            m.checkNotNullExpressionValue(str, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        }
        if (str == null) {
            str = "";
        }
        return asMentionToken(str, intValue, true);
    }

    public final MentionToken getMessageAutocompleteToken(CharSequence charSequence, IntRange intRange) {
        m.checkNotNullParameter(charSequence, "input");
        m.checkNotNullParameter(intRange, "selection");
        Pair<String, Integer> selectedToken = MentionUtilsKt.getSelectedToken(charSequence.toString(), intRange.getFirst());
        String str = selectedToken.component1();
        int i = selectedToken.component2().intValue();
        Pattern compile = Pattern.compile("^(/([a-zA-Z0-9_-]+\\s*){1,3})(.|\\n)*");
        m.checkNotNullExpressionValue(compile, "Pattern.compile(\"^(/([a-…_-]+\\\\s*){1,3})(.|\\\\n)*\")");
        Matcher matcher = compile.matcher(charSequence);
        if (matcher.matches()) {
            str = matcher.group(0);
            i = 0;
        }
        return asMentionToken$default(this, str, i, false, 2, null);
    }

    public final List<Sticker> getStickerMatches(MentionToken mentionToken) {
        if ((mentionToken != null ? mentionToken.getLeadingIdentifier() : null) == LeadingIdentifier.EMOJI_AND_STICKERS) {
            String token = mentionToken.getToken();
            StringBuilder sb = new StringBuilder();
            int length = token.length();
            for (int i = 0; i < length; i++) {
                char charAt = token.charAt(i);
                if (Character.isLetterOrDigit(charAt)) {
                    sb.append(charAt);
                }
            }
            String sb2 = sb.toString();
            m.checkNotNullExpressionValue(sb2, "filterTo(StringBuilder(), predicate).toString()");
            if (sb2.length() - 1 >= 3) {
                StickerUtils stickerUtils = StickerUtils.INSTANCE;
                return u.toList(stickerUtils.findStickerMatches(t.replace(mentionToken.getToken(), mentionToken.getLeadingIdentifier().toString(), "", true), StickerUtils.getStickersForAutocomplete$default(stickerUtils, null, null, null, null, null, 31, null), true));
            }
        }
        return n.emptyList();
    }

    public final boolean isBoolean(String str) {
        String str2;
        if (str != null) {
            Locale locale = Locale.getDefault();
            m.checkNotNullExpressionValue(locale, "Locale.getDefault()");
            str2 = str.toLowerCase(locale);
            m.checkNotNullExpressionValue(str2, "(this as java.lang.String).toLowerCase(locale)");
        } else {
            str2 = null;
        }
        return m.areEqual(str2, "true") || m.areEqual(str2, "false");
    }

    public final ChatInputMentionsMap mapInputToMentions(String str, Map<LeadingIdentifier, ? extends Set<? extends Autocompletable>> map, Map<IntRange, ? extends Autocompletable> map2, boolean z2) {
        boolean z3;
        m.checkNotNullParameter(str, "input");
        m.checkNotNullParameter(map, "autocompletables");
        m.checkNotNullParameter(map2, "currentInputMentionMap");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator<Map.Entry<LeadingIdentifier, ? extends Set<? extends Autocompletable>>> it = map.entrySet().iterator();
        while (true) {
            boolean z4 = false;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry<LeadingIdentifier, ? extends Set<? extends Autocompletable>> next = it.next();
            Character identifier = next.getKey().getIdentifier();
            if (identifier == null || w.contains$default((CharSequence) str, identifier.charValue(), false, 2, (Object) null)) {
                z4 = true;
            }
            if (z4) {
                linkedHashMap2.put(next.getKey(), next.getValue());
            }
        }
        List<Autocompletable> plus = u.plus((Collection) o.flatten(linkedHashMap2.values()), (Iterable) map2.values());
        if (!z2) {
            plus = filterAutocompletablesForMessageContext(plus);
        }
        for (Autocompletable autocompletable : plus) {
            Iterator<String> iterateTextMatchers = autocompletable.iterateTextMatchers();
            while (iterateTextMatchers.hasNext()) {
                String next2 = iterateTextMatchers.next();
                int indexOf$default = w.indexOf$default((CharSequence) str, next2, 0, false, 4, (Object) null);
                while (indexOf$default != -1) {
                    IntRange intRange = new IntRange(indexOf$default, next2.length() + indexOf$default);
                    if (!linkedHashMap.isEmpty()) {
                        Iterator it2 = linkedHashMap.entrySet().iterator();
                        while (it2.hasNext()) {
                            Map.Entry entry = (Map.Entry) it2.next();
                            IntRange intRange2 = (IntRange) entry.getKey();
                            Autocompletable autocompletable2 = (Autocompletable) entry.getValue();
                            if (!m.areEqual(intRange, intRange2) || !(autocompletable instanceof RoleAutocompletable) || (autocompletable2 instanceof RoleAutocompletable)) {
                                AutocompleteModelUtils autocompleteModelUtils = INSTANCE;
                                if (!autocompleteModelUtils.isSubRangeOf(intRange2, intRange)) {
                                    if (m.areEqual(intRange, intRange2) || autocompleteModelUtils.isSubRangeOf(intRange, intRange2)) {
                                        z3 = false;
                                        break;
                                    }
                                }
                            }
                            it2.remove();
                        }
                    }
                    z3 = true;
                    if (z3) {
                        Autocompletable autocompletable3 = map2.get(intRange);
                        if (autocompletable3 != null) {
                            linkedHashMap.put(intRange, autocompletable3);
                        } else {
                            linkedHashMap.put(intRange, autocompletable);
                        }
                    }
                    indexOf$default = w.indexOf$default((CharSequence) str, next2, indexOf$default + 1, false, 4, (Object) null);
                }
            }
        }
        return new ChatInputMentionsMap(str, linkedHashMap);
    }

    public final IntRange shiftOrRemove(IntRange intRange, int i, int i2, int i3) {
        m.checkNotNullParameter(intRange, "$this$shiftOrRemove");
        if (i >= intRange.getLast()) {
            return intRange;
        }
        int i4 = (i + i3) - i2;
        int i5 = i + i2;
        int i6 = i3 - i2;
        if (i < intRange.getFirst() && i5 >= intRange.getFirst()) {
            return null;
        }
        if (i > intRange.getFirst() && i < intRange.getLast()) {
            return null;
        }
        if (i >= intRange.getFirst() && i < intRange.getLast() && i2 > 0) {
            return null;
        }
        if (intRange.getFirst() >= i4 || (intRange.getFirst() == i && i2 == 0)) {
            return new IntRange(intRange.getFirst() + i6, intRange.getLast() + i6);
        }
        return null;
    }
}
