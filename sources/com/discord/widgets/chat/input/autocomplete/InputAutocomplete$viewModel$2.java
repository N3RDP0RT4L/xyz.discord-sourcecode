package com.discord.widgets.chat.input.autocomplete;

import com.discord.app.AppViewModel;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: InputAutocomplete.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/autocomplete/ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InputAutocomplete$viewModel$2 extends o implements Function0<AppViewModel<ViewState>> {
    public final /* synthetic */ InputAutocomplete this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InputAutocomplete$viewModel$2(InputAutocomplete inputAutocomplete) {
        super(0);
        this.this$0 = inputAutocomplete;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<ViewState> invoke() {
        Long l;
        int themedColor = ColorCompat.getThemedColor(this.this$0.editText, (int) R.attr.theme_chat_mention_foreground);
        int themedColor2 = ColorCompat.getThemedColor(this.this$0.editText, (int) R.attr.colorBackgroundFloating);
        int color = ColorCompat.getColor(this.this$0.editText, (int) R.color.status_red_500);
        l = this.this$0.channel;
        return new AutocompleteViewModel(l, null, StoreStream.Companion.getExperiments(), themedColor, themedColor2, color, null, 66, null);
    }
}
