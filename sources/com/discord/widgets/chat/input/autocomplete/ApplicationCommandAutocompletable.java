package com.discord.widgets.chat.input.autocomplete;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: Autocompletable.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u0016\u001a\u00020\u000e\u0012\u0006\u0010\u0017\u001a\u00020\u0011\u0012\u0006\u0010\u0018\u001a\u00020\u0011¢\u0006\u0004\b-\u0010.J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\bH\u0016¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0013J:\u0010\u0019\u001a\u00020\u00002\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\u0016\u001a\u00020\u000e2\b\b\u0002\u0010\u0017\u001a\u00020\u00112\b\b\u0002\u0010\u0018\u001a\u00020\u0011HÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001b\u0010\u0007J\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010!\u001a\u00020\u00112\b\u0010 \u001a\u0004\u0018\u00010\u001fHÖ\u0003¢\u0006\u0004\b!\u0010\"R\u001f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00050\b8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010\nR\u0019\u0010\u0016\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b'\u0010\u0010R\u0019\u0010\u0017\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010(\u001a\u0004\b)\u0010\u0013R\u0019\u0010\u0018\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b*\u0010\u0013R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010+\u001a\u0004\b,\u0010\r¨\u0006/"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/ApplicationCommandAutocompletable;", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "leadingIdentifier", "()Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "", "getInputReplacement", "()Ljava/lang/String;", "", "getInputTextMatchers", "()Ljava/util/List;", "Lcom/discord/models/commands/Application;", "component1", "()Lcom/discord/models/commands/Application;", "Lcom/discord/models/commands/ApplicationCommand;", "component2", "()Lcom/discord/models/commands/ApplicationCommand;", "", "component3", "()Z", "component4", "application", "command", "hasPermissionToUse", "showAvatar", "copy", "(Lcom/discord/models/commands/Application;Lcom/discord/models/commands/ApplicationCommand;ZZ)Lcom/discord/widgets/chat/input/autocomplete/ApplicationCommandAutocompletable;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "textMatchers", "Ljava/util/List;", "getTextMatchers", "Lcom/discord/models/commands/ApplicationCommand;", "getCommand", "Z", "getHasPermissionToUse", "getShowAvatar", "Lcom/discord/models/commands/Application;", "getApplication", HookHelper.constructorName, "(Lcom/discord/models/commands/Application;Lcom/discord/models/commands/ApplicationCommand;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ApplicationCommandAutocompletable extends Autocompletable {
    private final Application application;
    private final ApplicationCommand command;
    private final boolean hasPermissionToUse;
    private final boolean showAvatar;
    private final List<String> textMatchers;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ApplicationCommandAutocompletable(Application application, ApplicationCommand applicationCommand, boolean z2, boolean z3) {
        super(null);
        m.checkNotNullParameter(applicationCommand, "command");
        this.application = application;
        this.command = applicationCommand;
        this.hasPermissionToUse = z2;
        this.showAvatar = z3;
        this.textMatchers = d0.t.m.listOf(leadingIdentifier().getIdentifier() + applicationCommand.getName());
    }

    public static /* synthetic */ ApplicationCommandAutocompletable copy$default(ApplicationCommandAutocompletable applicationCommandAutocompletable, Application application, ApplicationCommand applicationCommand, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            application = applicationCommandAutocompletable.application;
        }
        if ((i & 2) != 0) {
            applicationCommand = applicationCommandAutocompletable.command;
        }
        if ((i & 4) != 0) {
            z2 = applicationCommandAutocompletable.hasPermissionToUse;
        }
        if ((i & 8) != 0) {
            z3 = applicationCommandAutocompletable.showAvatar;
        }
        return applicationCommandAutocompletable.copy(application, applicationCommand, z2, z3);
    }

    public final Application component1() {
        return this.application;
    }

    public final ApplicationCommand component2() {
        return this.command;
    }

    public final boolean component3() {
        return this.hasPermissionToUse;
    }

    public final boolean component4() {
        return this.showAvatar;
    }

    public final ApplicationCommandAutocompletable copy(Application application, ApplicationCommand applicationCommand, boolean z2, boolean z3) {
        m.checkNotNullParameter(applicationCommand, "command");
        return new ApplicationCommandAutocompletable(application, applicationCommand, z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApplicationCommandAutocompletable)) {
            return false;
        }
        ApplicationCommandAutocompletable applicationCommandAutocompletable = (ApplicationCommandAutocompletable) obj;
        return m.areEqual(this.application, applicationCommandAutocompletable.application) && m.areEqual(this.command, applicationCommandAutocompletable.command) && this.hasPermissionToUse == applicationCommandAutocompletable.hasPermissionToUse && this.showAvatar == applicationCommandAutocompletable.showAvatar;
    }

    public final Application getApplication() {
        return this.application;
    }

    public final ApplicationCommand getCommand() {
        return this.command;
    }

    public final boolean getHasPermissionToUse() {
        return this.hasPermissionToUse;
    }

    @Override // com.discord.widgets.chat.input.autocomplete.Autocompletable
    public String getInputReplacement() {
        return leadingIdentifier().getIdentifier() + this.command.getName();
    }

    @Override // com.discord.widgets.chat.input.autocomplete.Autocompletable
    public List<String> getInputTextMatchers() {
        return this.textMatchers;
    }

    public final boolean getShowAvatar() {
        return this.showAvatar;
    }

    public final List<String> getTextMatchers() {
        return this.textMatchers;
    }

    public int hashCode() {
        Application application = this.application;
        int i = 0;
        int hashCode = (application != null ? application.hashCode() : 0) * 31;
        ApplicationCommand applicationCommand = this.command;
        if (applicationCommand != null) {
            i = applicationCommand.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z2 = this.hasPermissionToUse;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z3 = this.showAvatar;
        if (!z3) {
            i3 = z3 ? 1 : 0;
        }
        return i6 + i3;
    }

    @Override // com.discord.widgets.chat.input.autocomplete.Autocompletable
    public LeadingIdentifier leadingIdentifier() {
        return LeadingIdentifier.APP_COMMAND;
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommandAutocompletable(application=");
        R.append(this.application);
        R.append(", command=");
        R.append(this.command);
        R.append(", hasPermissionToUse=");
        R.append(this.hasPermissionToUse);
        R.append(", showAvatar=");
        return a.M(R, this.showAvatar, ")");
    }
}
