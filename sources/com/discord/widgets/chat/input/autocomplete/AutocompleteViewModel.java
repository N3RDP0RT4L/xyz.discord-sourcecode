package com.discord.widgets.chat.input.autocomplete;

import andhook.lib.HookHelper;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.style.MetricAffectingSpan;
import android.text.style.ReplacementSpan;
import android.text.style.StyleSpan;
import androidx.annotation.ColorInt;
import androidx.annotation.MainThread;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.api.role.GuildRole;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppLog;
import com.discord.app.AppViewModel;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.models.commands.ApplicationSubCommand;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.CommandAutocompleteState;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreApplicationCommandFrecency;
import com.discord.stores.StoreApplicationCommands;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.textprocessing.FontColorSpan;
import com.discord.utilities.textprocessing.SimpleRoundedBackgroundSpan;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import com.discord.widgets.chat.MessageContent;
import com.discord.widgets.chat.input.AutocompleteStickerItem;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.input.WidgetChatInputDiscoveryCommandsModel;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewState;
import com.discord.widgets.chat.input.autocomplete.Event;
import com.discord.widgets.chat.input.autocomplete.InputEditTextAction;
import com.discord.widgets.chat.input.autocomplete.SelectedCommandViewState;
import com.discord.widgets.chat.input.autocomplete.commands.AutocompleteCommandUtils;
import com.discord.widgets.chat.input.autocomplete.sources.ApplicationCommandsAutocompletableSource;
import com.discord.widgets.chat.input.emoji.EmojiAutocompletePremiumUpsellFeatureFlag;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.discord.widgets.chat.input.models.AutocompleteApplicationCommands;
import com.discord.widgets.chat.input.models.AutocompleteInputModel;
import com.discord.widgets.chat.input.models.AutocompleteInputSelectionModel;
import com.discord.widgets.chat.input.models.ChatInputMentionsMap;
import com.discord.widgets.chat.input.models.CommandOptionValue;
import com.discord.widgets.chat.input.models.InputCommandContext;
import com.discord.widgets.chat.input.models.InputSelectionModel;
import com.discord.widgets.chat.input.models.InputSelectionModelKt;
import com.discord.widgets.chat.input.models.MentionInputModel;
import com.discord.widgets.chat.input.models.MentionToken;
import com.discord.widgets.chat.input.models.OptionRange;
import d0.d0.f;
import d0.g0.s;
import d0.g0.w;
import d0.g0.y;
import d0.t.h0;
import d0.t.n;
import d0.t.n0;
import d0.t.t;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.functions.Func9;
import rx.subjects.BehaviorSubject;
/* compiled from: AutocompleteViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009a\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u0000 ¿\u00012\b\u0012\u0004\u0012\u00020\u00020\u0001:\b¿\u0001À\u0001Á\u0001Â\u0001Be\u0012\u0012\b\u0002\u0010\u0089\u0001\u001a\u000b\u0018\u00010{j\u0005\u0018\u0001`\u0088\u0001\u0012\n\b\u0002\u0010\u009b\u0001\u001a\u00030\u009a\u0001\u0012\b\u0010\u0098\u0001\u001a\u00030\u0097\u0001\u0012\t\b\u0001\u0010¨\u0001\u001a\u00020Q\u0012\t\b\u0001\u0010\u008d\u0001\u001a\u00020Q\u0012\t\b\u0001\u0010\u0095\u0001\u001a\u00020Q\u0012\u000f\b\u0002\u0010¼\u0001\u001a\b\u0012\u0004\u0012\u00020\f0L¢\u0006\u0006\b½\u0001\u0010¾\u0001J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ+\u0010\u0015\u001a\u00020\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\b\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0014\u001a\u00020\u0012H\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u001f\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u0019H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ#\u0010\"\u001a\u0004\u0018\u00010!2\u0006\u0010\u0004\u001a\u00020\b2\b\u0010 \u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b\"\u0010#J#\u0010%\u001a\u0004\u0018\u00010$2\u0006\u0010\u0004\u001a\u00020\b2\b\u0010 \u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b%\u0010&J=\u00101\u001a\u0002002\u0006\u0010(\u001a\u00020'2\f\u0010+\u001a\b\u0012\u0004\u0012\u00020*0)2\f\u0010-\u001a\b\u0012\u0004\u0012\u00020,0)2\b\b\u0002\u0010/\u001a\u00020.H\u0002¢\u0006\u0004\b1\u00102J/\u00108\u001a\u0002072\u0006\u0010\t\u001a\u0002032\b\u00104\u001a\u0004\u0018\u00010$2\f\u00106\u001a\b\u0012\u0004\u0012\u00020$05H\u0002¢\u0006\u0004\b8\u00109J)\u0010=\u001a\u00020\u00052\u0006\u0010:\u001a\u00020\u00102\b\u0010;\u001a\u0004\u0018\u00010\u00102\u0006\u0010<\u001a\u00020\u0012H\u0002¢\u0006\u0004\b=\u0010>J\u0019\u0010@\u001a\u0004\u0018\u00010?2\u0006\u0010<\u001a\u00020\u0012H\u0002¢\u0006\u0004\b@\u0010AJ\u001f\u0010C\u001a\u00020\u00052\u0006\u0010B\u001a\u00020'2\u0006\u0010<\u001a\u00020\u0012H\u0003¢\u0006\u0004\bC\u0010DJ\u0017\u0010E\u001a\u0002072\u0006\u0010\t\u001a\u00020\u0019H\u0003¢\u0006\u0004\bE\u0010FJ\u0017\u0010G\u001a\u0002072\u0006\u0010:\u001a\u00020\u0010H\u0003¢\u0006\u0004\bG\u0010HJ\u0017\u0010I\u001a\u0002072\u0006\u0010:\u001a\u00020\u0010H\u0003¢\u0006\u0004\bI\u0010HJ\u000f\u0010J\u001a\u00020\u0005H\u0003¢\u0006\u0004\bJ\u0010KJ\u0013\u0010M\u001a\b\u0012\u0004\u0012\u0002070L¢\u0006\u0004\bM\u0010NJ\u0013\u0010P\u001a\b\u0012\u0004\u0012\u00020O0L¢\u0006\u0004\bP\u0010NJ/\u0010U\u001a\u0002072\u0006\u0010:\u001a\u00020\u00102\u0006\u0010R\u001a\u00020Q2\u0006\u0010S\u001a\u00020Q2\u0006\u0010T\u001a\u00020QH\u0007¢\u0006\u0004\bU\u0010VJ!\u0010Z\u001a\u0002072\u0006\u0010X\u001a\u00020W2\b\u0010Y\u001a\u0004\u0018\u00010\fH\u0007¢\u0006\u0004\bZ\u0010[J%\u0010]\u001a\u00020\u00052\u0006\u0010:\u001a\u00020'2\u0006\u0010R\u001a\u00020Q2\u0006\u0010\\\u001a\u00020Q¢\u0006\u0004\b]\u0010^J\u0017\u0010`\u001a\u00020\u00052\u0006\u0010_\u001a\u00020*H\u0007¢\u0006\u0004\b`\u0010aJ\u0017\u0010c\u001a\u00020\u00052\u0006\u0010b\u001a\u00020$H\u0007¢\u0006\u0004\bc\u0010dJ\u0017\u0010g\u001a\u00020\u00052\u0006\u0010f\u001a\u00020eH\u0007¢\u0006\u0004\bg\u0010hJ\u001b\u0010k\u001a\u0004\u0018\u00010j2\n\b\u0002\u0010i\u001a\u0004\u0018\u00010$¢\u0006\u0004\bk\u0010lJ\u0015\u0010o\u001a\u00020n2\u0006\u0010m\u001a\u00020'¢\u0006\u0004\bo\u0010pJ\r\u0010q\u001a\u00020\u0005¢\u0006\u0004\bq\u0010KJ\u001b\u0010s\u001a\u00020\u00052\f\u0010r\u001a\b\u0012\u0004\u0012\u00020*0)¢\u0006\u0004\bs\u0010tJ\r\u0010u\u001a\u00020\u0005¢\u0006\u0004\bu\u0010KJ\u000f\u0010v\u001a\u000200H\u0007¢\u0006\u0004\bv\u0010wJ'\u0010}\u001a\u0012\u0012\b\u0012\u00060{j\u0002`|\u0012\u0004\u0012\u00020Q0z2\u0006\u0010y\u001a\u00020xH\u0007¢\u0006\u0004\b}\u0010~J\u0019\u0010\u0081\u0001\u001a\u00020\u00052\u0007\u0010\u0080\u0001\u001a\u00020\u007f¢\u0006\u0006\b\u0081\u0001\u0010\u0082\u0001J+\u0010\u0086\u0001\u001a\u00020\u00052\u0007\u0010\u0083\u0001\u001a\u00020Q2\u0007\u0010\u0084\u0001\u001a\u00020Q2\u0007\u0010\u0085\u0001\u001a\u00020Q¢\u0006\u0006\b\u0086\u0001\u0010\u0087\u0001R'\u0010\u0089\u0001\u001a\u000b\u0018\u00010{j\u0005\u0018\u0001`\u0088\u00018\u0006@\u0006¢\u0006\u0010\n\u0006\b\u0089\u0001\u0010\u008a\u0001\u001a\u0006\b\u008b\u0001\u0010\u008c\u0001R\u001e\u0010\u008d\u0001\u001a\u00020Q8\u0006@\u0006¢\u0006\u0010\n\u0006\b\u008d\u0001\u0010\u008e\u0001\u001a\u0006\b\u008f\u0001\u0010\u0090\u0001R(\u0010Y\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0086\u000e¢\u0006\u0016\n\u0005\bY\u0010\u0091\u0001\u001a\u0006\b\u0092\u0001\u0010\u0093\u0001\"\u0005\b\u0094\u0001\u0010\u000fR\u001e\u0010\u0095\u0001\u001a\u00020Q8\u0006@\u0006¢\u0006\u0010\n\u0006\b\u0095\u0001\u0010\u008e\u0001\u001a\u0006\b\u0096\u0001\u0010\u0090\u0001R\u001a\u0010\u0098\u0001\u001a\u00030\u0097\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0098\u0001\u0010\u0099\u0001R\u001f\u0010\u009b\u0001\u001a\u00030\u009a\u00018\u0006@\u0006¢\u0006\u0010\n\u0006\b\u009b\u0001\u0010\u009c\u0001\u001a\u0006\b\u009d\u0001\u0010\u009e\u0001R\u001e\u0010\u009f\u0001\u001a\u00020.8\u0006@\u0006¢\u0006\u0010\n\u0006\b\u009f\u0001\u0010 \u0001\u001a\u0006\b¡\u0001\u0010¢\u0001RB\u0010¥\u0001\u001a+\u0012\r\u0012\u000b ¤\u0001*\u0004\u0018\u00010\u00190\u0019 ¤\u0001*\u0014\u0012\r\u0012\u000b ¤\u0001*\u0004\u0018\u00010\u00190\u0019\u0018\u00010£\u00010£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¥\u0001\u0010¦\u0001RB\u0010§\u0001\u001a+\u0012\r\u0012\u000b ¤\u0001*\u0004\u0018\u00010\b0\b ¤\u0001*\u0014\u0012\r\u0012\u000b ¤\u0001*\u0004\u0018\u00010\b0\b\u0018\u00010£\u00010£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b§\u0001\u0010¦\u0001R\u001e\u0010¨\u0001\u001a\u00020Q8\u0006@\u0006¢\u0006\u0010\n\u0006\b¨\u0001\u0010\u008e\u0001\u001a\u0006\b©\u0001\u0010\u0090\u0001RB\u0010ª\u0001\u001a+\u0012\r\u0012\u000b ¤\u0001*\u0004\u0018\u00010O0O ¤\u0001*\u0014\u0012\r\u0012\u000b ¤\u0001*\u0004\u0018\u00010O0O\u0018\u00010£\u00010£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bª\u0001\u0010¦\u0001R\u001b\u0010«\u0001\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b«\u0001\u0010¬\u0001R\u001b\u0010\u00ad\u0001\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u00ad\u0001\u0010®\u0001R)\u0010¯\u0001\u001a\u00020Q8\u0006@\u0006X\u0086\u000e¢\u0006\u0018\n\u0006\b¯\u0001\u0010\u008e\u0001\u001a\u0006\b°\u0001\u0010\u0090\u0001\"\u0006\b±\u0001\u0010²\u0001R\u0019\u0010³\u0001\u001a\u00020.8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b³\u0001\u0010 \u0001RF\u0010µ\u0001\u001a/\u0012\u000f\u0012\r ¤\u0001*\u0005\u0018\u00010´\u00010´\u0001 ¤\u0001*\u0016\u0012\u000f\u0012\r ¤\u0001*\u0005\u0018\u00010´\u00010´\u0001\u0018\u00010£\u00010£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bµ\u0001\u0010¦\u0001RB\u0010¶\u0001\u001a+\u0012\r\u0012\u000b ¤\u0001*\u0004\u0018\u00010707 ¤\u0001*\u0014\u0012\r\u0012\u000b ¤\u0001*\u0004\u0018\u00010707\u0018\u00010£\u00010£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¶\u0001\u0010¦\u0001R'\u0010X\u001a\u00020W8\u0006@\u0006X\u0086\u000e¢\u0006\u0017\n\u0005\bX\u0010·\u0001\u001a\u0006\b¸\u0001\u0010¹\u0001\"\u0006\bº\u0001\u0010»\u0001¨\u0006Ã\u0001"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/autocomplete/ViewState;", "Lcom/discord/widgets/chat/input/models/InputSelectionModel;", "newModel", "", "handleInputSelectionModel", "(Lcom/discord/widgets/chat/input/models/InputSelectionModel;)V", "Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;", "model", "handleAutocompleteInputSelectionModel", "(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;)V", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;", "newState", "handleStoreState", "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;)V", "", "currentInput", "Lcom/discord/api/channel/Channel;", "oldChannel", "newChannel", "handleNewChannel", "(Ljava/lang/CharSequence;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)V", "Lkotlin/ranges/IntRange;", "selection", "Lcom/discord/widgets/chat/input/models/MentionInputModel;", "inputModel", "handleSelectionWithInputModel", "(Lkotlin/ranges/IntRange;Lcom/discord/widgets/chat/input/models/MentionInputModel;)Lcom/discord/widgets/chat/input/models/InputSelectionModel;", "Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;", "getSelectedCommandViewState", "(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;)Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;", "oldModel", "Lcom/discord/models/commands/ApplicationCommand;", "newSelectedCommand", "(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;)Lcom/discord/models/commands/ApplicationCommand;", "Lcom/discord/models/commands/ApplicationCommandOption;", "newSelectedCommandOption", "(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;)Lcom/discord/models/commands/ApplicationCommandOption;", "", "token", "", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "autocompletables", "Lcom/discord/widgets/chat/input/AutocompleteStickerItem;", "stickers", "", "isAutocompletable", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;", "getAutocompleteViewState", "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Z)Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;", "Lcom/discord/widgets/chat/input/models/MentionInputModel$VerifiedCommandInputModel;", "selectedOption", "", "showError", "Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "applyCommandOptionSpans", "(Lcom/discord/widgets/chat/input/models/MentionInputModel$VerifiedCommandInputModel;Lcom/discord/models/commands/ApplicationCommandOption;Ljava/util/Set;)Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "input", "previousInput", "channel", "checkForCommandsToQuery", "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/discord/api/channel/Channel;)V", "Lcom/discord/models/user/User;", "getChannelBot", "(Lcom/discord/api/channel/Channel;)Lcom/discord/models/user/User;", "commandPrefix", "queryCommandsFromCommandPrefixToken", "(Ljava/lang/String;Lcom/discord/api/channel/Channel;)V", "generateSpanUpdates", "(Lcom/discord/widgets/chat/input/models/MentionInputModel;)Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "onPreAutocompleteCompute", "(Ljava/lang/CharSequence;)Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "replacementSpanCommandParamDeletion", "selectFirstInvalidCommandOption", "()V", "Lrx/Observable;", "observeEditTextActions", "()Lrx/Observable;", "Lcom/discord/widgets/chat/input/autocomplete/Event;", "observeEvents", "", "start", "before", "count", "onInputTextChanged", "(Ljava/lang/CharSequence;III)Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$InputState;", "inputState", "storeState", "onDataUpdated", "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$InputState;Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;)Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "finish", "onSelectionChanged", "(Ljava/lang/String;II)V", "autocompletable", "selectAutocompleteItem", "(Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;)V", "applicationCommandsOption", "selectCommandOption", "(Lcom/discord/models/commands/ApplicationCommandOption;)V", "Lcom/discord/api/sticker/Sticker;", "sticker", "selectStickerItem", "(Lcom/discord/api/sticker/Sticker;)V", "focusedOption", "Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "getApplicationSendData", "(Lcom/discord/models/commands/ApplicationCommandOption;)Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "content", "Lcom/discord/widgets/chat/MessageContent;", "replaceAutocompletableDataWithServerValues", "(Ljava/lang/String;)Lcom/discord/widgets/chat/MessageContent;", "onApplicationCommandSendError", "visibleItems", "checkEmojiAutocompleteUpsellViewed", "(Ljava/util/List;)V", "onAutocompleteItemsUpdated", "getApplicationCommandsBrowserViewState", "()Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;", "Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", "discoveryCommands", "", "", "Lcom/discord/primitives/ApplicationId;", "getCommandBrowserCommandPositions", "(Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;)Ljava/util/Map;", "Lcom/discord/models/commands/Application;", "application", "selectCommandBrowserApplication", "(Lcom/discord/models/commands/Application;)V", "firstVisibleItemPosition", "lastVisibleItemPosition", "totalItems", "onCommandsBrowserScroll", "(III)V", "Lcom/discord/primitives/ChannelId;", "channelId", "Ljava/lang/Long;", "getChannelId", "()Ljava/lang/Long;", "defaultCommandOptionBackgroundColor", "I", "getDefaultCommandOptionBackgroundColor", "()I", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;", "getStoreState", "()Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;", "setStoreState", "commandOptionErrorColor", "getCommandOptionErrorColor", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/stores/StoreExperiments;", "Lcom/discord/stores/StoreApplicationCommands;", "storeApplicationCommands", "Lcom/discord/stores/StoreApplicationCommands;", "getStoreApplicationCommands", "()Lcom/discord/stores/StoreApplicationCommands;", "emojiAutocompleteUpsellEnabled", "Z", "getEmojiAutocompleteUpsellEnabled", "()Z", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "inputMentionModelSubject", "Lrx/subjects/BehaviorSubject;", "autocompleteInputSelectionModelSubject", "defaultMentionColor", "getDefaultMentionColor", "events", "lastChatInputModel", "Lcom/discord/widgets/chat/input/models/InputSelectionModel;", "lastAutocompleteInputSelectionModel", "Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;", "lastJumpedSequenceId", "getLastJumpedSequenceId", "setLastJumpedSequenceId", "(I)V", "logEmojiAutocompleteUpsellViewed", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$SelectionState;", "inputSelectionSubject", "editTextAction", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$InputState;", "getInputState", "()Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$InputState;", "setInputState", "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$InputState;)V", "storeObservable", HookHelper.constructorName, "(Ljava/lang/Long;Lcom/discord/stores/StoreApplicationCommands;Lcom/discord/stores/StoreExperiments;IIILrx/Observable;)V", "Companion", "InputState", "SelectionState", "StoreState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AutocompleteViewModel extends AppViewModel<ViewState> {
    public static final String COMMAND_DISCOVER_TOKEN = "/";
    public static final Companion Companion = new Companion(null);
    public static final int DEFAULT_PLACEHOLDER_COUNT = 3;
    private final BehaviorSubject<AutocompleteInputSelectionModel> autocompleteInputSelectionModelSubject;
    private final Long channelId;
    private final int commandOptionErrorColor;
    private final int defaultCommandOptionBackgroundColor;
    private final int defaultMentionColor;
    private final BehaviorSubject<InputEditTextAction> editTextAction;
    private final boolean emojiAutocompleteUpsellEnabled;
    private final BehaviorSubject<Event> events;
    private final BehaviorSubject<MentionInputModel> inputMentionModelSubject;
    private final BehaviorSubject<SelectionState> inputSelectionSubject;
    private InputState inputState;
    private AutocompleteInputSelectionModel lastAutocompleteInputSelectionModel;
    private InputSelectionModel lastChatInputModel;
    private int lastJumpedSequenceId;
    private boolean logEmojiAutocompleteUpsellViewed;
    private final StoreApplicationCommands storeApplicationCommands;
    private final StoreExperiments storeExperiments;
    private StoreState storeState;

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T> implements Action1<Throwable> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final void call(Throwable th) {
            Logger.e$default(AppLog.g, "Autocomplete Store Error", th, null, 4, null);
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052&\u0010\u0004\u001a\"\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002 \u0003*\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lkotlin/Pair;", "Lcom/discord/models/commands/ApplicationCommandOption;", "", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$11  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass11 extends o implements Function1<Pair<? extends ApplicationCommandOption, ? extends String>, Unit> {
        public AnonymousClass11() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends ApplicationCommandOption, ? extends String> pair) {
            invoke2((Pair<ApplicationCommandOption, String>) pair);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Pair<ApplicationCommandOption, String> pair) {
            AutocompleteViewModel.this.events.onNext(new Event.RequestAutocompleteData(pair.getFirst()));
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;)Ljava/lang/String;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$12  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass12<T, R> implements b<AutocompleteInputSelectionModel, String> {
        public static final AnonymousClass12 INSTANCE = new AnonymousClass12();

        public final String call(AutocompleteInputSelectionModel autocompleteInputSelectionModel) {
            MentionToken autocompleteToken = autocompleteInputSelectionModel.getAutocompleteToken();
            if (autocompleteToken != null) {
                return autocompleteToken.getToken();
            }
            return null;
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass2 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass2(AutocompleteViewModel autocompleteViewModel) {
            super(1, autocompleteViewModel, AutocompleteViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((AutocompleteViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$4  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass4<T> implements Action1<Throwable> {
        public static final AnonymousClass4 INSTANCE = new AnonymousClass4();

        public final void call(Throwable th) {
            Logger.e$default(AppLog.g, "Autocomplete Input Selection Model Error", th, null, 4, null);
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/models/InputSelectionModel;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/models/InputSelectionModel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$5  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass5 extends k implements Function1<InputSelectionModel, Unit> {
        public AnonymousClass5(AutocompleteViewModel autocompleteViewModel) {
            super(1, autocompleteViewModel, AutocompleteViewModel.class, "handleInputSelectionModel", "handleInputSelectionModel(Lcom/discord/widgets/chat/input/models/InputSelectionModel;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(InputSelectionModel inputSelectionModel) {
            invoke2(inputSelectionModel);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(InputSelectionModel inputSelectionModel) {
            m.checkNotNullParameter(inputSelectionModel, "p1");
            ((AutocompleteViewModel) this.receiver).handleInputSelectionModel(inputSelectionModel);
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$6  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass6<T> implements Action1<Throwable> {
        public static final AnonymousClass6 INSTANCE = new AnonymousClass6();

        public final void call(Throwable th) {
            Logger.e$default(AppLog.g, "Full Autocomplete Model Error", th, null, 4, null);
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$7  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass7 extends k implements Function1<AutocompleteInputSelectionModel, Unit> {
        public AnonymousClass7(AutocompleteViewModel autocompleteViewModel) {
            super(1, autocompleteViewModel, AutocompleteViewModel.class, "handleAutocompleteInputSelectionModel", "handleAutocompleteInputSelectionModel(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(AutocompleteInputSelectionModel autocompleteInputSelectionModel) {
            invoke2(autocompleteInputSelectionModel);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(AutocompleteInputSelectionModel autocompleteInputSelectionModel) {
            m.checkNotNullParameter(autocompleteInputSelectionModel, "p1");
            ((AutocompleteViewModel) this.receiver).handleAutocompleteInputSelectionModel(autocompleteInputSelectionModel);
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$8  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass8<T> implements Action1<Throwable> {
        public static final AnonymousClass8 INSTANCE = new AnonymousClass8();

        public final void call(Throwable th) {
            Logger.e$default(AppLog.g, "Full Autocomplete Model Error", th, null, 4, null);
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\b\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;", "kotlin.jvm.PlatformType", "it", "Lkotlin/Pair;", "Lcom/discord/models/commands/ApplicationCommandOption;", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/chat/input/models/AutocompleteInputSelectionModel;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$9  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass9<T, R> implements b<AutocompleteInputSelectionModel, Pair<? extends ApplicationCommandOption, ? extends String>> {
        public static final AnonymousClass9 INSTANCE = new AnonymousClass9();

        public final Pair<ApplicationCommandOption, String> call(AutocompleteInputSelectionModel autocompleteInputSelectionModel) {
            ApplicationCommandOption selectedCommandOption;
            InputSelectionModel inputSelectionModel = autocompleteInputSelectionModel.getInputSelectionModel();
            Object obj = null;
            if (!(inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel)) {
                inputSelectionModel = null;
            }
            InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
            if (commandInputSelectionModel == null || (selectedCommandOption = commandInputSelectionModel.getSelectedCommandOption()) == null || !selectedCommandOption.getAutocomplete()) {
                return null;
            }
            CommandOptionValue commandOptionValue = commandInputSelectionModel.getInputModel().getInputCommandOptionValues().get(selectedCommandOption);
            if (commandOptionValue != null) {
                obj = commandOptionValue.getValue();
            }
            return new Pair<>(selectedCommandOption, String.valueOf(obj));
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J5\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "inputChannelId", "Lcom/discord/stores/StoreApplicationCommands;", "storeApplicationCommands", "Lcom/discord/stores/StoreApplicationCommandFrecency;", "storeApplicationCommandsFrecency", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;", "observeStores", "(Ljava/lang/Long;Lcom/discord/stores/StoreApplicationCommands;Lcom/discord/stores/StoreApplicationCommandFrecency;)Lrx/Observable;", "", "COMMAND_DISCOVER_TOKEN", "Ljava/lang/String;", "", "DEFAULT_PLACEHOLDER_COUNT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores(Long l, final StoreApplicationCommands storeApplicationCommands, final StoreApplicationCommandFrecency storeApplicationCommandFrecency) {
            Observable observable;
            if (l != null) {
                observable = StoreStream.Companion.getChannels().observeChannel(l.longValue()).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
                m.checkNotNullExpressionValue(observable, "filter { it != null }.map { it!! }");
            } else {
                observable = StoreStream.Companion.getChannelsSelected().observeSelectedChannel().x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
                m.checkNotNullExpressionValue(observable, "filter { it != null }.map { it!! }");
            }
            Observable<StoreState> Y = observable.Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$Companion$observeStores$1
                public final Observable<? extends AutocompleteViewModel.StoreState> call(final Channel channel) {
                    Observable<List<ApplicationCommand>> observable2;
                    m.checkNotNullExpressionValue(channel, "channel");
                    if (ChannelUtils.j(channel)) {
                        observable2 = new j0.l.e.k<>(n.emptyList());
                    } else {
                        observable2 = StoreApplicationCommands.this.observeQueryCommands(channel.h());
                    }
                    Observable<List<ApplicationCommand>> observable3 = observable2;
                    StoreStream.Companion companion = StoreStream.Companion;
                    return Observable.c(companion.getUsers().observeMeId(), companion.getGuilds().observeComputed(channel.f()), ApplicationCommandsAutocompletableSource.Companion.getDiscoveryCommands(true, 3, channel), observable3, ChatInputAutocompletables.INSTANCE.observeChannelAutocompletables(channel.h()), StoreApplicationCommands.this.observeGuildApplications(channel.h()), StoreApplicationCommands.this.observeAutocompleteResults(), storeApplicationCommandFrecency.observeTopCommandIds(Long.valueOf(channel.f())), StoreApplicationCommands.this.observeFrecencyCommands(channel.f()), new Func9<Long, Map<Long, ? extends GuildMember>, WidgetChatInputDiscoveryCommandsModel, List<? extends ApplicationCommand>, Map<LeadingIdentifier, ? extends Set<? extends Autocompletable>>, List<? extends Application>, Map<String, ? extends Map<String, ? extends CommandAutocompleteState>>, List<? extends String>, List<? extends ApplicationCommand>, AutocompleteViewModel.StoreState>() { // from class: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$Companion$observeStores$1.1
                        @Override // rx.functions.Func9
                        public /* bridge */ /* synthetic */ AutocompleteViewModel.StoreState call(Long l2, Map<Long, ? extends GuildMember> map, WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel, List<? extends ApplicationCommand> list, Map<LeadingIdentifier, ? extends Set<? extends Autocompletable>> map2, List<? extends Application> list2, Map<String, ? extends Map<String, ? extends CommandAutocompleteState>> map3, List<? extends String> list3, List<? extends ApplicationCommand> list4) {
                            return call2(l2, (Map<Long, GuildMember>) map, widgetChatInputDiscoveryCommandsModel, list, map2, (List<Application>) list2, map3, (List<String>) list3, list4);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final AutocompleteViewModel.StoreState call2(Long l2, Map<Long, GuildMember> map, WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel, List<? extends ApplicationCommand> list, Map<LeadingIdentifier, ? extends Set<? extends Autocompletable>> map2, List<Application> list2, Map<String, ? extends Map<String, ? extends CommandAutocompleteState>> map3, List<String> list3, List<? extends ApplicationCommand> list4) {
                            m.checkNotNullExpressionValue(l2, "userId");
                            long longValue = l2.longValue();
                            GuildMember guildMember = map.get(l2);
                            List<Long> roles = guildMember != null ? guildMember.getRoles() : null;
                            if (roles == null) {
                                roles = n.emptyList();
                            }
                            Channel channel2 = Channel.this;
                            m.checkNotNullExpressionValue(channel2, "channel");
                            m.checkNotNullExpressionValue(list, "queriedCommands");
                            m.checkNotNullExpressionValue(list2, "apps");
                            m.checkNotNullExpressionValue(map3, "autoOptions");
                            m.checkNotNullExpressionValue(map2, "autocompletables");
                            m.checkNotNullExpressionValue(list3, "frecencyIds");
                            m.checkNotNullExpressionValue(list4, "frecencyApps");
                            return new AutocompleteViewModel.StoreState(longValue, roles, channel2, list, list2, map3, map2, widgetChatInputDiscoveryCommandsModel, list3, list4);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "inputChannel.switchMap {…      )\n        }\n      }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BY\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0005\u0012\u0018\b\u0002\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000e\u0012\u0014\b\u0002\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\b¢\u0006\u0004\b/\u00100J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J \u0010\f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001c\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\bHÆ\u0003¢\u0006\u0004\b\u0013\u0010\rJb\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00022\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00052\u0018\b\u0002\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b2\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000e2\u0014\b\u0002\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\bHÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010\"\u001a\u00020!2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010$\u001a\u0004\b%\u0010\u0010R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b'\u0010\u0007R2\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\r\"\u0004\b*\u0010+R%\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b,\u0010\rR\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010-\u001a\u0004\b.\u0010\u0004¨\u00061"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$InputState;", "", "", "component1", "()Ljava/lang/CharSequence;", "Lcom/discord/models/commands/ApplicationCommand;", "component2", "()Lcom/discord/models/commands/ApplicationCommand;", "", "", "Lcom/discord/primitives/ApplicationId;", "", "component3", "()Ljava/util/Map;", "Lcom/discord/models/commands/ApplicationCommandOption;", "component4", "()Lcom/discord/models/commands/ApplicationCommandOption;", "Lkotlin/ranges/IntRange;", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "component5", "currentInput", "selectedCommand", "applicationsPosition", "showErrorForOption", "inputAutocompleteMap", "copy", "(Ljava/lang/CharSequence;Lcom/discord/models/commands/ApplicationCommand;Ljava/util/Map;Lcom/discord/models/commands/ApplicationCommandOption;Ljava/util/Map;)Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$InputState;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/commands/ApplicationCommandOption;", "getShowErrorForOption", "Lcom/discord/models/commands/ApplicationCommand;", "getSelectedCommand", "Ljava/util/Map;", "getApplicationsPosition", "setApplicationsPosition", "(Ljava/util/Map;)V", "getInputAutocompleteMap", "Ljava/lang/CharSequence;", "getCurrentInput", HookHelper.constructorName, "(Ljava/lang/CharSequence;Lcom/discord/models/commands/ApplicationCommand;Ljava/util/Map;Lcom/discord/models/commands/ApplicationCommandOption;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class InputState {
        private Map<Long, Integer> applicationsPosition;
        private final CharSequence currentInput;
        private final Map<IntRange, Autocompletable> inputAutocompleteMap;
        private final ApplicationCommand selectedCommand;
        private final ApplicationCommandOption showErrorForOption;

        public InputState() {
            this(null, null, null, null, null, 31, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public InputState(CharSequence charSequence, ApplicationCommand applicationCommand, Map<Long, Integer> map, ApplicationCommandOption applicationCommandOption, Map<IntRange, ? extends Autocompletable> map2) {
            m.checkNotNullParameter(charSequence, "currentInput");
            m.checkNotNullParameter(map, "applicationsPosition");
            m.checkNotNullParameter(map2, "inputAutocompleteMap");
            this.currentInput = charSequence;
            this.selectedCommand = applicationCommand;
            this.applicationsPosition = map;
            this.showErrorForOption = applicationCommandOption;
            this.inputAutocompleteMap = map2;
        }

        public static /* synthetic */ InputState copy$default(InputState inputState, CharSequence charSequence, ApplicationCommand applicationCommand, Map map, ApplicationCommandOption applicationCommandOption, Map map2, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = inputState.currentInput;
            }
            if ((i & 2) != 0) {
                applicationCommand = inputState.selectedCommand;
            }
            ApplicationCommand applicationCommand2 = applicationCommand;
            Map<Long, Integer> map3 = map;
            if ((i & 4) != 0) {
                map3 = inputState.applicationsPosition;
            }
            Map map4 = map3;
            if ((i & 8) != 0) {
                applicationCommandOption = inputState.showErrorForOption;
            }
            ApplicationCommandOption applicationCommandOption2 = applicationCommandOption;
            Map<IntRange, Autocompletable> map5 = map2;
            if ((i & 16) != 0) {
                map5 = inputState.inputAutocompleteMap;
            }
            return inputState.copy(charSequence, applicationCommand2, map4, applicationCommandOption2, map5);
        }

        public final CharSequence component1() {
            return this.currentInput;
        }

        public final ApplicationCommand component2() {
            return this.selectedCommand;
        }

        public final Map<Long, Integer> component3() {
            return this.applicationsPosition;
        }

        public final ApplicationCommandOption component4() {
            return this.showErrorForOption;
        }

        public final Map<IntRange, Autocompletable> component5() {
            return this.inputAutocompleteMap;
        }

        public final InputState copy(CharSequence charSequence, ApplicationCommand applicationCommand, Map<Long, Integer> map, ApplicationCommandOption applicationCommandOption, Map<IntRange, ? extends Autocompletable> map2) {
            m.checkNotNullParameter(charSequence, "currentInput");
            m.checkNotNullParameter(map, "applicationsPosition");
            m.checkNotNullParameter(map2, "inputAutocompleteMap");
            return new InputState(charSequence, applicationCommand, map, applicationCommandOption, map2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof InputState)) {
                return false;
            }
            InputState inputState = (InputState) obj;
            return m.areEqual(this.currentInput, inputState.currentInput) && m.areEqual(this.selectedCommand, inputState.selectedCommand) && m.areEqual(this.applicationsPosition, inputState.applicationsPosition) && m.areEqual(this.showErrorForOption, inputState.showErrorForOption) && m.areEqual(this.inputAutocompleteMap, inputState.inputAutocompleteMap);
        }

        public final Map<Long, Integer> getApplicationsPosition() {
            return this.applicationsPosition;
        }

        public final CharSequence getCurrentInput() {
            return this.currentInput;
        }

        public final Map<IntRange, Autocompletable> getInputAutocompleteMap() {
            return this.inputAutocompleteMap;
        }

        public final ApplicationCommand getSelectedCommand() {
            return this.selectedCommand;
        }

        public final ApplicationCommandOption getShowErrorForOption() {
            return this.showErrorForOption;
        }

        public int hashCode() {
            CharSequence charSequence = this.currentInput;
            int i = 0;
            int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
            ApplicationCommand applicationCommand = this.selectedCommand;
            int hashCode2 = (hashCode + (applicationCommand != null ? applicationCommand.hashCode() : 0)) * 31;
            Map<Long, Integer> map = this.applicationsPosition;
            int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
            ApplicationCommandOption applicationCommandOption = this.showErrorForOption;
            int hashCode4 = (hashCode3 + (applicationCommandOption != null ? applicationCommandOption.hashCode() : 0)) * 31;
            Map<IntRange, Autocompletable> map2 = this.inputAutocompleteMap;
            if (map2 != null) {
                i = map2.hashCode();
            }
            return hashCode4 + i;
        }

        public final void setApplicationsPosition(Map<Long, Integer> map) {
            m.checkNotNullParameter(map, "<set-?>");
            this.applicationsPosition = map;
        }

        public String toString() {
            StringBuilder R = a.R("InputState(currentInput=");
            R.append(this.currentInput);
            R.append(", selectedCommand=");
            R.append(this.selectedCommand);
            R.append(", applicationsPosition=");
            R.append(this.applicationsPosition);
            R.append(", showErrorForOption=");
            R.append(this.showErrorForOption);
            R.append(", inputAutocompleteMap=");
            return a.L(R, this.inputAutocompleteMap, ")");
        }

        public /* synthetic */ InputState(String str, ApplicationCommand applicationCommand, Map map, ApplicationCommandOption applicationCommandOption, Map map2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? null : applicationCommand, (i & 4) != 0 ? new HashMap() : map, (i & 8) == 0 ? applicationCommandOption : null, (i & 16) != 0 ? h0.emptyMap() : map2);
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0015\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$SelectionState;", "", "", "component1", "()Ljava/lang/String;", "Lkotlin/ranges/IntRange;", "component2", "()Lkotlin/ranges/IntRange;", "input", "selection", "copy", "(Ljava/lang/String;Lkotlin/ranges/IntRange;)Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$SelectionState;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/ranges/IntRange;", "getSelection", "Ljava/lang/String;", "getInput", HookHelper.constructorName, "(Ljava/lang/String;Lkotlin/ranges/IntRange;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SelectionState {
        private final String input;
        private final IntRange selection;

        public SelectionState(String str, IntRange intRange) {
            m.checkNotNullParameter(str, "input");
            m.checkNotNullParameter(intRange, "selection");
            this.input = str;
            this.selection = intRange;
        }

        public static /* synthetic */ SelectionState copy$default(SelectionState selectionState, String str, IntRange intRange, int i, Object obj) {
            if ((i & 1) != 0) {
                str = selectionState.input;
            }
            if ((i & 2) != 0) {
                intRange = selectionState.selection;
            }
            return selectionState.copy(str, intRange);
        }

        public final String component1() {
            return this.input;
        }

        public final IntRange component2() {
            return this.selection;
        }

        public final SelectionState copy(String str, IntRange intRange) {
            m.checkNotNullParameter(str, "input");
            m.checkNotNullParameter(intRange, "selection");
            return new SelectionState(str, intRange);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SelectionState)) {
                return false;
            }
            SelectionState selectionState = (SelectionState) obj;
            return m.areEqual(this.input, selectionState.input) && m.areEqual(this.selection, selectionState.selection);
        }

        public final String getInput() {
            return this.input;
        }

        public final IntRange getSelection() {
            return this.selection;
        }

        public int hashCode() {
            String str = this.input;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            IntRange intRange = this.selection;
            if (intRange != null) {
                i = intRange.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("SelectionState(input=");
            R.append(this.input);
            R.append(", selection=");
            R.append(this.selection);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001B·\u0001\u0012\n\u0010\u001f\u001a\u00060\u0002j\u0002`\u0003\u0012\u0010\u0010 \u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u0006\u0012\u0006\u0010!\u001a\u00020\n\u0012\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\r0\u0006\u0012\u000e\b\u0002\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0006\u0012 \b\u0002\u0010$\u001a\u001a\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u00110\u0011\u0012\u001a\b\u0002\u0010%\u001a\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u0011\u0012\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u001a\u0012\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00120\u0006\u0012\u000e\b\u0002\u0010(\u001a\b\u0012\u0004\u0012\u00020\r0\u0006¢\u0006\u0004\bC\u0010DJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001a\u0010\b\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u0006HÆ\u0003¢\u0006\u0004\b\u000e\u0010\tJ\u0016\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0006HÆ\u0003¢\u0006\u0004\b\u0010\u0010\tJ(\u0010\u0014\u001a\u001a\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u00110\u0011HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\"\u0010\u0019\u001a\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u0011HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0015J\u0012\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0016\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00120\u0006HÆ\u0003¢\u0006\u0004\b\u001d\u0010\tJ\u0016\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\r0\u0006HÆ\u0003¢\u0006\u0004\b\u001e\u0010\tJÆ\u0001\u0010)\u001a\u00020\u00002\f\b\u0002\u0010\u001f\u001a\u00060\u0002j\u0002`\u00032\u0012\b\u0002\u0010 \u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u00062\b\b\u0002\u0010!\u001a\u00020\n2\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\r0\u00062\u000e\b\u0002\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00062 \b\u0002\u0010$\u001a\u001a\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u00110\u00112\u001a\b\u0002\u0010%\u001a\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u00112\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u001a2\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00120\u00062\u000e\b\u0002\u0010(\u001a\b\u0012\u0004\u0012\u00020\r0\u0006HÆ\u0001¢\u0006\u0004\b)\u0010*J\u0010\u0010+\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b+\u0010,J\u0010\u0010.\u001a\u00020-HÖ\u0001¢\u0006\u0004\b.\u0010/J\u001a\u00102\u001a\u0002012\b\u00100\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b2\u00103R1\u0010$\u001a\u001a\u0012\u0004\u0012\u00020\u0012\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u00110\u00118\u0006@\u0006¢\u0006\f\n\u0004\b$\u00104\u001a\u0004\b5\u0010\u0015R\u001f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b#\u00106\u001a\u0004\b7\u0010\tR\u0019\u0010!\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00108\u001a\u0004\b9\u0010\fR\u001b\u0010&\u001a\u0004\u0018\u00010\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010:\u001a\u0004\b;\u0010\u001cR\u001f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\r0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00106\u001a\u0004\b<\u0010\tR+\u0010%\u001a\u0014\u0012\u0004\u0012\u00020\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00170\u00118\u0006@\u0006¢\u0006\f\n\u0004\b%\u00104\u001a\u0004\b=\u0010\u0015R\u001f\u0010(\u001a\b\u0012\u0004\u0012\u00020\r0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b(\u00106\u001a\u0004\b>\u0010\tR\u001f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00120\u00068\u0006@\u0006¢\u0006\f\n\u0004\b'\u00106\u001a\u0004\b?\u0010\tR\u001d\u0010\u001f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010@\u001a\u0004\bA\u0010\u0005R#\u0010 \u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b \u00106\u001a\u0004\bB\u0010\t¨\u0006E"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;", "", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "", "Lcom/discord/primitives/RoleId;", "component2", "()Ljava/util/List;", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/commands/ApplicationCommand;", "component4", "Lcom/discord/models/commands/Application;", "component5", "", "", "Lcom/discord/stores/CommandAutocompleteState;", "component6", "()Ljava/util/Map;", "Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "component7", "Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", "component8", "()Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", "component9", "component10", "userId", "userRoles", "channel", "queriedCommands", "applications", "commandOptionAutocompleteItems", "autocompletables", "browserCommands", "frecencyCommandIds", "frecencyCommands", "copy", "(JLjava/util/List;Lcom/discord/api/channel/Channel;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel$StoreState;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getCommandOptionAutocompleteItems", "Ljava/util/List;", "getApplications", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", "getBrowserCommands", "getQueriedCommands", "getAutocompletables", "getFrecencyCommands", "getFrecencyCommandIds", "J", "getUserId", "getUserRoles", HookHelper.constructorName, "(JLjava/util/List;Lcom/discord/api/channel/Channel;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;Ljava/util/List;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final List<Application> applications;
        private final Map<LeadingIdentifier, Set<Autocompletable>> autocompletables;
        private final WidgetChatInputDiscoveryCommandsModel browserCommands;
        private final Channel channel;
        private final Map<String, Map<String, CommandAutocompleteState>> commandOptionAutocompleteItems;
        private final List<String> frecencyCommandIds;
        private final List<ApplicationCommand> frecencyCommands;
        private final List<ApplicationCommand> queriedCommands;
        private final long userId;
        private final List<Long> userRoles;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(long j, List<Long> list, Channel channel, List<? extends ApplicationCommand> list2, List<Application> list3, Map<String, ? extends Map<String, ? extends CommandAutocompleteState>> map, Map<LeadingIdentifier, ? extends Set<? extends Autocompletable>> map2, WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel, List<String> list4, List<? extends ApplicationCommand> list5) {
            m.checkNotNullParameter(list, "userRoles");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(list2, "queriedCommands");
            m.checkNotNullParameter(list3, "applications");
            m.checkNotNullParameter(map, "commandOptionAutocompleteItems");
            m.checkNotNullParameter(map2, "autocompletables");
            m.checkNotNullParameter(list4, "frecencyCommandIds");
            m.checkNotNullParameter(list5, "frecencyCommands");
            this.userId = j;
            this.userRoles = list;
            this.channel = channel;
            this.queriedCommands = list2;
            this.applications = list3;
            this.commandOptionAutocompleteItems = map;
            this.autocompletables = map2;
            this.browserCommands = widgetChatInputDiscoveryCommandsModel;
            this.frecencyCommandIds = list4;
            this.frecencyCommands = list5;
        }

        public final long component1() {
            return this.userId;
        }

        public final List<ApplicationCommand> component10() {
            return this.frecencyCommands;
        }

        public final List<Long> component2() {
            return this.userRoles;
        }

        public final Channel component3() {
            return this.channel;
        }

        public final List<ApplicationCommand> component4() {
            return this.queriedCommands;
        }

        public final List<Application> component5() {
            return this.applications;
        }

        public final Map<String, Map<String, CommandAutocompleteState>> component6() {
            return this.commandOptionAutocompleteItems;
        }

        public final Map<LeadingIdentifier, Set<Autocompletable>> component7() {
            return this.autocompletables;
        }

        public final WidgetChatInputDiscoveryCommandsModel component8() {
            return this.browserCommands;
        }

        public final List<String> component9() {
            return this.frecencyCommandIds;
        }

        public final StoreState copy(long j, List<Long> list, Channel channel, List<? extends ApplicationCommand> list2, List<Application> list3, Map<String, ? extends Map<String, ? extends CommandAutocompleteState>> map, Map<LeadingIdentifier, ? extends Set<? extends Autocompletable>> map2, WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel, List<String> list4, List<? extends ApplicationCommand> list5) {
            m.checkNotNullParameter(list, "userRoles");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(list2, "queriedCommands");
            m.checkNotNullParameter(list3, "applications");
            m.checkNotNullParameter(map, "commandOptionAutocompleteItems");
            m.checkNotNullParameter(map2, "autocompletables");
            m.checkNotNullParameter(list4, "frecencyCommandIds");
            m.checkNotNullParameter(list5, "frecencyCommands");
            return new StoreState(j, list, channel, list2, list3, map, map2, widgetChatInputDiscoveryCommandsModel, list4, list5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return this.userId == storeState.userId && m.areEqual(this.userRoles, storeState.userRoles) && m.areEqual(this.channel, storeState.channel) && m.areEqual(this.queriedCommands, storeState.queriedCommands) && m.areEqual(this.applications, storeState.applications) && m.areEqual(this.commandOptionAutocompleteItems, storeState.commandOptionAutocompleteItems) && m.areEqual(this.autocompletables, storeState.autocompletables) && m.areEqual(this.browserCommands, storeState.browserCommands) && m.areEqual(this.frecencyCommandIds, storeState.frecencyCommandIds) && m.areEqual(this.frecencyCommands, storeState.frecencyCommands);
        }

        public final List<Application> getApplications() {
            return this.applications;
        }

        public final Map<LeadingIdentifier, Set<Autocompletable>> getAutocompletables() {
            return this.autocompletables;
        }

        public final WidgetChatInputDiscoveryCommandsModel getBrowserCommands() {
            return this.browserCommands;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Map<String, Map<String, CommandAutocompleteState>> getCommandOptionAutocompleteItems() {
            return this.commandOptionAutocompleteItems;
        }

        public final List<String> getFrecencyCommandIds() {
            return this.frecencyCommandIds;
        }

        public final List<ApplicationCommand> getFrecencyCommands() {
            return this.frecencyCommands;
        }

        public final List<ApplicationCommand> getQueriedCommands() {
            return this.queriedCommands;
        }

        public final long getUserId() {
            return this.userId;
        }

        public final List<Long> getUserRoles() {
            return this.userRoles;
        }

        public int hashCode() {
            int a = a0.a.a.b.a(this.userId) * 31;
            List<Long> list = this.userRoles;
            int i = 0;
            int hashCode = (a + (list != null ? list.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            List<ApplicationCommand> list2 = this.queriedCommands;
            int hashCode3 = (hashCode2 + (list2 != null ? list2.hashCode() : 0)) * 31;
            List<Application> list3 = this.applications;
            int hashCode4 = (hashCode3 + (list3 != null ? list3.hashCode() : 0)) * 31;
            Map<String, Map<String, CommandAutocompleteState>> map = this.commandOptionAutocompleteItems;
            int hashCode5 = (hashCode4 + (map != null ? map.hashCode() : 0)) * 31;
            Map<LeadingIdentifier, Set<Autocompletable>> map2 = this.autocompletables;
            int hashCode6 = (hashCode5 + (map2 != null ? map2.hashCode() : 0)) * 31;
            WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel = this.browserCommands;
            int hashCode7 = (hashCode6 + (widgetChatInputDiscoveryCommandsModel != null ? widgetChatInputDiscoveryCommandsModel.hashCode() : 0)) * 31;
            List<String> list4 = this.frecencyCommandIds;
            int hashCode8 = (hashCode7 + (list4 != null ? list4.hashCode() : 0)) * 31;
            List<ApplicationCommand> list5 = this.frecencyCommands;
            if (list5 != null) {
                i = list5.hashCode();
            }
            return hashCode8 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(userId=");
            R.append(this.userId);
            R.append(", userRoles=");
            R.append(this.userRoles);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", queriedCommands=");
            R.append(this.queriedCommands);
            R.append(", applications=");
            R.append(this.applications);
            R.append(", commandOptionAutocompleteItems=");
            R.append(this.commandOptionAutocompleteItems);
            R.append(", autocompletables=");
            R.append(this.autocompletables);
            R.append(", browserCommands=");
            R.append(this.browserCommands);
            R.append(", frecencyCommandIds=");
            R.append(this.frecencyCommandIds);
            R.append(", frecencyCommands=");
            return a.K(R, this.frecencyCommands, ")");
        }

        public /* synthetic */ StoreState(long j, List list, Channel channel, List list2, List list3, Map map, Map map2, WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel, List list4, List list5, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(j, list, channel, (i & 8) != 0 ? n.emptyList() : list2, (i & 16) != 0 ? n.emptyList() : list3, (i & 32) != 0 ? h0.emptyMap() : map, (i & 64) != 0 ? h0.emptyMap() : map2, (i & 128) != 0 ? null : widgetChatInputDiscoveryCommandsModel, (i & 256) != 0 ? n.emptyList() : list4, (i & 512) != 0 ? n.emptyList() : list5);
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            ApplicationCommandType.values();
            int[] iArr = new int[12];
            $EnumSwitchMapping$0 = iArr;
            iArr[ApplicationCommandType.USER.ordinal()] = 1;
            iArr[ApplicationCommandType.CHANNEL.ordinal()] = 2;
            iArr[ApplicationCommandType.ROLE.ordinal()] = 3;
            iArr[ApplicationCommandType.MENTIONABLE.ordinal()] = 4;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ AutocompleteViewModel(java.lang.Long r10, com.discord.stores.StoreApplicationCommands r11, com.discord.stores.StoreExperiments r12, int r13, int r14, int r15, rx.Observable r16, int r17, kotlin.jvm.internal.DefaultConstructorMarker r18) {
        /*
            r9 = this;
            r0 = r17 & 1
            if (r0 == 0) goto L7
            r0 = 0
            r2 = r0
            goto L8
        L7:
            r2 = r10
        L8:
            r0 = r17 & 2
            if (r0 == 0) goto L14
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreApplicationCommands r0 = r0.getApplicationCommands()
            r3 = r0
            goto L15
        L14:
            r3 = r11
        L15:
            r0 = r17 & 64
            if (r0 == 0) goto L2b
            com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel$Companion r0 = com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel.Companion
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreApplicationCommands r4 = r1.getApplicationCommands()
            com.discord.stores.StoreApplicationCommandFrecency r1 = r1.getApplicationComandFrecency()
            rx.Observable r0 = com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel.Companion.access$observeStores(r0, r2, r4, r1)
            r8 = r0
            goto L2d
        L2b:
            r8 = r16
        L2d:
            r1 = r9
            r4 = r12
            r5 = r13
            r6 = r14
            r7 = r15
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel.<init>(java.lang.Long, com.discord.stores.StoreApplicationCommands, com.discord.stores.StoreExperiments, int, int, int, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final InputEditTextAction applyCommandOptionSpans(MentionInputModel.VerifiedCommandInputModel verifiedCommandInputModel, ApplicationCommandOption applicationCommandOption, Set<ApplicationCommandOption> set) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (ApplicationCommandOption applicationCommandOption2 : verifiedCommandInputModel.getInputCommandOptionsRanges().keySet()) {
            boolean z2 = !set.contains(applicationCommandOption2);
            OptionRange optionRange = verifiedCommandInputModel.getInputCommandOptionsRanges().get(applicationCommandOption2);
            if (optionRange != null) {
                int last = optionRange.getParam().getLast();
                if (!m.areEqual(applicationCommandOption, applicationCommandOption2)) {
                    last = optionRange.getValue().getLast();
                }
                int i = last;
                linkedHashMap.put(new IntRange(optionRange.getParam().getFirst(), i), new SimpleRoundedBackgroundSpan(optionRange.getParam().getFirst(), i, DimenUtils.dpToPixels(4), DimenUtils.dpToPixels(4), this.defaultCommandOptionBackgroundColor, DimenUtils.dpToPixels(4), !z2 ? Integer.valueOf(this.commandOptionErrorColor) : null, false, AutocompleteViewModel$applyCommandOptionSpans$1$1$1.INSTANCE, 128, null));
            }
        }
        return new InputEditTextAction.ReplacePillSpans(verifiedCommandInputModel.getInput(), linkedHashMap);
    }

    private final void checkForCommandsToQuery(CharSequence charSequence, CharSequence charSequence2, Channel channel) {
        AutocompleteCommandUtils autocompleteCommandUtils = AutocompleteCommandUtils.INSTANCE;
        String commandPrefix = autocompleteCommandUtils.getCommandPrefix(charSequence);
        String commandPrefix2 = charSequence2 != null ? autocompleteCommandUtils.getCommandPrefix(charSequence2) : null;
        if (commandPrefix != null && (!m.areEqual(commandPrefix, commandPrefix2))) {
            queryCommandsFromCommandPrefixToken((String) w.split$default((CharSequence) commandPrefix, new String[]{" "}, false, 0, 6, (Object) null).get(0), channel);
        }
    }

    @MainThread
    private final InputEditTextAction generateSpanUpdates(MentionInputModel mentionInputModel) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<IntRange, Autocompletable> entry : mentionInputModel.getInputMentionsMap().entrySet()) {
            IntRange key = entry.getKey();
            Autocompletable value = entry.getValue();
            if (!(value instanceof ApplicationCommandAutocompletable)) {
                GuildRole guildRole = null;
                if (!(value instanceof RoleAutocompletable)) {
                    value = null;
                }
                RoleAutocompletable roleAutocompletable = (RoleAutocompletable) value;
                if (roleAutocompletable != null) {
                    guildRole = roleAutocompletable.getRole();
                }
                linkedHashMap.put(key, n.listOf((Object[]) new MetricAffectingSpan[]{new FontColorSpan(RoleUtils.getOpaqueColor(guildRole, this.defaultMentionColor)), new StyleSpan(1)}));
            }
        }
        return new InputEditTextAction.ReplaceCharacterStyleSpans(mentionInputModel.getInput(), linkedHashMap);
    }

    public static /* synthetic */ ApplicationCommandData getApplicationSendData$default(AutocompleteViewModel autocompleteViewModel, ApplicationCommandOption applicationCommandOption, int i, Object obj) {
        if ((i & 1) != 0) {
            applicationCommandOption = null;
        }
        return autocompleteViewModel.getApplicationSendData(applicationCommandOption);
    }

    private final AutocompleteViewState getAutocompleteViewState(String str, List<? extends Autocompletable> list, List<AutocompleteStickerItem> list2, boolean z2) {
        List mutableList = u.toMutableList((Collection) list);
        if (this.emojiAutocompleteUpsellEnabled) {
            boolean isReducedMotionEnabled = StoreStream.Companion.getAccessibility().isReducedMotionEnabled();
            List filterIsInstance = t.filterIsInstance(list, EmojiAutocompletable.class);
            ArrayList arrayList = new ArrayList();
            for (Object obj : filterIsInstance) {
                if (!((EmojiAutocompletable) obj).getEmoji().isUsable()) {
                    arrayList.add(obj);
                }
            }
            if (!arrayList.isEmpty()) {
                int size = arrayList.size();
                List<EmojiAutocompletable> take = u.take(arrayList, 3);
                ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(take, 10));
                for (EmojiAutocompletable emojiAutocompletable : take) {
                    arrayList2.add(emojiAutocompletable.getEmoji());
                }
                mutableList.add(new EmojiUpsellPlaceholder(size, arrayList2, isReducedMotionEnabled && ((EmojiAutocompletable) u.first((List<? extends Object>) arrayList)).getAnimationsEnabled()));
                mutableList.removeAll(arrayList);
            }
        }
        if (z2 || (!mutableList.isEmpty()) || (!list2.isEmpty())) {
            return new AutocompleteViewState.Autocomplete(false, false, z2, mutableList, list2, str, 2, null);
        }
        return AutocompleteViewState.Hidden.INSTANCE;
    }

    public static /* synthetic */ AutocompleteViewState getAutocompleteViewState$default(AutocompleteViewModel autocompleteViewModel, String str, List list, List list2, boolean z2, int i, Object obj) {
        if ((i & 8) != 0) {
            z2 = false;
        }
        return autocompleteViewModel.getAutocompleteViewState(str, list, list2, z2);
    }

    private final User getChannelBot(Channel channel) {
        User a = ChannelUtils.a(channel);
        if (a == null) {
            return null;
        }
        if (!a.isBot()) {
            a = null;
        }
        return a;
    }

    private final SelectedCommandViewState getSelectedCommandViewState(AutocompleteInputSelectionModel autocompleteInputSelectionModel) {
        InputSelectionModel inputSelectionModel = autocompleteInputSelectionModel.getInputSelectionModel();
        if (inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel) {
            ApplicationCommand selectedCommand = ((InputSelectionModel.CommandInputSelectionModel) autocompleteInputSelectionModel.getInputSelectionModel()).getInputModel().getInputCommandContext().getSelectedCommand();
            Application selectedApplication = ((InputSelectionModel.CommandInputSelectionModel) autocompleteInputSelectionModel.getInputSelectionModel()).getInputModel().getInputCommandContext().getSelectedApplication();
            ApplicationCommandOption selectedCommandOption = ((InputSelectionModel.CommandInputSelectionModel) autocompleteInputSelectionModel.getInputSelectionModel()).getSelectedCommandOption();
            Set<ApplicationCommandOption> showErrorsForOptions = autocompleteInputSelectionModel.getShowErrorsForOptions();
            Map<ApplicationCommandOption, Boolean> inputCommandOptionValidity = ((InputSelectionModel.CommandInputSelectionModel) autocompleteInputSelectionModel.getInputSelectionModel()).getInputModel().getInputCommandOptionValidity();
            if (selectedCommand == null || selectedApplication == null) {
                return SelectedCommandViewState.Hidden.INSTANCE;
            }
            return new SelectedCommandViewState.SelectedCommand(selectedCommand, selectedCommandOption, showErrorsForOptions, inputCommandOptionValidity, selectedApplication);
        } else if (inputSelectionModel instanceof InputSelectionModel.MessageInputSelectionModel) {
            return SelectedCommandViewState.Hidden.INSTANCE;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleAutocompleteInputSelectionModel(AutocompleteInputSelectionModel autocompleteInputSelectionModel) {
        AutocompleteViewState autocompleteViewState;
        CommandAutocompleteState commandAutocompleteState;
        StoreState storeState;
        WidgetChatInputDiscoveryCommandsModel browserCommands;
        WidgetChatInputDiscoveryCommandsModel browserCommands2;
        MentionToken autocompleteToken = autocompleteInputSelectionModel.getAutocompleteToken();
        AutocompleteViewState.Autocomplete autocomplete = null;
        r2 = null;
        Long l = null;
        autocomplete = null;
        autocomplete = null;
        autocomplete = null;
        if (m.areEqual(autocompleteInputSelectionModel.getInputSelectionModel().getInputModel().getInput().toString(), COMMAND_DISCOVER_TOKEN)) {
            StoreState storeState2 = this.storeState;
            if (!(storeState2 == null || (browserCommands2 = storeState2.getBrowserCommands()) == null)) {
                l = browserCommands2.getJumpedApplicationId();
            }
            if (l != null && ((storeState = this.storeState) == null || (browserCommands = storeState.getBrowserCommands()) == null || browserCommands.getJumpedSequenceId() != this.lastJumpedSequenceId)) {
                Integer num = this.inputState.getApplicationsPosition().get(l);
                this.events.onNext(new Event.ScrollAutocompletablesToApplication(l.longValue(), num != null ? num.intValue() : 3));
            }
            autocompleteViewState = getApplicationCommandsBrowserViewState();
        } else if (autocompleteToken != null) {
            boolean z2 = false;
            if (autocompleteInputSelectionModel.getInputSelectionModel() instanceof InputSelectionModel.CommandInputSelectionModel) {
                ApplicationCommandOption selectedCommandOption = ((InputSelectionModel.CommandInputSelectionModel) autocompleteInputSelectionModel.getInputSelectionModel()).getSelectedCommandOption();
                Map<String, CommandAutocompleteState> map = ((InputSelectionModel.CommandInputSelectionModel) autocompleteInputSelectionModel.getInputSelectionModel()).getInputModel().getCommandOptionAutocompleteItems().get(selectedCommandOption != null ? selectedCommandOption.getName() : null);
                if (map != null) {
                    CommandOptionValue commandOptionValue = ((InputSelectionModel.CommandInputSelectionModel) autocompleteInputSelectionModel.getInputSelectionModel()).getInputModel().getInputCommandOptionValues().get(selectedCommandOption);
                    commandAutocompleteState = map.get(String.valueOf(commandOptionValue != null ? commandOptionValue.getValue() : null));
                } else {
                    commandAutocompleteState = null;
                }
                if (selectedCommandOption != null && selectedCommandOption.getAutocomplete()) {
                    z2 = true;
                }
                if (selectedCommandOption != null && selectedCommandOption.getAutocomplete() && !(commandAutocompleteState instanceof CommandAutocompleteState.Choices)) {
                    autocomplete = new AutocompleteViewState.Autocomplete(true, commandAutocompleteState instanceof CommandAutocompleteState.Failed, true, n.emptyList(), n.emptyList(), autocompleteToken.getToken());
                }
            }
            if (autocomplete == null) {
                List<Autocompletable> filterMentionsFromToken = AutocompleteModelUtils.INSTANCE.filterMentionsFromToken(autocompleteToken, autocompleteInputSelectionModel.getInputSelectionModel(), autocompleteInputSelectionModel.getFilteredAutocompletables());
                List<Sticker> stickerMatches = autocompleteInputSelectionModel.getStickerMatches();
                ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(stickerMatches, 10));
                for (Sticker sticker : stickerMatches) {
                    arrayList.add(new AutocompleteStickerItem(sticker, null, 0, 6, null));
                }
                autocompleteViewState = getAutocompleteViewState(autocompleteToken.getToken(), filterMentionsFromToken, arrayList, z2);
            } else {
                autocompleteViewState = autocomplete;
            }
        } else {
            autocompleteViewState = AutocompleteViewState.Hidden.INSTANCE;
        }
        SelectedCommandViewState selectedCommandViewState = getSelectedCommandViewState(autocompleteInputSelectionModel);
        ApplicationCommand newSelectedCommand = newSelectedCommand(autocompleteInputSelectionModel, this.lastAutocompleteInputSelectionModel);
        if (newSelectedCommand != null) {
            StoreStream.Companion companion = StoreStream.Companion;
            StoreAnalytics analytics = companion.getAnalytics();
            long id2 = companion.getChannelsSelected().getId();
            long applicationId = newSelectedCommand.getApplicationId();
            Long longOrNull = s.toLongOrNull(newSelectedCommand.getId());
            analytics.trackApplicationCommandSelected(id2, applicationId, longOrNull != null ? longOrNull.longValue() : 0L);
        }
        if (newSelectedCommandOption(autocompleteInputSelectionModel, this.lastAutocompleteInputSelectionModel) != null) {
            this.storeApplicationCommands.clearAutocompleteResults();
        }
        this.lastAutocompleteInputSelectionModel = autocompleteInputSelectionModel;
        updateViewState(new ViewState(autocompleteViewState, selectedCommandViewState));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleInputSelectionModel(InputSelectionModel inputSelectionModel) {
        Set<ApplicationCommandOption> set;
        MentionToken mentionToken;
        if (inputSelectionModel.getSelection().getLast() <= inputSelectionModel.getInputModel().getInput().length()) {
            InputSelectionModel inputSelectionModel2 = this.lastChatInputModel;
            ApplicationCommandOption applicationCommandOption = null;
            InputEditTextAction replacePillSpans = new InputEditTextAction.ReplacePillSpans(inputSelectionModel.getInputModel().getInput(), null, 2, null);
            boolean z2 = inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel;
            if (z2) {
                AutocompleteCommandUtils autocompleteCommandUtils = AutocompleteCommandUtils.INSTANCE;
                InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
                ApplicationCommand selectedCommand = commandInputSelectionModel.getInputModel().getInputCommandContext().getSelectedCommand();
                ApplicationCommandOption selectedCommandOption = commandInputSelectionModel.getSelectedCommandOption();
                if (!(inputSelectionModel2 instanceof InputSelectionModel.CommandInputSelectionModel)) {
                    inputSelectionModel2 = null;
                }
                InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel2 = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel2;
                if (commandInputSelectionModel2 != null) {
                    applicationCommandOption = commandInputSelectionModel2.getSelectedCommandOption();
                }
                set = u.toMutableSet(autocompleteCommandUtils.getErrorsToShowForCommandParameters(selectedCommand, selectedCommandOption, applicationCommandOption, commandInputSelectionModel.getInputModel().getInputCommandOptionValidity(), commandInputSelectionModel.getInputModel().getInputCommandOptionValues()));
                ApplicationCommandOption showErrorForOption = this.inputState.getShowErrorForOption();
                if (showErrorForOption != null) {
                    if (m.areEqual(commandInputSelectionModel.getInputModel().getInputCommandOptionValidity().get(showErrorForOption), Boolean.TRUE) || (!m.areEqual(commandInputSelectionModel.getSelectedCommandOption(), showErrorForOption))) {
                        this.inputState = InputState.copy$default(this.inputState, null, null, null, null, null, 23, null);
                    } else {
                        set.add(showErrorForOption);
                    }
                }
                replacePillSpans = applyCommandOptionSpans(commandInputSelectionModel.getInputModel(), commandInputSelectionModel.getSelectedCommandOption(), set);
            } else {
                set = n0.emptySet();
            }
            Set<ApplicationCommandOption> set2 = set;
            this.editTextAction.onNext(replacePillSpans);
            if (z2) {
                InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel3 = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
                if (commandInputSelectionModel3.getSelectedCommandOption() != null) {
                    mentionToken = AutocompleteModelUtils.INSTANCE.getCommandAutocompleteToken(commandInputSelectionModel3.getInputModel().getInput(), inputSelectionModel.getSelection(), commandInputSelectionModel3.getSelectedCommandOption(), InputSelectionModelKt.hasSelectedFreeformInput(inputSelectionModel), commandInputSelectionModel3.getInputModel().getInputCommandOptionsRanges(), commandInputSelectionModel3.getInputModel().getInputCommandOptionValues());
                    MentionToken mentionToken2 = mentionToken;
                    AutocompleteModelUtils autocompleteModelUtils = AutocompleteModelUtils.INSTANCE;
                    List<Sticker> stickerMatches = autocompleteModelUtils.getStickerMatches(mentionToken2);
                    Map<LeadingIdentifier, List<Autocompletable>> filterAutocompletablesForInputContext = autocompleteModelUtils.filterAutocompletablesForInputContext(inputSelectionModel);
                    this.lastChatInputModel = inputSelectionModel;
                    this.autocompleteInputSelectionModelSubject.onNext(new AutocompleteInputSelectionModel(mentionToken2, filterAutocompletablesForInputContext, inputSelectionModel, set2, stickerMatches));
                }
            }
            mentionToken = AutocompleteModelUtils.INSTANCE.getMessageAutocompleteToken(inputSelectionModel.getInputModel().getInput(), inputSelectionModel.getSelection());
            MentionToken mentionToken22 = mentionToken;
            AutocompleteModelUtils autocompleteModelUtils2 = AutocompleteModelUtils.INSTANCE;
            List<Sticker> stickerMatches2 = autocompleteModelUtils2.getStickerMatches(mentionToken22);
            Map<LeadingIdentifier, List<Autocompletable>> filterAutocompletablesForInputContext2 = autocompleteModelUtils2.filterAutocompletablesForInputContext(inputSelectionModel);
            this.lastChatInputModel = inputSelectionModel;
            this.autocompleteInputSelectionModelSubject.onNext(new AutocompleteInputSelectionModel(mentionToken22, filterAutocompletablesForInputContext2, inputSelectionModel, set2, stickerMatches2));
        }
    }

    @MainThread
    private final void handleNewChannel(CharSequence charSequence, Channel channel, Channel channel2) {
        User channelBot = getChannelBot(channel2);
        long j = 0;
        String str = null;
        Long valueOf = channel2.f() == 0 ? null : Long.valueOf(channel2.f());
        boolean z2 = !m.areEqual(channel != null ? Long.valueOf(channel.f()) : null, valueOf);
        if (channelBot != null) {
            this.storeApplicationCommands.handleDmUserApplication(channelBot);
        } else if (z2) {
            StoreApplicationCommands storeApplicationCommands = this.storeApplicationCommands;
            if (valueOf != null) {
                j = valueOf.longValue();
            }
            storeApplicationCommands.requestFrecencyCommands(j);
            this.storeApplicationCommands.requestApplications(valueOf);
        }
        if (charSequence != null) {
            str = AutocompleteCommandUtils.INSTANCE.getCommandPrefix(charSequence);
        }
        if (str != null) {
            queryCommandsFromCommandPrefixToken(str, channel2);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final InputSelectionModel handleSelectionWithInputModel(IntRange intRange, MentionInputModel mentionInputModel) {
        if (mentionInputModel instanceof MentionInputModel.VerifiedCommandInputModel) {
            MentionInputModel.VerifiedCommandInputModel verifiedCommandInputModel = (MentionInputModel.VerifiedCommandInputModel) mentionInputModel;
            return new InputSelectionModel.CommandInputSelectionModel(verifiedCommandInputModel, intRange, AutocompleteCommandUtils.INSTANCE.getSelectedCommandOption(intRange.getFirst(), verifiedCommandInputModel.getInputCommandOptionsRanges()));
        } else if (mentionInputModel instanceof MentionInputModel.VerifiedMessageInputModel) {
            return new InputSelectionModel.MessageInputSelectionModel(mentionInputModel, intRange);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        Channel channel;
        StoreState storeState2 = this.storeState;
        WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel = null;
        if (storeState2 == null || (channel = storeState2.getChannel()) == null || channel.h() != storeState.getChannel().h()) {
            CharSequence currentInput = this.inputState.getCurrentInput();
            StoreState storeState3 = this.storeState;
            handleNewChannel(currentInput, storeState3 != null ? storeState3.getChannel() : null, storeState.getChannel());
        }
        StoreState storeState4 = this.storeState;
        if (!m.areEqual(storeState4 != null ? storeState4.getFrecencyCommandIds() : null, storeState.getFrecencyCommandIds())) {
            this.storeApplicationCommands.requestFrecencyCommands(storeState.getChannel().f());
        }
        StoreState storeState5 = this.storeState;
        if (storeState5 != null) {
            widgetChatInputDiscoveryCommandsModel = storeState5.getBrowserCommands();
        }
        if ((!m.areEqual(widgetChatInputDiscoveryCommandsModel, storeState.getBrowserCommands())) && storeState.getBrowserCommands() != null) {
            this.inputState = InputState.copy$default(this.inputState, null, null, getCommandBrowserCommandPositions(storeState.getBrowserCommands()), null, null, 27, null);
        }
        this.storeState = storeState;
        onDataUpdated(this.inputState, storeState);
    }

    private final ApplicationCommand newSelectedCommand(AutocompleteInputSelectionModel autocompleteInputSelectionModel, AutocompleteInputSelectionModel autocompleteInputSelectionModel2) {
        ApplicationCommand selectedCommand;
        MentionInputModel.VerifiedCommandInputModel inputModel;
        InputCommandContext inputCommandContext;
        MentionInputModel.VerifiedCommandInputModel inputModel2;
        InputCommandContext inputCommandContext2;
        InputSelectionModel inputSelectionModel = autocompleteInputSelectionModel.getInputSelectionModel();
        if (!(inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel)) {
            inputSelectionModel = null;
        }
        InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
        ApplicationCommand selectedCommand2 = (commandInputSelectionModel == null || (inputModel2 = commandInputSelectionModel.getInputModel()) == null || (inputCommandContext2 = inputModel2.getInputCommandContext()) == null) ? null : inputCommandContext2.getSelectedCommand();
        InputSelectionModel inputSelectionModel2 = autocompleteInputSelectionModel2 != null ? autocompleteInputSelectionModel2.getInputSelectionModel() : null;
        if (!(inputSelectionModel2 instanceof InputSelectionModel.CommandInputSelectionModel)) {
            inputSelectionModel2 = null;
        }
        InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel2 = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel2;
        if (!m.areEqual(selectedCommand2 != null ? selectedCommand2.getId() : null, ((commandInputSelectionModel2 == null || (inputModel = commandInputSelectionModel2.getInputModel()) == null || (inputCommandContext = inputModel.getInputCommandContext()) == null) ? null : inputCommandContext.getSelectedCommand()) != null ? selectedCommand.getId() : null)) {
            return selectedCommand2;
        }
        return null;
    }

    private final ApplicationCommandOption newSelectedCommandOption(AutocompleteInputSelectionModel autocompleteInputSelectionModel, AutocompleteInputSelectionModel autocompleteInputSelectionModel2) {
        ApplicationCommandOption selectedCommandOption;
        InputSelectionModel inputSelectionModel = autocompleteInputSelectionModel.getInputSelectionModel();
        if (!(inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel)) {
            inputSelectionModel = null;
        }
        InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
        ApplicationCommandOption selectedCommandOption2 = commandInputSelectionModel != null ? commandInputSelectionModel.getSelectedCommandOption() : null;
        InputSelectionModel inputSelectionModel2 = autocompleteInputSelectionModel2 != null ? autocompleteInputSelectionModel2.getInputSelectionModel() : null;
        if (!(inputSelectionModel2 instanceof InputSelectionModel.CommandInputSelectionModel)) {
            inputSelectionModel2 = null;
        }
        InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel2 = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel2;
        if (!m.areEqual(selectedCommandOption2 != null ? selectedCommandOption2.getName() : null, (commandInputSelectionModel2 != null ? commandInputSelectionModel2.getSelectedCommandOption() : null) != null ? selectedCommandOption.getName() : null)) {
            return selectedCommandOption2;
        }
        return null;
    }

    @MainThread
    private final InputEditTextAction onPreAutocompleteCompute(CharSequence charSequence) {
        return replacementSpanCommandParamDeletion(charSequence);
    }

    @MainThread
    private final void queryCommandsFromCommandPrefixToken(String str, Channel channel) {
        User channelBot = getChannelBot(channel);
        Long valueOf = channelBot != null ? Long.valueOf(channelBot.getId()) : null;
        long f = channel.f();
        if (str.length() <= 1 && valueOf == null) {
            this.storeApplicationCommands.clearQueryCommands();
        }
        if (str.length() == 1) {
            this.storeApplicationCommands.requestInitialApplicationCommands(Long.valueOf(f), valueOf, true);
        } else if (str.length() > 1 && valueOf == null) {
            StoreApplicationCommands storeApplicationCommands = this.storeApplicationCommands;
            Long valueOf2 = Long.valueOf(f);
            String substring = str.substring(1);
            m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
            storeApplicationCommands.requestApplicationCommandsQuery(valueOf2, substring);
        }
    }

    @MainThread
    private final InputEditTextAction replacementSpanCommandParamDeletion(CharSequence charSequence) {
        ReplacementSpan[] replacementSpanArr;
        String obj = charSequence.toString();
        SpannedString valueOf = SpannedString.valueOf(charSequence);
        m.checkNotNullExpressionValue(valueOf, "valueOf(this)");
        Object[] spans = valueOf.getSpans(0, charSequence.length(), ReplacementSpan.class);
        m.checkNotNullExpressionValue(spans, "getSpans(start, end, T::class.java)");
        for (ReplacementSpan replacementSpan : (ReplacementSpan[]) spans) {
            IntRange intRange = new IntRange(valueOf.getSpanStart(replacementSpan), valueOf.getSpanEnd(replacementSpan));
            Character orNull = y.getOrNull(obj, intRange.getLast());
            boolean z2 = orNull != null && orNull.charValue() == ':';
            int first = intRange.getFirst();
            int last = intRange.getLast();
            Objects.requireNonNull(obj, "null cannot be cast to non-null type java.lang.String");
            String substring = obj.substring(first, last);
            m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            if (!(w.contains$default((CharSequence) substring, (char) MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, false, 2, (Object) null) || z2)) {
                return new InputEditTextAction.RemoveText(obj, new IntRange(intRange.getFirst(), intRange.getLast()), intRange.getFirst());
            }
        }
        return new InputEditTextAction.None(charSequence);
    }

    @MainThread
    private final void selectFirstInvalidCommandOption() {
        InputSelectionModel inputSelectionModel = this.lastChatInputModel;
        MentionInputModel inputModel = inputSelectionModel != null ? inputSelectionModel.getInputModel() : null;
        if (inputModel instanceof MentionInputModel.VerifiedCommandInputModel) {
            Map<ApplicationCommandOption, Boolean> inputCommandOptionValidity = ((MentionInputModel.VerifiedCommandInputModel) inputModel).getInputCommandOptionValidity();
            for (ApplicationCommandOption applicationCommandOption : inputCommandOptionValidity.keySet()) {
                if (m.areEqual(inputCommandOptionValidity.get(applicationCommandOption), Boolean.FALSE)) {
                    this.inputState = InputState.copy$default(this.inputState, null, null, null, applicationCommandOption, null, 23, null);
                    selectCommandOption(applicationCommandOption);
                    return;
                }
            }
        }
    }

    public final void checkEmojiAutocompleteUpsellViewed(List<? extends Autocompletable> list) {
        Autocompletable autocompletable;
        m.checkNotNullParameter(list, "visibleItems");
        if (this.emojiAutocompleteUpsellEnabled && this.logEmojiAutocompleteUpsellViewed) {
            ListIterator<? extends Autocompletable> listIterator = list.listIterator(list.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    autocompletable = null;
                    break;
                }
                autocompletable = listIterator.previous();
                if (autocompletable instanceof EmojiUpsellPlaceholder) {
                    break;
                }
            }
            if (autocompletable != null) {
                this.logEmojiAutocompleteUpsellViewed = false;
                StoreStream.Companion.getAnalytics().emojiAutocompleteUpsellInlineViewed();
            }
        }
    }

    @MainThread
    public final AutocompleteViewState getApplicationCommandsBrowserViewState() {
        Experiment userExperiment;
        StoreState storeState = this.storeState;
        if (storeState == null || storeState.getBrowserCommands() == null) {
            return AutocompleteViewState.Hidden.INSTANCE;
        }
        boolean z2 = (storeState.getFrecencyCommands().isEmpty() ^ true) && (userExperiment = this.storeExperiments.getUserExperiment("2021-09_android_app_commands_frecency", true)) != null && userExperiment.getBucket() == 1;
        List<ApplicationCommand> frecencyCommands = z2 ? storeState.getFrecencyCommands() : n.emptyList();
        List<Application> applications = storeState.getApplications();
        ArrayList arrayList = new ArrayList();
        for (Object obj : applications) {
            Application application = (Application) obj;
            if (application.getCommandCount() > 0 || (z2 && application.getId() == -2 && (storeState.getFrecencyCommands().isEmpty() ^ true))) {
                arrayList.add(obj);
            }
        }
        return new AutocompleteViewState.CommandBrowser(frecencyCommands, arrayList, storeState.getBrowserCommands());
    }

    public final ApplicationCommandData getApplicationSendData(ApplicationCommandOption applicationCommandOption) {
        AutocompleteModelUtils autocompleteModelUtils = AutocompleteModelUtils.INSTANCE;
        AutocompleteInputSelectionModel autocompleteInputSelectionModel = this.lastAutocompleteInputSelectionModel;
        StoreState storeState = this.storeState;
        List<ApplicationCommand> list = null;
        List<Application> applications = storeState != null ? storeState.getApplications() : null;
        if (applications == null) {
            applications = n.emptyList();
        }
        StoreState storeState2 = this.storeState;
        if (storeState2 != null) {
            list = storeState2.getQueriedCommands();
        }
        if (list == null) {
            list = n.emptyList();
        }
        return autocompleteModelUtils.getApplicationSendData(autocompleteInputSelectionModel, applicationCommandOption, applications, list);
    }

    public final Long getChannelId() {
        return this.channelId;
    }

    @MainThread
    public final Map<Long, Integer> getCommandBrowserCommandPositions(WidgetChatInputDiscoveryCommandsModel widgetChatInputDiscoveryCommandsModel) {
        m.checkNotNullParameter(widgetChatInputDiscoveryCommandsModel, "discoveryCommands");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        int i = 0;
        for (Pair<Application, List<Autocompletable>> pair : widgetChatInputDiscoveryCommandsModel.getCommandsByApplication()) {
            Application component1 = pair.component1();
            List<Autocompletable> component2 = pair.component2();
            Iterator<Autocompletable> it = component2.iterator();
            int i2 = 0;
            while (true) {
                if (!it.hasNext()) {
                    i2 = -1;
                    break;
                } else if (!(it.next() instanceof ApplicationCommandLoadingPlaceholder)) {
                    break;
                } else {
                    i2++;
                }
            }
            linkedHashMap.put(Long.valueOf(component1.getId()), Integer.valueOf(f.coerceAtLeast(i2, 0) + i));
            i += component2.size();
        }
        return linkedHashMap;
    }

    public final int getCommandOptionErrorColor() {
        return this.commandOptionErrorColor;
    }

    public final int getDefaultCommandOptionBackgroundColor() {
        return this.defaultCommandOptionBackgroundColor;
    }

    public final int getDefaultMentionColor() {
        return this.defaultMentionColor;
    }

    public final boolean getEmojiAutocompleteUpsellEnabled() {
        return this.emojiAutocompleteUpsellEnabled;
    }

    public final InputState getInputState() {
        return this.inputState;
    }

    public final int getLastJumpedSequenceId() {
        return this.lastJumpedSequenceId;
    }

    public final StoreApplicationCommands getStoreApplicationCommands() {
        return this.storeApplicationCommands;
    }

    public final StoreState getStoreState() {
        return this.storeState;
    }

    public final Observable<InputEditTextAction> observeEditTextActions() {
        BehaviorSubject<InputEditTextAction> behaviorSubject = this.editTextAction;
        m.checkNotNullExpressionValue(behaviorSubject, "editTextAction");
        return behaviorSubject;
    }

    public final Observable<Event> observeEvents() {
        BehaviorSubject<Event> behaviorSubject = this.events;
        m.checkNotNullExpressionValue(behaviorSubject, "events");
        return behaviorSubject;
    }

    public final void onApplicationCommandSendError() {
        InputSelectionModel inputSelectionModel = this.lastChatInputModel;
        Object obj = null;
        MentionInputModel inputModel = inputSelectionModel != null ? inputSelectionModel.getInputModel() : null;
        if (inputModel instanceof MentionInputModel.VerifiedCommandInputModel) {
            MentionInputModel.VerifiedCommandInputModel verifiedCommandInputModel = (MentionInputModel.VerifiedCommandInputModel) inputModel;
            ApplicationCommand selectedCommand = verifiedCommandInputModel.getInputCommandContext().getSelectedCommand();
            if (selectedCommand != null) {
                Map<ApplicationCommandOption, Boolean> inputCommandOptionValidity = verifiedCommandInputModel.getInputCommandOptionValidity();
                Iterator<T> it = selectedCommand.getOptions().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Object next = it.next();
                    if (m.areEqual(inputCommandOptionValidity.get((ApplicationCommandOption) next), Boolean.FALSE)) {
                        obj = next;
                        break;
                    }
                }
                ApplicationCommandOption applicationCommandOption = (ApplicationCommandOption) obj;
                if (applicationCommandOption != null) {
                    long applicationId = selectedCommand.getApplicationId();
                    Long longOrNull = selectedCommand instanceof ApplicationSubCommand ? s.toLongOrNull(((ApplicationSubCommand) selectedCommand).getRootCommand().getId()) : s.toLongOrNull(selectedCommand.getId());
                    if (longOrNull != null) {
                        long longValue = longOrNull.longValue();
                        selectFirstInvalidCommandOption();
                        StoreStream.Companion.getAnalytics().trackApplicationCommandValidationFailure(applicationId, longValue, applicationCommandOption.getType().toString(), applicationCommandOption.getRequired());
                    }
                }
            }
        }
    }

    public final void onAutocompleteItemsUpdated() {
        this.logEmojiAutocompleteUpsellViewed = true;
    }

    public final void onCommandsBrowserScroll(int i, int i2, int i3) {
        WidgetChatInputDiscoveryCommandsModel browserCommands;
        WidgetChatInputDiscoveryCommandsModel browserCommands2;
        StoreState storeState = this.storeState;
        if (storeState == null || (browserCommands2 = storeState.getBrowserCommands()) == null || !browserCommands2.getHasMoreBefore() || i > 3) {
            StoreState storeState2 = this.storeState;
            if (storeState2 != null && (browserCommands = storeState2.getBrowserCommands()) != null && browserCommands.getHasMoreAfter() && i2 >= (i3 - 3) - 6) {
                StoreStream.Companion.getApplicationCommands().requestLoadMoreDown();
            }
        } else {
            StoreStream.Companion.getApplicationCommands().requestLoadMoreUp();
        }
        StoreStream.Companion.getAnalytics().trackApplicationCommandBrowserScrolled();
    }

    @MainThread
    public final InputEditTextAction onDataUpdated(InputState inputState, StoreState storeState) {
        MentionInputModel mentionInputModel;
        InputEditTextAction inputEditTextAction;
        m.checkNotNullParameter(inputState, "inputState");
        if (storeState == null) {
            return new InputEditTextAction.None(inputState.getCurrentInput());
        }
        AutocompleteApplicationCommands autocompleteApplicationCommands = new AutocompleteApplicationCommands(storeState.getApplications(), storeState.getQueriedCommands(), storeState.getBrowserCommands());
        AutocompleteInputModel autocompleteInputModel = new AutocompleteInputModel(inputState.getCurrentInput(), storeState.getAutocompletables(), autocompleteApplicationCommands);
        AutocompleteCommandUtils autocompleteCommandUtils = AutocompleteCommandUtils.INSTANCE;
        InputCommandContext inputCommandContext = autocompleteCommandUtils.getInputCommandContext(autocompleteInputModel.getInput(), storeState.getUserId(), storeState.getUserRoles(), autocompleteApplicationCommands, inputState.getSelectedCommand());
        ChatInputMentionsMap mapInputToMentions = AutocompleteModelUtils.INSTANCE.mapInputToMentions(autocompleteInputModel.getInput().toString(), storeState.getAutocompletables(), inputState.getInputAutocompleteMap(), inputCommandContext.isCommand());
        if (inputCommandContext.getSelectedCommand() != null) {
            ApplicationCommand selectedCommand = inputCommandContext.getSelectedCommand();
            Map<ApplicationCommandOption, OptionRange> findOptionRanges = autocompleteCommandUtils.findOptionRanges(autocompleteInputModel.getInput(), selectedCommand);
            Map<ApplicationCommandOption, CommandOptionValue> parseCommandOptions = autocompleteCommandUtils.parseCommandOptions(autocompleteInputModel.getInput(), selectedCommand, mapInputToMentions);
            mentionInputModel = new MentionInputModel.VerifiedCommandInputModel(autocompleteInputModel.getInput(), autocompleteInputModel.getAutocompletables(), mapInputToMentions.getMentions(), null, null, null, storeState.getCommandOptionAutocompleteItems(), inputCommandContext, autocompleteInputModel.getApplicationCommands(), parseCommandOptions, findOptionRanges, autocompleteCommandUtils.getInputValidity(selectedCommand, parseCommandOptions), 56, null);
        } else {
            mentionInputModel = new MentionInputModel.VerifiedMessageInputModel(autocompleteInputModel.getInput(), autocompleteInputModel.getAutocompletables(), mapInputToMentions.getMentions());
        }
        if (mentionInputModel instanceof MentionInputModel.VerifiedCommandInputModel) {
            InputEditTextAction appendTextForCommandForInput = autocompleteCommandUtils.appendTextForCommandForInput((MentionInputModel.VerifiedCommandInputModel) mentionInputModel);
            if (appendTextForCommandForInput instanceof InputEditTextAction.InsertText) {
                return appendTextForCommandForInput;
            }
        }
        if (inputState.getCurrentInput().length() == 0) {
            inputEditTextAction = new InputEditTextAction.ClearSpans(inputState.getCurrentInput());
        } else if (m.areEqual(inputState.getCurrentInput().toString(), COMMAND_DISCOVER_TOKEN)) {
            inputEditTextAction = new InputEditTextAction.ClearSpans(inputState.getCurrentInput());
        } else {
            inputEditTextAction = generateSpanUpdates(mentionInputModel);
        }
        this.inputMentionModelSubject.onNext(mentionInputModel);
        this.inputState = InputState.copy$default(inputState, null, inputCommandContext.getSelectedCommand(), null, null, mapInputToMentions.getMentions(), 13, null);
        return inputEditTextAction;
    }

    @MainThread
    public final InputEditTextAction onInputTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        Channel channel;
        m.checkNotNullParameter(charSequence, "input");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<IntRange, Autocompletable> entry : this.inputState.getInputAutocompleteMap().entrySet()) {
            if (charSequence.length() < entry.getKey().getLast() || !entry.getValue().matchesText(charSequence.subSequence(entry.getKey().getFirst(), entry.getKey().getLast()).toString())) {
                IntRange shiftOrRemove = AutocompleteModelUtils.INSTANCE.shiftOrRemove(entry.getKey(), i, i2, i3);
                if (shiftOrRemove != null) {
                    linkedHashMap.put(shiftOrRemove, entry.getValue());
                }
            } else {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        this.inputState = InputState.copy$default(this.inputState, null, null, null, null, linkedHashMap, 15, null);
        InputEditTextAction onPreAutocompleteCompute = onPreAutocompleteCompute(charSequence);
        if (!(onPreAutocompleteCompute instanceof InputEditTextAction.None)) {
            return onPreAutocompleteCompute;
        }
        StoreState storeState = this.storeState;
        if (!(storeState == null || (channel = storeState.getChannel()) == null)) {
            checkForCommandsToQuery(charSequence, this.inputState.getCurrentInput(), channel);
        }
        InputState inputState = this.inputState;
        SpannableString valueOf = SpannableString.valueOf(charSequence.toString());
        m.checkNotNullExpressionValue(valueOf, "valueOf(this)");
        return onDataUpdated(InputState.copy$default(inputState, valueOf, null, null, null, null, 30, null), this.storeState);
    }

    public final void onSelectionChanged(String str, int i, int i2) {
        m.checkNotNullParameter(str, "input");
        this.inputSelectionSubject.onNext(new SelectionState(str, new IntRange(i, i2)));
    }

    public final MessageContent replaceAutocompletableDataWithServerValues(String str) {
        MentionInputModel inputModel;
        Map<IntRange, Autocompletable> inputMentionsMap;
        m.checkNotNullParameter(str, "content");
        InputSelectionModel inputSelectionModel = this.lastChatInputModel;
        if (inputSelectionModel == null || (inputModel = inputSelectionModel.getInputModel()) == null || (inputMentionsMap = inputModel.getInputMentionsMap()) == null) {
            return new MessageContent(str, n.emptyList());
        }
        String replaceAutocompleteDataWithServerValues = AutocompleteExtensionsKt.replaceAutocompleteDataWithServerValues(str, inputMentionsMap);
        List<UserAutocompletable> filterIsInstance = t.filterIsInstance(inputMentionsMap.values(), UserAutocompletable.class);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(filterIsInstance, 10));
        for (UserAutocompletable userAutocompletable : filterIsInstance) {
            arrayList.add(userAutocompletable.getUser());
        }
        int length = replaceAutocompleteDataWithServerValues.length() - 1;
        int i = 0;
        boolean z2 = false;
        while (i <= length) {
            boolean z3 = m.compare(replaceAutocompleteDataWithServerValues.charAt(!z2 ? i : length), 32) <= 0;
            if (!z2) {
                if (!z3) {
                    z2 = true;
                } else {
                    i++;
                }
            } else if (!z3) {
                break;
            } else {
                length--;
            }
        }
        return new MessageContent(replaceAutocompleteDataWithServerValues.subSequence(i, length + 1).toString(), arrayList);
    }

    @MainThread
    public final void selectAutocompleteItem(Autocompletable autocompletable) {
        IntRange intRange;
        InputSelectionModel inputSelectionModel;
        MentionInputModel inputModel;
        CharSequence input;
        IntRange intRange2;
        MentionInputModel inputModel2;
        CharSequence input2;
        m.checkNotNullParameter(autocompletable, "autocompletable");
        String str = "";
        if (autocompletable instanceof ApplicationCommandAutocompletable) {
            StringBuilder sb = new StringBuilder();
            sb.append(autocompletable.leadingIdentifier().getIdentifier());
            ApplicationCommandAutocompletable applicationCommandAutocompletable = (ApplicationCommandAutocompletable) autocompletable;
            sb.append(applicationCommandAutocompletable.getCommand().getName());
            sb.append(' ');
            String sb2 = sb.toString();
            this.inputState = InputState.copy$default(this.inputState, null, applicationCommandAutocompletable.getCommand(), null, null, null, 29, null);
            BehaviorSubject<InputEditTextAction> behaviorSubject = this.editTextAction;
            InputSelectionModel inputSelectionModel2 = this.lastChatInputModel;
            behaviorSubject.onNext(new InputEditTextAction.ReplaceText((inputSelectionModel2 == null || (inputModel2 = inputSelectionModel2.getInputModel()) == null || (input2 = inputModel2.getInput()) == null) ? str : input2, sb2, 0, 4, null));
            return;
        }
        InputSelectionModel inputSelectionModel3 = this.lastChatInputModel;
        if (inputSelectionModel3 instanceof InputSelectionModel.CommandInputSelectionModel) {
            InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel3;
            commandInputSelectionModel.getInputModel().getInputCommandContext().getSelectedCommand();
            ApplicationCommandOption selectedCommandOption = commandInputSelectionModel.getSelectedCommandOption();
            OptionRange optionRange = commandInputSelectionModel.getInputModel().getInputCommandOptionsRanges().get(selectedCommandOption);
            if ((!autocompletable.getInputTextMatchers().isEmpty()) && selectedCommandOption != null && optionRange != null) {
                String str2 = (String) u.first((List<? extends Object>) autocompletable.getInputTextMatchers());
                String str3 = str2 + ' ';
                IntRange selection = inputSelectionModel3.getSelection();
                if (InputSelectionModelKt.hasSelectedFreeformInput(inputSelectionModel3)) {
                    intRange2 = new IntRange(selection.getFirst() - 1, selection.getLast());
                } else {
                    intRange2 = optionRange.getValue();
                }
                InputState inputState = this.inputState;
                Map mutableMap = h0.toMutableMap(inputState.getInputAutocompleteMap());
                mutableMap.put(new IntRange(optionRange.getValue().getFirst(), str2.length() + optionRange.getValue().getFirst()), autocompletable);
                this.inputState = InputState.copy$default(inputState, null, null, null, null, mutableMap, 15, null);
                InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel2 = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel3;
                this.editTextAction.onNext(new InputEditTextAction.InsertText(commandInputSelectionModel2.getInputModel().getInput(), str3, intRange2, (str3.length() + commandInputSelectionModel2.getInputModel().getInput().length()) - (intRange2.getLast() - intRange2.getFirst())));
            }
        } else if ((inputSelectionModel3 instanceof InputSelectionModel.MessageInputSelectionModel) || inputSelectionModel3 == null) {
            AutocompleteInputSelectionModel autocompleteInputSelectionModel = this.lastAutocompleteInputSelectionModel;
            if (!(autocompleteInputSelectionModel == null || (inputSelectionModel = autocompleteInputSelectionModel.getInputSelectionModel()) == null || (inputModel = inputSelectionModel.getInputModel()) == null || (input = inputModel.getInput()) == null)) {
                str = input;
            }
            AutocompleteInputSelectionModel autocompleteInputSelectionModel2 = this.lastAutocompleteInputSelectionModel;
            MentionToken autocompleteToken = autocompleteInputSelectionModel2 != null ? autocompleteInputSelectionModel2.getAutocompleteToken() : null;
            if (!autocompletable.getInputTextMatchers().isEmpty()) {
                if (autocompleteToken != null) {
                    intRange = new IntRange(autocompleteToken.getStartIndex(), autocompleteToken.getToken().length() + autocompleteToken.getStartIndex());
                } else {
                    intRange = new IntRange(str.length(), str.length());
                }
                String str4 = (String) u.first((List<? extends Object>) autocompletable.getInputTextMatchers());
                String str5 = str4 + ' ';
                InputState inputState2 = this.inputState;
                Map mutableMap2 = h0.toMutableMap(inputState2.getInputAutocompleteMap());
                mutableMap2.put(new IntRange(intRange.getFirst(), str4.length() + intRange.getFirst()), autocompletable);
                this.inputState = InputState.copy$default(inputState2, null, null, null, null, mutableMap2, 15, null);
                this.editTextAction.onNext(new InputEditTextAction.InsertText(str, str5, intRange, str5.length() + intRange.getFirst()));
            }
        }
    }

    public final void selectCommandBrowserApplication(Application application) {
        m.checkNotNullParameter(application, "application");
        StoreState storeState = this.storeState;
        if ((storeState != null ? storeState.getBrowserCommands() : null) == null) {
            StoreStream.Companion.getApplicationCommands().requestDiscoverCommands(application.getId());
        } else {
            Integer num = this.inputState.getApplicationsPosition().get(Long.valueOf(application.getId()));
            if (num == null || !StoreStream.Companion.getApplicationCommands().hasFetchedApplicationCommands(application.getId())) {
                StoreStream.Companion.getApplicationCommands().requestDiscoverCommands(application.getId());
            } else {
                this.events.onNext(new Event.ScrollAutocompletablesToApplication(application.getId(), num.intValue()));
            }
        }
        StoreStream.Companion.getAnalytics().trackApplicationCommandBrowserJump(application.getId());
    }

    @MainThread
    public final void selectCommandOption(ApplicationCommandOption applicationCommandOption) {
        InputEditTextAction inputEditTextAction;
        m.checkNotNullParameter(applicationCommandOption, "applicationCommandsOption");
        InputSelectionModel inputSelectionModel = this.lastChatInputModel;
        if (inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel) {
            InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
            OptionRange optionRange = commandInputSelectionModel.getInputModel().getInputCommandOptionsRanges().get(applicationCommandOption);
            IntRange value = optionRange != null ? optionRange.getValue() : null;
            if (value != null) {
                int ordinal = applicationCommandOption.getType().ordinal();
                int i = 0;
                int i2 = 1;
                if (!(ordinal == 5 || ordinal == 6 || ordinal == 7 || ordinal == 8)) {
                    i2 = 0;
                }
                if (value.getLast() != commandInputSelectionModel.getInputModel().getInput().length()) {
                    i = -1;
                }
                inputEditTextAction = new InputEditTextAction.SelectText(commandInputSelectionModel.getInputModel().getInput(), new IntRange(value.getFirst() + i2, value.getLast() + i));
            } else {
                inputEditTextAction = AutocompleteCommandUtils.appendParam$default(AutocompleteCommandUtils.INSTANCE, commandInputSelectionModel.getInputModel().getInput(), applicationCommandOption, null, 4, null);
            }
            this.editTextAction.onNext(inputEditTextAction);
            return;
        }
        boolean z2 = inputSelectionModel instanceof InputSelectionModel.MessageInputSelectionModel;
    }

    @MainThread
    public final void selectStickerItem(Sticker sticker) {
        String str;
        m.checkNotNullParameter(sticker, "sticker");
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getExpressionSuggestions().setExpressionSuggestionsEnabled(false);
        String obj = this.inputState.getCurrentInput().toString();
        if (obj.length() <= 1 || y.last(obj) != ':') {
            str = w.replaceAfterLast$default(obj, MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, "", null, 4, null);
        } else {
            String substring = obj.substring(0, w.getLastIndex(obj));
            m.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            str = w.replaceAfterLast$default(substring, MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, "", null, 4, null);
        }
        String substring2 = str.substring(0, f.coerceAtLeast(str.length() - 1, 0));
        m.checkNotNullExpressionValue(substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        this.editTextAction.onNext(new InputEditTextAction.ReplaceText(obj, substring2, substring2.length()));
        AutocompleteInputSelectionModel autocompleteInputSelectionModel = this.lastAutocompleteInputSelectionModel;
        if (autocompleteInputSelectionModel != null) {
            companion.getAnalytics().trackAutocompleteSelect(companion.getChannelsSelected().getId(), autocompleteInputSelectionModel.getAutocompleteType(), autocompleteInputSelectionModel.getEmojiNumCount(), autocompleteInputSelectionModel.getStickerNumCount(), AutocompleteSelectionTypes.STICKER, "sticker", Long.valueOf(sticker.getId()));
        }
        companion.getStickers().onStickerUsed(sticker);
    }

    public final void setInputState(InputState inputState) {
        m.checkNotNullParameter(inputState, "<set-?>");
        this.inputState = inputState;
    }

    public final void setLastJumpedSequenceId(int i) {
        this.lastJumpedSequenceId = i;
    }

    public final void setStoreState(StoreState storeState) {
        this.storeState = storeState;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AutocompleteViewModel(Long l, StoreApplicationCommands storeApplicationCommands, StoreExperiments storeExperiments, @ColorInt int i, @ColorInt int i2, @ColorInt int i3, Observable<StoreState> observable) {
        super(new ViewState(AutocompleteViewState.Hidden.INSTANCE, SelectedCommandViewState.Hidden.INSTANCE));
        m.checkNotNullParameter(storeApplicationCommands, "storeApplicationCommands");
        m.checkNotNullParameter(storeExperiments, "storeExperiments");
        m.checkNotNullParameter(observable, "storeObservable");
        this.channelId = l;
        this.storeApplicationCommands = storeApplicationCommands;
        this.storeExperiments = storeExperiments;
        this.defaultMentionColor = i;
        this.defaultCommandOptionBackgroundColor = i2;
        this.commandOptionErrorColor = i3;
        this.emojiAutocompleteUpsellEnabled = EmojiAutocompletePremiumUpsellFeatureFlag.Companion.getINSTANCE().isEnabled();
        this.logEmojiAutocompleteUpsellViewed = true;
        BehaviorSubject<MentionInputModel> k0 = BehaviorSubject.k0();
        this.inputMentionModelSubject = k0;
        BehaviorSubject<AutocompleteInputSelectionModel> k02 = BehaviorSubject.k0();
        this.autocompleteInputSelectionModelSubject = k02;
        BehaviorSubject<SelectionState> k03 = BehaviorSubject.k0();
        this.inputSelectionSubject = k03;
        this.editTextAction = BehaviorSubject.k0();
        this.events = BehaviorSubject.k0();
        this.inputState = new InputState(null, null, null, null, null, 31, null);
        Observable s2 = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null).s(AnonymousClass1.INSTANCE);
        m.checkNotNullExpressionValue(s2, "storeObservable\n        …ore Error\", it)\n        }");
        ObservableExtensionsKt.appSubscribe(s2, AutocompleteViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2(this));
        Observable<SelectionState> q = k03.q();
        m.checkNotNullExpressionValue(k0, "inputMentionModelSubject");
        Observable j = Observable.j(q, ObservableExtensionsKt.computationLatest(k0), new Func2<SelectionState, MentionInputModel, InputSelectionModel>() { // from class: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel.3
            public final InputSelectionModel call(SelectionState selectionState, MentionInputModel mentionInputModel) {
                if (!m.areEqual(selectionState.getInput(), mentionInputModel.getInput().toString())) {
                    return null;
                }
                AutocompleteViewModel autocompleteViewModel = AutocompleteViewModel.this;
                IntRange selection = selectionState.getSelection();
                m.checkNotNullExpressionValue(mentionInputModel, "inputModel");
                return autocompleteViewModel.handleSelectionWithInputModel(selection, mentionInputModel);
            }
        });
        m.checkNotNullExpressionValue(j, "Observable.combineLatest…inputModel)\n      }\n    }");
        ObservableExtensionsKt$filterNull$1 observableExtensionsKt$filterNull$1 = ObservableExtensionsKt$filterNull$1.INSTANCE;
        Observable x2 = j.x(observableExtensionsKt$filterNull$1);
        ObservableExtensionsKt$filterNull$2 observableExtensionsKt$filterNull$2 = ObservableExtensionsKt$filterNull$2.INSTANCE;
        Observable F = x2.F(observableExtensionsKt$filterNull$2);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable s3 = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(F), this, null, 2, null).s(AnonymousClass4.INSTANCE);
        m.checkNotNullExpressionValue(s3, "Observable.combineLatest…del Error\", it)\n        }");
        ObservableExtensionsKt.appSubscribe(s3, AutocompleteViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass5(this));
        m.checkNotNullExpressionValue(k02, "autocompleteInputSelectionModelSubject");
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        Observable s4 = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.leadingEdgeThrottle(k02, 100L, timeUnit)), this, null, 2, null).s(AnonymousClass6.INSTANCE);
        m.checkNotNullExpressionValue(s4, "autocompleteInputSelecti…del Error\", it)\n        }");
        ObservableExtensionsKt.appSubscribe(s4, AutocompleteViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass7(this));
        m.checkNotNullExpressionValue(k02, "autocompleteInputSelectionModelSubject");
        Observable q2 = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(k02), this, null, 2, null).s(AnonymousClass8.INSTANCE).F(AnonymousClass9.INSTANCE).q();
        m.checkNotNullExpressionValue(q2, "autocompleteInputSelecti…  .distinctUntilChanged()");
        Observable F2 = q2.x(observableExtensionsKt$filterNull$1).F(observableExtensionsKt$filterNull$2);
        m.checkNotNullExpressionValue(F2, "filter { it != null }.map { it!! }");
        Observable o = F2.t(new Action1<Pair<? extends ApplicationCommandOption, ? extends String>>() { // from class: com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel.10
            @Override // rx.functions.Action1
            public /* bridge */ /* synthetic */ void call(Pair<? extends ApplicationCommandOption, ? extends String> pair) {
                call2((Pair<ApplicationCommandOption, String>) pair);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final void call2(Pair<ApplicationCommandOption, String> pair) {
                AutocompleteViewModel.this.getStoreApplicationCommands().setAutocompleteLoading(pair.getFirst().getName(), pair.getSecond());
            }
        }).o(500L, timeUnit);
        m.checkNotNullExpressionValue(o, "autocompleteInputSelecti…0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(o, AutocompleteViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass11());
        Observable q3 = k02.F(AnonymousClass12.INSTANCE).q();
        m.checkNotNullExpressionValue(q3, "autocompleteInputSelecti… }.distinctUntilChanged()");
        StoreGuilds.Actions.requestMembers(this, q3, true);
        this.lastJumpedSequenceId = -1;
    }
}
