package com.discord.widgets.chat.input.autocomplete;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.commands.ApplicationCommandOption;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: AutocompleteViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/Event;", "", HookHelper.constructorName, "()V", "RequestAutocompleteData", "ScrollAutocompletablesToApplication", "Lcom/discord/widgets/chat/input/autocomplete/Event$ScrollAutocompletablesToApplication;", "Lcom/discord/widgets/chat/input/autocomplete/Event$RequestAutocompleteData;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class Event {

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/Event$RequestAutocompleteData;", "Lcom/discord/widgets/chat/input/autocomplete/Event;", "Lcom/discord/models/commands/ApplicationCommandOption;", "component1", "()Lcom/discord/models/commands/ApplicationCommandOption;", "option", "copy", "(Lcom/discord/models/commands/ApplicationCommandOption;)Lcom/discord/widgets/chat/input/autocomplete/Event$RequestAutocompleteData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/commands/ApplicationCommandOption;", "getOption", HookHelper.constructorName, "(Lcom/discord/models/commands/ApplicationCommandOption;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RequestAutocompleteData extends Event {
        private final ApplicationCommandOption option;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public RequestAutocompleteData(ApplicationCommandOption applicationCommandOption) {
            super(null);
            m.checkNotNullParameter(applicationCommandOption, "option");
            this.option = applicationCommandOption;
        }

        public static /* synthetic */ RequestAutocompleteData copy$default(RequestAutocompleteData requestAutocompleteData, ApplicationCommandOption applicationCommandOption, int i, Object obj) {
            if ((i & 1) != 0) {
                applicationCommandOption = requestAutocompleteData.option;
            }
            return requestAutocompleteData.copy(applicationCommandOption);
        }

        public final ApplicationCommandOption component1() {
            return this.option;
        }

        public final RequestAutocompleteData copy(ApplicationCommandOption applicationCommandOption) {
            m.checkNotNullParameter(applicationCommandOption, "option");
            return new RequestAutocompleteData(applicationCommandOption);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof RequestAutocompleteData) && m.areEqual(this.option, ((RequestAutocompleteData) obj).option);
            }
            return true;
        }

        public final ApplicationCommandOption getOption() {
            return this.option;
        }

        public int hashCode() {
            ApplicationCommandOption applicationCommandOption = this.option;
            if (applicationCommandOption != null) {
                return applicationCommandOption.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = a.R("RequestAutocompleteData(option=");
            R.append(this.option);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: AutocompleteViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0007J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/Event$ScrollAutocompletablesToApplication;", "Lcom/discord/widgets/chat/input/autocomplete/Event;", "", "component1", "()J", "", "component2", "()I", "applicationId", "targetPosition", "copy", "(JI)Lcom/discord/widgets/chat/input/autocomplete/Event$ScrollAutocompletablesToApplication;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getApplicationId", "I", "getTargetPosition", HookHelper.constructorName, "(JI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ScrollAutocompletablesToApplication extends Event {
        private final long applicationId;
        private final int targetPosition;

        public ScrollAutocompletablesToApplication(long j, int i) {
            super(null);
            this.applicationId = j;
            this.targetPosition = i;
        }

        public static /* synthetic */ ScrollAutocompletablesToApplication copy$default(ScrollAutocompletablesToApplication scrollAutocompletablesToApplication, long j, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = scrollAutocompletablesToApplication.applicationId;
            }
            if ((i2 & 2) != 0) {
                i = scrollAutocompletablesToApplication.targetPosition;
            }
            return scrollAutocompletablesToApplication.copy(j, i);
        }

        public final long component1() {
            return this.applicationId;
        }

        public final int component2() {
            return this.targetPosition;
        }

        public final ScrollAutocompletablesToApplication copy(long j, int i) {
            return new ScrollAutocompletablesToApplication(j, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ScrollAutocompletablesToApplication)) {
                return false;
            }
            ScrollAutocompletablesToApplication scrollAutocompletablesToApplication = (ScrollAutocompletablesToApplication) obj;
            return this.applicationId == scrollAutocompletablesToApplication.applicationId && this.targetPosition == scrollAutocompletablesToApplication.targetPosition;
        }

        public final long getApplicationId() {
            return this.applicationId;
        }

        public final int getTargetPosition() {
            return this.targetPosition;
        }

        public int hashCode() {
            return (b.a(this.applicationId) * 31) + this.targetPosition;
        }

        public String toString() {
            StringBuilder R = a.R("ScrollAutocompletablesToApplication(applicationId=");
            R.append(this.applicationId);
            R.append(", targetPosition=");
            return a.A(R, this.targetPosition, ")");
        }
    }

    private Event() {
    }

    public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
