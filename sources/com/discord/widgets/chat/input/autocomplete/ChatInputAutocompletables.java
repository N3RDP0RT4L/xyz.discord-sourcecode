package com.discord.widgets.chat.input.autocomplete;

import andhook.lib.HookHelper;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.widgets.chat.input.autocomplete.sources.ApplicationCommandsAutocompletableSource;
import com.discord.widgets.chat.input.autocomplete.sources.ChannelAutocompletableSource;
import com.discord.widgets.chat.input.autocomplete.sources.EmojiAutocompletableSource;
import com.discord.widgets.chat.input.autocomplete.sources.UserAutocompletableSource;
import d0.g;
import d0.z.d.m;
import java.util.Map;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: InputAutocompletables.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b!\u0010\"J1\u0010\n\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001d\u0010\u0011\u001a\u00020\f8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0016\u001a\u00020\u00128F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0015R\u001d\u0010\u001b\u001a\u00020\u00178F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u000e\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010 \u001a\u00020\u001c8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u000e\u001a\u0004\b\u001e\u0010\u001f¨\u0006#"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/ChatInputAutocompletables;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lrx/Observable;", "", "Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "observeChannelAutocompletables", "(J)Lrx/Observable;", "Lcom/discord/widgets/chat/input/autocomplete/sources/ApplicationCommandsAutocompletableSource;", "APPLICATION_COMMANDS_SOURCE$delegate", "Lkotlin/Lazy;", "getAPPLICATION_COMMANDS_SOURCE", "()Lcom/discord/widgets/chat/input/autocomplete/sources/ApplicationCommandsAutocompletableSource;", "APPLICATION_COMMANDS_SOURCE", "Lcom/discord/widgets/chat/input/autocomplete/sources/ChannelAutocompletableSource;", "CHANNEL_SOURCE$delegate", "getCHANNEL_SOURCE", "()Lcom/discord/widgets/chat/input/autocomplete/sources/ChannelAutocompletableSource;", "CHANNEL_SOURCE", "Lcom/discord/widgets/chat/input/autocomplete/sources/EmojiAutocompletableSource;", "EMOJI_SOURCE$delegate", "getEMOJI_SOURCE", "()Lcom/discord/widgets/chat/input/autocomplete/sources/EmojiAutocompletableSource;", "EMOJI_SOURCE", "Lcom/discord/widgets/chat/input/autocomplete/sources/UserAutocompletableSource;", "USERS_SOURCE$delegate", "getUSERS_SOURCE", "()Lcom/discord/widgets/chat/input/autocomplete/sources/UserAutocompletableSource;", "USERS_SOURCE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatInputAutocompletables {
    public static final ChatInputAutocompletables INSTANCE = new ChatInputAutocompletables();
    private static final Lazy EMOJI_SOURCE$delegate = g.lazy(ChatInputAutocompletables$EMOJI_SOURCE$2.INSTANCE);
    private static final Lazy CHANNEL_SOURCE$delegate = g.lazy(ChatInputAutocompletables$CHANNEL_SOURCE$2.INSTANCE);
    private static final Lazy USERS_SOURCE$delegate = g.lazy(ChatInputAutocompletables$USERS_SOURCE$2.INSTANCE);
    private static final Lazy APPLICATION_COMMANDS_SOURCE$delegate = g.lazy(ChatInputAutocompletables$APPLICATION_COMMANDS_SOURCE$2.INSTANCE);

    private ChatInputAutocompletables() {
    }

    public final ApplicationCommandsAutocompletableSource getAPPLICATION_COMMANDS_SOURCE() {
        return (ApplicationCommandsAutocompletableSource) APPLICATION_COMMANDS_SOURCE$delegate.getValue();
    }

    public final ChannelAutocompletableSource getCHANNEL_SOURCE() {
        return (ChannelAutocompletableSource) CHANNEL_SOURCE$delegate.getValue();
    }

    public final EmojiAutocompletableSource getEMOJI_SOURCE() {
        return (EmojiAutocompletableSource) EMOJI_SOURCE$delegate.getValue();
    }

    public final UserAutocompletableSource getUSERS_SOURCE() {
        return (UserAutocompletableSource) USERS_SOURCE$delegate.getValue();
    }

    public final Observable<Map<LeadingIdentifier, Set<Autocompletable>>> observeChannelAutocompletables(long j) {
        Observable<R> F = StoreStream.Companion.getChannels().observeChannel(j).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable Y = F.Y(ChatInputAutocompletables$observeChannelAutocompletables$1.INSTANCE);
        m.checkNotNullExpressionValue(Y, "StoreStream.getChannels(…e>>\n          }\n        }");
        return ObservableExtensionsKt.computationLatest(Y);
    }
}
