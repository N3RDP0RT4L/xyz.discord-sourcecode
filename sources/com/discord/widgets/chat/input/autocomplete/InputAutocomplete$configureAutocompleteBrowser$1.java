package com.discord.widgets.chat.input.autocomplete;

import com.discord.widgets.chat.input.autocomplete.adapter.ChatInputAutocompleteAdapter;
import d0.t.c0;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.ranges.IntRange;
/* compiled from: InputAutocomplete.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "firstVisibleItem", "lastVisibleItem", "", "invoke", "(II)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InputAutocomplete$configureAutocompleteBrowser$1 extends o implements Function2<Integer, Integer, Unit> {
    public final /* synthetic */ InputAutocomplete this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InputAutocomplete$configureAutocompleteBrowser$1(InputAutocomplete inputAutocomplete) {
        super(2);
        this.this$0 = inputAutocomplete;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Integer num, Integer num2) {
        invoke(num.intValue(), num2.intValue());
        return Unit.a;
    }

    public final void invoke(int i, int i2) {
        ChatInputAutocompleteAdapter chatInputAutocompleteAdapter;
        ChatInputAutocompleteAdapter chatInputAutocompleteAdapter2;
        chatInputAutocompleteAdapter = this.this$0.autocompleteAdapter;
        int min = Math.min(i2, chatInputAutocompleteAdapter.getItemCount() - 1);
        if (i >= 0 && min >= i) {
            IntRange intRange = new IntRange(i, min);
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(intRange, 10));
            Iterator<Integer> it = intRange.iterator();
            while (it.hasNext()) {
                int nextInt = ((c0) it).nextInt();
                chatInputAutocompleteAdapter2 = this.this$0.autocompleteAdapter;
                arrayList.add(chatInputAutocompleteAdapter2.getItem(nextInt));
            }
            this.this$0.getViewModel().checkEmojiAutocompleteUpsellViewed(arrayList);
        }
    }
}
