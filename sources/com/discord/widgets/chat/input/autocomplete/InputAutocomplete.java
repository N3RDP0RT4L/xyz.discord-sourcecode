package com.discord.widgets.chat.input.autocomplete;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChatInputApplicationCommandsBinding;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.stores.LoadState;
import com.discord.stores.StoreApplicationCommandsKt;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.recycler.SelfHealingLinearLayoutManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.SimpleRoundedBackgroundSpan;
import com.discord.widgets.chat.MessageContent;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.ChatInputApplicationsAdapter;
import com.discord.widgets.chat.input.WidgetChatInputAutocompleteStickerAdapter;
import com.discord.widgets.chat.input.WidgetChatInputEditText;
import com.discord.widgets.chat.input.applicationcommands.SelectedApplicationCommandAdapter;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewState;
import com.discord.widgets.chat.input.autocomplete.Event;
import com.discord.widgets.chat.input.autocomplete.InputEditTextAction;
import com.discord.widgets.chat.input.autocomplete.SelectedCommandViewState;
import com.discord.widgets.chat.input.autocomplete.adapter.ChatInputAutocompleteAdapter;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.lytefast.flexinput.widget.FlexEditText;
import d0.g0.t;
import d0.t.n;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import rx.Observable;
import xyz.discord.R;
/* compiled from: InputAutocomplete.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ô\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001Ba\u0012\u0006\u0010j\u001a\u00020i\u0012\u0006\u0010Y\u001a\u00020X\u0012\u0010\b\u0002\u0010m\u001a\n\u0018\u00010&j\u0004\u0018\u0001`l\u0012\u0006\u0010L\u001a\u00020K\u0012\u0006\u0010C\u001a\u00020B\u0012\u0006\u0010[\u001a\u00020B\u0012\u0006\u0010S\u001a\u00020R\u0012\u0006\u0010N\u001a\u00020B\u0012\u0006\u0010o\u001a\u00020K\u0012\u0006\u0010@\u001a\u00020?¢\u0006\u0004\bp\u0010qJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0011\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001f\u0010\u0016J\u0017\u0010\"\u001a\u00020\u00042\u0006\u0010!\u001a\u00020 H\u0002¢\u0006\u0004\b\"\u0010#J\u001f\u0010(\u001a\n\u0018\u00010&j\u0004\u0018\u0001`'2\u0006\u0010%\u001a\u00020$H\u0002¢\u0006\u0004\b(\u0010)J!\u0010.\u001a\u00020\u00042\u0006\u0010+\u001a\u00020*2\b\b\u0002\u0010-\u001a\u00020,H\u0002¢\u0006\u0004\b.\u0010/J\u0017\u00100\u001a\u00020\u00042\u0006\u0010+\u001a\u00020*H\u0002¢\u0006\u0004\b0\u00101J\r\u00102\u001a\u00020\u0004¢\u0006\u0004\b2\u0010\u0016J\u001b\u00106\u001a\u0004\u0018\u0001052\n\b\u0002\u00104\u001a\u0004\u0018\u000103¢\u0006\u0004\b6\u00107J\r\u00109\u001a\u000208¢\u0006\u0004\b9\u0010:J\r\u0010;\u001a\u00020\u0004¢\u0006\u0004\b;\u0010\u0016R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>R\u0016\u0010@\u001a\u00020?8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010AR\u0016\u0010C\u001a\u00020B8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bC\u0010DR\u001d\u0010J\u001a\u00020E8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bF\u0010G\u001a\u0004\bH\u0010IR\u0016\u0010L\u001a\u00020K8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010MR\u0016\u0010N\u001a\u00020B8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bN\u0010DR\u0016\u0010P\u001a\u00020O8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bP\u0010QR\u0016\u0010S\u001a\u00020R8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010TR\u0016\u0010V\u001a\u00020U8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bV\u0010WR\u0016\u0010Y\u001a\u00020X8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bY\u0010ZR\u0016\u0010[\u001a\u00020B8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b[\u0010DR\u0016\u0010]\u001a\u00020\\8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010`\u001a\u00020_8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b`\u0010aR0\u0010c\u001a\u0010\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020\u0004\u0018\u00010b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bc\u0010d\u001a\u0004\be\u0010f\"\u0004\bg\u0010hR\u0016\u0010j\u001a\u00020i8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bj\u0010kR\u001e\u0010m\u001a\n\u0018\u00010&j\u0004\u0018\u0001`l8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bm\u0010nR\u0016\u0010o\u001a\u00020K8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bo\u0010M¨\u0006r"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/InputAutocomplete;", "", "Lcom/discord/widgets/chat/input/autocomplete/Event;", "event", "", "handleEvent", "(Lcom/discord/widgets/chat/input/autocomplete/Event;)V", "Lcom/discord/widgets/chat/input/autocomplete/ViewState;", "viewState", "configureUI", "(Lcom/discord/widgets/chat/input/autocomplete/ViewState;)V", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;", "autocompleteViewState", "configureAutocomplete", "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;)V", "Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;", "selectedCommandViewState", "configureSelectedCommand", "(Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;)V", "Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState$SelectedCommand;", "(Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState$SelectedCommand;)V", "hideSelectedCommand", "()V", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState$CommandBrowser;", "browser", "configureCommandBrowser", "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState$CommandBrowser;)V", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState$Autocomplete;", "autocomplete", "configureAutocompleteBrowser", "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState$Autocomplete;)V", "hideAutocomplete", "Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "action", "applyEditTextAction", "(Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;)V", "", "topPosition", "", "Lcom/discord/primitives/ApplicationId;", "getCurrentlySelectedCategory", "(I)Ljava/lang/Long;", "Landroid/text/Spannable;", "spannable", "Lkotlin/ranges/IntRange;", "spanRange", "removeSpans", "(Landroid/text/Spannable;Lkotlin/ranges/IntRange;)V", "removePillSpans", "(Landroid/text/Spannable;)V", "onViewBoundOrOnResume", "Lcom/discord/models/commands/ApplicationCommandOption;", "focusedOption", "Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "getApplicationCommandData", "(Lcom/discord/models/commands/ApplicationCommandOption;)Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "Lcom/discord/widgets/chat/MessageContent;", "getInputContent", "()Lcom/discord/widgets/chat/MessageContent;", "onCommandInputsSendError", "Lcom/discord/widgets/chat/input/autocomplete/adapter/ChatInputAutocompleteAdapter;", "autocompleteAdapter", "Lcom/discord/widgets/chat/input/autocomplete/adapter/ChatInputAutocompleteAdapter;", "Lcom/discord/databinding/WidgetChatInputApplicationCommandsBinding;", "selectedApplicationCommandUiBinding", "Lcom/discord/databinding/WidgetChatInputApplicationCommandsBinding;", "Landroidx/recyclerview/widget/RecyclerView;", "autocompleteRecyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewModel;", "viewModel", "Landroid/widget/TextView;", "autocompleteHeader", "Landroid/widget/TextView;", "stickersRecyclerView", "Lcom/discord/widgets/chat/input/WidgetChatInputAutocompleteStickerAdapter;", "stickersAdapter", "Lcom/discord/widgets/chat/input/WidgetChatInputAutocompleteStickerAdapter;", "Landroid/view/View;", "stickersContainer", "Landroid/view/View;", "", "commandBrowserOpen", "Z", "Lcom/lytefast/flexinput/widget/FlexEditText;", "editText", "Lcom/lytefast/flexinput/widget/FlexEditText;", "commandBrowserAppsRecyclerView", "Lcom/discord/widgets/chat/input/ChatInputApplicationsAdapter;", "categoriesAdapter", "Lcom/discord/widgets/chat/input/ChatInputApplicationsAdapter;", "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;", "selectedApplicationCommandAdapter", "Lcom/discord/widgets/chat/input/applicationcommands/SelectedApplicationCommandAdapter;", "Lkotlin/Function1;", "onPerformCommandAutocomplete", "Lkotlin/jvm/functions/Function1;", "getOnPerformCommandAutocomplete", "()Lkotlin/jvm/functions/Function1;", "setOnPerformCommandAutocomplete", "(Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/app/AppFragment;", "fragment", "Lcom/discord/app/AppFragment;", "Lcom/discord/primitives/ChannelId;", "channel", "Ljava/lang/Long;", "stickerHeader", HookHelper.constructorName, "(Lcom/discord/app/AppFragment;Lcom/lytefast/flexinput/widget/FlexEditText;Ljava/lang/Long;Landroid/widget/TextView;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/TextView;Lcom/discord/databinding/WidgetChatInputApplicationCommandsBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InputAutocomplete {
    private final ChatInputAutocompleteAdapter autocompleteAdapter;
    private final TextView autocompleteHeader;
    private final RecyclerView autocompleteRecyclerView;
    private final ChatInputApplicationsAdapter categoriesAdapter;
    private final Long channel;
    private final RecyclerView commandBrowserAppsRecyclerView;
    private boolean commandBrowserOpen;
    private final FlexEditText editText;
    private final AppFragment fragment;
    private Function1<? super ApplicationCommandOption, Unit> onPerformCommandAutocomplete;
    private final SelectedApplicationCommandAdapter selectedApplicationCommandAdapter;
    private final WidgetChatInputApplicationCommandsBinding selectedApplicationCommandUiBinding;
    private final TextView stickerHeader;
    private final WidgetChatInputAutocompleteStickerAdapter stickersAdapter;
    private final View stickersContainer;
    private final RecyclerView stickersRecyclerView;
    private final Lazy viewModel$delegate;

    /* compiled from: InputAutocomplete.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "start", "end", "", "invoke", "(II)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.InputAutocomplete$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function2<Integer, Integer, Unit> {
        public AnonymousClass2() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public /* bridge */ /* synthetic */ Unit invoke(Integer num, Integer num2) {
            invoke(num.intValue(), num2.intValue());
            return Unit.a;
        }

        public final void invoke(int i, int i2) {
            InputAutocomplete.this.getViewModel().onSelectionChanged(InputAutocomplete.this.editText.getEditableText().toString(), i, i2);
        }
    }

    /* compiled from: InputAutocomplete.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/commands/Application;", "it", "", "invoke", "(Lcom/discord/models/commands/Application;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.InputAutocomplete$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends o implements Function1<Application, Unit> {
        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Application application) {
            invoke2(application);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Application application) {
            m.checkNotNullParameter(application, "it");
            InputAutocomplete.this.getViewModel().selectCommandBrowserApplication(application);
            InputAutocomplete.this.categoriesAdapter.selectApplication(application.getId());
        }
    }

    /* compiled from: InputAutocomplete.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "it", "", "invoke", "(Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.InputAutocomplete$4  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass4 extends o implements Function1<Autocompletable, Unit> {
        public AnonymousClass4() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Autocompletable autocompletable) {
            invoke2(autocompletable);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Autocompletable autocompletable) {
            m.checkNotNullParameter(autocompletable, "it");
            InputAutocomplete.this.getViewModel().selectAutocompleteItem(autocompletable);
        }
    }

    /* compiled from: InputAutocomplete.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/sticker/Sticker;", "sticker", "", "invoke", "(Lcom/discord/api/sticker/Sticker;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.InputAutocomplete$5  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass5 extends o implements Function1<Sticker, Unit> {
        public AnonymousClass5() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Sticker sticker) {
            invoke2(sticker);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Sticker sticker) {
            m.checkNotNullParameter(sticker, "sticker");
            InputAutocomplete.this.getViewModel().selectStickerItem(sticker);
            Context context = InputAutocomplete.this.editText.getContext();
            m.checkNotNullExpressionValue(context, "editText.context");
            MessageManager.sendMessage$default(new MessageManager(context, null, null, null, null, null, null, null, null, 510, null), null, null, null, null, d0.t.m.listOf(sticker), false, null, null, null, 495, null);
        }
    }

    /* compiled from: InputAutocomplete.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/commands/ApplicationCommandOption;", "it", "", "invoke", "(Lcom/discord/models/commands/ApplicationCommandOption;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.autocomplete.InputAutocomplete$6  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass6 extends o implements Function1<ApplicationCommandOption, Unit> {
        public AnonymousClass6() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ApplicationCommandOption applicationCommandOption) {
            invoke2(applicationCommandOption);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ApplicationCommandOption applicationCommandOption) {
            m.checkNotNullParameter(applicationCommandOption, "it");
            InputAutocomplete.this.getViewModel().selectCommandOption(applicationCommandOption);
        }
    }

    public InputAutocomplete(AppFragment appFragment, FlexEditText flexEditText, Long l, TextView textView, RecyclerView recyclerView, RecyclerView recyclerView2, View view, RecyclerView recyclerView3, TextView textView2, WidgetChatInputApplicationCommandsBinding widgetChatInputApplicationCommandsBinding) {
        m.checkNotNullParameter(appFragment, "fragment");
        m.checkNotNullParameter(flexEditText, "editText");
        m.checkNotNullParameter(textView, "autocompleteHeader");
        m.checkNotNullParameter(recyclerView, "autocompleteRecyclerView");
        m.checkNotNullParameter(recyclerView2, "commandBrowserAppsRecyclerView");
        m.checkNotNullParameter(view, "stickersContainer");
        m.checkNotNullParameter(recyclerView3, "stickersRecyclerView");
        m.checkNotNullParameter(textView2, "stickerHeader");
        m.checkNotNullParameter(widgetChatInputApplicationCommandsBinding, "selectedApplicationCommandUiBinding");
        this.fragment = appFragment;
        this.editText = flexEditText;
        this.channel = l;
        this.autocompleteHeader = textView;
        this.autocompleteRecyclerView = recyclerView;
        this.commandBrowserAppsRecyclerView = recyclerView2;
        this.stickersContainer = view;
        this.stickersRecyclerView = recyclerView3;
        this.stickerHeader = textView2;
        this.selectedApplicationCommandUiBinding = widgetChatInputApplicationCommandsBinding;
        ChatInputAutocompleteAdapter chatInputAutocompleteAdapter = new ChatInputAutocompleteAdapter();
        this.autocompleteAdapter = chatInputAutocompleteAdapter;
        InputAutocomplete$viewModel$2 inputAutocomplete$viewModel$2 = new InputAutocomplete$viewModel$2(this);
        f0 f0Var = new f0(appFragment);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(appFragment, a0.getOrCreateKotlinClass(AutocompleteViewModel.class), new InputAutocomplete$appViewModels$$inlined$viewModels$1(f0Var), new h0(inputAutocomplete$viewModel$2));
        flexEditText.addTextChangedListener(new TextWatcher() { // from class: com.discord.widgets.chat.input.autocomplete.InputAutocomplete.1
            private int before;
            private int count;
            private int start;

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                m.checkNotNullParameter(editable, "s");
                InputAutocomplete.this.applyEditTextAction(InputAutocomplete.this.getViewModel().onInputTextChanged(editable, this.start, this.before, this.count));
            }

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public final int getBefore() {
                return this.before;
            }

            public final int getCount() {
                return this.count;
            }

            public final int getStart() {
                return this.start;
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                this.start = i;
                this.before = i2;
                this.count = i3;
            }

            public final void setBefore(int i) {
                this.before = i;
            }

            public final void setCount(int i) {
                this.count = i;
            }

            public final void setStart(int i) {
                this.start = i;
            }
        });
        flexEditText.setOnSelectionChangedListener(new AnonymousClass2());
        ChatInputApplicationsAdapter chatInputApplicationsAdapter = new ChatInputApplicationsAdapter();
        this.categoriesAdapter = chatInputApplicationsAdapter;
        chatInputApplicationsAdapter.setOnClickApplication(new AnonymousClass3());
        RecyclerView.LayoutManager selfHealingLinearLayoutManager = new SelfHealingLinearLayoutManager(recyclerView2, chatInputApplicationsAdapter, 0, false, 8, null);
        recyclerView2.setAdapter(chatInputApplicationsAdapter);
        recyclerView2.setLayoutManager(selfHealingLinearLayoutManager);
        recyclerView2.setItemAnimator(null);
        chatInputAutocompleteAdapter.setOnItemSelected(new AnonymousClass4());
        recyclerView.setAdapter(chatInputAutocompleteAdapter);
        recyclerView.setItemAnimator(null);
        recyclerView.setBackgroundColor(ColorCompat.getThemedColor(flexEditText, (int) R.attr.colorBackgroundSecondary));
        WidgetChatInputAutocompleteStickerAdapter widgetChatInputAutocompleteStickerAdapter = (WidgetChatInputAutocompleteStickerAdapter) MGRecyclerAdapter.Companion.configure(new WidgetChatInputAutocompleteStickerAdapter(recyclerView3, new AnonymousClass5()));
        this.stickersAdapter = widgetChatInputAutocompleteStickerAdapter;
        recyclerView3.setLayoutManager(new SelfHealingLinearLayoutManager(recyclerView3, widgetChatInputAutocompleteStickerAdapter, 0, false, 8, null));
        recyclerView3.setAdapter(widgetChatInputAutocompleteStickerAdapter);
        RecyclerView recyclerView4 = widgetChatInputApplicationCommandsBinding.c;
        m.checkNotNullExpressionValue(recyclerView4, "selectedApplicationComma…plicationCommandsRecycler");
        SelectedApplicationCommandAdapter selectedApplicationCommandAdapter = new SelectedApplicationCommandAdapter(recyclerView4, new AnonymousClass6());
        this.selectedApplicationCommandAdapter = selectedApplicationCommandAdapter;
        selectedApplicationCommandAdapter.getRecycler().setLayoutManager(new SelfHealingLinearLayoutManager(selectedApplicationCommandAdapter.getRecycler(), selectedApplicationCommandAdapter, 0, false, 8, null));
        selectedApplicationCommandAdapter.getRecycler().setAdapter(selectedApplicationCommandAdapter);
        selectedApplicationCommandAdapter.getRecycler().setItemAnimator(null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void applyEditTextAction(InputEditTextAction inputEditTextAction) {
        if (!(!m.areEqual(this.editText.getEditableText().toString(), inputEditTextAction.getAssumedInput().toString()))) {
            if (inputEditTextAction instanceof InputEditTextAction.ClearSpans) {
                Editable editableText = this.editText.getEditableText();
                m.checkNotNullExpressionValue(editableText, "editText.editableText");
                removeSpans$default(this, editableText, null, 2, null);
                Editable editableText2 = this.editText.getEditableText();
                m.checkNotNullExpressionValue(editableText2, "editText.editableText");
                removePillSpans(editableText2);
            } else if (inputEditTextAction instanceof InputEditTextAction.ReplaceCharacterStyleSpans) {
                Editable editableText3 = this.editText.getEditableText();
                m.checkNotNullExpressionValue(editableText3, "editText.editableText");
                removeSpans$default(this, editableText3, null, 2, null);
                for (Map.Entry<IntRange, List<CharacterStyle>> entry : ((InputEditTextAction.ReplaceCharacterStyleSpans) inputEditTextAction).getSpans().entrySet()) {
                    IntRange key = entry.getKey();
                    for (CharacterStyle characterStyle : entry.getValue()) {
                        this.editText.getEditableText().setSpan(characterStyle, key.getFirst(), key.getLast(), 33);
                    }
                }
            } else if (inputEditTextAction instanceof InputEditTextAction.InsertText) {
                InputEditTextAction.InsertText insertText = (InputEditTextAction.InsertText) inputEditTextAction;
                this.editText.getEditableText().replace(insertText.getInsertRange().getFirst(), insertText.getInsertRange().getLast(), insertText.getToAppend());
                this.editText.setSelection(insertText.getSelectionIndex());
            } else if (inputEditTextAction instanceof InputEditTextAction.RemoveText) {
                InputEditTextAction.RemoveText removeText = (InputEditTextAction.RemoveText) inputEditTextAction;
                this.editText.getEditableText().replace(removeText.getRange().getFirst(), removeText.getRange().getLast(), "");
                this.editText.setSelection(removeText.getSelectionIndex());
            } else if (inputEditTextAction instanceof InputEditTextAction.ReplacePillSpans) {
                Editable editableText4 = this.editText.getEditableText();
                m.checkNotNullExpressionValue(editableText4, "editText.editableText");
                removePillSpans(editableText4);
                for (Map.Entry<IntRange, SimpleRoundedBackgroundSpan> entry2 : ((InputEditTextAction.ReplacePillSpans) inputEditTextAction).getSpans().entrySet()) {
                    IntRange key2 = entry2.getKey();
                    this.editText.getEditableText().setSpan(entry2.getValue(), key2.getFirst(), key2.getLast(), 33);
                }
            } else if (inputEditTextAction instanceof InputEditTextAction.SelectText) {
                InputEditTextAction.SelectText selectText = (InputEditTextAction.SelectText) inputEditTextAction;
                this.editText.setSelection(selectText.getSelection().getFirst(), selectText.getSelection().getLast());
            } else if (inputEditTextAction instanceof InputEditTextAction.ReplaceText) {
                InputEditTextAction.ReplaceText replaceText = (InputEditTextAction.ReplaceText) inputEditTextAction;
                this.editText.setText(replaceText.getNewText());
                this.editText.setSelection(replaceText.getSelectionIndex());
            } else {
                boolean z2 = inputEditTextAction instanceof InputEditTextAction.None;
            }
        }
    }

    private final void configureAutocomplete(AutocompleteViewState autocompleteViewState) {
        boolean z2 = autocompleteViewState instanceof AutocompleteViewState.Autocomplete;
        if (z2) {
            configureAutocompleteBrowser((AutocompleteViewState.Autocomplete) autocompleteViewState);
        } else if (z2) {
            configureAutocompleteBrowser((AutocompleteViewState.Autocomplete) autocompleteViewState);
        } else if (autocompleteViewState instanceof AutocompleteViewState.CommandBrowser) {
            configureCommandBrowser((AutocompleteViewState.CommandBrowser) autocompleteViewState);
        } else if (m.areEqual(autocompleteViewState, AutocompleteViewState.Hidden.INSTANCE)) {
            hideAutocomplete();
        }
        if (!(autocompleteViewState instanceof AutocompleteViewState.CommandBrowser)) {
            this.commandBrowserOpen = false;
        }
        StoreStream.Companion.getAutocomplete().setAutocompleteVisible(!(autocompleteViewState instanceof AutocompleteViewState.Hidden));
    }

    private final void configureAutocompleteBrowser(AutocompleteViewState.Autocomplete autocomplete) {
        List<Autocompletable> list;
        CharSequence d;
        CharSequence d2;
        CharSequence d3;
        CharSequence d4;
        CharSequence d5;
        boolean z2 = !autocomplete.getStickers().isEmpty();
        if (!(this.autocompleteRecyclerView.getVisibility() == 0)) {
            this.autocompleteRecyclerView.setVisibility(0);
        }
        int i = 8;
        if (this.commandBrowserAppsRecyclerView.getVisibility() == 0) {
            this.commandBrowserAppsRecyclerView.setVisibility(8);
        }
        if (!t.startsWith$default(autocomplete.getToken(), String.valueOf(LeadingIdentifier.EMOJI_AND_STICKERS.getIdentifier()), false, 2, null) || !(!autocomplete.getAutocompletables().isEmpty())) {
            this.autocompleteHeader.setVisibility(8);
        } else {
            this.autocompleteHeader.setVisibility(0);
            TextView textView = this.autocompleteHeader;
            d5 = b.d(textView, R.string.emoji_matching, new Object[]{autocomplete.getToken()}, (r4 & 4) != 0 ? b.c.j : null);
            textView.setText(d5);
        }
        this.autocompleteHeader.setTextColor(ColorCompat.getThemedColor(this.autocompleteHeader, (int) R.attr.colorTextNormal));
        if (autocomplete.isAutocomplete()) {
            this.autocompleteHeader.setVisibility(t.isBlank(autocomplete.getToken()) ^ true ? 0 : 8);
            if (autocomplete.isError()) {
                TextView textView2 = this.autocompleteHeader;
                d4 = b.d(textView2, R.string.application_command_autocomplete_failed, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                textView2.setText(d4);
                this.autocompleteHeader.setTextColor(ColorCompat.getThemedColor(this.autocompleteHeader, (int) R.attr.colorTextDanger));
            } else {
                if (!autocomplete.isLoading()) {
                    List<Autocompletable> autocompletables = autocomplete.getAutocompletables();
                    if (autocompletables == null || autocompletables.isEmpty()) {
                        TextView textView3 = this.autocompleteHeader;
                        d3 = b.d(textView3, R.string.application_command_autocomplete_no_options, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                        textView3.setText(d3);
                    }
                }
                TextView textView4 = this.autocompleteHeader;
                d2 = b.d(textView4, R.string.options_matching, new Object[]{autocomplete.getToken()}, (r4 & 4) != 0 ? b.c.j : null);
                textView4.setText(d2);
            }
        }
        this.autocompleteAdapter.setVisiblePositionListener(new InputAutocomplete$configureAutocompleteBrowser$1(this));
        getViewModel().onAutocompleteItemsUpdated();
        if (!autocomplete.isLoading() || autocomplete.isError()) {
            list = autocomplete.getAutocompletables();
        } else {
            list = new ArrayList<>();
            for (int i2 = 0; i2 < 4; i2++) {
                list.add(new ApplicationCommandLoadingPlaceholder(null, 1, null));
            }
        }
        ChatInputAutocompleteAdapter.setData$default(this.autocompleteAdapter, list, z2, false, 4, null);
        this.stickersRecyclerView.setVisibility(z2 ? 0 : 8);
        View view = this.stickersContainer;
        if (z2) {
            i = 0;
        }
        view.setVisibility(i);
        if (z2) {
            TextView textView5 = this.stickerHeader;
            d = b.d(textView5, R.string.stickers_matching, new Object[]{t.replace$default(autocomplete.getToken(), String.valueOf(LeadingIdentifier.EMOJI_AND_STICKERS.getIdentifier()), "", false, 4, (Object) null)}, (r4 & 4) != 0 ? b.c.j : null);
            textView5.setText(d);
            this.stickersAdapter.setData(autocomplete.getStickers());
        }
    }

    private final void configureCommandBrowser(AutocompleteViewState.CommandBrowser commandBrowser) {
        int i = 8;
        this.autocompleteHeader.setVisibility(8);
        this.stickersContainer.setVisibility(8);
        List<Autocompletable> flattenCommandsModel = commandBrowser.getDiscoverCommands().getFlattenCommandsModel();
        boolean z2 = (flattenCommandsModel.isEmpty() ^ true) || commandBrowser.getDiscoverCommands().getLoadState().isLoading();
        if (!this.commandBrowserOpen && z2) {
            this.commandBrowserOpen = true;
            StoreStream.Companion companion = StoreStream.Companion;
            companion.getAnalytics().trackApplicationCommandBrowserOpened(companion.getChannelsSelected().getId());
        }
        if ((this.autocompleteRecyclerView.getVisibility() == 0) != z2) {
            this.autocompleteRecyclerView.setVisibility(z2 ? 0 : 8);
        }
        boolean z3 = commandBrowser.getApplications().size() > 1;
        if ((this.commandBrowserAppsRecyclerView.getVisibility() == 0) != z3) {
            RecyclerView recyclerView = this.commandBrowserAppsRecyclerView;
            if (z3) {
                i = 0;
            }
            recyclerView.setVisibility(i);
            if (z3) {
                this.categoriesAdapter.selectApplication(0L);
                this.commandBrowserAppsRecyclerView.smoothScrollToPosition(0);
            }
        }
        this.categoriesAdapter.setApplicationData(commandBrowser.getApplications());
        this.autocompleteAdapter.setVisiblePositionListener(new InputAutocomplete$configureCommandBrowser$1(this));
        if (this.autocompleteAdapter.getItemCount() > 3 && (commandBrowser.getDiscoverCommands().getLoadState() instanceof LoadState.JustLoadedUp)) {
            RecyclerView recyclerView2 = this.autocompleteRecyclerView;
            int childAdapterPosition = recyclerView2.getChildAdapterPosition(recyclerView2.getChildAt(0));
            View childAt = this.autocompleteRecyclerView.getChildAt(0);
            m.checkNotNullExpressionValue(childAt, "autocompleteRecyclerView.getChildAt(0)");
            this.autocompleteAdapter.scrollToPosition((flattenCommandsModel.size() - this.autocompleteAdapter.getItemCount()) + childAdapterPosition, childAt.getTop(), false);
        }
        if ((!flattenCommandsModel.isEmpty()) && this.autocompleteAdapter.getItemCount() == 4 && (this.autocompleteAdapter.getItem(0) instanceof ApplicationCommandLoadingPlaceholder) && (this.autocompleteAdapter.getItem(3) instanceof ApplicationCommandLoadingPlaceholder) && (flattenCommandsModel.get(0) instanceof ApplicationCommandLoadingPlaceholder)) {
            this.autocompleteAdapter.scrollToPosition(3, 0, false);
        }
        ChatInputAutocompleteAdapter chatInputAutocompleteAdapter = this.autocompleteAdapter;
        if (commandBrowser.getDiscoverCommands().getLoadState() instanceof LoadState.Loading) {
            flattenCommandsModel = new ArrayList<>();
            int count = u.count(new IntRange(1, 4));
            for (int i2 = 0; i2 < count; i2++) {
                flattenCommandsModel.add(new ApplicationCommandLoadingPlaceholder(null, 1, null));
            }
        }
        chatInputAutocompleteAdapter.setData(flattenCommandsModel, false, commandBrowser.getDiscoverCommands().getLoadState() instanceof LoadState.Loading);
    }

    private final void configureSelectedCommand(SelectedCommandViewState selectedCommandViewState) {
        if (m.areEqual(selectedCommandViewState, SelectedCommandViewState.Hidden.INSTANCE)) {
            hideSelectedCommand();
        } else if (selectedCommandViewState instanceof SelectedCommandViewState.SelectedCommand) {
            configureSelectedCommand((SelectedCommandViewState.SelectedCommand) selectedCommandViewState);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(ViewState viewState) {
        configureAutocomplete(viewState.getAutocompleteViewState());
        configureSelectedCommand(viewState.getSelectedCommandViewState());
    }

    public static /* synthetic */ ApplicationCommandData getApplicationCommandData$default(InputAutocomplete inputAutocomplete, ApplicationCommandOption applicationCommandOption, int i, Object obj) {
        if ((i & 1) != 0) {
            applicationCommandOption = null;
        }
        return inputAutocomplete.getApplicationCommandData(applicationCommandOption);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Long getCurrentlySelectedCategory(int i) {
        Application application;
        ChatInputAutocompleteAdapter chatInputAutocompleteAdapter = this.autocompleteAdapter;
        Integer headerPositionForItem = chatInputAutocompleteAdapter.getHeaderPositionForItem(i);
        Autocompletable item = chatInputAutocompleteAdapter.getItem(headerPositionForItem != null ? headerPositionForItem.intValue() : 0);
        if (!(item instanceof ApplicationPlaceholder)) {
            item = null;
        }
        ApplicationPlaceholder applicationPlaceholder = (ApplicationPlaceholder) item;
        if (applicationPlaceholder == null || (application = applicationPlaceholder.getApplication()) == null) {
            return null;
        }
        return Long.valueOf(application.getId());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final AutocompleteViewModel getViewModel() {
        return (AutocompleteViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(Event event) {
        Function1<? super ApplicationCommandOption, Unit> function1;
        if (event instanceof Event.ScrollAutocompletablesToApplication) {
            Event.ScrollAutocompletablesToApplication scrollAutocompletablesToApplication = (Event.ScrollAutocompletablesToApplication) event;
            ChatInputAutocompleteAdapter.scrollToPosition$default(this.autocompleteAdapter, scrollAutocompletablesToApplication.getTargetPosition(), 0, false, 6, null);
            this.categoriesAdapter.selectApplication(scrollAutocompletablesToApplication.getApplicationId());
            if (this.categoriesAdapter.getPositionOfApplication(scrollAutocompletablesToApplication.getApplicationId()) != -1) {
                this.commandBrowserAppsRecyclerView.smoothScrollToPosition(this.categoriesAdapter.getPositionOfApplication(scrollAutocompletablesToApplication.getApplicationId()));
            }
        } else if ((event instanceof Event.RequestAutocompleteData) && (function1 = this.onPerformCommandAutocomplete) != null) {
            function1.invoke(((Event.RequestAutocompleteData) event).getOption());
        }
    }

    private final void hideAutocomplete() {
        this.autocompleteRecyclerView.setVisibility(8);
        this.commandBrowserAppsRecyclerView.setVisibility(8);
        this.stickersContainer.setVisibility(8);
        this.autocompleteHeader.setVisibility(8);
    }

    private final void hideSelectedCommand() {
        ConstraintLayout constraintLayout = this.selectedApplicationCommandUiBinding.a;
        m.checkNotNullExpressionValue(constraintLayout, "selectedApplicationCommandUiBinding.root");
        constraintLayout.setVisibility(8);
    }

    private final void removePillSpans(Spannable spannable) {
        Object[] spans;
        for (Object obj : spannable.getSpans(0, spannable.length(), Object.class)) {
            if (obj instanceof SimpleRoundedBackgroundSpan) {
                spannable.removeSpan(obj);
            }
        }
    }

    private final void removeSpans(Spannable spannable, IntRange intRange) {
        Object[] spans;
        for (Object obj : spannable.getSpans(intRange.getFirst(), intRange.getLast(), Object.class)) {
            if ((obj instanceof CharacterStyle) && !(obj instanceof SimpleRoundedBackgroundSpan)) {
                spannable.removeSpan(obj);
            }
        }
    }

    public static /* synthetic */ void removeSpans$default(InputAutocomplete inputAutocomplete, Spannable spannable, IntRange intRange, int i, Object obj) {
        if ((i & 2) != 0) {
            intRange = new IntRange(0, spannable.length());
        }
        inputAutocomplete.removeSpans(spannable, intRange);
    }

    public final ApplicationCommandData getApplicationCommandData(ApplicationCommandOption applicationCommandOption) {
        return getViewModel().getApplicationSendData(applicationCommandOption);
    }

    public final MessageContent getInputContent() {
        WidgetChatInputEditText.Companion companion = WidgetChatInputEditText.Companion;
        String stringSafe = companion.toStringSafe(this.editText);
        String stringSafe2 = companion.toStringSafe(this.editText);
        int length = stringSafe2.length() - 1;
        int i = 0;
        boolean z2 = false;
        while (i <= length) {
            boolean z3 = m.compare(stringSafe2.charAt(!z2 ? i : length), 32) <= 0;
            if (!z2) {
                if (!z3) {
                    z2 = true;
                } else {
                    i++;
                }
            } else if (!z3) {
                break;
            } else {
                length--;
            }
        }
        String obj = stringSafe2.subSequence(i, length + 1).toString();
        if (!TextUtils.isEmpty(obj)) {
            return getViewModel().replaceAutocompletableDataWithServerValues(stringSafe);
        }
        return new MessageContent(obj, n.emptyList());
    }

    public final Function1<ApplicationCommandOption, Unit> getOnPerformCommandAutocomplete() {
        return this.onPerformCommandAutocomplete;
    }

    public final void onCommandInputsSendError() {
        getViewModel().onApplicationCommandSendError();
    }

    public final void onViewBoundOrOnResume() {
        this.autocompleteAdapter.configureSubscriptions(this.fragment);
        Observable<ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel.observeViewSta…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this.fragment, null, 2, null), InputAutocomplete.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new InputAutocomplete$onViewBoundOrOnResume$1(this));
        Observable<InputEditTextAction> q2 = getViewModel().observeEditTextActions().q();
        m.checkNotNullExpressionValue(q2, "viewModel.observeEditTex…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q2, this.fragment, null, 2, null), InputAutocomplete.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new InputAutocomplete$onViewBoundOrOnResume$2(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(getViewModel().observeEvents(), this.fragment, null, 2, null), InputAutocomplete.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new InputAutocomplete$onViewBoundOrOnResume$3(this));
    }

    public final void setOnPerformCommandAutocomplete(Function1<? super ApplicationCommandOption, Unit> function1) {
        this.onPerformCommandAutocomplete = function1;
    }

    private final void configureSelectedCommand(SelectedCommandViewState.SelectedCommand selectedCommand) {
        ConstraintLayout constraintLayout = this.selectedApplicationCommandUiBinding.a;
        m.checkNotNullExpressionValue(constraintLayout, "selectedApplicationCommandUiBinding.root");
        constraintLayout.setVisibility(0);
        this.selectedApplicationCommandAdapter.clear();
        this.selectedApplicationCommandAdapter.setApplicationCommand(selectedCommand.getSelectedCommand(), selectedCommand.getSelectedApplication());
        ApplicationCommandOption selectedCommandOption = selectedCommand.getSelectedCommandOption();
        if (selectedCommandOption != null) {
            if (selectedCommand.getSelectedCommandOptionErrors().contains(selectedCommandOption)) {
                TextView textView = this.selectedApplicationCommandUiBinding.f2284b;
                m.checkNotNullExpressionValue(textView, "selectedApplicationComma…CommandsOptionDescription");
                Resources resources = this.editText.getResources();
                m.checkNotNullExpressionValue(resources, "editText.resources");
                textView.setText(StoreApplicationCommandsKt.getErrorText(selectedCommandOption, resources));
                WidgetChatInputApplicationCommandsBinding widgetChatInputApplicationCommandsBinding = this.selectedApplicationCommandUiBinding;
                TextView textView2 = widgetChatInputApplicationCommandsBinding.f2284b;
                ConstraintLayout constraintLayout2 = widgetChatInputApplicationCommandsBinding.a;
                m.checkNotNullExpressionValue(constraintLayout2, "selectedApplicationCommandUiBinding.root");
                textView2.setTextColor(ColorCompat.getColor(constraintLayout2.getContext(), (int) R.color.status_red_500));
            } else {
                TextView textView3 = this.selectedApplicationCommandUiBinding.f2284b;
                m.checkNotNullExpressionValue(textView3, "selectedApplicationComma…CommandsOptionDescription");
                Resources resources2 = this.editText.getResources();
                m.checkNotNullExpressionValue(resources2, "editText.resources");
                textView3.setText(StoreApplicationCommandsKt.getDescriptionText(selectedCommandOption, resources2));
                this.selectedApplicationCommandUiBinding.f2284b.setTextColor(ColorCompat.getThemedColor(this.editText.getContext(), (int) R.attr.colorTextNormal));
            }
            this.selectedApplicationCommandAdapter.highlightOption(selectedCommandOption);
        } else {
            TextView textView4 = this.selectedApplicationCommandUiBinding.f2284b;
            m.checkNotNullExpressionValue(textView4, "selectedApplicationComma…CommandsOptionDescription");
            ApplicationCommand selectedCommand2 = selectedCommand.getSelectedCommand();
            Resources resources3 = this.editText.getResources();
            m.checkNotNullExpressionValue(resources3, "editText.resources");
            textView4.setText(StoreApplicationCommandsKt.getDescriptionText(selectedCommand2, resources3));
            this.selectedApplicationCommandUiBinding.f2284b.setTextColor(ColorCompat.getThemedColor(this.editText.getContext(), (int) R.attr.colorTextNormal));
            this.selectedApplicationCommandAdapter.clearParamOptionHighlight();
        }
        this.selectedApplicationCommandAdapter.setVerified(selectedCommand.getValidSelectedCommandOptions(), selectedCommand.getSelectedCommandOptionErrors());
    }

    public /* synthetic */ InputAutocomplete(AppFragment appFragment, FlexEditText flexEditText, Long l, TextView textView, RecyclerView recyclerView, RecyclerView recyclerView2, View view, RecyclerView recyclerView3, TextView textView2, WidgetChatInputApplicationCommandsBinding widgetChatInputApplicationCommandsBinding, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(appFragment, flexEditText, (i & 4) != 0 ? null : l, textView, recyclerView, recyclerView2, view, recyclerView3, textView2, widgetChatInputApplicationCommandsBinding);
    }
}
