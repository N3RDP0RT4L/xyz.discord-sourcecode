package com.discord.widgets.chat.input.autocomplete;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: AutocompleteViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/ViewState;", "", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;", "component1", "()Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;", "Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;", "component2", "()Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;", "autocompleteViewState", "selectedCommandViewState", "copy", "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;)Lcom/discord/widgets/chat/input/autocomplete/ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;", "getAutocompleteViewState", "Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;", "getSelectedCommandViewState", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/autocomplete/AutocompleteViewState;Lcom/discord/widgets/chat/input/autocomplete/SelectedCommandViewState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewState {
    private final AutocompleteViewState autocompleteViewState;
    private final SelectedCommandViewState selectedCommandViewState;

    public ViewState(AutocompleteViewState autocompleteViewState, SelectedCommandViewState selectedCommandViewState) {
        m.checkNotNullParameter(autocompleteViewState, "autocompleteViewState");
        m.checkNotNullParameter(selectedCommandViewState, "selectedCommandViewState");
        this.autocompleteViewState = autocompleteViewState;
        this.selectedCommandViewState = selectedCommandViewState;
    }

    public static /* synthetic */ ViewState copy$default(ViewState viewState, AutocompleteViewState autocompleteViewState, SelectedCommandViewState selectedCommandViewState, int i, Object obj) {
        if ((i & 1) != 0) {
            autocompleteViewState = viewState.autocompleteViewState;
        }
        if ((i & 2) != 0) {
            selectedCommandViewState = viewState.selectedCommandViewState;
        }
        return viewState.copy(autocompleteViewState, selectedCommandViewState);
    }

    public final AutocompleteViewState component1() {
        return this.autocompleteViewState;
    }

    public final SelectedCommandViewState component2() {
        return this.selectedCommandViewState;
    }

    public final ViewState copy(AutocompleteViewState autocompleteViewState, SelectedCommandViewState selectedCommandViewState) {
        m.checkNotNullParameter(autocompleteViewState, "autocompleteViewState");
        m.checkNotNullParameter(selectedCommandViewState, "selectedCommandViewState");
        return new ViewState(autocompleteViewState, selectedCommandViewState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ViewState)) {
            return false;
        }
        ViewState viewState = (ViewState) obj;
        return m.areEqual(this.autocompleteViewState, viewState.autocompleteViewState) && m.areEqual(this.selectedCommandViewState, viewState.selectedCommandViewState);
    }

    public final AutocompleteViewState getAutocompleteViewState() {
        return this.autocompleteViewState;
    }

    public final SelectedCommandViewState getSelectedCommandViewState() {
        return this.selectedCommandViewState;
    }

    public int hashCode() {
        AutocompleteViewState autocompleteViewState = this.autocompleteViewState;
        int i = 0;
        int hashCode = (autocompleteViewState != null ? autocompleteViewState.hashCode() : 0) * 31;
        SelectedCommandViewState selectedCommandViewState = this.selectedCommandViewState;
        if (selectedCommandViewState != null) {
            i = selectedCommandViewState.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("ViewState(autocompleteViewState=");
        R.append(this.autocompleteViewState);
        R.append(", selectedCommandViewState=");
        R.append(this.selectedCommandViewState);
        R.append(")");
        return R.toString();
    }
}
