package com.discord.widgets.chat.input.autocomplete.sources;

import andhook.lib.HookHelper;
import com.discord.api.channel.Channel;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.api.commands.CommandChoice;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandKt;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.models.member.GuildMember;
import com.discord.stores.DiscoverCommands;
import com.discord.stores.StoreApplicationCommands;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.chat.input.WidgetChatInputDiscoveryCommandsModel;
import com.discord.widgets.chat.input.autocomplete.ApplicationCommandAutocompletable;
import com.discord.widgets.chat.input.autocomplete.ApplicationCommandChoiceAutocompletable;
import com.discord.widgets.chat.input.autocomplete.Autocompletable;
import com.discord.widgets.chat.input.autocomplete.AutocompletableComparator;
import com.discord.widgets.chat.input.autocomplete.LeadingIdentifier;
import d0.t.g0;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func4;
import rx.functions.Func5;
/* compiled from: ApplicationCommandsAutocompletableSource.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 (2\u00020\u0001:\u0001(B\u001f\u0012\u0006\u0010\"\u001a\u00020!\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b&\u0010'J[\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0010\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00060\u00052\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00052\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\u0005H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J-\u0010\u0015\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\f0\u00142\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\u0018\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u0019\u0010\u001d\u001a\u00020\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%¨\u0006)"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/sources/ApplicationCommandsAutocompletableSource;", "", "", "Lcom/discord/primitives/UserId;", "myId", "", "Lcom/discord/primitives/RoleId;", "myChannelRoles", "Lcom/discord/models/commands/Application;", "apps", "Lcom/discord/models/commands/ApplicationCommand;", "appCommands", "", "Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "Ljava/util/TreeSet;", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "createAutocompletablesForApplicationCommands", "(JLjava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/Map;", "Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "observeApplicationCommandAutocompletables", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "Lcom/discord/stores/StoreApplicationCommands;", "storeApplicationCommands", "Lcom/discord/stores/StoreApplicationCommands;", "getStoreApplicationCommands", "()Lcom/discord/stores/StoreApplicationCommands;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "getStoreGuilds", "()Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreUser;", "getStoreUsers", "()Lcom/discord/stores/StoreUser;", HookHelper.constructorName, "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreApplicationCommands;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ApplicationCommandsAutocompletableSource {
    public static final Companion Companion = new Companion(null);
    private final StoreApplicationCommands storeApplicationCommands;
    private final StoreGuilds storeGuilds;
    private final StoreUser storeUsers;

    /* compiled from: ApplicationCommandsAutocompletableSource.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007J+\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/sources/ApplicationCommandsAutocompletableSource$Companion;", "", "Lcom/discord/models/commands/ApplicationCommandOption;", "option", "", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "createFromCommandOption", "(Lcom/discord/models/commands/ApplicationCommandOption;)Ljava/util/List;", "", "includeHeaders", "", "placeholdersCount", "Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/WidgetChatInputDiscoveryCommandsModel;", "getDiscoveryCommands", "(ZILcom/discord/api/channel/Channel;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final List<Autocompletable> createFromCommandOption(ApplicationCommandOption applicationCommandOption) {
            m.checkNotNullParameter(applicationCommandOption, "option");
            ArrayList arrayList = new ArrayList();
            if (applicationCommandOption.getType() == ApplicationCommandType.BOOLEAN) {
                arrayList.add(new ApplicationCommandChoiceAutocompletable(new CommandChoice("true", "true")));
                arrayList.add(new ApplicationCommandChoiceAutocompletable(new CommandChoice("false", "false")));
            } else {
                List<CommandChoice> choices = applicationCommandOption.getChoices();
                if (!(choices == null || choices.isEmpty())) {
                    List<CommandChoice> choices2 = applicationCommandOption.getChoices();
                    ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(choices2, 10));
                    for (CommandChoice commandChoice : choices2) {
                        arrayList2.add(new ApplicationCommandChoiceAutocompletable(commandChoice));
                    }
                    arrayList.addAll(arrayList2);
                }
            }
            return arrayList;
        }

        public final Observable<WidgetChatInputDiscoveryCommandsModel> getDiscoveryCommands(final boolean z2, final int i, Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<WidgetChatInputDiscoveryCommandsModel> g = Observable.g(companion.getUsers().observeMeId(), companion.getGuilds().observeComputed(channel.f()), companion.getApplicationCommands().observeDiscoverCommands(channel.h()), companion.getApplicationCommands().observeGuildApplications(channel.h()), companion.getApplicationCommands().observeFrecencyCommands(channel.f()), new Func5<Long, Map<Long, ? extends GuildMember>, DiscoverCommands, List<? extends Application>, List<? extends ApplicationCommand>, WidgetChatInputDiscoveryCommandsModel>() { // from class: com.discord.widgets.chat.input.autocomplete.sources.ApplicationCommandsAutocompletableSource$Companion$getDiscoveryCommands$1
                @Override // rx.functions.Func5
                public /* bridge */ /* synthetic */ WidgetChatInputDiscoveryCommandsModel call(Long l, Map<Long, ? extends GuildMember> map, DiscoverCommands discoverCommands, List<? extends Application> list, List<? extends ApplicationCommand> list2) {
                    return call2(l, (Map<Long, GuildMember>) map, discoverCommands, (List<Application>) list, list2);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetChatInputDiscoveryCommandsModel call2(Long l, Map<Long, GuildMember> map, DiscoverCommands discoverCommands, List<Application> list, List<? extends ApplicationCommand> list2) {
                    WidgetChatInputDiscoveryCommandsModel.Companion companion2 = WidgetChatInputDiscoveryCommandsModel.Companion;
                    m.checkNotNullExpressionValue(l, "meId");
                    long longValue = l.longValue();
                    GuildMember guildMember = map.get(l);
                    List<Long> roles = guildMember != null ? guildMember.getRoles() : null;
                    if (roles == null) {
                        roles = n.emptyList();
                    }
                    m.checkNotNullExpressionValue(list, "apps");
                    m.checkNotNullExpressionValue(discoverCommands, "discoveryCommands");
                    boolean z3 = z2;
                    int i2 = i;
                    m.checkNotNullExpressionValue(list2, "frecency");
                    return companion2.parseModelDiscoveryCommands(longValue, roles, list, discoverCommands, z3, i2, list2);
                }
            });
            m.checkNotNullExpressionValue(g, "Observable.combineLatest…recency\n        )\n      }");
            return g;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ApplicationCommandsAutocompletableSource(StoreUser storeUser, StoreGuilds storeGuilds, StoreApplicationCommands storeApplicationCommands) {
        m.checkNotNullParameter(storeUser, "storeUsers");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeApplicationCommands, "storeApplicationCommands");
        this.storeUsers = storeUser;
        this.storeGuilds = storeGuilds;
        this.storeApplicationCommands = storeApplicationCommands;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<LeadingIdentifier, TreeSet<Autocompletable>> createAutocompletablesForApplicationCommands(long j, List<Long> list, List<Application> list2, List<? extends ApplicationCommand> list3) {
        TreeSet treeSet = new TreeSet(new AutocompletableComparator());
        HashMap hashMap = new HashMap();
        for (Application application : list2) {
            hashMap.put(Long.valueOf(application.getId()), application);
        }
        for (ApplicationCommand applicationCommand : list3) {
            treeSet.add(new ApplicationCommandAutocompletable((Application) hashMap.get(Long.valueOf(applicationCommand.getApplicationId())), applicationCommand, ApplicationCommandKt.hasPermission(applicationCommand, j, list), true));
        }
        return g0.mapOf(d0.o.to(LeadingIdentifier.APP_COMMAND, treeSet));
    }

    public final StoreApplicationCommands getStoreApplicationCommands() {
        return this.storeApplicationCommands;
    }

    public final StoreGuilds getStoreGuilds() {
        return this.storeGuilds;
    }

    public final StoreUser getStoreUsers() {
        return this.storeUsers;
    }

    public final Observable<Map<LeadingIdentifier, TreeSet<Autocompletable>>> observeApplicationCommandAutocompletables(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        Observable h = Observable.h(ObservableExtensionsKt.computationLatest(this.storeUsers.observeMeId()), ObservableExtensionsKt.computationLatest(this.storeGuilds.observeComputed(channel.f())), ObservableExtensionsKt.computationLatest(this.storeApplicationCommands.observeGuildApplications(channel.h())), ObservableExtensionsKt.computationLatest(this.storeApplicationCommands.observeQueryCommands(channel.h())), new Func4<Long, Map<Long, ? extends GuildMember>, List<? extends Application>, List<? extends ApplicationCommand>, Map<LeadingIdentifier, ? extends TreeSet<Autocompletable>>>() { // from class: com.discord.widgets.chat.input.autocomplete.sources.ApplicationCommandsAutocompletableSource$observeApplicationCommandAutocompletables$1
            @Override // rx.functions.Func4
            public /* bridge */ /* synthetic */ Map<LeadingIdentifier, ? extends TreeSet<Autocompletable>> call(Long l, Map<Long, ? extends GuildMember> map, List<? extends Application> list, List<? extends ApplicationCommand> list2) {
                return call2(l, (Map<Long, GuildMember>) map, (List<Application>) list, list2);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Map<LeadingIdentifier, TreeSet<Autocompletable>> call2(Long l, Map<Long, GuildMember> map, List<Application> list, List<? extends ApplicationCommand> list2) {
                Map<LeadingIdentifier, TreeSet<Autocompletable>> createAutocompletablesForApplicationCommands;
                m.checkNotNullExpressionValue(list2, "queries");
                if (!(!list2.isEmpty())) {
                    return g0.mapOf(d0.o.to(LeadingIdentifier.APP_COMMAND, new TreeSet()));
                }
                ApplicationCommandsAutocompletableSource applicationCommandsAutocompletableSource = ApplicationCommandsAutocompletableSource.this;
                m.checkNotNullExpressionValue(l, "myId");
                long longValue = l.longValue();
                GuildMember guildMember = map.get(l);
                List<Long> roles = guildMember != null ? guildMember.getRoles() : null;
                if (roles == null) {
                    roles = n.emptyList();
                }
                m.checkNotNullExpressionValue(list, "apps");
                createAutocompletablesForApplicationCommands = applicationCommandsAutocompletableSource.createAutocompletablesForApplicationCommands(longValue, roles, list, list2);
                return createAutocompletablesForApplicationCommands;
            }
        });
        m.checkNotNullExpressionValue(h, "Observable.combineLatest… TreeSet())\n      }\n    }");
        Observable<Map<LeadingIdentifier, TreeSet<Autocompletable>>> q = ObservableExtensionsKt.computationLatest(h).q();
        m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
        return q;
    }
}
