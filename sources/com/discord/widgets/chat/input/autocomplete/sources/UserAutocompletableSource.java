package com.discord.widgets.chat.input.autocomplete.sources;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelRecipientNick;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.member.GuildMember;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserPresence;
import com.discord.utilities.collections.ShallowPartitionCollection;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.widgets.chat.input.autocomplete.Autocompletable;
import com.discord.widgets.chat.input.autocomplete.AutocompletableComparator;
import com.discord.widgets.chat.input.autocomplete.GlobalRoleAutocompletable;
import com.discord.widgets.chat.input.autocomplete.LeadingIdentifier;
import com.discord.widgets.chat.input.autocomplete.RoleAutocompletable;
import com.discord.widgets.chat.input.autocomplete.UserAutocompletable;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
import rx.functions.Func8;
/* compiled from: UserMentionableSource.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 <2\u00020\u0001:\u0001<B/\u0012\u0006\u0010,\u001a\u00020+\u0012\u0006\u00106\u001a\u000205\u0012\u0006\u00101\u001a\u000200\u0012\u0006\u0010\"\u001a\u00020!\u0012\u0006\u0010'\u001a\u00020&¢\u0006\u0004\b:\u0010;J»\u0001\u0010\u0017\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\u00052\n\u0010\b\u001a\u00060\u0002j\u0002`\u00032\u0016\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\n0\t2\u0016\u0010\r\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\f0\t2\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u000e0\t2\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00100\t2\u000e\u0010\u0013\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0012H\u0002¢\u0006\u0004\b\u0017\u0010\u0018Ja\u0010\u001c\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\t2\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u000e0\t2\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00100\t2\u000e\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019H\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ-\u0010\u001f\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\t0\u001e2\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u001f\u0010 R\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u0019\u0010'\u001a\u00020&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u0019\u0010,\u001a\u00020+8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R\u0019\u00101\u001a\u0002008\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u0019\u00106\u001a\u0002058\u0006@\u0006¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109¨\u0006="}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/sources/UserAutocompletableSource;", "", "", "Lcom/discord/primitives/UserId;", "myId", "Lcom/discord/api/channel/Channel;", "channel", "parentChannel", "guildOwnerId", "", "Lcom/discord/api/role/GuildRole;", "roles", "Lcom/discord/models/member/GuildMember;", "members", "Lcom/discord/models/user/User;", "users", "Lcom/discord/models/presence/Presence;", "presences", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "Ljava/util/TreeSet;", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "createAutocompletablesForUsers", "(JLcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;JLjava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;)Ljava/util/Map;", "", "Lcom/discord/api/channel/ChannelRecipientNick;", "nicks", "createAutocompletablesForDmUsers", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)Ljava/util/Map;", "Lrx/Observable;", "observeUserAutocompletables", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "getStorePermissions", "()Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "getStoreChannels", "()Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreUser;", "getStoreUsers", "()Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreUserPresence;", "storePresences", "Lcom/discord/stores/StoreUserPresence;", "getStorePresences", "()Lcom/discord/stores/StoreUserPresence;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "getStoreGuilds", "()Lcom/discord/stores/StoreGuilds;", HookHelper.constructorName, "(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreChannels;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserAutocompletableSource {
    public static final Companion Companion = new Companion(null);
    private static final int PARTITION_HUGE_GUILD_SIZE = 3000;
    private static final int PARTITION_IDEAL_PARTITION_SIZE = 100;
    private final StoreChannels storeChannels;
    private final StoreGuilds storeGuilds;
    private final StorePermissions storePermissions;
    private final StoreUserPresence storePresences;
    private final StoreUser storeUsers;

    /* compiled from: UserMentionableSource.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/sources/UserAutocompletableSource$Companion;", "", "", "PARTITION_HUGE_GUILD_SIZE", "I", "PARTITION_IDEAL_PARTITION_SIZE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public UserAutocompletableSource(StoreUser storeUser, StoreGuilds storeGuilds, StoreUserPresence storeUserPresence, StorePermissions storePermissions, StoreChannels storeChannels) {
        m.checkNotNullParameter(storeUser, "storeUsers");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeUserPresence, "storePresences");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        this.storeUsers = storeUser;
        this.storeGuilds = storeGuilds;
        this.storePresences = storeUserPresence;
        this.storePermissions = storePermissions;
        this.storeChannels = storeChannels;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<LeadingIdentifier, TreeSet<Autocompletable>> createAutocompletablesForDmUsers(Map<Long, ? extends User> map, Map<Long, Presence> map2, List<ChannelRecipientNick> list) {
        ChannelRecipientNick channelRecipientNick;
        TreeSet treeSet = new TreeSet(new AutocompletableComparator());
        for (User user : map.values()) {
            String str = null;
            if (list != null) {
                int size = list.size();
                channelRecipientNick = null;
                for (int i = 0; i < size; i++) {
                    if (list.get(i).a() == user.getId()) {
                        channelRecipientNick = list.get(i);
                    }
                }
            } else {
                channelRecipientNick = null;
            }
            if (channelRecipientNick != null) {
                str = channelRecipientNick.c();
            }
            treeSet.add(new UserAutocompletable(user, null, str, (Presence) a.e(user, map2), false, 16, null));
        }
        return g0.mapOf(o.to(LeadingIdentifier.MENTION, treeSet));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Map<LeadingIdentifier, TreeSet<Autocompletable>> createAutocompletablesForUsers(long j, Channel channel, Channel channel2, long j2, Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Map<Long, ? extends User> map3, Map<Long, Presence> map4, Long l) {
        Collection collection;
        boolean z2;
        boolean can;
        if (map2.size() > 3000) {
            int size = map2.size() / 100;
            collection = ShallowPartitionCollection.Companion.withArrayListPartions(size, new UserAutocompletableSource$createAutocompletablesForUsers$mentions$1(size));
        } else {
            collection = new ArrayList(map2.size());
        }
        boolean canEveryone = PermissionUtils.canEveryone(Permission.VIEW_CHANNEL, channel, channel2, map);
        for (Map.Entry<Long, GuildMember> entry : map2.entrySet()) {
            long longValue = entry.getKey().longValue();
            GuildMember value = entry.getValue();
            if (canEveryone || (can = PermissionUtils.can(Permission.VIEW_CHANNEL, Long.valueOf(PermissionUtils.computePermissions(longValue, channel, channel2, j2, value, map, null, true))))) {
                User user = map3.get(Long.valueOf(longValue));
                if (user != null) {
                    collection.add(new UserAutocompletable(user, value, value.getNick(), map4.get(Long.valueOf(longValue)), false, 16, null));
                }
            } else {
                User user2 = map3.get(Long.valueOf(longValue));
                if (user2 != null) {
                    collection.add(new UserAutocompletable(user2, value, value.getNick(), map4.get(Long.valueOf(longValue)), can));
                }
            }
        }
        if (map2.get(Long.valueOf(j)) != null) {
            z2 = PermissionUtils.can(Permission.MENTION_EVERYONE, l);
            if (z2) {
                GlobalRoleAutocompletable.Companion companion = GlobalRoleAutocompletable.Companion;
                collection.add(companion.getHere());
                collection.add(companion.getEveryone());
            }
        } else {
            z2 = false;
        }
        for (GuildRole guildRole : map.values()) {
            if (guildRole.getId() != channel.f()) {
                collection.add(new RoleAutocompletable(guildRole, guildRole.f() || z2));
            }
        }
        TreeSet treeSet = new TreeSet(new AutocompletableComparator());
        treeSet.addAll(collection);
        return g0.mapOf(o.to(LeadingIdentifier.MENTION, treeSet));
    }

    public final StoreChannels getStoreChannels() {
        return this.storeChannels;
    }

    public final StoreGuilds getStoreGuilds() {
        return this.storeGuilds;
    }

    public final StorePermissions getStorePermissions() {
        return this.storePermissions;
    }

    public final StoreUserPresence getStorePresences() {
        return this.storePresences;
    }

    public final StoreUser getStoreUsers() {
        return this.storeUsers;
    }

    public final Observable<Map<LeadingIdentifier, TreeSet<Autocompletable>>> observeUserAutocompletables(final Channel channel) {
        boolean isTextOrVoiceChannel;
        Observable observable;
        boolean isDmOrGroupDm;
        m.checkNotNullParameter(channel, "channel");
        long f = channel.f();
        isTextOrVoiceChannel = UserMentionableSourceKt.isTextOrVoiceChannel(channel);
        if (isTextOrVoiceChannel) {
            Observable computationLatest = ObservableExtensionsKt.computationLatest(this.storeUsers.observeMeId());
            Observable<R> F = this.storeGuilds.observeGuild(f).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
            m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
            Observable q = F.F(UserAutocompletableSource$observeUserAutocompletables$1.INSTANCE).q();
            m.checkNotNullExpressionValue(q, "storeGuilds.observeGuild… }.distinctUntilChanged()");
            Observable computationLatest2 = ObservableExtensionsKt.computationLatest(q);
            Observable computationLatest3 = ObservableExtensionsKt.computationLatest(this.storeGuilds.observeRoles(f));
            Observable<Map<Long, GuildMember>> observeComputed = this.storeGuilds.observeComputed(f);
            TimeUnit timeUnit = TimeUnit.SECONDS;
            Observable computationLatest4 = ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.leadingEdgeThrottle(observeComputed, 5L, timeUnit));
            Observable computationLatest5 = ObservableExtensionsKt.computationLatest(this.storeUsers.observeAllUsers());
            Observable computationLatest6 = ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.leadingEdgeThrottle(this.storePresences.observeAllPresences(), 10L, timeUnit));
            Observable<Long> q2 = this.storePermissions.observePermissionsForChannel(channel.h()).q();
            m.checkNotNullExpressionValue(q2, "storePermissions.observe…d).distinctUntilChanged()");
            Observable computationLatest7 = ObservableExtensionsKt.computationLatest(q2);
            Observable<Channel> q3 = this.storeChannels.observeChannel(channel.r()).q();
            m.checkNotNullExpressionValue(q3, "storeChannels.observeCha…d).distinctUntilChanged()");
            observable = Observable.d(computationLatest, computationLatest2, computationLatest3, computationLatest4, computationLatest5, computationLatest6, computationLatest7, ObservableExtensionsKt.computationLatest(q3), new Func8<Long, Long, Map<Long, ? extends GuildRole>, Map<Long, ? extends GuildMember>, Map<Long, ? extends User>, Map<Long, ? extends Presence>, Long, Channel, Map<LeadingIdentifier, ? extends TreeSet<Autocompletable>>>() { // from class: com.discord.widgets.chat.input.autocomplete.sources.UserAutocompletableSource$observeUserAutocompletables$2
                @Override // rx.functions.Func8
                public /* bridge */ /* synthetic */ Map<LeadingIdentifier, ? extends TreeSet<Autocompletable>> call(Long l, Long l2, Map<Long, ? extends GuildRole> map, Map<Long, ? extends GuildMember> map2, Map<Long, ? extends User> map3, Map<Long, ? extends Presence> map4, Long l3, Channel channel2) {
                    return call2(l, l2, (Map<Long, GuildRole>) map, (Map<Long, GuildMember>) map2, map3, (Map<Long, Presence>) map4, l3, channel2);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Map<LeadingIdentifier, TreeSet<Autocompletable>> call2(Long l, Long l2, Map<Long, GuildRole> map, Map<Long, GuildMember> map2, Map<Long, ? extends User> map3, Map<Long, Presence> map4, Long l3, Channel channel2) {
                    Map<LeadingIdentifier, TreeSet<Autocompletable>> createAutocompletablesForUsers;
                    UserAutocompletableSource userAutocompletableSource = UserAutocompletableSource.this;
                    m.checkNotNullExpressionValue(l, "myId");
                    long longValue = l.longValue();
                    Channel channel3 = channel;
                    m.checkNotNullExpressionValue(l2, "guildOwnerId");
                    long longValue2 = l2.longValue();
                    m.checkNotNullExpressionValue(map, "roles");
                    m.checkNotNullExpressionValue(map2, "members");
                    m.checkNotNullExpressionValue(map3, "users");
                    m.checkNotNullExpressionValue(map4, "presences");
                    createAutocompletablesForUsers = userAutocompletableSource.createAutocompletablesForUsers(longValue, channel3, channel2, longValue2, map, map2, map3, map4, l3);
                    return createAutocompletablesForUsers;
                }
            });
        } else {
            isDmOrGroupDm = UserMentionableSourceKt.isDmOrGroupDm(channel);
            if (isDmOrGroupDm) {
                observable = Observable.j(Observable.A(ChannelUtils.g(channel)).F(UserAutocompletableSource$observeUserAutocompletables$3.INSTANCE).f0(), this.storeUsers.observeMeId(), UserAutocompletableSource$observeUserAutocompletables$4.INSTANCE).Y(new b<List<Long>, Observable<? extends Map<LeadingIdentifier, ? extends TreeSet<Autocompletable>>>>() { // from class: com.discord.widgets.chat.input.autocomplete.sources.UserAutocompletableSource$observeUserAutocompletables$5
                    public final Observable<? extends Map<LeadingIdentifier, TreeSet<Autocompletable>>> call(List<Long> list) {
                        StoreUser storeUsers = UserAutocompletableSource.this.getStoreUsers();
                        m.checkNotNullExpressionValue(list, "recipientIds");
                        return Observable.j(storeUsers.observeUsers(list), UserAutocompletableSource.this.getStorePresences().observePresencesForUsers(list), new Func2<Map<Long, ? extends User>, Map<Long, ? extends Presence>, Map<LeadingIdentifier, ? extends TreeSet<Autocompletable>>>() { // from class: com.discord.widgets.chat.input.autocomplete.sources.UserAutocompletableSource$observeUserAutocompletables$5.1
                            @Override // rx.functions.Func2
                            public /* bridge */ /* synthetic */ Map<LeadingIdentifier, ? extends TreeSet<Autocompletable>> call(Map<Long, ? extends User> map, Map<Long, ? extends Presence> map2) {
                                return call2(map, (Map<Long, Presence>) map2);
                            }

                            /* renamed from: call  reason: avoid collision after fix types in other method */
                            public final Map<LeadingIdentifier, TreeSet<Autocompletable>> call2(Map<Long, ? extends User> map, Map<Long, Presence> map2) {
                                Map<LeadingIdentifier, TreeSet<Autocompletable>> createAutocompletablesForDmUsers;
                                UserAutocompletableSource userAutocompletableSource = UserAutocompletableSource.this;
                                m.checkNotNullExpressionValue(map, "users");
                                m.checkNotNullExpressionValue(map2, "presences");
                                createAutocompletablesForDmUsers = userAutocompletableSource.createAutocompletablesForDmUsers(map, map2, channel.n());
                                return createAutocompletablesForDmUsers;
                            }
                        }).q();
                    }
                });
            } else {
                observable = new k(h0.emptyMap());
            }
        }
        m.checkNotNullExpressionValue(observable, "when {\n      // Guild Ch…vable.just(mapOf())\n    }");
        Observable<Map<LeadingIdentifier, TreeSet<Autocompletable>>> q4 = ObservableExtensionsKt.computationLatest(observable).q();
        m.checkNotNullExpressionValue(q4, "when {\n      // Guild Ch…  .distinctUntilChanged()");
        return q4;
    }
}
