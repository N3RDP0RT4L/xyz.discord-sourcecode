package com.discord.widgets.chat.input.autocomplete.sources;

import com.discord.api.channel.Channel;
import kotlin.Metadata;
/* compiled from: UserMentionableSource.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\u001a\u0013\u0010\u0002\u001a\u00020\u0001*\u00020\u0000H\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u001a\u0013\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u0002¢\u0006\u0004\b\u0004\u0010\u0003¨\u0006\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "", "isTextOrVoiceChannel", "(Lcom/discord/api/channel/Channel;)Z", "isDmOrGroupDm", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class UserMentionableSourceKt {
    /* JADX INFO: Access modifiers changed from: private */
    public static final boolean isDmOrGroupDm(Channel channel) {
        return channel.A() == 3 || channel.A() == 1;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static final boolean isTextOrVoiceChannel(Channel channel) {
        return channel.A() == 5 || channel.A() == 0 || channel.A() == 11 || channel.A() == 12 || channel.A() == 2;
    }
}
