package com.discord.widgets.chat.input.autocomplete.commands;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.annotation.VisibleForTesting;
import b.d.b.a.a;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.models.commands.ApplicationCommand;
import com.discord.models.commands.ApplicationCommandKt;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.input.autocomplete.InputEditTextAction;
import com.discord.widgets.chat.input.models.AutocompleteApplicationCommands;
import com.discord.widgets.chat.input.models.CommandOptionValue;
import com.discord.widgets.chat.input.models.InputCommandContext;
import com.discord.widgets.chat.input.models.MentionInputModel;
import com.discord.widgets.chat.input.models.OptionRange;
import d0.g0.e;
import d0.g0.t;
import d0.g0.w;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.ranges.IntRange;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
/* compiled from: AutocompleteCommandUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0010\u0004\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\bO\u0010PJ\u0017\u0010\u0004\u001a\u0004\u0018\u00010\u0003*\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\u0007\u001a\u00020\u0006*\u00020\u0003H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\t\u001a\u0004\u0018\u00010\u0003*\u0004\u0018\u00010\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\t\u001a\u0004\u0018\u00010\u000b*\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\t\u0010\fJ\u0017\u0010\t\u001a\u0004\u0018\u00010\r*\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\t\u0010\u000eJC\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\r2\u0010\u0010\u0014\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u00130\u00122\u0006\u0010\u0016\u001a\u00020\u00152\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\b\u001a\u0010\u001bJ-\u0010!\u001a\u0004\u0018\u00010\u001e2\u0006\u0010\u001c\u001a\u00020\u000b2\u0014\u0010 \u001a\u0010\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001f\u0018\u00010\u001d¢\u0006\u0004\b!\u0010\"J\u0017\u0010&\u001a\u00020%2\u0006\u0010$\u001a\u00020#H\u0007¢\u0006\u0004\b&\u0010'J+\u0010+\u001a\u00020*2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010(\u001a\u00020\u001e2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\u000bH\u0007¢\u0006\u0004\b+\u0010,JY\u00103\u001a\b\u0012\u0004\u0012\u00020\u001e022\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\b\u0010-\u001a\u0004\u0018\u00010\u001e2\b\u0010.\u001a\u0004\u0018\u00010\u001e2\u0012\u0010/\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00060\u001d2\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u0002000\u001d¢\u0006\u0004\b3\u00104J5\u00105\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u00060\u001d2\u0006\u0010\u0018\u001a\u00020\u00172\u0012\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u0002000\u001d¢\u0006\u0004\b5\u00106J1\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u0002000\u001d2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u00107\u001a\u00020\u00172\u0006\u00109\u001a\u000208¢\u0006\u0004\b:\u0010;JK\u0010@\u001a\u0004\u0018\u00010\u00172\f\u0010<\u001a\b\u0012\u0004\u0012\u00020\u00170\u00122\u0006\u0010=\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00022\n\u0010\u0011\u001a\u00060\rj\u0002`>2\u0010\u0010?\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u00130\u0012¢\u0006\u0004\b@\u0010AJ\u0013\u0010B\u001a\u0004\u0018\u00010\u0002*\u00020\u000f¢\u0006\u0004\bB\u0010CJ'\u0010D\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001f0\u001d*\u00020\u000f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\bD\u0010EJ%\u0010H\u001a\u0004\u0018\u00010G*\u00020\u000f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010F\u001a\u00020\u0002¢\u0006\u0004\bH\u0010IJ\u001d\u0010J\u001a\u0004\u0018\u00010\u000b*\u00020\u000f2\u0006\u0010F\u001a\u00020\u0002H\u0007¢\u0006\u0004\bJ\u0010KJ\u001d\u0010L\u001a\u0004\u0018\u00010\u000b*\u00020\u000f2\u0006\u0010F\u001a\u00020\u0002H\u0007¢\u0006\u0004\bL\u0010KJ\u0015\u0010M\u001a\u0004\u0018\u00010\u0002*\u0004\u0018\u00010\u0002¢\u0006\u0004\bM\u0010N¨\u0006Q"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/commands/AutocompleteCommandUtils;", "", "", "", "parseNumber", "(Ljava/lang/String;)Ljava/lang/Number;", "", "isSafeNumber", "(Ljava/lang/Number;)Z", "asSafeNumberOrNull", "(Ljava/lang/Number;)Ljava/lang/Number;", "", "(Ljava/lang/Integer;)Ljava/lang/Integer;", "", "(Ljava/lang/Long;)Ljava/lang/Long;", "", "input", "userId", "", "Lcom/discord/primitives/RoleId;", "userRoles", "Lcom/discord/widgets/chat/input/models/AutocompleteApplicationCommands;", "applicationCommands", "Lcom/discord/models/commands/ApplicationCommand;", "selectedCommand", "Lcom/discord/widgets/chat/input/models/InputCommandContext;", "getInputCommandContext", "(Ljava/lang/CharSequence;JLjava/util/List;Lcom/discord/widgets/chat/input/models/AutocompleteApplicationCommands;Lcom/discord/models/commands/ApplicationCommand;)Lcom/discord/widgets/chat/input/models/InputCommandContext;", "cursorPosition", "", "Lcom/discord/models/commands/ApplicationCommandOption;", "Lcom/discord/widgets/chat/input/models/OptionRange;", "optionRanges", "getSelectedCommandOption", "(ILjava/util/Map;)Lcom/discord/models/commands/ApplicationCommandOption;", "Lcom/discord/widgets/chat/input/models/MentionInputModel$VerifiedCommandInputModel;", "model", "Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "appendTextForCommandForInput", "(Lcom/discord/widgets/chat/input/models/MentionInputModel$VerifiedCommandInputModel;)Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction;", "applicationCommandsOption", "insertIndex", "Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction$InsertText;", "appendParam", "(Ljava/lang/CharSequence;Lcom/discord/models/commands/ApplicationCommandOption;Ljava/lang/Integer;)Lcom/discord/widgets/chat/input/autocomplete/InputEditTextAction$InsertText;", "selectedCommandOption", "previouslySelected", "validMap", "Lcom/discord/widgets/chat/input/models/CommandOptionValue;", "parsedCommandOptions", "", "getErrorsToShowForCommandParameters", "(Lcom/discord/models/commands/ApplicationCommand;Lcom/discord/models/commands/ApplicationCommandOption;Lcom/discord/models/commands/ApplicationCommandOption;Ljava/util/Map;Ljava/util/Map;)Ljava/util/Set;", "getInputValidity", "(Lcom/discord/models/commands/ApplicationCommand;Ljava/util/Map;)Ljava/util/Map;", "command", "Lcom/discord/widgets/chat/input/models/ChatInputMentionsMap;", "mentionMap", "parseCommandOptions", "(Ljava/lang/CharSequence;Lcom/discord/models/commands/ApplicationCommand;Lcom/discord/widgets/chat/input/models/ChatInputMentionsMap;)Ljava/util/Map;", "commands", "prefix", "Lcom/discord/primitives/UserId;", "roles", "getSelectedCommand", "(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;JLjava/util/List;)Lcom/discord/models/commands/ApplicationCommand;", "getCommandPrefix", "(Ljava/lang/CharSequence;)Ljava/lang/String;", "findOptionRanges", "(Ljava/lang/CharSequence;Lcom/discord/models/commands/ApplicationCommand;)Ljava/util/Map;", "paramName", "Lkotlin/ranges/IntRange;", "findValueRange", "(Ljava/lang/CharSequence;Lcom/discord/models/commands/ApplicationCommand;Ljava/lang/String;)Lkotlin/ranges/IntRange;", "findStartOfParam", "(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/Integer;", "findStartOfValue", "transformParameterSpannableString", "(Ljava/lang/String;)Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AutocompleteCommandUtils {
    public static final AutocompleteCommandUtils INSTANCE = new AutocompleteCommandUtils();

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            ApplicationCommandType.values();
            int[] iArr = new int[12];
            $EnumSwitchMapping$0 = iArr;
            ApplicationCommandType applicationCommandType = ApplicationCommandType.USER;
            iArr[applicationCommandType.ordinal()] = 1;
            ApplicationCommandType applicationCommandType2 = ApplicationCommandType.CHANNEL;
            iArr[applicationCommandType2.ordinal()] = 2;
            ApplicationCommandType applicationCommandType3 = ApplicationCommandType.ROLE;
            iArr[applicationCommandType3.ordinal()] = 3;
            ApplicationCommandType applicationCommandType4 = ApplicationCommandType.MENTIONABLE;
            iArr[applicationCommandType4.ordinal()] = 4;
            ApplicationCommandType.values();
            int[] iArr2 = new int[12];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[ApplicationCommandType.SUBCOMMAND.ordinal()] = 1;
            iArr2[ApplicationCommandType.SUBCOMMAND_GROUP.ordinal()] = 2;
            ApplicationCommandType applicationCommandType5 = ApplicationCommandType.STRING;
            iArr2[applicationCommandType5.ordinal()] = 3;
            ApplicationCommandType applicationCommandType6 = ApplicationCommandType.INTEGER;
            iArr2[applicationCommandType6.ordinal()] = 4;
            ApplicationCommandType applicationCommandType7 = ApplicationCommandType.NUMBER;
            iArr2[applicationCommandType7.ordinal()] = 5;
            ApplicationCommandType applicationCommandType8 = ApplicationCommandType.BOOLEAN;
            iArr2[applicationCommandType8.ordinal()] = 6;
            iArr2[applicationCommandType.ordinal()] = 7;
            iArr2[applicationCommandType2.ordinal()] = 8;
            iArr2[applicationCommandType3.ordinal()] = 9;
            iArr2[applicationCommandType4.ordinal()] = 10;
            iArr2[ApplicationCommandType.UNKNOWN.ordinal()] = 11;
            iArr2[ApplicationCommandType.ATTACHMENT.ordinal()] = 12;
            ApplicationCommandType.values();
            int[] iArr3 = new int[12];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[applicationCommandType5.ordinal()] = 1;
            iArr3[applicationCommandType6.ordinal()] = 2;
            iArr3[applicationCommandType7.ordinal()] = 3;
            iArr3[applicationCommandType8.ordinal()] = 4;
            iArr3[applicationCommandType.ordinal()] = 5;
            iArr3[applicationCommandType2.ordinal()] = 6;
            iArr3[applicationCommandType3.ordinal()] = 7;
            iArr3[applicationCommandType4.ordinal()] = 8;
        }
    }

    private AutocompleteCommandUtils() {
    }

    public static /* synthetic */ InputEditTextAction.InsertText appendParam$default(AutocompleteCommandUtils autocompleteCommandUtils, CharSequence charSequence, ApplicationCommandOption applicationCommandOption, Integer num, int i, Object obj) {
        if ((i & 4) != 0) {
            num = null;
        }
        return autocompleteCommandUtils.appendParam(charSequence, applicationCommandOption, num);
    }

    private final Number asSafeNumberOrNull(Number number) {
        if (number == null || !INSTANCE.isSafeNumber(number)) {
            return null;
        }
        return number;
    }

    public static /* synthetic */ InputCommandContext getInputCommandContext$default(AutocompleteCommandUtils autocompleteCommandUtils, CharSequence charSequence, long j, List list, AutocompleteApplicationCommands autocompleteApplicationCommands, ApplicationCommand applicationCommand, int i, Object obj) {
        if ((i & 16) != 0) {
            applicationCommand = null;
        }
        return autocompleteCommandUtils.getInputCommandContext(charSequence, j, list, autocompleteApplicationCommands, applicationCommand);
    }

    private final boolean isSafeNumber(Number number) {
        double doubleValue = number.doubleValue();
        return doubleValue >= ((double) (-9007199254740991L)) && doubleValue <= ((double) 9007199254740991L);
    }

    private final Number parseNumber(String str) {
        if (str == null) {
            return null;
        }
        try {
            ParsePosition parsePosition = new ParsePosition(0);
            Number parse = NumberFormat.getInstance().parse(str, parsePosition);
            if (parsePosition.getIndex() == str.length() && parsePosition.getIndex() != 0) {
                return parse;
            }
            throw new ParseException("Invalid input", parsePosition.getIndex());
        } catch (ParseException unused) {
            return null;
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x0049, code lost:
        if (r6 != 8) goto L17;
     */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0056  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x005b  */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.chat.input.autocomplete.InputEditTextAction.InsertText appendParam(java.lang.CharSequence r5, com.discord.models.commands.ApplicationCommandOption r6, java.lang.Integer r7) {
        /*
            r4 = this;
            java.lang.String r0 = "input"
            d0.z.d.m.checkNotNullParameter(r5, r0)
            java.lang.String r0 = "applicationCommandsOption"
            d0.z.d.m.checkNotNullParameter(r6, r0)
            r5.length()
            r0 = 32
            r1 = 0
            r2 = 2
            r3 = 0
            boolean r0 = d0.g0.w.endsWith$default(r5, r0, r1, r2, r3)
            java.lang.String r1 = ""
            if (r0 != 0) goto L1f
            if (r7 != 0) goto L1f
            java.lang.String r0 = " "
            goto L20
        L1f:
            r0 = r1
        L20:
            java.lang.StringBuilder r0 = b.d.b.a.a.R(r0)
            java.lang.String r2 = r6.getName()
            r0.append(r2)
            r2 = 58
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.discord.api.commands.ApplicationCommandType r6 = r6.getType()
            int r6 = r6.ordinal()
            r2 = 5
            java.lang.String r3 = "@"
            if (r6 == r2) goto L4f
            r2 = 6
            if (r6 == r2) goto L4c
            r2 = 7
            if (r6 == r2) goto L4f
            r2 = 8
            if (r6 == r2) goto L4f
            goto L50
        L4c:
            java.lang.String r1 = "#"
            goto L50
        L4f:
            r1 = r3
        L50:
            java.lang.String r6 = b.d.b.a.a.v(r0, r1)
            if (r7 == 0) goto L5b
            int r7 = r7.intValue()
            goto L5f
        L5b:
            int r7 = r5.length()
        L5f:
            int r0 = r5.length()
            int r1 = r6.length()
            int r1 = r1 + r0
            com.discord.widgets.chat.input.autocomplete.InputEditTextAction$InsertText r0 = new com.discord.widgets.chat.input.autocomplete.InputEditTextAction$InsertText
            kotlin.ranges.IntRange r2 = new kotlin.ranges.IntRange
            r2.<init>(r7, r7)
            r0.<init>(r5, r6, r2, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.autocomplete.commands.AutocompleteCommandUtils.appendParam(java.lang.CharSequence, com.discord.models.commands.ApplicationCommandOption, java.lang.Integer):com.discord.widgets.chat.input.autocomplete.InputEditTextAction$InsertText");
    }

    @MainThread
    public final InputEditTextAction appendTextForCommandForInput(MentionInputModel.VerifiedCommandInputModel verifiedCommandInputModel) {
        m.checkNotNullParameter(verifiedCommandInputModel, "model");
        ApplicationCommand selectedCommand = verifiedCommandInputModel.getInputCommandContext().getSelectedCommand();
        if (selectedCommand != null && selectedCommand.getOptions().size() == 1) {
            ApplicationCommandOption applicationCommandOption = (ApplicationCommandOption) u.first((List<? extends Object>) selectedCommand.getOptions());
            StringBuilder O = a.O(MentionUtilsKt.SLASH_CHAR);
            O.append(selectedCommand.getName());
            O.append(' ');
            String sb = O.toString();
            if (!verifiedCommandInputModel.getInputCommandOptionsRanges().containsKey(applicationCommandOption) && verifiedCommandInputModel.getInput().length() > sb.length()) {
                return appendParam(verifiedCommandInputModel.getInput(), applicationCommandOption, Integer.valueOf(sb.length()));
            }
        }
        return new InputEditTextAction.None(verifiedCommandInputModel.getInput());
    }

    public final Map<ApplicationCommandOption, OptionRange> findOptionRanges(CharSequence charSequence, ApplicationCommand applicationCommand) {
        List<ApplicationCommandOption> options;
        IntRange findValueRange;
        m.checkNotNullParameter(charSequence, "$this$findOptionRanges");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (!(applicationCommand == null || (options = applicationCommand.getOptions()) == null)) {
            for (ApplicationCommandOption applicationCommandOption : options) {
                AutocompleteCommandUtils autocompleteCommandUtils = INSTANCE;
                Integer findStartOfParam = autocompleteCommandUtils.findStartOfParam(charSequence, applicationCommandOption.getName());
                if (!(findStartOfParam == null || (findValueRange = autocompleteCommandUtils.findValueRange(charSequence, applicationCommand, applicationCommandOption.getName())) == null)) {
                    OptionRange optionRange = (OptionRange) linkedHashMap.put(applicationCommandOption, new OptionRange(new IntRange(findStartOfParam.intValue(), findValueRange.getFirst()), findValueRange));
                }
            }
        }
        return linkedHashMap;
    }

    @VisibleForTesting
    public final Integer findStartOfParam(CharSequence charSequence, String str) {
        IntRange range;
        m.checkNotNullParameter(charSequence, "$this$findStartOfParam");
        m.checkNotNullParameter(str, "paramName");
        MatchResult find$default = Regex.find$default(new Regex("[\\s|\\n]" + str + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR), charSequence, 0, 2, null);
        int first = (find$default == null || (range = find$default.getRange()) == null) ? -1 : range.getFirst();
        if (first == -1) {
            return null;
        }
        return Integer.valueOf(first + 1);
    }

    @VisibleForTesting
    public final Integer findStartOfValue(CharSequence charSequence, String str) {
        m.checkNotNullParameter(charSequence, "$this$findStartOfValue");
        m.checkNotNullParameter(str, "paramName");
        Integer findStartOfParam = findStartOfParam(charSequence, str);
        int intValue = findStartOfParam != null ? findStartOfParam.intValue() : -1;
        if (intValue != -1) {
            return Integer.valueOf(str.length() + intValue + 1);
        }
        return null;
    }

    public final IntRange findValueRange(CharSequence charSequence, ApplicationCommand applicationCommand, String str) {
        int i;
        e eVar;
        IntRange range;
        ApplicationCommandOption applicationCommandOption;
        boolean z2;
        List<ApplicationCommandOption> options;
        Object obj;
        m.checkNotNullParameter(charSequence, "$this$findValueRange");
        m.checkNotNullParameter(str, "paramName");
        Integer findStartOfValue = findStartOfValue(charSequence, str);
        if (findStartOfValue == null) {
            return null;
        }
        int intValue = findStartOfValue.intValue();
        Iterator it = Regex.findAll$default(new Regex(" ([\\p{L}0-9-_]*):"), charSequence.subSequence(intValue, charSequence.length()).toString(), 0, 2, null).iterator();
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            }
            Iterator<e> it2 = ((MatchResult) it.next()).getGroups().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    eVar = null;
                    break;
                }
                eVar = it2.next();
                e eVar2 = eVar;
                if (applicationCommand == null || (options = applicationCommand.getOptions()) == null) {
                    applicationCommandOption = null;
                } else {
                    Iterator<T> it3 = options.iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it3.next();
                        if (m.areEqual(eVar2 != null ? eVar2.getValue() : null, ((ApplicationCommandOption) obj).getName())) {
                            break;
                        }
                    }
                    applicationCommandOption = (ApplicationCommandOption) obj;
                }
                if (applicationCommandOption != null) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            e eVar3 = eVar;
            if (eVar3 == null || (range = eVar3.getRange()) == null) {
                i = -1;
                continue;
            } else {
                i = range.getFirst();
                continue;
            }
            if (i != -1) {
                break;
            }
        }
        if (i == -1) {
            return new IntRange(intValue, charSequence.length());
        }
        return new IntRange(intValue, i + intValue);
    }

    public final String getCommandPrefix(CharSequence charSequence) {
        List<String> groupValues;
        m.checkNotNullParameter(charSequence, "$this$getCommandPrefix");
        MatchResult find$default = Regex.find$default(new Regex("^(/([\\p{L}0-9-_]+\\s*){0,3})"), charSequence, 0, 2, null);
        if (find$default == null || (groupValues = find$default.getGroupValues()) == null) {
            return null;
        }
        return (String) u.getOrNull(groupValues, 1);
    }

    public final Set<ApplicationCommandOption> getErrorsToShowForCommandParameters(ApplicationCommand applicationCommand, ApplicationCommandOption applicationCommandOption, ApplicationCommandOption applicationCommandOption2, Map<ApplicationCommandOption, Boolean> map, Map<ApplicationCommandOption, ? extends CommandOptionValue> map2) {
        m.checkNotNullParameter(map, "validMap");
        m.checkNotNullParameter(map2, "parsedCommandOptions");
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        if (applicationCommand == null) {
            return n0.emptySet();
        }
        for (ApplicationCommandOption applicationCommandOption3 : applicationCommand.getOptions()) {
            if (map2.containsKey(applicationCommandOption3)) {
                CommandOptionValue commandOptionValue = map2.get(applicationCommandOption3);
                String valueOf = String.valueOf(commandOptionValue != null ? commandOptionValue.getValue() : null);
                boolean z2 = false;
                boolean z3 = t.isBlank(valueOf) || (valueOf.length() == 1 && MentionUtilsKt.getDEFAULT_LEADING_IDENTIFIERS().contains(Character.valueOf(valueOf.charAt(0))));
                boolean areEqual = m.areEqual(applicationCommandOption, applicationCommandOption3);
                boolean z4 = applicationCommandOption2 != null && m.areEqual(applicationCommandOption2, applicationCommandOption3);
                boolean containsKey = map2.containsKey(applicationCommandOption3);
                boolean z5 = (areEqual && z4) || (areEqual && z3);
                if (m.areEqual(map.get(applicationCommandOption3), Boolean.FALSE) && !z5 && containsKey) {
                    z2 = true;
                }
                if (z2) {
                    linkedHashSet.add(applicationCommandOption3);
                }
            }
        }
        return linkedHashSet;
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x005a, code lost:
        if ((!d0.z.d.m.areEqual(r12, r15)) != false) goto L16;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:106:0x01c8 A[EDGE_INSN: B:106:0x01c8->B:94:0x01c8 ?: BREAK  , SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:86:0x01ad  */
    /* JADX WARN: Type inference failed for: r2v13 */
    /* JADX WARN: Type inference failed for: r6v7 */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.chat.input.models.InputCommandContext getInputCommandContext(java.lang.CharSequence r17, long r18, java.util.List<java.lang.Long> r20, com.discord.widgets.chat.input.models.AutocompleteApplicationCommands r21, com.discord.models.commands.ApplicationCommand r22) {
        /*
            Method dump skipped, instructions count: 466
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.autocomplete.commands.AutocompleteCommandUtils.getInputCommandContext(java.lang.CharSequence, long, java.util.List, com.discord.widgets.chat.input.models.AutocompleteApplicationCommands, com.discord.models.commands.ApplicationCommand):com.discord.widgets.chat.input.models.InputCommandContext");
    }

    /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x008b, code lost:
        if ((r2 instanceof com.discord.widgets.chat.input.models.SnowflakeOptionValue) == false) goto L77;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x0094, code lost:
        if ((r2 instanceof com.discord.widgets.chat.input.models.SnowflakeOptionValue) == false) goto L77;
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00c4, code lost:
        if (r1.getChannelTypes().contains(java.lang.Integer.valueOf(((com.discord.widgets.chat.input.models.ChannelOptionValue) r2).getChannel().A())) == false) goto L46;
     */
    /* JADX WARN: Code restructure failed: missing block: B:47:0x00c8, code lost:
        if ((r2 instanceof com.discord.widgets.chat.input.models.SnowflakeOptionValue) != false) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x00d1, code lost:
        if ((r2 instanceof com.discord.widgets.chat.input.models.SnowflakeOptionValue) == false) goto L77;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:0x00fb, code lost:
        if (r7 <= ((java.lang.Number) r3).doubleValue()) goto L59;
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x011a, code lost:
        if (r7 >= ((java.lang.Number) r2).doubleValue()) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x0141, code lost:
        if (r7 <= ((java.lang.Long) r3).longValue()) goto L69;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x0160, code lost:
        if (r7 >= ((java.lang.Long) r2).longValue()) goto L24;
     */
    /* JADX WARN: Code restructure failed: missing block: B:76:0x0175, code lost:
        if ((!d0.g0.t.isBlank(r2.getValue().toString())) != false) goto L24;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.Map<com.discord.models.commands.ApplicationCommandOption, java.lang.Boolean> getInputValidity(com.discord.models.commands.ApplicationCommand r12, java.util.Map<com.discord.models.commands.ApplicationCommandOption, ? extends com.discord.widgets.chat.input.models.CommandOptionValue> r13) {
        /*
            Method dump skipped, instructions count: 428
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.autocomplete.commands.AutocompleteCommandUtils.getInputValidity(com.discord.models.commands.ApplicationCommand, java.util.Map):java.util.Map");
    }

    public final ApplicationCommand getSelectedCommand(List<? extends ApplicationCommand> list, String str, String str2, long j, List<Long> list2) {
        boolean z2;
        m.checkNotNullParameter(list, "commands");
        m.checkNotNullParameter(str, "prefix");
        m.checkNotNullParameter(str2, "input");
        m.checkNotNullParameter(list2, "roles");
        Object obj = null;
        if (!(str.length() > 0)) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            ApplicationCommand applicationCommand = (ApplicationCommand) next;
            StringBuilder sb = new StringBuilder();
            String name = applicationCommand.getName();
            sb.append(String.valueOf((char) MentionUtilsKt.SLASH_CHAR) + name);
            sb.append(' ');
            if (!t.startsWith$default(str2, sb.toString(), false, 2, null) || !ApplicationCommandKt.hasPermission(applicationCommand, j, list2)) {
                z2 = false;
                continue;
            } else {
                z2 = true;
                continue;
            }
            if (z2) {
                obj = next;
                break;
            }
        }
        return (ApplicationCommand) obj;
    }

    public final ApplicationCommandOption getSelectedCommandOption(int i, Map<ApplicationCommandOption, OptionRange> map) {
        Set<ApplicationCommandOption> keySet;
        if (map == null || (keySet = map.keySet()) == null) {
            return null;
        }
        for (ApplicationCommandOption applicationCommandOption : keySet) {
            OptionRange optionRange = map.get(applicationCommandOption);
            if (optionRange != null && optionRange.getParam().getFirst() < i && optionRange.getValue().getLast() >= i) {
                return applicationCommandOption;
            }
        }
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:22:0x00b7, code lost:
        if (r3 != null) goto L24;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.Map<com.discord.models.commands.ApplicationCommandOption, com.discord.widgets.chat.input.models.CommandOptionValue> parseCommandOptions(java.lang.CharSequence r11, com.discord.models.commands.ApplicationCommand r12, com.discord.widgets.chat.input.models.ChatInputMentionsMap r13) {
        /*
            Method dump skipped, instructions count: 412
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.autocomplete.commands.AutocompleteCommandUtils.parseCommandOptions(java.lang.CharSequence, com.discord.models.commands.ApplicationCommand, com.discord.widgets.chat.input.models.ChatInputMentionsMap):java.util.Map");
    }

    public final String transformParameterSpannableString(String str) {
        String str2 = null;
        if (str == null) {
            return null;
        }
        if (w.endsWith$default((CharSequence) w.trim(str).toString(), (char) MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, false, 2, (Object) null)) {
            return w.trim(str).toString();
        }
        if (!w.contains$default((CharSequence) str, (char) MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR, false, 2, (Object) null)) {
            return str;
        }
        List split$default = w.split$default((CharSequence) str, new char[]{MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR}, false, 0, 6, (Object) null);
        String str3 = (String) u.firstOrNull((List<? extends Object>) split$default);
        if (str3 != null) {
            str2 = w.trim(str3).toString();
        }
        String joinToString$default = u.joinToString$default(split$default.subList(1, split$default.size()), ":", null, null, 0, null, null, 62, null);
        Objects.requireNonNull(joinToString$default, "null cannot be cast to non-null type kotlin.CharSequence");
        return a.w(str2, ": ", w.trim(joinToString$default).toString());
    }

    private final Integer asSafeNumberOrNull(Integer num) {
        if (num == null) {
            return null;
        }
        int intValue = num.intValue();
        if (INSTANCE.isSafeNumber(Integer.valueOf(intValue))) {
            return Integer.valueOf(intValue);
        }
        return null;
    }

    private final Long asSafeNumberOrNull(Long l) {
        if (l == null) {
            return null;
        }
        long longValue = l.longValue();
        if (INSTANCE.isSafeNumber(Long.valueOf(longValue))) {
            return Long.valueOf(longValue);
        }
        return null;
    }
}
