package com.discord.widgets.chat.input.autocomplete.adapter;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppComponent;
import com.discord.databinding.WidgetChatInputAutocompleteItemBinding;
import com.discord.databinding.WidgetChatInputCommandApplicationHeaderItemBinding;
import com.discord.databinding.WidgetChatInputEmojiAutocompleteUpsellItemBinding;
import com.discord.databinding.WidgetChatInputSlashLoadingItemBinding;
import com.discord.models.commands.Application;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.utilities.views.StickyHeaderItemDecoration;
import com.discord.views.PileView;
import com.discord.views.StatusView;
import com.discord.widgets.chat.input.autocomplete.ApplicationCommandAutocompletable;
import com.discord.widgets.chat.input.autocomplete.ApplicationCommandChoiceAutocompletable;
import com.discord.widgets.chat.input.autocomplete.ApplicationCommandLoadingPlaceholder;
import com.discord.widgets.chat.input.autocomplete.ApplicationPlaceholder;
import com.discord.widgets.chat.input.autocomplete.Autocompletable;
import com.discord.widgets.chat.input.autocomplete.ChannelAutocompletable;
import com.discord.widgets.chat.input.autocomplete.EmojiAutocompletable;
import com.discord.widgets.chat.input.autocomplete.EmojiUpsellPlaceholder;
import com.discord.widgets.chat.input.autocomplete.GlobalRoleAutocompletable;
import com.discord.widgets.chat.input.autocomplete.RoleAutocompletable;
import com.discord.widgets.chat.input.autocomplete.UserAutocompletable;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.n;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Emitter;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: ChatInputAutocompleteAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 a2\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001aB\u0007¢\u0006\u0004\b_\u0010`J#\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\r\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0011\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J-\u0010\u0016\u001a\u00020\f2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J%\u0010\u0019\u001a\u00020\u00072\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0018\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ/\u0010\u001c\u001a\u00020\f2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u00132\b\b\u0002\u0010\u0015\u001a\u00020\u0013¢\u0006\u0004\b\u001c\u0010\u0017J)\u0010 \u001a\u00020\f2\u0006\u0010\u001d\u001a\u00020\u00072\b\b\u0002\u0010\u001e\u001a\u00020\u00072\b\b\u0002\u0010\u001f\u001a\u00020\u0013¢\u0006\u0004\b \u0010!J\u0017\u0010\"\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\"\u0010\u000eJ\u0017\u0010#\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b#\u0010\u000eJ\u0015\u0010$\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b$\u0010%J\u001f\u0010)\u001a\u00020\u00022\u0006\u0010'\u001a\u00020&2\u0006\u0010(\u001a\u00020\u0007H\u0016¢\u0006\u0004\b)\u0010*J\u0015\u0010,\u001a\u00020\f2\u0006\u0010+\u001a\u00020\u0005¢\u0006\u0004\b,\u0010-J\u0017\u0010/\u001a\u00020.2\u0006\u0010\u001d\u001a\u00020\u0007H\u0016¢\u0006\u0004\b/\u00100J\u001f\u00102\u001a\u00020\f2\u0006\u00101\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u0007H\u0016¢\u0006\u0004\b2\u00103J\u0015\u00104\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u0007¢\u0006\u0004\b4\u00105J\u000f\u00106\u001a\u00020\u0007H\u0016¢\u0006\u0004\b6\u00107J\u0017\u00108\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u0007H\u0016¢\u0006\u0004\b8\u00109J\u0019\u0010;\u001a\u0004\u0018\u00010\u00072\u0006\u0010:\u001a\u00020\u0007H\u0016¢\u0006\u0004\b;\u0010<J\u0017\u0010=\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u0007H\u0016¢\u0006\u0004\b=\u0010>J\u0019\u0010@\u001a\u0004\u0018\u00010?2\u0006\u0010\u001d\u001a\u00020\u0007H\u0016¢\u0006\u0004\b@\u0010AJ'\u0010D\u001a\u00020\f2\u0018\u0010C\u001a\u0014\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\f0B¢\u0006\u0004\bD\u0010ER\u0018\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000b\u0010FR\u0018\u0010H\u001a\u0004\u0018\u00010G8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bH\u0010IR$\u0010K\u001a\u0004\u0018\u00010J8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bK\u0010L\u001a\u0004\bM\u0010N\"\u0004\bO\u0010PR*\u0010Q\u001a\u0016\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\f\u0018\u00010B8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bQ\u0010RR\u001c\u0010S\u001a\b\u0012\u0004\u0012\u00020\u00070\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bS\u0010TR\u0018\u0010V\u001a\u0004\u0018\u00010U8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bV\u0010WR0\u0010Y\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\f\u0018\u00010X8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bY\u0010Z\u001a\u0004\b[\u0010\\\"\u0004\b]\u0010^R\u001c\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0006\u0010T¨\u0006b"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/adapter/ChatInputAutocompleteAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/utilities/views/StickyHeaderItemDecoration$StickyHeaderAdapter;", "", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "data", "", "calculateCommandHeaderPositions", "(Ljava/util/List;)Ljava/util/List;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "", "setupStickyApplicationHeaders", "(Landroidx/recyclerview/widget/RecyclerView;)V", "Lcom/discord/app/AppComponent;", "appComponent", "setupScrollObservables", "(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppComponent;)V", "", "hasStickers", "sizeToMax", "resizeRecyclerToData", "(Ljava/util/List;ZZ)V", "maxAllowedHeight", "getDataHeightOrMax", "(Ljava/util/List;I)I", "autocompletables", "setData", ModelAuditLogEntry.CHANGE_KEY_POSITION, "offset", "enableListeners", "scrollToPosition", "(IIZ)V", "onAttachedToRecyclerView", "onDetachedFromRecyclerView", "configureSubscriptions", "(Lcom/discord/app/AppComponent;)V", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "autocompletable", "onAutocompletableSelected", "(Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;)V", "", "getItemId", "(I)J", "holder", "onBindViewHolder", "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V", "getItem", "(I)Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "getItemCount", "()I", "getItemViewType", "(I)I", "itemPosition", "getHeaderPositionForItem", "(I)Ljava/lang/Integer;", "isHeader", "(I)Z", "Landroid/view/View;", "getAndBindHeaderView", "(I)Landroid/view/View;", "Lkotlin/Function2;", "onScrollVisibleDiscoveryCommands", "setVisiblePositionListener", "(Lkotlin/jvm/functions/Function2;)V", "Landroidx/recyclerview/widget/RecyclerView;", "Lcom/discord/widgets/chat/input/autocomplete/adapter/StickyHeaderManager;", "stickyHeaderManager", "Lcom/discord/widgets/chat/input/autocomplete/adapter/StickyHeaderManager;", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "setSubscription", "(Lrx/Subscription;)V", "onScrollPositionListener", "Lkotlin/jvm/functions/Function2;", "headerPositions", "Ljava/util/List;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "onScrollListener", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "Lkotlin/Function1;", "onItemSelected", "Lkotlin/jvm/functions/Function1;", "getOnItemSelected", "()Lkotlin/jvm/functions/Function1;", "setOnItemSelected", "(Lkotlin/jvm/functions/Function1;)V", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatInputAutocompleteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyHeaderItemDecoration.StickyHeaderAdapter {
    private static final int COMMAND_HEADER_HEIGHT = 32;
    public static final int CONTAINER_ROW_MAX_VISIBLE_COUNT = 4;
    public static final Companion Companion = new Companion(null);
    private static final int EMOJI_AUTOCOMPLETE_UPSELL_HEIGHT = 52;
    private static final int MENTION_CONTAINER_MAX_SIZE = 176;
    private static final int MENTION_CONTAINER_MAX_SIZE_WITH_STICKERS = 132;
    private static final int MENTION_ROW_HEIGHT_DEFAULT = 44;
    public static final int VIEW_TYPE_AUTOCOMPLETE_ITEM = 0;
    public static final int VIEW_TYPE_COMMAND_HEADER_ITEM = 1;
    public static final int VIEW_TYPE_COMMAND_LOADING_ITEM = 2;
    public static final int VIEW_TYPE_EMOJI_UPSELL_AUTOCOMPLETE = 3;
    private List<? extends Autocompletable> data = n.emptyList();
    private List<Integer> headerPositions = n.emptyList();
    private Function1<? super Autocompletable, Unit> onItemSelected;
    private RecyclerView.OnScrollListener onScrollListener;
    private Function2<? super Integer, ? super Integer, Unit> onScrollPositionListener;
    private RecyclerView recyclerView;
    private StickyHeaderManager stickyHeaderManager;
    private Subscription subscription;

    /* compiled from: ChatInputAutocompleteAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\u0004R\u0016\u0010\f\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\u0004R\u0016\u0010\r\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\u0004¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/adapter/ChatInputAutocompleteAdapter$Companion;", "", "", "COMMAND_HEADER_HEIGHT", "I", "CONTAINER_ROW_MAX_VISIBLE_COUNT", "EMOJI_AUTOCOMPLETE_UPSELL_HEIGHT", "MENTION_CONTAINER_MAX_SIZE", "MENTION_CONTAINER_MAX_SIZE_WITH_STICKERS", "MENTION_ROW_HEIGHT_DEFAULT", "VIEW_TYPE_AUTOCOMPLETE_ITEM", "VIEW_TYPE_COMMAND_HEADER_ITEM", "VIEW_TYPE_COMMAND_LOADING_ITEM", "VIEW_TYPE_EMOJI_UPSELL_AUTOCOMPLETE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public ChatInputAutocompleteAdapter() {
        setHasStableIds(true);
    }

    private final List<Integer> calculateCommandHeaderPositions(List<? extends Autocompletable> list) {
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (list.get(i) instanceof ApplicationPlaceholder) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        return arrayList;
    }

    private final int getDataHeightOrMax(List<? extends Autocompletable> list, int i) {
        int i2;
        int i3 = 0;
        for (Autocompletable autocompletable : list) {
            if (autocompletable instanceof ApplicationPlaceholder) {
                i2 = 32;
            } else {
                i2 = autocompletable instanceof EmojiUpsellPlaceholder ? 52 : 44;
            }
            i3 += i2;
            if (i3 >= i) {
                return i;
            }
        }
        return i3;
    }

    private final void resizeRecyclerToData(List<? extends Autocompletable> list, boolean z2, boolean z3) {
        ViewGroup.LayoutParams layoutParams;
        int i = z2 ? 132 : 176;
        if (!z3) {
            i = getDataHeightOrMax(list, i);
        }
        int dpToPixels = DimenUtils.dpToPixels(i);
        RecyclerView recyclerView = this.recyclerView;
        if (!(recyclerView == null || (layoutParams = recyclerView.getLayoutParams()) == null)) {
            layoutParams.height = dpToPixels;
        }
        RecyclerView recyclerView2 = this.recyclerView;
        if (recyclerView2 != null) {
            recyclerView2.requestLayout();
        }
    }

    public static /* synthetic */ void scrollToPosition$default(ChatInputAutocompleteAdapter chatInputAutocompleteAdapter, int i, int i2, boolean z2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i2 = 0;
        }
        if ((i3 & 4) != 0) {
            z2 = false;
        }
        chatInputAutocompleteAdapter.scrollToPosition(i, i2, z2);
    }

    public static /* synthetic */ void setData$default(ChatInputAutocompleteAdapter chatInputAutocompleteAdapter, List list, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        if ((i & 4) != 0) {
            z3 = false;
        }
        chatInputAutocompleteAdapter.setData(list, z2, z3);
    }

    private final void setupScrollObservables(RecyclerView recyclerView, AppComponent appComponent) {
        Observable n = Observable.n(new Action1<Emitter<Object>>() { // from class: com.discord.widgets.chat.input.autocomplete.adapter.ChatInputAutocompleteAdapter$setupScrollObservables$1
            public final void call(final Emitter<Object> emitter) {
                m.checkNotNullParameter(emitter, "emitter");
                ChatInputAutocompleteAdapter.this.onScrollListener = new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.chat.input.autocomplete.adapter.ChatInputAutocompleteAdapter$setupScrollObservables$1.1
                    @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
                    public void onScrolled(RecyclerView recyclerView2, int i, int i2) {
                        m.checkNotNullParameter(recyclerView2, "recyclerView");
                        super.onScrolled(recyclerView2, i, i2);
                        Emitter.this.onNext(0);
                    }
                };
            }
        }, Emitter.BackpressureMode.LATEST);
        m.checkNotNullExpressionValue(n, "Observable\n        .crea….BackpressureMode.LATEST)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(n, appComponent, null, 2, null), ChatInputAutocompleteAdapter.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ChatInputAutocompleteAdapter$setupScrollObservables$2(this, recyclerView));
        RecyclerView.OnScrollListener onScrollListener = this.onScrollListener;
        if (onScrollListener != null) {
            recyclerView.addOnScrollListener(onScrollListener);
        }
    }

    private final void setupStickyApplicationHeaders(final RecyclerView recyclerView) {
        this.stickyHeaderManager = new StickyHeaderManager(recyclerView, this);
        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: com.discord.widgets.chat.input.autocomplete.adapter.ChatInputAutocompleteAdapter$setupStickyApplicationHeaders$1
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                StickyHeaderManager stickyHeaderManager;
                stickyHeaderManager = ChatInputAutocompleteAdapter.this.stickyHeaderManager;
                if (stickyHeaderManager != null) {
                    stickyHeaderManager.layoutViews(recyclerView);
                }
            }
        });
        StickyHeaderItemDecoration stickyHeaderItemDecoration = new StickyHeaderItemDecoration(this);
        recyclerView.addItemDecoration(stickyHeaderItemDecoration);
        stickyHeaderItemDecoration.blockClicks(recyclerView);
    }

    public final void configureSubscriptions(AppComponent appComponent) {
        m.checkNotNullParameter(appComponent, "appComponent");
        RecyclerView recyclerView = this.recyclerView;
        if (recyclerView != null) {
            setupScrollObservables(recyclerView, appComponent);
        }
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public View getAndBindHeaderView(int i) {
        StickyHeaderHolder stickyHeaderHolder;
        StickyHeaderHolder stickyHeaderHolder2;
        Autocompletable autocompletable = this.data.get(i);
        if (!(autocompletable instanceof ApplicationPlaceholder)) {
            return null;
        }
        StickyHeaderManager stickyHeaderManager = this.stickyHeaderManager;
        if (!(stickyHeaderManager == null || (stickyHeaderHolder2 = stickyHeaderManager.getStickyHeaderHolder()) == null)) {
            stickyHeaderHolder2.bind((ApplicationPlaceholder) autocompletable);
        }
        StickyHeaderManager stickyHeaderManager2 = this.stickyHeaderManager;
        if (stickyHeaderManager2 == null || (stickyHeaderHolder = stickyHeaderManager2.getStickyHeaderHolder()) == null) {
            return null;
        }
        return stickyHeaderHolder.getItemView();
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public Integer getHeaderPositionForItem(int i) {
        int intValue;
        int size = this.headerPositions.size();
        do {
            size--;
            if (size < 0) {
                return null;
            }
            intValue = this.headerPositions.get(size).intValue();
        } while (intValue > i);
        return Integer.valueOf(intValue);
    }

    public final Autocompletable getItem(int i) {
        return this.data.get(i);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        Autocompletable item = getItem(i);
        if (item instanceof ApplicationCommandAutocompletable) {
            i = ((ApplicationCommandAutocompletable) item).getCommand().hashCode();
        } else if (item instanceof ApplicationCommandChoiceAutocompletable) {
            i = ((ApplicationCommandChoiceAutocompletable) item).getChoice().b().hashCode();
        } else if (item instanceof ApplicationCommandLoadingPlaceholder) {
            Application application = ((ApplicationCommandLoadingPlaceholder) item).getApplication();
            if (application != null) {
                return application.getId();
            }
        } else if (item instanceof ApplicationPlaceholder) {
            return ((ApplicationPlaceholder) item).getApplication().getId();
        } else {
            if (item instanceof ChannelAutocompletable) {
                return ((ChannelAutocompletable) item).getChannel().h();
            }
            if (item instanceof EmojiAutocompletable) {
                i = ((EmojiAutocompletable) item).getEmoji().getUniqueId().hashCode();
            } else if (item instanceof EmojiUpsellPlaceholder) {
                i = ((EmojiUpsellPlaceholder) item).getLockedFirstThree().hashCode();
            } else if (item instanceof GlobalRoleAutocompletable) {
                i = ((GlobalRoleAutocompletable) item).getText().hashCode();
            } else if (item instanceof RoleAutocompletable) {
                return ((RoleAutocompletable) item).getRole().getId();
            } else {
                if (item instanceof UserAutocompletable) {
                    return ((UserAutocompletable) item).getUser().getId();
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        return i;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        Autocompletable autocompletable = this.data.get(i);
        if (autocompletable instanceof ApplicationCommandLoadingPlaceholder) {
            return 2;
        }
        if (autocompletable instanceof ApplicationPlaceholder) {
            return 1;
        }
        return autocompletable instanceof EmojiUpsellPlaceholder ? 3 : 0;
    }

    public final Function1<Autocompletable, Unit> getOnItemSelected() {
        return this.onItemSelected;
    }

    public final Subscription getSubscription() {
        return this.subscription;
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public boolean isHeader(int i) {
        return this.headerPositions.contains(Integer.valueOf(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        setupStickyApplicationHeaders(recyclerView);
    }

    public final void onAutocompletableSelected(Autocompletable autocompletable) {
        m.checkNotNullParameter(autocompletable, "autocompletable");
        Function1<? super Autocompletable, Unit> function1 = this.onItemSelected;
        if (function1 != null) {
            function1.invoke(autocompletable);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        m.checkNotNullParameter(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == 0) {
            ((AutocompleteItemViewHolder) viewHolder).bind(this.data.get(i));
        } else if (itemViewType == 1) {
            Autocompletable autocompletable = this.data.get(i);
            Objects.requireNonNull(autocompletable, "null cannot be cast to non-null type com.discord.widgets.chat.input.autocomplete.ApplicationPlaceholder");
            ((CommandHeaderViewHolder) viewHolder).bind((ApplicationPlaceholder) autocompletable);
        } else if (itemViewType == 2) {
        } else {
            if (itemViewType == 3) {
                Autocompletable autocompletable2 = this.data.get(i);
                Objects.requireNonNull(autocompletable2, "null cannot be cast to non-null type com.discord.widgets.chat.input.autocomplete.EmojiUpsellPlaceholder");
                ((EmojiAutocompleteUpsellViewHolder) viewHolder).bind((EmojiUpsellPlaceholder) autocompletable2);
                return;
            }
            throw new IllegalStateException("Cannot bind. Unknown View Type");
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            View inflate = from.inflate(R.layout.widget_chat_input_autocomplete_item, viewGroup, false);
            int i2 = R.id.chat_input_icon_barrier;
            Barrier barrier = (Barrier) inflate.findViewById(R.id.chat_input_icon_barrier);
            if (barrier != null) {
                i2 = R.id.chat_input_item_avatar;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.chat_input_item_avatar);
                if (simpleDraweeView != null) {
                    i2 = R.id.chat_input_item_description;
                    TextView textView = (TextView) inflate.findViewById(R.id.chat_input_item_description);
                    if (textView != null) {
                        i2 = R.id.chat_input_item_divider;
                        View findViewById = inflate.findViewById(R.id.chat_input_item_divider);
                        if (findViewById != null) {
                            i2 = R.id.chat_input_item_emoji;
                            SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) inflate.findViewById(R.id.chat_input_item_emoji);
                            if (simpleDraweeSpanTextView != null) {
                                i2 = R.id.chat_input_item_name;
                                TextView textView2 = (TextView) inflate.findViewById(R.id.chat_input_item_name);
                                if (textView2 != null) {
                                    i2 = R.id.chat_input_item_name_right;
                                    TextView textView3 = (TextView) inflate.findViewById(R.id.chat_input_item_name_right);
                                    if (textView3 != null) {
                                        i2 = R.id.chat_input_item_status;
                                        StatusView statusView = (StatusView) inflate.findViewById(R.id.chat_input_item_status);
                                        if (statusView != null) {
                                            WidgetChatInputAutocompleteItemBinding widgetChatInputAutocompleteItemBinding = new WidgetChatInputAutocompleteItemBinding((ConstraintLayout) inflate, barrier, simpleDraweeView, textView, findViewById, simpleDraweeSpanTextView, textView2, textView3, statusView);
                                            m.checkNotNullExpressionValue(widgetChatInputAutocompleteItemBinding, "WidgetChatInputAutocompl…tInflater, parent, false)");
                                            return new AutocompleteItemViewHolder(widgetChatInputAutocompleteItemBinding, new ChatInputAutocompleteAdapter$onCreateViewHolder$1(this));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
        } else if (i == 1) {
            WidgetChatInputCommandApplicationHeaderItemBinding a = WidgetChatInputCommandApplicationHeaderItemBinding.a(from, viewGroup, false);
            m.checkNotNullExpressionValue(a, "WidgetChatInputCommandAp…tInflater, parent, false)");
            return new CommandHeaderViewHolder(a);
        } else if (i == 2) {
            View inflate2 = from.inflate(R.layout.widget_chat_input_slash_loading_item, viewGroup, false);
            int i3 = R.id.application_name_placeholder;
            View findViewById2 = inflate2.findViewById(R.id.application_name_placeholder);
            if (findViewById2 != null) {
                i3 = R.id.application_name_start_guideline;
                Guideline guideline = (Guideline) inflate2.findViewById(R.id.application_name_start_guideline);
                if (guideline != null) {
                    i3 = R.id.description_placeholder;
                    View findViewById3 = inflate2.findViewById(R.id.description_placeholder);
                    if (findViewById3 != null) {
                        i3 = R.id.description_placeholder_end_guideline;
                        Guideline guideline2 = (Guideline) inflate2.findViewById(R.id.description_placeholder_end_guideline);
                        if (guideline2 != null) {
                            i3 = R.id.name_placeholder;
                            View findViewById4 = inflate2.findViewById(R.id.name_placeholder);
                            if (findViewById4 != null) {
                                i3 = R.id.name_placeholder_end_guideline;
                                Guideline guideline3 = (Guideline) inflate2.findViewById(R.id.name_placeholder_end_guideline);
                                if (guideline3 != null) {
                                    WidgetChatInputSlashLoadingItemBinding widgetChatInputSlashLoadingItemBinding = new WidgetChatInputSlashLoadingItemBinding((ConstraintLayout) inflate2, findViewById2, guideline, findViewById3, guideline2, findViewById4, guideline3);
                                    m.checkNotNullExpressionValue(widgetChatInputSlashLoadingItemBinding, "WidgetChatInputSlashLoad…tInflater, parent, false)");
                                    return new ApplicationCommandLoadingViewHolder(widgetChatInputSlashLoadingItemBinding);
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(i3)));
        } else if (i == 3) {
            View inflate3 = from.inflate(R.layout.widget_chat_input_emoji_autocomplete_upsell_item, viewGroup, false);
            int i4 = R.id.chat_input_emoji_upsell_pile;
            PileView pileView = (PileView) inflate3.findViewById(R.id.chat_input_emoji_upsell_pile);
            if (pileView != null) {
                i4 = R.id.chat_input_emoji_upsell_text;
                TextView textView4 = (TextView) inflate3.findViewById(R.id.chat_input_emoji_upsell_text);
                if (textView4 != null) {
                    WidgetChatInputEmojiAutocompleteUpsellItemBinding widgetChatInputEmojiAutocompleteUpsellItemBinding = new WidgetChatInputEmojiAutocompleteUpsellItemBinding((LinearLayout) inflate3, pileView, textView4);
                    m.checkNotNullExpressionValue(widgetChatInputEmojiAutocompleteUpsellItemBinding, "WidgetChatInputEmojiAuto…tInflater, parent, false)");
                    return new EmojiAutocompleteUpsellViewHolder(widgetChatInputEmojiAutocompleteUpsellItemBinding, new ChatInputAutocompleteAdapter$onCreateViewHolder$2(this));
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate3.getResources().getResourceName(i4)));
        } else {
            throw new IllegalStateException(a.p("Cannot create view holder. Unknown View Type: ", i));
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        super.onDetachedFromRecyclerView(recyclerView);
        this.recyclerView = null;
    }

    public final void scrollToPosition(int i, int i2, boolean z2) {
        RecyclerView.OnScrollListener onScrollListener;
        RecyclerView recyclerView;
        RecyclerView.OnScrollListener onScrollListener2;
        RecyclerView recyclerView2;
        if (!(z2 || (onScrollListener2 = this.onScrollListener) == null || (recyclerView2 = this.recyclerView) == null)) {
            recyclerView2.removeOnScrollListener(onScrollListener2);
        }
        RecyclerView recyclerView3 = this.recyclerView;
        RecyclerView.LayoutManager layoutManager = recyclerView3 != null ? recyclerView3.getLayoutManager() : null;
        if (layoutManager instanceof LinearLayoutManager) {
            ((LinearLayoutManager) layoutManager).scrollToPositionWithOffset(i, i2);
        }
        if (!z2 && (onScrollListener = this.onScrollListener) != null && (recyclerView = this.recyclerView) != null) {
            recyclerView.addOnScrollListener(onScrollListener);
        }
    }

    public final void setData(List<? extends Autocompletable> list, boolean z2, boolean z3) {
        m.checkNotNullParameter(list, "autocompletables");
        this.headerPositions = calculateCommandHeaderPositions(list);
        resizeRecyclerToData(list, z2, z3);
        this.data = list;
        notifyDataSetChanged();
    }

    public final void setOnItemSelected(Function1<? super Autocompletable, Unit> function1) {
        this.onItemSelected = function1;
    }

    public final void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public final void setVisiblePositionListener(Function2<? super Integer, ? super Integer, Unit> function2) {
        m.checkNotNullParameter(function2, "onScrollVisibleDiscoveryCommands");
        this.onScrollPositionListener = function2;
    }
}
