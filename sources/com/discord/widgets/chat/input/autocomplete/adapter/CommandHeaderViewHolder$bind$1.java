package com.discord.widgets.chat.input.autocomplete.adapter;

import android.graphics.Bitmap;
import com.discord.databinding.WidgetChatInputCommandApplicationHeaderItemBinding;
import com.discord.models.commands.Application;
import com.discord.utilities.images.MGImagesBitmap;
import com.discord.widgets.chat.input.autocomplete.ApplicationPlaceholder;
import d0.g0.t;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: CommandHeaderViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;", "closeableBitmaps", "", "invoke", "(Lcom/discord/utilities/images/MGImagesBitmap$CloseableBitmaps;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CommandHeaderViewHolder$bind$1 extends o implements Function1<MGImagesBitmap.CloseableBitmaps, Unit> {
    public final /* synthetic */ String $iconUrl;
    public final /* synthetic */ ApplicationPlaceholder $item;
    public final /* synthetic */ CommandHeaderViewHolder this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CommandHeaderViewHolder$bind$1(CommandHeaderViewHolder commandHeaderViewHolder, ApplicationPlaceholder applicationPlaceholder, String str) {
        super(1);
        this.this$0 = commandHeaderViewHolder;
        this.$item = applicationPlaceholder;
        this.$iconUrl = str;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MGImagesBitmap.CloseableBitmaps closeableBitmaps) {
        invoke2(closeableBitmaps);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MGImagesBitmap.CloseableBitmaps closeableBitmaps) {
        WidgetChatInputCommandApplicationHeaderItemBinding widgetChatInputCommandApplicationHeaderItemBinding;
        Application application;
        m.checkNotNullParameter(closeableBitmaps, "closeableBitmaps");
        ApplicationPlaceholder currentItem = this.this$0.getCurrentItem();
        if (t.equals$default((currentItem == null || (application = currentItem.getApplication()) == null) ? null : application.getIcon(), this.$item.getApplication().getIcon(), false, 2, null)) {
            widgetChatInputCommandApplicationHeaderItemBinding = this.this$0.binding;
            widgetChatInputCommandApplicationHeaderItemBinding.f2288b.setImageBitmap((Bitmap) closeableBitmaps.get((Object) this.$iconUrl));
        }
    }
}
