package com.discord.widgets.chat.input.autocomplete.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.discord.databinding.WidgetChatInputCommandApplicationHeaderItemBinding;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.error.Error;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: CommandHeaderViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "<anonymous parameter 0>", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CommandHeaderViewHolder$bind$2 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ int $tint;
    public final /* synthetic */ CommandHeaderViewHolder this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CommandHeaderViewHolder$bind$2(CommandHeaderViewHolder commandHeaderViewHolder, int i) {
        super(1);
        this.this$0 = commandHeaderViewHolder;
        this.$tint = i;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        WidgetChatInputCommandApplicationHeaderItemBinding widgetChatInputCommandApplicationHeaderItemBinding;
        WidgetChatInputCommandApplicationHeaderItemBinding widgetChatInputCommandApplicationHeaderItemBinding2;
        m.checkNotNullParameter(error, "<anonymous parameter 0>");
        widgetChatInputCommandApplicationHeaderItemBinding = this.this$0.binding;
        ImageView imageView = widgetChatInputCommandApplicationHeaderItemBinding.f2288b;
        m.checkNotNullExpressionValue(imageView, "binding.chatInputApplicationAvatar");
        Context context = imageView.getContext();
        m.checkNotNullExpressionValue(context, "binding.chatInputApplicationAvatar.context");
        Drawable drawable$default = DrawableCompat.getDrawable$default(context, R.drawable.ic_slash_command_24dp, this.$tint, false, 4, null);
        if (drawable$default != null) {
            widgetChatInputCommandApplicationHeaderItemBinding2 = this.this$0.binding;
            widgetChatInputCommandApplicationHeaderItemBinding2.f2288b.setImageDrawable(drawable$default);
        }
    }
}
