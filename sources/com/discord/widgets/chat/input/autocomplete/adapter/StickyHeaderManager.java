package com.discord.widgets.chat.input.autocomplete.adapter;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetChatInputCommandApplicationHeaderItemBinding;
import com.discord.utilities.views.StickyHeaderItemDecoration;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StickyHeaderManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\f\u001a\u0004\b\r\u0010\u000eR\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/adapter/StickyHeaderManager;", "", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "", "layoutViews", "(Landroidx/recyclerview/widget/RecyclerView;)V", "Lcom/discord/widgets/chat/input/autocomplete/adapter/ChatInputAutocompleteAdapter;", "adapter", "Lcom/discord/widgets/chat/input/autocomplete/adapter/ChatInputAutocompleteAdapter;", "getAdapter", "()Lcom/discord/widgets/chat/input/autocomplete/adapter/ChatInputAutocompleteAdapter;", "Landroidx/recyclerview/widget/RecyclerView;", "getRecyclerView", "()Landroidx/recyclerview/widget/RecyclerView;", "Lcom/discord/widgets/chat/input/autocomplete/adapter/StickyHeaderHolder;", "stickyHeaderHolder", "Lcom/discord/widgets/chat/input/autocomplete/adapter/StickyHeaderHolder;", "getStickyHeaderHolder", "()Lcom/discord/widgets/chat/input/autocomplete/adapter/StickyHeaderHolder;", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/widgets/chat/input/autocomplete/adapter/ChatInputAutocompleteAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickyHeaderManager {
    private final ChatInputAutocompleteAdapter adapter;
    private final RecyclerView recyclerView;
    private final StickyHeaderHolder stickyHeaderHolder;

    public StickyHeaderManager(RecyclerView recyclerView, ChatInputAutocompleteAdapter chatInputAutocompleteAdapter) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        m.checkNotNullParameter(chatInputAutocompleteAdapter, "adapter");
        this.recyclerView = recyclerView;
        this.adapter = chatInputAutocompleteAdapter;
        WidgetChatInputCommandApplicationHeaderItemBinding a = WidgetChatInputCommandApplicationHeaderItemBinding.a(LayoutInflater.from(recyclerView.getContext()), recyclerView, false);
        m.checkNotNullExpressionValue(a, "WidgetChatInputCommandAp…clerView, false\n        )");
        this.stickyHeaderHolder = new CommandHeaderViewHolder(a);
    }

    public final ChatInputAutocompleteAdapter getAdapter() {
        return this.adapter;
    }

    public final RecyclerView getRecyclerView() {
        return this.recyclerView;
    }

    public final StickyHeaderHolder getStickyHeaderHolder() {
        return this.stickyHeaderHolder;
    }

    public final void layoutViews(RecyclerView recyclerView) {
        m.checkNotNullParameter(recyclerView, "recyclerView");
        StickyHeaderItemDecoration.LayoutManager.layoutHeaderView(recyclerView, this.stickyHeaderHolder.getItemView());
    }
}
