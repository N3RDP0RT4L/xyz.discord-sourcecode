package com.discord.widgets.chat.input.autocomplete.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.api.user.User;
import com.discord.databinding.WidgetChatInputCommandApplicationHeaderItemBinding;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImagesBitmap;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.autocomplete.ApplicationPlaceholder;
import d0.z.d.m;
import java.util.HashSet;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: CommandHeaderViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0012\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0006\u0010\u0007R\u001c\u0010\t\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR$\u0010\r\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0007R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/chat/input/autocomplete/adapter/CommandHeaderViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/chat/input/autocomplete/adapter/StickyHeaderHolder;", "Lcom/discord/widgets/chat/input/autocomplete/ApplicationPlaceholder;", "item", "", "bind", "(Lcom/discord/widgets/chat/input/autocomplete/ApplicationPlaceholder;)V", "Landroid/view/View;", "itemView", "Landroid/view/View;", "getItemView", "()Landroid/view/View;", "currentItem", "Lcom/discord/widgets/chat/input/autocomplete/ApplicationPlaceholder;", "getCurrentItem", "()Lcom/discord/widgets/chat/input/autocomplete/ApplicationPlaceholder;", "setCurrentItem", "Lcom/discord/databinding/WidgetChatInputCommandApplicationHeaderItemBinding;", "binding", "Lcom/discord/databinding/WidgetChatInputCommandApplicationHeaderItemBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetChatInputCommandApplicationHeaderItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CommandHeaderViewHolder extends RecyclerView.ViewHolder implements StickyHeaderHolder {
    private final WidgetChatInputCommandApplicationHeaderItemBinding binding;
    private ApplicationPlaceholder currentItem;
    private final View itemView;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CommandHeaderViewHolder(WidgetChatInputCommandApplicationHeaderItemBinding widgetChatInputCommandApplicationHeaderItemBinding) {
        super(widgetChatInputCommandApplicationHeaderItemBinding.a);
        m.checkNotNullParameter(widgetChatInputCommandApplicationHeaderItemBinding, "binding");
        this.binding = widgetChatInputCommandApplicationHeaderItemBinding;
        ConstraintLayout constraintLayout = widgetChatInputCommandApplicationHeaderItemBinding.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.root");
        this.itemView = constraintLayout;
    }

    @Override // com.discord.widgets.chat.input.autocomplete.adapter.StickyHeaderHolder
    public void bind(ApplicationPlaceholder applicationPlaceholder) {
        String str;
        m.checkNotNullParameter(applicationPlaceholder, "item");
        if (!m.areEqual(this.currentItem, applicationPlaceholder)) {
            this.currentItem = applicationPlaceholder;
            User bot = applicationPlaceholder.getApplication().getBot();
            if (bot == null || (str = bot.r()) == null) {
                str = applicationPlaceholder.getApplication().getName();
            }
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.chatInputApplicationName");
            textView.setText(str);
            ImageView imageView = this.binding.f2288b;
            m.checkNotNullExpressionValue(imageView, "binding.chatInputApplicationAvatar");
            int themedColor = ColorCompat.getThemedColor(imageView, (int) R.attr.colorTextMuted);
            if (applicationPlaceholder.getApplication().getIconRes() != null) {
                ImageView imageView2 = this.binding.f2288b;
                m.checkNotNullExpressionValue(imageView2, "binding.chatInputApplicationAvatar");
                Context context = imageView2.getContext();
                m.checkNotNullExpressionValue(context, "binding.chatInputApplicationAvatar.context");
                Drawable drawable$default = DrawableCompat.getDrawable$default(context, applicationPlaceholder.getApplication().getIconRes().intValue(), themedColor, false, 4, null);
                if (drawable$default != null) {
                    this.binding.f2288b.setImageDrawable(drawable$default);
                } else {
                    this.binding.f2288b.setImageResource(applicationPlaceholder.getApplication().getIconRes().intValue());
                }
            } else {
                String applicationIcon$default = IconUtils.getApplicationIcon$default(IconUtils.INSTANCE, applicationPlaceholder.getApplication(), 0, 2, (Object) null);
                HashSet hashSet = new HashSet();
                hashSet.add(new MGImagesBitmap.ImageRequest(applicationIcon$default, true));
                this.binding.f2288b.setImageBitmap(null);
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(MGImagesBitmap.getBitmaps(hashSet)), CommandHeaderViewHolder.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new CommandHeaderViewHolder$bind$2(this, themedColor), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new CommandHeaderViewHolder$bind$1(this, applicationPlaceholder, applicationIcon$default));
            }
        }
    }

    public final ApplicationPlaceholder getCurrentItem() {
        return this.currentItem;
    }

    @Override // com.discord.widgets.chat.input.autocomplete.adapter.StickyHeaderHolder
    public View getItemView() {
        return this.itemView;
    }

    public final void setCurrentItem(ApplicationPlaceholder applicationPlaceholder) {
        this.currentItem = applicationPlaceholder;
    }
}
