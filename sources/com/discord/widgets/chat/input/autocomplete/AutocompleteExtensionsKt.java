package com.discord.widgets.chat.input.autocomplete;

import com.discord.app.AppLog;
import com.discord.utilities.logging.Logger;
import d0.g0.w;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import java.util.Comparator;
import java.util.Map;
import kotlin.Metadata;
import kotlin.ranges.IntRange;
/* compiled from: AutocompleteExtensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u000e\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a%\u0010\u0005\u001a\u00020\u0000*\u00020\u00002\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"", "", "Lkotlin/ranges/IntRange;", "Lcom/discord/widgets/chat/input/autocomplete/Autocompletable;", "inputMentionsMap", "replaceAutocompleteDataWithServerValues", "(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AutocompleteExtensionsKt {
    public static final String replaceAutocompleteDataWithServerValues(String str, Map<IntRange, ? extends Autocompletable> map) {
        m.checkNotNullParameter(str, "$this$replaceAutocompleteDataWithServerValues");
        m.checkNotNullParameter(map, "inputMentionsMap");
        String str2 = str;
        for (IntRange intRange : u.sortedWith(map.keySet(), new Comparator() { // from class: com.discord.widgets.chat.input.autocomplete.AutocompleteExtensionsKt$replaceAutocompleteDataWithServerValues$$inlined$sortedByDescending$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return a.compareValues(Integer.valueOf(((IntRange) t2).getFirst()), Integer.valueOf(((IntRange) t).getFirst()));
            }
        })) {
            Autocompletable autocompletable = map.get(intRange);
            if (autocompletable != null) {
                if (intRange.getFirst() > str2.length() || intRange.getLast() > str2.length()) {
                    AppLog appLog = AppLog.g;
                    StringBuilder R = b.d.b.a.a.R("Invalid mention position to insert ");
                    R.append(autocompletable.getInputReplacement());
                    R.append(' ');
                    R.append("into ");
                    R.append(str2);
                    R.append(" at [");
                    R.append(intRange.getFirst());
                    R.append(" - ");
                    R.append(intRange.getLast());
                    R.append("]. Original input: ");
                    R.append(str);
                    String sb = R.toString();
                    StringBuilder R2 = b.d.b.a.a.R("first ");
                    R2.append(intRange.getFirst());
                    R2.append(", last ");
                    R2.append(intRange.getLast());
                    R2.append(", s.length() ");
                    R2.append(str2.length());
                    Logger.e$default(appLog, sb, new IndexOutOfBoundsException(R2.toString()), null, 4, null);
                } else {
                    str2 = w.replaceRange(str2, intRange.getFirst(), intRange.getLast(), autocompletable.getInputReplacement()).toString();
                }
            }
        }
        return str2;
    }
}
