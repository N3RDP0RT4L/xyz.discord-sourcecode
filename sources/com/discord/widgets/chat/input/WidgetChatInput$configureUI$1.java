package com.discord.widgets.chat.input;

import com.discord.widgets.chat.input.ChatInputViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInput$configureUI$1 extends o implements Function0<Unit> {
    public final /* synthetic */ ChatInputViewModel.ViewState $viewState;
    public final /* synthetic */ WidgetChatInput this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInput$configureUI$1(WidgetChatInput widgetChatInput, ChatInputViewModel.ViewState viewState) {
        super(0);
        this.this$0 = widgetChatInput;
        this.$viewState = viewState;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WidgetChatInputTruncatedHint widgetChatInputTruncatedHint;
        CharSequence hint;
        widgetChatInputTruncatedHint = this.this$0.chatInputTruncatedHint;
        if (widgetChatInputTruncatedHint != null) {
            WidgetChatInput widgetChatInput = this.this$0;
            hint = widgetChatInput.getHint(widgetChatInput.requireContext(), ((ChatInputViewModel.ViewState.Loaded) this.$viewState).getChannel(), ((ChatInputViewModel.ViewState.Loaded) this.$viewState).isBlocked(), ((ChatInputViewModel.ViewState.Loaded) this.$viewState).getAbleToSendMessage());
            widgetChatInputTruncatedHint.setHint(hint);
        }
        this.this$0.configureSendListeners((ChatInputViewModel.ViewState.Loaded) this.$viewState);
    }
}
