package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import android.content.ContentResolver;
import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.core.view.inputmethod.InputContentInfoCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.i.r4;
import b.a.k.b;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppFragment;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.experiments.domain.Experiment;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.widgets.chat.input.emoji.EmojiPickerListener;
import com.discord.widgets.chat.input.expression.WidgetExpressionTray;
import com.discord.widgets.chat.input.sticker.StickerPickerListener;
import com.google.android.material.button.MaterialButton;
import com.lytefast.flexinput.FlexInputListener;
import com.lytefast.flexinput.adapters.AttachmentPreviewAdapter;
import com.lytefast.flexinput.adapters.EmptyListAdapter;
import com.lytefast.flexinput.fragment.FilesFragment;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.fragment.MediaFragment;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetChatInputAttachments.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0003-./B\u000f\u0012\u0006\u0010)\u001a\u00020(¢\u0006\u0004\b+\u0010,J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\u000e\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ/\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012\"\u000e\b\u0000\u0010\u0011*\b\u0012\u0004\u0012\u00020\u00010\u00102\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0017\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018J\u0015\u0010\u001b\u001a\u00020\r2\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u0015\u0010\u001f\u001a\u00020\r2\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010#\u001a\u00020\r2\b\u0010\"\u001a\u0004\u0018\u00010!¢\u0006\u0004\b#\u0010$J\u001b\u0010&\u001a\u00020\r2\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00010\u0010¢\u0006\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*¨\u00060"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Landroid/widget/TextView;", "chatInput", "Landroidx/fragment/app/Fragment;", "createAndConfigureExpressionFragment", "(Landroidx/fragment/app/FragmentManager;Landroid/widget/TextView;)Landroidx/fragment/app/Fragment;", "Landroid/content/Context;", "context", "Landroidx/core/view/inputmethod/InputContentInfoCompat;", "inputContentInfoCompat", "", "setAttachmentFromPicker", "(Landroid/content/Context;Landroidx/core/view/inputmethod/InputContentInfoCompat;)V", "Lcom/lytefast/flexinput/model/Attachment;", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;", "createPreviewAdapter", "(Landroid/content/Context;)Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;", "", "canCreateThread", "configureFlexInputContentPages", "(Z)V", "Lcom/discord/app/AppFragment;", "fragment", "configureFlexInputFragment", "(Lcom/discord/app/AppFragment;)V", "Lcom/lytefast/flexinput/FlexInputListener;", "inputListener", "setInputListener", "(Lcom/lytefast/flexinput/FlexInputListener;)V", "Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;", "viewModel", "setViewModel", "(Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;)V", "attachment", "addExternalAttachment", "(Lcom/lytefast/flexinput/model/Attachment;)V", "Lcom/lytefast/flexinput/fragment/FlexInputFragment;", "flexInputFragment", "Lcom/lytefast/flexinput/fragment/FlexInputFragment;", HookHelper.constructorName, "(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V", "DiscordFilesFragment", "DiscordMediaFragment", "PermissionsEmptyListAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInputAttachments {
    private final FlexInputFragment flexInputFragment;

    /* compiled from: WidgetChatInputAttachments.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$DiscordFilesFragment;", "Lcom/lytefast/flexinput/fragment/FilesFragment;", "Landroid/view/View$OnClickListener;", "onClickListener", "Lcom/lytefast/flexinput/adapters/EmptyListAdapter;", "newPermissionsRequestAdapter", "(Landroid/view/View$OnClickListener;)Lcom/lytefast/flexinput/adapters/EmptyListAdapter;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DiscordFilesFragment extends FilesFragment {
        @Override // com.lytefast.flexinput.fragment.FilesFragment
        public EmptyListAdapter newPermissionsRequestAdapter(View.OnClickListener onClickListener) {
            m.checkNotNullParameter(onClickListener, "onClickListener");
            return new PermissionsEmptyListAdapter(R.layout.widget_chat_input_perm_req_files, R.id.action_btn, onClickListener);
        }
    }

    /* compiled from: WidgetChatInputAttachments.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$DiscordMediaFragment;", "Lcom/lytefast/flexinput/fragment/MediaFragment;", "Landroid/view/View$OnClickListener;", "onClickListener", "Lcom/lytefast/flexinput/adapters/EmptyListAdapter;", "newPermissionsRequestAdapter", "(Landroid/view/View$OnClickListener;)Lcom/lytefast/flexinput/adapters/EmptyListAdapter;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DiscordMediaFragment extends MediaFragment {
        @Override // com.lytefast.flexinput.fragment.MediaFragment
        public EmptyListAdapter newPermissionsRequestAdapter(View.OnClickListener onClickListener) {
            m.checkNotNullParameter(onClickListener, "onClickListener");
            return new PermissionsEmptyListAdapter(R.layout.widget_chat_input_perm_req_files, R.id.action_btn, onClickListener);
        }
    }

    /* compiled from: WidgetChatInputAttachments.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B#\u0012\b\b\u0001\u0010\t\u001a\u00020\u0004\u0012\b\b\u0001\u0010\n\u001a\u00020\u0004\u0012\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\u0007\u001a\u00060\u0006R\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInputAttachments$PermissionsEmptyListAdapter;", "Lcom/lytefast/flexinput/adapters/EmptyListAdapter;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/lytefast/flexinput/adapters/EmptyListAdapter$ViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/lytefast/flexinput/adapters/EmptyListAdapter$ViewHolder;", "itemLayoutId", "actionBtnId", "Landroid/view/View$OnClickListener;", "onClickListener", HookHelper.constructorName, "(IILandroid/view/View$OnClickListener;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PermissionsEmptyListAdapter extends EmptyListAdapter {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PermissionsEmptyListAdapter(@LayoutRes int i, @IdRes int i2, View.OnClickListener onClickListener) {
            super(i, i2, onClickListener);
            m.checkNotNullParameter(onClickListener, "onClickListener");
        }

        @Override // com.lytefast.flexinput.adapters.EmptyListAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
        public EmptyListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            EmptyListAdapter.ViewHolder onCreateViewHolder = super.onCreateViewHolder(viewGroup, i);
            View view = onCreateViewHolder.itemView;
            int i2 = R.id.action_btn;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.action_btn);
            if (materialButton != null) {
                i2 = R.id.perm_req_text;
                TextView textView = (TextView) view.findViewById(R.id.perm_req_text);
                if (textView != null) {
                    m.checkNotNullExpressionValue(new r4((LinearLayout) view, materialButton, textView), "WidgetChatInputPermReqFi…ing.bind(holder.itemView)");
                    m.checkNotNullExpressionValue(textView, "binding.permReqText");
                    b.m(textView, R.string.system_permission_request_files, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                    return onCreateViewHolder;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i2)));
        }
    }

    public WidgetChatInputAttachments(FlexInputFragment flexInputFragment) {
        m.checkNotNullParameter(flexInputFragment, "flexInputFragment");
        this.flexInputFragment = flexInputFragment;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Fragment createAndConfigureExpressionFragment(FragmentManager fragmentManager, final TextView textView) {
        EmojiPickerListener widgetChatInputAttachments$createAndConfigureExpressionFragment$emojiPickerListener$1 = new EmojiPickerListener() { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$createAndConfigureExpressionFragment$emojiPickerListener$1
            @Override // com.discord.widgets.chat.input.emoji.EmojiPickerListener
            public void onEmojiPicked(Emoji emoji) {
                FlexInputFragment flexInputFragment;
                m.checkNotNullParameter(emoji, "emoji");
                flexInputFragment = WidgetChatInputAttachments.this.flexInputFragment;
                String chatInputText = emoji.getChatInputText();
                m.checkNotNullExpressionValue(chatInputText, "emoji.chatInputText");
                Objects.requireNonNull(flexInputFragment);
                m.checkNotNullParameter(chatInputText, "emojiText");
                FlexInputViewModel flexInputViewModel = flexInputFragment.f3137s;
                if (flexInputViewModel != null) {
                    flexInputViewModel.onInputTextAppended(chatInputText + ' ');
                }
            }
        };
        StickerPickerListener widgetChatInputAttachments$createAndConfigureExpressionFragment$stickerPickerListener$1 = new StickerPickerListener() { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$createAndConfigureExpressionFragment$stickerPickerListener$1
            @Override // com.discord.widgets.chat.input.sticker.StickerPickerListener
            public void onStickerPicked(Sticker sticker) {
                FlexInputFragment flexInputFragment;
                m.checkNotNullParameter(sticker, "sticker");
                flexInputFragment = WidgetChatInputAttachments.this.flexInputFragment;
                FlexInputViewModel flexInputViewModel = flexInputFragment.f3137s;
                if (flexInputViewModel != null) {
                    flexInputViewModel.hideExpressionTray();
                }
            }
        };
        OnBackspacePressedListener widgetChatInputAttachments$createAndConfigureExpressionFragment$onBackspacePressedListener$1 = new OnBackspacePressedListener() { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$createAndConfigureExpressionFragment$onBackspacePressedListener$1
            @Override // com.discord.widgets.chat.input.OnBackspacePressedListener
            public void onBackspacePressed() {
                textView.dispatchKeyEvent(new KeyEvent(0, 67));
            }
        };
        Fragment findFragmentById = fragmentManager.findFragmentById(R.id.expression_tray_container);
        if (!(findFragmentById instanceof WidgetExpressionTray)) {
            findFragmentById = null;
        }
        WidgetExpressionTray widgetExpressionTray = (WidgetExpressionTray) findFragmentById;
        if (widgetExpressionTray == null) {
            widgetExpressionTray = new WidgetExpressionTray();
        }
        widgetExpressionTray.setEmojiPickerListener(widgetChatInputAttachments$createAndConfigureExpressionFragment$emojiPickerListener$1);
        widgetExpressionTray.setStickerPickerListener(widgetChatInputAttachments$createAndConfigureExpressionFragment$stickerPickerListener$1);
        widgetExpressionTray.setOnBackspacePressedListener(widgetChatInputAttachments$createAndConfigureExpressionFragment$onBackspacePressedListener$1);
        widgetExpressionTray.setOnEmojiSearchOpenedListener(new WidgetChatInputAttachments$createAndConfigureExpressionFragment$1(this));
        return widgetExpressionTray;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final <T extends Attachment<? extends Object>> AttachmentPreviewAdapter<T> createPreviewAdapter(Context context) {
        WidgetChatInputAttachments$createPreviewAdapter$onAttachmentSelected$1 widgetChatInputAttachments$createPreviewAdapter$onAttachmentSelected$1 = new WidgetChatInputAttachments$createPreviewAdapter$onAttachmentSelected$1(this);
        boolean z2 = true;
        Experiment userExperiment = StoreStream.Companion.getExperiments().getUserExperiment("2021-10_android_attachment_bottom_sheet", true);
        if (userExperiment == null || userExperiment.getBucket() != 1) {
            z2 = false;
        }
        return new AttachmentPreviewAdapter<>(z2, widgetChatInputAttachments$createPreviewAdapter$onAttachmentSelected$1, new WidgetChatInputAttachments$createPreviewAdapter$1(this, context));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setAttachmentFromPicker(Context context, InputContentInfoCompat inputContentInfoCompat) {
        CharSequence b2;
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver != null) {
            b2 = b.b(context, R.string.attachment_filename_unknown, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            this.flexInputFragment.f(new SourcedAttachment(Attachment.Companion.c(inputContentInfoCompat, contentResolver, true, b2.toString()), AnalyticsTracker.ATTACHMENT_SOURCE_KEYBOARD));
        }
    }

    public final void addExternalAttachment(Attachment<? extends Object> attachment) {
        m.checkNotNullParameter(attachment, "attachment");
        this.flexInputFragment.i(new WidgetChatInputAttachments$addExternalAttachment$1(this, attachment));
    }

    public final void configureFlexInputContentPages(boolean z2) {
        this.flexInputFragment.i(new WidgetChatInputAttachments$configureFlexInputContentPages$1(this, z2));
    }

    public final void configureFlexInputFragment(AppFragment appFragment) {
        m.checkNotNullParameter(appFragment, "fragment");
        this.flexInputFragment.i(new WidgetChatInputAttachments$configureFlexInputFragment$1(this, appFragment));
        appFragment.setOnBackPressed(new Func0<Boolean>() { // from class: com.discord.widgets.chat.input.WidgetChatInputAttachments$configureFlexInputFragment$2
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                FlexInputFragment flexInputFragment;
                flexInputFragment = WidgetChatInputAttachments.this.flexInputFragment;
                FlexInputViewModel flexInputViewModel = flexInputFragment.f3137s;
                Boolean valueOf = flexInputViewModel != null ? Boolean.valueOf(flexInputViewModel.hideExpressionTray()) : null;
                return valueOf != null ? valueOf : Boolean.FALSE;
            }
        }, 1);
    }

    public final void setInputListener(FlexInputListener flexInputListener) {
        m.checkNotNullParameter(flexInputListener, "inputListener");
        FlexInputFragment flexInputFragment = this.flexInputFragment;
        Objects.requireNonNull(flexInputFragment);
        m.checkNotNullParameter(flexInputListener, "inputListener");
        flexInputFragment.o = flexInputListener;
    }

    public final void setViewModel(FlexInputViewModel flexInputViewModel) {
        this.flexInputFragment.f3137s = flexInputViewModel;
    }
}
