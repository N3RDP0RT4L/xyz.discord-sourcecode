package com.discord.widgets.chat.input.gifpicker;

import com.discord.widgets.chat.input.gifpicker.GifAdapterItem;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGifPickerSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;", "gifItem", "", "invoke", "(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGifPickerSearch$setUpGifRecycler$1 extends o implements Function1<GifAdapterItem.GifItem, Unit> {
    public final /* synthetic */ WidgetGifPickerSearch this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGifPickerSearch$setUpGifRecycler$1(WidgetGifPickerSearch widgetGifPickerSearch) {
        super(1);
        this.this$0 = widgetGifPickerSearch;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GifAdapterItem.GifItem gifItem) {
        invoke2(gifItem);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GifAdapterItem.GifItem gifItem) {
        GifSearchViewModel gifPickerViewModel;
        Function0 function0;
        m.checkNotNullParameter(gifItem, "gifItem");
        gifPickerViewModel = this.this$0.getGifPickerViewModel();
        gifPickerViewModel.selectGif(gifItem);
        function0 = this.this$0.onGifSelected;
        if (function0 != null) {
            Unit unit = (Unit) function0.invoke();
        }
    }
}
