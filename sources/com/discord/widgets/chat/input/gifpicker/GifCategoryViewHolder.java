package com.discord.widgets.chat.input.gifpicker;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.GifCategoryItemViewBinding;
import com.discord.models.gifpicker.domain.ModelGifCategory;
import com.discord.utilities.images.MGImages;
import com.discord.widgets.chat.input.gifpicker.GifCategoryItem;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: GifCategoryViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J+\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0014\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0004\u0018\u00010\t¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "", "imageUrl", "", "setPreviewImage", "(Ljava/lang/String;)V", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;", "gifCategoryItem", "Lkotlin/Function1;", "onSelectGifCategory", "configure", "(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/databinding/GifCategoryItemViewBinding;", "binding", "Lcom/discord/databinding/GifCategoryItemViewBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/GifCategoryItemViewBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GifCategoryViewHolder extends RecyclerView.ViewHolder {
    private final GifCategoryItemViewBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GifCategoryViewHolder(GifCategoryItemViewBinding gifCategoryItemViewBinding) {
        super(gifCategoryItemViewBinding.a);
        m.checkNotNullParameter(gifCategoryItemViewBinding, "binding");
        this.binding = gifCategoryItemViewBinding;
        this.itemView.setOnTouchListener(new ViewScalingOnTouchListener(0.9f));
    }

    private final void setPreviewImage(String str) {
        SimpleDraweeView simpleDraweeView = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.gifCategoryItemPreview");
        ViewGroup.LayoutParams layoutParams = simpleDraweeView.getLayoutParams();
        int i = layoutParams.width;
        int i2 = layoutParams.height;
        SimpleDraweeView simpleDraweeView2 = this.binding.c;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.gifCategoryItemPreview");
        MGImages.setImage$default(simpleDraweeView2, str, i, i2, false, null, null, 112, null);
    }

    public final void configure(final GifCategoryItem gifCategoryItem, final Function1<? super GifCategoryItem, Unit> function1) {
        m.checkNotNullParameter(gifCategoryItem, "gifCategoryItem");
        if (gifCategoryItem instanceof GifCategoryItem.Standard) {
            ModelGifCategory gifCategory = ((GifCategoryItem.Standard) gifCategoryItem).getGifCategory();
            setPreviewImage(gifCategory.getGifPreviewUrl());
            TextView textView = this.binding.d;
            m.checkNotNullExpressionValue(textView, "binding.gifCategoryItemTitle");
            textView.setText(gifCategory.getCategoryName());
            ImageView imageView = this.binding.f2095b;
            m.checkNotNullExpressionValue(imageView, "binding.gifCategoryItemIcon");
            imageView.setVisibility(8);
        } else if (gifCategoryItem instanceof GifCategoryItem.Trending) {
            setPreviewImage(((GifCategoryItem.Trending) gifCategoryItem).getGifPreviewUrl());
            TextView textView2 = this.binding.d;
            m.checkNotNullExpressionValue(textView2, "binding.gifCategoryItemTitle");
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            textView2.setText(view.getResources().getString(R.string.gif_picker_result_type_trending_gifs));
            ImageView imageView2 = this.binding.f2095b;
            m.checkNotNullExpressionValue(imageView2, "binding.gifCategoryItemIcon");
            imageView2.setVisibility(0);
            ImageView imageView3 = this.binding.f2095b;
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            imageView3.setImageDrawable(ResourcesCompat.getDrawable(view2.getResources(), R.drawable.ic_analytics_16dp, null));
        }
        this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.gifpicker.GifCategoryViewHolder$configure$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                Function1 function12 = Function1.this;
                if (function12 != null) {
                    Unit unit = (Unit) function12.invoke(gifCategoryItem);
                }
            }
        });
    }
}
