package com.discord.widgets.chat.input.gifpicker;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetGifPickerSearchBinding;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.input.gifpicker.GifAdapter;
import com.discord.widgets.chat.input.gifpicker.GifSearchViewModel;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGifPickerSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b(\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0011\u0010\bJ\u001b\u0010\u0014\u001a\u00020\u00042\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\r\u0010\u0016\u001a\u00020\u0004¢\u0006\u0004\b\u0016\u0010\bR\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u001d\u0010\u001f\u001a\u00020\u001a8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001d\u0010%\u001a\u00020 8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u001e\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'¨\u0006)"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/WidgetGifPickerSearch;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;", "viewState", "", "handleViewState", "(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;)V", "setupSearchBar", "()V", "", "categoryColumnsCount", "setUpGifRecycler", "(I)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lkotlin/Function0;", "onSelected", "setOnGifSelected", "(Lkotlin/jvm/functions/Function0;)V", "clearSearchBar", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;", "gifAdapter", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;", "Lcom/discord/databinding/WidgetGifPickerSearchBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGifPickerSearchBinding;", "binding", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;", "gifPickerViewModel$delegate", "Lkotlin/Lazy;", "getGifPickerViewModel", "()Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;", "gifPickerViewModel", "onGifSelected", "Lkotlin/jvm/functions/Function0;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGifPickerSearch extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGifPickerSearch.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGifPickerSearchBinding;", 0)};
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGifPickerSearch$binding$2.INSTANCE, null, 2, null);
    private GifAdapter gifAdapter;
    private final Lazy gifPickerViewModel$delegate;
    private Function0<Unit> onGifSelected;

    public WidgetGifPickerSearch() {
        super(R.layout.widget_gif_picker_search);
        WidgetGifPickerSearch$gifPickerViewModel$2 widgetGifPickerSearch$gifPickerViewModel$2 = new WidgetGifPickerSearch$gifPickerViewModel$2(this);
        f0 f0Var = new f0(this);
        this.gifPickerViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GifSearchViewModel.class), new WidgetGifPickerSearch$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGifPickerSearch$gifPickerViewModel$2));
    }

    private final WidgetGifPickerSearchBinding getBinding() {
        return (WidgetGifPickerSearchBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GifSearchViewModel getGifPickerViewModel() {
        return (GifSearchViewModel) this.gifPickerViewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleViewState(GifSearchViewModel.ViewState viewState) {
        if (viewState instanceof GifSearchViewModel.ViewState.Loaded) {
            GifAdapter gifAdapter = this.gifAdapter;
            if (gifAdapter == null) {
                m.throwUninitializedPropertyAccessException("gifAdapter");
            }
            gifAdapter.setItems(((GifSearchViewModel.ViewState.Loaded) viewState).getAdapterItems());
            AppViewFlipper appViewFlipper = getBinding().d;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.gifSearchViewFlipper");
            appViewFlipper.setDisplayedChild(0);
        } else if (m.areEqual(viewState, GifSearchViewModel.ViewState.LoadingSearchResults.INSTANCE)) {
            GifAdapter gifAdapter2 = this.gifAdapter;
            if (gifAdapter2 == null) {
                m.throwUninitializedPropertyAccessException("gifAdapter");
            }
            gifAdapter2.clearItems();
            AppViewFlipper appViewFlipper2 = getBinding().d;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.gifSearchViewFlipper");
            appViewFlipper2.setDisplayedChild(1);
        }
    }

    private final void setUpGifRecycler(int i) {
        RecyclerView recyclerView = getBinding().f2380b;
        m.checkNotNullExpressionValue(recyclerView, "binding.gifSearchGifRecycler");
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(i, 1));
        RecyclerView recyclerView2 = getBinding().f2380b;
        m.checkNotNullExpressionValue(recyclerView2, "binding.gifSearchGifRecycler");
        recyclerView2.setItemAnimator(null);
        int dpToPixels = DimenUtils.dpToPixels(8);
        WidgetGifPickerSearch$setUpGifRecycler$1 widgetGifPickerSearch$setUpGifRecycler$1 = new WidgetGifPickerSearch$setUpGifRecycler$1(this);
        GifAdapter.Companion companion = GifAdapter.Companion;
        RecyclerView recyclerView3 = getBinding().f2380b;
        m.checkNotNullExpressionValue(recyclerView3, "binding.gifSearchGifRecycler");
        this.gifAdapter = new GifAdapter(this, widgetGifPickerSearch$setUpGifRecycler$1, companion.calculateColumnWidth(recyclerView3, i, dpToPixels), new WidgetGifPickerSearch$setUpGifRecycler$2(getBinding().e), null, 16, null);
        RecyclerView recyclerView4 = getBinding().f2380b;
        m.checkNotNullExpressionValue(recyclerView4, "binding.gifSearchGifRecycler");
        GifAdapter gifAdapter = this.gifAdapter;
        if (gifAdapter == null) {
            m.throwUninitializedPropertyAccessException("gifAdapter");
        }
        recyclerView4.setAdapter(gifAdapter);
        getBinding().f2380b.addItemDecoration(new u(dpToPixels, i));
    }

    private final void setupSearchBar() {
        getBinding().e.a(this, new WidgetGifPickerSearch$setupSearchBar$1(this));
    }

    public final void clearSearchBar() {
        getGifPickerViewModel().setSearchText("");
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        GifAdapter.Companion companion = GifAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2380b;
        m.checkNotNullExpressionValue(recyclerView, "binding.gifSearchGifRecycler");
        int calculateColumnCount = companion.calculateColumnCount(recyclerView);
        setUpGifRecycler(calculateColumnCount);
        setupSearchBar();
        GifLoadingView.updateView$default(getBinding().c, calculateColumnCount, 0, 2, null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        getBinding().e.k.c.requestFocus();
        Observable<GifSearchViewModel.ViewState> q = getGifPickerViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "gifPickerViewModel\n     …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetGifPickerSearch.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGifPickerSearch$onViewBoundOrOnResume$1(this));
    }

    public final void setOnGifSelected(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSelected");
        this.onGifSelected = function0;
    }
}
