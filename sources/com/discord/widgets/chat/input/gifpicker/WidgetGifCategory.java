package com.discord.widgets.chat.input.gifpicker;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.TextView;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import b.a.d.e0;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGifCategoryBinding;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.input.expression.ExpressionTrayViewModel;
import com.discord.widgets.chat.input.gifpicker.GifAdapter;
import com.discord.widgets.chat.input.gifpicker.GifAdapterItem;
import com.discord.widgets.chat.input.gifpicker.GifCategoryItem;
import com.discord.widgets.chat.input.gifpicker.GifCategoryViewModel;
import d0.z.d.a0;
import d0.z.d.m;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetGifCategory.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 72\u00020\u0001:\u00017B\u0007¢\u0006\u0004\b6\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\f\u0010\u000bJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0015\u0010\u000bJ\u000f\u0010\u0016\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0016\u0010\u000bJ\u0017\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u0017H\u0016¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001b\u0010\u000bJ\u001b\u0010\u001e\u001a\u00020\u00042\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00040\u001c¢\u0006\u0004\b\u001e\u0010\u001fR\u001d\u0010%\u001a\u00020 8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u001d\u0010+\u001a\u00020&8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001d\u00100\u001a\u00020,8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b-\u0010(\u001a\u0004\b.\u0010/R\u0016\u00102\u001a\u0002018\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b2\u00103R\u001e\u00104\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b4\u00105¨\u00068"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;", "viewState", "", "handleViewState", "(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;)V", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;", "getGifCategory", "()Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;", "setUpTitle", "()V", "setUpBackBehavior", "", "categoryColumnsCount", "setUpGifRecycler", "(I)V", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;", "gifItem", "selectGif", "(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V", "handleBack", "setWindowInsetsListeners", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lkotlin/Function0;", "onSelected", "setOnGifSelected", "(Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/databinding/WidgetGifCategoryBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGifCategoryBinding;", "binding", "Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;", "expressionTrayViewModel$delegate", "Lkotlin/Lazy;", "getExpressionTrayViewModel", "()Lcom/discord/widgets/chat/input/expression/ExpressionTrayViewModel;", "expressionTrayViewModel", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;", "gifCategoryViewModel$delegate", "getGifCategoryViewModel", "()Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;", "gifCategoryViewModel", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;", "gifAdapter", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapter;", "onGifSelected", "Lkotlin/jvm/functions/Function0;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGifCategory extends AppFragment {
    public static final String ARG_GIF_CATEGORY_ITEM = "GIF_CATEGORY_ITEM";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGifCategory$binding$2.INSTANCE, null, 2, null);
    private final Lazy expressionTrayViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(ExpressionTrayViewModel.class), new WidgetGifCategory$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetGifCategory$expressionTrayViewModel$2.INSTANCE));
    private GifAdapter gifAdapter;
    private final Lazy gifCategoryViewModel$delegate;
    private Function0<Unit> onGifSelected;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGifCategory.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGifCategoryBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetGifCategory.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/WidgetGifCategory$Companion;", "", "", "ARG_GIF_CATEGORY_ITEM", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGifCategory() {
        super(R.layout.widget_gif_category);
        WidgetGifCategory$gifCategoryViewModel$2 widgetGifCategory$gifCategoryViewModel$2 = new WidgetGifCategory$gifCategoryViewModel$2(this);
        f0 f0Var = new f0(this);
        this.gifCategoryViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GifCategoryViewModel.class), new WidgetGifCategory$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGifCategory$gifCategoryViewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGifCategoryBinding getBinding() {
        return (WidgetGifCategoryBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final ExpressionTrayViewModel getExpressionTrayViewModel() {
        return (ExpressionTrayViewModel) this.expressionTrayViewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GifCategoryItem getGifCategory() {
        Serializable serializable = requireArguments().getSerializable(ARG_GIF_CATEGORY_ITEM);
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type com.discord.widgets.chat.input.gifpicker.GifCategoryItem");
        return (GifCategoryItem) serializable;
    }

    private final GifCategoryViewModel getGifCategoryViewModel() {
        return (GifCategoryViewModel) this.gifCategoryViewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleBack() {
        getExpressionTrayViewModel().clickBack();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleViewState(GifCategoryViewModel.ViewState viewState) {
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.gifCategoryGifRecycler");
        recyclerView.setVisibility(0);
        GifLoadingView gifLoadingView = getBinding().d;
        m.checkNotNullExpressionValue(gifLoadingView, "binding.gifCategoryLoadingView");
        gifLoadingView.setVisibility(8);
        GifAdapter gifAdapter = this.gifAdapter;
        if (gifAdapter == null) {
            m.throwUninitializedPropertyAccessException("gifAdapter");
        }
        gifAdapter.setItems(viewState.getGifItems());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void selectGif(GifAdapterItem.GifItem gifItem) {
        getGifCategoryViewModel().selectGif(gifItem);
        Function0<Unit> function0 = this.onGifSelected;
        if (function0 != null) {
            function0.invoke();
        }
    }

    private final void setUpBackBehavior() {
        setOnBackPressed(new Func0<Boolean>() { // from class: com.discord.widgets.chat.input.gifpicker.WidgetGifCategory$setUpBackBehavior$1
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                WidgetGifCategory.this.handleBack();
                return Boolean.TRUE;
            }
        }, 0);
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.gifpicker.WidgetGifCategory$setUpBackBehavior$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGifCategory.this.handleBack();
            }
        });
    }

    private final void setUpGifRecycler(int i) {
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(i, 1);
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.gifCategoryGifRecycler");
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        RecyclerView recyclerView2 = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView2, "binding.gifCategoryGifRecycler");
        recyclerView2.setItemAnimator(null);
        int dpToPixels = DimenUtils.dpToPixels(8);
        GifAdapter.Companion companion = GifAdapter.Companion;
        RecyclerView recyclerView3 = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView3, "binding.gifCategoryGifRecycler");
        this.gifAdapter = new GifAdapter(this, new WidgetGifCategory$setUpGifRecycler$1(this), companion.calculateColumnWidth(recyclerView3, i, dpToPixels), null, null, 24, null);
        RecyclerView recyclerView4 = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView4, "binding.gifCategoryGifRecycler");
        GifAdapter gifAdapter = this.gifAdapter;
        if (gifAdapter == null) {
            m.throwUninitializedPropertyAccessException("gifAdapter");
        }
        recyclerView4.setAdapter(gifAdapter);
        getBinding().c.addItemDecoration(new u(dpToPixels, i));
    }

    private final void setUpTitle() {
        String str;
        GifCategoryItem gifCategory = getGifCategory();
        TextView textView = getBinding().e;
        m.checkNotNullExpressionValue(textView, "binding.gifCategoryTitle");
        if (gifCategory instanceof GifCategoryItem.Standard) {
            str = ((GifCategoryItem.Standard) gifCategory).getGifCategory().getCategoryName();
        } else if (gifCategory instanceof GifCategoryItem.Trending) {
            str = getResources().getString(R.string.gif_picker_result_type_trending_gifs);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        textView.setText(str);
    }

    private final void setWindowInsetsListeners() {
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().c, WidgetGifCategory$setWindowInsetsListeners$1.INSTANCE);
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().f2378b, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.chat.input.gifpicker.WidgetGifCategory$setWindowInsetsListeners$2
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetGifCategoryBinding binding;
                binding = WidgetGifCategory.this.getBinding();
                ViewCompat.dispatchApplyWindowInsets(binding.c, windowInsetsCompat);
                return windowInsetsCompat.consumeSystemWindowInsets();
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        GifAdapter.Companion companion = GifAdapter.Companion;
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.gifCategoryGifRecycler");
        int calculateColumnCount = companion.calculateColumnCount(recyclerView);
        setUpTitle();
        setUpBackBehavior();
        setUpGifRecycler(calculateColumnCount);
        GifLoadingView.updateView$default(getBinding().d, calculateColumnCount, 0, 2, null);
        setWindowInsetsListeners();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getGifCategoryViewModel().observeViewState(), this, null, 2, null), WidgetGifCategory.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGifCategory$onViewBoundOrOnResume$1(this));
    }

    public final void setOnGifSelected(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSelected");
        this.onGifSelected = function0;
    }
}
