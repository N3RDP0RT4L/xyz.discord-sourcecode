package com.discord.widgets.chat.input.gifpicker;

import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.gifpicker.dto.ModelGif;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreGifPicker;
import com.discord.utilities.analytics.SearchType;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.gifpicker.GifAdapterItem;
import com.discord.widgets.chat.input.gifpicker.GifCategoryItem;
import d0.g;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: GifCategoryViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001e2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001e\u001f BE\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\b\b\u0002\u0010\r\u001a\u00020\f\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0015\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012\u0012\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00030\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006!"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;)V", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;", "gifItem", "selectGif", "(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V", "Lcom/discord/widgets/chat/MessageManager;", "messageManager", "Lcom/discord/widgets/chat/MessageManager;", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;", "gifCategoryItem", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Lcom/discord/stores/StoreGifPicker;", "storeGifPicker", "Lcom/discord/stores/StoreGifPicker;", "Landroid/content/Context;", "context", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Landroid/content/Context;Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/stores/StoreGifPicker;Lcom/discord/stores/StoreAnalytics;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GifCategoryViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final GifCategoryItem gifCategoryItem;
    private final MessageManager messageManager;
    private final StoreAnalytics storeAnalytics;
    private final StoreGifPicker storeGifPicker;

    /* compiled from: GifCategoryViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.gifpicker.GifCategoryViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GifCategoryViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GifCategoryViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ%\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$Companion;", "", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;", "gifCategoryItem", "Lcom/discord/stores/StoreGifPicker;", "storeGifPicker", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;", "observeStoreState", "(Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;Lcom/discord/stores/StoreGifPicker;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(GifCategoryItem gifCategoryItem, StoreGifPicker storeGifPicker) {
            Observable<List<ModelGif>> observable;
            if (gifCategoryItem instanceof GifCategoryItem.Standard) {
                observable = storeGifPicker.observeGifsForSearchQuery(((GifCategoryItem.Standard) gifCategoryItem).getGifCategory().getCategoryName());
            } else if (gifCategoryItem instanceof GifCategoryItem.Trending) {
                observable = storeGifPicker.observeTrendingCategoryGifs();
            } else {
                throw new NoWhenBranchMatchedException();
            }
            Observable F = observable.F(GifCategoryViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(F, "gifsObservable.map { gifs -> StoreState(gifs) }");
            return F;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GifCategoryViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0005¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;", "", "", "Lcom/discord/models/gifpicker/dto/ModelGif;", "component1", "()Ljava/util/List;", "gifs", "copy", "(Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getGifs", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final List<ModelGif> gifs;

        public StoreState(List<ModelGif> list) {
            m.checkNotNullParameter(list, "gifs");
            this.gifs = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                list = storeState.gifs;
            }
            return storeState.copy(list);
        }

        public final List<ModelGif> component1() {
            return this.gifs;
        }

        public final StoreState copy(List<ModelGif> list) {
            m.checkNotNullParameter(list, "gifs");
            return new StoreState(list);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof StoreState) && m.areEqual(this.gifs, ((StoreState) obj).gifs);
            }
            return true;
        }

        public final List<ModelGif> getGifs() {
            return this.gifs;
        }

        public int hashCode() {
            List<ModelGif> list = this.gifs;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.K(a.R("StoreState(gifs="), this.gifs, ")");
        }
    }

    /* compiled from: GifCategoryViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R#\u0010\u0018\u001a\u00020\f8F@\u0006X\u0086\u0084\u0002¢\u0006\u0012\n\u0004\b\u0013\u0010\u0014\u0012\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0015\u0010\u000eR\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0019\u001a\u0004\b\u001a\u0010\u0005¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;", "", "", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;", "component1", "()Ljava/util/List;", "gifItems", "copy", "(Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "gifCount$delegate", "Lkotlin/Lazy;", "getGifCount", "getGifCount$annotations", "()V", "gifCount", "Ljava/util/List;", "getGifItems", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final Lazy gifCount$delegate = g.lazy(new GifCategoryViewModel$ViewState$gifCount$2(this));
        private final List<GifAdapterItem.GifItem> gifItems;

        public ViewState(List<GifAdapterItem.GifItem> list) {
            m.checkNotNullParameter(list, "gifItems");
            this.gifItems = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                list = viewState.gifItems;
            }
            return viewState.copy(list);
        }

        public static /* synthetic */ void getGifCount$annotations() {
        }

        public final List<GifAdapterItem.GifItem> component1() {
            return this.gifItems;
        }

        public final ViewState copy(List<GifAdapterItem.GifItem> list) {
            m.checkNotNullParameter(list, "gifItems");
            return new ViewState(list);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ViewState) && m.areEqual(this.gifItems, ((ViewState) obj).gifItems);
            }
            return true;
        }

        public final int getGifCount() {
            return ((Number) this.gifCount$delegate.getValue()).intValue();
        }

        public final List<GifAdapterItem.GifItem> getGifItems() {
            return this.gifItems;
        }

        public int hashCode() {
            List<GifAdapterItem.GifItem> list = this.gifItems;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.K(a.R("ViewState(gifItems="), this.gifItems, ")");
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GifCategoryViewModel(android.content.Context r21, com.discord.widgets.chat.input.gifpicker.GifCategoryItem r22, com.discord.widgets.chat.MessageManager r23, com.discord.stores.StoreGifPicker r24, com.discord.stores.StoreAnalytics r25, rx.Observable r26, int r27, kotlin.jvm.internal.DefaultConstructorMarker r28) {
        /*
            r20 = this;
            r0 = r27 & 4
            if (r0 == 0) goto L1a
            com.discord.widgets.chat.MessageManager r0 = new com.discord.widgets.chat.MessageManager
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 510(0x1fe, float:7.15E-43)
            r12 = 0
            r1 = r0
            r2 = r21
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r16 = r0
            goto L1c
        L1a:
            r16 = r23
        L1c:
            r0 = r27 & 8
            if (r0 == 0) goto L27
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGifPicker r0 = r0.getGifPicker()
            goto L29
        L27:
            r0 = r24
        L29:
            r1 = r27 & 16
            if (r1 == 0) goto L36
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreAnalytics r1 = r1.getAnalytics()
            r18 = r1
            goto L38
        L36:
            r18 = r25
        L38:
            r1 = r27 & 32
            if (r1 == 0) goto L47
            com.discord.widgets.chat.input.gifpicker.GifCategoryViewModel$Companion r1 = com.discord.widgets.chat.input.gifpicker.GifCategoryViewModel.Companion
            r2 = r22
            rx.Observable r1 = com.discord.widgets.chat.input.gifpicker.GifCategoryViewModel.Companion.access$observeStoreState(r1, r2, r0)
            r19 = r1
            goto L4b
        L47:
            r2 = r22
            r19 = r26
        L4b:
            r13 = r20
            r14 = r21
            r15 = r22
            r17 = r0
            r13.<init>(r14, r15, r16, r17, r18, r19)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.gifpicker.GifCategoryViewModel.<init>(android.content.Context, com.discord.widgets.chat.input.gifpicker.GifCategoryItem, com.discord.widgets.chat.MessageManager, com.discord.stores.StoreGifPicker, com.discord.stores.StoreAnalytics, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        List<ModelGif> gifs = storeState.getGifs();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(gifs, 10));
        for (ModelGif modelGif : gifs) {
            arrayList.add(new GifAdapterItem.GifItem(modelGif, null, 2, null));
        }
        ViewState viewState = new ViewState(arrayList);
        this.storeAnalytics.trackSearchResultViewed(SearchType.GIF, viewState.getGifCount(), (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? false : false);
        updateViewState(viewState);
    }

    public final void selectGif(GifAdapterItem.GifItem gifItem) {
        m.checkNotNullParameter(gifItem, "gifItem");
        ViewState viewState = getViewState();
        if (viewState != null) {
            StoreAnalytics.trackSearchResultSelected$default(this.storeAnalytics, SearchType.GIF, viewState.getGifCount(), null, new Traits.Source(null, null, Traits.Source.Obj.GIF_PICKER, null, null, 27, null), 4, null);
        }
        MessageManager.sendMessage$default(this.messageManager, gifItem.getGif().getTenorGifUrl(), null, null, null, null, false, null, null, null, 510, null);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GifCategoryViewModel(Context context, GifCategoryItem gifCategoryItem, MessageManager messageManager, StoreGifPicker storeGifPicker, StoreAnalytics storeAnalytics, Observable<StoreState> observable) {
        super(null);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(gifCategoryItem, "gifCategoryItem");
        m.checkNotNullParameter(messageManager, "messageManager");
        m.checkNotNullParameter(storeGifPicker, "storeGifPicker");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.gifCategoryItem = gifCategoryItem;
        this.messageManager = messageManager;
        this.storeGifPicker = storeGifPicker;
        this.storeAnalytics = storeAnalytics;
        StoreAnalytics.trackSearchStarted$default(storeAnalytics, SearchType.GIF, null, false, 6, null);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(observable, false, 1, null), this, null, 2, null), GifCategoryViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
