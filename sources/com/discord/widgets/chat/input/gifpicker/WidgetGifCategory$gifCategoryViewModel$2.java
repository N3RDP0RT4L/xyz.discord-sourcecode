package com.discord.widgets.chat.input.gifpicker;

import android.content.Context;
import com.discord.app.AppViewModel;
import com.discord.widgets.chat.input.gifpicker.GifCategoryViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGifCategory.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGifCategory$gifCategoryViewModel$2 extends o implements Function0<AppViewModel<GifCategoryViewModel.ViewState>> {
    public final /* synthetic */ WidgetGifCategory this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGifCategory$gifCategoryViewModel$2(WidgetGifCategory widgetGifCategory) {
        super(0);
        this.this$0 = widgetGifCategory;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<GifCategoryViewModel.ViewState> invoke() {
        GifCategoryItem gifCategory;
        Context requireContext = this.this$0.requireContext();
        gifCategory = this.this$0.getGifCategory();
        return new GifCategoryViewModel(requireContext, gifCategory, null, null, null, null, 60, null);
    }
}
