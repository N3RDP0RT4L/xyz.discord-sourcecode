package com.discord.widgets.chat.input.gifpicker;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import com.discord.app.AppViewModel;
import com.discord.models.gifpicker.dto.ModelGif;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreGifPicker;
import com.discord.utilities.analytics.SearchType;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.LeadingEdgeThrottle;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.gifpicker.GifAdapterItem;
import com.discord.widgets.chat.input.gifpicker.GifSearchViewModel;
import d0.g;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.a.r;
import j0.l.e.k;
import j0.p.a;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: GifSearchViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \"2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\"#$BM\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\b0\u0013\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0010\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0019\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016\u0012\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00030\u001e¢\u0006\u0004\b \u0010!J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\b0\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006%"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;)V", "", "searchText", "setSearchText", "(Ljava/lang/String;)V", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;", "gifItem", "selectGif", "(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V", "Lcom/discord/stores/StoreGifPicker;", "storeGifPicker", "Lcom/discord/stores/StoreGifPicker;", "Lrx/subjects/BehaviorSubject;", "searchSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/widgets/chat/MessageManager;", "messageManager", "Lcom/discord/widgets/chat/MessageManager;", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Landroid/content/Context;", "context", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Landroid/content/Context;Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreGifPicker;Lcom/discord/stores/StoreAnalytics;Lcom/discord/widgets/chat/MessageManager;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GifSearchViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final MessageManager messageManager;
    private final BehaviorSubject<String> searchSubject;
    private final StoreAnalytics storeAnalytics;
    private final StoreGifPicker storeGifPicker;

    /* compiled from: GifSearchViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.gifpicker.GifSearchViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GifSearchViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GifSearchViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012JA\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\tH\u0002¢\u0006\u0004\b\r\u0010\u000eJ3\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$Companion;", "", "Lrx/subjects/BehaviorSubject;", "", "searchSubject", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreGifPicker;", "storeGifPicker", "", "trendingSearchTerms", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;", "observeQueryState", "(Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreGifPicker;Ljava/util/List;)Lrx/Observable;", "observeStoreState", "(Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreGifPicker;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeQueryState(BehaviorSubject<String> behaviorSubject, final StoreAnalytics storeAnalytics, final StoreGifPicker storeGifPicker, final List<String> list) {
            Observable<String> q = behaviorSubject.I(a.c()).q();
            Observable<StoreState> Y = Observable.h0(new r(q.j, new LeadingEdgeThrottle(1000L, TimeUnit.MILLISECONDS, a.c()))).t(new Action1<String>() { // from class: com.discord.widgets.chat.input.gifpicker.GifSearchViewModel$Companion$observeQueryState$1
                public final void call(String str) {
                    StoreAnalytics.trackSearchStarted$default(StoreAnalytics.this, SearchType.GIF, null, false, 6, null);
                }
            }).Y(new b<String, Observable<? extends StoreState>>() { // from class: com.discord.widgets.chat.input.gifpicker.GifSearchViewModel$Companion$observeQueryState$2

                /* compiled from: GifSearchViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0007\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/models/gifpicker/dto/ModelGif;", "kotlin.jvm.PlatformType", "gifResults", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.input.gifpicker.GifSearchViewModel$Companion$observeQueryState$2$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1<T, R> implements b<List<? extends ModelGif>, Boolean> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Boolean call(List<? extends ModelGif> list) {
                        return call2((List<ModelGif>) list);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Boolean call2(List<ModelGif> list) {
                        return Boolean.valueOf(list != StoreGifPicker.Companion.getSearchResultsLoadingList());
                    }
                }

                public final Observable<? extends GifSearchViewModel.StoreState> call(final String str) {
                    m.checkNotNullExpressionValue(str, "query");
                    if (str.length() == 0) {
                        return new k(new GifSearchViewModel.StoreState.TrendingSearchTermsResults(list));
                    }
                    return Observable.j(storeGifPicker.observeGifsForSearchQuery(str).x(AnonymousClass1.INSTANCE), storeGifPicker.observeSuggestedSearchTerms(str), new Func2<List<? extends ModelGif>, List<? extends String>, GifSearchViewModel.StoreState.SearchResults>() { // from class: com.discord.widgets.chat.input.gifpicker.GifSearchViewModel$Companion$observeQueryState$2.2
                        @Override // rx.functions.Func2
                        public /* bridge */ /* synthetic */ GifSearchViewModel.StoreState.SearchResults call(List<? extends ModelGif> list2, List<? extends String> list3) {
                            return call2((List<ModelGif>) list2, (List<String>) list3);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final GifSearchViewModel.StoreState.SearchResults call2(List<ModelGif> list2, List<String> list3) {
                            m.checkNotNullExpressionValue(list2, "gifResults");
                            m.checkNotNullExpressionValue(list3, "searchTerms");
                            return new GifSearchViewModel.StoreState.SearchResults(list2, list3, list, str);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "searchSubject\n          …          }\n            }");
            return Y;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(final BehaviorSubject<String> behaviorSubject, final StoreAnalytics storeAnalytics, final StoreGifPicker storeGifPicker) {
            Observable Y = storeGifPicker.observeGifTrendingSearchTerms().Y(new b<List<? extends String>, Observable<? extends StoreState>>() { // from class: com.discord.widgets.chat.input.gifpicker.GifSearchViewModel$Companion$observeStoreState$1
                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends GifSearchViewModel.StoreState> call(List<? extends String> list) {
                    return call2((List<String>) list);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends GifSearchViewModel.StoreState> call2(List<String> list) {
                    Observable<? extends GifSearchViewModel.StoreState> observeQueryState;
                    GifSearchViewModel.Companion companion = GifSearchViewModel.Companion;
                    BehaviorSubject behaviorSubject2 = BehaviorSubject.this;
                    StoreAnalytics storeAnalytics2 = storeAnalytics;
                    StoreGifPicker storeGifPicker2 = storeGifPicker;
                    m.checkNotNullExpressionValue(list, "terms");
                    observeQueryState = companion.observeQueryState(behaviorSubject2, storeAnalytics2, storeGifPicker2, list);
                    return observeQueryState;
                }
            });
            m.checkNotNullExpressionValue(Y, "storeGifPicker.observeGi…fPicker, terms)\n        }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GifSearchViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0017\b\u0002\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\b\u0010\tR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007\u0082\u0001\u0002\f\r¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;", "", "", "", "trendingSearchTerms", "Ljava/util/List;", "getTrendingSearchTerms", "()Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;)V", "SearchResults", "TrendingSearchTermsResults", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$TrendingSearchTermsResults;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {
        private final List<String> trendingSearchTerms;

        /* compiled from: GifSearchViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b \u0010!J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0005J\u0012\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\nJL\u0010\u000f\u001a\u00020\u00002\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00022\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00022\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\nJ\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0005R\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\u001c\u0010\u0005R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\nR\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001f\u0010\u0005¨\u0006\""}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;", "", "Lcom/discord/models/gifpicker/dto/ModelGif;", "component1", "()Ljava/util/List;", "", "component2", "component3", "component4", "()Ljava/lang/String;", "gifs", "suggested", "trending", "searchQuery", "copy", "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$SearchResults;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getGifs", "getTrending", "Ljava/lang/String;", "getSearchQuery", "getSuggested", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SearchResults extends StoreState {
            private final List<ModelGif> gifs;
            private final String searchQuery;
            private final List<String> suggested;
            private final List<String> trending;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SearchResults(List<ModelGif> list, List<String> list2, List<String> list3, String str) {
                super(list3, null);
                m.checkNotNullParameter(list, "gifs");
                m.checkNotNullParameter(list2, "suggested");
                m.checkNotNullParameter(list3, "trending");
                this.gifs = list;
                this.suggested = list2;
                this.trending = list3;
                this.searchQuery = str;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ SearchResults copy$default(SearchResults searchResults, List list, List list2, List list3, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = searchResults.gifs;
                }
                if ((i & 2) != 0) {
                    list2 = searchResults.suggested;
                }
                if ((i & 4) != 0) {
                    list3 = searchResults.trending;
                }
                if ((i & 8) != 0) {
                    str = searchResults.searchQuery;
                }
                return searchResults.copy(list, list2, list3, str);
            }

            public final List<ModelGif> component1() {
                return this.gifs;
            }

            public final List<String> component2() {
                return this.suggested;
            }

            public final List<String> component3() {
                return this.trending;
            }

            public final String component4() {
                return this.searchQuery;
            }

            public final SearchResults copy(List<ModelGif> list, List<String> list2, List<String> list3, String str) {
                m.checkNotNullParameter(list, "gifs");
                m.checkNotNullParameter(list2, "suggested");
                m.checkNotNullParameter(list3, "trending");
                return new SearchResults(list, list2, list3, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof SearchResults)) {
                    return false;
                }
                SearchResults searchResults = (SearchResults) obj;
                return m.areEqual(this.gifs, searchResults.gifs) && m.areEqual(this.suggested, searchResults.suggested) && m.areEqual(this.trending, searchResults.trending) && m.areEqual(this.searchQuery, searchResults.searchQuery);
            }

            public final List<ModelGif> getGifs() {
                return this.gifs;
            }

            public final String getSearchQuery() {
                return this.searchQuery;
            }

            public final List<String> getSuggested() {
                return this.suggested;
            }

            public final List<String> getTrending() {
                return this.trending;
            }

            public int hashCode() {
                List<ModelGif> list = this.gifs;
                int i = 0;
                int hashCode = (list != null ? list.hashCode() : 0) * 31;
                List<String> list2 = this.suggested;
                int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
                List<String> list3 = this.trending;
                int hashCode3 = (hashCode2 + (list3 != null ? list3.hashCode() : 0)) * 31;
                String str = this.searchQuery;
                if (str != null) {
                    i = str.hashCode();
                }
                return hashCode3 + i;
            }

            public String toString() {
                StringBuilder R = b.d.b.a.a.R("SearchResults(gifs=");
                R.append(this.gifs);
                R.append(", suggested=");
                R.append(this.suggested);
                R.append(", trending=");
                R.append(this.trending);
                R.append(", searchQuery=");
                return b.d.b.a.a.H(R, this.searchQuery, ")");
            }
        }

        /* compiled from: GifSearchViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0005¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$TrendingSearchTermsResults;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState;", "", "", "component1", "()Ljava/util/List;", "trending", "copy", "(Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$StoreState$TrendingSearchTermsResults;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getTrending", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class TrendingSearchTermsResults extends StoreState {
            private final List<String> trending;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public TrendingSearchTermsResults(List<String> list) {
                super(list, null);
                m.checkNotNullParameter(list, "trending");
                this.trending = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ TrendingSearchTermsResults copy$default(TrendingSearchTermsResults trendingSearchTermsResults, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = trendingSearchTermsResults.trending;
                }
                return trendingSearchTermsResults.copy(list);
            }

            public final List<String> component1() {
                return this.trending;
            }

            public final TrendingSearchTermsResults copy(List<String> list) {
                m.checkNotNullParameter(list, "trending");
                return new TrendingSearchTermsResults(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof TrendingSearchTermsResults) && m.areEqual(this.trending, ((TrendingSearchTermsResults) obj).trending);
                }
                return true;
            }

            public final List<String> getTrending() {
                return this.trending;
            }

            public int hashCode() {
                List<String> list = this.trending;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return b.d.b.a.a.K(b.d.b.a.a.R("TrendingSearchTermsResults(trending="), this.trending, ")");
            }
        }

        private StoreState(List<String> list) {
            this.trendingSearchTerms = list;
        }

        public final List<String> getTrendingSearchTerms() {
            return this.trendingSearchTerms;
        }

        public /* synthetic */ StoreState(List list, DefaultConstructorMarker defaultConstructorMarker) {
            this(list);
        }
    }

    /* compiled from: GifSearchViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "LoadingSearchResults", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$LoadingSearchResults;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: GifSearchViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005R\u001d\u0010\u0019\u001a\u00020\f8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u000e¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;", "", "Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem;", "component1", "()Ljava/util/List;", "adapterItems", "copy", "(Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getAdapterItems", "gifCount$delegate", "Lkotlin/Lazy;", "getGifCount", "gifCount", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final List<GifAdapterItem> adapterItems;
            private final Lazy gifCount$delegate = g.lazy(new GifSearchViewModel$ViewState$Loaded$gifCount$2(this));

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(List<? extends GifAdapterItem> list) {
                super(null);
                m.checkNotNullParameter(list, "adapterItems");
                this.adapterItems = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = loaded.adapterItems;
                }
                return loaded.copy(list);
            }

            public final List<GifAdapterItem> component1() {
                return this.adapterItems;
            }

            public final Loaded copy(List<? extends GifAdapterItem> list) {
                m.checkNotNullParameter(list, "adapterItems");
                return new Loaded(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.adapterItems, ((Loaded) obj).adapterItems);
                }
                return true;
            }

            public final List<GifAdapterItem> getAdapterItems() {
                return this.adapterItems;
            }

            public final int getGifCount() {
                return ((Number) this.gifCount$delegate.getValue()).intValue();
            }

            public int hashCode() {
                List<GifAdapterItem> list = this.adapterItems;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return b.d.b.a.a.K(b.d.b.a.a.R("Loaded(adapterItems="), this.adapterItems, ")");
            }
        }

        /* compiled from: GifSearchViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState$LoadingSearchResults;", "Lcom/discord/widgets/chat/input/gifpicker/GifSearchViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LoadingSearchResults extends ViewState {
            public static final LoadingSearchResults INSTANCE = new LoadingSearchResults();

            private LoadingSearchResults() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GifSearchViewModel(android.content.Context r17, rx.subjects.BehaviorSubject r18, com.discord.stores.StoreGifPicker r19, com.discord.stores.StoreAnalytics r20, com.discord.widgets.chat.MessageManager r21, rx.Observable r22, int r23, kotlin.jvm.internal.DefaultConstructorMarker r24) {
        /*
            r16 = this;
            r0 = r23 & 2
            if (r0 == 0) goto L10
            java.lang.String r0 = ""
            rx.subjects.BehaviorSubject r0 = rx.subjects.BehaviorSubject.l0(r0)
            java.lang.String r1 = "BehaviorSubject.create(\"\")"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            goto L12
        L10:
            r0 = r18
        L12:
            r1 = r23 & 4
            if (r1 == 0) goto L1d
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGifPicker r1 = r1.getGifPicker()
            goto L1f
        L1d:
            r1 = r19
        L1f:
            r2 = r23 & 8
            if (r2 == 0) goto L2a
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreAnalytics r2 = r2.getAnalytics()
            goto L2c
        L2a:
            r2 = r20
        L2c:
            r3 = r23 & 16
            if (r3 == 0) goto L44
            com.discord.widgets.chat.MessageManager r3 = new com.discord.widgets.chat.MessageManager
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 510(0x1fe, float:7.15E-43)
            r15 = 0
            r4 = r3
            r5 = r17
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            goto L46
        L44:
            r3 = r21
        L46:
            r4 = r23 & 32
            if (r4 == 0) goto L51
            com.discord.widgets.chat.input.gifpicker.GifSearchViewModel$Companion r4 = com.discord.widgets.chat.input.gifpicker.GifSearchViewModel.Companion
            rx.Observable r4 = com.discord.widgets.chat.input.gifpicker.GifSearchViewModel.Companion.access$observeStoreState(r4, r0, r2, r1)
            goto L53
        L51:
            r4 = r22
        L53:
            r18 = r16
            r19 = r17
            r20 = r0
            r21 = r1
            r22 = r2
            r23 = r3
            r24 = r4
            r18.<init>(r19, r20, r21, r22, r23, r24)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.gifpicker.GifSearchViewModel.<init>(android.content.Context, rx.subjects.BehaviorSubject, com.discord.stores.StoreGifPicker, com.discord.stores.StoreAnalytics, com.discord.widgets.chat.MessageManager, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        Object obj;
        ArrayList arrayList = new ArrayList();
        List<String> trendingSearchTerms = storeState.getTrendingSearchTerms();
        boolean z2 = storeState instanceof StoreState.SearchResults;
        if (z2) {
            StoreState.SearchResults searchResults = (StoreState.SearchResults) storeState;
            List<ModelGif> gifs = searchResults.getGifs();
            ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(gifs, 10));
            for (ModelGif modelGif : gifs) {
                arrayList2.add(new GifAdapterItem.GifItem(modelGif, searchResults.getSearchQuery()));
            }
            arrayList.addAll(arrayList2);
            List<String> suggested = searchResults.getSuggested();
            boolean z3 = true;
            boolean z4 = arrayList.size() > 0;
            boolean z5 = searchResults.getSuggested().size() > 0;
            String n0 = this.searchSubject.n0();
            m.checkNotNullExpressionValue(n0, "searchSubject.value");
            if (n0.length() <= 0) {
                z3 = false;
            }
            if (z4) {
                obj = new GifAdapterItem.SuggestedTermsItem.SuggestedTermsNonEmptyResults(suggested, R.string.gif_picker_related_search);
            } else if (z3 && !z5) {
                obj = new GifAdapterItem.SuggestedTermsItem.SuggestedTermsEmptyResults(suggested, R.string.no_gif_search_results_without_related_search);
            } else if (!z3 || !z5) {
                obj = new GifAdapterItem.SuggestedTermsItem.SuggestedTermsEmptyResults(suggested, R.string.gif_picker_enter_search);
            } else {
                obj = new GifAdapterItem.SuggestedTermsItem.SuggestedTermsEmptyResults(suggested, R.string.no_gif_search_results_with_related_search);
            }
            arrayList.add(obj);
        } else if (storeState instanceof StoreState.TrendingSearchTermsResults) {
            arrayList.add(new GifAdapterItem.SuggestedTermsItem.SuggestedTermsEmptyResults(trendingSearchTerms, R.string.gif_picker_enter_search));
        }
        ViewState.Loaded loaded = new ViewState.Loaded(arrayList);
        if (z2) {
            this.storeAnalytics.trackSearchResultViewed(SearchType.GIF, loaded.getGifCount(), (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? false : false);
        }
        updateViewState(loaded);
    }

    public final void selectGif(GifAdapterItem.GifItem gifItem) {
        m.checkNotNullParameter(gifItem, "gifItem");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            StoreAnalytics.trackSearchResultSelected$default(this.storeAnalytics, SearchType.GIF, loaded.getGifCount(), null, new Traits.Source(null, null, Traits.Source.Obj.GIF_PICKER, null, null, 27, null), 4, null);
        }
        MessageManager.sendMessage$default(this.messageManager, gifItem.getGif().getTenorGifUrl(), null, null, null, null, false, null, null, null, 510, null);
    }

    public final void setSearchText(String str) {
        m.checkNotNullParameter(str, "searchText");
        boolean z2 = true;
        if (!m.areEqual(this.searchSubject.n0(), str)) {
            if (str.length() <= 0) {
                z2 = false;
            }
            if (z2) {
                updateViewState(ViewState.LoadingSearchResults.INSTANCE);
            }
        }
        this.searchSubject.onNext(str);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GifSearchViewModel(Context context, BehaviorSubject<String> behaviorSubject, StoreGifPicker storeGifPicker, StoreAnalytics storeAnalytics, MessageManager messageManager, Observable<StoreState> observable) {
        super(null);
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(behaviorSubject, "searchSubject");
        m.checkNotNullParameter(storeGifPicker, "storeGifPicker");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(messageManager, "messageManager");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.searchSubject = behaviorSubject;
        this.storeGifPicker = storeGifPicker;
        this.storeAnalytics = storeAnalytics;
        this.messageManager = messageManager;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), GifSearchViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
