package com.discord.widgets.chat.input.gifpicker;

import com.discord.widgets.chat.input.gifpicker.GifAdapterItem;
import com.discord.widgets.chat.input.gifpicker.GifSearchViewModel;
import d0.t.n;
import d0.z.d.o;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: GifSearchViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GifSearchViewModel$ViewState$Loaded$gifCount$2 extends o implements Function0<Integer> {
    public final /* synthetic */ GifSearchViewModel.ViewState.Loaded this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GifSearchViewModel$ViewState$Loaded$gifCount$2(GifSearchViewModel.ViewState.Loaded loaded) {
        super(0);
        this.this$0 = loaded;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.lang.Integer] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4, types: [int] */
    /* JADX WARN: Type inference failed for: r2v5 */
    @Override // kotlin.jvm.functions.Function0
    public final Integer invoke() {
        List<GifAdapterItem> adapterItems = this.this$0.getAdapterItems();
        ?? r2 = 0;
        r2 = 0;
        if (!(adapterItems instanceof Collection) || !adapterItems.isEmpty()) {
            for (GifAdapterItem gifAdapterItem : adapterItems) {
                if ((gifAdapterItem instanceof GifAdapterItem.GifItem) && (r2 = (r2 == true ? 1 : 0) + 1) < 0) {
                    n.throwCountOverflow();
                }
            }
        }
        return r2;
    }
}
