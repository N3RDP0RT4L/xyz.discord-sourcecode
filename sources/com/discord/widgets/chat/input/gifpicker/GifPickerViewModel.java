package com.discord.widgets.chat.input.gifpicker;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.gifpicker.domain.ModelGifCategory;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.gifpicker.GifCategoryItem;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: GifPickerViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\f\rB\u0017\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\b¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;", "Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;)V", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Lrx/Observable;)V", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GifPickerViewModel extends AppViewModel<ViewState> {

    /* compiled from: GifPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\n \u0002*\u0004\u0018\u00010\u00060\u00062\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u00002\u000e\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00040\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "Lcom/discord/models/gifpicker/domain/ModelGifCategory;", "kotlin.jvm.PlatformType", "gifCategories", "", "trendingGifCategoryPreviewUrl", "Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;Ljava/lang/String;)Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.gifpicker.GifPickerViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T1, T2, R> implements Func2<List<? extends ModelGifCategory>, String, StoreState> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        @Override // rx.functions.Func2
        public /* bridge */ /* synthetic */ StoreState call(List<? extends ModelGifCategory> list, String str) {
            return call2((List<ModelGifCategory>) list, str);
        }

        /* renamed from: call  reason: avoid collision after fix types in other method */
        public final StoreState call2(List<ModelGifCategory> list, String str) {
            m.checkNotNullExpressionValue(list, "gifCategories");
            m.checkNotNullExpressionValue(str, "trendingGifCategoryPreviewUrl");
            return new StoreState(list, str);
        }
    }

    /* compiled from: GifPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.gifpicker.GifPickerViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GifPickerViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GifPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\r\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\b¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;", "", "", "Lcom/discord/models/gifpicker/domain/ModelGifCategory;", "component1", "()Ljava/util/List;", "", "component2", "()Ljava/lang/String;", "gifCategories", "trendingGifCategoryPreviewUrl", "copy", "(Ljava/util/List;Ljava/lang/String;)Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$StoreState;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getGifCategories", "Ljava/lang/String;", "getTrendingGifCategoryPreviewUrl", HookHelper.constructorName, "(Ljava/util/List;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final List<ModelGifCategory> gifCategories;
        private final String trendingGifCategoryPreviewUrl;

        public StoreState(List<ModelGifCategory> list, String str) {
            m.checkNotNullParameter(list, "gifCategories");
            m.checkNotNullParameter(str, "trendingGifCategoryPreviewUrl");
            this.gifCategories = list;
            this.trendingGifCategoryPreviewUrl = str;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, List list, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                list = storeState.gifCategories;
            }
            if ((i & 2) != 0) {
                str = storeState.trendingGifCategoryPreviewUrl;
            }
            return storeState.copy(list, str);
        }

        public final List<ModelGifCategory> component1() {
            return this.gifCategories;
        }

        public final String component2() {
            return this.trendingGifCategoryPreviewUrl;
        }

        public final StoreState copy(List<ModelGifCategory> list, String str) {
            m.checkNotNullParameter(list, "gifCategories");
            m.checkNotNullParameter(str, "trendingGifCategoryPreviewUrl");
            return new StoreState(list, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.gifCategories, storeState.gifCategories) && m.areEqual(this.trendingGifCategoryPreviewUrl, storeState.trendingGifCategoryPreviewUrl);
        }

        public final List<ModelGifCategory> getGifCategories() {
            return this.gifCategories;
        }

        public final String getTrendingGifCategoryPreviewUrl() {
            return this.trendingGifCategoryPreviewUrl;
        }

        public int hashCode() {
            List<ModelGifCategory> list = this.gifCategories;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            String str = this.trendingGifCategoryPreviewUrl;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(gifCategories=");
            R.append(this.gifCategories);
            R.append(", trendingGifCategoryPreviewUrl=");
            return a.H(R, this.trendingGifCategoryPreviewUrl, ")");
        }
    }

    /* compiled from: GifPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0014\u0010\u0005R\u0019\u0010\u0015\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0015\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;", "", "", "Lcom/discord/widgets/chat/input/gifpicker/GifCategoryItem;", "component1", "()Ljava/util/List;", "gifCategoryItems", "copy", "(Ljava/util/List;)Lcom/discord/widgets/chat/input/gifpicker/GifPickerViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getGifCategoryItems", "isLoaded", "Z", "()Z", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final List<GifCategoryItem> gifCategoryItems;
        private final boolean isLoaded;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(List<? extends GifCategoryItem> list) {
            m.checkNotNullParameter(list, "gifCategoryItems");
            this.gifCategoryItems = list;
            this.isLoaded = !list.isEmpty();
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ViewState copy$default(ViewState viewState, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                list = viewState.gifCategoryItems;
            }
            return viewState.copy(list);
        }

        public final List<GifCategoryItem> component1() {
            return this.gifCategoryItems;
        }

        public final ViewState copy(List<? extends GifCategoryItem> list) {
            m.checkNotNullParameter(list, "gifCategoryItems");
            return new ViewState(list);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ViewState) && m.areEqual(this.gifCategoryItems, ((ViewState) obj).gifCategoryItems);
            }
            return true;
        }

        public final List<GifCategoryItem> getGifCategoryItems() {
            return this.gifCategoryItems;
        }

        public int hashCode() {
            List<GifCategoryItem> list = this.gifCategoryItems;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        public final boolean isLoaded() {
            return this.isLoaded;
        }

        public String toString() {
            return a.K(a.R("ViewState(gifCategoryItems="), this.gifCategoryItems, ")");
        }
    }

    public GifPickerViewModel() {
        this(null, 1, null);
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GifPickerViewModel(rx.Observable r1, int r2, kotlin.jvm.internal.DefaultConstructorMarker r3) {
        /*
            r0 = this;
            r2 = r2 & 1
            if (r2 == 0) goto L21
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGifPicker r2 = r1.getGifPicker()
            rx.Observable r2 = r2.observeGifCategories()
            com.discord.stores.StoreGifPicker r1 = r1.getGifPicker()
            rx.Observable r1 = r1.observeTrendingGifCategoryPreviewUrl()
            com.discord.widgets.chat.input.gifpicker.GifPickerViewModel$1 r3 = com.discord.widgets.chat.input.gifpicker.GifPickerViewModel.AnonymousClass1.INSTANCE
            rx.Observable r1 = rx.Observable.j(r2, r1, r3)
            java.lang.String r2 = "Observable.combineLatest…egoryPreviewUrl\n    )\n  }"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)
        L21:
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.gifpicker.GifPickerViewModel.<init>(rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        ArrayList arrayList = new ArrayList();
        if (storeState.getTrendingGifCategoryPreviewUrl().length() > 0) {
            arrayList.add(new GifCategoryItem.Trending(storeState.getTrendingGifCategoryPreviewUrl()));
        }
        List<ModelGifCategory> gifCategories = storeState.getGifCategories();
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(gifCategories, 10));
        for (ModelGifCategory modelGifCategory : gifCategories) {
            arrayList2.add(new GifCategoryItem.Standard(modelGifCategory));
        }
        arrayList.addAll(arrayList2);
        updateViewState(new ViewState(arrayList));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GifPickerViewModel(Observable<StoreState> observable) {
        super(new ViewState(n.emptyList()));
        m.checkNotNullParameter(observable, "storeStateObservable");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), GifPickerViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
