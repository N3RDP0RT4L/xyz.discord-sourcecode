package com.discord.widgets.chat.input.gifpicker;

import com.discord.widgets.chat.input.gifpicker.GifAdapterItem;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetGifCategory.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGifCategory$setUpGifRecycler$1 extends k implements Function1<GifAdapterItem.GifItem, Unit> {
    public WidgetGifCategory$setUpGifRecycler$1(WidgetGifCategory widgetGifCategory) {
        super(1, widgetGifCategory, WidgetGifCategory.class, "selectGif", "selectGif(Lcom/discord/widgets/chat/input/gifpicker/GifAdapterItem$GifItem;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GifAdapterItem.GifItem gifItem) {
        invoke2(gifItem);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GifAdapterItem.GifItem gifItem) {
        m.checkNotNullParameter(gifItem, "p1");
        ((WidgetGifCategory) this.receiver).selectGif(gifItem);
    }
}
