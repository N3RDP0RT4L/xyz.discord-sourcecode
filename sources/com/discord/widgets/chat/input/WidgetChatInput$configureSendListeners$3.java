package com.discord.widgets.chat.input;

import com.discord.widgets.chat.input.autocomplete.InputAutocomplete;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.lytefast.flexinput.model.Attachment;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetChatInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\t\u001a\u00020\u00052\u0010\u0010\u0002\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00010\u00002\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "Lcom/lytefast/flexinput/model/Attachment;", "attachmentsRaw", "Lkotlin/Function1;", "", "", "onValidationResult", "invoke", "(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V", "trySend"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInput$configureSendListeners$3 extends o implements Function2<List<? extends Attachment<?>>, Function1<? super Boolean, ? extends Unit>, Unit> {
    public final /* synthetic */ WidgetChatInput$configureSendListeners$1 $sendCommand$1;
    public final /* synthetic */ WidgetChatInput$configureSendListeners$2 $sendMessage$2;
    public final /* synthetic */ WidgetChatInput this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInput$configureSendListeners$3(WidgetChatInput widgetChatInput, WidgetChatInput$configureSendListeners$1 widgetChatInput$configureSendListeners$1, WidgetChatInput$configureSendListeners$2 widgetChatInput$configureSendListeners$2) {
        super(2);
        this.this$0 = widgetChatInput;
        this.$sendCommand$1 = widgetChatInput$configureSendListeners$1;
        this.$sendMessage$2 = widgetChatInput$configureSendListeners$2;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends Attachment<?>> list, Function1<? super Boolean, ? extends Unit> function1) {
        invoke2(list, (Function1<? super Boolean, Unit>) function1);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<? extends Attachment<?>> list, Function1<? super Boolean, Unit> function1) {
        InputAutocomplete inputAutocomplete;
        ChatInputViewModel viewModel;
        m.checkNotNullParameter(list, "attachmentsRaw");
        m.checkNotNullParameter(function1, "onValidationResult");
        inputAutocomplete = this.this$0.autocomplete;
        ApplicationCommandData applicationCommandData = null;
        if (inputAutocomplete != null) {
            applicationCommandData = InputAutocomplete.getApplicationCommandData$default(inputAutocomplete, null, 1, null);
        }
        if (applicationCommandData != null && !applicationCommandData.getValidInputs()) {
            viewModel = this.this$0.getViewModel();
            viewModel.onCommandInputsInvalid();
        } else if (applicationCommandData == null || applicationCommandData.getApplicationCommand().getBuiltIn()) {
            this.$sendMessage$2.invoke2(list, applicationCommandData, function1);
        } else {
            this.$sendCommand$1.invoke(applicationCommandData, list, false, function1);
        }
    }
}
