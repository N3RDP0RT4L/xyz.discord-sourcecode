package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import andhook.lib.xposed.callbacks.XCallback;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.discord.stores.StoreStream;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.view.text.TextWatcher;
import com.discord.widgets.chat.input.MessageDraftsRepo;
import com.lytefast.flexinput.widget.FlexEditText;
import d0.g0.w;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
/* compiled from: WidgetChatInputEditText.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 )2\u00020\u0001:\u0001)B\u0019\u0012\u0006\u0010\u0013\u001a\u00020\u0012\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b'\u0010(J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0004J\u001d\u0010\u000b\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000b\u0010\fJ\r\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\r\u0010\u0004J\r\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u0004J\r\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\"\u0010\b\u001a\u00020\u00078\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u000b\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u0015R\"\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR*\u0010!\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010 8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&¨\u0006*"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInputEditText;", "", "", "setOnTextChangedListener", "()V", "setSoftwareKeyboardSendBehavior", "setHardwareKeyboardSendBehavior", "", "channelId", "", "saveExistingText", "setChannelId", "(JZ)V", "saveText", "clearLastTypingEmission", "", "getText", "()Ljava/lang/String;", "Lcom/lytefast/flexinput/widget/FlexEditText;", "editText", "Lcom/lytefast/flexinput/widget/FlexEditText;", "J", "getChannelId", "()J", "(J)V", "lastTypingEmissionMillis", "Lrx/subjects/Subject;", "emptyTextSubject", "Lrx/subjects/Subject;", "Lcom/discord/widgets/chat/input/MessageDraftsRepo;", "messageDraftsRepo", "Lcom/discord/widgets/chat/input/MessageDraftsRepo;", "Lkotlin/Function0;", "onSendListener", "Lkotlin/jvm/functions/Function0;", "getOnSendListener", "()Lkotlin/jvm/functions/Function0;", "setOnSendListener", "(Lkotlin/jvm/functions/Function0;)V", HookHelper.constructorName, "(Lcom/lytefast/flexinput/widget/FlexEditText;Lcom/discord/widgets/chat/input/MessageDraftsRepo;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInputEditText {
    public static final Companion Companion = new Companion(null);
    private long channelId;
    private final FlexEditText editText;
    private final Subject<Boolean, Boolean> emptyTextSubject;
    private long lastTypingEmissionMillis;
    private final MessageDraftsRepo messageDraftsRepo;
    private Function0<Unit> onSendListener;

    /* compiled from: WidgetChatInputEditText.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInputEditText$Companion;", "", "Landroid/widget/TextView;", "textView", "", "toStringSafe", "(Landroid/widget/TextView;)Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final String toStringSafe(TextView textView) {
            m.checkNotNullParameter(textView, "textView");
            try {
                return textView.getText().toString();
            } catch (Exception unused) {
                return "";
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChatInputEditText(FlexEditText flexEditText, MessageDraftsRepo messageDraftsRepo) {
        m.checkNotNullParameter(flexEditText, "editText");
        m.checkNotNullParameter(messageDraftsRepo, "messageDraftsRepo");
        this.editText = flexEditText;
        this.messageDraftsRepo = messageDraftsRepo;
        BehaviorSubject l0 = BehaviorSubject.l0(Boolean.TRUE);
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(true)");
        this.emptyTextSubject = l0;
        setOnTextChangedListener();
        setSoftwareKeyboardSendBehavior();
        setHardwareKeyboardSendBehavior();
    }

    private final void setHardwareKeyboardSendBehavior() {
        this.editText.setOnKeyListener(new View.OnKeyListener() { // from class: com.discord.widgets.chat.input.WidgetChatInputEditText$setHardwareKeyboardSendBehavior$1
            @Override // android.view.View.OnKeyListener
            public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                Function0<Unit> onSendListener;
                m.checkNotNullParameter(keyEvent, "event");
                boolean z2 = (keyEvent.getFlags() & 2) == 2;
                if ((i == 66) && !z2) {
                    boolean hasModifiers = keyEvent.hasModifiers(1);
                    boolean isShiftEnterToSendEnabled = StoreStream.Companion.getUserSettings().getIsShiftEnterToSendEnabled();
                    if ((isShiftEnterToSendEnabled && hasModifiers) || (!isShiftEnterToSendEnabled && !hasModifiers)) {
                        if (!(WidgetChatInputEditText.this.getOnSendListener() == null || keyEvent.getAction() != 1 || (onSendListener = WidgetChatInputEditText.this.getOnSendListener()) == null)) {
                            onSendListener.invoke();
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private final void setOnTextChangedListener() {
        this.editText.addTextChangedListener(new TextWatcher() { // from class: com.discord.widgets.chat.input.WidgetChatInputEditText$setOnTextChangedListener$1
            private boolean empty = true;

            {
                super(null, null, null, 7, null);
            }

            @Override // com.discord.utilities.view.text.TextWatcher, android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                long j;
                Subject subject;
                m.checkNotNullParameter(editable, "s");
                super.afterTextChanged(editable);
                WidgetChatInputEditText.this.saveText();
                boolean isEmpty = TextUtils.isEmpty(editable);
                if (this.empty != isEmpty) {
                    this.empty = isEmpty;
                    subject = WidgetChatInputEditText.this.emptyTextSubject;
                    subject.onNext(Boolean.valueOf(isEmpty));
                }
                boolean startsWith$default = w.startsWith$default((CharSequence) editable.toString(), (char) MentionUtilsKt.SLASH_CHAR, false, 2, (Object) null);
                j = WidgetChatInputEditText.this.lastTypingEmissionMillis;
                if (j - ClockFactory.get().currentTimeMillis() < ((long) XCallback.PRIORITY_LOWEST) && !isEmpty && !startsWith$default) {
                    WidgetChatInputEditText.this.lastTypingEmissionMillis = ClockFactory.get().currentTimeMillis();
                    StoreStream.Companion.getUsersTyping().setUserTyping(WidgetChatInputEditText.this.getChannelId());
                }
            }
        });
    }

    private final void setSoftwareKeyboardSendBehavior() {
        this.editText.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: com.discord.widgets.chat.input.WidgetChatInputEditText$setSoftwareKeyboardSendBehavior$1
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                int i2 = i & 255;
                if (WidgetChatInputEditText.this.getOnSendListener() == null || i2 != 6) {
                    return false;
                }
                Function0<Unit> onSendListener = WidgetChatInputEditText.this.getOnSendListener();
                if (onSendListener == null) {
                    return true;
                }
                onSendListener.invoke();
                return true;
            }
        });
    }

    public final void clearLastTypingEmission() {
        this.lastTypingEmissionMillis = 0L;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final Function0<Unit> getOnSendListener() {
        return this.onSendListener;
    }

    public final String getText() {
        return Companion.toStringSafe(this.editText);
    }

    public final void saveText() {
        this.messageDraftsRepo.setTextChannelInput(this.channelId, this.editText.getText());
    }

    public final void setChannelId(long j) {
        this.channelId = j;
    }

    public final void setOnSendListener(Function0<Unit> function0) {
        this.onSendListener = function0;
    }

    public final void setChannelId(long j, boolean z2) {
        if (z2) {
            saveText();
        }
        this.channelId = j;
    }

    public /* synthetic */ WidgetChatInputEditText(FlexEditText flexEditText, MessageDraftsRepo messageDraftsRepo, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(flexEditText, (i & 2) != 0 ? MessageDraftsRepo.Provider.INSTANCE.get() : messageDraftsRepo);
    }
}
