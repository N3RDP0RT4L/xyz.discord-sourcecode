package com.discord.widgets.chat.input;

import androidx.core.app.NotificationCompat;
import com.discord.api.user.User;
import com.discord.models.member.GuildMember;
import com.discord.models.user.CoreUser;
import com.discord.stores.StorePendingReplies;
import com.discord.stores.StoreStream;
import com.discord.widgets.chat.input.ChatInputViewModel;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.Map;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: ChatInputViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003 \u0004*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/stores/StorePendingReplies$PendingReply;", "pendingReply", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$StoreState$Loaded$PendingReply;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StorePendingReplies$PendingReply;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatInputViewModel$Companion$getPendingReplyStateObservable$1<T, R> implements b<StorePendingReplies.PendingReply, Observable<? extends ChatInputViewModel.StoreState.Loaded.PendingReply>> {
    public static final ChatInputViewModel$Companion$getPendingReplyStateObservable$1 INSTANCE = new ChatInputViewModel$Companion$getPendingReplyStateObservable$1();

    public final Observable<? extends ChatInputViewModel.StoreState.Loaded.PendingReply> call(final StorePendingReplies.PendingReply pendingReply) {
        Observable<R> observable;
        if (pendingReply == null) {
            return new k(null);
        }
        if (pendingReply.getOriginalMessage().isWebhook()) {
            User author = pendingReply.getOriginalMessage().getAuthor();
            m.checkNotNull(author);
            return new k(new ChatInputViewModel.StoreState.Loaded.PendingReply(pendingReply, new CoreUser(author), null));
        }
        User author2 = pendingReply.getOriginalMessage().getAuthor();
        m.checkNotNull(author2);
        final long i = author2.i();
        Long b2 = pendingReply.getMessageReference().b();
        if (b2 != null) {
            observable = StoreStream.Companion.getGuilds().observeComputed(b2.longValue()).F(new b<Map<Long, ? extends GuildMember>, GuildMember>() { // from class: com.discord.widgets.chat.input.ChatInputViewModel$Companion$getPendingReplyStateObservable$1$$special$$inlined$let$lambda$1
                @Override // j0.k.b
                public /* bridge */ /* synthetic */ GuildMember call(Map<Long, ? extends GuildMember> map) {
                    return call2((Map<Long, GuildMember>) map);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final GuildMember call2(Map<Long, GuildMember> map) {
                    return map.get(Long.valueOf(i));
                }
            }).q();
        } else {
            observable = new k(null);
        }
        return Observable.j(StoreStream.Companion.getUsers().observeUser(i), observable, new Func2<com.discord.models.user.User, GuildMember, ChatInputViewModel.StoreState.Loaded.PendingReply>() { // from class: com.discord.widgets.chat.input.ChatInputViewModel$Companion$getPendingReplyStateObservable$1.1
            public final ChatInputViewModel.StoreState.Loaded.PendingReply call(com.discord.models.user.User user, GuildMember guildMember) {
                return new ChatInputViewModel.StoreState.Loaded.PendingReply(StorePendingReplies.PendingReply.this, user, guildMember);
            }
        });
    }
}
