package com.discord.widgets.chat.input;

import d0.t.n;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInput$configureSendListeners$6 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetChatInput$configureSendListeners$3 $trySend$3;
    public final /* synthetic */ WidgetChatInput this$0;

    /* compiled from: WidgetChatInput.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "validationSucceeded", "", "invoke", "(Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.WidgetChatInput$configureSendListeners$6$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Boolean, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke(bool.booleanValue());
            return Unit.a;
        }

        public final void invoke(boolean z2) {
            if (z2) {
                WidgetChatInput.clearInput$default(WidgetChatInput$configureSendListeners$6.this.this$0, null, false, 3, null);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInput$configureSendListeners$6(WidgetChatInput widgetChatInput, WidgetChatInput$configureSendListeners$3 widgetChatInput$configureSendListeners$3) {
        super(0);
        this.this$0 = widgetChatInput;
        this.$trySend$3 = widgetChatInput$configureSendListeners$3;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.$trySend$3.invoke2(n.emptyList(), (Function1<? super Boolean, Unit>) new AnonymousClass1());
    }
}
