package com.discord.widgets.chat.input;

import android.content.Context;
import com.discord.stores.StoreSlowMode;
import com.discord.stores.StoreStream;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPIAbortMessages;
import com.discord.widgets.chat.input.ChatInputViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: ChatInputViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatInputViewModel$createAndGotoThread$3 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ ChatInputViewModel.ViewState.Loaded $loadedViewState;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChatInputViewModel$createAndGotoThread$3(ChatInputViewModel.ViewState.Loaded loaded, Context context) {
        super(1);
        this.$loadedViewState = loaded;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        m.checkNotNullParameter(error, "error");
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        Integer abortCodeMessageResId = RestAPIAbortMessages.getAbortCodeMessageResId(response.getCode());
        Error.Response response2 = error.getResponse();
        m.checkNotNullExpressionValue(response2, "error.response");
        if (response2.getCode() == 20016) {
            Error.Response response3 = error.getResponse();
            m.checkNotNullExpressionValue(response3, "error.response");
            Long retryAfterMs = response3.getRetryAfterMs();
            if (retryAfterMs == null) {
                retryAfterMs = 0L;
            }
            m.checkNotNullExpressionValue(retryAfterMs, "error.response.retryAfterMs ?: 0");
            StoreStream.Companion.getSlowMode().onCooldown(this.$loadedViewState.getChannelId(), retryAfterMs.longValue(), StoreSlowMode.Type.ThreadCreate.INSTANCE);
        }
        b.a.d.m.g(this.$context, abortCodeMessageResId != null ? abortCodeMessageResId.intValue() : R.string.network_error_bad_request, 0, null, 12);
    }
}
