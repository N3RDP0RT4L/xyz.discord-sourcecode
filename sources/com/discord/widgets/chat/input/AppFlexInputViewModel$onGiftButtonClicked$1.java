package com.discord.widgets.chat.input;

import androidx.appcompat.widget.ActivityChooserModel;
import androidx.fragment.app.FragmentActivity;
import com.discord.api.channel.Channel;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.Traits;
import com.discord.widgets.settings.premium.WidgetSettingsGifting;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: AppFlexInputViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroidx/fragment/app/FragmentActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "", "invoke", "(Landroidx/fragment/app/FragmentActivity;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AppFlexInputViewModel$onGiftButtonClicked$1 extends o implements Function1<FragmentActivity, Boolean> {
    public static final AppFlexInputViewModel$onGiftButtonClicked$1 INSTANCE = new AppFlexInputViewModel$onGiftButtonClicked$1();

    public AppFlexInputViewModel$onGiftButtonClicked$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(FragmentActivity fragmentActivity) {
        return Boolean.valueOf(invoke2(fragmentActivity));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(FragmentActivity fragmentActivity) {
        m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        StoreStream.Companion companion = StoreStream.Companion;
        Channel findChannelById = companion.getChannels().findChannelById(companion.getChannelsSelected().getId());
        WidgetSettingsGifting.Companion.launch(fragmentActivity, new Traits.Location((findChannelById == null || findChannelById.f() != 0) ? Traits.Location.Page.GUILD_CHANNEL : "DM Channel", Traits.Location.Section.CHANNEL_TEXT_AREA, "Button Icon", "gift", null, 16, null));
        StoreNotices.markSeen$default(companion.getNotices(), "CHAT_GIFTING_NOTICE", 0L, 2, null);
        return true;
    }
}
