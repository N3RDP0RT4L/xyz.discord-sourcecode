package com.discord.widgets.chat.input.emoji;

import com.discord.widgets.chat.input.emoji.EmojiPickerViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetEmojiPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;", "viewState", "", "invoke", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEmojiPicker$onViewBoundOrOnResume$1 extends o implements Function1<EmojiPickerViewModel.ViewState, Unit> {
    public final /* synthetic */ WidgetEmojiPicker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEmojiPicker$onViewBoundOrOnResume$1(WidgetEmojiPicker widgetEmojiPicker) {
        super(1);
        this.this$0 = widgetEmojiPicker;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(EmojiPickerViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(EmojiPickerViewModel.ViewState viewState) {
        m.checkNotNullParameter(viewState, "viewState");
        this.this$0.configureUI(viewState);
    }
}
