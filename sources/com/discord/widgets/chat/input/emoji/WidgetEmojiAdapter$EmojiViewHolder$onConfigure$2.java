package com.discord.widgets.chat.input.emoji;

import android.view.View;
import androidx.fragment.app.FragmentManager;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter;
import com.discord.widgets.emoji.WidgetEmojiSheet;
import d0.k;
import d0.l;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetEmojiAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEmojiAdapter$EmojiViewHolder$onConfigure$2 extends o implements Function1<View, Unit> {
    public final /* synthetic */ WidgetEmojiAdapter.EmojiItem $emojiItem;
    public final /* synthetic */ WidgetEmojiAdapter.EmojiViewHolder this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEmojiAdapter$EmojiViewHolder$onConfigure$2(WidgetEmojiAdapter.EmojiViewHolder emojiViewHolder, WidgetEmojiAdapter.EmojiItem emojiItem) {
        super(1);
        this.this$0 = emojiViewHolder;
        this.$emojiItem = emojiItem;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        Object obj;
        Function0 function0;
        FragmentManager fragmentManager;
        Emoji emoji;
        m.checkNotNullParameter(view, "it");
        try {
            k.a aVar = k.j;
            emoji = this.$emojiItem.getEmoji();
        } catch (Throwable th) {
            k.a aVar2 = k.j;
            obj = k.m73constructorimpl(l.createFailure(th));
        }
        if (emoji != null) {
            obj = k.m73constructorimpl(EmojiNode.Companion.generateEmojiIdAndType((ModelEmojiCustom) emoji));
            if (k.m75exceptionOrNullimpl(obj) != null) {
                try {
                    k.a aVar3 = k.j;
                    Emoji emoji2 = this.$emojiItem.getEmoji();
                    if (emoji2 != null) {
                        obj = k.m73constructorimpl(EmojiNode.Companion.generateEmojiIdAndType((ModelEmojiUnicode) emoji2));
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type com.discord.models.domain.emoji.ModelEmojiUnicode");
                    }
                } catch (Throwable th2) {
                    k.a aVar4 = k.j;
                    obj = k.m73constructorimpl(l.createFailure(th2));
                }
            }
            if (k.m78isSuccessimpl(obj)) {
                function0 = WidgetEmojiAdapter.EmojiViewHolder.access$getAdapter$p(this.this$0).hideKeyboard;
                function0.invoke();
                WidgetEmojiSheet.Companion companion = WidgetEmojiSheet.Companion;
                fragmentManager = WidgetEmojiAdapter.EmojiViewHolder.access$getAdapter$p(this.this$0).fragmentManager;
                companion.show(fragmentManager, (EmojiNode.EmojiIdAndType) obj);
                return;
            }
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type com.discord.models.domain.emoji.ModelEmojiCustom");
    }
}
