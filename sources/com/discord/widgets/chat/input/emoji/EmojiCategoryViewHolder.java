package com.discord.widgets.chat.input.emoji;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.DrawableRes;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.EmojiCategoryItemGuildBinding;
import com.discord.databinding.EmojiCategoryItemStandardBinding;
import com.discord.models.domain.emoji.EmojiCategory;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.widgets.chat.input.emoji.EmojiCategoryItem;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: EmojiCategoryViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00062\u00020\u0001:\u0003\u0006\u0007\bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005\u0082\u0001\u0002\t\n¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;)V", "Companion", "Guild", "Standard", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class EmojiCategoryViewHolder extends RecyclerView.ViewHolder {
    public static final Companion Companion = new Companion(null);

    /* compiled from: EmojiCategoryViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Companion;", "", "Lcom/discord/models/domain/emoji/EmojiCategory;", "emojiCategory", "", "getCategoryIconResId", "(Lcom/discord/models/domain/emoji/EmojiCategory;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                EmojiCategory.values();
                int[] iArr = new int[11];
                $EnumSwitchMapping$0 = iArr;
                iArr[EmojiCategory.FAVORITE.ordinal()] = 1;
                iArr[EmojiCategory.PEOPLE.ordinal()] = 2;
                iArr[EmojiCategory.NATURE.ordinal()] = 3;
                iArr[EmojiCategory.FOOD.ordinal()] = 4;
                iArr[EmojiCategory.ACTIVITY.ordinal()] = 5;
                iArr[EmojiCategory.TRAVEL.ordinal()] = 6;
                iArr[EmojiCategory.OBJECTS.ordinal()] = 7;
                iArr[EmojiCategory.SYMBOLS.ordinal()] = 8;
                iArr[EmojiCategory.FLAGS.ordinal()] = 9;
                iArr[EmojiCategory.RECENT.ordinal()] = 10;
                iArr[EmojiCategory.CUSTOM.ordinal()] = 11;
            }
        }

        private Companion() {
        }

        @DrawableRes
        public final int getCategoryIconResId(EmojiCategory emojiCategory) {
            m.checkNotNullParameter(emojiCategory, "emojiCategory");
            switch (emojiCategory.ordinal()) {
                case 0:
                    return R.drawable.ic_emoji_picker_category_favorites_star;
                case 1:
                    return R.drawable.ic_emoji_picker_category_recent;
                case 2:
                default:
                    return R.drawable.ic_emoji_picker_category_custom;
                case 3:
                    return R.drawable.ic_emoji_picker_category_people;
                case 4:
                    return R.drawable.ic_emoji_picker_category_nature;
                case 5:
                    return R.drawable.ic_emoji_picker_category_food;
                case 6:
                    return R.drawable.ic_emoji_picker_category_activity;
                case 7:
                    return R.drawable.ic_emoji_picker_category_travel;
                case 8:
                    return R.drawable.ic_emoji_picker_category_objects;
                case 9:
                    return R.drawable.ic_emoji_picker_category_symbols;
                case 10:
                    return R.drawable.ic_emoji_picker_category_flags;
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EmojiCategoryViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\r\u0010\u000eJ)\u0010\b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Guild;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$GuildItem;", "guildCategoryItem", "Lkotlin/Function1;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;", "", "onCategoryClicked", "configure", "(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$GuildItem;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/databinding/EmojiCategoryItemGuildBinding;", "binding", "Lcom/discord/databinding/EmojiCategoryItemGuildBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/EmojiCategoryItemGuildBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Guild extends EmojiCategoryViewHolder {
        private final EmojiCategoryItemGuildBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public Guild(com.discord.databinding.EmojiCategoryItemGuildBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.FrameLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.emoji.EmojiCategoryViewHolder.Guild.<init>(com.discord.databinding.EmojiCategoryItemGuildBinding):void");
        }

        public final void configure(final EmojiCategoryItem.GuildItem guildItem, final Function1<? super EmojiCategoryItem, Unit> function1) {
            m.checkNotNullParameter(guildItem, "guildCategoryItem");
            m.checkNotNullParameter(function1, "onCategoryClicked");
            this.binding.f2087b.updateView(guildItem.getGuild());
            View view = this.binding.c.f160b;
            m.checkNotNullExpressionValue(view, "binding.overline.express…CategorySelectionOverline");
            view.setVisibility(guildItem.isSelected() ? 0 : 8);
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.emoji.EmojiCategoryViewHolder$Guild$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    Function1.this.invoke(guildItem);
                }
            });
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            view2.setContentDescription(guildItem.getGuild().getName());
        }
    }

    /* compiled from: EmojiCategoryViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\r\u0010\u000eJ)\u0010\b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder$Standard;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryViewHolder;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;", "standardCategoryItem", "Lkotlin/Function1;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;", "", "onCategoryClicked", "configure", "(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/databinding/EmojiCategoryItemStandardBinding;", "binding", "Lcom/discord/databinding/EmojiCategoryItemStandardBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/EmojiCategoryItemStandardBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Standard extends EmojiCategoryViewHolder {
        private final EmojiCategoryItemStandardBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public Standard(com.discord.databinding.EmojiCategoryItemStandardBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.FrameLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.emoji.EmojiCategoryViewHolder.Standard.<init>(com.discord.databinding.EmojiCategoryItemStandardBinding):void");
        }

        public final void configure(final EmojiCategoryItem.StandardItem standardItem, final Function1<? super EmojiCategoryItem, Unit> function1) {
            int i;
            m.checkNotNullParameter(standardItem, "standardCategoryItem");
            m.checkNotNullParameter(function1, "onCategoryClicked");
            EmojiCategory emojiCategory = standardItem.getEmojiCategory();
            int categoryIconResId = EmojiCategoryViewHolder.Companion.getCategoryIconResId(emojiCategory);
            ImageView imageView = this.binding.f2088b;
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            imageView.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), categoryIconResId, null));
            boolean isSelected = standardItem.isSelected();
            ImageView imageView2 = this.binding.f2088b;
            m.checkNotNullExpressionValue(imageView2, "binding.emojiCategoryItemStandardIcon");
            imageView2.setSelected(isSelected);
            View view2 = this.binding.c.f160b;
            m.checkNotNullExpressionValue(view2, "binding.overline.express…CategorySelectionOverline");
            view2.setVisibility(isSelected ? 0 : 8);
            if (isSelected) {
                ImageView imageView3 = this.binding.f2088b;
                m.checkNotNullExpressionValue(imageView3, "binding.emojiCategoryItemStandardIcon");
                i = ColorCompat.getThemedColor(imageView3, (int) R.attr.colorInteractiveActive);
            } else {
                ImageView imageView4 = this.binding.f2088b;
                m.checkNotNullExpressionValue(imageView4, "binding.emojiCategoryItemStandardIcon");
                i = ColorCompat.getThemedColor(imageView4, (int) R.attr.colorInteractiveNormal);
            }
            ImageView imageView5 = this.binding.f2088b;
            m.checkNotNullExpressionValue(imageView5, "binding.emojiCategoryItemStandardIcon");
            ColorCompatKt.tintWithColor(imageView5, i);
            ImageView imageView6 = this.binding.f2088b;
            m.checkNotNullExpressionValue(imageView6, "binding.emojiCategoryItemStandardIcon");
            imageView6.setAlpha(isSelected ? 1.0f : 0.5f);
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.emoji.EmojiCategoryViewHolder$Standard$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    Function1.this.invoke(standardItem);
                }
            });
            View view3 = this.itemView;
            m.checkNotNullExpressionValue(view3, "itemView");
            view3.setContentDescription(emojiCategory.name());
        }
    }

    private EmojiCategoryViewHolder(View view) {
        super(view);
    }

    public /* synthetic */ EmojiCategoryViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }
}
