package com.discord.widgets.chat.input.emoji;

import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: EmojiCategoryAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;", "items", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EmojiCategoryAdapter$setItems$1 extends o implements Function1<List<? extends EmojiCategoryItem>, Unit> {
    public final /* synthetic */ EmojiCategoryAdapter this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EmojiCategoryAdapter$setItems$1(EmojiCategoryAdapter emojiCategoryAdapter) {
        super(1);
        this.this$0 = emojiCategoryAdapter;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends EmojiCategoryItem> list) {
        invoke2(list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<? extends EmojiCategoryItem> list) {
        Function1 function1;
        m.checkNotNullParameter(list, "items");
        this.this$0.items = list;
        Iterator<? extends EmojiCategoryItem> it = list.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (it.next().isSelected()) {
                break;
            } else {
                i++;
            }
        }
        if (i != -1) {
            function1 = this.this$0.onSelectedItemAdapterPositionUpdated;
            function1.invoke(Integer.valueOf(i));
        }
    }
}
