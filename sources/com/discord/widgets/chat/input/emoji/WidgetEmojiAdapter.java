package com.discord.widgets.chat.input.emoji;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.databinding.EmojiPickerEmojiItemBinding;
import com.discord.databinding.EmojiPickerPremiumUpsellBinding;
import com.discord.databinding.ExpressionPickerHeaderItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.EmojiCategory;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreStream;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.view.extensions.ImageViewExtensionsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter;
import com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.d0.f;
import d0.k;
import d0.l;
import d0.t.u;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetEmojiAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 +2\u00020\u0001:\u0007+,-./01B;\u0012\u0006\u0010(\u001a\u00020'\u0012\u0006\u0010#\u001a\u00020\"\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019\u0012\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019¢\u0006\u0004\b)\u0010*J+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u0001H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u00020\u00048\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u001c\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u001c\u0010\u001e\u001a\u00020\u001d8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u001c\u0010%\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010\u001cR\u0016\u0010&\u001a\u00020\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010\u0013¨\u00062"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "isHeader", "(I)Z", "adapter", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;", "createStickyHeaderViewHolder", "(Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter;)Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;", "numColumns", "I", "getNumColumns", "()I", "Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;", "onEmojiSelectedListener", "Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;", "Lkotlin/Function0;", "", "onGetPremiumCtaClicked", "Lkotlin/jvm/functions/Function0;", "Landroidx/recyclerview/widget/GridLayoutManager;", "layoutManager", "Landroidx/recyclerview/widget/GridLayoutManager;", "getLayoutManager", "()Landroidx/recyclerview/widget/GridLayoutManager;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "hideKeyboard", "emojiSizePx", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "Companion", "EmojiItem", "EmojiViewHolder", "HeaderItem", "HeaderViewHolder", "PremiumEmojiUpsellViewHolder", "UpsellItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEmojiAdapter extends WidgetExpressionPickerAdapter {
    public static final Companion Companion = new Companion(null);
    private static final int DEFAULT_NUM_COLUMNS = 8;
    private static final int ITEM_TYPE_EMOJI = 1;
    private static final int ITEM_TYPE_PREMIUM_UPSELL = 2;
    private static final int MAX_EMOJI_SIZE_PX = 64;
    private final int emojiSizePx;
    private final FragmentManager fragmentManager;
    private final Function0<Unit> hideKeyboard;
    private final GridLayoutManager layoutManager;
    private final int numColumns;
    private final OnEmojiSelectedListener onEmojiSelectedListener;
    private final Function0<Unit> onGetPremiumCtaClicked;

    /* compiled from: WidgetEmojiAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$Companion;", "", "", "DEFAULT_NUM_COLUMNS", "I", "ITEM_TYPE_EMOJI", "ITEM_TYPE_PREMIUM_UPSELL", "MAX_EMOJI_SIZE_PX", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetEmojiAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Lcom/discord/databinding/EmojiPickerEmojiItemBinding;", "binding", "Lcom/discord/databinding/EmojiPickerEmojiItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EmojiViewHolder extends MGRecyclerViewHolder<WidgetEmojiAdapter, MGRecyclerDataPayload> {
        private final EmojiPickerEmojiItemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public EmojiViewHolder(WidgetEmojiAdapter widgetEmojiAdapter) {
            super((int) R.layout.emoji_picker_emoji_item, widgetEmojiAdapter);
            m.checkNotNullParameter(widgetEmojiAdapter, "adapter");
            View view = this.itemView;
            Objects.requireNonNull(view, "rootView");
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view;
            EmojiPickerEmojiItemBinding emojiPickerEmojiItemBinding = new EmojiPickerEmojiItemBinding(simpleDraweeView, simpleDraweeView);
            m.checkNotNullExpressionValue(emojiPickerEmojiItemBinding, "EmojiPickerEmojiItemBinding.bind(itemView)");
            this.binding = emojiPickerEmojiItemBinding;
        }

        public static final /* synthetic */ WidgetEmojiAdapter access$getAdapter$p(EmojiViewHolder emojiViewHolder) {
            return (WidgetEmojiAdapter) emojiViewHolder.adapter;
        }

        public void onConfigure(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
            m.checkNotNullParameter(mGRecyclerDataPayload, "data");
            super.onConfigure(i, (int) mGRecyclerDataPayload);
            if (!(mGRecyclerDataPayload instanceof EmojiItem)) {
                mGRecyclerDataPayload = null;
            }
            EmojiItem emojiItem = (EmojiItem) mGRecyclerDataPayload;
            if (emojiItem != null) {
                final Emoji emoji = emojiItem.getEmoji();
                int coerceAtMost = f.coerceAtMost(IconUtils.getMediaProxySize(((WidgetEmojiAdapter) this.adapter).emojiSizePx), 64);
                boolean allowEmojisToAnimate = emojiItem.getAllowEmojisToAnimate();
                SimpleDraweeView simpleDraweeView = this.binding.f2089b;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.emojiItemDraweeview");
                String imageUri = emoji.getImageUri(allowEmojisToAnimate, coerceAtMost, simpleDraweeView.getContext());
                SimpleDraweeView simpleDraweeView2 = this.binding.f2089b;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.emojiItemDraweeview");
                MGImages.setImage$default(simpleDraweeView2, imageUri, 0, 0, true, null, null, 108, null);
                SimpleDraweeView simpleDraweeView3 = this.binding.f2089b;
                m.checkNotNullExpressionValue(simpleDraweeView3, "binding.emojiItemDraweeview");
                ImageViewExtensionsKt.setGrayscale(simpleDraweeView3, !emoji.isUsable() || !emoji.isAvailable());
                SimpleDraweeView simpleDraweeView4 = this.binding.f2089b;
                m.checkNotNullExpressionValue(simpleDraweeView4, "binding.emojiItemDraweeview");
                simpleDraweeView4.setImageAlpha((!emoji.isUsable() || !emoji.isAvailable()) ? 100 : 255);
                SimpleDraweeView simpleDraweeView5 = this.binding.f2089b;
                m.checkNotNullExpressionValue(simpleDraweeView5, "binding.emojiItemDraweeview");
                simpleDraweeView5.setContentDescription(emoji.getFirstName());
                this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter$EmojiViewHolder$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        OnEmojiSelectedListener onEmojiSelectedListener;
                        EmojiPickerEmojiItemBinding emojiPickerEmojiItemBinding;
                        if (emoji.isUsable() && emoji.isAvailable()) {
                            StoreStream.Companion.getEmojis().onEmojiUsed(emoji);
                        }
                        onEmojiSelectedListener = WidgetEmojiAdapter.EmojiViewHolder.access$getAdapter$p(WidgetEmojiAdapter.EmojiViewHolder.this).onEmojiSelectedListener;
                        onEmojiSelectedListener.onEmojiSelected(emoji);
                        WidgetEmojiAdapter.EmojiViewHolder emojiViewHolder = WidgetEmojiAdapter.EmojiViewHolder.this;
                        try {
                            k.a aVar = k.j;
                            emojiPickerEmojiItemBinding = emojiViewHolder.binding;
                            k.m73constructorimpl(Boolean.valueOf(emojiPickerEmojiItemBinding.f2089b.performHapticFeedback(3)));
                        } catch (Throwable th) {
                            k.a aVar2 = k.j;
                            k.m73constructorimpl(l.createFailure(th));
                        }
                    }
                });
                SimpleDraweeView simpleDraweeView6 = this.binding.a;
                m.checkNotNullExpressionValue(simpleDraweeView6, "binding.root");
                ViewExtensions.setOnLongClickListenerConsumeClick(simpleDraweeView6, new WidgetEmojiAdapter$EmojiViewHolder$onConfigure$2(this, emojiItem));
            }
        }
    }

    /* compiled from: WidgetEmojiAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0003\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "type", "I", "getType", "()I", HookHelper.constructorName, "()V", "GuildHeaderItem", "StandardHeaderItem", "StringHeaderItem", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$GuildHeaderItem;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StandardHeaderItem;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StringHeaderItem;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class HeaderItem implements MGRecyclerDataPayload {
        private final int type;

        /* compiled from: WidgetEmojiAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017B\u0011\b\u0016\u0012\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u0016\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0007\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0013\u001a\u0004\b\u0015\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$GuildHeaderItem;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;", "", "component1", "()Ljava/lang/String;", "component2", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "key", "copy", "(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$GuildHeaderItem;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getKey", "getText", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "Lcom/discord/models/guild/Guild;", "guild", "(Lcom/discord/models/guild/Guild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class GuildHeaderItem extends HeaderItem {
            private final String key;
            private final String text;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public GuildHeaderItem(String str, String str2) {
                super(null);
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                m.checkNotNullParameter(str2, "key");
                this.text = str;
                this.key = str2;
            }

            public static /* synthetic */ GuildHeaderItem copy$default(GuildHeaderItem guildHeaderItem, String str, String str2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = guildHeaderItem.text;
                }
                if ((i & 2) != 0) {
                    str2 = guildHeaderItem.getKey();
                }
                return guildHeaderItem.copy(str, str2);
            }

            public final String component1() {
                return this.text;
            }

            public final String component2() {
                return getKey();
            }

            public final GuildHeaderItem copy(String str, String str2) {
                m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                m.checkNotNullParameter(str2, "key");
                return new GuildHeaderItem(str, str2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof GuildHeaderItem)) {
                    return false;
                }
                GuildHeaderItem guildHeaderItem = (GuildHeaderItem) obj;
                return m.areEqual(this.text, guildHeaderItem.text) && m.areEqual(getKey(), guildHeaderItem.getKey());
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final String getText() {
                return this.text;
            }

            public int hashCode() {
                String str = this.text;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                String key = getKey();
                if (key != null) {
                    i = key.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("GuildHeaderItem(text=");
                R.append(this.text);
                R.append(", key=");
                R.append(getKey());
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
            public GuildHeaderItem(Guild guild) {
                this(guild.getName(), String.valueOf(guild.getId()));
                m.checkNotNullParameter(guild, "guild");
            }
        }

        /* compiled from: WidgetEmojiAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\nR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StandardHeaderItem;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;", "Lcom/discord/models/domain/emoji/EmojiCategory;", "component1", "()Lcom/discord/models/domain/emoji/EmojiCategory;", "emojiCategory", "copy", "(Lcom/discord/models/domain/emoji/EmojiCategory;)Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StandardHeaderItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/domain/emoji/EmojiCategory;", "getEmojiCategory", HookHelper.constructorName, "(Lcom/discord/models/domain/emoji/EmojiCategory;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class StandardHeaderItem extends HeaderItem {
            private final EmojiCategory emojiCategory;
            private final String key;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public StandardHeaderItem(EmojiCategory emojiCategory) {
                super(null);
                m.checkNotNullParameter(emojiCategory, "emojiCategory");
                this.emojiCategory = emojiCategory;
                this.key = emojiCategory.name();
            }

            public static /* synthetic */ StandardHeaderItem copy$default(StandardHeaderItem standardHeaderItem, EmojiCategory emojiCategory, int i, Object obj) {
                if ((i & 1) != 0) {
                    emojiCategory = standardHeaderItem.emojiCategory;
                }
                return standardHeaderItem.copy(emojiCategory);
            }

            public final EmojiCategory component1() {
                return this.emojiCategory;
            }

            public final StandardHeaderItem copy(EmojiCategory emojiCategory) {
                m.checkNotNullParameter(emojiCategory, "emojiCategory");
                return new StandardHeaderItem(emojiCategory);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof StandardHeaderItem) && m.areEqual(this.emojiCategory, ((StandardHeaderItem) obj).emojiCategory);
                }
                return true;
            }

            public final EmojiCategory getEmojiCategory() {
                return this.emojiCategory;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public int hashCode() {
                EmojiCategory emojiCategory = this.emojiCategory;
                if (emojiCategory != null) {
                    return emojiCategory.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("StandardHeaderItem(emojiCategory=");
                R.append(this.emojiCategory);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetEmojiAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004R\u001c\u0010\u0013\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\n¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StringHeaderItem;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;", "", "component1", "()I", "stringRes", "copy", "(I)Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem$StringHeaderItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getStringRes", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class StringHeaderItem extends HeaderItem {
            private final String key;
            private final int stringRes;

            public StringHeaderItem(@StringRes int i) {
                super(null);
                this.stringRes = i;
                this.key = String.valueOf(i);
            }

            public static /* synthetic */ StringHeaderItem copy$default(StringHeaderItem stringHeaderItem, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = stringHeaderItem.stringRes;
                }
                return stringHeaderItem.copy(i);
            }

            public final int component1() {
                return this.stringRes;
            }

            public final StringHeaderItem copy(@StringRes int i) {
                return new StringHeaderItem(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof StringHeaderItem) && this.stringRes == ((StringHeaderItem) obj).stringRes;
                }
                return true;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final int getStringRes() {
                return this.stringRes;
            }

            public int hashCode() {
                return this.stringRes;
            }

            public String toString() {
                return a.A(a.R("StringHeaderItem(stringRes="), this.stringRes, ")");
            }
        }

        private HeaderItem() {
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public /* synthetic */ HeaderItem(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetEmojiAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001c2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004:\u0001\u001cB\u000f\u0012\u0006\u0010\u0019\u001a\u00020\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u000e\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u000e\u0010\nR\"\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/widgets/chat/input/expression/WidgetExpressionPickerAdapter$StickyHeaderViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "bind", "(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "Landroid/view/View;", "getItemView", "()Landroid/view/View;", "onConfigure", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;", "boundItem", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;", "getBoundItem", "()Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;", "setBoundItem", "(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderItem;)V", "Lcom/discord/databinding/ExpressionPickerHeaderItemBinding;", "binding", "Lcom/discord/databinding/ExpressionPickerHeaderItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class HeaderViewHolder extends MGRecyclerViewHolder<WidgetEmojiAdapter, MGRecyclerDataPayload> implements WidgetExpressionPickerAdapter.StickyHeaderViewHolder {
        public static final Companion Companion = new Companion(null);
        private final ExpressionPickerHeaderItemBinding binding;
        public HeaderItem boundItem;

        /* compiled from: WidgetEmojiAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$HeaderViewHolder$Companion;", "", "Lcom/discord/models/domain/emoji/EmojiCategory;", "emojiCategory", "", "getCategoryString", "(Lcom/discord/models/domain/emoji/EmojiCategory;)I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {

            @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public final /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    EmojiCategory.values();
                    int[] iArr = new int[11];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[EmojiCategory.FAVORITE.ordinal()] = 1;
                    iArr[EmojiCategory.PEOPLE.ordinal()] = 2;
                    iArr[EmojiCategory.NATURE.ordinal()] = 3;
                    iArr[EmojiCategory.FOOD.ordinal()] = 4;
                    iArr[EmojiCategory.ACTIVITY.ordinal()] = 5;
                    iArr[EmojiCategory.TRAVEL.ordinal()] = 6;
                    iArr[EmojiCategory.OBJECTS.ordinal()] = 7;
                    iArr[EmojiCategory.SYMBOLS.ordinal()] = 8;
                    iArr[EmojiCategory.FLAGS.ordinal()] = 9;
                    iArr[EmojiCategory.CUSTOM.ordinal()] = 10;
                    iArr[EmojiCategory.RECENT.ordinal()] = 11;
                }
            }

            private Companion() {
            }

            @StringRes
            public final int getCategoryString(EmojiCategory emojiCategory) {
                m.checkNotNullParameter(emojiCategory, "emojiCategory");
                switch (emojiCategory.ordinal()) {
                    case 0:
                        return R.string.category_favorite;
                    case 1:
                        return R.string.emoji_category_recent;
                    case 2:
                        return R.string.emoji_category_custom;
                    case 3:
                        return R.string.emoji_category_people;
                    case 4:
                        return R.string.emoji_category_nature;
                    case 5:
                        return R.string.emoji_category_food;
                    case 6:
                        return R.string.emoji_category_activity;
                    case 7:
                        return R.string.emoji_category_travel;
                    case 8:
                        return R.string.emoji_category_objects;
                    case 9:
                        return R.string.emoji_category_symbols;
                    case 10:
                        return R.string.emoji_category_flags;
                    default:
                        throw new NoWhenBranchMatchedException();
                }
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public HeaderViewHolder(WidgetEmojiAdapter widgetEmojiAdapter) {
            super((int) R.layout.expression_picker_header_item, widgetEmojiAdapter);
            m.checkNotNullParameter(widgetEmojiAdapter, "adapter");
            View view = this.itemView;
            Objects.requireNonNull(view, "rootView");
            TextView textView = (TextView) view;
            ExpressionPickerHeaderItemBinding expressionPickerHeaderItemBinding = new ExpressionPickerHeaderItemBinding(textView, textView);
            m.checkNotNullExpressionValue(expressionPickerHeaderItemBinding, "ExpressionPickerHeaderItemBinding.bind(itemView)");
            this.binding = expressionPickerHeaderItemBinding;
        }

        @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter.StickyHeaderViewHolder
        public void bind(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
            m.checkNotNullParameter(mGRecyclerDataPayload, "data");
            onConfigure(i, mGRecyclerDataPayload);
        }

        public final HeaderItem getBoundItem() {
            HeaderItem headerItem = this.boundItem;
            if (headerItem == null) {
                m.throwUninitializedPropertyAccessException("boundItem");
            }
            return headerItem;
        }

        @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter.StickyHeaderViewHolder
        public View getItemView() {
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            return view;
        }

        public final void setBoundItem(HeaderItem headerItem) {
            m.checkNotNullParameter(headerItem, "<set-?>");
            this.boundItem = headerItem;
        }

        public void onConfigure(int i, MGRecyclerDataPayload mGRecyclerDataPayload) {
            Unit unit;
            m.checkNotNullParameter(mGRecyclerDataPayload, "data");
            super.onConfigure(i, (int) mGRecyclerDataPayload);
            if (!(mGRecyclerDataPayload instanceof HeaderItem)) {
                mGRecyclerDataPayload = null;
            }
            HeaderItem headerItem = (HeaderItem) mGRecyclerDataPayload;
            if (headerItem != null) {
                this.boundItem = headerItem;
                if (headerItem instanceof HeaderItem.StandardHeaderItem) {
                    TextView textView = this.binding.f2093b;
                    m.checkNotNullExpressionValue(textView, "binding.headerItemText");
                    b.m(textView, Companion.getCategoryString(((HeaderItem.StandardHeaderItem) headerItem).getEmojiCategory()), new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                    unit = Unit.a;
                } else if (headerItem instanceof HeaderItem.GuildHeaderItem) {
                    TextView textView2 = this.binding.f2093b;
                    m.checkNotNullExpressionValue(textView2, "binding.headerItemText");
                    textView2.setText(((HeaderItem.GuildHeaderItem) headerItem).getText());
                    unit = Unit.a;
                } else if (headerItem instanceof HeaderItem.StringHeaderItem) {
                    TextView textView3 = this.binding.f2093b;
                    m.checkNotNullExpressionValue(textView3, "binding.headerItemText");
                    b.m(textView3, ((HeaderItem.StringHeaderItem) headerItem).getStringRes(), new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                    unit = Unit.a;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                KotlinExtensionsKt.getExhaustive(unit);
            }
        }
    }

    /* compiled from: WidgetEmojiAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0005\u001a\u00020\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$PremiumEmojiUpsellViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/databinding/EmojiPickerPremiumUpsellBinding;", "binding", "Lcom/discord/databinding/EmojiPickerPremiumUpsellBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PremiumEmojiUpsellViewHolder extends MGRecyclerViewHolder<WidgetEmojiAdapter, MGRecyclerDataPayload> {
        private final EmojiPickerPremiumUpsellBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PremiumEmojiUpsellViewHolder(final WidgetEmojiAdapter widgetEmojiAdapter) {
            super((int) R.layout.emoji_picker_premium_upsell, widgetEmojiAdapter);
            m.checkNotNullParameter(widgetEmojiAdapter, "adapter");
            View view = this.itemView;
            TextView textView = (TextView) view.findViewById(R.id.emojiPickerPremiumCta);
            if (textView != null) {
                EmojiPickerPremiumUpsellBinding emojiPickerPremiumUpsellBinding = new EmojiPickerPremiumUpsellBinding((LinearLayout) view, textView);
                m.checkNotNullExpressionValue(emojiPickerPremiumUpsellBinding, "EmojiPickerPremiumUpsellBinding.bind(itemView)");
                this.binding = emojiPickerPremiumUpsellBinding;
                textView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter.PremiumEmojiUpsellViewHolder.1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        WidgetEmojiAdapter.this.onGetPremiumCtaClicked.invoke();
                    }
                });
                return;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.emojiPickerPremiumCta)));
        }
    }

    /* compiled from: WidgetEmojiAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\u00020\u00078\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$UpsellItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "type", "I", "getType", "()I", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UpsellItem implements MGRecyclerDataPayload {
        private static final String key;
        public static final UpsellItem INSTANCE = new UpsellItem();
        private static final int type = 2;

        static {
            String name = UpsellItem.class.getName();
            m.checkNotNullExpressionValue(name, "javaClass.name");
            key = name;
        }

        private UpsellItem() {
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return type;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEmojiAdapter(RecyclerView recyclerView, FragmentManager fragmentManager, OnEmojiSelectedListener onEmojiSelectedListener, Function0<Unit> function0, Function0<Unit> function02) {
        super(recyclerView, null, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(onEmojiSelectedListener, "onEmojiSelectedListener");
        m.checkNotNullParameter(function0, "onGetPremiumCtaClicked");
        m.checkNotNullParameter(function02, "hideKeyboard");
        this.fragmentManager = fragmentManager;
        this.onEmojiSelectedListener = onEmojiSelectedListener;
        this.onGetPremiumCtaClicked = function0;
        this.hideKeyboard = function02;
        Context context = recyclerView.getContext();
        m.checkNotNullExpressionValue(context, "recycler.context");
        this.emojiSizePx = context.getResources().getDimensionPixelSize(R.dimen.chat_input_emoji_size);
        Context context2 = recyclerView.getContext();
        m.checkNotNullExpressionValue(context2, "recycler.context");
        this.numColumns = WidgetExpressionPickerAdapter.Companion.calculateNumOfColumns(recyclerView, context2.getResources().getDimension(R.dimen.chat_input_emoji_size), 8);
        this.layoutManager = new GridLayoutManager(recyclerView.getContext(), getNumColumns());
        getLayoutManager().setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter.1
            @Override // androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
            public int getSpanSize(int i) {
                int itemViewType = WidgetEmojiAdapter.this.getItemViewType(i);
                if (itemViewType == 0 || itemViewType == 2) {
                    return WidgetEmojiAdapter.this.getNumColumns();
                }
                return 1;
            }
        });
        recyclerView.setLayoutManager(getLayoutManager());
        recyclerView.setAdapter(this);
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter
    public GridLayoutManager getLayoutManager() {
        return this.layoutManager;
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter
    public int getNumColumns() {
        return this.numColumns;
    }

    @Override // com.discord.utilities.views.StickyHeaderItemDecoration.StickyHeaderAdapter
    public boolean isHeader(int i) {
        return u.getOrNull(getInternalData(), i) instanceof HeaderItem;
    }

    @Override // com.discord.widgets.chat.input.expression.WidgetExpressionPickerAdapter
    public HeaderViewHolder createStickyHeaderViewHolder(WidgetExpressionPickerAdapter widgetExpressionPickerAdapter) {
        m.checkNotNullParameter(widgetExpressionPickerAdapter, "adapter");
        return new HeaderViewHolder((WidgetEmojiAdapter) widgetExpressionPickerAdapter);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<WidgetEmojiAdapter, MGRecyclerDataPayload> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new HeaderViewHolder(this);
        }
        if (i == 1) {
            return new EmojiViewHolder(this);
        }
        if (i == 2) {
            return new PremiumEmojiUpsellViewHolder(this);
        }
        throw invalidViewTypeException(i);
    }

    /* compiled from: WidgetEmojiAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000f\u001a\u00020\t¢\u0006\u0004\b&\u0010'J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ:\u0010\u0010\u001a\u00020\u00002\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0004J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\t2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u00020\u00138\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0015R\u001c\u0010\u001d\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b \u0010\u0004R\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010!\u001a\u0004\b\"\u0010\u0007R\u0019\u0010\u000f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010#\u001a\u0004\b$\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b%\u0010\u0004¨\u0006("}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "component1", "()Ljava/lang/String;", "Lcom/discord/models/domain/emoji/Emoji;", "component2", "()Lcom/discord/models/domain/emoji/Emoji;", "component3", "", "component4", "()Z", "guildName", "emoji", "emojiName", "allowEmojisToAnimate", "copy", "(Ljava/lang/String;Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;Z)Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "getEmojiName", "Lcom/discord/models/domain/emoji/Emoji;", "getEmoji", "Z", "getAllowEmojisToAnimate", "getGuildName", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/models/domain/emoji/Emoji;Ljava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EmojiItem implements MGRecyclerDataPayload {
        private final boolean allowEmojisToAnimate;
        private final Emoji emoji;
        private final String emojiName;
        private final String guildName;
        private final String key;
        private final int type;

        public EmojiItem(String str, Emoji emoji, String str2, boolean z2) {
            m.checkNotNullParameter(emoji, "emoji");
            m.checkNotNullParameter(str2, "emojiName");
            this.guildName = str;
            this.emoji = emoji;
            this.emojiName = str2;
            this.allowEmojisToAnimate = z2;
            this.type = 1;
            String uniqueId = emoji.getUniqueId();
            m.checkNotNullExpressionValue(uniqueId, "emoji.uniqueId");
            this.key = uniqueId;
        }

        public static /* synthetic */ EmojiItem copy$default(EmojiItem emojiItem, String str, Emoji emoji, String str2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                str = emojiItem.guildName;
            }
            if ((i & 2) != 0) {
                emoji = emojiItem.emoji;
            }
            if ((i & 4) != 0) {
                str2 = emojiItem.emojiName;
            }
            if ((i & 8) != 0) {
                z2 = emojiItem.allowEmojisToAnimate;
            }
            return emojiItem.copy(str, emoji, str2, z2);
        }

        public final String component1() {
            return this.guildName;
        }

        public final Emoji component2() {
            return this.emoji;
        }

        public final String component3() {
            return this.emojiName;
        }

        public final boolean component4() {
            return this.allowEmojisToAnimate;
        }

        public final EmojiItem copy(String str, Emoji emoji, String str2, boolean z2) {
            m.checkNotNullParameter(emoji, "emoji");
            m.checkNotNullParameter(str2, "emojiName");
            return new EmojiItem(str, emoji, str2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EmojiItem)) {
                return false;
            }
            EmojiItem emojiItem = (EmojiItem) obj;
            return m.areEqual(this.guildName, emojiItem.guildName) && m.areEqual(this.emoji, emojiItem.emoji) && m.areEqual(this.emojiName, emojiItem.emojiName) && this.allowEmojisToAnimate == emojiItem.allowEmojisToAnimate;
        }

        public final boolean getAllowEmojisToAnimate() {
            return this.allowEmojisToAnimate;
        }

        public final Emoji getEmoji() {
            return this.emoji;
        }

        public final String getEmojiName() {
            return this.emojiName;
        }

        public final String getGuildName() {
            return this.guildName;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            String str = this.guildName;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Emoji emoji = this.emoji;
            int hashCode2 = (hashCode + (emoji != null ? emoji.hashCode() : 0)) * 31;
            String str2 = this.emojiName;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.allowEmojisToAnimate;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("EmojiItem(guildName=");
            R.append(this.guildName);
            R.append(", emoji=");
            R.append(this.emoji);
            R.append(", emojiName=");
            R.append(this.emojiName);
            R.append(", allowEmojisToAnimate=");
            return a.M(R, this.allowEmojisToAnimate, ")");
        }

        public /* synthetic */ EmojiItem(String str, Emoji emoji, String str2, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, emoji, str2, (i & 8) != 0 ? false : z2);
        }
    }
}
