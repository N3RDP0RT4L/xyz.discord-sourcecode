package com.discord.widgets.chat.input.emoji;

import com.discord.widgets.chat.input.emoji.EmojiCategoryItem;
import com.discord.widgets.chat.input.emoji.EmojiPickerViewModel;
import d0.z.d.o;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: EmojiPickerViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;", "invoke", "()Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EmojiPickerViewModel$ViewState$Results$firstUnicodeEmojiCategoryItem$2 extends o implements Function0<EmojiCategoryItem.StandardItem> {
    public final /* synthetic */ EmojiPickerViewModel.ViewState.Results this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EmojiPickerViewModel$ViewState$Results$firstUnicodeEmojiCategoryItem$2(EmojiPickerViewModel.ViewState.Results results) {
        super(0);
        this.this$0 = results;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final EmojiCategoryItem.StandardItem invoke() {
        EmojiCategoryItem.StandardItem standardItem;
        Object obj;
        Iterator<T> it = this.this$0.getCategoryItems().iterator();
        while (true) {
            standardItem = null;
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((EmojiCategoryItem) obj).containsOnlyUnicodeEmoji()) {
                break;
            }
        }
        if (obj instanceof EmojiCategoryItem.StandardItem) {
            standardItem = obj;
        }
        return standardItem;
    }
}
