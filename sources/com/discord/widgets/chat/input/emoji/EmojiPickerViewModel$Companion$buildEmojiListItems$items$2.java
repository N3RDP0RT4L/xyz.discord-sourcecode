package com.discord.widgets.chat.input.emoji;

import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.guild.Guild;
import com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter;
import d0.g0.w;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: EmojiPickerViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/emoji/Emoji;", "emoji", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;", "invoke", "(Lcom/discord/models/domain/emoji/Emoji;)Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter$EmojiItem;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EmojiPickerViewModel$Companion$buildEmojiListItems$items$2 extends o implements Function1<Emoji, WidgetEmojiAdapter.EmojiItem> {
    public final /* synthetic */ boolean $allowEmojisToAnimate;
    public final /* synthetic */ Function1 $getGuild;
    public final /* synthetic */ String $searchInputLower;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EmojiPickerViewModel$Companion$buildEmojiListItems$items$2(String str, Function1 function1, boolean z2) {
        super(1);
        this.$searchInputLower = str;
        this.$getGuild = function1;
        this.$allowEmojisToAnimate = z2;
    }

    public final WidgetEmojiAdapter.EmojiItem invoke(Emoji emoji) {
        String str;
        Object obj;
        m.checkNotNullParameter(emoji, "emoji");
        List<String> names = emoji.getNames();
        m.checkNotNullExpressionValue(names, "emoji.names");
        Iterator<T> it = names.iterator();
        while (true) {
            str = null;
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            String str2 = (String) obj;
            m.checkNotNullExpressionValue(str2, ModelAuditLogEntry.CHANGE_KEY_NAME);
            Locale locale = Locale.getDefault();
            m.checkNotNullExpressionValue(locale, "Locale.getDefault()");
            Objects.requireNonNull(str2, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = str2.toLowerCase(locale);
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            if (w.contains$default((CharSequence) lowerCase, (CharSequence) this.$searchInputLower, false, 2, (Object) null)) {
                break;
            }
        }
        String str3 = (String) obj;
        if (str3 == null) {
            return null;
        }
        ModelEmojiCustom modelEmojiCustom = (ModelEmojiCustom) (!(emoji instanceof ModelEmojiCustom) ? null : emoji);
        if (modelEmojiCustom != null) {
            Guild guild = (Guild) this.$getGuild.invoke(Long.valueOf(modelEmojiCustom.getGuildId()));
            if (guild != null) {
                str = guild.getName();
            }
        }
        return new WidgetEmojiAdapter.EmojiItem(str, emoji, str3, this.$allowEmojisToAnimate);
    }
}
