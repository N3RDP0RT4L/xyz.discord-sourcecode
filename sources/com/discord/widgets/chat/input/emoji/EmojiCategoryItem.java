package com.discord.widgets.chat.input.emoji;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.emoji.EmojiCategory;
import com.discord.models.guild.Guild;
import com.discord.utilities.recycler.DiffKeyProvider;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: EmojiCategoryItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00192\u00020\u0001:\u0003\u0019\u001a\u001bB-\b\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0010\u0010\u001a\u00020\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR(\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\n8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0010\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0010\u0010\u0004R\u001c\u0010\u0013\u001a\u00020\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016\u0082\u0001\u0002\u001c\u001d¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;", "Lcom/discord/utilities/recycler/DiffKeyProvider;", "", "containsOnlyUnicodeEmoji", "()Z", "", "stableId", "J", "getStableId", "()J", "Lkotlin/Pair;", "", "categoryRange", "Lkotlin/Pair;", "getCategoryRange", "()Lkotlin/Pair;", "isSelected", "Z", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", HookHelper.constructorName, "(JLkotlin/Pair;Z)V", "Companion", "GuildItem", "StandardItem", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$GuildItem;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class EmojiCategoryItem implements DiffKeyProvider {
    public static final Companion Companion = new Companion(null);
    public static final int TYPE_GUILD = 1;
    public static final int TYPE_STANDARD = 0;
    private final Pair<Integer, Integer> categoryRange;
    private final boolean isSelected;
    private final String key;
    private final long stableId;

    /* compiled from: EmojiCategoryItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u000b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000e\u0010\r¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$Companion;", "", "Lcom/discord/models/domain/emoji/EmojiCategory;", "emojiCategory", "", "mapEmojiCategoryToItemId", "(Lcom/discord/models/domain/emoji/EmojiCategory;)J", "Lcom/discord/models/guild/Guild;", "guild", "mapGuildToItemId", "(Lcom/discord/models/guild/Guild;)J", "", "TYPE_GUILD", "I", "TYPE_STANDARD", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final long mapEmojiCategoryToItemId(EmojiCategory emojiCategory) {
            m.checkNotNullParameter(emojiCategory, "emojiCategory");
            return emojiCategory.name().hashCode();
        }

        public final long mapGuildToItemId(Guild guild) {
            m.checkNotNullParameter(guild, "guild");
            return guild.getId();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EmojiCategoryItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ:\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\u0014\b\u0002\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\t2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001c\u0010\u000e\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001a\u001a\u0004\b\u000e\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R(\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\b¨\u0006!"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$GuildItem;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lkotlin/Pair;", "", "component2", "()Lkotlin/Pair;", "", "component3", "()Z", "guild", "categoryRange", "isSelected", "copy", "(Lcom/discord/models/guild/Guild;Lkotlin/Pair;Z)Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$GuildItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lkotlin/Pair;", "getCategoryRange", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lkotlin/Pair;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class GuildItem extends EmojiCategoryItem {
        private final Pair<Integer, Integer> categoryRange;
        private final Guild guild;
        private final boolean isSelected;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public GuildItem(Guild guild, Pair<Integer, Integer> pair, boolean z2) {
            super(EmojiCategoryItem.Companion.mapGuildToItemId(guild), pair, z2, null);
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(pair, "categoryRange");
            this.guild = guild;
            this.categoryRange = pair;
            this.isSelected = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ GuildItem copy$default(GuildItem guildItem, Guild guild, Pair pair, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = guildItem.guild;
            }
            if ((i & 2) != 0) {
                pair = guildItem.getCategoryRange();
            }
            if ((i & 4) != 0) {
                z2 = guildItem.isSelected();
            }
            return guildItem.copy(guild, pair, z2);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final Pair<Integer, Integer> component2() {
            return getCategoryRange();
        }

        public final boolean component3() {
            return isSelected();
        }

        public final GuildItem copy(Guild guild, Pair<Integer, Integer> pair, boolean z2) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(pair, "categoryRange");
            return new GuildItem(guild, pair, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GuildItem)) {
                return false;
            }
            GuildItem guildItem = (GuildItem) obj;
            return m.areEqual(this.guild, guildItem.guild) && m.areEqual(getCategoryRange(), guildItem.getCategoryRange()) && isSelected() == guildItem.isSelected();
        }

        @Override // com.discord.widgets.chat.input.emoji.EmojiCategoryItem
        public Pair<Integer, Integer> getCategoryRange() {
            return this.categoryRange;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            Pair<Integer, Integer> categoryRange = getCategoryRange();
            if (categoryRange != null) {
                i = categoryRange.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean isSelected = isSelected();
            if (isSelected) {
                isSelected = true;
            }
            int i3 = isSelected ? 1 : 0;
            int i4 = isSelected ? 1 : 0;
            return i2 + i3;
        }

        @Override // com.discord.widgets.chat.input.emoji.EmojiCategoryItem
        public boolean isSelected() {
            return this.isSelected;
        }

        public String toString() {
            StringBuilder R = a.R("GuildItem(guild=");
            R.append(this.guild);
            R.append(", categoryRange=");
            R.append(getCategoryRange());
            R.append(", isSelected=");
            R.append(isSelected());
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: EmojiCategoryItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ:\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\u0014\b\u0002\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\t2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001c\u0010\u000e\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001a\u001a\u0004\b\u000e\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R(\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\b¨\u0006!"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;", "Lcom/discord/models/domain/emoji/EmojiCategory;", "component1", "()Lcom/discord/models/domain/emoji/EmojiCategory;", "Lkotlin/Pair;", "", "component2", "()Lkotlin/Pair;", "", "component3", "()Z", "emojiCategory", "categoryRange", "isSelected", "copy", "(Lcom/discord/models/domain/emoji/EmojiCategory;Lkotlin/Pair;Z)Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/models/domain/emoji/EmojiCategory;", "getEmojiCategory", "Lkotlin/Pair;", "getCategoryRange", HookHelper.constructorName, "(Lcom/discord/models/domain/emoji/EmojiCategory;Lkotlin/Pair;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StandardItem extends EmojiCategoryItem {
        private final Pair<Integer, Integer> categoryRange;
        private final EmojiCategory emojiCategory;
        private final boolean isSelected;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public StandardItem(EmojiCategory emojiCategory, Pair<Integer, Integer> pair, boolean z2) {
            super(EmojiCategoryItem.Companion.mapEmojiCategoryToItemId(emojiCategory), pair, z2, null);
            m.checkNotNullParameter(emojiCategory, "emojiCategory");
            m.checkNotNullParameter(pair, "categoryRange");
            this.emojiCategory = emojiCategory;
            this.categoryRange = pair;
            this.isSelected = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StandardItem copy$default(StandardItem standardItem, EmojiCategory emojiCategory, Pair pair, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                emojiCategory = standardItem.emojiCategory;
            }
            if ((i & 2) != 0) {
                pair = standardItem.getCategoryRange();
            }
            if ((i & 4) != 0) {
                z2 = standardItem.isSelected();
            }
            return standardItem.copy(emojiCategory, pair, z2);
        }

        public final EmojiCategory component1() {
            return this.emojiCategory;
        }

        public final Pair<Integer, Integer> component2() {
            return getCategoryRange();
        }

        public final boolean component3() {
            return isSelected();
        }

        public final StandardItem copy(EmojiCategory emojiCategory, Pair<Integer, Integer> pair, boolean z2) {
            m.checkNotNullParameter(emojiCategory, "emojiCategory");
            m.checkNotNullParameter(pair, "categoryRange");
            return new StandardItem(emojiCategory, pair, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StandardItem)) {
                return false;
            }
            StandardItem standardItem = (StandardItem) obj;
            return m.areEqual(this.emojiCategory, standardItem.emojiCategory) && m.areEqual(getCategoryRange(), standardItem.getCategoryRange()) && isSelected() == standardItem.isSelected();
        }

        @Override // com.discord.widgets.chat.input.emoji.EmojiCategoryItem
        public Pair<Integer, Integer> getCategoryRange() {
            return this.categoryRange;
        }

        public final EmojiCategory getEmojiCategory() {
            return this.emojiCategory;
        }

        public int hashCode() {
            EmojiCategory emojiCategory = this.emojiCategory;
            int i = 0;
            int hashCode = (emojiCategory != null ? emojiCategory.hashCode() : 0) * 31;
            Pair<Integer, Integer> categoryRange = getCategoryRange();
            if (categoryRange != null) {
                i = categoryRange.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean isSelected = isSelected();
            if (isSelected) {
                isSelected = true;
            }
            int i3 = isSelected ? 1 : 0;
            int i4 = isSelected ? 1 : 0;
            return i2 + i3;
        }

        @Override // com.discord.widgets.chat.input.emoji.EmojiCategoryItem
        public boolean isSelected() {
            return this.isSelected;
        }

        public String toString() {
            StringBuilder R = a.R("StandardItem(emojiCategory=");
            R.append(this.emojiCategory);
            R.append(", categoryRange=");
            R.append(getCategoryRange());
            R.append(", isSelected=");
            R.append(isSelected());
            R.append(")");
            return R.toString();
        }
    }

    private EmojiCategoryItem(long j, Pair<Integer, Integer> pair, boolean z2) {
        this.stableId = j;
        this.categoryRange = pair;
        this.isSelected = z2;
        this.key = String.valueOf(j);
    }

    public final boolean containsOnlyUnicodeEmoji() {
        return (this instanceof StandardItem) && ((StandardItem) this).getEmojiCategory().containsOnlyUnicode;
    }

    public Pair<Integer, Integer> getCategoryRange() {
        return this.categoryRange;
    }

    @Override // com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final long getStableId() {
        return this.stableId;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public /* synthetic */ EmojiCategoryItem(long j, Pair pair, boolean z2, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, pair, z2);
    }
}
