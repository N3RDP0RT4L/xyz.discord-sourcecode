package com.discord.widgets.chat.input.emoji;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.EmojiCategory;
import com.discord.models.domain.emoji.EmojiSet;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreAccessibility;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreEmoji;
import com.discord.stores.StoreGuildsSorted;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.analytics.SearchType;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.input.emoji.EmojiCategoryItem;
import com.discord.widgets.chat.input.emoji.EmojiPickerContextType;
import com.discord.widgets.chat.input.emoji.EmojiPickerViewModel;
import com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter;
import d0.f0.q;
import d0.g;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.sequences.Sequence;
import rx.Observable;
import rx.functions.Func6;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: EmojiPickerViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\b\u0016\u0018\u0000 32\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0006345678BQ\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u0010!\u001a\u00020 \u0012\u000e\b\u0002\u0010$\u001a\b\u0012\u0004\u0012\u00020\f0#\u0012\u000e\b\u0002\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00100#\u0012\u000e\b\u0002\u00100\u001a\b\u0012\u0004\u0012\u00020\u00030\b\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b1\u00102J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\r\u0010\u0014\u001a\u00020\u0005¢\u0006\u0004\b\u0014\u0010\u0015J)\u0010\u001a\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u00162\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00050\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\r\u0010\u001c\u001a\u00020\u0005¢\u0006\u0004\b\u001c\u0010\u0015R\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u001c\u0010$\u001a\b\u0012\u0004\u0012\u00020\f0#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001c\u0010*\u001a\b\u0012\u0004\u0012\u00020\t0)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u001c\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00100#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u0010%¨\u00069"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;)V", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;", "observeEvents", "()Lrx/Observable;", "", "searchText", "setSearchText", "(Ljava/lang/String;)V", "", "categoryId", "setSelectedCategoryItemId", "(J)V", "onClickUnicodeEmojiCategories", "()V", "Lcom/discord/models/domain/emoji/Emoji;", "emoji", "Lkotlin/Function1;", "validEmojiSelected", "onEmojiSelected", "(Lcom/discord/models/domain/emoji/Emoji;Lkotlin/jvm/functions/Function1;)V", "onUpsellHeaderVisible", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Ljava/util/Locale;", "locale", "Ljava/util/Locale;", "Lrx/subjects/BehaviorSubject;", "searchSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;", "emojiPickerContextType", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;", "Lrx/subjects/PublishSubject;", "eventSubject", "Lrx/subjects/PublishSubject;", "", "logWhenUpsellHeaderIsViewed", "Z", "selectedCategoryItemIdSubject", "storeStateObservable", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;Ljava/util/Locale;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Lrx/Observable;Lcom/discord/stores/StoreAnalytics;)V", "Companion", "Event", "Inline", "Sheet", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class EmojiPickerViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final EmojiPickerContextType emojiPickerContextType;
    private final PublishSubject<Event> eventSubject;
    private final Locale locale;
    private boolean logWhenUpsellHeaderIsViewed;
    private final BehaviorSubject<String> searchSubject;
    private final BehaviorSubject<Long> selectedCategoryItemIdSubject;
    private final StoreAnalytics storeAnalytics;

    /* compiled from: EmojiPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            EmojiPickerViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: EmojiPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001:\u00011B\t\b\u0002¢\u0006\u0004\b/\u00100JY\u0010\u0011\u001a\u00020\u00102\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00022\u0018\u0010\t\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00052\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012JA\u0010\u0016\u001a\u00020\u00102\b\u0010\u0013\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0016\u0010\u0017J'\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c2\u0006\u0010\u0019\u001a\u00020\u00182\b\b\u0002\u0010\u001b\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJe\u0010-\u001a\b\u0012\u0004\u0012\u00020,0\u001c2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\n0!2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00060!2\b\b\u0002\u0010%\u001a\u00020$2\b\b\u0002\u0010'\u001a\u00020&2\b\b\u0002\u0010)\u001a\u00020(2\b\b\u0002\u0010+\u001a\u00020*¢\u0006\u0004\b-\u0010.¨\u00062"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion;", "", "", "Lcom/discord/models/domain/emoji/Emoji;", "emojis", "Lkotlin/Function1;", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/models/guild/Guild;", "getGuild", "", "searchInputLower", "", "allowEmojisToAnimate", "partition", "includeUnavailable", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems;", "buildEmojiListItems", "(Ljava/util/Collection;Lkotlin/jvm/functions/Function1;Ljava/lang/String;ZZZ)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems;", "guild", "Lcom/discord/models/domain/emoji/EmojiSet;", "emojiSet", "buildGuildEmojiListItems", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/emoji/EmojiSet;Ljava/lang/String;ZZZ)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;", "emojiPickerContextType", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lrx/Observable;", "Lcom/discord/stores/StoreEmoji$EmojiContext;", "getEmojiContextObservable", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;Lcom/discord/stores/StoreChannelsSelected;)Lrx/Observable;", "emojiContextObservable", "Lrx/subjects/BehaviorSubject;", "searchSubject", "selectedCategoryItemIdSubject", "Lcom/discord/stores/StoreEmoji;", "storeEmoji", "Lcom/discord/stores/StoreGuildsSorted;", "storeGuildsSorted", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreAccessibility;", "storeAccessibility", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;", "observeStoreState", "(Lrx/Observable;Lrx/subjects/BehaviorSubject;Lrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreEmoji;Lcom/discord/stores/StoreGuildsSorted;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreAccessibility;)Lrx/Observable;", HookHelper.constructorName, "()V", "EmojiItems", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: EmojiPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems;", "", HookHelper.constructorName, "()V", "Partitioned", "Regular", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Regular;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Partitioned;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static abstract class EmojiItems {

            /* compiled from: EmojiPickerViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\b\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB#\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0019\u0010\u001aB)\b\u0016\u0012\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u001b¢\u0006\u0004\b\u0019\u0010\u001dJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0005J0\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0005R\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0016\u001a\u0004\b\u0018\u0010\u0005¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Partitioned;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems;", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "component1", "()Ljava/util/List;", "component2", "regularItems", "premiumItems", "copy", "(Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Partitioned;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getPremiumItems", "getRegularItems", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;)V", "Lkotlin/Pair;", "p", "(Lkotlin/Pair;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Partitioned extends EmojiItems {
                public static final C0239Companion Companion = new C0239Companion(null);
                private static final Partitioned Empty = new Partitioned(n.emptyList(), n.emptyList());
                private final List<MGRecyclerDataPayload> premiumItems;
                private final List<MGRecyclerDataPayload> regularItems;

                /* compiled from: EmojiPickerViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Partitioned$Companion;", "", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Partitioned;", "Empty", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Partitioned;", "getEmpty", "()Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Partitioned;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$EmojiItems$Partitioned$Companion  reason: collision with other inner class name */
                /* loaded from: classes2.dex */
                public static final class C0239Companion {
                    private C0239Companion() {
                    }

                    public final Partitioned getEmpty() {
                        return Partitioned.Empty;
                    }

                    public /* synthetic */ C0239Companion(DefaultConstructorMarker defaultConstructorMarker) {
                        this();
                    }
                }

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                /* JADX WARN: Multi-variable type inference failed */
                public Partitioned(List<? extends MGRecyclerDataPayload> list, List<? extends MGRecyclerDataPayload> list2) {
                    super(null);
                    m.checkNotNullParameter(list, "regularItems");
                    m.checkNotNullParameter(list2, "premiumItems");
                    this.regularItems = list;
                    this.premiumItems = list2;
                }

                /* JADX WARN: Multi-variable type inference failed */
                public static /* synthetic */ Partitioned copy$default(Partitioned partitioned, List list, List list2, int i, Object obj) {
                    if ((i & 1) != 0) {
                        list = partitioned.regularItems;
                    }
                    if ((i & 2) != 0) {
                        list2 = partitioned.premiumItems;
                    }
                    return partitioned.copy(list, list2);
                }

                public final List<MGRecyclerDataPayload> component1() {
                    return this.regularItems;
                }

                public final List<MGRecyclerDataPayload> component2() {
                    return this.premiumItems;
                }

                public final Partitioned copy(List<? extends MGRecyclerDataPayload> list, List<? extends MGRecyclerDataPayload> list2) {
                    m.checkNotNullParameter(list, "regularItems");
                    m.checkNotNullParameter(list2, "premiumItems");
                    return new Partitioned(list, list2);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof Partitioned)) {
                        return false;
                    }
                    Partitioned partitioned = (Partitioned) obj;
                    return m.areEqual(this.regularItems, partitioned.regularItems) && m.areEqual(this.premiumItems, partitioned.premiumItems);
                }

                public final List<MGRecyclerDataPayload> getPremiumItems() {
                    return this.premiumItems;
                }

                public final List<MGRecyclerDataPayload> getRegularItems() {
                    return this.regularItems;
                }

                public int hashCode() {
                    List<MGRecyclerDataPayload> list = this.regularItems;
                    int i = 0;
                    int hashCode = (list != null ? list.hashCode() : 0) * 31;
                    List<MGRecyclerDataPayload> list2 = this.premiumItems;
                    if (list2 != null) {
                        i = list2.hashCode();
                    }
                    return hashCode + i;
                }

                public String toString() {
                    StringBuilder R = a.R("Partitioned(regularItems=");
                    R.append(this.regularItems);
                    R.append(", premiumItems=");
                    return a.K(R, this.premiumItems, ")");
                }

                /* JADX WARN: 'this' call moved to the top of the method (can break code semantics) */
                public Partitioned(Pair<? extends List<? extends MGRecyclerDataPayload>, ? extends List<? extends MGRecyclerDataPayload>> pair) {
                    this(pair.getFirst(), pair.getSecond());
                    m.checkNotNullParameter(pair, "p");
                }
            }

            /* compiled from: EmojiPickerViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Regular;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems;", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "component1", "()Ljava/util/List;", "items", "copy", "(Ljava/util/List;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Regular;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Ljava/util/List;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Regular extends EmojiItems {
                public static final C0240Companion Companion = new C0240Companion(null);
                private static final Regular Empty = new Regular(n.emptyList());
                private final List<MGRecyclerDataPayload> items;

                /* compiled from: EmojiPickerViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Regular$Companion;", "", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Regular;", "Empty", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Regular;", "getEmpty", "()Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Companion$EmojiItems$Regular;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$EmojiItems$Regular$Companion  reason: collision with other inner class name */
                /* loaded from: classes2.dex */
                public static final class C0240Companion {
                    private C0240Companion() {
                    }

                    public final Regular getEmpty() {
                        return Regular.Empty;
                    }

                    public /* synthetic */ C0240Companion(DefaultConstructorMarker defaultConstructorMarker) {
                        this();
                    }
                }

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                /* JADX WARN: Multi-variable type inference failed */
                public Regular(List<? extends MGRecyclerDataPayload> list) {
                    super(null);
                    m.checkNotNullParameter(list, "items");
                    this.items = list;
                }

                /* JADX WARN: Multi-variable type inference failed */
                public static /* synthetic */ Regular copy$default(Regular regular, List list, int i, Object obj) {
                    if ((i & 1) != 0) {
                        list = regular.items;
                    }
                    return regular.copy(list);
                }

                public final List<MGRecyclerDataPayload> component1() {
                    return this.items;
                }

                public final Regular copy(List<? extends MGRecyclerDataPayload> list) {
                    m.checkNotNullParameter(list, "items");
                    return new Regular(list);
                }

                public boolean equals(Object obj) {
                    if (this != obj) {
                        return (obj instanceof Regular) && m.areEqual(this.items, ((Regular) obj).items);
                    }
                    return true;
                }

                public final List<MGRecyclerDataPayload> getItems() {
                    return this.items;
                }

                public int hashCode() {
                    List<MGRecyclerDataPayload> list = this.items;
                    if (list != null) {
                        return list.hashCode();
                    }
                    return 0;
                }

                public String toString() {
                    return a.K(a.R("Regular(items="), this.items, ")");
                }
            }

            private EmojiItems() {
            }

            public /* synthetic */ EmojiItems(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final EmojiItems buildEmojiListItems(Collection<? extends Emoji> collection, Function1<? super Long, Guild> function1, String str, boolean z2, boolean z3, boolean z4) {
            if (collection == null) {
                collection = n.emptyList();
            }
            Sequence mapNotNull = q.mapNotNull(q.filter(u.asSequence(collection), new EmojiPickerViewModel$Companion$buildEmojiListItems$items$1(z4)), new EmojiPickerViewModel$Companion$buildEmojiListItems$items$2(str, function1, z2));
            if (!z3) {
                return new EmojiItems.Regular(q.toList(mapNotNull));
            }
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (Object obj : mapNotNull) {
                if (((WidgetEmojiAdapter.EmojiItem) obj).getEmoji().isUsable()) {
                    arrayList.add(obj);
                } else {
                    arrayList2.add(obj);
                }
            }
            return new EmojiItems.Partitioned(new Pair(arrayList, arrayList2));
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* JADX WARN: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
        /* JADX WARN: Removed duplicated region for block: B:8:0x002f  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.Companion.EmojiItems buildGuildEmojiListItems(com.discord.models.guild.Guild r9, com.discord.models.domain.emoji.EmojiSet r10, java.lang.String r11, boolean r12, boolean r13, boolean r14) {
            /*
                r8 = this;
                if (r9 == 0) goto L2b
                long r0 = r9.getId()
                java.util.Map<java.lang.Long, java.util.List<com.discord.models.domain.emoji.Emoji>> r10 = r10.customEmojis
                java.lang.Long r0 = java.lang.Long.valueOf(r0)
                java.lang.Object r10 = r10.get(r0)
                r1 = r10
                java.util.List r1 = (java.util.List) r1
                if (r1 == 0) goto L2b
                com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion r0 = com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.Companion
                com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$buildGuildEmojiListItems$$inlined$let$lambda$1 r10 = new com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$buildGuildEmojiListItems$$inlined$let$lambda$1
                r2 = r10
                r3 = r9
                r4 = r11
                r5 = r12
                r6 = r13
                r7 = r14
                r2.<init>(r3, r4, r5, r6, r7)
                r3 = r11
                r4 = r12
                r5 = r13
                r6 = r14
                com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$EmojiItems r9 = r0.buildEmojiListItems(r1, r2, r3, r4, r5, r6)
                goto L2c
            L2b:
                r9 = 0
            L2c:
                if (r9 == 0) goto L2f
                goto L3e
            L2f:
                if (r13 == 0) goto L38
                com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$EmojiItems$Partitioned$Companion r9 = com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.Companion.EmojiItems.Partitioned.Companion
                com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$EmojiItems$Partitioned r9 = r9.getEmpty()
                goto L3e
            L38:
                com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$EmojiItems$Regular$Companion r9 = com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.Companion.EmojiItems.Regular.Companion
                com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$EmojiItems$Regular r9 = r9.getEmpty()
            L3e:
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.Companion.buildGuildEmojiListItems(com.discord.models.guild.Guild, com.discord.models.domain.emoji.EmojiSet, java.lang.String, boolean, boolean, boolean):com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$EmojiItems");
        }

        private final Observable<StoreEmoji.EmojiContext> getEmojiContextObservable(EmojiPickerContextType emojiPickerContextType, StoreChannelsSelected storeChannelsSelected) {
            if (m.areEqual(emojiPickerContextType, EmojiPickerContextType.Global.INSTANCE)) {
                k kVar = new k(StoreEmoji.EmojiContext.Global.INSTANCE);
                m.checkNotNullExpressionValue(kVar, "Observable.just(\n       …ontext.Global\n          )");
                return kVar;
            } else if (emojiPickerContextType instanceof EmojiPickerContextType.Guild) {
                k kVar2 = new k(new StoreEmoji.EmojiContext.Guild(((EmojiPickerContextType.Guild) emojiPickerContextType).getGuildId()));
                m.checkNotNullExpressionValue(kVar2, "Observable.just(\n       …pe.guildId)\n            )");
                return kVar2;
            } else {
                Observable F = storeChannelsSelected.observeResolvedSelectedChannel().F(EmojiPickerViewModel$Companion$getEmojiContextObservable$1.INSTANCE);
                m.checkNotNullExpressionValue(F, "storeChannelsSelected.ob…      }\n                }");
                return F;
            }
        }

        public static /* synthetic */ Observable getEmojiContextObservable$default(Companion companion, EmojiPickerContextType emojiPickerContextType, StoreChannelsSelected storeChannelsSelected, int i, Object obj) {
            if ((i & 2) != 0) {
                storeChannelsSelected = StoreStream.Companion.getChannelsSelected();
            }
            return companion.getEmojiContextObservable(emojiPickerContextType, storeChannelsSelected);
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, Observable observable, BehaviorSubject behaviorSubject, BehaviorSubject behaviorSubject2, StoreEmoji storeEmoji, StoreGuildsSorted storeGuildsSorted, StoreUserSettings storeUserSettings, StoreAccessibility storeAccessibility, int i, Object obj) {
            return companion.observeStoreState(observable, behaviorSubject, behaviorSubject2, (i & 8) != 0 ? StoreStream.Companion.getEmojis() : storeEmoji, (i & 16) != 0 ? StoreStream.Companion.getGuildsSorted() : storeGuildsSorted, (i & 32) != 0 ? StoreStream.Companion.getUserSettings() : storeUserSettings, (i & 64) != 0 ? StoreStream.Companion.getAccessibility() : storeAccessibility);
        }

        public final Observable<StoreState> observeStoreState(Observable<StoreEmoji.EmojiContext> observable, final BehaviorSubject<String> behaviorSubject, final BehaviorSubject<Long> behaviorSubject2, final StoreEmoji storeEmoji, final StoreGuildsSorted storeGuildsSorted, final StoreUserSettings storeUserSettings, final StoreAccessibility storeAccessibility) {
            m.checkNotNullParameter(observable, "emojiContextObservable");
            m.checkNotNullParameter(behaviorSubject, "searchSubject");
            m.checkNotNullParameter(behaviorSubject2, "selectedCategoryItemIdSubject");
            m.checkNotNullParameter(storeEmoji, "storeEmoji");
            m.checkNotNullParameter(storeGuildsSorted, "storeGuildsSorted");
            m.checkNotNullParameter(storeUserSettings, "storeUserSettings");
            m.checkNotNullParameter(storeAccessibility, "storeAccessibility");
            Observable Y = observable.Y(new b<StoreEmoji.EmojiContext, Observable<? extends StoreState>>() { // from class: com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$observeStoreState$1
                public final Observable<? extends EmojiPickerViewModel.StoreState> call(final StoreEmoji.EmojiContext emojiContext) {
                    if (emojiContext == null) {
                        return new k(EmojiPickerViewModel.StoreState.Uninitialized.INSTANCE);
                    }
                    return Observable.f(StoreEmoji.this.getEmojiSet(emojiContext, true, true), storeGuildsSorted.observeOrderedGuilds(), behaviorSubject, StoreUserSettings.observeIsAnimatedEmojisEnabled$default(storeUserSettings, false, 1, null), storeAccessibility.observeReducedMotionEnabled(), behaviorSubject2, new Func6<EmojiSet, LinkedHashMap<Long, Guild>, String, Boolean, Boolean, Long, EmojiPickerViewModel.StoreState>() { // from class: com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion$observeStoreState$1.1
                        @Override // rx.functions.Func6
                        public /* bridge */ /* synthetic */ EmojiPickerViewModel.StoreState call(EmojiSet emojiSet, LinkedHashMap<Long, Guild> linkedHashMap, String str, Boolean bool, Boolean bool2, Long l) {
                            return call(emojiSet, linkedHashMap, str, bool.booleanValue(), bool2.booleanValue(), l.longValue());
                        }

                        public final EmojiPickerViewModel.StoreState call(EmojiSet emojiSet, LinkedHashMap<Long, Guild> linkedHashMap, String str, boolean z2, boolean z3, long j) {
                            m.checkNotNullParameter(linkedHashMap, "allGuilds");
                            m.checkNotNullParameter(str, "searchInputString");
                            if (emojiSet == null) {
                                return EmojiPickerViewModel.StoreState.Uninitialized.INSTANCE;
                            }
                            StoreEmoji.EmojiContext emojiContext2 = StoreEmoji.EmojiContext.this;
                            boolean z4 = z2 && !z3;
                            Set<Emoji> set = emojiSet.favoriteEmoji;
                            m.checkNotNullExpressionValue(set, "emojiSet.favoriteEmoji");
                            return new EmojiPickerViewModel.StoreState.Emoji(emojiSet, emojiContext2, linkedHashMap, str, z4, j, set);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "emojiContextObservable.s…  )\n          }\n        }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EmojiPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;", "", HookHelper.constructorName, "()V", "ScrollToEmojiListPosition", "ShowPremiumUpsellDialog", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ScrollToEmojiListPosition;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ShowPremiumUpsellDialog;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: EmojiPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ScrollToEmojiListPosition;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;", "", "component1", "()I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "copy", "(I)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ScrollToEmojiListPosition;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getPosition", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ScrollToEmojiListPosition extends Event {
            private final int position;

            public ScrollToEmojiListPosition(int i) {
                super(null);
                this.position = i;
            }

            public static /* synthetic */ ScrollToEmojiListPosition copy$default(ScrollToEmojiListPosition scrollToEmojiListPosition, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = scrollToEmojiListPosition.position;
                }
                return scrollToEmojiListPosition.copy(i);
            }

            public final int component1() {
                return this.position;
            }

            public final ScrollToEmojiListPosition copy(int i) {
                return new ScrollToEmojiListPosition(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ScrollToEmojiListPosition) && this.position == ((ScrollToEmojiListPosition) obj).position;
                }
                return true;
            }

            public final int getPosition() {
                return this.position;
            }

            public int hashCode() {
                return this.position;
            }

            public String toString() {
                return a.A(a.R("ScrollToEmojiListPosition(position="), this.position, ")");
            }
        }

        /* compiled from: EmojiPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u0000\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u000f\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0007\u0012\u0006\u0010\u0012\u001a\u00020\n\u0012\u0006\u0010\u0013\u001a\u00020\n¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\r\u0010\fJL\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0003\u0010\u000f\u001a\u00020\u00022\b\b\u0003\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00072\b\b\u0002\u0010\u0012\u001a\u00020\n2\b\b\u0002\u0010\u0013\u001a\u00020\nHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u0016\u0010\tJ\u0010\u0010\u0017\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0004J\u001a\u0010\u001a\u001a\u00020\n2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001e\u001a\u0004\b\u001f\u0010\fR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001c\u001a\u0004\b \u0010\u0004R\u0019\u0010\u0013\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001e\u001a\u0004\b!\u0010\fR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\"\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b$\u0010\t¨\u0006'"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ShowPremiumUpsellDialog;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;", "", "component1", "()I", "component2", "component3", "", "component4", "()Ljava/lang/String;", "", "component5", "()Z", "component6", "pageNumber", "header", "body", "sectionName", "showOtherPages", "showLearnMore", "copy", "(IIILjava/lang/String;ZZ)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event$ShowPremiumUpsellDialog;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getHeader", "Z", "getShowOtherPages", "getBody", "getShowLearnMore", "getPageNumber", "Ljava/lang/String;", "getSectionName", HookHelper.constructorName, "(IIILjava/lang/String;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowPremiumUpsellDialog extends Event {
            private final int body;
            private final int header;
            private final int pageNumber;
            private final String sectionName;
            private final boolean showLearnMore;
            private final boolean showOtherPages;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ShowPremiumUpsellDialog(int i, @StringRes int i2, @StringRes int i3, String str, boolean z2, boolean z3) {
                super(null);
                m.checkNotNullParameter(str, "sectionName");
                this.pageNumber = i;
                this.header = i2;
                this.body = i3;
                this.sectionName = str;
                this.showOtherPages = z2;
                this.showLearnMore = z3;
            }

            public static /* synthetic */ ShowPremiumUpsellDialog copy$default(ShowPremiumUpsellDialog showPremiumUpsellDialog, int i, int i2, int i3, String str, boolean z2, boolean z3, int i4, Object obj) {
                if ((i4 & 1) != 0) {
                    i = showPremiumUpsellDialog.pageNumber;
                }
                if ((i4 & 2) != 0) {
                    i2 = showPremiumUpsellDialog.header;
                }
                int i5 = i2;
                if ((i4 & 4) != 0) {
                    i3 = showPremiumUpsellDialog.body;
                }
                int i6 = i3;
                if ((i4 & 8) != 0) {
                    str = showPremiumUpsellDialog.sectionName;
                }
                String str2 = str;
                if ((i4 & 16) != 0) {
                    z2 = showPremiumUpsellDialog.showOtherPages;
                }
                boolean z4 = z2;
                if ((i4 & 32) != 0) {
                    z3 = showPremiumUpsellDialog.showLearnMore;
                }
                return showPremiumUpsellDialog.copy(i, i5, i6, str2, z4, z3);
            }

            public final int component1() {
                return this.pageNumber;
            }

            public final int component2() {
                return this.header;
            }

            public final int component3() {
                return this.body;
            }

            public final String component4() {
                return this.sectionName;
            }

            public final boolean component5() {
                return this.showOtherPages;
            }

            public final boolean component6() {
                return this.showLearnMore;
            }

            public final ShowPremiumUpsellDialog copy(int i, @StringRes int i2, @StringRes int i3, String str, boolean z2, boolean z3) {
                m.checkNotNullParameter(str, "sectionName");
                return new ShowPremiumUpsellDialog(i, i2, i3, str, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ShowPremiumUpsellDialog)) {
                    return false;
                }
                ShowPremiumUpsellDialog showPremiumUpsellDialog = (ShowPremiumUpsellDialog) obj;
                return this.pageNumber == showPremiumUpsellDialog.pageNumber && this.header == showPremiumUpsellDialog.header && this.body == showPremiumUpsellDialog.body && m.areEqual(this.sectionName, showPremiumUpsellDialog.sectionName) && this.showOtherPages == showPremiumUpsellDialog.showOtherPages && this.showLearnMore == showPremiumUpsellDialog.showLearnMore;
            }

            public final int getBody() {
                return this.body;
            }

            public final int getHeader() {
                return this.header;
            }

            public final int getPageNumber() {
                return this.pageNumber;
            }

            public final String getSectionName() {
                return this.sectionName;
            }

            public final boolean getShowLearnMore() {
                return this.showLearnMore;
            }

            public final boolean getShowOtherPages() {
                return this.showOtherPages;
            }

            public int hashCode() {
                int i = ((((this.pageNumber * 31) + this.header) * 31) + this.body) * 31;
                String str = this.sectionName;
                int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
                boolean z2 = this.showOtherPages;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode + i3) * 31;
                boolean z3 = this.showLearnMore;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                return i5 + i2;
            }

            public String toString() {
                StringBuilder R = a.R("ShowPremiumUpsellDialog(pageNumber=");
                R.append(this.pageNumber);
                R.append(", header=");
                R.append(this.header);
                R.append(", body=");
                R.append(this.body);
                R.append(", sectionName=");
                R.append(this.sectionName);
                R.append(", showOtherPages=");
                R.append(this.showOtherPages);
                R.append(", showLearnMore=");
                return a.M(R, this.showLearnMore, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EmojiPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Inline;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;", "emojiPickerContextType", "Ljava/util/Locale;", "locale", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;Ljava/util/Locale;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Inline extends EmojiPickerViewModel {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Inline(EmojiPickerContextType emojiPickerContextType, Locale locale) {
            super(emojiPickerContextType, locale, null, null, null, null, 60, null);
            m.checkNotNullParameter(emojiPickerContextType, "emojiPickerContextType");
            m.checkNotNullParameter(locale, "locale");
        }
    }

    /* compiled from: EmojiPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Sheet;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;", "emojiPickerContextType", "Ljava/util/Locale;", "locale", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;Ljava/util/Locale;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Sheet extends EmojiPickerViewModel {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Sheet(EmojiPickerContextType emojiPickerContextType, Locale locale) {
            super(emojiPickerContextType, locale, null, null, null, null, 60, null);
            m.checkNotNullParameter(emojiPickerContextType, "emojiPickerContextType");
            m.checkNotNullParameter(locale, "locale");
        }
    }

    /* compiled from: EmojiPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Emoji", "Uninitialized", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Uninitialized;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: EmojiPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001BQ\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0005\u0012\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b\u0012\u0006\u0010\u001c\u001a\u00020\r\u0012\u0006\u0010\u001d\u001a\u00020\u0010\u0012\u0006\u0010\u001e\u001a\u00020\t\u0012\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015¢\u0006\u0004\b8\u00109J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001c\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018Jh\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00052\u0014\b\u0002\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b2\b\b\u0002\u0010\u001c\u001a\u00020\r2\b\b\u0002\u0010\u001d\u001a\u00020\u00102\b\b\u0002\u0010\u001e\u001a\u00020\t2\u000e\b\u0002\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015HÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010\"\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\"\u0010\u000fJ\u0010\u0010$\u001a\u00020#HÖ\u0001¢\u0006\u0004\b$\u0010%J\u001a\u0010(\u001a\u00020\u00102\b\u0010'\u001a\u0004\u0018\u00010&HÖ\u0003¢\u0006\u0004\b(\u0010)R\u0019\u0010\u001a\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010*\u001a\u0004\b+\u0010\u0007R\u0019\u0010\u001e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010,\u001a\u0004\b-\u0010\u0014R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010.\u001a\u0004\b/\u0010\u0004R\u001f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00100\u001a\u0004\b1\u0010\u0018R%\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00102\u001a\u0004\b3\u0010\fR\u0019\u0010\u001c\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00104\u001a\u0004\b5\u0010\u000fR\u0019\u0010\u001d\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00106\u001a\u0004\b7\u0010\u0012¨\u0006:"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;", "Lcom/discord/models/domain/emoji/EmojiSet;", "component1", "()Lcom/discord/models/domain/emoji/EmojiSet;", "Lcom/discord/stores/StoreEmoji$EmojiContext;", "component2", "()Lcom/discord/stores/StoreEmoji$EmojiContext;", "Ljava/util/LinkedHashMap;", "", "Lcom/discord/models/guild/Guild;", "component3", "()Ljava/util/LinkedHashMap;", "", "component4", "()Ljava/lang/String;", "", "component5", "()Z", "component6", "()J", "", "Lcom/discord/models/domain/emoji/Emoji;", "component7", "()Ljava/util/Set;", "emojiSet", "emojiContext", "allGuilds", "searchInputStringUpper", "allowEmojisToAnimate", "selectedCategoryItemId", "favoriteEmoji", "copy", "(Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/LinkedHashMap;Ljava/lang/String;ZJLjava/util/Set;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Emoji;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreEmoji$EmojiContext;", "getEmojiContext", "J", "getSelectedCategoryItemId", "Lcom/discord/models/domain/emoji/EmojiSet;", "getEmojiSet", "Ljava/util/Set;", "getFavoriteEmoji", "Ljava/util/LinkedHashMap;", "getAllGuilds", "Ljava/lang/String;", "getSearchInputStringUpper", "Z", "getAllowEmojisToAnimate", HookHelper.constructorName, "(Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreEmoji$EmojiContext;Ljava/util/LinkedHashMap;Ljava/lang/String;ZJLjava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Emoji extends StoreState {
            private final LinkedHashMap<Long, Guild> allGuilds;
            private final boolean allowEmojisToAnimate;
            private final StoreEmoji.EmojiContext emojiContext;
            private final EmojiSet emojiSet;
            private final Set<com.discord.models.domain.emoji.Emoji> favoriteEmoji;
            private final String searchInputStringUpper;
            private final long selectedCategoryItemId;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Emoji(EmojiSet emojiSet, StoreEmoji.EmojiContext emojiContext, LinkedHashMap<Long, Guild> linkedHashMap, String str, boolean z2, long j, Set<? extends com.discord.models.domain.emoji.Emoji> set) {
                super(null);
                m.checkNotNullParameter(emojiSet, "emojiSet");
                m.checkNotNullParameter(emojiContext, "emojiContext");
                m.checkNotNullParameter(linkedHashMap, "allGuilds");
                m.checkNotNullParameter(str, "searchInputStringUpper");
                m.checkNotNullParameter(set, "favoriteEmoji");
                this.emojiSet = emojiSet;
                this.emojiContext = emojiContext;
                this.allGuilds = linkedHashMap;
                this.searchInputStringUpper = str;
                this.allowEmojisToAnimate = z2;
                this.selectedCategoryItemId = j;
                this.favoriteEmoji = set;
            }

            public final EmojiSet component1() {
                return this.emojiSet;
            }

            public final StoreEmoji.EmojiContext component2() {
                return this.emojiContext;
            }

            public final LinkedHashMap<Long, Guild> component3() {
                return this.allGuilds;
            }

            public final String component4() {
                return this.searchInputStringUpper;
            }

            public final boolean component5() {
                return this.allowEmojisToAnimate;
            }

            public final long component6() {
                return this.selectedCategoryItemId;
            }

            public final Set<com.discord.models.domain.emoji.Emoji> component7() {
                return this.favoriteEmoji;
            }

            public final Emoji copy(EmojiSet emojiSet, StoreEmoji.EmojiContext emojiContext, LinkedHashMap<Long, Guild> linkedHashMap, String str, boolean z2, long j, Set<? extends com.discord.models.domain.emoji.Emoji> set) {
                m.checkNotNullParameter(emojiSet, "emojiSet");
                m.checkNotNullParameter(emojiContext, "emojiContext");
                m.checkNotNullParameter(linkedHashMap, "allGuilds");
                m.checkNotNullParameter(str, "searchInputStringUpper");
                m.checkNotNullParameter(set, "favoriteEmoji");
                return new Emoji(emojiSet, emojiContext, linkedHashMap, str, z2, j, set);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Emoji)) {
                    return false;
                }
                Emoji emoji = (Emoji) obj;
                return m.areEqual(this.emojiSet, emoji.emojiSet) && m.areEqual(this.emojiContext, emoji.emojiContext) && m.areEqual(this.allGuilds, emoji.allGuilds) && m.areEqual(this.searchInputStringUpper, emoji.searchInputStringUpper) && this.allowEmojisToAnimate == emoji.allowEmojisToAnimate && this.selectedCategoryItemId == emoji.selectedCategoryItemId && m.areEqual(this.favoriteEmoji, emoji.favoriteEmoji);
            }

            public final LinkedHashMap<Long, Guild> getAllGuilds() {
                return this.allGuilds;
            }

            public final boolean getAllowEmojisToAnimate() {
                return this.allowEmojisToAnimate;
            }

            public final StoreEmoji.EmojiContext getEmojiContext() {
                return this.emojiContext;
            }

            public final EmojiSet getEmojiSet() {
                return this.emojiSet;
            }

            public final Set<com.discord.models.domain.emoji.Emoji> getFavoriteEmoji() {
                return this.favoriteEmoji;
            }

            public final String getSearchInputStringUpper() {
                return this.searchInputStringUpper;
            }

            public final long getSelectedCategoryItemId() {
                return this.selectedCategoryItemId;
            }

            public int hashCode() {
                EmojiSet emojiSet = this.emojiSet;
                int i = 0;
                int hashCode = (emojiSet != null ? emojiSet.hashCode() : 0) * 31;
                StoreEmoji.EmojiContext emojiContext = this.emojiContext;
                int hashCode2 = (hashCode + (emojiContext != null ? emojiContext.hashCode() : 0)) * 31;
                LinkedHashMap<Long, Guild> linkedHashMap = this.allGuilds;
                int hashCode3 = (hashCode2 + (linkedHashMap != null ? linkedHashMap.hashCode() : 0)) * 31;
                String str = this.searchInputStringUpper;
                int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
                boolean z2 = this.allowEmojisToAnimate;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int a = (a0.a.a.b.a(this.selectedCategoryItemId) + ((hashCode4 + i2) * 31)) * 31;
                Set<com.discord.models.domain.emoji.Emoji> set = this.favoriteEmoji;
                if (set != null) {
                    i = set.hashCode();
                }
                return a + i;
            }

            public String toString() {
                StringBuilder R = a.R("Emoji(emojiSet=");
                R.append(this.emojiSet);
                R.append(", emojiContext=");
                R.append(this.emojiContext);
                R.append(", allGuilds=");
                R.append(this.allGuilds);
                R.append(", searchInputStringUpper=");
                R.append(this.searchInputStringUpper);
                R.append(", allowEmojisToAnimate=");
                R.append(this.allowEmojisToAnimate);
                R.append(", selectedCategoryItemId=");
                R.append(this.selectedCategoryItemId);
                R.append(", favoriteEmoji=");
                R.append(this.favoriteEmoji);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: EmojiPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState$Uninitialized;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends StoreState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EmojiPickerViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\t\nB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0002\u000b\f¨\u0006\r"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;", "", "", "searchQuery", "Ljava/lang/String;", "getSearchQuery", "()Ljava/lang/String;", HookHelper.constructorName, "(Ljava/lang/String;)V", "EmptySearch", "Results", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$EmptySearch;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {
        private final String searchQuery;

        /* compiled from: EmojiPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0005\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$EmptySearch;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;", "", "component1", "()Ljava/lang/String;", "searchQuery", "copy", "(Ljava/lang/String;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$EmptySearch;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getSearchQuery", HookHelper.constructorName, "(Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmptySearch extends ViewState {
            private final String searchQuery;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmptySearch(String str) {
                super(str, null);
                m.checkNotNullParameter(str, "searchQuery");
                this.searchQuery = str;
            }

            public static /* synthetic */ EmptySearch copy$default(EmptySearch emptySearch, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = emptySearch.getSearchQuery();
                }
                return emptySearch.copy(str);
            }

            public final String component1() {
                return getSearchQuery();
            }

            public final EmptySearch copy(String str) {
                m.checkNotNullParameter(str, "searchQuery");
                return new EmptySearch(str);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof EmptySearch) && m.areEqual(getSearchQuery(), ((EmptySearch) obj).getSearchQuery());
                }
                return true;
            }

            @Override // com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.ViewState
            public String getSearchQuery() {
                return this.searchQuery;
            }

            public int hashCode() {
                String searchQuery = getSearchQuery();
                if (searchQuery != null) {
                    return searchQuery.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("EmptySearch(searchQuery=");
                R.append(getSearchQuery());
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: EmojiPickerViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\u0005¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0005HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ:\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\u0005HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0013\u0010\u001b\u001a\u00020\u00168F@\u0006¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u001c\u0010\u000b\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b\u001f\u0010\bR\u001f\u0010%\u001a\u0004\u0018\u00010 8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\t0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b&\u0010\b¨\u0006)"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "component2", "()Ljava/util/List;", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;", "component3", "searchQuery", "resultItems", "categoryItems", "copy", "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState$Results;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "getShowBottomBar", "()Z", "showBottomBar", "Ljava/lang/String;", "getSearchQuery", "Ljava/util/List;", "getResultItems", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;", "firstUnicodeEmojiCategoryItem$delegate", "Lkotlin/Lazy;", "getFirstUnicodeEmojiCategoryItem", "()Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem$StandardItem;", "firstUnicodeEmojiCategoryItem", "getCategoryItems", HookHelper.constructorName, "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Results extends ViewState {
            private final List<EmojiCategoryItem> categoryItems;
            private final Lazy firstUnicodeEmojiCategoryItem$delegate = g.lazy(new EmojiPickerViewModel$ViewState$Results$firstUnicodeEmojiCategoryItem$2(this));
            private final List<MGRecyclerDataPayload> resultItems;
            private final String searchQuery;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Results(String str, List<? extends MGRecyclerDataPayload> list, List<? extends EmojiCategoryItem> list2) {
                super(str, null);
                m.checkNotNullParameter(str, "searchQuery");
                m.checkNotNullParameter(list, "resultItems");
                m.checkNotNullParameter(list2, "categoryItems");
                this.searchQuery = str;
                this.resultItems = list;
                this.categoryItems = list2;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Results copy$default(Results results, String str, List list, List list2, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = results.getSearchQuery();
                }
                if ((i & 2) != 0) {
                    list = results.resultItems;
                }
                if ((i & 4) != 0) {
                    list2 = results.categoryItems;
                }
                return results.copy(str, list, list2);
            }

            public final String component1() {
                return getSearchQuery();
            }

            public final List<MGRecyclerDataPayload> component2() {
                return this.resultItems;
            }

            public final List<EmojiCategoryItem> component3() {
                return this.categoryItems;
            }

            public final Results copy(String str, List<? extends MGRecyclerDataPayload> list, List<? extends EmojiCategoryItem> list2) {
                m.checkNotNullParameter(str, "searchQuery");
                m.checkNotNullParameter(list, "resultItems");
                m.checkNotNullParameter(list2, "categoryItems");
                return new Results(str, list, list2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Results)) {
                    return false;
                }
                Results results = (Results) obj;
                return m.areEqual(getSearchQuery(), results.getSearchQuery()) && m.areEqual(this.resultItems, results.resultItems) && m.areEqual(this.categoryItems, results.categoryItems);
            }

            public final List<EmojiCategoryItem> getCategoryItems() {
                return this.categoryItems;
            }

            public final EmojiCategoryItem.StandardItem getFirstUnicodeEmojiCategoryItem() {
                return (EmojiCategoryItem.StandardItem) this.firstUnicodeEmojiCategoryItem$delegate.getValue();
            }

            public final List<MGRecyclerDataPayload> getResultItems() {
                return this.resultItems;
            }

            @Override // com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.ViewState
            public String getSearchQuery() {
                return this.searchQuery;
            }

            public final boolean getShowBottomBar() {
                return getSearchQuery().length() == 0;
            }

            public int hashCode() {
                String searchQuery = getSearchQuery();
                int i = 0;
                int hashCode = (searchQuery != null ? searchQuery.hashCode() : 0) * 31;
                List<MGRecyclerDataPayload> list = this.resultItems;
                int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
                List<EmojiCategoryItem> list2 = this.categoryItems;
                if (list2 != null) {
                    i = list2.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Results(searchQuery=");
                R.append(getSearchQuery());
                R.append(", resultItems=");
                R.append(this.resultItems);
                R.append(", categoryItems=");
                return a.K(R, this.categoryItems, ")");
            }
        }

        private ViewState(String str) {
            this.searchQuery = str;
        }

        public String getSearchQuery() {
            return this.searchQuery;
        }

        public /* synthetic */ ViewState(String str, DefaultConstructorMarker defaultConstructorMarker) {
            this(str);
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ EmojiPickerViewModel(com.discord.widgets.chat.input.emoji.EmojiPickerContextType r14, java.util.Locale r15, rx.subjects.BehaviorSubject r16, rx.subjects.BehaviorSubject r17, rx.Observable r18, com.discord.stores.StoreAnalytics r19, int r20, kotlin.jvm.internal.DefaultConstructorMarker r21) {
        /*
            r13 = this;
            r0 = r20 & 4
            if (r0 == 0) goto L10
            java.lang.String r0 = ""
            rx.subjects.BehaviorSubject r0 = rx.subjects.BehaviorSubject.l0(r0)
            java.lang.String r1 = "BehaviorSubject.create(\"\")"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            goto L12
        L10:
            r0 = r16
        L12:
            r1 = r20 & 8
            if (r1 == 0) goto L2c
            com.discord.widgets.chat.input.emoji.EmojiCategoryItem$Companion r1 = com.discord.widgets.chat.input.emoji.EmojiCategoryItem.Companion
            com.discord.models.domain.emoji.EmojiCategory r2 = com.discord.models.domain.emoji.EmojiCategory.FAVORITE
            long r1 = r1.mapEmojiCategoryToItemId(r2)
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            rx.subjects.BehaviorSubject r1 = rx.subjects.BehaviorSubject.l0(r1)
            java.lang.String r2 = "BehaviorSubject.create(\n…ojiCategory.FAVORITE)\n  )"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)
            goto L2e
        L2c:
            r1 = r17
        L2e:
            r2 = r20 & 16
            if (r2 == 0) goto L4a
            com.discord.widgets.chat.input.emoji.EmojiPickerViewModel$Companion r2 = com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.Companion
            r3 = 2
            r4 = 0
            r12 = r14
            rx.Observable r3 = com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.Companion.getEmojiContextObservable$default(r2, r14, r4, r3, r4)
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 120(0x78, float:1.68E-43)
            r11 = 0
            r4 = r0
            r5 = r1
            rx.Observable r2 = com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.Companion.observeStoreState$default(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            r7 = r2
            goto L4d
        L4a:
            r12 = r14
            r7 = r18
        L4d:
            r2 = r20 & 32
            if (r2 == 0) goto L59
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreAnalytics r2 = r2.getAnalytics()
            r8 = r2
            goto L5b
        L59:
            r8 = r19
        L5b:
            r2 = r13
            r3 = r14
            r4 = r15
            r5 = r0
            r6 = r1
            r2.<init>(r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.emoji.EmojiPickerViewModel.<init>(com.discord.widgets.chat.input.emoji.EmojiPickerContextType, java.util.Locale, rx.subjects.BehaviorSubject, rx.subjects.BehaviorSubject, rx.Observable, com.discord.stores.StoreAnalytics, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        LinkedHashMap<Long, Guild> linkedHashMap;
        List list;
        EmojiPickerViewModel emojiPickerViewModel;
        int i;
        LinkedHashMap<Long, Guild> linkedHashMap2;
        EmojiCategory emojiCategory;
        if (storeState instanceof StoreState.Emoji) {
            StoreState.Emoji emoji = (StoreState.Emoji) storeState;
            EmojiSet emojiSet = emoji.getEmojiSet();
            long selectedCategoryItemId = emoji.getSelectedCategoryItemId();
            boolean allowEmojisToAnimate = emoji.getAllowEmojisToAnimate();
            String searchInputStringUpper = emoji.getSearchInputStringUpper();
            Locale locale = this.locale;
            Objects.requireNonNull(searchInputStringUpper, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = searchInputStringUpper.toLowerCase(locale);
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            boolean z2 = lowerCase.length() > 0;
            ArrayList arrayList = new ArrayList();
            LinkedHashMap<Long, Guild> allGuilds = emoji.getAllGuilds();
            Collection<Guild> values = emoji.getAllGuilds().values();
            m.checkNotNullExpressionValue(values, "storeState.allGuilds.values");
            List list2 = u.toList(values);
            Collection<MGRecyclerDataPayload> linkedHashSet = z2 ? new LinkedHashSet() : new ArrayList();
            Collection linkedHashSet2 = z2 ? new LinkedHashSet() : new ArrayList();
            boolean z3 = !z2;
            Companion companion = Companion;
            Companion.EmojiItems buildEmojiListItems = companion.buildEmojiListItems(emoji.getFavoriteEmoji(), new EmojiPickerViewModel$handleStoreState$favoriteItems$1(allGuilds), lowerCase, allowEmojisToAnimate, z2, z3);
            if (buildEmojiListItems instanceof Companion.EmojiItems.Partitioned) {
                Companion.EmojiItems.Partitioned partitioned = (Companion.EmojiItems.Partitioned) buildEmojiListItems;
                linkedHashSet.addAll(partitioned.getRegularItems());
                linkedHashSet2.addAll(partitioned.getPremiumItems());
            } else if (buildEmojiListItems instanceof Companion.EmojiItems.Regular) {
                Companion.EmojiItems.Regular regular = (Companion.EmojiItems.Regular) buildEmojiListItems;
                if (!regular.getItems().isEmpty()) {
                    int size = linkedHashSet.size();
                    if (!z2) {
                        linkedHashSet.add(new WidgetEmojiAdapter.HeaderItem.StandardHeaderItem(EmojiCategory.FAVORITE));
                    }
                    linkedHashSet.addAll(regular.getItems());
                    EmojiCategoryItem.Companion companion2 = EmojiCategoryItem.Companion;
                    EmojiCategory emojiCategory2 = EmojiCategory.FAVORITE;
                    arrayList.add(new EmojiCategoryItem.StandardItem(emojiCategory2, new Pair(Integer.valueOf(size), Integer.valueOf(linkedHashSet.size())), companion2.mapEmojiCategoryToItemId(emojiCategory2) == selectedCategoryItemId));
                }
            }
            Companion.EmojiItems buildEmojiListItems2 = companion.buildEmojiListItems(emojiSet.recentEmojis, new EmojiPickerViewModel$handleStoreState$frequentItems$1(allGuilds), lowerCase, allowEmojisToAnimate, z2, z3);
            if (buildEmojiListItems2 instanceof Companion.EmojiItems.Partitioned) {
                Companion.EmojiItems.Partitioned partitioned2 = (Companion.EmojiItems.Partitioned) buildEmojiListItems2;
                linkedHashSet.addAll(partitioned2.getRegularItems());
                linkedHashSet2.addAll(partitioned2.getPremiumItems());
            } else if (buildEmojiListItems2 instanceof Companion.EmojiItems.Regular) {
                Companion.EmojiItems.Regular regular2 = (Companion.EmojiItems.Regular) buildEmojiListItems2;
                if (!regular2.getItems().isEmpty()) {
                    int size2 = linkedHashSet.size();
                    if (!z2) {
                        linkedHashSet.add(new WidgetEmojiAdapter.HeaderItem.StandardHeaderItem(EmojiCategory.RECENT));
                    }
                    linkedHashSet.addAll(regular2.getItems());
                    EmojiCategoryItem.Companion companion3 = EmojiCategoryItem.Companion;
                    EmojiCategory emojiCategory3 = EmojiCategory.RECENT;
                    arrayList.add(new EmojiCategoryItem.StandardItem(emojiCategory3, new Pair(Integer.valueOf(size2), Integer.valueOf(linkedHashSet.size())), companion3.mapEmojiCategoryToItemId(emojiCategory3) == selectedCategoryItemId));
                }
            }
            StoreEmoji.EmojiContext emojiContext = emoji.getEmojiContext();
            long guildId = emojiContext instanceof StoreEmoji.EmojiContext.Chat ? ((StoreEmoji.EmojiContext.Chat) emojiContext).getGuildId() : 0L;
            Guild guild = emoji.getAllGuilds().get(Long.valueOf(guildId));
            List<Emoji> list3 = emojiSet.customEmojis.get(Long.valueOf(guildId));
            if (guild == null || list3 == null || !(!list3.isEmpty())) {
                linkedHashMap = allGuilds;
                list = list2;
            } else {
                linkedHashMap = allGuilds;
                Companion.EmojiItems buildGuildEmojiListItems = companion.buildGuildEmojiListItems(guild, emojiSet, lowerCase, allowEmojisToAnimate, z2, z3);
                if (buildGuildEmojiListItems instanceof Companion.EmojiItems.Partitioned) {
                    Companion.EmojiItems.Partitioned partitioned3 = (Companion.EmojiItems.Partitioned) buildGuildEmojiListItems;
                    linkedHashSet.addAll(partitioned3.getRegularItems());
                    linkedHashSet2.addAll(partitioned3.getPremiumItems());
                } else if (buildGuildEmojiListItems instanceof Companion.EmojiItems.Regular) {
                    Companion.EmojiItems.Regular regular3 = (Companion.EmojiItems.Regular) buildGuildEmojiListItems;
                    if (!regular3.getItems().isEmpty()) {
                        int size3 = linkedHashSet.size();
                        if (!z2) {
                            linkedHashSet.add(new WidgetEmojiAdapter.HeaderItem.GuildHeaderItem(guild));
                        }
                        linkedHashSet.addAll(regular3.getItems());
                        arrayList.add(new EmojiCategoryItem.GuildItem(guild, new Pair(Integer.valueOf(size3), Integer.valueOf(linkedHashSet.size())), EmojiCategoryItem.Companion.mapGuildToItemId(guild) == selectedCategoryItemId));
                    }
                }
                list = u.minus(list2, guild);
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Guild guild2 = (Guild) it.next();
                it = it;
                Companion.EmojiItems buildGuildEmojiListItems2 = Companion.buildGuildEmojiListItems(guild2, emojiSet, lowerCase, allowEmojisToAnimate, z2, z3);
                if (buildGuildEmojiListItems2 instanceof Companion.EmojiItems.Partitioned) {
                    Companion.EmojiItems.Partitioned partitioned4 = (Companion.EmojiItems.Partitioned) buildGuildEmojiListItems2;
                    linkedHashSet.addAll(partitioned4.getRegularItems());
                    linkedHashSet2.addAll(partitioned4.getPremiumItems());
                } else if (buildGuildEmojiListItems2 instanceof Companion.EmojiItems.Regular) {
                    Companion.EmojiItems.Regular regular4 = (Companion.EmojiItems.Regular) buildGuildEmojiListItems2;
                    if (!regular4.getItems().isEmpty()) {
                        int size4 = linkedHashSet.size();
                        if (!z2) {
                            m.checkNotNullExpressionValue(guild2, "guild");
                            linkedHashSet.add(new WidgetEmojiAdapter.HeaderItem.GuildHeaderItem(guild2));
                        }
                        linkedHashSet.addAll(regular4.getItems());
                        EmojiCategoryItem.Companion companion4 = EmojiCategoryItem.Companion;
                        m.checkNotNullExpressionValue(guild2, "guild");
                        arrayList.add(new EmojiCategoryItem.GuildItem(guild2, new Pair(Integer.valueOf(size4), Integer.valueOf(linkedHashSet.size())), companion4.mapGuildToItemId(guild2) == selectedCategoryItemId));
                    }
                }
            }
            Map<EmojiCategory, List<Emoji>> map = emojiSet.unicodeEmojis;
            EmojiCategory[] values2 = EmojiCategory.values();
            int i2 = 0;
            for (int i3 = 11; i2 < i3; i3 = 11) {
                EmojiCategory emojiCategory4 = values2[i2];
                if (!map.containsKey(emojiCategory4)) {
                    i = i2;
                    linkedHashMap2 = linkedHashMap;
                } else {
                    int size5 = linkedHashSet.size();
                    LinkedHashMap<Long, Guild> linkedHashMap3 = linkedHashMap;
                    linkedHashMap2 = linkedHashMap3;
                    i = i2;
                    Companion.EmojiItems buildEmojiListItems3 = Companion.buildEmojiListItems(map.get(emojiCategory4), new EmojiPickerViewModel$handleStoreState$unicodeCategoryItems$1(linkedHashMap3), lowerCase, allowEmojisToAnimate, false, false);
                    if (buildEmojiListItems3 instanceof Companion.EmojiItems.Regular) {
                        Companion.EmojiItems.Regular regular5 = (Companion.EmojiItems.Regular) buildEmojiListItems3;
                        if (!regular5.getItems().isEmpty()) {
                            if (!z2) {
                                emojiCategory = emojiCategory4;
                                linkedHashSet.add(new WidgetEmojiAdapter.HeaderItem.StandardHeaderItem(emojiCategory));
                            } else {
                                emojiCategory = emojiCategory4;
                            }
                            linkedHashSet.addAll(regular5.getItems());
                            arrayList.add(new EmojiCategoryItem.StandardItem(emojiCategory, new Pair(Integer.valueOf(size5), Integer.valueOf(linkedHashSet.size())), EmojiCategoryItem.Companion.mapEmojiCategoryToItemId(emojiCategory) == selectedCategoryItemId));
                        }
                    }
                }
                i2 = i + 1;
                linkedHashMap = linkedHashMap2;
            }
            if (z2 && (!linkedHashSet2.isEmpty())) {
                linkedHashSet.add(new WidgetEmojiAdapter.HeaderItem.StringHeaderItem(R.string.emoji_available_with_premium));
                linkedHashSet.addAll(linkedHashSet2);
                linkedHashSet.add(WidgetEmojiAdapter.UpsellItem.INSTANCE);
            }
            if (z2) {
                emojiPickerViewModel = this;
                emojiPickerViewModel.logWhenUpsellHeaderIsViewed = true;
                int i4 = 0;
                int i5 = 0;
                for (MGRecyclerDataPayload mGRecyclerDataPayload : linkedHashSet) {
                    if (mGRecyclerDataPayload instanceof WidgetEmojiAdapter.EmojiItem) {
                        i4++;
                        if (!((WidgetEmojiAdapter.EmojiItem) mGRecyclerDataPayload).getEmoji().isUsable()) {
                            i5++;
                        }
                    }
                }
                if (i4 > 0) {
                    emojiPickerViewModel.storeAnalytics.trackSearchResultViewed(SearchType.EMOJI, i4, Integer.valueOf(i5), emojiPickerViewModel.storeAnalytics.getEmojiPickerUpsellLocation(), true);
                } else {
                    StoreAnalytics storeAnalytics = emojiPickerViewModel.storeAnalytics;
                    storeAnalytics.trackSearchResultsEmpty(SearchType.EMOJI, storeAnalytics.getEmojiPickerUpsellLocation(), true);
                }
            } else {
                emojiPickerViewModel = this;
            }
            if (!linkedHashSet.isEmpty() || !z2) {
                List list4 = (List) (!(linkedHashSet instanceof List) ? null : linkedHashSet);
                if (list4 == null) {
                    list4 = u.toList(linkedHashSet);
                }
                emojiPickerViewModel.updateViewState(new ViewState.Results(emoji.getSearchInputStringUpper(), list4, arrayList));
                return;
            }
            emojiPickerViewModel.updateViewState(new ViewState.EmptySearch(emoji.getSearchInputStringUpper()));
        }
    }

    public final Observable<Event> observeEvents() {
        return this.eventSubject;
    }

    public final void onClickUnicodeEmojiCategories() {
        EmojiCategoryItem.StandardItem firstUnicodeEmojiCategoryItem;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Results)) {
            viewState = null;
        }
        ViewState.Results results = (ViewState.Results) viewState;
        if (results != null && (firstUnicodeEmojiCategoryItem = results.getFirstUnicodeEmojiCategoryItem()) != null) {
            setSelectedCategoryItemId(firstUnicodeEmojiCategoryItem.getStableId());
            this.eventSubject.k.onNext(new Event.ScrollToEmojiListPosition(firstUnicodeEmojiCategoryItem.getCategoryRange().getFirst().intValue()));
        }
    }

    public final void onEmojiSelected(Emoji emoji, Function1<? super Emoji, Unit> function1) {
        int i;
        m.checkNotNullParameter(emoji, "emoji");
        m.checkNotNullParameter(function1, "validEmojiSelected");
        ViewState viewState = getViewState();
        List<MGRecyclerDataPayload> list = null;
        if (!(viewState instanceof ViewState.Results)) {
            viewState = null;
        }
        ViewState.Results results = (ViewState.Results) viewState;
        if (results != null) {
            list = results.getResultItems();
        }
        if (list == null) {
            list = n.emptyList();
        }
        boolean z2 = false;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            int i2 = 0;
            for (MGRecyclerDataPayload mGRecyclerDataPayload : list) {
                if ((mGRecyclerDataPayload instanceof WidgetEmojiAdapter.EmojiItem) && (i2 = i2 + 1) < 0) {
                    n.throwCountOverflow();
                }
            }
            i = i2;
        } else {
            i = 0;
        }
        StoreAnalytics storeAnalytics = this.storeAnalytics;
        StoreAnalytics.trackSearchResultSelected$default(storeAnalytics, SearchType.EMOJI, i, Traits.Location.copy$default(storeAnalytics.getEmojiPickerUpsellLocation(), null, null, "Search Result", null, null, 27, null), null, 8, null);
        if (!emoji.isUsable()) {
            if ((emoji instanceof ModelEmojiCustom) && ((ModelEmojiCustom) emoji).isAnimated()) {
                z2 = true;
            }
            this.eventSubject.k.onNext(new Event.ShowPremiumUpsellDialog(z2 ? 2 : 1, z2 ? R.string.premium_upsell_animated_emojis_active_mobile : R.string.premium_upsell_emoji_active_mobile, z2 ? R.string.premium_upsell_animated_emojis_description_mobile : R.string.premium_upsell_emoji_description_mobile, Traits.Location.Section.EMOJI_PICKER_POPOUT, false, false));
            this.storeAnalytics.emojiPickerUpsellLockedItemClicked(this.emojiPickerContextType, z2);
        } else if (emoji.isAvailable()) {
            function1.invoke(emoji);
        }
    }

    public final void onUpsellHeaderVisible() {
        this.storeAnalytics.emojiPickerUpsellHeaderViewed(this.emojiPickerContextType);
        this.logWhenUpsellHeaderIsViewed = false;
    }

    public final void setSearchText(String str) {
        m.checkNotNullParameter(str, "searchText");
        this.searchSubject.onNext(str);
        StoreAnalytics storeAnalytics = this.storeAnalytics;
        storeAnalytics.trackSearchStarted(SearchType.EMOJI, storeAnalytics.getEmojiPickerUpsellLocation(), true);
    }

    public final void setSelectedCategoryItemId(long j) {
        this.selectedCategoryItemIdSubject.onNext(Long.valueOf(j));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EmojiPickerViewModel(EmojiPickerContextType emojiPickerContextType, Locale locale, BehaviorSubject<String> behaviorSubject, BehaviorSubject<Long> behaviorSubject2, Observable<StoreState> observable, StoreAnalytics storeAnalytics) {
        super(null);
        m.checkNotNullParameter(emojiPickerContextType, "emojiPickerContextType");
        m.checkNotNullParameter(locale, "locale");
        m.checkNotNullParameter(behaviorSubject, "searchSubject");
        m.checkNotNullParameter(behaviorSubject2, "selectedCategoryItemIdSubject");
        m.checkNotNullParameter(observable, "storeStateObservable");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        this.emojiPickerContextType = emojiPickerContextType;
        this.locale = locale;
        this.searchSubject = behaviorSubject;
        this.selectedCategoryItemIdSubject = behaviorSubject2;
        this.storeAnalytics = storeAnalytics;
        PublishSubject<Event> k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.eventSubject = k0;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
