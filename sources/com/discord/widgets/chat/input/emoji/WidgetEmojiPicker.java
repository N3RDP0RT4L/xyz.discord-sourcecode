package com.discord.widgets.chat.input.emoji;

import andhook.lib.HookHelper;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.a.b.c;
import b.a.d.e0;
import b.a.k.b;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetEmojiPickerBinding;
import com.discord.models.domain.emoji.Emoji;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.press.RepeatingOnTouchListener;
import com.discord.utilities.recycler.SelfHealingLinearLayoutManager;
import com.discord.utilities.rx.LeadingEdgeThrottle;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.TextWatcherKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.StickyHeaderItemDecoration;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.widgets.chat.input.OnBackspacePressedListener;
import com.discord.widgets.chat.input.emoji.EmojiCategoryItem;
import com.discord.widgets.chat.input.emoji.EmojiPickerViewModel;
import com.discord.widgets.chat.input.emoji.WidgetEmojiAdapter;
import com.discord.widgets.settings.premium.WidgetSettingsPremium;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import d0.k;
import d0.l;
import d0.w.i.a.e;
import d0.z.d.a0;
import d0.z.d.m;
import j0.l.a.r;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import kotlin.reflect.KProperty;
import kotlinx.coroutines.CoroutineScope;
import rx.Observable;
import rx.functions.Action0;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetEmojiPicker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ú\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u0082\u00012\u00020\u00012\u00020\u0002:\u0002\u0082\u0001B\b¢\u0006\u0005\b\u0081\u0001\u0010\u0011J\u0019\u0010\u0006\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u001e\u0010\u0011J\u0017\u0010!\u001a\u00020\u00052\u0006\u0010 \u001a\u00020\u001fH\u0002¢\u0006\u0004\b!\u0010\"J\u000f\u0010#\u001a\u00020\u0005H\u0002¢\u0006\u0004\b#\u0010\u0011J\u000f\u0010$\u001a\u00020\u0005H\u0002¢\u0006\u0004\b$\u0010\u0011J\u0019\u0010%\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0002¢\u0006\u0004\b%\u0010\u0007J\u000f\u0010&\u001a\u00020\u0005H\u0002¢\u0006\u0004\b&\u0010\u0011J\u000f\u0010'\u001a\u00020\u0005H\u0002¢\u0006\u0004\b'\u0010\u0011J%\u0010+\u001a\u00020\u00052\u0006\u0010(\u001a\u00020\u00162\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00120)H\u0002¢\u0006\u0004\b+\u0010,J\u0017\u0010/\u001a\u00020\u00052\u0006\u0010.\u001a\u00020-H\u0002¢\u0006\u0004\b/\u00100J\u000f\u00101\u001a\u00020\u0005H\u0002¢\u0006\u0004\b1\u0010\u0011J\u000f\u00103\u001a\u000202H\u0002¢\u0006\u0004\b3\u00104J\u000f\u00105\u001a\u00020\u0005H\u0002¢\u0006\u0004\b5\u0010\u0011J\u000f\u00106\u001a\u00020\u0016H\u0002¢\u0006\u0004\b6\u00107J\u0017\u0010:\u001a\u00020\u00052\b\u00109\u001a\u0004\u0018\u000108¢\u0006\u0004\b:\u0010;J\u0017\u0010>\u001a\u00020\u00052\b\u0010=\u001a\u0004\u0018\u00010<¢\u0006\u0004\b>\u0010?J\u0019\u0010B\u001a\u00020\u00052\b\u0010A\u001a\u0004\u0018\u00010@H\u0016¢\u0006\u0004\bB\u0010CJ\u0017\u0010F\u001a\u00020\u00052\u0006\u0010E\u001a\u00020DH\u0016¢\u0006\u0004\bF\u0010GJ\u000f\u0010H\u001a\u00020\u0005H\u0016¢\u0006\u0004\bH\u0010\u0011J\r\u0010I\u001a\u00020\u0005¢\u0006\u0004\bI\u0010\u0011J\u0017\u0010L\u001a\u00020\u00052\u0006\u0010K\u001a\u00020JH\u0016¢\u0006\u0004\bL\u0010MJ\r\u0010N\u001a\u00020\u0005¢\u0006\u0004\bN\u0010\u0011R\u0016\u0010P\u001a\u00020O8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bP\u0010QR\u0016\u0010U\u001a\u00020R8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bS\u0010TR\u0018\u0010V\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bV\u0010WR\u001d\u0010]\u001a\u00020X8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bY\u0010Z\u001a\u0004\b[\u0010\\R\u0016\u0010_\u001a\u00020^8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b_\u0010`R\u0018\u0010a\u001a\u0004\u0018\u0001028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\ba\u0010bR:\u0010e\u001a&\u0012\f\u0012\n d*\u0004\u0018\u00010\u00050\u0005 d*\u0012\u0012\f\u0012\n d*\u0004\u0018\u00010\u00050\u0005\u0018\u00010c0c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\be\u0010fR\u001d\u0010l\u001a\u00020g8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bh\u0010i\u001a\u0004\bj\u0010kR\u0016\u0010p\u001a\u00020m8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bn\u0010oR\u0016\u0010r\u001a\u00020q8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\br\u0010sR\u0016\u0010t\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bt\u0010uR\u001e\u0010w\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010v8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bw\u0010xR\u0018\u00109\u001a\u0004\u0018\u0001088\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b9\u0010yR\u001d\u0010~\u001a\u00020z8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b{\u0010Z\u001a\u0004\b|\u0010}R\u0016\u0010\u007f\u001a\u00020\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u007f\u0010uR\u0019\u0010=\u001a\u0004\u0018\u00010<8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b=\u0010\u0080\u0001¨\u0006\u0083\u0001"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker;", "Lcom/discord/widgets/chat/input/emoji/OnEmojiSelectedListener;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;)V", "", "visible", "setEmojiPickerBottomBarVisible", "(Z)V", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Event;)V", "onPremiumCtaClicked", "()V", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;", "emojiCategoryItem", "onCategoryClicked", "(Lcom/discord/widgets/chat/input/emoji/EmojiCategoryItem;)V", "", "selectedCategoryPosition", "onSelectedCategoryAdapterPositionUpdated", "(I)V", "", "itemId", "selectCategoryByItemId", "(J)V", "setUpEmojiRecycler", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "checkUpsellHeaderVisibility", "(Landroidx/recyclerview/widget/RecyclerView;)V", "setUpCategoryRecycler", "subscribeToCategoryRecyclerScrolls", "configureUnicodeCategoriesShortcutButton", "initializeSearchBar", "initializeInputButtons", "emojiRecyclerScrollPosition", "", "emojiCategoryItems", "handleNewEmojiRecyclerScrollPosition", "(ILjava/util/List;)V", "", "input", "handleInputChanged", "(Ljava/lang/String;)V", "setWindowInsetsListeners", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;", "getMode", "()Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;", "launchBottomSheet", "getAdditionalBottomPaddingPx", "()I", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;", "emojiPickerListener", "setListener", "(Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;)V", "Lcom/discord/widgets/chat/input/OnBackspacePressedListener;", "onBackspacePressedListener", "setOnBackspacePressedListener", "(Lcom/discord/widgets/chat/input/OnBackspacePressedListener;)V", "Landroid/os/Bundle;", "savedInstanceState", "onCreate", "(Landroid/os/Bundle;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "clearSearchInput", "Lcom/discord/models/domain/emoji/Emoji;", "emoji", "onEmojiSelected", "(Lcom/discord/models/domain/emoji/Emoji;)V", "scrollToTop", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;", "categoryAdapter", "Lcom/discord/widgets/chat/input/emoji/EmojiCategoryAdapter;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;", "getViewModel", "()Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel;", "viewModel", "previousViewState", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$ViewState;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Sheet;", "viewModelForSheet$delegate", "Lkotlin/Lazy;", "getViewModelForSheet", "()Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Sheet;", "viewModelForSheet", "Landroidx/recyclerview/widget/LinearLayoutManager;", "categoryLayoutManager", "Landroidx/recyclerview/widget/LinearLayoutManager;", "emojiPickerMode", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerMode;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "emojiCategoryScrollSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/databinding/WidgetEmojiPickerBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetEmojiPickerBinding;", "binding", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;", "getEmojiPickerContextType", "()Lcom/discord/widgets/chat/input/emoji/EmojiPickerContextType;", "emojiPickerContextType", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;", "emojiAdapter", "Lcom/discord/widgets/chat/input/emoji/WidgetEmojiAdapter;", "isNextCategoryScrollSmooth", "Z", "Lkotlin/Function0;", "onEmojiSearchOpenedListener", "Lkotlin/jvm/functions/Function0;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;", "Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Inline;", "viewModelForInline$delegate", "getViewModelForInline", "()Lcom/discord/widgets/chat/input/emoji/EmojiPickerViewModel$Inline;", "viewModelForInline", "restoredSearchQueryFromViewModel", "Lcom/discord/widgets/chat/input/OnBackspacePressedListener;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEmojiPicker extends AppFragment implements OnEmojiSelectedListener {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetEmojiPicker.class, "binding", "getBinding()Lcom/discord/databinding/WidgetEmojiPickerBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int EMOJI_PICKER_VIEW_FLIPPER_EMPTY_STATE = 1;
    private static final int EMOJI_PICKER_VIEW_FLIPPER_RESULTS = 0;
    private EmojiCategoryAdapter categoryAdapter;
    private LinearLayoutManager categoryLayoutManager;
    private WidgetEmojiAdapter emojiAdapter;
    private EmojiPickerListener emojiPickerListener;
    private EmojiPickerMode emojiPickerMode;
    private OnBackspacePressedListener onBackspacePressedListener;
    private Function0<Unit> onEmojiSearchOpenedListener;
    private EmojiPickerViewModel.ViewState previousViewState;
    private boolean restoredSearchQueryFromViewModel;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetEmojiPicker$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModelForInline$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(EmojiPickerViewModel.Inline.class), new WidgetEmojiPicker$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(new WidgetEmojiPicker$viewModelForInline$2(this)));
    private final Lazy viewModelForSheet$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(EmojiPickerViewModel.Sheet.class), new WidgetEmojiPicker$appActivityViewModels$$inlined$activityViewModels$3(this), new e0(new WidgetEmojiPicker$viewModelForSheet$2(this)));
    private final PublishSubject<Unit> emojiCategoryScrollSubject = PublishSubject.k0();
    private boolean isNextCategoryScrollSmooth = true;

    /* compiled from: WidgetEmojiPicker.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/input/emoji/WidgetEmojiPicker$Companion;", "", "", "EMOJI_PICKER_VIEW_FLIPPER_EMPTY_STATE", "I", "EMOJI_PICKER_VIEW_FLIPPER_RESULTS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetEmojiPicker() {
        super(R.layout.widget_emoji_picker);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void checkUpsellHeaderVisibility(RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        Objects.requireNonNull(layoutManager, "null cannot be cast to non-null type androidx.recyclerview.widget.GridLayoutManager");
        GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
        int findFirstCompletelyVisibleItemPosition = gridLayoutManager.findFirstCompletelyVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = gridLayoutManager.findLastCompletelyVisibleItemPosition();
        if (findLastCompletelyVisibleItemPosition >= findFirstCompletelyVisibleItemPosition) {
            while (true) {
                RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(findLastCompletelyVisibleItemPosition);
                WidgetEmojiAdapter.HeaderItem.StringHeaderItem stringHeaderItem = null;
                if (!(findViewHolderForAdapterPosition instanceof WidgetEmojiAdapter.HeaderViewHolder)) {
                    findViewHolderForAdapterPosition = null;
                }
                WidgetEmojiAdapter.HeaderViewHolder headerViewHolder = (WidgetEmojiAdapter.HeaderViewHolder) findViewHolderForAdapterPosition;
                if (headerViewHolder != null) {
                    WidgetEmojiAdapter.HeaderItem boundItem = headerViewHolder.getBoundItem();
                    if (boundItem instanceof WidgetEmojiAdapter.HeaderItem.StringHeaderItem) {
                        stringHeaderItem = boundItem;
                    }
                    WidgetEmojiAdapter.HeaderItem.StringHeaderItem stringHeaderItem2 = stringHeaderItem;
                    if (stringHeaderItem2 != null && stringHeaderItem2.getStringRes() == R.string.emoji_available_with_premium) {
                        getViewModel().onUpsellHeaderVisible();
                    }
                }
                if (findLastCompletelyVisibleItemPosition != findFirstCompletelyVisibleItemPosition) {
                    findLastCompletelyVisibleItemPosition--;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(EmojiPickerViewModel.ViewState viewState) {
        Unit unit;
        String searchQuery = viewState != null ? viewState.getSearchQuery() : null;
        if (!this.restoredSearchQueryFromViewModel && searchQuery != null) {
            this.restoredSearchQueryFromViewModel = true;
            getBinding().m.setText(searchQuery);
        }
        if (viewState != null) {
            if (viewState instanceof EmojiPickerViewModel.ViewState.EmptySearch) {
                AppViewFlipper appViewFlipper = getBinding().c;
                m.checkNotNullExpressionValue(appViewFlipper, "binding.chatInputEmojiPickerViewFlipper");
                appViewFlipper.setDisplayedChild(1);
                setEmojiPickerBottomBarVisible(false);
                unit = Unit.a;
            } else if (viewState instanceof EmojiPickerViewModel.ViewState.Results) {
                EmojiPickerViewModel.ViewState.Results results = (EmojiPickerViewModel.ViewState.Results) viewState;
                setEmojiPickerBottomBarVisible(results.getShowBottomBar());
                AppViewFlipper appViewFlipper2 = getBinding().c;
                m.checkNotNullExpressionValue(appViewFlipper2, "binding.chatInputEmojiPickerViewFlipper");
                appViewFlipper2.setDisplayedChild(0);
                WidgetEmojiAdapter widgetEmojiAdapter = this.emojiAdapter;
                if (widgetEmojiAdapter == null) {
                    m.throwUninitializedPropertyAccessException("emojiAdapter");
                }
                widgetEmojiAdapter.setData(results.getResultItems());
                WidgetEmojiAdapter widgetEmojiAdapter2 = this.emojiAdapter;
                if (widgetEmojiAdapter2 == null) {
                    m.throwUninitializedPropertyAccessException("emojiAdapter");
                }
                widgetEmojiAdapter2.setOnScrollPositionListener(new WidgetEmojiPicker$configureUI$1(this, viewState));
                EmojiCategoryAdapter emojiCategoryAdapter = this.categoryAdapter;
                if (emojiCategoryAdapter == null) {
                    m.throwUninitializedPropertyAccessException("categoryAdapter");
                }
                emojiCategoryAdapter.setItems(results.getCategoryItems());
                configureUnicodeCategoriesShortcutButton(viewState);
                unit = Unit.a;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            KotlinExtensionsKt.getExhaustive(unit);
            this.previousViewState = viewState;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUnicodeCategoriesShortcutButton(EmojiPickerViewModel.ViewState viewState) {
        if (!(viewState instanceof EmojiPickerViewModel.ViewState.Results)) {
            viewState = null;
        }
        EmojiPickerViewModel.ViewState.Results results = (EmojiPickerViewModel.ViewState.Results) viewState;
        if (results != null) {
            EmojiCategoryItem.StandardItem firstUnicodeEmojiCategoryItem = results.getFirstUnicodeEmojiCategoryItem();
            if (firstUnicodeEmojiCategoryItem != null) {
                getBinding().k.setImageDrawable(ContextCompat.getDrawable(requireContext(), EmojiCategoryViewHolder.Companion.getCategoryIconResId(firstUnicodeEmojiCategoryItem.getEmojiCategory())));
            }
            LinearLayoutManager linearLayoutManager = this.categoryLayoutManager;
            if (linearLayoutManager == null) {
                m.throwUninitializedPropertyAccessException("categoryLayoutManager");
            }
            int findFirstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
            LinearLayoutManager linearLayoutManager2 = this.categoryLayoutManager;
            if (linearLayoutManager2 == null) {
                m.throwUninitializedPropertyAccessException("categoryLayoutManager");
            }
            int findLastCompletelyVisibleItemPosition = linearLayoutManager2.findLastCompletelyVisibleItemPosition();
            if (findFirstCompletelyVisibleItemPosition != -1 && findLastCompletelyVisibleItemPosition != -1) {
                EmojiCategoryAdapter emojiCategoryAdapter = this.categoryAdapter;
                if (emojiCategoryAdapter == null) {
                    m.throwUninitializedPropertyAccessException("categoryAdapter");
                }
                if (findLastCompletelyVisibleItemPosition < emojiCategoryAdapter.getItemCount()) {
                    EmojiCategoryAdapter emojiCategoryAdapter2 = this.categoryAdapter;
                    if (emojiCategoryAdapter2 == null) {
                        m.throwUninitializedPropertyAccessException("categoryAdapter");
                    }
                    EmojiCategoryItem itemAtPosition = emojiCategoryAdapter2.getItemAtPosition(findFirstCompletelyVisibleItemPosition);
                    EmojiCategoryAdapter emojiCategoryAdapter3 = this.categoryAdapter;
                    if (emojiCategoryAdapter3 == null) {
                        m.throwUninitializedPropertyAccessException("categoryAdapter");
                    }
                    EmojiCategoryItem itemAtPosition2 = emojiCategoryAdapter3.getItemAtPosition(findLastCompletelyVisibleItemPosition);
                    boolean z2 = true;
                    int i = 0;
                    boolean z3 = itemAtPosition.containsOnlyUnicodeEmoji() || itemAtPosition2.containsOnlyUnicodeEmoji();
                    ImageView imageView = getBinding().k;
                    m.checkNotNullExpressionValue(imageView, "binding.emojiPickerUnicodeEmojiShortcutButton");
                    if (z3 || firstUnicodeEmojiCategoryItem == null) {
                        z2 = false;
                    }
                    if (!z2) {
                        i = 8;
                    }
                    imageView.setVisibility(i);
                }
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final int getAdditionalBottomPaddingPx() {
        if (Build.VERSION.SDK_INT >= 29) {
            return DimenUtils.dpToPixels(8);
        }
        return 0;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetEmojiPickerBinding getBinding() {
        return (WidgetEmojiPickerBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final EmojiPickerContextType getEmojiPickerContextType() {
        Bundle arguments = getArguments();
        Serializable serializable = arguments != null ? arguments.getSerializable(EmojiPickerNavigator.ARG_EMOJI_PICKER_CONTEXT_TYPE) : null;
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type com.discord.widgets.chat.input.emoji.EmojiPickerContextType");
        return (EmojiPickerContextType) serializable;
    }

    private final EmojiPickerMode getMode() {
        Bundle arguments = getArguments();
        EmojiPickerMode emojiPickerMode = null;
        Serializable serializable = arguments != null ? arguments.getSerializable("MODE") : null;
        if (serializable instanceof EmojiPickerMode) {
            emojiPickerMode = serializable;
        }
        EmojiPickerMode emojiPickerMode2 = emojiPickerMode;
        return emojiPickerMode2 != null ? emojiPickerMode2 : EmojiPickerMode.INLINE;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final EmojiPickerViewModel getViewModel() {
        if (getMode() == EmojiPickerMode.INLINE) {
            return getViewModelForInline();
        }
        return getViewModelForSheet();
    }

    private final EmojiPickerViewModel.Inline getViewModelForInline() {
        return (EmojiPickerViewModel.Inline) this.viewModelForInline$delegate.getValue();
    }

    private final EmojiPickerViewModel.Sheet getViewModelForSheet() {
        return (EmojiPickerViewModel.Sheet) this.viewModelForSheet$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(EmojiPickerViewModel.Event event) {
        Unit unit;
        CharSequence e;
        CharSequence e2;
        if (event instanceof EmojiPickerViewModel.Event.ScrollToEmojiListPosition) {
            WidgetEmojiAdapter widgetEmojiAdapter = this.emojiAdapter;
            if (widgetEmojiAdapter == null) {
                m.throwUninitializedPropertyAccessException("emojiAdapter");
            }
            widgetEmojiAdapter.scrollToPosition(((EmojiPickerViewModel.Event.ScrollToEmojiListPosition) event).getPosition());
            unit = Unit.a;
        } else if (event instanceof EmojiPickerViewModel.Event.ShowPremiumUpsellDialog) {
            c.b bVar = c.k;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            EmojiPickerViewModel.Event.ShowPremiumUpsellDialog showPremiumUpsellDialog = (EmojiPickerViewModel.Event.ShowPremiumUpsellDialog) event;
            int pageNumber = showPremiumUpsellDialog.getPageNumber();
            e = b.e(this, showPremiumUpsellDialog.getHeader(), new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            String obj = e.toString();
            e2 = b.e(this, showPremiumUpsellDialog.getBody(), new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            c.b.a(bVar, parentFragmentManager, pageNumber, obj, e2.toString(), null, showPremiumUpsellDialog.getSectionName(), null, null, showPremiumUpsellDialog.getShowOtherPages(), showPremiumUpsellDialog.getShowLearnMore(), 208);
            unit = Unit.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleInputChanged(String str) {
        ColorStateList colorStateList;
        CharSequence e;
        CharSequence e2;
        getViewModel().setSearchText(str);
        boolean z2 = str.length() == 0;
        getBinding().l.setImageResource(z2 ? R.drawable.ic_search_16dp : R.drawable.ic_clear_white_24dp);
        ImageView imageView = getBinding().l;
        m.checkNotNullExpressionValue(imageView, "binding.emojiSearchClear");
        if (z2) {
            colorStateList = ColorStateList.valueOf(ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorTextMuted));
        } else {
            colorStateList = ColorStateList.valueOf(ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorTextNormal));
        }
        imageView.setImageTintList(colorStateList);
        if (z2) {
            ImageView imageView2 = getBinding().l;
            m.checkNotNullExpressionValue(imageView2, "binding.emojiSearchClear");
            e2 = b.e(this, R.string.search_emojis, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            imageView2.setContentDescription(e2);
            ImageView imageView3 = getBinding().l;
            m.checkNotNullExpressionValue(imageView3, "binding.emojiSearchClear");
            imageView3.setImportantForAccessibility(2);
            return;
        }
        ImageView imageView4 = getBinding().l;
        m.checkNotNullExpressionValue(imageView4, "binding.emojiSearchClear");
        e = b.e(this, R.string.reset, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        imageView4.setContentDescription(e);
        ImageView imageView5 = getBinding().l;
        m.checkNotNullExpressionValue(imageView5, "binding.emojiSearchClear");
        imageView5.setImportantForAccessibility(1);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleNewEmojiRecyclerScrollPosition(int i, List<? extends EmojiCategoryItem> list) {
        for (EmojiCategoryItem emojiCategoryItem : list) {
            Pair<Integer, Integer> categoryRange = emojiCategoryItem.getCategoryRange();
            int intValue = categoryRange.getFirst().intValue();
            int intValue2 = categoryRange.getSecond().intValue();
            if (intValue <= i && intValue2 > i && !emojiCategoryItem.isSelected()) {
                selectCategoryByItemId(emojiCategoryItem.getStableId());
                return;
            }
        }
    }

    private final void initializeInputButtons() {
        int i = 0;
        boolean z2 = this.emojiPickerMode == EmojiPickerMode.INLINE;
        ImageView imageView = getBinding().f;
        m.checkNotNullExpressionValue(imageView, "binding.emojiPickerBackspaceIcon");
        if (!z2) {
            i = 8;
        }
        imageView.setVisibility(i);
        getBinding().k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$initializeInputButtons$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EmojiPickerViewModel viewModel;
                WidgetEmojiPicker.this.isNextCategoryScrollSmooth = false;
                viewModel = WidgetEmojiPicker.this.getViewModel();
                viewModel.onClickUnicodeEmojiCategories();
            }
        });
        getBinding().f.setOnTouchListener(new RepeatingOnTouchListener(250L, 50L, TimeUnit.MILLISECONDS, new Action0() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$initializeInputButtons$2
            @Override // rx.functions.Action0
            public final void call() {
                OnBackspacePressedListener onBackspacePressedListener;
                onBackspacePressedListener = WidgetEmojiPicker.this.onBackspacePressedListener;
                if (onBackspacePressedListener != null) {
                    onBackspacePressedListener.onBackspacePressed();
                }
            }
        }, new Action0() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$initializeInputButtons$3
            @Override // rx.functions.Action0
            public final void call() {
                OnBackspacePressedListener onBackspacePressedListener;
                WidgetEmojiPickerBinding binding;
                WidgetEmojiPicker widgetEmojiPicker = WidgetEmojiPicker.this;
                try {
                    k.a aVar = k.j;
                    binding = widgetEmojiPicker.getBinding();
                    k.m73constructorimpl(Boolean.valueOf(binding.f.performHapticFeedback(3)));
                } catch (Throwable th) {
                    k.a aVar2 = k.j;
                    k.m73constructorimpl(l.createFailure(th));
                }
                onBackspacePressedListener = WidgetEmojiPicker.this.onBackspacePressedListener;
                if (onBackspacePressedListener != null) {
                    onBackspacePressedListener.onBackspacePressed();
                }
            }
        }));
    }

    private final void initializeSearchBar() {
        AppBarLayout appBarLayout = getBinding().d;
        m.checkNotNullExpressionValue(appBarLayout, "binding.emojiAppBar");
        EmojiPickerMode emojiPickerMode = this.emojiPickerMode;
        EmojiPickerMode emojiPickerMode2 = EmojiPickerMode.INLINE;
        boolean z2 = true;
        int i = 0;
        appBarLayout.setVisibility(emojiPickerMode != emojiPickerMode2 ? 0 : 8);
        TextView textView = getBinding().e;
        m.checkNotNullExpressionValue(textView, "binding.emojiInlineSearchButton");
        textView.setVisibility(this.emojiPickerMode == emojiPickerMode2 ? 0 : 8);
        TextInputEditText textInputEditText = getBinding().m;
        m.checkNotNullExpressionValue(textInputEditText, "binding.emojiSearchInput");
        if (this.emojiPickerMode == emojiPickerMode2) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        textInputEditText.setVisibility(i);
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$initializeSearchBar$1

            /* compiled from: WidgetEmojiPicker.kt */
            @e(c = "com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$initializeSearchBar$1$1", f = "WidgetEmojiPicker.kt", l = {412}, m = "invokeSuspend")
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$initializeSearchBar$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends d0.w.i.a.k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
                public int label;

                public AnonymousClass1(Continuation continuation) {
                    super(2, continuation);
                }

                @Override // d0.w.i.a.a
                public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
                    m.checkNotNullParameter(continuation, "completion");
                    return new AnonymousClass1(continuation);
                }

                @Override // kotlin.jvm.functions.Function2
                public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
                    return ((AnonymousClass1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
                }

                @Override // d0.w.i.a.a
                public final Object invokeSuspend(Object obj) {
                    Function0 function0;
                    Object coroutine_suspended = d0.w.h.c.getCOROUTINE_SUSPENDED();
                    int i = this.label;
                    if (i == 0) {
                        l.throwOnFailure(obj);
                        this.label = 1;
                        if (f.P(250L, this) == coroutine_suspended) {
                            return coroutine_suspended;
                        }
                    } else if (i == 1) {
                        l.throwOnFailure(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    function0 = WidgetEmojiPicker.this.onEmojiSearchOpenedListener;
                    if (function0 != null) {
                        Unit unit = (Unit) function0.invoke();
                    }
                    return Unit.a;
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                m.checkNotNullExpressionValue(view, "view");
                CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(view);
                if (coroutineScope != null) {
                    f.H0(coroutineScope, null, null, new AnonymousClass1(null), 3, null);
                }
                WidgetEmojiPicker.this.launchBottomSheet();
            }
        });
        getBinding().l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$initializeSearchBar$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EmojiPickerMode emojiPickerMode3;
                WidgetEmojiPickerBinding binding;
                emojiPickerMode3 = WidgetEmojiPicker.this.emojiPickerMode;
                if (emojiPickerMode3 == EmojiPickerMode.INLINE) {
                    WidgetEmojiPicker.this.launchBottomSheet();
                    return;
                }
                binding = WidgetEmojiPicker.this.getBinding();
                binding.m.setText("");
            }
        });
        TextInputEditText textInputEditText2 = getBinding().m;
        m.checkNotNullExpressionValue(textInputEditText2, "binding.emojiSearchInput");
        TextWatcherKt.addLifecycleAwareTextWatcher(textInputEditText2, this, new WidgetEmojiPicker$initializeSearchBar$3(this));
        if (this.emojiPickerMode == EmojiPickerMode.BOTTOM_SHEET) {
            getBinding().m.requestFocus();
            TextInputEditText textInputEditText3 = getBinding().m;
            m.checkNotNullExpressionValue(textInputEditText3, "binding.emojiSearchInput");
            showKeyboard(textInputEditText3);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchBottomSheet() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        EmojiPickerNavigator.launchBottomSheet$default(parentFragmentManager, this.emojiPickerListener, getEmojiPickerContextType(), null, 8, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onCategoryClicked(EmojiCategoryItem emojiCategoryItem) {
        if (emojiCategoryItem instanceof EmojiCategoryItem.GuildItem) {
            AnalyticsTracker.INSTANCE.emojiCategorySelected(((EmojiCategoryItem.GuildItem) emojiCategoryItem).getGuild().getId());
        }
        selectCategoryByItemId(emojiCategoryItem.getStableId());
        Pair<Integer, Integer> categoryRange = emojiCategoryItem.getCategoryRange();
        WidgetEmojiAdapter widgetEmojiAdapter = this.emojiAdapter;
        if (widgetEmojiAdapter == null) {
            m.throwUninitializedPropertyAccessException("emojiAdapter");
        }
        widgetEmojiAdapter.scrollToPosition(categoryRange.getFirst().intValue());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onPremiumCtaClicked() {
        WidgetSettingsPremium.Companion.launch$default(WidgetSettingsPremium.Companion, requireContext(), null, null, 6, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onSelectedCategoryAdapterPositionUpdated(int i) {
        LinearLayoutManager linearLayoutManager = this.categoryLayoutManager;
        if (linearLayoutManager == null) {
            m.throwUninitializedPropertyAccessException("categoryLayoutManager");
        }
        int findFirstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
        LinearLayoutManager linearLayoutManager2 = this.categoryLayoutManager;
        if (linearLayoutManager2 == null) {
            m.throwUninitializedPropertyAccessException("categoryLayoutManager");
        }
        int findLastCompletelyVisibleItemPosition = linearLayoutManager2.findLastCompletelyVisibleItemPosition();
        int i2 = findLastCompletelyVisibleItemPosition - findFirstCompletelyVisibleItemPosition;
        if (!new IntRange(findFirstCompletelyVisibleItemPosition, findLastCompletelyVisibleItemPosition).contains(i)) {
            int max = Math.max(i < findFirstCompletelyVisibleItemPosition ? i - i2 : i + i2, 0);
            EmojiCategoryAdapter emojiCategoryAdapter = this.categoryAdapter;
            if (emojiCategoryAdapter == null) {
                m.throwUninitializedPropertyAccessException("categoryAdapter");
            }
            int min = Math.min(max, emojiCategoryAdapter.getItemCount() - 1);
            if (this.isNextCategoryScrollSmooth) {
                getBinding().i.smoothScrollToPosition(min);
                return;
            }
            getBinding().i.scrollToPosition(min);
            this.isNextCategoryScrollSmooth = true;
        }
    }

    private final void selectCategoryByItemId(long j) {
        getViewModel().setSelectedCategoryItemId(j);
    }

    private final void setEmojiPickerBottomBarVisible(boolean z2) {
        ConstraintLayout constraintLayout = getBinding().g;
        m.checkNotNullExpressionValue(constraintLayout, "binding.emojiPickerBottomBar");
        int i = 8;
        int i2 = 0;
        constraintLayout.setVisibility(z2 ? 0 : 8);
        View view = getBinding().h;
        m.checkNotNullExpressionValue(view, "binding.emojiPickerBottomBarDivider");
        if (z2) {
            i = 0;
        }
        view.setVisibility(i);
        RecyclerView recyclerView = getBinding().f2357b;
        m.checkNotNullExpressionValue(recyclerView, "binding.chatInputEmojiPickerRecycler");
        ViewGroup.LayoutParams layoutParams = recyclerView.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        if (z2) {
            i2 = getResources().getDimensionPixelSize(R.dimen.expression_picker_category_bar_height);
        }
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, marginLayoutParams.rightMargin, i2);
        recyclerView.setLayoutParams(marginLayoutParams);
    }

    private final void setUpCategoryRecycler() {
        RecyclerView recyclerView = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView, "binding.emojiPickerCategoryRecycler");
        recyclerView.setItemAnimator(null);
        EmojiCategoryAdapter emojiCategoryAdapter = new EmojiCategoryAdapter(new WidgetEmojiPicker$setUpCategoryRecycler$1(this), new WidgetEmojiPicker$setUpCategoryRecycler$2(this), this, null, 8, null);
        this.categoryAdapter = emojiCategoryAdapter;
        if (emojiCategoryAdapter == null) {
            m.throwUninitializedPropertyAccessException("categoryAdapter");
        }
        emojiCategoryAdapter.setHasStableIds(true);
        RecyclerView recyclerView2 = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView2, "binding.emojiPickerCategoryRecycler");
        EmojiCategoryAdapter emojiCategoryAdapter2 = this.categoryAdapter;
        if (emojiCategoryAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("categoryAdapter");
        }
        recyclerView2.setAdapter(emojiCategoryAdapter2);
        RecyclerView recyclerView3 = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView3, "binding.emojiPickerCategoryRecycler");
        EmojiCategoryAdapter emojiCategoryAdapter3 = this.categoryAdapter;
        if (emojiCategoryAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("categoryAdapter");
        }
        this.categoryLayoutManager = new SelfHealingLinearLayoutManager(recyclerView3, emojiCategoryAdapter3, 0, false, 8, null);
        RecyclerView recyclerView4 = getBinding().i;
        m.checkNotNullExpressionValue(recyclerView4, "binding.emojiPickerCategoryRecycler");
        LinearLayoutManager linearLayoutManager = this.categoryLayoutManager;
        if (linearLayoutManager == null) {
            m.throwUninitializedPropertyAccessException("categoryLayoutManager");
        }
        recyclerView4.setLayoutManager(linearLayoutManager);
        getBinding().i.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$setUpCategoryRecycler$3
            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrolled(RecyclerView recyclerView5, int i, int i2) {
                PublishSubject publishSubject;
                m.checkNotNullParameter(recyclerView5, "recyclerView");
                super.onScrolled(recyclerView5, i, i2);
                publishSubject = WidgetEmojiPicker.this.emojiCategoryScrollSubject;
                publishSubject.k.onNext(Unit.a);
            }
        });
    }

    private final void setUpEmojiRecycler() {
        RecyclerView recyclerView = getBinding().f2357b;
        m.checkNotNullExpressionValue(recyclerView, "binding.chatInputEmojiPickerRecycler");
        recyclerView.setItemAnimator(null);
        RecyclerView recyclerView2 = getBinding().f2357b;
        m.checkNotNullExpressionValue(recyclerView2, "binding.chatInputEmojiPickerRecycler");
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        WidgetEmojiAdapter widgetEmojiAdapter = new WidgetEmojiAdapter(recyclerView2, parentFragmentManager, this, new WidgetEmojiPicker$setUpEmojiRecycler$1(this), new WidgetEmojiPicker$setUpEmojiRecycler$2(this));
        this.emojiAdapter = widgetEmojiAdapter;
        if (widgetEmojiAdapter == null) {
            m.throwUninitializedPropertyAccessException("emojiAdapter");
        }
        StickyHeaderItemDecoration stickyHeaderItemDecoration = new StickyHeaderItemDecoration(widgetEmojiAdapter);
        getBinding().f2357b.addItemDecoration(stickyHeaderItemDecoration);
        RecyclerView recyclerView3 = getBinding().f2357b;
        m.checkNotNullExpressionValue(recyclerView3, "binding.chatInputEmojiPickerRecycler");
        stickyHeaderItemDecoration.blockClicks(recyclerView3);
        getBinding().f2357b.setHasFixedSize(true);
        getBinding().f2357b.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$setUpEmojiRecycler$3
            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrolled(RecyclerView recyclerView4, int i, int i2) {
                m.checkNotNullParameter(recyclerView4, "recyclerView");
                WidgetEmojiPicker.this.checkUpsellHeaderVisibility(recyclerView4);
            }
        });
    }

    private final void setWindowInsetsListeners() {
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().j, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.chat.input.emoji.WidgetEmojiPicker$setWindowInsetsListeners$1
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                int additionalBottomPaddingPx;
                m.checkNotNullParameter(view, "view");
                m.checkNotNullParameter(windowInsetsCompat, "insets");
                int systemWindowInsetBottom = windowInsetsCompat.getSystemWindowInsetBottom();
                additionalBottomPaddingPx = WidgetEmojiPicker.this.getAdditionalBottomPaddingPx();
                view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), additionalBottomPaddingPx + systemWindowInsetBottom);
                return windowInsetsCompat.consumeSystemWindowInsets();
            }
        });
    }

    private final void subscribeToCategoryRecyclerScrolls() {
        PublishSubject<Unit> publishSubject = this.emojiCategoryScrollSubject;
        Observable h02 = Observable.h0(new r(publishSubject.j, new LeadingEdgeThrottle(250L, TimeUnit.MILLISECONDS)));
        m.checkNotNullExpressionValue(h02, "emojiCategoryScrollSubje…, TimeUnit.MILLISECONDS))");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(h02, this, null, 2, null), WidgetEmojiPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEmojiPicker$subscribeToCategoryRecyclerScrolls$1(this));
    }

    public final void clearSearchInput() {
        getViewModel().setSearchText("");
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.emojiPickerMode = getMode();
    }

    @Override // com.discord.widgets.chat.input.emoji.OnEmojiSelectedListener
    public void onEmojiSelected(Emoji emoji) {
        m.checkNotNullParameter(emoji, "emoji");
        getViewModel().onEmojiSelected(emoji, new WidgetEmojiPicker$onEmojiSelected$1(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        CoordinatorLayout coordinatorLayout = getBinding().j;
        m.checkNotNullExpressionValue(coordinatorLayout, "binding.emojiPickerContainer");
        coordinatorLayout.setPadding(coordinatorLayout.getPaddingLeft(), coordinatorLayout.getPaddingTop(), coordinatorLayout.getPaddingRight(), getAdditionalBottomPaddingPx());
        EmojiPickerMode mode = getMode();
        EmojiPickerMode emojiPickerMode = EmojiPickerMode.INLINE;
        if (mode == emojiPickerMode) {
            setWindowInsetsListeners();
        }
        initializeInputButtons();
        initializeSearchBar();
        Toolbar toolbar = getBinding().n;
        m.checkNotNullExpressionValue(toolbar, "binding.emojiToolbar");
        ViewGroup.LayoutParams layoutParams = toolbar.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type com.google.android.material.appbar.AppBarLayout.LayoutParams");
        ((AppBarLayout.LayoutParams) layoutParams).setScrollFlags(this.emojiPickerMode == emojiPickerMode ? 5 : 0);
        setUpEmojiRecycler();
        setUpCategoryRecycler();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetEmojiPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEmojiPicker$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetEmojiPicker.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEmojiPicker$onViewBoundOrOnResume$2(this));
        subscribeToCategoryRecyclerScrolls();
    }

    public final void scrollToTop() {
        WidgetEmojiAdapter widgetEmojiAdapter = this.emojiAdapter;
        if (widgetEmojiAdapter == null) {
            m.throwUninitializedPropertyAccessException("emojiAdapter");
        }
        if (widgetEmojiAdapter.getItemCount() > 0) {
            getBinding().f2357b.scrollToPosition(0);
        }
    }

    public final void setListener(EmojiPickerListener emojiPickerListener) {
        this.emojiPickerListener = emojiPickerListener;
    }

    public final void setOnBackspacePressedListener(OnBackspacePressedListener onBackspacePressedListener) {
        this.onBackspacePressedListener = onBackspacePressedListener;
    }
}
