package com.discord.widgets.chat.input;

import com.discord.widgets.chat.MessageContent;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.ChatInputViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
/* compiled from: ChatInputViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "destChannelId", "", "invoke", "(J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatInputViewModel$sendMessage$sendMessage$1 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ ChatInputViewModel.AttachmentContext $attachmentsContext;
    public final /* synthetic */ ChatInputViewModel.ViewState.Loaded $loadedViewState;
    public final /* synthetic */ MessageContent $messageContent;
    public final /* synthetic */ MessageManager $messageManager;
    public final /* synthetic */ Function0 $messageResendCompressedHandler;
    public final /* synthetic */ Function2 $messageSendResultHandler;
    public final /* synthetic */ Function2 $onMessageTooLong;
    public final /* synthetic */ Function1 $onValidationResult;
    public final /* synthetic */ ChatInputViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ChatInputViewModel$sendMessage$sendMessage$1(ChatInputViewModel chatInputViewModel, MessageManager messageManager, MessageContent messageContent, ChatInputViewModel.AttachmentContext attachmentContext, ChatInputViewModel.ViewState.Loaded loaded, Function2 function2, Function2 function22, Function0 function0, Function1 function1) {
        super(1);
        this.this$0 = chatInputViewModel;
        this.$messageManager = messageManager;
        this.$messageContent = messageContent;
        this.$attachmentsContext = attachmentContext;
        this.$loadedViewState = loaded;
        this.$messageSendResultHandler = function2;
        this.$onMessageTooLong = function22;
        this.$messageResendCompressedHandler = function0;
        this.$onValidationResult = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    public final void invoke(long j) {
        this.$onValidationResult.invoke(Boolean.valueOf(MessageManager.sendMessage$default(this.$messageManager, this.$messageContent.getTextContent(), this.$messageContent.getMentionedUsers(), new MessageManager.AttachmentsRequest(this.$attachmentsContext.getCurrentFileSizeMB(), this.$loadedViewState.getMaxFileSizeMB(), this.$attachmentsContext.getAttachments()), Long.valueOf(j), null, false, this.$onMessageTooLong, new ChatInputViewModel$sendMessage$sendMessage$1$synchronousValidationSucceeded$2(this), new ChatInputViewModel$sendMessage$sendMessage$1$synchronousValidationSucceeded$1(this), 48, null)));
    }
}
