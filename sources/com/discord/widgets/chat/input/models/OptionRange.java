package com.discord.widgets.chat.input.models;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.ranges.IntRange;
/* compiled from: OptionRange.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/chat/input/models/OptionRange;", "", "Lkotlin/ranges/IntRange;", "component1", "()Lkotlin/ranges/IntRange;", "component2", "param", "value", "copy", "(Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;)Lcom/discord/widgets/chat/input/models/OptionRange;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/ranges/IntRange;", "getParam", "getValue", HookHelper.constructorName, "(Lkotlin/ranges/IntRange;Lkotlin/ranges/IntRange;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class OptionRange {
    private final IntRange param;
    private final IntRange value;

    public OptionRange(IntRange intRange, IntRange intRange2) {
        m.checkNotNullParameter(intRange, "param");
        m.checkNotNullParameter(intRange2, "value");
        this.param = intRange;
        this.value = intRange2;
    }

    public static /* synthetic */ OptionRange copy$default(OptionRange optionRange, IntRange intRange, IntRange intRange2, int i, Object obj) {
        if ((i & 1) != 0) {
            intRange = optionRange.param;
        }
        if ((i & 2) != 0) {
            intRange2 = optionRange.value;
        }
        return optionRange.copy(intRange, intRange2);
    }

    public final IntRange component1() {
        return this.param;
    }

    public final IntRange component2() {
        return this.value;
    }

    public final OptionRange copy(IntRange intRange, IntRange intRange2) {
        m.checkNotNullParameter(intRange, "param");
        m.checkNotNullParameter(intRange2, "value");
        return new OptionRange(intRange, intRange2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OptionRange)) {
            return false;
        }
        OptionRange optionRange = (OptionRange) obj;
        return m.areEqual(this.param, optionRange.param) && m.areEqual(this.value, optionRange.value);
    }

    public final IntRange getParam() {
        return this.param;
    }

    public final IntRange getValue() {
        return this.value;
    }

    public int hashCode() {
        IntRange intRange = this.param;
        int i = 0;
        int hashCode = (intRange != null ? intRange.hashCode() : 0) * 31;
        IntRange intRange2 = this.value;
        if (intRange2 != null) {
            i = intRange2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("OptionRange(param=");
        R.append(this.param);
        R.append(", value=");
        R.append(this.value);
        R.append(")");
        return R.toString();
    }
}
