package com.discord.widgets.chat.input.models;

import com.discord.api.commands.ApplicationCommandType;
import com.discord.api.commands.CommandChoice;
import com.discord.models.commands.ApplicationCommandOption;
import com.discord.widgets.chat.input.models.InputSelectionModel;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
/* compiled from: InputSelectionModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\u001a\u0011\u0010\u0002\u001a\u00020\u0001*\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u001a\u0011\u0010\u0004\u001a\u00020\u0001*\u00020\u0000¢\u0006\u0004\b\u0004\u0010\u0003¨\u0006\u0005"}, d2 = {"Lcom/discord/widgets/chat/input/models/InputSelectionModel;", "", "hasSelectedCommandOptionWithChoices", "(Lcom/discord/widgets/chat/input/models/InputSelectionModel;)Z", "hasSelectedFreeformInput", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InputSelectionModelKt {
    public static final boolean hasSelectedCommandOptionWithChoices(InputSelectionModel inputSelectionModel) {
        m.checkNotNullParameter(inputSelectionModel, "$this$hasSelectedCommandOptionWithChoices");
        if (inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel) {
            InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
            ApplicationCommandOption selectedCommandOption = commandInputSelectionModel.getSelectedCommandOption();
            ApplicationCommandType applicationCommandType = null;
            List<CommandChoice> choices = selectedCommandOption != null ? selectedCommandOption.getChoices() : null;
            if (!(choices == null || choices.isEmpty())) {
                return true;
            }
            ApplicationCommandOption selectedCommandOption2 = commandInputSelectionModel.getSelectedCommandOption();
            if (selectedCommandOption2 != null) {
                applicationCommandType = selectedCommandOption2.getType();
            }
            if (applicationCommandType == ApplicationCommandType.BOOLEAN) {
                return true;
            }
        }
        return false;
    }

    public static final boolean hasSelectedFreeformInput(InputSelectionModel inputSelectionModel) {
        m.checkNotNullParameter(inputSelectionModel, "$this$hasSelectedFreeformInput");
        if (inputSelectionModel instanceof InputSelectionModel.CommandInputSelectionModel) {
            InputSelectionModel.CommandInputSelectionModel commandInputSelectionModel = (InputSelectionModel.CommandInputSelectionModel) inputSelectionModel;
            if (commandInputSelectionModel.getSelectedCommandOption() == null) {
                return true;
            }
            List<CommandChoice> choices = commandInputSelectionModel.getSelectedCommandOption().getChoices();
            boolean z2 = choices != null && (choices.isEmpty() ^ true);
            ApplicationCommandType type = commandInputSelectionModel.getSelectedCommandOption().getType();
            return !commandInputSelectionModel.getSelectedCommandOption().getAutocomplete() && !z2 && (type == ApplicationCommandType.INTEGER || type == ApplicationCommandType.STRING);
        } else if (inputSelectionModel instanceof InputSelectionModel.MessageInputSelectionModel) {
            return true;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }
}
