package com.discord.widgets.chat.input.models;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.widgets.chat.input.autocomplete.LeadingIdentifier;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: MentionToken.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\u0006\u0010\u0011\u001a\u00020\u000b¢\u0006\u0004\b\"\u0010#J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ8\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0007J\u0010\u0010\u0015\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u0015\u0010\rJ\u001a\u0010\u0017\u001a\u00020\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0019\u001a\u0004\b\u0010\u0010\nR\u0019\u0010\u001a\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001d\u001a\u0004\b\u001e\u0010\rR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b \u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001b\u001a\u0004\b!\u0010\u0007¨\u0006$"}, d2 = {"Lcom/discord/widgets/chat/input/models/MentionToken;", "", "Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "component1", "()Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "", "component2", "()Ljava/lang/String;", "", "component3", "()Z", "", "component4", "()I", "leadingIdentifier", "token", "isAtStart", "startIndex", "copy", "(Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;Ljava/lang/String;ZI)Lcom/discord/widgets/chat/input/models/MentionToken;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "formattedToken", "Ljava/lang/String;", "getFormattedToken", "I", "getStartIndex", "Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;", "getLeadingIdentifier", "getToken", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/autocomplete/LeadingIdentifier;Ljava/lang/String;ZI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MentionToken {
    private final String formattedToken;
    private final boolean isAtStart;
    private final LeadingIdentifier leadingIdentifier;
    private final int startIndex;
    private final String token;

    public MentionToken(LeadingIdentifier leadingIdentifier, String str, boolean z2, int i) {
        m.checkNotNullParameter(leadingIdentifier, "leadingIdentifier");
        m.checkNotNullParameter(str, "token");
        this.leadingIdentifier = leadingIdentifier;
        this.token = str;
        this.isAtStart = z2;
        this.startIndex = i;
        this.formattedToken = t.replaceFirst$default(str, String.valueOf(leadingIdentifier.getIdentifier()), "", false, 4, null);
    }

    public static /* synthetic */ MentionToken copy$default(MentionToken mentionToken, LeadingIdentifier leadingIdentifier, String str, boolean z2, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            leadingIdentifier = mentionToken.leadingIdentifier;
        }
        if ((i2 & 2) != 0) {
            str = mentionToken.token;
        }
        if ((i2 & 4) != 0) {
            z2 = mentionToken.isAtStart;
        }
        if ((i2 & 8) != 0) {
            i = mentionToken.startIndex;
        }
        return mentionToken.copy(leadingIdentifier, str, z2, i);
    }

    public final LeadingIdentifier component1() {
        return this.leadingIdentifier;
    }

    public final String component2() {
        return this.token;
    }

    public final boolean component3() {
        return this.isAtStart;
    }

    public final int component4() {
        return this.startIndex;
    }

    public final MentionToken copy(LeadingIdentifier leadingIdentifier, String str, boolean z2, int i) {
        m.checkNotNullParameter(leadingIdentifier, "leadingIdentifier");
        m.checkNotNullParameter(str, "token");
        return new MentionToken(leadingIdentifier, str, z2, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MentionToken)) {
            return false;
        }
        MentionToken mentionToken = (MentionToken) obj;
        return m.areEqual(this.leadingIdentifier, mentionToken.leadingIdentifier) && m.areEqual(this.token, mentionToken.token) && this.isAtStart == mentionToken.isAtStart && this.startIndex == mentionToken.startIndex;
    }

    public final String getFormattedToken() {
        return this.formattedToken;
    }

    public final LeadingIdentifier getLeadingIdentifier() {
        return this.leadingIdentifier;
    }

    public final int getStartIndex() {
        return this.startIndex;
    }

    public final String getToken() {
        return this.token;
    }

    public int hashCode() {
        LeadingIdentifier leadingIdentifier = this.leadingIdentifier;
        int i = 0;
        int hashCode = (leadingIdentifier != null ? leadingIdentifier.hashCode() : 0) * 31;
        String str = this.token;
        if (str != null) {
            i = str.hashCode();
        }
        int i2 = (hashCode + i) * 31;
        boolean z2 = this.isAtStart;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return ((i2 + i3) * 31) + this.startIndex;
    }

    public final boolean isAtStart() {
        return this.isAtStart;
    }

    public String toString() {
        StringBuilder R = a.R("MentionToken(leadingIdentifier=");
        R.append(this.leadingIdentifier);
        R.append(", token=");
        R.append(this.token);
        R.append(", isAtStart=");
        R.append(this.isAtStart);
        R.append(", startIndex=");
        return a.A(R, this.startIndex, ")");
    }
}
