package com.discord.widgets.chat.input.models;

import kotlin.Metadata;
/* compiled from: ApplicationCommandData.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u001a\u0011\u0010\u0002\u001a\u00020\u0001*\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "", "getCommandId", "(Lcom/discord/widgets/chat/input/models/ApplicationCommandData;)Ljava/lang/String;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ApplicationCommandDataKt {
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0055, code lost:
        if (r5 != null) goto L36;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final java.lang.String getCommandId(com.discord.widgets.chat.input.models.ApplicationCommandData r7) {
        /*
            java.lang.String r0 = "$this$getCommandId"
            d0.z.d.m.checkNotNullParameter(r7, r0)
            java.util.List r0 = r7.getValues()
            java.util.Iterator r0 = r0.iterator()
        Ld:
            boolean r1 = r0.hasNext()
            r2 = 0
            r3 = 0
            r4 = 1
            if (r1 == 0) goto L2a
            java.lang.Object r1 = r0.next()
            r5 = r1
            com.discord.widgets.chat.input.models.ApplicationCommandValue r5 = (com.discord.widgets.chat.input.models.ApplicationCommandValue) r5
            int r5 = r5.getType()
            r6 = 2
            if (r5 != r6) goto L26
            r5 = 1
            goto L27
        L26:
            r5 = 0
        L27:
            if (r5 == 0) goto Ld
            goto L2b
        L2a:
            r1 = r3
        L2b:
            com.discord.widgets.chat.input.models.ApplicationCommandValue r1 = (com.discord.widgets.chat.input.models.ApplicationCommandValue) r1
            if (r1 == 0) goto L58
            java.util.List r0 = r1.getOptions()
            if (r0 == 0) goto L58
            java.util.Iterator r0 = r0.iterator()
        L39:
            boolean r5 = r0.hasNext()
            if (r5 == 0) goto L52
            java.lang.Object r5 = r0.next()
            r6 = r5
            com.discord.widgets.chat.input.models.ApplicationCommandValue r6 = (com.discord.widgets.chat.input.models.ApplicationCommandValue) r6
            int r6 = r6.getType()
            if (r6 != r4) goto L4e
            r6 = 1
            goto L4f
        L4e:
            r6 = 0
        L4f:
            if (r6 == 0) goto L39
            goto L53
        L52:
            r5 = r3
        L53:
            com.discord.widgets.chat.input.models.ApplicationCommandValue r5 = (com.discord.widgets.chat.input.models.ApplicationCommandValue) r5
            if (r5 == 0) goto L58
            goto L7c
        L58:
            java.util.List r0 = r7.getValues()
            java.util.Iterator r0 = r0.iterator()
        L60:
            boolean r5 = r0.hasNext()
            if (r5 == 0) goto L79
            java.lang.Object r5 = r0.next()
            r6 = r5
            com.discord.widgets.chat.input.models.ApplicationCommandValue r6 = (com.discord.widgets.chat.input.models.ApplicationCommandValue) r6
            int r6 = r6.getType()
            if (r6 != r4) goto L75
            r6 = 1
            goto L76
        L75:
            r6 = 0
        L76:
            if (r6 == 0) goto L60
            r3 = r5
        L79:
            r5 = r3
            com.discord.widgets.chat.input.models.ApplicationCommandValue r5 = (com.discord.widgets.chat.input.models.ApplicationCommandValue) r5
        L7c:
            java.lang.String r0 = " "
            if (r1 == 0) goto Lab
            if (r5 == 0) goto Lab
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            com.discord.models.commands.ApplicationCommand r7 = r7.getApplicationCommand()
            java.lang.String r7 = r7.getId()
            r2.append(r7)
            r2.append(r0)
            java.lang.String r7 = r1.getName()
            r2.append(r7)
            r2.append(r0)
            java.lang.String r7 = r5.getName()
            r2.append(r7)
            java.lang.String r7 = r2.toString()
            goto Ld4
        Lab:
            if (r5 == 0) goto Lcc
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.discord.models.commands.ApplicationCommand r7 = r7.getApplicationCommand()
            java.lang.String r7 = r7.getId()
            r1.append(r7)
            r1.append(r0)
            java.lang.String r7 = r5.getName()
            r1.append(r7)
            java.lang.String r7 = r1.toString()
            goto Ld4
        Lcc:
            com.discord.models.commands.ApplicationCommand r7 = r7.getApplicationCommand()
            java.lang.String r7 = r7.getId()
        Ld4:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.models.ApplicationCommandDataKt.getCommandId(com.discord.widgets.chat.input.models.ApplicationCommandData):java.lang.String");
    }
}
