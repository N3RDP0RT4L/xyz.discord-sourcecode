package com.discord.widgets.chat.input.models;

import andhook.lib.HookHelper;
import kotlin.Metadata;
/* compiled from: CommandOptionValue.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/chat/input/models/BooleanOptionValue;", "Lcom/discord/widgets/chat/input/models/CommandOptionValue;", "", "value", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class BooleanOptionValue extends CommandOptionValue {
    public BooleanOptionValue(boolean z2) {
        super(Boolean.valueOf(z2), null);
    }
}
