package com.discord.widgets.chat.input.models;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.commands.Application;
import com.discord.models.commands.ApplicationCommand;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ApplicationCommandData.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\b\b\u0002\u0010\u0012\u001a\u00020\f¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ>\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00052\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\u0012\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\f2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b\u001f\u0010\u000bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u0004R\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b#\u0010\u0007R\u0019\u0010\u0012\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010$\u001a\u0004\b%\u0010\u000e¨\u0006("}, d2 = {"Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "", "Lcom/discord/models/commands/Application;", "component1", "()Lcom/discord/models/commands/Application;", "Lcom/discord/models/commands/ApplicationCommand;", "component2", "()Lcom/discord/models/commands/ApplicationCommand;", "", "Lcom/discord/widgets/chat/input/models/ApplicationCommandValue;", "component3", "()Ljava/util/List;", "", "component4", "()Z", "application", "applicationCommand", "values", "validInputs", "copy", "(Lcom/discord/models/commands/Application;Lcom/discord/models/commands/ApplicationCommand;Ljava/util/List;Z)Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getValues", "Lcom/discord/models/commands/Application;", "getApplication", "Lcom/discord/models/commands/ApplicationCommand;", "getApplicationCommand", "Z", "getValidInputs", HookHelper.constructorName, "(Lcom/discord/models/commands/Application;Lcom/discord/models/commands/ApplicationCommand;Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ApplicationCommandData {
    private final Application application;
    private final ApplicationCommand applicationCommand;
    private final boolean validInputs;
    private final List<ApplicationCommandValue> values;

    public ApplicationCommandData(Application application, ApplicationCommand applicationCommand, List<ApplicationCommandValue> list, boolean z2) {
        m.checkNotNullParameter(application, "application");
        m.checkNotNullParameter(applicationCommand, "applicationCommand");
        m.checkNotNullParameter(list, "values");
        this.application = application;
        this.applicationCommand = applicationCommand;
        this.values = list;
        this.validInputs = z2;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ApplicationCommandData copy$default(ApplicationCommandData applicationCommandData, Application application, ApplicationCommand applicationCommand, List list, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            application = applicationCommandData.application;
        }
        if ((i & 2) != 0) {
            applicationCommand = applicationCommandData.applicationCommand;
        }
        if ((i & 4) != 0) {
            list = applicationCommandData.values;
        }
        if ((i & 8) != 0) {
            z2 = applicationCommandData.validInputs;
        }
        return applicationCommandData.copy(application, applicationCommand, list, z2);
    }

    public final Application component1() {
        return this.application;
    }

    public final ApplicationCommand component2() {
        return this.applicationCommand;
    }

    public final List<ApplicationCommandValue> component3() {
        return this.values;
    }

    public final boolean component4() {
        return this.validInputs;
    }

    public final ApplicationCommandData copy(Application application, ApplicationCommand applicationCommand, List<ApplicationCommandValue> list, boolean z2) {
        m.checkNotNullParameter(application, "application");
        m.checkNotNullParameter(applicationCommand, "applicationCommand");
        m.checkNotNullParameter(list, "values");
        return new ApplicationCommandData(application, applicationCommand, list, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApplicationCommandData)) {
            return false;
        }
        ApplicationCommandData applicationCommandData = (ApplicationCommandData) obj;
        return m.areEqual(this.application, applicationCommandData.application) && m.areEqual(this.applicationCommand, applicationCommandData.applicationCommand) && m.areEqual(this.values, applicationCommandData.values) && this.validInputs == applicationCommandData.validInputs;
    }

    public final Application getApplication() {
        return this.application;
    }

    public final ApplicationCommand getApplicationCommand() {
        return this.applicationCommand;
    }

    public final boolean getValidInputs() {
        return this.validInputs;
    }

    public final List<ApplicationCommandValue> getValues() {
        return this.values;
    }

    public int hashCode() {
        Application application = this.application;
        int i = 0;
        int hashCode = (application != null ? application.hashCode() : 0) * 31;
        ApplicationCommand applicationCommand = this.applicationCommand;
        int hashCode2 = (hashCode + (applicationCommand != null ? applicationCommand.hashCode() : 0)) * 31;
        List<ApplicationCommandValue> list = this.values;
        if (list != null) {
            i = list.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z2 = this.validInputs;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        return i2 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("ApplicationCommandData(application=");
        R.append(this.application);
        R.append(", applicationCommand=");
        R.append(this.applicationCommand);
        R.append(", values=");
        R.append(this.values);
        R.append(", validInputs=");
        return a.M(R, this.validInputs, ")");
    }

    public /* synthetic */ ApplicationCommandData(Application application, ApplicationCommand applicationCommand, List list, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(application, applicationCommand, list, (i & 8) != 0 ? false : z2);
    }
}
