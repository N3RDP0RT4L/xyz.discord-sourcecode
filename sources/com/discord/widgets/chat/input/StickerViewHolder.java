package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.FrameLayout;
import com.discord.databinding.StickerAutocompleteItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.views.sticker.StickerView;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetChatInputAutocompleteStickerAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/chat/input/StickerViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/input/WidgetChatInputAutocompleteStickerAdapter;", "Lcom/discord/widgets/chat/input/AutocompleteStickerItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/chat/input/AutocompleteStickerItem;)V", "Lcom/discord/databinding/StickerAutocompleteItemBinding;", "binding", "Lcom/discord/databinding/StickerAutocompleteItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/input/WidgetChatInputAutocompleteStickerAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerViewHolder extends MGRecyclerViewHolder<WidgetChatInputAutocompleteStickerAdapter, AutocompleteStickerItem> {
    private final StickerAutocompleteItemBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public StickerViewHolder(WidgetChatInputAutocompleteStickerAdapter widgetChatInputAutocompleteStickerAdapter) {
        super((int) R.layout.sticker_autocomplete_item, widgetChatInputAutocompleteStickerAdapter);
        m.checkNotNullParameter(widgetChatInputAutocompleteStickerAdapter, "adapter");
        View view = this.itemView;
        StickerView stickerView = (StickerView) view.findViewById(R.id.sticker_autocomplete_sticker);
        if (stickerView != null) {
            StickerAutocompleteItemBinding stickerAutocompleteItemBinding = new StickerAutocompleteItemBinding((FrameLayout) view, stickerView);
            m.checkNotNullExpressionValue(stickerAutocompleteItemBinding, "StickerAutocompleteItemBinding.bind(itemView)");
            this.binding = stickerAutocompleteItemBinding;
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.sticker_autocomplete_sticker)));
    }

    public static final /* synthetic */ WidgetChatInputAutocompleteStickerAdapter access$getAdapter$p(StickerViewHolder stickerViewHolder) {
        return (WidgetChatInputAutocompleteStickerAdapter) stickerViewHolder.adapter;
    }

    public void onConfigure(int i, final AutocompleteStickerItem autocompleteStickerItem) {
        m.checkNotNullParameter(autocompleteStickerItem, "data");
        super.onConfigure(i, (int) autocompleteStickerItem);
        StickerView.e(this.binding.f2130b, autocompleteStickerItem.getSticker(), null, 2);
        this.binding.f2130b.b();
        this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.StickerViewHolder$onConfigure$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StickerViewHolder.access$getAdapter$p(StickerViewHolder.this).getOnClickSticker().invoke(autocompleteStickerItem.getSticker());
            }
        });
    }
}
