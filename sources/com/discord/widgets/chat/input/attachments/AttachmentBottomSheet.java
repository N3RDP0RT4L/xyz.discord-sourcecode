package com.discord.widgets.chat.input.attachments;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.os.BundleKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import b.f.g.a.a.b;
import b.f.g.a.a.d;
import b.f.j.d.f;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetAttachmentBottomSheetBinding;
import com.discord.utilities.embed.EmbedResourceUtils;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textview.MaterialTextView;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.model.Media;
import d0.o;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: AttachmentBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\t\u0018\u0000 C2\u00020\u0001:\u0001CB\u0007¢\u0006\u0004\bB\u0010\u000bJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0019\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\r\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ!\u0010\u000e\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0012\u0010\u0013J\r\u0010\u0014\u001a\u00020\u0007¢\u0006\u0004\b\u0014\u0010\u000bJ\u000f\u0010\u0015\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\u0015\u0010\u000bJ\u0017\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019R$\u0010\u001a\u001a\u0004\u0018\u00010\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR$\u0010!\u001a\u0004\u0018\u00010 8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R$\u0010(\u001a\u0010\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0007\u0018\u00010'8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010)R\u001e\u0010+\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010*8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,R\u001d\u00102\u001a\u00020-8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u00101R\u001f\u00104\u001a\b\u0012\u0004\u0012\u00020 038\u0006@\u0006¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u0016\u00108\u001a\u00020\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00109R*\u0010<\u001a\n\u0012\u0004\u0012\u00020;\u0018\u00010:8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b<\u0010=\u001a\u0004\b>\u0010?\"\u0004\b@\u0010A¨\u0006D"}, d2 = {"Lcom/discord/widgets/chat/input/attachments/AttachmentBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "", "getContentViewResId", "()I", "Landroid/os/Bundle;", "savedInstanceState", "", "onCreate", "(Landroid/os/Bundle;)V", "loadAttachment", "()V", "Landroid/view/View;", "view", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "", "spoiler", "markSpoiler", "(Z)V", "updateSpoilerViewSize", "onPause", "Landroid/content/DialogInterface;", "dialog", "onDismiss", "(Landroid/content/DialogInterface;)V", "attachmentViewSize", "Ljava/lang/Integer;", "getAttachmentViewSize", "()Ljava/lang/Integer;", "setAttachmentViewSize", "(Ljava/lang/Integer;)V", "Lcom/facebook/imagepipeline/image/ImageInfo;", "attachmentImageInfo", "Lcom/facebook/imagepipeline/image/ImageInfo;", "getAttachmentImageInfo", "()Lcom/facebook/imagepipeline/image/ImageInfo;", "setAttachmentImageInfo", "(Lcom/facebook/imagepipeline/image/ImageInfo;)V", "Lkotlin/Function1;", "onMarkSpoiler", "Lkotlin/jvm/functions/Function1;", "Lkotlin/Function0;", "onFileRemoved", "Lkotlin/jvm/functions/Function0;", "Lcom/discord/databinding/WidgetAttachmentBottomSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetAttachmentBottomSheetBinding;", "binding", "Lcom/facebook/drawee/controller/ControllerListener;", "draweeControllerListener", "Lcom/facebook/drawee/controller/ControllerListener;", "getDraweeControllerListener", "()Lcom/facebook/drawee/controller/ControllerListener;", "isSpoiler", "Z", "Lcom/lytefast/flexinput/model/Attachment;", "", "attachment", "Lcom/lytefast/flexinput/model/Attachment;", "getAttachment", "()Lcom/lytefast/flexinput/model/Attachment;", "setAttachment", "(Lcom/lytefast/flexinput/model/Attachment;)V", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AttachmentBottomSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(AttachmentBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetAttachmentBottomSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private Attachment<? extends Object> attachment;
    private ImageInfo attachmentImageInfo;
    private Integer attachmentViewSize;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, AttachmentBottomSheet$binding$2.INSTANCE, null, 2, null);
    private final ControllerListener<ImageInfo> draweeControllerListener = new ControllerListener<ImageInfo>() { // from class: com.discord.widgets.chat.input.attachments.AttachmentBottomSheet$draweeControllerListener$1
        @Override // com.facebook.drawee.controller.ControllerListener
        public void onFailure(String str, Throwable th) {
        }

        @Override // com.facebook.drawee.controller.ControllerListener
        public void onIntermediateImageFailed(String str, Throwable th) {
        }

        public void onIntermediateImageSet(String str, ImageInfo imageInfo) {
        }

        @Override // com.facebook.drawee.controller.ControllerListener
        public void onRelease(String str) {
        }

        @Override // com.facebook.drawee.controller.ControllerListener
        public void onSubmit(String str, Object obj) {
        }

        public void onFinalImageSet(String str, ImageInfo imageInfo, Animatable animatable) {
            WidgetAttachmentBottomSheetBinding binding;
            String str2;
            AttachmentBottomSheet.this.setAttachmentImageInfo(imageInfo);
            binding = AttachmentBottomSheet.this.getBinding();
            SimpleDraweeView simpleDraweeView = binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.attachmentPreview");
            ViewGroup.LayoutParams layoutParams = simpleDraweeView.getLayoutParams();
            Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
            ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
            int i = 0;
            int width = imageInfo != null ? imageInfo.getWidth() : 0;
            if (imageInfo != null) {
                i = imageInfo.getHeight();
            }
            if (width > i) {
                StringBuilder sb = new StringBuilder();
                Integer num = null;
                sb.append(imageInfo != null ? Integer.valueOf(imageInfo.getWidth()) : null);
                sb.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
                if (imageInfo != null) {
                    num = Integer.valueOf(imageInfo.getHeight());
                }
                sb.append(num);
                str2 = sb.toString();
            } else {
                str2 = "1";
            }
            layoutParams2.dimensionRatio = str2;
            simpleDraweeView.setLayoutParams(layoutParams2);
            AttachmentBottomSheet.this.updateSpoilerViewSize();
        }
    };
    private boolean isSpoiler;
    private Function0<Unit> onFileRemoved;
    private Function1<? super Boolean, Unit> onMarkSpoiler;

    /* compiled from: AttachmentBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010JE\u0010\r\u001a\u00020\f2\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00010\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00070\t¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/chat/input/attachments/AttachmentBottomSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/lytefast/flexinput/model/Attachment;", "attachment", "Lkotlin/Function0;", "", "onFileRemoved", "Lkotlin/Function1;", "", "onMarkSpoiler", "Lcom/discord/widgets/chat/input/attachments/AttachmentBottomSheet;", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/lytefast/flexinput/model/Attachment;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/chat/input/attachments/AttachmentBottomSheet;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final AttachmentBottomSheet show(FragmentManager fragmentManager, Attachment<? extends Object> attachment, Function0<Unit> function0, Function1<? super Boolean, Unit> function1) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(attachment, "attachment");
            m.checkNotNullParameter(function0, "onFileRemoved");
            m.checkNotNullParameter(function1, "onMarkSpoiler");
            AttachmentBottomSheet attachmentBottomSheet = new AttachmentBottomSheet();
            attachmentBottomSheet.setArguments(BundleKt.bundleOf(o.to("EXTRA_ATTACHMENT", attachment)));
            attachmentBottomSheet.onFileRemoved = function0;
            attachmentBottomSheet.onMarkSpoiler = function1;
            attachmentBottomSheet.isSpoiler = attachment.getSpoiler();
            attachmentBottomSheet.show(fragmentManager, a0.getOrCreateKotlinClass(AttachmentBottomSheet.class).toString());
            return attachmentBottomSheet;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public AttachmentBottomSheet() {
        super(false, 1, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetAttachmentBottomSheetBinding getBinding() {
        return (WidgetAttachmentBottomSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final Attachment<Object> getAttachment() {
        return this.attachment;
    }

    public final ImageInfo getAttachmentImageInfo() {
        return this.attachmentImageInfo;
    }

    public final Integer getAttachmentViewSize() {
        return this.attachmentViewSize;
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_attachment_bottom_sheet;
    }

    public final ControllerListener<ImageInfo> getDraweeControllerListener() {
        return this.draweeControllerListener;
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [REQUEST, com.facebook.imagepipeline.request.ImageRequest] */
    public final void loadAttachment() {
        Attachment<? extends Object> attachment = this.attachment;
        ImageRequestBuilder b2 = ImageRequestBuilder.b(attachment != null ? attachment.getUri() : null);
        b2.e = f.a;
        d a = b.a();
        a.m = true;
        SimpleDraweeView simpleDraweeView = getBinding().f;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.attachmentPreview");
        a.n = simpleDraweeView.getController();
        a.h = b2.a();
        AbstractDraweeController a2 = a.a();
        a2.f(this.draweeControllerListener);
        SimpleDraweeView simpleDraweeView2 = getBinding().f;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.attachmentPreview");
        simpleDraweeView2.setController(a2);
    }

    public final void markSpoiler(boolean z2) {
        this.isSpoiler = z2;
        MaterialCheckBox materialCheckBox = getBinding().e;
        m.checkNotNullExpressionValue(materialCheckBox, "binding.attachmentMarkSpoilerCheckbox");
        materialCheckBox.setChecked(z2);
        FrameLayout frameLayout = getBinding().h;
        m.checkNotNullExpressionValue(frameLayout, "binding.attachmentSpoilerCover");
        frameLayout.setVisibility(z2 ? 0 : 8);
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.attachment = (Attachment) getArgumentsOrDefault().getParcelable("EXTRA_ATTACHMENT");
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        this.onFileRemoved = null;
        this.onMarkSpoiler = null;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dismiss();
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        CharSequence charSequence;
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        AppCompatTextView appCompatTextView = getBinding().c;
        m.checkNotNullExpressionValue(appCompatTextView, "binding.attachmentFilename");
        Attachment<? extends Object> attachment = this.attachment;
        String str = null;
        appCompatTextView.setText(attachment != null ? attachment.getDisplayName() : null);
        SimpleDraweeView simpleDraweeView = getBinding().f;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.attachmentPreview");
        ViewExtensions.addOnSizeChangedListener(simpleDraweeView, new AttachmentBottomSheet$onViewCreated$1(this));
        Attachment<? extends Object> attachment2 = this.attachment;
        if (!(attachment2 instanceof Media)) {
            attachment2 = null;
        }
        Media media = (Media) attachment2;
        Long l = media != null ? media.k : null;
        MaterialTextView materialTextView = getBinding().f2210b;
        m.checkNotNullExpressionValue(materialTextView, "binding.attachmentDuration");
        int i = 0;
        if (!(l != null)) {
            i = 8;
        }
        materialTextView.setVisibility(i);
        if (l != null) {
            long longValue = l.longValue();
            MaterialTextView materialTextView2 = getBinding().f2210b;
            m.checkNotNullExpressionValue(materialTextView2, "binding.attachmentDuration");
            if (longValue > 0) {
                charSequence = TimeUtils.toFriendlyStringSimple$default(TimeUtils.INSTANCE, longValue, null, null, 6, null);
            } else {
                Context context = getContext();
                if (context != null) {
                    str = context.getString(R.string.video);
                }
                charSequence = str;
            }
            materialTextView2.setText(charSequence);
        }
        markSpoiler(this.isSpoiler);
        loadAttachment();
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.attachments.AttachmentBottomSheet$onViewCreated$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Function0 function0;
                function0 = AttachmentBottomSheet.this.onFileRemoved;
                if (function0 != null) {
                    Unit unit = (Unit) function0.invoke();
                }
                AttachmentBottomSheet.this.dismiss();
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.attachments.AttachmentBottomSheet$onViewCreated$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                boolean z2;
                Function1 function1;
                z2 = AttachmentBottomSheet.this.isSpoiler;
                boolean z3 = !z2;
                AttachmentBottomSheet.this.markSpoiler(z3);
                function1 = AttachmentBottomSheet.this.onMarkSpoiler;
                if (function1 != null) {
                    Unit unit = (Unit) function1.invoke(Boolean.valueOf(z3));
                }
            }
        });
        setPeekHeightBottomView(getBinding().g);
    }

    public final void setAttachment(Attachment<? extends Object> attachment) {
        this.attachment = attachment;
    }

    public final void setAttachmentImageInfo(ImageInfo imageInfo) {
        this.attachmentImageInfo = imageInfo;
    }

    public final void setAttachmentViewSize(Integer num) {
        this.attachmentViewSize = num;
    }

    public final void updateSpoilerViewSize() {
        Integer num;
        Pair calculateScaledSize;
        ImageInfo imageInfo = this.attachmentImageInfo;
        if (imageInfo != null && (num = this.attachmentViewSize) != null) {
            int intValue = num.intValue();
            int width = imageInfo.getWidth();
            int height = imageInfo.getHeight();
            EmbedResourceUtils embedResourceUtils = EmbedResourceUtils.INSTANCE;
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            calculateScaledSize = embedResourceUtils.calculateScaledSize(width, height, intValue, intValue, resources, (r14 & 32) != 0 ? 0 : 0);
            FrameLayout frameLayout = getBinding().h;
            m.checkNotNullExpressionValue(frameLayout, "binding.attachmentSpoilerCover");
            ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
            Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
            ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
            ((ViewGroup.MarginLayoutParams) layoutParams2).width = ((Number) calculateScaledSize.getFirst()).intValue();
            ((ViewGroup.MarginLayoutParams) layoutParams2).height = ((Number) calculateScaledSize.getSecond()).intValue();
            frameLayout.setLayoutParams(layoutParams2);
        }
    }
}
