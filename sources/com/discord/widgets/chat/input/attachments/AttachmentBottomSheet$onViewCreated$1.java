package com.discord.widgets.chat.input.attachments;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function4;
/* compiled from: AttachmentBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "width", "<anonymous parameter 1>", "height", "<anonymous parameter 3>", "", "invoke", "(IIII)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AttachmentBottomSheet$onViewCreated$1 extends o implements Function4<Integer, Integer, Integer, Integer, Unit> {
    public final /* synthetic */ AttachmentBottomSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AttachmentBottomSheet$onViewCreated$1(AttachmentBottomSheet attachmentBottomSheet) {
        super(4);
        this.this$0 = attachmentBottomSheet;
    }

    @Override // kotlin.jvm.functions.Function4
    public /* bridge */ /* synthetic */ Unit invoke(Integer num, Integer num2, Integer num3, Integer num4) {
        invoke(num.intValue(), num2.intValue(), num3.intValue(), num4.intValue());
        return Unit.a;
    }

    public final void invoke(int i, int i2, int i3, int i4) {
        this.this$0.setAttachmentViewSize(Integer.valueOf(i));
        this.this$0.updateSpoilerViewSize();
    }
}
