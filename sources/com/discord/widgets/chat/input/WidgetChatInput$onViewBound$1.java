package com.discord.widgets.chat.input;

import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetChatInputApplicationCommandsBinding;
import com.discord.databinding.WidgetChatInputBinding;
import com.discord.widgets.chat.input.autocomplete.InputAutocomplete;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.widget.FlexEditText;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInput$onViewBound$1 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetChatInput this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInput$onViewBound$1(WidgetChatInput widgetChatInput) {
        super(0);
        this.this$0 = widgetChatInput;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        FlexInputFragment flexInputFragment;
        WidgetChatInputBinding binding;
        WidgetChatInputBinding binding2;
        WidgetChatInputBinding binding3;
        WidgetChatInputBinding binding4;
        WidgetChatInputBinding binding5;
        WidgetChatInputBinding binding6;
        WidgetChatInputBinding binding7;
        WidgetChatInputTruncatedHint widgetChatInputTruncatedHint;
        flexInputFragment = this.this$0.getFlexInputFragment();
        FlexEditText l = flexInputFragment.l();
        WidgetChatInput widgetChatInput = this.this$0;
        binding = widgetChatInput.getBinding();
        TextView textView = binding.k;
        m.checkNotNullExpressionValue(textView, "binding.chatInputEmojiMatchingHeader");
        binding2 = this.this$0.getBinding();
        RecyclerView recyclerView = binding2.l;
        m.checkNotNullExpressionValue(recyclerView, "binding.chatInputMentionsRecycler");
        binding3 = this.this$0.getBinding();
        RecyclerView recyclerView2 = binding3.c;
        m.checkNotNullExpressionValue(recyclerView2, "binding.chatInputCategoriesRecycler");
        binding4 = this.this$0.getBinding();
        LinearLayout linearLayout = binding4.m;
        m.checkNotNullExpressionValue(linearLayout, "binding.chatInputStickersContainer");
        binding5 = this.this$0.getBinding();
        RecyclerView recyclerView3 = binding5.o;
        m.checkNotNullExpressionValue(recyclerView3, "binding.chatInputStickersRecycler");
        binding6 = this.this$0.getBinding();
        TextView textView2 = binding6.n;
        m.checkNotNullExpressionValue(textView2, "binding.chatInputStickersMatchingHeader");
        binding7 = this.this$0.getBinding();
        WidgetChatInputApplicationCommandsBinding widgetChatInputApplicationCommandsBinding = binding7.f2286b;
        m.checkNotNullExpressionValue(widgetChatInputApplicationCommandsBinding, "binding.applicationCommandsRoot");
        InputAutocomplete inputAutocomplete = new InputAutocomplete(widgetChatInput, l, null, textView, recyclerView, recyclerView2, linearLayout, recyclerView3, textView2, widgetChatInputApplicationCommandsBinding);
        this.this$0.autocomplete = inputAutocomplete;
        inputAutocomplete.onViewBoundOrOnResume();
        this.this$0.chatInputEditTextHolder = new WidgetChatInputEditText(l, null, 2, null);
        this.this$0.chatInputTruncatedHint = new WidgetChatInputTruncatedHint(l);
        widgetChatInputTruncatedHint = this.this$0.chatInputTruncatedHint;
        if (widgetChatInputTruncatedHint != null) {
            widgetChatInputTruncatedHint.addBindedTextWatcher(this.this$0);
        }
    }
}
