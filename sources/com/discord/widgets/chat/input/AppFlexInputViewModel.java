package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.annotation.MainThread;
import b.b.a.h.a;
import b.i.a.f.e.o.f;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppPermissionsRequests;
import com.discord.app.AppViewModel;
import com.discord.panels.PanelState;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreExpressionSuggestions;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStickers;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.ChatInputComponentTypes;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.home.WidgetHome;
import com.lytefast.flexinput.FlexInputListener;
import com.lytefast.flexinput.model.Attachment;
import com.lytefast.flexinput.viewmodel.FlexInputState;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
import d0.t.n;
import d0.t.n0;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.k;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.objectweb.asm.Opcodes;
import rx.Observable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: AppFlexInputViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 ]2\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0002]^BG\u0012\u0006\u0010W\u001a\u00020V\u0012\b\b\u0002\u0010Y\u001a\u00020\u0002\u0012\u000e\b\u0002\u0010Z\u001a\b\u0012\u0004\u0012\u00020\u00040\u000b\u0012\b\b\u0002\u0010J\u001a\u00020I\u0012\b\b\u0002\u0010T\u001a\u00020S\u0012\b\b\u0002\u0010Q\u001a\u00020P¢\u0006\u0004\b[\u0010\\J\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00020\u000bH\u0017¢\u0006\u0004\b\f\u0010\rJ\u0015\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u000bH\u0017¢\u0006\u0004\b\u000f\u0010\rJ\u000f\u0010\u0010\u001a\u00020\u0006H\u0017¢\u0006\u0004\b\u0010\u0010\nJ\u000f\u0010\u0011\u001a\u00020\u0006H\u0017¢\u0006\u0004\b\u0011\u0010\nJ\u0017\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0012H\u0017¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0006H\u0017¢\u0006\u0004\b\u0016\u0010\nJ\u000f\u0010\u0017\u001a\u00020\u0006H\u0017¢\u0006\u0004\b\u0017\u0010\nJ!\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u0019\u001a\u00020\u00182\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aH\u0017¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010\u001f\u001a\u00020\u00062\u0006\u0010\u001e\u001a\u00020\u0018H\u0017¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u001aH\u0017¢\u0006\u0004\b!\u0010\"J\u0019\u0010%\u001a\u00020\u00062\b\u0010$\u001a\u0004\u0018\u00010#H\u0017¢\u0006\u0004\b%\u0010&J\u0019\u0010(\u001a\u00020\u00062\b\b\u0002\u0010'\u001a\u00020\u001aH\u0007¢\u0006\u0004\b(\u0010)J\u0017\u0010,\u001a\u00020\u001a2\u0006\u0010+\u001a\u00020*H\u0017¢\u0006\u0004\b,\u0010-J\u0017\u0010.\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u001aH\u0017¢\u0006\u0004\b.\u0010)J#\u00103\u001a\u00020\u00062\u0012\u00102\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u000201000/H\u0017¢\u0006\u0004\b3\u00104J\u000f\u00105\u001a\u00020\u0006H\u0017¢\u0006\u0004\b5\u0010\nJ\u000f\u00106\u001a\u00020\u001aH\u0017¢\u0006\u0004\b6\u0010\"J\u000f\u00107\u001a\u00020\u001aH\u0017¢\u0006\u0004\b7\u0010\"J\u000f\u00108\u001a\u00020\u0006H\u0017¢\u0006\u0004\b8\u0010\nJ\u0017\u0010:\u001a\u00020\u00062\u0006\u00109\u001a\u00020\u001aH\u0016¢\u0006\u0004\b:\u0010)J\u000f\u0010;\u001a\u00020\u0006H\u0016¢\u0006\u0004\b;\u0010\nJ\u001d\u0010>\u001a\u00020\u00062\f\u0010=\u001a\b\u0012\u0004\u0012\u00020\u00060<H\u0016¢\u0006\u0004\b>\u0010?J\u000f\u0010@\u001a\u00020\u001aH\u0016¢\u0006\u0004\b@\u0010\"J\u0017\u0010B\u001a\u00020\u00062\u0006\u0010A\u001a\u00020\u001aH\u0007¢\u0006\u0004\bB\u0010)J\r\u0010C\u001a\u00020\u0006¢\u0006\u0004\bC\u0010\nJ\u001b\u0010E\u001a\b\u0012\u0004\u0012\u00020D0/2\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\bE\u0010FJ\r\u0010G\u001a\u00020\u0006¢\u0006\u0004\bG\u0010\nJ\r\u0010H\u001a\u00020\u0006¢\u0006\u0004\bH\u0010\nR\u0016\u0010J\u001a\u00020I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bJ\u0010KR:\u0010N\u001a&\u0012\f\u0012\n M*\u0004\u0018\u00010\u000e0\u000e M*\u0012\u0012\f\u0012\n M*\u0004\u0018\u00010\u000e0\u000e\u0018\u00010L0L8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bN\u0010OR\u0016\u0010Q\u001a\u00020P8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bQ\u0010RR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u0016\u0010W\u001a\u00020V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010X¨\u0006_"}, d2 = {"Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/lytefast/flexinput/viewmodel/FlexInputState;", "Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;)V", "showKeyboard", "()V", "Lrx/Observable;", "observeState", "()Lrx/Observable;", "Lb/b/a/h/a;", "observeEvents", "onGiftButtonClicked", "onGalleryButtonClicked", "", "index", "onContentDialogPageChanged", "(I)V", "onExpandButtonClicked", "onExpressionTrayButtonClicked", "", "inputText", "", "focused", "onInputTextChanged", "(Ljava/lang/String;Ljava/lang/Boolean;)V", "appendText", "onInputTextAppended", "(Ljava/lang/String;)V", "onInputTextClicked", "()Z", "Lcom/lytefast/flexinput/FlexInputListener;", "inputListener", "onSendButtonClicked", "(Lcom/lytefast/flexinput/FlexInputListener;)V", "clearText", "clean", "(Z)V", "Landroid/view/View;", "button", "onToolTipButtonLongPressed", "(Landroid/view/View;)Z", "onContentDialogDismissed", "", "Lcom/lytefast/flexinput/model/Attachment;", "", "attachments", "onAttachmentsUpdated", "(Ljava/util/List;)V", "onFlexInputFragmentPause", "hideExpressionTray", "showExpressionTray", "onShowDialog", "showBadge", "setShowExpressionTrayButtonBadge", "onCreateThreadSelected", "Lkotlin/Function0;", "onSuccess", "requestMediaPermissions", "(Lkotlin/jvm/functions/Function0;)V", "hasMediaPermissions", "shouldClearInput", "onStickerSuggestionSent", "showKeyboardAndHideExpressionTray", "Lcom/discord/api/sticker/Sticker;", "getMatchingStickers", "(Ljava/lang/String;)Ljava/util/List;", "hideKeyboard", "focus", "Lcom/discord/stores/StoreStickers;", "storeStickers", "Lcom/discord/stores/StoreStickers;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/stores/StoreExpressionSuggestions;", "storeExpressionSuggestions", "Lcom/discord/stores/StoreExpressionSuggestions;", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Lcom/discord/app/AppPermissionsRequests;", "permissionRequests", "Lcom/discord/app/AppPermissionsRequests;", "initialViewState", "storeObservable", HookHelper.constructorName, "(Lcom/discord/app/AppPermissionsRequests;Lcom/lytefast/flexinput/viewmodel/FlexInputState;Lrx/Observable;Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreExpressionSuggestions;)V", "Companion", "StoreState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AppFlexInputViewModel extends AppViewModel<FlexInputState> implements FlexInputViewModel {
    private static final String CHAT_GIFTING_NOTICE = "CHAT_GIFTING_NOTICE";
    public static final Companion Companion = new Companion(null);
    private PublishSubject<a> eventSubject;
    private final AppPermissionsRequests permissionRequests;
    private final StoreAnalytics storeAnalytics;
    private final StoreExpressionSuggestions storeExpressionSuggestions;
    private final StoreStickers storeStickers;

    /* compiled from: AppFlexInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.AppFlexInputViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(AppFlexInputViewModel appFlexInputViewModel) {
            super(1, appFlexInputViewModel, AppFlexInputViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((AppFlexInputViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: AppFlexInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/chat/input/AppFlexInputViewModel$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;", "observeStores", "()Lrx/Observable;", "", AppFlexInputViewModel.CHAT_GIFTING_NOTICE, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores() {
            Observable Y = StoreStream.Companion.getChannelsSelected().observeResolvedSelectedChannel().Y(AppFlexInputViewModel$Companion$observeStores$1.INSTANCE);
            m.checkNotNullExpressionValue(Y, "StoreStream\n          .g…            }\n          }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: AppFlexInputViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001BK\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0006\u0012\u000e\u0010\u0017\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\r\u0012\u0006\u0010\u0019\u001a\u00020\u0010\u0012\u0006\u0010\u001a\u001a\u00020\u0010¢\u0006\u0004\b1\u00102J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0018\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0012Jb\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00022\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00062\u0010\b\u0002\u0010\u0017\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n2\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\r2\b\b\u0002\u0010\u0019\u001a\u00020\u00102\b\b\u0002\u0010\u001a\u001a\u00020\u0010HÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u001a\u0010$\u001a\u00020\u00102\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b'\u0010\u0004R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b(\u0010\u0004R!\u0010\u0017\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b*\u0010\fR\u001b\u0010\u0018\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010+\u001a\u0004\b,\u0010\u000fR\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010-\u001a\u0004\b.\u0010\bR\u0019\u0010\u001a\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010/\u001a\u0004\b\u001a\u0010\u0012R\u0019\u0010\u0019\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010/\u001a\u0004\b0\u0010\u0012¨\u00063"}, d2 = {"Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;", "", "Lcom/discord/panels/PanelState;", "component1", "()Lcom/discord/panels/PanelState;", "component2", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/api/permission/PermissionBit;", "component4", "()Ljava/lang/Long;", "Lcom/discord/stores/StoreNotices$Notice;", "component5", "()Lcom/discord/stores/StoreNotices$Notice;", "", "component6", "()Z", "component7", "leftPanelState", "rightPanelState", "selectedChannel", "channelPermission", "notice", "stickerSuggestionsEnabled", "isThreadCreation", "copy", "(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/api/channel/Channel;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;ZZ)Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/panels/PanelState;", "getRightPanelState", "getLeftPanelState", "Ljava/lang/Long;", "getChannelPermission", "Lcom/discord/stores/StoreNotices$Notice;", "getNotice", "Lcom/discord/api/channel/Channel;", "getSelectedChannel", "Z", "getStickerSuggestionsEnabled", HookHelper.constructorName, "(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;Lcom/discord/api/channel/Channel;Ljava/lang/Long;Lcom/discord/stores/StoreNotices$Notice;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Long channelPermission;
        private final boolean isThreadCreation;
        private final PanelState leftPanelState;
        private final StoreNotices.Notice notice;
        private final PanelState rightPanelState;
        private final Channel selectedChannel;
        private final boolean stickerSuggestionsEnabled;

        public StoreState(PanelState panelState, PanelState panelState2, Channel channel, Long l, StoreNotices.Notice notice, boolean z2, boolean z3) {
            m.checkNotNullParameter(panelState, "leftPanelState");
            m.checkNotNullParameter(panelState2, "rightPanelState");
            this.leftPanelState = panelState;
            this.rightPanelState = panelState2;
            this.selectedChannel = channel;
            this.channelPermission = l;
            this.notice = notice;
            this.stickerSuggestionsEnabled = z2;
            this.isThreadCreation = z3;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, PanelState panelState, PanelState panelState2, Channel channel, Long l, StoreNotices.Notice notice, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                panelState = storeState.leftPanelState;
            }
            if ((i & 2) != 0) {
                panelState2 = storeState.rightPanelState;
            }
            PanelState panelState3 = panelState2;
            if ((i & 4) != 0) {
                channel = storeState.selectedChannel;
            }
            Channel channel2 = channel;
            if ((i & 8) != 0) {
                l = storeState.channelPermission;
            }
            Long l2 = l;
            if ((i & 16) != 0) {
                notice = storeState.notice;
            }
            StoreNotices.Notice notice2 = notice;
            if ((i & 32) != 0) {
                z2 = storeState.stickerSuggestionsEnabled;
            }
            boolean z4 = z2;
            if ((i & 64) != 0) {
                z3 = storeState.isThreadCreation;
            }
            return storeState.copy(panelState, panelState3, channel2, l2, notice2, z4, z3);
        }

        public final PanelState component1() {
            return this.leftPanelState;
        }

        public final PanelState component2() {
            return this.rightPanelState;
        }

        public final Channel component3() {
            return this.selectedChannel;
        }

        public final Long component4() {
            return this.channelPermission;
        }

        public final StoreNotices.Notice component5() {
            return this.notice;
        }

        public final boolean component6() {
            return this.stickerSuggestionsEnabled;
        }

        public final boolean component7() {
            return this.isThreadCreation;
        }

        public final StoreState copy(PanelState panelState, PanelState panelState2, Channel channel, Long l, StoreNotices.Notice notice, boolean z2, boolean z3) {
            m.checkNotNullParameter(panelState, "leftPanelState");
            m.checkNotNullParameter(panelState2, "rightPanelState");
            return new StoreState(panelState, panelState2, channel, l, notice, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.leftPanelState, storeState.leftPanelState) && m.areEqual(this.rightPanelState, storeState.rightPanelState) && m.areEqual(this.selectedChannel, storeState.selectedChannel) && m.areEqual(this.channelPermission, storeState.channelPermission) && m.areEqual(this.notice, storeState.notice) && this.stickerSuggestionsEnabled == storeState.stickerSuggestionsEnabled && this.isThreadCreation == storeState.isThreadCreation;
        }

        public final Long getChannelPermission() {
            return this.channelPermission;
        }

        public final PanelState getLeftPanelState() {
            return this.leftPanelState;
        }

        public final StoreNotices.Notice getNotice() {
            return this.notice;
        }

        public final PanelState getRightPanelState() {
            return this.rightPanelState;
        }

        public final Channel getSelectedChannel() {
            return this.selectedChannel;
        }

        public final boolean getStickerSuggestionsEnabled() {
            return this.stickerSuggestionsEnabled;
        }

        public int hashCode() {
            PanelState panelState = this.leftPanelState;
            int i = 0;
            int hashCode = (panelState != null ? panelState.hashCode() : 0) * 31;
            PanelState panelState2 = this.rightPanelState;
            int hashCode2 = (hashCode + (panelState2 != null ? panelState2.hashCode() : 0)) * 31;
            Channel channel = this.selectedChannel;
            int hashCode3 = (hashCode2 + (channel != null ? channel.hashCode() : 0)) * 31;
            Long l = this.channelPermission;
            int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
            StoreNotices.Notice notice = this.notice;
            if (notice != null) {
                i = notice.hashCode();
            }
            int i2 = (hashCode4 + i) * 31;
            boolean z2 = this.stickerSuggestionsEnabled;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.isThreadCreation;
            if (!z3) {
                i3 = z3 ? 1 : 0;
            }
            return i6 + i3;
        }

        public final boolean isThreadCreation() {
            return this.isThreadCreation;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("StoreState(leftPanelState=");
            R.append(this.leftPanelState);
            R.append(", rightPanelState=");
            R.append(this.rightPanelState);
            R.append(", selectedChannel=");
            R.append(this.selectedChannel);
            R.append(", channelPermission=");
            R.append(this.channelPermission);
            R.append(", notice=");
            R.append(this.notice);
            R.append(", stickerSuggestionsEnabled=");
            R.append(this.stickerSuggestionsEnabled);
            R.append(", isThreadCreation=");
            return b.d.b.a.a.M(R, this.isThreadCreation, ")");
        }
    }

    public /* synthetic */ AppFlexInputViewModel(AppPermissionsRequests appPermissionsRequests, FlexInputState flexInputState, Observable observable, StoreStickers storeStickers, StoreAnalytics storeAnalytics, StoreExpressionSuggestions storeExpressionSuggestions, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(appPermissionsRequests, (i & 2) != 0 ? new FlexInputState(null, false, null, null, false, false, false, false, false, null, null, 2047) : flexInputState, (i & 4) != 0 ? Companion.observeStores() : observable, (i & 8) != 0 ? StoreStream.Companion.getStickers() : storeStickers, (i & 16) != 0 ? StoreStream.Companion.getAnalytics() : storeAnalytics, (i & 32) != 0 ? StoreStream.Companion.getExpressionSuggestions() : storeExpressionSuggestions);
    }

    public static /* synthetic */ void clean$default(AppFlexInputViewModel appFlexInputViewModel, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        appFlexInputViewModel.clean(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        boolean z2;
        boolean z3;
        FlexInputState requireViewState = requireViewState();
        Channel selectedChannel = storeState.getSelectedChannel();
        Long channelPermission = storeState.getChannelPermission();
        PanelState leftPanelState = storeState.getLeftPanelState();
        PanelState.a aVar = PanelState.a.a;
        boolean z4 = (m.areEqual(leftPanelState, aVar) ^ true) || (m.areEqual(storeState.getRightPanelState(), aVar) ^ true);
        boolean z5 = storeState.getNotice() != null && !storeState.getNotice().isInAppNotification() && !storeState.getNotice().isPopup();
        if (selectedChannel == null) {
            z2 = false;
        } else if (storeState.isThreadCreation()) {
            z2 = PermissionUtils.can(Permission.SEND_MESSAGES_IN_THREADS, channelPermission);
        } else {
            z2 = PermissionUtils.INSTANCE.hasAccessWrite(selectedChannel, channelPermission);
        }
        boolean z6 = selectedChannel != null && (ChannelUtils.x(selectedChannel) || PermissionUtils.can(Permission.ATTACH_FILES, channelPermission));
        boolean z7 = requireViewState.g;
        if (z5 || z4) {
            hideKeyboard();
            z3 = false;
        } else {
            z3 = z7;
        }
        updateViewState(FlexInputState.a(requireViewState, null, false, null, null, z2, z6, z3, false, storeState.getStickerSuggestionsEnabled(), selectedChannel != null ? Long.valueOf(selectedChannel.h()) : null, selectedChannel != null ? Long.valueOf(selectedChannel.f()) : null, Opcodes.D2L));
    }

    private final void showKeyboard() {
        PublishSubject<a> publishSubject = this.eventSubject;
        publishSubject.k.onNext(a.c.a);
    }

    @MainThread
    public final void clean(boolean z2) {
        FlexInputState viewState = getViewState();
        if (viewState != null) {
            updateViewState(FlexInputState.a(viewState, z2 ? "" : viewState.a, true, n.emptyList(), null, false, false, false, false, false, null, null, 2032));
        }
    }

    public final void focus() {
        PublishSubject<a> publishSubject = this.eventSubject;
        publishSubject.k.onNext(a.C0059a.a);
    }

    public final List<Sticker> getMatchingStickers(String str) {
        Set<Sticker> set;
        m.checkNotNullParameter(str, "inputText");
        AppFlexInputViewModel$getMatchingStickers$1 appFlexInputViewModel$getMatchingStickers$1 = AppFlexInputViewModel$getMatchingStickers$1.INSTANCE;
        int length = str.length();
        if (3 <= length && 50 >= length) {
            set = appFlexInputViewModel$getMatchingStickers$1.invoke(str);
        } else {
            set = n0.emptySet();
        }
        return u.toList(set);
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    public boolean hasMediaPermissions() {
        return this.permissionRequests.hasMedia();
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public boolean hideExpressionTray() {
        FlexInputState viewState = getViewState();
        if (viewState == null || !viewState.g) {
            return false;
        }
        updateViewState(FlexInputState.a(viewState, null, false, null, null, false, false, false, false, false, null, null, 1983));
        return true;
    }

    public final void hideKeyboard() {
        PublishSubject<a> publishSubject = this.eventSubject;
        publishSubject.k.onNext(a.b.a);
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public Observable<a> observeEvents() {
        PublishSubject<a> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public Observable<FlexInputState> observeState() {
        return observeViewState();
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onAttachmentsUpdated(List<? extends Attachment<? extends Object>> list) {
        m.checkNotNullParameter(list, "attachments");
        updateViewState(FlexInputState.a(requireViewState(), null, false, new ArrayList(list), null, false, false, false, false, false, null, null, 2043));
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onContentDialogDismissed(boolean z2) {
        if (z2) {
            showKeyboard();
        }
        updateViewState(FlexInputState.a(requireViewState(), null, false, null, null, false, false, false, false, false, null, null, 2039));
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onContentDialogPageChanged(int i) {
        updateViewState(FlexInputState.a(requireViewState(), null, false, null, Integer.valueOf(i), false, false, false, false, false, null, null, 2039));
        if (i == 0) {
            this.storeAnalytics.trackChatInputComponentViewed(ChatInputComponentTypes.MEDIA_PICKER);
        } else if (i == 1) {
            this.storeAnalytics.trackChatInputComponentViewed(ChatInputComponentTypes.FILES);
        } else if (i == 2) {
            this.storeAnalytics.trackChatInputComponentViewed(ChatInputComponentTypes.CAMERA);
        }
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    public void onCreateThreadSelected() {
        Long l = requireViewState().j;
        Long l2 = requireViewState().k;
        if (l != null) {
            l.longValue();
            if (l2 != null) {
                l2.longValue();
                ChannelSelector.openCreateThread$default(ChannelSelector.Companion.getInstance(), l2.longValue(), l.longValue(), null, "Plus Button", 4, null);
                onContentDialogDismissed(false);
            }
        }
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onExpandButtonClicked() {
        updateViewState(FlexInputState.a(requireViewState(), null, true, null, null, false, false, false, false, false, null, null, 2045));
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onExpressionTrayButtonClicked() {
        FlexInputState requireViewState = requireViewState();
        if (requireViewState.g) {
            showKeyboard();
        } else {
            hideKeyboard();
        }
        updateViewState(FlexInputState.a(requireViewState, null, false, null, null, false, false, !requireViewState.g, false, false, null, null, 1983));
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onFlexInputFragmentPause() {
        hideKeyboard();
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onGalleryButtonClicked() {
        FlexInputState requireViewState = requireViewState();
        if (!requireViewState.f) {
            PublishSubject<a> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new a.e(R.string.cannot_attach_files));
            return;
        }
        hideKeyboard();
        updateViewState(FlexInputState.a(requireViewState, null, false, null, 0, false, false, false, false, false, null, null, 1975));
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onGiftButtonClicked() {
        hideKeyboard();
        StoreStream.Companion.getNotices().requestToShow(new StoreNotices.Notice(CHAT_GIFTING_NOTICE, null, 0L, 0, false, d0.t.m.listOf(a0.getOrCreateKotlinClass(WidgetHome.class)), 0L, false, 0L, AppFlexInputViewModel$onGiftButtonClicked$1.INSTANCE, 150, null));
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onInputTextAppended(String str) {
        m.checkNotNullParameter(str, "appendText");
        f.P0(this, b.d.b.a.a.H(new StringBuilder(), requireViewState().a, str), null, 2, null);
    }

    /* JADX WARN: Removed duplicated region for block: B:23:0x0051  */
    /* JADX WARN: Removed duplicated region for block: B:24:0x0053  */
    /* JADX WARN: Removed duplicated region for block: B:26:0x0056  */
    /* JADX WARN: Removed duplicated region for block: B:29:0x0061  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0064  */
    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onInputTextChanged(java.lang.String r16, java.lang.Boolean r17) {
        /*
            r15 = this;
            r0 = r15
            r2 = r16
            r1 = r17
            java.lang.String r3 = "inputText"
            d0.z.d.m.checkNotNullParameter(r2, r3)
            java.lang.Object r3 = r15.requireViewState()
            com.lytefast.flexinput.viewmodel.FlexInputState r3 = (com.lytefast.flexinput.viewmodel.FlexInputState) r3
            java.lang.String r4 = r3.a
            boolean r4 = d0.z.d.m.areEqual(r2, r4)
            if (r4 != 0) goto L87
            boolean r4 = r3.e
            if (r4 != 0) goto L1e
            goto L87
        L1e:
            java.lang.Boolean r4 = java.lang.Boolean.TRUE
            boolean r4 = d0.z.d.m.areEqual(r1, r4)
            if (r4 == 0) goto L2a
            r15.showKeyboard()
            goto L35
        L2a:
            java.lang.Boolean r4 = java.lang.Boolean.FALSE
            boolean r1 = d0.z.d.m.areEqual(r1, r4)
            if (r1 == 0) goto L35
            r15.hideKeyboard()
        L35:
            boolean r1 = r3.f3144b
            r4 = 1
            r5 = 0
            if (r1 == 0) goto L48
            int r1 = r16.length()
            if (r1 <= 0) goto L43
            r1 = 1
            goto L44
        L43:
            r1 = 0
        L44:
            if (r1 == 0) goto L48
            r6 = 0
            goto L4b
        L48:
            boolean r1 = r3.f3144b
            r6 = r1
        L4b:
            int r1 = r16.length()
            if (r1 <= 0) goto L53
            r1 = 1
            goto L54
        L53:
            r1 = 0
        L54:
            if (r1 == 0) goto L5b
            com.discord.stores.StoreStickers r1 = r0.storeStickers
            r1.fetchEnabledStickerDirectory()
        L5b:
            int r1 = r16.length()
            if (r1 != 0) goto L62
            r5 = 1
        L62:
            if (r5 == 0) goto L69
            com.discord.stores.StoreExpressionSuggestions r1 = r0.storeExpressionSuggestions
            r1.setExpressionSuggestionsEnabled(r4)
        L69:
            r4 = 0
            r5 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 2044(0x7fc, float:2.864E-42)
            r1 = r3
            r2 = r16
            r3 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r13 = r14
            com.lytefast.flexinput.viewmodel.FlexInputState r1 = com.lytefast.flexinput.viewmodel.FlexInputState.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            r15.updateViewState(r1)
        L87:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.input.AppFlexInputViewModel.onInputTextChanged(java.lang.String, java.lang.Boolean):void");
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public boolean onInputTextClicked() {
        showKeyboard();
        updateViewState(FlexInputState.a(requireViewState(), null, false, null, null, false, false, false, false, false, null, null, 1983));
        return false;
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public void onSendButtonClicked(FlexInputListener flexInputListener) {
        FlexInputState viewState = getViewState();
        if (viewState != null && flexInputListener != null) {
            flexInputListener.onSend(viewState.a, viewState.c, new AppFlexInputViewModel$onSendButtonClicked$1(this));
        }
    }

    @MainThread
    public void onShowDialog() {
        hideKeyboard();
    }

    @MainThread
    public final void onStickerSuggestionSent(boolean z2) {
        FlexInputState viewState = getViewState();
        if (viewState != null) {
            updateViewState(FlexInputState.a(viewState, z2 ? "" : viewState.a, true, n.emptyList(), null, false, false, false, false, false, null, null, 2032));
            if (z2) {
                hideKeyboard();
            } else {
                this.storeExpressionSuggestions.setExpressionSuggestionsEnabled(false);
            }
        }
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    @MainThread
    public boolean onToolTipButtonLongPressed(View view) {
        m.checkNotNullParameter(view, "button");
        PublishSubject<a> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new a.d(view.getContentDescription().toString()));
        return true;
    }

    @Override // com.lytefast.flexinput.viewmodel.FlexInputViewModel
    public void requestMediaPermissions(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        this.permissionRequests.requestMedia(function0);
    }

    public void setShowExpressionTrayButtonBadge(boolean z2) {
        updateViewState(FlexInputState.a(requireViewState(), null, false, null, null, false, false, false, z2, false, null, null, 1919));
    }

    @MainThread
    public boolean showExpressionTray() {
        FlexInputState viewState = getViewState();
        if (viewState == null || viewState.g) {
            return false;
        }
        updateViewState(FlexInputState.a(viewState, null, false, null, null, false, false, true, false, false, null, null, 1983));
        return true;
    }

    public final void showKeyboardAndHideExpressionTray() {
        updateViewState(FlexInputState.a(requireViewState(), null, false, null, null, false, false, false, false, false, null, null, 1983));
        showKeyboard();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AppFlexInputViewModel(AppPermissionsRequests appPermissionsRequests, FlexInputState flexInputState, Observable<StoreState> observable, StoreStickers storeStickers, StoreAnalytics storeAnalytics, StoreExpressionSuggestions storeExpressionSuggestions) {
        super(flexInputState);
        m.checkNotNullParameter(appPermissionsRequests, "permissionRequests");
        m.checkNotNullParameter(flexInputState, "initialViewState");
        m.checkNotNullParameter(observable, "storeObservable");
        m.checkNotNullParameter(storeStickers, "storeStickers");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(storeExpressionSuggestions, "storeExpressionSuggestions");
        this.permissionRequests = appPermissionsRequests;
        this.storeStickers = storeStickers;
        this.storeAnalytics = storeAnalytics;
        this.storeExpressionSuggestions = storeExpressionSuggestions;
        this.eventSubject = PublishSubject.k0();
        Observable q = ObservableExtensionsKt.computationLatest(observable).q();
        m.checkNotNullExpressionValue(q, "storeObservable\n        …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this, null, 2, null), AppFlexInputViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
