package com.discord.widgets.chat.input;

import android.content.Context;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.models.ApplicationCommandData;
import com.lytefast.flexinput.model.Attachment;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function4;
/* compiled from: WidgetChatInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\f\u001a\u00020\b2\u0006\u0010\u0001\u001a\u00020\u00002\u0010\u0010\u0004\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00030\u00022\b\b\u0002\u0010\u0006\u001a\u00020\u00052\u0014\b\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b0\u0007H\n¢\u0006\u0004\b\n\u0010\u000b"}, d2 = {"Lcom/discord/widgets/chat/input/models/ApplicationCommandData;", "applicationCommandData", "", "Lcom/lytefast/flexinput/model/Attachment;", "attachmentsRaw", "", "autocomplete", "Lkotlin/Function1;", "", "onValidationResult", "invoke", "(Lcom/discord/widgets/chat/input/models/ApplicationCommandData;Ljava/util/List;ZLkotlin/jvm/functions/Function1;)V", "sendCommand"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInput$configureSendListeners$1 extends o implements Function4<ApplicationCommandData, List<? extends Attachment<?>>, Boolean, Function1<? super Boolean, ? extends Unit>, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ MessageManager $messageManager;
    public final /* synthetic */ WidgetChatInput this$0;

    /* compiled from: WidgetChatInput.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.input.WidgetChatInput$configureSendListeners$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Boolean, Unit> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
            invoke(bool.booleanValue());
            return Unit.a;
        }

        public final void invoke(boolean z2) {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatInput$configureSendListeners$1(WidgetChatInput widgetChatInput, Context context, MessageManager messageManager) {
        super(4);
        this.this$0 = widgetChatInput;
        this.$context = context;
        this.$messageManager = messageManager;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void invoke$default(WidgetChatInput$configureSendListeners$1 widgetChatInput$configureSendListeners$1, ApplicationCommandData applicationCommandData, List list, boolean z2, Function1 function1, int i, Object obj) {
        if ((i & 4) != 0) {
            z2 = false;
        }
        if ((i & 8) != 0) {
            function1 = AnonymousClass1.INSTANCE;
        }
        widgetChatInput$configureSendListeners$1.invoke(applicationCommandData, list, z2, function1);
    }

    @Override // kotlin.jvm.functions.Function4
    public /* bridge */ /* synthetic */ Unit invoke(ApplicationCommandData applicationCommandData, List<? extends Attachment<?>> list, Boolean bool, Function1<? super Boolean, ? extends Unit> function1) {
        invoke(applicationCommandData, list, bool.booleanValue(), (Function1<? super Boolean, Unit>) function1);
        return Unit.a;
    }

    public final void invoke(ApplicationCommandData applicationCommandData, List<? extends Attachment<?>> list, boolean z2, Function1<? super Boolean, Unit> function1) {
        ChatInputViewModel viewModel;
        m.checkNotNullParameter(applicationCommandData, "applicationCommandData");
        m.checkNotNullParameter(list, "attachmentsRaw");
        m.checkNotNullParameter(function1, "onValidationResult");
        viewModel = this.this$0.getViewModel();
        viewModel.sendCommand(this.$context, this.$messageManager, applicationCommandData, list, z2, false, function1);
    }
}
