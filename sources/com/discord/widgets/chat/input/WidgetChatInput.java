package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.graphics.Insets;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.b.c;
import b.a.a.c;
import b.a.d.e0;
import b.a.d.f;
import b.a.i.o4;
import b.a.i.p4;
import b.a.i.q4;
import b.a.k.b;
import b.a.o.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChatInputBinding;
import com.discord.models.member.GuildMember;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.ShareUtils;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.duration.DurationUtilsKt;
import com.discord.utilities.guilds.MemberVerificationUtils;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.locale.LocaleManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.ViewVisibilityObserver;
import com.discord.utilities.views.ViewVisibilityObserverProvider;
import com.discord.widgets.announcements.WidgetChannelFollowSheet;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.ChatInputViewModel;
import com.discord.widgets.chat.input.MessageDraftsRepo;
import com.discord.widgets.chat.input.autocomplete.InputAutocomplete;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.google.android.material.button.MaterialButton;
import com.lytefast.flexinput.FlexInputListener;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.model.Attachment;
import d0.g;
import d0.g0.t;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.text.NumberFormat;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetChatInput.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ì\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\bj\u0010\u0013J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ'\u0010\u0010\u001a\u00020\u00042\n\u0010\r\u001a\u00060\u000bj\u0002`\f2\n\u0010\u000f\u001a\u00060\u000bj\u0002`\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0017\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001b\u0010\u001aJ\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\u001c\u0010\u001aJ\u000f\u0010\u001d\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001d\u0010\u0013J\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001eH\u0002¢\u0006\u0004\b \u0010!J\u0017\u0010\"\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0018H\u0002¢\u0006\u0004\b\"\u0010\u001aJ\u0017\u0010#\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0018H\u0002¢\u0006\u0004\b#\u0010\u001aJ%\u0010&\u001a\u00020\u00042\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010%\u001a\u00020\u0007H\u0002¢\u0006\u0004\b&\u0010'J\u0017\u0010*\u001a\u00020\u00042\u0006\u0010)\u001a\u00020(H\u0003¢\u0006\u0004\b*\u0010+J!\u00101\u001a\u0004\u0018\u0001002\u0006\u0010-\u001a\u00020,2\u0006\u0010/\u001a\u00020.H\u0002¢\u0006\u0004\b1\u00102J!\u00104\u001a\u0004\u0018\u0001032\u0006\u0010-\u001a\u00020,2\u0006\u0010/\u001a\u00020.H\u0002¢\u0006\u0004\b4\u00105J/\u0010:\u001a\u0002002\u0006\u0010-\u001a\u00020,2\u0006\u00107\u001a\u0002062\u0006\u00108\u001a\u00020\u00072\u0006\u00109\u001a\u00020\u0007H\u0002¢\u0006\u0004\b:\u0010;J\u0017\u0010>\u001a\u00020\u00042\u0006\u0010=\u001a\u00020<H\u0016¢\u0006\u0004\b>\u0010?J\u000f\u0010@\u001a\u00020\u0004H\u0016¢\u0006\u0004\b@\u0010\u0013R\u0016\u0010B\u001a\u00020A8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bB\u0010CR\u001d\u0010\u0003\u001a\u00020\u00028B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bD\u0010E\u001a\u0004\bF\u0010GR\u0016\u0010I\u001a\u00020H8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bI\u0010JR\u0018\u0010L\u001a\u0004\u0018\u00010K8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bL\u0010MR\u001d\u0010S\u001a\u00020N8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bO\u0010P\u001a\u0004\bQ\u0010RR\u0018\u0010U\u001a\u0004\u0018\u00010T8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bU\u0010VR\u001d\u0010[\u001a\u00020W8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bX\u0010P\u001a\u0004\bY\u0010ZR\u001d\u0010`\u001a\u00020\\8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b]\u0010P\u001a\u0004\b^\u0010_R\u0018\u0010b\u001a\u0004\u0018\u00010a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bb\u0010cR\u0016\u0010e\u001a\u00020d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\be\u0010fR\u0016\u0010h\u001a\u00020g8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bh\u0010i¨\u0006k"}, d2 = {"Lcom/discord/widgets/chat/input/WidgetChatInput;", "Lcom/discord/app/AppFragment;", "Lcom/discord/databinding/WidgetChatInputBinding;", "binding", "", "onViewBindingDestroy", "(Lcom/discord/databinding/WidgetChatInputBinding;)V", "", "shouldApplyWindowInsets", "setWindowInsetsListeners", "(Z)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "showFollowSheet", "(JJ)V", "populateDirectShareData", "()V", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState;", "viewState", "configureUI", "(Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState;)V", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded;", "configureChatGuard", "(Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded;)V", "configureSendListeners", "configureContextBar", "configureContextBarEditing", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState$Replying;", "model", "configureContextBarReplying", "(Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState$Loaded$PendingReplyState$Replying;)V", "configureInitialText", "configureText", "focused", "clearText", "clearInput", "(Ljava/lang/Boolean;Z)V", "Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/chat/input/ChatInputViewModel$Event;)V", "Landroid/content/Context;", "context", "Lcom/discord/api/guild/GuildVerificationLevel;", "verificationLevelTriggered", "", "getVerificationText", "(Landroid/content/Context;Lcom/discord/api/guild/GuildVerificationLevel;)Ljava/lang/CharSequence;", "", "getVerificationActionText", "(Landroid/content/Context;Lcom/discord/api/guild/GuildVerificationLevel;)Ljava/lang/String;", "Lcom/discord/api/channel/Channel;", "channel", "isBlocked", "hasSendMessagePermissions", "getHint", "(Landroid/content/Context;Lcom/discord/api/channel/Channel;ZZ)Ljava/lang/CharSequence;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/utilities/views/ViewVisibilityObserver;", "inlineVoiceVisibilityObserver", "Lcom/discord/utilities/views/ViewVisibilityObserver;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChatInputBinding;", "Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;", "chatAttachments", "Lcom/discord/widgets/chat/input/WidgetChatInputAttachments;", "Lcom/discord/widgets/chat/input/WidgetChatInputEditText;", "chatInputEditTextHolder", "Lcom/discord/widgets/chat/input/WidgetChatInputEditText;", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel$delegate", "Lkotlin/Lazy;", "getFlexInputViewModel", "()Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel", "Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;", "chatInputTruncatedHint", "Lcom/discord/widgets/chat/input/WidgetChatInputTruncatedHint;", "Lcom/lytefast/flexinput/fragment/FlexInputFragment;", "flexInputFragment$delegate", "getFlexInputFragment", "()Lcom/lytefast/flexinput/fragment/FlexInputFragment;", "flexInputFragment", "Lcom/discord/widgets/chat/input/ChatInputViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/chat/input/ChatInputViewModel;", "viewModel", "Lcom/discord/widgets/chat/input/autocomplete/InputAutocomplete;", "autocomplete", "Lcom/discord/widgets/chat/input/autocomplete/InputAutocomplete;", "Lcom/discord/widgets/chat/input/MessageDraftsRepo;", "messageDraftsRepo", "Lcom/discord/widgets/chat/input/MessageDraftsRepo;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatInput extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChatInput.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChatInputBinding;", 0)};
    private InputAutocomplete autocomplete;
    private WidgetChatInputAttachments chatAttachments;
    private WidgetChatInputEditText chatInputEditTextHolder;
    private WidgetChatInputTruncatedHint chatInputTruncatedHint;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding(this, WidgetChatInput$binding$2.INSTANCE, new WidgetChatInput$binding$3(this));
    private final ViewVisibilityObserver inlineVoiceVisibilityObserver = ViewVisibilityObserverProvider.INSTANCE.get(ViewVisibilityObserverProvider.INLINE_VOICE_FEATURE);
    private final MessageDraftsRepo messageDraftsRepo = MessageDraftsRepo.Provider.INSTANCE.get();
    private final Clock clock = ClockFactory.get();
    private final Lazy flexInputFragment$delegate = g.lazy(new WidgetChatInput$flexInputFragment$2(this));
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(ChatInputViewModel.class), new WidgetChatInput$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetChatInput$viewModel$2.INSTANCE));
    private final Lazy flexInputViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(AppFlexInputViewModel.class), new WidgetChatInput$appActivityViewModels$$inlined$activityViewModels$3(this), new e0(new WidgetChatInput$flexInputViewModel$2(this)));

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            ApplicationStatus.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[ApplicationStatus.REJECTED.ordinal()] = 1;
            iArr[ApplicationStatus.PENDING.ordinal()] = 2;
            GuildVerificationLevel.values();
            int[] iArr2 = new int[5];
            $EnumSwitchMapping$1 = iArr2;
            GuildVerificationLevel guildVerificationLevel = GuildVerificationLevel.LOW;
            iArr2[guildVerificationLevel.ordinal()] = 1;
            iArr2[GuildVerificationLevel.MEDIUM.ordinal()] = 2;
            iArr2[GuildVerificationLevel.HIGH.ordinal()] = 3;
            GuildVerificationLevel guildVerificationLevel2 = GuildVerificationLevel.HIGHEST;
            iArr2[guildVerificationLevel2.ordinal()] = 4;
            GuildVerificationLevel.values();
            int[] iArr3 = new int[5];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[guildVerificationLevel.ordinal()] = 1;
            iArr3[guildVerificationLevel2.ordinal()] = 2;
        }
    }

    public WidgetChatInput() {
        super(R.layout.widget_chat_input);
    }

    private final void clearInput(Boolean bool, boolean z2) {
        WidgetChatInputEditText widgetChatInputEditText = this.chatInputEditTextHolder;
        if (widgetChatInputEditText != null) {
            if (z2) {
                getFlexInputViewModel().onInputTextChanged("", bool);
            }
            getFlexInputViewModel().clean(z2);
            widgetChatInputEditText.clearLastTypingEmission();
            getViewModel().deleteEditingMessage();
        }
    }

    public static /* synthetic */ void clearInput$default(WidgetChatInput widgetChatInput, Boolean bool, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            bool = null;
        }
        if ((i & 2) != 0) {
            z2 = true;
        }
        widgetChatInput.clearInput(bool, z2);
    }

    private final void configureChatGuard(final ChatInputViewModel.ViewState.Loaded loaded) {
        p4 p4Var = getBinding().r;
        m.checkNotNullExpressionValue(p4Var, "binding.guard");
        LinearLayout linearLayout = p4Var.a;
        m.checkNotNullExpressionValue(linearLayout, "binding.guard.root");
        linearLayout.setVisibility(loaded.isLurking() || loaded.isVerificationLevelTriggered() || loaded.isSystemDM() || loaded.getShouldShowFollow() ? 0 : 8);
        q4 q4Var = getBinding().t;
        m.checkNotNullExpressionValue(q4Var, "binding.guardMemberVerification");
        RelativeLayout relativeLayout = q4Var.a;
        m.checkNotNullExpressionValue(relativeLayout, "binding.guardMemberVerification.root");
        p4 p4Var2 = getBinding().r;
        m.checkNotNullExpressionValue(p4Var2, "binding.guard");
        LinearLayout linearLayout2 = p4Var2.a;
        m.checkNotNullExpressionValue(linearLayout2, "binding.guard.root");
        relativeLayout.setVisibility(!(linearLayout2.getVisibility() == 0) && loaded.getShouldShowVerificationGate() ? 0 : 8);
        o4 o4Var = getBinding().f2287s;
        m.checkNotNullExpressionValue(o4Var, "binding.guardCommunicationDisabled");
        RelativeLayout relativeLayout2 = o4Var.a;
        m.checkNotNullExpressionValue(relativeLayout2, "binding.guardCommunicationDisabled.root");
        p4 p4Var3 = getBinding().r;
        m.checkNotNullExpressionValue(p4Var3, "binding.guard");
        LinearLayout linearLayout3 = p4Var3.a;
        m.checkNotNullExpressionValue(linearLayout3, "binding.guard.root");
        relativeLayout2.setVisibility(!(linearLayout3.getVisibility() == 0) && !loaded.getShouldShowVerificationGate() && loaded.isCommunicationDisabled() ? 0 : 8);
        if (loaded.isSystemDM()) {
            getBinding().r.e.setText(R.string.system_dm_channel_description);
            TextView textView = getBinding().r.d;
            m.checkNotNullExpressionValue(textView, "binding.guard.chatInputGuardSubtext");
            ViewExtensions.setTextAndVisibilityBy(textView, getString(R.string.system_dm_channel_description_subtext));
            MaterialButton materialButton = getBinding().r.f176b;
            m.checkNotNullExpressionValue(materialButton, "binding.guard.chatInputGuardAction");
            materialButton.setVisibility(8);
            MaterialButton materialButton2 = getBinding().r.c;
            m.checkNotNullExpressionValue(materialButton2, "binding.guard.chatInputGuardActionSecondary");
            materialButton2.setVisibility(8);
        } else if (loaded.getShouldShowFollow()) {
            getBinding().r.e.setText(R.string.follow_news_chat_input_message);
            MaterialButton materialButton3 = getBinding().r.f176b;
            m.checkNotNullExpressionValue(materialButton3, "binding.guard.chatInputGuardAction");
            ViewExtensions.setTextAndVisibilityBy(materialButton3, getString(R.string.game_popout_follow));
            getBinding().r.f176b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChatInput.this.showFollowSheet(loaded.getChannel().h(), loaded.getChannel().f());
                }
            });
            MaterialButton materialButton4 = getBinding().r.c;
            m.checkNotNullExpressionValue(materialButton4, "binding.guard.chatInputGuardActionSecondary");
            materialButton4.setVisibility(8);
        } else if (loaded.isLurking()) {
            if (loaded.getShouldShowFollow()) {
                getBinding().r.e.setText(R.string.follow_news_chat_input_message);
                MaterialButton materialButton5 = getBinding().r.f176b;
                m.checkNotNullExpressionValue(materialButton5, "binding.guard.chatInputGuardAction");
                ViewExtensions.setTextAndVisibilityBy(materialButton5, getString(R.string.game_popout_follow));
                getBinding().r.f176b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetChatInput.this.showFollowSheet(loaded.getChannel().h(), loaded.getChannel().f());
                    }
                });
                MaterialButton materialButton6 = getBinding().r.c;
                m.checkNotNullExpressionValue(materialButton6, "binding.guard.chatInputGuardActionSecondary");
                ViewExtensions.setTextAndVisibilityBy(materialButton6, getString(R.string.lurker_mode_chat_input_button));
                getBinding().r.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$3
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ChatInputViewModel viewModel;
                        viewModel = WidgetChatInput.this.getViewModel();
                        viewModel.lurkGuild(WidgetChatInput.this);
                    }
                });
                return;
            }
            getBinding().r.e.setText(R.string.lurker_mode_chat_input_message);
            MaterialButton materialButton7 = getBinding().r.f176b;
            m.checkNotNullExpressionValue(materialButton7, "binding.guard.chatInputGuardAction");
            ViewExtensions.setTextAndVisibilityBy(materialButton7, getString(R.string.lurker_mode_chat_input_button));
            getBinding().r.f176b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ChatInputViewModel viewModel;
                    viewModel = WidgetChatInput.this.getViewModel();
                    viewModel.lurkGuild(WidgetChatInput.this);
                }
            });
            MaterialButton materialButton8 = getBinding().r.c;
            m.checkNotNullExpressionValue(materialButton8, "binding.guard.chatInputGuardActionSecondary");
            materialButton8.setVisibility(8);
        } else if (loaded.isCommunicationDisabled()) {
            final String a = f.a.a(360045138571L, null);
            TextView textView2 = getBinding().f2287s.f169b;
            m.checkNotNullExpressionValue(textView2, "binding.guardCommunicati…nicationDisabledGuardText");
            b.m(textView2, R.string.guild_communication_disabled_chat_notice_description, new Object[]{a}, (r4 & 4) != 0 ? b.g.j : null);
            getBinding().f2287s.f169b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChatInputBinding binding;
                    UriHandler uriHandler = UriHandler.INSTANCE;
                    binding = WidgetChatInput.this.getBinding();
                    TextView textView3 = binding.f2287s.f169b;
                    m.checkNotNullExpressionValue(textView3, "binding.guardCommunicati…nicationDisabledGuardText");
                    Context context = textView3.getContext();
                    m.checkNotNullExpressionValue(context, "binding.guardCommunicati…DisabledGuardText.context");
                    UriHandler.handle$default(uriHandler, context, a, null, 4, null);
                }
            });
            TextView textView3 = getBinding().f2287s.c;
            m.checkNotNullExpressionValue(textView3, "binding.guardCommunicati…ionDisabledGuardTimerText");
            textView3.setText(DurationUtilsKt.humanizeCountdownDuration(requireContext(), loaded.getTimeoutLeftMs()));
        } else if (loaded.getShouldShowVerificationGate()) {
            getBinding().t.f182b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$6

                /* compiled from: WidgetChatInput.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$6$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function0<Unit> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    public AnonymousClass1() {
                        super(0);
                    }

                    @Override // kotlin.jvm.functions.Function0
                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2() {
                    }
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationUtils memberVerificationUtils = MemberVerificationUtils.INSTANCE;
                    Context requireContext = WidgetChatInput.this.requireContext();
                    FragmentManager parentFragmentManager = WidgetChatInput.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    MemberVerificationUtils.maybeShowVerificationGate$default(memberVerificationUtils, requireContext, parentFragmentManager, loaded.getChannel().f(), Traits.Location.Page.GUILD_CHANNEL, null, null, AnonymousClass1.INSTANCE, 48, null);
                }
            });
            ApplicationStatus joinRequestStatus = loaded.getJoinRequestStatus();
            if (joinRequestStatus != null) {
                int ordinal = joinRequestStatus.ordinal();
                if (ordinal == 1) {
                    ImageView imageView = getBinding().t.c;
                    m.checkNotNullExpressionValue(imageView, "binding.guardMemberVerif…erVerificationGuardAction");
                    imageView.setVisibility(8);
                    MaterialButton materialButton9 = getBinding().t.d;
                    m.checkNotNullExpressionValue(materialButton9, "binding.guardMemberVerif…erVerificationGuardButton");
                    materialButton9.setVisibility(0);
                    getBinding().t.f.setText(R.string.member_verification_application_confirmation_title);
                    getBinding().t.e.setImageResource(R.drawable.img_member_verification_pending);
                    getBinding().t.c.setImageResource(R.drawable.ic_close_circle_nova_grey_24dp);
                    getBinding().t.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$7

                        /* compiled from: WidgetChatInput.kt */
                        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                        /* renamed from: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$7$1  reason: invalid class name */
                        /* loaded from: classes2.dex */
                        public static final class AnonymousClass1 extends o implements Function0<Unit> {
                            public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                            public AnonymousClass1() {
                                super(0);
                            }

                            @Override // kotlin.jvm.functions.Function0
                            /* renamed from: invoke  reason: avoid collision after fix types in other method */
                            public final void invoke2() {
                            }
                        }

                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            MemberVerificationUtils memberVerificationUtils = MemberVerificationUtils.INSTANCE;
                            Context requireContext = WidgetChatInput.this.requireContext();
                            FragmentManager parentFragmentManager = WidgetChatInput.this.getParentFragmentManager();
                            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                            MemberVerificationUtils.maybeShowVerificationGate$default(memberVerificationUtils, requireContext, parentFragmentManager, loaded.getChannel().f(), Traits.Location.Page.GUILD_CHANNEL, null, null, AnonymousClass1.INSTANCE, 48, null);
                        }
                    });
                    return;
                } else if (ordinal == 2) {
                    MaterialButton materialButton10 = getBinding().t.d;
                    m.checkNotNullExpressionValue(materialButton10, "binding.guardMemberVerif…erVerificationGuardButton");
                    materialButton10.setVisibility(8);
                    ImageView imageView2 = getBinding().t.c;
                    m.checkNotNullExpressionValue(imageView2, "binding.guardMemberVerif…erVerificationGuardAction");
                    imageView2.setVisibility(0);
                    getBinding().t.f.setText(R.string.member_verification_application_rejected_title);
                    getBinding().t.e.setImageResource(R.drawable.img_member_verification_denied);
                    getBinding().t.c.setImageResource(R.drawable.ic_arrow_right_24dp);
                    return;
                }
            }
            MaterialButton materialButton11 = getBinding().t.d;
            m.checkNotNullExpressionValue(materialButton11, "binding.guardMemberVerif…erVerificationGuardButton");
            materialButton11.setVisibility(8);
            ImageView imageView3 = getBinding().t.c;
            m.checkNotNullExpressionValue(imageView3, "binding.guardMemberVerif…erVerificationGuardAction");
            imageView3.setVisibility(0);
            getBinding().t.f.setText(R.string.member_verification_chat_blocker_text);
            getBinding().t.e.setImageResource(R.drawable.img_member_verification_started);
            getBinding().t.c.setImageResource(R.drawable.ic_arrow_right_24dp);
        } else {
            TextView textView4 = getBinding().r.e;
            m.checkNotNullExpressionValue(textView4, "binding.guard.chatInputGuardText");
            TextView textView5 = getBinding().r.e;
            m.checkNotNullExpressionValue(textView5, "binding.guard.chatInputGuardText");
            Context context = textView5.getContext();
            m.checkNotNullExpressionValue(context, "binding.guard.chatInputGuardText.context");
            textView4.setText(getVerificationText(context, loaded.getVerificationLevelTriggered()));
            MaterialButton materialButton12 = getBinding().r.f176b;
            m.checkNotNullExpressionValue(materialButton12, "binding.guard.chatInputGuardAction");
            MaterialButton materialButton13 = getBinding().r.f176b;
            m.checkNotNullExpressionValue(materialButton13, "binding.guard.chatInputGuardAction");
            Context context2 = materialButton13.getContext();
            m.checkNotNullExpressionValue(context2, "binding.guard.chatInputGuardAction.context");
            ViewExtensions.setTextAndVisibilityBy(materialButton12, getVerificationActionText(context2, loaded.getVerificationLevelTriggered()));
            getBinding().r.f176b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureChatGuard$8
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ChatInputViewModel viewModel;
                    viewModel = WidgetChatInput.this.getViewModel();
                    m.checkNotNullExpressionValue(view, "it");
                    Context context3 = view.getContext();
                    m.checkNotNullExpressionValue(context3, "it.context");
                    viewModel.verifyAccount(context3);
                }
            });
            MaterialButton materialButton14 = getBinding().r.c;
            m.checkNotNullExpressionValue(materialButton14, "binding.guard.chatInputGuardActionSecondary");
            materialButton14.setVisibility(8);
        }
    }

    private final void configureContextBar(ChatInputViewModel.ViewState.Loaded loaded) {
        if (loaded.isEditing()) {
            configureContextBarEditing();
        } else if (loaded.getPendingReplyState() instanceof ChatInputViewModel.ViewState.Loaded.PendingReplyState.Replying) {
            configureContextBarReplying((ChatInputViewModel.ViewState.Loaded.PendingReplyState.Replying) loaded.getPendingReplyState());
        } else {
            RelativeLayout relativeLayout = getBinding().e;
            m.checkNotNullExpressionValue(relativeLayout, "binding.chatInputContextBar");
            relativeLayout.setVisibility(8);
        }
    }

    private final void configureContextBarEditing() {
        RelativeLayout relativeLayout = getBinding().e;
        m.checkNotNullExpressionValue(relativeLayout, "binding.chatInputContextBar");
        relativeLayout.setVisibility(0);
        RelativeLayout relativeLayout2 = getBinding().e;
        m.checkNotNullExpressionValue(relativeLayout2, "binding.chatInputContextBar");
        relativeLayout2.setClickable(false);
        getBinding().g.setText(R.string.editing_message);
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureContextBarEditing$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChatInput.clearInput$default(WidgetChatInput.this, Boolean.FALSE, false, 2, null);
            }
        });
        LinearLayout linearLayout = getBinding().h;
        m.checkNotNullExpressionValue(linearLayout, "binding.chatInputContextReplyMentionButton");
        linearLayout.setVisibility(8);
    }

    private final void configureContextBarReplying(final ChatInputViewModel.ViewState.Loaded.PendingReplyState.Replying replying) {
        String str;
        Context requireContext = requireContext();
        RelativeLayout relativeLayout = getBinding().e;
        m.checkNotNullExpressionValue(relativeLayout, "binding.chatInputContextBar");
        int i = 0;
        relativeLayout.setVisibility(0);
        GuildMember repliedAuthorGuildMember = replying.getRepliedAuthorGuildMember();
        if (repliedAuthorGuildMember == null || (str = repliedAuthorGuildMember.getNick()) == null) {
            str = replying.getRepliedAuthor().getUsername();
        }
        TextView textView = getBinding().g;
        m.checkNotNullExpressionValue(textView, "binding.chatInputContextDescription");
        b.m(textView, R.string.mobile_replying_to, new Object[]{str}, new WidgetChatInput$configureContextBarReplying$1(requireContext, replying));
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureContextBarReplying$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChatInputViewModel viewModel;
                viewModel = WidgetChatInput.this.getViewModel();
                viewModel.jumpToMessageReference(replying.getMessageReference());
            }
        });
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureContextBarReplying$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChatInputViewModel viewModel;
                viewModel = WidgetChatInput.this.getViewModel();
                viewModel.deletePendingReply();
            }
        });
        LinearLayout linearLayout = getBinding().h;
        m.checkNotNullExpressionValue(linearLayout, "binding.chatInputContextReplyMentionButton");
        if (!replying.getShowMentionToggle()) {
            i = 8;
        }
        linearLayout.setVisibility(i);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureContextBarReplying$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChatInputViewModel viewModel;
                viewModel = WidgetChatInput.this.getViewModel();
                viewModel.togglePendingReplyShouldMention();
            }
        });
        int themedColor = replying.getShouldMention() ? ColorCompat.getThemedColor(requireContext, (int) R.attr.colorControlBrandForeground) : ColorCompat.getThemedColor(requireContext, (int) R.attr.colorTextMuted);
        ImageView imageView = getBinding().i;
        m.checkNotNullExpressionValue(imageView, "binding.chatInputContextReplyMentionButtonImage");
        ColorCompatKt.tintWithColor(imageView, themedColor);
        getBinding().j.setTextColor(themedColor);
        getBinding().j.setText(replying.getShouldMention() ? R.string.reply_mention_on : R.string.reply_mention_off);
    }

    private final void configureInitialText(ChatInputViewModel.ViewState.Loaded loaded) {
        String str;
        getFlexInputViewModel().hideExpressionTray();
        if (!loaded.isEditing() || loaded.getEditingMessage() == null) {
            CharSequence textChannelInput = this.messageDraftsRepo.getTextChannelInput(loaded.getChannelId());
            AppFlexInputViewModel flexInputViewModel = getFlexInputViewModel();
            if (textChannelInput == null || (str = textChannelInput.toString()) == null) {
                str = "";
            }
            flexInputViewModel.onInputTextChanged(str, (loaded.isEditing() || loaded.isReplying()) ? Boolean.TRUE : null);
            return;
        }
        getFlexInputViewModel().onInputTextChanged(loaded.getEditingMessage().getContent().toString(), Boolean.TRUE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureSendListeners(final ChatInputViewModel.ViewState.Loaded loaded) {
        WidgetChatInputEditText widgetChatInputEditText = this.chatInputEditTextHolder;
        if (widgetChatInputEditText != null) {
            Context requireContext = requireContext();
            MessageManager messageManager = new MessageManager(requireContext, null, null, null, null, null, null, null, null, 510, null);
            WidgetChatInput$configureSendListeners$1 widgetChatInput$configureSendListeners$1 = new WidgetChatInput$configureSendListeners$1(this, requireContext, messageManager);
            final WidgetChatInput$configureSendListeners$3 widgetChatInput$configureSendListeners$3 = new WidgetChatInput$configureSendListeners$3(this, widgetChatInput$configureSendListeners$1, new WidgetChatInput$configureSendListeners$2(this, widgetChatInputEditText, requireContext, messageManager));
            final WidgetChatInput$configureSendListeners$4 widgetChatInput$configureSendListeners$4 = new WidgetChatInput$configureSendListeners$4(requireContext);
            boolean z2 = widgetChatInputEditText.getChannelId() != loaded.getChannelId();
            widgetChatInputEditText.setChannelId(loaded.getChannelId(), z2);
            InputAutocomplete inputAutocomplete = this.autocomplete;
            if (inputAutocomplete != null) {
                inputAutocomplete.setOnPerformCommandAutocomplete(new WidgetChatInput$configureSendListeners$5(this, widgetChatInput$configureSendListeners$1));
            }
            widgetChatInputEditText.setOnSendListener(new WidgetChatInput$configureSendListeners$6(this, widgetChatInput$configureSendListeners$3));
            if (z2) {
                configureInitialText(loaded);
            }
            configureText(loaded);
            WidgetChatInputAttachments widgetChatInputAttachments = this.chatAttachments;
            if (widgetChatInputAttachments == null) {
                m.throwUninitializedPropertyAccessException("chatAttachments");
            }
            widgetChatInputAttachments.setInputListener(new FlexInputListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$configureSendListeners$7

                /* compiled from: WidgetChatInput.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "validationSucceeded", "", "invoke", "(Z)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.input.WidgetChatInput$configureSendListeners$7$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function1<Boolean, Unit> {
                    public final /* synthetic */ Function1 $onSendResult;

                    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                    public AnonymousClass1(Function1 function1) {
                        super(1);
                        this.$onSendResult = function1;
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
                        invoke(bool.booleanValue());
                        return Unit.a;
                    }

                    public final void invoke(boolean z2) {
                        if (z2) {
                            WidgetChatInput.clearInput$default(WidgetChatInput.this, null, false, 3, null);
                        }
                        this.$onSendResult.invoke(Boolean.valueOf(z2));
                    }
                }

                @Override // com.lytefast.flexinput.FlexInputListener
                public final void onSend(String str, List<? extends Attachment<?>> list, Function1<? super Boolean, Unit> function1) {
                    m.checkNotNullParameter(list, "list");
                    m.checkNotNullParameter(function1, "onSendResult");
                    if (loaded.isOnCooldown() && !loaded.isEditing() && loaded.getSelectedThreadDraft() == null) {
                        function1.invoke(Boolean.valueOf(widgetChatInput$configureSendListeners$4.invoke(R.string.channel_slowmode_desc_short)));
                    } else if (loaded.isEditing() && (!list.isEmpty())) {
                        function1.invoke(Boolean.valueOf(widgetChatInput$configureSendListeners$4.invoke(R.string.editing_with_attachment_error)));
                    } else if (loaded.getAbleToSendMessage()) {
                        widgetChatInput$configureSendListeners$3.invoke2(list, (Function1<? super Boolean, Unit>) new AnonymousClass1(function1));
                    } else {
                        function1.invoke(Boolean.valueOf(widgetChatInput$configureSendListeners$4.invoke(R.string.no_send_messages_permission_placeholder)));
                    }
                }
            });
        }
    }

    private final void configureText(ChatInputViewModel.ViewState.Loaded loaded) {
        if (!loaded.getAbleToSendMessage()) {
            b.i.a.f.e.o.f.P0(getFlexInputViewModel(), "", null, 2, null);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(ChatInputViewModel.ViewState viewState) {
        int i = 8;
        if (viewState instanceof ChatInputViewModel.ViewState.Loading) {
            LinearLayout linearLayout = getBinding().q;
            m.checkNotNullExpressionValue(linearLayout, "binding.chatInputWrap");
            linearLayout.setVisibility(8);
            q4 q4Var = getBinding().t;
            m.checkNotNullExpressionValue(q4Var, "binding.guardMemberVerification");
            RelativeLayout relativeLayout = q4Var.a;
            m.checkNotNullExpressionValue(relativeLayout, "binding.guardMemberVerification.root");
            relativeLayout.setVisibility(8);
            o4 o4Var = getBinding().f2287s;
            m.checkNotNullExpressionValue(o4Var, "binding.guardCommunicationDisabled");
            RelativeLayout relativeLayout2 = o4Var.a;
            m.checkNotNullExpressionValue(relativeLayout2, "binding.guardCommunicationDisabled.root");
            relativeLayout2.setVisibility(8);
            p4 p4Var = getBinding().r;
            m.checkNotNullExpressionValue(p4Var, "binding.guard");
            LinearLayout linearLayout2 = p4Var.a;
            m.checkNotNullExpressionValue(linearLayout2, "binding.guard.root");
            linearLayout2.setVisibility(8);
        } else if (viewState instanceof ChatInputViewModel.ViewState.Loaded) {
            WidgetChatInputAttachments widgetChatInputAttachments = this.chatAttachments;
            if (widgetChatInputAttachments == null) {
                m.throwUninitializedPropertyAccessException("chatAttachments");
            }
            ChatInputViewModel.ViewState.Loaded loaded = (ChatInputViewModel.ViewState.Loaded) viewState;
            widgetChatInputAttachments.configureFlexInputContentPages(loaded.getShowCreateThreadOption());
            LinearLayout linearLayout3 = getBinding().q;
            m.checkNotNullExpressionValue(linearLayout3, "binding.chatInputWrap");
            if (loaded.isInputShowing()) {
                i = 0;
            }
            linearLayout3.setVisibility(i);
            configureChatGuard(loaded);
            getFlexInputFragment().i(new WidgetChatInput$configureUI$1(this, viewState));
            configureContextBar(loaded);
            getFlexInputViewModel().setShowExpressionTrayButtonBadge(loaded.getShouldBadgeChatInput());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChatInputBinding getBinding() {
        return (WidgetChatInputBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final FlexInputFragment getFlexInputFragment() {
        return (FlexInputFragment) this.flexInputFragment$delegate.getValue();
    }

    private final AppFlexInputViewModel getFlexInputViewModel() {
        return (AppFlexInputViewModel) this.flexInputViewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final CharSequence getHint(Context context, Channel channel, boolean z2, boolean z3) {
        CharSequence b2;
        if (z2) {
            String string = context.getString(R.string.dm_verification_text_blocked);
            m.checkNotNullExpressionValue(string, "context.getString(R.stri…erification_text_blocked)");
            return string;
        } else if (!z3) {
            String string2 = context.getString(R.string.no_send_messages_permission_placeholder);
            m.checkNotNullExpressionValue(string2, "context.getString(R.stri…s_permission_placeholder)");
            return string2;
        } else {
            b2 = b.b(context, R.string.textarea_placeholder, new Object[]{ChannelUtils.e(channel, context, false, 2)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        }
    }

    private final String getVerificationActionText(Context context, GuildVerificationLevel guildVerificationLevel) {
        int ordinal = guildVerificationLevel.ordinal();
        if (ordinal == 1) {
            return context.getString(R.string.verify_account);
        }
        if (ordinal != 4) {
            return null;
        }
        return context.getString(R.string.verify_phone);
    }

    private final CharSequence getVerificationText(Context context, GuildVerificationLevel guildVerificationLevel) {
        CharSequence b2;
        CharSequence b3;
        int ordinal = guildVerificationLevel.ordinal();
        if (ordinal == 1) {
            return context.getString(R.string.guild_verification_text_not_claimed);
        }
        if (ordinal == 2) {
            b2 = b.b(context, R.string.guild_verification_text_account_age, new Object[]{"5"}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b2;
        } else if (ordinal == 3) {
            b3 = b.b(context, R.string.guild_verification_text_member_age, new Object[]{"10"}, (r4 & 4) != 0 ? b.C0034b.j : null);
            return b3;
        } else if (ordinal != 4) {
            return null;
        } else {
            return context.getString(R.string.guild_verification_text_not_phone_verified);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ChatInputViewModel getViewModel() {
        return (ChatInputViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    @SuppressLint({"StringFormatMatches"})
    public final void handleEvent(ChatInputViewModel.Event event) {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        Unit unit = null;
        if (event instanceof ChatInputViewModel.Event.FilesTooLarge) {
            getFlexInputViewModel().hideKeyboard();
            c.b bVar = c.k;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            ChatInputViewModel.Event.FilesTooLarge filesTooLarge = (ChatInputViewModel.Event.FilesTooLarge) event;
            float currentFileSizeMB = filesTooLarge.getCurrentFileSizeMB();
            float maxAttachmentSizeMB = filesTooLarge.getMaxAttachmentSizeMB();
            bVar.a(parentFragmentManager, filesTooLarge.isUserPremium(), filesTooLarge.getMaxFileSizeMB(), maxAttachmentSizeMB, currentFileSizeMB, new WidgetChatInput$handleEvent$1(this, event), filesTooLarge.getAttachments().size(), filesTooLarge.getHasImage(), filesTooLarge.getHasVideo(), filesTooLarge.getHasGif());
            clearInput$default(this, null, false, 1, null);
            unit = Unit.a;
        } else if (event instanceof ChatInputViewModel.Event.MessageTooLong) {
            getFlexInputViewModel().hideKeyboard();
            NumberFormat numberInstance = NumberFormat.getNumberInstance(new LocaleManager().getPrimaryLocale(requireContext()));
            WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
            FragmentManager parentFragmentManager2 = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
            String string = getString(R.string.message_too_long_header);
            ChatInputViewModel.Event.MessageTooLong messageTooLong = (ChatInputViewModel.Event.MessageTooLong) event;
            e3 = b.e(this, R.string.message_too_long_body_text, new Object[]{numberInstance.format(Integer.valueOf(messageTooLong.getCurrentCharacterCount())), numberInstance.format(Integer.valueOf(messageTooLong.getMaxCharacterCount()))}, (r4 & 4) != 0 ? b.a.j : null);
            WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager2, string, e3, getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
            unit = Unit.a;
        } else if (event instanceof ChatInputViewModel.Event.EmptyThreadName) {
            getFlexInputViewModel().hideKeyboard();
            WidgetNoticeDialog.Companion companion2 = WidgetNoticeDialog.Companion;
            FragmentManager parentFragmentManager3 = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager3, "parentFragmentManager");
            e2 = b.e(this, R.string.form_thread_name_required_error, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            WidgetNoticeDialog.Companion.show$default(companion2, parentFragmentManager3, null, e2, getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16370, null);
            unit = Unit.a;
        } else if (event instanceof ChatInputViewModel.Event.FailedDeliveryToRecipient) {
            getFlexInputViewModel().hideKeyboard();
            WidgetNoticeDialog.Companion companion3 = WidgetNoticeDialog.Companion;
            FragmentManager parentFragmentManager4 = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager4, "parentFragmentManager");
            String string2 = getString(R.string.error);
            e = b.e(this, R.string.bot_dm_send_failed_with_help_link_mobile, new Object[]{f.a.a(360060145013L, null)}, (r4 & 4) != 0 ? b.a.j : null);
            WidgetNoticeDialog.Companion.show$default(companion3, parentFragmentManager4, string2, e, getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
            unit = Unit.a;
        } else if (event instanceof ChatInputViewModel.Event.AppendChatText) {
            getFlexInputViewModel().onInputTextAppended(((ChatInputViewModel.Event.AppendChatText) event).getText());
            unit = Unit.a;
        } else if (event instanceof ChatInputViewModel.Event.SetChatText) {
            getFlexInputViewModel().hideExpressionTray();
            getFlexInputViewModel().onInputTextChanged(((ChatInputViewModel.Event.SetChatText) event).getText(), Boolean.TRUE);
            unit = Unit.a;
        } else if (event instanceof ChatInputViewModel.Event.CommandInputsInvalid) {
            InputAutocomplete inputAutocomplete = this.autocomplete;
            if (inputAutocomplete != null) {
                inputAutocomplete.onCommandInputsSendError();
                unit = Unit.a;
            }
        } else if (event instanceof ChatInputViewModel.Event.ShowPremiumUpsell) {
            c.b bVar2 = b.a.a.b.c.k;
            FragmentManager parentFragmentManager5 = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager5, "parentFragmentManager");
            ChatInputViewModel.Event.ShowPremiumUpsell showPremiumUpsell = (ChatInputViewModel.Event.ShowPremiumUpsell) event;
            c.b.a(bVar2, parentFragmentManager5, showPremiumUpsell.getPage(), getString(showPremiumUpsell.getHeaderResId()), getString(showPremiumUpsell.getBodyResId()), null, null, null, null, showPremiumUpsell.getShowOtherPages(), showPremiumUpsell.getShowLearnMore(), 240);
            unit = Unit.a;
        } else if (event instanceof ChatInputViewModel.Event.ThreadDraftClosed) {
            getFlexInputViewModel().hideKeyboard();
            clearInput$default(this, null, false, 3, null);
            unit = Unit.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onViewBindingDestroy(WidgetChatInputBinding widgetChatInputBinding) {
        b.a.o.b a = b.C0038b.a();
        LinearLayout linearLayout = widgetChatInputBinding.q;
        m.checkNotNullExpressionValue(linearLayout, "binding.chatInputWrap");
        a.c(linearLayout);
    }

    private final void populateDirectShareData() {
        ContentResolver contentResolver;
        List<Uri> uris;
        Long directShareId = IntentUtils.INSTANCE.getDirectShareId(getMostRecentIntent());
        if (directShareId != null) {
            long longValue = directShareId.longValue();
            boolean z2 = true;
            ShareUtils.SharedContent sharedContent = ShareUtils.INSTANCE.getSharedContent(getMostRecentIntent(), true);
            CharSequence text = sharedContent.getText();
            if (text != null && !t.isBlank(text)) {
                z2 = false;
            }
            if (!z2) {
                this.messageDraftsRepo.setTextChannelInput(longValue, sharedContent.getText().toString());
            }
            Context context = getContext();
            if (!(context == null || (contentResolver = context.getContentResolver()) == null || (uris = sharedContent.getUris()) == null)) {
                for (Uri uri : uris) {
                    WidgetChatInputAttachments widgetChatInputAttachments = this.chatAttachments;
                    if (widgetChatInputAttachments == null) {
                        m.throwUninitializedPropertyAccessException("chatAttachments");
                    }
                    widgetChatInputAttachments.addExternalAttachment(Attachment.Companion.b(uri, contentResolver));
                }
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setWindowInsetsListeners(final boolean z2) {
        LinearLayout linearLayout = getBinding().q;
        m.checkNotNullExpressionValue(linearLayout, "binding.chatInputWrap");
        ViewExtensions.setForwardingWindowInsetsListener(linearLayout);
        FragmentContainerView fragmentContainerView = getBinding().p;
        m.checkNotNullExpressionValue(fragmentContainerView, "binding.chatInputWidget");
        ViewExtensions.setForwardingWindowInsetsListener(fragmentContainerView);
        p4 p4Var = getBinding().r;
        m.checkNotNullExpressionValue(p4Var, "binding.guard");
        ViewCompat.setOnApplyWindowInsetsListener(p4Var.a, WidgetChatInput$setWindowInsetsListeners$1.INSTANCE);
        q4 q4Var = getBinding().t;
        m.checkNotNullExpressionValue(q4Var, "binding.guardMemberVerification");
        ViewCompat.setOnApplyWindowInsetsListener(q4Var.a, WidgetChatInput$setWindowInsetsListeners$2.INSTANCE);
        o4 o4Var = getBinding().f2287s;
        m.checkNotNullExpressionValue(o4Var, "binding.guardCommunicationDisabled");
        ViewCompat.setOnApplyWindowInsetsListener(o4Var.a, WidgetChatInput$setWindowInsetsListeners$3.INSTANCE);
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().d, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.chat.input.WidgetChatInput$setWindowInsetsListeners$4
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetChatInputBinding binding;
                WidgetChatInputBinding binding2;
                WidgetChatInputBinding binding3;
                WidgetChatInputBinding binding4;
                m.checkNotNullParameter(view, "<anonymous parameter 0>");
                m.checkNotNullParameter(windowInsetsCompat, "insets");
                WindowInsetsCompat build = new WindowInsetsCompat.Builder().setSystemWindowInsets(Insets.of(0, 0, 0, z2 ? windowInsetsCompat.getSystemWindowInsetBottom() : 0)).build();
                m.checkNotNullExpressionValue(build, "WindowInsetsCompat.Build…        )\n      ).build()");
                binding = WidgetChatInput.this.getBinding();
                ViewCompat.dispatchApplyWindowInsets(binding.q, build);
                binding2 = WidgetChatInput.this.getBinding();
                p4 p4Var2 = binding2.r;
                m.checkNotNullExpressionValue(p4Var2, "binding.guard");
                ViewCompat.dispatchApplyWindowInsets(p4Var2.a, build);
                binding3 = WidgetChatInput.this.getBinding();
                q4 q4Var2 = binding3.t;
                m.checkNotNullExpressionValue(q4Var2, "binding.guardMemberVerification");
                ViewCompat.dispatchApplyWindowInsets(q4Var2.a, build);
                binding4 = WidgetChatInput.this.getBinding();
                o4 o4Var2 = binding4.f2287s;
                m.checkNotNullExpressionValue(o4Var2, "binding.guardCommunicationDisabled");
                ViewCompat.dispatchApplyWindowInsets(o4Var2.a, build);
                return windowInsetsCompat.consumeSystemWindowInsets();
            }
        });
        getBinding().d.requestApplyInsets();
    }

    public static /* synthetic */ void setWindowInsetsListeners$default(WidgetChatInput widgetChatInput, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        widgetChatInput.setWindowInsetsListeners(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showFollowSheet(long j, long j2) {
        WidgetChannelFollowSheet.Companion companion = WidgetChannelFollowSheet.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.show(parentFragmentManager, j, j2);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        WidgetChatInputAttachments widgetChatInputAttachments = new WidgetChatInputAttachments(getFlexInputFragment());
        this.chatAttachments = widgetChatInputAttachments;
        if (widgetChatInputAttachments == null) {
            m.throwUninitializedPropertyAccessException("chatAttachments");
        }
        widgetChatInputAttachments.configureFlexInputFragment(this);
        getFlexInputFragment().i(new WidgetChatInput$onViewBound$1(this));
        b.a.o.b a = b.C0038b.a();
        LinearLayout linearLayout = getBinding().q;
        m.checkNotNullExpressionValue(linearLayout, "binding.chatInputWrap");
        a.b(linearLayout);
        setWindowInsetsListeners$default(this, false, 1, null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        populateDirectShareData();
        WidgetChatInputTruncatedHint widgetChatInputTruncatedHint = this.chatInputTruncatedHint;
        if (widgetChatInputTruncatedHint != null) {
            widgetChatInputTruncatedHint.addBindedTextWatcher(this);
        }
        Observable<ChatInputViewModel.ViewState> q = getViewModel().observeChatInputViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetChatInput.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatInput$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetChatInput.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatInput$onViewBoundOrOnResume$2(this));
        WidgetChatInputAttachments widgetChatInputAttachments = this.chatAttachments;
        if (widgetChatInputAttachments == null) {
            m.throwUninitializedPropertyAccessException("chatAttachments");
        }
        widgetChatInputAttachments.setViewModel(getFlexInputViewModel());
        InputAutocomplete inputAutocomplete = this.autocomplete;
        if (inputAutocomplete != null) {
            inputAutocomplete.onViewBoundOrOnResume();
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(this.inlineVoiceVisibilityObserver.observeIsVisible(), this, null, 2, null), WidgetChatInput.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatInput$onViewBoundOrOnResume$3(this));
    }
}
