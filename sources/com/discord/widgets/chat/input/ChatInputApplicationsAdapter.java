package com.discord.widgets.chat.input;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.CommandCategoryItemBinding;
import com.discord.models.commands.Application;
import com.discord.models.domain.ModelAuditLogEntry;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.n;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatInputCategoriesAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b(\u0010)J\u001b\u0010\u0007\u001a\u00020\u00062\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0019\u0010\f\u001a\u00020\u00062\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u000f\u001a\u00020\u000e2\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001c\u001a\u00020\t2\u0006\u0010\u0017\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\u00060\tj\u0002`\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001c\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0005\u0010 R.\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00060!8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%\"\u0004\b&\u0010'¨\u0006*"}, d2 = {"Lcom/discord/widgets/chat/input/ChatInputApplicationsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/chat/input/ApplicationCategoryViewHolder;", "", "Lcom/discord/models/commands/Application;", "data", "", "setApplicationData", "(Ljava/util/List;)V", "", "Lcom/discord/primitives/ApplicationId;", "applicationId", "selectApplication", "(J)V", "", "getPositionOfApplication", "(J)I", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/chat/input/ApplicationCategoryViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onBindViewHolder", "(Lcom/discord/widgets/chat/input/ApplicationCategoryViewHolder;I)V", "getItemCount", "()I", "getItemId", "(I)J", "selectedApplication", "J", "Ljava/util/List;", "Lkotlin/Function1;", "onClickApplication", "Lkotlin/jvm/functions/Function1;", "getOnClickApplication", "()Lkotlin/jvm/functions/Function1;", "setOnClickApplication", "(Lkotlin/jvm/functions/Function1;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatInputApplicationsAdapter extends RecyclerView.Adapter<ApplicationCategoryViewHolder> {
    private List<Application> data = n.emptyList();
    public Function1<? super Application, Unit> onClickApplication;
    private long selectedApplication;

    public ChatInputApplicationsAdapter() {
        setHasStableIds(true);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return this.data.get(i).getId();
    }

    public final Function1<Application, Unit> getOnClickApplication() {
        Function1 function1 = this.onClickApplication;
        if (function1 == null) {
            m.throwUninitializedPropertyAccessException("onClickApplication");
        }
        return function1;
    }

    public final int getPositionOfApplication(long j) {
        Iterator<Application> it = this.data.iterator();
        int i = 0;
        while (it.hasNext()) {
            if (it.next().getId() == j) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public final void selectApplication(long j) {
        if (this.selectedApplication != j) {
            this.selectedApplication = j;
            notifyDataSetChanged();
        }
    }

    public final void setApplicationData(List<Application> list) {
        m.checkNotNullParameter(list, "data");
        this.data = list;
        notifyDataSetChanged();
    }

    public final void setOnClickApplication(Function1<? super Application, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClickApplication = function1;
    }

    public void onBindViewHolder(ApplicationCategoryViewHolder applicationCategoryViewHolder, int i) {
        m.checkNotNullParameter(applicationCategoryViewHolder, "holder");
        applicationCategoryViewHolder.bind(this.data.get(i), this.selectedApplication == this.data.get(i).getId());
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public ApplicationCategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.command_category_item, viewGroup, false);
        int i2 = R.id.command_category_item_icon;
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.command_category_item_icon);
        if (simpleDraweeView != null) {
            i2 = R.id.overline;
            View findViewById = inflate.findViewById(R.id.overline);
            if (findViewById != null) {
                CommandCategoryItemBinding commandCategoryItemBinding = new CommandCategoryItemBinding((FrameLayout) inflate, simpleDraweeView, new b.a.i.n(findViewById, findViewById));
                m.checkNotNullExpressionValue(commandCategoryItemBinding, "CommandCategoryItemBindi…tInflater, parent, false)");
                Function1<? super Application, Unit> function1 = this.onClickApplication;
                if (function1 == null) {
                    m.throwUninitializedPropertyAccessException("onClickApplication");
                }
                return new ApplicationCategoryViewHolder(commandCategoryItemBinding, function1);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }
}
