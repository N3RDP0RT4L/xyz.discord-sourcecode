package com.discord.widgets.chat.input;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.panels.PanelState;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.widgets.chat.input.AppFlexInputViewModel;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func6;
/* compiled from: AppFlexInputViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;", "kotlin.jvm.PlatformType", "selectedChannel", "Lrx/Observable;", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreChannelsSelected$ResolvedSelectedChannel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AppFlexInputViewModel$Companion$observeStores$1<T, R> implements b<StoreChannelsSelected.ResolvedSelectedChannel, Observable<? extends AppFlexInputViewModel.StoreState>> {
    public static final AppFlexInputViewModel$Companion$observeStores$1 INSTANCE = new AppFlexInputViewModel$Companion$observeStores$1();

    public final Observable<? extends AppFlexInputViewModel.StoreState> call(final StoreChannelsSelected.ResolvedSelectedChannel resolvedSelectedChannel) {
        final Channel channelOrParent = resolvedSelectedChannel.getChannelOrParent();
        StoreStream.Companion companion = StoreStream.Companion;
        return Observable.f(companion.getNavigation().observeLeftPanelState(), companion.getNavigation().observeRightPanelState(), companion.getPermissions().observePermissionsForChannel(channelOrParent != null ? channelOrParent.h() : 0L), companion.getNotices().getNotices(), companion.getUserSettings().observeIsStickerSuggestionsEnabled(), companion.getExpressionSuggestions().observeSuggestionsEnabled(), new Func6<PanelState, PanelState, Long, StoreNotices.Notice, Boolean, Boolean, AppFlexInputViewModel.StoreState>() { // from class: com.discord.widgets.chat.input.AppFlexInputViewModel$Companion$observeStores$1.1
            public final AppFlexInputViewModel.StoreState call(PanelState panelState, PanelState panelState2, Long l, StoreNotices.Notice notice, Boolean bool, Boolean bool2) {
                boolean z2;
                m.checkNotNullExpressionValue(panelState, "leftPanelState");
                m.checkNotNullExpressionValue(panelState2, "rightPanelState");
                Channel channel = Channel.this;
                m.checkNotNullExpressionValue(bool, "stickerSuggestionsEnabled");
                if (bool.booleanValue()) {
                    m.checkNotNullExpressionValue(bool2, "expressionSuggestionsEnabled");
                    if (bool2.booleanValue()) {
                        z2 = true;
                        return new AppFlexInputViewModel.StoreState(panelState, panelState2, channel, l, notice, z2, resolvedSelectedChannel instanceof StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft);
                    }
                }
                z2 = false;
                return new AppFlexInputViewModel.StoreState(panelState, panelState2, channel, l, notice, z2, resolvedSelectedChannel instanceof StoreChannelsSelected.ResolvedSelectedChannel.ThreadDraft);
            }
        });
    }
}
