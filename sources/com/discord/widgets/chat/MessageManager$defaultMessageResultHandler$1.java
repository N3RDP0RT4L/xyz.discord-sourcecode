package com.discord.widgets.chat;

import android.content.Context;
import com.discord.utilities.error.Error;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.utilities.rest.SendUtils;
import com.discord.widgets.chat.MessageManager;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: MessageManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/MessageManager$MessageSendResult;", "messageSendResult", "", "invoke", "(Lcom/discord/widgets/chat/MessageManager$MessageSendResult;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageManager$defaultMessageResultHandler$1 extends o implements Function1<MessageManager.MessageSendResult, Unit> {
    public final /* synthetic */ MessageManager this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MessageManager$defaultMessageResultHandler$1(MessageManager messageManager) {
        super(1);
        this.this$0 = messageManager;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(MessageManager.MessageSendResult messageSendResult) {
        invoke2(messageSendResult);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(MessageManager.MessageSendResult messageSendResult) {
        Context context;
        Context context2;
        m.checkNotNullParameter(messageSendResult, "messageSendResult");
        MessageResult messageResult = messageSendResult.getMessageResult();
        if (messageResult instanceof MessageResult.CaptchaRequired) {
            SendUtils.INSTANCE.handleCaptchaRequired((MessageResult.CaptchaRequired) messageResult);
        } else if (messageResult instanceof MessageResult.UnknownFailure) {
            SendUtils sendUtils = SendUtils.INSTANCE;
            Error error = ((MessageResult.UnknownFailure) messageResult).getError();
            context2 = this.this$0.context;
            SendUtils.handleSendError$default(sendUtils, error, context2, null, null, 12, null);
        } else if (messageResult instanceof MessageResult.Slowmode) {
            context = this.this$0.context;
            b.a.d.m.g(context, R.string.channel_slowmode_desc_short, 0, null, 12);
        }
    }
}
