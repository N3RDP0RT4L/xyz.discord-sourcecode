package com.discord.widgets.chat.managereactions;

import com.discord.api.channel.Channel;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: ManageReactionsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "invoke", "()Ljava/util/Map;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ManageReactionsModelProvider$get$guildMembersObservable$1 extends o implements Function0<Map<Long, ? extends GuildMember>> {
    public final /* synthetic */ ManageReactionsModelProvider this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ManageReactionsModelProvider$get$guildMembersObservable$1(ManageReactionsModelProvider manageReactionsModelProvider) {
        super(0);
        this.this$0 = manageReactionsModelProvider;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Map<Long, ? extends GuildMember> invoke() {
        StoreChannels storeChannels;
        StoreGuilds storeGuilds;
        storeChannels = this.this$0.storeChannels;
        Channel channel = storeChannels.getChannel(this.this$0.getChannelId());
        Long valueOf = channel != null ? Long.valueOf(channel.f()) : null;
        storeGuilds = this.this$0.storeGuilds;
        Map<Long, GuildMember> map = storeGuilds.getMembers().get(valueOf);
        return map != null ? map : h0.emptyMap();
    }
}
