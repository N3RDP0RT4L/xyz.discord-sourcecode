package com.discord.widgets.chat.managereactions;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.message.reaction.MessageReactionEmoji;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreMessageReactions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.chat.managereactions.ManageReactionsEmojisAdapter;
import com.discord.widgets.chat.managereactions.ManageReactionsResultsAdapter;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: ManageReactionsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001BE\u0012\n\u0010/\u001a\u00060\u000ej\u0002`.\u0012\u0006\u0010\u001e\u001a\u00020\u000e\u0012\n\b\u0002\u00101\u001a\u0004\u0018\u00010\u0019\u0012\b\b\u0002\u0010,\u001a\u00020+\u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010)\u001a\u00020(¢\u0006\u0004\b2\u00103J\u001d\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J[\u0010\u0015\u001a\u00020\u00142\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u000b\u001a\u00020\u00052\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\r\u001a\u00020\f2\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f2\u0016\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u00120\u0011H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0015\u0010\u0017\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00140\u0004¢\u0006\u0004\b\u0017\u0010\u0018J\u0015\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u001e\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R&\u0010&\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0019\u0012\u0006\u0012\u0004\u0018\u00010\u00190%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u001d\u0010/\u001a\u00060\u000ej\u0002`.8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010\u001f\u001a\u0004\b0\u0010!¨\u00064"}, d2 = {"Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;", "", "Lcom/discord/api/message/reaction/MessageReactionEmoji;", "targetedEmoji", "Lrx/Observable;", "Lcom/discord/stores/StoreMessageReactions$EmojiResults;", "getUsersForReaction", "(Lcom/discord/api/message/reaction/MessageReactionEmoji;)Lrx/Observable;", "", "Lcom/discord/api/message/reaction/MessageReaction;", "reactions", "results", "", "canManageMessages", "", "Lcom/discord/primitives/UserId;", "myId", "", "Lcom/discord/models/member/GuildMember;", "guildMembers", "Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;", "createModel", "(Ljava/util/List;Lcom/discord/stores/StoreMessageReactions$EmojiResults;Lcom/discord/api/message/reaction/MessageReactionEmoji;ZJLjava/util/Map;)Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;", "get", "()Lrx/Observable;", "", "emojiKey", "", "onEmojiTargeted", "(Ljava/lang/String;)V", "messageId", "J", "getMessageId", "()J", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lrx/subjects/SerializedSubject;", "targetedEmojiKeySubject", "Lrx/subjects/SerializedSubject;", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/updates/ObservationDeck;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/primitives/ChannelId;", "channelId", "getChannelId", "initTargetedReactionKey", HookHelper.constructorName, "(JJLjava/lang/String;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/updates/ObservationDeck;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ManageReactionsModelProvider {
    private final long channelId;
    private final long messageId;
    private final ObservationDeck observationDeck;
    private final StoreChannels storeChannels;
    private final StoreGuilds storeGuilds;
    private final SerializedSubject<String, String> targetedEmojiKeySubject;

    public ManageReactionsModelProvider(long j, long j2, String str, StoreChannels storeChannels, StoreGuilds storeGuilds, ObservationDeck observationDeck) {
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        this.channelId = j;
        this.messageId = j2;
        this.storeChannels = storeChannels;
        this.storeGuilds = storeGuilds;
        this.observationDeck = observationDeck;
        this.targetedEmojiKeySubject = new SerializedSubject<>(BehaviorSubject.l0(str));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ManageReactionsModel createModel(List<MessageReaction> list, StoreMessageReactions.EmojiResults emojiResults, MessageReactionEmoji messageReactionEmoji, boolean z2, long j, Map<Long, GuildMember> map) {
        List list2;
        Map<Long, GuildMember> map2;
        boolean z3;
        ArrayList arrayList = new ArrayList();
        for (MessageReaction messageReaction : list) {
            arrayList.add(new ManageReactionsEmojisAdapter.ReactionEmojiItem(messageReaction, m.areEqual(messageReaction.b(), messageReactionEmoji)));
        }
        if (emojiResults instanceof StoreMessageReactions.EmojiResults.Users) {
            StoreMessageReactions.EmojiResults.Users users = (StoreMessageReactions.EmojiResults.Users) emojiResults;
            Collection<User> values = users.getUsers().values();
            m.checkNotNullExpressionValue(values, "results.users.values");
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(values, 10));
            for (User user : values) {
                m.checkNotNullExpressionValue(user, "user");
                long j2 = this.channelId;
                long j3 = this.messageId;
                MessageReactionEmoji emoji = users.getEmoji();
                if (z2 || user.getId() == j) {
                    map2 = map;
                    z3 = true;
                } else {
                    map2 = map;
                    z3 = false;
                }
                arrayList2.add(new ManageReactionsResultsAdapter.ReactionUserItem(user, j2, j3, emoji, z3, (GuildMember) a.e(user, map2)));
            }
            list2 = u.toList(arrayList2);
        } else if (emojiResults instanceof StoreMessageReactions.EmojiResults.Loading) {
            list2 = d0.t.m.listOf(new ManageReactionsResultsAdapter.LoadingItem());
        } else if (emojiResults instanceof StoreMessageReactions.EmojiResults.Failure) {
            StoreMessageReactions.EmojiResults.Failure failure = (StoreMessageReactions.EmojiResults.Failure) emojiResults;
            list2 = d0.t.m.listOf(new ManageReactionsResultsAdapter.ErrorItem(failure.getChannelId(), failure.getMessageId(), failure.getEmoji()));
        } else {
            throw new NoWhenBranchMatchedException();
        }
        return new ManageReactionsModel(arrayList, list2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Observable<StoreMessageReactions.EmojiResults> getUsersForReaction(MessageReactionEmoji messageReactionEmoji) {
        return StoreStream.Companion.getMessageReactions().observeMessageReactions(this.channelId, this.messageId, messageReactionEmoji);
    }

    public final Observable<ManageReactionsModel> get() {
        ManageReactionsModelProvider$get$1 manageReactionsModelProvider$get$1 = new ManageReactionsModelProvider$get$1(this);
        StoreStream.Companion companion = StoreStream.Companion;
        Observable F = companion.getMessages().observeMessagesForChannel(this.channelId, this.messageId).F(ManageReactionsModelProvider$get$reactionsObs$1.INSTANCE).F(ManageReactionsModelProvider$get$reactionsObs$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "StoreStream\n        .get…ion -> reaction.count } }");
        Observable Y = StoreUser.observeMe$default(companion.getUsers(), false, 1, null).Y(new ManageReactionsModelProvider$get$2(this, manageReactionsModelProvider$get$1, F, ObservationDeck.connectRx$default(this.observationDeck, new ObservationDeck.UpdateSource[]{this.storeChannels, this.storeGuilds}, false, null, null, new ManageReactionsModelProvider$get$guildMembersObservable$1(this), 14, null)));
        m.checkNotNullExpressionValue(Y, "StoreStream\n        .get…              }\n        }");
        Observable<ManageReactionsModel> q = ObservableExtensionsKt.computationLatest(Y).q();
        m.checkNotNullExpressionValue(q, "StoreStream\n        .get…  .distinctUntilChanged()");
        return q;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final long getMessageId() {
        return this.messageId;
    }

    public final void onEmojiTargeted(String str) {
        m.checkNotNullParameter(str, "emojiKey");
        this.targetedEmojiKeySubject.k.onNext(str);
    }

    public /* synthetic */ ManageReactionsModelProvider(long j, long j2, String str, StoreChannels storeChannels, StoreGuilds storeGuilds, ObservationDeck observationDeck, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, j2, (i & 4) != 0 ? null : str, (i & 8) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 16) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 32) != 0 ? ObservationDeckProvider.get() : observationDeck);
    }
}
