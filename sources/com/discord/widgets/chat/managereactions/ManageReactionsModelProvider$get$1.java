package com.discord.widgets.chat.managereactions;

import com.discord.api.permission.Permission;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreStream;
import com.discord.utilities.permissions.PermissionUtils;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: ManageReactionsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00000\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "isMeMfaEnabled", "Lrx/Observable;", "invoke", "(Z)Lrx/Observable;", "getCanManageMessagesObs"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ManageReactionsModelProvider$get$1 extends o implements Function1<Boolean, Observable<Boolean>> {
    public final /* synthetic */ ManageReactionsModelProvider this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ManageReactionsModelProvider$get$1(ManageReactionsModelProvider manageReactionsModelProvider) {
        super(1);
        this.this$0 = manageReactionsModelProvider;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Observable<Boolean> invoke(Boolean bool) {
        return invoke(bool.booleanValue());
    }

    public final Observable<Boolean> invoke(final boolean z2) {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<Boolean> j = Observable.j(companion.getPermissions().observePermissionsForChannel(this.this$0.getChannelId()), companion.getGuilds().observeFromChannelId(this.this$0.getChannelId()), new Func2<Long, Guild, Boolean>() { // from class: com.discord.widgets.chat.managereactions.ManageReactionsModelProvider$get$1.1
            public final Boolean call(Long l, Guild guild) {
                return Boolean.valueOf(guild != null && PermissionUtils.canAndIsElevated(Permission.MANAGE_MESSAGES, l, z2, guild.getMfaLevel()));
            }
        });
        m.checkNotNullExpressionValue(j, "Observable.combineLatest…ld.mfaLevel\n      )\n    }");
        return j;
    }
}
