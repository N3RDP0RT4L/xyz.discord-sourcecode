package com.discord.widgets.chat.managereactions;

import androidx.core.app.NotificationCompat;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.message.reaction.MessageReactionEmoji;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreMessageReactions;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Triple;
import rx.Observable;
import rx.functions.Func3;
import rx.subjects.SerializedSubject;
/* compiled from: ManageReactionsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004 \u0001*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/models/user/MeUser;", "kotlin.jvm.PlatformType", "me", "Lrx/Observable;", "Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/user/MeUser;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ManageReactionsModelProvider$get$2<T, R> implements b<MeUser, Observable<? extends ManageReactionsModel>> {
    public final /* synthetic */ ManageReactionsModelProvider$get$1 $getCanManageMessagesObs$1;
    public final /* synthetic */ Observable $guildMembersObservable;
    public final /* synthetic */ Observable $reactionsObs;
    public final /* synthetic */ ManageReactionsModelProvider this$0;

    /* compiled from: ManageReactionsModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000f\u001aÆ\u0001\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003\u00124\u00122\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\tj\u0002`\n \u0001*\u0018\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\tj\u0002`\n\u0018\u00010\u00060\u0006 \u0001*b\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003\u00124\u00122\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\tj\u0002`\n \u0001*\u0018\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\tj\u0002`\n\u0018\u00010\u00060\u0006\u0018\u00010\f0\f2\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u001a\u0010\u0005\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0001*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u000326\u0010\u000b\u001a2\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\tj\u0002`\n \u0001*\u0018\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\b\u0012\u00060\tj\u0002`\n\u0018\u00010\u00060\u0006H\n¢\u0006\u0004\b\r\u0010\u000e"}, d2 = {"", "kotlin.jvm.PlatformType", "canManageMessages", "", "Lcom/discord/api/message/reaction/MessageReaction;", "reactions", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "guildMembers", "Lkotlin/Triple;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;Ljava/util/List;Ljava/util/Map;)Lkotlin/Triple;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.managereactions.ManageReactionsModelProvider$get$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T1, T2, T3, R> implements Func3<Boolean, List<? extends MessageReaction>, Map<Long, ? extends GuildMember>, Triple<? extends Boolean, ? extends List<? extends MessageReaction>, ? extends Map<Long, ? extends GuildMember>>> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        @Override // rx.functions.Func3
        public /* bridge */ /* synthetic */ Triple<? extends Boolean, ? extends List<? extends MessageReaction>, ? extends Map<Long, ? extends GuildMember>> call(Boolean bool, List<? extends MessageReaction> list, Map<Long, ? extends GuildMember> map) {
            return call2(bool, (List<MessageReaction>) list, (Map<Long, GuildMember>) map);
        }

        /* renamed from: call  reason: avoid collision after fix types in other method */
        public final Triple<Boolean, List<MessageReaction>, Map<Long, GuildMember>> call2(Boolean bool, List<MessageReaction> list, Map<Long, GuildMember> map) {
            return new Triple<>(bool, list, map);
        }
    }

    /* compiled from: ManageReactionsModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000f\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\f \u0002*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\f\u0018\u00010\u000b0\u000b2Ë\u0001\u0010\n\u001aÆ\u0001\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0004 \u0002*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003\u00124\u00122\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\b\u0012\u00060\bj\u0002`\t \u0002*\u0018\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\b\u0012\u00060\bj\u0002`\t\u0018\u00010\u00050\u0005 \u0002*b\u0012\f\u0012\n \u0002*\u0004\u0018\u00010\u00010\u0001\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0004 \u0002*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003\u00124\u00122\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\b\u0012\u00060\bj\u0002`\t \u0002*\u0018\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\b\u0012\u00060\bj\u0002`\t\u0018\u00010\u00050\u0005\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\r\u0010\u000e"}, d2 = {"Lkotlin/Triple;", "", "kotlin.jvm.PlatformType", "", "Lcom/discord/api/message/reaction/MessageReaction;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "<name for destructuring parameter 0>", "Lrx/Observable;", "Lcom/discord/widgets/chat/managereactions/ManageReactionsModel;", NotificationCompat.CATEGORY_CALL, "(Lkotlin/Triple;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.managereactions.ManageReactionsModelProvider$get$2$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2<T, R> implements b<Triple<? extends Boolean, ? extends List<? extends MessageReaction>, ? extends Map<Long, ? extends GuildMember>>, Observable<? extends ManageReactionsModel>> {
        public final /* synthetic */ MeUser $me;

        public AnonymousClass2(MeUser meUser) {
            this.$me = meUser;
        }

        @Override // j0.k.b
        public /* bridge */ /* synthetic */ Observable<? extends ManageReactionsModel> call(Triple<? extends Boolean, ? extends List<? extends MessageReaction>, ? extends Map<Long, ? extends GuildMember>> triple) {
            return call2((Triple<Boolean, ? extends List<MessageReaction>, ? extends Map<Long, GuildMember>>) triple);
        }

        /* renamed from: call  reason: avoid collision after fix types in other method */
        public final Observable<? extends ManageReactionsModel> call2(Triple<Boolean, ? extends List<MessageReaction>, ? extends Map<Long, GuildMember>> triple) {
            SerializedSubject serializedSubject;
            final Boolean component1 = triple.component1();
            final List<MessageReaction> component2 = triple.component2();
            final Map<Long, GuildMember> component3 = triple.component3();
            if (component2.isEmpty()) {
                return new k(null);
            }
            serializedSubject = ManageReactionsModelProvider$get$2.this.this$0.targetedEmojiKeySubject;
            return (Observable<R>) serializedSubject.q().F(new b<String, MessageReactionEmoji>() { // from class: com.discord.widgets.chat.managereactions.ManageReactionsModelProvider.get.2.2.1
                public final MessageReactionEmoji call(String str) {
                    T t;
                    MessageReactionEmoji b2;
                    List list = component2;
                    m.checkNotNullExpressionValue(list, "reactions");
                    Iterator<T> it = list.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        if (m.areEqual(((MessageReaction) t).b().c(), str)) {
                            break;
                        }
                    }
                    MessageReaction messageReaction = (MessageReaction) t;
                    if (messageReaction != null && (b2 = messageReaction.b()) != null) {
                        return b2;
                    }
                    List list2 = component2;
                    m.checkNotNullExpressionValue(list2, "reactions");
                    return ((MessageReaction) u.first((List<? extends Object>) list2)).b();
                }
            }).Y(new b<MessageReactionEmoji, Observable<? extends ManageReactionsModel>>() { // from class: com.discord.widgets.chat.managereactions.ManageReactionsModelProvider.get.2.2.2
                public final Observable<? extends ManageReactionsModel> call(final MessageReactionEmoji messageReactionEmoji) {
                    Observable usersForReaction;
                    ManageReactionsModelProvider manageReactionsModelProvider = ManageReactionsModelProvider$get$2.this.this$0;
                    m.checkNotNullExpressionValue(messageReactionEmoji, "targetedEmoji");
                    usersForReaction = manageReactionsModelProvider.getUsersForReaction(messageReactionEmoji);
                    return usersForReaction.F(new b<StoreMessageReactions.EmojiResults, ManageReactionsModel>() { // from class: com.discord.widgets.chat.managereactions.ManageReactionsModelProvider.get.2.2.2.1
                        public final ManageReactionsModel call(StoreMessageReactions.EmojiResults emojiResults) {
                            ManageReactionsModel createModel;
                            C02462 r0 = C02462.this;
                            ManageReactionsModelProvider manageReactionsModelProvider2 = ManageReactionsModelProvider$get$2.this.this$0;
                            List list = component2;
                            m.checkNotNullExpressionValue(list, "reactions");
                            m.checkNotNullExpressionValue(emojiResults, "users");
                            MessageReactionEmoji messageReactionEmoji2 = messageReactionEmoji;
                            Boolean bool = component1;
                            m.checkNotNullExpressionValue(bool, "canManageMessages");
                            boolean booleanValue = bool.booleanValue();
                            long id2 = AnonymousClass2.this.$me.getId();
                            Map map = component3;
                            m.checkNotNullExpressionValue(map, "guildMembers");
                            createModel = manageReactionsModelProvider2.createModel(list, emojiResults, messageReactionEmoji2, booleanValue, id2, map);
                            return createModel;
                        }
                    });
                }
            });
        }
    }

    public ManageReactionsModelProvider$get$2(ManageReactionsModelProvider manageReactionsModelProvider, ManageReactionsModelProvider$get$1 manageReactionsModelProvider$get$1, Observable observable, Observable observable2) {
        this.this$0 = manageReactionsModelProvider;
        this.$getCanManageMessagesObs$1 = manageReactionsModelProvider$get$1;
        this.$reactionsObs = observable;
        this.$guildMembersObservable = observable2;
    }

    public final Observable<? extends ManageReactionsModel> call(MeUser meUser) {
        return Observable.i(this.$getCanManageMessagesObs$1.invoke(meUser.getMfaEnabled()), this.$reactionsObs, this.$guildMembersObservable, AnonymousClass1.INSTANCE).Y(new AnonymousClass2(meUser));
    }
}
