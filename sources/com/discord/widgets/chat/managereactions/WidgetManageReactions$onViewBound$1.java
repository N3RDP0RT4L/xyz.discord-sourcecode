package com.discord.widgets.chat.managereactions;

import d0.z.d.q;
import kotlin.Metadata;
/* compiled from: WidgetManageReactions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetManageReactions$onViewBound$1 extends q {
    public WidgetManageReactions$onViewBound$1(WidgetManageReactions widgetManageReactions) {
        super(widgetManageReactions, WidgetManageReactions.class, "modelProvider", "getModelProvider()Lcom/discord/widgets/chat/managereactions/ManageReactionsModelProvider;", 0);
    }

    @Override // d0.z.d.q, kotlin.reflect.KProperty0
    public Object get() {
        return WidgetManageReactions.access$getModelProvider$p((WidgetManageReactions) this.receiver);
    }

    @Override // d0.z.d.q
    public void set(Object obj) {
        ((WidgetManageReactions) this.receiver).modelProvider = (ManageReactionsModelProvider) obj;
    }
}
