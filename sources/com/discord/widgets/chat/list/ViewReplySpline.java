package com.discord.widgets.chat.list;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: ViewReplySpline.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 42\u00020\u0001:\u00014B\u0019\u0012\u0006\u0010/\u001a\u00020.\u0012\b\u00101\u001a\u0004\u0018\u000100¢\u0006\u0004\b2\u00103J/\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0002H\u0014¢\u0006\u0004\b\b\u0010\tJ\r\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bJ\r\u0010\f\u001a\u00020\u0002¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u0010\u001a\u00020\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eH\u0014¢\u0006\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0013R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00158\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\u001b\u0010\u0017R\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010\u001f\u001a\u00020\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010\u001eR\u0016\u0010 \u001a\u00020\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010\u001eR\u0016\u0010!\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\u0013R\u0016\u0010\"\u001a\u00020\u00158\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b\"\u0010\u0017R\u0016\u0010#\u001a\u00020\u00158\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b#\u0010\u0017R\u0016\u0010$\u001a\u00020\u00158\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b$\u0010\u0017R\u0016\u0010%\u001a\u00020\u00158\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b%\u0010\u0017R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010)\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010\u0013R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,R\u0016\u0010-\u001a\u00020\u00158\u0002@\u0002X\u0082D¢\u0006\u0006\n\u0004\b-\u0010\u0017¨\u00065"}, d2 = {"Lcom/discord/widgets/chat/list/ViewReplySpline;", "Landroid/view/View;", "", "w", "h", "oldw", "oldh", "", "onSizeChanged", "(IIII)V", "createPath", "()V", "getLongPathOffset", "()I", "Landroid/graphics/Canvas;", "canvas", "onDraw", "(Landroid/graphics/Canvas;)V", "spineEndPadding", "I", "capStyle", "", "halfStrokeWidth", "F", "Landroid/graphics/Path;", "path", "Landroid/graphics/Path;", "strokeWidth", "Landroid/graphics/RectF;", "pathRect", "Landroid/graphics/RectF;", "pathRectLocal", "arcRectLocal", "orientation", "startX", "endX", "startY", "endY", "Landroid/graphics/Paint;", "paint", "Landroid/graphics/Paint;", "spineStartPadding", "Landroid/graphics/Matrix;", "transformMatrix", "Landroid/graphics/Matrix;", "arcPercent", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewReplySpline extends View {
    private static final int BOTTOM_TO_END = 0;
    private static final int CAP_LONG = 1;
    private static final int CAP_LONG_AND_SHORT = 0;
    public static final Companion Companion = new Companion(null);
    private static final int TOP_TO_END = 1;
    private int capStyle;
    private int orientation;
    private final Paint paint;
    private int spineEndPadding;
    private int spineStartPadding;
    private final float strokeWidth;
    private final Path path = new Path();
    private final float halfStrokeWidth = 3.0f;
    private final float arcPercent = 0.25f;
    private final float startY = 1.0f;
    private final float endX = 1.0f;
    private final float startX;
    private final float endY;
    private final RectF pathRect = new RectF(this.startX, this.endY, 1.0f, 1.0f);
    private RectF pathRectLocal = new RectF();
    private RectF arcRectLocal = new RectF();
    private Matrix transformMatrix = new Matrix();

    /* compiled from: ViewReplySpline.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/list/ViewReplySpline$Companion;", "", "", "BOTTOM_TO_END", "I", "CAP_LONG", "CAP_LONG_AND_SHORT", "TOP_TO_END", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewReplySpline(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        Paint paint = new Paint();
        this.paint = paint;
        float f = 3.0f * 2;
        this.strokeWidth = f;
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(f);
        paint.setColor(ColorCompat.getThemedColor(this, (int) R.attr.colorBackgroundAccent));
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.discord.R.a.ViewReplySpline, 0, 0);
            m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…* defStyleRes */0\n      )");
            float dimension = obtainStyledAttributes.getDimension(3, 0.0f);
            float dimension2 = obtainStyledAttributes.getDimension(1, 0.0f);
            this.orientation = obtainStyledAttributes.getInteger(2, 0);
            this.capStyle = obtainStyledAttributes.getInteger(0, 0);
            float f2 = 0;
            if (dimension > f2) {
                this.spineStartPadding = DimenUtils.dpToPixels(dimension);
            }
            if (dimension2 > f2) {
                this.spineEndPadding = DimenUtils.dpToPixels(dimension2);
            }
            obtainStyledAttributes.recycle();
        }
    }

    public final void createPath() {
        this.path.reset();
        int i = this.orientation;
        if (i == 0) {
            Path path = this.path;
            RectF rectF = this.pathRectLocal;
            path.moveTo(rectF.left, rectF.bottom + getLongPathOffset());
            this.path.arcTo(this.arcRectLocal, 180.0f, 90.0f, false);
            Path path2 = this.path;
            RectF rectF2 = this.pathRectLocal;
            path2.lineTo(rectF2.right, rectF2.top);
        } else if (i == 1) {
            Path path3 = this.path;
            RectF rectF3 = this.pathRectLocal;
            path3.moveTo(rectF3.left, rectF3.top - getLongPathOffset());
            this.path.arcTo(this.arcRectLocal, 180.0f, -90.0f, false);
            Path path4 = this.path;
            RectF rectF4 = this.pathRectLocal;
            path4.lineTo(rectF4.right, rectF4.bottom);
        }
    }

    public final int getLongPathOffset() {
        int i = this.capStyle;
        if (i == 0) {
            return 0;
        }
        if (i == 1) {
            return DimenUtils.dpToPixels(1);
        }
        throw new IllegalStateException("Unknown cap style");
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (canvas != null) {
            canvas.drawPath(this.path, this.paint);
        }
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        RectF rectF;
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3 || i2 != i4) {
            this.transformMatrix.reset();
            Matrix matrix = this.transformMatrix;
            float f = this.halfStrokeWidth;
            matrix.preTranslate(f, f);
            Matrix matrix2 = this.transformMatrix;
            float f2 = this.strokeWidth;
            matrix2.preScale((i - f2) - this.spineEndPadding, (i2 - f2) - this.spineStartPadding);
            this.transformMatrix.mapRect(this.pathRectLocal, this.pathRect);
            float f3 = this.arcPercent;
            RectF rectF2 = this.pathRectLocal;
            float f4 = (rectF2.right - rectF2.left) * f3 * 2;
            int i5 = this.orientation;
            if (i5 == 0) {
                RectF rectF3 = this.pathRectLocal;
                float f5 = rectF3.left;
                float f6 = rectF3.top;
                rectF = new RectF(f5, f6, f5 + f4, f4 + f6);
            } else if (i5 == 1) {
                RectF rectF4 = this.pathRectLocal;
                float f7 = rectF4.left;
                float f8 = rectF4.bottom;
                rectF = new RectF(f7, f8 - f4, f4 + f7, f8);
            } else {
                throw new IllegalStateException("Unknown orientation");
            }
            this.arcRectLocal = rectF;
            createPath();
        }
    }
}
