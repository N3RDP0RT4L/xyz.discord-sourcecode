package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.discord.databinding.WidgetChatListAdapterItemTextDividerBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.TimestampEntry;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemTimestamp.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemTimestamp;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/databinding/WidgetChatListAdapterItemTextDividerBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemTextDividerBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemTimestamp extends WidgetChatListItem {
    private final WidgetChatListAdapterItemTextDividerBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemTimestamp(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_text_divider, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        WidgetChatListAdapterItemTextDividerBinding a = WidgetChatListAdapterItemTextDividerBinding.a(this.itemView);
        m.checkNotNullExpressionValue(a, "WidgetChatListAdapterIte…derBinding.bind(itemView)");
        this.binding = a;
        View view = a.f2320b;
        m.checkNotNullExpressionValue(view, "binding.dividerStrokeLeft");
        view.setBackgroundColor(ColorCompat.getThemedColor(view, (int) R.attr.colorTextMuted));
        View view2 = a.c;
        m.checkNotNullExpressionValue(view2, "binding.dividerStrokeRight");
        view2.setBackgroundColor(ColorCompat.getThemedColor(view2, (int) R.attr.colorTextMuted));
        TextView textView = a.d;
        m.checkNotNullExpressionValue(textView, "binding.dividerText");
        textView.setTextColor(ColorCompat.getThemedColor(textView, (int) R.attr.colorTextMuted));
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.dividerText");
        TimeUtils timeUtils = TimeUtils.INSTANCE;
        long timestamp = ((TimestampEntry) chatListEntry).getTimestamp();
        TextView textView2 = this.binding.d;
        m.checkNotNullExpressionValue(textView2, "binding.dividerText");
        Context context = textView2.getContext();
        m.checkNotNullExpressionValue(context, "binding.dividerText.context");
        textView.setText(TimeUtils.renderUtcDate$default(timeUtils, timestamp, context, 0, 4, null));
    }
}
