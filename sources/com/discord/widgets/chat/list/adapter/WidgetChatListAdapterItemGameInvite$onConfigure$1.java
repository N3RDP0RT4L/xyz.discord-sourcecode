package com.discord.widgets.chat.list.adapter;

import android.view.View;
import com.discord.api.activity.Activity;
import com.discord.widgets.chat.list.ViewEmbedGameInvite;
import com.discord.widgets.chat.list.entries.GameInviteEntry;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetChatListAdapterItemGameInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Landroid/view/View;", "<anonymous parameter 0>", "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "model", "", "invoke", "(Landroid/view/View;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemGameInvite$onConfigure$1 extends o implements Function2<View, ViewEmbedGameInvite.Model, Unit> {
    public final /* synthetic */ GameInviteEntry $item;
    public final /* synthetic */ WidgetChatListAdapterItemGameInvite this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemGameInvite$onConfigure$1(WidgetChatListAdapterItemGameInvite widgetChatListAdapterItemGameInvite, GameInviteEntry gameInviteEntry) {
        super(2);
        this.this$0 = widgetChatListAdapterItemGameInvite;
        this.$item = gameInviteEntry;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(View view, ViewEmbedGameInvite.Model model) {
        invoke2(view, model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view, ViewEmbedGameInvite.Model model) {
        m.checkNotNullParameter(view, "<anonymous parameter 0>");
        m.checkNotNullParameter(model, "model");
        Activity activity = model.getActivity();
        if (activity != null) {
            WidgetChatListAdapterItemGameInvite.access$getAdapter$p(this.this$0).onUserActivityAction(this.$item.getAuthorId(), this.$item.getMessageId(), this.$item.getActivity().b(), activity, this.$item.getApplication());
        }
    }
}
