package com.discord.widgets.chat.list.adapter;

import com.discord.simpleast.core.node.Node;
import com.discord.stores.StoreMessageState;
import com.discord.stores.StoreStream;
import com.discord.utilities.textprocessing.AstRenderer;
import com.discord.utilities.textprocessing.MessagePreprocessor;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Collection;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetChatListAdapterItemEmbed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u0006*\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u00010\u0000j\b\u0012\u0004\u0012\u00020\u0002`\u00032\u0006\u0010\u0005\u001a\u00020\u0004H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/simpleast/core/utils/Ast;", "", "prefixKey", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "invoke", "(Ljava/util/Collection;Ljava/lang/String;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "toDraweeSpanStringBuilder"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemEmbed$configureEmbedFields$1 extends o implements Function2<Collection<? extends Node<MessageRenderContext>>, String, DraweeSpanStringBuilder> {
    public final /* synthetic */ int $embedIndex;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ WidgetChatListAdapterItemEmbed.Model $model;
    public final /* synthetic */ MessageRenderContext $renderContext;
    public final /* synthetic */ Map $visibleSpoilerEmbedMap;

    /* compiled from: WidgetChatListAdapterItemEmbed.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\n\u0010\u0001\u001a\u0006\u0012\u0002\b\u00030\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "node", "", "invoke", "(Lcom/discord/utilities/textprocessing/node/SpoilerNode;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$configureEmbedFields$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<SpoilerNode<?>, Unit> {
        public final /* synthetic */ String $prefixKey;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(String str) {
            super(1);
            this.$prefixKey = str;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(SpoilerNode<?> spoilerNode) {
            invoke2(spoilerNode);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(SpoilerNode<?> spoilerNode) {
            m.checkNotNullParameter(spoilerNode, "node");
            StoreMessageState messageState = StoreStream.Companion.getMessageState();
            WidgetChatListAdapterItemEmbed$configureEmbedFields$1 widgetChatListAdapterItemEmbed$configureEmbedFields$1 = WidgetChatListAdapterItemEmbed$configureEmbedFields$1.this;
            long j = widgetChatListAdapterItemEmbed$configureEmbedFields$1.$messageId;
            int i = widgetChatListAdapterItemEmbed$configureEmbedFields$1.$embedIndex;
            messageState.revealSpoilerEmbedData(j, i, this.$prefixKey + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + spoilerNode.getId());
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemEmbed$configureEmbedFields$1(WidgetChatListAdapterItemEmbed.Model model, Map map, int i, MessageRenderContext messageRenderContext, long j) {
        super(2);
        this.$model = model;
        this.$visibleSpoilerEmbedMap = map;
        this.$embedIndex = i;
        this.$renderContext = messageRenderContext;
        this.$messageId = j;
    }

    public final DraweeSpanStringBuilder invoke(Collection<? extends Node<MessageRenderContext>> collection, String str) {
        MessageRenderContext copy;
        m.checkNotNullParameter(collection, "$this$toDraweeSpanStringBuilder");
        m.checkNotNullParameter(str, "prefixKey");
        long myId = this.$model.getMyId();
        Map map = this.$visibleSpoilerEmbedMap;
        new MessagePreprocessor(myId, map != null ? WidgetChatListAdapterItemEmbed.Companion.getEmbedFieldVisibleIndices(map, this.$embedIndex, str) : null, null, false, null, 28, null).process(collection);
        copy = r13.copy((r31 & 1) != 0 ? r13.getContext() : null, (r31 & 2) != 0 ? r13.getMyId() : 0L, (r31 & 4) != 0 ? r13.isAnimationEnabled() : false, (r31 & 8) != 0 ? r13.getUserNames() : null, (r31 & 16) != 0 ? r13.getChannelNames() : null, (r31 & 32) != 0 ? r13.getRoles() : null, (r31 & 64) != 0 ? r13.getLinkColorAttrResId() : 0, (r31 & 128) != 0 ? r13.getOnClickUrl() : null, (r31 & 256) != 0 ? r13.getOnLongPressUrl() : null, (r31 & 512) != 0 ? r13.getSpoilerColorRes() : 0, (r31 & 1024) != 0 ? r13.getSpoilerRevealedColorRes() : 0, (r31 & 2048) != 0 ? r13.getSpoilerOnClick() : new AnonymousClass1(str), (r31 & 4096) != 0 ? r13.getUserMentionOnClick() : null, (r31 & 8192) != 0 ? this.$renderContext.getChannelMentionOnClick() : null);
        return AstRenderer.render(collection, copy);
    }
}
