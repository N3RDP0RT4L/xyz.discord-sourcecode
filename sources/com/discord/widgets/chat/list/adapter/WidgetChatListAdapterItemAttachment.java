package com.discord.widgets.chat.list.adapter;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.net.Uri;
import android.text.format.Formatter;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.api.message.attachment.MessageAttachment;
import com.discord.api.message.attachment.MessageAttachmentType;
import com.discord.api.role.GuildRole;
import com.discord.databinding.WidgetChatListAdapterItemAttachmentBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageState;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.embed.EmbedResourceUtils;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.chat.list.FragmentLifecycleListener;
import com.discord.widgets.chat.list.InlineMediaView;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.discord.widgets.chat.list.entries.AttachmentEntry;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.media.WidgetMedia;
import com.google.android.material.card.MaterialCardView;
import d0.z.d.m;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemAttachment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 '2\u00020\u00012\u00020\u0002:\u0002'(B\u000f\u0012\u0006\u0010$\u001a\u00020#¢\u0006\u0004\b%\u0010&J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001f\u0010\f\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0011\u0010\u000f\u001a\u0004\u0018\u00010\u000eH\u0014¢\u0006\u0004\b\u000f\u0010\u0010J\u001f\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\t\u001a\u00020\u0013H\u0014¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0018\u0010\u0017R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0018\u0010\u001f\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010!\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"¨\u0006)"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemAttachment;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/widgets/chat/list/FragmentLifecycleListener;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemAttachment$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemAttachment$Model;)V", "Lcom/discord/api/message/attachment/MessageAttachment;", "data", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "renderContext", "configureFileData", "(Lcom/discord/api/message/attachment/MessageAttachment;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "onResume", "()V", "onPause", "Lcom/discord/databinding/WidgetChatListAdapterItemAttachmentBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemAttachmentBinding;", "Lcom/discord/stores/StoreUserSettings;", "userSettings", "Lcom/discord/stores/StoreUserSettings;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "maxAttachmentImageWidth", "I", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemAttachment extends WidgetChatListItem implements FragmentLifecycleListener {
    public static final Companion Companion = new Companion(null);
    private final WidgetChatListAdapterItemAttachmentBinding binding;
    private final int maxAttachmentImageWidth;
    private Subscription subscription;
    private final StoreUserSettings userSettings;

    /* compiled from: WidgetChatListAdapterItemAttachment.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001a\u0010\n\u001a\u00020\t*\u00020\u00048B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemAttachment$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/api/message/attachment/MessageAttachment;", "attachment", "", "navigateToAttachment", "(Landroid/content/Context;Lcom/discord/api/message/attachment/MessageAttachment;)V", "", "isInlinedAttachment", "(Lcom/discord/api/message/attachment/MessageAttachment;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                MessageAttachmentType.values();
                int[] iArr = new int[3];
                $EnumSwitchMapping$0 = iArr;
                iArr[MessageAttachmentType.VIDEO.ordinal()] = 1;
                iArr[MessageAttachmentType.IMAGE.ordinal()] = 2;
            }
        }

        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final boolean isInlinedAttachment(MessageAttachment messageAttachment) {
            return messageAttachment.e() != MessageAttachmentType.FILE;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void navigateToAttachment(Context context, MessageAttachment messageAttachment) {
            int ordinal = messageAttachment.e().ordinal();
            if (ordinal == 0 || ordinal == 1) {
                WidgetMedia.Companion.launch(context, messageAttachment);
            } else {
                UriHandler.handleOrUntrusted$default(context, messageAttachment.f(), null, 4, null);
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemAttachment(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_attachment, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        EmbedResourceUtils embedResourceUtils = EmbedResourceUtils.INSTANCE;
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "itemView.context");
        this.maxAttachmentImageWidth = embedResourceUtils.computeMaximumImageWidthPx(context);
        View view2 = this.itemView;
        int i = R.id.chat_list_adapter_item_gutter_bg;
        View findViewById = view2.findViewById(R.id.chat_list_adapter_item_gutter_bg);
        if (findViewById != null) {
            i = R.id.chat_list_adapter_item_highlighted_bg;
            View findViewById2 = view2.findViewById(R.id.chat_list_adapter_item_highlighted_bg);
            if (findViewById2 != null) {
                i = R.id.chat_list_item_attachment_barrier;
                Barrier barrier = (Barrier) view2.findViewById(R.id.chat_list_item_attachment_barrier);
                if (barrier != null) {
                    i = R.id.chat_list_item_attachment_card;
                    MaterialCardView materialCardView = (MaterialCardView) view2.findViewById(R.id.chat_list_item_attachment_card);
                    if (materialCardView != null) {
                        i = R.id.chat_list_item_attachment_description;
                        TextView textView = (TextView) view2.findViewById(R.id.chat_list_item_attachment_description);
                        if (textView != null) {
                            i = R.id.chat_list_item_attachment_download;
                            ImageView imageView = (ImageView) view2.findViewById(R.id.chat_list_item_attachment_download);
                            if (imageView != null) {
                                i = R.id.chat_list_item_attachment_icon;
                                ImageView imageView2 = (ImageView) view2.findViewById(R.id.chat_list_item_attachment_icon);
                                if (imageView2 != null) {
                                    i = R.id.chat_list_item_attachment_inline_media;
                                    InlineMediaView inlineMediaView = (InlineMediaView) view2.findViewById(R.id.chat_list_item_attachment_inline_media);
                                    if (inlineMediaView != null) {
                                        i = R.id.chat_list_item_attachment_name;
                                        TextView textView2 = (TextView) view2.findViewById(R.id.chat_list_item_attachment_name);
                                        if (textView2 != null) {
                                            i = R.id.chat_list_item_attachment_spoiler;
                                            FrameLayout frameLayout = (FrameLayout) view2.findViewById(R.id.chat_list_item_attachment_spoiler);
                                            if (frameLayout != null) {
                                                i = R.id.uikit_chat_guideline;
                                                Guideline guideline = (Guideline) view2.findViewById(R.id.uikit_chat_guideline);
                                                if (guideline != null) {
                                                    WidgetChatListAdapterItemAttachmentBinding widgetChatListAdapterItemAttachmentBinding = new WidgetChatListAdapterItemAttachmentBinding((ConstraintLayout) view2, findViewById, findViewById2, barrier, materialCardView, textView, imageView, imageView2, inlineMediaView, textView2, frameLayout, guideline);
                                                    m.checkNotNullExpressionValue(widgetChatListAdapterItemAttachmentBinding, "WidgetChatListAdapterIte…entBinding.bind(itemView)");
                                                    this.binding = widgetChatListAdapterItemAttachmentBinding;
                                                    this.userSettings = StoreStream.Companion.getUserSettings();
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view2.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemAttachment widgetChatListAdapterItemAttachment) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemAttachment.adapter;
    }

    private final void configureFileData(final MessageAttachment messageAttachment, MessageRenderContext messageRenderContext) {
        TextView textView = this.binding.i;
        m.checkNotNullExpressionValue(textView, "binding.chatListItemAttachmentName");
        textView.setText(messageAttachment.a());
        TextView textView2 = this.binding.i;
        m.checkNotNullExpressionValue(textView2, "binding.chatListItemAttachmentName");
        ViewExtensions.setOnLongClickListenerConsumeClick(textView2, WidgetChatListAdapterItemAttachment$configureFileData$1.INSTANCE);
        TextView textView3 = this.binding.e;
        m.checkNotNullExpressionValue(textView3, "binding.chatListItemAttachmentDescription");
        textView3.setText(Formatter.formatFileSize(messageRenderContext.getContext(), messageAttachment.d()));
        this.binding.g.setImageResource(EmbedResourceUtils.INSTANCE.getFileDrawable(messageAttachment.a()));
        ImageView imageView = this.binding.f;
        m.checkNotNullExpressionValue(imageView, "binding.chatListItemAttachmentDownload");
        imageView.setEnabled(true);
        ImageView imageView2 = this.binding.f;
        m.checkNotNullExpressionValue(imageView2, "binding.chatListItemAttachmentDownload");
        imageView2.setAlpha(1.0f);
        this.binding.f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemAttachment$configureFileData$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChatListAdapterItemAttachmentBinding widgetChatListAdapterItemAttachmentBinding;
                WidgetChatListAdapterItemAttachmentBinding widgetChatListAdapterItemAttachmentBinding2;
                WidgetChatListAdapter.EventHandler eventHandler = WidgetChatListAdapterItemAttachment.access$getAdapter$p(WidgetChatListAdapterItemAttachment.this).getEventHandler();
                Uri parse = Uri.parse(messageAttachment.f());
                m.checkNotNullExpressionValue(parse, "Uri.parse(data.url)");
                boolean onQuickDownloadClicked = eventHandler.onQuickDownloadClicked(parse, messageAttachment.a());
                widgetChatListAdapterItemAttachmentBinding = WidgetChatListAdapterItemAttachment.this.binding;
                ImageView imageView3 = widgetChatListAdapterItemAttachmentBinding.f;
                m.checkNotNullExpressionValue(imageView3, "binding.chatListItemAttachmentDownload");
                imageView3.setEnabled(!onQuickDownloadClicked);
                widgetChatListAdapterItemAttachmentBinding2 = WidgetChatListAdapterItemAttachment.this.binding;
                ImageView imageView4 = widgetChatListAdapterItemAttachmentBinding2.f;
                m.checkNotNullExpressionValue(imageView4, "binding.chatListItemAttachmentDownload");
                imageView4.setAlpha(0.3f);
            }
        });
    }

    /* JADX WARN: Code restructure failed: missing block: B:21:0x00b5, code lost:
        if (r5.intValue() > 0) goto L23;
     */
    /* JADX WARN: Removed duplicated region for block: B:20:0x00aa  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureUI(final com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemAttachment.Model r22) {
        /*
            Method dump skipped, instructions count: 371
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemAttachment.configureUI(com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemAttachment$Model):void");
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.subscription;
    }

    @Override // com.discord.widgets.chat.list.FragmentLifecycleListener
    public void onPause() {
        this.binding.h.onPause();
    }

    @Override // com.discord.widgets.chat.list.FragmentLifecycleListener
    public void onResume() {
        this.binding.h.onResume();
    }

    /* compiled from: WidgetChatListAdapterItemAttachment.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0082\b\u0018\u00002\u00020\u0001Bm\u0012\u0006\u0010\u0019\u001a\u00020\t\u0012\u001a\b\u0002\u0010\u001a\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\f\u0012\u001a\b\u0002\u0010\u001b\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u0012\u0012\u0004\u0012\u00020\u000f\u0018\u00010\f\u0012\u001a\b\u0002\u0010\u001c\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u0014\u0012\u0004\u0012\u00020\u0015\u0018\u00010\f\u0012\b\b\u0002\u0010\u001d\u001a\u00020\r¢\u0006\u0004\b6\u00107J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\"\u0010\u0010\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\"\u0010\u0013\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u0012\u0012\u0004\u0012\u00020\u000f\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0011J\"\u0010\u0016\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u0014\u0012\u0004\u0012\u00020\u0015\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0011J\u0010\u0010\u0017\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018Jx\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\t2\u001a\b\u0002\u0010\u001a\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\f2\u001a\b\u0002\u0010\u001b\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u0012\u0012\u0004\u0012\u00020\u000f\u0018\u00010\f2\u001a\b\u0002\u0010\u001c\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u0014\u0012\u0004\u0012\u00020\u0015\u0018\u00010\f2\b\b\u0002\u0010\u001d\u001a\u00020\rHÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u001a\u0010'\u001a\u00020&2\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b'\u0010(R+\u0010\u001a\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010)\u001a\u0004\b*\u0010\u0011R\u0019\u0010\u0019\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010+\u001a\u0004\b,\u0010\u000bR\u0019\u0010\u001d\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010-\u001a\u0004\b.\u0010\u0018R+\u0010\u001b\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u0012\u0012\u0004\u0012\u00020\u000f\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010)\u001a\u0004\b/\u0010\u0011R+\u0010\u001c\u001a\u0014\u0012\b\u0012\u00060\rj\u0002`\u0014\u0012\u0004\u0012\u00020\u0015\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010)\u001a\u0004\b0\u0010\u0011R\u0019\u00101\u001a\u00020&8\u0006@\u0006¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b1\u00103R\u001a\u00104\u001a\u00020&*\u00020\t8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b4\u00105¨\u00068"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemAttachment$Model;", "", "Landroid/content/Context;", "androidContext", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "eventHandler", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "createRenderContext", "(Landroid/content/Context;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;)Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/chat/list/entries/AttachmentEntry;", "component1", "()Lcom/discord/widgets/chat/list/entries/AttachmentEntry;", "", "", "Lcom/discord/primitives/ChannelId;", "", "component2", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "component3", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component4", "component5", "()J", "attachmentEntry", "channelNames", "userNames", "roles", "myId", "copy", "(Lcom/discord/widgets/chat/list/entries/AttachmentEntry;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;J)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemAttachment$Model;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getChannelNames", "Lcom/discord/widgets/chat/list/entries/AttachmentEntry;", "getAttachmentEntry", "J", "getMyId", "getUserNames", "getRoles", "isSpoilerHidden", "Z", "()Z", "isSpoilerEmbedRevealed", "(Lcom/discord/widgets/chat/list/entries/AttachmentEntry;)Z", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/entries/AttachmentEntry;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        private final AttachmentEntry attachmentEntry;
        private final Map<Long, String> channelNames;
        private final boolean isSpoilerHidden;
        private final long myId;
        private final Map<Long, GuildRole> roles;
        private final Map<Long, String> userNames;

        public Model(AttachmentEntry attachmentEntry, Map<Long, String> map, Map<Long, String> map2, Map<Long, GuildRole> map3, long j) {
            m.checkNotNullParameter(attachmentEntry, "attachmentEntry");
            this.attachmentEntry = attachmentEntry;
            this.channelNames = map;
            this.userNames = map2;
            this.roles = map3;
            this.myId = j;
            this.isSpoilerHidden = attachmentEntry.getAttachment().h() && !isSpoilerEmbedRevealed(attachmentEntry);
        }

        public static /* synthetic */ Model copy$default(Model model, AttachmentEntry attachmentEntry, Map map, Map map2, Map map3, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                attachmentEntry = model.attachmentEntry;
            }
            Map<Long, String> map4 = map;
            if ((i & 2) != 0) {
                map4 = model.channelNames;
            }
            Map map5 = map4;
            Map<Long, String> map6 = map2;
            if ((i & 4) != 0) {
                map6 = model.userNames;
            }
            Map map7 = map6;
            Map<Long, GuildRole> map8 = map3;
            if ((i & 8) != 0) {
                map8 = model.roles;
            }
            Map map9 = map8;
            if ((i & 16) != 0) {
                j = model.myId;
            }
            return model.copy(attachmentEntry, map5, map7, map9, j);
        }

        private final boolean isSpoilerEmbedRevealed(AttachmentEntry attachmentEntry) {
            Map<Integer, Set<String>> visibleSpoilerEmbedMap;
            StoreMessageState.State messageState = attachmentEntry.getMessageState();
            return (messageState == null || (visibleSpoilerEmbedMap = messageState.getVisibleSpoilerEmbedMap()) == null || !visibleSpoilerEmbedMap.containsKey(Integer.valueOf(attachmentEntry.getEmbedIndex()))) ? false : true;
        }

        public final AttachmentEntry component1() {
            return this.attachmentEntry;
        }

        public final Map<Long, String> component2() {
            return this.channelNames;
        }

        public final Map<Long, String> component3() {
            return this.userNames;
        }

        public final Map<Long, GuildRole> component4() {
            return this.roles;
        }

        public final long component5() {
            return this.myId;
        }

        public final Model copy(AttachmentEntry attachmentEntry, Map<Long, String> map, Map<Long, String> map2, Map<Long, GuildRole> map3, long j) {
            m.checkNotNullParameter(attachmentEntry, "attachmentEntry");
            return new Model(attachmentEntry, map, map2, map3, j);
        }

        public final MessageRenderContext createRenderContext(Context context, WidgetChatListAdapter.EventHandler eventHandler) {
            m.checkNotNullParameter(context, "androidContext");
            m.checkNotNullParameter(eventHandler, "eventHandler");
            return new MessageRenderContext(context, this.myId, this.attachmentEntry.getAllowAnimatedEmojis(), this.userNames, this.channelNames, this.roles, 0, null, new WidgetChatListAdapterItemAttachment$Model$createRenderContext$1(eventHandler), 0, 0, null, null, null, 16064, null);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.attachmentEntry, model.attachmentEntry) && m.areEqual(this.channelNames, model.channelNames) && m.areEqual(this.userNames, model.userNames) && m.areEqual(this.roles, model.roles) && this.myId == model.myId;
        }

        public final AttachmentEntry getAttachmentEntry() {
            return this.attachmentEntry;
        }

        public final Map<Long, String> getChannelNames() {
            return this.channelNames;
        }

        public final long getMyId() {
            return this.myId;
        }

        public final Map<Long, GuildRole> getRoles() {
            return this.roles;
        }

        public final Map<Long, String> getUserNames() {
            return this.userNames;
        }

        public int hashCode() {
            AttachmentEntry attachmentEntry = this.attachmentEntry;
            int i = 0;
            int hashCode = (attachmentEntry != null ? attachmentEntry.hashCode() : 0) * 31;
            Map<Long, String> map = this.channelNames;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, String> map2 = this.userNames;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map3 = this.roles;
            if (map3 != null) {
                i = map3.hashCode();
            }
            return b.a(this.myId) + ((hashCode3 + i) * 31);
        }

        public final boolean isSpoilerHidden() {
            return this.isSpoilerHidden;
        }

        public String toString() {
            StringBuilder R = a.R("Model(attachmentEntry=");
            R.append(this.attachmentEntry);
            R.append(", channelNames=");
            R.append(this.channelNames);
            R.append(", userNames=");
            R.append(this.userNames);
            R.append(", roles=");
            R.append(this.roles);
            R.append(", myId=");
            return a.B(R, this.myId, ")");
        }

        public /* synthetic */ Model(AttachmentEntry attachmentEntry, Map map, Map map2, Map map3, long j, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(attachmentEntry, (i & 2) != 0 ? null : map, (i & 4) != 0 ? null : map2, (i & 8) == 0 ? map3 : null, (i & 16) != 0 ? 0L : j);
        }
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        AttachmentEntry attachmentEntry = (AttachmentEntry) chatListEntry;
        Message message = attachmentEntry.getMessage();
        View view = this.binding.c;
        m.checkNotNullExpressionValue(view, "binding.chatListAdapterItemHighlightedBg");
        View view2 = this.binding.f2292b;
        m.checkNotNullExpressionValue(view2, "binding.chatListAdapterItemGutterBg");
        configureCellHighlight(message, view, view2);
        configureUI(new Model(attachmentEntry, null, null, null, 0L, 30, null));
    }
}
