package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.message.call.MessageCall;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.databinding.WidgetChatListAdapterItemCallBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.message.Message;
import com.discord.models.user.CoreUser;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.duration.DurationUtilsKt;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.channels.list.WidgetCollapsedUsersListAdapter;
import com.discord.widgets.channels.list.items.CollapsedUser;
import com.discord.widgets.chat.list.FragmentLifecycleListener;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemCallMessage;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.MessageEntry;
import d0.d0.f;
import d0.t.c0;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemCallMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¾\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u00012\u00020\u0002:\u0002RSB\u000f\u0012\u0006\u0010O\u001a\u00020N¢\u0006\u0004\bP\u0010QJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ#\u0010\u0011\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\f2\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0015\u0010\u0014J!\u0010\u0019\u001a\u0004\u0018\u00010\u00182\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u0017\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u001f\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ'\u0010\"\u001a\u00020\u00052\u0006\u0010!\u001a\u00020 2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u0017\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\"\u0010#J\u0017\u0010'\u001a\u00020&2\u0006\u0010%\u001a\u00020$H\u0002¢\u0006\u0004\b'\u0010(J/\u0010/\u001a\u00020\f2\u0006\u0010*\u001a\u00020)2\u0016\u0010.\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`,\u0012\u0004\u0012\u00020-0+H\u0002¢\u0006\u0004\b/\u00100J=\u00105\u001a\b\u0012\u0004\u0012\u000204032\u0016\u0010.\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`,\u0012\u0004\u0012\u00020-0+2\u0006\u0010\r\u001a\u00020\f2\u0006\u00102\u001a\u000201H\u0002¢\u0006\u0004\b5\u00106J\u001d\u00108\u001a\b\u0012\u0004\u0012\u00020\b072\u0006\u0010!\u001a\u00020 H\u0002¢\u0006\u0004\b8\u00109J\u000f\u0010:\u001a\u00020\u0005H\u0016¢\u0006\u0004\b:\u0010\u0014J\u000f\u0010;\u001a\u00020\u0005H\u0016¢\u0006\u0004\b;\u0010\u0014J\u001f\u0010=\u001a\u00020\u00052\u0006\u0010<\u001a\u00020&2\u0006\u0010\u0004\u001a\u00020\u0003H\u0014¢\u0006\u0004\b=\u0010>R\u0018\u0010@\u001a\u0004\u0018\u00010?8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b@\u0010AR\u0018\u0010B\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bB\u0010CR\u0018\u0010D\u001a\u0004\u0018\u00010?8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bD\u0010AR\u0016\u0010F\u001a\u00020E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u0016\u0010I\u001a\u00020H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010JR\u0016\u0010L\u001a\u00020K8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010M¨\u0006T"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/widgets/chat/list/FragmentLifecycleListener;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "", "configure", "(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$State;", "state", "handleState", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$State;)V", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;", "callStatus", "", "Lcom/discord/primitives/ChannelId;", "channelId", "onItemClick", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;J)V", "resetCurrentChatListEntry", "()V", "clearSubscriptions", "Landroid/content/Context;", "context", "Landroid/graphics/drawable/Drawable;", "getCallDrawable", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;", "Landroid/content/res/Resources;", "resources", "", "getTitleString", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/res/Resources;)Ljava/lang/CharSequence;", "Lcom/discord/widgets/chat/list/entries/MessageEntry;", "messageEntry", "configureSubtitle", "(Lcom/discord/widgets/chat/list/entries/MessageEntry;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;Landroid/content/Context;)V", "", "time", "", "getMinWidthPxForTime", "(Ljava/lang/String;)I", "Lcom/discord/models/message/Message;", "message", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "voiceParticipants", "getCallStatus", "(Lcom/discord/models/message/Message;Ljava/util/Map;)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;", "Lcom/discord/models/user/User;", "messageAuthor", "", "Lcom/discord/widgets/channels/list/items/CollapsedUser;", "createCallParticipantUsers", "(Ljava/util/Map;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;Lcom/discord/models/user/User;)Ljava/util/List;", "Lrx/Observable;", "observeState", "(Lcom/discord/widgets/chat/list/entries/MessageEntry;)Lrx/Observable;", "onResume", "onPause", ModelAuditLogEntry.CHANGE_KEY_POSITION, "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lrx/Subscription;", "ongoingCallDurationSubscription", "Lrx/Subscription;", "chatListEntry", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "stateSubscription", "Lcom/discord/databinding/WidgetChatListAdapterItemCallBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemCallBinding;", "Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;", "usersAdapter", "Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "CallStatus", "State", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemCallMessage extends WidgetChatListItem implements FragmentLifecycleListener {
    private final WidgetChatListAdapterItemCallBinding binding;
    private ChatListEntry chatListEntry;
    private final Clock clock;
    private Subscription ongoingCallDurationSubscription;
    private Subscription stateSubscription;
    private final WidgetCollapsedUsersListAdapter usersAdapter;

    /* compiled from: WidgetChatListAdapterItemCallMessage.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ACTIVE_UNJOINED", "ACTIVE_JOINED", "INACTIVE_UNJOINED", "INACTIVE_JOINED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum CallStatus {
        ACTIVE_UNJOINED,
        ACTIVE_JOINED,
        INACTIVE_UNJOINED,
        INACTIVE_JOINED
    }

    /* compiled from: WidgetChatListAdapterItemCallMessage.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0016\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\u0006\u0010\f\u001a\u00020\b¢\u0006\u0004\b\u001d\u0010\u001eJ \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ4\u0010\r\u001a\u00020\u00002\u0018\b\u0002\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\b\b\u0002\u0010\f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\nR)\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$State;", "", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component1", "()Ljava/util/Map;", "Lcom/discord/widgets/chat/list/entries/MessageEntry;", "component2", "()Lcom/discord/widgets/chat/list/entries/MessageEntry;", "voiceParticipants", "messageEntry", "copy", "(Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/MessageEntry;)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$State;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/list/entries/MessageEntry;", "getMessageEntry", "Ljava/util/Map;", "getVoiceParticipants", HookHelper.constructorName, "(Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/MessageEntry;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class State {
        private final MessageEntry messageEntry;
        private final Map<Long, StoreVoiceParticipants.VoiceUser> voiceParticipants;

        public State(Map<Long, StoreVoiceParticipants.VoiceUser> map, MessageEntry messageEntry) {
            m.checkNotNullParameter(map, "voiceParticipants");
            m.checkNotNullParameter(messageEntry, "messageEntry");
            this.voiceParticipants = map;
            this.messageEntry = messageEntry;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ State copy$default(State state, Map map, MessageEntry messageEntry, int i, Object obj) {
            if ((i & 1) != 0) {
                map = state.voiceParticipants;
            }
            if ((i & 2) != 0) {
                messageEntry = state.messageEntry;
            }
            return state.copy(map, messageEntry);
        }

        public final Map<Long, StoreVoiceParticipants.VoiceUser> component1() {
            return this.voiceParticipants;
        }

        public final MessageEntry component2() {
            return this.messageEntry;
        }

        public final State copy(Map<Long, StoreVoiceParticipants.VoiceUser> map, MessageEntry messageEntry) {
            m.checkNotNullParameter(map, "voiceParticipants");
            m.checkNotNullParameter(messageEntry, "messageEntry");
            return new State(map, messageEntry);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return m.areEqual(this.voiceParticipants, state.voiceParticipants) && m.areEqual(this.messageEntry, state.messageEntry);
        }

        public final MessageEntry getMessageEntry() {
            return this.messageEntry;
        }

        public final Map<Long, StoreVoiceParticipants.VoiceUser> getVoiceParticipants() {
            return this.voiceParticipants;
        }

        public int hashCode() {
            Map<Long, StoreVoiceParticipants.VoiceUser> map = this.voiceParticipants;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            MessageEntry messageEntry = this.messageEntry;
            if (messageEntry != null) {
                i = messageEntry.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("State(voiceParticipants=");
            R.append(this.voiceParticipants);
            R.append(", messageEntry=");
            R.append(this.messageEntry);
            R.append(")");
            return R.toString();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;

        static {
            CallStatus.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            CallStatus callStatus = CallStatus.INACTIVE_UNJOINED;
            iArr[callStatus.ordinal()] = 1;
            CallStatus callStatus2 = CallStatus.INACTIVE_JOINED;
            iArr[callStatus2.ordinal()] = 2;
            CallStatus callStatus3 = CallStatus.ACTIVE_JOINED;
            iArr[callStatus3.ordinal()] = 3;
            CallStatus callStatus4 = CallStatus.ACTIVE_UNJOINED;
            iArr[callStatus4.ordinal()] = 4;
            CallStatus.values();
            int[] iArr2 = new int[4];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[callStatus.ordinal()] = 1;
            iArr2[callStatus2.ordinal()] = 2;
            iArr2[callStatus3.ordinal()] = 3;
            iArr2[callStatus4.ordinal()] = 4;
            CallStatus.values();
            int[] iArr3 = new int[4];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[callStatus.ordinal()] = 1;
            iArr3[callStatus2.ordinal()] = 2;
            iArr3[callStatus3.ordinal()] = 3;
            iArr3[callStatus4.ordinal()] = 4;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemCallMessage(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_call, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_call_icon;
        ImageView imageView = (ImageView) view.findViewById(R.id.chat_list_adapter_item_call_icon);
        if (imageView != null) {
            i = R.id.chat_list_adapter_item_call_participants;
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.chat_list_adapter_item_call_participants);
            if (recyclerView != null) {
                i = R.id.chat_list_adapter_item_call_subtitle;
                TextView textView = (TextView) view.findViewById(R.id.chat_list_adapter_item_call_subtitle);
                if (textView != null) {
                    i = R.id.chat_list_adapter_item_call_title;
                    TextView textView2 = (TextView) view.findViewById(R.id.chat_list_adapter_item_call_title);
                    if (textView2 != null) {
                        i = R.id.chat_list_adapter_item_call_unjoined_ongoing_subtitle;
                        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.chat_list_adapter_item_call_unjoined_ongoing_subtitle);
                        if (linearLayout != null) {
                            i = R.id.chat_list_adapter_item_unjoined_call_duration;
                            TextView textView3 = (TextView) view.findViewById(R.id.chat_list_adapter_item_unjoined_call_duration);
                            if (textView3 != null) {
                                WidgetChatListAdapterItemCallBinding widgetChatListAdapterItemCallBinding = new WidgetChatListAdapterItemCallBinding((CardView) view, imageView, recyclerView, textView, textView2, linearLayout, textView3);
                                m.checkNotNullExpressionValue(widgetChatListAdapterItemCallBinding, "WidgetChatListAdapterIte…allBinding.bind(itemView)");
                                this.binding = widgetChatListAdapterItemCallBinding;
                                this.clock = ClockFactory.get();
                                MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
                                m.checkNotNullExpressionValue(recyclerView, "binding.chatListAdapterItemCallParticipants");
                                this.usersAdapter = (WidgetCollapsedUsersListAdapter) companion.configure(new WidgetCollapsedUsersListAdapter(recyclerView));
                                this.itemView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemCallMessage.1
                                    @Override // android.view.View.OnAttachStateChangeListener
                                    public void onViewAttachedToWindow(View view2) {
                                        m.checkNotNullParameter(view2, "v");
                                        WidgetChatListAdapterItemCallMessage.this.resetCurrentChatListEntry();
                                    }

                                    @Override // android.view.View.OnAttachStateChangeListener
                                    public void onViewDetachedFromWindow(View view2) {
                                        m.checkNotNullParameter(view2, "v");
                                        WidgetChatListAdapterItemCallMessage.this.clearSubscriptions();
                                    }
                                });
                                recyclerView.setHasFixedSize(false);
                                return;
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void clearSubscriptions() {
        Subscription subscription = this.ongoingCallDurationSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        Subscription subscription2 = this.stateSubscription;
        if (subscription2 != null) {
            subscription2.unsubscribe();
        }
    }

    private final void configure(ChatListEntry chatListEntry) {
        this.chatListEntry = chatListEntry;
        Objects.requireNonNull(chatListEntry, "null cannot be cast to non-null type com.discord.widgets.chat.list.entries.MessageEntry");
        clearSubscriptions();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.computationLatest(observeState((MessageEntry) chatListEntry))), WidgetChatListAdapterItemCallMessage.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetChatListAdapterItemCallMessage$configure$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterItemCallMessage$configure$2(this));
    }

    private final void configureSubtitle(MessageEntry messageEntry, CallStatus callStatus, Context context) {
        Subscription subscription = this.ongoingCallDurationSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        Message message = messageEntry.getMessage();
        CharSequence humanizeDuration = DurationUtilsKt.humanizeDuration(context, message.getCallDuration());
        Context x2 = a.x(this.itemView, "itemView", "itemView.context");
        UtcDateTime timestamp = message.getTimestamp();
        String obj = TimeUtils.toReadableTimeString$default(x2, timestamp != null ? timestamp.g() : 0L, null, 4, null).toString();
        LinearLayout linearLayout = this.binding.f;
        m.checkNotNullExpressionValue(linearLayout, "binding.chatListAdapterI…llUnjoinedOngoingSubtitle");
        int i = 8;
        linearLayout.setVisibility(callStatus == CallStatus.ACTIVE_UNJOINED ? 0 : 8);
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.chatListAdapterItemCallSubtitle");
        LinearLayout linearLayout2 = this.binding.f;
        m.checkNotNullExpressionValue(linearLayout2, "binding.chatListAdapterI…llUnjoinedOngoingSubtitle");
        if (!(linearLayout2.getVisibility() == 0)) {
            i = 0;
        }
        textView.setVisibility(i);
        int ordinal = callStatus.ordinal();
        if (ordinal == 0 || ordinal == 1) {
            UtcDateTime timestamp2 = message.getTimestamp();
            long g = timestamp2 != null ? timestamp2.g() : 0L;
            Observable<Long> D = Observable.D(0L, 1L, TimeUnit.SECONDS);
            m.checkNotNullExpressionValue(D, "Observable\n            .…0L, 1L, TimeUnit.SECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(D), WidgetChatListAdapterItemCallMessage.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetChatListAdapterItemCallMessage$configureSubtitle$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterItemCallMessage$configureSubtitle$1(this, g));
        } else if (ordinal == 2) {
            TextView textView2 = this.binding.d;
            m.checkNotNullExpressionValue(textView2, "binding.chatListAdapterItemCallSubtitle");
            b.m(textView2, R.string.call_ended_description, new Object[]{humanizeDuration, obj}, (r4 & 4) != 0 ? b.g.j : null);
        } else if (ordinal == 3) {
            TextView textView3 = this.binding.d;
            m.checkNotNullExpressionValue(textView3, "binding.chatListAdapterItemCallSubtitle");
            b.m(textView3, R.string.call_ended_description, new Object[]{humanizeDuration, obj}, (r4 & 4) != 0 ? b.g.j : null);
        }
    }

    private final List<CollapsedUser> createCallParticipantUsers(Map<Long, StoreVoiceParticipants.VoiceUser> map, CallStatus callStatus, User user) {
        if (callStatus == CallStatus.INACTIVE_JOINED || callStatus == CallStatus.INACTIVE_UNJOINED) {
            return d0.t.m.listOf(new CollapsedUser(user, false, 0L, 6, null));
        }
        List list = u.toList(map.values());
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (true) {
            boolean z2 = false;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((StoreVoiceParticipants.VoiceUser) next).getVoiceState() != null) {
                z2 = true;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        int size = arrayList.size();
        ArrayList arrayList2 = new ArrayList();
        int i = size - 3;
        Iterator<Integer> it2 = f.until(0, Math.min(size, 3)).iterator();
        while (it2.hasNext()) {
            arrayList2.add(new CollapsedUser(((StoreVoiceParticipants.VoiceUser) arrayList.get(((c0) it2).nextInt())).getUser(), false, 0L, 6, null));
        }
        if (i > 0) {
            arrayList2.add(CollapsedUser.Companion.createEmptyUser(i));
        }
        return arrayList2;
    }

    private final Drawable getCallDrawable(CallStatus callStatus, Context context) {
        Drawable drawable;
        int ordinal = callStatus.ordinal();
        if (ordinal == 0 || ordinal == 1) {
            drawable = AppCompatResources.getDrawable(context, R.drawable.ic_call_24dp);
            if (drawable == null) {
                return null;
            }
            ColorCompatKt.setTint(drawable, ColorCompat.getColor(context, (int) R.color.status_green_600), false);
        } else if (ordinal == 2) {
            drawable = AppCompatResources.getDrawable(context, R.drawable.ic_call_disconnect_24dp);
            if (drawable == null) {
                return null;
            }
            ColorCompatKt.setTint(drawable, ColorCompat.getColor(context, (int) R.color.status_red), false);
        } else if (ordinal == 3) {
            drawable = AppCompatResources.getDrawable(context, R.drawable.ic_call_24dp);
            if (drawable == null) {
                return null;
            }
            ColorCompatKt.setTint(drawable, ColorCompat.getThemedColor(context, (int) R.attr.colorInteractiveNormal), false);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        return drawable;
    }

    private final CallStatus getCallStatus(Message message, Map<Long, StoreVoiceParticipants.VoiceUser> map) {
        boolean z2;
        MessageCall call = message.getCall();
        if (call == null) {
            return CallStatus.INACTIVE_UNJOINED;
        }
        long userId = ((WidgetChatListAdapter) this.adapter).getData().getUserId();
        List list = u.toList(map.entrySet());
        ArrayList<Map.Entry> arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (true) {
            z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((StoreVoiceParticipants.VoiceUser) ((Map.Entry) next).getValue()).getVoiceState() == null) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
        for (Map.Entry entry : arrayList) {
            arrayList2.add(Long.valueOf(((Number) entry.getKey()).longValue()));
        }
        boolean contains = call.b().contains(Long.valueOf(userId));
        boolean contains2 = arrayList2.contains(Long.valueOf(userId));
        if (call.a() != null) {
            z2 = false;
        }
        if (z2 && contains2) {
            return CallStatus.ACTIVE_JOINED;
        }
        if (z2) {
            return CallStatus.ACTIVE_UNJOINED;
        }
        if (contains) {
            return CallStatus.INACTIVE_JOINED;
        }
        return CallStatus.INACTIVE_UNJOINED;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final int getMinWidthPxForTime(String str) {
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (str.charAt(i2) == ':') {
                i++;
            }
        }
        return (i * DimenUtils.dpToPixels(3.047619f)) + ((str.length() - i) * DimenUtils.dpToPixels(7.6190476f));
    }

    private final CharSequence getTitleString(CallStatus callStatus, Resources resources) {
        CharSequence c;
        CharSequence c2;
        CharSequence c3;
        int ordinal = callStatus.ordinal();
        if (ordinal == 0 || ordinal == 1) {
            c = b.c(resources, R.string.ongoing_call, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
            return c;
        } else if (ordinal == 2) {
            c2 = b.c(resources, R.string.missed_call, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
            return c2;
        } else if (ordinal == 3) {
            c3 = b.c(resources, R.string.call_ended, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
            return c3;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleState(State state) {
        MessageEntry messageEntry = state.getMessageEntry();
        final Message message = messageEntry.getMessage();
        Map<Long, StoreVoiceParticipants.VoiceUser> voiceParticipants = state.getVoiceParticipants();
        final CallStatus callStatus = getCallStatus(message, voiceParticipants);
        com.discord.api.user.User author = message.getAuthor();
        m.checkNotNull(author);
        List<CollapsedUser> createCallParticipantUsers = createCallParticipantUsers(voiceParticipants, callStatus, new CoreUser(author));
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.chatListAdapterItemCallSubtitle");
        Context context = textView.getContext();
        m.checkNotNullExpressionValue(context, "binding.chatListAdapterItemCallSubtitle.context");
        configureSubtitle(messageEntry, callStatus, context);
        this.binding.f2295b.setImageDrawable(getCallDrawable(callStatus, a.x(this.itemView, "itemView", "itemView.context")));
        TextView textView2 = this.binding.e;
        m.checkNotNullExpressionValue(textView2, "binding.chatListAdapterItemCallTitle");
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        Resources resources = view.getResources();
        m.checkNotNullExpressionValue(resources, "itemView.resources");
        textView2.setText(getTitleString(callStatus, resources));
        this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemCallMessage$handleState$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChatListAdapterItemCallMessage.this.onItemClick(callStatus, message.getChannelId());
            }
        });
        this.usersAdapter.setData(u.toList(createCallParticipantUsers));
    }

    private final Observable<State> observeState(final MessageEntry messageEntry) {
        Observable F = StoreStream.Companion.getVoiceParticipants().get(messageEntry.getMessage().getChannelId()).F(new j0.k.b<Map<Long, ? extends StoreVoiceParticipants.VoiceUser>, State>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemCallMessage$observeState$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ WidgetChatListAdapterItemCallMessage.State call(Map<Long, ? extends StoreVoiceParticipants.VoiceUser> map) {
                return call2((Map<Long, StoreVoiceParticipants.VoiceUser>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final WidgetChatListAdapterItemCallMessage.State call2(Map<Long, StoreVoiceParticipants.VoiceUser> map) {
                m.checkNotNullExpressionValue(map, "voiceParticipants");
                return new WidgetChatListAdapterItemCallMessage.State(map, MessageEntry.this);
            }
        });
        m.checkNotNullExpressionValue(F, "StoreStream.getVoicePart…, messageEntry)\n        }");
        return F;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onItemClick(CallStatus callStatus, long j) {
        ((WidgetChatListAdapter) this.adapter).getEventHandler().onCallMessageClicked(j, callStatus);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void resetCurrentChatListEntry() {
        ChatListEntry chatListEntry = this.chatListEntry;
        if (chatListEntry != null) {
            configure(chatListEntry);
        }
    }

    @Override // com.discord.widgets.chat.list.FragmentLifecycleListener
    public void onPause() {
        clearSubscriptions();
    }

    @Override // com.discord.widgets.chat.list.FragmentLifecycleListener
    public void onResume() {
        resetCurrentChatListEntry();
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        configure(chatListEntry);
    }
}
