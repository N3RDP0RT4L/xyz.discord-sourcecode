package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityAssets;
import com.discord.api.activity.ActivityParty;
import com.discord.databinding.WidgetChatListAdapterItemSpotifyListenTogetherBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.presence.Presence;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.integrations.SpotifyHelper;
import com.discord.utilities.platform.Platform;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.channels.list.WidgetCollapsedUsersListAdapter;
import com.discord.widgets.channels.list.items.CollapsedUser;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSpotifyListenTogether;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.SpotifyListenTogetherEntry;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.d0.f;
import d0.g;
import d0.g0.w;
import d0.t.d0;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func3;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemSpotifyListenTogether.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 +2\u00020\u0001:\u0002+,B\u000f\u0012\u0006\u0010(\u001a\u00020'¢\u0006\u0004\b)\u0010*J\u0013\u0010\u0004\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001f\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ!\u0010\u0011\u001a\u00020\u00102\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000f\u001a\u00020\bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0015H\u0014¢\u0006\u0004\b\u0017\u0010\u0018J\u0011\u0010\u001a\u001a\u0004\u0018\u00010\u0019H\u0014¢\u0006\u0004\b\u001a\u0010\u001bR\u0018\u0010\u001c\u001a\u0004\u0018\u00010\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u001d\u0010&\u001a\u00020!8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%¨\u0006-"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether$Model;", "", "configureUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether$Model;)V", "Landroid/content/Context;", "context", "Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;", "spotifyListenTogetherEntry", "", "getActivityName", "(Landroid/content/Context;Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;)Ljava/lang/String;", "Lcom/discord/models/presence/Presence;", "presence", "item", "", "isDeadInvite", "(Lcom/discord/models/presence/Presence;Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;)Z", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "Lcom/discord/databinding/WidgetChatListAdapterItemSpotifyListenTogetherBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemSpotifyListenTogetherBinding;", "Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;", "userAdapter$delegate", "Lkotlin/Lazy;", "getUserAdapter", "()Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;", "userAdapter", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemSpotifyListenTogether extends WidgetChatListItem {
    public static final Companion Companion = new Companion(null);
    private static final long EMBED_LIFETIME_MILLIS = 7200000;
    private static final long MAX_USERS_SHOWN = 6;
    private final WidgetChatListAdapterItemSpotifyListenTogetherBinding binding;
    private Subscription subscription;
    private final Lazy userAdapter$delegate;

    /* compiled from: WidgetChatListAdapterItemSpotifyListenTogether.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether$Companion;", "", "", "EMBED_LIFETIME_MILLIS", "J", "MAX_USERS_SHOWN", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChatListAdapterItemSpotifyListenTogether.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0082\b\u0018\u0000 '2\u00020\u0001:\u0001'B/\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0011\u001a\u00020\t\u0012\u0006\u0010\u0012\u001a\u00020\f¢\u0006\u0004\b%\u0010&J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ@\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00022\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0011\u001a\u00020\t2\b\b\u0002\u0010\u0012\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001c\u001a\u00020\f2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0012\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u001e\u001a\u0004\b\u0012\u0010\u000eR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u0004R\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b\"\u0010\bR\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b$\u0010\u000b¨\u0006("}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether$Model;", "", "Lcom/discord/models/presence/Presence;", "component1", "()Lcom/discord/models/presence/Presence;", "", "Lcom/discord/widgets/channels/list/items/CollapsedUser;", "component2", "()Ljava/util/List;", "Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;", "component3", "()Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;", "", "component4", "()Z", "presence", "users", "item", "isMe", "copy", "(Lcom/discord/models/presence/Presence;Ljava/util/List;Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;Z)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/models/presence/Presence;", "getPresence", "Ljava/util/List;", "getUsers", "Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;", "getItem", HookHelper.constructorName, "(Lcom/discord/models/presence/Presence;Ljava/util/List;Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;Z)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean isMe;
        private final SpotifyListenTogetherEntry item;
        private final Presence presence;
        private final List<CollapsedUser> users;

        /* compiled from: WidgetChatListAdapterItemSpotifyListenTogether.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J=\u0010\r\u001a\u00020\f2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001b\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\f0\u000f2\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether$Model$Companion;", "", "Lcom/discord/models/presence/Presence;", "presence", "", "", "Lcom/discord/models/user/User;", "userMap", "Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;", "item", "", "isMe", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether$Model;", "create", "(Lcom/discord/models/presence/Presence;Ljava/util/Map;Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;Z)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSpotifyListenTogether$Model;", "Lrx/Observable;", "get", "(Lcom/discord/widgets/chat/list/entries/SpotifyListenTogetherEntry;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Model create(Presence presence, Map<Long, ? extends User> map, SpotifyListenTogetherEntry spotifyListenTogetherEntry, boolean z2) {
                ActivityParty i;
                HashMap hashMap = new HashMap();
                for (User user : map.values()) {
                    hashMap.put(Long.valueOf(user.getId()), new CollapsedUser(user, false, 0L, 6, null));
                }
                if (z2) {
                    MeUser me2 = StoreStream.Companion.getUsers().getMe();
                    hashMap.put(Long.valueOf(me2.getId()), new CollapsedUser(me2, false, 0L, 6, null));
                }
                Activity spotifyListeningActivity = presence != null ? PresenceUtils.INSTANCE.getSpotifyListeningActivity(presence) : null;
                long maxSize = (spotifyListeningActivity == null || (i = spotifyListeningActivity.i()) == null) ? 0L : PresenceUtils.INSTANCE.getMaxSize(i);
                Collection values = hashMap.values();
                m.checkNotNullExpressionValue(values, "collapsedUserMap.values");
                List mutableList = u.toMutableList(values);
                Iterator<Long> it = f.until(map.size(), Math.min((long) WidgetChatListAdapterItemSpotifyListenTogether.MAX_USERS_SHOWN, maxSize)).iterator();
                while (it.hasNext()) {
                    mutableList.add(CollapsedUser.Companion.createEmptyUser(((d0) it).nextLong() == 5 ? maxSize - WidgetChatListAdapterItemSpotifyListenTogether.MAX_USERS_SHOWN : 0L));
                }
                return new Model(presence, mutableList, spotifyListenTogetherEntry, z2);
            }

            public final Observable<Model> get(final SpotifyListenTogetherEntry spotifyListenTogetherEntry) {
                m.checkNotNullParameter(spotifyListenTogetherEntry, "item");
                StoreStream.Companion companion = StoreStream.Companion;
                Observable i = Observable.i(companion.getPresences().observePresenceForUser(spotifyListenTogetherEntry.getUserId()), companion.getGameParty().observeUsersForPartyId(spotifyListenTogetherEntry.getActivity().a()), companion.getUsers().observeMeId(), new Func3<Presence, Map<Long, ? extends User>, Long, Model>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSpotifyListenTogether$Model$Companion$get$1
                    public final WidgetChatListAdapterItemSpotifyListenTogether.Model call(Presence presence, Map<Long, ? extends User> map, Long l) {
                        WidgetChatListAdapterItemSpotifyListenTogether.Model create;
                        WidgetChatListAdapterItemSpotifyListenTogether.Model.Companion companion2 = WidgetChatListAdapterItemSpotifyListenTogether.Model.Companion;
                        m.checkNotNullExpressionValue(map, "userMap");
                        SpotifyListenTogetherEntry spotifyListenTogetherEntry2 = SpotifyListenTogetherEntry.this;
                        create = companion2.create(presence, map, spotifyListenTogetherEntry2, l != null && l.longValue() == spotifyListenTogetherEntry2.getUserId());
                        return create;
                    }
                });
                m.checkNotNullExpressionValue(i, "Observable\n             …m.userId)\n              }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(i).q();
                m.checkNotNullExpressionValue(q, "Observable\n             …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Presence presence, List<CollapsedUser> list, SpotifyListenTogetherEntry spotifyListenTogetherEntry, boolean z2) {
            m.checkNotNullParameter(list, "users");
            m.checkNotNullParameter(spotifyListenTogetherEntry, "item");
            this.presence = presence;
            this.users = list;
            this.item = spotifyListenTogetherEntry;
            this.isMe = z2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Model copy$default(Model model, Presence presence, List list, SpotifyListenTogetherEntry spotifyListenTogetherEntry, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                presence = model.presence;
            }
            if ((i & 2) != 0) {
                list = model.users;
            }
            if ((i & 4) != 0) {
                spotifyListenTogetherEntry = model.item;
            }
            if ((i & 8) != 0) {
                z2 = model.isMe;
            }
            return model.copy(presence, list, spotifyListenTogetherEntry, z2);
        }

        public final Presence component1() {
            return this.presence;
        }

        public final List<CollapsedUser> component2() {
            return this.users;
        }

        public final SpotifyListenTogetherEntry component3() {
            return this.item;
        }

        public final boolean component4() {
            return this.isMe;
        }

        public final Model copy(Presence presence, List<CollapsedUser> list, SpotifyListenTogetherEntry spotifyListenTogetherEntry, boolean z2) {
            m.checkNotNullParameter(list, "users");
            m.checkNotNullParameter(spotifyListenTogetherEntry, "item");
            return new Model(presence, list, spotifyListenTogetherEntry, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.presence, model.presence) && m.areEqual(this.users, model.users) && m.areEqual(this.item, model.item) && this.isMe == model.isMe;
        }

        public final SpotifyListenTogetherEntry getItem() {
            return this.item;
        }

        public final Presence getPresence() {
            return this.presence;
        }

        public final List<CollapsedUser> getUsers() {
            return this.users;
        }

        public int hashCode() {
            Presence presence = this.presence;
            int i = 0;
            int hashCode = (presence != null ? presence.hashCode() : 0) * 31;
            List<CollapsedUser> list = this.users;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            SpotifyListenTogetherEntry spotifyListenTogetherEntry = this.item;
            if (spotifyListenTogetherEntry != null) {
                i = spotifyListenTogetherEntry.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.isMe;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isMe() {
            return this.isMe;
        }

        public String toString() {
            StringBuilder R = a.R("Model(presence=");
            R.append(this.presence);
            R.append(", users=");
            R.append(this.users);
            R.append(", item=");
            R.append(this.item);
            R.append(", isMe=");
            return a.M(R, this.isMe, ")");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemSpotifyListenTogether(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_spotify_listen_together, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.barrier;
        Barrier barrier = (Barrier) view.findViewById(R.id.barrier);
        if (barrier != null) {
            i = R.id.item_listen_together_album_image;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.item_listen_together_album_image);
            if (simpleDraweeView != null) {
                i = R.id.item_listen_together_artist;
                TextView textView = (TextView) view.findViewById(R.id.item_listen_together_artist);
                if (textView != null) {
                    ConstraintLayout constraintLayout = (ConstraintLayout) view;
                    i = R.id.item_listen_together_header;
                    TextView textView2 = (TextView) view.findViewById(R.id.item_listen_together_header);
                    if (textView2 != null) {
                        i = R.id.item_listen_together_join;
                        TextView textView3 = (TextView) view.findViewById(R.id.item_listen_together_join);
                        if (textView3 != null) {
                            i = R.id.item_listen_together_recycler;
                            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.item_listen_together_recycler);
                            if (recyclerView != null) {
                                i = R.id.item_listen_together_session_ended;
                                TextView textView4 = (TextView) view.findViewById(R.id.item_listen_together_session_ended);
                                if (textView4 != null) {
                                    i = R.id.item_listen_together_track;
                                    TextView textView5 = (TextView) view.findViewById(R.id.item_listen_together_track);
                                    if (textView5 != null) {
                                        WidgetChatListAdapterItemSpotifyListenTogetherBinding widgetChatListAdapterItemSpotifyListenTogetherBinding = new WidgetChatListAdapterItemSpotifyListenTogetherBinding(constraintLayout, barrier, simpleDraweeView, textView, constraintLayout, textView2, textView3, recyclerView, textView4, textView5);
                                        m.checkNotNullExpressionValue(widgetChatListAdapterItemSpotifyListenTogetherBinding, "WidgetChatListAdapterIte…herBinding.bind(itemView)");
                                        this.binding = widgetChatListAdapterItemSpotifyListenTogetherBinding;
                                        this.userAdapter$delegate = g.lazy(new WidgetChatListAdapterItemSpotifyListenTogether$userAdapter$2(this));
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        CharSequence b2;
        String a;
        TextView textView = this.binding.e;
        m.checkNotNullExpressionValue(textView, "binding.itemListenTogetherHeader");
        TextView textView2 = this.binding.e;
        m.checkNotNullExpressionValue(textView2, "binding.itemListenTogetherHeader");
        Context context = textView2.getContext();
        m.checkNotNullExpressionValue(context, "binding.itemListenTogetherHeader.context");
        TextView textView3 = this.binding.e;
        m.checkNotNullExpressionValue(textView3, "binding.itemListenTogetherHeader");
        Context context2 = textView3.getContext();
        m.checkNotNullExpressionValue(context2, "binding.itemListenTogetherHeader.context");
        String str = null;
        b2 = b.b(context, R.string.invite_embed_invite_to_listen, new Object[]{getActivityName(context2, model.getItem())}, (r4 & 4) != 0 ? b.C0034b.j : null);
        textView.setText(b2);
        boolean isDeadInvite = isDeadInvite(model.getPresence(), model.getItem());
        Presence presence = model.getPresence();
        final Activity spotifyListeningActivity = presence != null ? PresenceUtils.INSTANCE.getSpotifyListeningActivity(presence) : null;
        TextView textView4 = this.binding.g;
        m.checkNotNullExpressionValue(textView4, "binding.itemListenTogetherSessionEnded");
        int i = 8;
        textView4.setVisibility(isDeadInvite ? 0 : 8);
        RecyclerView recyclerView = this.binding.f;
        m.checkNotNullExpressionValue(recyclerView, "binding.itemListenTogetherRecycler");
        recyclerView.setVisibility(isDeadInvite ^ true ? 0 : 8);
        TextView textView5 = this.binding.h;
        m.checkNotNullExpressionValue(textView5, "binding.itemListenTogetherTrack");
        textView5.setVisibility(isDeadInvite ^ true ? 0 : 8);
        TextView textView6 = this.binding.c;
        m.checkNotNullExpressionValue(textView6, "binding.itemListenTogetherArtist");
        textView6.setVisibility(isDeadInvite ^ true ? 0 : 8);
        SimpleDraweeView simpleDraweeView = this.binding.f2312b;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.itemListenTogetherAlbumImage");
        if (!isDeadInvite) {
            i = 0;
        }
        simpleDraweeView.setVisibility(i);
        ConstraintLayout constraintLayout = this.binding.d;
        m.checkNotNullExpressionValue(constraintLayout, "binding.itemListenTogetherContainer");
        constraintLayout.setSelected(true);
        if (!isDeadInvite) {
            getUserAdapter().setData(model.getUsers());
            TextView textView7 = this.binding.h;
            m.checkNotNullExpressionValue(textView7, "binding.itemListenTogetherTrack");
            textView7.setText(spotifyListeningActivity != null ? spotifyListeningActivity.e() : null);
            TextView textView8 = this.binding.c;
            m.checkNotNullExpressionValue(textView8, "binding.itemListenTogetherArtist");
            Object[] objArr = new Object[1];
            objArr[0] = spotifyListeningActivity != null ? spotifyListeningActivity.l() : null;
            b.m(textView8, R.string.user_activity_listening_artists, objArr, (r4 & 4) != 0 ? b.g.j : null);
            ActivityAssets b3 = spotifyListeningActivity != null ? spotifyListeningActivity.b() : null;
            if (!(b3 == null || (a = b3.a()) == null)) {
                SimpleDraweeView simpleDraweeView2 = this.binding.f2312b;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.itemListenTogetherAlbumImage");
                MGImages.setImage$default(simpleDraweeView2, IconUtils.getAssetImage$default(IconUtils.INSTANCE, null, a, 0, 4, null), 0, 0, false, null, null, 124, null);
            }
            SimpleDraweeView simpleDraweeView3 = this.binding.f2312b;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.itemListenTogetherAlbumImage");
            if (b3 != null) {
                str = b3.b();
            }
            simpleDraweeView3.setContentDescription(str);
            this.binding.h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSpotifyListenTogether$configureUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SpotifyHelper.INSTANCE.launchTrack(a.x(view, "it", "it.context"), Activity.this);
                }
            });
            this.binding.f2312b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSpotifyListenTogether$configureUI$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SpotifyHelper.INSTANCE.launchAlbum(a.x(view, "it", "it.context"), spotifyListeningActivity, WidgetChatListAdapterItemSpotifyListenTogether.Model.this.getItem().getUserId(), WidgetChatListAdapterItemSpotifyListenTogether.Model.this.isMe());
                }
            });
        }
    }

    private final String getActivityName(Context context, SpotifyListenTogetherEntry spotifyListenTogetherEntry) {
        String a = spotifyListenTogetherEntry.getActivity().a();
        Platform platform = Platform.SPOTIFY;
        if (w.contains((CharSequence) a, (CharSequence) platform.getPlatformId(), true)) {
            return platform.getProperName();
        }
        String string = context.getString(R.string.form_label_desktop_only);
        m.checkNotNullExpressionValue(string, "context.getString(R.stri….form_label_desktop_only)");
        return string;
    }

    private final WidgetCollapsedUsersListAdapter getUserAdapter() {
        return (WidgetCollapsedUsersListAdapter) this.userAdapter$delegate.getValue();
    }

    private final boolean isDeadInvite(Presence presence, SpotifyListenTogetherEntry spotifyListenTogetherEntry) {
        ActivityParty i;
        String a;
        Activity spotifyListeningActivity = presence != null ? PresenceUtils.INSTANCE.getSpotifyListeningActivity(presence) : null;
        return !((spotifyListeningActivity == null || (i = spotifyListeningActivity.i()) == null || (a = i.a()) == null) ? false : a.equals(spotifyListenTogetherEntry.getActivity().a())) || TimeUtils.parseSnowflake(Long.valueOf(spotifyListenTogetherEntry.getMessageId())) + EMBED_LIFETIME_MILLIS < ClockFactory.get().currentTimeMillis();
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.subscription;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(Model.Companion.get((SpotifyListenTogetherEntry) chatListEntry)), WidgetChatListAdapterItemSpotifyListenTogether.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetChatListAdapterItemSpotifyListenTogether$onConfigure$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterItemSpotifyListenTogether$onConfigure$2(this));
    }
}
