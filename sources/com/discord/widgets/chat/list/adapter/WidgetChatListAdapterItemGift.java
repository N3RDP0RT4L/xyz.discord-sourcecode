package com.discord.widgets.chat.list.adapter;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewGroupKt;
import androidx.exifinterface.media.ExifInterface;
import b.a.a.z.c;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.application.Application;
import com.discord.api.user.User;
import com.discord.databinding.WidgetChatListAdapterItemGiftBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGift;
import com.discord.models.domain.ModelLibraryApplication;
import com.discord.models.domain.ModelSku;
import com.discord.models.domain.ModelStoreListing;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreGifting;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.fresco.GrayscalePostprocessor;
import com.discord.utilities.gifting.GiftStyleKt;
import com.discord.utilities.gifting.GiftingUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGift;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.GiftEntry;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.g0.t;
import d0.z.d.m;
import j0.l.e.k;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func2;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemGift.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 $2\u00020\u0001:\u0003$%&B\u000f\u0012\u0006\u0010!\u001a\u00020 ¢\u0006\u0004\b\"\u0010#J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\tH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0019\u0010\r\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0011H\u0014¢\u0006\u0004\b\u0013\u0010\u0014J\u0011\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0018\u0010\u001e\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001f¨\u0006'"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model;)V", "configureLoadingUI", "()V", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Resolved;", "configureResolvedUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Resolved;)V", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Invalid;", "configureInvalidUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Invalid;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "Lcom/discord/databinding/WidgetChatListAdapterItemGiftBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemGiftBinding;", "Lcom/discord/widgets/chat/list/entries/GiftEntry;", "item", "Lcom/discord/widgets/chat/list/entries/GiftEntry;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Companion", ExifInterface.TAG_MODEL, "ModelProvider", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemGift extends WidgetChatListItem {
    public static final Companion Companion = new Companion(null);
    private static final GrayscalePostprocessor SPLASH_IMAGE_POSTPROCESSOR = new GrayscalePostprocessor();
    private final WidgetChatListAdapterItemGiftBinding binding;
    private GiftEntry item;
    private Subscription subscription;

    /* compiled from: WidgetChatListAdapterItemGift.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Companion;", "", "", "skuId", "", "getDiscordStoreURL", "(J)Ljava/lang/String;", "Lcom/discord/utilities/fresco/GrayscalePostprocessor;", "SPLASH_IMAGE_POSTPROCESSOR", "Lcom/discord/utilities/fresco/GrayscalePostprocessor;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final String getDiscordStoreURL(long j) {
            return a.s("https://discord.com/store/skus/", j);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChatListAdapterItemGift.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model;", "", HookHelper.constructorName, "()V", "Invalid", "Loading", "Resolved", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Loading;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Resolved;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Invalid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Model {

        /* compiled from: WidgetChatListAdapterItemGift.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u000e\u0010\b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0018\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J0\u0010\n\u001a\u00020\u00002\u0010\b\u0002\u0010\b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R!\u0010\b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Invalid;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model;", "", "Lcom/discord/primitives/UserId;", "component1", "()Ljava/lang/Long;", "component2", "()J", "gifterUserId", "meId", "copy", "(Ljava/lang/Long;J)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Invalid;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getGifterUserId", "J", "getMeId", HookHelper.constructorName, "(Ljava/lang/Long;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends Model {
            private final Long gifterUserId;
            private final long meId;

            public Invalid(Long l, long j) {
                super(null);
                this.gifterUserId = l;
                this.meId = j;
            }

            public static /* synthetic */ Invalid copy$default(Invalid invalid, Long l, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    l = invalid.gifterUserId;
                }
                if ((i & 2) != 0) {
                    j = invalid.meId;
                }
                return invalid.copy(l, j);
            }

            public final Long component1() {
                return this.gifterUserId;
            }

            public final long component2() {
                return this.meId;
            }

            public final Invalid copy(Long l, long j) {
                return new Invalid(l, j);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Invalid)) {
                    return false;
                }
                Invalid invalid = (Invalid) obj;
                return m.areEqual(this.gifterUserId, invalid.gifterUserId) && this.meId == invalid.meId;
            }

            public final Long getGifterUserId() {
                return this.gifterUserId;
            }

            public final long getMeId() {
                return this.meId;
            }

            public int hashCode() {
                Long l = this.gifterUserId;
                return b.a(this.meId) + ((l != null ? l.hashCode() : 0) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("Invalid(gifterUserId=");
                R.append(this.gifterUserId);
                R.append(", meId=");
                return a.B(R, this.meId, ")");
            }
        }

        /* compiled from: WidgetChatListAdapterItemGift.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Loading;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends Model {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: WidgetChatListAdapterItemGift.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\b\u0012\u0006\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b#\u0010$J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ8\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u001a\u001a\u00020\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\u000f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\nR\u0019\u0010\u000e\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010 \u001a\u0004\b\"\u0010\n¨\u0006%"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Resolved;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model;", "Lcom/discord/models/domain/ModelGift;", "component1", "()Lcom/discord/models/domain/ModelGift;", "Lcom/discord/models/user/MeUser;", "component2", "()Lcom/discord/models/user/MeUser;", "", "component3", "()Z", "component4", "gift", "meUser", "inLibrary", "redeeming", "copy", "(Lcom/discord/models/domain/ModelGift;Lcom/discord/models/user/MeUser;ZZ)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model$Resolved;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelGift;", "getGift", "Lcom/discord/models/user/MeUser;", "getMeUser", "Z", "getRedeeming", "getInLibrary", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelGift;Lcom/discord/models/user/MeUser;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Resolved extends Model {
            private final ModelGift gift;
            private final boolean inLibrary;
            private final MeUser meUser;
            private final boolean redeeming;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Resolved(ModelGift modelGift, MeUser meUser, boolean z2, boolean z3) {
                super(null);
                m.checkNotNullParameter(modelGift, "gift");
                m.checkNotNullParameter(meUser, "meUser");
                this.gift = modelGift;
                this.meUser = meUser;
                this.inLibrary = z2;
                this.redeeming = z3;
            }

            public static /* synthetic */ Resolved copy$default(Resolved resolved, ModelGift modelGift, MeUser meUser, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelGift = resolved.gift;
                }
                if ((i & 2) != 0) {
                    meUser = resolved.meUser;
                }
                if ((i & 4) != 0) {
                    z2 = resolved.inLibrary;
                }
                if ((i & 8) != 0) {
                    z3 = resolved.redeeming;
                }
                return resolved.copy(modelGift, meUser, z2, z3);
            }

            public final ModelGift component1() {
                return this.gift;
            }

            public final MeUser component2() {
                return this.meUser;
            }

            public final boolean component3() {
                return this.inLibrary;
            }

            public final boolean component4() {
                return this.redeeming;
            }

            public final Resolved copy(ModelGift modelGift, MeUser meUser, boolean z2, boolean z3) {
                m.checkNotNullParameter(modelGift, "gift");
                m.checkNotNullParameter(meUser, "meUser");
                return new Resolved(modelGift, meUser, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Resolved)) {
                    return false;
                }
                Resolved resolved = (Resolved) obj;
                return m.areEqual(this.gift, resolved.gift) && m.areEqual(this.meUser, resolved.meUser) && this.inLibrary == resolved.inLibrary && this.redeeming == resolved.redeeming;
            }

            public final ModelGift getGift() {
                return this.gift;
            }

            public final boolean getInLibrary() {
                return this.inLibrary;
            }

            public final MeUser getMeUser() {
                return this.meUser;
            }

            public final boolean getRedeeming() {
                return this.redeeming;
            }

            public int hashCode() {
                ModelGift modelGift = this.gift;
                int i = 0;
                int hashCode = (modelGift != null ? modelGift.hashCode() : 0) * 31;
                MeUser meUser = this.meUser;
                if (meUser != null) {
                    i = meUser.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.inLibrary;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.redeeming;
                if (!z3) {
                    i3 = z3 ? 1 : 0;
                }
                return i6 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("Resolved(gift=");
                R.append(this.gift);
                R.append(", meUser=");
                R.append(this.meUser);
                R.append(", inLibrary=");
                R.append(this.inLibrary);
                R.append(", redeeming=");
                return a.M(R, this.redeeming, ")");
            }
        }

        private Model() {
        }

        public /* synthetic */ Model(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChatListAdapterItemGift.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\bÂ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001d\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J'\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u001b\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u0007¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$ModelProvider;", "", "Lcom/discord/widgets/chat/list/entries/GiftEntry;", "item", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGift$Model;", "getInvalidGift", "(Lcom/discord/widgets/chat/list/entries/GiftEntry;)Lrx/Observable;", "Lcom/discord/models/domain/ModelGift;", "gift", "", "redeeming", "getResolvedGiftModel", "(Lcom/discord/models/domain/ModelGift;Z)Lrx/Observable;", "get", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ModelProvider {
        public static final ModelProvider INSTANCE = new ModelProvider();

        private ModelProvider() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Model> getInvalidGift(GiftEntry giftEntry) {
            Observable<Model> j = Observable.j(new k(Long.valueOf(giftEntry.getUserId())), StoreStream.Companion.getUsers().observeMeId(), WidgetChatListAdapterItemGift$ModelProvider$getInvalidGift$1.INSTANCE);
            m.checkNotNullExpressionValue(j, "Observable.combineLatest…lid(authorUserId, meId) }");
            return j;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Model> getResolvedGiftModel(final ModelGift modelGift, final boolean z2) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<Model> j = Observable.j(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getLibrary().observeApplications(), new Func2<MeUser, Map<Long, ? extends ModelLibraryApplication>, Model>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGift$ModelProvider$getResolvedGiftModel$1
                @Override // rx.functions.Func2
                public /* bridge */ /* synthetic */ WidgetChatListAdapterItemGift.Model call(MeUser meUser, Map<Long, ? extends ModelLibraryApplication> map) {
                    return call2(meUser, (Map<Long, ModelLibraryApplication>) map);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetChatListAdapterItemGift.Model call2(MeUser meUser, Map<Long, ModelLibraryApplication> map) {
                    WidgetChatListAdapterItemGift.Model model;
                    if (!ModelGift.this.isExpired(ClockFactory.get().currentTimeMillis())) {
                        ModelGift modelGift2 = ModelGift.this;
                        m.checkNotNullExpressionValue(meUser, "me");
                        boolean z3 = true;
                        if (map == null || !map.containsKey(Long.valueOf(ModelGift.this.getSkuId()))) {
                            z3 = false;
                        }
                        model = new WidgetChatListAdapterItemGift.Model.Resolved(modelGift2, meUser, z3, z2);
                    } else {
                        User user = ModelGift.this.getUser();
                        model = new WidgetChatListAdapterItemGift.Model.Invalid(user != null ? Long.valueOf(user.i()) : null, meUser.getId());
                    }
                    return model;
                }
            });
            m.checkNotNullExpressionValue(j, "Observable.combineLatest… me.id)\n        }\n      }");
            return j;
        }

        public static /* synthetic */ Observable getResolvedGiftModel$default(ModelProvider modelProvider, ModelGift modelGift, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z2 = false;
            }
            return modelProvider.getResolvedGiftModel(modelGift, z2);
        }

        public final Observable<Model> get(final GiftEntry giftEntry) {
            m.checkNotNullParameter(giftEntry, "item");
            Observable Y = StoreStream.Companion.getGifting().requestGift(giftEntry.getGiftCode()).Y(new j0.k.b<StoreGifting.GiftState, Observable<? extends Model>>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGift$ModelProvider$get$1
                public final Observable<? extends WidgetChatListAdapterItemGift.Model> call(StoreGifting.GiftState giftState) {
                    Observable<? extends WidgetChatListAdapterItemGift.Model> invalidGift;
                    Observable<? extends WidgetChatListAdapterItemGift.Model> resolvedGiftModel;
                    if ((giftState instanceof StoreGifting.GiftState.Loading) || (giftState instanceof StoreGifting.GiftState.LoadFailed)) {
                        return new k(WidgetChatListAdapterItemGift.Model.Loading.INSTANCE);
                    }
                    if (giftState instanceof StoreGifting.GiftState.Revoking) {
                        return WidgetChatListAdapterItemGift.ModelProvider.getResolvedGiftModel$default(WidgetChatListAdapterItemGift.ModelProvider.INSTANCE, ((StoreGifting.GiftState.Revoking) giftState).getGift(), false, 2, null);
                    }
                    if (giftState instanceof StoreGifting.GiftState.Resolved) {
                        return WidgetChatListAdapterItemGift.ModelProvider.getResolvedGiftModel$default(WidgetChatListAdapterItemGift.ModelProvider.INSTANCE, ((StoreGifting.GiftState.Resolved) giftState).getGift(), false, 2, null);
                    }
                    if (giftState instanceof StoreGifting.GiftState.Redeeming) {
                        resolvedGiftModel = WidgetChatListAdapterItemGift.ModelProvider.INSTANCE.getResolvedGiftModel(((StoreGifting.GiftState.Redeeming) giftState).getGift(), true);
                        return resolvedGiftModel;
                    } else if (giftState instanceof StoreGifting.GiftState.RedeemedFailed) {
                        return WidgetChatListAdapterItemGift.ModelProvider.getResolvedGiftModel$default(WidgetChatListAdapterItemGift.ModelProvider.INSTANCE, ((StoreGifting.GiftState.RedeemedFailed) giftState).getGift(), false, 2, null);
                    } else {
                        if (giftState instanceof StoreGifting.GiftState.Invalid) {
                            invalidGift = WidgetChatListAdapterItemGift.ModelProvider.INSTANCE.getInvalidGift(GiftEntry.this);
                            return invalidGift;
                        }
                        throw new NoWhenBranchMatchedException();
                    }
                }
            });
            m.checkNotNullExpressionValue(Y, "StoreStream\n            …          }\n            }");
            return Y;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemGift(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_gift, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.barrier;
        Barrier barrier = (Barrier) view.findViewById(R.id.barrier);
        if (barrier != null) {
            i = R.id.buttonsContainer;
            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.buttonsContainer);
            if (frameLayout != null) {
                i = R.id.item_gift_accept_button;
                MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.item_gift_accept_button);
                if (materialButton != null) {
                    i = R.id.item_gift_cannot_claim_button;
                    MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.item_gift_cannot_claim_button);
                    if (materialButton2 != null) {
                        i = R.id.item_gift_details;
                        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.item_gift_details);
                        if (linearLayout != null) {
                            i = R.id.item_gift_expires;
                            TextView textView = (TextView) view.findViewById(R.id.item_gift_expires);
                            if (textView != null) {
                                i = R.id.item_gift_header;
                                TextView textView2 = (TextView) view.findViewById(R.id.item_gift_header);
                                if (textView2 != null) {
                                    i = R.id.item_gift_image;
                                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.item_gift_image);
                                    if (simpleDraweeView != null) {
                                        i = R.id.item_gift_image_background;
                                        SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) view.findViewById(R.id.item_gift_image_background);
                                        if (simpleDraweeView2 != null) {
                                            i = R.id.item_gift_loading_button_placeholder;
                                            View findViewById = view.findViewById(R.id.item_gift_loading_button_placeholder);
                                            if (findViewById != null) {
                                                i = R.id.item_gift_name;
                                                TextView textView3 = (TextView) view.findViewById(R.id.item_gift_name);
                                                if (textView3 != null) {
                                                    i = R.id.item_gift_open_button;
                                                    MaterialButton materialButton3 = (MaterialButton) view.findViewById(R.id.item_gift_open_button);
                                                    if (materialButton3 != null) {
                                                        i = R.id.item_gift_subtext;
                                                        TextView textView4 = (TextView) view.findViewById(R.id.item_gift_subtext);
                                                        if (textView4 != null) {
                                                            i = R.id.item_gift_verify_button;
                                                            MaterialButton materialButton4 = (MaterialButton) view.findViewById(R.id.item_gift_verify_button);
                                                            if (materialButton4 != null) {
                                                                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                                                                WidgetChatListAdapterItemGiftBinding widgetChatListAdapterItemGiftBinding = new WidgetChatListAdapterItemGiftBinding(constraintLayout, barrier, frameLayout, materialButton, materialButton2, linearLayout, textView, textView2, simpleDraweeView, simpleDraweeView2, findViewById, textView3, materialButton3, textView4, materialButton4, constraintLayout);
                                                                m.checkNotNullExpressionValue(widgetChatListAdapterItemGiftBinding, "WidgetChatListAdapterIte…iftBinding.bind(itemView)");
                                                                this.binding = widgetChatListAdapterItemGiftBinding;
                                                                return;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ GiftEntry access$getItem$p(WidgetChatListAdapterItemGift widgetChatListAdapterItemGift) {
        GiftEntry giftEntry = widgetChatListAdapterItemGift.item;
        if (giftEntry == null) {
            m.throwUninitializedPropertyAccessException("item");
        }
        return giftEntry;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureInvalidUI(Model.Invalid invalid) {
        String str;
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        Context context = view.getContext();
        boolean areEqual = m.areEqual(invalid != null ? invalid.getGifterUserId() : null, invalid != null ? Long.valueOf(invalid.getMeId()) : null);
        SimpleDraweeView simpleDraweeView = this.binding.h;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.itemGiftImageBackground");
        simpleDraweeView.setVisibility(4);
        TextView textView = this.binding.f;
        m.checkNotNullExpressionValue(textView, "binding.itemGiftHeader");
        if (areEqual) {
            str = context.getString(R.string.gift_embed_invalid_title_self);
        } else {
            str = context.getString(R.string.gift_embed_invalid_title_other);
        }
        b.a.k.b.o(textView, str, new Object[0], null, 4);
        TextView textView2 = this.binding.j;
        m.checkNotNullExpressionValue(textView2, "binding.itemGiftName");
        b.a.k.b.m(textView2, R.string.gift_embed_invalid, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        this.binding.j.setTextColor(ColorCompat.getColor(context, (int) R.color.status_red_500));
        this.binding.j.setBackgroundResource(0);
        m.checkNotNullExpressionValue(context, "context");
        this.binding.g.setImageResource(DrawableCompat.getThemedDrawableRes(context, (int) R.attr.img_poop, (int) R.drawable.img_poop_dark));
        TextView textView3 = this.binding.l;
        m.checkNotNullExpressionValue(textView3, "binding.itemGiftSubtext");
        textView3.setVisibility(8);
        TextView textView4 = this.binding.e;
        m.checkNotNullExpressionValue(textView4, "binding.itemGiftExpires");
        textView4.setVisibility(8);
        MaterialButton materialButton = this.binding.c;
        m.checkNotNullExpressionValue(materialButton, "binding.itemGiftAcceptButton");
        materialButton.setVisibility(8);
        MaterialButton materialButton2 = this.binding.d;
        m.checkNotNullExpressionValue(materialButton2, "binding.itemGiftCannotClaimButton");
        materialButton2.setVisibility(8);
        View view2 = this.binding.i;
        m.checkNotNullExpressionValue(view2, "binding.itemGiftLoadingButtonPlaceholder");
        view2.setVisibility(8);
        this.itemView.setOnClickListener(null);
    }

    private final void configureLoadingUI() {
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        Context context = view.getContext();
        TextView textView = this.binding.f;
        m.checkNotNullExpressionValue(textView, "binding.itemGiftHeader");
        b.a.k.b.m(textView, R.string.gift_embed_resolving, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        TextView textView2 = this.binding.j;
        m.checkNotNullExpressionValue(textView2, "binding.itemGiftName");
        textView2.setText((CharSequence) null);
        this.binding.j.setTextColor(ColorCompat.getThemedColor(context, (int) R.attr.primary_100));
        this.binding.j.setBackgroundResource(R.drawable.drawable_empty_text_placeholder_dark);
        this.binding.g.setActualImageResource(R.drawable.drawable_empty_text_placeholder_dark);
        SimpleDraweeView simpleDraweeView = this.binding.h;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.itemGiftImageBackground");
        simpleDraweeView.setVisibility(4);
        TextView textView3 = this.binding.l;
        m.checkNotNullExpressionValue(textView3, "binding.itemGiftSubtext");
        textView3.setVisibility(8);
        TextView textView4 = this.binding.e;
        m.checkNotNullExpressionValue(textView4, "binding.itemGiftExpires");
        textView4.setVisibility(8);
        MaterialButton materialButton = this.binding.c;
        m.checkNotNullExpressionValue(materialButton, "binding.itemGiftAcceptButton");
        materialButton.setVisibility(8);
        MaterialButton materialButton2 = this.binding.d;
        m.checkNotNullExpressionValue(materialButton2, "binding.itemGiftCannotClaimButton");
        materialButton2.setVisibility(8);
        View view2 = this.binding.i;
        m.checkNotNullExpressionValue(view2, "binding.itemGiftLoadingButtonPlaceholder");
        view2.setVisibility(0);
        this.itemView.setOnClickListener(null);
    }

    private final void configureResolvedUI(final Model.Resolved resolved) {
        String str;
        String str2;
        MaterialButton materialButton;
        CharSequence b2;
        String str3;
        ModelSku sku;
        Application application;
        String f;
        ModelSku sku2;
        CharSequence b3;
        ModelSku sku3;
        ModelSku sku4;
        Application application2;
        ModelSku sku5;
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        final Context context = view.getContext();
        User user = resolved.getGift().getUser();
        boolean z2 = user != null && user.i() == resolved.getMeUser().getId();
        boolean isClaimedByMe = resolved.getGift().isClaimedByMe();
        boolean z3 = resolved.getGift().getUses() == resolved.getGift().getMaxUses();
        boolean z4 = !resolved.getInLibrary() && !z3 && !resolved.getRedeeming() && resolved.getMeUser().isVerified() && !isClaimedByMe;
        IconUtils iconUtils = IconUtils.INSTANCE;
        ModelStoreListing storeListing = resolved.getGift().getStoreListing();
        long applicationId = (storeListing == null || (sku5 = storeListing.getSku()) == null) ? 0L : sku5.getApplicationId();
        ModelStoreListing storeListing2 = resolved.getGift().getStoreListing();
        String i = (storeListing2 == null || (sku4 = storeListing2.getSku()) == null || (application2 = sku4.getApplication()) == null) ? null : application2.i();
        View view2 = this.itemView;
        m.checkNotNullExpressionValue(view2, "itemView");
        String giftSplashUrl = iconUtils.getGiftSplashUrl(applicationId, i, Integer.valueOf(view2.getWidth()));
        if (resolved.getGift().isAnyNitroGift()) {
            SimpleDraweeView simpleDraweeView = this.binding.h;
            PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
            ModelGift gift = resolved.getGift();
            m.checkNotNullExpressionValue(context, "context");
            simpleDraweeView.setActualImageResource(premiumUtils.getNitroGiftBackground(gift, context));
            SimpleDraweeView simpleDraweeView2 = this.binding.h;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.itemGiftImageBackground");
            simpleDraweeView2.setVisibility(0);
        } else if (giftSplashUrl != null) {
            SimpleDraweeView simpleDraweeView3 = this.binding.h;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.itemGiftImageBackground");
            MGImages.setImage$default(simpleDraweeView3, giftSplashUrl, 0, 0, false, WidgetChatListAdapterItemGift$configureResolvedUI$1.INSTANCE, null, 92, null);
            SimpleDraweeView simpleDraweeView4 = this.binding.h;
            m.checkNotNullExpressionValue(simpleDraweeView4, "binding.itemGiftImageBackground");
            simpleDraweeView4.setVisibility(0);
        } else {
            SimpleDraweeView simpleDraweeView5 = this.binding.h;
            m.checkNotNullExpressionValue(simpleDraweeView5, "binding.itemGiftImageBackground");
            simpleDraweeView5.setVisibility(8);
        }
        TextView textView = this.binding.f;
        m.checkNotNullExpressionValue(textView, "binding.itemGiftHeader");
        if (z2) {
            str = context.getString(R.string.gift_embed_title_self);
        } else {
            str = context.getString(R.string.gift_embed_title);
        }
        b.a.k.b.o(textView, str, new Object[0], null, 4);
        TextView textView2 = this.binding.j;
        m.checkNotNullExpressionValue(textView2, "binding.itemGiftName");
        ModelStoreListing storeListing3 = resolved.getGift().getStoreListing();
        textView2.setText((storeListing3 == null || (sku3 = storeListing3.getSku()) == null) ? null : sku3.getName());
        this.binding.j.setTextColor(ColorCompat.getThemedColor(context, (int) R.attr.primary_100));
        this.binding.j.setBackgroundResource(0);
        TextView textView3 = this.binding.e;
        m.checkNotNullExpressionValue(textView3, "binding.itemGiftExpires");
        textView3.setVisibility(resolved.getGift().getExpiresAt() != null ? 0 : 8);
        if (resolved.getGift().getExpiresAt() != null) {
            TextView textView4 = this.binding.e;
            m.checkNotNullExpressionValue(textView4, "binding.itemGiftExpires");
            m.checkNotNullExpressionValue(context, "context");
            b3 = b.a.k.b.b(context, R.string.gift_embed_expiration, new Object[]{GiftingUtils.INSTANCE.getTimeString(resolved.getGift().getExpiresDiff(ClockFactory.get().currentTimeMillis()), context)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            textView4.setText(b3);
        }
        if (resolved.getGift().isAnyNitroGift()) {
            MGImages mGImages = MGImages.INSTANCE;
            SimpleDraweeView simpleDraweeView6 = this.binding.g;
            m.checkNotNullExpressionValue(simpleDraweeView6, "binding.itemGiftImage");
            MGImages.setImage$default(mGImages, simpleDraweeView6, PremiumUtils.INSTANCE.getNitroGiftIcon(resolved.getGift()), (MGImages.ChangeDetector) null, 4, (Object) null);
        } else {
            ModelStoreListing storeListing4 = resolved.getGift().getStoreListing();
            if (storeListing4 == null || (sku = storeListing4.getSku()) == null || (application = sku.getApplication()) == null || (f = application.f()) == null) {
                str3 = null;
            } else {
                ModelStoreListing storeListing5 = resolved.getGift().getStoreListing();
                str3 = IconUtils.getApplicationIcon$default((storeListing5 == null || (sku2 = storeListing5.getSku()) == null) ? 0L : sku2.getApplicationId(), f, 0, 4, (Object) null);
            }
            this.binding.g.setImageURI(str3);
        }
        if (!resolved.getMeUser().isVerified()) {
            String string = context.getString(R.string.gift_code_auth_help_text_verification_required);
            m.checkNotNullExpressionValue(string, "context.getString(R.stri…xt_verification_required)");
            str2 = t.replace$default(t.replace$default(t.replace$default(string, "(onClick)", "", false, 4, (Object) null), "[", "", false, 4, (Object) null), "]", "", false, 4, (Object) null);
        } else if (isClaimedByMe) {
            str2 = context.getString(R.string.gift_embed_body_claimed_self_mobile);
        } else if (resolved.getInLibrary()) {
            m.checkNotNullExpressionValue(context, "context");
            b2 = b.a.k.b.b(context, R.string.gift_code_auth_help_text_owned, new Object[]{""}, (r4 & 4) != 0 ? b.C0034b.j : null);
            str2 = t.replace$default(t.replace$default(t.replace$default(b2.toString(), "()", "", false, 4, (Object) null), "[", "", false, 4, (Object) null), "]", "", false, 4, (Object) null);
        } else {
            str2 = z3 ? context.getString(R.string.gift_code_auth_help_text_claimed) : null;
        }
        TextView textView5 = this.binding.l;
        m.checkNotNullExpressionValue(textView5, "binding.itemGiftSubtext");
        ViewExtensions.setTextAndVisibilityBy(textView5, str2);
        if (z4) {
            if (GiftStyleKt.hasCustomStyle(resolved.getGift())) {
                materialButton = this.binding.k;
            } else {
                materialButton = this.binding.c;
            }
            materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGift$configureResolvedUI$$inlined$apply$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view3) {
                    c.k.a(resolved.getGift().getCode(), "Embed", WidgetChatListAdapterItemGift.access$getItem$p(WidgetChatListAdapterItemGift.this).getChannelId());
                }
            });
        } else if (!resolved.getMeUser().isVerified()) {
            materialButton = this.binding.m;
            materialButton.setOnClickListener(WidgetChatListAdapterItemGift$configureResolvedUI$visibleButton$2$1.INSTANCE);
        } else {
            materialButton = this.binding.d;
            boolean redeeming = resolved.getRedeeming();
            int i2 = R.string.gift_embed_button_claimed;
            if (redeeming) {
                i2 = R.string.gift_embed_button_claiming;
            } else if (!isClaimedByMe && resolved.getInLibrary()) {
                i2 = R.string.gift_embed_button_owned;
            }
            b.a.k.b.m(materialButton, i2, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        }
        m.checkNotNullExpressionValue(materialButton, "if (canAccept) {\n      i…}\n        )\n      }\n    }");
        FrameLayout frameLayout = this.binding.f2301b;
        m.checkNotNullExpressionValue(frameLayout, "binding.buttonsContainer");
        for (View view3 : ViewGroupKt.getChildren(frameLayout)) {
            if (view3 == materialButton) {
                ((MaterialButton) view3).setVisibility(0);
            } else {
                view3.setVisibility(8);
            }
        }
        if (resolved.getGift().isAnyNitroGift()) {
            this.itemView.setOnClickListener(WidgetChatListAdapterItemGift$configureResolvedUI$3.INSTANCE);
        } else {
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGift$configureResolvedUI$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view4) {
                    String discordStoreURL;
                    UriHandler uriHandler = UriHandler.INSTANCE;
                    Context context2 = context;
                    m.checkNotNullExpressionValue(context2, "context");
                    discordStoreURL = WidgetChatListAdapterItemGift.Companion.getDiscordStoreURL(resolved.getGift().getSkuId());
                    UriHandler.handle$default(uriHandler, context2, discordStoreURL, null, 4, null);
                }
            });
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        if (model instanceof Model.Loading) {
            configureLoadingUI();
        } else if (model instanceof Model.Resolved) {
            configureResolvedUI((Model.Resolved) model);
        } else if (model instanceof Model.Invalid) {
            configureInvalidUI((Model.Invalid) model);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.subscription;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        GiftEntry giftEntry = (GiftEntry) chatListEntry;
        this.item = giftEntry;
        ModelProvider modelProvider = ModelProvider.INSTANCE;
        if (giftEntry == null) {
            m.throwUninitializedPropertyAccessException("item");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(modelProvider.get(giftEntry)), WidgetChatListAdapterItemGift.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetChatListAdapterItemGift$onConfigure$3(this), (r18 & 8) != 0 ? null : new WidgetChatListAdapterItemGift$onConfigure$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterItemGift$onConfigure$1(this));
    }
}
