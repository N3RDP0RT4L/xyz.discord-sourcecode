package com.discord.widgets.chat.list.adapter;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.net.Uri;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.analytics.generated.events.TrackWelcomeCtaClicked;
import com.discord.api.activity.Activity;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.message.MessageReference;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.message.reaction.MessageReactionUpdate;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerType;
import com.discord.api.user.User;
import com.discord.app.AppFragment;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.message.Message;
import com.discord.models.user.CoreUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChat;
import com.discord.stores.StoreEmoji;
import com.discord.stores.StoreMessages;
import com.discord.stores.StorePendingReplies;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.AnalyticsUtils;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.guilds.MemberVerificationUtils;
import com.discord.utilities.guilds.PublicGuildUtils;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stickers.StickerUtils;
import com.discord.widgets.channels.list.WidgetChannelsListItemThreadActions;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.WidgetUrlActions;
import com.discord.widgets.chat.input.AppFlexInputViewModel;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.list.actions.WidgetChatListActions;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemCallMessage;
import com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheet;
import com.discord.widgets.chat.pins.WidgetChannelPinnedMessages;
import com.discord.widgets.guilds.profile.WidgetGuildProfileSheet;
import com.discord.widgets.guilds.profile.WidgetPublicAnnouncementProfileSheet;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventUrlUtils;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.fullscreen.WidgetStartCallSheet;
import d0.o;
import d0.t.h0;
import d0.z.d.k;
import d0.z.d.m;
import j0.l.a.l2;
import j0.l.a.r;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterEventsHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¤\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0002\u009e\u0001B\u008b\u0001\u0012\u0006\u0010u\u001a\u00020n\u0012\b\u0010\u0094\u0001\u001a\u00030\u0093\u0001\u0012\n\b\u0002\u0010\u008a\u0001\u001a\u00030\u0089\u0001\u0012\b\b\u0002\u0010s\u001a\u00020r\u0012\n\b\u0002\u0010\u0081\u0001\u001a\u00030\u0080\u0001\u0012\n\b\u0002\u0010\u0097\u0001\u001a\u00030\u0096\u0001\u0012\n\b\u0002\u0010\u0087\u0001\u001a\u00030\u0086\u0001\u0012\b\b\u0002\u0010x\u001a\u00020w\u0012\n\b\u0002\u0010\u0091\u0001\u001a\u00030\u0090\u0001\u0012\b\b\u0002\u0010~\u001a\u00020}\u0012\n\b\u0002\u0010\u0084\u0001\u001a\u00030\u0083\u0001\u0012\b\b\u0002\u0010{\u001a\u00020z¢\u0006\u0006\b\u009c\u0001\u0010\u009d\u0001JK\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u00022\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0016¢\u0006\u0004\b\f\u0010\rJa\u0010\u0018\u001a\u00020\u000b2\n\u0010\b\u001a\u00060\u0002j\u0002`\u000e2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u000f2\n\u0010\u0005\u001a\u00060\u0002j\u0002`\u00102\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u00022\n\u0010\u0015\u001a\u00060\u0013j\u0002`\u00142\u0006\u0010\u0017\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001e\u0010\u001dJ\u0017\u0010!\u001a\u00020\u000b2\u0006\u0010 \u001a\u00020\u001fH\u0016¢\u0006\u0004\b!\u0010\"J'\u0010$\u001a\u00020\u000b2\n\u0010\u0005\u001a\u00060\u0002j\u0002`\u00102\n\u0010#\u001a\u00060\u0002j\u0002`\u0011H\u0016¢\u0006\u0004\b$\u0010%J\u001f\u0010*\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&2\u0006\u0010)\u001a\u00020(H\u0016¢\u0006\u0004\b*\u0010+J#\u0010,\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u000fH\u0016¢\u0006\u0004\b,\u0010-J'\u00100\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&2\u0006\u0010/\u001a\u00020.2\u0006\u0010)\u001a\u00020(H\u0016¢\u0006\u0004\b0\u00101J\u0017\u00103\u001a\u00020\u000b2\u0006\u00102\u001a\u00020\tH\u0016¢\u0006\u0004\b3\u00104J\u0017\u00105\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&H\u0016¢\u0006\u0004\b5\u00106J#\u00107\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u000fH\u0016¢\u0006\u0004\b7\u0010-J'\u00108\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&2\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u000fH\u0016¢\u0006\u0004\b8\u00109J\u0017\u0010:\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&H\u0016¢\u0006\u0004\b:\u00106J?\u0010?\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010;\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010=\u001a\u00020<2\u0006\u0010>\u001a\u00020(H\u0016¢\u0006\u0004\b?\u0010@J;\u0010A\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u000f2\n\u0010\u0005\u001a\u00060\u0002j\u0002`\u00102\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00112\u0006\u0010=\u001a\u00020<H\u0016¢\u0006\u0004\bA\u0010BJ'\u0010D\u001a\u00020\u000b2\u0006\u0010C\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0002H\u0016¢\u0006\u0004\bD\u0010EJ?\u0010G\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u000f2\n\u0010;\u001a\u00060\u0002j\u0002`F2\n\u0010\u0005\u001a\u00060\u0002j\u0002`\u00102\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0011H\u0016¢\u0006\u0004\bG\u0010HJ\u001f\u0010L\u001a\u00020(2\u0006\u0010J\u001a\u00020I2\u0006\u0010K\u001a\u00020\tH\u0016¢\u0006\u0004\bL\u0010MJK\u0010U\u001a\u00020\u000b2\n\u0010N\u001a\u00060\u0002j\u0002`F2\n\u0010\u0005\u001a\u00060\u0002j\u0002`\u00102\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00112\u0006\u0010P\u001a\u00020O2\u0006\u0010R\u001a\u00020Q2\u0006\u0010T\u001a\u00020SH\u0016¢\u0006\u0004\bU\u0010VJ\u000f\u0010W\u001a\u00020\u000bH\u0016¢\u0006\u0004\bW\u0010XJ#\u0010\\\u001a\u00020\u000b2\n\u0010Y\u001a\u00060\u0002j\u0002`\u00102\u0006\u0010[\u001a\u00020ZH\u0016¢\u0006\u0004\b\\\u0010]J\u001f\u0010`\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&2\u0006\u0010_\u001a\u00020^H\u0016¢\u0006\u0004\b`\u0010aJ+\u0010d\u001a\u00020\u000b2\n\u0010\u0005\u001a\u00060\u0002j\u0002`\u00102\u0006\u0010b\u001a\u00020\u00132\u0006\u0010_\u001a\u00020cH\u0016¢\u0006\u0004\bd\u0010eJ\u0017\u0010f\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&H\u0016¢\u0006\u0004\bf\u00106J'\u0010g\u001a\u00020\u000b2\u0006\u0010'\u001a\u00020&2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010_\u001a\u00020^H\u0016¢\u0006\u0004\bg\u0010hJ3\u0010p\u001a\u00020\u000b2\u0006\u0010j\u001a\u00020i2\f\u0010m\u001a\b\u0012\u0004\u0012\u00020l0k2\f\u0010o\u001a\b\u0012\u0004\u0012\u00020n0kH\u0016¢\u0006\u0004\bp\u0010qR\u0016\u0010s\u001a\u00020r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bs\u0010tR\u0016\u0010u\u001a\u00020n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bu\u0010vR\u0016\u0010x\u001a\u00020w8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bx\u0010yR\u0016\u0010{\u001a\u00020z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b{\u0010|R\u0016\u0010~\u001a\u00020}8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b~\u0010\u007fR\u001a\u0010\u0081\u0001\u001a\u00030\u0080\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0081\u0001\u0010\u0082\u0001R\u001a\u0010\u0084\u0001\u001a\u00030\u0083\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0084\u0001\u0010\u0085\u0001R\u001a\u0010\u0087\u0001\u001a\u00030\u0086\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0087\u0001\u0010\u0088\u0001R\u001a\u0010\u008a\u0001\u001a\u00030\u0089\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u008a\u0001\u0010\u008b\u0001R\u001a\u0010\u008f\u0001\u001a\u00030\u008c\u00018B@\u0002X\u0082\u0004¢\u0006\b\u001a\u0006\b\u008d\u0001\u0010\u008e\u0001R\u001a\u0010\u0091\u0001\u001a\u00030\u0090\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0091\u0001\u0010\u0092\u0001R\u001a\u0010\u0094\u0001\u001a\u00030\u0093\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0094\u0001\u0010\u0095\u0001R\u001a\u0010\u0097\u0001\u001a\u00030\u0096\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0097\u0001\u0010\u0098\u0001R\u0019\u0010\u009b\u0001\u001a\u00020l8B@\u0002X\u0082\u0004¢\u0006\b\u001a\u0006\b\u0099\u0001\u0010\u009a\u0001¨\u0006\u009f\u0001"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "", "interactionId", "guildId", "channelId", "messageId", "interactionUserId", "applicationId", "", "messageNonce", "", "onCommandClicked", "(JLjava/lang/Long;JJJJLjava/lang/String;)V", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/primitives/GuildId;", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/primitives/MessageId;", "messageFlags", "", "Lcom/discord/widgets/botuikit/ComponentIndex;", "componentIndex", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "componentSendData", "onBotUiComponentClicked", "(JLjava/lang/Long;JJLjava/lang/Long;ILcom/discord/restapi/RestAPIParams$ComponentInteractionData;)V", "Lcom/discord/api/channel/Channel;", "channel", "onThreadClicked", "(Lcom/discord/api/channel/Channel;)V", "onThreadLongClicked", "Lcom/discord/stores/StoreChat$InteractionState;", "interactionState", "onInteractionStateUpdated", "(Lcom/discord/stores/StoreChat$InteractionState;)V", "oldestMessageId", "onOldestMessageId", "(JJ)V", "Lcom/discord/models/message/Message;", "message", "", "isThreadStarterMessage", "onMessageClicked", "(Lcom/discord/models/message/Message;Z)V", "onMessageAuthorAvatarClicked", "(Lcom/discord/models/message/Message;J)V", "", "formattedMessage", "onMessageLongClicked", "(Lcom/discord/models/message/Message;Ljava/lang/CharSequence;Z)V", "url", "onUrlLongClicked", "(Ljava/lang/String;)V", "onOpenPinsClicked", "(Lcom/discord/models/message/Message;)V", "onMessageAuthorNameClicked", "onMessageAuthorLongClicked", "(Lcom/discord/models/message/Message;Ljava/lang/Long;)V", "onMessageBlockedGroupClicked", "myUserId", "Lcom/discord/api/message/reaction/MessageReaction;", "reaction", "canAddReactions", "onReactionClicked", "(JJJJLcom/discord/api/message/reaction/MessageReaction;Z)V", "onReactionLongClicked", "(JJJLcom/discord/api/message/reaction/MessageReaction;)V", "userId", "onUserMentionClicked", "(JJJ)V", "Lcom/discord/primitives/UserId;", "onQuickAddReactionClicked", "(JJJJ)V", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "fileName", "onQuickDownloadClicked", "(Landroid/net/Uri;Ljava/lang/String;)Z", "authorId", "Lcom/discord/api/message/activity/MessageActivityType;", "actionType", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/api/application/Application;", "application", "onUserActivityAction", "(JJJLcom/discord/api/message/activity/MessageActivityType;Lcom/discord/api/activity/Activity;Lcom/discord/api/application/Application;)V", "onListClicked", "()V", "voiceChannelId", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;", "callStatus", "onCallMessageClicked", "(JLcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;)V", "Lcom/discord/api/sticker/BaseSticker;", "sticker", "onStickerClicked", "(Lcom/discord/models/message/Message;Lcom/discord/api/sticker/BaseSticker;)V", "channelType", "Lcom/discord/api/sticker/Sticker;", "onSendGreetMessageClicked", "(JILcom/discord/api/sticker/Sticker;)V", "onDismissClicked", "onWelcomeCtaClicked", "(Lcom/discord/models/message/Message;Lcom/discord/api/channel/Channel;Lcom/discord/api/sticker/BaseSticker;)V", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "Ljava/lang/ref/WeakReference;", "Landroid/content/Context;", "weakContext", "Lcom/discord/app/AppFragment;", "weakFragment", "onShareButtonClick", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V", "Lcom/discord/stores/StoreMessages;", "storeMessages", "Lcom/discord/stores/StoreMessages;", "host", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/chat/MessageManager;", "messageManager", "Lcom/discord/widgets/chat/MessageManager;", "Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "analyticsTracker", "Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler;", "userReactionHandler", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler;", "Lcom/discord/stores/StorePendingReplies;", "storePendingReplies", "Lcom/discord/stores/StorePendingReplies;", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreEmoji;", "storeEmoji", "Lcom/discord/stores/StoreEmoji;", "Lcom/discord/stores/StoreChat;", "storeChat", "Lcom/discord/stores/StoreChat;", "Landroidx/fragment/app/FragmentManager;", "getFragmentManager", "()Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/utilities/channel/ChannelSelector;", "channelSelector", "Lcom/discord/utilities/channel/ChannelSelector;", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "getContext", "()Landroid/content/Context;", "context", HookHelper.constructorName, "(Lcom/discord/app/AppFragment;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Lcom/discord/stores/StoreChat;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StorePendingReplies;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreEmoji;Lcom/discord/widgets/chat/MessageManager;Lcom/discord/utilities/channel/ChannelSelector;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler;Lcom/discord/stores/StoreChannels;Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;)V", "UserReactionHandler", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterEventsHandler implements WidgetChatListAdapter.EventHandler {
    private final AnalyticsUtils.Tracker analyticsTracker;
    private final ChannelSelector channelSelector;
    private final StoreChannels channelStore;
    private final AppFlexInputViewModel flexInputViewModel;
    private final AppFragment host;
    private final MessageManager messageManager;
    private final StoreChat storeChat;
    private final StoreEmoji storeEmoji;
    private final StoreMessages storeMessages;
    private final StorePendingReplies storePendingReplies;
    private final StoreUser storeUser;
    private final UserReactionHandler userReactionHandler;

    /* compiled from: WidgetChatListAdapterEventsHandler.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 (2\u00020\u0001:\u0002()B\u001f\u0012\u0006\u0010!\u001a\u00020 \u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\u0006\u0010$\u001a\u00020#¢\u0006\u0004\b&\u0010'J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J-\u0010\u000e\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\n\u0010\r\u001a\u00060\tj\u0002`\f¢\u0006\u0004\b\u000e\u0010\u000fJ1\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\t2\n\u0010\r\u001a\u00060\tj\u0002`\f2\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0013\u0010\u0014R\"\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\"\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00040\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\"\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00040\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010\u001eR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%¨\u0006*"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler;", "", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler$UpdateRequest;", "updateRequest", "", "requestReactionUpdate", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler$UpdateRequest;)V", "Lcom/discord/models/domain/emoji/Emoji;", "emoji", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "messageId", "addNewReaction", "(Lcom/discord/models/domain/emoji/Emoji;JJ)V", "userId", "Lcom/discord/api/message/reaction/MessageReaction;", "reaction", "toggleReaction", "(JJJLcom/discord/api/message/reaction/MessageReaction;)V", "Lrx/subjects/Subject;", "requestStream", "Lrx/subjects/Subject;", "Lcom/discord/stores/StoreMessages;", "storeMessages", "Lcom/discord/stores/StoreMessages;", "Lkotlin/Function1;", "Lcom/discord/api/message/reaction/MessageReactionUpdate;", "commitReactionAdd", "Lkotlin/jvm/functions/Function1;", "commitReactionRemove", "Lcom/discord/app/AppFragment;", "host", "Lcom/discord/app/AppFragment;", "Lcom/discord/stores/StoreEmoji;", "storeEmoji", "Lcom/discord/stores/StoreEmoji;", HookHelper.constructorName, "(Lcom/discord/app/AppFragment;Lcom/discord/stores/StoreMessages;Lcom/discord/stores/StoreEmoji;)V", "Companion", "UpdateRequest", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UserReactionHandler {
        public static final Companion Companion = new Companion(null);
        private static final long REQUEST_RATE_LIMIT_MILLIS = 250;
        private final Function1<MessageReactionUpdate, Unit> commitReactionAdd = new WidgetChatListAdapterEventsHandler$UserReactionHandler$commitReactionAdd$1(this);
        private final Function1<MessageReactionUpdate, Unit> commitReactionRemove = new WidgetChatListAdapterEventsHandler$UserReactionHandler$commitReactionRemove$1(this);
        private final AppFragment host;
        private final Subject<UpdateRequest, UpdateRequest> requestStream;
        private final StoreEmoji storeEmoji;
        private final StoreMessages storeMessages;

        /* compiled from: WidgetChatListAdapterEventsHandler.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler$UpdateRequest;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler$UpdateRequest;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler$UserReactionHandler$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<UpdateRequest, Unit> {
            public AnonymousClass1(UserReactionHandler userReactionHandler) {
                super(1, userReactionHandler, UserReactionHandler.class, "requestReactionUpdate", "requestReactionUpdate(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler$UpdateRequest;)V", 0);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(UpdateRequest updateRequest) {
                invoke2(updateRequest);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(UpdateRequest updateRequest) {
                m.checkNotNullParameter(updateRequest, "p1");
                ((UserReactionHandler) this.receiver).requestReactionUpdate(updateRequest);
            }
        }

        /* compiled from: WidgetChatListAdapterEventsHandler.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler$Companion;", "", "", "REQUEST_RATE_LIMIT_MILLIS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetChatListAdapterEventsHandler.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0082\b\u0018\u00002\u00020\u0001B3\u0012\n\u0010\r\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u000e\u001a\u00060\u0002j\u0002`\u0006\u0012\n\u0010\u000f\u001a\u00060\u0002j\u0002`\b\u0012\u0006\u0010\u0010\u001a\u00020\n¢\u0006\u0004\b#\u0010$J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0014\u0010\t\u001a\u00060\u0002j\u0002`\bHÆ\u0003¢\u0006\u0004\b\t\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJD\u0010\u0011\u001a\u00020\u00002\f\b\u0002\u0010\r\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u000e\u001a\u00060\u0002j\u0002`\u00062\f\b\u0002\u0010\u000f\u001a\u00060\u0002j\u0002`\b2\b\b\u0002\u0010\u0010\u001a\u00020\nHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u001d\u0010\u000f\u001a\u00060\u0002j\u0002`\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0005R\u001d\u0010\u000e\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001f\u0010\u0005R\u0019\u0010\u0010\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\fR\u001d\u0010\r\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\"\u0010\u0005¨\u0006%"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler$UpdateRequest;", "", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "Lcom/discord/primitives/MessageId;", "component3", "Lcom/discord/api/message/reaction/MessageReaction;", "component4", "()Lcom/discord/api/message/reaction/MessageReaction;", "userId", "channelId", "messageId", "reaction", "copy", "(JJJLcom/discord/api/message/reaction/MessageReaction;)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterEventsHandler$UserReactionHandler$UpdateRequest;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getMessageId", "getChannelId", "Lcom/discord/api/message/reaction/MessageReaction;", "getReaction", "getUserId", HookHelper.constructorName, "(JJJLcom/discord/api/message/reaction/MessageReaction;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UpdateRequest {
            private final long channelId;
            private final long messageId;
            private final MessageReaction reaction;
            private final long userId;

            public UpdateRequest(long j, long j2, long j3, MessageReaction messageReaction) {
                m.checkNotNullParameter(messageReaction, "reaction");
                this.userId = j;
                this.channelId = j2;
                this.messageId = j3;
                this.reaction = messageReaction;
            }

            public final long component1() {
                return this.userId;
            }

            public final long component2() {
                return this.channelId;
            }

            public final long component3() {
                return this.messageId;
            }

            public final MessageReaction component4() {
                return this.reaction;
            }

            public final UpdateRequest copy(long j, long j2, long j3, MessageReaction messageReaction) {
                m.checkNotNullParameter(messageReaction, "reaction");
                return new UpdateRequest(j, j2, j3, messageReaction);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof UpdateRequest)) {
                    return false;
                }
                UpdateRequest updateRequest = (UpdateRequest) obj;
                return this.userId == updateRequest.userId && this.channelId == updateRequest.channelId && this.messageId == updateRequest.messageId && m.areEqual(this.reaction, updateRequest.reaction);
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final long getMessageId() {
                return this.messageId;
            }

            public final MessageReaction getReaction() {
                return this.reaction;
            }

            public final long getUserId() {
                return this.userId;
            }

            public int hashCode() {
                int a = (b.a(this.messageId) + ((b.a(this.channelId) + (b.a(this.userId) * 31)) * 31)) * 31;
                MessageReaction messageReaction = this.reaction;
                return a + (messageReaction != null ? messageReaction.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("UpdateRequest(userId=");
                R.append(this.userId);
                R.append(", channelId=");
                R.append(this.channelId);
                R.append(", messageId=");
                R.append(this.messageId);
                R.append(", reaction=");
                R.append(this.reaction);
                R.append(")");
                return R.toString();
            }
        }

        public UserReactionHandler(AppFragment appFragment, StoreMessages storeMessages, StoreEmoji storeEmoji) {
            m.checkNotNullParameter(appFragment, "host");
            m.checkNotNullParameter(storeMessages, "storeMessages");
            m.checkNotNullParameter(storeEmoji, "storeEmoji");
            this.host = appFragment;
            this.storeMessages = storeMessages;
            this.storeEmoji = storeEmoji;
            PublishSubject k0 = PublishSubject.k0();
            m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
            this.requestStream = k0;
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            Objects.requireNonNull(k0);
            Observable h02 = Observable.h0(new r(k0.j, new l2(REQUEST_RATE_LIMIT_MILLIS, timeUnit, j0.p.a.a())));
            m.checkNotNullExpressionValue(h02, "requestStream\n          …S, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(h02, appFragment.getClass(), (r18 & 2) != 0 ? null : appFragment.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void requestReactionUpdate(UpdateRequest updateRequest) {
            String str;
            Observable<Void> observable;
            Function1<MessageReactionUpdate, Unit> function1;
            Function1<MessageReactionUpdate, Unit> function12;
            long component1 = updateRequest.component1();
            long component2 = updateRequest.component2();
            long component3 = updateRequest.component3();
            MessageReaction component4 = updateRequest.component4();
            MessageReactionUpdate messageReactionUpdate = new MessageReactionUpdate(component1, component2, component3, component4.b());
            if (component4.b().e()) {
                str = component4.b().d() + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + component4.b().b();
            } else {
                str = component4.b().d();
                if (str == null) {
                    str = "";
                }
            }
            if (component4.c()) {
                observable = RestAPI.Companion.getApi().removeSelfReaction(component2, component3, str);
            } else {
                observable = RestAPI.Companion.getApi().addReaction(component2, component3, str);
            }
            if (component4.c()) {
                function1 = this.commitReactionRemove;
            } else {
                function1 = this.commitReactionAdd;
            }
            if (component4.c()) {
                function12 = this.commitReactionAdd;
            } else {
                function12 = this.commitReactionRemove;
            }
            function1.invoke(messageReactionUpdate);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(observable, false, 1, null), this.host, null, 2, null), this.host.getClass(), (r18 & 2) != 0 ? null : this.host.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetChatListAdapterEventsHandler$UserReactionHandler$requestReactionUpdate$2(function12, messageReactionUpdate), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterEventsHandler$UserReactionHandler$requestReactionUpdate$1(this, component4));
        }

        public final void addNewReaction(Emoji emoji, long j, long j2) {
            m.checkNotNullParameter(emoji, "emoji");
            RestAPI api = RestAPI.Companion.getApi();
            String reactionKey = emoji.getReactionKey();
            m.checkNotNullExpressionValue(reactionKey, "emoji.reactionKey");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(api.addReaction(j, j2, reactionKey), false, 1, null), this.host, null, 2, null), this.host.getClass(), (r18 & 2) != 0 ? null : this.host.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetChatListAdapterEventsHandler$UserReactionHandler$addNewReaction$1.INSTANCE);
        }

        public final void toggleReaction(long j, long j2, long j3, MessageReaction messageReaction) {
            m.checkNotNullParameter(messageReaction, "reaction");
            this.requestStream.onNext(new UpdateRequest(j, j2, j3, messageReaction));
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            WidgetChatListAdapterItemCallMessage.CallStatus.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[WidgetChatListAdapterItemCallMessage.CallStatus.ACTIVE_JOINED.ordinal()] = 1;
            iArr[WidgetChatListAdapterItemCallMessage.CallStatus.ACTIVE_UNJOINED.ordinal()] = 2;
            StickerType.values();
            int[] iArr2 = new int[3];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[StickerType.STANDARD.ordinal()] = 1;
            iArr2[StickerType.GUILD.ordinal()] = 2;
        }
    }

    public WidgetChatListAdapterEventsHandler(AppFragment appFragment, AppFlexInputViewModel appFlexInputViewModel, StoreChat storeChat, StoreMessages storeMessages, StorePendingReplies storePendingReplies, StoreUser storeUser, StoreEmoji storeEmoji, MessageManager messageManager, ChannelSelector channelSelector, UserReactionHandler userReactionHandler, StoreChannels storeChannels, AnalyticsUtils.Tracker tracker) {
        m.checkNotNullParameter(appFragment, "host");
        m.checkNotNullParameter(appFlexInputViewModel, "flexInputViewModel");
        m.checkNotNullParameter(storeChat, "storeChat");
        m.checkNotNullParameter(storeMessages, "storeMessages");
        m.checkNotNullParameter(storePendingReplies, "storePendingReplies");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(storeEmoji, "storeEmoji");
        m.checkNotNullParameter(messageManager, "messageManager");
        m.checkNotNullParameter(channelSelector, "channelSelector");
        m.checkNotNullParameter(userReactionHandler, "userReactionHandler");
        m.checkNotNullParameter(storeChannels, "channelStore");
        m.checkNotNullParameter(tracker, "analyticsTracker");
        this.host = appFragment;
        this.flexInputViewModel = appFlexInputViewModel;
        this.storeChat = storeChat;
        this.storeMessages = storeMessages;
        this.storePendingReplies = storePendingReplies;
        this.storeUser = storeUser;
        this.storeEmoji = storeEmoji;
        this.messageManager = messageManager;
        this.channelSelector = channelSelector;
        this.userReactionHandler = userReactionHandler;
        this.channelStore = storeChannels;
        this.analyticsTracker = tracker;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Context getContext() {
        return this.host.requireContext();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final FragmentManager getFragmentManager() {
        FragmentManager parentFragmentManager = this.host.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "host.parentFragmentManager");
        return parentFragmentManager;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onBotUiComponentClicked(long j, Long l, long j2, long j3, Long l2, int i, RestAPIParams.ComponentInteractionData componentInteractionData) {
        m.checkNotNullParameter(componentInteractionData, "componentSendData");
        StoreStream.Companion.getInteractions().sendComponentInteraction(j, l, j2, j3, i, componentInteractionData, l2);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onCallMessageClicked(long j, WidgetChatListAdapterItemCallMessage.CallStatus callStatus) {
        m.checkNotNullParameter(callStatus, "callStatus");
        int ordinal = callStatus.ordinal();
        if (ordinal == 0) {
            d.S1(this.host, null, new WidgetChatListAdapterEventsHandler$onCallMessageClicked$1(this, j), 1, null);
        } else if (ordinal != 1) {
            WidgetStartCallSheet.Companion.show(j, getFragmentManager());
        } else {
            WidgetCallFullscreen.Companion.launch$default(WidgetCallFullscreen.Companion, getContext(), j, false, null, null, 28, null);
        }
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onCommandClicked(long j, Long l, long j2, long j3, long j4, long j5, String str) {
        WidgetApplicationCommandBottomSheet.Companion companion = WidgetApplicationCommandBottomSheet.Companion;
        FragmentManager childFragmentManager = this.host.getChildFragmentManager();
        m.checkNotNullExpressionValue(childFragmentManager, "host.childFragmentManager");
        companion.show(childFragmentManager, j, j3, j2, l, j4, j5, str);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onDismissClicked(Message message) {
        m.checkNotNullParameter(message, "message");
        this.storeMessages.deleteMessage(message);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onInteractionStateUpdated(StoreChat.InteractionState interactionState) {
        m.checkNotNullParameter(interactionState, "interactionState");
        this.storeChat.setInteractionState(interactionState);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onListClicked() {
        AppFragment.hideKeyboard$default(this.host, null, 1, null);
        this.flexInputViewModel.hideExpressionTray();
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onMessageAuthorAvatarClicked(Message message, long j) {
        m.checkNotNullParameter(message, "message");
        if (PublicGuildUtils.INSTANCE.isPublicGuildSystemMessage(message) || message.isCrosspost()) {
            onMessageAuthorNameClicked(message, j);
            return;
        }
        User author = message.getAuthor();
        if (author != null) {
            WidgetUserSheet.Companion.show$default(WidgetUserSheet.Companion, author.i(), Long.valueOf(message.getChannelId()), getFragmentManager(), Long.valueOf(j), null, null, null, 112, null);
        }
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onMessageAuthorLongClicked(Message message, Long l) {
        m.checkNotNullParameter(message, "message");
        if (!message.isWebhook()) {
            User author = message.getAuthor();
            if (author != null) {
                WidgetUserSheet.Companion.show$default(WidgetUserSheet.Companion, author.i(), Long.valueOf(message.getChannelId()), getFragmentManager(), l, null, null, null, 112, null);
                return;
            }
            return;
        }
        b.a.d.m.g(getContext(), R.string.user_profile_failure_to_open_message, 0, null, 8);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onMessageAuthorNameClicked(Message message, long j) {
        m.checkNotNullParameter(message, "message");
        if (PublicGuildUtils.INSTANCE.isPublicGuildSystemMessage(message)) {
            WidgetPublicAnnouncementProfileSheet.Companion.show(getFragmentManager());
        } else if (!message.isCrosspost() || message.getMessageReference() == null) {
            StoreChat storeChat = this.storeChat;
            User author = message.getAuthor();
            m.checkNotNull(author);
            storeChat.appendMention(new CoreUser(author), j);
        } else {
            MessageReference messageReference = message.getMessageReference();
            Long l = null;
            Long a = messageReference != null ? messageReference.a() : null;
            MessageReference messageReference2 = message.getMessageReference();
            if (messageReference2 != null) {
                l = messageReference2.b();
            }
            if (l != null && a != null) {
                WidgetGuildProfileSheet.Companion.show$default(WidgetGuildProfileSheet.Companion, getFragmentManager(), false, l.longValue(), a.longValue(), false, 16, null);
            }
        }
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onMessageBlockedGroupClicked(Message message) {
        m.checkNotNullParameter(message, "message");
        this.storeChat.toggleBlockedMessageGroup(message.getId());
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onMessageClicked(Message message, boolean z2) {
        m.checkNotNullParameter(message, "message");
        if (z2) {
            StoreStream.Companion.getMessagesLoader().jumpToMessage(message.getChannelId(), message.getId());
        }
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onMessageLongClicked(Message message, CharSequence charSequence, boolean z2) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(charSequence, "formattedMessage");
        if (!z2) {
            WidgetChatListActions.Companion.showForChat(getFragmentManager(), message.getChannelId(), message.getId(), charSequence);
        }
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onOldestMessageId(long j, long j2) {
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onOpenPinsClicked(Message message) {
        m.checkNotNullParameter(message, "message");
        WidgetChannelPinnedMessages.Companion.show(getContext(), message.getChannelId());
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onQuickAddReactionClicked(long j, long j2, long j3, long j4) {
        MemberVerificationUtils.maybeShowVerificationGate$default(MemberVerificationUtils.INSTANCE, getContext(), getFragmentManager(), j, Traits.Location.Section.EMOJI_PICKER_POPOUT, null, null, new WidgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1(this, j3, j4), 32, null);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public boolean onQuickDownloadClicked(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "fileName");
        this.host.requestMediaDownload(new WidgetChatListAdapterEventsHandler$onQuickDownloadClicked$1(this, uri, str, new WeakReference(getContext())));
        return true;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onReactionClicked(long j, long j2, long j3, long j4, MessageReaction messageReaction, boolean z2) {
        m.checkNotNullParameter(messageReaction, "reaction");
        if (z2) {
            MemberVerificationUtils.maybeShowVerificationGate$default(MemberVerificationUtils.INSTANCE, getContext(), getFragmentManager(), j, Traits.Location.Section.EMOJI_PICKER_POPOUT, null, null, new WidgetChatListAdapterEventsHandler$onReactionClicked$1(this, j2, j3, j4, messageReaction), 32, null);
        } else {
            b.a.d.m.g(getContext(), R.string.archived_thread_reactions_disabled_toast, 0, null, 8);
        }
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onReactionLongClicked(long j, long j2, long j3, MessageReaction messageReaction) {
        m.checkNotNullParameter(messageReaction, "reaction");
        MemberVerificationUtils.maybeShowVerificationGate$default(MemberVerificationUtils.INSTANCE, getContext(), getFragmentManager(), j, Traits.Location.Section.EMOJI_PICKER_POPOUT, null, null, new WidgetChatListAdapterEventsHandler$onReactionLongClicked$1(this, j2, j3, messageReaction), 32, null);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onSendGreetMessageClicked(long j, int i, Sticker sticker) {
        m.checkNotNullParameter(sticker, "sticker");
        AnalyticsTracker.INSTANCE.getTracker().track("dm_empty_action", h0.mutableMapOf(o.to(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID, Long.valueOf(j)), o.to("channel_type", Integer.valueOf(i)), o.to("source", "Wave"), o.to("type", "Send wave")));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().sendGreetMessage(j, new RestAPIParams.GreetMessage(d0.t.m.listOf(Long.valueOf(sticker.getId())))), false, 1, null), this.host, null, 2, null), this.host.getClass(), (r18 & 2) != 0 ? null : this.host.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, WidgetChatListAdapterEventsHandler$onSendGreetMessageClicked$1.INSTANCE);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onShareButtonClick(GuildScheduledEvent guildScheduledEvent, WeakReference<Context> weakReference, WeakReference<AppFragment> weakReference2) {
        Channel channel;
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        m.checkNotNullParameter(weakReference, "weakContext");
        m.checkNotNullParameter(weakReference2, "weakFragment");
        long h = guildScheduledEvent.h();
        long i = guildScheduledEvent.i();
        Long b2 = guildScheduledEvent.b();
        boolean canShareEvent$default = GuildScheduledEventUtilities.Companion.canShareEvent$default(GuildScheduledEventUtilities.Companion, b2, h, null, null, null, null, 60, null);
        if (b2 != null) {
            channel = this.channelStore.getChannel(b2.longValue());
        } else {
            channel = null;
        }
        if (canShareEvent$default) {
            Observable<Channel> y2 = StoreStream.Companion.getChannels().observeDefaultChannel(h).y();
            m.checkNotNullExpressionValue(y2, "StoreStream.getChannels(…ildId)\n          .first()");
            ObservableExtensionsKt.appSubscribe(y2, WidgetChatListAdapterEventsHandler.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterEventsHandler$onShareButtonClick$1(weakReference2, channel, i));
            return;
        }
        Context context = weakReference.get();
        if (context != null) {
            CharSequence eventDetailsUrl = GuildScheduledEventUrlUtils.INSTANCE.getEventDetailsUrl(h, i);
            m.checkNotNullExpressionValue(context, "context");
            b.a.d.m.c(context, eventDetailsUrl, 0, 4);
        }
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onStickerClicked(Message message, BaseSticker baseSticker) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(baseSticker, "sticker");
        WidgetChatListAdapterEventsHandler$onStickerClicked$1 widgetChatListAdapterEventsHandler$onStickerClicked$1 = new WidgetChatListAdapterEventsHandler$onStickerClicked$1(this, baseSticker, message);
        this.flexInputViewModel.hideKeyboard();
        if (baseSticker instanceof Sticker) {
            widgetChatListAdapterEventsHandler$onStickerClicked$1.invoke2(baseSticker);
            return;
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StickerUtils.INSTANCE.getGuildOrStandardSticker(baseSticker.d(), true), this.host, null, 2, null), WidgetChatListAdapterEventsHandler.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetChatListAdapterEventsHandler$onStickerClicked$2(this, baseSticker), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterEventsHandler$onStickerClicked$3(widgetChatListAdapterEventsHandler$onStickerClicked$1));
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onThreadClicked(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        this.channelSelector.selectChannel(channel, Long.valueOf(channel.r()), SelectedChannelAnalyticsLocation.EMBED);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onThreadLongClicked(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        WidgetChannelsListItemThreadActions.Companion.show(getFragmentManager(), channel.h());
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onUrlLongClicked(String str) {
        m.checkNotNullParameter(str, "url");
        WidgetUrlActions.Companion.launch(getFragmentManager(), str);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onUserActivityAction(long j, long j2, long j3, MessageActivityType messageActivityType, Activity activity, Application application) {
        m.checkNotNullParameter(messageActivityType, "actionType");
        m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        m.checkNotNullParameter(application, "application");
        Long a = activity.a();
        String k = activity.k();
        if (a != null && k != null && messageActivityType == MessageActivityType.JOIN) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().userActivityActionJoin(j, a.longValue(), k, Long.valueOf(j2), Long.valueOf(j3)), false, 1, null), this.host, null, 2, null), WidgetChatListAdapterEventsHandler.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterEventsHandler$onUserActivityAction$1(this, application));
        }
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onUserMentionClicked(long j, long j2, long j3) {
        WidgetUserSheet.Companion.show$default(WidgetUserSheet.Companion, j, Long.valueOf(j2), getFragmentManager(), Long.valueOf(j3), null, null, null, 112, null);
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.EventHandler
    public void onWelcomeCtaClicked(Message message, Channel channel, BaseSticker baseSticker) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(baseSticker, "sticker");
        this.storePendingReplies.onCreatePendingReply(channel, message, true, true);
        MessageManager.sendMessage$default(this.messageManager, null, null, null, null, d0.t.m.listOf(baseSticker), false, null, null, null, 495, null);
        AnalyticsUtils.Tracker tracker = this.analyticsTracker;
        Boolean bool = Boolean.TRUE;
        String valueOf = String.valueOf(baseSticker.d());
        User author = message.getAuthor();
        tracker.track(new TrackWelcomeCtaClicked(bool, valueOf, author != null ? Long.valueOf(author.i()) : null, Long.valueOf(this.storeUser.getMe().getId())));
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetChatListAdapterEventsHandler(com.discord.app.AppFragment r23, com.discord.widgets.chat.input.AppFlexInputViewModel r24, com.discord.stores.StoreChat r25, com.discord.stores.StoreMessages r26, com.discord.stores.StorePendingReplies r27, com.discord.stores.StoreUser r28, com.discord.stores.StoreEmoji r29, com.discord.widgets.chat.MessageManager r30, com.discord.utilities.channel.ChannelSelector r31, com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler.UserReactionHandler r32, com.discord.stores.StoreChannels r33, com.discord.utilities.analytics.AnalyticsUtils.Tracker r34, int r35, kotlin.jvm.internal.DefaultConstructorMarker r36) {
        /*
            r22 = this;
            r0 = r35
            r1 = r0 & 4
            if (r1 == 0) goto Le
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChat r1 = r1.getChat()
            r5 = r1
            goto L10
        Le:
            r5 = r25
        L10:
            r1 = r0 & 8
            if (r1 == 0) goto L1c
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreMessages r1 = r1.getMessages()
            r6 = r1
            goto L1e
        L1c:
            r6 = r26
        L1e:
            r1 = r0 & 16
            if (r1 == 0) goto L2a
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StorePendingReplies r1 = r1.getPendingReplies()
            r7 = r1
            goto L2c
        L2a:
            r7 = r27
        L2c:
            r1 = r0 & 32
            if (r1 == 0) goto L38
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r1 = r1.getUsers()
            r8 = r1
            goto L3a
        L38:
            r8 = r28
        L3a:
            r1 = r0 & 64
            if (r1 == 0) goto L46
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreEmoji r1 = r1.getEmojis()
            r9 = r1
            goto L48
        L46:
            r9 = r29
        L48:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L67
            com.discord.widgets.chat.MessageManager r1 = new com.discord.widgets.chat.MessageManager
            android.content.Context r11 = r23.requireContext()
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 510(0x1fe, float:7.15E-43)
            r21 = 0
            r10 = r1
            r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            goto L69
        L67:
            r10 = r30
        L69:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L75
            com.discord.utilities.channel.ChannelSelector$Companion r1 = com.discord.utilities.channel.ChannelSelector.Companion
            com.discord.utilities.channel.ChannelSelector r1 = r1.getInstance()
            r11 = r1
            goto L77
        L75:
            r11 = r31
        L77:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L84
            com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler$UserReactionHandler r1 = new com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler$UserReactionHandler
            r3 = r23
            r1.<init>(r3, r6, r9)
            r12 = r1
            goto L88
        L84:
            r3 = r23
            r12 = r32
        L88:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L94
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r1 = r1.getChannels()
            r13 = r1
            goto L96
        L94:
            r13 = r33
        L96:
            r0 = r0 & 2048(0x800, float:2.87E-42)
            if (r0 == 0) goto La2
            com.discord.utilities.analytics.AnalyticsUtils$Tracker$Companion r0 = com.discord.utilities.analytics.AnalyticsUtils.Tracker.Companion
            com.discord.utilities.analytics.AnalyticsUtils$Tracker r0 = r0.getInstance()
            r14 = r0
            goto La4
        La2:
            r14 = r34
        La4:
            r2 = r22
            r3 = r23
            r4 = r24
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler.<init>(com.discord.app.AppFragment, com.discord.widgets.chat.input.AppFlexInputViewModel, com.discord.stores.StoreChat, com.discord.stores.StoreMessages, com.discord.stores.StorePendingReplies, com.discord.stores.StoreUser, com.discord.stores.StoreEmoji, com.discord.widgets.chat.MessageManager, com.discord.utilities.channel.ChannelSelector, com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler$UserReactionHandler, com.discord.stores.StoreChannels, com.discord.utilities.analytics.AnalyticsUtils$Tracker, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
