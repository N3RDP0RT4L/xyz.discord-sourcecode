package com.discord.widgets.chat.list.adapter;

import com.discord.api.sticker.Sticker;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.StickerEntry;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemSticker.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/sticker/Sticker;", "localSticker", "", "invoke", "(Lcom/discord/api/sticker/Sticker;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemSticker$onConfigure$1 extends o implements Function1<Sticker, Unit> {
    public final /* synthetic */ ChatListEntry $data;
    public final /* synthetic */ StickerEntry $stickerEntry;
    public final /* synthetic */ WidgetChatListAdapterItemSticker this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemSticker$onConfigure$1(WidgetChatListAdapterItemSticker widgetChatListAdapterItemSticker, StickerEntry stickerEntry, ChatListEntry chatListEntry) {
        super(1);
        this.this$0 = widgetChatListAdapterItemSticker;
        this.$stickerEntry = stickerEntry;
        this.$data = chatListEntry;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Sticker sticker) {
        invoke2(sticker);
        return Unit.a;
    }

    /* JADX WARN: Code restructure failed: missing block: B:0:?, code lost:
        r4 = r4;
     */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void invoke2(com.discord.api.sticker.Sticker r4) {
        /*
            r3 = this;
            if (r4 == 0) goto L3
            goto L9
        L3:
            com.discord.widgets.chat.list.entries.StickerEntry r4 = r3.$stickerEntry
            com.discord.api.sticker.BaseSticker r4 = r4.getSticker()
        L9:
            com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSticker r0 = r3.this$0
            com.discord.databinding.WidgetChatListAdapterItemStickerBinding r0 = com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSticker.access$getBinding$p(r0)
            com.discord.views.sticker.StickerView r0 = r0.f2315b
            r1 = 0
            r2 = 2
            com.discord.views.sticker.StickerView.e(r0, r4, r1, r2)
            com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSticker r0 = r3.this$0
            com.discord.databinding.WidgetChatListAdapterItemStickerBinding r0 = com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSticker.access$getBinding$p(r0)
            com.discord.views.sticker.StickerView r0 = r0.f2315b
            com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSticker$onConfigure$1$1 r1 = new com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSticker$onConfigure$1$1
            r1.<init>()
            r0.setOnClickListener(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSticker$onConfigure$1.invoke2(com.discord.api.sticker.Sticker):void");
    }
}
