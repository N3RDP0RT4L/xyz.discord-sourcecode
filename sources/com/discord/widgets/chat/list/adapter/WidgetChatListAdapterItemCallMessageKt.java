package com.discord.widgets.chat.list.adapter;

import kotlin.Metadata;
/* compiled from: WidgetChatListAdapterItemCallMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\"\u0016\u0010\u0001\u001a\u00020\u00008\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "MAX_USERS_SHOWN", "I", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemCallMessageKt {
    private static final int MAX_USERS_SHOWN = 3;
}
