package com.discord.widgets.chat.list.adapter;

import com.discord.stores.StoreMessageUploads;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/stores/StoreMessageUploads$MessageUploadState;", "invoke", "()Lcom/discord/stores/StoreMessageUploads$MessageUploadState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemUploadProgress$ModelProvider$get$1 extends o implements Function0<StoreMessageUploads.MessageUploadState> {
    public final /* synthetic */ StoreMessageUploads $messageUploadStore;
    public final /* synthetic */ String $nonce;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemUploadProgress$ModelProvider$get$1(StoreMessageUploads storeMessageUploads, String str) {
        super(0);
        this.$messageUploadStore = storeMessageUploads;
        this.$nonce = str;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final StoreMessageUploads.MessageUploadState invoke() {
        return this.$messageUploadStore.getUploadProgress(this.$nonce);
    }
}
