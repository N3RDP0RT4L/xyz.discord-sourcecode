package com.discord.widgets.chat.list.adapter;

import com.discord.stores.StoreStream;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.UploadProgressEntry;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemUploadProgress$onConfigure$cancel$1 extends o implements Function0<Unit> {
    public final /* synthetic */ ChatListEntry $data;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemUploadProgress$onConfigure$cancel$1(ChatListEntry chatListEntry) {
        super(0);
        this.$data = chatListEntry;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream.Companion.getMessages().cancelMessageSend(((UploadProgressEntry) this.$data).getChannelId(), ((UploadProgressEntry) this.$data).getMessageNonce());
    }
}
