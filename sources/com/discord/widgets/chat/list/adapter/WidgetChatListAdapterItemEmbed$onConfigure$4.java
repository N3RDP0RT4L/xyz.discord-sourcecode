package com.discord.widgets.chat.list.adapter;

import b.d.b.a.a;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemEmbed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;", "kotlin.jvm.PlatformType", "model", "", "invoke", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemEmbed$onConfigure$4 extends o implements Function1<WidgetChatListAdapterItemEmbed.Model, Unit> {
    public final /* synthetic */ WidgetChatListAdapterItemEmbed this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemEmbed$onConfigure$4(WidgetChatListAdapterItemEmbed widgetChatListAdapterItemEmbed) {
        super(1);
        this.this$0 = widgetChatListAdapterItemEmbed;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetChatListAdapterItemEmbed.Model model) {
        invoke2(model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetChatListAdapterItemEmbed.Model model) {
        MessageRenderContext createRenderContext = model.createRenderContext(a.x(this.this$0.itemView, "itemView", "itemView.context"), WidgetChatListAdapterItemEmbed.access$getAdapter$p(this.this$0).getEventHandler());
        WidgetChatListAdapterItemEmbed widgetChatListAdapterItemEmbed = this.this$0;
        m.checkNotNullExpressionValue(model, "model");
        widgetChatListAdapterItemEmbed.configureEmbedTitle(model, createRenderContext);
        this.this$0.configureEmbedDescription(model, createRenderContext);
        this.this$0.configureEmbedFields(model, createRenderContext);
    }
}
