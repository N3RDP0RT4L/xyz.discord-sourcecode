package com.discord.widgets.chat.list.adapter;

import androidx.fragment.app.FragmentManager;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.StickerPartial;
import com.discord.utilities.error.Error;
import com.discord.widgets.stickers.WidgetUnknownStickerSheet;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterEventsHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterEventsHandler$onStickerClicked$2 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ BaseSticker $sticker;
    public final /* synthetic */ WidgetChatListAdapterEventsHandler this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterEventsHandler$onStickerClicked$2(WidgetChatListAdapterEventsHandler widgetChatListAdapterEventsHandler, BaseSticker baseSticker) {
        super(1);
        this.this$0 = widgetChatListAdapterEventsHandler;
        this.$sticker = baseSticker;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        FragmentManager fragmentManager;
        m.checkNotNullParameter(error, "it");
        WidgetUnknownStickerSheet.Companion companion = WidgetUnknownStickerSheet.Companion;
        fragmentManager = this.this$0.getFragmentManager();
        BaseSticker baseSticker = this.$sticker;
        Objects.requireNonNull(baseSticker, "null cannot be cast to non-null type com.discord.api.sticker.StickerPartial");
        companion.show(fragmentManager, (StickerPartial) baseSticker);
    }
}
