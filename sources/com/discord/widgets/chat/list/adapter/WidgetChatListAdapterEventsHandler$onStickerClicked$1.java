package com.discord.widgets.chat.list.adapter;

import androidx.fragment.app.FragmentManager;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerPartial;
import com.discord.models.message.Message;
import com.discord.widgets.stickers.WidgetGuildStickerSheet;
import com.discord.widgets.stickers.WidgetStickerSheet;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterEventsHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/sticker/BaseSticker;", "fetchedSticker", "", "invoke", "(Lcom/discord/api/sticker/BaseSticker;)V", "handleFullStickerClicked"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterEventsHandler$onStickerClicked$1 extends o implements Function1<BaseSticker, Unit> {
    public final /* synthetic */ Message $message;
    public final /* synthetic */ BaseSticker $sticker;
    public final /* synthetic */ WidgetChatListAdapterEventsHandler this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterEventsHandler$onStickerClicked$1(WidgetChatListAdapterEventsHandler widgetChatListAdapterEventsHandler, BaseSticker baseSticker, Message message) {
        super(1);
        this.this$0 = widgetChatListAdapterEventsHandler;
        this.$sticker = baseSticker;
        this.$message = message;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(BaseSticker baseSticker) {
        invoke2(baseSticker);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(BaseSticker baseSticker) {
        FragmentManager fragmentManager;
        FragmentManager fragmentManager2;
        if (baseSticker == null) {
            baseSticker = this.$sticker;
        }
        if (!(baseSticker instanceof StickerPartial)) {
            Objects.requireNonNull(baseSticker, "null cannot be cast to non-null type com.discord.api.sticker.Sticker");
            Sticker sticker = (Sticker) baseSticker;
            int ordinal = sticker.k().ordinal();
            if (ordinal == 1) {
                WidgetStickerSheet.Companion companion = WidgetStickerSheet.Companion;
                fragmentManager = this.this$0.getFragmentManager();
                companion.show(fragmentManager, sticker, this.$message.getChannelId());
            } else if (ordinal == 2) {
                WidgetGuildStickerSheet.Companion companion2 = WidgetGuildStickerSheet.Companion;
                fragmentManager2 = this.this$0.getFragmentManager();
                companion2.show(fragmentManager2, sticker);
            }
        }
    }
}
