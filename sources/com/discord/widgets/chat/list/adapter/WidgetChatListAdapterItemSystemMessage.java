package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import b.d.b.a.a;
import com.discord.api.message.MessageReference;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.databinding.WidgetChatListAdapterItemSystemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.sticker.StickerView;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.MessageEntry;
import com.discord.widgets.servers.guildboost.WidgetGuildBoost;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemSystemMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010#\u001a\u00020\"¢\u0006\u0004\b$\u0010%JY\u0010\u0010\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0006\u0010\b\u001a\u00020\u00072\u000e\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n2\b\u0010\f\u001a\u0004\u0018\u00010\u00052\u0006\u0010\r\u001a\u00020\u00072\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0013\u0010\u0012\u001a\u00020\u0007*\u00020\u0002H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0011\u0010\u0015\u001a\u0004\u0018\u00010\u0014H\u0014¢\u0006\u0004\b\u0015\u0010\u0016J\u001f\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0019\u001a\u00020\u0018H\u0014¢\u0006\u0004\b\u001b\u0010\u001cR\u0018\u0010\u001d\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!¨\u0006&"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemSystemMessage;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/models/message/Message;", "Landroid/content/Context;", "context", "", "authorName", "", "authorRoleColor", "", "Lcom/discord/primitives/UserId;", "firstMentionedUserId", "firstMentionedUserName", "otherRoleColor", "guildName", "", "getSystemMessage", "(Lcom/discord/models/message/Message;Landroid/content/Context;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/CharSequence;", "getIcon", "(Lcom/discord/models/message/Message;)I", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "Lcom/discord/databinding/WidgetChatListAdapterItemSystemBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemSystemBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemSystemMessage extends WidgetChatListItem {
    private final WidgetChatListAdapterItemSystemBinding binding;
    private Subscription subscription;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemSystemMessage(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_system, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_thread_embed_spine;
        AppCompatImageView appCompatImageView = (AppCompatImageView) view.findViewById(R.id.chat_list_adapter_item_thread_embed_spine);
        if (appCompatImageView != null) {
            i = R.id.system_icon;
            ImageView imageView = (ImageView) view.findViewById(R.id.system_icon);
            if (imageView != null) {
                i = R.id.system_text;
                LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.system_text);
                if (linkifiedTextView != null) {
                    i = R.id.system_timestamp;
                    TextView textView = (TextView) view.findViewById(R.id.system_timestamp);
                    if (textView != null) {
                        i = R.id.system_welcome_cta_button;
                        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.system_welcome_cta_button);
                        if (linearLayout != null) {
                            i = R.id.system_welcome_cta_button_sticker;
                            StickerView stickerView = (StickerView) view.findViewById(R.id.system_welcome_cta_button_sticker);
                            if (stickerView != null) {
                                i = R.id.uikit_chat_guideline;
                                Guideline guideline = (Guideline) view.findViewById(R.id.uikit_chat_guideline);
                                if (guideline != null) {
                                    WidgetChatListAdapterItemSystemBinding widgetChatListAdapterItemSystemBinding = new WidgetChatListAdapterItemSystemBinding((ConstraintLayout) view, appCompatImageView, imageView, linkifiedTextView, textView, linearLayout, stickerView, guideline);
                                    m.checkNotNullExpressionValue(widgetChatListAdapterItemSystemBinding, "WidgetChatListAdapterIte…temBinding.bind(itemView)");
                                    this.binding = widgetChatListAdapterItemSystemBinding;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemSystemMessage widgetChatListAdapterItemSystemMessage) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemSystemMessage.adapter;
    }

    private final int getIcon(Message message) {
        Integer type = message.getType();
        if (type == null || type.intValue() != 1) {
            if (type != null && type.intValue() == 2) {
                return R.drawable.ic_group_leave;
            }
            if ((type != null && type.intValue() == 4) || (type != null && type.intValue() == 5)) {
                return R.drawable.ic_group_edit;
            }
            if (type != null && type.intValue() == 6) {
                return R.drawable.ic_channel_pinned_message;
            }
            if (type == null || type.intValue() != 7) {
                if ((type != null && type.intValue() == 8) || ((type != null && type.intValue() == 9) || ((type != null && type.intValue() == 10) || (type != null && type.intValue() == 11)))) {
                    return R.drawable.ic_user_premium_guild_subscription;
                }
                if (type == null || type.intValue() != 12) {
                    if (type == null || type.intValue() != 14) {
                        if (type != null && type.intValue() == 15) {
                            return R.drawable.ic_check_green_24dp;
                        }
                        if (type != null && type.intValue() == 16) {
                            return R.drawable.ic_warning_circle_24dp;
                        }
                        if (type != null && type.intValue() == 17) {
                            return R.drawable.ic_warning_circle_24dp;
                        }
                        if (type != null && type.intValue() == 18) {
                            return R.drawable.ic_thread;
                        }
                        if (type != null) {
                            type.intValue();
                        }
                    }
                    return R.drawable.ic_x_red_24dp;
                }
            }
        }
        return R.drawable.ic_group_join;
    }

    private final CharSequence getSystemMessage(Message message, Context context, String str, int i, Long l, String str2, int i2, String str3) {
        return new WidgetChatListAdapterItemSystemMessage$getSystemMessage$1(message, str, str2, new WidgetChatListAdapterItemSystemMessage$getSystemMessage$usernameRenderContext$1(i, i2), l, context, str3, new WidgetChatListAdapterItemSystemMessage$getSystemMessage$actorRenderContext$1(i)).invoke(context);
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.subscription;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, final ChatListEntry chatListEntry) {
        User user;
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        MessageEntry messageEntry = (MessageEntry) chatListEntry;
        final Message component1 = messageEntry.component1();
        GuildMember component3 = messageEntry.component3();
        GuildMember component4 = messageEntry.component4();
        Map<Long, String> component6 = messageEntry.component6();
        User author = component1.getAuthor();
        String str = component6.get(author != null ? Long.valueOf(author.i()) : null);
        Guild guild = ((WidgetChatListAdapter) this.adapter).getData().getGuild();
        String name = guild != null ? guild.getName() : null;
        List<User> mentions = component1.getMentions();
        Long valueOf = (mentions == null || (user = (User) u.firstOrNull((List<? extends Object>) mentions)) == null) ? null : Long.valueOf(user.i());
        String str2 = valueOf != null ? component6.get(Long.valueOf(valueOf.longValue())) : null;
        LinkifiedTextView linkifiedTextView = this.binding.d;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.systemText");
        int themedColor = ColorCompat.getThemedColor(linkifiedTextView.getContext(), (int) R.attr.colorHeaderPrimary);
        GuildMember.Companion companion = GuildMember.Companion;
        int color = companion.getColor(component3, themedColor);
        int color2 = companion.getColor(component4, themedColor);
        LinkifiedTextView linkifiedTextView2 = this.binding.d;
        m.checkNotNullExpressionValue(linkifiedTextView2, "binding.systemText");
        Context context = linkifiedTextView2.getContext();
        m.checkNotNullExpressionValue(context, "binding.systemText.context");
        CharSequence systemMessage = getSystemMessage(component1, context, str, color, valueOf, str2, color2, name);
        final StoreExperiments experiments = StoreStream.Companion.getExperiments();
        this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSystemMessage$onConfigure$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChatListAdapterItemSystemBinding widgetChatListAdapterItemSystemBinding;
                Integer type = component1.getType();
                if (type != null && type.intValue() == 7) {
                    WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getEventHandler().onMessageAuthorNameClicked(component1, WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getData().getGuildId());
                } else if ((type != null && type.intValue() == 8) || ((type != null && type.intValue() == 9) || ((type != null && type.intValue() == 10) || (type != null && type.intValue() == 11)))) {
                    boolean z2 = true;
                    Experiment guildExperiment = experiments.getGuildExperiment("2022-01_boost_announcement_upsell", WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getData().getGuildId(), true);
                    if (guildExperiment == null || guildExperiment.getBucket() != 1) {
                        z2 = false;
                    }
                    if (z2) {
                        AnalyticsTracker.guildBoostPromotionOpened$default(AnalyticsTracker.INSTANCE, WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getData().getGuildId(), new Traits.Location(null, Traits.Location.Section.CHANNEL_TEXT_AREA, Traits.Location.Obj.BOOST_GEM_ICON, null, null, 25, null), null, 4, null);
                        WidgetGuildBoost.Companion companion2 = WidgetGuildBoost.Companion;
                        widgetChatListAdapterItemSystemBinding = WidgetChatListAdapterItemSystemMessage.this.binding;
                        LinkifiedTextView linkifiedTextView3 = widgetChatListAdapterItemSystemBinding.d;
                        m.checkNotNullExpressionValue(linkifiedTextView3, "binding.systemText");
                        Context context2 = linkifiedTextView3.getContext();
                        m.checkNotNullExpressionValue(context2, "binding.systemText.context");
                        companion2.create(context2, WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getData().getGuildId());
                        return;
                    }
                    WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getEventHandler().onMessageAuthorNameClicked(component1, WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getData().getGuildId());
                } else if (type != null && type.intValue() == 12) {
                    WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getEventHandler().onMessageAuthorNameClicked(component1, WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getData().getGuildId());
                } else if (type != null && type.intValue() == 6) {
                    WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getEventHandler().onOpenPinsClicked(component1);
                } else if (type != null && type.intValue() == 18) {
                    MessageReference messageReference = component1.getMessageReference();
                    Long a = messageReference != null ? messageReference.a() : null;
                    if (a != null) {
                        ChannelSelector.Companion.getInstance().selectChannel(WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getData().getGuildId(), a.longValue(), (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null);
                    }
                }
            }
        });
        ConstraintLayout constraintLayout = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.root");
        ViewExtensions.setOnLongClickListenerConsumeClick(constraintLayout, new WidgetChatListAdapterItemSystemMessage$onConfigure$2(this, component1));
        TextView textView = this.binding.e;
        m.checkNotNullExpressionValue(textView, "binding.systemTimestamp");
        Context x2 = a.x(this.itemView, "itemView", "itemView.context");
        UtcDateTime timestamp = component1.getTimestamp();
        textView.setText(TimeUtils.toReadableTimeString$default(x2, timestamp != null ? timestamp.g() : 0L, null, 4, null));
        LinkifiedTextView linkifiedTextView3 = this.binding.d;
        m.checkNotNullExpressionValue(linkifiedTextView3, "binding.systemText");
        linkifiedTextView3.setText(systemMessage);
        this.binding.c.setImageResource(getIcon(component1));
        AppCompatImageView appCompatImageView = this.binding.f2318b;
        m.checkNotNullExpressionValue(appCompatImageView, "binding.chatListAdapterItemThreadEmbedSpine");
        boolean z2 = true;
        int i2 = 0;
        appCompatImageView.setVisibility(component1.hasThread() && !messageEntry.isThreadStarterMessage() ? 0 : 8);
        LinearLayout linearLayout = this.binding.f;
        m.checkNotNullExpressionValue(linearLayout, "binding.systemWelcomeCtaButton");
        if (messageEntry.getWelcomeData() == null) {
            z2 = false;
        }
        if (!z2) {
            i2 = 8;
        }
        linearLayout.setVisibility(i2);
        if (messageEntry.getWelcomeData() != null) {
            StickerView.e(this.binding.g, messageEntry.getWelcomeData().getSticker(), null, 2);
            this.binding.g.b();
            this.binding.f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemSystemMessage$onConfigure$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(WidgetChatListAdapterItemSystemMessage.this).getEventHandler().onWelcomeCtaClicked(component1, ((MessageEntry) chatListEntry).getWelcomeData().getChannel(), ((MessageEntry) chatListEntry).getWelcomeData().getSticker());
                }
            });
        }
    }
}
