package com.discord.widgets.chat.list.adapter;

import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.permission.Permission;
import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.models.guild.UserGuildMember;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGuildScheduledEventInvite;
import com.discord.widgets.chat.list.entries.GuildScheduledEventInviteEntry;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatListAdapterItemGuildScheduledEventInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite$Model;", "invoke", "()Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemGuildScheduledEventInvite$Model$Companion$observe$1 extends o implements Function0<WidgetChatListAdapterItemGuildScheduledEventInvite.Model> {
    public final /* synthetic */ StoreChannels $channelStore;
    public final /* synthetic */ StoreGuildScheduledEvents $guildScheduledEventStore;
    public final /* synthetic */ StoreGuilds $guildStore;
    public final /* synthetic */ GuildScheduledEventInviteEntry $item;
    public final /* synthetic */ StorePermissions $permissionStore;
    public final /* synthetic */ StoreUser $userStore;
    public final /* synthetic */ StoreVoiceChannelSelected $voiceChannelSelectedStore;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemGuildScheduledEventInvite$Model$Companion$observe$1(GuildScheduledEventInviteEntry guildScheduledEventInviteEntry, StoreGuilds storeGuilds, StoreUser storeUser, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreChannels storeChannels, StoreVoiceChannelSelected storeVoiceChannelSelected, StorePermissions storePermissions) {
        super(0);
        this.$item = guildScheduledEventInviteEntry;
        this.$guildStore = storeGuilds;
        this.$userStore = storeUser;
        this.$guildScheduledEventStore = storeGuildScheduledEvents;
        this.$channelStore = storeChannels;
        this.$voiceChannelSelectedStore = storeVoiceChannelSelected;
        this.$permissionStore = storePermissions;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetChatListAdapterItemGuildScheduledEventInvite.Model invoke() {
        Channel channel;
        GuildScheduledEvent guildScheduledEvent = this.$item.getGuildScheduledEvent();
        Channel channel2 = this.$item.getInvite().getChannel();
        Guild guild = null;
        if (channel2 != null) {
            com.discord.api.guild.Guild guild2 = this.$item.getInvite().guild;
            channel = Channel.a(channel2, null, 0, guild2 != null ? guild2.r() : 0L, null, 0L, 0L, 0L, null, null, 0, null, 0, 0, null, 0L, 0L, null, false, 0L, null, 0, null, null, null, null, null, null, null, null, 536870907);
        } else {
            channel = null;
        }
        UserGuildMember creatorUserGuildMember$default = GuildScheduledEventUtilitiesKt.getCreatorUserGuildMember$default(guildScheduledEvent, (StoreGuilds) null, (StoreUser) null, 3, (Object) null);
        ModelInvite invite = this.$item.getInvite();
        boolean z2 = this.$guildStore.getMember(this.$item.getGuildScheduledEvent().h(), this.$userStore.getMe().getId()) != null;
        boolean isMeRsvpedToEvent = this.$guildScheduledEventStore.isMeRsvpedToEvent(this.$item.getGuildScheduledEvent().h(), this.$item.getGuildScheduledEvent().i());
        Channel channel3 = channel != null ? this.$channelStore.getChannel(channel.h()) : null;
        Guild guild3 = this.$guildStore.getGuild(this.$item.getGuildScheduledEvent().h());
        if (guild3 != null) {
            guild = guild3;
        } else {
            com.discord.api.guild.Guild guild4 = this.$item.getInvite().guild;
            if (guild4 != null) {
                m.checkNotNullExpressionValue(guild4, "apiGuild");
                guild = new Guild(guild4);
            }
        }
        return new WidgetChatListAdapterItemGuildScheduledEventInvite.Model(invite, z2, isMeRsvpedToEvent, guildScheduledEvent, channel3, guild, creatorUserGuildMember$default, Long.valueOf(this.$voiceChannelSelectedStore.getSelectedVoiceChannelId()), channel != null ? PermissionUtils.can(Permission.CONNECT, this.$permissionStore.getPermissionsByChannel().get(Long.valueOf(channel.h()))) : false, GuildScheduledEventUtilities.Companion.canShareEvent$default(GuildScheduledEventUtilities.Companion, this.$item.getGuildScheduledEvent().b(), this.$item.getGuildScheduledEvent().h(), this.$channelStore, this.$guildStore, null, null, 48, null));
    }
}
