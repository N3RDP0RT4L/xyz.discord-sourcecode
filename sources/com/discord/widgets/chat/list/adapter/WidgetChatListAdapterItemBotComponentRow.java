package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import com.discord.api.botuikit.SelectItem;
import com.discord.databinding.WidgetChatListAdapterItemBotComponentRowBinding;
import com.discord.models.botuikit.MessageComponent;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.message.Message;
import com.discord.restapi.RestAPIParams;
import com.discord.widgets.botuikit.ComponentProvider;
import com.discord.widgets.botuikit.views.ComponentActionListener;
import com.discord.widgets.botuikit.views.select.ComponentContext;
import com.discord.widgets.botuikit.views.select.SelectComponentBottomSheet;
import com.discord.widgets.chat.list.entries.BotUiComponentEntry;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemBotComponentRow.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0012\u0006\u0010&\u001a\u00020%¢\u0006\u0004\b'\u0010(J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0014¢\u0006\u0004\b\b\u0010\tJ#\u0010\u000e\u001a\u00020\u00072\n\u0010\u000b\u001a\u00060\u0003j\u0002`\n2\u0006\u0010\r\u001a\u00020\fH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJa\u0010\u0019\u001a\u00020\u00072\n\u0010\u000b\u001a\u00060\u0003j\u0002`\n2\u0006\u0010\r\u001a\u00020\f2\b\u0010\u0010\u001a\u0004\u0018\u00010\f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0017H\u0016¢\u0006\u0004\b\u0019\u0010\u001aR\"\u0010\u001c\u001a\u00020\u001b8\u0006@\u0006X\u0086.¢\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$¨\u0006)"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemBotComponentRow;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/widgets/botuikit/views/ComponentActionListener;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/widgets/botuikit/ComponentIndex;", "componentIndex", "", "customId", "onButtonComponentClick", "(ILjava/lang/String;)V", "placeholder", "", "Lcom/discord/api/botuikit/SelectItem;", "options", "selectedItems", "minOptionsToSelect", "maxOptionsToSelect", "", "emojiAnimationsEnabled", "onSelectComponentClick", "(ILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;IIZ)V", "Lcom/discord/widgets/chat/list/entries/BotUiComponentEntry;", "entry", "Lcom/discord/widgets/chat/list/entries/BotUiComponentEntry;", "getEntry", "()Lcom/discord/widgets/chat/list/entries/BotUiComponentEntry;", "setEntry", "(Lcom/discord/widgets/chat/list/entries/BotUiComponentEntry;)V", "Lcom/discord/databinding/WidgetChatListAdapterItemBotComponentRowBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemBotComponentRowBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemBotComponentRow extends WidgetChatListItem implements ComponentActionListener {
    private final WidgetChatListAdapterItemBotComponentRowBinding binding;
    public BotUiComponentEntry entry;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemBotComponentRow(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_bot_component_row, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_component_root;
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.chat_list_adapter_item_component_root);
        if (linearLayout != null) {
            i = R.id.chat_list_adapter_item_gutter_bg;
            View findViewById = view.findViewById(R.id.chat_list_adapter_item_gutter_bg);
            if (findViewById != null) {
                i = R.id.chat_list_adapter_item_highlighted_bg;
                View findViewById2 = view.findViewById(R.id.chat_list_adapter_item_highlighted_bg);
                if (findViewById2 != null) {
                    WidgetChatListAdapterItemBotComponentRowBinding widgetChatListAdapterItemBotComponentRowBinding = new WidgetChatListAdapterItemBotComponentRowBinding((ConstraintLayout) view, linearLayout, findViewById, findViewById2);
                    m.checkNotNullExpressionValue(widgetChatListAdapterItemBotComponentRowBinding, "WidgetChatListAdapterIte…RowBinding.bind(itemView)");
                    this.binding = widgetChatListAdapterItemBotComponentRowBinding;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public final BotUiComponentEntry getEntry() {
        BotUiComponentEntry botUiComponentEntry = this.entry;
        if (botUiComponentEntry == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        return botUiComponentEntry;
    }

    @Override // com.discord.widgets.botuikit.views.ComponentActionListener
    public void onButtonComponentClick(int i, String str) {
        m.checkNotNullParameter(str, "customId");
        WidgetChatListAdapter widgetChatListAdapter = (WidgetChatListAdapter) this.adapter;
        BotUiComponentEntry botUiComponentEntry = this.entry;
        if (botUiComponentEntry == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        long applicationId = botUiComponentEntry.getApplicationId();
        BotUiComponentEntry botUiComponentEntry2 = this.entry;
        if (botUiComponentEntry2 == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        long id2 = botUiComponentEntry2.getMessage().getId();
        BotUiComponentEntry botUiComponentEntry3 = this.entry;
        if (botUiComponentEntry3 == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        widgetChatListAdapter.onBotUiComponentClicked(applicationId, id2, botUiComponentEntry3.getMessage().getFlags(), i, new RestAPIParams.ComponentInteractionData.ButtonComponentInteractionData(null, str, 1, null));
    }

    @Override // com.discord.widgets.botuikit.views.ComponentActionListener
    public void onSelectComponentClick(int i, String str, String str2, List<SelectItem> list, List<SelectItem> list2, int i2, int i3, boolean z2) {
        m.checkNotNullParameter(str, "customId");
        m.checkNotNullParameter(list, "options");
        m.checkNotNullParameter(list2, "selectedItems");
        SelectComponentBottomSheet.Companion companion = SelectComponentBottomSheet.Companion;
        FragmentManager fragmentManager = ((WidgetChatListAdapter) this.adapter).getFragmentManager();
        BotUiComponentEntry botUiComponentEntry = this.entry;
        if (botUiComponentEntry == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        Long guildId = botUiComponentEntry.getGuildId();
        BotUiComponentEntry botUiComponentEntry2 = this.entry;
        if (botUiComponentEntry2 == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        long id2 = botUiComponentEntry2.getMessage().getId();
        BotUiComponentEntry botUiComponentEntry3 = this.entry;
        if (botUiComponentEntry3 == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        long channelId = botUiComponentEntry3.getMessage().getChannelId();
        BotUiComponentEntry botUiComponentEntry4 = this.entry;
        if (botUiComponentEntry4 == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        Long flags = botUiComponentEntry4.getMessage().getFlags();
        BotUiComponentEntry botUiComponentEntry5 = this.entry;
        if (botUiComponentEntry5 == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        companion.show(fragmentManager, new ComponentContext(guildId, id2, channelId, flags, botUiComponentEntry5.getApplicationId()), i, str, str2, i2, i3, list, list2, z2);
    }

    public final void setEntry(BotUiComponentEntry botUiComponentEntry) {
        m.checkNotNullParameter(botUiComponentEntry, "<set-?>");
        this.entry = botUiComponentEntry;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        BotUiComponentEntry botUiComponentEntry = (BotUiComponentEntry) chatListEntry;
        this.entry = botUiComponentEntry;
        if (botUiComponentEntry == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        Message message = botUiComponentEntry.getMessage();
        View view = this.binding.c;
        m.checkNotNullExpressionValue(view, "binding.chatListAdapterItemGutterBg");
        View view2 = this.binding.d;
        m.checkNotNullExpressionValue(view2, "binding.chatListAdapterItemHighlightedBg");
        configureCellHighlight(message, view2, view);
        BotUiComponentEntry botUiComponentEntry2 = this.entry;
        if (botUiComponentEntry2 == null) {
            m.throwUninitializedPropertyAccessException("entry");
        }
        List<MessageComponent> messageComponents = botUiComponentEntry2.getMessageComponents();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(messageComponents, 10));
        int i2 = 0;
        for (Object obj : messageComponents) {
            i2++;
            if (i2 < 0) {
                n.throwIndexOverflow();
            }
            ComponentProvider botUiComponentProvider = ((WidgetChatListAdapter) this.adapter).getBotUiComponentProvider();
            LinearLayout linearLayout = this.binding.f2294b;
            m.checkNotNullExpressionValue(linearLayout, "binding.chatListAdapterItemComponentRoot");
            arrayList.add(botUiComponentProvider.getConfiguredComponentView(this, (MessageComponent) obj, linearLayout, i2));
        }
        LinearLayout linearLayout2 = this.binding.f2294b;
        m.checkNotNullExpressionValue(linearLayout2, "binding.chatListAdapterItemComponentRoot");
        WidgetChatListAdapterItemBotComponentRowKt.replaceViews(linearLayout2, u.filterNotNull(arrayList));
    }
}
