package com.discord.widgets.chat.list.adapter;

import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.Sticker;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterEventsHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/sticker/Sticker;", "fetchedSticker", "", "invoke", "(Lcom/discord/api/sticker/Sticker;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterEventsHandler$onStickerClicked$3 extends o implements Function1<Sticker, Unit> {
    public final /* synthetic */ WidgetChatListAdapterEventsHandler$onStickerClicked$1 $handleFullStickerClicked$1;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterEventsHandler$onStickerClicked$3(WidgetChatListAdapterEventsHandler$onStickerClicked$1 widgetChatListAdapterEventsHandler$onStickerClicked$1) {
        super(1);
        this.$handleFullStickerClicked$1 = widgetChatListAdapterEventsHandler$onStickerClicked$1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Sticker sticker) {
        invoke2(sticker);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Sticker sticker) {
        this.$handleFullStickerClicked$1.invoke2((BaseSticker) sticker);
    }
}
