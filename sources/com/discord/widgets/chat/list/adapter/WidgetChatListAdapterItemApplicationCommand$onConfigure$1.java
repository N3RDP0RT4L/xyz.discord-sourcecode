package com.discord.widgets.chat.list.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemApplicationCommand.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Landroid/widget/TextView;", "", "invoke", "(Landroid/widget/TextView;)V", "copyTextOnLongPress"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemApplicationCommand$onConfigure$1 extends o implements Function1<TextView, Unit> {
    public static final WidgetChatListAdapterItemApplicationCommand$onConfigure$1 INSTANCE = new WidgetChatListAdapterItemApplicationCommand$onConfigure$1();

    public WidgetChatListAdapterItemApplicationCommand$onConfigure$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(TextView textView) {
        invoke2(textView);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(final TextView textView) {
        m.checkNotNullParameter(textView, "$this$copyTextOnLongPress");
        textView.setOnLongClickListener(new View.OnLongClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemApplicationCommand$onConfigure$1.1
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                Context context = textView.getContext();
                m.checkNotNullExpressionValue(context, "context");
                CharSequence text = textView.getText();
                m.checkNotNullExpressionValue(text, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                b.a.d.m.c(context, text, 0, 4);
                return false;
            }
        });
    }
}
