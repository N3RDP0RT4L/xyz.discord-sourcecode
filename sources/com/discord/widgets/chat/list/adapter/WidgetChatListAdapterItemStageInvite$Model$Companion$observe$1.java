package com.discord.widgets.chat.list.adapter;

import com.discord.stores.StoreAccessibility;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreRequestedStageChannels;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreStageInstances;
import com.discord.stores.StoreUser;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStageInvite;
import com.discord.widgets.chat.list.entries.StageInviteEntry;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatListAdapterItemStageInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite$Model;", "invoke", "()Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemStageInvite$Model$Companion$observe$1 extends o implements Function0<WidgetChatListAdapterItemStageInvite.Model> {
    public final /* synthetic */ StoreAccessibility $accessibilityStore;
    public final /* synthetic */ StoreChannels $channelStore;
    public final /* synthetic */ StoreGuilds $guildStore;
    public final /* synthetic */ StageInviteEntry $item;
    public final /* synthetic */ StoreRequestedStageChannels $requestedInstanceStore;
    public final /* synthetic */ StoreStageChannels $stageChannelStore;
    public final /* synthetic */ StoreStageInstances $stageInstanceStore;
    public final /* synthetic */ StoreUser $userStore;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemStageInvite$Model$Companion$observe$1(StoreUser storeUser, StageInviteEntry stageInviteEntry, StoreChannels storeChannels, StoreAccessibility storeAccessibility, StoreStageInstances storeStageInstances, StoreRequestedStageChannels storeRequestedStageChannels, StoreStageChannels storeStageChannels, StoreGuilds storeGuilds) {
        super(0);
        this.$userStore = storeUser;
        this.$item = stageInviteEntry;
        this.$channelStore = storeChannels;
        this.$accessibilityStore = storeAccessibility;
        this.$stageInstanceStore = storeStageInstances;
        this.$requestedInstanceStore = storeRequestedStageChannels;
        this.$stageChannelStore = storeStageChannels;
        this.$guildStore = storeGuilds;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    /* JADX WARN: Code restructure failed: missing block: B:5:0x0039, code lost:
        if (r1 != null) goto L7;
     */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00f8  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00fa  */
    @Override // kotlin.jvm.functions.Function0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStageInvite.Model invoke() {
        /*
            Method dump skipped, instructions count: 262
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStageInvite$Model$Companion$observe$1.invoke():com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStageInvite$Model");
    }
}
