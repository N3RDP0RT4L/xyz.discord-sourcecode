package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.i.f1;
import b.a.y.b0;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.databinding.WidgetChatListAdapterItemReactionsBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.message.Message;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.ReactionsEntry;
import com.google.android.flexbox.FlexboxLayout;
import d0.z.d.m;
import java.util.Collection;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemReactions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 #2\u00020\u0001:\u0001#B\u000f\u0012\u0006\u0010 \u001a\u00020\u001f¢\u0006\u0004\b!\u0010\"J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J=\u0010\u0010\u001a\u00020\u00042\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0003\u001a\u00020\u0016H\u0014¢\u0006\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006$"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemReactions;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/widgets/chat/list/entries/ReactionsEntry;", "data", "", "processReactions", "(Lcom/discord/widgets/chat/list/entries/ReactionsEntry;)V", "", "Lcom/discord/api/message/reaction/MessageReaction;", "reactions", "", "messageId", "", "canAddReactions", "canCreateReactions", "animateEmojis", "displayReactions", "(Ljava/util/Collection;JZZZ)V", "removeQuickAddReactionView", "()V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/databinding/WidgetChatListAdapterItemReactionsBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemReactionsBinding;", "Landroid/widget/ImageView;", "quickAddReactionView", "Landroid/widget/ImageView;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemReactions extends WidgetChatListItem {
    public static final Companion Companion = new Companion(null);
    private static final int REACTION_LIMIT = 20;
    private final WidgetChatListAdapterItemReactionsBinding binding;
    private final ImageView quickAddReactionView;

    /* compiled from: WidgetChatListAdapterItemReactions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemReactions$Companion;", "", "", "REACTION_LIMIT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemReactions(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_reactions, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_gutter_bg;
        View findViewById = view.findViewById(R.id.chat_list_adapter_item_gutter_bg);
        if (findViewById != null) {
            i = R.id.chat_list_adapter_item_highlighted_bg;
            View findViewById2 = view.findViewById(R.id.chat_list_adapter_item_highlighted_bg);
            if (findViewById2 != null) {
                i = R.id.chat_list_item_reactions;
                FlexboxLayout flexboxLayout = (FlexboxLayout) view.findViewById(R.id.chat_list_item_reactions);
                if (flexboxLayout != null) {
                    ConstraintLayout constraintLayout = (ConstraintLayout) view;
                    WidgetChatListAdapterItemReactionsBinding widgetChatListAdapterItemReactionsBinding = new WidgetChatListAdapterItemReactionsBinding(constraintLayout, findViewById, findViewById2, flexboxLayout);
                    m.checkNotNullExpressionValue(widgetChatListAdapterItemReactionsBinding, "WidgetChatListAdapterIte…onsBinding.bind(itemView)");
                    this.binding = widgetChatListAdapterItemReactionsBinding;
                    m.checkNotNullExpressionValue(constraintLayout, "binding.root");
                    View inflate = LayoutInflater.from(constraintLayout.getContext()).inflate(R.layout.reaction_quick_add, (ViewGroup) null, false);
                    Objects.requireNonNull(inflate, "rootView");
                    ImageView imageView = (ImageView) inflate;
                    m.checkNotNullExpressionValue(new f1(imageView, imageView), "ReactionQuickAddBinding.…ot.context), null, false)");
                    m.checkNotNullExpressionValue(imageView, "ReactionQuickAddBinding.…ntext), null, false).root");
                    this.quickAddReactionView = imageView;
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemReactions widgetChatListAdapterItemReactions) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemReactions.adapter;
    }

    private final void displayReactions(Collection<MessageReaction> collection, final long j, final boolean z2, boolean z3, boolean z4) {
        b0 b0Var;
        removeQuickAddReactionView();
        FlexboxLayout flexboxLayout = this.binding.d;
        m.checkNotNullExpressionValue(flexboxLayout, "binding.chatListItemReactions");
        int childCount = flexboxLayout.getChildCount();
        for (int size = collection.size(); size < childCount; size++) {
            View childAt = this.binding.d.getChildAt(size);
            m.checkNotNullExpressionValue(childAt, "binding.chatListItemReactions.getChildAt(i)");
            childAt.setVisibility(8);
        }
        FlexboxLayout flexboxLayout2 = this.binding.d;
        m.checkNotNullExpressionValue(flexboxLayout2, "binding.chatListItemReactions");
        int childCount2 = flexboxLayout2.getChildCount();
        int i = 0;
        int i2 = 0;
        for (final MessageReaction messageReaction : collection) {
            if (i2 < childCount2) {
                View childAt2 = this.binding.d.getChildAt(i2);
                Objects.requireNonNull(childAt2, "null cannot be cast to non-null type com.discord.views.ReactionView");
                b0Var = (b0) childAt2;
                b0Var.setVisibility(i);
                i2++;
            } else {
                FlexboxLayout flexboxLayout3 = this.binding.d;
                m.checkNotNullExpressionValue(flexboxLayout3, "binding.chatListItemReactions");
                b0Var = new b0(flexboxLayout3.getContext());
                this.binding.d.addView(b0Var);
            }
            i2 = i2;
            b0 b0Var2 = b0Var;
            b0Var2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemReactions$displayReactions$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChatListAdapterItemReactions.access$getAdapter$p(WidgetChatListAdapterItemReactions.this).onReactionClicked(j, messageReaction, z2);
                }
            });
            ViewExtensions.setOnLongClickListenerConsumeClick(b0Var2, new WidgetChatListAdapterItemReactions$displayReactions$2(this, j, messageReaction));
            b0Var2.a(messageReaction, j, z4);
            i = 0;
        }
        if (collection.size() < 20 && z3) {
            this.quickAddReactionView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemReactions$displayReactions$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChatListAdapterItemReactions.access$getAdapter$p(WidgetChatListAdapterItemReactions.this).onQuickAddReactionClicked(j);
                }
            });
            this.binding.d.addView(this.quickAddReactionView);
        }
    }

    private final void processReactions(ReactionsEntry reactionsEntry) {
        Message message = reactionsEntry.getMessage();
        View view = this.binding.c;
        m.checkNotNullExpressionValue(view, "binding.chatListAdapterItemHighlightedBg");
        View view2 = this.binding.f2310b;
        m.checkNotNullExpressionValue(view2, "binding.chatListAdapterItemGutterBg");
        configureCellHighlight(message, view, view2);
        displayReactions(reactionsEntry.getMessage().getReactionsMap().values(), reactionsEntry.getMessage().getId(), reactionsEntry.getCanAddReactions(), reactionsEntry.getCanCreateReactions(), reactionsEntry.getAnimateEmojis());
    }

    private final void removeQuickAddReactionView() {
        FlexboxLayout flexboxLayout = this.binding.d;
        m.checkNotNullExpressionValue(flexboxLayout, "binding.chatListItemReactions");
        int childCount = flexboxLayout.getChildCount();
        if (childCount > 0) {
            int i = childCount - 1;
            if (this.binding.d.getChildAt(i) == this.quickAddReactionView) {
                this.binding.d.removeViewAt(i);
            }
        }
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        processReactions((ReactionsEntry) chatListEntry);
    }
}
