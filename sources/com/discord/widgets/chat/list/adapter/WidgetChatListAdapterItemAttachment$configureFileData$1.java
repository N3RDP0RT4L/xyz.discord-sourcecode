package com.discord.widgets.chat.list.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemAttachment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemAttachment$configureFileData$1 extends o implements Function1<View, Unit> {
    public static final WidgetChatListAdapterItemAttachment$configureFileData$1 INSTANCE = new WidgetChatListAdapterItemAttachment$configureFileData$1();

    public WidgetChatListAdapterItemAttachment$configureFileData$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        m.checkNotNullParameter(view, "it");
        TextView textView = (TextView) view;
        Context context = textView.getContext();
        m.checkNotNullExpressionValue(context, "it.context");
        CharSequence text = textView.getText();
        m.checkNotNullExpressionValue(text, "it.text");
        b.a.d.m.c(context, text, 0, 4);
    }
}
