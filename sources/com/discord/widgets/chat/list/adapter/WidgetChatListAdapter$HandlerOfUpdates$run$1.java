package com.discord.widgets.chat.list.adapter;

import androidx.core.app.NotificationCompat;
import kotlin.Metadata;
import rx.functions.Action0;
/* compiled from: WidgetChatListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", NotificationCompat.CATEGORY_CALL, "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapter$HandlerOfUpdates$run$1 implements Action0 {
    public static final WidgetChatListAdapter$HandlerOfUpdates$run$1 INSTANCE = new WidgetChatListAdapter$HandlerOfUpdates$run$1();

    @Override // rx.functions.Action0
    public final void call() {
    }
}
