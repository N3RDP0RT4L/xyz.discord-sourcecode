package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import com.discord.api.activity.Activity;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.api.sticker.BaseSticker;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppFragment;
import com.discord.app.AppPermissionsRequests;
import com.discord.models.guild.Guild;
import com.discord.models.message.Message;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChat;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.widgets.botuikit.ComponentProvider;
import com.discord.widgets.chat.input.AppFlexInputViewModel;
import com.discord.widgets.chat.list.FragmentLifecycleListener;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemCallMessage;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.MessageEntry;
import com.discord.widgets.chat.list.entries.NewMessagesEntry;
import com.discord.widgets.chat.list.model.WidgetChatListModel;
import d0.t.h0;
import d0.t.n;
import d0.t.n0;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Job;
import rx.functions.Action0;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0096\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 \u008f\u00012\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0010\u008f\u0001\u0090\u0001\u0091\u0001\u0092\u0001\u0093\u0001\u0094\u0001\u0095\u0001\u0096\u0001BC\b\u0007\u0012\b\u0010\u008c\u0001\u001a\u00030\u008b\u0001\u0012\u0006\u0010Z\u001a\u00020Y\u0012\u0006\u0010n\u001a\u00020m\u0012\u0006\u0010b\u001a\u00020a\u0012\n\b\u0002\u0010\u007f\u001a\u0004\u0018\u00010~\u0012\b\b\u0002\u0010U\u001a\u00020T¢\u0006\u0006\b\u008d\u0001\u0010\u008e\u0001J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\r\u0010\u0006\u001a\u00020\u0003¢\u0006\u0004\b\u0006\u0010\u0005J\r\u0010\u0007\u001a\u00020\u0003¢\u0006\u0004\b\u0007\u0010\u0005J!\u0010\r\u001a\u00020\u00032\n\u0010\n\u001a\u00060\bj\u0002`\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\r\u0010\u000eJ+\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\r\u0010\u0016\u001a\u00020\u0003¢\u0006\u0004\b\u0016\u0010\u0005J\r\u0010\u0017\u001a\u00020\u0003¢\u0006\u0004\b\u0017\u0010\u0005J)\u0010\u001c\u001a\u00020\u00032\n\u0010\n\u001a\u00060\bj\u0002`\t2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ\u0015\u0010 \u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b \u0010!J\u0015\u0010\"\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020\u001e¢\u0006\u0004\b\"\u0010!J!\u0010#\u001a\u00020\u00032\n\u0010\n\u001a\u00060\bj\u0002`\t2\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b#\u0010$J\u0019\u0010%\u001a\u00020\u00032\n\u0010\n\u001a\u00060\bj\u0002`\t¢\u0006\u0004\b%\u0010&J=\u0010/\u001a\u00020\u00032\n\u0010(\u001a\u00060\bj\u0002`'2\n\u0010\n\u001a\u00060\bj\u0002`\t2\u0006\u0010*\u001a\u00020)2\u0006\u0010,\u001a\u00020+2\u0006\u0010.\u001a\u00020-¢\u0006\u0004\b/\u00100J\u001d\u00105\u001a\u00020\u00032\u0006\u00102\u001a\u0002012\u0006\u00104\u001a\u000203¢\u0006\u0004\b5\u00106J1\u0010>\u001a\u00020\u00032\u0006\u00108\u001a\u0002072\f\u0010;\u001a\b\u0012\u0004\u0012\u00020:092\f\u0010=\u001a\b\u0012\u0004\u0012\u00020<09¢\u0006\u0004\b>\u0010?JC\u0010G\u001a\u00020\u00032\n\u0010A\u001a\u00060\bj\u0002`@2\n\u0010\n\u001a\u00060\bj\u0002`\t2\b\u0010B\u001a\u0004\u0018\u00010\b2\n\u0010D\u001a\u00060\u0011j\u0002`C2\u0006\u0010F\u001a\u00020E¢\u0006\u0004\bG\u0010HR\u0019\u0010J\u001a\u00020I8\u0006@\u0006¢\u0006\f\n\u0004\bJ\u0010K\u001a\u0004\bL\u0010MR\u0018\u0010O\u001a\u0004\u0018\u00010N8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bO\u0010PR\u001a\u0010R\u001a\u00060QR\u00020\u00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bR\u0010SR\u0019\u0010U\u001a\u00020T8\u0006@\u0006¢\u0006\f\n\u0004\bU\u0010V\u001a\u0004\bW\u0010XR\u0019\u0010Z\u001a\u00020Y8\u0006@\u0006¢\u0006\f\n\u0004\bZ\u0010[\u001a\u0004\b\\\u0010]R\u0018\u0010_\u001a\u0004\u0018\u00010^8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b_\u0010`R\u0019\u0010b\u001a\u00020a8\u0006@\u0006¢\u0006\f\n\u0004\bb\u0010c\u001a\u0004\bd\u0010eR\u0015\u0010i\u001a\u0004\u0018\u00010f8F@\u0006¢\u0006\u0006\u001a\u0004\bg\u0010hR\u001a\u0010k\u001a\u00060jR\u00020\u00008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bk\u0010lR\"\u0010n\u001a\u00020m8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bn\u0010o\u001a\u0004\bp\u0010q\"\u0004\br\u0010sR\u0016\u0010t\u001a\u00020\u001a8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bt\u0010uR*\u0010x\u001a\u00020v2\u0006\u0010w\u001a\u00020v8\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\bx\u0010y\u001a\u0004\bz\u0010{\"\u0004\b|\u0010}R\u001e\u0010\u007f\u001a\u0004\u0018\u00010~8\u0006@\u0006¢\u0006\u000f\n\u0005\b\u007f\u0010\u0080\u0001\u001a\u0006\b\u0081\u0001\u0010\u0082\u0001R(\u0010\u0083\u0001\u001a\u00020\u001a8\u0006@\u0006X\u0086\u000e¢\u0006\u0017\n\u0005\b\u0083\u0001\u0010u\u001a\u0006\b\u0084\u0001\u0010\u0085\u0001\"\u0006\b\u0086\u0001\u0010\u0087\u0001R\u001e\u0010\u0089\u0001\u001a\u00070\u0088\u0001R\u00020\u00008\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0089\u0001\u0010\u008a\u0001¨\u0006\u0097\u0001"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "", "publishInteractionState", "()V", "setHandlers", "disposeHandlers", "", "Lcom/discord/primitives/MessageId;", "messageId", "Lrx/functions/Action0;", "onCompleted", "scrollToMessageId", "(JLrx/functions/Action0;)V", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onResume", "onPause", "Lcom/discord/api/message/reaction/MessageReaction;", "reaction", "", "canAddReactions", "onReactionClicked", "(JLcom/discord/api/message/reaction/MessageReaction;Z)V", "Lcom/discord/api/channel/Channel;", "channel", "onThreadClicked", "(Lcom/discord/api/channel/Channel;)V", "onThreadLongClicked", "onReactionLongClicked", "(JLcom/discord/api/message/reaction/MessageReaction;)V", "onQuickAddReactionClicked", "(J)V", "Lcom/discord/primitives/UserId;", "authorId", "Lcom/discord/api/message/activity/MessageActivityType;", "messageActivityType", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/api/application/Application;", "application", "onUserActivityAction", "(JJLcom/discord/api/message/activity/MessageActivityType;Lcom/discord/api/activity/Activity;Lcom/discord/api/application/Application;)V", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/api/sticker/BaseSticker;", "sticker", "onStickerClicked", "(Lcom/discord/models/message/Message;Lcom/discord/api/sticker/BaseSticker;)V", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildEvent", "Ljava/lang/ref/WeakReference;", "Landroid/content/Context;", "weakContext", "Lcom/discord/app/AppFragment;", "weakFragment", "onShareButtonClick", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V", "Lcom/discord/primitives/ApplicationId;", "applicationId", "messageFlags", "Lcom/discord/widgets/botuikit/ComponentIndex;", "componentIndex", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "componentSendData", "onBotUiComponentClicked", "(JJLjava/lang/Long;ILcom/discord/restapi/RestAPIParams$ComponentInteractionData;)V", "Lcom/discord/widgets/botuikit/ComponentProvider;", "botUiComponentProvider", "Lcom/discord/widgets/botuikit/ComponentProvider;", "getBotUiComponentProvider", "()Lcom/discord/widgets/botuikit/ComponentProvider;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$ScrollToWithHighlight;", "scrollToWithHighlight", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$ScrollToWithHighlight;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfTouches;", "handlerOfTouches", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfTouches;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "getClock", "()Lcom/discord/utilities/time/Clock;", "Lcom/discord/app/AppPermissionsRequests;", "appPermissionsRequests", "Lcom/discord/app/AppPermissionsRequests;", "getAppPermissionsRequests", "()Lcom/discord/app/AppPermissionsRequests;", "Lkotlinx/coroutines/Job;", "lastUpdateJob", "Lkotlinx/coroutines/Job;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "eventHandler", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "getEventHandler", "()Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "Landroidx/recyclerview/widget/LinearLayoutManager;", "getLayoutManager", "()Landroidx/recyclerview/widget/LinearLayoutManager;", "layoutManager", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfScrolls;", "handlerOfScrolls", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfScrolls;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "getFragmentManager", "()Landroidx/fragment/app/FragmentManager;", "setFragmentManager", "(Landroidx/fragment/app/FragmentManager;)V", "isTouchedSinceLastJump", "Z", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;", "value", "data", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;", "getData", "()Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;", "setData", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;)V", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "getFlexInputViewModel", "()Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "mentionMeMessageLevelHighlighting", "getMentionMeMessageLevelHighlighting", "()Z", "setMentionMeMessageLevelHighlighting", "(Z)V", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfUpdates;", "handlerOfUpdates", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfUpdates;", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Lcom/discord/app/AppPermissionsRequests;Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;Lcom/discord/utilities/time/Clock;)V", "Companion", "Data", "EmptyData", "EventHandler", "HandlerOfScrolls", "HandlerOfTouches", "HandlerOfUpdates", "ScrollToWithHighlight", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapter extends MGRecyclerAdapterSimple<ChatListEntry> {
    public static final Companion Companion = new Companion(null);
    private static final int NEW_MESSAGES_MAX_SCROLLBACK_COUNT = 30;
    private static final int NEW_MESSAGES_MIN_SCROLLBACK_COUNT = 10;
    private final AppPermissionsRequests appPermissionsRequests;
    private final ComponentProvider botUiComponentProvider;
    private final Clock clock;
    private Data data;
    private final EventHandler eventHandler;
    private final AppFlexInputViewModel flexInputViewModel;
    private FragmentManager fragmentManager;
    private final HandlerOfScrolls handlerOfScrolls;
    private final HandlerOfTouches handlerOfTouches;
    private final HandlerOfUpdates handlerOfUpdates;
    private boolean isTouchedSinceLastJump;
    private Job lastUpdateJob;
    private boolean mentionMeMessageLevelHighlighting;
    private ScrollToWithHighlight scrollToWithHighlight;

    /* compiled from: WidgetChatListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\u0007¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Companion;", "", "", "numKnownMessages", "findBestNewMessagesPosition", "(I)I", "NEW_MESSAGES_MAX_SCROLLBACK_COUNT", "I", "NEW_MESSAGES_MIN_SCROLLBACK_COUNT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final int findBestNewMessagesPosition(int i) {
            if (-1 <= i && 10 > i) {
                return -1;
            }
            if (10 <= i && 30 > i) {
                return i;
            }
            return 30;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChatListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001R\u001a\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u001a\u0010\t\u001a\u00060\u0002j\u0002`\u00078&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\u0005R\u0018\u0010\r\u001a\u0004\u0018\u00010\n8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR \u0010\u0012\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u000f0\u000e8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R&\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0014\u0012\u0004\u0012\u00020\u00150\u00138&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0016\u0010\u001a\u001a\u00020\u00198&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u001a\u0010\u001d\u001a\u00060\u0002j\u0002`\u00148&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0005R\u001c\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b \u0010!R\u001a\u0010%\u001a\u00060\u0002j\u0002`#8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b$\u0010\u0005R\u001a\u0010'\u001a\u00060\u0002j\u0002`\u00038&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b&\u0010\u0005¨\u0006("}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;", "", "", "Lcom/discord/primitives/MessageId;", "getOldestMessageId", "()J", "oldestMessageId", "Lcom/discord/primitives/GuildId;", "getGuildId", "guildId", "Lcom/discord/models/guild/Guild;", "getGuild", "()Lcom/discord/models/guild/Guild;", "guild", "", "Lcom/discord/primitives/RoleId;", "getMyRoleIds", "()Ljava/util/Set;", "myRoleIds", "", "Lcom/discord/primitives/ChannelId;", "", "getChannelNames", "()Ljava/util/Map;", "channelNames", "", "isSpoilerClickAllowed", "()Z", "getChannelId", "channelId", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "getList", "()Ljava/util/List;", "list", "Lcom/discord/primitives/UserId;", "getUserId", "userId", "getNewMessagesMarkerMessageId", "newMessagesMarkerMessageId", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface Data {
        long getChannelId();

        Map<Long, String> getChannelNames();

        Guild getGuild();

        long getGuildId();

        List<ChatListEntry> getList();

        Set<Long> getMyRoleIds();

        long getNewMessagesMarkerMessageId();

        long getOldestMessageId();

        long getUserId();

        boolean isSpoilerClickAllowed();
    }

    /* compiled from: WidgetChatListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b.\u0010/R \u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R \u0010\t\u001a\u00060\u0002j\u0002`\b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\t\u0010\u0005\u001a\u0004\b\n\u0010\u0007R \u0010\f\u001a\u00060\u0002j\u0002`\u000b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\f\u0010\u0005\u001a\u0004\b\r\u0010\u0007R,\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u000f0\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\"\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00148\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R \u0010\u001b\u001a\u00060\u0002j\u0002`\u001a8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001b\u0010\u0005\u001a\u0004\b\u001c\u0010\u0007R \u0010\u001d\u001a\u00060\u0002j\u0002`\b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001d\u0010\u0005\u001a\u0004\b\u001e\u0010\u0007R\u001c\u0010 \u001a\u00020\u001f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b \u0010\"R&\u0010%\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`$0#8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u001e\u0010*\u001a\u0004\u0018\u00010)8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-¨\u00060"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EmptyData;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "getChannelId", "()J", "Lcom/discord/primitives/MessageId;", "oldestMessageId", "getOldestMessageId", "Lcom/discord/primitives/GuildId;", "guildId", "getGuildId", "", "", "channelNames", "Ljava/util/Map;", "getChannelNames", "()Ljava/util/Map;", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "list", "Ljava/util/List;", "getList", "()Ljava/util/List;", "Lcom/discord/primitives/UserId;", "userId", "getUserId", "newMessagesMarkerMessageId", "getNewMessagesMarkerMessageId", "", "isSpoilerClickAllowed", "Z", "()Z", "", "Lcom/discord/primitives/RoleId;", "myRoleIds", "Ljava/util/Set;", "getMyRoleIds", "()Ljava/util/Set;", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/guild/Guild;", "getGuild", "()Lcom/discord/models/guild/Guild;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EmptyData implements Data {
        private final long channelId;
        private final Guild guild;
        private final long guildId;
        private final boolean isSpoilerClickAllowed;
        private final long newMessagesMarkerMessageId;
        private final long oldestMessageId;
        private final long userId;
        private final Map<Long, String> channelNames = h0.emptyMap();
        private final List<ChatListEntry> list = n.emptyList();
        private final Set<Long> myRoleIds = n0.emptySet();

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getChannelId() {
            return this.channelId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Map<Long, String> getChannelNames() {
            return this.channelNames;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Guild getGuild() {
            return this.guild;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getGuildId() {
            return this.guildId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public List<ChatListEntry> getList() {
            return this.list;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public Set<Long> getMyRoleIds() {
            return this.myRoleIds;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getNewMessagesMarkerMessageId() {
            return this.newMessagesMarkerMessageId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getOldestMessageId() {
            return this.oldestMessageId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public long getUserId() {
            return this.userId;
        }

        @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
        public boolean isSpoilerClickAllowed() {
            return this.isSpoilerClickAllowed;
        }
    }

    /* compiled from: WidgetChatListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ì\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\f\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010\u000b\u001a\u00060\u0007j\u0002`\nH\u0016¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u0017\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0018\u0010\u0017J'\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u001dH\u0016¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010!\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b!\u0010\"J#\u0010%\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\n\u0010$\u001a\u00060\u0007j\u0002`#H\u0016¢\u0006\u0004\b%\u0010&J#\u0010'\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\n\u0010$\u001a\u00060\u0007j\u0002`#H\u0016¢\u0006\u0004\b'\u0010&J'\u0010(\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\u000e\u0010$\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`#H\u0016¢\u0006\u0004\b(\u0010)J\u0017\u0010*\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b*\u0010\"JO\u00101\u001a\u00020\u00042\n\u0010$\u001a\u00060\u0007j\u0002`#2\n\u0010,\u001a\u00060\u0007j\u0002`+2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010-\u001a\u00060\u0007j\u0002`\n2\u0006\u0010/\u001a\u00020.2\u0006\u00100\u001a\u00020\u0010H\u0016¢\u0006\u0004\b1\u00102J;\u00103\u001a\u00020\u00042\n\u0010$\u001a\u00060\u0007j\u0002`#2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010-\u001a\u00060\u0007j\u0002`\n2\u0006\u0010/\u001a\u00020.H\u0016¢\u0006\u0004\b3\u00104J3\u00106\u001a\u00020\u00042\n\u00105\u001a\u00060\u0007j\u0002`+2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010$\u001a\u00060\u0007j\u0002`#H\u0016¢\u0006\u0004\b6\u00107J?\u00108\u001a\u00020\u00042\n\u0010$\u001a\u00060\u0007j\u0002`#2\n\u0010,\u001a\u00060\u0007j\u0002`+2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010-\u001a\u00060\u0007j\u0002`\nH\u0016¢\u0006\u0004\b8\u00109J\u001f\u0010=\u001a\u00020\u00102\u0006\u0010;\u001a\u00020:2\u0006\u0010<\u001a\u00020\u001dH\u0016¢\u0006\u0004\b=\u0010>JK\u0010F\u001a\u00020\u00042\n\u0010?\u001a\u00060\u0007j\u0002`+2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010-\u001a\u00060\u0007j\u0002`\n2\u0006\u0010A\u001a\u00020@2\u0006\u0010C\u001a\u00020B2\u0006\u0010E\u001a\u00020DH\u0016¢\u0006\u0004\bF\u0010GJ\u000f\u0010H\u001a\u00020\u0004H\u0016¢\u0006\u0004\bH\u0010IJ#\u0010M\u001a\u00020\u00042\n\u0010J\u001a\u00060\u0007j\u0002`\b2\u0006\u0010L\u001a\u00020KH\u0016¢\u0006\u0004\bM\u0010NJ\u001f\u0010Q\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010P\u001a\u00020OH\u0016¢\u0006\u0004\bQ\u0010RJ+\u0010V\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0006\u0010T\u001a\u00020S2\u0006\u0010P\u001a\u00020UH\u0016¢\u0006\u0004\bV\u0010WJ\u0017\u0010X\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\bX\u0010\"JK\u0010]\u001a\u00020\u00042\u0006\u0010Y\u001a\u00020\u00072\b\u0010$\u001a\u0004\u0018\u00010\u00072\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010-\u001a\u00020\u00072\u0006\u0010Z\u001a\u00020\u00072\u0006\u0010[\u001a\u00020\u00072\b\u0010\\\u001a\u0004\u0018\u00010\u001dH\u0016¢\u0006\u0004\b]\u0010^Ja\u0010e\u001a\u00020\u00042\n\u0010[\u001a\u00060\u0007j\u0002`_2\u000e\u0010$\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`#2\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\n\u0010-\u001a\u00060\u0007j\u0002`\n2\b\u0010`\u001a\u0004\u0018\u00010\u00072\n\u0010b\u001a\u00060Sj\u0002`a2\u0006\u0010d\u001a\u00020cH\u0016¢\u0006\u0004\be\u0010fJ'\u0010g\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010P\u001a\u00020OH\u0016¢\u0006\u0004\bg\u0010hJ3\u0010p\u001a\u00020\u00042\u0006\u0010j\u001a\u00020i2\f\u0010m\u001a\b\u0012\u0004\u0012\u00020l0k2\f\u0010o\u001a\b\u0012\u0004\u0012\u00020n0kH\u0016¢\u0006\u0004\bp\u0010q¨\u0006r"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "", "Lcom/discord/stores/StoreChat$InteractionState;", "interactionState", "", "onInteractionStateUpdated", "(Lcom/discord/stores/StoreChat$InteractionState;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "oldestMessageId", "onOldestMessageId", "(JJ)V", "Lcom/discord/models/message/Message;", "message", "", "isThreadStarterMessage", "onMessageClicked", "(Lcom/discord/models/message/Message;Z)V", "Lcom/discord/api/channel/Channel;", "channel", "onThreadClicked", "(Lcom/discord/api/channel/Channel;)V", "onThreadLongClicked", "", "formattedMessage", "onMessageLongClicked", "(Lcom/discord/models/message/Message;Ljava/lang/CharSequence;Z)V", "", "url", "onUrlLongClicked", "(Ljava/lang/String;)V", "onOpenPinsClicked", "(Lcom/discord/models/message/Message;)V", "Lcom/discord/primitives/GuildId;", "guildId", "onMessageAuthorNameClicked", "(Lcom/discord/models/message/Message;J)V", "onMessageAuthorAvatarClicked", "onMessageAuthorLongClicked", "(Lcom/discord/models/message/Message;Ljava/lang/Long;)V", "onMessageBlockedGroupClicked", "Lcom/discord/primitives/UserId;", "myUserId", "messageId", "Lcom/discord/api/message/reaction/MessageReaction;", "reaction", "canAddReactions", "onReactionClicked", "(JJJJLcom/discord/api/message/reaction/MessageReaction;Z)V", "onReactionLongClicked", "(JJJLcom/discord/api/message/reaction/MessageReaction;)V", "userId", "onUserMentionClicked", "(JJJ)V", "onQuickAddReactionClicked", "(JJJJ)V", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "fileName", "onQuickDownloadClicked", "(Landroid/net/Uri;Ljava/lang/String;)Z", "authorId", "Lcom/discord/api/message/activity/MessageActivityType;", "messageActivityType", "Lcom/discord/api/activity/Activity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lcom/discord/api/application/Application;", "application", "onUserActivityAction", "(JJJLcom/discord/api/message/activity/MessageActivityType;Lcom/discord/api/activity/Activity;Lcom/discord/api/application/Application;)V", "onListClicked", "()V", "voiceChannelId", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;", "callStatus", "onCallMessageClicked", "(JLcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemCallMessage$CallStatus;)V", "Lcom/discord/api/sticker/BaseSticker;", "sticker", "onStickerClicked", "(Lcom/discord/models/message/Message;Lcom/discord/api/sticker/BaseSticker;)V", "", "channelType", "Lcom/discord/api/sticker/Sticker;", "onSendGreetMessageClicked", "(JILcom/discord/api/sticker/Sticker;)V", "onDismissClicked", "interactionId", "interactionUserId", "applicationId", "messageNonce", "onCommandClicked", "(JLjava/lang/Long;JJJJLjava/lang/String;)V", "Lcom/discord/primitives/ApplicationId;", "messageFlags", "Lcom/discord/widgets/botuikit/ComponentIndex;", "componentIndex", "Lcom/discord/restapi/RestAPIParams$ComponentInteractionData;", "componentSendData", "onBotUiComponentClicked", "(JLjava/lang/Long;JJLjava/lang/Long;ILcom/discord/restapi/RestAPIParams$ComponentInteractionData;)V", "onWelcomeCtaClicked", "(Lcom/discord/models/message/Message;Lcom/discord/api/channel/Channel;Lcom/discord/api/sticker/BaseSticker;)V", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildEvent", "Ljava/lang/ref/WeakReference;", "Landroid/content/Context;", "weakContext", "Lcom/discord/app/AppFragment;", "weakFragment", "onShareButtonClick", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface EventHandler {

        /* compiled from: WidgetChatListAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DefaultImpls {
            public static void onBotUiComponentClicked(EventHandler eventHandler, long j, Long l, long j2, long j3, Long l2, int i, RestAPIParams.ComponentInteractionData componentInteractionData) {
                m.checkNotNullParameter(componentInteractionData, "componentSendData");
            }

            public static void onCallMessageClicked(EventHandler eventHandler, long j, WidgetChatListAdapterItemCallMessage.CallStatus callStatus) {
                m.checkNotNullParameter(callStatus, "callStatus");
            }

            public static void onCommandClicked(EventHandler eventHandler, long j, Long l, long j2, long j3, long j4, long j5, String str) {
            }

            public static void onDismissClicked(EventHandler eventHandler, Message message) {
                m.checkNotNullParameter(message, "message");
            }

            public static void onInteractionStateUpdated(EventHandler eventHandler, StoreChat.InteractionState interactionState) {
                m.checkNotNullParameter(interactionState, "interactionState");
            }

            public static void onListClicked(EventHandler eventHandler) {
            }

            public static void onMessageAuthorAvatarClicked(EventHandler eventHandler, Message message, long j) {
                m.checkNotNullParameter(message, "message");
            }

            public static void onMessageAuthorLongClicked(EventHandler eventHandler, Message message, Long l) {
                m.checkNotNullParameter(message, "message");
            }

            public static void onMessageAuthorNameClicked(EventHandler eventHandler, Message message, long j) {
                m.checkNotNullParameter(message, "message");
            }

            public static void onMessageBlockedGroupClicked(EventHandler eventHandler, Message message) {
                m.checkNotNullParameter(message, "message");
            }

            public static void onMessageClicked(EventHandler eventHandler, Message message, boolean z2) {
                m.checkNotNullParameter(message, "message");
            }

            public static void onMessageLongClicked(EventHandler eventHandler, Message message, CharSequence charSequence, boolean z2) {
                m.checkNotNullParameter(message, "message");
                m.checkNotNullParameter(charSequence, "formattedMessage");
            }

            public static void onOldestMessageId(EventHandler eventHandler, long j, long j2) {
            }

            public static void onOpenPinsClicked(EventHandler eventHandler, Message message) {
                m.checkNotNullParameter(message, "message");
            }

            public static void onQuickAddReactionClicked(EventHandler eventHandler, long j, long j2, long j3, long j4) {
            }

            public static boolean onQuickDownloadClicked(EventHandler eventHandler, Uri uri, String str) {
                m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
                m.checkNotNullParameter(str, "fileName");
                return false;
            }

            public static void onReactionClicked(EventHandler eventHandler, long j, long j2, long j3, long j4, MessageReaction messageReaction, boolean z2) {
                m.checkNotNullParameter(messageReaction, "reaction");
            }

            public static void onReactionLongClicked(EventHandler eventHandler, long j, long j2, long j3, MessageReaction messageReaction) {
                m.checkNotNullParameter(messageReaction, "reaction");
            }

            public static void onSendGreetMessageClicked(EventHandler eventHandler, long j, int i, Sticker sticker) {
                m.checkNotNullParameter(sticker, "sticker");
            }

            public static void onShareButtonClick(EventHandler eventHandler, GuildScheduledEvent guildScheduledEvent, WeakReference<Context> weakReference, WeakReference<AppFragment> weakReference2) {
                m.checkNotNullParameter(guildScheduledEvent, "guildEvent");
                m.checkNotNullParameter(weakReference, "weakContext");
                m.checkNotNullParameter(weakReference2, "weakFragment");
            }

            public static void onStickerClicked(EventHandler eventHandler, Message message, BaseSticker baseSticker) {
                m.checkNotNullParameter(message, "message");
                m.checkNotNullParameter(baseSticker, "sticker");
            }

            public static void onThreadClicked(EventHandler eventHandler, Channel channel) {
                m.checkNotNullParameter(channel, "channel");
            }

            public static void onThreadLongClicked(EventHandler eventHandler, Channel channel) {
                m.checkNotNullParameter(channel, "channel");
            }

            public static void onUrlLongClicked(EventHandler eventHandler, String str) {
                m.checkNotNullParameter(str, "url");
            }

            public static void onUserActivityAction(EventHandler eventHandler, long j, long j2, long j3, MessageActivityType messageActivityType, Activity activity, Application application) {
                m.checkNotNullParameter(messageActivityType, "messageActivityType");
                m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
                m.checkNotNullParameter(application, "application");
            }

            public static void onUserMentionClicked(EventHandler eventHandler, long j, long j2, long j3) {
            }

            public static void onWelcomeCtaClicked(EventHandler eventHandler, Message message, Channel channel, BaseSticker baseSticker) {
                m.checkNotNullParameter(message, "message");
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(baseSticker, "sticker");
            }
        }

        void onBotUiComponentClicked(long j, Long l, long j2, long j3, Long l2, int i, RestAPIParams.ComponentInteractionData componentInteractionData);

        void onCallMessageClicked(long j, WidgetChatListAdapterItemCallMessage.CallStatus callStatus);

        void onCommandClicked(long j, Long l, long j2, long j3, long j4, long j5, String str);

        void onDismissClicked(Message message);

        void onInteractionStateUpdated(StoreChat.InteractionState interactionState);

        void onListClicked();

        void onMessageAuthorAvatarClicked(Message message, long j);

        void onMessageAuthorLongClicked(Message message, Long l);

        void onMessageAuthorNameClicked(Message message, long j);

        void onMessageBlockedGroupClicked(Message message);

        void onMessageClicked(Message message, boolean z2);

        void onMessageLongClicked(Message message, CharSequence charSequence, boolean z2);

        void onOldestMessageId(long j, long j2);

        void onOpenPinsClicked(Message message);

        void onQuickAddReactionClicked(long j, long j2, long j3, long j4);

        boolean onQuickDownloadClicked(Uri uri, String str);

        void onReactionClicked(long j, long j2, long j3, long j4, MessageReaction messageReaction, boolean z2);

        void onReactionLongClicked(long j, long j2, long j3, MessageReaction messageReaction);

        void onSendGreetMessageClicked(long j, int i, Sticker sticker);

        void onShareButtonClick(GuildScheduledEvent guildScheduledEvent, WeakReference<Context> weakReference, WeakReference<AppFragment> weakReference2);

        void onStickerClicked(Message message, BaseSticker baseSticker);

        void onThreadClicked(Channel channel);

        void onThreadLongClicked(Channel channel);

        void onUrlLongClicked(String str);

        void onUserActivityAction(long j, long j2, long j3, MessageActivityType messageActivityType, Activity activity, Application application);

        void onUserMentionClicked(long j, long j2, long j3);

        void onWelcomeCtaClicked(Message message, Channel channel, BaseSticker baseSticker);
    }

    /* compiled from: WidgetChatListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u001a\u0010\u000b\u001a\u00060\tj\u0002`\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u0005\u001a\u00020\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0005\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfScrolls;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "", "scrollState", "", "onScrollStateChanged", "(Landroidx/recyclerview/widget/RecyclerView;I)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "I", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class HandlerOfScrolls extends RecyclerView.OnScrollListener {
        private long channelId;
        private int scrollState;

        public HandlerOfScrolls() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            m.checkNotNullParameter(recyclerView, "recyclerView");
            super.onScrollStateChanged(recyclerView, i);
            if (this.scrollState != i || WidgetChatListAdapter.this.getData().getChannelId() != this.channelId) {
                this.scrollState = i;
                this.channelId = WidgetChatListAdapter.this.getData().getChannelId();
                WidgetChatListAdapter.this.publishInteractionState();
            }
        }
    }

    /* compiled from: WidgetChatListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\f\u0010\rJ#\u0010\u0007\u001a\u00020\u00062\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfTouches;", "Landroid/view/View$OnTouchListener;", "Landroid/view/View;", "v", "Landroid/view/MotionEvent;", "event", "", "onTouch", "(Landroid/view/View;Landroid/view/MotionEvent;)Z", "Landroid/view/GestureDetector;", "tapGestureDetector", "Landroid/view/GestureDetector;", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class HandlerOfTouches implements View.OnTouchListener {
        private final GestureDetector tapGestureDetector;

        public HandlerOfTouches() {
            this.tapGestureDetector = new GestureDetector(WidgetChatListAdapter.this.getContext(), new GestureDetector.SimpleOnGestureListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapter$HandlerOfTouches$tapGestureDetector$1
                @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
                public boolean onSingleTapUp(MotionEvent motionEvent) {
                    WidgetChatListAdapter.this.getEventHandler().onListClicked();
                    return super.onSingleTapUp(motionEvent);
                }
            });
        }

        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            boolean z2 = motionEvent != null && motionEvent.getAction() == 2;
            if (!WidgetChatListAdapter.this.isTouchedSinceLastJump && z2) {
                WidgetChatListAdapter.this.isTouchedSinceLastJump = true;
                WidgetChatListAdapter.this.publishInteractionState();
            }
            return this.tapGestureDetector.onTouchEvent(motionEvent);
        }
    }

    /* compiled from: WidgetChatListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004R\u001a\u0010\u0007\u001a\u00060\u0005j\u0002`\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u001a\u0010\n\u001a\u00060\u0005j\u0002`\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\n\u0010\b¨\u0006\r"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$HandlerOfUpdates;", "Ljava/lang/Runnable;", "", "run", "()V", "", "Lcom/discord/primitives/MessageId;", "oldestMessageId", "J", "Lcom/discord/primitives/ChannelId;", "channelId", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final class HandlerOfUpdates implements Runnable {
        private long channelId;
        private long oldestMessageId;

        public HandlerOfUpdates() {
        }

        @Override // java.lang.Runnable
        public void run() {
            if (this.channelId != WidgetChatListAdapter.this.getData().getChannelId()) {
                boolean z2 = this.channelId != 0;
                this.channelId = WidgetChatListAdapter.this.getData().getChannelId();
                if (z2) {
                    WidgetChatListAdapter.this.scrollToMessageId(0L, WidgetChatListAdapter$HandlerOfUpdates$run$1.INSTANCE);
                }
                WidgetChatListAdapter.this.isTouchedSinceLastJump = false;
            }
            if (this.oldestMessageId != WidgetChatListAdapter.this.getData().getOldestMessageId()) {
                this.oldestMessageId = WidgetChatListAdapter.this.getData().getOldestMessageId();
                WidgetChatListAdapter.this.getEventHandler().onOldestMessageId(this.channelId, this.oldestMessageId);
            }
            WidgetChatListAdapter.this.publishInteractionState();
        }
    }

    /* compiled from: WidgetChatListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\n\u0010\u001d\u001a\u00060\u001bj\u0002`\u001c\u0012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00030\u0014¢\u0006\u0004\b!\u0010\"J\u0013\u0010\u0004\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0006\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0019\u0010\u000b\u001a\u00020\n*\b\u0012\u0004\u0012\u00020\t0\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\r\u0010\r\u001a\u00020\u0003¢\u0006\u0004\b\r\u0010\u0007J\u000f\u0010\u000e\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u000e\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00030\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u001d\u0010\u001d\u001a\u00060\u001bj\u0002`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 ¨\u0006#"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$ScrollToWithHighlight;", "Ljava/lang/Runnable;", "Landroid/view/View;", "", "animateHighlight", "(Landroid/view/View;)V", "scheduleRetry", "()V", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "", "getNewMessageEntryIndex", "(Ljava/util/List;)I", "cancel", "run", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "getAdapter", "()Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "Lkotlin/Function0;", "onCompleted", "Lkotlin/jvm/functions/Function0;", "getOnCompleted", "()Lkotlin/jvm/functions/Function0;", "attempts", "I", "", "Lcom/discord/primitives/MessageId;", "messageId", "J", "getMessageId", "()J", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;JLkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ScrollToWithHighlight implements Runnable {
        private final WidgetChatListAdapter adapter;
        private int attempts;
        private final long messageId;
        private final Function0<Unit> onCompleted;

        public ScrollToWithHighlight(WidgetChatListAdapter widgetChatListAdapter, long j, Function0<Unit> function0) {
            m.checkNotNullParameter(widgetChatListAdapter, "adapter");
            m.checkNotNullParameter(function0, "onCompleted");
            this.adapter = widgetChatListAdapter;
            this.messageId = j;
            this.onCompleted = function0;
            widgetChatListAdapter.getRecycler().post(this);
        }

        private final void animateHighlight(View view) {
            view.setBackgroundResource(R.drawable.drawable_bg_highlight);
            Drawable background = view.getBackground();
            Objects.requireNonNull(background, "null cannot be cast to non-null type android.graphics.drawable.TransitionDrawable");
            TransitionDrawable transitionDrawable = (TransitionDrawable) background;
            transitionDrawable.startTransition(500);
            CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(view);
            if (coroutineScope != null) {
                f.H0(coroutineScope, null, null, new WidgetChatListAdapter$ScrollToWithHighlight$animateHighlight$1(transitionDrawable, null), 3, null);
            }
        }

        private final int getNewMessageEntryIndex(List<? extends ChatListEntry> list) {
            boolean z2;
            long j = this.messageId;
            boolean z3 = j == 0;
            if (j == 1) {
                return 0;
            }
            Integer num = null;
            if (j == 0) {
                Long valueOf = Long.valueOf(this.adapter.getData().getNewMessagesMarkerMessageId());
                if (!(valueOf.longValue() > 0)) {
                    valueOf = null;
                }
                if (valueOf == null) {
                    return 0;
                }
                j = valueOf.longValue();
            }
            int i = -1;
            if (j <= 0) {
                return -1;
            }
            Iterator<? extends ChatListEntry> it = list.iterator();
            int i2 = 0;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ChatListEntry next = it.next();
                if ((next instanceof MessageEntry) && ((MessageEntry) next).getMessage().getId() <= j) {
                    i = i2;
                    break;
                }
                i2++;
            }
            if (i < 0 && z3) {
                i = WidgetChatListAdapter.Companion.findBestNewMessagesPosition(n.getLastIndex(list));
            }
            Iterator<Integer> it2 = d0.d0.f.downTo(i, 0).iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Integer next2 = it2.next();
                ChatListEntry chatListEntry = list.get(next2.intValue());
                if (!(chatListEntry instanceof NewMessagesEntry) || ((NewMessagesEntry) chatListEntry).getMessageId() != j) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    num = next2;
                    break;
                }
            }
            Integer num2 = num;
            return num2 != null ? num2.intValue() : i;
        }

        private final void scheduleRetry() {
            int i = this.attempts;
            if (i < 20) {
                this.attempts = i + 1;
                CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(this.adapter.getRecycler());
                if (coroutineScope != null) {
                    f.H0(coroutineScope, null, null, new WidgetChatListAdapter$ScrollToWithHighlight$scheduleRetry$1(this, null), 3, null);
                    return;
                }
                return;
            }
            this.onCompleted.invoke();
        }

        public final void cancel() {
            this.adapter.getRecycler().removeCallbacks(this);
        }

        public final WidgetChatListAdapter getAdapter() {
            return this.adapter;
        }

        public final long getMessageId() {
            return this.messageId;
        }

        public final Function0<Unit> getOnCompleted() {
            return this.onCompleted;
        }

        @Override // java.lang.Runnable
        public void run() {
            Data data = this.adapter.getData();
            View view = null;
            if (!(data instanceof WidgetChatListModel)) {
                data = null;
            }
            WidgetChatListModel widgetChatListModel = (WidgetChatListModel) data;
            if (widgetChatListModel == null || !widgetChatListModel.isLoadingMessages()) {
                int newMessageEntryIndex = getNewMessageEntryIndex(this.adapter.getData().getList());
                if (newMessageEntryIndex < 0) {
                    scheduleRetry();
                    return;
                }
                int height = (int) (this.adapter.getRecycler().getHeight() / 2.0f);
                LinearLayoutManager layoutManager = this.adapter.getLayoutManager();
                if (layoutManager != null) {
                    layoutManager.scrollToPositionWithOffset(newMessageEntryIndex, height);
                    view = layoutManager.findViewByPosition(newMessageEntryIndex);
                }
                if (view != null) {
                    if (!(newMessageEntryIndex < this.adapter.getData().getList().size() && (this.adapter.getData().getList().get(newMessageEntryIndex) instanceof NewMessagesEntry))) {
                        animateHighlight(view);
                    }
                    this.onCompleted.invoke();
                    return;
                }
                scheduleRetry();
                return;
            }
            scheduleRetry();
        }
    }

    public WidgetChatListAdapter(RecyclerView recyclerView, AppPermissionsRequests appPermissionsRequests, FragmentManager fragmentManager, EventHandler eventHandler) {
        this(recyclerView, appPermissionsRequests, fragmentManager, eventHandler, null, null, 48, null);
    }

    public WidgetChatListAdapter(RecyclerView recyclerView, AppPermissionsRequests appPermissionsRequests, FragmentManager fragmentManager, EventHandler eventHandler, AppFlexInputViewModel appFlexInputViewModel) {
        this(recyclerView, appPermissionsRequests, fragmentManager, eventHandler, appFlexInputViewModel, null, 32, null);
    }

    public /* synthetic */ WidgetChatListAdapter(RecyclerView recyclerView, AppPermissionsRequests appPermissionsRequests, FragmentManager fragmentManager, EventHandler eventHandler, AppFlexInputViewModel appFlexInputViewModel, Clock clock, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(recyclerView, appPermissionsRequests, fragmentManager, eventHandler, (i & 16) != 0 ? null : appFlexInputViewModel, (i & 32) != 0 ? ClockFactory.get() : clock);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void publishInteractionState() {
        Data data = this.data;
        if (!(data instanceof WidgetChatListModel)) {
            data = null;
        }
        WidgetChatListModel widgetChatListModel = (WidgetChatListModel) data;
        this.eventHandler.onInteractionStateUpdated(new StoreChat.InteractionState(this.data.getChannelId(), widgetChatListModel != null ? widgetChatListModel.getNewestKnownMessageId() : 0L, this.isTouchedSinceLastJump, getLayoutManager()));
    }

    public final void disposeHandlers() {
        setOnUpdated(WidgetChatListAdapter$disposeHandlers$1.INSTANCE);
        Job job = this.lastUpdateJob;
        if (job != null) {
            f.t(job, null, 1, null);
        }
        getRecycler().setOnTouchListener(null);
        getRecycler().removeOnScrollListener(this.handlerOfScrolls);
        this.eventHandler.onInteractionStateUpdated(new StoreChat.InteractionState(this.data.getChannelId(), 0L, this.isTouchedSinceLastJump, (LinearLayoutManager) null));
    }

    public final AppPermissionsRequests getAppPermissionsRequests() {
        return this.appPermissionsRequests;
    }

    public final ComponentProvider getBotUiComponentProvider() {
        return this.botUiComponentProvider;
    }

    public final Clock getClock() {
        return this.clock;
    }

    public final Data getData() {
        return this.data;
    }

    public final EventHandler getEventHandler() {
        return this.eventHandler;
    }

    public final AppFlexInputViewModel getFlexInputViewModel() {
        return this.flexInputViewModel;
    }

    public final FragmentManager getFragmentManager() {
        return this.fragmentManager;
    }

    public final LinearLayoutManager getLayoutManager() {
        RecyclerView.LayoutManager layoutManager = getRecycler().getLayoutManager();
        if (!(layoutManager instanceof LinearLayoutManager)) {
            layoutManager = null;
        }
        return (LinearLayoutManager) layoutManager;
    }

    public final boolean getMentionMeMessageLevelHighlighting() {
        return this.mentionMeMessageLevelHighlighting;
    }

    public final void onBotUiComponentClicked(long j, long j2, Long l, int i, RestAPIParams.ComponentInteractionData componentInteractionData) {
        m.checkNotNullParameter(componentInteractionData, "componentSendData");
        EventHandler eventHandler = this.eventHandler;
        Guild guild = this.data.getGuild();
        eventHandler.onBotUiComponentClicked(j, guild != null ? Long.valueOf(guild.getId()) : null, this.data.getChannelId(), j2, l, i, componentInteractionData);
    }

    public final void onPause() {
        int childCount = getRecycler().getChildCount();
        for (int i = 0; i < childCount; i++) {
            RecyclerView.ViewHolder childViewHolder = getRecycler().getChildViewHolder(getRecycler().getChildAt(i));
            if (childViewHolder instanceof FragmentLifecycleListener) {
                ((FragmentLifecycleListener) childViewHolder).onPause();
            }
        }
    }

    public final void onQuickAddReactionClicked(long j) {
        this.eventHandler.onQuickAddReactionClicked(this.data.getGuildId(), this.data.getUserId(), this.data.getChannelId(), j);
    }

    public final void onReactionClicked(long j, MessageReaction messageReaction, boolean z2) {
        m.checkNotNullParameter(messageReaction, "reaction");
        this.eventHandler.onReactionClicked(this.data.getGuildId(), this.data.getUserId(), this.data.getChannelId(), j, messageReaction, z2);
    }

    public final void onReactionLongClicked(long j, MessageReaction messageReaction) {
        m.checkNotNullParameter(messageReaction, "reaction");
        this.eventHandler.onReactionLongClicked(this.data.getGuildId(), this.data.getChannelId(), j, messageReaction);
    }

    public final void onResume() {
        int childCount = getRecycler().getChildCount();
        for (int i = 0; i < childCount; i++) {
            RecyclerView.ViewHolder childViewHolder = getRecycler().getChildViewHolder(getRecycler().getChildAt(i));
            if (childViewHolder instanceof FragmentLifecycleListener) {
                ((FragmentLifecycleListener) childViewHolder).onResume();
            }
        }
    }

    public final void onShareButtonClick(GuildScheduledEvent guildScheduledEvent, WeakReference<Context> weakReference, WeakReference<AppFragment> weakReference2) {
        m.checkNotNullParameter(guildScheduledEvent, "guildEvent");
        m.checkNotNullParameter(weakReference, "weakContext");
        m.checkNotNullParameter(weakReference2, "weakFragment");
        this.eventHandler.onShareButtonClick(guildScheduledEvent, weakReference, weakReference2);
    }

    public final void onStickerClicked(Message message, BaseSticker baseSticker) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(baseSticker, "sticker");
        this.eventHandler.onStickerClicked(message, baseSticker);
    }

    public final void onThreadClicked(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        this.eventHandler.onThreadClicked(channel);
    }

    public final void onThreadLongClicked(Channel channel) {
        m.checkNotNullParameter(channel, "channel");
        this.eventHandler.onThreadLongClicked(channel);
    }

    public final void onUserActivityAction(long j, long j2, MessageActivityType messageActivityType, Activity activity, Application application) {
        m.checkNotNullParameter(messageActivityType, "messageActivityType");
        m.checkNotNullParameter(activity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        m.checkNotNullParameter(application, "application");
        this.eventHandler.onUserActivityAction(j, this.data.getChannelId(), j2, messageActivityType, activity, application);
    }

    public final void scrollToMessageId(long j, Action0 action0) {
        m.checkNotNullParameter(action0, "onCompleted");
        ScrollToWithHighlight scrollToWithHighlight = this.scrollToWithHighlight;
        if (scrollToWithHighlight != null && scrollToWithHighlight.getMessageId() > 0 && j <= 0) {
            action0.call();
            return;
        }
        this.isTouchedSinceLastJump = false;
        ScrollToWithHighlight scrollToWithHighlight2 = this.scrollToWithHighlight;
        if (scrollToWithHighlight2 != null) {
            scrollToWithHighlight2.cancel();
        }
        this.scrollToWithHighlight = new ScrollToWithHighlight(this, j, new WidgetChatListAdapter$scrollToMessageId$1(this, action0));
    }

    public final void setData(Data data) {
        m.checkNotNullParameter(data, "value");
        this.data = data;
        setData(data.getList());
    }

    public final void setFragmentManager(FragmentManager fragmentManager) {
        m.checkNotNullParameter(fragmentManager, "<set-?>");
        this.fragmentManager = fragmentManager;
    }

    public final void setHandlers() {
        setOnUpdated(new WidgetChatListAdapter$setHandlers$1(this));
    }

    public final void setMentionMeMessageLevelHighlighting(boolean z2) {
        this.mentionMeMessageLevelHighlighting = z2;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapter(RecyclerView recyclerView, AppPermissionsRequests appPermissionsRequests, FragmentManager fragmentManager, EventHandler eventHandler, AppFlexInputViewModel appFlexInputViewModel, Clock clock) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
        m.checkNotNullParameter(appPermissionsRequests, "appPermissionsRequests");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(eventHandler, "eventHandler");
        m.checkNotNullParameter(clock, "clock");
        this.appPermissionsRequests = appPermissionsRequests;
        this.fragmentManager = fragmentManager;
        this.eventHandler = eventHandler;
        this.flexInputViewModel = appFlexInputViewModel;
        this.clock = clock;
        Context context = recyclerView.getContext();
        m.checkNotNullExpressionValue(context, "recycler.context");
        this.botUiComponentProvider = new ComponentProvider(context);
        this.data = new EmptyData();
        this.mentionMeMessageLevelHighlighting = true;
        this.handlerOfTouches = new HandlerOfTouches();
        this.handlerOfScrolls = new HandlerOfScrolls();
        this.handlerOfUpdates = new HandlerOfUpdates();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<WidgetChatListAdapter, ChatListEntry> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        switch (i) {
            case 0:
            case 32:
                return new WidgetChatListAdapterItemMessage(R.layout.widget_chat_list_adapter_item_text, this);
            case 1:
                return new WidgetChatListAdapterItemMessage(R.layout.widget_chat_list_adapter_item_minimal, this);
            case 2:
                return new WidgetChatListItem(R.layout.widget_chat_list_adapter_item_loading, this);
            case 3:
                return new WidgetChatListAdapterItemStart(this);
            case 4:
                return new WidgetChatListAdapterItemReactions(this);
            case 5:
                return new WidgetChatListAdapterItemSystemMessage(this);
            case 6:
                return new WidgetChatListAdapterItemUploadProgress(this);
            case 7:
                return new WidgetChatListItem(R.layout.widget_chat_list_adapter_item_spacer, this);
            case 8:
                return new WidgetChatListAdapterItemNewMessages(this);
            case 9:
                return new WidgetChatListAdapterItemTimestamp(this);
            case 10:
                return new WidgetChatListAdapterItemBlocked(this);
            case 11:
                return new WidgetChatListAdapterItemSearchResultCount(this);
            case 12:
                return new MGRecyclerViewHolder<>((int) R.layout.widget_chat_list_adapter_item_search_indexing, this);
            case 13:
                return new MGRecyclerViewHolder<>((int) R.layout.widget_chat_list_adapter_item_search_empty, this);
            case 14:
                return new MGRecyclerViewHolder<>((int) R.layout.widget_chat_list_adapter_item_search_error, this);
            case 15:
                return new MGRecyclerViewHolder<>((int) R.layout.widget_chat_list_adapter_item_divider, this);
            case 16:
                return new WidgetChatListAdapterItemEmptyPins(this);
            case 17:
                return new WidgetChatListAdapterItemMessageHeader(this);
            case 18:
                return new WidgetChatListAdapterItemMentionFooter(this);
            case 19:
                return new WidgetChatListAdapterItemCallMessage(this);
            case 20:
                return new WidgetChatListAdapterItemMessage(R.layout.widget_chat_list_adapter_item_failed, this);
            case 21:
                return new WidgetChatListAdapterItemEmbed(this);
            case 22:
                return new WidgetChatListAdapterItemGameInvite(this);
            case 23:
                return new WidgetChatListAdapterItemSpotifyListenTogether(this);
            case 24:
                return new WidgetChatListAdapterItemInvite(this);
            case 25:
                return new WidgetChatListAdapterItemGuildWelcome(this, null, 2, null);
            case 26:
                return new WidgetChatListAdapterItemGift(this);
            case 27:
            default:
                throw invalidViewTypeException(i);
            case 28:
                return new WidgetChatListAdapterItemAttachment(this);
            case 29:
                return new WidgetChatListAdapterItemPrivateChannelStart(this);
            case 30:
                return new WidgetChatListAdapterItemGuildTemplate(this);
            case 31:
                return new WidgetChatListAdapterItemSticker(this);
            case 33:
                return new WidgetChatListAdapterItemApplicationCommand(this);
            case 34:
                return new WidgetChatListAdapterItemEphemeralMessage(this);
            case 35:
                return new WidgetChatListAdapterItemThreadEmbed(this);
            case 36:
                return new WidgetChatListAdapterItemBotComponentRow(this);
            case 37:
                return new WidgetChatListAdapterItemThreadDraftForm(this, this.flexInputViewModel);
            case 38:
                return new WidgetChatListAdapterItemGuildInviteReminder(this);
            case 39:
                return new WidgetChatListAdapterItemStageInvite(this);
            case 40:
                return new MGRecyclerViewHolder<>((int) R.layout.widget_chat_list_adapter_item_thread_starter_divider, this);
            case 41:
                return new WidgetChatListAdapterItemStickerGreet(this);
            case 42:
                return new WidgetChatListAdapterItemStickerGreetCompact(this);
            case 43:
                return new WidgetChatListAdapterItemGuildScheduledEventInvite(this, null, 2, null);
        }
    }
}
