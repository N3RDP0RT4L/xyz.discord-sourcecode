package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.graphics.drawable.Drawable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import b.a.a.b.b;
import com.discord.api.guild.GuildFeature;
import com.discord.databinding.WidgetChatListAdapterItemThreadDraftFormBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadDraft;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.channels.threads.WidgetThreadDraftArchiveSheet;
import com.discord.widgets.chat.input.AppFlexInputViewModel;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.ThreadDraftFormEntry;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemThreadDraftForm.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0015\u001a\u00020\u0014\u0012\b\u0010\n\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b\u0016\u0010\u0017J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u001b\u0010\n\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0018\u0010\u000f\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemThreadDraftForm;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "getFlexInputViewModel", "()Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "Landroid/text/TextWatcher;", "nameTextWatcher", "Landroid/text/TextWatcher;", "Lcom/discord/databinding/WidgetChatListAdapterItemThreadDraftFormBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemThreadDraftFormBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;Lcom/discord/widgets/chat/input/AppFlexInputViewModel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemThreadDraftForm extends WidgetChatListItem {
    private final WidgetChatListAdapterItemThreadDraftFormBinding binding;
    private final AppFlexInputViewModel flexInputViewModel;
    private TextWatcher nameTextWatcher;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemThreadDraftForm(WidgetChatListAdapter widgetChatListAdapter, AppFlexInputViewModel appFlexInputViewModel) {
        super(R.layout.widget_chat_list_adapter_item_thread_draft_form, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        this.flexInputViewModel = appFlexInputViewModel;
        View view = this.itemView;
        int i = R.id.auto_archive_duration;
        TextView textView = (TextView) view.findViewById(R.id.auto_archive_duration);
        if (textView != null) {
            i = R.id.divider_stroke;
            View findViewById = view.findViewById(R.id.divider_stroke);
            if (findViewById != null) {
                i = R.id.private_thread_toggle;
                ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.private_thread_toggle);
                if (constraintLayout != null) {
                    i = R.id.private_thread_toggle_badge;
                    TextView textView2 = (TextView) view.findViewById(R.id.private_thread_toggle_badge);
                    if (textView2 != null) {
                        i = R.id.private_thread_toggle_switch;
                        SwitchMaterial switchMaterial = (SwitchMaterial) view.findViewById(R.id.private_thread_toggle_switch);
                        if (switchMaterial != null) {
                            i = R.id.private_thread_toggle_text;
                            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.private_thread_toggle_text);
                            if (linearLayout != null) {
                                i = R.id.thread_icon;
                                ImageView imageView = (ImageView) view.findViewById(R.id.thread_icon);
                                if (imageView != null) {
                                    i = R.id.thread_name_input;
                                    TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.thread_name_input);
                                    if (textInputLayout != null) {
                                        WidgetChatListAdapterItemThreadDraftFormBinding widgetChatListAdapterItemThreadDraftFormBinding = new WidgetChatListAdapterItemThreadDraftFormBinding((LinearLayout) view, textView, findViewById, constraintLayout, textView2, switchMaterial, linearLayout, imageView, textInputLayout);
                                        m.checkNotNullExpressionValue(widgetChatListAdapterItemThreadDraftFormBinding, "WidgetChatListAdapterIte…ormBinding.bind(itemView)");
                                        this.binding = widgetChatListAdapterItemThreadDraftFormBinding;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemThreadDraftForm widgetChatListAdapterItemThreadDraftForm) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemThreadDraftForm.adapter;
    }

    public final AppFlexInputViewModel getFlexInputViewModel() {
        return this.flexInputViewModel;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        PremiumUtils.BoostFeatureBadgeData boostFeatureBadgeDataForGuildFeature;
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        final StoreThreadDraft threadDraft = StoreStream.Companion.getThreadDraft();
        final ThreadDraftFormEntry threadDraftFormEntry = (ThreadDraftFormEntry) chatListEntry;
        final StoreThreadDraft.ThreadDraftState threadDraftState = threadDraftFormEntry.getThreadDraftState();
        String threadName = threadDraftState.getThreadName();
        boolean z2 = true;
        if (threadName == null || threadName.length() == 0) {
            TextInputLayout textInputLayout = this.binding.g;
            m.checkNotNullExpressionValue(textInputLayout, "binding.threadNameInput");
            ViewExtensions.clear(textInputLayout);
        }
        this.binding.f.setImageResource(threadDraftState.isPrivate() ? R.drawable.ic_thread_locked : R.drawable.ic_thread);
        TextInputLayout textInputLayout2 = this.binding.g;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.threadNameInput");
        EditText editText = textInputLayout2.getEditText();
        if (editText != null) {
            editText.removeTextChangedListener(this.nameTextWatcher);
            TextWatcher widgetChatListAdapterItemThreadDraftForm$onConfigure$$inlined$apply$lambda$1 = new TextWatcher() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$$inlined$apply$lambda$1
                /* JADX WARN: Code restructure failed: missing block: B:13:0x0023, code lost:
                    if ((r10.length() == 0) == true) goto L15;
                 */
                @Override // android.text.TextWatcher
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public void afterTextChanged(android.text.Editable r10) {
                    /*
                        r9 = this;
                        com.discord.stores.StoreThreadDraft r0 = r2
                        com.discord.stores.StoreThreadDraft$ThreadDraftState r1 = r3
                        if (r10 == 0) goto Lb
                        java.lang.String r2 = r10.toString()
                        goto Lc
                    Lb:
                        r2 = 0
                    Lc:
                        r4 = r2
                        r5 = 0
                        com.discord.stores.StoreThreadDraft$ThreadDraftState r2 = r3
                        boolean r2 = r2.getShouldDisplayNameError()
                        r3 = 0
                        r6 = 1
                        if (r2 == 0) goto L26
                        if (r10 == 0) goto L26
                        int r10 = r10.length()
                        if (r10 != 0) goto L22
                        r10 = 1
                        goto L23
                    L22:
                        r10 = 0
                    L23:
                        if (r10 != r6) goto L26
                        goto L27
                    L26:
                        r6 = 0
                    L27:
                        r7 = 11
                        r8 = 0
                        r2 = 0
                        r3 = 0
                        com.discord.stores.StoreThreadDraft$ThreadDraftState r10 = com.discord.stores.StoreThreadDraft.ThreadDraftState.copy$default(r1, r2, r3, r4, r5, r6, r7, r8)
                        r0.setDraftState(r10)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$$inlined$apply$lambda$1.afterTextChanged(android.text.Editable):void");
                }

                @Override // android.text.TextWatcher
                public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
                }

                @Override // android.text.TextWatcher
                public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
                }
            };
            editText.addTextChangedListener(widgetChatListAdapterItemThreadDraftForm$onConfigure$$inlined$apply$lambda$1);
            this.nameTextWatcher = widgetChatListAdapterItemThreadDraftForm$onConfigure$$inlined$apply$lambda$1;
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$$inlined$apply$lambda$2
                @Override // android.view.View.OnFocusChangeListener
                public final void onFocusChange(View view, boolean z3) {
                    if (!z3 && threadDraftState.getShouldDisplayNameError()) {
                        threadDraft.setDraftState(StoreThreadDraft.ThreadDraftState.copy$default(threadDraftState, false, null, null, false, false, 15, null));
                    }
                }
            });
            editText.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$$inlined$apply$lambda$3
                @Override // android.widget.TextView.OnEditorActionListener
                public final boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
                    AppFlexInputViewModel flexInputViewModel;
                    if (i2 != 5 || (flexInputViewModel = WidgetChatListAdapterItemThreadDraftForm.this.getFlexInputViewModel()) == null) {
                        return false;
                    }
                    flexInputViewModel.focus();
                    return false;
                }
            });
        }
        if (threadDraftState.getShouldDisplayNameError()) {
            TextInputLayout textInputLayout3 = this.binding.g;
            m.checkNotNullExpressionValue(textInputLayout3, "binding.threadNameInput");
            TextInputLayout textInputLayout4 = this.binding.g;
            m.checkNotNullExpressionValue(textInputLayout4, "binding.threadNameInput");
            textInputLayout3.setError(textInputLayout4.getContext().getString(R.string.member_verification_form_required_item));
            TextInputLayout textInputLayout5 = this.binding.g;
            m.checkNotNullExpressionValue(textInputLayout5, "binding.threadNameInput");
            textInputLayout5.setErrorEnabled(true);
        } else {
            TextInputLayout textInputLayout6 = this.binding.g;
            m.checkNotNullExpressionValue(textInputLayout6, "binding.threadNameInput");
            textInputLayout6.setErrorEnabled(false);
        }
        ConstraintLayout constraintLayout = this.binding.c;
        m.checkNotNullExpressionValue(constraintLayout, "binding.privateThreadToggle");
        if (threadDraftFormEntry.getParentMessageId() != null || !threadDraftFormEntry.getCanSeePrivateThreadOption()) {
            z2 = false;
        }
        constraintLayout.setVisibility(z2 ? 0 : 8);
        if (!threadDraftFormEntry.getCanCreatePublicThread() && !threadDraftState.isPrivate()) {
            threadDraft.setDraftState(StoreThreadDraft.ThreadDraftState.copy$default(threadDraftState, true, null, null, false, false, 30, null));
            this.binding.c.setOnClickListener(null);
        } else if (!threadDraftFormEntry.getCanCreatePrivateThread()) {
            this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    b.k.a(WidgetChatListAdapterItemThreadDraftForm.access$getAdapter$p(WidgetChatListAdapterItemThreadDraftForm.this).getFragmentManager(), threadDraftFormEntry.getGuildId(), Long.valueOf(threadDraftFormEntry.getParentChannelId()), PremiumUtils.INSTANCE.getMinimumBoostTierForGuildFeature(GuildFeature.PRIVATE_THREADS), new Traits.Location(Traits.Location.Page.GUILD_CHANNEL, null, null, null, null, 30, null));
                }
            });
        } else {
            this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StoreThreadDraft storeThreadDraft = StoreThreadDraft.this;
                    StoreThreadDraft.ThreadDraftState threadDraftState2 = threadDraftState;
                    storeThreadDraft.setDraftState(StoreThreadDraft.ThreadDraftState.copy$default(threadDraftState2, !threadDraftState2.isPrivate(), null, null, false, false, 30, null));
                }
            });
        }
        SwitchMaterial switchMaterial = this.binding.e;
        m.checkNotNullExpressionValue(switchMaterial, "binding.privateThreadToggleSwitch");
        switchMaterial.setChecked(threadDraftState.isPrivate());
        boostFeatureBadgeDataForGuildFeature = PremiumUtils.INSTANCE.getBoostFeatureBadgeDataForGuildFeature(threadDraftFormEntry.getGuild(), Long.valueOf(threadDraftFormEntry.getParentChannelId()), GuildFeature.PRIVATE_THREADS, ((WidgetChatListAdapter) this.adapter).getContext(), ((WidgetChatListAdapter) this.adapter).getFragmentManager(), (r18 & 32) != 0 ? null : null, (r18 & 64) != 0 ? null : new Traits.Location(null, Traits.Location.Section.THREAD_CREATION_OPTIONS, Traits.Location.Obj.PRIVATE_THREAD_CHECKBOX, null, null, 25, null));
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.privateThreadToggleBadge");
        textView.setText(boostFeatureBadgeDataForGuildFeature.getText());
        TextView textView2 = this.binding.d;
        Drawable drawable = ContextCompat.getDrawable(((WidgetChatListAdapter) this.adapter).getContext(), R.drawable.ic_boosted_badge_12dp);
        if (drawable != null) {
            drawable.setTint(boostFeatureBadgeDataForGuildFeature.getIconColor());
        } else {
            drawable = null;
        }
        textView2.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        TextView textView3 = this.binding.f2321b;
        m.checkNotNullExpressionValue(textView3, "binding.autoArchiveDuration");
        b.a.k.b.m(textView3, R.string.create_thread_header_2, new Object[0], new WidgetChatListAdapterItemThreadDraftForm$onConfigure$5(this, threadDraftState, chatListEntry, threadDraftFormEntry));
        this.binding.f2321b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetThreadDraftArchiveSheet.Companion.show(WidgetChatListAdapterItemThreadDraftForm.access$getAdapter$p(WidgetChatListAdapterItemThreadDraftForm.this).getFragmentManager(), threadDraftFormEntry.getGuildId());
            }
        });
        this.binding.g.requestFocus();
    }
}
