package com.discord.widgets.chat.list.adapter;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.graphics.ColorUtils;
import androidx.exifinterface.media.ExifInterface;
import b.a.i.t4;
import b.d.b.a.a;
import com.discord.api.message.embed.EmbedAuthor;
import com.discord.api.message.embed.EmbedField;
import com.discord.api.message.embed.EmbedFooter;
import com.discord.api.message.embed.EmbedProvider;
import com.discord.api.message.embed.EmbedThumbnail;
import com.discord.api.message.embed.EmbedType;
import com.discord.api.message.embed.MessageEmbed;
import com.discord.api.role.GuildRole;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetChatListAdapterItemEmbedBinding;
import com.discord.embed.RenderableEmbedMedia;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.message.Message;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.Parser;
import com.discord.stores.StoreMessageState;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.embed.EmbedResourceUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.AstRenderer;
import com.discord.utilities.textprocessing.DiscordParser;
import com.discord.utilities.textprocessing.MessageParseState;
import com.discord.utilities.textprocessing.MessagePreprocessor;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.utilities.textprocessing.Tags;
import com.discord.utilities.textprocessing.TagsBuilder;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import com.discord.utilities.textprocessing.node.UrlNode;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.widgets.chat.list.FragmentLifecycleListener;
import com.discord.widgets.chat.list.InlineMediaView;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.EmbedEntry;
import com.discord.widgets.media.WidgetMedia;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.card.MaterialCardView;
import d0.g0.s;
import d0.g0.t;
import d0.g0.w;
import d0.g0.y;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.a.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func4;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemEmbed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000®\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \\2\u00020\u00012\u00020\u0002:\u0002\\]B\u000f\u0012\u0006\u0010Y\u001a\u00020X¢\u0006\u0004\bZ\u0010[J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0019\u0010\n\u001a\u00020\u00052\b\u0010\t\u001a\u0004\u0018\u00010\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u0010\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u001f\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0019\u0010\u0018J\u0017\u0010\u001a\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001c\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u001c\u0010\u001bJ\u001f\u0010\u001f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u001e\u001a\u00020\u001dH\u0002¢\u0006\u0004\b\u001f\u0010 J\u001f\u0010!\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u000eH\u0002¢\u0006\u0004\b!\u0010\u0018J#\u0010&\u001a\u00020\u00052\b\u0010#\u001a\u0004\u0018\u00010\"2\b\u0010%\u001a\u0004\u0018\u00010$H\u0002¢\u0006\u0004\b&\u0010'J\u000f\u0010(\u001a\u00020\u001dH\u0002¢\u0006\u0004\b(\u0010)Jg\u00103\u001a\n\u0012\u0004\u0012\u000202\u0018\u00010*2\u000e\u0010,\u001a\n\u0012\u0004\u0012\u00020+\u0018\u00010*2\u001e\u00100\u001a\u001a\u0012\u0004\u0012\u00020\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0.\u0012\u0004\u0012\u00020/0-2\u001e\u00101\u001a\u001a\u0012\u0004\u0012\u00020\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0.\u0012\u0004\u0012\u00020/0-H\u0002¢\u0006\u0004\b3\u00104J1\u0010\u001c\u001a\u00020\u00052\u0006\u00106\u001a\u0002052\u0006\u00108\u001a\u0002072\u0006\u00109\u001a\u00020\b2\b\b\u0002\u0010:\u001a\u00020\bH\u0002¢\u0006\u0004\b\u001c\u0010;JC\u0010\u001c\u001a\u00020\u00052\u0006\u00108\u001a\u0002072\u0006\u00109\u001a\u00020\b2\u0006\u0010<\u001a\u00020\b2\u0006\u0010=\u001a\u00020\b2\b\u0010?\u001a\u0004\u0018\u00010>2\b\b\u0002\u0010:\u001a\u00020\bH\u0002¢\u0006\u0004\b\u001c\u0010@J\u0011\u0010B\u001a\u0004\u0018\u00010AH\u0014¢\u0006\u0004\bB\u0010CJ\u001f\u0010G\u001a\u00020\u00052\u0006\u0010D\u001a\u00020\b2\u0006\u0010F\u001a\u00020EH\u0014¢\u0006\u0004\bG\u0010HJ\u000f\u0010I\u001a\u00020\u0005H\u0016¢\u0006\u0004\bI\u0010JJ\u000f\u0010K\u001a\u00020\u0005H\u0016¢\u0006\u0004\bK\u0010JR\u0016\u0010L\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bL\u0010MR\u0016\u0010O\u001a\u00020N8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bO\u0010PR\u0018\u0010Q\u001a\u0004\u0018\u00010A8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bQ\u0010RR\u0016\u0010T\u001a\u00020S8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bT\u0010UR\u0016\u0010V\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010MR\u0016\u0010W\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010M¨\u0006^"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/widgets/chat/list/FragmentLifecycleListener;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;)V", "", ModelAuditLogEntry.CHANGE_KEY_COLOR, "configureEmbedDivider", "(Ljava/lang/Integer;)V", "Lcom/discord/api/message/embed/MessageEmbed;", "embed", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "context", "configureEmbedProvider", "(Lcom/discord/api/message/embed/MessageEmbed;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V", "Lcom/discord/api/message/embed/EmbedAuthor;", "author", "configureEmbedAuthor", "(Lcom/discord/api/message/embed/EmbedAuthor;)V", "renderContext", "configureEmbedTitle", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;Lcom/discord/utilities/textprocessing/MessageRenderContext;)V", "configureEmbedDescription", "configureEmbedThumbnail", "(Lcom/discord/api/message/embed/MessageEmbed;)V", "configureEmbedImage", "", "autoPlayGifs", "configureInlineEmbed", "(Lcom/discord/api/message/embed/MessageEmbed;Z)V", "configureEmbedFields", "Lcom/discord/api/message/embed/EmbedFooter;", "footer", "Lcom/discord/api/utcdatetime/UtcDateTime;", "timestamp", "configureFooter", "(Lcom/discord/api/message/embed/EmbedFooter;Lcom/discord/api/utcdatetime/UtcDateTime;)V", "shouldRenderMedia", "()Z", "", "Lcom/discord/api/message/embed/EmbedField;", "fields", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageParseState;", "nameParser", "valueParser", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model$ParsedField;", "parseFields", "(Ljava/util/List;Lcom/discord/simpleast/core/parser/Parser;Lcom/discord/simpleast/core/parser/Parser;)Ljava/util/List;", "Lcom/discord/api/message/embed/EmbedThumbnail;", "thumbnail", "Landroid/widget/ImageView;", "view", "maxWidth", "minWidth", "(Lcom/discord/api/message/embed/EmbedThumbnail;Landroid/widget/ImageView;II)V", "width", "height", "", "imageProxyUrl", "(Landroid/widget/ImageView;IIILjava/lang/String;I)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "onResume", "()V", "onPause", "embedTinyIconSize", "I", "Lcom/discord/stores/StoreUserSettings;", "userSettings", "Lcom/discord/stores/StoreUserSettings;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "Lcom/discord/databinding/WidgetChatListAdapterItemEmbedBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemEmbedBinding;", "maxEmbedImageWidth", "embedThumbnailMaxSize", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemEmbed extends WidgetChatListItem implements FragmentLifecycleListener {
    public static final Companion Companion;
    private static final String EMBED_TYPE_DESC = "desc";
    private static final String EMBED_TYPE_FIELD_NAME = "f_name";
    private static final String EMBED_TYPE_FIELD_VALUE = "f_value";
    private static final String EMBED_TYPE_TITLE = "title";
    private static final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> UI_THREAD_TITLES_PARSER;
    private final WidgetChatListAdapterItemEmbedBinding binding;
    private final int embedThumbnailMaxSize;
    private final int embedTinyIconSize;
    private final int maxEmbedImageWidth;
    private Subscription subscription;
    private final StoreUserSettings userSettings;
    private static final int MAX_IMAGE_VIEW_HEIGHT_PX = DimenUtils.dpToPixels(360);
    private static final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> UI_THREAD_VALUES_PARSER = DiscordParser.createParser$default(true, true, false, 4, null);

    /* compiled from: WidgetChatListAdapterItemEmbed.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Landroid/widget/TextView;", "", "invoke", "(Landroid/widget/TextView;)V", "copyTextOnLongPress"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<TextView, Unit> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(TextView textView) {
            invoke2(textView);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(final TextView textView) {
            m.checkNotNullParameter(textView, "$this$copyTextOnLongPress");
            textView.setOnLongClickListener(new View.OnLongClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed.1.1
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    Context context = textView.getContext();
                    m.checkNotNullExpressionValue(context, "context");
                    CharSequence text = textView.getText();
                    m.checkNotNullExpressionValue(text, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
                    b.a.d.m.c(context, text, 0, 4);
                    return false;
                }
            });
        }
    }

    /* compiled from: WidgetChatListAdapterItemEmbed.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b*\u0010+JI\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\n*\u001c\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u000e\u0012\f\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u00070\u00022\n\u0010\b\u001a\u00060\u0003j\u0002`\u00042\u0006\u0010\t\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u000b\u0010\fJ'\u0010\u0011\u001a\u00020\u0010*\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u001d\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00130\u00152\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J%\u0010\u001c\u001a\u001a\u0012\u0004\u0012\u00020\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u001a\u0012\u0004\u0012\u00020\u001b0\u0018¢\u0006\u0004\b\u001c\u0010\u001dR1\u0010\u001e\u001a\u001a\u0012\u0004\u0012\u00020\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u001a\u0012\u0004\u0012\u00020\u001b0\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010\u001dR1\u0010!\u001a\u001a\u0012\u0004\u0012\u00020\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\u001a\u0012\u0004\u0012\u00020\u001b0\u00188\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u001f\u001a\u0004\b\"\u0010\u001dR\u0016\u0010#\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010%\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010$R\u0016\u0010&\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b&\u0010$R\u0016\u0010'\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b'\u0010$R\u0016\u0010(\u001a\u00020\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)¨\u0006,"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Companion;", "", "", "", "Lcom/discord/primitives/Index;", "", "", "Lcom/discord/stores/VisibleKeys;", "embedIndex", "keyPrefix", "", "getEmbedFieldVisibleIndices", "(Ljava/util/Map;ILjava/lang/String;)Ljava/util/List;", "Landroid/view/View;", "url", "mask", "", "bindUrlOnClick", "(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;", "initialModel", "Lrx/Observable;", "getModel", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;)Lrx/Observable;", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageParseState;", "createTitlesParser", "()Lcom/discord/simpleast/core/parser/Parser;", "UI_THREAD_TITLES_PARSER", "Lcom/discord/simpleast/core/parser/Parser;", "getUI_THREAD_TITLES_PARSER", "UI_THREAD_VALUES_PARSER", "getUI_THREAD_VALUES_PARSER", "EMBED_TYPE_DESC", "Ljava/lang/String;", "EMBED_TYPE_FIELD_NAME", "EMBED_TYPE_FIELD_VALUE", "EMBED_TYPE_TITLE", "MAX_IMAGE_VIEW_HEIGHT_PX", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void bindUrlOnClick(View view, final String str, final String str2) {
            if (str != null) {
                view.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$Companion$bindUrlOnClick$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        UriHandler.handleOrUntrusted(a.x(view2, "view", "view.context"), str, str2);
                    }
                });
            } else {
                view.setOnClickListener(null);
            }
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final List<Integer> getEmbedFieldVisibleIndices(Map<Integer, ? extends Set<String>> map, int i, String str) {
            Set<String> set = map.get(Integer.valueOf(i));
            ArrayList arrayList = null;
            if (set != null) {
                ArrayList<String> arrayList2 = new ArrayList();
                for (Object obj : set) {
                    if (t.startsWith$default((String) obj, str, false, 2, null)) {
                        arrayList2.add(obj);
                    }
                }
                arrayList = new ArrayList();
                for (String str2 : arrayList2) {
                    Integer intOrNull = s.toIntOrNull(y.drop(str2, str.length() + 1));
                    if (intOrNull != null) {
                        arrayList.add(intOrNull);
                    }
                }
            }
            return arrayList;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Model> getModel(Model model) {
            final EmbedEntry embedEntry = model.getEmbedEntry();
            final Collection<Node<MessageRenderContext>> parsedDescription = model.getParsedDescription();
            final List<Model.ParsedField> parsedFields = model.getParsedFields();
            TagsBuilder tagsBuilder = new TagsBuilder();
            if (parsedDescription != null) {
                tagsBuilder.processAst(parsedDescription);
            }
            if (parsedFields != null) {
                for (Model.ParsedField parsedField : parsedFields) {
                    tagsBuilder.processAst(parsedField.getParsedName());
                    tagsBuilder.processAst(parsedField.getParsedValue());
                }
            }
            Tags build = tagsBuilder.build();
            if (build.isEmpty()) {
                Observable observable = d.k;
                m.checkNotNullExpressionValue(observable, "Observable.never()");
                return observable;
            }
            StoreStream.Companion companion = StoreStream.Companion;
            Observable h = Observable.h(companion.getUsers().observeMeId(), companion.getChannels().observeNames(build.getChannels()), Observable.j(companion.getGuilds().observeComputed(embedEntry.getGuildId(), build.getUsers()), companion.getUsers().observeUsernames(build.getUsers()), WidgetChatListAdapterItemEmbed$Companion$getModel$1.INSTANCE), companion.getGuilds().observeRoles(embedEntry.getGuildId(), build.getRoles()), new Func4<Long, Map<Long, ? extends String>, HashMap<Long, String>, Map<Long, ? extends GuildRole>, Model>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$Companion$getModel$2
                @Override // rx.functions.Func4
                public /* bridge */ /* synthetic */ WidgetChatListAdapterItemEmbed.Model call(Long l, Map<Long, ? extends String> map, HashMap<Long, String> hashMap, Map<Long, ? extends GuildRole> map2) {
                    return call2(l, (Map<Long, String>) map, hashMap, (Map<Long, GuildRole>) map2);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetChatListAdapterItemEmbed.Model call2(Long l, Map<Long, String> map, HashMap<Long, String> hashMap, Map<Long, GuildRole> map2) {
                    EmbedEntry embedEntry2 = EmbedEntry.this;
                    Collection collection = parsedDescription;
                    List list = parsedFields;
                    m.checkNotNullExpressionValue(l, "myId");
                    return new WidgetChatListAdapterItemEmbed.Model(embedEntry2, collection, list, map, hashMap, map2, l.longValue());
                }
            });
            m.checkNotNullExpressionValue(h, "Observable\n          .co… roles, myId)\n          }");
            Observable<Model> q = ObservableExtensionsKt.computationLatest(h).q();
            m.checkNotNullExpressionValue(q, "Observable\n          .co…  .distinctUntilChanged()");
            return q;
        }

        public final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> createTitlesParser() {
            return DiscordParser.createParser$default(false, false, false, 4, null);
        }

        public final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> getUI_THREAD_TITLES_PARSER() {
            return WidgetChatListAdapterItemEmbed.UI_THREAD_TITLES_PARSER;
        }

        public final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> getUI_THREAD_VALUES_PARSER() {
            return WidgetChatListAdapterItemEmbed.UI_THREAD_VALUES_PARSER;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    static {
        Companion companion = new Companion(null);
        Companion = companion;
        UI_THREAD_TITLES_PARSER = companion.createTitlesParser();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemEmbed(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_embed, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_gutter_bg;
        View findViewById = view.findViewById(R.id.chat_list_adapter_item_gutter_bg);
        if (findViewById != null) {
            i = R.id.chat_list_adapter_item_highlighted_bg;
            View findViewById2 = view.findViewById(R.id.chat_list_adapter_item_highlighted_bg);
            if (findViewById2 != null) {
                i = R.id.chat_list_item_embed_author_icon;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.chat_list_item_embed_author_icon);
                if (simpleDraweeView != null) {
                    i = R.id.chat_list_item_embed_author_text;
                    TextView textView = (TextView) view.findViewById(R.id.chat_list_item_embed_author_text);
                    if (textView != null) {
                        i = R.id.chat_list_item_embed_barrier_data;
                        Barrier barrier = (Barrier) view.findViewById(R.id.chat_list_item_embed_barrier_data);
                        if (barrier != null) {
                            i = R.id.chat_list_item_embed_barrier_header;
                            Barrier barrier2 = (Barrier) view.findViewById(R.id.chat_list_item_embed_barrier_header);
                            if (barrier2 != null) {
                                i = R.id.chat_list_item_embed_container_card;
                                MaterialCardView materialCardView = (MaterialCardView) view.findViewById(R.id.chat_list_item_embed_container_card);
                                if (materialCardView != null) {
                                    i = R.id.chat_list_item_embed_content;
                                    ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.chat_list_item_embed_content);
                                    if (constraintLayout != null) {
                                        i = R.id.chat_list_item_embed_description;
                                        LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.chat_list_item_embed_description);
                                        if (linkifiedTextView != null) {
                                            i = R.id.chat_list_item_embed_divider;
                                            View findViewById3 = view.findViewById(R.id.chat_list_item_embed_divider);
                                            if (findViewById3 != null) {
                                                i = R.id.chat_list_item_embed_fields;
                                                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.chat_list_item_embed_fields);
                                                if (linearLayout != null) {
                                                    i = R.id.chat_list_item_embed_footer_icon;
                                                    SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) view.findViewById(R.id.chat_list_item_embed_footer_icon);
                                                    if (simpleDraweeView2 != null) {
                                                        i = R.id.chat_list_item_embed_footer_text;
                                                        TextView textView2 = (TextView) view.findViewById(R.id.chat_list_item_embed_footer_text);
                                                        if (textView2 != null) {
                                                            i = R.id.chat_list_item_embed_image;
                                                            SimpleDraweeView simpleDraweeView3 = (SimpleDraweeView) view.findViewById(R.id.chat_list_item_embed_image);
                                                            if (simpleDraweeView3 != null) {
                                                                i = R.id.chat_list_item_embed_image_icons;
                                                                ImageView imageView = (ImageView) view.findViewById(R.id.chat_list_item_embed_image_icons);
                                                                if (imageView != null) {
                                                                    i = R.id.chat_list_item_embed_image_thumbnail;
                                                                    SimpleDraweeView simpleDraweeView4 = (SimpleDraweeView) view.findViewById(R.id.chat_list_item_embed_image_thumbnail);
                                                                    if (simpleDraweeView4 != null) {
                                                                        i = R.id.chat_list_item_embed_provider;
                                                                        TextView textView3 = (TextView) view.findViewById(R.id.chat_list_item_embed_provider);
                                                                        if (textView3 != null) {
                                                                            i = R.id.chat_list_item_embed_spoiler;
                                                                            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.chat_list_item_embed_spoiler);
                                                                            if (frameLayout != null) {
                                                                                i = R.id.chat_list_item_embed_title;
                                                                                LinkifiedTextView linkifiedTextView2 = (LinkifiedTextView) view.findViewById(R.id.chat_list_item_embed_title);
                                                                                if (linkifiedTextView2 != null) {
                                                                                    i = R.id.embed_image_container;
                                                                                    CardView cardView = (CardView) view.findViewById(R.id.embed_image_container);
                                                                                    if (cardView != null) {
                                                                                        i = R.id.embed_inline_media;
                                                                                        InlineMediaView inlineMediaView = (InlineMediaView) view.findViewById(R.id.embed_inline_media);
                                                                                        if (inlineMediaView != null) {
                                                                                            WidgetChatListAdapterItemEmbedBinding widgetChatListAdapterItemEmbedBinding = new WidgetChatListAdapterItemEmbedBinding((ConstraintLayout) view, findViewById, findViewById2, simpleDraweeView, textView, barrier, barrier2, materialCardView, constraintLayout, linkifiedTextView, findViewById3, linearLayout, simpleDraweeView2, textView2, simpleDraweeView3, imageView, simpleDraweeView4, textView3, frameLayout, linkifiedTextView2, cardView, inlineMediaView);
                                                                                            m.checkNotNullExpressionValue(widgetChatListAdapterItemEmbedBinding, "WidgetChatListAdapterIte…bedBinding.bind(itemView)");
                                                                                            this.binding = widgetChatListAdapterItemEmbedBinding;
                                                                                            AnonymousClass1 r2 = AnonymousClass1.INSTANCE;
                                                                                            m.checkNotNullExpressionValue(textView3, "binding.chatListItemEmbedProvider");
                                                                                            r2.invoke2(textView3);
                                                                                            m.checkNotNullExpressionValue(linkifiedTextView2, "binding.chatListItemEmbedTitle");
                                                                                            r2.invoke2((TextView) linkifiedTextView2);
                                                                                            m.checkNotNullExpressionValue(textView, "binding.chatListItemEmbedAuthorText");
                                                                                            r2.invoke2(textView);
                                                                                            m.checkNotNullExpressionValue(linkifiedTextView, "binding.chatListItemEmbedDescription");
                                                                                            r2.invoke2((TextView) linkifiedTextView);
                                                                                            m.checkNotNullExpressionValue(textView2, "binding.chatListItemEmbedFooterText");
                                                                                            r2.invoke2(textView2);
                                                                                            this.userSettings = StoreStream.Companion.getUserSettings();
                                                                                            View view2 = this.itemView;
                                                                                            m.checkNotNullExpressionValue(view2, "itemView");
                                                                                            this.embedTinyIconSize = (int) view2.getResources().getDimension(R.dimen.embed_tiny_icon_size);
                                                                                            View view3 = this.itemView;
                                                                                            m.checkNotNullExpressionValue(view3, "itemView");
                                                                                            this.embedThumbnailMaxSize = (int) view3.getResources().getDimension(R.dimen.embed_thumbnail_max_size);
                                                                                            EmbedResourceUtils embedResourceUtils = EmbedResourceUtils.INSTANCE;
                                                                                            View view4 = this.itemView;
                                                                                            m.checkNotNullExpressionValue(view4, "itemView");
                                                                                            Context context = view4.getContext();
                                                                                            m.checkNotNullExpressionValue(context, "itemView.context");
                                                                                            this.maxEmbedImageWidth = embedResourceUtils.computeMaximumImageWidthPx(context);
                                                                                            return;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemEmbed widgetChatListAdapterItemEmbed) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemEmbed.adapter;
    }

    private final void configureEmbedAuthor(EmbedAuthor embedAuthor) {
        if (embedAuthor != null) {
            TextView textView = this.binding.e;
            m.checkNotNullExpressionValue(textView, "binding.chatListItemEmbedAuthorText");
            textView.setText(embedAuthor.a());
            Companion companion = Companion;
            TextView textView2 = this.binding.e;
            m.checkNotNullExpressionValue(textView2, "binding.chatListItemEmbedAuthorText");
            companion.bindUrlOnClick(textView2, embedAuthor.c(), embedAuthor.a());
            TextView textView3 = this.binding.e;
            m.checkNotNullExpressionValue(textView3, "binding.chatListItemEmbedAuthorText");
            textView3.setVisibility(0);
        } else {
            TextView textView4 = this.binding.e;
            m.checkNotNullExpressionValue(textView4, "binding.chatListItemEmbedAuthorText");
            textView4.setVisibility(8);
        }
        if ((embedAuthor != null ? embedAuthor.b() : null) != null) {
            SimpleDraweeView simpleDraweeView = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.chatListItemEmbedAuthorIcon");
            simpleDraweeView.setVisibility(0);
            SimpleDraweeView simpleDraweeView2 = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.chatListItemEmbedAuthorIcon");
            int i = this.embedTinyIconSize;
            configureEmbedImage$default(this, simpleDraweeView2, i, i, i, embedAuthor.b(), 0, 32, null);
            return;
        }
        SimpleDraweeView simpleDraweeView3 = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.chatListItemEmbedAuthorIcon");
        simpleDraweeView3.setVisibility(8);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureEmbedDescription(Model model, MessageRenderContext messageRenderContext) {
        MessageRenderContext copy;
        Map<Integer, Set<String>> visibleSpoilerEmbedMap;
        if (model.getParsedDescription() != null) {
            long id2 = model.getEmbedEntry().getMessage().getId();
            int embedIndex = model.getEmbedEntry().getEmbedIndex();
            long myId = model.getMyId();
            StoreMessageState.State messageState = model.getEmbedEntry().getMessageState();
            new MessagePreprocessor(myId, (messageState == null || (visibleSpoilerEmbedMap = messageState.getVisibleSpoilerEmbedMap()) == null) ? null : Companion.getEmbedFieldVisibleIndices(visibleSpoilerEmbedMap, embedIndex, EMBED_TYPE_DESC), null, false, null, 28, null).process(model.getParsedDescription());
            LinkifiedTextView linkifiedTextView = this.binding.h;
            Collection<Node<MessageRenderContext>> parsedDescription = model.getParsedDescription();
            copy = messageRenderContext.copy((r31 & 1) != 0 ? messageRenderContext.getContext() : null, (r31 & 2) != 0 ? messageRenderContext.getMyId() : 0L, (r31 & 4) != 0 ? messageRenderContext.isAnimationEnabled() : false, (r31 & 8) != 0 ? messageRenderContext.getUserNames() : null, (r31 & 16) != 0 ? messageRenderContext.getChannelNames() : null, (r31 & 32) != 0 ? messageRenderContext.getRoles() : null, (r31 & 64) != 0 ? messageRenderContext.getLinkColorAttrResId() : 0, (r31 & 128) != 0 ? messageRenderContext.getOnClickUrl() : null, (r31 & 256) != 0 ? messageRenderContext.getOnLongPressUrl() : null, (r31 & 512) != 0 ? messageRenderContext.getSpoilerColorRes() : 0, (r31 & 1024) != 0 ? messageRenderContext.getSpoilerRevealedColorRes() : 0, (r31 & 2048) != 0 ? messageRenderContext.getSpoilerOnClick() : new WidgetChatListAdapterItemEmbed$configureEmbedDescription$1(id2, embedIndex), (r31 & 4096) != 0 ? messageRenderContext.getUserMentionOnClick() : null, (r31 & 8192) != 0 ? messageRenderContext.getChannelMentionOnClick() : null);
            linkifiedTextView.setDraweeSpanStringBuilder(AstRenderer.render(parsedDescription, copy));
            LinkifiedTextView linkifiedTextView2 = this.binding.h;
            m.checkNotNullExpressionValue(linkifiedTextView2, "binding.chatListItemEmbedDescription");
            linkifiedTextView2.setVisibility(0);
            return;
        }
        LinkifiedTextView linkifiedTextView3 = this.binding.h;
        m.checkNotNullExpressionValue(linkifiedTextView3, "binding.chatListItemEmbedDescription");
        linkifiedTextView3.setVisibility(8);
    }

    private final void configureEmbedDivider(Integer num) {
        int i;
        View view = this.binding.i;
        if (num != null) {
            i = ColorUtils.setAlphaComponent(num.intValue(), 255);
        } else {
            m.checkNotNullExpressionValue(view, "binding.chatListItemEmbedDivider");
            i = ColorCompat.getThemedColor(view.getContext(), (int) R.attr.colorBackgroundModifierAccent);
        }
        view.setBackgroundColor(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v10, types: [android.view.View] */
    /* JADX WARN: Type inference failed for: r5v4, types: [android.view.View] */
    /* JADX WARN: Type inference failed for: r5v7, types: [android.widget.LinearLayout] */
    /* JADX WARN: Type inference failed for: r6v5, types: [android.widget.LinearLayout] */
    public final void configureEmbedFields(Model model, MessageRenderContext messageRenderContext) {
        ?? r5;
        List<Model.ParsedField> parsedFields = model.getParsedFields();
        this.binding.j.removeAllViews();
        if (parsedFields == null) {
            LinearLayout linearLayout = this.binding.j;
            m.checkNotNullExpressionValue(linearLayout, "binding.chatListItemEmbedFields");
            linearLayout.setVisibility(8);
            return;
        }
        LinearLayout linearLayout2 = this.binding.j;
        m.checkNotNullExpressionValue(linearLayout2, "binding.chatListItemEmbedFields");
        linearLayout2.setVisibility(0);
        LinearLayout linearLayout3 = this.binding.j;
        m.checkNotNullExpressionValue(linearLayout3, "binding.chatListItemEmbedFields");
        Context context = linearLayout3.getContext();
        long id2 = model.getEmbedEntry().getMessage().getId();
        int embedIndex = model.getEmbedEntry().getEmbedIndex();
        StoreMessageState.State messageState = model.getEmbedEntry().getMessageState();
        WidgetChatListAdapterItemEmbed$configureEmbedFields$1 widgetChatListAdapterItemEmbed$configureEmbedFields$1 = new WidgetChatListAdapterItemEmbed$configureEmbedFields$1(model, messageState != null ? messageState.getVisibleSpoilerEmbedMap() : null, embedIndex, messageRenderContext, id2);
        int size = parsedFields.size();
        for (int i = 0; i < size; i++) {
            LinearLayout linearLayout4 = this.binding.j;
            m.checkNotNullExpressionValue(linearLayout4, "binding.chatListItemEmbedFields");
            int childCount = linearLayout4.getChildCount();
            int i2 = R.id.chat_list_item_embed_field_value;
            if (i < childCount) {
                r5 = this.binding.j.getChildAt(i);
            } else {
                View inflate = LayoutInflater.from(context).inflate(R.layout.widget_chat_list_adapter_item_embed_field, (ViewGroup) this.binding.j, false);
                LinkifiedTextView linkifiedTextView = (LinkifiedTextView) inflate.findViewById(R.id.chat_list_item_embed_field_name);
                if (linkifiedTextView != null) {
                    LinkifiedTextView linkifiedTextView2 = (LinkifiedTextView) inflate.findViewById(R.id.chat_list_item_embed_field_value);
                    if (linkifiedTextView2 != null) {
                        r5 = (LinearLayout) inflate;
                        m.checkNotNullExpressionValue(new t4(r5, linkifiedTextView, linkifiedTextView2), "WidgetChatListAdapterIte…edFields, false\n        )");
                    }
                } else {
                    i2 = R.id.chat_list_item_embed_field_name;
                }
                throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
            }
            ((SimpleDraweeSpanTextView) r5.findViewById(R.id.chat_list_item_embed_field_name)).setDraweeSpanStringBuilder(widgetChatListAdapterItemEmbed$configureEmbedFields$1.invoke2((Collection<? extends Node<MessageRenderContext>>) parsedFields.get(i).getParsedName(), a.p("f_name:", i)));
            ((SimpleDraweeSpanTextView) r5.findViewById(R.id.chat_list_item_embed_field_value)).setDraweeSpanStringBuilder(widgetChatListAdapterItemEmbed$configureEmbedFields$1.invoke2((Collection<? extends Node<MessageRenderContext>>) parsedFields.get(i).getParsedValue(), "f_value:" + i));
            this.binding.j.addView(r5);
        }
    }

    private final void configureEmbedImage(final MessageEmbed messageEmbed) {
        Integer num;
        EmbedResourceUtils embedResourceUtils = EmbedResourceUtils.INSTANCE;
        final RenderableEmbedMedia previewImage = embedResourceUtils.getPreviewImage(messageEmbed);
        int i = 8;
        if (previewImage == null) {
            CardView cardView = this.binding.f2297s;
            m.checkNotNullExpressionValue(cardView, "binding.embedImageContainer");
            cardView.setVisibility(8);
            return;
        }
        boolean shouldRenderMedia = shouldRenderMedia();
        Integer num2 = previewImage.f2679b;
        boolean z2 = true;
        boolean z3 = num2 != null && d0.a0.a.getSign(num2.intValue()) == 1 && (num = previewImage.c) != null && d0.a0.a.getSign(num.intValue()) == 1;
        if (!shouldRenderMedia || !z3) {
            CardView cardView2 = this.binding.f2297s;
            m.checkNotNullExpressionValue(cardView2, "binding.embedImageContainer");
            cardView2.setVisibility(8);
        } else {
            SimpleDraweeView simpleDraweeView = this.binding.m;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.chatListItemEmbedImage");
            int i2 = this.maxEmbedImageWidth;
            int i3 = i2 / 2;
            Integer num3 = previewImage.f2679b;
            int intValue = num3 != null ? num3.intValue() : 0;
            Integer num4 = previewImage.c;
            configureEmbedImage(simpleDraweeView, i2, intValue, num4 != null ? num4.intValue() : 0, previewImage.a, i3);
            CardView cardView3 = this.binding.f2297s;
            m.checkNotNullExpressionValue(cardView3, "binding.embedImageContainer");
            cardView3.setVisibility(0);
        }
        ImageView imageView = this.binding.n;
        m.checkNotNullExpressionValue(imageView, "binding.chatListItemEmbedImageIcons");
        if (!embedResourceUtils.isPlayable(messageEmbed) || !shouldRenderMedia) {
            z2 = false;
        }
        if (z2) {
            i = 0;
        }
        imageView.setVisibility(i);
        this.binding.m.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$configureEmbedImage$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                String externalOpenUrl = EmbedResourceUtils.INSTANCE.getExternalOpenUrl(MessageEmbed.this);
                if (externalOpenUrl != null) {
                    UriHandler.handleOrUntrusted(a.x(view, "view", "view.context"), externalOpenUrl, messageEmbed.j());
                } else {
                    WidgetMedia.Companion.launch(a.x(view, "view", "view.context"), messageEmbed);
                }
            }
        });
    }

    public static /* synthetic */ void configureEmbedImage$default(WidgetChatListAdapterItemEmbed widgetChatListAdapterItemEmbed, EmbedThumbnail embedThumbnail, ImageView imageView, int i, int i2, int i3, Object obj) {
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        widgetChatListAdapterItemEmbed.configureEmbedImage(embedThumbnail, imageView, i, i2);
    }

    private final void configureEmbedProvider(MessageEmbed messageEmbed, MessageRenderContext messageRenderContext) {
        EmbedProvider g = messageEmbed.g();
        if (g != null) {
            String a = g.a();
            TextView textView = this.binding.p;
            m.checkNotNullExpressionValue(textView, "binding.chatListItemEmbedProvider");
            textView.setText(a);
            Companion companion = Companion;
            TextView textView2 = this.binding.p;
            m.checkNotNullExpressionValue(textView2, "binding.chatListItemEmbedProvider");
            companion.bindUrlOnClick(textView2, g.b(), g.a());
            TextView textView3 = this.binding.p;
            m.checkNotNullExpressionValue(textView3, "binding.chatListItemEmbedProvider");
            textView3.setVisibility(0);
            return;
        }
        TextView textView4 = this.binding.p;
        m.checkNotNullExpressionValue(textView4, "binding.chatListItemEmbedProvider");
        textView4.setVisibility(8);
    }

    private final void configureEmbedThumbnail(MessageEmbed messageEmbed) {
        boolean z2 = messageEmbed.k() == EmbedType.LINK || messageEmbed.k() == EmbedType.HTML || messageEmbed.k() == EmbedType.RICH;
        EmbedThumbnail h = messageEmbed.h();
        if (!z2 || h == null) {
            SimpleDraweeView simpleDraweeView = this.binding.o;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.chatListItemEmbedImageThumbnail");
            simpleDraweeView.setVisibility(8);
            return;
        }
        SimpleDraweeView simpleDraweeView2 = this.binding.o;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.chatListItemEmbedImageThumbnail");
        simpleDraweeView2.setVisibility(0);
        SimpleDraweeView simpleDraweeView3 = this.binding.o;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.chatListItemEmbedImageThumbnail");
        configureEmbedImage$default(this, h, simpleDraweeView3, this.embedThumbnailMaxSize, 0, 8, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureEmbedTitle(Model model, MessageRenderContext messageRenderContext) {
        MessageRenderContext copy;
        int i;
        Map<Integer, Set<String>> visibleSpoilerEmbedMap;
        MessageEmbed embed = model.getEmbedEntry().getEmbed();
        String j = embed.j();
        if (j != null) {
            int embedIndex = model.getEmbedEntry().getEmbedIndex();
            List parse$default = Parser.parse$default(UI_THREAD_TITLES_PARSER, j, MessageParseState.Companion.getInitialState(), null, 4, null);
            long myId = model.getMyId();
            StoreMessageState.State messageState = model.getEmbedEntry().getMessageState();
            new MessagePreprocessor(myId, (messageState == null || (visibleSpoilerEmbedMap = messageState.getVisibleSpoilerEmbedMap()) == null) ? null : Companion.getEmbedFieldVisibleIndices(visibleSpoilerEmbedMap, embedIndex, EMBED_TYPE_TITLE), null, false, null, 28, null).process(parse$default);
            LinkifiedTextView linkifiedTextView = this.binding.r;
            copy = messageRenderContext.copy((r31 & 1) != 0 ? messageRenderContext.getContext() : null, (r31 & 2) != 0 ? messageRenderContext.getMyId() : 0L, (r31 & 4) != 0 ? messageRenderContext.isAnimationEnabled() : false, (r31 & 8) != 0 ? messageRenderContext.getUserNames() : null, (r31 & 16) != 0 ? messageRenderContext.getChannelNames() : null, (r31 & 32) != 0 ? messageRenderContext.getRoles() : null, (r31 & 64) != 0 ? messageRenderContext.getLinkColorAttrResId() : 0, (r31 & 128) != 0 ? messageRenderContext.getOnClickUrl() : null, (r31 & 256) != 0 ? messageRenderContext.getOnLongPressUrl() : null, (r31 & 512) != 0 ? messageRenderContext.getSpoilerColorRes() : 0, (r31 & 1024) != 0 ? messageRenderContext.getSpoilerRevealedColorRes() : 0, (r31 & 2048) != 0 ? messageRenderContext.getSpoilerOnClick() : new WidgetChatListAdapterItemEmbed$configureEmbedTitle$1(model, embedIndex), (r31 & 4096) != 0 ? messageRenderContext.getUserMentionOnClick() : null, (r31 & 8192) != 0 ? messageRenderContext.getChannelMentionOnClick() : null);
            linkifiedTextView.setDraweeSpanStringBuilder(AstRenderer.render(parse$default, copy));
            LinkifiedTextView linkifiedTextView2 = this.binding.r;
            if (embed.l() != null) {
                LinkifiedTextView linkifiedTextView3 = this.binding.r;
                m.checkNotNullExpressionValue(linkifiedTextView3, "binding.chatListItemEmbedTitle");
                i = ColorCompat.getThemedColor(linkifiedTextView3, (int) R.attr.colorTextLink);
            } else {
                LinkifiedTextView linkifiedTextView4 = this.binding.r;
                m.checkNotNullExpressionValue(linkifiedTextView4, "binding.chatListItemEmbedTitle");
                i = ColorCompat.getThemedColor(linkifiedTextView4, (int) R.attr.primary_100);
            }
            linkifiedTextView2.setTextColor(i);
            Companion companion = Companion;
            LinkifiedTextView linkifiedTextView5 = this.binding.r;
            m.checkNotNullExpressionValue(linkifiedTextView5, "binding.chatListItemEmbedTitle");
            companion.bindUrlOnClick(linkifiedTextView5, embed.l(), embed.j());
            LinkifiedTextView linkifiedTextView6 = this.binding.r;
            m.checkNotNullExpressionValue(linkifiedTextView6, "binding.chatListItemEmbedTitle");
            linkifiedTextView6.setVisibility(0);
            return;
        }
        LinkifiedTextView linkifiedTextView7 = this.binding.r;
        m.checkNotNullExpressionValue(linkifiedTextView7, "binding.chatListItemEmbedTitle");
        linkifiedTextView7.setVisibility(8);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v9, types: [java.lang.CharSequence] */
    private final void configureFooter(EmbedFooter embedFooter, UtcDateTime utcDateTime) {
        String str;
        String str2 = null;
        String b2 = embedFooter != null ? embedFooter.b() : null;
        boolean z2 = true;
        int i = 0;
        if (b2 == null || b2.length() == 0) {
            SimpleDraweeView simpleDraweeView = this.binding.k;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.chatListItemEmbedFooterIcon");
            simpleDraweeView.setVisibility(8);
        } else {
            SimpleDraweeView simpleDraweeView2 = this.binding.k;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.chatListItemEmbedFooterIcon");
            simpleDraweeView2.setVisibility((embedFooter != null ? embedFooter.a() : null) != null ? 0 : 8);
            SimpleDraweeView simpleDraweeView3 = this.binding.k;
            m.checkNotNullExpressionValue(simpleDraweeView3, "binding.chatListItemEmbedFooterIcon");
            int i2 = this.embedTinyIconSize;
            configureEmbedImage$default(this, simpleDraweeView3, i2, i2, i2, embedFooter != null ? embedFooter.a() : null, 0, 32, null);
        }
        if (utcDateTime != null) {
            TextView textView = this.binding.l;
            m.checkNotNullExpressionValue(textView, "binding.chatListItemEmbedFooterText");
            Context context = textView.getContext();
            m.checkNotNullExpressionValue(context, "binding.chatListItemEmbedFooterText.context");
            str = TimeUtils.toReadableTimeString$default(context, utcDateTime.g(), null, 4, null);
        } else {
            str = null;
        }
        TextView textView2 = this.binding.l;
        if (embedFooter != null && str != null) {
            str2 = embedFooter.b() + " | " + ((CharSequence) str);
        } else if (embedFooter != null) {
            str2 = embedFooter.b();
        } else if (str != null) {
            str2 = str;
        }
        textView2.setText(str2);
        CharSequence text = textView2.getText();
        m.checkNotNullExpressionValue(text, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        if (text.length() <= 0) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        textView2.setVisibility(i);
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x0064  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00c8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureInlineEmbed(final com.discord.api.message.embed.MessageEmbed r17, boolean r18) {
        /*
            Method dump skipped, instructions count: 234
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed.configureInlineEmbed(com.discord.api.message.embed.MessageEmbed, boolean):void");
    }

    private final void configureUI(final Model model) {
        final EmbedEntry embedEntry = model.getEmbedEntry();
        MessageEmbed embed = embedEntry.getEmbed();
        MessageRenderContext createRenderContext = model.createRenderContext(a.x(this.itemView, "itemView", "itemView.context"), ((WidgetChatListAdapter) this.adapter).getEventHandler());
        if (model.isSpoilerHidden()) {
            ViewExtensions.fadeIn$default(this.binding.q, 50L, null, WidgetChatListAdapterItemEmbed$configureUI$1.INSTANCE, null, 10, null);
        } else {
            ViewExtensions.fadeOut$default(this.binding.q, 200L, WidgetChatListAdapterItemEmbed$configureUI$2.INSTANCE, null, 4, null);
        }
        this.binding.q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (WidgetChatListAdapterItemEmbed.access$getAdapter$p(WidgetChatListAdapterItemEmbed.this).getData().isSpoilerClickAllowed()) {
                    StoreStream.Companion.getMessageState().revealSpoilerEmbed(model.getEmbedEntry().getMessage().getId(), embedEntry.getEmbedIndex());
                } else {
                    WidgetChatListAdapterItemEmbed.access$getAdapter$p(WidgetChatListAdapterItemEmbed.this).getEventHandler().onMessageClicked(model.getEmbedEntry().getMessage(), embedEntry.isThreadStarterMessage());
                }
            }
        });
        try {
            if (EmbedResourceUtils.INSTANCE.isInlineEmbed(embed)) {
                ConstraintLayout constraintLayout = this.binding.g;
                m.checkNotNullExpressionValue(constraintLayout, "binding.chatListItemEmbedContent");
                constraintLayout.setVisibility(8);
                View view = this.binding.i;
                m.checkNotNullExpressionValue(view, "binding.chatListItemEmbedDivider");
                view.setVisibility(8);
                SimpleDraweeView simpleDraweeView = this.binding.o;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.chatListItemEmbedImageThumbnail");
                simpleDraweeView.setVisibility(8);
                InlineMediaView inlineMediaView = this.binding.t;
                m.checkNotNullExpressionValue(inlineMediaView, "binding.embedInlineMedia");
                inlineMediaView.setVisibility(0);
                configureInlineEmbed(embed, embedEntry.getAutoPlayGifs());
                MaterialCardView materialCardView = this.binding.f;
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                materialCardView.setCardBackgroundColor(ColorCompat.getColor(view2, (int) R.color.transparent));
                MaterialCardView materialCardView2 = this.binding.f;
                m.checkNotNullExpressionValue(materialCardView2, "binding.chatListItemEmbedContainerCard");
                materialCardView2.setStrokeWidth(0);
            } else {
                ConstraintLayout constraintLayout2 = this.binding.g;
                m.checkNotNullExpressionValue(constraintLayout2, "binding.chatListItemEmbedContent");
                constraintLayout2.setVisibility(0);
                View view3 = this.binding.i;
                m.checkNotNullExpressionValue(view3, "binding.chatListItemEmbedDivider");
                view3.setVisibility(0);
                InlineMediaView inlineMediaView2 = this.binding.t;
                m.checkNotNullExpressionValue(inlineMediaView2, "binding.embedInlineMedia");
                inlineMediaView2.setVisibility(8);
                configureEmbedDivider(!model.isSpoilerHidden() ? embed.b() : null);
                configureEmbedProvider(embed, createRenderContext);
                configureEmbedAuthor(embed.a());
                configureEmbedTitle(model, createRenderContext);
                configureEmbedDescription(model, createRenderContext);
                configureEmbedThumbnail(embed);
                configureEmbedImage(embed);
                configureEmbedFields(model, createRenderContext);
                configureFooter(embed.e(), embed.i());
                MaterialCardView materialCardView3 = this.binding.f;
                View view4 = this.itemView;
                m.checkNotNullExpressionValue(view4, "itemView");
                materialCardView3.setCardBackgroundColor(ColorCompat.getThemedColor(view4, (int) R.attr.colorBackgroundSecondary));
                MaterialCardView materialCardView4 = this.binding.f;
                m.checkNotNullExpressionValue(materialCardView4, "binding.chatListItemEmbedContainerCard");
                MaterialCardView materialCardView5 = this.binding.f;
                m.checkNotNullExpressionValue(materialCardView5, "binding.chatListItemEmbedContainerCard");
                materialCardView4.setStrokeWidth(materialCardView5.getResources().getDimensionPixelSize(R.dimen.chat_embed_card_stroke_width));
            }
            View view5 = this.itemView;
            m.checkNotNullExpressionValue(view5, "itemView");
            view5.setVisibility(0);
        } catch (Exception e) {
            View view6 = this.itemView;
            m.checkNotNullExpressionValue(view6, "itemView");
            view6.setVisibility(8);
            Logger.e$default(AppLog.g, "Unable to render embed.", e, null, 4, null);
        }
    }

    private final List<Model.ParsedField> parseFields(List<EmbedField> list, Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser, Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser2) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        for (EmbedField embedField : list) {
            String a = embedField.a();
            MessageParseState.Companion companion = MessageParseState.Companion;
            arrayList.add(new Model.ParsedField(Parser.parse$default(parser, a, companion.getInitialState(), null, 4, null), Parser.parse$default(parser2, embedField.b(), companion.getInitialState(), null, 4, null)));
        }
        return arrayList;
    }

    private final boolean shouldRenderMedia() {
        return this.userSettings.getIsEmbedMediaInlined() && this.userSettings.getIsRenderEmbedsEnabled();
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.subscription;
    }

    @Override // com.discord.widgets.chat.list.FragmentLifecycleListener
    public void onPause() {
        this.binding.t.onPause();
    }

    @Override // com.discord.widgets.chat.list.FragmentLifecycleListener
    public void onResume() {
        this.binding.t.onResume();
    }

    public static /* synthetic */ void configureEmbedImage$default(WidgetChatListAdapterItemEmbed widgetChatListAdapterItemEmbed, ImageView imageView, int i, int i2, int i3, String str, int i4, int i5, Object obj) {
        widgetChatListAdapterItemEmbed.configureEmbedImage(imageView, i, i2, i3, str, (i5 & 32) != 0 ? 0 : i4);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, final ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        EmbedEntry embedEntry = (EmbedEntry) chatListEntry;
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        ViewExtensions.setOnLongClickListenerConsumeClick(view, new WidgetChatListAdapterItemEmbed$onConfigure$1(this, chatListEntry));
        this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$onConfigure$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetChatListAdapterItemEmbed.access$getAdapter$p(WidgetChatListAdapterItemEmbed.this).getEventHandler().onMessageClicked(((EmbedEntry) chatListEntry).getMessage(), ((EmbedEntry) chatListEntry).isThreadStarterMessage());
            }
        });
        Message message = embedEntry.getMessage();
        View view2 = this.binding.c;
        m.checkNotNullExpressionValue(view2, "binding.chatListAdapterItemHighlightedBg");
        View view3 = this.binding.f2296b;
        m.checkNotNullExpressionValue(view3, "binding.chatListAdapterItemGutterBg");
        configureCellHighlight(message, view2, view3);
        String c = embedEntry.getEmbed().c();
        Model model = new Model(embedEntry, c != null ? Parser.parse$default(UI_THREAD_VALUES_PARSER, c, MessageParseState.Companion.getInitialState(), null, 4, null) : null, parseFields(embedEntry.getEmbed().d(), UI_THREAD_TITLES_PARSER, UI_THREAD_VALUES_PARSER), null, null, null, 0L, 120, null);
        configureUI(model);
        Observable Z = Companion.getModel(model).Z(1);
        m.checkNotNullExpressionValue(Z, "getModel(initialModel)\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(Z), WidgetChatListAdapterItemEmbed.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetChatListAdapterItemEmbed$onConfigure$3(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterItemEmbed$onConfigure$4(this));
    }

    /* compiled from: WidgetChatListAdapterItemEmbed.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\b\b\u0082\b\u0018\u00002\u00020\u0001:\u0001KB\u009f\u0001\u0012\u0006\u0010\"\u001a\u00020\t\u0012 \u0010#\u001a\u001c\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\r\u0018\u00010\fj\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`\u000e\u0012\u000e\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011\u0012\u001a\b\u0002\u0010%\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u0017\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0015\u0012\u001a\b\u0002\u0010&\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u001b\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0015\u0012\u001a\b\u0002\u0010'\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u001d\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u0015\u0012\b\b\u0002\u0010(\u001a\u00020\u0016¢\u0006\u0004\bI\u0010JJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ*\u0010\u000f\u001a\u001c\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\r\u0018\u00010\fj\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0018\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\"\u0010\u0019\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u0017\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\"\u0010\u001c\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u001b\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001aJ\"\u0010\u001f\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u001d\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u001f\u0010\u001aJ\u0010\u0010 \u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b \u0010!J®\u0001\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\"\u001a\u00020\t2\"\b\u0002\u0010#\u001a\u001c\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\r\u0018\u00010\fj\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`\u000e2\u0010\b\u0002\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00112\u001a\b\u0002\u0010%\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u0017\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u00152\u001a\b\u0002\u0010&\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u001b\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u00152\u001a\b\u0002\u0010'\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u001d\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u00152\b\b\u0002\u0010(\u001a\u00020\u0016HÆ\u0001¢\u0006\u0004\b)\u0010*J\u0010\u0010+\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b+\u0010,J\u0010\u0010.\u001a\u00020-HÖ\u0001¢\u0006\u0004\b.\u0010/J\u001a\u00102\u001a\u0002012\b\u00100\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b2\u00103R+\u0010%\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u0017\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b%\u00104\u001a\u0004\b5\u0010\u001aR+\u0010&\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u001b\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b&\u00104\u001a\u0004\b6\u0010\u001aR\u0019\u0010(\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b(\u00107\u001a\u0004\b8\u0010!R!\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b$\u00109\u001a\u0004\b:\u0010\u0014R\u0016\u0010;\u001a\u0002018B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b;\u0010<R+\u0010'\u001a\u0014\u0012\b\u0012\u00060\u0016j\u0002`\u001d\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b'\u00104\u001a\u0004\b=\u0010\u001aR\u0019\u0010>\u001a\u0002018\u0006@\u0006¢\u0006\f\n\u0004\b>\u0010?\u001a\u0004\b>\u0010<R3\u0010#\u001a\u001c\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\r\u0018\u00010\fj\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010@\u001a\u0004\bA\u0010\u0010R\u001a\u0010B\u001a\u000201*\u00020\t8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bB\u0010CR\"\u0010E\u001a\u000e\u0012\b\u0012\u0006\u0012\u0002\b\u00030D\u0018\u00010\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bE\u00109R\u0019\u0010\"\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010F\u001a\u0004\bG\u0010\u000bR\u0016\u0010H\u001a\u0002018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bH\u0010?¨\u0006L"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;", "", "Landroid/content/Context;", "androidContext", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;", "eventHandler", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "createRenderContext", "(Landroid/content/Context;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$EventHandler;)Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/chat/list/entries/EmbedEntry;", "component1", "()Lcom/discord/widgets/chat/list/entries/EmbedEntry;", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/simpleast/core/utils/Ast;", "component2", "()Ljava/util/Collection;", "", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model$ParsedField;", "component3", "()Ljava/util/List;", "", "", "Lcom/discord/primitives/ChannelId;", "", "component4", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "component5", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component6", "component7", "()J", "embedEntry", "parsedDescription", "parsedFields", "channelNames", "userNames", "roles", "myId", "copy", "(Lcom/discord/widgets/chat/list/entries/EmbedEntry;Ljava/util/Collection;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;J)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getChannelNames", "getUserNames", "J", "getMyId", "Ljava/util/List;", "getParsedFields", "isSpoilerEmbed", "()Z", "getRoles", "isSpoilerHidden", "Z", "Ljava/util/Collection;", "getParsedDescription", "isSpoilerEmbedRevealed", "(Lcom/discord/widgets/chat/list/entries/EmbedEntry;)Z", "Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "spoilers", "Lcom/discord/widgets/chat/list/entries/EmbedEntry;", "getEmbedEntry", "isEmbedUrlFoundInVisibleSpoilerNode", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/entries/EmbedEntry;Ljava/util/Collection;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;J)V", "ParsedField", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        private final Map<Long, String> channelNames;
        private final EmbedEntry embedEntry;
        private final boolean isEmbedUrlFoundInVisibleSpoilerNode;
        private final boolean isSpoilerHidden;
        private final long myId;
        private final Collection<Node<MessageRenderContext>> parsedDescription;
        private final List<ParsedField> parsedFields;
        private final Map<Long, GuildRole> roles;
        private final List<SpoilerNode<?>> spoilers;
        private final Map<Long, String> userNames;

        /* compiled from: WidgetChatListAdapterItemEmbed.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\u001c\u0010\t\u001a\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u0012\u001c\u0010\n\u001a\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0004`\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ&\u0010\u0006\u001a\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0004`\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\b\u001a\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0004`\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007JP\u0010\u000b\u001a\u00020\u00002\u001e\b\u0002\u0010\t\u001a\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0004`\u00052\u001e\b\u0002\u0010\n\u001a\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0004`\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R/\u0010\n\u001a\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0004`\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R/\u0010\t\u001a\u0018\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0004`\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model$ParsedField;", "", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/simpleast/core/utils/Ast;", "component1", "()Ljava/util/Collection;", "component2", "parsedName", "parsedValue", "copy", "(Ljava/util/Collection;Ljava/util/Collection;)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEmbed$Model$ParsedField;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Collection;", "getParsedValue", "getParsedName", HookHelper.constructorName, "(Ljava/util/Collection;Ljava/util/Collection;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ParsedField {
            private final Collection<Node<MessageRenderContext>> parsedName;
            private final Collection<Node<MessageRenderContext>> parsedValue;

            /* JADX WARN: Multi-variable type inference failed */
            public ParsedField(Collection<? extends Node<MessageRenderContext>> collection, Collection<? extends Node<MessageRenderContext>> collection2) {
                m.checkNotNullParameter(collection, "parsedName");
                m.checkNotNullParameter(collection2, "parsedValue");
                this.parsedName = collection;
                this.parsedValue = collection2;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ ParsedField copy$default(ParsedField parsedField, Collection collection, Collection collection2, int i, Object obj) {
                if ((i & 1) != 0) {
                    collection = parsedField.parsedName;
                }
                if ((i & 2) != 0) {
                    collection2 = parsedField.parsedValue;
                }
                return parsedField.copy(collection, collection2);
            }

            public final Collection<Node<MessageRenderContext>> component1() {
                return this.parsedName;
            }

            public final Collection<Node<MessageRenderContext>> component2() {
                return this.parsedValue;
            }

            public final ParsedField copy(Collection<? extends Node<MessageRenderContext>> collection, Collection<? extends Node<MessageRenderContext>> collection2) {
                m.checkNotNullParameter(collection, "parsedName");
                m.checkNotNullParameter(collection2, "parsedValue");
                return new ParsedField(collection, collection2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ParsedField)) {
                    return false;
                }
                ParsedField parsedField = (ParsedField) obj;
                return m.areEqual(this.parsedName, parsedField.parsedName) && m.areEqual(this.parsedValue, parsedField.parsedValue);
            }

            public final Collection<Node<MessageRenderContext>> getParsedName() {
                return this.parsedName;
            }

            public final Collection<Node<MessageRenderContext>> getParsedValue() {
                return this.parsedValue;
            }

            public int hashCode() {
                Collection<Node<MessageRenderContext>> collection = this.parsedName;
                int i = 0;
                int hashCode = (collection != null ? collection.hashCode() : 0) * 31;
                Collection<Node<MessageRenderContext>> collection2 = this.parsedValue;
                if (collection2 != null) {
                    i = collection2.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("ParsedField(parsedName=");
                R.append(this.parsedName);
                R.append(", parsedValue=");
                R.append(this.parsedValue);
                R.append(")");
                return R.toString();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(EmbedEntry embedEntry, Collection<? extends Node<MessageRenderContext>> collection, List<ParsedField> list, Map<Long, String> map, Map<Long, String> map2, Map<Long, GuildRole> map3, long j) {
            boolean z2;
            boolean z3;
            Set<Integer> visibleSpoilerNodeIndices;
            m.checkNotNullParameter(embedEntry, "embedEntry");
            this.embedEntry = embedEntry;
            this.parsedDescription = collection;
            this.parsedFields = list;
            this.channelNames = map;
            this.userNames = map2;
            this.roles = map3;
            this.myId = j;
            Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> createTitlesParser = WidgetChatListAdapterItemEmbed.Companion.createTitlesParser();
            String content = embedEntry.getMessage().getContent();
            List parse$default = Parser.parse$default(createTitlesParser, content == null ? "" : content, MessageParseState.Companion.getInitialState(), null, 4, null);
            MessagePreprocessor messagePreprocessor = new MessagePreprocessor(j, embedEntry.getMessageState());
            messagePreprocessor.process(parse$default);
            List<SpoilerNode<?>> spoilers = messagePreprocessor.getSpoilers();
            this.spoilers = spoilers;
            boolean z4 = true;
            if (spoilers != null) {
                StoreMessageState.State messageState = embedEntry.getMessageState();
                if (!(messageState == null || (visibleSpoilerNodeIndices = messageState.getVisibleSpoilerNodeIndices()) == null)) {
                    ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(visibleSpoilerNodeIndices, 10));
                    for (Number number : visibleSpoilerNodeIndices) {
                        arrayList.add(this.spoilers.get(number.intValue()));
                    }
                    final String l = embedEntry.getEmbed().l();
                    try {
                        b.c.a.a0.d.i2(arrayList, new b.a.t.b.c.a() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$Model$isEmbedUrlFoundInVisibleSpoilerNode$1$2$1
                            @Override // b.a.t.b.c.a
                            public final void processNode(Node<Object> node) {
                                if ((node instanceof UrlNode) && t.equals(((UrlNode) node).getUrl(), l, true)) {
                                    throw new RuntimeException() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEmbed$Model$isEmbedUrlFoundInVisibleSpoilerNode$1$2$MatchFoundException
                                    };
                                }
                            }
                        });
                    } catch (WidgetChatListAdapterItemEmbed$Model$isEmbedUrlFoundInVisibleSpoilerNode$1$2$MatchFoundException unused) {
                        z3 = true;
                    }
                }
                z3 = false;
                if (z3) {
                    z2 = true;
                    this.isEmbedUrlFoundInVisibleSpoilerNode = z2;
                    this.isSpoilerHidden = (!isSpoilerEmbedRevealed(this.embedEntry) || !isSpoilerEmbed()) ? false : z4;
                }
            }
            z2 = false;
            this.isEmbedUrlFoundInVisibleSpoilerNode = z2;
            this.isSpoilerHidden = (!isSpoilerEmbedRevealed(this.embedEntry) || !isSpoilerEmbed()) ? false : z4;
        }

        private final boolean isSpoilerEmbed() {
            boolean z2;
            List<SpoilerNode<?>> list = this.spoilers;
            if (list != null) {
                if (!list.isEmpty()) {
                    Iterator<T> it = list.iterator();
                    while (it.hasNext()) {
                        if (w.contains$default((CharSequence) ((SpoilerNode) it.next()).getContent(), (CharSequence) "http", false, 2, (Object) null)) {
                            z2 = true;
                            break;
                        }
                    }
                }
                z2 = false;
                if (z2 && !this.isEmbedUrlFoundInVisibleSpoilerNode) {
                    return true;
                }
            }
            return false;
        }

        private final boolean isSpoilerEmbedRevealed(EmbedEntry embedEntry) {
            Map<Integer, Set<String>> visibleSpoilerEmbedMap;
            StoreMessageState.State messageState = embedEntry.getMessageState();
            return (messageState == null || (visibleSpoilerEmbedMap = messageState.getVisibleSpoilerEmbedMap()) == null || !visibleSpoilerEmbedMap.containsKey(Integer.valueOf(embedEntry.getEmbedIndex()))) ? false : true;
        }

        public final EmbedEntry component1() {
            return this.embedEntry;
        }

        public final Collection<Node<MessageRenderContext>> component2() {
            return this.parsedDescription;
        }

        public final List<ParsedField> component3() {
            return this.parsedFields;
        }

        public final Map<Long, String> component4() {
            return this.channelNames;
        }

        public final Map<Long, String> component5() {
            return this.userNames;
        }

        public final Map<Long, GuildRole> component6() {
            return this.roles;
        }

        public final long component7() {
            return this.myId;
        }

        public final Model copy(EmbedEntry embedEntry, Collection<? extends Node<MessageRenderContext>> collection, List<ParsedField> list, Map<Long, String> map, Map<Long, String> map2, Map<Long, GuildRole> map3, long j) {
            m.checkNotNullParameter(embedEntry, "embedEntry");
            return new Model(embedEntry, collection, list, map, map2, map3, j);
        }

        public final MessageRenderContext createRenderContext(Context context, WidgetChatListAdapter.EventHandler eventHandler) {
            m.checkNotNullParameter(context, "androidContext");
            m.checkNotNullParameter(eventHandler, "eventHandler");
            return new MessageRenderContext(context, this.myId, this.embedEntry.getAllowAnimatedEmojis(), this.userNames, this.channelNames, this.roles, 0, null, new WidgetChatListAdapterItemEmbed$Model$createRenderContext$1(eventHandler), 0, 0, null, null, null, 16064, null);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.embedEntry, model.embedEntry) && m.areEqual(this.parsedDescription, model.parsedDescription) && m.areEqual(this.parsedFields, model.parsedFields) && m.areEqual(this.channelNames, model.channelNames) && m.areEqual(this.userNames, model.userNames) && m.areEqual(this.roles, model.roles) && this.myId == model.myId;
        }

        public final Map<Long, String> getChannelNames() {
            return this.channelNames;
        }

        public final EmbedEntry getEmbedEntry() {
            return this.embedEntry;
        }

        public final long getMyId() {
            return this.myId;
        }

        public final Collection<Node<MessageRenderContext>> getParsedDescription() {
            return this.parsedDescription;
        }

        public final List<ParsedField> getParsedFields() {
            return this.parsedFields;
        }

        public final Map<Long, GuildRole> getRoles() {
            return this.roles;
        }

        public final Map<Long, String> getUserNames() {
            return this.userNames;
        }

        public int hashCode() {
            EmbedEntry embedEntry = this.embedEntry;
            int i = 0;
            int hashCode = (embedEntry != null ? embedEntry.hashCode() : 0) * 31;
            Collection<Node<MessageRenderContext>> collection = this.parsedDescription;
            int hashCode2 = (hashCode + (collection != null ? collection.hashCode() : 0)) * 31;
            List<ParsedField> list = this.parsedFields;
            int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
            Map<Long, String> map = this.channelNames;
            int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, String> map2 = this.userNames;
            int hashCode5 = (hashCode4 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map3 = this.roles;
            if (map3 != null) {
                i = map3.hashCode();
            }
            return b.a(this.myId) + ((hashCode5 + i) * 31);
        }

        public final boolean isSpoilerHidden() {
            return this.isSpoilerHidden;
        }

        public String toString() {
            StringBuilder R = a.R("Model(embedEntry=");
            R.append(this.embedEntry);
            R.append(", parsedDescription=");
            R.append(this.parsedDescription);
            R.append(", parsedFields=");
            R.append(this.parsedFields);
            R.append(", channelNames=");
            R.append(this.channelNames);
            R.append(", userNames=");
            R.append(this.userNames);
            R.append(", roles=");
            R.append(this.roles);
            R.append(", myId=");
            return a.B(R, this.myId, ")");
        }

        public /* synthetic */ Model(EmbedEntry embedEntry, Collection collection, List list, Map map, Map map2, Map map3, long j, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(embedEntry, collection, list, (i & 8) != 0 ? null : map, (i & 16) != 0 ? null : map2, (i & 32) != 0 ? null : map3, (i & 64) != 0 ? 0L : j);
        }
    }

    private final void configureEmbedImage(EmbedThumbnail embedThumbnail, ImageView imageView, int i, int i2) {
        Integer d = embedThumbnail.d();
        int intValue = d != null ? d.intValue() : 0;
        Integer a = embedThumbnail.a();
        configureEmbedImage(imageView, i, intValue, a != null ? a.intValue() : 0, embedThumbnail.b(), i2);
    }

    private final void configureEmbedImage(ImageView imageView, int i, int i2, int i3, String str, int i4) {
        if (str != null) {
            imageView.setVisibility(0);
            EmbedResourceUtils embedResourceUtils = EmbedResourceUtils.INSTANCE;
            InlineMediaView inlineMediaView = this.binding.t;
            m.checkNotNullExpressionValue(inlineMediaView, "binding.embedInlineMedia");
            Resources resources = inlineMediaView.getResources();
            m.checkNotNullExpressionValue(resources, "binding.embedInlineMedia.resources");
            Pair<Integer, Integer> calculateScaledSize = embedResourceUtils.calculateScaledSize(i2, i3, i, i, resources, i4);
            int intValue = calculateScaledSize.component1().intValue();
            int intValue2 = calculateScaledSize.component2().intValue();
            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            if (!(layoutParams.width == intValue && layoutParams.height == intValue2)) {
                layoutParams.width = intValue;
                layoutParams.height = intValue2;
                imageView.requestLayout();
            }
            MGImages.setImage$default(imageView, EmbedResourceUtils.getPreviewUrls$default(embedResourceUtils, str, intValue, intValue2, false, 8, null), 0, 0, false, null, null, null, 252, null);
        }
    }
}
