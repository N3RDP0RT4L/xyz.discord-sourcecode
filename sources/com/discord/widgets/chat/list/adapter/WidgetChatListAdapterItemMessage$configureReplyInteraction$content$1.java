package com.discord.widgets.chat.list.adapter;

import android.text.style.StyleSpan;
import android.view.View;
import com.discord.api.interaction.Interaction;
import com.discord.api.user.User;
import com.discord.i18n.Hook;
import com.discord.i18n.RenderContext;
import com.discord.models.guild.Guild;
import com.discord.models.message.Message;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.spans.ClickableSpan;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ Interaction $interaction;
    public final /* synthetic */ User $interactionUser;
    public final /* synthetic */ Message $message;
    public final /* synthetic */ WidgetChatListAdapterItemMessage this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1(WidgetChatListAdapterItemMessage widgetChatListAdapterItemMessage, Interaction interaction, Message message, User user) {
        super(1);
        this.this$0 = widgetChatListAdapterItemMessage;
        this.$interaction = interaction;
        this.$message = message;
        this.$interactionUser = user;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        m.checkNotNullParameter(renderContext, "$receiver");
        renderContext.a("commandNameOnClick", new AnonymousClass1());
    }

    /* compiled from: WidgetChatListAdapterItemMessage.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/Hook;", "", "invoke", "(Lcom/discord/i18n/Hook;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Hook, Unit> {

        /* compiled from: WidgetChatListAdapterItemMessage.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02441 extends o implements Function1<View, Unit> {
            public C02441() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(View view) {
                invoke2(view);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(View view) {
                m.checkNotNullParameter(view, "it");
                Guild guild = WidgetChatListAdapterItemMessage.access$getAdapter$p(WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.this$0).getData().getGuild();
                Long valueOf = guild != null ? Long.valueOf(guild.getId()) : null;
                Long a = WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$interaction.a();
                if (a != null) {
                    long longValue = a.longValue();
                    if (!WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$message.isLocalApplicationCommand() || WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$message.isFailed()) {
                        Integer type = WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$message.getType();
                        if (type == null || type.intValue() != 23) {
                            WidgetChatListAdapter.EventHandler eventHandler = WidgetChatListAdapterItemMessage.access$getAdapter$p(WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.this$0).getEventHandler();
                            long channelId = WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$message.getChannelId();
                            long id2 = WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$message.getId();
                            long i = WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$interactionUser.i();
                            User author = WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$message.getAuthor();
                            m.checkNotNull(author);
                            eventHandler.onCommandClicked(longValue, valueOf, channelId, id2, i, author.i(), WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.$message.getNonce());
                        }
                    }
                }
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Hook hook) {
            SimpleDraweeSpanTextView simpleDraweeSpanTextView;
            m.checkNotNullParameter(hook, "$receiver");
            hook.a.add(new StyleSpan(1));
            List<Object> list = hook.a;
            simpleDraweeSpanTextView = WidgetChatListAdapterItemMessage$configureReplyInteraction$content$1.this.this$0.replyText;
            list.add(new ClickableSpan(Integer.valueOf(ColorCompat.getThemedColor(simpleDraweeSpanTextView, (int) R.attr.colorTextLink)), false, null, new C02441(), 4, null));
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Hook hook) {
            invoke2(hook);
            return Unit.a;
        }
    }
}
