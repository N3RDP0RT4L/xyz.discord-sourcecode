package com.discord.widgets.chat.list.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import b.a.k.b;
import com.discord.api.activity.ActivityActionConfirmation;
import com.discord.api.application.Application;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.uri.UriHandler;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterEventsHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/activity/ActivityActionConfirmation;", "<name for destructuring parameter 0>", "", "invoke", "(Lcom/discord/api/activity/ActivityActionConfirmation;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterEventsHandler$onUserActivityAction$1 extends o implements Function1<ActivityActionConfirmation, Unit> {
    public final /* synthetic */ Application $application;
    public final /* synthetic */ WidgetChatListAdapterEventsHandler this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterEventsHandler$onUserActivityAction$1(WidgetChatListAdapterEventsHandler widgetChatListAdapterEventsHandler, Application application) {
        super(1);
        this.this$0 = widgetChatListAdapterEventsHandler;
        this.$application = application;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ActivityActionConfirmation activityActionConfirmation) {
        invoke2(activityActionConfirmation);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ActivityActionConfirmation activityActionConfirmation) {
        Context context;
        Context context2;
        CharSequence b2;
        Context context3;
        Context context4;
        m.checkNotNullParameter(activityActionConfirmation, "<name for destructuring parameter 0>");
        try {
            Intent join = IntentUtils.RouteBuilders.SDK.join(this.$application.a(), this.$application.g(), activityActionConfirmation.a());
            join.addFlags(268435456);
            context4 = this.this$0.getContext();
            context4.startActivity(join);
        } catch (ActivityNotFoundException unused) {
            context = this.this$0.getContext();
            context2 = this.this$0.getContext();
            b2 = b.b(context2, R.string.user_activity_not_detected, new Object[]{this.$application.h()}, (r4 & 4) != 0 ? b.C0034b.j : null);
            b.a.d.m.h(context, b2, 0, null, 12);
            String str = (String) u.firstOrNull((List<? extends Object>) this.$application.d());
            if (str != null) {
                context3 = this.this$0.getContext();
                UriHandler.directToPlayStore$default(context3, str, null, 4, null);
            }
        }
    }
}
