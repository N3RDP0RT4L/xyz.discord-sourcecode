package com.discord.widgets.chat.list.adapter;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.Job;
/* compiled from: WidgetChatListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u00002\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "<anonymous parameter 0>", "<anonymous parameter 1>", "", "invoke", "(Ljava/util/List;Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapter$setHandlers$1 extends o implements Function2<List<? extends ChatListEntry>, List<? extends ChatListEntry>, Unit> {
    public final /* synthetic */ WidgetChatListAdapter this$0;

    /* compiled from: WidgetChatListAdapter.kt */
    @e(c = "com.discord.widgets.chat.list.adapter.WidgetChatListAdapter$setHandlers$1$1", f = "WidgetChatListAdapter.kt", l = {117}, m = "invokeSuspend")
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapter$setHandlers$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        public int label;

        public AnonymousClass1(Continuation continuation) {
            super(2, continuation);
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            m.checkNotNullParameter(continuation, "completion");
            return new AnonymousClass1(continuation);
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
            return ((AnonymousClass1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            WidgetChatListAdapter.HandlerOfUpdates handlerOfUpdates;
            Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                l.throwOnFailure(obj);
                this.label = 1;
                if (f.P(250L, this) == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                l.throwOnFailure(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            handlerOfUpdates = WidgetChatListAdapter$setHandlers$1.this.this$0.handlerOfUpdates;
            handlerOfUpdates.run();
            return Unit.a;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapter$setHandlers$1(WidgetChatListAdapter widgetChatListAdapter) {
        super(2);
        this.this$0 = widgetChatListAdapter;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ChatListEntry> list, List<? extends ChatListEntry> list2) {
        invoke2(list, list2);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<? extends ChatListEntry> list, List<? extends ChatListEntry> list2) {
        Job job;
        WidgetChatListAdapter.HandlerOfTouches handlerOfTouches;
        WidgetChatListAdapter.HandlerOfScrolls handlerOfScrolls;
        WidgetChatListAdapter.HandlerOfScrolls handlerOfScrolls2;
        m.checkNotNullParameter(list, "<anonymous parameter 0>");
        m.checkNotNullParameter(list2, "<anonymous parameter 1>");
        job = this.this$0.lastUpdateJob;
        Job job2 = null;
        if (job != null) {
            f.t(job, null, 1, null);
        }
        WidgetChatListAdapter widgetChatListAdapter = this.this$0;
        CoroutineScope coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(widgetChatListAdapter.getRecycler());
        if (coroutineScope != null) {
            job2 = f.H0(coroutineScope, null, null, new AnonymousClass1(null), 3, null);
        }
        widgetChatListAdapter.lastUpdateJob = job2;
        RecyclerView recycler = this.this$0.getRecycler();
        handlerOfTouches = this.this$0.handlerOfTouches;
        recycler.setOnTouchListener(handlerOfTouches);
        RecyclerView recycler2 = this.this$0.getRecycler();
        handlerOfScrolls = this.this$0.handlerOfScrolls;
        recycler2.removeOnScrollListener(handlerOfScrolls);
        RecyclerView recycler3 = this.this$0.getRecycler();
        handlerOfScrolls2 = this.this$0.handlerOfScrolls;
        recycler3.addOnScrollListener(handlerOfScrolls2);
    }
}
