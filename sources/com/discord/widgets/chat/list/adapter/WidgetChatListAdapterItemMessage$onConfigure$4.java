package com.discord.widgets.chat.list.adapter;

import android.view.View;
import com.discord.models.message.Message;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemMessage$onConfigure$4 extends o implements Function1<View, Unit> {
    public final /* synthetic */ boolean $isThreadStarterMessage;
    public final /* synthetic */ Message $message;
    public final /* synthetic */ WidgetChatListAdapterItemMessage this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemMessage$onConfigure$4(WidgetChatListAdapterItemMessage widgetChatListAdapterItemMessage, Message message, boolean z2) {
        super(1);
        this.this$0 = widgetChatListAdapterItemMessage;
        this.$message = message;
        this.$isThreadStarterMessage = z2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        SimpleDraweeSpanTextView simpleDraweeSpanTextView;
        SimpleDraweeSpanTextView simpleDraweeSpanTextView2;
        m.checkNotNullParameter(view, "it");
        WidgetChatListAdapter.EventHandler eventHandler = WidgetChatListAdapterItemMessage.access$getAdapter$p(this.this$0).getEventHandler();
        Message message = this.$message;
        simpleDraweeSpanTextView = this.this$0.itemText;
        CharSequence text = simpleDraweeSpanTextView.getText();
        simpleDraweeSpanTextView2 = this.this$0.itemText;
        eventHandler.onMessageLongClicked(message, text.subSequence(0, Math.max(simpleDraweeSpanTextView2.getText().length() - 1, 0)), this.$isThreadStarterMessage);
    }
}
