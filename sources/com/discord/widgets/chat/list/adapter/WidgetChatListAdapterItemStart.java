package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.a.k.b;
import com.discord.databinding.WidgetChatListAdapterItemStartBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.member.GuildMember;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.channels.settings.WidgetTextChannelSettings;
import com.discord.widgets.channels.settings.WidgetThreadSettings;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.StartOfChatEntry;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemStart.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b#\u0010$J;\u0010\f\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\f\u0010\rJO\u0010\u0014\u001a\u00020\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0011\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0019\u0010\u0017\u001a\u00020\u00122\b\u0010\u0016\u001a\u0004\u0018\u00010\u000fH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u001f\u0010\u001c\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u001b\u001a\u00020\u001aH\u0014¢\u0006\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006%"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStart;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "channelName", "", "isTextInVoice", "canReadMessageHistory", "canManageChannel", "", "configureChannel", "(JLjava/lang/String;ZZZ)V", "canManageThread", "Lcom/discord/models/member/GuildMember;", "threadCreatorMember", "threadCreatorName", "", "threadAutoArchiveDuration", "configureThread", "(JLjava/lang/String;ZZLcom/discord/models/member/GuildMember;Ljava/lang/String;Ljava/lang/Integer;)V", "member", "getAuthorTextColor", "(Lcom/discord/models/member/GuildMember;)I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/databinding/WidgetChatListAdapterItemStartBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemStartBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemStart extends WidgetChatListItem {
    private final WidgetChatListAdapterItemStartBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemStart(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_start, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_edit_channel;
        TextView textView = (TextView) view.findViewById(R.id.chat_list_adapter_item_edit_channel);
        if (textView != null) {
            i = R.id.chat_list_adapter_item_header;
            TextView textView2 = (TextView) view.findViewById(R.id.chat_list_adapter_item_header);
            if (textView2 != null) {
                i = R.id.chat_list_adapter_item_subheader1;
                TextView textView3 = (TextView) view.findViewById(R.id.chat_list_adapter_item_subheader1);
                if (textView3 != null) {
                    i = R.id.chat_list_adapter_item_subheader2;
                    TextView textView4 = (TextView) view.findViewById(R.id.chat_list_adapter_item_subheader2);
                    if (textView4 != null) {
                        i = R.id.chat_list_adapter_thread_header_icon;
                        ImageView imageView = (ImageView) view.findViewById(R.id.chat_list_adapter_thread_header_icon);
                        if (imageView != null) {
                            WidgetChatListAdapterItemStartBinding widgetChatListAdapterItemStartBinding = new WidgetChatListAdapterItemStartBinding((LinearLayout) view, textView, textView2, textView3, textView4, imageView);
                            m.checkNotNullExpressionValue(widgetChatListAdapterItemStartBinding, "WidgetChatListAdapterIte…artBinding.bind(itemView)");
                            this.binding = widgetChatListAdapterItemStartBinding;
                            return;
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemStart widgetChatListAdapterItemStart) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemStart.adapter;
    }

    private final void configureChannel(final long j, String str, boolean z2, boolean z3, boolean z4) {
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.chatListAdapterItemHeader");
        int i = 0;
        b.m(textView, R.string.android_welcome_message_title_channel, new Object[]{str}, (r4 & 4) != 0 ? b.g.j : null);
        TextView textView2 = this.binding.d;
        m.checkNotNullExpressionValue(textView2, "binding.chatListAdapterItemSubheader1");
        textView2.setVisibility(8);
        if (z2) {
            ImageView imageView = this.binding.f;
            m.checkNotNullExpressionValue(imageView, "binding.chatListAdapterThreadHeaderIcon");
            imageView.setVisibility(0);
            this.binding.f.setImageResource(R.drawable.ic_chat_message_white_24dp);
        } else {
            ImageView imageView2 = this.binding.f;
            m.checkNotNullExpressionValue(imageView2, "binding.chatListAdapterThreadHeaderIcon");
            imageView2.setVisibility(8);
        }
        if (z3) {
            TextView textView3 = this.binding.e;
            m.checkNotNullExpressionValue(textView3, "binding.chatListAdapterItemSubheader2");
            b.m(textView3, R.string.android_welcome_message_subtitle_channel, new Object[]{str}, (r4 & 4) != 0 ? b.g.j : null);
        } else {
            TextView textView4 = this.binding.e;
            m.checkNotNullExpressionValue(textView4, "binding.chatListAdapterItemSubheader2");
            b.m(textView4, R.string.beginning_channel_no_history, new Object[]{str}, (r4 & 4) != 0 ? b.g.j : null);
        }
        this.binding.f2314b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStart$configureChannel$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetTextChannelSettings.Companion.launch(j, WidgetChatListAdapterItemStart.access$getAdapter$p(WidgetChatListAdapterItemStart.this).getContext());
            }
        });
        TextView textView5 = this.binding.f2314b;
        m.checkNotNullExpressionValue(textView5, "binding.chatListAdapterItemEditChannel");
        b.m(textView5, R.string.edit_channel, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        TextView textView6 = this.binding.f2314b;
        m.checkNotNullExpressionValue(textView6, "binding.chatListAdapterItemEditChannel");
        if (!z4) {
            i = 8;
        }
        textView6.setVisibility(i);
    }

    private final void configureThread(final long j, String str, boolean z2, boolean z3, GuildMember guildMember, String str2, Integer num) {
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.chatListAdapterItemHeader");
        Context context = textView.getContext();
        TextView textView2 = this.binding.c;
        m.checkNotNullExpressionValue(textView2, "binding.chatListAdapterItemHeader");
        textView2.setText(str);
        TextView textView3 = this.binding.d;
        m.checkNotNullExpressionValue(textView3, "binding.chatListAdapterItemSubheader1");
        int i = 0;
        textView3.setVisibility(0);
        ImageView imageView = this.binding.f;
        m.checkNotNullExpressionValue(imageView, "binding.chatListAdapterThreadHeaderIcon");
        imageView.setVisibility(0);
        this.binding.f.setImageResource(R.drawable.ic_thread);
        TextView textView4 = this.binding.d;
        m.checkNotNullExpressionValue(textView4, "binding.chatListAdapterItemSubheader1");
        b.m(textView4, R.string.thread_started_by, new Object[]{str2}, new WidgetChatListAdapterItemStart$configureThread$1(this, str2, context, guildMember));
        if (z2) {
            ThreadUtils threadUtils = ThreadUtils.INSTANCE;
            m.checkNotNullExpressionValue(context, "context");
            String autoArchiveDurationName = threadUtils.autoArchiveDurationName(context, num != null ? num.intValue() : 0);
            TextView textView5 = this.binding.e;
            m.checkNotNullExpressionValue(textView5, "binding.chatListAdapterItemSubheader2");
            b.m(textView5, R.string.beginning_thread_archive_description, new Object[]{autoArchiveDurationName}, new WidgetChatListAdapterItemStart$configureThread$2(autoArchiveDurationName, context));
        } else {
            TextView textView6 = this.binding.e;
            m.checkNotNullExpressionValue(textView6, "binding.chatListAdapterItemSubheader2");
            b.m(textView6, R.string.beginning_channel_no_history, new Object[]{str}, (r4 & 4) != 0 ? b.g.j : null);
        }
        this.binding.f2314b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStart$configureThread$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetThreadSettings.Companion.launch(j, WidgetChatListAdapterItemStart.access$getAdapter$p(WidgetChatListAdapterItemStart.this).getContext());
            }
        });
        TextView textView7 = this.binding.f2314b;
        m.checkNotNullExpressionValue(textView7, "binding.chatListAdapterItemEditChannel");
        b.m(textView7, R.string.edit_thread, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        TextView textView8 = this.binding.f2314b;
        m.checkNotNullExpressionValue(textView8, "binding.chatListAdapterItemEditChannel");
        if (!z3) {
            i = 8;
        }
        textView8.setVisibility(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final int getAuthorTextColor(GuildMember guildMember) {
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        return GuildMember.Companion.getColor(guildMember, ColorCompat.getThemedColor(view.getContext(), (int) R.attr.colorHeaderPrimary));
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        StartOfChatEntry startOfChatEntry = (StartOfChatEntry) chatListEntry;
        long component1 = startOfChatEntry.component1();
        String component2 = startOfChatEntry.component2();
        boolean component3 = startOfChatEntry.component3();
        boolean component4 = startOfChatEntry.component4();
        boolean component5 = startOfChatEntry.component5();
        if (startOfChatEntry.isThread()) {
            configureThread(component1, component2, component3, component5, startOfChatEntry.getThreadCreatorMember(), startOfChatEntry.getThreadCreatorName(), startOfChatEntry.getThreadAutoArchiveDuration());
        } else {
            configureChannel(component1, component2, startOfChatEntry.isTextInVoice(), component3, component4);
        }
    }
}
