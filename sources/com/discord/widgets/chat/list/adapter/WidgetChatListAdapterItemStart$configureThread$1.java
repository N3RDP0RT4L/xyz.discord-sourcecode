package com.discord.widgets.chat.list.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.style.ForegroundColorSpan;
import com.discord.i18n.Hook;
import com.discord.i18n.RenderContext;
import com.discord.models.member.GuildMember;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.spans.TypefaceSpanCompat;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemStart.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemStart$configureThread$1 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ Context $context;
    public final /* synthetic */ GuildMember $threadCreatorMember;
    public final /* synthetic */ String $threadCreatorName;
    public final /* synthetic */ WidgetChatListAdapterItemStart this$0;

    /* compiled from: WidgetChatListAdapterItemStart.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/Hook;", "", "invoke", "(Lcom/discord/i18n/Hook;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStart$configureThread$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Hook, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Hook hook) {
            invoke2(hook);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Hook hook) {
            int authorTextColor;
            m.checkNotNullParameter(hook, "$receiver");
            WidgetChatListAdapterItemStart$configureThread$1 widgetChatListAdapterItemStart$configureThread$1 = WidgetChatListAdapterItemStart$configureThread$1.this;
            hook.f2681b = widgetChatListAdapterItemStart$configureThread$1.$threadCreatorName;
            FontUtils fontUtils = FontUtils.INSTANCE;
            Context context = widgetChatListAdapterItemStart$configureThread$1.$context;
            m.checkNotNullExpressionValue(context, "context");
            Typeface themedFont = fontUtils.getThemedFont(context, R.attr.font_primary_semibold);
            if (themedFont != null) {
                hook.a.add(new TypefaceSpanCompat(themedFont));
            }
            List<Object> list = hook.a;
            WidgetChatListAdapterItemStart$configureThread$1 widgetChatListAdapterItemStart$configureThread$12 = WidgetChatListAdapterItemStart$configureThread$1.this;
            authorTextColor = widgetChatListAdapterItemStart$configureThread$12.this$0.getAuthorTextColor(widgetChatListAdapterItemStart$configureThread$12.$threadCreatorMember);
            list.add(new ForegroundColorSpan(authorTextColor));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemStart$configureThread$1(WidgetChatListAdapterItemStart widgetChatListAdapterItemStart, String str, Context context, GuildMember guildMember) {
        super(1);
        this.this$0 = widgetChatListAdapterItemStart;
        this.$threadCreatorName = str;
        this.$context = context;
        this.$threadCreatorMember = guildMember;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        m.checkNotNullParameter(renderContext, "$receiver");
        renderContext.a("usernameHook", new AnonymousClass1());
    }
}
