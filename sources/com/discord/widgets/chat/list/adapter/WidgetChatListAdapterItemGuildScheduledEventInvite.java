package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.ViewKt;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.databinding.WidgetChatListAdapterItemGuildScheduledEventInviteBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.models.guild.UserGuildMember;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.GuildScheduledEventInviteEntry;
import com.discord.widgets.guildscheduledevent.GuildScheduledEventItemView;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventDetailsBottomSheet;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.Job;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemGuildScheduledEventInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u001fB\u0019\u0012\u0006\u0010\u001c\u001a\u00020\u001b\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0014¢\u0006\u0004\b\u000b\u0010\fJ\u0011\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0014¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0018\u0010\u0013\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0018\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001a¨\u0006 "}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemInviteBase;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite$Model;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "Lkotlinx/coroutines/Job;", "inviteJoinJob", "Lkotlinx/coroutines/Job;", "Lcom/discord/databinding/WidgetChatListAdapterItemGuildScheduledEventInviteBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemGuildScheduledEventInviteBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;Lcom/discord/stores/StoreGuildScheduledEvents;)V", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemGuildScheduledEventInvite extends WidgetChatListAdapterItemInviteBase {
    private final WidgetChatListAdapterItemGuildScheduledEventInviteBinding binding;
    private final StoreGuildScheduledEvents guildScheduledEventStore;
    private Job inviteJoinJob;
    private Subscription subscription;

    /* compiled from: WidgetChatListAdapterItemGuildScheduledEventInvite.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0018\b\u0082\b\u0018\u0000 A2\u00020\u0001:\u0001ABe\u0012\u0006\u0010\u001b\u001a\u00020\u0002\u0012\u0006\u0010\u001c\u001a\u00020\u0005\u0012\u0006\u0010\u001d\u001a\u00020\u0005\u0012\u0006\u0010\u001e\u001a\u00020\t\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\f\u0012\b\u0010 \u001a\u0004\u0018\u00010\u000f\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0012\u0012\u000e\u0010\"\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u0016\u0012\u0006\u0010#\u001a\u00020\u0005\u0012\u0006\u0010$\u001a\u00020\u0005¢\u0006\u0004\b?\u0010@J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0018\u0010\u0017\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0007J\u0010\u0010\u001a\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0007J\u0082\u0001\u0010%\u001a\u00020\u00002\b\b\u0002\u0010\u001b\u001a\u00020\u00022\b\b\u0002\u0010\u001c\u001a\u00020\u00052\b\b\u0002\u0010\u001d\u001a\u00020\u00052\b\b\u0002\u0010\u001e\u001a\u00020\t2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00122\u0010\b\u0002\u0010\"\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u00162\b\b\u0002\u0010#\u001a\u00020\u00052\b\b\u0002\u0010$\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010(\u001a\u00020'HÖ\u0001¢\u0006\u0004\b(\u0010)J\u0010\u0010+\u001a\u00020*HÖ\u0001¢\u0006\u0004\b+\u0010,J\u001a\u0010.\u001a\u00020\u00052\b\u0010-\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b.\u0010/R\u001b\u0010!\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b!\u00100\u001a\u0004\b1\u0010\u0014R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00102\u001a\u0004\b3\u0010\u0004R\u0019\u0010\u001d\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00104\u001a\u0004\b\u001d\u0010\u0007R\u0019\u0010\u001e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00105\u001a\u0004\b6\u0010\u000bR\u0019\u0010$\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b$\u00104\u001a\u0004\b7\u0010\u0007R!\u0010\"\u001a\n\u0018\u00010\u0015j\u0004\u0018\u0001`\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00108\u001a\u0004\b9\u0010\u0018R\u0019\u0010\u001c\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00104\u001a\u0004\b\u001c\u0010\u0007R\u0019\u0010#\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\b:\u0010\u0007R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010;\u001a\u0004\b<\u0010\u000eR\u001b\u0010 \u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010=\u001a\u0004\b>\u0010\u0011¨\u0006B"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite$Model;", "", "Lcom/discord/models/domain/ModelInvite;", "component1", "()Lcom/discord/models/domain/ModelInvite;", "", "component2", "()Z", "component3", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component4", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lcom/discord/api/channel/Channel;", "component5", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component6", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/guild/UserGuildMember;", "component7", "()Lcom/discord/models/guild/UserGuildMember;", "", "Lcom/discord/primitives/ChannelId;", "component8", "()Ljava/lang/Long;", "component9", "component10", "invite", "isInGuild", "isRsvped", "guildScheduledEvent", "channel", "guild", "creator", "selectedVoiceChannelId", "canConnect", "canShare", "copy", "(Lcom/discord/models/domain/ModelInvite;ZZLcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/UserGuildMember;Ljava/lang/Long;ZZ)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/UserGuildMember;", "getCreator", "Lcom/discord/models/domain/ModelInvite;", "getInvite", "Z", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEvent", "getCanShare", "Ljava/lang/Long;", "getSelectedVoiceChannelId", "getCanConnect", "Lcom/discord/api/channel/Channel;", "getChannel", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelInvite;ZZLcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/UserGuildMember;Ljava/lang/Long;ZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canConnect;
        private final boolean canShare;
        private final Channel channel;
        private final UserGuildMember creator;
        private final Guild guild;
        private final GuildScheduledEvent guildScheduledEvent;
        private final ModelInvite invite;
        private final boolean isInGuild;
        private final boolean isRsvped;
        private final Long selectedVoiceChannelId;

        /* compiled from: WidgetChatListAdapterItemGuildScheduledEventInvite.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015JW\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\n2\b\b\u0002\u0010\r\u001a\u00020\f2\b\b\u0002\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite$Model$Companion;", "", "Lcom/discord/widgets/chat/list/entries/GuildScheduledEventInviteEntry;", "item", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreGuildScheduledEvents;", "guildScheduledEventStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "voiceChannelSelectedStore", "Lcom/discord/stores/StorePermissions;", "permissionStore", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemGuildScheduledEventInvite$Model;", "observe", "(Lcom/discord/widgets/chat/list/entries/GuildScheduledEventInviteEntry;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuildScheduledEvents;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StorePermissions;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public static /* synthetic */ Observable observe$default(Companion companion, GuildScheduledEventInviteEntry guildScheduledEventInviteEntry, StoreGuilds storeGuilds, StoreUser storeUser, StoreChannels storeChannels, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreVoiceChannelSelected storeVoiceChannelSelected, StorePermissions storePermissions, int i, Object obj) {
                return companion.observe(guildScheduledEventInviteEntry, (i & 2) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 4) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 8) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 16) != 0 ? StoreStream.Companion.getGuildScheduledEvents() : storeGuildScheduledEvents, (i & 32) != 0 ? StoreStream.Companion.getVoiceChannelSelected() : storeVoiceChannelSelected, (i & 64) != 0 ? StoreStream.Companion.getPermissions() : storePermissions);
            }

            public final Observable<Model> observe(GuildScheduledEventInviteEntry guildScheduledEventInviteEntry, StoreGuilds storeGuilds, StoreUser storeUser, StoreChannels storeChannels, StoreGuildScheduledEvents storeGuildScheduledEvents, StoreVoiceChannelSelected storeVoiceChannelSelected, StorePermissions storePermissions) {
                m.checkNotNullParameter(guildScheduledEventInviteEntry, "item");
                m.checkNotNullParameter(storeGuilds, "guildStore");
                m.checkNotNullParameter(storeUser, "userStore");
                m.checkNotNullParameter(storeChannels, "channelStore");
                m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventStore");
                m.checkNotNullParameter(storeVoiceChannelSelected, "voiceChannelSelectedStore");
                m.checkNotNullParameter(storePermissions, "permissionStore");
                return ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{storeGuilds, storeUser, storeGuildScheduledEvents, storeVoiceChannelSelected, storePermissions}, false, null, null, new WidgetChatListAdapterItemGuildScheduledEventInvite$Model$Companion$observe$1(guildScheduledEventInviteEntry, storeGuilds, storeUser, storeGuildScheduledEvents, storeChannels, storeVoiceChannelSelected, storePermissions), 14, null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(ModelInvite modelInvite, boolean z2, boolean z3, GuildScheduledEvent guildScheduledEvent, Channel channel, Guild guild, UserGuildMember userGuildMember, Long l, boolean z4, boolean z5) {
            m.checkNotNullParameter(modelInvite, "invite");
            m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
            this.invite = modelInvite;
            this.isInGuild = z2;
            this.isRsvped = z3;
            this.guildScheduledEvent = guildScheduledEvent;
            this.channel = channel;
            this.guild = guild;
            this.creator = userGuildMember;
            this.selectedVoiceChannelId = l;
            this.canConnect = z4;
            this.canShare = z5;
        }

        public final ModelInvite component1() {
            return this.invite;
        }

        public final boolean component10() {
            return this.canShare;
        }

        public final boolean component2() {
            return this.isInGuild;
        }

        public final boolean component3() {
            return this.isRsvped;
        }

        public final GuildScheduledEvent component4() {
            return this.guildScheduledEvent;
        }

        public final Channel component5() {
            return this.channel;
        }

        public final Guild component6() {
            return this.guild;
        }

        public final UserGuildMember component7() {
            return this.creator;
        }

        public final Long component8() {
            return this.selectedVoiceChannelId;
        }

        public final boolean component9() {
            return this.canConnect;
        }

        public final Model copy(ModelInvite modelInvite, boolean z2, boolean z3, GuildScheduledEvent guildScheduledEvent, Channel channel, Guild guild, UserGuildMember userGuildMember, Long l, boolean z4, boolean z5) {
            m.checkNotNullParameter(modelInvite, "invite");
            m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
            return new Model(modelInvite, z2, z3, guildScheduledEvent, channel, guild, userGuildMember, l, z4, z5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.invite, model.invite) && this.isInGuild == model.isInGuild && this.isRsvped == model.isRsvped && m.areEqual(this.guildScheduledEvent, model.guildScheduledEvent) && m.areEqual(this.channel, model.channel) && m.areEqual(this.guild, model.guild) && m.areEqual(this.creator, model.creator) && m.areEqual(this.selectedVoiceChannelId, model.selectedVoiceChannelId) && this.canConnect == model.canConnect && this.canShare == model.canShare;
        }

        public final boolean getCanConnect() {
            return this.canConnect;
        }

        public final boolean getCanShare() {
            return this.canShare;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final UserGuildMember getCreator() {
            return this.creator;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final GuildScheduledEvent getGuildScheduledEvent() {
            return this.guildScheduledEvent;
        }

        public final ModelInvite getInvite() {
            return this.invite;
        }

        public final Long getSelectedVoiceChannelId() {
            return this.selectedVoiceChannelId;
        }

        public int hashCode() {
            ModelInvite modelInvite = this.invite;
            int i = 0;
            int hashCode = (modelInvite != null ? modelInvite.hashCode() : 0) * 31;
            boolean z2 = this.isInGuild;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            boolean z3 = this.isRsvped;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
            int hashCode2 = (i8 + (guildScheduledEvent != null ? guildScheduledEvent.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int hashCode3 = (hashCode2 + (channel != null ? channel.hashCode() : 0)) * 31;
            Guild guild = this.guild;
            int hashCode4 = (hashCode3 + (guild != null ? guild.hashCode() : 0)) * 31;
            UserGuildMember userGuildMember = this.creator;
            int hashCode5 = (hashCode4 + (userGuildMember != null ? userGuildMember.hashCode() : 0)) * 31;
            Long l = this.selectedVoiceChannelId;
            if (l != null) {
                i = l.hashCode();
            }
            int i9 = (hashCode5 + i) * 31;
            boolean z4 = this.canConnect;
            if (z4) {
                z4 = true;
            }
            int i10 = z4 ? 1 : 0;
            int i11 = z4 ? 1 : 0;
            int i12 = (i9 + i10) * 31;
            boolean z5 = this.canShare;
            if (!z5) {
                i2 = z5 ? 1 : 0;
            }
            return i12 + i2;
        }

        public final boolean isInGuild() {
            return this.isInGuild;
        }

        public final boolean isRsvped() {
            return this.isRsvped;
        }

        public String toString() {
            StringBuilder R = a.R("Model(invite=");
            R.append(this.invite);
            R.append(", isInGuild=");
            R.append(this.isInGuild);
            R.append(", isRsvped=");
            R.append(this.isRsvped);
            R.append(", guildScheduledEvent=");
            R.append(this.guildScheduledEvent);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", creator=");
            R.append(this.creator);
            R.append(", selectedVoiceChannelId=");
            R.append(this.selectedVoiceChannelId);
            R.append(", canConnect=");
            R.append(this.canConnect);
            R.append(", canShare=");
            return a.M(R, this.canShare, ")");
        }
    }

    public /* synthetic */ WidgetChatListAdapterItemGuildScheduledEventInvite(WidgetChatListAdapter widgetChatListAdapter, StoreGuildScheduledEvents storeGuildScheduledEvents, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(widgetChatListAdapter, (i & 2) != 0 ? StoreStream.Companion.getGuildScheduledEvents() : storeGuildScheduledEvents);
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemGuildScheduledEventInvite widgetChatListAdapterItemGuildScheduledEventInvite) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemGuildScheduledEventInvite.adapter;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        GuildScheduledEventItemView guildScheduledEventItemView = this.binding.f2303b;
        m.checkNotNullExpressionValue(guildScheduledEventItemView, "binding.guildScheduledEventInviteContainer");
        guildScheduledEventItemView.setBackground(null);
        GuildScheduledEventItemView guildScheduledEventItemView2 = this.binding.f2303b;
        GuildScheduledEvent guildScheduledEvent = model.getGuildScheduledEvent();
        Channel channel = model.getChannel();
        Guild guild = model.getGuild();
        UserGuildMember creator = model.getCreator();
        boolean isInGuild = model.isInGuild();
        boolean isRsvped = model.isRsvped();
        Long b2 = model.getGuildScheduledEvent().b();
        boolean z2 = false;
        if (b2 != null) {
            long longValue = b2.longValue();
            Long selectedVoiceChannelId = model.getSelectedVoiceChannelId();
            if (selectedVoiceChannelId != null && longValue == selectedVoiceChannelId.longValue()) {
                z2 = true;
            }
        }
        guildScheduledEventItemView2.configureInChatList(guildScheduledEvent, channel, guild, creator, isInGuild, isRsvped, z2, model.getCanConnect(), new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGuildScheduledEventInvite$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                if (model.isInGuild()) {
                    WidgetGuildScheduledEventDetailsBottomSheet.Companion.showForGuild(WidgetChatListAdapterItemGuildScheduledEventInvite.access$getAdapter$p(WidgetChatListAdapterItemGuildScheduledEventInvite.this).getFragmentManager(), model.getGuildScheduledEvent().i());
                } else {
                    WidgetChatListAdapterItemInviteBase.joinServerOrDM$default(WidgetChatListAdapterItemGuildScheduledEventInvite.this, WidgetChatListAdapterItemGuildScheduledEventInvite.class, model.getInvite(), null, null, 12, null);
                }
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGuildScheduledEventInvite$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoreGuildScheduledEvents storeGuildScheduledEvents;
                storeGuildScheduledEvents = WidgetChatListAdapterItemGuildScheduledEventInvite.this.guildScheduledEventStore;
                storeGuildScheduledEvents.toggleMeRsvpForEvent(model.getGuildScheduledEvent());
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGuildScheduledEventInvite$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Job job;
                job = WidgetChatListAdapterItemGuildScheduledEventInvite.this.inviteJoinJob;
                if (job == null || !job.a()) {
                    WidgetChatListAdapterItemGuildScheduledEventInvite widgetChatListAdapterItemGuildScheduledEventInvite = WidgetChatListAdapterItemGuildScheduledEventInvite.this;
                    widgetChatListAdapterItemGuildScheduledEventInvite.inviteJoinJob = WidgetChatListAdapterItemInviteBase.joinServerOrDM$default(widgetChatListAdapterItemGuildScheduledEventInvite, widgetChatListAdapterItemGuildScheduledEventInvite.getClass(), model.getInvite(), null, null, 12, null);
                }
            }
        }, new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemGuildScheduledEventInvite$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChatListAdapterItemGuildScheduledEventInvite.access$getAdapter$p(WidgetChatListAdapterItemGuildScheduledEventInvite.this).onShareButtonClick(model.getGuildScheduledEvent(), new WeakReference<>(WidgetChatListAdapterItemGuildScheduledEventInvite.access$getAdapter$p(WidgetChatListAdapterItemGuildScheduledEventInvite.this).getContext()), new WeakReference<>(ViewKt.findFragment(WidgetChatListAdapterItemGuildScheduledEventInvite.access$getAdapter$p(WidgetChatListAdapterItemGuildScheduledEventInvite.this).getRecycler())));
            }
        });
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.subscription;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemGuildScheduledEventInvite(WidgetChatListAdapter widgetChatListAdapter, StoreGuildScheduledEvents storeGuildScheduledEvents) {
        super(R.layout.widget_chat_list_adapter_item_guild_scheduled_event_invite, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        m.checkNotNullParameter(storeGuildScheduledEvents, "guildScheduledEventStore");
        this.guildScheduledEventStore = storeGuildScheduledEvents;
        View view = this.itemView;
        Objects.requireNonNull(view, "rootView");
        GuildScheduledEventItemView guildScheduledEventItemView = (GuildScheduledEventItemView) view;
        WidgetChatListAdapterItemGuildScheduledEventInviteBinding widgetChatListAdapterItemGuildScheduledEventInviteBinding = new WidgetChatListAdapterItemGuildScheduledEventInviteBinding(guildScheduledEventItemView, guildScheduledEventItemView);
        m.checkNotNullExpressionValue(widgetChatListAdapterItemGuildScheduledEventInviteBinding, "WidgetChatListAdapterIte…iteBinding.bind(itemView)");
        this.binding = widgetChatListAdapterItemGuildScheduledEventInviteBinding;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(Model.Companion.observe$default(Model.Companion, (GuildScheduledEventInviteEntry) chatListEntry, null, null, null, null, null, null, 126, null)), WidgetChatListAdapterItemGuildScheduledEventInvite.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetChatListAdapterItemGuildScheduledEventInvite$onConfigure$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterItemGuildScheduledEventInvite$onConfigure$1(this));
    }
}
