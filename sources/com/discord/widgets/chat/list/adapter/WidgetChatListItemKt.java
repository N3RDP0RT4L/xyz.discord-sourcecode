package com.discord.widgets.chat.list.adapter;

import com.discord.api.user.User;
import com.discord.models.message.Message;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: WidgetChatListItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u001a\u0019\u0010\u0004\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "Lcom/discord/models/message/Message;", "message", "", "isUserMentioned", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;Lcom/discord/models/message/Message;)Z", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListItemKt {
    public static final boolean isUserMentioned(WidgetChatListAdapter widgetChatListAdapter, Message message) {
        boolean z2;
        m.checkNotNullParameter(widgetChatListAdapter, "$this$isUserMentioned");
        m.checkNotNullParameter(message, "message");
        if (widgetChatListAdapter.getMentionMeMessageLevelHighlighting()) {
            if (m.areEqual(message.getMentionEveryone(), Boolean.TRUE)) {
                return true;
            }
            List<User> mentions = message.getMentions();
            if (mentions != null) {
                for (User user : mentions) {
                    if (user.i() == widgetChatListAdapter.getData().getUserId()) {
                        return true;
                    }
                }
            }
            List<Long> mentionRoles = message.getMentionRoles();
            if (mentionRoles != null) {
                if (!mentionRoles.isEmpty()) {
                    for (Number number : mentionRoles) {
                        if (widgetChatListAdapter.getData().getMyRoleIds().contains(Long.valueOf(number.longValue()))) {
                            z2 = true;
                            break;
                        }
                    }
                }
                z2 = false;
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }
}
