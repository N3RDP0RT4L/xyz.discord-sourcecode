package com.discord.widgets.chat.list.adapter;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.stageinstance.RecommendedStageInstance;
import com.discord.api.stageinstance.StageInstance;
import com.discord.databinding.WidgetChatListAdapterItemStageInviteBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.models.member.GuildMember;
import com.discord.models.user.CoreUser;
import com.discord.models.user.User;
import com.discord.stores.StoreAccessibility;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreInstantInvites;
import com.discord.stores.StoreRequestedStageChannels;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreStageInstances;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.views.stages.StageCardSpeakersView;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.StageInviteEntry;
import com.discord.widgets.stage.StageCardSpeaker;
import com.discord.widgets.stage.StageRoles;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.Job;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemStageInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001$B\u000f\u0012\u0006\u0010!\u001a\u00020 ¢\u0006\u0004\b\"\u0010#J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000f\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0014¢\u0006\u0004\b\u000f\u0010\u0010J\u0011\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0014¢\u0006\u0004\b\u0012\u0010\u0013R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\b\u0010\u0014R\u0018\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0018\u0010\u001e\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001f¨\u0006%"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemInviteBase;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite$Model;)V", "", "iconUrl", "updateIconUrlIfChanged", "(Ljava/lang/String;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "Ljava/lang/String;", "Lkotlinx/coroutines/Job;", "inviteJoinJob", "Lkotlinx/coroutines/Job;", "Lcom/discord/widgets/chat/list/entries/StageInviteEntry;", "item", "Lcom/discord/widgets/chat/list/entries/StageInviteEntry;", "Lcom/discord/databinding/WidgetChatListAdapterItemStageInviteBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemStageInviteBinding;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemStageInvite extends WidgetChatListAdapterItemInviteBase {
    private final WidgetChatListAdapterItemStageInviteBinding binding;
    private String iconUrl;
    private Job inviteJoinJob;
    private StageInviteEntry item;
    private Subscription subscription;

    /* compiled from: WidgetChatListAdapterItemStageInvite.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u001b\b\u0082\b\u0018\u0000 @2\u00020\u0001:\u0001@BY\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\n\u0010\u001d\u001a\u00060\u0005j\u0002`\u0006\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\t\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010 \u001a\u00020\u000f\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0012\u0012\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015\u0012\b\u0010#\u001a\u0004\u0018\u00010\u0019¢\u0006\u0004\b>\u0010?J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJr\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u001c\u001a\u00020\u00022\f\b\u0002\u0010\u001d\u001a\u00060\u0005j\u0002`\u00062\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010 \u001a\u00020\u000f2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00122\u000e\b\u0002\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u0019HÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010'\u001a\u00020&HÖ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010)\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b)\u0010*J\u001a\u0010,\u001a\u00020\u000f2\b\u0010+\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b,\u0010-R\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010.\u001a\u0004\b/\u0010\u0004R\u001b\u0010#\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b#\u00100\u001a\u0004\b1\u0010\u001bR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00102\u001a\u0004\b3\u0010\u000bR\u001b\u0010!\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b!\u00104\u001a\u0004\b5\u0010\u0014R\u001d\u0010\u001d\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00106\u001a\u0004\b7\u0010\bR\u001b\u0010\u001f\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00108\u001a\u0004\b9\u0010\u000eR\u0019\u0010 \u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010:\u001a\u0004\b;\u0010\u0011R\u001f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00160\u00158\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010<\u001a\u0004\b=\u0010\u0018¨\u0006A"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite$Model;", "", "Lcom/discord/models/domain/ModelInvite;", "component1", "()Lcom/discord/models/domain/ModelInvite;", "", "Lcom/discord/primitives/UserId;", "component2", "()J", "Lcom/discord/models/user/User;", "component3", "()Lcom/discord/models/user/User;", "Lcom/discord/api/channel/Channel;", "component4", "()Lcom/discord/api/channel/Channel;", "", "component5", "()Z", "Lcom/discord/api/stageinstance/StageInstance;", "component6", "()Lcom/discord/api/stageinstance/StageInstance;", "", "Lcom/discord/widgets/stage/StageCardSpeaker;", "component7", "()Ljava/util/List;", "", "component8", "()Ljava/lang/Integer;", "invite", "meId", "authorUser", "channel", "shouldAnimateGuildIcon", "stageInstance", "speakers", "listenersCount", "copy", "(Lcom/discord/models/domain/ModelInvite;JLcom/discord/models/user/User;Lcom/discord/api/channel/Channel;ZLcom/discord/api/stageinstance/StageInstance;Ljava/util/List;Ljava/lang/Integer;)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/domain/ModelInvite;", "getInvite", "Ljava/lang/Integer;", "getListenersCount", "Lcom/discord/models/user/User;", "getAuthorUser", "Lcom/discord/api/stageinstance/StageInstance;", "getStageInstance", "J", "getMeId", "Lcom/discord/api/channel/Channel;", "getChannel", "Z", "getShouldAnimateGuildIcon", "Ljava/util/List;", "getSpeakers", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelInvite;JLcom/discord/models/user/User;Lcom/discord/api/channel/Channel;ZLcom/discord/api/stageinstance/StageInstance;Ljava/util/List;Ljava/lang/Integer;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final User authorUser;
        private final Channel channel;
        private final ModelInvite invite;
        private final Integer listenersCount;
        private final long meId;
        private final boolean shouldAnimateGuildIcon;
        private final List<StageCardSpeaker> speakers;
        private final StageInstance stageInstance;

        /* compiled from: WidgetChatListAdapterItemStageInvite.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b*\u0010+JG\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\n\u0010\n\u001a\u00060\bj\u0002`\t2\n\u0010\f\u001a\u00060\bj\u0002`\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J!\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u001d\u0010\u0018\u001a\u00020\u000e*\u00020\u00152\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016H\u0002¢\u0006\u0004\b\u0018\u0010\u0019Jm\u0010(\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010'0&2\u0006\u0010\u001b\u001a\u00020\u001a2\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u001d\u001a\u00020\u001c2\b\b\u0002\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u001f\u001a\u00020\u001e2\b\b\u0002\u0010!\u001a\u00020 2\b\b\u0002\u0010#\u001a\u00020\"2\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010%\u001a\u00020$¢\u0006\u0004\b(\u0010)¨\u0006,"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite$Model$Companion;", "", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lcom/discord/stores/StoreUser;", "userStore", "Lcom/discord/stores/StoreStageChannels;", "stageChannelStore", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "", "Lcom/discord/widgets/stage/StageCardSpeaker;", "speakersFromLocalStore", "(Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreStageChannels;JJ)Ljava/util/List;", "Lcom/discord/stores/StoreRequestedStageChannels$StageInstanceState;", "stageInstanceState", "speakersFromRequest", "(Lcom/discord/stores/StoreRequestedStageChannels$StageInstanceState;)Ljava/util/List;", "Lcom/discord/models/user/User;", "Lcom/discord/models/member/GuildMember;", "member", "toStageCardSpeaker", "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)Lcom/discord/widgets/stage/StageCardSpeaker;", "Lcom/discord/widgets/chat/list/entries/StageInviteEntry;", "item", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreAccessibility;", "accessibilityStore", "Lcom/discord/stores/StoreStageInstances;", "stageInstanceStore", "Lcom/discord/stores/StoreRequestedStageChannels;", "requestedInstanceStore", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemStageInvite$Model;", "observe", "(Lcom/discord/widgets/chat/list/entries/StageInviteEntry;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreAccessibility;Lcom/discord/stores/StoreStageInstances;Lcom/discord/stores/StoreRequestedStageChannels;Lcom/discord/stores/StoreStageChannels;Lcom/discord/stores/updates/ObservationDeck;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public static /* synthetic */ Observable observe$default(Companion companion, StageInviteEntry stageInviteEntry, StoreUser storeUser, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreAccessibility storeAccessibility, StoreStageInstances storeStageInstances, StoreRequestedStageChannels storeRequestedStageChannels, StoreStageChannels storeStageChannels, ObservationDeck observationDeck, int i, Object obj) {
                return companion.observe(stageInviteEntry, (i & 2) != 0 ? StoreStream.Companion.getUsers() : storeUser, (i & 4) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 8) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 16) != 0 ? StoreStream.Companion.getAccessibility() : storeAccessibility, (i & 32) != 0 ? StoreStream.Companion.getStageInstances() : storeStageInstances, (i & 64) != 0 ? StoreStream.Companion.getRequestedStageChannels() : storeRequestedStageChannels, (i & 128) != 0 ? StoreStream.Companion.getStageChannels() : storeStageChannels, (i & 256) != 0 ? ObservationDeckProvider.get() : observationDeck);
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final List<StageCardSpeaker> speakersFromLocalStore(StoreGuilds storeGuilds, StoreUser storeUser, StoreStageChannels storeStageChannels, long j, long j2) {
                User user;
                Map<Long, StageRoles> channelRoles = storeStageChannels.getChannelRoles(j);
                if (channelRoles == null) {
                    return null;
                }
                ArrayList arrayList = new ArrayList();
                for (Map.Entry<Long, StageRoles> entry : channelRoles.entrySet()) {
                    long longValue = entry.getKey().longValue();
                    StageCardSpeaker stageCardSpeaker = (!StageRoles.m27isSpeakerimpl(entry.getValue().m29unboximpl()) || (user = storeUser.getUsers().get(Long.valueOf(longValue))) == null) ? null : Model.Companion.toStageCardSpeaker(user, storeGuilds.getMember(j2, longValue));
                    if (stageCardSpeaker != null) {
                        arrayList.add(stageCardSpeaker);
                    }
                }
                return arrayList;
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final List<StageCardSpeaker> speakersFromRequest(StoreRequestedStageChannels.StageInstanceState stageInstanceState) {
                RecommendedStageInstance stageInstance;
                GuildMember from;
                if (stageInstanceState == null || (stageInstance = stageInstanceState.getStageInstance()) == null) {
                    return null;
                }
                List<com.discord.api.guildmember.GuildMember> c = stageInstance.c();
                ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(c, 10));
                for (com.discord.api.guildmember.GuildMember guildMember : c) {
                    long f = guildMember.f();
                    CoreUser coreUser = new CoreUser(guildMember.m());
                    from = GuildMember.Companion.from(guildMember, f, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : StoreStream.Companion.getGuilds());
                    arrayList.add(new StageCardSpeaker(coreUser, from));
                }
                return arrayList;
            }

            private final StageCardSpeaker toStageCardSpeaker(User user, GuildMember guildMember) {
                return new StageCardSpeaker(user, guildMember);
            }

            public final Observable<Model> observe(StageInviteEntry stageInviteEntry, StoreUser storeUser, StoreChannels storeChannels, StoreGuilds storeGuilds, StoreAccessibility storeAccessibility, StoreStageInstances storeStageInstances, StoreRequestedStageChannels storeRequestedStageChannels, StoreStageChannels storeStageChannels, ObservationDeck observationDeck) {
                m.checkNotNullParameter(stageInviteEntry, "item");
                m.checkNotNullParameter(storeUser, "userStore");
                m.checkNotNullParameter(storeChannels, "channelStore");
                m.checkNotNullParameter(storeGuilds, "guildStore");
                m.checkNotNullParameter(storeAccessibility, "accessibilityStore");
                m.checkNotNullParameter(storeStageInstances, "stageInstanceStore");
                m.checkNotNullParameter(storeRequestedStageChannels, "requestedInstanceStore");
                m.checkNotNullParameter(storeStageChannels, "stageChannelStore");
                m.checkNotNullParameter(observationDeck, "observationDeck");
                return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeUser, storeChannels, storeGuilds, storeAccessibility, storeStageInstances, storeRequestedStageChannels, storeStageChannels}, false, null, null, new WidgetChatListAdapterItemStageInvite$Model$Companion$observe$1(storeUser, stageInviteEntry, storeChannels, storeAccessibility, storeStageInstances, storeRequestedStageChannels, storeStageChannels, storeGuilds), 14, null);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(ModelInvite modelInvite, long j, User user, Channel channel, boolean z2, StageInstance stageInstance, List<StageCardSpeaker> list, Integer num) {
            m.checkNotNullParameter(modelInvite, "invite");
            m.checkNotNullParameter(list, "speakers");
            this.invite = modelInvite;
            this.meId = j;
            this.authorUser = user;
            this.channel = channel;
            this.shouldAnimateGuildIcon = z2;
            this.stageInstance = stageInstance;
            this.speakers = list;
            this.listenersCount = num;
        }

        public final ModelInvite component1() {
            return this.invite;
        }

        public final long component2() {
            return this.meId;
        }

        public final User component3() {
            return this.authorUser;
        }

        public final Channel component4() {
            return this.channel;
        }

        public final boolean component5() {
            return this.shouldAnimateGuildIcon;
        }

        public final StageInstance component6() {
            return this.stageInstance;
        }

        public final List<StageCardSpeaker> component7() {
            return this.speakers;
        }

        public final Integer component8() {
            return this.listenersCount;
        }

        public final Model copy(ModelInvite modelInvite, long j, User user, Channel channel, boolean z2, StageInstance stageInstance, List<StageCardSpeaker> list, Integer num) {
            m.checkNotNullParameter(modelInvite, "invite");
            m.checkNotNullParameter(list, "speakers");
            return new Model(modelInvite, j, user, channel, z2, stageInstance, list, num);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.invite, model.invite) && this.meId == model.meId && m.areEqual(this.authorUser, model.authorUser) && m.areEqual(this.channel, model.channel) && this.shouldAnimateGuildIcon == model.shouldAnimateGuildIcon && m.areEqual(this.stageInstance, model.stageInstance) && m.areEqual(this.speakers, model.speakers) && m.areEqual(this.listenersCount, model.listenersCount);
        }

        public final User getAuthorUser() {
            return this.authorUser;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final ModelInvite getInvite() {
            return this.invite;
        }

        public final Integer getListenersCount() {
            return this.listenersCount;
        }

        public final long getMeId() {
            return this.meId;
        }

        public final boolean getShouldAnimateGuildIcon() {
            return this.shouldAnimateGuildIcon;
        }

        public final List<StageCardSpeaker> getSpeakers() {
            return this.speakers;
        }

        public final StageInstance getStageInstance() {
            return this.stageInstance;
        }

        public int hashCode() {
            ModelInvite modelInvite = this.invite;
            int i = 0;
            int a = (b.a(this.meId) + ((modelInvite != null ? modelInvite.hashCode() : 0) * 31)) * 31;
            User user = this.authorUser;
            int hashCode = (a + (user != null ? user.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int hashCode2 = (hashCode + (channel != null ? channel.hashCode() : 0)) * 31;
            boolean z2 = this.shouldAnimateGuildIcon;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode2 + i2) * 31;
            StageInstance stageInstance = this.stageInstance;
            int hashCode3 = (i4 + (stageInstance != null ? stageInstance.hashCode() : 0)) * 31;
            List<StageCardSpeaker> list = this.speakers;
            int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
            Integer num = this.listenersCount;
            if (num != null) {
                i = num.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(invite=");
            R.append(this.invite);
            R.append(", meId=");
            R.append(this.meId);
            R.append(", authorUser=");
            R.append(this.authorUser);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", shouldAnimateGuildIcon=");
            R.append(this.shouldAnimateGuildIcon);
            R.append(", stageInstance=");
            R.append(this.stageInstance);
            R.append(", speakers=");
            R.append(this.speakers);
            R.append(", listenersCount=");
            return a.E(R, this.listenersCount, ")");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemStageInvite(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_stage_invite, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.item_invite_channel_topic;
        TextView textView = (TextView) view.findViewById(R.id.item_invite_channel_topic);
        if (textView != null) {
            i = R.id.item_invite_guild_name;
            TextView textView2 = (TextView) view.findViewById(R.id.item_invite_guild_name);
            if (textView2 != null) {
                i = R.id.item_invite_header;
                TextView textView3 = (TextView) view.findViewById(R.id.item_invite_header);
                if (textView3 != null) {
                    i = R.id.item_invite_image;
                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.item_invite_image);
                    if (simpleDraweeView != null) {
                        i = R.id.item_invite_image_text;
                        TextView textView4 = (TextView) view.findViewById(R.id.item_invite_image_text);
                        if (textView4 != null) {
                            i = R.id.item_invite_join_button;
                            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.item_invite_join_button);
                            if (materialButton != null) {
                                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                                i = R.id.item_listeners_count;
                                TextView textView5 = (TextView) view.findViewById(R.id.item_listeners_count);
                                if (textView5 != null) {
                                    i = R.id.stage_discovery_speakers_view;
                                    StageCardSpeakersView stageCardSpeakersView = (StageCardSpeakersView) view.findViewById(R.id.stage_discovery_speakers_view);
                                    if (stageCardSpeakersView != null) {
                                        WidgetChatListAdapterItemStageInviteBinding widgetChatListAdapterItemStageInviteBinding = new WidgetChatListAdapterItemStageInviteBinding(constraintLayout, textView, textView2, textView3, simpleDraweeView, textView4, materialButton, constraintLayout, textView5, stageCardSpeakersView);
                                        m.checkNotNullExpressionValue(widgetChatListAdapterItemStageInviteBinding, "WidgetChatListAdapterIte…iteBinding.bind(itemView)");
                                        this.binding = widgetChatListAdapterItemStageInviteBinding;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemStageInvite widgetChatListAdapterItemStageInvite) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemStageInvite.adapter;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0090  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x009d  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00a2  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00b5  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00c2  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00c7  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00cb  */
    /* JADX WARN: Removed duplicated region for block: B:52:0x00e9  */
    /* JADX WARN: Removed duplicated region for block: B:56:0x0126  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0170  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void configureUI(final com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStageInvite.Model r15) {
        /*
            Method dump skipped, instructions count: 417
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStageInvite.configureUI(com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStageInvite$Model):void");
    }

    private final void updateIconUrlIfChanged(String str) {
        if (!m.areEqual(this.iconUrl, str)) {
            SimpleDraweeView simpleDraweeView = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.itemInviteImage");
            IconUtils.setIcon$default(simpleDraweeView, str, (int) R.dimen.avatar_size_large, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
            this.iconUrl = str;
        }
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.subscription;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        this.item = (StageInviteEntry) chatListEntry;
        StoreInstantInvites instantInvites = StoreStream.Companion.getInstantInvites();
        StageInviteEntry stageInviteEntry = this.item;
        if (stageInviteEntry == null) {
            m.throwUninitializedPropertyAccessException("item");
        }
        instantInvites.fetchInviteIfNotLoaded(stageInviteEntry.getInviteCode(), (r13 & 2) != 0 ? null : null, (r13 & 4) != 0 ? null : "Invite Button Embed", (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? null : null);
        Model.Companion companion = Model.Companion;
        StageInviteEntry stageInviteEntry2 = this.item;
        if (stageInviteEntry2 == null) {
            m.throwUninitializedPropertyAccessException("item");
        }
        Observable F = ObservableExtensionsKt.ui(Model.Companion.observe$default(companion, stageInviteEntry2, null, null, null, null, null, null, null, null, 510, null)).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        ObservableExtensionsKt.appSubscribe(F, WidgetChatListAdapterItemStageInvite.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetChatListAdapterItemStageInvite$onConfigure$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterItemStageInvite$onConfigure$1(this));
    }
}
