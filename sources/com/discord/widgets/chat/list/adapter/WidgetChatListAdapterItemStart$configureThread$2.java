package com.discord.widgets.chat.list.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.style.ForegroundColorSpan;
import com.discord.i18n.Hook;
import com.discord.i18n.RenderContext;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.spans.TypefaceSpanCompat;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemStart.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemStart$configureThread$2 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ String $autoArchiveString;
    public final /* synthetic */ Context $context;

    /* compiled from: WidgetChatListAdapterItemStart.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/Hook;", "", "invoke", "(Lcom/discord/i18n/Hook;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemStart$configureThread$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Hook, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Hook hook) {
            invoke2(hook);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Hook hook) {
            m.checkNotNullParameter(hook, "$receiver");
            WidgetChatListAdapterItemStart$configureThread$2 widgetChatListAdapterItemStart$configureThread$2 = WidgetChatListAdapterItemStart$configureThread$2.this;
            hook.f2681b = widgetChatListAdapterItemStart$configureThread$2.$autoArchiveString;
            FontUtils fontUtils = FontUtils.INSTANCE;
            Context context = widgetChatListAdapterItemStart$configureThread$2.$context;
            m.checkNotNullExpressionValue(context, "context");
            Typeface themedFont = fontUtils.getThemedFont(context, R.attr.font_primary_normal);
            if (themedFont != null) {
                hook.a.add(new TypefaceSpanCompat(themedFont));
            }
            hook.a.add(new ForegroundColorSpan(ColorCompat.getThemedColor(WidgetChatListAdapterItemStart$configureThread$2.this.$context, (int) R.attr.colorHeaderPrimary)));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemStart$configureThread$2(String str, Context context) {
        super(1);
        this.$autoArchiveString = str;
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        m.checkNotNullParameter(renderContext, "$receiver");
        renderContext.a("autoArchiveDurationHook", new AnonymousClass1());
    }
}
