package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import b.a.d.f;
import b.a.k.b;
import com.discord.databinding.WidgetChatListAdapterItemEphemeralMessageBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.message.Message;
import com.discord.utilities.uri.UriHandler;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.EphemeralMessageEntry;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemEphemeralMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemEphemeralMessage;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/databinding/WidgetChatListAdapterItemEphemeralMessageBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemEphemeralMessageBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemEphemeralMessage extends WidgetChatListItem {
    private final WidgetChatListAdapterItemEphemeralMessageBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemEphemeralMessage(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_ephemeral_message, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_gutter_bg;
        View findViewById = view.findViewById(R.id.chat_list_adapter_item_gutter_bg);
        if (findViewById != null) {
            i = R.id.chat_list_adapter_item_highlighted_bg;
            View findViewById2 = view.findViewById(R.id.chat_list_adapter_item_highlighted_bg);
            if (findViewById2 != null) {
                i = R.id.chat_list_adapter_item_icon_help;
                ImageView imageView = (ImageView) view.findViewById(R.id.chat_list_adapter_item_icon_help);
                if (imageView != null) {
                    i = R.id.chat_list_adapter_item_text_dismiss;
                    TextView textView = (TextView) view.findViewById(R.id.chat_list_adapter_item_text_dismiss);
                    if (textView != null) {
                        i = R.id.chat_list_adapter_item_visibility_icon;
                        ImageView imageView2 = (ImageView) view.findViewById(R.id.chat_list_adapter_item_visibility_icon);
                        if (imageView2 != null) {
                            i = R.id.uikit_chat_guideline;
                            Guideline guideline = (Guideline) view.findViewById(R.id.uikit_chat_guideline);
                            if (guideline != null) {
                                WidgetChatListAdapterItemEphemeralMessageBinding widgetChatListAdapterItemEphemeralMessageBinding = new WidgetChatListAdapterItemEphemeralMessageBinding((ConstraintLayout) view, findViewById, findViewById2, imageView, textView, imageView2, guideline);
                                m.checkNotNullExpressionValue(widgetChatListAdapterItemEphemeralMessageBinding, "WidgetChatListAdapterIte…ageBinding.bind(itemView)");
                                this.binding = widgetChatListAdapterItemEphemeralMessageBinding;
                                return;
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemEphemeralMessage widgetChatListAdapterItemEphemeralMessage) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemEphemeralMessage.adapter;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        Message message = ((EphemeralMessageEntry) chatListEntry).getMessage();
        View view = this.binding.f2299b;
        m.checkNotNullExpressionValue(view, "binding.chatListAdapterItemGutterBg");
        View view2 = this.binding.c;
        m.checkNotNullExpressionValue(view2, "binding.chatListAdapterItemHighlightedBg");
        configureCellHighlight(message, view2, view);
        this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemEphemeralMessage$onConfigure$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                WidgetChatListAdapterItemEphemeralMessageBinding widgetChatListAdapterItemEphemeralMessageBinding;
                UriHandler uriHandler = UriHandler.INSTANCE;
                widgetChatListAdapterItemEphemeralMessageBinding = WidgetChatListAdapterItemEphemeralMessage.this.binding;
                ImageView imageView = widgetChatListAdapterItemEphemeralMessageBinding.d;
                m.checkNotNullExpressionValue(imageView, "binding.chatListAdapterItemIconHelp");
                Context context = imageView.getContext();
                m.checkNotNullExpressionValue(context, "binding.chatListAdapterItemIconHelp.context");
                UriHandler.handle$default(uriHandler, context, f.a.a(1500000580222L, null), null, 4, null);
            }
        });
        TextView textView = this.binding.e;
        m.checkNotNullExpressionValue(textView, "binding.chatListAdapterItemTextDismiss");
        String quantityString = textView.getResources().getQuantityString(R.plurals.only_you_can_see_and_delete_these_count, 1);
        m.checkNotNullExpressionValue(quantityString, "itemDismiss.resources.ge…nd_delete_these_count, 1)");
        String quantityString2 = textView.getResources().getQuantityString(R.plurals.only_you_can_see_and_delete_these_countMessages, 1);
        m.checkNotNullExpressionValue(quantityString2, "itemDismiss.resources.ge…e_these_countMessages, 1)");
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(b.d(textView, R.string.only_you_can_see_and_delete_these, new Object[]{quantityString, quantityString2}, new WidgetChatListAdapterItemEphemeralMessage$onConfigure$2(this, message)));
    }
}
