package com.discord.widgets.chat.list.adapter;

import b.d.b.a.a;
import com.discord.stores.StoreMessageState;
import com.discord.stores.StoreStream;
import com.discord.utilities.textprocessing.node.SpoilerNode;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemEmbed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\n\u0010\u0001\u001a\u0006\u0012\u0002\b\u00030\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/textprocessing/node/SpoilerNode;", "node", "", "invoke", "(Lcom/discord/utilities/textprocessing/node/SpoilerNode;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemEmbed$configureEmbedDescription$1 extends o implements Function1<SpoilerNode<?>, Unit> {
    public final /* synthetic */ int $embedIndex;
    public final /* synthetic */ long $messageId;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemEmbed$configureEmbedDescription$1(long j, int i) {
        super(1);
        this.$messageId = j;
        this.$embedIndex = i;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(SpoilerNode<?> spoilerNode) {
        invoke2(spoilerNode);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(SpoilerNode<?> spoilerNode) {
        m.checkNotNullParameter(spoilerNode, "node");
        StoreMessageState messageState = StoreStream.Companion.getMessageState();
        long j = this.$messageId;
        int i = this.$embedIndex;
        StringBuilder R = a.R("desc:");
        R.append(spoilerNode.getId());
        messageState.revealSpoilerEmbedData(j, i, R.toString());
    }
}
