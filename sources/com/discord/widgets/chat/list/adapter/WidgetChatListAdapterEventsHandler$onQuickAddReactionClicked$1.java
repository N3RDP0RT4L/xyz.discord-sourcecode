package com.discord.widgets.chat.list.adapter;

import androidx.fragment.app.FragmentManager;
import com.discord.models.domain.emoji.Emoji;
import com.discord.widgets.chat.input.emoji.EmojiPickerContextType;
import com.discord.widgets.chat.input.emoji.EmojiPickerListener;
import com.discord.widgets.chat.input.emoji.EmojiPickerNavigator;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatListAdapterEventsHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ WidgetChatListAdapterEventsHandler this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1(WidgetChatListAdapterEventsHandler widgetChatListAdapterEventsHandler, long j, long j2) {
        super(0);
        this.this$0 = widgetChatListAdapterEventsHandler;
        this.$channelId = j;
        this.$messageId = j2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        FragmentManager fragmentManager;
        fragmentManager = this.this$0.getFragmentManager();
        EmojiPickerNavigator.launchBottomSheet$default(fragmentManager, new EmojiPickerListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1.1
            @Override // com.discord.widgets.chat.input.emoji.EmojiPickerListener
            public void onEmojiPicked(Emoji emoji) {
                WidgetChatListAdapterEventsHandler.UserReactionHandler userReactionHandler;
                m.checkNotNullParameter(emoji, "emoji");
                userReactionHandler = WidgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1.this.this$0.userReactionHandler;
                WidgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1 widgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1 = WidgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1.this;
                userReactionHandler.addNewReaction(emoji, widgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1.$channelId, widgetChatListAdapterEventsHandler$onQuickAddReactionClicked$1.$messageId);
            }
        }, EmojiPickerContextType.Chat.INSTANCE, null, 8, null);
    }
}
