package com.discord.widgets.chat.list.adapter;

import android.content.Context;
import com.discord.api.message.reaction.MessageReaction;
import com.discord.widgets.chat.managereactions.WidgetManageReactions;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatListAdapterEventsHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterEventsHandler$onReactionLongClicked$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ long $messageId;
    public final /* synthetic */ MessageReaction $reaction;
    public final /* synthetic */ WidgetChatListAdapterEventsHandler this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterEventsHandler$onReactionLongClicked$1(WidgetChatListAdapterEventsHandler widgetChatListAdapterEventsHandler, long j, long j2, MessageReaction messageReaction) {
        super(0);
        this.this$0 = widgetChatListAdapterEventsHandler;
        this.$channelId = j;
        this.$messageId = j2;
        this.$reaction = messageReaction;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Context context;
        WidgetManageReactions.Companion companion = WidgetManageReactions.Companion;
        long j = this.$channelId;
        long j2 = this.$messageId;
        context = this.this$0.getContext();
        companion.create(j, j2, context, this.$reaction);
    }
}
