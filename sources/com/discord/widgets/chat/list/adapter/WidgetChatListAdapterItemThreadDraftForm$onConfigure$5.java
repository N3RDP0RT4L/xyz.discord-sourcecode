package com.discord.widgets.chat.list.adapter;

import android.content.Context;
import android.text.style.StyleSpan;
import android.view.View;
import com.discord.i18n.Hook;
import com.discord.i18n.RenderContext;
import com.discord.stores.StoreThreadDraft;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.spans.ClickableSpan;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.channels.threads.WidgetThreadDraftArchiveSheet;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.ThreadDraftFormEntry;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Locale;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemThreadDraftForm.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/RenderContext;", "", "invoke", "(Lcom/discord/i18n/RenderContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemThreadDraftForm$onConfigure$5 extends o implements Function1<RenderContext, Unit> {
    public final /* synthetic */ ChatListEntry $data;
    public final /* synthetic */ ThreadDraftFormEntry $draftData;
    public final /* synthetic */ StoreThreadDraft.ThreadDraftState $draftState;
    public final /* synthetic */ WidgetChatListAdapterItemThreadDraftForm this$0;

    /* compiled from: WidgetChatListAdapterItemThreadDraftForm.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/i18n/Hook;", "", "invoke", "(Lcom/discord/i18n/Hook;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$5$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Hook, Unit> {

        /* compiled from: WidgetChatListAdapterItemThreadDraftForm.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadDraftForm$onConfigure$5$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02451 extends o implements Function1<View, Unit> {
            public C02451() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(View view) {
                invoke2(view);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(View view) {
                m.checkNotNullParameter(view, "it");
                WidgetThreadDraftArchiveSheet.Companion.show(WidgetChatListAdapterItemThreadDraftForm.access$getAdapter$p(WidgetChatListAdapterItemThreadDraftForm$onConfigure$5.this.this$0).getFragmentManager(), WidgetChatListAdapterItemThreadDraftForm$onConfigure$5.this.$draftData.getGuildId());
            }
        }

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Hook hook) {
            invoke2(hook);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Hook hook) {
            m.checkNotNullParameter(hook, "$receiver");
            ThreadUtils threadUtils = ThreadUtils.INSTANCE;
            Context context = WidgetChatListAdapterItemThreadDraftForm.access$getAdapter$p(WidgetChatListAdapterItemThreadDraftForm$onConfigure$5.this.this$0).getContext();
            Integer autoArchiveDuration = WidgetChatListAdapterItemThreadDraftForm$onConfigure$5.this.$draftState.getAutoArchiveDuration();
            if (autoArchiveDuration == null) {
                autoArchiveDuration = ((ThreadDraftFormEntry) WidgetChatListAdapterItemThreadDraftForm$onConfigure$5.this.$data).getDefaultAutoArchiveDuration();
            }
            String autoArchiveDurationName = threadUtils.autoArchiveDurationName(context, autoArchiveDuration != null ? autoArchiveDuration.intValue() : 1440);
            Locale locale = Locale.getDefault();
            m.checkNotNullExpressionValue(locale, "Locale.getDefault()");
            Objects.requireNonNull(autoArchiveDurationName, "null cannot be cast to non-null type java.lang.String");
            String lowerCase = autoArchiveDurationName.toLowerCase(locale);
            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            hook.f2681b = lowerCase;
            hook.a.add(new ClickableSpan(Integer.valueOf(ColorCompat.getThemedColor(WidgetChatListAdapterItemThreadDraftForm.access$getAdapter$p(WidgetChatListAdapterItemThreadDraftForm$onConfigure$5.this.this$0).getContext(), (int) R.attr.colorHeaderPrimary)), true, null, new C02451(), 4, null));
            hook.a.add(new StyleSpan(1));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemThreadDraftForm$onConfigure$5(WidgetChatListAdapterItemThreadDraftForm widgetChatListAdapterItemThreadDraftForm, StoreThreadDraft.ThreadDraftState threadDraftState, ChatListEntry chatListEntry, ThreadDraftFormEntry threadDraftFormEntry) {
        super(1);
        this.this$0 = widgetChatListAdapterItemThreadDraftForm;
        this.$draftState = threadDraftState;
        this.$data = chatListEntry;
        this.$draftData = threadDraftFormEntry;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RenderContext renderContext) {
        invoke2(renderContext);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(RenderContext renderContext) {
        m.checkNotNullParameter(renderContext, "$receiver");
        renderContext.a("durationHook", new AnonymousClass1());
    }
}
