package com.discord.widgets.chat.list.adapter;

import android.view.View;
import com.discord.databinding.WidgetChatListAdapterItemSystemBinding;
import com.discord.models.message.Message;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemSystemMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "it", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemSystemMessage$onConfigure$2 extends o implements Function1<View, Unit> {
    public final /* synthetic */ Message $message;
    public final /* synthetic */ WidgetChatListAdapterItemSystemMessage this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemSystemMessage$onConfigure$2(WidgetChatListAdapterItemSystemMessage widgetChatListAdapterItemSystemMessage, Message message) {
        super(1);
        this.this$0 = widgetChatListAdapterItemSystemMessage;
        this.$message = message;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        WidgetChatListAdapterItemSystemBinding widgetChatListAdapterItemSystemBinding;
        m.checkNotNullParameter(view, "it");
        WidgetChatListAdapter.EventHandler eventHandler = WidgetChatListAdapterItemSystemMessage.access$getAdapter$p(this.this$0).getEventHandler();
        Message message = this.$message;
        widgetChatListAdapterItemSystemBinding = this.this$0.binding;
        LinkifiedTextView linkifiedTextView = widgetChatListAdapterItemSystemBinding.d;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.systemText");
        CharSequence text = linkifiedTextView.getText();
        m.checkNotNullExpressionValue(text, "binding.systemText.text");
        eventHandler.onMessageLongClicked(message, text, false);
    }
}
