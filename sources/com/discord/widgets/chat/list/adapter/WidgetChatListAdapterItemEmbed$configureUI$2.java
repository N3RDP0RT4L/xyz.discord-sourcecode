package com.discord.widgets.chat.list.adapter;

import android.view.ViewPropertyAnimator;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemEmbed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Landroid/view/ViewPropertyAnimator;", "", "invoke", "(Landroid/view/ViewPropertyAnimator;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemEmbed$configureUI$2 extends o implements Function1<ViewPropertyAnimator, Unit> {
    public static final WidgetChatListAdapterItemEmbed$configureUI$2 INSTANCE = new WidgetChatListAdapterItemEmbed$configureUI$2();

    public WidgetChatListAdapterItemEmbed$configureUI$2() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ViewPropertyAnimator viewPropertyAnimator) {
        invoke2(viewPropertyAnimator);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ViewPropertyAnimator viewPropertyAnimator) {
        m.checkNotNullParameter(viewPropertyAnimator, "$receiver");
        viewPropertyAnimator.scaleX(0.9f);
        viewPropertyAnimator.scaleY(0.9f);
    }
}
