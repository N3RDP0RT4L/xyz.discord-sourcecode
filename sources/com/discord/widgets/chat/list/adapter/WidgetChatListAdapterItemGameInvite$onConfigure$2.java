package com.discord.widgets.chat.list.adapter;

import com.discord.databinding.WidgetChatListAdapterItemGameInviteBinding;
import com.discord.widgets.chat.list.ViewEmbedGameInvite;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListAdapterItemGameInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "it", "", "invoke", "(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemGameInvite$onConfigure$2 extends o implements Function1<ViewEmbedGameInvite.Model, Unit> {
    public final /* synthetic */ WidgetChatListAdapterItemGameInvite this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemGameInvite$onConfigure$2(WidgetChatListAdapterItemGameInvite widgetChatListAdapterItemGameInvite) {
        super(1);
        this.this$0 = widgetChatListAdapterItemGameInvite;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ViewEmbedGameInvite.Model model) {
        invoke2(model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ViewEmbedGameInvite.Model model) {
        WidgetChatListAdapterItemGameInviteBinding widgetChatListAdapterItemGameInviteBinding;
        m.checkNotNullParameter(model, "it");
        widgetChatListAdapterItemGameInviteBinding = this.this$0.binding;
        widgetChatListAdapterItemGameInviteBinding.f2300b.bind(model, WidgetChatListAdapterItemGameInvite.access$getAdapter$p(this.this$0).getClock());
    }
}
