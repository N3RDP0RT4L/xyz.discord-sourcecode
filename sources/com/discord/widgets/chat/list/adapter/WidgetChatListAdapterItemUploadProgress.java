package com.discord.widgets.chat.list.adapter;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.exifinterface.media.ExifInterface;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetChatListAdapterItemUploadProgressBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreMessageUploads;
import com.discord.stores.StoreStream;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.file.FileUtilsKt;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.SendUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.views.UploadProgressView;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.UploadProgressEntry;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$LongRef;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001c2\u00020\u0001:\u0003\u001c\u001d\u001eB\u000f\u0012\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\b\u001a\u00020\u0004*\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\fH\u0014¢\u0006\u0004\b\u000e\u0010\u000fJ\u0011\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0014¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0018\u0010\u0016\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;", "uploadState", "", "configureUI", "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;)V", "Lcom/discord/views/UploadProgressView;", "setUploadState", "(Lcom/discord/views/UploadProgressView;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lrx/Subscription;", "getSubscription", "()Lrx/Subscription;", "Lcom/discord/databinding/WidgetChatListAdapterItemUploadProgressBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemUploadProgressBinding;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Companion", ExifInterface.TAG_MODEL, "ModelProvider", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemUploadProgress extends WidgetChatListItem {
    public static final Companion Companion = new Companion(null);
    private static final long MODEL_THROTTLE_MS = 100;
    private final WidgetChatListAdapterItemUploadProgressBinding binding;
    private Subscription subscription;

    /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Companion;", "", "", "MODEL_THROTTLE_MS", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0006\u0004\u0005\u0006\u0007\b\tB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0005\n\u000b\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;", "", HookHelper.constructorName, "()V", "Companion", "Few", "Many", "None", "Preprocessing", "Single", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$None;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Single;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Few;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Many;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Model {
        public static final Companion Companion = new Companion(null);
        public static final int PROGRESS_INDETERMINATE = -1;
        public static final long SIZE_UNKNOWN = -1;

        /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Companion;", "", "", "PROGRESS_INDETERMINATE", "I", "", "SIZE_UNKNOWN", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Few;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;", "", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Single;", "component1", "()Ljava/util/List;", "uploads", "copy", "(Ljava/util/List;)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Few;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getUploads", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Few extends Model {
            private final List<Single> uploads;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Few(List<Single> list) {
                super(null);
                m.checkNotNullParameter(list, "uploads");
                this.uploads = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Few copy$default(Few few, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = few.uploads;
                }
                return few.copy(list);
            }

            public final List<Single> component1() {
                return this.uploads;
            }

            public final Few copy(List<Single> list) {
                m.checkNotNullParameter(list, "uploads");
                return new Few(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Few) && m.areEqual(this.uploads, ((Few) obj).uploads);
                }
                return true;
            }

            public final List<Single> getUploads() {
                return this.uploads;
            }

            public int hashCode() {
                List<Single> list = this.uploads;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("Few(uploads="), this.uploads, ")");
            }
        }

        /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Many;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;", "", "component1", "()I", "", "component2", "()J", "component3", "numFiles", "sizeBytes", "progress", "copy", "(IJI)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Many;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getProgress", "J", "getSizeBytes", "getNumFiles", HookHelper.constructorName, "(IJI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Many extends Model {
            private final int numFiles;
            private final int progress;
            private final long sizeBytes;

            public Many(int i, long j, int i2) {
                super(null);
                this.numFiles = i;
                this.sizeBytes = j;
                this.progress = i2;
            }

            public static /* synthetic */ Many copy$default(Many many, int i, long j, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    i = many.numFiles;
                }
                if ((i3 & 2) != 0) {
                    j = many.sizeBytes;
                }
                if ((i3 & 4) != 0) {
                    i2 = many.progress;
                }
                return many.copy(i, j, i2);
            }

            public final int component1() {
                return this.numFiles;
            }

            public final long component2() {
                return this.sizeBytes;
            }

            public final int component3() {
                return this.progress;
            }

            public final Many copy(int i, long j, int i2) {
                return new Many(i, j, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Many)) {
                    return false;
                }
                Many many = (Many) obj;
                return this.numFiles == many.numFiles && this.sizeBytes == many.sizeBytes && this.progress == many.progress;
            }

            public final int getNumFiles() {
                return this.numFiles;
            }

            public final int getProgress() {
                return this.progress;
            }

            public final long getSizeBytes() {
                return this.sizeBytes;
            }

            public int hashCode() {
                return ((b.a(this.sizeBytes) + (this.numFiles * 31)) * 31) + this.progress;
            }

            public String toString() {
                StringBuilder R = a.R("Many(numFiles=");
                R.append(this.numFiles);
                R.append(", sizeBytes=");
                R.append(this.sizeBytes);
                R.append(", progress=");
                return a.A(R, this.progress, ")");
            }
        }

        /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$None;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class None extends Model {
            public static final None INSTANCE = new None();

            private None() {
                super(null);
            }
        }

        /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J2\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0007J\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R\u001b\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "component3", "numFiles", "displayName", "mimeType", "copy", "(ILjava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Preprocessing;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getNumFiles", "Ljava/lang/String;", "getMimeType", "getDisplayName", HookHelper.constructorName, "(ILjava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Preprocessing extends Model {
            private final String displayName;
            private final String mimeType;
            private final int numFiles;

            public Preprocessing(int i, String str, String str2) {
                super(null);
                this.numFiles = i;
                this.displayName = str;
                this.mimeType = str2;
            }

            public static /* synthetic */ Preprocessing copy$default(Preprocessing preprocessing, int i, String str, String str2, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = preprocessing.numFiles;
                }
                if ((i2 & 2) != 0) {
                    str = preprocessing.displayName;
                }
                if ((i2 & 4) != 0) {
                    str2 = preprocessing.mimeType;
                }
                return preprocessing.copy(i, str, str2);
            }

            public final int component1() {
                return this.numFiles;
            }

            public final String component2() {
                return this.displayName;
            }

            public final String component3() {
                return this.mimeType;
            }

            public final Preprocessing copy(int i, String str, String str2) {
                return new Preprocessing(i, str, str2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Preprocessing)) {
                    return false;
                }
                Preprocessing preprocessing = (Preprocessing) obj;
                return this.numFiles == preprocessing.numFiles && m.areEqual(this.displayName, preprocessing.displayName) && m.areEqual(this.mimeType, preprocessing.mimeType);
            }

            public final String getDisplayName() {
                return this.displayName;
            }

            public final String getMimeType() {
                return this.mimeType;
            }

            public final int getNumFiles() {
                return this.numFiles;
            }

            public int hashCode() {
                int i = this.numFiles * 31;
                String str = this.displayName;
                int i2 = 0;
                int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.mimeType;
                if (str2 != null) {
                    i2 = str2.hashCode();
                }
                return hashCode + i2;
            }

            public String toString() {
                StringBuilder R = a.R("Preprocessing(numFiles=");
                R.append(this.numFiles);
                R.append(", displayName=");
                R.append(this.displayName);
                R.append(", mimeType=");
                return a.H(R, this.mimeType, ")");
            }
        }

        /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0006\u0012\u0006\u0010\u000f\u001a\u00020\t¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ8\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00062\b\b\u0002\u0010\u000f\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0013\u0010\u000bJ\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u000b¨\u0006\""}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Single;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()J", "", "component4", "()I", ModelAuditLogEntry.CHANGE_KEY_NAME, "mimeType", "sizeBytes", "progress", "copy", "(Ljava/lang/String;Ljava/lang/String;JI)Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Single;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getMimeType", "J", "getSizeBytes", "getName", "I", "getProgress", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;JI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Single extends Model {
            private final String mimeType;
            private final String name;
            private final int progress;
            private final long sizeBytes;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Single(String str, String str2, long j, int i) {
                super(null);
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullParameter(str2, "mimeType");
                this.name = str;
                this.mimeType = str2;
                this.sizeBytes = j;
                this.progress = i;
            }

            public static /* synthetic */ Single copy$default(Single single, String str, String str2, long j, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    str = single.name;
                }
                if ((i2 & 2) != 0) {
                    str2 = single.mimeType;
                }
                String str3 = str2;
                if ((i2 & 4) != 0) {
                    j = single.sizeBytes;
                }
                long j2 = j;
                if ((i2 & 8) != 0) {
                    i = single.progress;
                }
                return single.copy(str, str3, j2, i);
            }

            public final String component1() {
                return this.name;
            }

            public final String component2() {
                return this.mimeType;
            }

            public final long component3() {
                return this.sizeBytes;
            }

            public final int component4() {
                return this.progress;
            }

            public final Single copy(String str, String str2, long j, int i) {
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullParameter(str2, "mimeType");
                return new Single(str, str2, j, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Single)) {
                    return false;
                }
                Single single = (Single) obj;
                return m.areEqual(this.name, single.name) && m.areEqual(this.mimeType, single.mimeType) && this.sizeBytes == single.sizeBytes && this.progress == single.progress;
            }

            public final String getMimeType() {
                return this.mimeType;
            }

            public final String getName() {
                return this.name;
            }

            public final int getProgress() {
                return this.progress;
            }

            public final long getSizeBytes() {
                return this.sizeBytes;
            }

            public int hashCode() {
                String str = this.name;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                String str2 = this.mimeType;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return ((b.a(this.sizeBytes) + ((hashCode + i) * 31)) * 31) + this.progress;
            }

            public String toString() {
                StringBuilder R = a.R("Single(name=");
                R.append(this.name);
                R.append(", mimeType=");
                R.append(this.mimeType);
                R.append(", sizeBytes=");
                R.append(this.sizeBytes);
                R.append(", progress=");
                return a.A(R, this.progress, ")");
            }
        }

        private Model() {
        }

        public /* synthetic */ Model(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChatListAdapterItemUploadProgress.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ%\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ+\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u00062\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\r\u0010\u000eJ+\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00062\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0010\u0010\u000eJ\u001f\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J%\u0010\u0019\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00180\u00062\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u00020\u00138\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u001c¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$ModelProvider;", "", "Lcom/discord/utilities/rest/SendUtils$FileUpload;", "upload", "", "throttleIntervalMs", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Single;", "getSingleUploadObservable", "(Lcom/discord/utilities/rest/SendUtils$FileUpload;J)Lrx/Observable;", "", "uploads", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Few;", "getFewUploadsObservable", "(Ljava/util/List;J)Lrx/Observable;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model$Many;", "getManyUploadsObservable", "bytesWritten", "contentLengthBytes", "", "getPercentage", "(JJ)I", "", "nonce", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemUploadProgress$Model;", "get", "(Ljava/lang/String;J)Lrx/Observable;", "MAX_DETAILED_UPLOADS", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ModelProvider {
        public static final ModelProvider INSTANCE = new ModelProvider();
        private static final int MAX_DETAILED_UPLOADS = 3;

        private ModelProvider() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Model.Few> getFewUploadsObservable(List<SendUtils.FileUpload> list, long j) {
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (SendUtils.FileUpload fileUpload : list) {
                arrayList.add(INSTANCE.getSingleUploadObservable(fileUpload, j));
            }
            Observable<Model.Few> b2 = Observable.b(arrayList, WidgetChatListAdapterItemUploadProgress$ModelProvider$getFewUploadsObservable$2.INSTANCE);
            m.checkNotNullExpressionValue(b2, "Observable\n          .co…{ it as Model.Single }) }");
            return b2;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Model.Many> getManyUploadsObservable(final List<SendUtils.FileUpload> list, long j) {
            final Ref$LongRef ref$LongRef = new Ref$LongRef();
            ref$LongRef.element = 0L;
            for (SendUtils.FileUpload fileUpload : list) {
                if (fileUpload.getContentLength() <= 0) {
                    ref$LongRef.element = -1L;
                } else {
                    ref$LongRef.element = fileUpload.getContentLength() + ref$LongRef.element;
                }
            }
            if (ref$LongRef.element <= 0) {
                k kVar = new k(new Model.Many(list.size(), -1L, -1));
                m.checkNotNullExpressionValue(kVar, "Observable\n            .…          )\n            )");
                return kVar;
            }
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (SendUtils.FileUpload fileUpload2 : list) {
                arrayList.add(ObservableExtensionsKt.leadingEdgeThrottle(fileUpload2.getBytesWrittenObservable(), j, TimeUnit.MILLISECONDS));
            }
            Observable<Model.Many> F = Observable.b(arrayList, WidgetChatListAdapterItemUploadProgress$ModelProvider$getManyUploadsObservable$3.INSTANCE).F(new j0.k.b<Long, Integer>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress$ModelProvider$getManyUploadsObservable$4
                public final Integer call(Long l) {
                    int percentage;
                    WidgetChatListAdapterItemUploadProgress.ModelProvider modelProvider = WidgetChatListAdapterItemUploadProgress.ModelProvider.INSTANCE;
                    m.checkNotNullExpressionValue(l, "totalBytesWritten");
                    percentage = modelProvider.getPercentage(l.longValue(), Ref$LongRef.this.element);
                    return Integer.valueOf(percentage);
                }
            }).q().F(new j0.k.b<Integer, Model.Many>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress$ModelProvider$getManyUploadsObservable$5
                public final WidgetChatListAdapterItemUploadProgress.Model.Many call(Integer num) {
                    int size = list.size();
                    long j2 = ref$LongRef.element;
                    m.checkNotNullExpressionValue(num, "overallProgressPercent");
                    return new WidgetChatListAdapterItemUploadProgress.Model.Many(size, j2, num.intValue());
                }
            });
            m.checkNotNullExpressionValue(F, "Observable\n            .…essPercent)\n            }");
            return F;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final int getPercentage(long j, long j2) {
            float f = (float) j;
            if (j2 > 0) {
                return (int) ((f / ((float) j2)) * 100);
            }
            Logger.e$default(AppLog.g, "contentLengthBytes was not positive", new Exception(), null, 4, null);
            return 0;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Model.Single> getSingleUploadObservable(final SendUtils.FileUpload fileUpload, long j) {
            if (fileUpload.getContentLength() <= 0) {
                k kVar = new k(new Model.Single(fileUpload.getName(), fileUpload.getMimeType(), -1L, -1));
                m.checkNotNullExpressionValue(kVar, "Observable.just(\n       …E\n            )\n        )");
                return kVar;
            }
            Observable<Model.Single> F = ObservableExtensionsKt.leadingEdgeThrottle(fileUpload.getBytesWrittenObservable(), j, TimeUnit.MILLISECONDS).F(new j0.k.b<Long, Integer>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress$ModelProvider$getSingleUploadObservable$1
                public final Integer call(Long l) {
                    int percentage;
                    WidgetChatListAdapterItemUploadProgress.ModelProvider modelProvider = WidgetChatListAdapterItemUploadProgress.ModelProvider.INSTANCE;
                    m.checkNotNullExpressionValue(l, "bytesWritten");
                    percentage = modelProvider.getPercentage(l.longValue(), SendUtils.FileUpload.this.getContentLength());
                    return Integer.valueOf(percentage);
                }
            }).q().F(new j0.k.b<Integer, Model.Single>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress$ModelProvider$getSingleUploadObservable$2
                public final WidgetChatListAdapterItemUploadProgress.Model.Single call(Integer num) {
                    String name = SendUtils.FileUpload.this.getName();
                    String mimeType = SendUtils.FileUpload.this.getMimeType();
                    long contentLength = SendUtils.FileUpload.this.getContentLength();
                    m.checkNotNullExpressionValue(num, "progressPercent");
                    return new WidgetChatListAdapterItemUploadProgress.Model.Single(name, mimeType, contentLength, num.intValue());
                }
            });
            m.checkNotNullExpressionValue(F, "upload\n            .byte…          )\n            }");
            return F;
        }

        public final Observable<? extends Model> get(String str, final long j) {
            m.checkNotNullParameter(str, "nonce");
            StoreMessageUploads messageUploads = StoreStream.Companion.getMessageUploads();
            Observable<? extends Model> Y = ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{messageUploads}, false, null, null, new WidgetChatListAdapterItemUploadProgress$ModelProvider$get$1(messageUploads, str), 14, null).Y(new j0.k.b<StoreMessageUploads.MessageUploadState, Observable<? extends Model>>() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress$ModelProvider$get$2
                public final Observable<? extends WidgetChatListAdapterItemUploadProgress.Model> call(StoreMessageUploads.MessageUploadState messageUploadState) {
                    Observable<? extends WidgetChatListAdapterItemUploadProgress.Model> manyUploadsObservable;
                    Observable<? extends WidgetChatListAdapterItemUploadProgress.Model> fewUploadsObservable;
                    Observable<? extends WidgetChatListAdapterItemUploadProgress.Model> singleUploadObservable;
                    if (messageUploadState instanceof StoreMessageUploads.MessageUploadState.None) {
                        return new k(WidgetChatListAdapterItemUploadProgress.Model.None.INSTANCE);
                    }
                    if (messageUploadState instanceof StoreMessageUploads.MessageUploadState.Preprocessing) {
                        StoreMessageUploads.MessageUploadState.Preprocessing preprocessing = (StoreMessageUploads.MessageUploadState.Preprocessing) messageUploadState;
                        return new k(new WidgetChatListAdapterItemUploadProgress.Model.Preprocessing(preprocessing.getNumFiles(), preprocessing.getDisplayName(), preprocessing.getMimeType()));
                    } else if (messageUploadState instanceof StoreMessageUploads.MessageUploadState.Uploading) {
                        List<SendUtils.FileUpload> uploads = ((StoreMessageUploads.MessageUploadState.Uploading) messageUploadState).getUploads();
                        if (uploads.size() == 1) {
                            singleUploadObservable = WidgetChatListAdapterItemUploadProgress.ModelProvider.INSTANCE.getSingleUploadObservable((SendUtils.FileUpload) u.first((List<? extends Object>) uploads), j);
                            return singleUploadObservable;
                        } else if (uploads.size() <= 3) {
                            fewUploadsObservable = WidgetChatListAdapterItemUploadProgress.ModelProvider.INSTANCE.getFewUploadsObservable(uploads, j);
                            return fewUploadsObservable;
                        } else {
                            manyUploadsObservable = WidgetChatListAdapterItemUploadProgress.ModelProvider.INSTANCE.getManyUploadsObservable(uploads, j);
                            return manyUploadsObservable;
                        }
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                }
            });
            m.checkNotNullExpressionValue(Y, "ObservationDeckProvider.…      }\n        }\n      }");
            return Y;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemUploadProgress(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_upload_progress, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.progress_cancel_centered;
        ImageView imageView = (ImageView) view.findViewById(R.id.progress_cancel_centered);
        if (imageView != null) {
            i = R.id.progress_cancel_top;
            ImageView imageView2 = (ImageView) view.findViewById(R.id.progress_cancel_top);
            if (imageView2 != null) {
                i = R.id.upload_progress_1;
                UploadProgressView uploadProgressView = (UploadProgressView) view.findViewById(R.id.upload_progress_1);
                if (uploadProgressView != null) {
                    i = R.id.upload_progress_2;
                    UploadProgressView uploadProgressView2 = (UploadProgressView) view.findViewById(R.id.upload_progress_2);
                    if (uploadProgressView2 != null) {
                        i = R.id.upload_progress_3;
                        UploadProgressView uploadProgressView3 = (UploadProgressView) view.findViewById(R.id.upload_progress_3);
                        if (uploadProgressView3 != null) {
                            WidgetChatListAdapterItemUploadProgressBinding widgetChatListAdapterItemUploadProgressBinding = new WidgetChatListAdapterItemUploadProgressBinding((LinearLayout) view, imageView, imageView2, uploadProgressView, uploadProgressView2, uploadProgressView3);
                            m.checkNotNullExpressionValue(widgetChatListAdapterItemUploadProgressBinding, "WidgetChatListAdapterIte…essBinding.bind(itemView)");
                            this.binding = widgetChatListAdapterItemUploadProgressBinding;
                            return;
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x00db, code lost:
        if (r1 == false) goto L31;
     */
    /* JADX WARN: Removed duplicated region for block: B:39:0x00f3  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x0112  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void configureUI(com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress.Model r10) {
        /*
            Method dump skipped, instructions count: 295
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress.configureUI(com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress$Model):void");
    }

    private final void setUploadState(UploadProgressView uploadProgressView, Model model) {
        CharSequence d;
        if (m.areEqual(model, Model.None.INSTANCE)) {
            d = b.a.k.b.d(uploadProgressView, R.string.upload_queued, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            int i = UploadProgressView.j;
            uploadProgressView.a(d, 0, null);
            Context context = uploadProgressView.getContext();
            m.checkNotNullExpressionValue(context, "context");
            uploadProgressView.setIcon(DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.ic_uploads_generic, 0, 2, (Object) null));
            return;
        }
        int i2 = -1;
        if (model instanceof Model.Preprocessing) {
            Model.Preprocessing preprocessing = (Model.Preprocessing) model;
            CharSequence displayName = preprocessing.getDisplayName();
            if (displayName == null) {
                Resources resources = uploadProgressView.getResources();
                m.checkNotNullExpressionValue(resources, "resources");
                Context context2 = uploadProgressView.getContext();
                m.checkNotNullExpressionValue(context2, "context");
                displayName = StringResourceUtilsKt.getQuantityString(resources, context2, (int) R.plurals.uploading_files_count, preprocessing.getNumFiles(), Integer.valueOf(preprocessing.getNumFiles()));
            }
            int i3 = UploadProgressView.j;
            uploadProgressView.a(displayName, -1, null);
            if (preprocessing.getMimeType() != null) {
                Context context3 = uploadProgressView.getContext();
                m.checkNotNullExpressionValue(context3, "context");
                uploadProgressView.setIcon(FileUtilsKt.getIconForFiletype(context3, preprocessing.getMimeType()));
                return;
            }
            Context context4 = uploadProgressView.getContext();
            m.checkNotNullExpressionValue(context4, "context");
            uploadProgressView.setIcon(DrawableCompat.getThemedDrawableRes$default(context4, (int) R.attr.ic_uploads_generic, 0, 2, (Object) null));
        } else if (model instanceof Model.Single) {
            Model.Single single = (Model.Single) model;
            String name = single.getName();
            if (single.getProgress() != -1) {
                i2 = single.getProgress();
            }
            uploadProgressView.a(name, i2, FileUtilsKt.getSizeSubtitle(single.getSizeBytes()));
            Context context5 = uploadProgressView.getContext();
            m.checkNotNullExpressionValue(context5, "context");
            uploadProgressView.setIcon(FileUtilsKt.getIconForFiletype(context5, single.getMimeType()));
        } else if (model instanceof Model.Many) {
            Resources resources2 = uploadProgressView.getResources();
            m.checkNotNullExpressionValue(resources2, "resources");
            Context context6 = uploadProgressView.getContext();
            m.checkNotNullExpressionValue(context6, "context");
            Model.Many many = (Model.Many) model;
            CharSequence quantityString = StringResourceUtilsKt.getQuantityString(resources2, context6, (int) R.plurals.uploading_files_count, many.getNumFiles(), Integer.valueOf(many.getNumFiles()));
            if (many.getProgress() != -1) {
                i2 = many.getProgress();
            }
            uploadProgressView.a(quantityString, i2, FileUtilsKt.getSizeSubtitle(many.getSizeBytes()));
            Context context7 = uploadProgressView.getContext();
            m.checkNotNullExpressionValue(context7, "context");
            uploadProgressView.setIcon(DrawableCompat.getThemedDrawableRes$default(context7, (int) R.attr.ic_uploads_generic, 0, 2, (Object) null));
        }
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerViewHolder
    public Subscription getSubscription() {
        return this.subscription;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        Observable<? extends Model> K = ModelProvider.INSTANCE.get(((UploadProgressEntry) chatListEntry).getMessageNonce(), 100L).K();
        m.checkNotNullExpressionValue(K, "ModelProvider.get(data.m…  .onBackpressureLatest()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(K), WidgetChatListAdapterItemUploadProgress.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetChatListAdapterItemUploadProgress$onConfigure$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListAdapterItemUploadProgress$onConfigure$1(this));
        final WidgetChatListAdapterItemUploadProgress$onConfigure$cancel$1 widgetChatListAdapterItemUploadProgress$onConfigure$cancel$1 = new WidgetChatListAdapterItemUploadProgress$onConfigure$cancel$1(chatListEntry);
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress$onConfigure$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        this.binding.f2323b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemUploadProgress$onConfigure$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }
}
