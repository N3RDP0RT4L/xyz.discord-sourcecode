package com.discord.widgets.chat.list.adapter;

import android.content.Context;
import b.a.k.b;
import com.discord.api.user.User;
import com.discord.models.message.Message;
import com.discord.utilities.message.MessageUtils;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemSystemMessage.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Landroid/content/Context;", "", "invoke", "(Landroid/content/Context;)Ljava/lang/CharSequence;", "getString"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemSystemMessage$getSystemMessage$1 extends o implements Function1<Context, CharSequence> {
    public final /* synthetic */ Function1 $actorRenderContext;
    public final /* synthetic */ String $authorName;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ Long $firstMentionedUserId;
    public final /* synthetic */ String $firstMentionedUserName;
    public final /* synthetic */ String $guildName;
    public final /* synthetic */ Message $this_getSystemMessage;
    public final /* synthetic */ Function1 $usernameRenderContext;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemSystemMessage$getSystemMessage$1(Message message, String str, String str2, Function1 function1, Long l, Context context, String str3, Function1 function12) {
        super(1);
        this.$this_getSystemMessage = message;
        this.$authorName = str;
        this.$firstMentionedUserName = str2;
        this.$usernameRenderContext = function1;
        this.$firstMentionedUserId = l;
        this.$context = context;
        this.$guildName = str3;
        this.$actorRenderContext = function12;
    }

    public final CharSequence invoke(Context context) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence b6;
        CharSequence b7;
        CharSequence b8;
        CharSequence b9;
        CharSequence b10;
        m.checkNotNullParameter(context, "$this$getString");
        Integer type = this.$this_getSystemMessage.getType();
        if (type != null && type.intValue() == 1) {
            return b.b(context, R.string.system_message_recipient_add, new Object[]{this.$authorName, this.$firstMentionedUserName}, this.$usernameRenderContext);
        }
        Long l = null;
        if (type != null && type.intValue() == 2) {
            Long l2 = this.$firstMentionedUserId;
            User author = this.$this_getSystemMessage.getAuthor();
            if (author != null) {
                l = Long.valueOf(author.i());
            }
            if (m.areEqual(l2, l)) {
                return b.b(context, R.string.system_message_recipient_remove_self, new Object[]{this.$authorName}, this.$usernameRenderContext);
            }
            return b.b(context, R.string.system_message_recipient_remove, new Object[]{this.$authorName, this.$firstMentionedUserName}, this.$usernameRenderContext);
        } else if (type != null && type.intValue() == 4) {
            return b.b(context, R.string.system_message_channel_name_change, new Object[]{this.$authorName, this.$this_getSystemMessage.getContent()}, this.$usernameRenderContext);
        } else {
            if (type != null && type.intValue() == 5) {
                return b.b(context, R.string.system_message_channel_icon_change, new Object[]{this.$authorName}, this.$usernameRenderContext);
            }
            if (type != null && type.intValue() == 6) {
                return b.b(context, R.string.system_message_pinned_message_no_cta, new Object[]{this.$authorName}, this.$usernameRenderContext);
            }
            if (type != null && type.intValue() == 7) {
                return b.b(context, MessageUtils.INSTANCE.getSystemMessageUserJoin(this.$context, this.$this_getSystemMessage.getId()), new Object[]{this.$authorName}, this.$usernameRenderContext);
            }
            if (type != null && type.intValue() == 8) {
                String content = this.$this_getSystemMessage.getContent();
                int parseInt = content == null || content.length() == 0 ? 1 : Integer.parseInt(this.$this_getSystemMessage.getContent());
                if (parseInt > 1) {
                    return b.b(context, R.string.system_message_guild_member_subscribed_many, new Object[]{this.$authorName, String.valueOf(parseInt)}, this.$usernameRenderContext);
                }
                return b.b(context, R.string.system_message_guild_member_subscribed, new Object[]{this.$authorName}, this.$usernameRenderContext);
            } else if (type != null && type.intValue() == 9) {
                b10 = b.b(context, R.string.premium_guild_tier_1, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b.b(context, R.string.system_message_guild_member_subscribed_achieved_tier, new Object[]{this.$authorName, this.$guildName, b10}, this.$usernameRenderContext);
            } else if (type != null && type.intValue() == 10) {
                b9 = b.b(context, R.string.premium_guild_tier_2, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b.b(context, R.string.system_message_guild_member_subscribed_achieved_tier, new Object[]{this.$authorName, this.$guildName, b9}, this.$usernameRenderContext);
            } else if (type != null && type.intValue() == 11) {
                b8 = b.b(context, R.string.premium_guild_tier_3, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                return b.b(context, R.string.system_message_guild_member_subscribed_achieved_tier, new Object[]{this.$authorName, this.$guildName, b8}, this.$usernameRenderContext);
            } else if (type != null && type.intValue() == 12) {
                return b.b(context, R.string.system_message_channel_follow_add, new Object[]{this.$authorName, this.$this_getSystemMessage.getContent()}, this.$usernameRenderContext);
            } else {
                if (type != null && type.intValue() == 14) {
                    b7 = b.b(context, R.string.system_message_guild_discovery_disqualified_mobile, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b7;
                } else if (type != null && type.intValue() == 15) {
                    b6 = b.b(context, R.string.system_message_guild_discovery_requalified, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b6;
                } else if (type != null && type.intValue() == 16) {
                    b5 = b.b(context, R.string.system_message_guild_discovery_grace_period_initial_warning, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b5;
                } else if (type != null && type.intValue() == 17) {
                    b4 = b.b(context, R.string.system_message_guild_discovery_grace_period_final_warning, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b4;
                } else if (type != null && type.intValue() == 18) {
                    return b.b(context, R.string.system_message_thread_created_mobile, new Object[]{this.$authorName, this.$this_getSystemMessage.getContent()}, this.$actorRenderContext);
                } else {
                    if (type != null && type.intValue() == 24) {
                        b3 = b.b(context, R.string.thread_starter_message_not_loaded, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                        return b3;
                    }
                    b2 = b.b(context, R.string.reply_quote_message_not_loaded, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                    return b2;
                }
            }
        }
    }
}
