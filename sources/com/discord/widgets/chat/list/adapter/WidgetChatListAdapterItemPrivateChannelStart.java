package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.StringRes;
import b.a.k.b;
import com.discord.databinding.WidgetChatListAdapterItemPrivateChannelStartBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.views.PileView;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.StartOfPrivateChatEntry;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemPrivateChannelStart.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001f\u0010\n\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0007H\u0014¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemPrivateChannelStart;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "", "channelType", "getStartResId", "(I)I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "data", "", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/databinding/WidgetChatListAdapterItemPrivateChannelStartBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemPrivateChannelStartBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemPrivateChannelStart extends WidgetChatListItem {
    private final WidgetChatListAdapterItemPrivateChannelStartBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemPrivateChannelStart(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_private_channel_start, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_private_channel_start_header;
        TextView textView = (TextView) view.findViewById(R.id.chat_list_adapter_item_private_channel_start_header);
        if (textView != null) {
            i = R.id.mutual_guild_pile;
            PileView pileView = (PileView) view.findViewById(R.id.mutual_guild_pile);
            if (pileView != null) {
                i = R.id.mutual_guilds;
                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.mutual_guilds);
                if (linearLayout != null) {
                    i = R.id.mutual_guilds_text;
                    TextView textView2 = (TextView) view.findViewById(R.id.mutual_guilds_text);
                    if (textView2 != null) {
                        i = R.id.private_channel_start_image;
                        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.private_channel_start_image);
                        if (simpleDraweeView != null) {
                            i = R.id.private_channel_start_text;
                            TextView textView3 = (TextView) view.findViewById(R.id.private_channel_start_text);
                            if (textView3 != null) {
                                WidgetChatListAdapterItemPrivateChannelStartBinding widgetChatListAdapterItemPrivateChannelStartBinding = new WidgetChatListAdapterItemPrivateChannelStartBinding((LinearLayout) view, textView, pileView, linearLayout, textView2, simpleDraweeView, textView3);
                                m.checkNotNullExpressionValue(widgetChatListAdapterItemPrivateChannelStartBinding, "WidgetChatListAdapterIte…artBinding.bind(itemView)");
                                this.binding = widgetChatListAdapterItemPrivateChannelStartBinding;
                                return;
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @StringRes
    private final int getStartResId(int i) {
        return (i == 1 || i != 3) ? R.string.beginning_dm : R.string.beginning_group_dm;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, ChatListEntry chatListEntry) {
        CharSequence charSequence;
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        StartOfPrivateChatEntry startOfPrivateChatEntry = (StartOfPrivateChatEntry) chatListEntry;
        String component2 = startOfPrivateChatEntry.component2();
        int component3 = startOfPrivateChatEntry.component3();
        String component4 = startOfPrivateChatEntry.component4();
        boolean component5 = startOfPrivateChatEntry.component5();
        List<Guild> component6 = startOfPrivateChatEntry.component6();
        if (!(!t.isBlank(component2))) {
            TextView textView = this.binding.f2309b;
            m.checkNotNullExpressionValue(textView, "binding.chatListAdapterI…PrivateChannelStartHeader");
            component2 = textView.getContext().getString(R.string.unnamed);
            m.checkNotNullExpressionValue(component2, "binding.chatListAdapterI…tString(R.string.unnamed)");
        }
        TextView textView2 = this.binding.f2309b;
        m.checkNotNullExpressionValue(textView2, "binding.chatListAdapterI…PrivateChannelStartHeader");
        textView2.setText(component2);
        if (component4 != null) {
            SimpleDraweeView simpleDraweeView = this.binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.privateChannelStartImage");
            IconUtils.setIcon$default(simpleDraweeView, component4, (int) R.dimen.avatar_size_xxlarge, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
        } else {
            SimpleDraweeView simpleDraweeView2 = this.binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.privateChannelStartImage");
            IconUtils.setIcon$default(simpleDraweeView2, IconUtils.DEFAULT_ICON, (int) R.dimen.avatar_size_xxlarge, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
        }
        if (component5) {
            this.binding.g.setText(R.string.system_dm_empty_message);
        } else {
            TextView textView3 = this.binding.g;
            m.checkNotNullExpressionValue(textView3, "binding.privateChannelStartText");
            b.m(textView3, getStartResId(component3), new Object[]{component2}, (r4 & 4) != 0 ? b.g.j : null);
        }
        boolean z2 = !component5 && component3 == 1;
        LinearLayout linearLayout = this.binding.d;
        m.checkNotNullExpressionValue(linearLayout, "binding.mutualGuilds");
        linearLayout.setVisibility(z2 ? 0 : 8);
        if (z2) {
            PileView pileView = this.binding.c;
            ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(component6, 10));
            for (Guild guild : component6) {
                arrayList.add(new PileView.c(new WidgetChatListAdapterItemPrivateChannelStart$onConfigure$1$1(IconUtils.getForGuild$default(guild, null, false, Integer.valueOf(DimenUtils.dpToPixels(24)), 2, null)), new WidgetChatListAdapterItemPrivateChannelStart$onConfigure$1$2(guild)));
            }
            pileView.setItems(arrayList);
            TextView textView4 = this.binding.e;
            m.checkNotNullExpressionValue(textView4, "binding.mutualGuildsText");
            if (!component6.isEmpty()) {
                TextView textView5 = this.binding.e;
                m.checkNotNullExpressionValue(textView5, "binding.mutualGuildsText");
                Context context = textView5.getContext();
                m.checkNotNullExpressionValue(context, "binding.mutualGuildsText.context");
                charSequence = StringResourceUtilsKt.getI18nPluralString(context, R.plurals.user_profile_mutual_guilds_count, component6.size(), Integer.valueOf(component6.size()));
            } else {
                TextView textView6 = this.binding.e;
                m.checkNotNullExpressionValue(textView6, "binding.mutualGuildsText");
                charSequence = b.d(textView6, R.string.no_mutual_guilds, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            }
            textView4.setText(charSequence);
        }
    }
}
