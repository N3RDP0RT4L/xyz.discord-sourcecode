package com.discord.widgets.chat.list.adapter;

import android.content.Context;
import android.net.Uri;
import b.a.d.m;
import b.a.k.b;
import com.discord.app.AppLog;
import com.discord.utilities.io.NetworkUtils;
import d0.z.d.o;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterEventsHandler.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterEventsHandler$onQuickDownloadClicked$1 extends o implements Function0<Unit> {
    public final /* synthetic */ String $fileName;
    public final /* synthetic */ Uri $uri;
    public final /* synthetic */ WeakReference $weakContext;
    public final /* synthetic */ WidgetChatListAdapterEventsHandler this$0;

    /* compiled from: WidgetChatListAdapterEventsHandler.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "savedFileName", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler$onQuickDownloadClicked$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<String, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            CharSequence b2;
            Context context = (Context) WidgetChatListAdapterEventsHandler$onQuickDownloadClicked$1.this.$weakContext.get();
            if (context != null) {
                b2 = b.b(context, R.string.download_file_complete, new Object[]{str}, (r4 & 4) != 0 ? b.C0034b.j : null);
                m.h(context, b2, 0, null, 12);
            }
        }
    }

    /* compiled from: WidgetChatListAdapterEventsHandler.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "error", "", "invoke", "(Ljava/lang/Throwable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterEventsHandler$onQuickDownloadClicked$1$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<Throwable, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
            invoke2(th);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Throwable th) {
            d0.z.d.m.checkNotNullParameter(th, "error");
            AppLog.i("Could not download attachment due to:  \n" + th);
            Context context = (Context) WidgetChatListAdapterEventsHandler$onQuickDownloadClicked$1.this.$weakContext.get();
            if (context != null) {
                m.h((Context) WidgetChatListAdapterEventsHandler$onQuickDownloadClicked$1.this.$weakContext.get(), context.getString(R.string.download_failed), 0, null, 12);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterEventsHandler$onQuickDownloadClicked$1(WidgetChatListAdapterEventsHandler widgetChatListAdapterEventsHandler, Uri uri, String str, WeakReference weakReference) {
        super(0);
        this.this$0 = widgetChatListAdapterEventsHandler;
        this.$uri = uri;
        this.$fileName = str;
        this.$weakContext = weakReference;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Context context;
        context = this.this$0.getContext();
        NetworkUtils.downloadFile(context, this.$uri, this.$fileName, null, new AnonymousClass1(), new AnonymousClass2());
    }
}
