package com.discord.widgets.chat.list.adapter;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import rx.functions.Action0;
/* compiled from: WidgetChatListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapter$scrollToMessageId$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Action0 $onCompleted;
    public final /* synthetic */ WidgetChatListAdapter this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapter$scrollToMessageId$1(WidgetChatListAdapter widgetChatListAdapter, Action0 action0) {
        super(0);
        this.this$0 = widgetChatListAdapter;
        this.$onCompleted = action0;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.scrollToWithHighlight = null;
        this.this$0.publishInteractionState();
        this.$onCompleted.call();
    }
}
