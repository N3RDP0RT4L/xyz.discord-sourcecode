package com.discord.widgets.chat.list.adapter;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.LeadingMarginSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.ContextCompat;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.api.thread.ThreadMetadata;
import com.discord.databinding.WidgetChatListAdapterItemThreadEmbedBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.CoreUser;
import com.discord.stores.StoreMessageState;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.widgets.chat.list.ViewReplySpline;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.ThreadEmbedEntry;
import com.discord.widgets.chat.list.utils.EmbeddedMessageParser;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetChatListAdapterItemThreadEmbed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 52\u00020\u0001:\u00015B\u000f\u0012\u0006\u00102\u001a\u000201¢\u0006\u0004\b3\u00104J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJS\u0010\u0017\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\u000b0\u000f2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00132\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018JU\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\r2\u0014\u0010\u001a\u001a\u0010\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0019\u0018\u00010\u000f2\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\u000b0\u000f2\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ;\u0010\u001f\u001a\u00020\u00042\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0010j\u0002`\u0011\u0012\u0004\u0012\u00020\u000b0\u000f2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013H\u0002¢\u0006\u0004\b\u001f\u0010 J\u001b\u0010!\u001a\u00020\u00042\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\u0002¢\u0006\u0004\b!\u0010\"J\u0019\u0010$\u001a\u00020\u00022\b\u0010#\u001a\u0004\u0018\u00010\u0013H\u0002¢\u0006\u0004\b$\u0010%J\u0019\u0010(\u001a\u00020'2\b\b\u0002\u0010&\u001a\u00020\u001bH\u0002¢\u0006\u0004\b(\u0010)J\u001f\u0010,\u001a\u00020\u00042\u0006\u0010*\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020+H\u0014¢\u0006\u0004\b,\u0010-R\u0016\u0010/\u001a\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100¨\u00066"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemThreadEmbed;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListItem;", "", "count", "", "configureMessageCount", "(I)V", "Lcom/discord/widgets/chat/list/entries/ThreadEmbedEntry;", "data", "configureMostRecentMessageText", "(Lcom/discord/widgets/chat/list/entries/ThreadEmbedEntry;)V", "", "messageText", "Lcom/discord/models/message/Message;", "mostRecentMessage", "", "", "Lcom/discord/primitives/UserId;", "nickOrUsernames", "Lcom/discord/models/member/GuildMember;", "mostRecentMessageAuthorGuildMember", "Landroid/graphics/drawable/Drawable;", "icon", "setNoMostRecentMessage", "(Ljava/lang/String;Lcom/discord/models/message/Message;Ljava/util/Map;Lcom/discord/models/member/GuildMember;Landroid/graphics/drawable/Drawable;)V", "Lcom/discord/api/role/GuildRole;", "roleMentions", "", "animateEmojis", "setMostRecentMessage", "(Lcom/discord/models/message/Message;Ljava/util/Map;Ljava/util/Map;ZLcom/discord/models/member/GuildMember;)V", "setAuthorAndAvatar", "(Lcom/discord/models/message/Message;Ljava/util/Map;Lcom/discord/models/member/GuildMember;)V", "setIcon", "(Landroid/graphics/drawable/Drawable;)V", "member", "getAuthorTextColor", "(Lcom/discord/models/member/GuildMember;)I", "showAuthor", "Landroid/text/style/LeadingMarginSpan;", "getLeadingMarginSpan", "(Z)Landroid/text/style/LeadingMarginSpan;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "onConfigure", "(ILcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/databinding/WidgetChatListAdapterItemThreadEmbedBinding;", "binding", "Lcom/discord/databinding/WidgetChatListAdapterItemThreadEmbedBinding;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListAdapterItemThreadEmbed extends WidgetChatListItem {
    public static final Companion Companion = new Companion(null);
    public static final int MAX_THREAD_MESSAGE_AST_NODES = 50;
    private final WidgetChatListAdapterItemThreadEmbedBinding binding;

    /* compiled from: WidgetChatListAdapterItemThreadEmbed.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapterItemThreadEmbed$Companion;", "", "", "MAX_THREAD_MESSAGE_AST_NODES", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListAdapterItemThreadEmbed(WidgetChatListAdapter widgetChatListAdapter) {
        super(R.layout.widget_chat_list_adapter_item_thread_embed, widgetChatListAdapter);
        m.checkNotNullParameter(widgetChatListAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.chat_list_adapter_item_gutter_bg;
        View findViewById = view.findViewById(R.id.chat_list_adapter_item_gutter_bg);
        if (findViewById != null) {
            i = R.id.chat_list_adapter_item_highlighted_bg;
            View findViewById2 = view.findViewById(R.id.chat_list_adapter_item_highlighted_bg);
            if (findViewById2 != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                i = R.id.thread_embed_icon;
                ImageView imageView = (ImageView) view.findViewById(R.id.thread_embed_icon);
                if (imageView != null) {
                    i = R.id.thread_embed_message_center_guideline;
                    View findViewById3 = view.findViewById(R.id.thread_embed_message_center_guideline);
                    if (findViewById3 != null) {
                        i = R.id.thread_embed_messages_count;
                        TextView textView = (TextView) view.findViewById(R.id.thread_embed_messages_count);
                        if (textView != null) {
                            i = R.id.thread_embed_messages_count_chevron;
                            ImageView imageView2 = (ImageView) view.findViewById(R.id.thread_embed_messages_count_chevron);
                            if (imageView2 != null) {
                                i = R.id.thread_embed_most_recent_message_avatar;
                                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.thread_embed_most_recent_message_avatar);
                                if (simpleDraweeView != null) {
                                    i = R.id.thread_embed_most_recent_message_content;
                                    SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) view.findViewById(R.id.thread_embed_most_recent_message_content);
                                    if (simpleDraweeSpanTextView != null) {
                                        i = R.id.thread_embed_most_recent_message_header;
                                        ConstraintLayout constraintLayout2 = (ConstraintLayout) view.findViewById(R.id.thread_embed_most_recent_message_header);
                                        if (constraintLayout2 != null) {
                                            i = R.id.thread_embed_most_recent_message_name;
                                            TextView textView2 = (TextView) view.findViewById(R.id.thread_embed_most_recent_message_name);
                                            if (textView2 != null) {
                                                i = R.id.thread_embed_name;
                                                TextView textView3 = (TextView) view.findViewById(R.id.thread_embed_name);
                                                if (textView3 != null) {
                                                    i = R.id.thread_embed_selectable_background;
                                                    ConstraintLayout constraintLayout3 = (ConstraintLayout) view.findViewById(R.id.thread_embed_selectable_background);
                                                    if (constraintLayout3 != null) {
                                                        i = R.id.thread_embed_spine;
                                                        ViewReplySpline viewReplySpline = (ViewReplySpline) view.findViewById(R.id.thread_embed_spine);
                                                        if (viewReplySpline != null) {
                                                            i = R.id.uikit_chat_guideline;
                                                            Guideline guideline = (Guideline) view.findViewById(R.id.uikit_chat_guideline);
                                                            if (guideline != null) {
                                                                WidgetChatListAdapterItemThreadEmbedBinding widgetChatListAdapterItemThreadEmbedBinding = new WidgetChatListAdapterItemThreadEmbedBinding(constraintLayout, findViewById, findViewById2, constraintLayout, imageView, findViewById3, textView, imageView2, simpleDraweeView, simpleDraweeSpanTextView, constraintLayout2, textView2, textView3, constraintLayout3, viewReplySpline, guideline);
                                                                m.checkNotNullExpressionValue(widgetChatListAdapterItemThreadEmbedBinding, "WidgetChatListAdapterIte…bedBinding.bind(itemView)");
                                                                this.binding = widgetChatListAdapterItemThreadEmbedBinding;
                                                                return;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetChatListAdapter access$getAdapter$p(WidgetChatListAdapterItemThreadEmbed widgetChatListAdapterItemThreadEmbed) {
        return (WidgetChatListAdapter) widgetChatListAdapterItemThreadEmbed.adapter;
    }

    private final void configureMessageCount(int i) {
        ColorStateList colorStateList;
        TextView textView = this.binding.f;
        if (i == 0) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
            textView.setText(i >= 50 ? "50+" : String.valueOf(i));
        }
        ImageView imageView = this.binding.g;
        if (i == 0) {
            colorStateList = ColorStateList.valueOf(ColorCompat.getThemedColor(imageView.getContext(), (int) R.attr.colorInteractiveNormal));
        } else {
            colorStateList = ColorStateList.valueOf(ColorCompat.getThemedColor(imageView.getContext(), (int) R.attr.colorTextLink));
        }
        imageView.setImageTintList(colorStateList);
    }

    private final void configureMostRecentMessageText(ThreadEmbedEntry threadEmbedEntry) {
        ConstraintLayout constraintLayout = this.binding.d;
        m.checkNotNullExpressionValue(constraintLayout, "binding.threadEmbedContainer");
        Context context = constraintLayout.getContext();
        Message mostRecentMessage = threadEmbedEntry.getMostRecentMessage();
        ConstraintLayout constraintLayout2 = this.binding.j;
        m.checkNotNullExpressionValue(constraintLayout2, "binding.threadEmbedMostRecentMessageHeader");
        constraintLayout2.setVisibility(8);
        ThreadMetadata y2 = threadEmbedEntry.getThread().y();
        if (y2 != null && y2.b()) {
            m.checkNotNullExpressionValue(context, "context");
            Drawable drawable = ContextCompat.getDrawable(context, DrawableCompat.getThemedDrawableRes(context, (int) R.attr.ic_thread_archived_clock, 0));
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            String string = view.getResources().getString(R.string.thread_archived);
            m.checkNotNullExpressionValue(string, "itemView.resources.getSt…R.string.thread_archived)");
            setNoMostRecentMessage$default(this, string, null, threadEmbedEntry.getNickOrUsernames(), null, drawable, 10, null);
        } else if (threadEmbedEntry.getThreadMessageCount() == 0) {
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            String string2 = view2.getResources().getString(R.string.no_thread_messages);
            m.checkNotNullExpressionValue(string2, "itemView.resources.getSt…tring.no_thread_messages)");
            setNoMostRecentMessage$default(this, string2, null, threadEmbedEntry.getNickOrUsernames(), null, null, 26, null);
        } else {
            if ((mostRecentMessage != null ? mostRecentMessage.getContent() : null) == null || mostRecentMessage.getAuthor() == null || threadEmbedEntry.getMostRecentMessageGuildMember() == null) {
                View view3 = this.itemView;
                m.checkNotNullExpressionValue(view3, "itemView");
                String string3 = view3.getResources().getString(R.string.no_recent_thread_messages);
                m.checkNotNullExpressionValue(string3, "itemView.resources.getSt…o_recent_thread_messages)");
                setNoMostRecentMessage$default(this, string3, null, threadEmbedEntry.getNickOrUsernames(), null, null, 26, null);
            } else if (mostRecentMessage.hasStickers()) {
                View view4 = this.itemView;
                m.checkNotNullExpressionValue(view4, "itemView");
                String string4 = view4.getResources().getString(R.string.reply_quote_sticker_mobile);
                m.checkNotNullExpressionValue(string4, "itemView.resources.getSt…ply_quote_sticker_mobile)");
                setNoMostRecentMessage$default(this, string4, mostRecentMessage, threadEmbedEntry.getNickOrUsernames(), threadEmbedEntry.getMostRecentMessageGuildMember(), null, 16, null);
            } else if (mostRecentMessage.hasAttachments() || mostRecentMessage.hasEmbeds()) {
                m.checkNotNullExpressionValue(context, "context");
                Drawable drawable2 = ContextCompat.getDrawable(context, DrawableCompat.getThemedDrawableRes(context, (int) R.attr.ic_flex_input_image, 0));
                View view5 = this.itemView;
                m.checkNotNullExpressionValue(view5, "itemView");
                String string5 = view5.getResources().getString(R.string.reply_quote_no_text_content_mobile);
                m.checkNotNullExpressionValue(string5, "itemView.resources.getSt…e_no_text_content_mobile)");
                setNoMostRecentMessage(string5, mostRecentMessage, threadEmbedEntry.getNickOrUsernames(), threadEmbedEntry.getMostRecentMessageGuildMember(), drawable2);
            } else {
                setMostRecentMessage(mostRecentMessage, threadEmbedEntry.getRoleMentions(), threadEmbedEntry.getNickOrUsernames(), threadEmbedEntry.getAnimateEmojis(), threadEmbedEntry.getMostRecentMessageGuildMember());
            }
        }
    }

    private final int getAuthorTextColor(GuildMember guildMember) {
        View view = this.itemView;
        m.checkNotNullExpressionValue(view, "itemView");
        return GuildMember.Companion.getColor(guildMember, ColorCompat.getThemedColor(view.getContext(), (int) R.attr.colorHeaderPrimary));
    }

    private final LeadingMarginSpan getLeadingMarginSpan(boolean z2) {
        int i;
        this.binding.j.measure(0, 0);
        if (z2) {
            ConstraintLayout constraintLayout = this.binding.j;
            m.checkNotNullExpressionValue(constraintLayout, "binding.threadEmbedMostRecentMessageHeader");
            i = constraintLayout.getMeasuredWidth();
        } else {
            i = 0;
        }
        return new LeadingMarginSpan.Standard(i, 0);
    }

    public static /* synthetic */ LeadingMarginSpan getLeadingMarginSpan$default(WidgetChatListAdapterItemThreadEmbed widgetChatListAdapterItemThreadEmbed, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        return widgetChatListAdapterItemThreadEmbed.getLeadingMarginSpan(z2);
    }

    private final void setAuthorAndAvatar(Message message, Map<Long, String> map, GuildMember guildMember) {
        if (message != null && message.getAuthor() != null && guildMember != null) {
            ConstraintLayout constraintLayout = this.binding.j;
            m.checkNotNullExpressionValue(constraintLayout, "binding.threadEmbedMostRecentMessageHeader");
            constraintLayout.setVisibility(0);
            TextView textView = this.binding.k;
            m.checkNotNullExpressionValue(textView, "binding.threadEmbedMostRecentMessageName");
            textView.setText(map.get(Long.valueOf(message.getAuthor().i())));
            this.binding.k.setTextColor(getAuthorTextColor(guildMember));
            CoreUser coreUser = new CoreUser(message.getAuthor());
            SimpleDraweeView simpleDraweeView = this.binding.h;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.threadEmbedMostRecentMessageAvatar");
            IconUtils.setIcon$default(simpleDraweeView, coreUser, R.dimen.avatar_size_small, null, null, guildMember, 24, null);
        }
    }

    private final void setIcon(Drawable drawable) {
        ImageView imageView = this.binding.e;
        m.checkNotNullExpressionValue(imageView, "binding.threadEmbedIcon");
        int i = 0;
        if (!(drawable != null)) {
            i = 8;
        }
        imageView.setVisibility(i);
        this.binding.e.setImageDrawable(drawable);
    }

    public static /* synthetic */ void setIcon$default(WidgetChatListAdapterItemThreadEmbed widgetChatListAdapterItemThreadEmbed, Drawable drawable, int i, Object obj) {
        if ((i & 1) != 0) {
            drawable = null;
        }
        widgetChatListAdapterItemThreadEmbed.setIcon(drawable);
    }

    private final void setMostRecentMessage(Message message, Map<Long, GuildRole> map, Map<Long, String> map2, boolean z2, GuildMember guildMember) {
        DraweeSpanStringBuilder parse = EmbeddedMessageParser.INSTANCE.parse(new EmbeddedMessageParser.ParserData(a.x(this.itemView, "itemView", "itemView.context"), map, map2, z2, new StoreMessageState.State(null, null, 3, null), 50, message, (WidgetChatListAdapter) this.adapter));
        setAuthorAndAvatar(message, map2, guildMember);
        parse.setSpan(getLeadingMarginSpan$default(this, false, 1, null), 0, parse.length(), 33);
        this.binding.i.setDraweeSpanStringBuilder(parse);
        SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.i;
        FontUtils fontUtils = FontUtils.INSTANCE;
        Context context = simpleDraweeSpanTextView.getContext();
        m.checkNotNullExpressionValue(context, "context");
        simpleDraweeSpanTextView.setTypeface(fontUtils.getThemedFont(context, R.attr.font_primary_normal), 0);
        setIcon$default(this, null, 1, null);
    }

    private final void setNoMostRecentMessage(String str, Message message, Map<Long, String> map, GuildMember guildMember, Drawable drawable) {
        SpannableString spannableString = new SpannableString(str + (char) 160);
        setAuthorAndAvatar(message, map, guildMember);
        spannableString.setSpan(getLeadingMarginSpan((message == null || guildMember == null) ? false : true), 0, spannableString.length(), 33);
        SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.i;
        m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.threadEmbedMostRecentMessageContent");
        simpleDraweeSpanTextView.setText(spannableString);
        SimpleDraweeSpanTextView simpleDraweeSpanTextView2 = this.binding.i;
        FontUtils fontUtils = FontUtils.INSTANCE;
        Context context = simpleDraweeSpanTextView2.getContext();
        m.checkNotNullExpressionValue(context, "context");
        simpleDraweeSpanTextView2.setTypeface(fontUtils.getThemedFont(context, R.attr.font_primary_normal), 2);
        setIcon(drawable);
    }

    public static /* synthetic */ void setNoMostRecentMessage$default(WidgetChatListAdapterItemThreadEmbed widgetChatListAdapterItemThreadEmbed, String str, Message message, Map map, GuildMember guildMember, Drawable drawable, int i, Object obj) {
        widgetChatListAdapterItemThreadEmbed.setNoMostRecentMessage(str, (i & 2) != 0 ? null : message, map, (i & 8) != 0 ? null : guildMember, (i & 16) != 0 ? null : drawable);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListItem
    public void onConfigure(int i, final ChatListEntry chatListEntry) {
        m.checkNotNullParameter(chatListEntry, "data");
        super.onConfigure(i, chatListEntry);
        if (chatListEntry instanceof ThreadEmbedEntry) {
            TextView textView = this.binding.l;
            m.checkNotNullExpressionValue(textView, "binding.threadEmbedName");
            ThreadEmbedEntry threadEmbedEntry = (ThreadEmbedEntry) chatListEntry;
            textView.setText(threadEmbedEntry.getThread().m());
            this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadEmbed$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetChatListAdapterItemThreadEmbed.access$getAdapter$p(WidgetChatListAdapterItemThreadEmbed.this).onThreadClicked(((ThreadEmbedEntry) chatListEntry).getThread());
                }
            });
            this.binding.a.setOnLongClickListener(new View.OnLongClickListener() { // from class: com.discord.widgets.chat.list.adapter.WidgetChatListAdapterItemThreadEmbed$onConfigure$2
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    WidgetChatListAdapterItemThreadEmbed.access$getAdapter$p(WidgetChatListAdapterItemThreadEmbed.this).onThreadLongClicked(((ThreadEmbedEntry) chatListEntry).getThread());
                    return true;
                }
            });
            configureMessageCount(threadEmbedEntry.getThreadMessageCount());
            configureMostRecentMessageText(threadEmbedEntry);
            Message parentMessage = threadEmbedEntry.getParentMessage();
            View view = this.binding.c;
            m.checkNotNullExpressionValue(view, "binding.chatListAdapterItemHighlightedBg");
            View view2 = this.binding.f2322b;
            m.checkNotNullExpressionValue(view2, "binding.chatListAdapterItemGutterBg");
            configureCellHighlight(parentMessage, view, view2);
        }
    }
}
