package com.discord.widgets.chat.list.sheet;

import com.discord.api.commands.ApplicationCommandValue;
import d0.g0.w;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u001a\u001d\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000*\b\u0012\u0004\u0012\u00020\u00010\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u001a\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000*\u00020\u0001¢\u0006\u0004\b\u0002\u0010\u0004\u001a\u0013\u0010\u0007\u001a\u00020\u0006*\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"", "Lcom/discord/api/commands/ApplicationCommandValue;", "flattenOptions", "(Ljava/util/List;)Ljava/util/List;", "(Lcom/discord/api/commands/ApplicationCommandValue;)Ljava/util/List;", "", "", "toStringTruncateZeroDecimal", "(Ljava/lang/Object;)Ljava/lang/String;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetApplicationCommandBottomSheetViewModelKt {
    public static final List<ApplicationCommandValue> flattenOptions(List<ApplicationCommandValue> list) {
        m.checkNotNullParameter(list, "$this$flattenOptions");
        ArrayList arrayList = new ArrayList();
        for (ApplicationCommandValue applicationCommandValue : list) {
            arrayList.addAll(flattenOptions(applicationCommandValue));
        }
        return arrayList;
    }

    public static final String toStringTruncateZeroDecimal(Object obj) {
        if (obj instanceof Number) {
            return w.removeSuffix(obj.toString(), ".0");
        }
        return String.valueOf(obj);
    }

    public static final List<ApplicationCommandValue> flattenOptions(ApplicationCommandValue applicationCommandValue) {
        m.checkNotNullParameter(applicationCommandValue, "$this$flattenOptions");
        List<ApplicationCommandValue> b2 = applicationCommandValue.b();
        if (b2 == null || b2.isEmpty()) {
            return d0.t.m.listOf(applicationCommandValue);
        }
        ArrayList arrayList = new ArrayList();
        List<ApplicationCommandValue> b3 = applicationCommandValue.b();
        if (b3 != null) {
            for (ApplicationCommandValue applicationCommandValue2 : b3) {
                arrayList.addAll(flattenOptions(applicationCommandValue2));
            }
        }
        return arrayList;
    }
}
