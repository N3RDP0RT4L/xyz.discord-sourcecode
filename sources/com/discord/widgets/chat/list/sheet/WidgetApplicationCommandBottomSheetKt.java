package com.discord.widgets.chat.list.sheet;

import com.discord.api.commands.ApplicationCommandData;
import com.discord.api.commands.ApplicationCommandValue;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: WidgetApplicationCommandBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a%\u0010\u0005\u001a\u00020\u0002*\u00020\u00002\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001¢\u0006\u0004\b\u0005\u0010\u0006\u001a%\u0010\u0005\u001a\u00020\u0002*\u00020\u00072\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001¢\u0006\u0004\b\u0005\u0010\b¨\u0006\t"}, d2 = {"Lcom/discord/api/commands/ApplicationCommandData;", "", "", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$SlashCommandParam;", "commandValues", "toSlashCommandCopyString", "(Lcom/discord/api/commands/ApplicationCommandData;Ljava/util/Map;)Ljava/lang/String;", "Lcom/discord/api/commands/ApplicationCommandValue;", "(Lcom/discord/api/commands/ApplicationCommandValue;Ljava/util/Map;)Ljava/lang/String;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetApplicationCommandBottomSheetKt {
    public static final String toSlashCommandCopyString(ApplicationCommandData applicationCommandData, Map<String, WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam> map) {
        String str;
        m.checkNotNullParameter(applicationCommandData, "$this$toSlashCommandCopyString");
        m.checkNotNullParameter(map, "commandValues");
        StringBuilder sb = new StringBuilder();
        sb.append(MentionUtilsKt.SLASH_CHAR);
        sb.append(applicationCommandData.a());
        sb.append(' ');
        List<ApplicationCommandValue> b2 = applicationCommandData.b();
        if (b2 == null || (str = u.joinToString$default(b2, " ", null, null, 0, null, new WidgetApplicationCommandBottomSheetKt$toSlashCommandCopyString$1(map), 30, null)) == null) {
            str = "";
        }
        sb.append(str);
        return sb.toString();
    }

    public static final String toSlashCommandCopyString(ApplicationCommandValue applicationCommandValue, Map<String, WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam> map) {
        m.checkNotNullParameter(applicationCommandValue, "$this$toSlashCommandCopyString");
        m.checkNotNullParameter(map, "commandValues");
        String str = null;
        if (applicationCommandValue.b() != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(applicationCommandValue.a());
            sb.append(' ');
            List<ApplicationCommandValue> b2 = applicationCommandValue.b();
            if (b2 != null) {
                str = u.joinToString$default(b2, " ", null, null, 0, null, new WidgetApplicationCommandBottomSheetKt$toSlashCommandCopyString$2(map), 30, null);
            }
            sb.append(str);
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(applicationCommandValue.a());
        sb2.append(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam slashCommandParam = map.get(applicationCommandValue.a());
        if (slashCommandParam != null) {
            str = slashCommandParam.getCopyText();
        }
        sb2.append(str);
        return sb2.toString();
    }
}
