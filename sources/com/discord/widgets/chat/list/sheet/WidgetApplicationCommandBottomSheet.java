package com.discord.widgets.chat.list.sheet;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.commands.ApplicationCommandData;
import com.discord.api.commands.ApplicationCommandValue;
import com.discord.api.user.User;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetApplicationCommandBottomSheetBinding;
import com.discord.models.commands.Application;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.g0.t;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetApplicationCommandBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 #2\u00020\u0001:\u0001#B\u0007¢\u0006\u0004\b\"\u0010\u0014J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\tJ!\u0010\n\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0003\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\n\u0010\u000eJ\u001f\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u000eJ\u000f\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007¢\u0006\u0004\b\u0015\u0010\tR\u001d\u0010\u001b\u001a\u00020\u00168B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010!\u001a\u00020\u001c8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 ¨\u0006$"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState;)V", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Loaded;", "configureCommandTitle", "(Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Loaded;)V", "configureSlashCommandString", "Lcom/discord/api/commands/ApplicationCommandValue;", "option", "Landroid/text/Spannable;", "(Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Loaded;Lcom/discord/api/commands/ApplicationCommandValue;)Landroid/text/Spannable;", "configureSlashCommandStringOptions", "", "getContentViewResId", "()I", "onResume", "()V", "configureLoaded", "Lcom/discord/databinding/WidgetApplicationCommandBottomSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetApplicationCommandBottomSheetBinding;", "binding", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetApplicationCommandBottomSheet extends AppBottomSheet {
    public static final String ARG_MESSAGE_NONCE = "arg_message_nonce";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetApplicationCommandBottomSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetApplicationCommandBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetApplicationCommandBottomSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetApplicationCommandBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013JQ\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u00042\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00042\b\u0010\f\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u000b8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "interactionId", "messageId", "channelId", "guildId", "userId", "applicationId", "", "messageNonce", "", "show", "(Landroidx/fragment/app/FragmentManager;JJJLjava/lang/Long;JJLjava/lang/String;)V", "ARG_MESSAGE_NONCE", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, long j, long j2, long j3, Long l, long j4, long j5, String str) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            boolean z2 = false;
            Experiment userExperiment = StoreStream.Companion.getExperiments().getUserExperiment("2021-03_android_app_slash_commands_bottom_sheet_disabled", false);
            if (userExperiment != null && userExperiment.getBucket() == 1) {
                z2 = true;
            }
            if (!z2) {
                WidgetApplicationCommandBottomSheet widgetApplicationCommandBottomSheet = new WidgetApplicationCommandBottomSheet();
                Bundle I = a.I("com.discord.intent.extra.EXTRA_INTERACTION_ID", j);
                I.putLong("com.discord.intent.extra.EXTRA_CHANNEL_ID", j3);
                I.putLong("com.discord.intent.extra.EXTRA_MESSAGE_ID", j2);
                if (l != null) {
                    I.putLong("com.discord.intent.extra.EXTRA_GUILD_ID", l.longValue());
                }
                I.putLong("com.discord.intent.extra.EXTRA_USER_ID", j4);
                I.putLong("com.discord.intent.extra.EXTRA_APPLICATION_ID", j5);
                I.putString(WidgetApplicationCommandBottomSheet.ARG_MESSAGE_NONCE, str);
                widgetApplicationCommandBottomSheet.setArguments(I);
                widgetApplicationCommandBottomSheet.show(fragmentManager, WidgetApplicationCommandBottomSheet.class.getName());
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetApplicationCommandBottomSheet() {
        super(false, 1, null);
        WidgetApplicationCommandBottomSheet$viewModel$2 widgetApplicationCommandBottomSheet$viewModel$2 = new WidgetApplicationCommandBottomSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetApplicationCommandBottomSheetViewModel.class), new WidgetApplicationCommandBottomSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetApplicationCommandBottomSheet$viewModel$2));
    }

    private final void configureCommandTitle(WidgetApplicationCommandBottomSheetViewModel.ViewState.Loaded loaded) {
        String str;
        String nick;
        User bot;
        StringBuilder O = a.O(MentionUtilsKt.SLASH_CHAR);
        O.append(loaded.getApplicationCommandData().a());
        String sb = O.toString();
        Application application = loaded.getApplication();
        String str2 = null;
        if (application == null || (bot = application.getBot()) == null || (str = bot.r()) == null) {
            Application application2 = loaded.getApplication();
            str = application2 != null ? application2.getName() : null;
        }
        int themedColor = ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorTextNormal);
        Application application3 = loaded.getApplication();
        if (application3 != null) {
            SimpleDraweeView simpleDraweeView = getBinding().f2209b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.commandBottomSheetCommandAvatar");
            IconUtils.setApplicationIcon(simpleDraweeView, application3);
        }
        TextView textView = getBinding().f;
        m.checkNotNullExpressionValue(textView, "binding.commandBottomSheetCommandTitle");
        Object[] objArr = new Object[2];
        GuildMember interactionUser = loaded.getInteractionUser();
        if (interactionUser == null || (nick = interactionUser.getNick()) == null) {
            com.discord.models.user.User user = loaded.getUser();
            if (user != null) {
                str2 = user.getUsername();
            }
        } else {
            str2 = nick;
        }
        int i = 0;
        objArr[0] = str2;
        boolean z2 = true;
        objArr[1] = sb;
        CharSequence d = b.d(textView, R.string.system_message_application_command_used_as_title, objArr, new WidgetApplicationCommandBottomSheet$configureCommandTitle$content$1(this, loaded, themedColor));
        TextView textView2 = getBinding().f;
        m.checkNotNullExpressionValue(textView2, "binding.commandBottomSheetCommandTitle");
        textView2.setMovementMethod(LinkMovementMethod.getInstance());
        TextView textView3 = getBinding().f;
        m.checkNotNullExpressionValue(textView3, "binding.commandBottomSheetCommandTitle");
        textView3.setText(d);
        TextView textView4 = getBinding().g;
        m.checkNotNullExpressionValue(textView4, "binding.commandBottomShe…mmandTitleApplicationName");
        if (str == null) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        textView4.setVisibility(i);
        TextView textView5 = getBinding().g;
        m.checkNotNullExpressionValue(textView5, "binding.commandBottomShe…mmandTitleApplicationName");
        textView5.setText(str);
    }

    private final void configureSlashCommandString(WidgetApplicationCommandBottomSheetViewModel.ViewState.Loaded loaded) {
        ApplicationCommandData applicationCommandData = loaded.getApplicationCommandData();
        List<ApplicationCommandValue> b2 = loaded.getApplicationCommandData().b();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        StringBuilder O = a.O(MentionUtilsKt.SLASH_CHAR);
        O.append(applicationCommandData.a());
        O.append(' ');
        spannableStringBuilder.append((CharSequence) O.toString());
        if (b2 != null) {
            for (ApplicationCommandValue applicationCommandValue : b2) {
                spannableStringBuilder.append((CharSequence) configureSlashCommandString(loaded, applicationCommandValue));
            }
        }
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.commandBottomSheetCommandContent");
        textView.setText(spannableStringBuilder);
    }

    private final Spannable configureSlashCommandStringOptions(WidgetApplicationCommandBottomSheetViewModel.ViewState.Loaded loaded, ApplicationCommandValue applicationCommandValue) {
        Integer valueColor;
        int themedColor = ColorCompat.getThemedColor(this, (int) R.attr.colorHeaderPrimary);
        WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam slashCommandParam = loaded.getCommandValues().get(applicationCommandValue.a());
        String str = null;
        if ((slashCommandParam != null ? slashCommandParam.getValueColor() : null) != null && ((valueColor = slashCommandParam.getValueColor()) == null || valueColor.intValue() != 0)) {
            themedColor = slashCommandParam.getValueColor().intValue();
        }
        if (slashCommandParam != null) {
            str = slashCommandParam.getValue();
        }
        SpannableStringBuilder append = new SpannableStringBuilder().append((CharSequence) applicationCommandValue.a());
        if (!(str == null || t.isBlank(str))) {
            SpannableStringBuilder append2 = append.append((CharSequence) ": ");
            m.checkNotNullExpressionValue(append2, "builder.append(\": \")");
            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(themedColor);
            int length = append2.length();
            append2.append((CharSequence) (str + ' '));
            append2.setSpan(foregroundColorSpan, length, append2.length(), 17);
        }
        m.checkNotNullExpressionValue(append, "builder");
        return append;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetApplicationCommandBottomSheetViewModel.ViewState viewState) {
        boolean z2 = viewState instanceof WidgetApplicationCommandBottomSheetViewModel.ViewState.Loading;
        ProgressBar progressBar = getBinding().e;
        m.checkNotNullExpressionValue(progressBar, "binding.commandBottomSheetCommandLoader");
        progressBar.setVisibility(z2 ? 0 : 8);
        MaterialButton materialButton = getBinding().d;
        m.checkNotNullExpressionValue(materialButton, "binding.commandBottomSheetCommandCopyButton");
        materialButton.setEnabled(!z2);
        if (viewState instanceof WidgetApplicationCommandBottomSheetViewModel.ViewState.Loaded) {
            MaterialButton materialButton2 = getBinding().d;
            m.checkNotNullExpressionValue(materialButton2, "binding.commandBottomSheetCommandCopyButton");
            materialButton2.setEnabled(true);
            configureLoaded((WidgetApplicationCommandBottomSheetViewModel.ViewState.Loaded) viewState);
        } else if (viewState instanceof WidgetApplicationCommandBottomSheetViewModel.ViewState.Failed) {
            dismiss();
        }
    }

    private final WidgetApplicationCommandBottomSheetBinding getBinding() {
        return (WidgetApplicationCommandBottomSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final WidgetApplicationCommandBottomSheetViewModel getViewModel() {
        return (WidgetApplicationCommandBottomSheetViewModel) this.viewModel$delegate.getValue();
    }

    public final void configureLoaded(final WidgetApplicationCommandBottomSheetViewModel.ViewState.Loaded loaded) {
        m.checkNotNullParameter(loaded, "viewState");
        configureCommandTitle(loaded);
        configureSlashCommandString(loaded);
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheet$configureLoaded$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Context requireContext = WidgetApplicationCommandBottomSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                b.a.d.m.c(requireContext, WidgetApplicationCommandBottomSheetKt.toSlashCommandCopyString(loaded.getApplicationCommandData(), loaded.getCommandValues()), 0, 4);
                WidgetApplicationCommandBottomSheet.this.dismiss();
            }
        });
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_application_command_bottom_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AppBottomSheet.hideKeyboard$default(this, null, 1, null);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetApplicationCommandBottomSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetApplicationCommandBottomSheet$onResume$1(this));
    }

    private final Spannable configureSlashCommandString(WidgetApplicationCommandBottomSheetViewModel.ViewState.Loaded loaded, ApplicationCommandValue applicationCommandValue) {
        List<ApplicationCommandValue> b2 = applicationCommandValue.b();
        if (b2 == null || b2.isEmpty()) {
            return configureSlashCommandStringOptions(loaded, applicationCommandValue);
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        SpannableStringBuilder append = spannableStringBuilder.append((CharSequence) (applicationCommandValue.a() + ' '));
        List<ApplicationCommandValue> b3 = applicationCommandValue.b();
        if (b3 != null) {
            for (ApplicationCommandValue applicationCommandValue2 : b3) {
                append.append((CharSequence) configureSlashCommandString(loaded, applicationCommandValue2));
            }
        }
        return append;
    }
}
