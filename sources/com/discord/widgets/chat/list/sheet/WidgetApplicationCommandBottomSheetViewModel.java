package com.discord.widgets.chat.list.sheet;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.commands.ApplicationCommandData;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.commands.Application;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreApplicationCommands;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGatewayConnection;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 &2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0005&'()*BW\u0012\u0006\u0010\u001a\u001a\u00020\u000b\u0012\u0006\u0010\u001c\u001a\u00020\u000b\u0012\n\u0010\r\u001a\u00060\u000bj\u0002`\f\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u001e\u001a\u00020\u000b\u0012\u0006\u0010 \u001a\u00020\u000b\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011\u0012\u000e\b\u0002\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00060\"¢\u0006\u0004\b$\u0010%J\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\r\u0010\n\u001a\u00020\u0003¢\u0006\u0004\b\n\u0010\u0005R\u001d\u0010\r\u001a\u00060\u000bj\u0002`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001a\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000e\u001a\u0004\b\u001b\u0010\u0010R\u0019\u0010\u001c\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u000e\u001a\u0004\b\u001d\u0010\u0010R\u0019\u0010\u001e\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010\u000e\u001a\u0004\b\u001f\u0010\u0010R\u0019\u0010 \u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u000e\u001a\u0004\b!\u0010\u0010¨\u0006+"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState;", "", "requestInteractionData", "()V", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;)V", "retry", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "getChannelId", "()J", "", "messageNonce", "Ljava/lang/String;", "getMessageNonce", "()Ljava/lang/String;", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "interactionId", "getInteractionId", "messageId", "getMessageId", "interactionUserId", "getInteractionUserId", "applicationId", "getApplicationId", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(JJJLjava/lang/Long;JJLjava/lang/String;Lrx/Observable;)V", "Companion", "SlashCommandParam", "StoreState", "UserData", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetApplicationCommandBottomSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final long applicationId;
    private final long channelId;
    private final Long guildId;
    private final long interactionId;
    private final long interactionUserId;
    private final long messageId;
    private final String messageNonce;

    /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(WidgetApplicationCommandBottomSheetViewModel widgetApplicationCommandBottomSheetViewModel) {
            super(1, widgetApplicationCommandBottomSheetViewModel, WidgetApplicationCommandBottomSheetViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((WidgetApplicationCommandBottomSheetViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$UserData;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;)Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$UserData;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2<T, R> implements b<StoreState, UserData> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public final UserData call(StoreState storeState) {
            return new UserData(storeState.getMentionedUsers(), storeState.getUsers());
        }
    }

    /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$UserData;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$UserData;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends o implements Function1<UserData, Unit> {
        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(UserData userData) {
            invoke2(userData);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(UserData userData) {
            if (WidgetApplicationCommandBottomSheetViewModel.this.getGuildId() != null) {
                StoreGatewayConnection.requestGuildMembers$default(StoreStream.Companion.getGatewaySocket(), WidgetApplicationCommandBottomSheetViewModel.this.getGuildId().longValue(), null, u.toList(userData.getMentionedUserIds()), null, 10, null);
            }
            if (userData.getUsers().size() != userData.getMentionedUserIds().size()) {
                StoreStream.Companion.getUsers().fetchUsers(u.toList(userData.getMentionedUserIds()));
            }
        }
    }

    /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0019\u0010\u001aJq\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u000e\u0010\u0007\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00062\n\u0010\t\u001a\u00060\u0004j\u0002`\b2\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$Companion;", "", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "", "interactionId", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/UserId;", "userId", "applicationId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreApplicationInteractions;", "storeInteractions", "Lcom/discord/stores/StoreApplicationCommands;", "storeApplicationCommands", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;", "observeStores", "(Lcom/discord/stores/updates/ObservationDeck;JLjava/lang/Long;JJLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreApplicationInteractions;Lcom/discord/stores/StoreApplicationCommands;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStores(ObservationDeck observationDeck, long j, Long l, long j2, long j3, StoreGuilds storeGuilds, StoreChannels storeChannels, StoreUser storeUser, StoreApplicationInteractions storeApplicationInteractions, StoreApplicationCommands storeApplicationCommands) {
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeApplicationCommands, storeChannels, storeApplicationInteractions, storeGuilds, storeUser}, false, null, null, new WidgetApplicationCommandBottomSheetViewModel$Companion$observeStores$1(storeApplicationInteractions, j, storeApplicationCommands, j3, j2, storeGuilds, l, storeUser, storeChannels), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J:\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\r\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0017\u001a\u0004\b\u0018\u0010\bR\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001c\u0010\u0004¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$SlashCommandParam;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()Ljava/lang/Integer;", "component4", ModelAuditLogEntry.CHANGE_KEY_NAME, "value", "valueColor", "copyText", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$SlashCommandParam;", "toString", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getValueColor", "Ljava/lang/String;", "getName", "getCopyText", "getValue", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SlashCommandParam {
        private final String copyText;
        private final String name;
        private final String value;
        private final Integer valueColor;

        public SlashCommandParam(String str, String str2, Integer num, String str3) {
            a.n0(str, ModelAuditLogEntry.CHANGE_KEY_NAME, str2, "value", str3, "copyText");
            this.name = str;
            this.value = str2;
            this.valueColor = num;
            this.copyText = str3;
        }

        public static /* synthetic */ SlashCommandParam copy$default(SlashCommandParam slashCommandParam, String str, String str2, Integer num, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = slashCommandParam.name;
            }
            if ((i & 2) != 0) {
                str2 = slashCommandParam.value;
            }
            if ((i & 4) != 0) {
                num = slashCommandParam.valueColor;
            }
            if ((i & 8) != 0) {
                str3 = slashCommandParam.copyText;
            }
            return slashCommandParam.copy(str, str2, num, str3);
        }

        public final String component1() {
            return this.name;
        }

        public final String component2() {
            return this.value;
        }

        public final Integer component3() {
            return this.valueColor;
        }

        public final String component4() {
            return this.copyText;
        }

        public final SlashCommandParam copy(String str, String str2, Integer num, String str3) {
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
            m.checkNotNullParameter(str2, "value");
            m.checkNotNullParameter(str3, "copyText");
            return new SlashCommandParam(str, str2, num, str3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SlashCommandParam)) {
                return false;
            }
            SlashCommandParam slashCommandParam = (SlashCommandParam) obj;
            return m.areEqual(this.name, slashCommandParam.name) && m.areEqual(this.value, slashCommandParam.value) && m.areEqual(this.valueColor, slashCommandParam.valueColor) && m.areEqual(this.copyText, slashCommandParam.copyText);
        }

        public final String getCopyText() {
            return this.copyText;
        }

        public final String getName() {
            return this.name;
        }

        public final String getValue() {
            return this.value;
        }

        public final Integer getValueColor() {
            return this.valueColor;
        }

        public int hashCode() {
            String str = this.name;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.value;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Integer num = this.valueColor;
            int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
            String str3 = this.copyText;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("SlashCommandParam(name=");
            R.append(this.name);
            R.append(", value=");
            R.append(this.value);
            R.append(", valueColor=");
            R.append(this.valueColor);
            R.append(", copyText=");
            return a.H(R, this.copyText, ")");
        }
    }

    /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001B·\u0001\u0012\b\u0010 \u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\"\u001a\u0004\u0018\u00010\b\u0012\b\u0010#\u001a\u0004\u0018\u00010\u000b\u0012\f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0018\u0010%\u001a\u0014\u0012\b\u0012\u00060\u000fj\u0002`\u0013\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0012\u0012\u0018\u0010&\u001a\u0014\u0012\b\u0012\u00060\u000fj\u0002`\u0016\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0012\u0012\u0016\u0010'\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0013\u0012\u0004\u0012\u00020\u00020\u0012\u0012\u0016\u0010(\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u001a\u0012\u0004\u0012\u00020\u001b0\u0012\u0012\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u0012¢\u0006\u0004\bE\u0010FJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0016\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\"\u0010\u0014\u001a\u0014\u0012\b\u0012\u00060\u000fj\u0002`\u0013\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\"\u0010\u0018\u001a\u0014\u0012\b\u0012\u00060\u000fj\u0002`\u0016\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0015J \u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0013\u0012\u0004\u0012\u00020\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0015J \u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u001a\u0012\u0004\u0012\u00020\u001b0\u0012HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0015J\u001c\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u0012HÆ\u0003¢\u0006\u0004\b\u001f\u0010\u0015JÒ\u0001\u0010*\u001a\u00020\u00002\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u000b2\u000e\b\u0002\u0010$\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u001a\b\u0002\u0010%\u001a\u0014\u0012\b\u0012\u00060\u000fj\u0002`\u0013\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00122\u001a\b\u0002\u0010&\u001a\u0014\u0012\b\u0012\u00060\u000fj\u0002`\u0016\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u00122\u0018\b\u0002\u0010'\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0013\u0012\u0004\u0012\u00020\u00020\u00122\u0018\b\u0002\u0010(\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u001a\u0012\u0004\u0012\u00020\u001b0\u00122\u0014\b\u0002\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u0012HÆ\u0001¢\u0006\u0004\b*\u0010+J\u0010\u0010,\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b,\u0010-J\u0010\u0010/\u001a\u00020.HÖ\u0001¢\u0006\u0004\b/\u00100J\u001a\u00103\u001a\u0002022\b\u00101\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b3\u00104R%\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u00128\u0006@\u0006¢\u0006\f\n\u0004\b)\u00105\u001a\u0004\b6\u0010\u0015R\u001b\u0010!\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b!\u00107\u001a\u0004\b8\u0010\u0007R\u001b\u0010#\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00109\u001a\u0004\b:\u0010\rR+\u0010&\u001a\u0014\u0012\b\u0012\u00060\u000fj\u0002`\u0016\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b&\u00105\u001a\u0004\b;\u0010\u0015R+\u0010%\u001a\u0014\u0012\b\u0012\u00060\u000fj\u0002`\u0013\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b%\u00105\u001a\u0004\b<\u0010\u0015R\u001f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010=\u001a\u0004\b>\u0010\u0011R\u001b\u0010\"\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010?\u001a\u0004\b@\u0010\nR)\u0010'\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0013\u0012\u0004\u0012\u00020\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b'\u00105\u001a\u0004\bA\u0010\u0015R)\u0010(\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u001a\u0012\u0004\u0012\u00020\u001b0\u00128\u0006@\u0006¢\u0006\f\n\u0004\b(\u00105\u001a\u0004\bB\u0010\u0015R\u001b\u0010 \u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u0010C\u001a\u0004\bD\u0010\u0004¨\u0006G"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;", "", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/models/member/GuildMember;", "component2", "()Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/StoreApplicationInteractions$State;", "component3", "()Lcom/discord/stores/StoreApplicationInteractions$State;", "Lcom/discord/models/commands/Application;", "component4", "()Lcom/discord/models/commands/Application;", "", "", "component5", "()Ljava/util/Set;", "", "Lcom/discord/primitives/UserId;", "component6", "()Ljava/util/Map;", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component7", "component8", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component9", "", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$SlashCommandParam;", "component10", "user", "interactionUser", "interactionState", "application", "mentionedUsers", "guildMembers", "guildRoles", "users", "channels", "commandValues", "copy", "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;Lcom/discord/stores/StoreApplicationInteractions$State;Lcom/discord/models/commands/Application;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getCommandValues", "Lcom/discord/models/member/GuildMember;", "getInteractionUser", "Lcom/discord/models/commands/Application;", "getApplication", "getGuildRoles", "getGuildMembers", "Ljava/util/Set;", "getMentionedUsers", "Lcom/discord/stores/StoreApplicationInteractions$State;", "getInteractionState", "getUsers", "getChannels", "Lcom/discord/models/user/User;", "getUser", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;Lcom/discord/stores/StoreApplicationInteractions$State;Lcom/discord/models/commands/Application;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Application application;
        private final Map<Long, Channel> channels;
        private final Map<String, SlashCommandParam> commandValues;
        private final Map<Long, GuildMember> guildMembers;
        private final Map<Long, GuildRole> guildRoles;
        private final StoreApplicationInteractions.State interactionState;
        private final GuildMember interactionUser;
        private final Set<Long> mentionedUsers;
        private final User user;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(User user, GuildMember guildMember, StoreApplicationInteractions.State state, Application application, Set<Long> set, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, ? extends User> map3, Map<Long, Channel> map4, Map<String, SlashCommandParam> map5) {
            m.checkNotNullParameter(set, "mentionedUsers");
            m.checkNotNullParameter(map3, "users");
            m.checkNotNullParameter(map4, "channels");
            m.checkNotNullParameter(map5, "commandValues");
            this.user = user;
            this.interactionUser = guildMember;
            this.interactionState = state;
            this.application = application;
            this.mentionedUsers = set;
            this.guildMembers = map;
            this.guildRoles = map2;
            this.users = map3;
            this.channels = map4;
            this.commandValues = map5;
        }

        public final User component1() {
            return this.user;
        }

        public final Map<String, SlashCommandParam> component10() {
            return this.commandValues;
        }

        public final GuildMember component2() {
            return this.interactionUser;
        }

        public final StoreApplicationInteractions.State component3() {
            return this.interactionState;
        }

        public final Application component4() {
            return this.application;
        }

        public final Set<Long> component5() {
            return this.mentionedUsers;
        }

        public final Map<Long, GuildMember> component6() {
            return this.guildMembers;
        }

        public final Map<Long, GuildRole> component7() {
            return this.guildRoles;
        }

        public final Map<Long, User> component8() {
            return this.users;
        }

        public final Map<Long, Channel> component9() {
            return this.channels;
        }

        public final StoreState copy(User user, GuildMember guildMember, StoreApplicationInteractions.State state, Application application, Set<Long> set, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, ? extends User> map3, Map<Long, Channel> map4, Map<String, SlashCommandParam> map5) {
            m.checkNotNullParameter(set, "mentionedUsers");
            m.checkNotNullParameter(map3, "users");
            m.checkNotNullParameter(map4, "channels");
            m.checkNotNullParameter(map5, "commandValues");
            return new StoreState(user, guildMember, state, application, set, map, map2, map3, map4, map5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.user, storeState.user) && m.areEqual(this.interactionUser, storeState.interactionUser) && m.areEqual(this.interactionState, storeState.interactionState) && m.areEqual(this.application, storeState.application) && m.areEqual(this.mentionedUsers, storeState.mentionedUsers) && m.areEqual(this.guildMembers, storeState.guildMembers) && m.areEqual(this.guildRoles, storeState.guildRoles) && m.areEqual(this.users, storeState.users) && m.areEqual(this.channels, storeState.channels) && m.areEqual(this.commandValues, storeState.commandValues);
        }

        public final Application getApplication() {
            return this.application;
        }

        public final Map<Long, Channel> getChannels() {
            return this.channels;
        }

        public final Map<String, SlashCommandParam> getCommandValues() {
            return this.commandValues;
        }

        public final Map<Long, GuildMember> getGuildMembers() {
            return this.guildMembers;
        }

        public final Map<Long, GuildRole> getGuildRoles() {
            return this.guildRoles;
        }

        public final StoreApplicationInteractions.State getInteractionState() {
            return this.interactionState;
        }

        public final GuildMember getInteractionUser() {
            return this.interactionUser;
        }

        public final Set<Long> getMentionedUsers() {
            return this.mentionedUsers;
        }

        public final User getUser() {
            return this.user;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            User user = this.user;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            GuildMember guildMember = this.interactionUser;
            int hashCode2 = (hashCode + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
            StoreApplicationInteractions.State state = this.interactionState;
            int hashCode3 = (hashCode2 + (state != null ? state.hashCode() : 0)) * 31;
            Application application = this.application;
            int hashCode4 = (hashCode3 + (application != null ? application.hashCode() : 0)) * 31;
            Set<Long> set = this.mentionedUsers;
            int hashCode5 = (hashCode4 + (set != null ? set.hashCode() : 0)) * 31;
            Map<Long, GuildMember> map = this.guildMembers;
            int hashCode6 = (hashCode5 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map2 = this.guildRoles;
            int hashCode7 = (hashCode6 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, User> map3 = this.users;
            int hashCode8 = (hashCode7 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Map<Long, Channel> map4 = this.channels;
            int hashCode9 = (hashCode8 + (map4 != null ? map4.hashCode() : 0)) * 31;
            Map<String, SlashCommandParam> map5 = this.commandValues;
            if (map5 != null) {
                i = map5.hashCode();
            }
            return hashCode9 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(user=");
            R.append(this.user);
            R.append(", interactionUser=");
            R.append(this.interactionUser);
            R.append(", interactionState=");
            R.append(this.interactionState);
            R.append(", application=");
            R.append(this.application);
            R.append(", mentionedUsers=");
            R.append(this.mentionedUsers);
            R.append(", guildMembers=");
            R.append(this.guildMembers);
            R.append(", guildRoles=");
            R.append(this.guildRoles);
            R.append(", users=");
            R.append(this.users);
            R.append(", channels=");
            R.append(this.channels);
            R.append(", commandValues=");
            return a.L(R, this.commandValues, ")");
        }

        public /* synthetic */ StoreState(User user, GuildMember guildMember, StoreApplicationInteractions.State state, Application application, Set set, Map map, Map map2, Map map3, Map map4, Map map5, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(user, (i & 2) != 0 ? null : guildMember, state, application, set, map, map2, map3, map4, map5);
        }
    }

    /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\u0010\u0010\u000b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002\u0012\u0016\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J \u0010\t\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ>\u0010\r\u001a\u00020\u00002\u0012\b\u0002\u0010\u000b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00022\u0018\b\u0002\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R)\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\b0\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\nR#\u0010\u000b\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0006¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$UserData;", "", "", "", "Lcom/discord/primitives/UserId;", "component1", "()Ljava/util/Set;", "", "Lcom/discord/models/user/User;", "component2", "()Ljava/util/Map;", "mentionedUserIds", "users", "copy", "(Ljava/util/Set;Ljava/util/Map;)Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$UserData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getUsers", "Ljava/util/Set;", "getMentionedUserIds", HookHelper.constructorName, "(Ljava/util/Set;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UserData {
        private final Set<Long> mentionedUserIds;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public UserData(Set<Long> set, Map<Long, ? extends User> map) {
            m.checkNotNullParameter(set, "mentionedUserIds");
            m.checkNotNullParameter(map, "users");
            this.mentionedUserIds = set;
            this.users = map;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ UserData copy$default(UserData userData, Set set, Map map, int i, Object obj) {
            if ((i & 1) != 0) {
                set = userData.mentionedUserIds;
            }
            if ((i & 2) != 0) {
                map = userData.users;
            }
            return userData.copy(set, map);
        }

        public final Set<Long> component1() {
            return this.mentionedUserIds;
        }

        public final Map<Long, User> component2() {
            return this.users;
        }

        public final UserData copy(Set<Long> set, Map<Long, ? extends User> map) {
            m.checkNotNullParameter(set, "mentionedUserIds");
            m.checkNotNullParameter(map, "users");
            return new UserData(set, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UserData)) {
                return false;
            }
            UserData userData = (UserData) obj;
            return m.areEqual(this.mentionedUserIds, userData.mentionedUserIds) && m.areEqual(this.users, userData.users);
        }

        public final Set<Long> getMentionedUserIds() {
            return this.mentionedUserIds;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            Set<Long> set = this.mentionedUserIds;
            int i = 0;
            int hashCode = (set != null ? set.hashCode() : 0) * 31;
            Map<Long, User> map = this.users;
            if (map != null) {
                i = map.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("UserData(mentionedUserIds=");
            R.append(this.mentionedUserIds);
            R.append(", users=");
            return a.L(R, this.users, ")");
        }
    }

    /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Failed", "Loaded", "Loading", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Failed;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Failed;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Failed extends ViewState {
            public static final Failed INSTANCE = new Failed();

            private Failed() {
                super(null);
            }
        }

        /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001BÏ\u0001\u0012\u0006\u0010\u001c\u001a\u00020\u0003\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f\u0012\b\u00100\u001a\u0004\u0018\u00010\u0015\u0012\b\u0010,\u001a\u0004\u0018\u00010+\u0012\u0006\u0010'\u001a\u00020&\u0012\u000e\u0010\u000b\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\n\u0012\u000e\u00104\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u0004\u0012\u0018\u0010\u0016\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0014\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0002\u0012\u0018\u0010\"\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002` \u0012\u0004\u0012\u00020!\u0018\u00010\u0002\u0012\u0018\u0010\u0006\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0002\u0012\u0016\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0014\u0012\u0004\u0012\u00020\u00180\u0002\u0012\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u0002¢\u0006\u0004\b6\u00107R+\u0010\u0006\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR!\u0010\u000b\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R+\u0010\u0016\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0014\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0007\u001a\u0004\b\u0017\u0010\tR%\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0007\u001a\u0004\b\u001b\u0010\tR\u0019\u0010\u001c\u001a\u00020\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR+\u0010\"\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002` \u0012\u0004\u0012\u00020!\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010\u0007\u001a\u0004\b#\u0010\tR)\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0014\u0012\u0004\u0012\u00020\u00180\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u0007\u001a\u0004\b%\u0010\tR\u0019\u0010'\u001a\u00020&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001b\u0010,\u001a\u0004\u0018\u00010+8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/R\u001b\u00100\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R!\u00104\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00048\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010\f\u001a\u0004\b5\u0010\u000e¨\u00068"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "channels", "Ljava/util/Map;", "getChannels", "()Ljava/util/Map;", "Lcom/discord/primitives/GuildId;", "guildId", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "Lcom/discord/models/user/User;", "user", "Lcom/discord/models/user/User;", "getUser", "()Lcom/discord/models/user/User;", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "guildMembers", "getGuildMembers", "", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$SlashCommandParam;", "commandValues", "getCommandValues", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "getId", "()J", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "getGuildRoles", "usernamesOrNicks", "getUsernamesOrNicks", "Lcom/discord/api/commands/ApplicationCommandData;", "applicationCommandData", "Lcom/discord/api/commands/ApplicationCommandData;", "getApplicationCommandData", "()Lcom/discord/api/commands/ApplicationCommandData;", "Lcom/discord/models/commands/Application;", "application", "Lcom/discord/models/commands/Application;", "getApplication", "()Lcom/discord/models/commands/Application;", "interactionUser", "Lcom/discord/models/member/GuildMember;", "getInteractionUser", "()Lcom/discord/models/member/GuildMember;", "channelId", "getChannelId", HookHelper.constructorName, "(JLcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;Lcom/discord/models/commands/Application;Lcom/discord/api/commands/ApplicationCommandData;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final Application application;
            private final ApplicationCommandData applicationCommandData;
            private final Long channelId;
            private final Map<Long, Channel> channels;
            private final Map<String, SlashCommandParam> commandValues;
            private final Long guildId;
            private final Map<Long, GuildMember> guildMembers;
            private final Map<Long, GuildRole> guildRoles;

            /* renamed from: id  reason: collision with root package name */
            private final long f2826id;
            private final GuildMember interactionUser;
            private final User user;
            private final Map<Long, String> usernamesOrNicks;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(long j, User user, GuildMember guildMember, Application application, ApplicationCommandData applicationCommandData, Long l, Long l2, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, Channel> map3, Map<Long, String> map4, Map<String, SlashCommandParam> map5) {
                super(null);
                m.checkNotNullParameter(applicationCommandData, "applicationCommandData");
                m.checkNotNullParameter(map4, "usernamesOrNicks");
                m.checkNotNullParameter(map5, "commandValues");
                this.f2826id = j;
                this.user = user;
                this.interactionUser = guildMember;
                this.application = application;
                this.applicationCommandData = applicationCommandData;
                this.guildId = l;
                this.channelId = l2;
                this.guildMembers = map;
                this.guildRoles = map2;
                this.channels = map3;
                this.usernamesOrNicks = map4;
                this.commandValues = map5;
            }

            public final Application getApplication() {
                return this.application;
            }

            public final ApplicationCommandData getApplicationCommandData() {
                return this.applicationCommandData;
            }

            public final Long getChannelId() {
                return this.channelId;
            }

            public final Map<Long, Channel> getChannels() {
                return this.channels;
            }

            public final Map<String, SlashCommandParam> getCommandValues() {
                return this.commandValues;
            }

            public final Long getGuildId() {
                return this.guildId;
            }

            public final Map<Long, GuildMember> getGuildMembers() {
                return this.guildMembers;
            }

            public final Map<Long, GuildRole> getGuildRoles() {
                return this.guildRoles;
            }

            public final long getId() {
                return this.f2826id;
            }

            public final GuildMember getInteractionUser() {
                return this.interactionUser;
            }

            public final User getUser() {
                return this.user;
            }

            public final Map<Long, String> getUsernamesOrNicks() {
                return this.usernamesOrNicks;
            }
        }

        /* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetApplicationCommandBottomSheetViewModel(long r16, long r18, long r20, java.lang.Long r22, long r23, long r25, java.lang.String r27, rx.Observable r28, int r29, kotlin.jvm.internal.DefaultConstructorMarker r30) {
        /*
            r15 = this;
            r0 = r29
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L30
            com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel$Companion r1 = com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel.Companion
            com.discord.stores.updates.ObservationDeck r2 = com.discord.stores.updates.ObservationDeckProvider.get()
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r10 = r0.getGuilds()
            com.discord.stores.StoreChannels r11 = r0.getChannels()
            com.discord.stores.StoreUser r12 = r0.getUsers()
            com.discord.stores.StoreApplicationInteractions r13 = r0.getInteractions()
            com.discord.stores.StoreApplicationCommands r14 = r0.getApplicationCommands()
            r3 = r16
            r5 = r22
            r6 = r23
            r8 = r25
            rx.Observable r0 = com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel.Companion.access$observeStores(r1, r2, r3, r5, r6, r8, r10, r11, r12, r13, r14)
            r14 = r0
            goto L32
        L30:
            r14 = r28
        L32:
            r1 = r15
            r2 = r16
            r4 = r18
            r6 = r20
            r8 = r22
            r9 = r23
            r11 = r25
            r13 = r27
            r1.<init>(r2, r4, r6, r8, r9, r11, r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel.<init>(long, long, long, java.lang.Long, long, long, java.lang.String, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        String str;
        StoreApplicationInteractions.State interactionState = storeState.getInteractionState();
        if (m.areEqual(interactionState, StoreApplicationInteractions.State.Failure.INSTANCE)) {
            updateViewState(ViewState.Failed.INSTANCE);
        } else if (m.areEqual(interactionState, StoreApplicationInteractions.State.Fetching.INSTANCE)) {
            updateViewState(ViewState.Loading.INSTANCE);
        } else if (interactionState instanceof StoreApplicationInteractions.State.Loaded) {
            if (storeState.getMentionedUsers().size() == storeState.getUsers().size()) {
                int size = storeState.getMentionedUsers().size();
                Map<Long, GuildMember> guildMembers = storeState.getGuildMembers();
                if (guildMembers != null && size == guildMembers.size()) {
                    LinkedHashMap linkedHashMap = new LinkedHashMap();
                    for (Number number : storeState.getMentionedUsers()) {
                        long longValue = number.longValue();
                        GuildMember guildMember = storeState.getGuildMembers().get(Long.valueOf(longValue));
                        if (guildMember == null || (str = guildMember.getNick()) == null) {
                            User user = storeState.getUsers().get(Long.valueOf(longValue));
                            str = user != null ? user.getUsername() : null;
                        }
                        if (str == null) {
                            str = "";
                        }
                        linkedHashMap.put(Long.valueOf(longValue), str);
                    }
                    updateViewState(new ViewState.Loaded(this.interactionId, storeState.getUser(), storeState.getInteractionUser(), storeState.getApplication(), ((StoreApplicationInteractions.State.Loaded) storeState.getInteractionState()).getCommandOptions(), this.guildId, Long.valueOf(this.channelId), storeState.getGuildMembers(), storeState.getGuildRoles(), storeState.getChannels(), linkedHashMap, storeState.getCommandValues()));
                    return;
                }
            }
            updateViewState(ViewState.Loading.INSTANCE);
        }
    }

    private final void requestInteractionData() {
        StoreStream.Companion.getInteractions().fetchInteractionDataIfNonExisting(this.interactionId, this.channelId, this.messageId, this.messageNonce);
    }

    public final long getApplicationId() {
        return this.applicationId;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final Long getGuildId() {
        return this.guildId;
    }

    public final long getInteractionId() {
        return this.interactionId;
    }

    public final long getInteractionUserId() {
        return this.interactionUserId;
    }

    public final long getMessageId() {
        return this.messageId;
    }

    public final String getMessageNonce() {
        return this.messageNonce;
    }

    public final void retry() {
        requestInteractionData();
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetApplicationCommandBottomSheetViewModel(long j, long j2, long j3, Long l, long j4, long j5, String str, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(observable, "storeObservable");
        this.interactionId = j;
        this.messageId = j2;
        this.channelId = j3;
        this.guildId = l;
        this.interactionUserId = j4;
        this.applicationId = j5;
        this.messageNonce = str;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetApplicationCommandBottomSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
        requestInteractionData();
        Observable q = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null).F(AnonymousClass2.INSTANCE).q();
        m.checkNotNullExpressionValue(q, "storeObservable.computat…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, WidgetApplicationCommandBottomSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass3());
    }
}
