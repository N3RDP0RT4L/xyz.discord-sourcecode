package com.discord.widgets.chat.list.sheet;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetApplicationCommandBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetApplicationCommandBottomSheet$viewModel$2 extends o implements Function0<AppViewModel<WidgetApplicationCommandBottomSheetViewModel.ViewState>> {
    public final /* synthetic */ WidgetApplicationCommandBottomSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetApplicationCommandBottomSheet$viewModel$2(WidgetApplicationCommandBottomSheet widgetApplicationCommandBottomSheet) {
        super(0);
        this.this$0 = widgetApplicationCommandBottomSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetApplicationCommandBottomSheetViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        Bundle argumentsOrDefault3;
        Bundle argumentsOrDefault4;
        Bundle argumentsOrDefault5;
        Bundle argumentsOrDefault6;
        Bundle argumentsOrDefault7;
        Bundle arguments;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        long j = argumentsOrDefault.getLong("com.discord.intent.extra.EXTRA_INTERACTION_ID");
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        Long l = null;
        if (argumentsOrDefault2.containsKey("com.discord.intent.extra.EXTRA_GUILD_ID") && (arguments = this.this$0.getArguments()) != null) {
            l = Long.valueOf(arguments.getLong("com.discord.intent.extra.EXTRA_GUILD_ID"));
        }
        argumentsOrDefault3 = this.this$0.getArgumentsOrDefault();
        long j2 = argumentsOrDefault3.getLong("com.discord.intent.extra.EXTRA_MESSAGE_ID");
        argumentsOrDefault4 = this.this$0.getArgumentsOrDefault();
        long j3 = argumentsOrDefault4.getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID");
        argumentsOrDefault5 = this.this$0.getArgumentsOrDefault();
        long j4 = argumentsOrDefault5.getLong("com.discord.intent.extra.EXTRA_USER_ID");
        argumentsOrDefault6 = this.this$0.getArgumentsOrDefault();
        long j5 = argumentsOrDefault6.getLong("com.discord.intent.extra.EXTRA_APPLICATION_ID");
        argumentsOrDefault7 = this.this$0.getArgumentsOrDefault();
        return new WidgetApplicationCommandBottomSheetViewModel(j, j2, j3, l, j4, j5, argumentsOrDefault7.getString(WidgetApplicationCommandBottomSheet.ARG_MESSAGE_NONCE), null, 128, null);
    }
}
