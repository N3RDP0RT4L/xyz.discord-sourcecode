package com.discord.widgets.chat.list.sheet;

import com.discord.api.commands.ApplicationCommandValue;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetApplicationCommandBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/commands/ApplicationCommandValue;", "it", "", "invoke", "(Lcom/discord/api/commands/ApplicationCommandValue;)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetApplicationCommandBottomSheetKt$toSlashCommandCopyString$1 extends o implements Function1<ApplicationCommandValue, CharSequence> {
    public final /* synthetic */ Map $commandValues;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetApplicationCommandBottomSheetKt$toSlashCommandCopyString$1(Map map) {
        super(1);
        this.$commandValues = map;
    }

    public final CharSequence invoke(ApplicationCommandValue applicationCommandValue) {
        m.checkNotNullParameter(applicationCommandValue, "it");
        return WidgetApplicationCommandBottomSheetKt.toSlashCommandCopyString(applicationCommandValue, this.$commandValues);
    }
}
