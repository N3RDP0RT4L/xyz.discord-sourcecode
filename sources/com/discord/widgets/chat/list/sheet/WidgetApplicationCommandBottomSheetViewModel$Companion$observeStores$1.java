package com.discord.widgets.chat.list.sheet;

import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.commands.ApplicationCommandType;
import com.discord.api.commands.ApplicationCommandValue;
import com.discord.api.role.GuildRole;
import com.discord.models.commands.Application;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreApplicationCommands;
import com.discord.stores.StoreApplicationInteractions;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreUser;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.chat.list.sheet.WidgetApplicationCommandBottomSheetViewModel;
import d0.g0.s;
import d0.t.n0;
import d0.z.d.o;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetApplicationCommandBottomSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;", "invoke", "()Lcom/discord/widgets/chat/list/sheet/WidgetApplicationCommandBottomSheetViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetApplicationCommandBottomSheetViewModel$Companion$observeStores$1 extends o implements Function0<WidgetApplicationCommandBottomSheetViewModel.StoreState> {
    public final /* synthetic */ long $applicationId;
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ long $interactionId;
    public final /* synthetic */ StoreApplicationCommands $storeApplicationCommands;
    public final /* synthetic */ StoreChannels $storeChannels;
    public final /* synthetic */ StoreGuilds $storeGuilds;
    public final /* synthetic */ StoreApplicationInteractions $storeInteractions;
    public final /* synthetic */ StoreUser $storeUsers;
    public final /* synthetic */ long $userId;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetApplicationCommandBottomSheetViewModel$Companion$observeStores$1(StoreApplicationInteractions storeApplicationInteractions, long j, StoreApplicationCommands storeApplicationCommands, long j2, long j3, StoreGuilds storeGuilds, Long l, StoreUser storeUser, StoreChannels storeChannels) {
        super(0);
        this.$storeInteractions = storeApplicationInteractions;
        this.$interactionId = j;
        this.$storeApplicationCommands = storeApplicationCommands;
        this.$applicationId = j2;
        this.$userId = j3;
        this.$storeGuilds = storeGuilds;
        this.$guildId = l;
        this.$storeUsers = storeUser;
        this.$storeChannels = storeChannels;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetApplicationCommandBottomSheetViewModel.StoreState invoke() {
        LinkedHashMap linkedHashMap;
        LinkedHashMap linkedHashMap2;
        List<ApplicationCommandValue> b2;
        List<ApplicationCommandValue> flattenOptions;
        Iterator it;
        String str;
        String stringTruncateZeroDecimal;
        Long l;
        StoreApplicationInteractions.State interactionData = this.$storeInteractions.getInteractionData(this.$interactionId);
        Application application = this.$storeApplicationCommands.getApplicationMap().get(Long.valueOf(this.$applicationId));
        LinkedHashMap linkedHashMap3 = new LinkedHashMap();
        Set<Number> mutableSetOf = n0.mutableSetOf(Long.valueOf(this.$userId));
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        LinkedHashMap linkedHashMap4 = new LinkedHashMap();
        if (!(!(interactionData instanceof StoreApplicationInteractions.State.Loaded) || (b2 = ((StoreApplicationInteractions.State.Loaded) interactionData).getCommandOptions().b()) == null || (flattenOptions = WidgetApplicationCommandBottomSheetViewModelKt.flattenOptions(b2)) == null)) {
            for (Iterator it2 = flattenOptions.iterator(); it2.hasNext(); it2 = it) {
                ApplicationCommandValue applicationCommandValue = (ApplicationCommandValue) it2.next();
                if (applicationCommandValue.c() == ApplicationCommandType.USER.getType()) {
                    Long longOrNull = s.toLongOrNull(String.valueOf(applicationCommandValue.d()));
                    if (longOrNull != null) {
                        mutableSetOf.add(longOrNull);
                        Map<Long, GuildMember> map = this.$storeGuilds.getMembers().get(this.$guildId);
                        GuildMember guildMember = map != null ? map.get(longOrNull) : null;
                        User user = this.$storeUsers.getUsers().get(longOrNull);
                        String a = applicationCommandValue.a();
                        String a2 = applicationCommandValue.a();
                        String valueOf = String.valueOf(user != null ? user.getUsername() : null);
                        Integer valueOf2 = guildMember != null ? Integer.valueOf(guildMember.getColor()) : null;
                        StringBuilder O = a.O(MentionUtilsKt.MENTIONS_CHAR);
                        it = it2;
                        O.append(user != null ? user.getUsername() : null);
                        O.append(MentionUtilsKt.CHANNELS_CHAR);
                        O.append(user != null ? Integer.valueOf(user.getDiscriminator()) : null);
                        linkedHashMap3.put(a, new WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam(a2, valueOf, valueOf2, O.toString()));
                    } else {
                        it = it2;
                    }
                } else {
                    it = it2;
                    if (applicationCommandValue.c() == ApplicationCommandType.ROLE.getType()) {
                        Long longOrNull2 = s.toLongOrNull(String.valueOf(applicationCommandValue.d()));
                        if (longOrNull2 != null) {
                            Map<Long, GuildRole> map2 = this.$storeGuilds.getRoles().get(this.$guildId);
                            GuildRole guildRole = map2 != null ? map2.get(longOrNull2) : null;
                            String a3 = applicationCommandValue.a();
                            String a4 = applicationCommandValue.a();
                            String valueOf3 = String.valueOf(guildRole != null ? guildRole.g() : null);
                            Integer valueOf4 = Integer.valueOf(RoleUtils.getOpaqueColor(guildRole));
                            StringBuilder O2 = a.O(MentionUtilsKt.MENTIONS_CHAR);
                            O2.append(guildRole != null ? guildRole.g() : null);
                            linkedHashMap3.put(a3, new WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam(a4, valueOf3, valueOf4, O2.toString()));
                        }
                    } else if (applicationCommandValue.c() == ApplicationCommandType.MENTIONABLE.getType()) {
                        Long longOrNull3 = s.toLongOrNull(String.valueOf(applicationCommandValue.d()));
                        if (longOrNull3 != null) {
                            Map<Long, GuildRole> map3 = this.$storeGuilds.getRoles().get(this.$guildId);
                            GuildRole guildRole2 = map3 != null ? map3.get(longOrNull3) : null;
                            if (guildRole2 != null) {
                                String a5 = applicationCommandValue.a();
                                String a6 = applicationCommandValue.a();
                                String valueOf5 = String.valueOf(guildRole2.g());
                                Integer valueOf6 = Integer.valueOf(RoleUtils.getOpaqueColor(guildRole2));
                                StringBuilder O3 = a.O(MentionUtilsKt.MENTIONS_CHAR);
                                O3.append(guildRole2.g());
                                linkedHashMap3.put(a5, new WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam(a6, valueOf5, valueOf6, O3.toString()));
                            } else {
                                Map<Long, GuildMember> map4 = this.$storeGuilds.getMembers().get(this.$guildId);
                                GuildMember guildMember2 = map4 != null ? map4.get(longOrNull3) : null;
                                User user2 = this.$storeUsers.getUsers().get(longOrNull3);
                                String a7 = applicationCommandValue.a();
                                String a8 = applicationCommandValue.a();
                                String valueOf7 = String.valueOf(user2 != null ? user2.getUsername() : null);
                                Integer valueOf8 = guildMember2 != null ? Integer.valueOf(guildMember2.getColor()) : null;
                                StringBuilder O4 = a.O(MentionUtilsKt.MENTIONS_CHAR);
                                O4.append(user2 != null ? user2.getUsername() : null);
                                O4.append(MentionUtilsKt.CHANNELS_CHAR);
                                O4.append(user2 != null ? Integer.valueOf(user2.getDiscriminator()) : null);
                                linkedHashMap3.put(a7, new WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam(a8, valueOf7, valueOf8, O4.toString()));
                            }
                        }
                    } else if (applicationCommandValue.c() == ApplicationCommandType.CHANNEL.getType()) {
                        Long longOrNull4 = s.toLongOrNull(String.valueOf(applicationCommandValue.d()));
                        if (!(longOrNull4 == null || (l = this.$guildId) == null)) {
                            Channel channel = this.$storeChannels.getChannelsForGuild(l.longValue()).get(longOrNull4);
                            String a9 = applicationCommandValue.a();
                            String a10 = applicationCommandValue.a();
                            String valueOf9 = String.valueOf(channel != null ? channel.m() : null);
                            StringBuilder O5 = a.O(MentionUtilsKt.CHANNELS_CHAR);
                            O5.append(channel != null ? channel.m() : null);
                            linkedHashMap3.put(a9, new WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam(a10, valueOf9, null, O5.toString()));
                        }
                    } else {
                        String a11 = applicationCommandValue.a();
                        String a12 = applicationCommandValue.a();
                        Object d = applicationCommandValue.d();
                        String str2 = "";
                        if (d == null || (str = WidgetApplicationCommandBottomSheetViewModelKt.toStringTruncateZeroDecimal(d)) == null) {
                            str = str2;
                        }
                        Object d2 = applicationCommandValue.d();
                        if (!(d2 == null || (stringTruncateZeroDecimal = WidgetApplicationCommandBottomSheetViewModelKt.toStringTruncateZeroDecimal(d2)) == null)) {
                            str2 = stringTruncateZeroDecimal;
                        }
                        linkedHashMap3.put(a11, new WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam(a12, str, null, str2));
                    }
                }
                if (linkedHashMap3.get(applicationCommandValue.a()) == null) {
                    linkedHashMap3.put(applicationCommandValue.a(), new WidgetApplicationCommandBottomSheetViewModel.SlashCommandParam(applicationCommandValue.a(), WidgetApplicationCommandBottomSheetViewModelKt.toStringTruncateZeroDecimal(applicationCommandValue.d()), null, WidgetApplicationCommandBottomSheetViewModelKt.toStringTruncateZeroDecimal(applicationCommandValue.d())));
                }
            }
        }
        GuildMember guildMember3 = null;
        Map<Long, GuildMember> map5 = this.$storeGuilds.getMembers().get(this.$guildId);
        if (map5 != null) {
            LinkedHashMap linkedHashMap5 = new LinkedHashMap();
            for (Map.Entry<Long, GuildMember> entry : map5.entrySet()) {
                if (mutableSetOf.contains(entry.getKey())) {
                    linkedHashMap5.put(entry.getKey(), entry.getValue());
                }
            }
            linkedHashMap = linkedHashMap5;
        } else {
            linkedHashMap = null;
        }
        Map<Long, GuildRole> map6 = this.$storeGuilds.getRoles().get(this.$guildId);
        if (map6 != null) {
            LinkedHashMap linkedHashMap6 = new LinkedHashMap();
            for (Map.Entry<Long, GuildRole> entry2 : map6.entrySet()) {
                if (linkedHashSet.contains(entry2.getKey())) {
                    linkedHashMap6.put(entry2.getKey(), entry2.getValue());
                }
            }
            linkedHashMap2 = linkedHashMap6;
        } else {
            linkedHashMap2 = null;
        }
        LinkedHashMap linkedHashMap7 = new LinkedHashMap();
        for (Number number : mutableSetOf) {
            long longValue = number.longValue();
            User user3 = this.$storeUsers.getUsers().get(Long.valueOf(longValue));
            if (user3 != null) {
                linkedHashMap7.put(Long.valueOf(longValue), user3);
            }
        }
        User user4 = this.$storeUsers.getUsers().get(Long.valueOf(this.$userId));
        Map<Long, GuildMember> map7 = this.$storeGuilds.getMembers().get(this.$guildId);
        if (map7 != null) {
            guildMember3 = map7.get(Long.valueOf(this.$userId));
        }
        return new WidgetApplicationCommandBottomSheetViewModel.StoreState(user4, guildMember3, interactionData, application, mutableSetOf, linkedHashMap, linkedHashMap2, linkedHashMap7, linkedHashMap4, linkedHashMap3);
    }
}
