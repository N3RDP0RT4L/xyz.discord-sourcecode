package com.discord.widgets.chat.list;

import andhook.lib.HookHelper;
import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityAssets;
import com.discord.api.activity.ActivityParty;
import com.discord.api.application.Application;
import com.discord.api.message.activity.MessageActivity;
import com.discord.api.message.activity.MessageActivityType;
import com.discord.app.AppLog;
import com.discord.databinding.ViewChatEmbedGameInviteBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserPresence;
import com.discord.utilities.SnowflakeUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.presence.ActivityUtilsKt;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.time.Clock;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.channels.list.WidgetCollapsedUsersListAdapter;
import com.discord.widgets.channels.list.items.CollapsedUser;
import com.discord.widgets.chat.list.ViewEmbedGameInvite;
import com.discord.widgets.chat.list.entries.GameInviteEntry;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.d0.f;
import d0.g0.s;
import d0.t.d0;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.functions.Function6;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func5;
import rx.functions.Func6;
import xyz.discord.R;
/* compiled from: ViewEmbedGameInvite.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u0000 +2\u00020\u0001:\u0002+,B\u0011\b\u0016\u0012\u0006\u0010 \u001a\u00020\u001f¢\u0006\u0004\b!\u0010\"B\u001b\b\u0016\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\b\u0010$\u001a\u0004\u0018\u00010#¢\u0006\u0004\b!\u0010%B#\b\u0016\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\b\u0010$\u001a\u0004\u0018\u00010#\u0012\u0006\u0010'\u001a\u00020&¢\u0006\u0004\b!\u0010(B+\b\u0016\u0012\u0006\u0010 \u001a\u00020\u001f\u0012\b\u0010$\u001a\u0004\u0018\u00010#\u0012\u0006\u0010'\u001a\u00020&\u0012\u0006\u0010)\u001a\u00020&¢\u0006\u0004\b!\u0010*J\u001b\u0010\u0006\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001b\u0010\n\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ#\u0010\r\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u001d\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0010\u0010\u0007R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R6\u0010\u0019\u001a\u0016\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00178\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001e¨\u0006-"}, d2 = {"Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;", "Landroid/widget/LinearLayout;", "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "Lcom/discord/utilities/time/Clock;", "clock", "", "configureUI", "(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/utilities/time/Clock;)V", "", "isDeadInvite", "configureActivityImages", "(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Z)V", "isPartyFull", "onConfigureActionButton", "(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;ZZ)V", "model", "bind", "Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;", "userAdapter", "Lcom/discord/widgets/channels/list/WidgetCollapsedUsersListAdapter;", "Lcom/discord/databinding/ViewChatEmbedGameInviteBinding;", "binding", "Lcom/discord/databinding/ViewChatEmbedGameInviteBinding;", "Lkotlin/Function2;", "Landroid/view/View;", "onActionButtonClick", "Lkotlin/jvm/functions/Function2;", "getOnActionButtonClick", "()Lkotlin/jvm/functions/Function2;", "setOnActionButtonClick", "(Lkotlin/jvm/functions/Function2;)V", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "", "attributeSetId", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "defStyleRes", "(Landroid/content/Context;Landroid/util/AttributeSet;II)V", "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ViewEmbedGameInvite extends LinearLayout {
    public static final Companion Companion = new Companion(null);
    private static final long EMBED_LIFETIME_MILLIS = 7200000;
    private static final long MAX_USERS_SHOWN = 4;
    private final ViewChatEmbedGameInviteBinding binding;
    private Function2<? super View, ? super Model, Unit> onActionButtonClick;
    private final WidgetCollapsedUsersListAdapter userAdapter;

    /* compiled from: ViewEmbedGameInvite.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Companion;", "", "", "EMBED_LIFETIME_MILLIS", "J", "MAX_USERS_SHOWN", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ViewEmbedGameInvite.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\b\u0018\u0000 I2\u00020\u0001:\u0001IBO\u0012\u0006\u0010\u001d\u001a\u00020\t\u0012\n\u0010\u001e\u001a\u00060\u0002j\u0002`\f\u0012\n\u0010\u001f\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010 \u001a\u00020\u0010\u0012\u0006\u0010!\u001a\u00020\u0013\u0012\b\u0010\"\u001a\u0004\u0018\u00010\u0016\u0012\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019¢\u0006\u0004\bG\u0010HJ\u0019\u0010\u0006\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0019\u0010\b\u001a\u00020\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0014\u0010\r\u001a\u00060\u0002j\u0002`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0014\u0010\u000f\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000eJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0016\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJf\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u001d\u001a\u00020\t2\f\b\u0002\u0010\u001e\u001a\u00060\u0002j\u0002`\f2\f\b\u0002\u0010\u001f\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010 \u001a\u00020\u00102\b\b\u0002\u0010!\u001a\u00020\u00132\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00162\u000e\b\u0002\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019HÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010'\u001a\u00020&HÖ\u0001¢\u0006\u0004\b'\u0010(J\u0010\u0010*\u001a\u00020)HÖ\u0001¢\u0006\u0004\b*\u0010+J\u001a\u0010-\u001a\u00020\u00052\b\u0010,\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b-\u0010.R\u0019\u0010/\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102R\u0019\u0010\u001d\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00103\u001a\u0004\b4\u0010\u000bR\u001b\u0010\"\u001a\u0004\u0018\u00010\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00105\u001a\u0004\b6\u0010\u0018R\u0019\u0010 \u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b \u00107\u001a\u0004\b8\u0010\u0012R\u0019\u0010!\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b!\u00109\u001a\u0004\b:\u0010\u0015R\u0019\u0010;\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b;\u00100\u001a\u0004\b;\u00102R\u0019\u0010<\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b<\u00100\u001a\u0004\b<\u00102R\u001a\u0010?\u001a\u00060&j\u0002`=8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b>\u0010(R\u001f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00198\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010@\u001a\u0004\bA\u0010\u001cR\u001d\u0010\u001e\u001a\u00060\u0002j\u0002`\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010B\u001a\u0004\bC\u0010\u000eR\u001f\u0010D\u001a\b\u0012\u0004\u0012\u00020&0\u00198\u0006@\u0006¢\u0006\f\n\u0004\bD\u0010@\u001a\u0004\bE\u0010\u001cR\u001d\u0010\u001f\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010B\u001a\u0004\bF\u0010\u000e¨\u0006J"}, d2 = {"Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "", "", "Lcom/discord/primitives/Timestamp;", "now", "", "isExpiredInvite", "(J)Z", "isDeadInvite", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/primitives/UserId;", "component2", "()J", "component3", "Lcom/discord/api/application/Application;", "component4", "()Lcom/discord/api/application/Application;", "Lcom/discord/api/message/activity/MessageActivity;", "component5", "()Lcom/discord/api/message/activity/MessageActivity;", "Lcom/discord/api/activity/Activity;", "component6", "()Lcom/discord/api/activity/Activity;", "", "Lcom/discord/widgets/channels/list/items/CollapsedUser;", "component7", "()Ljava/util/List;", "meUser", "creatorId", "creationTime", "application", "messageActivity", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "users", "copy", "(Lcom/discord/models/user/MeUser;JJLcom/discord/api/application/Application;Lcom/discord/api/message/activity/MessageActivity;Lcom/discord/api/activity/Activity;Ljava/util/List;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "canJoin", "Z", "getCanJoin", "()Z", "Lcom/discord/models/user/MeUser;", "getMeUser", "Lcom/discord/api/activity/Activity;", "getActivity", "Lcom/discord/api/application/Application;", "getApplication", "Lcom/discord/api/message/activity/MessageActivity;", "getMessageActivity", "isInParty", "isPartyMatch", "Lcom/discord/primitives/ActivityPartyId;", "getPartyId", "partyId", "Ljava/util/List;", "getUsers", "J", "getCreatorId", "gPlayPackageNames", "getGPlayPackageNames", "getCreationTime", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;JJLcom/discord/api/application/Application;Lcom/discord/api/message/activity/MessageActivity;Lcom/discord/api/activity/Activity;Ljava/util/List;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Activity activity;
        private final Application application;
        private final boolean canJoin;
        private final long creationTime;
        private final long creatorId;
        private final List<String> gPlayPackageNames;
        private final boolean isInParty;
        private final boolean isPartyMatch;
        private final MeUser meUser;
        private final MessageActivity messageActivity;
        private final List<CollapsedUser> users;

        /* compiled from: ViewEmbedGameInvite.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b'\u0010(JY\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\t2\u0016\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\f\u0012\u0004\u0012\u00020\r0\u000b2\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0002¢\u0006\u0004\b\u0012\u0010\u0013JK\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\b\u0010\n\u001a\u0004\u0018\u00010\t2\u0016\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\f\u0012\u0004\u0012\u00020\r0\u000bH\u0002¢\u0006\u0004\b\u0016\u0010\u0017JA\u0010\u001b\u001a\u0012\u0012\u0004\u0012\u00020\u00190\u0018j\b\u0012\u0004\u0012\u00020\u0019`\u001a2\u0016\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\f\u0012\u0004\u0012\u00020\r0\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ1\u0010#\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00110\"2\u0006\u0010\u001e\u001a\u00020\u001d2\b\u0010 \u001a\u0004\u0018\u00010\u001f2\b\u0010!\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b#\u0010$J\u001b\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00110\"2\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b%\u0010&¨\u0006)"}, d2 = {"Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model$Companion;", "", "Lcom/discord/models/user/MeUser;", "meUser", "", "Lcom/discord/primitives/Timestamp;", "creationTime", "Lcom/discord/api/message/activity/MessageActivity;", "msgActivity", "Lcom/discord/api/activity/Activity;", "gameActivity", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "userMap", "Lcom/discord/api/application/Application;", "application", "Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "createForShare", "(Lcom/discord/models/user/MeUser;JLcom/discord/api/message/activity/MessageActivity;Lcom/discord/api/activity/Activity;Ljava/util/Map;Lcom/discord/api/application/Application;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "Lcom/discord/widgets/chat/list/entries/GameInviteEntry;", "item", "create", "(Lcom/discord/widgets/chat/list/entries/GameInviteEntry;Lcom/discord/models/user/MeUser;Lcom/discord/api/application/Application;Lcom/discord/api/activity/Activity;Ljava/util/Map;)Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;", "Ljava/util/ArrayList;", "Lcom/discord/widgets/channels/list/items/CollapsedUser;", "Lkotlin/collections/ArrayList;", "createPartyUsers", "(Ljava/util/Map;Lcom/discord/api/activity/Activity;)Ljava/util/ArrayList;", "Lcom/discord/utilities/time/Clock;", "clock", "Landroid/net/Uri;", "shareUri", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "Lrx/Observable;", "getForShare", "(Lcom/discord/utilities/time/Clock;Landroid/net/Uri;Lcom/discord/api/activity/Activity;)Lrx/Observable;", "get", "(Lcom/discord/widgets/chat/list/entries/GameInviteEntry;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Model create(GameInviteEntry gameInviteEntry, MeUser meUser, Application application, Activity activity, Map<Long, ? extends User> map) {
                return new Model(meUser, gameInviteEntry.getAuthorId(), SnowflakeUtils.DISCORD_EPOCH + (gameInviteEntry.getMessageId() >>> 22), application != null ? application : gameInviteEntry.getApplication(), gameInviteEntry.getActivity(), activity, createPartyUsers(map, activity));
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Model createForShare(MeUser meUser, long j, MessageActivity messageActivity, Activity activity, Map<Long, ? extends User> map, Application application) {
                if (application != null) {
                    return new Model(meUser, meUser.getId(), j, application, messageActivity, activity, createPartyUsers(map, activity));
                }
                return null;
            }

            private final ArrayList<CollapsedUser> createPartyUsers(Map<Long, ? extends User> map, Activity activity) {
                ActivityParty i;
                ArrayList<CollapsedUser> arrayList = new ArrayList<>();
                for (User user : map.values()) {
                    arrayList.add(new CollapsedUser(user, false, 0L, 6, null));
                }
                long maxSize = (activity == null || (i = activity.i()) == null) ? 0L : PresenceUtils.INSTANCE.getMaxSize(i);
                Iterator<Long> it = f.until(map.size(), Math.min(4L, maxSize)).iterator();
                while (it.hasNext()) {
                    arrayList.add(CollapsedUser.Companion.createEmptyUser(((d0) it).nextLong() == 3 ? maxSize - 4 : 0L));
                }
                return arrayList;
            }

            public final Observable<Model> get(GameInviteEntry gameInviteEntry) {
                m.checkNotNullParameter(gameInviteEntry, "item");
                k kVar = new k(gameInviteEntry);
                StoreStream.Companion companion = StoreStream.Companion;
                Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
                Observable<Application> observeApplication = companion.getApplication().observeApplication(Long.valueOf(gameInviteEntry.getApplication().g()));
                Observable<Activity> observeApplicationActivity = companion.getPresences().observeApplicationActivity(gameInviteEntry.getAuthorId(), gameInviteEntry.getApplication().g());
                Observable<Map<Long, User>> observeUsersForPartyId = companion.getGameParty().observeUsersForPartyId(gameInviteEntry.getActivity().a());
                final ViewEmbedGameInvite$Model$Companion$get$1 viewEmbedGameInvite$Model$Companion$get$1 = new ViewEmbedGameInvite$Model$Companion$get$1(this);
                Observable g = Observable.g(kVar, observeMe$default, observeApplication, observeApplicationActivity, observeUsersForPartyId, new Func5() { // from class: com.discord.widgets.chat.list.ViewEmbedGameInvite$sam$rx_functions_Func5$0
                    @Override // rx.functions.Func5
                    public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
                        return Function5.this.invoke(obj, obj2, obj3, obj4, obj5);
                    }
                });
                m.checkNotNullExpressionValue(g, "Observable\n            .…   ::create\n            )");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(g).q();
                m.checkNotNullExpressionValue(q, "Observable\n            .…  .distinctUntilChanged()");
                return q;
            }

            public final Observable<Model> getForShare(Clock clock, Uri uri, Activity activity) {
                m.checkNotNullParameter(clock, "clock");
                if (uri != null) {
                    String queryParameter = uri.getQueryParameter(ModelAuditLogEntry.CHANGE_KEY_APPLICATION_ID);
                    final Long longOrNull = queryParameter != null ? s.toLongOrNull(queryParameter) : null;
                    final String queryParameter2 = uri.getQueryParameter("party_id");
                    String queryParameter3 = uri.getQueryParameter("type");
                    Integer intOrNull = queryParameter3 != null ? s.toIntOrNull(queryParameter3) : null;
                    if (longOrNull == null || queryParameter2 == null || intOrNull == null || (!m.areEqual(uri.getPath(), "/send/activity"))) {
                        AppLog appLog = AppLog.g;
                        Logger.w$default(appLog, "Malformed Share URI: " + uri, null, 2, null);
                        k kVar = new k(null);
                        m.checkNotNullExpressionValue(kVar, "Observable.just(null)");
                        return kVar;
                    }
                    MessageActivity messageActivity = new MessageActivity(MessageActivityType.Companion.a(intOrNull), queryParameter2);
                    k kVar2 = new k(activity);
                    StoreStream.Companion companion = StoreStream.Companion;
                    Observable m = Observable.m(kVar2, companion.getUsers().observeMeId().Y(new b<Long, Observable<? extends Activity>>() { // from class: com.discord.widgets.chat.list.ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1
                        public final Observable<? extends Activity> call(Long l) {
                            StoreUserPresence presences = StoreStream.Companion.getPresences();
                            m.checkNotNullExpressionValue(l, "authorId");
                            return presences.observeApplicationActivity(l.longValue(), longOrNull.longValue()).x(new b<Activity, Boolean>() { // from class: com.discord.widgets.chat.list.ViewEmbedGameInvite$Model$Companion$getForShare$activityObs$1.1
                                public final Boolean call(Activity activity2) {
                                    boolean z2;
                                    if (activity2 != null) {
                                        ActivityParty i = activity2.i();
                                        if (m.areEqual(i != null ? i.a() : null, queryParameter2)) {
                                            z2 = true;
                                            return Boolean.valueOf(z2);
                                        }
                                    }
                                    z2 = false;
                                    return Boolean.valueOf(z2);
                                }
                            });
                        }
                    }));
                    Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
                    k kVar3 = new k(Long.valueOf(clock.currentTimeMillis()));
                    k kVar4 = new k(messageActivity);
                    Observable<Map<Long, User>> observeUsersForPartyId = companion.getGameParty().observeUsersForPartyId(messageActivity.a());
                    Observable<Application> observeApplication = companion.getApplication().observeApplication(longOrNull);
                    final ViewEmbedGameInvite$Model$Companion$getForShare$1 viewEmbedGameInvite$Model$Companion$getForShare$1 = new ViewEmbedGameInvite$Model$Companion$getForShare$1(this);
                    Observable f = Observable.f(observeMe$default, kVar3, kVar4, m, observeUsersForPartyId, observeApplication, new Func6() { // from class: com.discord.widgets.chat.list.ViewEmbedGameInvite$sam$rx_functions_Func6$0
                        @Override // rx.functions.Func6
                        public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6) {
                            return Function6.this.invoke(obj, obj2, obj3, obj4, obj5, obj6);
                        }
                    });
                    m.checkNotNullExpressionValue(f, "Observable\n            .…ateForShare\n            )");
                    Observable<Model> q = ObservableExtensionsKt.computationLatest(f).q();
                    m.checkNotNullExpressionValue(q, "Observable\n            .…  .distinctUntilChanged()");
                    return q;
                }
                k kVar5 = new k(null);
                m.checkNotNullExpressionValue(kVar5, "Observable.just(null)");
                return kVar5;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(MeUser meUser, long j, long j2, Application application, MessageActivity messageActivity, Activity activity, List<CollapsedUser> list) {
            boolean z2;
            ActivityParty i;
            String a;
            boolean z3;
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(application, "application");
            m.checkNotNullParameter(messageActivity, "messageActivity");
            m.checkNotNullParameter(list, "users");
            this.meUser = meUser;
            this.creatorId = j;
            this.creationTime = j2;
            this.application = application;
            this.messageActivity = messageActivity;
            this.activity = activity;
            this.users = list;
            boolean z4 = true;
            boolean z5 = false;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                for (CollapsedUser collapsedUser : list) {
                    if (collapsedUser.getUser().getId() == this.meUser.getId()) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        z2 = true;
                        break;
                    }
                }
            }
            z2 = false;
            this.isInParty = z2;
            Activity activity2 = this.activity;
            this.canJoin = (activity2 == null || !ActivityUtilsKt.hasFlag(activity2, 2) || !ActivityUtilsKt.isCurrentPlatform(this.activity)) ? false : z4;
            this.gPlayPackageNames = this.application.d();
            Activity activity3 = this.activity;
            if (!(activity3 == null || (i = activity3.i()) == null || (a = i.a()) == null)) {
                z5 = a.equals(getPartyId());
            }
            this.isPartyMatch = z5;
        }

        private final String getPartyId() {
            return this.messageActivity.a();
        }

        public final MeUser component1() {
            return this.meUser;
        }

        public final long component2() {
            return this.creatorId;
        }

        public final long component3() {
            return this.creationTime;
        }

        public final Application component4() {
            return this.application;
        }

        public final MessageActivity component5() {
            return this.messageActivity;
        }

        public final Activity component6() {
            return this.activity;
        }

        public final List<CollapsedUser> component7() {
            return this.users;
        }

        public final Model copy(MeUser meUser, long j, long j2, Application application, MessageActivity messageActivity, Activity activity, List<CollapsedUser> list) {
            m.checkNotNullParameter(meUser, "meUser");
            m.checkNotNullParameter(application, "application");
            m.checkNotNullParameter(messageActivity, "messageActivity");
            m.checkNotNullParameter(list, "users");
            return new Model(meUser, j, j2, application, messageActivity, activity, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.meUser, model.meUser) && this.creatorId == model.creatorId && this.creationTime == model.creationTime && m.areEqual(this.application, model.application) && m.areEqual(this.messageActivity, model.messageActivity) && m.areEqual(this.activity, model.activity) && m.areEqual(this.users, model.users);
        }

        public final Activity getActivity() {
            return this.activity;
        }

        public final Application getApplication() {
            return this.application;
        }

        public final boolean getCanJoin() {
            return this.canJoin;
        }

        public final long getCreationTime() {
            return this.creationTime;
        }

        public final long getCreatorId() {
            return this.creatorId;
        }

        public final List<String> getGPlayPackageNames() {
            return this.gPlayPackageNames;
        }

        public final MeUser getMeUser() {
            return this.meUser;
        }

        public final MessageActivity getMessageActivity() {
            return this.messageActivity;
        }

        public final List<CollapsedUser> getUsers() {
            return this.users;
        }

        public int hashCode() {
            MeUser meUser = this.meUser;
            int i = 0;
            int hashCode = meUser != null ? meUser.hashCode() : 0;
            int a = (a0.a.a.b.a(this.creationTime) + ((a0.a.a.b.a(this.creatorId) + (hashCode * 31)) * 31)) * 31;
            Application application = this.application;
            int hashCode2 = (a + (application != null ? application.hashCode() : 0)) * 31;
            MessageActivity messageActivity = this.messageActivity;
            int hashCode3 = (hashCode2 + (messageActivity != null ? messageActivity.hashCode() : 0)) * 31;
            Activity activity = this.activity;
            int hashCode4 = (hashCode3 + (activity != null ? activity.hashCode() : 0)) * 31;
            List<CollapsedUser> list = this.users;
            if (list != null) {
                i = list.hashCode();
            }
            return hashCode4 + i;
        }

        public final boolean isDeadInvite(long j) {
            return !this.isPartyMatch || isExpiredInvite(j);
        }

        public final boolean isExpiredInvite(long j) {
            return j > this.creationTime + ViewEmbedGameInvite.EMBED_LIFETIME_MILLIS;
        }

        public final boolean isInParty() {
            return this.isInParty;
        }

        public final boolean isPartyMatch() {
            return this.isPartyMatch;
        }

        public String toString() {
            StringBuilder R = a.R("Model(meUser=");
            R.append(this.meUser);
            R.append(", creatorId=");
            R.append(this.creatorId);
            R.append(", creationTime=");
            R.append(this.creationTime);
            R.append(", application=");
            R.append(this.application);
            R.append(", messageActivity=");
            R.append(this.messageActivity);
            R.append(", activity=");
            R.append(this.activity);
            R.append(", users=");
            return a.K(R, this.users, ")");
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewEmbedGameInvite(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        ViewChatEmbedGameInviteBinding a = ViewChatEmbedGameInviteBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewChatEmbedGameInviteB…ater.from(context), this)");
        this.binding = a;
        setOrientation(1);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = a.h;
        m.checkNotNullExpressionValue(recyclerView, "binding.itemGameInviteRecycler");
        this.userAdapter = (WidgetCollapsedUsersListAdapter) companion.configure(new WidgetCollapsedUsersListAdapter(recyclerView));
    }

    private final void configureActivityImages(Model model, boolean z2) {
        String c;
        SimpleDraweeView simpleDraweeView = this.binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.itemGameInviteAvatarIv");
        String f = model.getApplication().f();
        String str = null;
        MGImages.setImage$default(simpleDraweeView, f != null ? IconUtils.getApplicationIcon$default(model.getApplication().g(), f, 0, 4, (Object) null) : null, 0, 0, false, null, null, 124, null);
        Activity activity = model.getActivity();
        ActivityAssets b2 = activity != null ? activity.b() : null;
        SimpleDraweeView simpleDraweeView2 = this.binding.e;
        m.checkNotNullExpressionValue(simpleDraweeView2, "binding.itemGameInviteAvatarStatusIv");
        simpleDraweeView2.setVisibility((b2 != null ? b2.c() : null) != null ? 0 : 8);
        SimpleDraweeView simpleDraweeView3 = this.binding.e;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.itemGameInviteAvatarStatusIv");
        MGImages.setImage$default(simpleDraweeView3, (b2 == null || (c = b2.c()) == null) ? null : IconUtils.getAssetImage$default(IconUtils.INSTANCE, Long.valueOf(model.getApplication().g()), c, 0, 4, null), 0, 0, false, null, null, 124, null);
        SimpleDraweeView simpleDraweeView4 = this.binding.e;
        m.checkNotNullExpressionValue(simpleDraweeView4, "binding.itemGameInviteAvatarStatusIv");
        simpleDraweeView4.setContentDescription(b2 != null ? b2.d() : null);
        if (z2) {
            SimpleDraweeView simpleDraweeView5 = this.binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView5, "binding.itemGameInviteCoverIv");
            simpleDraweeView5.setVisibility(8);
            SimpleDraweeView simpleDraweeView6 = this.binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView6, "binding.itemGameInviteCoverIv");
            MGImages.setImage$default(simpleDraweeView6, null, 0, 0, false, null, null, 124, null);
            return;
        }
        SimpleDraweeView simpleDraweeView7 = this.binding.f;
        m.checkNotNullExpressionValue(simpleDraweeView7, "binding.itemGameInviteCoverIv");
        simpleDraweeView7.setVisibility(0);
        String a = b2 != null ? b2.a() : null;
        if (a != null) {
            SimpleDraweeView simpleDraweeView8 = this.binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView8, "binding.itemGameInviteCoverIv");
            simpleDraweeView8.setContentDescription(b2.b());
            SimpleDraweeView simpleDraweeView9 = this.binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView9, "binding.itemGameInviteCoverIv");
            simpleDraweeView9.setImportantForAccessibility(1);
            String assetImage = IconUtils.INSTANCE.getAssetImage(Long.valueOf(model.getApplication().g()), a, IconUtils.getMediaProxySize(getWidth()));
            SimpleDraweeView simpleDraweeView10 = this.binding.f;
            m.checkNotNullExpressionValue(simpleDraweeView10, "binding.itemGameInviteCoverIv");
            MGImages.setImage$default(simpleDraweeView10, assetImage, 0, 0, false, null, null, 124, null);
            return;
        }
        SimpleDraweeView simpleDraweeView11 = this.binding.f;
        m.checkNotNullExpressionValue(simpleDraweeView11, "binding.itemGameInviteCoverIv");
        simpleDraweeView11.setImportantForAccessibility(2);
        SimpleDraweeView simpleDraweeView12 = this.binding.f;
        m.checkNotNullExpressionValue(simpleDraweeView12, "binding.itemGameInviteCoverIv");
        simpleDraweeView12.setContentDescription(null);
        String b3 = model.getApplication().b();
        if (b3 != null) {
            str = IconUtils.getApplicationIcon$default(model.getApplication().g(), b3, 0, 4, (Object) null);
        }
        SimpleDraweeView simpleDraweeView13 = this.binding.f;
        m.checkNotNullExpressionValue(simpleDraweeView13, "binding.itemGameInviteCoverIv");
        MGImages.setImage$default(simpleDraweeView13, str, 0, 0, false, null, null, 124, null);
    }

    private final void configureUI(Model model, Clock clock) {
        Activity activity;
        ActivityParty i;
        boolean isDeadInvite = model.isDeadInvite(clock.currentTimeMillis());
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.itemGameInviteApplicationNameTv");
        textView.setText(model.getApplication().h());
        TextView textView2 = this.binding.g;
        int i2 = R.string.invite_embed_game_has_ended;
        if (!isDeadInvite && model.getMessageActivity().b() != MessageActivityType.SPECTATE) {
            i2 = R.string.invite_embed_invite_to_join_group;
        }
        textView2.setText(i2);
        int i3 = 0;
        boolean z2 = ((!isDeadInvite && (activity = model.getActivity()) != null && (i = activity.i()) != null) ? PresenceUtils.INSTANCE.getNumOpenSlots(i) : 0L) <= 0;
        TextView textView3 = this.binding.i;
        m.checkNotNullExpressionValue(textView3, "binding.itemGameInviteSubtext");
        String str = null;
        if (z2) {
            Activity activity2 = model.getActivity();
            if (activity2 != null) {
                str = activity2.e();
            }
        } else {
            Activity activity3 = model.getActivity();
            if (activity3 != null) {
                str = activity3.l();
            }
        }
        ViewExtensions.setTextAndVisibilityBy(textView3, str);
        configureActivityImages(model, isDeadInvite);
        onConfigureActionButton(model, isDeadInvite, z2);
        RecyclerView recyclerView = this.binding.h;
        m.checkNotNullExpressionValue(recyclerView, "binding.itemGameInviteRecycler");
        if (!(!isDeadInvite)) {
            i3 = 8;
        }
        recyclerView.setVisibility(i3);
        if (!isDeadInvite) {
            this.userAdapter.setData(model.getUsers());
        }
    }

    private final void onConfigureActionButton(final Model model, boolean z2, boolean z3) {
        MaterialButton materialButton = this.binding.f2160b;
        m.checkNotNullExpressionValue(materialButton, "binding.itemGameInviteActionBtn");
        boolean z4 = false;
        materialButton.setVisibility(0);
        int i = R.string.join;
        if (z2 || !model.getCanJoin()) {
            MaterialButton materialButton2 = this.binding.f2160b;
            m.checkNotNullExpressionValue(materialButton2, "binding.itemGameInviteActionBtn");
            materialButton2.setEnabled(false);
            this.binding.f2160b.setText(R.string.join);
            return;
        }
        MaterialButton materialButton3 = this.binding.f2160b;
        m.checkNotNullExpressionValue(materialButton3, "binding.itemGameInviteActionBtn");
        if (!model.isInParty() && !z3 && model.getCreatorId() != model.getMeUser().getId()) {
            z4 = true;
        }
        materialButton3.setEnabled(z4);
        MaterialButton materialButton4 = this.binding.f2160b;
        if (z3) {
            i = R.string.invite_embed_full_group;
        } else if (model.isInParty()) {
            i = R.string.invite_embed_joined;
        }
        materialButton4.setText(i);
        this.binding.f2160b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.ViewEmbedGameInvite$onConfigureActionButton$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function2<View, ViewEmbedGameInvite.Model, Unit> onActionButtonClick = ViewEmbedGameInvite.this.getOnActionButtonClick();
                if (onActionButtonClick != null) {
                    m.checkNotNullExpressionValue(view, "it");
                    onActionButtonClick.invoke(view, model);
                }
            }
        });
    }

    public final void bind(Model model, Clock clock) {
        m.checkNotNullParameter(model, "model");
        m.checkNotNullParameter(clock, "clock");
        configureUI(model, clock);
    }

    public final Function2<View, Model, Unit> getOnActionButtonClick() {
        return this.onActionButtonClick;
    }

    public final void setOnActionButtonClick(Function2<? super View, ? super Model, Unit> function2) {
        this.onActionButtonClick = function2;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewEmbedGameInvite(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        ViewChatEmbedGameInviteBinding a = ViewChatEmbedGameInviteBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewChatEmbedGameInviteB…ater.from(context), this)");
        this.binding = a;
        setOrientation(1);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = a.h;
        m.checkNotNullExpressionValue(recyclerView, "binding.itemGameInviteRecycler");
        this.userAdapter = (WidgetCollapsedUsersListAdapter) companion.configure(new WidgetCollapsedUsersListAdapter(recyclerView));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewEmbedGameInvite(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        ViewChatEmbedGameInviteBinding a = ViewChatEmbedGameInviteBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewChatEmbedGameInviteB…ater.from(context), this)");
        this.binding = a;
        setOrientation(1);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = a.h;
        m.checkNotNullExpressionValue(recyclerView, "binding.itemGameInviteRecycler");
        this.userAdapter = (WidgetCollapsedUsersListAdapter) companion.configure(new WidgetCollapsedUsersListAdapter(recyclerView));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ViewEmbedGameInvite(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        m.checkNotNullParameter(context, "context");
        ViewChatEmbedGameInviteBinding a = ViewChatEmbedGameInviteBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewChatEmbedGameInviteB…ater.from(context), this)");
        this.binding = a;
        setOrientation(1);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = a.h;
        m.checkNotNullExpressionValue(recyclerView, "binding.itemGameInviteRecycler");
        this.userAdapter = (WidgetCollapsedUsersListAdapter) companion.configure(new WidgetCollapsedUsersListAdapter(recyclerView));
    }
}
