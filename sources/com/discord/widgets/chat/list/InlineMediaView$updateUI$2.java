package com.discord.widgets.chat.list;

import android.view.View;
import android.widget.ImageView;
import androidx.core.content.ContextCompat;
import b.a.k.b;
import com.discord.databinding.InlineMediaViewBinding;
import com.discord.player.AppMediaPlayer;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: InlineMediaView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "volume", "", "invoke", "(F)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InlineMediaView$updateUI$2 extends o implements Function1<Float, Unit> {
    public final /* synthetic */ AppMediaPlayer $player;
    public final /* synthetic */ InlineMediaView this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InlineMediaView$updateUI$2(InlineMediaView inlineMediaView, AppMediaPlayer appMediaPlayer) {
        super(1);
        this.this$0 = inlineMediaView;
        this.$player = appMediaPlayer;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Float f) {
        invoke(f.floatValue());
        return Unit.a;
    }

    public final void invoke(float f) {
        InlineMediaViewBinding inlineMediaViewBinding;
        InlineMediaViewBinding inlineMediaViewBinding2;
        InlineMediaViewBinding inlineMediaViewBinding3;
        CharSequence d;
        final boolean z2 = f > ((float) 0);
        int i = z2 ? R.drawable.ic_volume_up_white_24dp : R.drawable.ic_volume_off_24dp;
        inlineMediaViewBinding = this.this$0.binding;
        inlineMediaViewBinding.g.setImageDrawable(ContextCompat.getDrawable(this.this$0.getContext(), i));
        inlineMediaViewBinding2 = this.this$0.binding;
        inlineMediaViewBinding2.g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.InlineMediaView$updateUI$2.1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                InlineMediaView$updateUI$2.this.$player.d(z2 ? 0.0f : 1.0f);
            }
        });
        int i2 = z2 ? R.string.video_playback_mute_accessibility_label : R.string.video_playback_unmute_accessibility_label;
        inlineMediaViewBinding3 = this.this$0.binding;
        ImageView imageView = inlineMediaViewBinding3.g;
        m.checkNotNullExpressionValue(imageView, "binding.inlineMediaVolumeToggle");
        d = b.d(this.this$0, i2, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        imageView.setContentDescription(d);
    }
}
