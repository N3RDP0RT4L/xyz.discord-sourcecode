package com.discord.widgets.chat.list.entries;

import andhook.lib.HookHelper;
import b.a.d.l0.a;
import com.discord.api.application.Application;
import com.discord.api.botuikit.Component;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.message.activity.MessageActivity;
import com.discord.api.message.attachment.MessageAttachment;
import com.discord.api.message.embed.EmbedType;
import com.discord.api.message.embed.MessageEmbed;
import com.discord.api.role.GuildRole;
import com.discord.api.sticker.Sticker;
import com.discord.api.sticker.StickerPartial;
import com.discord.api.user.User;
import com.discord.models.botuikit.MessageComponent;
import com.discord.models.domain.ModelInvite;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageState;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadMessages;
import com.discord.utilities.embed.InviteEmbedModel;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.message.MessageUtils;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.widgets.botuikit.BotComponentExperiments;
import com.discord.widgets.botuikit.ComponentChatListState;
import com.discord.widgets.botuikit.ComponentStateMapper;
import com.discord.widgets.chat.AutocompleteSelectionTypes;
import com.discord.widgets.chat.AutocompleteTypes;
import d0.g0.s;
import d0.t.h0;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.MatchResult;
/* compiled from: ChatListEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\b&\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004R\"\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\u0004\"\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "isInExpandedBlockedMessageChunk", "()Z", "shouldShowThreadSpine", "Z", "getShouldShowThreadSpine", "setShouldShowThreadSpine", "(Z)V", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class ChatListEntry implements MGRecyclerDataPayload {
    public static final int APPLICATION_COMMAND = 33;
    public static final int BLOCKED_MESSAGES = 10;
    public static final int BOT_UI_COMPONENT = 36;
    public static final int CALL_MESSAGE = 19;
    public static final Companion Companion = new Companion(null);
    public static final int DIVIDER = 15;
    public static final int EMPTY_PINS = 16;
    public static final int EPHEMERAL_MESSAGE = 34;
    public static final int GAME_INVITE = 22;
    public static final int GIFT = 26;
    public static final int GUILD_INVITE_REMINDER = 38;
    public static final int GUILD_SCHEDULED_EVENT_INVITE = 43;
    public static final int GUILD_TEMPLATE = 30;
    public static final int GUILD_WELCOME = 25;
    public static final int INVITE = 24;
    public static final int LOAD_INDICATOR = 2;
    public static final int MENTION_FOOTER = 18;
    public static final int MESSAGE = 0;
    public static final int MESSAGE_ATTACHMENT = 28;
    public static final int MESSAGE_EMBED = 21;
    public static final int MESSAGE_FAILED = 20;
    public static final int MESSAGE_HEADER = 17;
    public static final int MESSAGE_MINIMAL = 1;
    public static final int NEW_MESSAGES = 8;
    public static final int REACTIONS = 4;
    public static final int REPLY = 32;
    public static final int SEARCH_EMPTY = 13;
    public static final int SEARCH_ERROR = 14;
    public static final int SEARCH_INDEXING = 12;
    public static final int SEARCH_RESULT_COUNT = 11;
    public static final int SPACER = 7;
    public static final int SPOTIFY_LISTEN_TOGETHER = 23;
    public static final int STAGE_INVITE = 39;
    public static final int START_OF_CHAT = 3;
    public static final int START_OF_PRIVATE_CHAT = 29;
    public static final int STICKER = 31;
    public static final int STICKER_GREET = 41;
    public static final int STICKER_GREET_COMPACT = 42;
    public static final int SYSTEM_MESSAGE = 5;
    public static final int THREAD_DRAFT_FORM = 37;
    public static final int THREAD_EMBED = 35;
    public static final int THREAD_STARTER_DIVIDER = 40;
    public static final int TIMESTAMP = 9;
    public static final int UPLOAD_STATUS = 6;
    private boolean shouldShowThreadSpine;

    /* compiled from: ChatListEntry.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b/\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\bc\u0010dJ1\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJs\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\t0\u001b2\u0006\u0010\r\u001a\u00020\f2\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u000fj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\f2\b\u0010\u0015\u001a\u0004\u0018\u00010\u00142\u0014\u0010\u0017\u001a\u0010\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u000e2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0018¢\u0006\u0004\b\u001c\u0010\u001dJ\u001b\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\t0\u001b2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ9\u0010$\u001a\b\u0012\u0004\u0012\u00020\t0\u001b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010!\u001a\u00060\u000fj\u0002` 2\b\u0010#\u001a\u0004\u0018\u00010\"2\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b$\u0010%J#\u0010&\u001a\b\u0012\u0004\u0012\u00020\t0\u001b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b&\u0010'J\u001b\u0010(\u001a\b\u0012\u0004\u0012\u00020\t0\u001b2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b(\u0010\u001fJ\u001b\u0010*\u001a\b\u0012\u0004\u0012\u00020\t0)2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b*\u0010+J\u001b\u0010,\u001a\b\u0012\u0004\u0012\u00020\t0)2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b,\u0010+J\u001b\u0010-\u001a\b\u0012\u0004\u0012\u00020\t0)2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b-\u0010+JU\u00104\u001a\b\u0012\u0004\u0012\u00020\t0)2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010/\u001a\u0004\u0018\u00010.2\u0006\u00100\u001a\u00020\u00182\u0006\u00101\u001a\u00020\u00182\u0006\u00102\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u00182\u0006\u0010!\u001a\u00020\u000f2\u0006\u00103\u001a\u00020\u0018¢\u0006\u0004\b4\u00105R\u0016\u00107\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u00109\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b9\u00108R\u0016\u0010:\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b:\u00108R\u0016\u0010;\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b;\u00108R\u0016\u0010<\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b<\u00108R\u0016\u0010=\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b=\u00108R\u0016\u0010>\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b>\u00108R\u0016\u0010?\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b?\u00108R\u0016\u0010@\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b@\u00108R\u0016\u0010A\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bA\u00108R\u0016\u0010B\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bB\u00108R\u0016\u0010C\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bC\u00108R\u0016\u0010D\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bD\u00108R\u0016\u0010E\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bE\u00108R\u0016\u0010F\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bF\u00108R\u0016\u0010G\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bG\u00108R\u0016\u0010H\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bH\u00108R\u0016\u0010I\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bI\u00108R\u0016\u0010J\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bJ\u00108R\u0016\u0010K\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bK\u00108R\u0016\u0010L\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bL\u00108R\u0016\u0010M\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bM\u00108R\u0016\u0010N\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bN\u00108R\u0016\u0010O\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bO\u00108R\u0016\u0010P\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bP\u00108R\u0016\u0010Q\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bQ\u00108R\u0016\u0010R\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bR\u00108R\u0016\u0010S\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bS\u00108R\u0016\u0010T\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bT\u00108R\u0016\u0010U\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bU\u00108R\u0016\u0010V\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bV\u00108R\u0016\u0010W\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bW\u00108R\u0016\u0010X\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bX\u00108R\u0016\u0010Y\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bY\u00108R\u0016\u0010Z\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bZ\u00108R\u0016\u0010[\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b[\u00108R\u0016\u0010\\\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\\\u00108R\u0016\u0010]\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b]\u00108R\u0016\u0010^\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b^\u00108R\u0016\u0010_\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b_\u00108R\u0016\u0010`\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b`\u00108R\u0016\u0010a\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\ba\u00108R\u0016\u0010b\u001a\u0002068\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\bb\u00108¨\u0006e"}, d2 = {"Lcom/discord/widgets/chat/list/entries/ChatListEntry$Companion;", "", "Lcom/discord/models/message/Message;", "message", "", "inviteCode", "eventId", "Lcom/discord/utilities/embed/InviteEmbedModel;", "inviteEmbedModel", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "createEntryForInvite", "(Lcom/discord/models/message/Message;Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/embed/InviteEmbedModel;)Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "Lcom/discord/api/channel/Channel;", "channel", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "channelMembers", "thread", "Lcom/discord/stores/StoreThreadMessages$ThreadState;", "threadEmbedMetadata", "Lcom/discord/api/role/GuildRole;", "guildRoles", "", "animateEmojis", "renderEmbeds", "", "createThreadEmbedEntries", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Lcom/discord/models/message/Message;Lcom/discord/api/channel/Channel;Lcom/discord/stores/StoreThreadMessages$ThreadState;Ljava/util/Map;ZZ)Ljava/util/Collection;", "createGiftEntries", "(Lcom/discord/models/message/Message;)Ljava/util/Collection;", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;", "componentStoreState", "createBotComponentEntries", "(Lcom/discord/models/message/Message;JLcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;Z)Ljava/util/Collection;", "createInviteEntries", "(Lcom/discord/models/message/Message;Lcom/discord/utilities/embed/InviteEmbedModel;)Ljava/util/Collection;", "createGuildTemplateEntries", "", "createSpotifyListenTogetherEntries", "(Lcom/discord/models/message/Message;)Ljava/util/List;", "createGameInviteEntries", "createStickerEntries", "Lcom/discord/stores/StoreMessageState$State;", "messageState", "blockedChunkExpanded", "allowAnimatedEmojis", "autoPlayGifs", "isThreadStarterMessage", "createEmbedEntries", "(Lcom/discord/models/message/Message;Lcom/discord/stores/StoreMessageState$State;ZZZZJZ)Ljava/util/List;", "", "APPLICATION_COMMAND", "I", "BLOCKED_MESSAGES", "BOT_UI_COMPONENT", "CALL_MESSAGE", "DIVIDER", "EMPTY_PINS", "EPHEMERAL_MESSAGE", "GAME_INVITE", "GIFT", "GUILD_INVITE_REMINDER", "GUILD_SCHEDULED_EVENT_INVITE", "GUILD_TEMPLATE", "GUILD_WELCOME", "INVITE", "LOAD_INDICATOR", "MENTION_FOOTER", "MESSAGE", "MESSAGE_ATTACHMENT", "MESSAGE_EMBED", "MESSAGE_FAILED", "MESSAGE_HEADER", "MESSAGE_MINIMAL", "NEW_MESSAGES", AutocompleteTypes.REACTIONS, "REPLY", "SEARCH_EMPTY", "SEARCH_ERROR", "SEARCH_INDEXING", "SEARCH_RESULT_COUNT", "SPACER", "SPOTIFY_LISTEN_TOGETHER", "STAGE_INVITE", "START_OF_CHAT", "START_OF_PRIVATE_CHAT", AutocompleteSelectionTypes.STICKER, "STICKER_GREET", "STICKER_GREET_COMPACT", "SYSTEM_MESSAGE", "THREAD_DRAFT_FORM", "THREAD_EMBED", "THREAD_STARTER_DIVIDER", "TIMESTAMP", "UPLOAD_STATUS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final ChatListEntry createEntryForInvite(Message message, String str, String str2, InviteEmbedModel inviteEmbedModel) {
            if (str2 != null) {
                ModelInvite resolveInviteForGuildScheduledEvent = inviteEmbedModel.resolveInviteForGuildScheduledEvent(str, str2);
                if ((resolveInviteForGuildScheduledEvent != null ? resolveInviteForGuildScheduledEvent.getGuildScheduledEvent() : null) != null) {
                    GuildScheduledEvent guildScheduledEvent = resolveInviteForGuildScheduledEvent.getGuildScheduledEvent();
                    m.checkNotNull(guildScheduledEvent);
                    m.checkNotNullExpressionValue(guildScheduledEvent, "eventInvite.guildScheduledEvent!!");
                    return new GuildScheduledEventInviteEntry(resolveInviteForGuildScheduledEvent, str, guildScheduledEvent);
                }
            }
            ModelInvite resolveInviteCodeForPublicStage = inviteEmbedModel.resolveInviteCodeForPublicStage(str);
            long j = 0;
            if (resolveInviteCodeForPublicStage != null) {
                User author = message.getAuthor();
                m.checkNotNull(author);
                long i = author.i();
                long id2 = message.getId();
                Long guildId = message.getGuildId();
                if (guildId != null) {
                    j = guildId.longValue();
                }
                return new StageInviteEntry(i, id2, str, resolveInviteCodeForPublicStage, j);
            }
            User author2 = message.getAuthor();
            m.checkNotNull(author2);
            long i2 = author2.i();
            long id3 = message.getId();
            Long guildId2 = message.getGuildId();
            if (guildId2 != null) {
                j = guildId2.longValue();
            }
            return new InviteEntry(i2, id3, str, j, str2);
        }

        public final Collection<ChatListEntry> createBotComponentEntries(Message message, long j, ComponentChatListState.ComponentStoreState componentStoreState, boolean z2) {
            long j2;
            m.checkNotNullParameter(message, "message");
            List<Component> components = message.getComponents();
            if (components == null || !(!components.isEmpty())) {
                return n.emptyList();
            }
            List<MessageComponent> processComponentsToMessageComponents = ComponentStateMapper.INSTANCE.processComponentsToMessageComponents(message.getComponents(), componentStoreState, BotComponentExperiments.Companion.get(StoreStream.Companion.getExperiments()), z2);
            Long applicationId = message.getApplicationId();
            if (applicationId != null) {
                j2 = applicationId.longValue();
            } else {
                User author = message.getAuthor();
                m.checkNotNull(author);
                j2 = author.i();
            }
            return d0.t.m.listOf(new BotUiComponentEntry(message, j2, Long.valueOf(j), processComponentsToMessageComponents));
        }

        public final List<ChatListEntry> createEmbedEntries(Message message, StoreMessageState.State state, boolean z2, boolean z3, boolean z4, boolean z5, long j, boolean z6) {
            m.checkNotNullParameter(message, "message");
            if (!(message.hasAttachments() || (message.hasEmbeds() && z5))) {
                return n.emptyList();
            }
            HashSet hashSet = new HashSet();
            ArrayList<MessageEmbed> arrayList = new ArrayList();
            List<MessageEmbed> embeds = message.getEmbeds();
            if (embeds != null) {
                for (MessageEmbed messageEmbed : embeds) {
                    if (!hashSet.contains(messageEmbed.l())) {
                        hashSet.add(messageEmbed.l());
                        arrayList.add(messageEmbed);
                    } else if (messageEmbed.l() == null) {
                        arrayList.add(messageEmbed);
                    }
                }
            }
            List<MessageAttachment> attachments = message.getAttachments();
            int i = 0;
            ArrayList arrayList2 = new ArrayList(arrayList.size() + (attachments != null ? attachments.size() : 0));
            List<MessageAttachment> attachments2 = message.getAttachments();
            if (attachments2 != null) {
                int i2 = 0;
                for (MessageAttachment messageAttachment : attachments2) {
                    i2++;
                    arrayList2.add(new AttachmentEntry(i2, j, message, state, messageAttachment, z2, z3, z4, z6));
                }
                i = i2;
            }
            int i3 = i;
            for (MessageEmbed messageEmbed2 : arrayList) {
                if (messageEmbed2.k() != EmbedType.APPLICATION_NEWS) {
                    i3++;
                    arrayList2.add(new EmbedEntry(i3, j, message, state, messageEmbed2, z2, z3, z4, z6));
                }
            }
            return arrayList2;
        }

        public final List<ChatListEntry> createGameInviteEntries(Message message) {
            m.checkNotNullParameter(message, "message");
            Application application = message.getApplication();
            MessageActivity activity = message.getActivity();
            if (application == null || activity == null) {
                return n.emptyList();
            }
            User author = message.getAuthor();
            m.checkNotNull(author);
            return d0.t.m.listOf(new GameInviteEntry(author.i(), message.getId(), activity, application));
        }

        public final Collection<ChatListEntry> createGiftEntries(Message message) {
            m.checkNotNullParameter(message, "message");
            String content = message.getContent();
            if (content == null || content.length() == 0) {
                return n.emptyList();
            }
            HashMap hashMap = new HashMap();
            for (MatchResult matchResult : a.r.findAll(content, 0)) {
                List<String> groupValues = matchResult.getGroupValues();
                if (!groupValues.isEmpty()) {
                    String str = groupValues.get(groupValues.size() - 1);
                    User author = message.getAuthor();
                    m.checkNotNull(author);
                    hashMap.put(str, new GiftEntry(author.i(), message.getId(), message.getChannelId(), str));
                }
            }
            Collection<ChatListEntry> values = hashMap.values();
            m.checkNotNullExpressionValue(values, "entries.values");
            return values;
        }

        public final Collection<ChatListEntry> createGuildTemplateEntries(Message message) {
            m.checkNotNullParameter(message, "message");
            String content = message.getContent();
            if (content != null) {
                if (!(content.length() == 0)) {
                    HashMap hashMap = new HashMap();
                    for (MatchResult matchResult : a.q.findAll(content, 0)) {
                        List<String> groupValues = matchResult.getGroupValues();
                        if (!groupValues.isEmpty()) {
                            String str = groupValues.get(groupValues.size() - 1);
                            User author = message.getAuthor();
                            m.checkNotNull(author);
                            hashMap.put(str, new GuildTemplateEntry(author.i(), message.getId(), str));
                        }
                    }
                    Collection<ChatListEntry> values = hashMap.values();
                    m.checkNotNullExpressionValue(values, "entries.values");
                    return values;
                }
            }
            return n.emptyList();
        }

        public final Collection<ChatListEntry> createInviteEntries(Message message, InviteEmbedModel inviteEmbedModel) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(inviteEmbedModel, "inviteEmbedModel");
            String content = message.getContent();
            if (content == null || content.length() == 0) {
                return n.emptyList();
            }
            HashMap hashMap = new HashMap();
            for (MatchResult matchResult : a.o.findAll(content, 0)) {
                List<String> groupValues = matchResult.getGroupValues();
                if (groupValues.size() == 3) {
                    String str = groupValues.get(1);
                    Long longOrNull = s.toLongOrNull(groupValues.get(2));
                    hashMap.put(str, createEntryForInvite(message, str, longOrNull != null ? String.valueOf(longOrNull.longValue()) : null, inviteEmbedModel));
                }
            }
            Collection<ChatListEntry> values = hashMap.values();
            m.checkNotNullExpressionValue(values, "entries.values");
            return values;
        }

        public final List<ChatListEntry> createSpotifyListenTogetherEntries(Message message) {
            m.checkNotNullParameter(message, "message");
            MessageActivity activity = message.getActivity();
            if (activity == null || !message.isSpotifyListeningActivity()) {
                return n.emptyList();
            }
            User author = message.getAuthor();
            m.checkNotNull(author);
            return d0.t.m.listOf(new SpotifyListenTogetherEntry(author.i(), message.getId(), activity));
        }

        public final List<ChatListEntry> createStickerEntries(Message message) {
            ArrayList arrayList;
            m.checkNotNullParameter(message, "message");
            List<StickerPartial> stickerItems = message.getStickerItems();
            if (!(stickerItems == null || stickerItems.isEmpty())) {
                List<StickerPartial> stickerItems2 = message.getStickerItems();
                arrayList = new ArrayList(o.collectionSizeOrDefault(stickerItems2, 10));
                for (StickerPartial stickerPartial : stickerItems2) {
                    arrayList.add(new StickerEntry(message, stickerPartial));
                }
            } else {
                List<Sticker> stickers = message.getStickers();
                if (stickers != null) {
                    arrayList = new ArrayList(o.collectionSizeOrDefault(stickers, 10));
                    for (Sticker sticker : stickers) {
                        arrayList.add(new StickerEntry(message, sticker));
                    }
                } else {
                    arrayList = null;
                }
            }
            return arrayList != null ? arrayList : n.emptyList();
        }

        public final Collection<ChatListEntry> createThreadEmbedEntries(Channel channel, Map<Long, GuildMember> map, Message message, Channel channel2, StoreThreadMessages.ThreadState threadState, Map<Long, GuildRole> map2, boolean z2, boolean z3) {
            Map<Long, String> map3;
            Message mostRecentMessage;
            User author;
            Message mostRecentMessage2;
            Message mostRecentMessage3;
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "channelMembers");
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(channel2, "thread");
            if (!z3) {
                return n.emptyList();
            }
            long id2 = message.getId();
            int count = threadState != null ? threadState.getCount() : 0;
            Long l = null;
            Message mostRecentMessage4 = threadState != null ? threadState.getMostRecentMessage() : null;
            Map<Long, GuildRole> map4 = RoleUtils.containsRoleMentions((threadState == null || (mostRecentMessage3 = threadState.getMostRecentMessage()) == null) ? null : mostRecentMessage3.getContent()) ? map2 : null;
            if (threadState == null || (mostRecentMessage2 = threadState.getMostRecentMessage()) == null || (map3 = MessageUtils.getNickOrUsernames(mostRecentMessage2, channel, map, channel.n())) == null) {
                map3 = h0.emptyMap();
            }
            Map<Long, String> map5 = map3;
            if (!(threadState == null || (mostRecentMessage = threadState.getMostRecentMessage()) == null || (author = mostRecentMessage.getAuthor()) == null)) {
                l = Long.valueOf(author.i());
            }
            return d0.t.m.listOf(new ThreadEmbedEntry(id2, channel2, count, mostRecentMessage4, map4, map5, z2, map.get(l), message));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public final boolean getShouldShowThreadSpine() {
        return this.shouldShowThreadSpine;
    }

    public boolean isInExpandedBlockedMessageChunk() {
        return false;
    }

    public final void setShouldShowThreadSpine(boolean z2) {
        this.shouldShowThreadSpine = z2;
    }
}
