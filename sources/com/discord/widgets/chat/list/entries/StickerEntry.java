package com.discord.widgets.chat.list.entries;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.sticker.BaseSticker;
import com.discord.models.message.Message;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StickerEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R\u001c\u0010\u0019\u001a\u00020\u000f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u0011R\u001c\u0010\u001c\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u000eR\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u001f\u001a\u0004\b \u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/widgets/chat/list/entries/StickerEntry;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "Lcom/discord/models/message/Message;", "component1", "()Lcom/discord/models/message/Message;", "Lcom/discord/api/sticker/BaseSticker;", "component2", "()Lcom/discord/api/sticker/BaseSticker;", "message", "sticker", "copy", "(Lcom/discord/models/message/Message;Lcom/discord/api/sticker/BaseSticker;)Lcom/discord/widgets/chat/list/entries/StickerEntry;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/sticker/BaseSticker;", "getSticker", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/message/Message;", "getMessage", HookHelper.constructorName, "(Lcom/discord/models/message/Message;Lcom/discord/api/sticker/BaseSticker;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StickerEntry extends ChatListEntry {
    private final String key;
    private final Message message;
    private final BaseSticker sticker;
    private final int type = 31;

    public StickerEntry(Message message, BaseSticker baseSticker) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(baseSticker, "sticker");
        this.message = message;
        this.sticker = baseSticker;
        this.key = getType() + " -- " + message.getId() + " -- " + baseSticker.d();
    }

    public static /* synthetic */ StickerEntry copy$default(StickerEntry stickerEntry, Message message, BaseSticker baseSticker, int i, Object obj) {
        if ((i & 1) != 0) {
            message = stickerEntry.message;
        }
        if ((i & 2) != 0) {
            baseSticker = stickerEntry.sticker;
        }
        return stickerEntry.copy(message, baseSticker);
    }

    public final Message component1() {
        return this.message;
    }

    public final BaseSticker component2() {
        return this.sticker;
    }

    public final StickerEntry copy(Message message, BaseSticker baseSticker) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(baseSticker, "sticker");
        return new StickerEntry(message, baseSticker);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StickerEntry)) {
            return false;
        }
        StickerEntry stickerEntry = (StickerEntry) obj;
        return m.areEqual(this.message, stickerEntry.message) && m.areEqual(this.sticker, stickerEntry.sticker);
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final Message getMessage() {
        return this.message;
    }

    public final BaseSticker getSticker() {
        return this.sticker;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        Message message = this.message;
        int i = 0;
        int hashCode = (message != null ? message.hashCode() : 0) * 31;
        BaseSticker baseSticker = this.sticker;
        if (baseSticker != null) {
            i = baseSticker.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("StickerEntry(message=");
        R.append(this.message);
        R.append(", sticker=");
        R.append(this.sticker);
        R.append(")");
        return R.toString();
    }
}
