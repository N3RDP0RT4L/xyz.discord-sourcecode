package com.discord.widgets.chat.list.entries;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.member.GuildMember;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StartOfChatEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u0000\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\n\u0010\u0017\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u0018\u001a\u00020\u0006\u0012\u0006\u0010\u0019\u001a\u00020\t\u0012\u0006\u0010\u001a\u001a\u00020\t\u0012\u0006\u0010\u001b\u001a\u00020\t\u0012\u0006\u0010\u001c\u001a\u00020\t\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u000f\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010\u001f\u001a\u00020\u0006\u0012\u0006\u0010 \u001a\u00020\t¢\u0006\u0004\b<\u0010=J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0010\u0010\r\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\r\u0010\u000bJ\u0010\u0010\u000e\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000bJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0015\u0010\bJ\u0010\u0010\u0016\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u0016\u0010\u000bJ|\u0010!\u001a\u00020\u00002\f\b\u0002\u0010\u0017\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0018\u001a\u00020\u00062\b\b\u0002\u0010\u0019\u001a\u00020\t2\b\b\u0002\u0010\u001a\u001a\u00020\t2\b\b\u0002\u0010\u001b\u001a\u00020\t2\b\b\u0002\u0010\u001c\u001a\u00020\t2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010\u001f\u001a\u00020\u00062\b\b\u0002\u0010 \u001a\u00020\tHÆ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010#\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b#\u0010\bJ\u0010\u0010$\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b$\u0010%J\u001a\u0010(\u001a\u00020\t2\b\u0010'\u001a\u0004\u0018\u00010&HÖ\u0003¢\u0006\u0004\b(\u0010)R\u0019\u0010\u001a\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010*\u001a\u0004\b+\u0010\u000bR\u001c\u0010,\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010\bR\u0019\u0010 \u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010*\u001a\u0004\b \u0010\u000bR\u001d\u0010\u0017\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010/\u001a\u0004\b0\u0010\u0005R\u0019\u0010\u001f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010-\u001a\u0004\b1\u0010\bR\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00102\u001a\u0004\b3\u0010\u0011R\u0019\u0010\u0019\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b4\u0010\u000bR\u0019\u0010\u001c\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010*\u001a\u0004\b\u001c\u0010\u000bR\u0019\u0010\u0018\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010-\u001a\u0004\b5\u0010\bR\u001c\u00106\u001a\u00020\u000f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u0010%R\u0019\u0010\u001b\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010*\u001a\u0004\b9\u0010\u000bR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010:\u001a\u0004\b;\u0010\u0014¨\u0006>"}, d2 = {"Lcom/discord/widgets/chat/list/entries/StartOfChatEntry;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "", "component3", "()Z", "component4", "component5", "component6", "", "component7", "()Ljava/lang/Integer;", "Lcom/discord/models/member/GuildMember;", "component8", "()Lcom/discord/models/member/GuildMember;", "component9", "component10", "channelId", "channelName", "canReadMessageHistory", "canManageChannel", "canManageThread", "isThread", "threadAutoArchiveDuration", "threadCreatorMember", "threadCreatorName", "isTextInVoice", "copy", "(JLjava/lang/String;ZZZZLjava/lang/Integer;Lcom/discord/models/member/GuildMember;Ljava/lang/String;Z)Lcom/discord/widgets/chat/list/entries/StartOfChatEntry;", "toString", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanManageChannel", "key", "Ljava/lang/String;", "getKey", "J", "getChannelId", "getThreadCreatorName", "Ljava/lang/Integer;", "getThreadAutoArchiveDuration", "getCanReadMessageHistory", "getChannelName", "type", "I", "getType", "getCanManageThread", "Lcom/discord/models/member/GuildMember;", "getThreadCreatorMember", HookHelper.constructorName, "(JLjava/lang/String;ZZZZLjava/lang/Integer;Lcom/discord/models/member/GuildMember;Ljava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StartOfChatEntry extends ChatListEntry {
    private final boolean canManageChannel;
    private final boolean canManageThread;
    private final boolean canReadMessageHistory;
    private final long channelId;
    private final String channelName;
    private final boolean isTextInVoice;
    private final boolean isThread;
    private final String key;
    private final Integer threadAutoArchiveDuration;
    private final GuildMember threadCreatorMember;
    private final String threadCreatorName;
    private final int type = 3;

    public StartOfChatEntry(long j, String str, boolean z2, boolean z3, boolean z4, boolean z5, Integer num, GuildMember guildMember, String str2, boolean z6) {
        m.checkNotNullParameter(str, "channelName");
        m.checkNotNullParameter(str2, "threadCreatorName");
        this.channelId = j;
        this.channelName = str;
        this.canReadMessageHistory = z2;
        this.canManageChannel = z3;
        this.canManageThread = z4;
        this.isThread = z5;
        this.threadAutoArchiveDuration = num;
        this.threadCreatorMember = guildMember;
        this.threadCreatorName = str2;
        this.isTextInVoice = z6;
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(j);
        this.key = sb.toString();
    }

    public final long component1() {
        return this.channelId;
    }

    public final boolean component10() {
        return this.isTextInVoice;
    }

    public final String component2() {
        return this.channelName;
    }

    public final boolean component3() {
        return this.canReadMessageHistory;
    }

    public final boolean component4() {
        return this.canManageChannel;
    }

    public final boolean component5() {
        return this.canManageThread;
    }

    public final boolean component6() {
        return this.isThread;
    }

    public final Integer component7() {
        return this.threadAutoArchiveDuration;
    }

    public final GuildMember component8() {
        return this.threadCreatorMember;
    }

    public final String component9() {
        return this.threadCreatorName;
    }

    public final StartOfChatEntry copy(long j, String str, boolean z2, boolean z3, boolean z4, boolean z5, Integer num, GuildMember guildMember, String str2, boolean z6) {
        m.checkNotNullParameter(str, "channelName");
        m.checkNotNullParameter(str2, "threadCreatorName");
        return new StartOfChatEntry(j, str, z2, z3, z4, z5, num, guildMember, str2, z6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StartOfChatEntry)) {
            return false;
        }
        StartOfChatEntry startOfChatEntry = (StartOfChatEntry) obj;
        return this.channelId == startOfChatEntry.channelId && m.areEqual(this.channelName, startOfChatEntry.channelName) && this.canReadMessageHistory == startOfChatEntry.canReadMessageHistory && this.canManageChannel == startOfChatEntry.canManageChannel && this.canManageThread == startOfChatEntry.canManageThread && this.isThread == startOfChatEntry.isThread && m.areEqual(this.threadAutoArchiveDuration, startOfChatEntry.threadAutoArchiveDuration) && m.areEqual(this.threadCreatorMember, startOfChatEntry.threadCreatorMember) && m.areEqual(this.threadCreatorName, startOfChatEntry.threadCreatorName) && this.isTextInVoice == startOfChatEntry.isTextInVoice;
    }

    public final boolean getCanManageChannel() {
        return this.canManageChannel;
    }

    public final boolean getCanManageThread() {
        return this.canManageThread;
    }

    public final boolean getCanReadMessageHistory() {
        return this.canReadMessageHistory;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final String getChannelName() {
        return this.channelName;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final Integer getThreadAutoArchiveDuration() {
        return this.threadAutoArchiveDuration;
    }

    public final GuildMember getThreadCreatorMember() {
        return this.threadCreatorMember;
    }

    public final String getThreadCreatorName() {
        return this.threadCreatorName;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        int a = b.a(this.channelId) * 31;
        String str = this.channelName;
        int i = 0;
        int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
        boolean z2 = this.canReadMessageHistory;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode + i3) * 31;
        boolean z3 = this.canManageChannel;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        boolean z4 = this.canManageThread;
        if (z4) {
            z4 = true;
        }
        int i9 = z4 ? 1 : 0;
        int i10 = z4 ? 1 : 0;
        int i11 = (i8 + i9) * 31;
        boolean z5 = this.isThread;
        if (z5) {
            z5 = true;
        }
        int i12 = z5 ? 1 : 0;
        int i13 = z5 ? 1 : 0;
        int i14 = (i11 + i12) * 31;
        Integer num = this.threadAutoArchiveDuration;
        int hashCode2 = (i14 + (num != null ? num.hashCode() : 0)) * 31;
        GuildMember guildMember = this.threadCreatorMember;
        int hashCode3 = (hashCode2 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
        String str2 = this.threadCreatorName;
        if (str2 != null) {
            i = str2.hashCode();
        }
        int i15 = (hashCode3 + i) * 31;
        boolean z6 = this.isTextInVoice;
        if (!z6) {
            i2 = z6 ? 1 : 0;
        }
        return i15 + i2;
    }

    public final boolean isTextInVoice() {
        return this.isTextInVoice;
    }

    public final boolean isThread() {
        return this.isThread;
    }

    public String toString() {
        StringBuilder R = a.R("StartOfChatEntry(channelId=");
        R.append(this.channelId);
        R.append(", channelName=");
        R.append(this.channelName);
        R.append(", canReadMessageHistory=");
        R.append(this.canReadMessageHistory);
        R.append(", canManageChannel=");
        R.append(this.canManageChannel);
        R.append(", canManageThread=");
        R.append(this.canManageThread);
        R.append(", isThread=");
        R.append(this.isThread);
        R.append(", threadAutoArchiveDuration=");
        R.append(this.threadAutoArchiveDuration);
        R.append(", threadCreatorMember=");
        R.append(this.threadCreatorMember);
        R.append(", threadCreatorName=");
        R.append(this.threadCreatorName);
        R.append(", isTextInVoice=");
        return a.M(R, this.isTextInVoice, ")");
    }
}
