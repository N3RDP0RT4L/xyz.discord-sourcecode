package com.discord.widgets.chat.list.entries;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreThreadDraft;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: ThreadDraftFormEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u001b\b\u0086\b\u0018\u00002\u00020\u0001Bc\u0012\n\u0010\u0019\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\u001a\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006\u0012\n\u0010\u001b\u001a\u00060\u0002j\u0002`\t\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u0010\u001e\u001a\u00020\u0011\u0012\u0006\u0010\u001f\u001a\u00020\u0014\u0012\u0006\u0010 \u001a\u00020\u0014\u0012\u0006\u0010!\u001a\u00020\u0014¢\u0006\u0004\bB\u0010CJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\n\u001a\u00060\u0002j\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u0005J\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0016J~\u0010\"\u001a\u00020\u00002\f\b\u0002\u0010\u0019\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\u001a\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00062\f\b\u0002\u0010\u001b\u001a\u00060\u0002j\u0002`\t2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u0010\u001e\u001a\u00020\u00112\b\b\u0002\u0010\u001f\u001a\u00020\u00142\b\b\u0002\u0010 \u001a\u00020\u00142\b\b\u0002\u0010!\u001a\u00020\u0014HÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b'\u0010(J\u001a\u0010+\u001a\u00020\u00142\b\u0010*\u001a\u0004\u0018\u00010)HÖ\u0003¢\u0006\u0004\b+\u0010,R\u001c\u0010-\u001a\u00020\u000e8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b-\u0010.\u001a\u0004\b/\u0010(R\u001d\u0010\u001b\u001a\u00060\u0002j\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00100\u001a\u0004\b1\u0010\u0005R\u0019\u0010 \u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b \u00102\u001a\u0004\b3\u0010\u0016R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00104\u001a\u0004\b5\u0010\u0010R!\u0010\u001a\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00106\u001a\u0004\b7\u0010\bR\u0019\u0010\u001f\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00102\u001a\u0004\b8\u0010\u0016R\u001d\u0010\u0019\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u00100\u001a\u0004\b9\u0010\u0005R\u0019\u0010!\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b!\u00102\u001a\u0004\b:\u0010\u0016R\u001c\u0010;\u001a\u00020$8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010&R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010>\u001a\u0004\b?\u0010\rR\u0019\u0010\u001e\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010@\u001a\u0004\bA\u0010\u0013¨\u0006D"}, d2 = {"Lcom/discord/widgets/chat/list/entries/ThreadDraftFormEntry;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "Lcom/discord/primitives/MessageId;", "component2", "()Ljava/lang/Long;", "Lcom/discord/primitives/GuildId;", "component3", "Lcom/discord/models/guild/Guild;", "component4", "()Lcom/discord/models/guild/Guild;", "", "component5", "()Ljava/lang/Integer;", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "component6", "()Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "", "component7", "()Z", "component8", "component9", "parentChannelId", "parentMessageId", "guildId", "guild", "defaultAutoArchiveDuration", "threadDraftState", "canCreatePrivateThread", "canSeePrivateThreadOption", "canCreatePublicThread", "copy", "(JLjava/lang/Long;JLcom/discord/models/guild/Guild;Ljava/lang/Integer;Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;ZZZ)Lcom/discord/widgets/chat/list/entries/ThreadDraftFormEntry;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "J", "getGuildId", "Z", "getCanSeePrivateThreadOption", "Ljava/lang/Integer;", "getDefaultAutoArchiveDuration", "Ljava/lang/Long;", "getParentMessageId", "getCanCreatePrivateThread", "getParentChannelId", "getCanCreatePublicThread", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;", "getThreadDraftState", HookHelper.constructorName, "(JLjava/lang/Long;JLcom/discord/models/guild/Guild;Ljava/lang/Integer;Lcom/discord/stores/StoreThreadDraft$ThreadDraftState;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ThreadDraftFormEntry extends ChatListEntry {
    private final boolean canCreatePrivateThread;
    private final boolean canCreatePublicThread;
    private final boolean canSeePrivateThreadOption;
    private final Integer defaultAutoArchiveDuration;
    private final Guild guild;
    private final long guildId;
    private final String key;
    private final long parentChannelId;
    private final Long parentMessageId;
    private final StoreThreadDraft.ThreadDraftState threadDraftState;
    private final int type = 37;

    public ThreadDraftFormEntry(long j, Long l, long j2, Guild guild, Integer num, StoreThreadDraft.ThreadDraftState threadDraftState, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(threadDraftState, "threadDraftState");
        this.parentChannelId = j;
        this.parentMessageId = l;
        this.guildId = j2;
        this.guild = guild;
        this.defaultAutoArchiveDuration = num;
        this.threadDraftState = threadDraftState;
        this.canCreatePrivateThread = z2;
        this.canSeePrivateThreadOption = z3;
        this.canCreatePublicThread = z4;
        this.key = "37" + j + l;
    }

    public final long component1() {
        return this.parentChannelId;
    }

    public final Long component2() {
        return this.parentMessageId;
    }

    public final long component3() {
        return this.guildId;
    }

    public final Guild component4() {
        return this.guild;
    }

    public final Integer component5() {
        return this.defaultAutoArchiveDuration;
    }

    public final StoreThreadDraft.ThreadDraftState component6() {
        return this.threadDraftState;
    }

    public final boolean component7() {
        return this.canCreatePrivateThread;
    }

    public final boolean component8() {
        return this.canSeePrivateThreadOption;
    }

    public final boolean component9() {
        return this.canCreatePublicThread;
    }

    public final ThreadDraftFormEntry copy(long j, Long l, long j2, Guild guild, Integer num, StoreThreadDraft.ThreadDraftState threadDraftState, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(threadDraftState, "threadDraftState");
        return new ThreadDraftFormEntry(j, l, j2, guild, num, threadDraftState, z2, z3, z4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThreadDraftFormEntry)) {
            return false;
        }
        ThreadDraftFormEntry threadDraftFormEntry = (ThreadDraftFormEntry) obj;
        return this.parentChannelId == threadDraftFormEntry.parentChannelId && m.areEqual(this.parentMessageId, threadDraftFormEntry.parentMessageId) && this.guildId == threadDraftFormEntry.guildId && m.areEqual(this.guild, threadDraftFormEntry.guild) && m.areEqual(this.defaultAutoArchiveDuration, threadDraftFormEntry.defaultAutoArchiveDuration) && m.areEqual(this.threadDraftState, threadDraftFormEntry.threadDraftState) && this.canCreatePrivateThread == threadDraftFormEntry.canCreatePrivateThread && this.canSeePrivateThreadOption == threadDraftFormEntry.canSeePrivateThreadOption && this.canCreatePublicThread == threadDraftFormEntry.canCreatePublicThread;
    }

    public final boolean getCanCreatePrivateThread() {
        return this.canCreatePrivateThread;
    }

    public final boolean getCanCreatePublicThread() {
        return this.canCreatePublicThread;
    }

    public final boolean getCanSeePrivateThreadOption() {
        return this.canSeePrivateThreadOption;
    }

    public final Integer getDefaultAutoArchiveDuration() {
        return this.defaultAutoArchiveDuration;
    }

    public final Guild getGuild() {
        return this.guild;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final long getParentChannelId() {
        return this.parentChannelId;
    }

    public final Long getParentMessageId() {
        return this.parentMessageId;
    }

    public final StoreThreadDraft.ThreadDraftState getThreadDraftState() {
        return this.threadDraftState;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        int a = b.a(this.parentChannelId) * 31;
        Long l = this.parentMessageId;
        int i = 0;
        int a2 = (b.a(this.guildId) + ((a + (l != null ? l.hashCode() : 0)) * 31)) * 31;
        Guild guild = this.guild;
        int hashCode = (a2 + (guild != null ? guild.hashCode() : 0)) * 31;
        Integer num = this.defaultAutoArchiveDuration;
        int hashCode2 = (hashCode + (num != null ? num.hashCode() : 0)) * 31;
        StoreThreadDraft.ThreadDraftState threadDraftState = this.threadDraftState;
        if (threadDraftState != null) {
            i = threadDraftState.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z2 = this.canCreatePrivateThread;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z3 = this.canSeePrivateThreadOption;
        if (z3) {
            z3 = true;
        }
        int i7 = z3 ? 1 : 0;
        int i8 = z3 ? 1 : 0;
        int i9 = (i6 + i7) * 31;
        boolean z4 = this.canCreatePublicThread;
        if (!z4) {
            i3 = z4 ? 1 : 0;
        }
        return i9 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("ThreadDraftFormEntry(parentChannelId=");
        R.append(this.parentChannelId);
        R.append(", parentMessageId=");
        R.append(this.parentMessageId);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", guild=");
        R.append(this.guild);
        R.append(", defaultAutoArchiveDuration=");
        R.append(this.defaultAutoArchiveDuration);
        R.append(", threadDraftState=");
        R.append(this.threadDraftState);
        R.append(", canCreatePrivateThread=");
        R.append(this.canCreatePrivateThread);
        R.append(", canSeePrivateThreadOption=");
        R.append(this.canSeePrivateThreadOption);
        R.append(", canCreatePublicThread=");
        return a.M(R, this.canCreatePublicThread, ")");
    }
}
