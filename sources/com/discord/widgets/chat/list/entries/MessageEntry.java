package com.discord.widgets.chat.list.entries;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.api.sticker.BaseSticker;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageReplies;
import com.discord.stores.StoreMessageState;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: MessageEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b!\b\u0086\b\u0018\u00002\u00020\u0001:\u0002Z[B»\u0001\u0012\u0006\u0010&\u001a\u00020\u0007\u0012\b\u0010'\u001a\u0004\u0018\u00010\n\u0012\b\u0010(\u001a\u0004\u0018\u00010\r\u0012\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\r\u0012\u0018\u0010*\u001a\u0014\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0011\u0012\u0016\u0010+\u001a\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0017\u0012\u0004\u0012\u00020\u00180\u0011\u0012\b\b\u0002\u0010,\u001a\u00020\u0002\u0012\u0006\u0010-\u001a\u00020\u0002\u0012\b\b\u0002\u0010.\u001a\u00020\u0002\u0012\n\b\u0002\u0010/\u001a\u0004\u0018\u00010\u001b\u0012\n\b\u0002\u00100\u001a\u0004\u0018\u00010\r\u0012\b\b\u0002\u00101\u001a\u00020\u0002\u0012\n\b\u0002\u00102\u001a\u0004\u0018\u00010 \u0012\u000e\u00103\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`#¢\u0006\u0004\bX\u0010YJ\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000fJ\"\u0010\u0015\u001a\u0014\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J \u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0017\u0012\u0004\u0012\u00020\u00180\u0011HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0016J\u0010\u0010\u001a\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0004J\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0012\u0010\u001e\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u000fJ\u0010\u0010\u001f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001f\u0010\u0004J\u0012\u0010!\u001a\u0004\u0018\u00010 HÆ\u0003¢\u0006\u0004\b!\u0010\"J\u0018\u0010$\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`#HÆ\u0003¢\u0006\u0004\b$\u0010%JÒ\u0001\u00104\u001a\u00020\u00002\b\b\u0002\u0010&\u001a\u00020\u00072\n\b\u0002\u0010'\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010)\u001a\u0004\u0018\u00010\r2\u001a\b\u0002\u0010*\u001a\u0014\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00112\u0018\b\u0002\u0010+\u001a\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0017\u0012\u0004\u0012\u00020\u00180\u00112\b\b\u0002\u0010,\u001a\u00020\u00022\b\b\u0002\u0010-\u001a\u00020\u00022\b\b\u0002\u0010.\u001a\u00020\u00022\n\b\u0002\u0010/\u001a\u0004\u0018\u00010\u001b2\n\b\u0002\u00100\u001a\u0004\u0018\u00010\r2\b\b\u0002\u00101\u001a\u00020\u00022\n\b\u0002\u00102\u001a\u0004\u0018\u00010 2\u0010\b\u0002\u00103\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`#HÆ\u0001¢\u0006\u0004\b4\u00105J\u0010\u00106\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b6\u00107J\u0010\u00109\u001a\u000208HÖ\u0001¢\u0006\u0004\b9\u0010:J\u001a\u0010=\u001a\u00020\u00022\b\u0010<\u001a\u0004\u0018\u00010;HÖ\u0003¢\u0006\u0004\b=\u0010>R\u001c\u0010?\u001a\u00020\u00188\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b?\u0010@\u001a\u0004\bA\u00107R!\u00103\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`#8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010B\u001a\u0004\bC\u0010%R+\u0010*\u001a\u0014\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010D\u001a\u0004\bE\u0010\u0016R\u001b\u0010/\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010F\u001a\u0004\bG\u0010\u001dR\u0016\u0010-\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010HR\u001b\u0010(\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010I\u001a\u0004\bJ\u0010\u000fR\u0019\u0010.\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010H\u001a\u0004\bK\u0010\u0004R\u001b\u0010'\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010L\u001a\u0004\bM\u0010\fR\u001b\u0010)\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010I\u001a\u0004\bN\u0010\u000fR\u0016\u0010,\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010HR\u001c\u0010O\u001a\u0002088\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bO\u0010P\u001a\u0004\bQ\u0010:R\u001b\u00100\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010I\u001a\u0004\bR\u0010\u000fR\u001b\u00102\u001a\u0004\u0018\u00010 8\u0006@\u0006¢\u0006\f\n\u0004\b2\u0010S\u001a\u0004\bT\u0010\"R)\u0010+\u001a\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0017\u0012\u0004\u0012\u00020\u00180\u00118\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010D\u001a\u0004\bU\u0010\u0016R\u0019\u0010&\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010V\u001a\u0004\bW\u0010\tR\u0019\u00101\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010H\u001a\u0004\b1\u0010\u0004¨\u0006\\"}, d2 = {"Lcom/discord/widgets/chat/list/entries/MessageEntry;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "", "component7", "()Z", "component8", "isInExpandedBlockedMessageChunk", "Lcom/discord/models/message/Message;", "component1", "()Lcom/discord/models/message/Message;", "Lcom/discord/stores/StoreMessageState$State;", "component2", "()Lcom/discord/stores/StoreMessageState$State;", "Lcom/discord/models/member/GuildMember;", "component3", "()Lcom/discord/models/member/GuildMember;", "component4", "", "", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component5", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "", "component6", "component9", "Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;", "component10", "()Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;", "component11", "component12", "Lcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;", "component13", "()Lcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;", "Lcom/discord/api/permission/PermissionBit;", "component14", "()Ljava/lang/Long;", "message", "messageState", "author", "firstMentionedUser", "roles", "nickOrUsernames", "isMinimal", "isExpandedBlocked", "animateEmojis", "replyData", "interactionAuthor", "isThreadStarterMessage", "welcomeData", "permissionsForChannel", "copy", "(Lcom/discord/models/message/Message;Lcom/discord/stores/StoreMessageState$State;Lcom/discord/models/member/GuildMember;Lcom/discord/models/member/GuildMember;Ljava/util/Map;Ljava/util/Map;ZZZLcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;Lcom/discord/models/member/GuildMember;ZLcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;Ljava/lang/Long;)Lcom/discord/widgets/chat/list/entries/MessageEntry;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Ljava/lang/Long;", "getPermissionsForChannel", "Ljava/util/Map;", "getRoles", "Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;", "getReplyData", "Z", "Lcom/discord/models/member/GuildMember;", "getAuthor", "getAnimateEmojis", "Lcom/discord/stores/StoreMessageState$State;", "getMessageState", "getFirstMentionedUser", "type", "I", "getType", "getInteractionAuthor", "Lcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;", "getWelcomeData", "getNickOrUsernames", "Lcom/discord/models/message/Message;", "getMessage", HookHelper.constructorName, "(Lcom/discord/models/message/Message;Lcom/discord/stores/StoreMessageState$State;Lcom/discord/models/member/GuildMember;Lcom/discord/models/member/GuildMember;Ljava/util/Map;Ljava/util/Map;ZZZLcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;Lcom/discord/models/member/GuildMember;ZLcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;Ljava/lang/Long;)V", "ReplyData", "WelcomeCtaData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageEntry extends ChatListEntry {
    private final boolean animateEmojis;
    private final GuildMember author;
    private final GuildMember firstMentionedUser;
    private final GuildMember interactionAuthor;
    private final boolean isExpandedBlocked;
    private final boolean isMinimal;
    private final boolean isThreadStarterMessage;
    private final String key;
    private final Message message;
    private final StoreMessageState.State messageState;
    private final Map<Long, String> nickOrUsernames;
    private final Long permissionsForChannel;
    private final ReplyData replyData;
    private final Map<Long, GuildRole> roles;
    private final int type;
    private final WelcomeCtaData welcomeData;

    /* compiled from: MessageEntry.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0017\u001a\u00020\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\r\u0010\nR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0007¨\u0006 "}, d2 = {"Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;", "", "Lcom/discord/stores/StoreMessageReplies$MessageState;", "component1", "()Lcom/discord/stores/StoreMessageReplies$MessageState;", "Lcom/discord/widgets/chat/list/entries/MessageEntry;", "component2", "()Lcom/discord/widgets/chat/list/entries/MessageEntry;", "", "component3", "()Z", "messageState", "messageEntry", "isRepliedUserBlocked", "copy", "(Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;Z)Lcom/discord/widgets/chat/list/entries/MessageEntry$ReplyData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreMessageReplies$MessageState;", "getMessageState", "Z", "Lcom/discord/widgets/chat/list/entries/MessageEntry;", "getMessageEntry", HookHelper.constructorName, "(Lcom/discord/stores/StoreMessageReplies$MessageState;Lcom/discord/widgets/chat/list/entries/MessageEntry;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ReplyData {
        private final boolean isRepliedUserBlocked;
        private final MessageEntry messageEntry;
        private final StoreMessageReplies.MessageState messageState;

        public ReplyData(StoreMessageReplies.MessageState messageState, MessageEntry messageEntry, boolean z2) {
            m.checkNotNullParameter(messageState, "messageState");
            this.messageState = messageState;
            this.messageEntry = messageEntry;
            this.isRepliedUserBlocked = z2;
        }

        public static /* synthetic */ ReplyData copy$default(ReplyData replyData, StoreMessageReplies.MessageState messageState, MessageEntry messageEntry, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                messageState = replyData.messageState;
            }
            if ((i & 2) != 0) {
                messageEntry = replyData.messageEntry;
            }
            if ((i & 4) != 0) {
                z2 = replyData.isRepliedUserBlocked;
            }
            return replyData.copy(messageState, messageEntry, z2);
        }

        public final StoreMessageReplies.MessageState component1() {
            return this.messageState;
        }

        public final MessageEntry component2() {
            return this.messageEntry;
        }

        public final boolean component3() {
            return this.isRepliedUserBlocked;
        }

        public final ReplyData copy(StoreMessageReplies.MessageState messageState, MessageEntry messageEntry, boolean z2) {
            m.checkNotNullParameter(messageState, "messageState");
            return new ReplyData(messageState, messageEntry, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ReplyData)) {
                return false;
            }
            ReplyData replyData = (ReplyData) obj;
            return m.areEqual(this.messageState, replyData.messageState) && m.areEqual(this.messageEntry, replyData.messageEntry) && this.isRepliedUserBlocked == replyData.isRepliedUserBlocked;
        }

        public final MessageEntry getMessageEntry() {
            return this.messageEntry;
        }

        public final StoreMessageReplies.MessageState getMessageState() {
            return this.messageState;
        }

        public int hashCode() {
            StoreMessageReplies.MessageState messageState = this.messageState;
            int i = 0;
            int hashCode = (messageState != null ? messageState.hashCode() : 0) * 31;
            MessageEntry messageEntry = this.messageEntry;
            if (messageEntry != null) {
                i = messageEntry.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.isRepliedUserBlocked;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isRepliedUserBlocked() {
            return this.isRepliedUserBlocked;
        }

        public String toString() {
            StringBuilder R = a.R("ReplyData(messageState=");
            R.append(this.messageState);
            R.append(", messageEntry=");
            R.append(this.messageEntry);
            R.append(", isRepliedUserBlocked=");
            return a.M(R, this.isRepliedUserBlocked, ")");
        }

        public /* synthetic */ ReplyData(StoreMessageReplies.MessageState messageState, MessageEntry messageEntry, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(messageState, messageEntry, (i & 4) != 0 ? false : z2);
        }
    }

    /* compiled from: MessageEntry.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;", "", "Lcom/discord/api/sticker/BaseSticker;", "component1", "()Lcom/discord/api/sticker/BaseSticker;", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "sticker", "channel", "copy", "(Lcom/discord/api/sticker/BaseSticker;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/sticker/BaseSticker;", "getSticker", "Lcom/discord/api/channel/Channel;", "getChannel", HookHelper.constructorName, "(Lcom/discord/api/sticker/BaseSticker;Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class WelcomeCtaData {
        private final Channel channel;
        private final BaseSticker sticker;

        public WelcomeCtaData(BaseSticker baseSticker, Channel channel) {
            m.checkNotNullParameter(baseSticker, "sticker");
            m.checkNotNullParameter(channel, "channel");
            this.sticker = baseSticker;
            this.channel = channel;
        }

        public static /* synthetic */ WelcomeCtaData copy$default(WelcomeCtaData welcomeCtaData, BaseSticker baseSticker, Channel channel, int i, Object obj) {
            if ((i & 1) != 0) {
                baseSticker = welcomeCtaData.sticker;
            }
            if ((i & 2) != 0) {
                channel = welcomeCtaData.channel;
            }
            return welcomeCtaData.copy(baseSticker, channel);
        }

        public final BaseSticker component1() {
            return this.sticker;
        }

        public final Channel component2() {
            return this.channel;
        }

        public final WelcomeCtaData copy(BaseSticker baseSticker, Channel channel) {
            m.checkNotNullParameter(baseSticker, "sticker");
            m.checkNotNullParameter(channel, "channel");
            return new WelcomeCtaData(baseSticker, channel);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof WelcomeCtaData)) {
                return false;
            }
            WelcomeCtaData welcomeCtaData = (WelcomeCtaData) obj;
            return m.areEqual(this.sticker, welcomeCtaData.sticker) && m.areEqual(this.channel, welcomeCtaData.channel);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final BaseSticker getSticker() {
            return this.sticker;
        }

        public int hashCode() {
            BaseSticker baseSticker = this.sticker;
            int i = 0;
            int hashCode = (baseSticker != null ? baseSticker.hashCode() : 0) * 31;
            Channel channel = this.channel;
            if (channel != null) {
                i = channel.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("WelcomeCtaData(sticker=");
            R.append(this.sticker);
            R.append(", channel=");
            R.append(this.channel);
            R.append(")");
            return R.toString();
        }
    }

    public /* synthetic */ MessageEntry(Message message, StoreMessageState.State state, GuildMember guildMember, GuildMember guildMember2, Map map, Map map2, boolean z2, boolean z3, boolean z4, ReplyData replyData, GuildMember guildMember3, boolean z5, WelcomeCtaData welcomeCtaData, Long l, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(message, state, guildMember, (i & 8) != 0 ? null : guildMember2, map, map2, (i & 64) != 0 ? false : z2, z3, (i & 256) != 0 ? true : z4, (i & 512) != 0 ? null : replyData, (i & 1024) != 0 ? null : guildMember3, (i & 2048) != 0 ? false : z5, (i & 4096) != 0 ? null : welcomeCtaData, l);
    }

    private final boolean component7() {
        return this.isMinimal;
    }

    private final boolean component8() {
        return this.isExpandedBlocked;
    }

    public final Message component1() {
        return this.message;
    }

    public final ReplyData component10() {
        return this.replyData;
    }

    public final GuildMember component11() {
        return this.interactionAuthor;
    }

    public final boolean component12() {
        return this.isThreadStarterMessage;
    }

    public final WelcomeCtaData component13() {
        return this.welcomeData;
    }

    public final Long component14() {
        return this.permissionsForChannel;
    }

    public final StoreMessageState.State component2() {
        return this.messageState;
    }

    public final GuildMember component3() {
        return this.author;
    }

    public final GuildMember component4() {
        return this.firstMentionedUser;
    }

    public final Map<Long, GuildRole> component5() {
        return this.roles;
    }

    public final Map<Long, String> component6() {
        return this.nickOrUsernames;
    }

    public final boolean component9() {
        return this.animateEmojis;
    }

    public final MessageEntry copy(Message message, StoreMessageState.State state, GuildMember guildMember, GuildMember guildMember2, Map<Long, GuildRole> map, Map<Long, String> map2, boolean z2, boolean z3, boolean z4, ReplyData replyData, GuildMember guildMember3, boolean z5, WelcomeCtaData welcomeCtaData, Long l) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(map2, "nickOrUsernames");
        return new MessageEntry(message, state, guildMember, guildMember2, map, map2, z2, z3, z4, replyData, guildMember3, z5, welcomeCtaData, l);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageEntry)) {
            return false;
        }
        MessageEntry messageEntry = (MessageEntry) obj;
        return m.areEqual(this.message, messageEntry.message) && m.areEqual(this.messageState, messageEntry.messageState) && m.areEqual(this.author, messageEntry.author) && m.areEqual(this.firstMentionedUser, messageEntry.firstMentionedUser) && m.areEqual(this.roles, messageEntry.roles) && m.areEqual(this.nickOrUsernames, messageEntry.nickOrUsernames) && this.isMinimal == messageEntry.isMinimal && this.isExpandedBlocked == messageEntry.isExpandedBlocked && this.animateEmojis == messageEntry.animateEmojis && m.areEqual(this.replyData, messageEntry.replyData) && m.areEqual(this.interactionAuthor, messageEntry.interactionAuthor) && this.isThreadStarterMessage == messageEntry.isThreadStarterMessage && m.areEqual(this.welcomeData, messageEntry.welcomeData) && m.areEqual(this.permissionsForChannel, messageEntry.permissionsForChannel);
    }

    public final boolean getAnimateEmojis() {
        return this.animateEmojis;
    }

    public final GuildMember getAuthor() {
        return this.author;
    }

    public final GuildMember getFirstMentionedUser() {
        return this.firstMentionedUser;
    }

    public final GuildMember getInteractionAuthor() {
        return this.interactionAuthor;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final Message getMessage() {
        return this.message;
    }

    public final StoreMessageState.State getMessageState() {
        return this.messageState;
    }

    public final Map<Long, String> getNickOrUsernames() {
        return this.nickOrUsernames;
    }

    public final Long getPermissionsForChannel() {
        return this.permissionsForChannel;
    }

    public final ReplyData getReplyData() {
        return this.replyData;
    }

    public final Map<Long, GuildRole> getRoles() {
        return this.roles;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public final WelcomeCtaData getWelcomeData() {
        return this.welcomeData;
    }

    public int hashCode() {
        Message message = this.message;
        int i = 0;
        int hashCode = (message != null ? message.hashCode() : 0) * 31;
        StoreMessageState.State state = this.messageState;
        int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
        GuildMember guildMember = this.author;
        int hashCode3 = (hashCode2 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
        GuildMember guildMember2 = this.firstMentionedUser;
        int hashCode4 = (hashCode3 + (guildMember2 != null ? guildMember2.hashCode() : 0)) * 31;
        Map<Long, GuildRole> map = this.roles;
        int hashCode5 = (hashCode4 + (map != null ? map.hashCode() : 0)) * 31;
        Map<Long, String> map2 = this.nickOrUsernames;
        int hashCode6 = (hashCode5 + (map2 != null ? map2.hashCode() : 0)) * 31;
        boolean z2 = this.isMinimal;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode6 + i3) * 31;
        boolean z3 = this.isExpandedBlocked;
        if (z3) {
            z3 = true;
        }
        int i6 = z3 ? 1 : 0;
        int i7 = z3 ? 1 : 0;
        int i8 = (i5 + i6) * 31;
        boolean z4 = this.animateEmojis;
        if (z4) {
            z4 = true;
        }
        int i9 = z4 ? 1 : 0;
        int i10 = z4 ? 1 : 0;
        int i11 = (i8 + i9) * 31;
        ReplyData replyData = this.replyData;
        int hashCode7 = (i11 + (replyData != null ? replyData.hashCode() : 0)) * 31;
        GuildMember guildMember3 = this.interactionAuthor;
        int hashCode8 = (hashCode7 + (guildMember3 != null ? guildMember3.hashCode() : 0)) * 31;
        boolean z5 = this.isThreadStarterMessage;
        if (!z5) {
            i2 = z5 ? 1 : 0;
        }
        int i12 = (hashCode8 + i2) * 31;
        WelcomeCtaData welcomeCtaData = this.welcomeData;
        int hashCode9 = (i12 + (welcomeCtaData != null ? welcomeCtaData.hashCode() : 0)) * 31;
        Long l = this.permissionsForChannel;
        if (l != null) {
            i = l.hashCode();
        }
        return hashCode9 + i;
    }

    @Override // com.discord.widgets.chat.list.entries.ChatListEntry
    public boolean isInExpandedBlockedMessageChunk() {
        return this.isExpandedBlocked;
    }

    public final boolean isThreadStarterMessage() {
        return this.isThreadStarterMessage;
    }

    public String toString() {
        StringBuilder R = a.R("MessageEntry(message=");
        R.append(this.message);
        R.append(", messageState=");
        R.append(this.messageState);
        R.append(", author=");
        R.append(this.author);
        R.append(", firstMentionedUser=");
        R.append(this.firstMentionedUser);
        R.append(", roles=");
        R.append(this.roles);
        R.append(", nickOrUsernames=");
        R.append(this.nickOrUsernames);
        R.append(", isMinimal=");
        R.append(this.isMinimal);
        R.append(", isExpandedBlocked=");
        R.append(this.isExpandedBlocked);
        R.append(", animateEmojis=");
        R.append(this.animateEmojis);
        R.append(", replyData=");
        R.append(this.replyData);
        R.append(", interactionAuthor=");
        R.append(this.interactionAuthor);
        R.append(", isThreadStarterMessage=");
        R.append(this.isThreadStarterMessage);
        R.append(", welcomeData=");
        R.append(this.welcomeData);
        R.append(", permissionsForChannel=");
        return a.F(R, this.permissionsForChannel, ")");
    }

    public MessageEntry(Message message, StoreMessageState.State state, GuildMember guildMember, GuildMember guildMember2, Map<Long, GuildRole> map, Map<Long, String> map2, boolean z2, boolean z3, boolean z4, ReplyData replyData, GuildMember guildMember3, boolean z5, WelcomeCtaData welcomeCtaData, Long l) {
        Integer type;
        Integer type2;
        Integer type3;
        Integer type4;
        Integer type5;
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(map2, "nickOrUsernames");
        this.message = message;
        this.messageState = state;
        this.author = guildMember;
        this.firstMentionedUser = guildMember2;
        this.roles = map;
        this.nickOrUsernames = map2;
        this.isMinimal = z2;
        this.isExpandedBlocked = z3;
        this.animateEmojis = z4;
        this.replyData = replyData;
        this.interactionAuthor = guildMember3;
        this.isThreadStarterMessage = z5;
        this.welcomeData = welcomeCtaData;
        this.permissionsForChannel = l;
        int i = 19;
        if (z2) {
            i = 1;
        } else {
            if (!message.isInteraction() && (((type = message.getType()) == null || type.intValue() != 0) && ((type2 = message.getType()) == null || type2.intValue() != -1))) {
                Integer type6 = message.getType();
                if (((type6 != null && type6.intValue() == 20) || ((type5 = message.getType()) != null && type5.intValue() == 23)) && message.getInteraction() == null) {
                    i = 33;
                } else {
                    Integer type7 = message.getType();
                    if ((type7 == null || type7.intValue() != 3) && ((type3 = message.getType()) == null || type3.intValue() != 13)) {
                        Integer type8 = message.getType();
                        if ((type8 != null && type8.intValue() == -2) || ((type4 = message.getType()) != null && type4.intValue() == -3)) {
                            i = 20;
                        } else {
                            Integer type9 = message.getType();
                            if (type9 == null || type9.intValue() != -6) {
                                Integer type10 = message.getType();
                                if (type10 != null && type10.intValue() == 19) {
                                    i = 32;
                                } else {
                                    Integer type11 = message.getType();
                                    i = (type11 != null && type11.intValue() == 22) ? 38 : 5;
                                }
                            }
                        }
                    }
                }
            }
            i = 0;
        }
        this.type = i;
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(' ');
        Object nonce = message.getNonce();
        sb.append(nonce == null ? Long.valueOf(message.getId()) : nonce);
        this.key = sb.toString();
    }
}
