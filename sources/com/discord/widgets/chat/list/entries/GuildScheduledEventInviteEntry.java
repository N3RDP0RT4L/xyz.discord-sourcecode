package com.discord.widgets.chat.list.entries;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.models.domain.ModelInvite;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: GuildScheduledEventInviteEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0007J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u001c\u0010\u001b\u001a\u00020\u00118\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0013R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010 \u001a\u0004\b!\u0010\u0004R\u001c\u0010\"\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\"\u0010\u0019\u001a\u0004\b#\u0010\u0007¨\u0006&"}, d2 = {"Lcom/discord/widgets/chat/list/entries/GuildScheduledEventInviteEntry;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "Lcom/discord/models/domain/ModelInvite;", "component1", "()Lcom/discord/models/domain/ModelInvite;", "", "component2", "()Ljava/lang/String;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component3", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "invite", "inviteCode", "guildScheduledEvent", "copy", "(Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/widgets/chat/list/entries/GuildScheduledEventInviteEntry;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getInviteCode", "type", "I", "getType", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEvent", "Lcom/discord/models/domain/ModelInvite;", "getInvite", "key", "getKey", HookHelper.constructorName, "(Lcom/discord/models/domain/ModelInvite;Ljava/lang/String;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildScheduledEventInviteEntry extends ChatListEntry {
    private final GuildScheduledEvent guildScheduledEvent;
    private final ModelInvite invite;
    private final String inviteCode;
    private final String key;
    private final int type = 43;

    public GuildScheduledEventInviteEntry(ModelInvite modelInvite, String str, GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(modelInvite, "invite");
        m.checkNotNullParameter(str, "inviteCode");
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        this.invite = modelInvite;
        this.inviteCode = str;
        this.guildScheduledEvent = guildScheduledEvent;
        StringBuilder R = a.R("43 -- ");
        R.append(guildScheduledEvent.i());
        R.append(" -- ");
        R.append(str);
        this.key = R.toString();
    }

    public static /* synthetic */ GuildScheduledEventInviteEntry copy$default(GuildScheduledEventInviteEntry guildScheduledEventInviteEntry, ModelInvite modelInvite, String str, GuildScheduledEvent guildScheduledEvent, int i, Object obj) {
        if ((i & 1) != 0) {
            modelInvite = guildScheduledEventInviteEntry.invite;
        }
        if ((i & 2) != 0) {
            str = guildScheduledEventInviteEntry.inviteCode;
        }
        if ((i & 4) != 0) {
            guildScheduledEvent = guildScheduledEventInviteEntry.guildScheduledEvent;
        }
        return guildScheduledEventInviteEntry.copy(modelInvite, str, guildScheduledEvent);
    }

    public final ModelInvite component1() {
        return this.invite;
    }

    public final String component2() {
        return this.inviteCode;
    }

    public final GuildScheduledEvent component3() {
        return this.guildScheduledEvent;
    }

    public final GuildScheduledEventInviteEntry copy(ModelInvite modelInvite, String str, GuildScheduledEvent guildScheduledEvent) {
        m.checkNotNullParameter(modelInvite, "invite");
        m.checkNotNullParameter(str, "inviteCode");
        m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
        return new GuildScheduledEventInviteEntry(modelInvite, str, guildScheduledEvent);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildScheduledEventInviteEntry)) {
            return false;
        }
        GuildScheduledEventInviteEntry guildScheduledEventInviteEntry = (GuildScheduledEventInviteEntry) obj;
        return m.areEqual(this.invite, guildScheduledEventInviteEntry.invite) && m.areEqual(this.inviteCode, guildScheduledEventInviteEntry.inviteCode) && m.areEqual(this.guildScheduledEvent, guildScheduledEventInviteEntry.guildScheduledEvent);
    }

    public final GuildScheduledEvent getGuildScheduledEvent() {
        return this.guildScheduledEvent;
    }

    public final ModelInvite getInvite() {
        return this.invite;
    }

    public final String getInviteCode() {
        return this.inviteCode;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        ModelInvite modelInvite = this.invite;
        int i = 0;
        int hashCode = (modelInvite != null ? modelInvite.hashCode() : 0) * 31;
        String str = this.inviteCode;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
        if (guildScheduledEvent != null) {
            i = guildScheduledEvent.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("GuildScheduledEventInviteEntry(invite=");
        R.append(this.invite);
        R.append(", inviteCode=");
        R.append(this.inviteCode);
        R.append(", guildScheduledEvent=");
        R.append(this.guildScheduledEvent);
        R.append(")");
        return R.toString();
    }
}
