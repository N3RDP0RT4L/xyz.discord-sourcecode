package com.discord.widgets.chat.list.entries;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.domain.ModelInvite;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: StageInviteEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u0006\u0012\u0006\u0010\u0012\u001a\u00020\b\u0012\u0006\u0010\u0013\u001a\u00020\u000b\u0012\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u000e¢\u0006\u0004\b-\u0010.J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0014\u0010\u000f\u001a\u00060\u0002j\u0002`\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0005JN\u0010\u0015\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\u0011\u001a\u00060\u0002j\u0002`\u00062\b\b\u0002\u0010\u0012\u001a\u00020\b2\b\b\u0002\u0010\u0013\u001a\u00020\u000b2\f\b\u0002\u0010\u0014\u001a\u00060\u0002j\u0002`\u000eHÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0017\u0010\nJ\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001e\u001a\u00020\u001d2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\u0005R\u001c\u0010\"\u001a\u00020\u00188\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010\u001aR\u0019\u0010\u0012\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010%\u001a\u0004\b&\u0010\nR\u001d\u0010\u0014\u001a\u00060\u0002j\u0002`\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010 \u001a\u0004\b'\u0010\u0005R\u0019\u0010\u0013\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010(\u001a\u0004\b)\u0010\rR\u001c\u0010*\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b*\u0010%\u001a\u0004\b+\u0010\nR\u001d\u0010\u0011\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b,\u0010\u0005¨\u0006/"}, d2 = {"Lcom/discord/widgets/chat/list/entries/StageInviteEntry;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/primitives/MessageId;", "component2", "", "component3", "()Ljava/lang/String;", "Lcom/discord/models/domain/ModelInvite;", "component4", "()Lcom/discord/models/domain/ModelInvite;", "Lcom/discord/primitives/GuildId;", "component5", "userId", "messageId", "inviteCode", "invite", "guildId", "copy", "(JJLjava/lang/String;Lcom/discord/models/domain/ModelInvite;J)Lcom/discord/widgets/chat/list/entries/StageInviteEntry;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getUserId", "type", "I", "getType", "Ljava/lang/String;", "getInviteCode", "getGuildId", "Lcom/discord/models/domain/ModelInvite;", "getInvite", "key", "getKey", "getMessageId", HookHelper.constructorName, "(JJLjava/lang/String;Lcom/discord/models/domain/ModelInvite;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageInviteEntry extends ChatListEntry {
    private final long guildId;
    private final ModelInvite invite;
    private final String inviteCode;
    private final String key;
    private final long messageId;
    private final int type = 39;
    private final long userId;

    public StageInviteEntry(long j, long j2, String str, ModelInvite modelInvite, long j3) {
        m.checkNotNullParameter(str, "inviteCode");
        m.checkNotNullParameter(modelInvite, "invite");
        this.userId = j;
        this.messageId = j2;
        this.inviteCode = str;
        this.invite = modelInvite;
        this.guildId = j3;
        this.key = "39 -- " + j2 + " -- " + str;
    }

    public final long component1() {
        return this.userId;
    }

    public final long component2() {
        return this.messageId;
    }

    public final String component3() {
        return this.inviteCode;
    }

    public final ModelInvite component4() {
        return this.invite;
    }

    public final long component5() {
        return this.guildId;
    }

    public final StageInviteEntry copy(long j, long j2, String str, ModelInvite modelInvite, long j3) {
        m.checkNotNullParameter(str, "inviteCode");
        m.checkNotNullParameter(modelInvite, "invite");
        return new StageInviteEntry(j, j2, str, modelInvite, j3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StageInviteEntry)) {
            return false;
        }
        StageInviteEntry stageInviteEntry = (StageInviteEntry) obj;
        return this.userId == stageInviteEntry.userId && this.messageId == stageInviteEntry.messageId && m.areEqual(this.inviteCode, stageInviteEntry.inviteCode) && m.areEqual(this.invite, stageInviteEntry.invite) && this.guildId == stageInviteEntry.guildId;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final ModelInvite getInvite() {
        return this.invite;
    }

    public final String getInviteCode() {
        return this.inviteCode;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final long getMessageId() {
        return this.messageId;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public final long getUserId() {
        return this.userId;
    }

    public int hashCode() {
        int a = (b.a(this.messageId) + (b.a(this.userId) * 31)) * 31;
        String str = this.inviteCode;
        int i = 0;
        int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
        ModelInvite modelInvite = this.invite;
        if (modelInvite != null) {
            i = modelInvite.hashCode();
        }
        return b.a(this.guildId) + ((hashCode + i) * 31);
    }

    public String toString() {
        StringBuilder R = a.R("StageInviteEntry(userId=");
        R.append(this.userId);
        R.append(", messageId=");
        R.append(this.messageId);
        R.append(", inviteCode=");
        R.append(this.inviteCode);
        R.append(", invite=");
        R.append(this.invite);
        R.append(", guildId=");
        return a.B(R, this.guildId, ")");
    }
}
