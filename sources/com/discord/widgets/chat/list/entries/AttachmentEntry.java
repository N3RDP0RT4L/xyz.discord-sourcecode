package com.discord.widgets.chat.list.entries;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.message.attachment.MessageAttachment;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageState;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: AttachmentEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0018\b\u0086\b\u0018\u00002\u00020\u0001BU\u0012\u0006\u0010\u0019\u001a\u00020\u0005\u0012\n\u0010\u001a\u001a\u00060\bj\u0002`\t\u0012\u0006\u0010\u001b\u001a\u00020\f\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010\u001d\u001a\u00020\u0012\u0012\u0006\u0010\u001e\u001a\u00020\u0002\u0012\u0006\u0010\u001f\u001a\u00020\u0002\u0012\u0006\u0010 \u001a\u00020\u0002\u0012\u0006\u0010!\u001a\u00020\u0002¢\u0006\u0004\b>\u0010?J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0014\u0010\n\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0004J\u0010\u0010\u0016\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0004J\u0010\u0010\u0017\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0004J\u0010\u0010\u0018\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0004Jp\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00052\f\b\u0002\u0010\u001a\u001a\u00060\bj\u0002`\t2\b\b\u0002\u0010\u001b\u001a\u00020\f2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u000f2\b\b\u0002\u0010\u001d\u001a\u00020\u00122\b\b\u0002\u0010\u001e\u001a\u00020\u00022\b\b\u0002\u0010\u001f\u001a\u00020\u00022\b\b\u0002\u0010 \u001a\u00020\u00022\b\b\u0002\u0010!\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010%\u001a\u00020$HÖ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b'\u0010\u0007J\u001a\u0010*\u001a\u00020\u00022\b\u0010)\u001a\u0004\u0018\u00010(HÖ\u0003¢\u0006\u0004\b*\u0010+R\u0019\u0010\u0019\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010,\u001a\u0004\b-\u0010\u0007R\u001c\u0010.\u001a\u00020$8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b.\u0010/\u001a\u0004\b0\u0010&R\u0019\u0010\u001e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00101\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010!\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u00101\u001a\u0004\b!\u0010\u0004R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00102\u001a\u0004\b3\u0010\u0011R\u0019\u0010\u001b\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00104\u001a\u0004\b5\u0010\u000eR\u0019\u0010\u001d\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00106\u001a\u0004\b7\u0010\u0014R\u001d\u0010\u001a\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00108\u001a\u0004\b9\u0010\u000bR\u001c\u0010:\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b:\u0010,\u001a\u0004\b;\u0010\u0007R\u0019\u0010 \u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u00101\u001a\u0004\b<\u0010\u0004R\u0019\u0010\u001f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00101\u001a\u0004\b=\u0010\u0004¨\u0006@"}, d2 = {"Lcom/discord/widgets/chat/list/entries/AttachmentEntry;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "", "isInExpandedBlockedMessageChunk", "()Z", "", "component1", "()I", "", "Lcom/discord/primitives/GuildId;", "component2", "()J", "Lcom/discord/models/message/Message;", "component3", "()Lcom/discord/models/message/Message;", "Lcom/discord/stores/StoreMessageState$State;", "component4", "()Lcom/discord/stores/StoreMessageState$State;", "Lcom/discord/api/message/attachment/MessageAttachment;", "component5", "()Lcom/discord/api/message/attachment/MessageAttachment;", "component6", "component7", "component8", "component9", "embedIndex", "guildId", "message", "messageState", "attachment", "isBlockedExpanded", "allowAnimatedEmojis", "autoPlayGifs", "isThreadStarterMessage", "copy", "(IJLcom/discord/models/message/Message;Lcom/discord/stores/StoreMessageState$State;Lcom/discord/api/message/attachment/MessageAttachment;ZZZZ)Lcom/discord/widgets/chat/list/entries/AttachmentEntry;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getEmbedIndex", "key", "Ljava/lang/String;", "getKey", "Z", "Lcom/discord/stores/StoreMessageState$State;", "getMessageState", "Lcom/discord/models/message/Message;", "getMessage", "Lcom/discord/api/message/attachment/MessageAttachment;", "getAttachment", "J", "getGuildId", "type", "getType", "getAutoPlayGifs", "getAllowAnimatedEmojis", HookHelper.constructorName, "(IJLcom/discord/models/message/Message;Lcom/discord/stores/StoreMessageState$State;Lcom/discord/api/message/attachment/MessageAttachment;ZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AttachmentEntry extends ChatListEntry {
    private final boolean allowAnimatedEmojis;
    private final MessageAttachment attachment;
    private final boolean autoPlayGifs;
    private final int embedIndex;
    private final long guildId;
    private final boolean isBlockedExpanded;
    private final boolean isThreadStarterMessage;
    private final String key;
    private final Message message;
    private final StoreMessageState.State messageState;
    private final int type = 28;

    public AttachmentEntry(int i, long j, Message message, StoreMessageState.State state, MessageAttachment messageAttachment, boolean z2, boolean z3, boolean z4, boolean z5) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(messageAttachment, "attachment");
        this.embedIndex = i;
        this.guildId = j;
        this.message = message;
        this.messageState = state;
        this.attachment = messageAttachment;
        this.isBlockedExpanded = z2;
        this.allowAnimatedEmojis = z3;
        this.autoPlayGifs = z4;
        this.isThreadStarterMessage = z5;
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(message.getId());
        this.key = sb.toString();
    }

    public final int component1() {
        return this.embedIndex;
    }

    public final long component2() {
        return this.guildId;
    }

    public final Message component3() {
        return this.message;
    }

    public final StoreMessageState.State component4() {
        return this.messageState;
    }

    public final MessageAttachment component5() {
        return this.attachment;
    }

    public final boolean component6() {
        return this.isBlockedExpanded;
    }

    public final boolean component7() {
        return this.allowAnimatedEmojis;
    }

    public final boolean component8() {
        return this.autoPlayGifs;
    }

    public final boolean component9() {
        return this.isThreadStarterMessage;
    }

    public final AttachmentEntry copy(int i, long j, Message message, StoreMessageState.State state, MessageAttachment messageAttachment, boolean z2, boolean z3, boolean z4, boolean z5) {
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(messageAttachment, "attachment");
        return new AttachmentEntry(i, j, message, state, messageAttachment, z2, z3, z4, z5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AttachmentEntry)) {
            return false;
        }
        AttachmentEntry attachmentEntry = (AttachmentEntry) obj;
        return this.embedIndex == attachmentEntry.embedIndex && this.guildId == attachmentEntry.guildId && m.areEqual(this.message, attachmentEntry.message) && m.areEqual(this.messageState, attachmentEntry.messageState) && m.areEqual(this.attachment, attachmentEntry.attachment) && this.isBlockedExpanded == attachmentEntry.isBlockedExpanded && this.allowAnimatedEmojis == attachmentEntry.allowAnimatedEmojis && this.autoPlayGifs == attachmentEntry.autoPlayGifs && this.isThreadStarterMessage == attachmentEntry.isThreadStarterMessage;
    }

    public final boolean getAllowAnimatedEmojis() {
        return this.allowAnimatedEmojis;
    }

    public final MessageAttachment getAttachment() {
        return this.attachment;
    }

    public final boolean getAutoPlayGifs() {
        return this.autoPlayGifs;
    }

    public final int getEmbedIndex() {
        return this.embedIndex;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final Message getMessage() {
        return this.message;
    }

    public final StoreMessageState.State getMessageState() {
        return this.messageState;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        int a = (b.a(this.guildId) + (this.embedIndex * 31)) * 31;
        Message message = this.message;
        int i = 0;
        int hashCode = (a + (message != null ? message.hashCode() : 0)) * 31;
        StoreMessageState.State state = this.messageState;
        int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
        MessageAttachment messageAttachment = this.attachment;
        if (messageAttachment != null) {
            i = messageAttachment.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z2 = this.isBlockedExpanded;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z3 = this.allowAnimatedEmojis;
        if (z3) {
            z3 = true;
        }
        int i7 = z3 ? 1 : 0;
        int i8 = z3 ? 1 : 0;
        int i9 = (i6 + i7) * 31;
        boolean z4 = this.autoPlayGifs;
        if (z4) {
            z4 = true;
        }
        int i10 = z4 ? 1 : 0;
        int i11 = z4 ? 1 : 0;
        int i12 = (i9 + i10) * 31;
        boolean z5 = this.isThreadStarterMessage;
        if (!z5) {
            i3 = z5 ? 1 : 0;
        }
        return i12 + i3;
    }

    public final boolean isBlockedExpanded() {
        return this.isBlockedExpanded;
    }

    @Override // com.discord.widgets.chat.list.entries.ChatListEntry
    public boolean isInExpandedBlockedMessageChunk() {
        return this.isBlockedExpanded;
    }

    public final boolean isThreadStarterMessage() {
        return this.isThreadStarterMessage;
    }

    public String toString() {
        StringBuilder R = a.R("AttachmentEntry(embedIndex=");
        R.append(this.embedIndex);
        R.append(", guildId=");
        R.append(this.guildId);
        R.append(", message=");
        R.append(this.message);
        R.append(", messageState=");
        R.append(this.messageState);
        R.append(", attachment=");
        R.append(this.attachment);
        R.append(", isBlockedExpanded=");
        R.append(this.isBlockedExpanded);
        R.append(", allowAnimatedEmojis=");
        R.append(this.allowAnimatedEmojis);
        R.append(", autoPlayGifs=");
        R.append(this.autoPlayGifs);
        R.append(", isThreadStarterMessage=");
        return a.M(R, this.isThreadStarterMessage, ")");
    }
}
