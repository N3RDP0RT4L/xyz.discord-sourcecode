package com.discord.widgets.chat.list.entries;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: ThreadEmbedEntry.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u0000\n\u0002\b\u001b\b\u0086\b\u0018\u00002\u00020\u0001Bu\u0012\n\u0010\u001d\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u001e\u001a\u00020\u0006\u0012\u0006\u0010\u001f\u001a\u00020\t\u0012\b\u0010 \u001a\u0004\u0018\u00010\f\u0012\u0014\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f\u0012\u0016\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u000f\u0012\u0006\u0010#\u001a\u00020\u0016\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0019\u0012\u0006\u0010%\u001a\u00020\f¢\u0006\u0004\bD\u0010EJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u001e\u0010\u0011\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u000fHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0012J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u000eJ\u0090\u0001\u0010&\u001a\u00020\u00002\f\b\u0002\u0010\u001d\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u001e\u001a\u00020\u00062\b\b\u0002\u0010\u001f\u001a\u00020\t2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\f2\u0016\b\u0002\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f2\u0018\b\u0002\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u000f2\b\b\u0002\u0010#\u001a\u00020\u00162\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00192\b\b\u0002\u0010%\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b&\u0010'J\u0010\u0010(\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b(\u0010)J\u0010\u0010*\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b*\u0010\u000bJ\u001a\u0010-\u001a\u00020\u00162\b\u0010,\u001a\u0004\u0018\u00010+HÖ\u0003¢\u0006\u0004\b-\u0010.R)\u0010\"\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010/\u001a\u0004\b0\u0010\u0012R\u0019\u0010#\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b#\u00101\u001a\u0004\b2\u0010\u0018R\u0019\u0010\u001e\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00103\u001a\u0004\b4\u0010\bR\u001b\u0010$\u001a\u0004\u0018\u00010\u00198\u0006@\u0006¢\u0006\f\n\u0004\b$\u00105\u001a\u0004\b6\u0010\u001bR\u001c\u00107\u001a\u00020\t8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b7\u00108\u001a\u0004\b9\u0010\u000bR\u001d\u0010\u001d\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010:\u001a\u0004\b;\u0010\u0005R\u001c\u0010<\u001a\u00020\u00148\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b>\u0010)R\u0019\u0010\u001f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00108\u001a\u0004\b?\u0010\u000bR'\u0010!\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010/\u001a\u0004\b@\u0010\u0012R\u001b\u0010 \u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010A\u001a\u0004\bB\u0010\u000eR\u0019\u0010%\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010A\u001a\u0004\bC\u0010\u000e¨\u0006F"}, d2 = {"Lcom/discord/widgets/chat/list/entries/ThreadEmbedEntry;", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "", "Lcom/discord/primitives/MessageId;", "component1", "()J", "Lcom/discord/api/channel/Channel;", "component2", "()Lcom/discord/api/channel/Channel;", "", "component3", "()I", "Lcom/discord/models/message/Message;", "component4", "()Lcom/discord/models/message/Message;", "", "Lcom/discord/api/role/GuildRole;", "component5", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "", "component6", "", "component7", "()Z", "Lcom/discord/models/member/GuildMember;", "component8", "()Lcom/discord/models/member/GuildMember;", "component9", "messageId", "thread", "threadMessageCount", "mostRecentMessage", "roleMentions", "nickOrUsernames", "animateEmojis", "mostRecentMessageGuildMember", "parentMessage", "copy", "(JLcom/discord/api/channel/Channel;ILcom/discord/models/message/Message;Ljava/util/Map;Ljava/util/Map;ZLcom/discord/models/member/GuildMember;Lcom/discord/models/message/Message;)Lcom/discord/widgets/chat/list/entries/ThreadEmbedEntry;", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getNickOrUsernames", "Z", "getAnimateEmojis", "Lcom/discord/api/channel/Channel;", "getThread", "Lcom/discord/models/member/GuildMember;", "getMostRecentMessageGuildMember", "type", "I", "getType", "J", "getMessageId", "key", "Ljava/lang/String;", "getKey", "getThreadMessageCount", "getRoleMentions", "Lcom/discord/models/message/Message;", "getMostRecentMessage", "getParentMessage", HookHelper.constructorName, "(JLcom/discord/api/channel/Channel;ILcom/discord/models/message/Message;Ljava/util/Map;Ljava/util/Map;ZLcom/discord/models/member/GuildMember;Lcom/discord/models/message/Message;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ThreadEmbedEntry extends ChatListEntry {
    private final boolean animateEmojis;
    private final String key;
    private final long messageId;
    private final Message mostRecentMessage;
    private final GuildMember mostRecentMessageGuildMember;
    private final Map<Long, String> nickOrUsernames;
    private final Message parentMessage;
    private final Map<Long, GuildRole> roleMentions;
    private final Channel thread;
    private final int threadMessageCount;
    private final int type = 35;

    public ThreadEmbedEntry(long j, Channel channel, int i, Message message, Map<Long, GuildRole> map, Map<Long, String> map2, boolean z2, GuildMember guildMember, Message message2) {
        m.checkNotNullParameter(channel, "thread");
        m.checkNotNullParameter(map2, "nickOrUsernames");
        m.checkNotNullParameter(message2, "parentMessage");
        this.messageId = j;
        this.thread = channel;
        this.threadMessageCount = i;
        this.mostRecentMessage = message;
        this.roleMentions = map;
        this.nickOrUsernames = map2;
        this.animateEmojis = z2;
        this.mostRecentMessageGuildMember = guildMember;
        this.parentMessage = message2;
        this.key = a.s("35 -- ", j);
    }

    public final long component1() {
        return this.messageId;
    }

    public final Channel component2() {
        return this.thread;
    }

    public final int component3() {
        return this.threadMessageCount;
    }

    public final Message component4() {
        return this.mostRecentMessage;
    }

    public final Map<Long, GuildRole> component5() {
        return this.roleMentions;
    }

    public final Map<Long, String> component6() {
        return this.nickOrUsernames;
    }

    public final boolean component7() {
        return this.animateEmojis;
    }

    public final GuildMember component8() {
        return this.mostRecentMessageGuildMember;
    }

    public final Message component9() {
        return this.parentMessage;
    }

    public final ThreadEmbedEntry copy(long j, Channel channel, int i, Message message, Map<Long, GuildRole> map, Map<Long, String> map2, boolean z2, GuildMember guildMember, Message message2) {
        m.checkNotNullParameter(channel, "thread");
        m.checkNotNullParameter(map2, "nickOrUsernames");
        m.checkNotNullParameter(message2, "parentMessage");
        return new ThreadEmbedEntry(j, channel, i, message, map, map2, z2, guildMember, message2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThreadEmbedEntry)) {
            return false;
        }
        ThreadEmbedEntry threadEmbedEntry = (ThreadEmbedEntry) obj;
        return this.messageId == threadEmbedEntry.messageId && m.areEqual(this.thread, threadEmbedEntry.thread) && this.threadMessageCount == threadEmbedEntry.threadMessageCount && m.areEqual(this.mostRecentMessage, threadEmbedEntry.mostRecentMessage) && m.areEqual(this.roleMentions, threadEmbedEntry.roleMentions) && m.areEqual(this.nickOrUsernames, threadEmbedEntry.nickOrUsernames) && this.animateEmojis == threadEmbedEntry.animateEmojis && m.areEqual(this.mostRecentMessageGuildMember, threadEmbedEntry.mostRecentMessageGuildMember) && m.areEqual(this.parentMessage, threadEmbedEntry.parentMessage);
    }

    public final boolean getAnimateEmojis() {
        return this.animateEmojis;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final long getMessageId() {
        return this.messageId;
    }

    public final Message getMostRecentMessage() {
        return this.mostRecentMessage;
    }

    public final GuildMember getMostRecentMessageGuildMember() {
        return this.mostRecentMessageGuildMember;
    }

    public final Map<Long, String> getNickOrUsernames() {
        return this.nickOrUsernames;
    }

    public final Message getParentMessage() {
        return this.parentMessage;
    }

    public final Map<Long, GuildRole> getRoleMentions() {
        return this.roleMentions;
    }

    public final Channel getThread() {
        return this.thread;
    }

    public final int getThreadMessageCount() {
        return this.threadMessageCount;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        int a = b.a(this.messageId) * 31;
        Channel channel = this.thread;
        int i = 0;
        int hashCode = (((a + (channel != null ? channel.hashCode() : 0)) * 31) + this.threadMessageCount) * 31;
        Message message = this.mostRecentMessage;
        int hashCode2 = (hashCode + (message != null ? message.hashCode() : 0)) * 31;
        Map<Long, GuildRole> map = this.roleMentions;
        int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
        Map<Long, String> map2 = this.nickOrUsernames;
        int hashCode4 = (hashCode3 + (map2 != null ? map2.hashCode() : 0)) * 31;
        boolean z2 = this.animateEmojis;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = (hashCode4 + i2) * 31;
        GuildMember guildMember = this.mostRecentMessageGuildMember;
        int hashCode5 = (i4 + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
        Message message2 = this.parentMessage;
        if (message2 != null) {
            i = message2.hashCode();
        }
        return hashCode5 + i;
    }

    public String toString() {
        StringBuilder R = a.R("ThreadEmbedEntry(messageId=");
        R.append(this.messageId);
        R.append(", thread=");
        R.append(this.thread);
        R.append(", threadMessageCount=");
        R.append(this.threadMessageCount);
        R.append(", mostRecentMessage=");
        R.append(this.mostRecentMessage);
        R.append(", roleMentions=");
        R.append(this.roleMentions);
        R.append(", nickOrUsernames=");
        R.append(this.nickOrUsernames);
        R.append(", animateEmojis=");
        R.append(this.animateEmojis);
        R.append(", mostRecentMessageGuildMember=");
        R.append(this.mostRecentMessageGuildMember);
        R.append(", parentMessage=");
        R.append(this.parentMessage);
        R.append(")");
        return R.toString();
    }
}
