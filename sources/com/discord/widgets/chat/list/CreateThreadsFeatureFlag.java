package com.discord.widgets.chat.list;

import andhook.lib.HookHelper;
import com.discord.api.guild.GuildFeature;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func3;
/* compiled from: CreateThreadsFeatureFlag.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u001b\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e\u0012\b\b\u0002\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0019\u0010\t\u001a\u00020\u00062\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/chat/list/CreateThreadsFeatureFlag;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "", "observeEnabled", "(J)Lrx/Observable;", "isEnabled", "(J)Z", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/stores/StoreExperiments;", HookHelper.constructorName, "(Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreGuilds;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CreateThreadsFeatureFlag {
    public static final Companion Companion = new Companion(null);
    private static final Lazy INSTANCE$delegate = g.lazy(CreateThreadsFeatureFlag$Companion$INSTANCE$2.INSTANCE);
    private final StoreExperiments storeExperiments;
    private final StoreGuilds storeGuilds;

    /* compiled from: CreateThreadsFeatureFlag.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J+\u0010\b\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0004\u001a\u0004\u0018\u00010\u00022\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\b\u0010\tR\u001d\u0010\u000f\u001a\u00020\n8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/chat/list/CreateThreadsFeatureFlag$Companion;", "", "Lcom/discord/models/experiments/domain/Experiment;", "createExperiment", "rolloutExperiment", "Lcom/discord/models/guild/Guild;", "guild", "", "computeIsEnabled", "(Lcom/discord/models/experiments/domain/Experiment;Lcom/discord/models/experiments/domain/Experiment;Lcom/discord/models/guild/Guild;)Z", "Lcom/discord/widgets/chat/list/CreateThreadsFeatureFlag;", "INSTANCE$delegate", "Lkotlin/Lazy;", "getINSTANCE", "()Lcom/discord/widgets/chat/list/CreateThreadsFeatureFlag;", "INSTANCE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final boolean computeIsEnabled(Experiment experiment, Experiment experiment2, Guild guild) {
            if (guild == null) {
                return false;
            }
            if (guild.hasFeature(GuildFeature.THREADS_ENABLED)) {
                return true;
            }
            return ((experiment2 != null && experiment2.getBucket() == 2) || !guild.hasFeature(GuildFeature.COMMUNITY)) && experiment != null && experiment.getBucket() == 1;
        }

        public final CreateThreadsFeatureFlag getINSTANCE() {
            Lazy lazy = CreateThreadsFeatureFlag.INSTANCE$delegate;
            Companion companion = CreateThreadsFeatureFlag.Companion;
            return (CreateThreadsFeatureFlag) lazy.getValue();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public CreateThreadsFeatureFlag() {
        this(null, null, 3, null);
    }

    public CreateThreadsFeatureFlag(StoreExperiments storeExperiments, StoreGuilds storeGuilds) {
        m.checkNotNullParameter(storeExperiments, "storeExperiments");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        this.storeExperiments = storeExperiments;
        this.storeGuilds = storeGuilds;
    }

    public final boolean isEnabled(long j) {
        return Companion.computeIsEnabled(this.storeExperiments.getGuildExperiment("2020-09_threads", j, true), this.storeExperiments.getGuildExperiment("2021-06_threads_rollout", j, false), this.storeGuilds.getGuild(j));
    }

    public final Observable<Boolean> observeEnabled(long j) {
        Observable<Experiment> observeGuildExperiment = this.storeExperiments.observeGuildExperiment("2020-09_threads", j, true);
        Observable<Experiment> observeGuildExperiment2 = this.storeExperiments.observeGuildExperiment("2021-06_threads_rollout", j, false);
        Observable<Guild> observeGuild = this.storeGuilds.observeGuild(j);
        final CreateThreadsFeatureFlag$observeEnabled$1 createThreadsFeatureFlag$observeEnabled$1 = new CreateThreadsFeatureFlag$observeEnabled$1(Companion);
        Observable<Boolean> i = Observable.i(observeGuildExperiment, observeGuildExperiment2, observeGuild, new Func3() { // from class: com.discord.widgets.chat.list.CreateThreadsFeatureFlag$sam$rx_functions_Func3$0
            @Override // rx.functions.Func3
            public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3) {
                return Function3.this.invoke(obj, obj2, obj3);
            }
        });
        m.checkNotNullExpressionValue(i, "Observable.combineLatest… ::computeIsEnabled\n    )");
        return i;
    }

    public /* synthetic */ CreateThreadsFeatureFlag(StoreExperiments storeExperiments, StoreGuilds storeGuilds, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getExperiments() : storeExperiments, (i & 2) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds);
    }
}
