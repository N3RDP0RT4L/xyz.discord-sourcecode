package com.discord.widgets.chat.list;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: NewThreadsPermissionsFeatureFlag.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/chat/list/NewThreadsPermissionsFeatureFlag;", "invoke", "()Lcom/discord/widgets/chat/list/NewThreadsPermissionsFeatureFlag;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NewThreadsPermissionsFeatureFlag$Companion$INSTANCE$2 extends o implements Function0<NewThreadsPermissionsFeatureFlag> {
    public static final NewThreadsPermissionsFeatureFlag$Companion$INSTANCE$2 INSTANCE = new NewThreadsPermissionsFeatureFlag$Companion$INSTANCE$2();

    public NewThreadsPermissionsFeatureFlag$Companion$INSTANCE$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final NewThreadsPermissionsFeatureFlag invoke() {
        return new NewThreadsPermissionsFeatureFlag(null, null, 3, null);
    }
}
