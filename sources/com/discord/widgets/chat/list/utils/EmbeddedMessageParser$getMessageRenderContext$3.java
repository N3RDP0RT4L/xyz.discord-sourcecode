package com.discord.widgets.chat.list.utils;

import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.discord.widgets.chat.list.utils.EmbeddedMessageParser;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: EmbeddedMessageParser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "userId", "", "invoke", "(J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EmbeddedMessageParser$getMessageRenderContext$3 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ EmbeddedMessageParser.ParserData $parserData;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EmbeddedMessageParser$getMessageRenderContext$3(EmbeddedMessageParser.ParserData parserData) {
        super(1);
        this.$parserData = parserData;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    public final void invoke(long j) {
        WidgetChatListAdapter.Data data = this.$parserData.getAdapter().getData();
        this.$parserData.getAdapter().getEventHandler().onUserMentionClicked(j, data.getChannelId(), data.getGuildId());
    }
}
