package com.discord.widgets.chat.list.utils;

import andhook.lib.HookHelper;
import android.content.Context;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.api.user.User;
import com.discord.models.message.Message;
import com.discord.stores.StoreMessageState;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.textprocessing.DiscordParser;
import com.discord.utilities.textprocessing.MessagePreprocessor;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.z.d.m;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.text.Regex;
import xyz.discord.R;
/* compiled from: EmbeddedMessageParser.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0017B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J3\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0015\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/chat/list/utils/EmbeddedMessageParser;", "", "Lcom/discord/widgets/chat/list/utils/EmbeddedMessageParser$ParserData;", "parserData", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "getMessageRenderContext", "(Lcom/discord/widgets/chat/list/utils/EmbeddedMessageParser$ParserData;)Lcom/discord/utilities/textprocessing/MessageRenderContext;", "", "userId", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/stores/StoreMessageState$State;", "messageState", "", "maxNodes", "Lcom/discord/utilities/textprocessing/MessagePreprocessor;", "getMessagePreprocessor", "(JLcom/discord/models/message/Message;Lcom/discord/stores/StoreMessageState$State;Ljava/lang/Integer;)Lcom/discord/utilities/textprocessing/MessagePreprocessor;", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "parse", "(Lcom/discord/widgets/chat/list/utils/EmbeddedMessageParser$ParserData;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", HookHelper.constructorName, "()V", "ParserData", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EmbeddedMessageParser {
    public static final EmbeddedMessageParser INSTANCE = new EmbeddedMessageParser();

    /* compiled from: EmbeddedMessageParser.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b%\b\u0086\b\u0018\u00002\u00020\u0001Bi\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\u0014\u0010\u001d\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005\u0012\u0016\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b0\u0005\u0012\u0006\u0010\u001f\u001a\u00020\r\u0012\b\u0010 \u001a\u0004\u0018\u00010\u0010\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010\"\u001a\u00020\u0016\u0012\u0006\u0010#\u001a\u00020\u0019¢\u0006\u0004\b<\u0010=J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001e\u0010\b\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ \u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b0\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\tJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u001bJ\u0082\u0001\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u001c\u001a\u00020\u00022\u0016\b\u0002\u0010\u001d\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00052\u0018\b\u0002\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b0\u00052\b\b\u0002\u0010\u001f\u001a\u00020\r2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u00132\b\b\u0002\u0010\"\u001a\u00020\u00162\b\b\u0002\u0010#\u001a\u00020\u0019HÆ\u0001¢\u0006\u0004\b$\u0010%J\u0010\u0010&\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b&\u0010'J\u0010\u0010(\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b(\u0010)J\u001a\u0010+\u001a\u00020\r2\b\u0010*\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b+\u0010,R\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010-\u001a\u0004\b.\u0010\u0004R)\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010/\u001a\u0004\b0\u0010\tR\u0019\u0010\u001f\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00101\u001a\u0004\b2\u0010\u000fR\u0019\u0010\"\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00103\u001a\u0004\b4\u0010\u0018R\u001b\u0010 \u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b \u00105\u001a\u0004\b6\u0010\u0012R\u0019\u0010#\u001a\u00020\u00198\u0006@\u0006¢\u0006\f\n\u0004\b#\u00107\u001a\u0004\b8\u0010\u001bR'\u0010\u001d\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010/\u001a\u0004\b9\u0010\tR\u001b\u0010!\u001a\u0004\u0018\u00010\u00138\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010:\u001a\u0004\b;\u0010\u0015¨\u0006>"}, d2 = {"Lcom/discord/widgets/chat/list/utils/EmbeddedMessageParser$ParserData;", "", "Landroid/content/Context;", "component1", "()Landroid/content/Context;", "", "", "Lcom/discord/api/role/GuildRole;", "component2", "()Ljava/util/Map;", "Lcom/discord/primitives/UserId;", "", "component3", "", "component4", "()Z", "Lcom/discord/stores/StoreMessageState$State;", "component5", "()Lcom/discord/stores/StoreMessageState$State;", "", "component6", "()Ljava/lang/Integer;", "Lcom/discord/models/message/Message;", "component7", "()Lcom/discord/models/message/Message;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "component8", "()Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "context", "roleMentions", "nickOrUsernames", "animateEmojis", "messageState", "maxNodes", "message", "adapter", "copy", "(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;ZLcom/discord/stores/StoreMessageState$State;Ljava/lang/Integer;Lcom/discord/models/message/Message;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)Lcom/discord/widgets/chat/list/utils/EmbeddedMessageParser$ParserData;", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Landroid/content/Context;", "getContext", "Ljava/util/Map;", "getNickOrUsernames", "Z", "getAnimateEmojis", "Lcom/discord/models/message/Message;", "getMessage", "Lcom/discord/stores/StoreMessageState$State;", "getMessageState", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;", "getAdapter", "getRoleMentions", "Ljava/lang/Integer;", "getMaxNodes", HookHelper.constructorName, "(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;ZLcom/discord/stores/StoreMessageState$State;Ljava/lang/Integer;Lcom/discord/models/message/Message;Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ParserData {
        private final WidgetChatListAdapter adapter;
        private final boolean animateEmojis;
        private final Context context;
        private final Integer maxNodes;
        private final Message message;
        private final StoreMessageState.State messageState;
        private final Map<Long, String> nickOrUsernames;
        private final Map<Long, GuildRole> roleMentions;

        public ParserData(Context context, Map<Long, GuildRole> map, Map<Long, String> map2, boolean z2, StoreMessageState.State state, Integer num, Message message, WidgetChatListAdapter widgetChatListAdapter) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(map2, "nickOrUsernames");
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(widgetChatListAdapter, "adapter");
            this.context = context;
            this.roleMentions = map;
            this.nickOrUsernames = map2;
            this.animateEmojis = z2;
            this.messageState = state;
            this.maxNodes = num;
            this.message = message;
            this.adapter = widgetChatListAdapter;
        }

        public final Context component1() {
            return this.context;
        }

        public final Map<Long, GuildRole> component2() {
            return this.roleMentions;
        }

        public final Map<Long, String> component3() {
            return this.nickOrUsernames;
        }

        public final boolean component4() {
            return this.animateEmojis;
        }

        public final StoreMessageState.State component5() {
            return this.messageState;
        }

        public final Integer component6() {
            return this.maxNodes;
        }

        public final Message component7() {
            return this.message;
        }

        public final WidgetChatListAdapter component8() {
            return this.adapter;
        }

        public final ParserData copy(Context context, Map<Long, GuildRole> map, Map<Long, String> map2, boolean z2, StoreMessageState.State state, Integer num, Message message, WidgetChatListAdapter widgetChatListAdapter) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(map2, "nickOrUsernames");
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(widgetChatListAdapter, "adapter");
            return new ParserData(context, map, map2, z2, state, num, message, widgetChatListAdapter);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ParserData)) {
                return false;
            }
            ParserData parserData = (ParserData) obj;
            return m.areEqual(this.context, parserData.context) && m.areEqual(this.roleMentions, parserData.roleMentions) && m.areEqual(this.nickOrUsernames, parserData.nickOrUsernames) && this.animateEmojis == parserData.animateEmojis && m.areEqual(this.messageState, parserData.messageState) && m.areEqual(this.maxNodes, parserData.maxNodes) && m.areEqual(this.message, parserData.message) && m.areEqual(this.adapter, parserData.adapter);
        }

        public final WidgetChatListAdapter getAdapter() {
            return this.adapter;
        }

        public final boolean getAnimateEmojis() {
            return this.animateEmojis;
        }

        public final Context getContext() {
            return this.context;
        }

        public final Integer getMaxNodes() {
            return this.maxNodes;
        }

        public final Message getMessage() {
            return this.message;
        }

        public final StoreMessageState.State getMessageState() {
            return this.messageState;
        }

        public final Map<Long, String> getNickOrUsernames() {
            return this.nickOrUsernames;
        }

        public final Map<Long, GuildRole> getRoleMentions() {
            return this.roleMentions;
        }

        public int hashCode() {
            Context context = this.context;
            int i = 0;
            int hashCode = (context != null ? context.hashCode() : 0) * 31;
            Map<Long, GuildRole> map = this.roleMentions;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, String> map2 = this.nickOrUsernames;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            boolean z2 = this.animateEmojis;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode3 + i2) * 31;
            StoreMessageState.State state = this.messageState;
            int hashCode4 = (i4 + (state != null ? state.hashCode() : 0)) * 31;
            Integer num = this.maxNodes;
            int hashCode5 = (hashCode4 + (num != null ? num.hashCode() : 0)) * 31;
            Message message = this.message;
            int hashCode6 = (hashCode5 + (message != null ? message.hashCode() : 0)) * 31;
            WidgetChatListAdapter widgetChatListAdapter = this.adapter;
            if (widgetChatListAdapter != null) {
                i = widgetChatListAdapter.hashCode();
            }
            return hashCode6 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ParserData(context=");
            R.append(this.context);
            R.append(", roleMentions=");
            R.append(this.roleMentions);
            R.append(", nickOrUsernames=");
            R.append(this.nickOrUsernames);
            R.append(", animateEmojis=");
            R.append(this.animateEmojis);
            R.append(", messageState=");
            R.append(this.messageState);
            R.append(", maxNodes=");
            R.append(this.maxNodes);
            R.append(", message=");
            R.append(this.message);
            R.append(", adapter=");
            R.append(this.adapter);
            R.append(")");
            return R.toString();
        }
    }

    private EmbeddedMessageParser() {
    }

    private final MessagePreprocessor getMessagePreprocessor(long j, Message message, StoreMessageState.State state, Integer num) {
        StoreUserSettings userSettings = StoreStream.Companion.getUserSettings();
        return new MessagePreprocessor(j, state, (!userSettings.getIsEmbedMediaInlined() || !userSettings.getIsRenderEmbedsEnabled()) ? null : message.getEmbeds(), false, num);
    }

    private final MessageRenderContext getMessageRenderContext(ParserData parserData) {
        return new MessageRenderContext(parserData.getContext(), parserData.getAdapter().getData().getUserId(), parserData.getAnimateEmojis(), parserData.getNickOrUsernames(), parserData.getAdapter().getData().getChannelNames(), parserData.getRoleMentions(), R.attr.colorTextLink, EmbeddedMessageParser$getMessageRenderContext$1.INSTANCE, new EmbeddedMessageParser$getMessageRenderContext$2(parserData), ColorCompat.getThemedColor(parserData.getContext(), (int) R.attr.theme_chat_spoiler_bg), ColorCompat.getThemedColor(parserData.getContext(), (int) R.attr.theme_chat_spoiler_bg_visible), null, new EmbeddedMessageParser$getMessageRenderContext$3(parserData), new EmbeddedMessageParser$getMessageRenderContext$4(parserData));
    }

    public final DraweeSpanStringBuilder parse(ParserData parserData) {
        m.checkNotNullParameter(parserData, "parserData");
        MessageRenderContext messageRenderContext = getMessageRenderContext(parserData);
        User author = parserData.getMessage().getAuthor();
        MessagePreprocessor messagePreprocessor = getMessagePreprocessor(author != null ? author.i() : 0L, parserData.getMessage(), parserData.getMessageState(), parserData.getMaxNodes());
        String content = parserData.getMessage().getContent();
        if (content == null) {
            content = "";
        }
        Context context = parserData.getContext();
        String property = System.getProperty("line.separator");
        Objects.requireNonNull(property);
        m.checkNotNullExpressionValue(property, "Objects.requireNonNull(S…operty(\"line.separator\"))");
        return DiscordParser.parseChannelMessage(context, new Regex(property).replace(content, " "), messageRenderContext, messagePreprocessor, DiscordParser.ParserOptions.REPLY, false);
    }
}
