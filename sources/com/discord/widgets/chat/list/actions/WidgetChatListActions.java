package com.discord.widgets.chat.list.actions;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.MainThread;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.thread.ThreadMetadata;
import com.discord.api.user.User;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetChatListActionsBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.EmojiSet;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.permissions.ManageMessageContext;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.textprocessing.MessageUnparser;
import com.discord.utilities.view.recycler.PaddedItemDecorator;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.list.actions.WidgetChatListActions;
import com.discord.widgets.notice.WidgetNoticeDialog;
import d0.o;
import d0.t.g0;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func3;
import rx.functions.Func5;
import xyz.discord.R;
/* compiled from: WidgetChatListActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 =2\u00020\u0001:\u0002=>B\u0007¢\u0006\u0004\b<\u0010)J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\b\u0010\u0006J\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\r\u0010\fJ\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0003¢\u0006\u0004\b\u000e\u0010\fJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0013\u0010\fJ\u001f\u0010\u0016\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J-\u0010\u001d\u001a\u00020\u00042\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00182\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010 \u001a\u00020\u001fH\u0016¢\u0006\u0004\b \u0010!J!\u0010&\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\"2\b\u0010%\u001a\u0004\u0018\u00010$H\u0016¢\u0006\u0004\b&\u0010'J\u000f\u0010(\u001a\u00020\u0004H\u0016¢\u0006\u0004\b(\u0010)R\u001a\u0010,\u001a\u00060*j\u0002`+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u0010/\u001a\u00020.8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b/\u00100R\u001a\u00102\u001a\u00060*j\u0002`18\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b2\u0010-R\u001d\u00108\u001a\u0002038B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b4\u00105\u001a\u0004\b6\u00107R\u0018\u0010:\u001a\u0004\u0018\u0001098\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u0010;¨\u0006?"}, d2 = {"Lcom/discord/widgets/chat/list/actions/WidgetChatListActions;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Model;", "data", "", "configureUI", "(Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Model;)V", "model", "removeAllReactions", "Lcom/discord/models/message/Message;", "message", "confirmPublishMessage", "(Lcom/discord/models/message/Message;)V", "deleteMessage", "toggleMessagePin", "Lcom/discord/models/domain/emoji/Emoji;", "emoji", "addReaction", "(Lcom/discord/models/domain/emoji/Emoji;)V", "editMessage", "Lcom/discord/api/channel/Channel;", "channel", "replyMessage", "(Lcom/discord/models/message/Message;Lcom/discord/api/channel/Channel;)V", "", "recentEmojis", "", "isLocalMessage", "canAddReactions", "configureAddReactionEmojisList", "(Ljava/util/List;ZZ)V", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "()V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "J", "Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;", "adapter", "Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;", "Lcom/discord/primitives/MessageId;", "messageId", "Lcom/discord/databinding/WidgetChatListActionsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChatListActionsBinding;", "binding", "Lcom/discord/utilities/view/recycler/PaddedItemDecorator;", "itemDecorator", "Lcom/discord/utilities/view/recycler/PaddedItemDecorator;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListActions extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChatListActions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChatListActionsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_MESSAGE_CHANNEL_ID = "INTENT_EXTRA_MESSAGE_CHANNEL_ID";
    private static final String INTENT_EXTRA_MESSAGE_CONTENT = "INTENT_EXTRA_MESSAGE_CONTENT";
    private static final String INTENT_EXTRA_MESSAGE_ID = "INTENT_EXTRA_MESSAGE_ID";
    private static final String INTENT_EXTRA_TYPE = "INTENT_EXTRA_TYPE";
    private static final int TYPE_CHAT = 0;
    private static final int TYPE_PINS = 1;
    private WidgetChatListActionsEmojisAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChatListActions$binding$2.INSTANCE, null, 2, null);
    private long channelId;
    private PaddedItemDecorator itemDecorator;
    private long messageId;

    /* compiled from: WidgetChatListActions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018J/\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bJ/\u0010\f\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\f\u0010\u000bR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000fR\u0016\u0010\u0012\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u000fR\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0016\u001a\u00020\u00138\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "channelId", "messageId", "", "messageContent", "", "showForChat", "(Landroidx/fragment/app/FragmentManager;JJLjava/lang/CharSequence;)V", "showForPin", "", WidgetChatListActions.INTENT_EXTRA_MESSAGE_CHANNEL_ID, "Ljava/lang/String;", WidgetChatListActions.INTENT_EXTRA_MESSAGE_CONTENT, WidgetChatListActions.INTENT_EXTRA_MESSAGE_ID, WidgetChatListActions.INTENT_EXTRA_TYPE, "", "TYPE_CHAT", "I", "TYPE_PINS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void showForChat(FragmentManager fragmentManager, long j, long j2, CharSequence charSequence) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(charSequence, "messageContent");
            WidgetChatListActions widgetChatListActions = new WidgetChatListActions();
            Bundle I = a.I(WidgetChatListActions.INTENT_EXTRA_MESSAGE_CHANNEL_ID, j);
            I.putLong(WidgetChatListActions.INTENT_EXTRA_MESSAGE_ID, j2);
            I.putCharSequence(WidgetChatListActions.INTENT_EXTRA_MESSAGE_CONTENT, charSequence);
            I.putInt(WidgetChatListActions.INTENT_EXTRA_TYPE, 0);
            widgetChatListActions.setArguments(I);
            widgetChatListActions.show(fragmentManager, WidgetChatListActions.class.getName());
        }

        public final void showForPin(FragmentManager fragmentManager, long j, long j2, CharSequence charSequence) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(charSequence, "messageContent");
            Bundle bundle = new Bundle();
            bundle.putLong(WidgetChatListActions.INTENT_EXTRA_MESSAGE_CHANNEL_ID, j);
            bundle.putLong(WidgetChatListActions.INTENT_EXTRA_MESSAGE_ID, j2);
            bundle.putCharSequence(WidgetChatListActions.INTENT_EXTRA_MESSAGE_CONTENT, charSequence);
            bundle.putInt(WidgetChatListActions.INTENT_EXTRA_TYPE, 1);
            WidgetChatListActions widgetChatListActions = new WidgetChatListActions();
            widgetChatListActions.setArguments(bundle);
            widgetChatListActions.show(fragmentManager, WidgetChatListActions.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChatListActions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b-\b\u0086\b\u0018\u0000 N2\u00020\u0001:\u0001NBs\u0012\u0006\u0010%\u001a\u00020\u0002\u0012\b\u0010&\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010'\u001a\u00020\b\u0012\b\u0010(\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010)\u001a\u00020\u000e\u0012\u0006\u0010*\u001a\u00020\u0011\u0012\u0006\u0010+\u001a\u00020\u0014\u0012\f\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017\u0012\b\u0010-\u001a\u0004\u0018\u00010\u001b\u0012\u000e\u0010.\u001a\n\u0018\u00010\u001ej\u0004\u0018\u0001`\u001f\u0012\u0006\u0010/\u001a\u00020\"¢\u0006\u0004\bL\u0010MJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0016\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÆ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0018\u0010 \u001a\n\u0018\u00010\u001ej\u0004\u0018\u0001`\u001fHÆ\u0003¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b#\u0010$J\u0092\u0001\u00100\u001a\u00020\u00002\b\b\u0002\u0010%\u001a\u00020\u00022\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010'\u001a\u00020\b2\n\b\u0002\u0010(\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010)\u001a\u00020\u000e2\b\b\u0002\u0010*\u001a\u00020\u00112\b\b\u0002\u0010+\u001a\u00020\u00142\u000e\b\u0002\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\n\b\u0002\u0010-\u001a\u0004\u0018\u00010\u001b2\u0010\b\u0002\u0010.\u001a\n\u0018\u00010\u001ej\u0004\u0018\u0001`\u001f2\b\b\u0002\u0010/\u001a\u00020\"HÆ\u0001¢\u0006\u0004\b0\u00101J\u0010\u00102\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b2\u0010\nJ\u0010\u00103\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b3\u0010\u0013J\u001a\u00105\u001a\u00020\u00142\b\u00104\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b5\u00106R\u0019\u0010%\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b%\u00107\u001a\u0004\b8\u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b&\u00109\u001a\u0004\b:\u0010\u0007R\u0019\u0010/\u001a\u00020\"8\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010;\u001a\u0004\b<\u0010$R\u0019\u0010+\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010=\u001a\u0004\b+\u0010\u0016R\u001f\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00180\u00178\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010>\u001a\u0004\b?\u0010\u001aR\u001b\u0010(\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010@\u001a\u0004\bA\u0010\rR\u0019\u0010*\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010B\u001a\u0004\bC\u0010\u0013R\u0019\u0010)\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010D\u001a\u0004\bE\u0010\u0010R!\u0010.\u001a\n\u0018\u00010\u001ej\u0004\u0018\u0001`\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010F\u001a\u0004\bG\u0010!R\u001b\u0010-\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010H\u001a\u0004\bI\u0010\u001dR\u0019\u0010'\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010J\u001a\u0004\bK\u0010\n¨\u0006O"}, d2 = {"Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Model;", "", "Lcom/discord/models/message/Message;", "component1", "()Lcom/discord/models/message/Message;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "", "component3", "()Ljava/lang/String;", "", "component4", "()Ljava/lang/CharSequence;", "Lcom/discord/utilities/permissions/ManageMessageContext;", "component5", "()Lcom/discord/utilities/permissions/ManageMessageContext;", "", "component6", "()I", "", "component7", "()Z", "", "Lcom/discord/models/domain/emoji/Emoji;", "component8", "()Ljava/util/List;", "Lcom/discord/api/channel/Channel;", "component9", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/api/permission/PermissionBit;", "component10", "()Ljava/lang/Long;", "Lcom/discord/models/user/MeUser;", "component11", "()Lcom/discord/models/user/MeUser;", "message", "guild", "messageAuthorName", "messageContent", "manageMessageContext", "type", "isDeveloper", "recentEmojis", "channel", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "me", "copy", "(Lcom/discord/models/message/Message;Lcom/discord/models/guild/Guild;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/discord/utilities/permissions/ManageMessageContext;IZLjava/util/List;Lcom/discord/api/channel/Channel;Ljava/lang/Long;Lcom/discord/models/user/MeUser;)Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Model;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/message/Message;", "getMessage", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/models/user/MeUser;", "getMe", "Z", "Ljava/util/List;", "getRecentEmojis", "Ljava/lang/CharSequence;", "getMessageContent", "I", "getType", "Lcom/discord/utilities/permissions/ManageMessageContext;", "getManageMessageContext", "Ljava/lang/Long;", "getPermissions", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/lang/String;", "getMessageAuthorName", HookHelper.constructorName, "(Lcom/discord/models/message/Message;Lcom/discord/models/guild/Guild;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/discord/utilities/permissions/ManageMessageContext;IZLjava/util/List;Lcom/discord/api/channel/Channel;Ljava/lang/Long;Lcom/discord/models/user/MeUser;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Channel channel;
        private final Guild guild;
        private final boolean isDeveloper;
        private final ManageMessageContext manageMessageContext;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2825me;
        private final Message message;
        private final String messageAuthorName;
        private final CharSequence messageContent;
        private final Long permissions;
        private final List<Emoji> recentEmojis;
        private final int type;

        /* compiled from: WidgetChatListActions.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001f\u0010 Jk\u0010\u0016\u001a\u0004\u0018\u00010\u00152\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u000e\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\r2\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J?\u0010\u001d\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00150\u001c2\n\u0010\u0019\u001a\u00060\u0006j\u0002`\u00182\n\u0010\u001b\u001a\u00060\u0006j\u0002`\u001a2\b\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u001d\u0010\u001e¨\u0006!"}, d2 = {"Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Model$Companion;", "", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/models/guild/Guild;", "guild", "", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "Lcom/discord/models/user/MeUser;", "meUser", "Lcom/discord/models/member/GuildMember;", "member", "Lcom/discord/api/channel/Channel;", "channel", "", "messageContent", "", "type", "Lcom/discord/models/domain/emoji/EmojiSet;", "emojis", "Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Model;", "create", "(Lcom/discord/models/message/Message;Lcom/discord/models/guild/Guild;Ljava/lang/Long;Lcom/discord/models/user/MeUser;Lcom/discord/models/member/GuildMember;Lcom/discord/api/channel/Channel;Ljava/lang/CharSequence;ILcom/discord/models/domain/emoji/EmojiSet;)Lcom/discord/widgets/chat/list/actions/WidgetChatListActions$Model;", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "messageId", "Lrx/Observable;", "get", "(JJLjava/lang/CharSequence;I)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Model create(Message message, Guild guild, Long l, MeUser meUser, GuildMember guildMember, Channel channel, CharSequence charSequence, int i, EmojiSet emojiSet) {
                Integer num;
                String nick;
                ThreadMetadata y2;
                String str = null;
                if (message == null) {
                    return null;
                }
                boolean z2 = channel != null && ChannelUtils.x(channel);
                boolean z3 = channel != null && ChannelUtils.A(channel);
                boolean z4 = (channel == null || (y2 = channel.y()) == null || !y2.b()) ? false : true;
                ManageMessageContext.Companion companion = ManageMessageContext.Companion;
                if (guild != null) {
                    num = Integer.valueOf(guild.getMfaLevel());
                } else {
                    num = null;
                }
                ManageMessageContext from = companion.from(message, l, meUser, num, z2, z3, z4);
                if (guildMember == null || (nick = guildMember.getNick()) == null) {
                    User author = message.getAuthor();
                    if (author != null) {
                        str = author.r();
                    }
                } else {
                    str = nick;
                }
                if (str == null) {
                    str = "";
                }
                String str2 = str;
                boolean isDeveloperMode = StoreStream.Companion.getUserSettings().getIsDeveloperMode();
                List<Emoji> list = emojiSet.recentEmojis;
                m.checkNotNullExpressionValue(list, "emojis.recentEmojis");
                return new Model(message, guild, str2, charSequence, from, i, isDeveloperMode, list, channel, l, meUser);
            }

            public final Observable<Model> get(final long j, long j2, final CharSequence charSequence, final int i) {
                Observable<Message> observable;
                if (i == 0) {
                    observable = StoreStream.Companion.getMessages().observeMessagesForChannel(j, j2);
                } else if (i != 1) {
                    observable = new k<>(null);
                } else {
                    observable = StoreStream.Companion.getPinnedMessages().observePinnedMessage(j, j2);
                }
                Observable<Model> Y = Observable.j(observable, StoreStream.Companion.getChannels().observeChannel(j), WidgetChatListActions$Model$Companion$get$1.INSTANCE).Y(new b<Pair<? extends Message, ? extends Channel>, Observable<? extends Model>>() { // from class: com.discord.widgets.chat.list.actions.WidgetChatListActions$Model$Companion$get$2
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Observable<? extends WidgetChatListActions.Model> call(Pair<? extends Message, ? extends Channel> pair) {
                        return call2((Pair<Message, Channel>) pair);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Observable<? extends WidgetChatListActions.Model> call2(Pair<Message, Channel> pair) {
                        Observable emojiSet;
                        User author;
                        final Message component1 = pair.component1();
                        final Channel component2 = pair.component2();
                        final long i2 = (component1 == null || (author = component1.getAuthor()) == null) ? 0L : author.i();
                        if (component2 == null) {
                            return new k(null);
                        }
                        StoreStream.Companion companion = StoreStream.Companion;
                        Observable<Long> observePermissionsForChannel = companion.getPermissions().observePermissionsForChannel(j);
                        Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
                        Observable<R> F = companion.getGuilds().observeComputed(component2.f(), d0.t.m.listOf(Long.valueOf(i2))).F(new b<Map<Long, ? extends GuildMember>, GuildMember>() { // from class: com.discord.widgets.chat.list.actions.WidgetChatListActions$Model$Companion$get$2.1
                            @Override // j0.k.b
                            public /* bridge */ /* synthetic */ GuildMember call(Map<Long, ? extends GuildMember> map) {
                                return call2((Map<Long, GuildMember>) map);
                            }

                            /* renamed from: call  reason: avoid collision after fix types in other method */
                            public final GuildMember call2(Map<Long, GuildMember> map) {
                                return map.get(Long.valueOf(i2));
                            }
                        });
                        Observable<Guild> q = companion.getGuilds().observeGuild(component2.f()).q();
                        emojiSet = companion.getEmojis().getEmojiSet(component2.f(), component2.h(), (r16 & 4) != 0 ? false : false, (r16 & 8) != 0 ? false : false);
                        return Observable.g(observePermissionsForChannel, observeMe$default, F, q, emojiSet, new Func5<Long, MeUser, GuildMember, Guild, EmojiSet, WidgetChatListActions.Model>() { // from class: com.discord.widgets.chat.list.actions.WidgetChatListActions$Model$Companion$get$2.2
                            public final WidgetChatListActions.Model call(Long l, MeUser meUser, GuildMember guildMember, Guild guild, EmojiSet emojiSet2) {
                                WidgetChatListActions.Model create;
                                WidgetChatListActions.Model.Companion companion2 = WidgetChatListActions.Model.Companion;
                                Message message = component1;
                                m.checkNotNullExpressionValue(meUser, "meUser");
                                Channel channel = component2;
                                WidgetChatListActions$Model$Companion$get$2 widgetChatListActions$Model$Companion$get$2 = WidgetChatListActions$Model$Companion$get$2.this;
                                CharSequence charSequence2 = charSequence;
                                int i3 = i;
                                m.checkNotNullExpressionValue(emojiSet2, "emojis");
                                create = companion2.create(message, guild, l, meUser, guildMember, channel, charSequence2, i3, emojiSet2);
                                return create;
                            }
                        });
                    }
                });
                m.checkNotNullExpressionValue(Y, "Observable\n            .…          }\n            }");
                return Y;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(Message message, Guild guild, String str, CharSequence charSequence, ManageMessageContext manageMessageContext, int i, boolean z2, List<? extends Emoji> list, Channel channel, Long l, MeUser meUser) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(str, "messageAuthorName");
            m.checkNotNullParameter(manageMessageContext, "manageMessageContext");
            m.checkNotNullParameter(list, "recentEmojis");
            m.checkNotNullParameter(meUser, "me");
            this.message = message;
            this.guild = guild;
            this.messageAuthorName = str;
            this.messageContent = charSequence;
            this.manageMessageContext = manageMessageContext;
            this.type = i;
            this.isDeveloper = z2;
            this.recentEmojis = list;
            this.channel = channel;
            this.permissions = l;
            this.f2825me = meUser;
        }

        public final Message component1() {
            return this.message;
        }

        public final Long component10() {
            return this.permissions;
        }

        public final MeUser component11() {
            return this.f2825me;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final String component3() {
            return this.messageAuthorName;
        }

        public final CharSequence component4() {
            return this.messageContent;
        }

        public final ManageMessageContext component5() {
            return this.manageMessageContext;
        }

        public final int component6() {
            return this.type;
        }

        public final boolean component7() {
            return this.isDeveloper;
        }

        public final List<Emoji> component8() {
            return this.recentEmojis;
        }

        public final Channel component9() {
            return this.channel;
        }

        public final Model copy(Message message, Guild guild, String str, CharSequence charSequence, ManageMessageContext manageMessageContext, int i, boolean z2, List<? extends Emoji> list, Channel channel, Long l, MeUser meUser) {
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(str, "messageAuthorName");
            m.checkNotNullParameter(manageMessageContext, "manageMessageContext");
            m.checkNotNullParameter(list, "recentEmojis");
            m.checkNotNullParameter(meUser, "me");
            return new Model(message, guild, str, charSequence, manageMessageContext, i, z2, list, channel, l, meUser);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.message, model.message) && m.areEqual(this.guild, model.guild) && m.areEqual(this.messageAuthorName, model.messageAuthorName) && m.areEqual(this.messageContent, model.messageContent) && m.areEqual(this.manageMessageContext, model.manageMessageContext) && this.type == model.type && this.isDeveloper == model.isDeveloper && m.areEqual(this.recentEmojis, model.recentEmojis) && m.areEqual(this.channel, model.channel) && m.areEqual(this.permissions, model.permissions) && m.areEqual(this.f2825me, model.f2825me);
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final ManageMessageContext getManageMessageContext() {
            return this.manageMessageContext;
        }

        public final MeUser getMe() {
            return this.f2825me;
        }

        public final Message getMessage() {
            return this.message;
        }

        public final String getMessageAuthorName() {
            return this.messageAuthorName;
        }

        public final CharSequence getMessageContent() {
            return this.messageContent;
        }

        public final Long getPermissions() {
            return this.permissions;
        }

        public final List<Emoji> getRecentEmojis() {
            return this.recentEmojis;
        }

        public final int getType() {
            return this.type;
        }

        public int hashCode() {
            Message message = this.message;
            int i = 0;
            int hashCode = (message != null ? message.hashCode() : 0) * 31;
            Guild guild = this.guild;
            int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
            String str = this.messageAuthorName;
            int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
            CharSequence charSequence = this.messageContent;
            int hashCode4 = (hashCode3 + (charSequence != null ? charSequence.hashCode() : 0)) * 31;
            ManageMessageContext manageMessageContext = this.manageMessageContext;
            int hashCode5 = (((hashCode4 + (manageMessageContext != null ? manageMessageContext.hashCode() : 0)) * 31) + this.type) * 31;
            boolean z2 = this.isDeveloper;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode5 + i2) * 31;
            List<Emoji> list = this.recentEmojis;
            int hashCode6 = (i4 + (list != null ? list.hashCode() : 0)) * 31;
            Channel channel = this.channel;
            int hashCode7 = (hashCode6 + (channel != null ? channel.hashCode() : 0)) * 31;
            Long l = this.permissions;
            int hashCode8 = (hashCode7 + (l != null ? l.hashCode() : 0)) * 31;
            MeUser meUser = this.f2825me;
            if (meUser != null) {
                i = meUser.hashCode();
            }
            return hashCode8 + i;
        }

        public final boolean isDeveloper() {
            return this.isDeveloper;
        }

        public String toString() {
            StringBuilder R = a.R("Model(message=");
            R.append(this.message);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", messageAuthorName=");
            R.append(this.messageAuthorName);
            R.append(", messageContent=");
            R.append(this.messageContent);
            R.append(", manageMessageContext=");
            R.append(this.manageMessageContext);
            R.append(", type=");
            R.append(this.type);
            R.append(", isDeveloper=");
            R.append(this.isDeveloper);
            R.append(", recentEmojis=");
            R.append(this.recentEmojis);
            R.append(", channel=");
            R.append(this.channel);
            R.append(", permissions=");
            R.append(this.permissions);
            R.append(", me=");
            R.append(this.f2825me);
            R.append(")");
            return R.toString();
        }
    }

    public WidgetChatListActions() {
        super(false, 1, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void addReaction(Emoji emoji) {
        RestAPI api = RestAPI.Companion.getApi();
        long j = this.channelId;
        long j2 = this.messageId;
        String reactionKey = emoji.getReactionKey();
        m.checkNotNullExpressionValue(reactionKey, "emoji.reactionKey");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(api.addReaction(j, j2, reactionKey), false, 1, null), this, null, 2, null), (r18 & 1) != 0 ? null : getContext(), "REST: addReaction", (r18 & 4) != 0 ? null : null, new WidgetChatListActions$addReaction$1(this, emoji), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    private final void configureAddReactionEmojisList(List<? extends Emoji> list, boolean z2, boolean z3) {
        if (list.isEmpty() || z2 || !z3) {
            RecyclerView recyclerView = getBinding().f2290b;
            m.checkNotNullExpressionValue(recyclerView, "binding.dialogChatActionsAddReactionEmojisList");
            recyclerView.setVisibility(8);
            return;
        }
        RecyclerView recyclerView2 = getBinding().f2290b;
        m.checkNotNullExpressionValue(recyclerView2, "binding.dialogChatActionsAddReactionEmojisList");
        recyclerView2.setVisibility(0);
        RecyclerView recyclerView3 = getBinding().f2290b;
        m.checkNotNullExpressionValue(recyclerView3, "binding.dialogChatActionsAddReactionEmojisList");
        int width = recyclerView3.getWidth();
        RecyclerView recyclerView4 = getBinding().f2290b;
        m.checkNotNullExpressionValue(recyclerView4, "binding.dialogChatActionsAddReactionEmojisList");
        int paddingStart = recyclerView4.getPaddingStart();
        RecyclerView recyclerView5 = getBinding().f2290b;
        m.checkNotNullExpressionValue(recyclerView5, "binding.dialogChatActionsAddReactionEmojisList");
        int paddingEnd = recyclerView5.getPaddingEnd() + paddingStart;
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.chat_input_emoji_size);
        int dpToPixels = DimenUtils.dpToPixels(8);
        int i = width - paddingEnd;
        int min = Math.min(list.size() + 1, (i + dpToPixels) / (dimensionPixelSize + dpToPixels));
        int i2 = min - 1;
        int max = Math.max(i - ((i2 * dpToPixels) + (dimensionPixelSize * min)), 0);
        PaddedItemDecorator paddedItemDecorator = this.itemDecorator;
        if (paddedItemDecorator != null) {
            getBinding().f2290b.removeItemDecoration(paddedItemDecorator);
        }
        PaddedItemDecorator paddedItemDecorator2 = new PaddedItemDecorator(0, (max / i2) + dpToPixels, 0, true);
        getBinding().f2290b.addItemDecoration(paddedItemDecorator2);
        this.itemDecorator = paddedItemDecorator2;
        WidgetChatListActionsEmojisAdapter widgetChatListActionsEmojisAdapter = this.adapter;
        if (widgetChatListActionsEmojisAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChatListActionsEmojisAdapter.setData(list, min);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:100:0x0272  */
    /* JADX WARN: Removed duplicated region for block: B:101:0x0274  */
    /* JADX WARN: Removed duplicated region for block: B:104:0x029c  */
    /* JADX WARN: Removed duplicated region for block: B:105:0x029e  */
    /* JADX WARN: Removed duplicated region for block: B:108:0x02b9  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x02bd  */
    /* JADX WARN: Removed duplicated region for block: B:112:0x02e6  */
    /* JADX WARN: Removed duplicated region for block: B:113:0x02e8  */
    /* JADX WARN: Removed duplicated region for block: B:138:0x0366  */
    /* JADX WARN: Removed duplicated region for block: B:139:0x0368  */
    /* JADX WARN: Removed duplicated region for block: B:142:0x039f  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x0205  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x0207  */
    /* JADX WARN: Removed duplicated region for block: B:92:0x0245  */
    /* JADX WARN: Removed duplicated region for block: B:93:0x0247  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void configureUI(final com.discord.widgets.chat.list.actions.WidgetChatListActions.Model r13) {
        /*
            Method dump skipped, instructions count: 947
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.actions.WidgetChatListActions.configureUI(com.discord.widgets.chat.list.actions.WidgetChatListActions$Model):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void confirmPublishMessage(Message message) {
        MessageActionDialogs messageActionDialogs = MessageActionDialogs.INSTANCE;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        messageActionDialogs.showPublishMessageConfirmation(parentFragmentManager, message, new WidgetChatListActions$confirmPublishMessage$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void deleteMessage(Message message) {
        MessageActionDialogs messageActionDialogs = MessageActionDialogs.INSTANCE;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        messageActionDialogs.showDeleteMessageConfirmation(parentFragmentManager, requireContext, message, new WidgetChatListActions$deleteMessage$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void editMessage(final Message message) {
        Observable<R> Y = StoreStream.Companion.getChannels().observeGuildAndPrivateChannels().Y(new b<Map<Long, ? extends Channel>, Observable<? extends CharSequence>>() { // from class: com.discord.widgets.chat.list.actions.WidgetChatListActions$editMessage$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends CharSequence> call(Map<Long, ? extends Channel> map) {
                return call2((Map<Long, Channel>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends CharSequence> call2(final Map<Long, Channel> map) {
                Observable emojiSet;
                Channel channel = map.get(Long.valueOf(Message.this.getChannelId()));
                final long f = channel != null ? channel.f() : 0L;
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<Map<Long, com.discord.models.user.User>> observeAllUsers = companion.getUsers().observeAllUsers();
                Observable<Map<Long, Guild>> observeGuilds = companion.getGuilds().observeGuilds();
                emojiSet = companion.getEmojis().getEmojiSet(f, Message.this.getChannelId(), (r16 & 4) != 0 ? false : false, (r16 & 8) != 0 ? false : false);
                return Observable.i(observeAllUsers, observeGuilds, emojiSet, new Func3<Map<Long, ? extends com.discord.models.user.User>, Map<Long, ? extends Guild>, EmojiSet, CharSequence>() { // from class: com.discord.widgets.chat.list.actions.WidgetChatListActions$editMessage$1.1
                    @Override // rx.functions.Func3
                    public /* bridge */ /* synthetic */ CharSequence call(Map<Long, ? extends com.discord.models.user.User> map2, Map<Long, ? extends Guild> map3, EmojiSet emojiSet2) {
                        return call2(map2, (Map<Long, Guild>) map3, emojiSet2);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final CharSequence call2(Map<Long, ? extends com.discord.models.user.User> map2, Map<Long, Guild> map3, EmojiSet emojiSet2) {
                        String content = Message.this.getContent();
                        if (content == null) {
                            content = "";
                        }
                        Map map4 = map;
                        m.checkNotNullExpressionValue(map4, "channels");
                        m.checkNotNullExpressionValue(map2, "users");
                        m.checkNotNullExpressionValue(emojiSet2, "emojiSet");
                        return MessageUnparser.unparse(content, map3.get(Long.valueOf(f)), map4, map2, emojiSet2);
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "StoreStream\n        .get…              }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(ObservableExtensionsKt.computationBuffered(Y), 0L, false, 3, null), (r18 & 1) != 0 ? null : null, "editMessage", (r18 & 4) != 0 ? null : null, new WidgetChatListActions$editMessage$2(message), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
    }

    private final WidgetChatListActionsBinding getBinding() {
        return (WidgetChatListActionsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void removeAllReactions(Model model) {
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        String string = getString(R.string.remove_all_reactions_confirm_title);
        String string2 = getString(R.string.remove_all_reactions_confirm_body);
        m.checkNotNullExpressionValue(string2, "getString(R.string.remov…l_reactions_confirm_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, getString(R.string.yes_text), getString(R.string.no_text), g0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), new WidgetChatListActions$removeAllReactions$1(this, model))), null, null, null, null, null, null, 0, null, 16320, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void replyMessage(Message message, Channel channel) {
        StoreStream.Companion companion = StoreStream.Companion;
        long id2 = companion.getUsers().getMe().getId();
        boolean x2 = ChannelUtils.x(channel);
        boolean isWebhook = message.isWebhook();
        User author = message.getAuthor();
        boolean z2 = true;
        boolean z3 = author != null && author.i() == id2;
        boolean z4 = !isWebhook && !z3;
        if (x2 || isWebhook || z3) {
            z2 = false;
        }
        companion.getPendingReplies().onCreatePendingReply(channel, message, z4, z2);
    }

    public static final void showForChat(FragmentManager fragmentManager, long j, long j2, CharSequence charSequence) {
        Companion.showForChat(fragmentManager, j, j2, charSequence);
    }

    public static final void showForPin(FragmentManager fragmentManager, long j, long j2, CharSequence charSequence) {
        Companion.showForPin(fragmentManager, j, j2, charSequence);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void toggleMessagePin(Message message) {
        MessageActionDialogs messageActionDialogs = MessageActionDialogs.INSTANCE;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        messageActionDialogs.showPinMessageConfirmation(parentFragmentManager, requireContext, message, this, new WidgetChatListActions$toggleMessagePin$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_chat_list_actions;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AppBottomSheet.hideKeyboard$default(this, null, 1, null);
        Observable q = ObservableExtensionsKt.computationLatest(Model.Companion.get(this.channelId, this.messageId, getArgumentsOrDefault().getCharSequence(INTENT_EXTRA_MESSAGE_CONTENT), getArgumentsOrDefault().getInt(INTENT_EXTRA_TYPE))).q();
        m.checkNotNullExpressionValue(q, "Model.get(channelId, mes…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q, this, null, 2, null), WidgetChatListActions.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatListActions$onResume$1(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        this.channelId = getArgumentsOrDefault().getLong(INTENT_EXTRA_MESSAGE_CHANNEL_ID);
        this.messageId = getArgumentsOrDefault().getLong(INTENT_EXTRA_MESSAGE_ID);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2290b;
        m.checkNotNullExpressionValue(recyclerView, "binding.dialogChatActionsAddReactionEmojisList");
        WidgetChatListActionsEmojisAdapter widgetChatListActionsEmojisAdapter = (WidgetChatListActionsEmojisAdapter) companion.configure(new WidgetChatListActionsEmojisAdapter(recyclerView));
        this.adapter = widgetChatListActionsEmojisAdapter;
        if (widgetChatListActionsEmojisAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChatListActionsEmojisAdapter.setOnClickEmoji(new WidgetChatListActions$onViewCreated$1(this));
        WidgetChatListActionsEmojisAdapter widgetChatListActionsEmojisAdapter2 = this.adapter;
        if (widgetChatListActionsEmojisAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        widgetChatListActionsEmojisAdapter2.setOnClickMoreEmojis(new WidgetChatListActions$onViewCreated$2(this));
        RecyclerView recyclerView2 = getBinding().f2290b;
        m.checkNotNullExpressionValue(recyclerView2, "binding.dialogChatActionsAddReactionEmojisList");
        WidgetChatListActionsEmojisAdapter widgetChatListActionsEmojisAdapter3 = this.adapter;
        if (widgetChatListActionsEmojisAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        recyclerView2.setAdapter(widgetChatListActionsEmojisAdapter3);
        getBinding().f2290b.setHasFixedSize(true);
    }
}
