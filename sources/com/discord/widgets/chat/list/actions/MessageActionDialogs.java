package com.discord.widgets.chat.list.actions;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.FragmentManager;
import com.discord.app.AppComponent;
import com.discord.models.message.Message;
import com.discord.widgets.chat.list.PublishActionDialog;
import com.discord.widgets.notice.WidgetNoticeDialog;
import d0.o;
import d0.t.g0;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: MessageActionDialogs.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\n\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J;\u0010\r\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0004\b\r\u0010\u000eJ+\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00062\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0004\b\u000f\u0010\u0010J3\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/list/actions/MessageActionDialogs;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Landroid/content/Context;", "context", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/app/AppComponent;", "appComponent", "Lkotlin/Function0;", "", "onSuccess", "showPinMessageConfirmation", "(Landroidx/fragment/app/FragmentManager;Landroid/content/Context;Lcom/discord/models/message/Message;Lcom/discord/app/AppComponent;Lkotlin/jvm/functions/Function0;)V", "showPublishMessageConfirmation", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/message/Message;Lkotlin/jvm/functions/Function0;)V", "showDeleteMessageConfirmation", "(Landroidx/fragment/app/FragmentManager;Landroid/content/Context;Lcom/discord/models/message/Message;Lkotlin/jvm/functions/Function0;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageActionDialogs {
    public static final MessageActionDialogs INSTANCE = new MessageActionDialogs();

    private MessageActionDialogs() {
    }

    public final void showDeleteMessageConfirmation(FragmentManager fragmentManager, Context context, Message message, Function0<Unit> function0) {
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(function0, "onSuccess");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = context.getString(R.string.delete_message);
        String string2 = context.getString(R.string.delete_message_body);
        m.checkNotNullExpressionValue(string2, "context.getString(R.string.delete_message_body)");
        WidgetNoticeDialog.Companion.show$default(companion, fragmentManager, string, string2, context.getString(R.string.delete), context.getString(R.string.cancel), g0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), new MessageActionDialogs$showDeleteMessageConfirmation$1(message, function0))), null, null, null, Integer.valueOf((int) R.attr.notice_theme_positive_red), null, null, 0, null, 15808, null);
    }

    public final void showPinMessageConfirmation(FragmentManager fragmentManager, Context context, Message message, AppComponent appComponent, Function0<Unit> function0) {
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(function0, "onSuccess");
        boolean areEqual = m.areEqual(message.getPinned(), Boolean.TRUE);
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = context.getString(areEqual ? R.string.unpin_message_title : R.string.pin_message_title);
        String string2 = context.getString(areEqual ? R.string.unpin_message_body : R.string.pin_message_body_mobile);
        m.checkNotNullExpressionValue(string2, "context.getString(\n     …age_body_mobile\n        )");
        WidgetNoticeDialog.Companion.show$default(companion, fragmentManager, string, string2, context.getString(areEqual ? R.string.unpin : R.string.pin), context.getString(R.string.cancel), g0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), new MessageActionDialogs$showPinMessageConfirmation$1(message, areEqual, appComponent, context, function0))), null, null, null, null, null, null, 0, null, 16320, null);
    }

    public final void showPublishMessageConfirmation(FragmentManager fragmentManager, Message message, Function0<Unit> function0) {
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(function0, "onSuccess");
        PublishActionDialog.Companion.show(fragmentManager, message.getId(), message.getChannelId(), function0, (r17 & 16) != 0 ? null : null);
    }
}
