package com.discord.widgets.chat.list.actions;

import andhook.lib.HookHelper;
import android.view.View;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: WidgetChatListActionsEmojisAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/chat/list/actions/MoreEmojisViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;", "Lcom/discord/widgets/chat/list/actions/EmojiItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/chat/list/actions/EmojiItem;)V", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MoreEmojisViewHolder extends MGRecyclerViewHolder<WidgetChatListActionsEmojisAdapter, EmojiItem> {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MoreEmojisViewHolder(WidgetChatListActionsEmojisAdapter widgetChatListActionsEmojisAdapter) {
        super((int) R.layout.view_chat_list_actions_emoji_item_more, widgetChatListActionsEmojisAdapter);
        m.checkNotNullParameter(widgetChatListActionsEmojisAdapter, "adapter");
    }

    public static final /* synthetic */ WidgetChatListActionsEmojisAdapter access$getAdapter$p(MoreEmojisViewHolder moreEmojisViewHolder) {
        return (WidgetChatListActionsEmojisAdapter) moreEmojisViewHolder.adapter;
    }

    public void onConfigure(int i, EmojiItem emojiItem) {
        m.checkNotNullParameter(emojiItem, "data");
        super.onConfigure(i, (int) emojiItem);
        this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.actions.MoreEmojisViewHolder$onConfigure$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MoreEmojisViewHolder.access$getAdapter$p(MoreEmojisViewHolder.this).getOnClickMoreEmojis().invoke();
            }
        });
    }
}
