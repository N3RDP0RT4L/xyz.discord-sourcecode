package com.discord.widgets.chat.list.actions;

import android.content.Context;
import android.view.View;
import b.a.d.m;
import com.discord.app.AppComponent;
import com.discord.models.message.Message;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: MessageActionDialogs.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "view", "", "invoke", "(Landroid/view/View;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageActionDialogs$showPinMessageConfirmation$1 extends o implements Function1<View, Unit> {
    public final /* synthetic */ AppComponent $appComponent;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ boolean $isPinned;
    public final /* synthetic */ Message $message;
    public final /* synthetic */ Function0 $onSuccess;

    /* compiled from: MessageActionDialogs.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.actions.MessageActionDialogs$showPinMessageConfirmation$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Void, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
            invoke2(r1);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Void r5) {
            MessageActionDialogs$showPinMessageConfirmation$1 messageActionDialogs$showPinMessageConfirmation$1 = MessageActionDialogs$showPinMessageConfirmation$1.this;
            m.g(messageActionDialogs$showPinMessageConfirmation$1.$context, messageActionDialogs$showPinMessageConfirmation$1.$isPinned ? R.string.message_unpinned : R.string.message_pinned, 0, null, 12);
            MessageActionDialogs$showPinMessageConfirmation$1.this.$onSuccess.invoke();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MessageActionDialogs$showPinMessageConfirmation$1(Message message, boolean z2, AppComponent appComponent, Context context, Function0 function0) {
        super(1);
        this.$message = message;
        this.$isPinned = z2;
        this.$appComponent = appComponent;
        this.$context = context;
        this.$onSuccess = function0;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(View view) {
        invoke2(view);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view) {
        d0.z.d.m.checkNotNullParameter(view, "view");
        long channelId = this.$message.getChannelId();
        long id2 = this.$message.getId();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.$isPinned ? RestAPI.Companion.getApi().deleteChannelPin(channelId, id2) : RestAPI.Companion.getApi().addChannelPin(channelId, id2), false, 1, null), this.$appComponent, null, 2, null), MessageActionDialogs.INSTANCE.getClass(), (r18 & 2) != 0 ? null : view.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
