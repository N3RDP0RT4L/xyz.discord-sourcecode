package com.discord.widgets.chat.list.actions;

import andhook.lib.HookHelper;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.models.domain.emoji.Emoji;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.widgets.chat.list.actions.EmojiItem;
import d0.t.n;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatListActionsEmojisAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0012\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b#\u0010$J+\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00020\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ)\u0010\u000e\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\r2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ#\u0010\u0011\u001a\u00020\u00102\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0011\u0010\u0012R(\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\u00138\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R.\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00100\u001a8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 ¨\u0006%"}, d2 = {"Lcom/discord/widgets/chat/list/actions/WidgetChatListActionsEmojisAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/chat/list/actions/EmojiItem;", "", "Lcom/discord/models/domain/emoji/Emoji;", "emojis", "", "emojisToShow", "getEmojiItems", "(Ljava/util/List;I)Ljava/util/List;", "Landroid/view/ViewGroup;", "parent", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "", "setData", "(Ljava/util/List;I)V", "Lkotlin/Function0;", "onClickMoreEmojis", "Lkotlin/jvm/functions/Function0;", "getOnClickMoreEmojis", "()Lkotlin/jvm/functions/Function0;", "setOnClickMoreEmojis", "(Lkotlin/jvm/functions/Function0;)V", "Lkotlin/Function1;", "onClickEmoji", "Lkotlin/jvm/functions/Function1;", "getOnClickEmoji", "()Lkotlin/jvm/functions/Function1;", "setOnClickEmoji", "(Lkotlin/jvm/functions/Function1;)V", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListActionsEmojisAdapter extends MGRecyclerAdapterSimple<EmojiItem> {
    private Function1<? super Emoji, Unit> onClickEmoji = WidgetChatListActionsEmojisAdapter$onClickEmoji$1.INSTANCE;
    private Function0<Unit> onClickMoreEmojis = WidgetChatListActionsEmojisAdapter$onClickMoreEmojis$1.INSTANCE;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListActionsEmojisAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
    }

    private final List<EmojiItem> getEmojiItems(List<? extends Emoji> list, int i) {
        if (list.isEmpty() || i <= 0) {
            return n.emptyList();
        }
        List<Emoji> take = u.take(list, i - 1);
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(take, 10));
        for (Emoji emoji : take) {
            arrayList.add(new EmojiItem.EmojiData(emoji));
        }
        List<EmojiItem> mutableList = u.toMutableList((Collection) arrayList);
        mutableList.add(EmojiItem.MoreEmoji.INSTANCE);
        return mutableList;
    }

    public final Function1<Emoji, Unit> getOnClickEmoji() {
        return this.onClickEmoji;
    }

    public final Function0<Unit> getOnClickMoreEmojis() {
        return this.onClickMoreEmojis;
    }

    public final void setData(List<? extends Emoji> list, int i) {
        m.checkNotNullParameter(list, "emojis");
        setData(getEmojiItems(list, i));
    }

    public final void setOnClickEmoji(Function1<? super Emoji, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClickEmoji = function1;
    }

    public final void setOnClickMoreEmojis(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onClickMoreEmojis = function0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, EmojiItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new EmojiViewHolder(this);
        }
        if (i == 1) {
            return new MoreEmojisViewHolder(this);
        }
        throw invalidViewTypeException(i);
    }
}
