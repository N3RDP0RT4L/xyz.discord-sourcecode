package com.discord.widgets.chat.list;

import andhook.lib.HookHelper;
import com.discord.api.guild.GuildFeature;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: NewThreadsPermissionsFeatureFlag.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B\u001b\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0010\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0016\u0010\u0017J#\u0010\u0007\u001a\u00020\u00062\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\f2\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\r\u0010\u000eJ\u0019\u0010\u0007\u001a\u00020\u00062\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\u0007\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/chat/list/NewThreadsPermissionsFeatureFlag;", "", "Lcom/discord/models/experiments/domain/Experiment;", "experiment", "Lcom/discord/models/guild/Guild;", "guild", "", "isEnabled", "(Lcom/discord/models/experiments/domain/Experiment;Lcom/discord/models/guild/Guild;)Z", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "observeEnabled", "(J)Lrx/Observable;", "(J)Z", "Lcom/discord/stores/StoreExperiments;", "storeExperiments", "Lcom/discord/stores/StoreExperiments;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", HookHelper.constructorName, "(Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreGuilds;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NewThreadsPermissionsFeatureFlag {
    public static final Companion Companion = new Companion(null);
    private static final Lazy INSTANCE$delegate = g.lazy(NewThreadsPermissionsFeatureFlag$Companion$INSTANCE$2.INSTANCE);
    private final StoreExperiments storeExperiments;
    private final StoreGuilds storeGuilds;

    /* compiled from: NewThreadsPermissionsFeatureFlag.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u001d\u0010\u0007\u001a\u00020\u00028F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/list/NewThreadsPermissionsFeatureFlag$Companion;", "", "Lcom/discord/widgets/chat/list/NewThreadsPermissionsFeatureFlag;", "INSTANCE$delegate", "Lkotlin/Lazy;", "getINSTANCE", "()Lcom/discord/widgets/chat/list/NewThreadsPermissionsFeatureFlag;", "INSTANCE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final NewThreadsPermissionsFeatureFlag getINSTANCE() {
            Lazy lazy = NewThreadsPermissionsFeatureFlag.INSTANCE$delegate;
            Companion companion = NewThreadsPermissionsFeatureFlag.Companion;
            return (NewThreadsPermissionsFeatureFlag) lazy.getValue();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public NewThreadsPermissionsFeatureFlag() {
        this(null, null, 3, null);
    }

    public NewThreadsPermissionsFeatureFlag(StoreExperiments storeExperiments, StoreGuilds storeGuilds) {
        m.checkNotNullParameter(storeExperiments, "storeExperiments");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        this.storeExperiments = storeExperiments;
        this.storeGuilds = storeGuilds;
    }

    public final boolean isEnabled(long j) {
        return isEnabled(this.storeExperiments.getGuildExperiment("2021-08_threads_permissions", j, true), this.storeGuilds.getGuild(j));
    }

    public final Observable<Boolean> observeEnabled(long j) {
        Observable<Boolean> j2 = Observable.j(this.storeExperiments.observeGuildExperiment("2021-08_threads_permissions", j, true), this.storeGuilds.observeGuild(j), new Func2<Experiment, Guild, Boolean>() { // from class: com.discord.widgets.chat.list.NewThreadsPermissionsFeatureFlag$observeEnabled$1
            public final Boolean call(Experiment experiment, Guild guild) {
                boolean isEnabled;
                isEnabled = NewThreadsPermissionsFeatureFlag.this.isEnabled(experiment, guild);
                return Boolean.valueOf(isEnabled);
            }
        });
        m.checkNotNullExpressionValue(j2, "Observable.combineLatest…dExperiment, guild)\n    }");
        return j2;
    }

    public /* synthetic */ NewThreadsPermissionsFeatureFlag(StoreExperiments storeExperiments, StoreGuilds storeGuilds, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? StoreStream.Companion.getExperiments() : storeExperiments, (i & 2) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean isEnabled(Experiment experiment, Guild guild) {
        return (experiment != null && experiment.getBucket() == 1) || (guild != null && guild.hasFeature(GuildFeature.NEW_THREAD_PERMISSIONS));
    }
}
