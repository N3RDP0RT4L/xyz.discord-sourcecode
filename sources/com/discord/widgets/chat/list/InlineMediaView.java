package com.discord.widgets.chat.list;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.cardview.widget.CardView;
import b.a.p.i;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.api.message.attachment.MessageAttachment;
import com.discord.api.message.attachment.MessageAttachmentType;
import com.discord.api.message.embed.EmbedType;
import com.discord.api.message.embed.EmbedVideo;
import com.discord.api.message.embed.MessageEmbed;
import com.discord.app.AppComponent;
import com.discord.databinding.InlineMediaViewBinding;
import com.discord.embed.RenderableEmbedMedia;
import com.discord.player.AppMediaPlayer;
import com.discord.player.MediaSource;
import com.discord.player.MediaType;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.embed.EmbedResourceUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.views.ViewCoroutineScopeKt;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.exoplayer2.ui.PlayerView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.CoroutineScope;
import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import rx.subscriptions.CompositeSubscription;
/* compiled from: InlineMediaView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0001RB\u0011\b\u0016\u0012\u0006\u0010J\u001a\u00020I¢\u0006\u0004\bK\u0010LB\u001d\b\u0016\u0012\u0006\u0010J\u001a\u00020I\u0012\n\b\u0002\u0010N\u001a\u0004\u0018\u00010M¢\u0006\u0004\bK\u0010OB'\b\u0016\u0012\u0006\u0010J\u001a\u00020I\u0012\n\b\u0002\u0010N\u001a\u0004\u0018\u00010M\u0012\b\b\u0002\u0010P\u001a\u00020\n¢\u0006\u0004\bK\u0010QJU\u0010\u0011\u001a\u00020\u00102\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\n2\u0006\u0010\r\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0011\u0010\u0012JM\u0010\u0013\u001a\u00020\u00102\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\t\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\n2\u0006\u0010\r\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u001b\u0010\u001aJ\u000f\u0010\u001c\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u001c\u0010\u001aJ\u000f\u0010\u001d\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u001d\u0010\u001aJ\u000f\u0010\u001e\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010\"\u001a\u00020\u00102\u0006\u0010!\u001a\u00020 H\u0016¢\u0006\u0004\b\"\u0010#J\u0017\u0010$\u001a\u00020\u00102\u0006\u0010!\u001a\u00020 H\u0016¢\u0006\u0004\b$\u0010#J\u001f\u0010'\u001a\u00020\u00102\u0006\u0010%\u001a\u00020 2\u0006\u0010&\u001a\u00020\nH\u0014¢\u0006\u0004\b'\u0010(J\r\u0010)\u001a\u00020\u0010¢\u0006\u0004\b)\u0010\u001aJ\r\u0010*\u001a\u00020\u0010¢\u0006\u0004\b*\u0010\u001aJ5\u0010-\u001a\u00020\u00102\u0006\u0010,\u001a\u00020+2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b-\u0010.J5\u00101\u001a\u00020\u00102\u0006\u00100\u001a\u00020/2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b1\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R\u0018\u00107\u001a\u0004\u0018\u0001068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R\u0018\u0010:\u001a\u0004\u0018\u0001098\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u0010;R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>R\u0016\u0010@\u001a\u00020?8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b@\u0010AR\u0016\u0010\r\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010BR(\u0010E\u001a\u000e\u0012\u0004\u0012\u00020D\u0012\u0004\u0012\u00020D0C8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bE\u0010F\u001a\u0004\bG\u0010H¨\u0006S"}, d2 = {"Lcom/discord/widgets/chat/list/InlineMediaView;", "Landroidx/cardview/widget/CardView;", "Landroid/view/View$OnAttachStateChangeListener;", "Lcom/discord/app/AppComponent;", "Lcom/discord/embed/RenderableEmbedMedia;", "previewImage", "", "progressiveMediaUri", "Lcom/discord/api/message/embed/EmbedType;", "embedType", "", "targetWidth", "targetHeight", "featureTag", "", "autoPlayGifs", "", "diffViewParamsAndUpdateEmbed", "(Lcom/discord/embed/RenderableEmbedMedia;Ljava/lang/String;Lcom/discord/api/message/embed/EmbedType;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V", "updateUI", "(Lcom/discord/embed/RenderableEmbedMedia;Ljava/lang/String;Lcom/discord/api/message/embed/EmbedType;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V", "Lcom/discord/player/AppMediaPlayer$Event;", "event", "handlePlayerEvent", "(Lcom/discord/player/AppMediaPlayer$Event;)V", "resetCurrentEmbed", "()V", "releasePlayer", "clearPlayerAndSubscriptions", "resetViews", "shouldAutoPlay", "()Z", "Landroid/view/View;", "view", "onViewAttachedToWindow", "(Landroid/view/View;)V", "onViewDetachedFromWindow", "changedView", "visibility", "onVisibilityChanged", "(Landroid/view/View;I)V", "onResume", "onPause", "Lcom/discord/api/message/attachment/MessageAttachment;", "attachment", "updateUIWithAttachment", "(Lcom/discord/api/message/attachment/MessageAttachment;Ljava/lang/Integer;Ljava/lang/Integer;Z)V", "Lcom/discord/api/message/embed/MessageEmbed;", "embed", "updateUIWithEmbed", "(Lcom/discord/api/message/embed/MessageEmbed;Ljava/lang/Integer;Ljava/lang/Integer;Z)V", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreUserSettings;", "Lcom/discord/player/AppMediaPlayer;", "appMediaPlayer", "Lcom/discord/player/AppMediaPlayer;", "Lcom/discord/widgets/chat/list/InlineMediaView$ViewParams;", "viewParams", "Lcom/discord/widgets/chat/list/InlineMediaView$ViewParams;", "Lcom/discord/databinding/InlineMediaViewBinding;", "binding", "Lcom/discord/databinding/InlineMediaViewBinding;", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "Lrx/subscriptions/CompositeSubscription;", "Ljava/lang/String;", "Lrx/subjects/Subject;", "Ljava/lang/Void;", "unsubscribeSignal", "Lrx/subjects/Subject;", "getUnsubscribeSignal", "()Lrx/subjects/Subject;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "ViewParams", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InlineMediaView extends CardView implements View.OnAttachStateChangeListener, AppComponent {
    private AppMediaPlayer appMediaPlayer;
    private final InlineMediaViewBinding binding;
    private CompositeSubscription compositeSubscription;
    private String featureTag;
    private final StoreUserSettings storeUserSettings;
    private final Subject<Void, Void> unsubscribeSignal;
    private ViewParams viewParams;

    /* compiled from: InlineMediaView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u001e\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\b\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u0017\u001a\u00020\u000f¢\u0006\u0004\b+\u0010,J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011JV\u0010\u0018\u001a\u00020\u00002\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u000b2\b\b\u0002\u0010\u0017\u001a\u00020\u000fHÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u0007J\u0010\u0010\u001b\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010\u001e\u001a\u00020\u000f2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010 \u001a\u0004\b!\u0010\rR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010$\u001a\u0004\b%\u0010\u0011R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010 \u001a\u0004\b&\u0010\rR\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010'\u001a\u0004\b(\u0010\u0007R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b*\u0010\n¨\u0006-"}, d2 = {"Lcom/discord/widgets/chat/list/InlineMediaView$ViewParams;", "", "Lcom/discord/embed/RenderableEmbedMedia;", "component1", "()Lcom/discord/embed/RenderableEmbedMedia;", "", "component2", "()Ljava/lang/String;", "Lcom/discord/api/message/embed/EmbedType;", "component3", "()Lcom/discord/api/message/embed/EmbedType;", "", "component4", "()Ljava/lang/Integer;", "component5", "", "component6", "()Z", "previewImage", "progressiveMediaUri", "embedType", "targetWidth", "targetHeight", "autoPlayGifs", "copy", "(Lcom/discord/embed/RenderableEmbedMedia;Ljava/lang/String;Lcom/discord/api/message/embed/EmbedType;Ljava/lang/Integer;Ljava/lang/Integer;Z)Lcom/discord/widgets/chat/list/InlineMediaView$ViewParams;", "toString", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getTargetWidth", "Lcom/discord/embed/RenderableEmbedMedia;", "getPreviewImage", "Z", "getAutoPlayGifs", "getTargetHeight", "Ljava/lang/String;", "getProgressiveMediaUri", "Lcom/discord/api/message/embed/EmbedType;", "getEmbedType", HookHelper.constructorName, "(Lcom/discord/embed/RenderableEmbedMedia;Ljava/lang/String;Lcom/discord/api/message/embed/EmbedType;Ljava/lang/Integer;Ljava/lang/Integer;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewParams {
        private final boolean autoPlayGifs;
        private final EmbedType embedType;
        private final RenderableEmbedMedia previewImage;
        private final String progressiveMediaUri;
        private final Integer targetHeight;
        private final Integer targetWidth;

        public ViewParams(RenderableEmbedMedia renderableEmbedMedia, String str, EmbedType embedType, Integer num, Integer num2, boolean z2) {
            this.previewImage = renderableEmbedMedia;
            this.progressiveMediaUri = str;
            this.embedType = embedType;
            this.targetWidth = num;
            this.targetHeight = num2;
            this.autoPlayGifs = z2;
        }

        public static /* synthetic */ ViewParams copy$default(ViewParams viewParams, RenderableEmbedMedia renderableEmbedMedia, String str, EmbedType embedType, Integer num, Integer num2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                renderableEmbedMedia = viewParams.previewImage;
            }
            if ((i & 2) != 0) {
                str = viewParams.progressiveMediaUri;
            }
            String str2 = str;
            if ((i & 4) != 0) {
                embedType = viewParams.embedType;
            }
            EmbedType embedType2 = embedType;
            if ((i & 8) != 0) {
                num = viewParams.targetWidth;
            }
            Integer num3 = num;
            if ((i & 16) != 0) {
                num2 = viewParams.targetHeight;
            }
            Integer num4 = num2;
            if ((i & 32) != 0) {
                z2 = viewParams.autoPlayGifs;
            }
            return viewParams.copy(renderableEmbedMedia, str2, embedType2, num3, num4, z2);
        }

        public final RenderableEmbedMedia component1() {
            return this.previewImage;
        }

        public final String component2() {
            return this.progressiveMediaUri;
        }

        public final EmbedType component3() {
            return this.embedType;
        }

        public final Integer component4() {
            return this.targetWidth;
        }

        public final Integer component5() {
            return this.targetHeight;
        }

        public final boolean component6() {
            return this.autoPlayGifs;
        }

        public final ViewParams copy(RenderableEmbedMedia renderableEmbedMedia, String str, EmbedType embedType, Integer num, Integer num2, boolean z2) {
            return new ViewParams(renderableEmbedMedia, str, embedType, num, num2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewParams)) {
                return false;
            }
            ViewParams viewParams = (ViewParams) obj;
            return m.areEqual(this.previewImage, viewParams.previewImage) && m.areEqual(this.progressiveMediaUri, viewParams.progressiveMediaUri) && m.areEqual(this.embedType, viewParams.embedType) && m.areEqual(this.targetWidth, viewParams.targetWidth) && m.areEqual(this.targetHeight, viewParams.targetHeight) && this.autoPlayGifs == viewParams.autoPlayGifs;
        }

        public final boolean getAutoPlayGifs() {
            return this.autoPlayGifs;
        }

        public final EmbedType getEmbedType() {
            return this.embedType;
        }

        public final RenderableEmbedMedia getPreviewImage() {
            return this.previewImage;
        }

        public final String getProgressiveMediaUri() {
            return this.progressiveMediaUri;
        }

        public final Integer getTargetHeight() {
            return this.targetHeight;
        }

        public final Integer getTargetWidth() {
            return this.targetWidth;
        }

        public int hashCode() {
            RenderableEmbedMedia renderableEmbedMedia = this.previewImage;
            int i = 0;
            int hashCode = (renderableEmbedMedia != null ? renderableEmbedMedia.hashCode() : 0) * 31;
            String str = this.progressiveMediaUri;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            EmbedType embedType = this.embedType;
            int hashCode3 = (hashCode2 + (embedType != null ? embedType.hashCode() : 0)) * 31;
            Integer num = this.targetWidth;
            int hashCode4 = (hashCode3 + (num != null ? num.hashCode() : 0)) * 31;
            Integer num2 = this.targetHeight;
            if (num2 != null) {
                i = num2.hashCode();
            }
            int i2 = (hashCode4 + i) * 31;
            boolean z2 = this.autoPlayGifs;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("ViewParams(previewImage=");
            R.append(this.previewImage);
            R.append(", progressiveMediaUri=");
            R.append(this.progressiveMediaUri);
            R.append(", embedType=");
            R.append(this.embedType);
            R.append(", targetWidth=");
            R.append(this.targetWidth);
            R.append(", targetHeight=");
            R.append(this.targetHeight);
            R.append(", autoPlayGifs=");
            return a.M(R, this.autoPlayGifs, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            MessageAttachmentType.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[MessageAttachmentType.VIDEO.ordinal()] = 1;
            iArr[MessageAttachmentType.IMAGE.ordinal()] = 2;
            iArr[MessageAttachmentType.FILE.ordinal()] = 3;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InlineMediaView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        InlineMediaViewBinding a = InlineMediaViewBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "InlineMediaViewBinding.i…ater.from(context), this)");
        this.binding = a;
        this.storeUserSettings = StoreStream.Companion.getUserSettings();
        this.featureTag = "";
        this.compositeSubscription = new CompositeSubscription();
        addOnAttachStateChangeListener(this);
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.unsubscribeSignal = k0;
    }

    private final void clearPlayerAndSubscriptions() {
        releasePlayer();
        getUnsubscribeSignal().onNext(null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void diffViewParamsAndUpdateEmbed(RenderableEmbedMedia renderableEmbedMedia, String str, EmbedType embedType, Integer num, Integer num2, String str2, boolean z2) {
        Integer num3;
        Integer num4;
        Integer num5 = null;
        if (num != null) {
            num3 = num;
        } else {
            ViewParams viewParams = this.viewParams;
            num3 = viewParams != null ? viewParams.getTargetWidth() : null;
        }
        if (num2 != null) {
            num4 = num2;
        } else {
            ViewParams viewParams2 = this.viewParams;
            if (viewParams2 != null) {
                num5 = viewParams2.getTargetHeight();
            }
            num4 = num5;
        }
        ViewParams viewParams3 = new ViewParams(renderableEmbedMedia, str, embedType, num3, num4, z2);
        if (!m.areEqual(viewParams3, this.viewParams)) {
            this.viewParams = viewParams3;
            updateUI(renderableEmbedMedia, str, embedType, num, num2, str2);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handlePlayerEvent(AppMediaPlayer.Event event) {
        boolean z2 = true;
        int i = 0;
        if (m.areEqual(event, AppMediaPlayer.Event.a.a)) {
            SimpleDraweeView simpleDraweeView = this.binding.c;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.inlineMediaImagePreview");
            if (!(!shouldAutoPlay())) {
                i = 8;
            }
            simpleDraweeView.setVisibility(i);
            ProgressBar progressBar = this.binding.d;
            m.checkNotNullExpressionValue(progressBar, "binding.inlineMediaLoadingIndicator");
            progressBar.setVisibility(8);
        } else if (m.areEqual(event, AppMediaPlayer.Event.f.a)) {
            ViewParams viewParams = this.viewParams;
            EmbedType embedType = viewParams != null ? viewParams.getEmbedType() : null;
            ImageView imageView = this.binding.g;
            m.checkNotNullExpressionValue(imageView, "binding.inlineMediaVolumeToggle");
            if (!(embedType == EmbedType.VIDEO || embedType == null)) {
                z2 = false;
            }
            if (!z2) {
                i = 8;
            }
            imageView.setVisibility(i);
        } else if (m.areEqual(event, AppMediaPlayer.Event.e.a)) {
            resetCurrentEmbed();
        }
    }

    private final void releasePlayer() {
        AppMediaPlayer appMediaPlayer = this.appMediaPlayer;
        if (appMediaPlayer != null) {
            appMediaPlayer.c();
        }
        this.appMediaPlayer = null;
    }

    private final void resetCurrentEmbed() {
        ViewParams viewParams = this.viewParams;
        if (viewParams != null) {
            updateUI(viewParams.getPreviewImage(), viewParams.getProgressiveMediaUri(), viewParams.getEmbedType(), viewParams.getTargetWidth(), viewParams.getTargetHeight(), this.featureTag);
        }
    }

    private final void resetViews() {
        ViewParams viewParams = this.viewParams;
        if (viewParams != null) {
            SimpleDraweeView simpleDraweeView = this.binding.c;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.inlineMediaImagePreview");
            int i = 0;
            simpleDraweeView.setVisibility(0);
            ImageView imageView = this.binding.f2110b;
            m.checkNotNullExpressionValue(imageView, "binding.inlineMediaGifIndicator");
            boolean z2 = true;
            imageView.setVisibility(shouldAutoPlay() ^ true ? 0 : 8);
            PlayerView playerView = this.binding.f;
            m.checkNotNullExpressionValue(playerView, "binding.inlineMediaPlayerView");
            playerView.setVisibility(8);
            ImageView imageView2 = this.binding.g;
            m.checkNotNullExpressionValue(imageView2, "binding.inlineMediaVolumeToggle");
            imageView2.setVisibility(8);
            ImageView imageView3 = this.binding.e;
            m.checkNotNullExpressionValue(imageView3, "binding.inlineMediaPlayButton");
            if (viewParams.getEmbedType() != EmbedType.VIDEO) {
                z2 = false;
            }
            if (!z2) {
                i = 8;
            }
            imageView3.setVisibility(i);
            ProgressBar progressBar = this.binding.d;
            m.checkNotNullExpressionValue(progressBar, "binding.inlineMediaLoadingIndicator");
            progressBar.setVisibility(8);
            ImageView imageView4 = this.binding.g;
            m.checkNotNullExpressionValue(imageView4, "binding.inlineMediaVolumeToggle");
            imageView4.setVisibility(8);
        }
    }

    private final boolean shouldAutoPlay() {
        RenderableEmbedMedia previewImage;
        EmbedResourceUtils embedResourceUtils = EmbedResourceUtils.INSTANCE;
        ViewParams viewParams = this.viewParams;
        String str = null;
        EmbedType embedType = viewParams != null ? viewParams.getEmbedType() : null;
        ViewParams viewParams2 = this.viewParams;
        if (!(viewParams2 == null || (previewImage = viewParams2.getPreviewImage()) == null)) {
            str = previewImage.a;
        }
        if (!embedResourceUtils.isAnimated(embedType, str)) {
            return true;
        }
        ViewParams viewParams3 = this.viewParams;
        return viewParams3 != null ? viewParams3.getAutoPlayGifs() : this.storeUserSettings.getIsAutoPlayGifsEnabled();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateUI(RenderableEmbedMedia renderableEmbedMedia, String str, EmbedType embedType, Integer num, Integer num2, String str2) {
        resetViews();
        this.compositeSubscription.unsubscribe();
        if (!(num == null || num2 == null)) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (!(layoutParams.width == num.intValue() && layoutParams.height == num2.intValue())) {
                layoutParams.width = num.intValue();
                layoutParams.height = num2.intValue();
                requestLayout();
            }
            if ((renderableEmbedMedia != null ? renderableEmbedMedia.a : null) != null) {
                SimpleDraweeView simpleDraweeView = this.binding.c;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.inlineMediaImagePreview");
                MGImages.setImage$default(simpleDraweeView, EmbedResourceUtils.INSTANCE.getPreviewUrls(renderableEmbedMedia.a, num.intValue(), num2.intValue(), shouldAutoPlay()), 0, 0, false, null, null, null, 252, null);
            }
        }
        if (str != null) {
            AppMediaPlayer appMediaPlayer = this.appMediaPlayer;
            if (appMediaPlayer == null) {
                Context context = getContext();
                m.checkNotNullExpressionValue(context, "context");
                appMediaPlayer = i.a(context);
            }
            this.appMediaPlayer = appMediaPlayer;
            PlayerView playerView = this.binding.f;
            m.checkNotNullExpressionValue(playerView, "binding.inlineMediaPlayerView");
            int i = 8;
            playerView.setVisibility(shouldAutoPlay() ? 0 : 8);
            ImageView imageView = this.binding.f2110b;
            m.checkNotNullExpressionValue(imageView, "binding.inlineMediaGifIndicator");
            if (!shouldAutoPlay()) {
                i = 0;
            }
            imageView.setVisibility(i);
            m.checkNotNullParameter(str, "progressiveMediaUri");
            m.checkNotNullParameter(str2, "featureTag");
            EmbedType embedType2 = EmbedType.GIFV;
            final MediaSource P = d.P(embedType == embedType2 ? MediaType.GIFV : MediaType.VIDEO, str, str2);
            this.compositeSubscription = new CompositeSubscription();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(appMediaPlayer.d, this, null, 2, null), InlineMediaView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new InlineMediaView$updateUI$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new InlineMediaView$updateUI$2(this, appMediaPlayer));
            Observable<AppMediaPlayer.Event> J = appMediaPlayer.a.J();
            m.checkNotNullExpressionValue(J, "eventSubject.onBackpressureBuffer()");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(J, this, null, 2, null), InlineMediaView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new InlineMediaView$updateUI$3(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new InlineMediaView$updateUI$4(this));
            if (embedType == embedType2) {
                boolean shouldAutoPlay = shouldAutoPlay();
                PlayerView playerView2 = this.binding.f;
                m.checkNotNullExpressionValue(playerView2, "binding.inlineMediaPlayerView");
                AppMediaPlayer.b(appMediaPlayer, P, shouldAutoPlay, true, 0L, playerView2, null, 40);
                appMediaPlayer.d(0.0f);
            } else {
                appMediaPlayer.d(1.0f);
            }
            this.binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.list.InlineMediaView$updateUI$5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    InlineMediaViewBinding inlineMediaViewBinding;
                    AppMediaPlayer appMediaPlayer2;
                    InlineMediaViewBinding inlineMediaViewBinding2;
                    InlineMediaViewBinding inlineMediaViewBinding3;
                    InlineMediaViewBinding inlineMediaViewBinding4;
                    inlineMediaViewBinding = InlineMediaView.this.binding;
                    PlayerView playerView3 = inlineMediaViewBinding.f;
                    m.checkNotNullExpressionValue(playerView3, "binding.inlineMediaPlayerView");
                    playerView3.setVisibility(0);
                    appMediaPlayer2 = InlineMediaView.this.appMediaPlayer;
                    if (appMediaPlayer2 != null) {
                        MediaSource mediaSource = P;
                        inlineMediaViewBinding4 = InlineMediaView.this.binding;
                        PlayerView playerView4 = inlineMediaViewBinding4.f;
                        m.checkNotNullExpressionValue(playerView4, "binding.inlineMediaPlayerView");
                        AppMediaPlayer.b(appMediaPlayer2, mediaSource, true, false, 0L, playerView4, null, 40);
                    }
                    inlineMediaViewBinding2 = InlineMediaView.this.binding;
                    ImageView imageView2 = inlineMediaViewBinding2.e;
                    m.checkNotNullExpressionValue(imageView2, "binding.inlineMediaPlayButton");
                    imageView2.setVisibility(8);
                    inlineMediaViewBinding3 = InlineMediaView.this.binding;
                    ProgressBar progressBar = inlineMediaViewBinding3.d;
                    m.checkNotNullExpressionValue(progressBar, "binding.inlineMediaLoadingIndicator");
                    progressBar.setVisibility(0);
                }
            });
        }
    }

    public static /* synthetic */ void updateUIWithAttachment$default(InlineMediaView inlineMediaView, MessageAttachment messageAttachment, Integer num, Integer num2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        if ((i & 4) != 0) {
            num2 = null;
        }
        inlineMediaView.updateUIWithAttachment(messageAttachment, num, num2, z2);
    }

    public static /* synthetic */ void updateUIWithEmbed$default(InlineMediaView inlineMediaView, MessageEmbed messageEmbed, Integer num, Integer num2, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            num = null;
        }
        if ((i & 4) != 0) {
            num2 = null;
        }
        inlineMediaView.updateUIWithEmbed(messageEmbed, num, num2, z2);
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.unsubscribeSignal;
    }

    public final void onPause() {
        clearPlayerAndSubscriptions();
    }

    public final void onResume() {
        resetCurrentEmbed();
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
        CoroutineScope coroutineScope;
        m.checkNotNullParameter(view, "view");
        resetCurrentEmbed();
        PlayerView playerView = this.binding.f;
        m.checkNotNullExpressionValue(playerView, "binding.inlineMediaPlayerView");
        if ((playerView.getVideoSurfaceView() instanceof SurfaceView) && (coroutineScope = ViewCoroutineScopeKt.getCoroutineScope(this)) != null) {
            f.H0(coroutineScope, null, null, new InlineMediaView$onViewAttachedToWindow$1(this, null), 3, null);
        }
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        m.checkNotNullParameter(view, "view");
        clearPlayerAndSubscriptions();
    }

    @Override // android.view.View
    public void onVisibilityChanged(View view, int i) {
        m.checkNotNullParameter(view, "changedView");
        super.onVisibilityChanged(view, i);
        if (i == 0) {
            resetCurrentEmbed();
        } else {
            clearPlayerAndSubscriptions();
        }
    }

    public final void updateUIWithAttachment(MessageAttachment messageAttachment, Integer num, Integer num2, boolean z2) {
        EmbedType embedType;
        m.checkNotNullParameter(messageAttachment, "attachment");
        MessageAttachmentType e = messageAttachment.e();
        boolean z3 = false;
        if ((e == MessageAttachmentType.IMAGE || e == MessageAttachmentType.VIDEO) && this.storeUserSettings.getIsAttachmentMediaInline()) {
            RenderableEmbedMedia createRenderableEmbedMediaFromAttachment = EmbedResourceUtils.INSTANCE.createRenderableEmbedMediaFromAttachment(messageAttachment);
            this.featureTag = InlineMediaView.class.getSimpleName() + ": attachment";
            if (messageAttachment.e() == MessageAttachmentType.VIDEO) {
                z3 = true;
            }
            String c = z3 ? messageAttachment.c() : null;
            int ordinal = e.ordinal();
            if (ordinal == 0) {
                embedType = EmbedType.VIDEO;
            } else if (ordinal == 1) {
                embedType = EmbedType.IMAGE;
            } else if (ordinal == 2) {
                embedType = EmbedType.FILE;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            diffViewParamsAndUpdateEmbed(createRenderableEmbedMediaFromAttachment, c, embedType, num, num2, this.featureTag, z2);
        }
    }

    public final void updateUIWithEmbed(MessageEmbed messageEmbed, Integer num, Integer num2, boolean z2) {
        String str;
        m.checkNotNullParameter(messageEmbed, "embed");
        EmbedResourceUtils embedResourceUtils = EmbedResourceUtils.INSTANCE;
        boolean z3 = true;
        if (!(embedResourceUtils.getPreviewImage(messageEmbed) != null) || !this.storeUserSettings.getIsEmbedMediaInlined() || !this.storeUserSettings.getIsRenderEmbedsEnabled()) {
            z3 = false;
        }
        if (z3) {
            this.featureTag = InlineMediaView.class.getSimpleName() + ": embed";
            EmbedVideo m = messageEmbed.m();
            if (m == null || (str = m.b()) == null) {
                EmbedVideo m2 = messageEmbed.m();
                str = m2 != null ? m2.c() : null;
            }
            diffViewParamsAndUpdateEmbed(embedResourceUtils.getPreviewImage(messageEmbed), str, messageEmbed.k(), num, num2, this.featureTag, z2);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InlineMediaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        InlineMediaViewBinding a = InlineMediaViewBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "InlineMediaViewBinding.i…ater.from(context), this)");
        this.binding = a;
        this.storeUserSettings = StoreStream.Companion.getUserSettings();
        this.featureTag = "";
        this.compositeSubscription = new CompositeSubscription();
        addOnAttachStateChangeListener(this);
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.unsubscribeSignal = k0;
    }

    public /* synthetic */ InlineMediaView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InlineMediaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        InlineMediaViewBinding a = InlineMediaViewBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "InlineMediaViewBinding.i…ater.from(context), this)");
        this.binding = a;
        this.storeUserSettings = StoreStream.Companion.getUserSettings();
        this.featureTag = "";
        this.compositeSubscription = new CompositeSubscription();
        addOnAttachStateChangeListener(this);
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.unsubscribeSignal = k0;
    }

    public /* synthetic */ InlineMediaView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
