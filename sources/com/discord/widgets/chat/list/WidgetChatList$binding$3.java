package com.discord.widgets.chat.list;

import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetChatListBinding;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/databinding/WidgetChatListBinding;", "binding", "", "invoke", "(Lcom/discord/databinding/WidgetChatListBinding;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatList$binding$3 extends o implements Function1<WidgetChatListBinding, Unit> {
    public final /* synthetic */ WidgetChatList this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatList$binding$3(WidgetChatList widgetChatList) {
        super(1);
        this.this$0 = widgetChatList;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetChatListBinding widgetChatListBinding) {
        invoke2(widgetChatListBinding);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetChatListBinding widgetChatListBinding) {
        WidgetChatListAdapter widgetChatListAdapter;
        m.checkNotNullParameter(widgetChatListBinding, "binding");
        RecyclerView recyclerView = widgetChatListBinding.f2324b;
        m.checkNotNullExpressionValue(recyclerView, "binding.chatListRecycler");
        recyclerView.setLayoutManager(null);
        RecyclerView recyclerView2 = widgetChatListBinding.f2324b;
        m.checkNotNullExpressionValue(recyclerView2, "binding.chatListRecycler");
        recyclerView2.setAdapter(null);
        widgetChatListAdapter = this.this$0.adapter;
        if (widgetChatListAdapter != null) {
            widgetChatListAdapter.dispose();
        }
    }
}
