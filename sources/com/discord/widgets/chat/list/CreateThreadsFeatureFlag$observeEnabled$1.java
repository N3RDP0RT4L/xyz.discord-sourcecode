package com.discord.widgets.chat.list;

import com.discord.models.experiments.domain.Experiment;
import com.discord.models.guild.Guild;
import com.discord.widgets.chat.list.CreateThreadsFeatureFlag;
import d0.z.d.k;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
/* compiled from: CreateThreadsFeatureFlag.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\b\u0010\u0002\u001a\u0004\u0018\u00010\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/models/experiments/domain/Experiment;", "p1", "p2", "Lcom/discord/models/guild/Guild;", "p3", "", "invoke", "(Lcom/discord/models/experiments/domain/Experiment;Lcom/discord/models/experiments/domain/Experiment;Lcom/discord/models/guild/Guild;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class CreateThreadsFeatureFlag$observeEnabled$1 extends k implements Function3<Experiment, Experiment, Guild, Boolean> {
    public CreateThreadsFeatureFlag$observeEnabled$1(CreateThreadsFeatureFlag.Companion companion) {
        super(3, companion, CreateThreadsFeatureFlag.Companion.class, "computeIsEnabled", "computeIsEnabled(Lcom/discord/models/experiments/domain/Experiment;Lcom/discord/models/experiments/domain/Experiment;Lcom/discord/models/guild/Guild;)Z", 0);
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Boolean invoke(Experiment experiment, Experiment experiment2, Guild guild) {
        return Boolean.valueOf(invoke2(experiment, experiment2, guild));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(Experiment experiment, Experiment experiment2, Guild guild) {
        return ((CreateThreadsFeatureFlag.Companion) this.receiver).computeIsEnabled(experiment, experiment2, guild);
    }
}
