package com.discord.widgets.chat.list;

import android.view.View;
import androidx.core.app.NotificationCompat;
import com.discord.databinding.InlineMediaViewBinding;
import com.discord.panels.PanelState;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.RxCoroutineUtilsKt;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import rx.Observable;
import rx.functions.Func2;
import s.a.c2.d;
/* compiled from: InlineMediaView.kt */
@e(c = "com.discord.widgets.chat.list.InlineMediaView$onViewAttachedToWindow$1", f = "InlineMediaView.kt", l = {89, 416}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InlineMediaView$onViewAttachedToWindow$1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    public int label;
    public final /* synthetic */ InlineMediaView this$0;

    /* compiled from: InlineMediaView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0007\u001a\n \u0001*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0003\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/panels/PanelState;", "kotlin.jvm.PlatformType", "left", "right", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.chat.list.InlineMediaView$onViewAttachedToWindow$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T1, T2, R> implements Func2<PanelState, PanelState, Boolean> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final Boolean call(PanelState panelState, PanelState panelState2) {
            PanelState.c cVar = PanelState.c.a;
            return Boolean.valueOf(m.areEqual(panelState, cVar) || m.areEqual(panelState2, cVar));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InlineMediaView$onViewAttachedToWindow$1(InlineMediaView inlineMediaView, Continuation continuation) {
        super(2, continuation);
        this.this$0 = inlineMediaView;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new InlineMediaView$onViewAttachedToWindow$1(this.this$0, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
        return ((InlineMediaView$onViewAttachedToWindow$1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        int i = this.label;
        if (i == 0) {
            l.throwOnFailure(obj);
            StoreStream.Companion companion = StoreStream.Companion;
            Observable q = Observable.j(companion.getNavigation().observeLeftPanelState(), companion.getNavigation().observeRightPanelState(), AnonymousClass1.INSTANCE).q();
            m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
            this.label = 1;
            obj = RxCoroutineUtilsKt.toFlow(q, this);
            if (obj == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            l.throwOnFailure(obj);
        } else if (i == 2) {
            l.throwOnFailure(obj);
            return Unit.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        s.a.c2.e<Boolean> inlineMediaView$onViewAttachedToWindow$1$invokeSuspend$$inlined$collect$1 = new s.a.c2.e<Boolean>() { // from class: com.discord.widgets.chat.list.InlineMediaView$onViewAttachedToWindow$1$invokeSuspend$$inlined$collect$1
            @Override // s.a.c2.e
            public Object emit(Boolean bool, Continuation continuation) {
                InlineMediaViewBinding inlineMediaViewBinding;
                Boolean bool2 = bool;
                inlineMediaViewBinding = InlineMediaView$onViewAttachedToWindow$1.this.this$0.binding;
                View view = inlineMediaViewBinding.h;
                m.checkNotNullExpressionValue(view, "binding.opacityShim");
                m.checkNotNullExpressionValue(bool2, "isAnyPanelOpen");
                view.setVisibility(bool2.booleanValue() ? 0 : 8);
                return Unit.a;
            }
        };
        this.label = 2;
        if (((d) obj).a(inlineMediaView$onViewAttachedToWindow$1$invokeSuspend$$inlined$collect$1, this) == coroutine_suspended) {
            return coroutine_suspended;
        }
        return Unit.a;
    }
}
