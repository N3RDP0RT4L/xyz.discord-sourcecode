package com.discord.widgets.chat.list;

import com.discord.player.AppMediaPlayer;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: InlineMediaView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/player/AppMediaPlayer$Event;", "event", "", "invoke", "(Lcom/discord/player/AppMediaPlayer$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class InlineMediaView$updateUI$4 extends o implements Function1<AppMediaPlayer.Event, Unit> {
    public final /* synthetic */ InlineMediaView this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public InlineMediaView$updateUI$4(InlineMediaView inlineMediaView) {
        super(1);
        this.this$0 = inlineMediaView;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(AppMediaPlayer.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(AppMediaPlayer.Event event) {
        m.checkNotNullParameter(event, "event");
        this.this$0.handlePlayerEvent(event);
    }
}
