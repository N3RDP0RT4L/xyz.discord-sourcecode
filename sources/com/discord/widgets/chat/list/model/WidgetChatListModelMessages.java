package com.discord.widgets.chat.list.model;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.interaction.Interaction;
import com.discord.api.message.MessageReference;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.api.thread.ThreadMetadata;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreMessageReplies;
import com.discord.stores.StoreMessageState;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreReadStates;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadMessages;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.embed.InviteEmbedModel;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.message.LocalMessageCreatorsKt;
import com.discord.utilities.message.MessageUtils;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.botuikit.ComponentChatListState;
import com.discord.widgets.chat.list.ViewThreadsFeatureFlag;
import com.discord.widgets.chat.list.entries.BlockedMessagesEntry;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.EphemeralMessageEntry;
import com.discord.widgets.chat.list.entries.MessageEntry;
import com.discord.widgets.chat.list.entries.NewMessagesEntry;
import com.discord.widgets.chat.list.entries.ReactionsEntry;
import com.discord.widgets.chat.list.entries.ThreadStarterDividerEntry;
import com.discord.widgets.chat.list.entries.TimestampEntry;
import com.discord.widgets.chat.list.entries.UploadProgressEntry;
import com.discord.widgets.chat.list.model.WidgetChatListModelMessages;
import d0.t.h0;
import d0.t.n;
import d0.t.s;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function6;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func3;
import rx.functions.Func6;
import rx.functions.Func9;
/* compiled from: WidgetChatListModelMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\b\u0018\u0000 12\u00020\u0001:\u0003123Bc\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\n\u0010\u0014\u001a\u00060\u0006j\u0002`\u0007\u0012\n\u0010\u0015\u001a\u00060\u0006j\u0002`\u0007\u0012\u0018\u0010\u0016\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\f\u0012\u0004\u0012\u00020\r\u0018\u00010\u000b\u0012\n\u0010\u0017\u001a\u00060\u0006j\u0002`\u0007\u0012\u000e\u0010\u0018\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007¢\u0006\u0004\b/\u00100J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0014\u0010\n\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\n\u0010\tJ\"\u0010\u000e\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\f\u0012\u0004\u0012\u00020\r\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0014\u0010\u0010\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\u0010\u0010\tJ\u0018\u0010\u0011\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012Jx\u0010\u0019\u001a\u00020\u00002\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\f\b\u0002\u0010\u0014\u001a\u00060\u0006j\u0002`\u00072\f\b\u0002\u0010\u0015\u001a\u00060\u0006j\u0002`\u00072\u001a\b\u0002\u0010\u0016\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\f\u0012\u0004\u0012\u00020\r\u0018\u00010\u000b2\f\b\u0002\u0010\u0017\u001a\u00060\u0006j\u0002`\u00072\u0010\b\u0002\u0010\u0018\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u001a\u0010#\u001a\u00020\"2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b#\u0010$R\u001d\u0010\u0014\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010%\u001a\u0004\b&\u0010\tR\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010'\u001a\u0004\b(\u0010\u0005R+\u0010\u0016\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\f\u0012\u0004\u0012\u00020\r\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010)\u001a\u0004\b*\u0010\u000fR!\u0010\u0018\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010+\u001a\u0004\b,\u0010\u0012R\u001d\u0010\u0015\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010%\u001a\u0004\b-\u0010\tR\u001d\u0010\u0017\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010%\u001a\u0004\b.\u0010\t¨\u00064"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages;", "", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "component1", "()Ljava/util/List;", "", "Lcom/discord/primitives/MessageId;", "component2", "()J", "component3", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "component4", "()Ljava/util/Map;", "component5", "component6", "()Ljava/lang/Long;", "items", "oldestMessageId", "newestKnownMessageId", "guildMembers", "newMessagesMarkerMessageId", "newestSentByUserMessageId", "copy", "(Ljava/util/List;JJLjava/util/Map;JLjava/lang/Long;)Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getOldestMessageId", "Ljava/util/List;", "getItems", "Ljava/util/Map;", "getGuildMembers", "Ljava/lang/Long;", "getNewestSentByUserMessageId", "getNewestKnownMessageId", "getNewMessagesMarkerMessageId", HookHelper.constructorName, "(Ljava/util/List;JJLjava/util/Map;JLjava/lang/Long;)V", "Companion", "Items", "MessagesWithMetadata", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListModelMessages {
    public static final Companion Companion = new Companion(null);
    private static final int MAX_CONCAT_COUNT = 5;
    private static final long MESSAGE_CONCAT_TIMESTAMP_DELTA_THRESHOLD = 420000;
    private final Map<Long, GuildMember> guildMembers;
    private final List<ChatListEntry> items;
    private final long newMessagesMarkerMessageId;
    private final long newestKnownMessageId;
    private final Long newestSentByUserMessageId;
    private final long oldestMessageId;

    /* compiled from: WidgetChatListModelMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ä\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\ba\u0010bJ)\u0010\b\u001a\u0010\u0012\f\u0012\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00070\u0005*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\b\u0010\tJW\u0010\u000f\u001a:\u0012\u0016\u0012\u0014 \u000e*\n\u0018\u00010\u0006j\u0004\u0018\u0001`\r0\u0006j\u0002`\r \u000e*\u001c\u0012\u0016\u0012\u0014 \u000e*\n\u0018\u00010\u0006j\u0004\u0018\u0001`\r0\u0006j\u0002`\r\u0018\u00010\u00050\u0005*\u00020\n2\n\u0010\f\u001a\u00060\u0006j\u0002`\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010JU\u0010\u001a\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u0012\u001a\u00020\u00112\n\u0010\u0014\u001a\u00060\u0006j\u0002`\u00132\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0013\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0004\u001a\u00020\u00032\u000e\u0010\u0018\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007H\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ=\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00030\u001f2\b\u0010\u001c\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u001dH\u0002¢\u0006\u0004\b \u0010!J%\u0010(\u001a\u00020'2\f\u0010$\u001a\b\u0012\u0004\u0012\u00020#0\"2\u0006\u0010&\u001a\u00020%H\u0002¢\u0006\u0004\b(\u0010)J\u001f\u0010-\u001a\u00020,2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010+\u001a\u00020\u0006H\u0002¢\u0006\u0004\b-\u0010.J/\u00102\u001a\u00020\u00062\u0006\u00100\u001a\u00020/2\u0006\u00101\u001a\u00020\u00062\u0006\u0010*\u001a\u00020\u00062\u0006\u0010+\u001a\u00020\u0006H\u0002¢\u0006\u0004\b2\u00103J;\u00106\u001a\u00020,2\u0006\u00100\u001a\u00020/2\n\u00104\u001a\u00060\u0006j\u0002`\r2\u0006\u00105\u001a\u00020,2\u0006\u00101\u001a\u00020\u00062\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b6\u00107J1\u0010:\u001a\u00020%2\u0006\u00100\u001a\u00020/2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u00108\u001a\u00020%2\u0006\u00109\u001a\u00020,H\u0002¢\u0006\u0004\b:\u0010;J)\u0010=\u001a\u00020,2\u0006\u00100\u001a\u00020/2\u0006\u0010\u0012\u001a\u00020\u00112\b\u0010<\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0004\b=\u0010>J\u001b\u0010@\u001a\b\u0012\u0004\u0012\u00020?0\u00052\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b@\u0010AJ-\u0010C\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020#0B0\u00052\u0006\u0010\u0004\u001a\u00020\u00032\n\u00101\u001a\u00060\u0006j\u0002`\r¢\u0006\u0004\bC\u0010DJ\u008f\u0002\u0010Y\u001a\b\u0012\u0004\u0012\u00020#0B2\u0006\u0010\u0004\u001a\u00020\u00032\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0013\u0012\u0004\u0012\u00020\u00160\u00152\u0012\u0010F\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020E0\u00152\u0012\u0010G\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020%0\u00152\b\u0010H\u001a\u0004\u0018\u00010\u00032\b\u0010J\u001a\u0004\u0018\u00010I2\u0006\u0010\u0012\u001a\u00020\u00112\b\u0010L\u001a\u0004\u0018\u00010K2\u0016\u0010N\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\r\u0012\u0004\u0012\u00020M0\u00152\u0006\u00109\u001a\u00020,2\u0006\u0010O\u001a\u00020,2\u000e\u0010\u0018\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\u0006\u0010P\u001a\u00020,2\u0006\u0010Q\u001a\u00020,2\u0006\u0010R\u001a\u00020,2\n\u0010\u0014\u001a\u00060\u0006j\u0002`\u00132\u0006\u0010S\u001a\u00020,2\u0016\u0010U\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\r\u0012\u0004\u0012\u00020T0\u00152\u0006\u0010W\u001a\u00020V2\b\b\u0002\u0010X\u001a\u00020,¢\u0006\u0004\bY\u0010ZJÁ\u0001\u0010[\u001a\b\u0012\u0004\u0012\u00020#0B2\b\u0010\u001c\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0013\u0012\u0004\u0012\u00020\u00160\u00152\u0012\u0010F\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020E0\u00152\u0012\u0010G\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020%0\u00152\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010P\u001a\u00020,2\u0006\u0010Q\u001a\u00020,2\u0006\u0010R\u001a\u00020,2\n\u0010\u0014\u001a\u00060\u0006j\u0002`\u00132\u0006\u0010S\u001a\u00020,2\u0016\u0010U\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\r\u0012\u0004\u0012\u00020T0\u00152\u0006\u0010W\u001a\u00020V¢\u0006\u0004\b[\u0010\\R\u0016\u0010]\u001a\u00020%8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b]\u0010^R\u0016\u0010_\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b_\u0010`¨\u0006c"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$Companion;", "", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "", "Lcom/discord/api/permission/PermissionBit;", "observePermissionsForChannel", "(Lcom/discord/stores/StorePermissions;Lcom/discord/api/channel/Channel;)Lrx/Observable;", "Lcom/discord/stores/StoreReadStates;", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/MessageId;", "kotlin.jvm.PlatformType", "observeUnreadMarkerMessageId", "(Lcom/discord/stores/StoreReadStates;J)Lrx/Observable;", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/primitives/UserId;", "meUserId", "", "Lcom/discord/models/member/GuildMember;", "guildMembers", "permissionsForChannel", "Lcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;", "parseWelcomeData", "(Lcom/discord/models/message/Message;JLjava/util/Map;Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Lcom/discord/widgets/chat/list/entries/MessageEntry$WelcomeCtaData;", "parentChannel", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;", "messagesWithMetadata", "Lkotlin/Pair;", "getThreadStarterMessageAndChannel", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/models/message/Message;Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;)Lkotlin/Pair;", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "messageItems", "", "threadSpineStartIndex", "", "enableThreadSpine", "(Ljava/util/List;I)V", "timestamp", "nextDayTimestamp", "", "willAddTimestamp", "(JJ)Z", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$Items;", "items", "messageId", "tryAddTimestamp", "(Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$Items;JJJ)J", "newMessagesMarkerMessageId", "messageMostRecent", "tryAddNewMessagesSeparator", "(Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$Items;JZJLcom/discord/api/channel/Channel;)Z", "blockedContiguousMessageCount", "blockedChunkExpanded", "addBlockedMessage", "(Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$Items;Lcom/discord/models/message/Message;IZ)I", "previousMessage", "shouldConcatMessage", "(Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$Items;Lcom/discord/models/message/Message;Lcom/discord/models/message/Message;)Z", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages;", "get", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "", "getSingleMessage", "(Lcom/discord/api/channel/Channel;J)Lrx/Observable;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "blockedRelationships", "thread", "Lcom/discord/stores/StoreThreadMessages$ThreadState;", "threadEmbedMetadata", "Lcom/discord/stores/StoreMessageState$State;", "messageState", "Lcom/discord/stores/StoreMessageReplies$MessageState;", "repliedMessages", "shouldConcat", "animateEmojis", "autoPlayGifs", "renderEmbeds", "showBotComponents", "Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;", "componentStoreState", "Lcom/discord/utilities/embed/InviteEmbedModel;", "inviteEmbedModel", "isThreadStarterMessage", "getMessageItems", "(Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/discord/api/channel/Channel;Lcom/discord/stores/StoreThreadMessages$ThreadState;Lcom/discord/models/message/Message;Lcom/discord/stores/StoreMessageState$State;Ljava/util/Map;ZZLjava/lang/Long;ZZZJZLjava/util/Map;Lcom/discord/utilities/embed/InviteEmbedModel;Z)Ljava/util/List;", "getThreadStarterMessageItems", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/discord/models/message/Message;Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;ZZZJZLjava/util/Map;Lcom/discord/utilities/embed/InviteEmbedModel;)Ljava/util/List;", "MAX_CONCAT_COUNT", "I", "MESSAGE_CONCAT_TIMESTAMP_DELTA_THRESHOLD", "J", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final int addBlockedMessage(Items items, Message message, int i, boolean z2) {
            if (i <= 0) {
                return i;
            }
            m.checkNotNull(message);
            items.addItem(new BlockedMessagesEntry(message, i, z2));
            return 0;
        }

        private final void enableThreadSpine(List<ChatListEntry> list, int i) {
            int size = list.size();
            while (i < size) {
                list.get(i).setShouldShowThreadSpine(true);
                i++;
            }
        }

        public static /* synthetic */ List getMessageItems$default(Companion companion, Channel channel, Map map, Map map2, Map map3, Channel channel2, StoreThreadMessages.ThreadState threadState, Message message, StoreMessageState.State state, Map map4, boolean z2, boolean z3, Long l, boolean z4, boolean z5, boolean z6, long j, boolean z7, Map map5, InviteEmbedModel inviteEmbedModel, boolean z8, int i, Object obj) {
            return companion.getMessageItems(channel, map, map2, map3, channel2, threadState, message, state, map4, z2, z3, l, z4, z5, z6, j, z7, map5, inviteEmbedModel, (i & 524288) != 0 ? false : z8);
        }

        public final Pair<Message, Channel> getThreadStarterMessageAndChannel(Channel channel, Channel channel2, Message message, MessagesWithMetadata messagesWithMetadata) {
            Message message2;
            Map<Long, StoreMessageReplies.MessageState> parentChannelMessageReplyState = messagesWithMetadata.getParentChannelMessageReplyState();
            MessageReference messageReference = message.getMessageReference();
            StoreMessageReplies.MessageState messageState = parentChannelMessageReplyState.get(messageReference != null ? messageReference.c() : null);
            if (channel == null || !(messageState instanceof StoreMessageReplies.MessageState.Loaded)) {
                long h = channel2.h();
                User author = message.getAuthor();
                m.checkNotNull(author);
                message2 = LocalMessageCreatorsKt.createThreadStarterMessageNotFoundMessage(h, author, ClockFactory.get());
                channel = channel2;
            } else {
                message2 = ((StoreMessageReplies.MessageState.Loaded) messageState).getMessage();
            }
            return new Pair<>(message2, channel);
        }

        private final Observable<Long> observePermissionsForChannel(StorePermissions storePermissions, Channel channel) {
            if (ChannelUtils.A(channel)) {
                k kVar = new k(null);
                m.checkNotNullExpressionValue(kVar, "Observable.just(null)");
                return kVar;
            } else if (ChannelUtils.x(channel)) {
                k kVar2 = new k(null);
                m.checkNotNullExpressionValue(kVar2, "Observable.just(null)");
                return kVar2;
            } else {
                Observable<Long> q = storePermissions.observePermissionsForChannel(channel.h()).q();
                m.checkNotNullExpressionValue(q, "observePermissionsForCha…  .distinctUntilChanged()");
                return q;
            }
        }

        private final Observable<Long> observeUnreadMarkerMessageId(StoreReadStates storeReadStates, long j) {
            Observable<R> F = storeReadStates.getUnreadMarker(j).F(WidgetChatListModelMessages$Companion$observeUnreadMarkerMessageId$1.INSTANCE);
            m.checkNotNullExpressionValue(F, "getUnreadMarker(channelI… marker.messageId ?: 0L }");
            return ObservableExtensionsKt.computationLatest(F).q();
        }

        private final MessageEntry.WelcomeCtaData parseWelcomeData(Message message, long j, Map<Long, GuildMember> map, Channel channel, Long l) {
            Guild guild;
            Boolean e;
            Integer type = message.getType();
            if (type == null || type.intValue() != 7 || (guild = StoreStream.Companion.getGuilds().getGuild(channel.f())) == null) {
                return null;
            }
            boolean z2 = true;
            boolean z3 = false;
            if (!((guild.getSystemChannelFlags() & 8) == 0)) {
                return null;
            }
            GuildMember guildMember = map.get(Long.valueOf(j));
            if (guildMember != null) {
                z2 = guildMember.getPending();
            }
            if (z2 || !PermissionUtils.can(Permission.SEND_MESSAGES, l)) {
                return null;
            }
            User author = message.getAuthor();
            if (!(author == null || (e = author.e()) == null)) {
                z3 = e.booleanValue();
            }
            if (z3) {
                return null;
            }
            return new MessageEntry.WelcomeCtaData(MessageUtils.INSTANCE.getWelcomeSticker(j, message.getId()), channel);
        }

        public final boolean shouldConcatMessage(Items items, Message message, Message message2) {
            MGRecyclerDataPayload listItemMostRecentlyAdded;
            MGRecyclerDataPayload listItemMostRecentlyAdded2;
            MGRecyclerDataPayload listItemMostRecentlyAdded3;
            Integer type;
            if (message2 == null || message2.isSystemMessage() || message.hasThread() || message2.hasThread()) {
                return false;
            }
            Integer type2 = message.getType();
            if ((type2 == null || type2.intValue() != 0) && ((type = message.getType()) == null || type.intValue() != -1)) {
                return false;
            }
            MGRecyclerDataPayload listItemMostRecentlyAdded4 = items.getListItemMostRecentlyAdded();
            if ((listItemMostRecentlyAdded4 == null || listItemMostRecentlyAdded4.getType() != 0) && (((listItemMostRecentlyAdded = items.getListItemMostRecentlyAdded()) == null || listItemMostRecentlyAdded.getType() != 1) && (((listItemMostRecentlyAdded2 = items.getListItemMostRecentlyAdded()) == null || listItemMostRecentlyAdded2.getType() != 21) && ((listItemMostRecentlyAdded3 = items.getListItemMostRecentlyAdded()) == null || listItemMostRecentlyAdded3.getType() != 4)))) {
                return false;
            }
            User author = message2.getAuthor();
            String str = null;
            Long valueOf = author != null ? Long.valueOf(author.i()) : null;
            User author2 = message.getAuthor();
            if (!m.areEqual(valueOf, author2 != null ? Long.valueOf(author2.i()) : null)) {
                return false;
            }
            UtcDateTime timestamp = message.getTimestamp();
            long j = 0;
            long g = timestamp != null ? timestamp.g() : 0L;
            UtcDateTime timestamp2 = message2.getTimestamp();
            if (timestamp2 != null) {
                j = timestamp2.g();
            }
            if (g - j >= WidgetChatListModelMessages.MESSAGE_CONCAT_TIMESTAMP_DELTA_THRESHOLD || message2.hasAttachments() || message2.hasEmbeds()) {
                return false;
            }
            List<User> mentions = message2.getMentions();
            if (!(mentions == null || mentions.isEmpty()) || message.hasAttachments() || message.hasEmbeds()) {
                return false;
            }
            List<User> mentions2 = message.getMentions();
            if (!(mentions2 == null || mentions2.isEmpty()) || items.getConcatCount() >= 5) {
                return false;
            }
            if (message.isWebhook()) {
                User author3 = message2.getAuthor();
                String r = author3 != null ? author3.r() : null;
                User author4 = message.getAuthor();
                if (author4 != null) {
                    str = author4.r();
                }
                if (!m.areEqual(r, str)) {
                    return false;
                }
            }
            return true;
        }

        public final boolean tryAddNewMessagesSeparator(Items items, long j, boolean z2, long j2, Channel channel) {
            boolean z3 = true;
            boolean z4 = j > 0;
            if (z2 || !z4 || MessageUtils.compareMessages(Long.valueOf(j2), Long.valueOf(j)) != 0) {
                z3 = false;
            }
            if (z3) {
                items.addItem(new NewMessagesEntry(channel.h(), j2));
            }
            return z3;
        }

        public final long tryAddTimestamp(Items items, long j, long j2, long j3) {
            if (!willAddTimestamp(j2, j3)) {
                return j3;
            }
            items.addItem(new TimestampEntry(j, j2));
            Calendar calendar = TimeUtils.toCalendar(j2);
            calendar.add(5, 1);
            calendar.set(11, 0);
            calendar.set(12, 0);
            calendar.set(13, 0);
            return calendar.getTimeInMillis();
        }

        public final boolean willAddTimestamp(long j, long j2) {
            return j > j2;
        }

        public final Observable<WidgetChatListModelMessages> get(Channel channel) {
            m.checkNotNullParameter(channel, "channel");
            Observable<MessagesWithMetadata> observable = MessagesWithMetadata.Companion.get(channel);
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<Channel> observeChannel = companion.getChannels().observeChannel(channel.r());
            Observable<Map<Long, Integer>> observeForType = companion.getUserRelationships().observeForType(2);
            Observable<List<Long>> observeExpandedBlockedMessageIds = companion.getChat().observeExpandedBlockedMessageIds();
            Observable<Map<Long, GuildMember>> observeGuildMembers = companion.getGuilds().observeGuildMembers(channel.f());
            m.checkNotNullExpressionValue(observeGuildMembers, "StoreStream\n            …dMembers(channel.guildId)");
            Observable<Long> observeUnreadMarkerMessageId = observeUnreadMarkerMessageId(companion.getReadStates(), channel.h());
            m.checkNotNullExpressionValue(observeUnreadMarkerMessageId, "StoreStream\n            …rkerMessageId(channel.id)");
            return ObservableCombineLatestOverloadsKt.combineLatest(observable, observeChannel, observeForType, observeExpandedBlockedMessageIds, observeGuildMembers, observeUnreadMarkerMessageId, companion.getGuilds().observeRoles(channel.f()), observePermissionsForChannel(companion.getPermissions(), channel), StoreUserSettings.observeIsAnimatedEmojisEnabled$default(companion.getUserSettings(), false, 1, null), StoreUserSettings.observeIsAutoPlayGifsEnabled$default(companion.getUserSettings(), false, 1, null), companion.getUserSettings().observeIsRenderEmbedsEnabled(), companion.getUsers().observeMeId(), ComponentChatListState.INSTANCE.observeChatListComponentState(), InviteEmbedModel.Companion.observe$default(InviteEmbedModel.Companion, null, null, null, null, 15, null), new WidgetChatListModelMessages$Companion$get$1(channel));
        }

        public final List<ChatListEntry> getMessageItems(Channel channel, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, Integer> map3, Channel channel2, StoreThreadMessages.ThreadState threadState, Message message, StoreMessageState.State state, Map<Long, ? extends StoreMessageReplies.MessageState> map4, boolean z2, boolean z3, Long l, boolean z4, boolean z5, boolean z6, long j, boolean z7, Map<Long, ComponentChatListState.ComponentStoreState> map5, InviteEmbedModel inviteEmbedModel, boolean z8) {
            ArrayList arrayList;
            MessageEntry.ReplyData replyData;
            Map<Long, GuildMember> map6;
            Long l2;
            User c;
            User user;
            MessageEntry.ReplyData replyData2;
            StoreMessageReplies.MessageState messageState;
            MessageEntry.ReplyData replyData3;
            boolean z9;
            MessageEntry messageEntry;
            boolean z10;
            StoreMessageReplies.MessageState messageState2;
            MessageEntry.ReplyData replyData4;
            Map<Long, Integer> map7;
            Long l3;
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(map, "guildMembers");
            m.checkNotNullParameter(map2, "guildRoles");
            m.checkNotNullParameter(map3, "blockedRelationships");
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(map4, "repliedMessages");
            m.checkNotNullParameter(map5, "componentStoreState");
            m.checkNotNullParameter(inviteEmbedModel, "inviteEmbedModel");
            Integer type = message.getType();
            if (type != null && type.intValue() == 38 && !PermissionUtils.can(1L, l)) {
                return n.emptyList();
            }
            ArrayList arrayList2 = new ArrayList();
            MessageReference messageReference = message.getMessageReference();
            Long l4 = null;
            StoreMessageReplies.MessageState messageState3 = map4.get(messageReference != null ? messageReference.c() : null);
            if (messageState3 != null) {
                boolean z11 = messageState3 instanceof StoreMessageReplies.MessageState.Loaded;
                if (z11) {
                    z9 = z11;
                    replyData3 = replyData2;
                    messageState = messageState3;
                    arrayList = arrayList2;
                    Object firstOrNull = u.firstOrNull((List<? extends Object>) getMessageItems$default(this, channel, map, map2, map3, channel2, threadState, ((StoreMessageReplies.MessageState.Loaded) messageState3).getMessage(), state, map4, z2, false, null, false, z5, z6, j, z7, map5, inviteEmbedModel, false, 524288, null));
                    if (!(firstOrNull instanceof MessageEntry)) {
                        firstOrNull = null;
                    }
                    messageEntry = (MessageEntry) firstOrNull;
                } else {
                    replyData3 = replyData2;
                    messageState = messageState3;
                    arrayList = arrayList2;
                    z9 = z11;
                    messageEntry = null;
                }
                if (z9) {
                    messageState2 = messageState;
                    User author = ((StoreMessageReplies.MessageState.Loaded) messageState2).getMessage().getAuthor();
                    if (author != null) {
                        l3 = Long.valueOf(author.i());
                        map7 = map3;
                    } else {
                        map7 = map3;
                        l3 = null;
                    }
                    z10 = map7.containsKey(l3);
                    replyData4 = replyData3;
                } else {
                    messageState2 = messageState;
                    replyData4 = replyData3;
                    z10 = false;
                }
                replyData = new MessageEntry.ReplyData(messageState2, messageEntry, z10);
            } else {
                arrayList = arrayList2;
                replyData = null;
            }
            User author2 = message.getAuthor();
            if (author2 != null) {
                l2 = Long.valueOf(author2.i());
                map6 = map;
            } else {
                map6 = map;
                l2 = null;
            }
            GuildMember guildMember = map6.get(l2);
            List<User> mentions = message.getMentions();
            GuildMember guildMember2 = map6.get((mentions == null || (user = (User) u.firstOrNull((List<? extends Object>) mentions)) == null) ? null : Long.valueOf(user.i()));
            Map<Long, GuildRole> map8 = RoleUtils.containsRoleMentions(message.getContent()) ? map2 : null;
            Map<Long, String> nickOrUsernames = MessageUtils.getNickOrUsernames(message, channel, map6, channel.n());
            Interaction interaction = message.getInteraction();
            if (!(interaction == null || (c = interaction.c()) == null)) {
                l4 = Long.valueOf(c.i());
            }
            MessageEntry messageEntry2 = new MessageEntry(message, state, guildMember, guildMember2, map8, nickOrUsernames, z3, z2, z4, replyData, map6.get(l4), z8, parseWelcomeData(message, j, map, channel, l), l);
            ArrayList arrayList3 = arrayList;
            arrayList3.add(messageEntry2);
            int size = arrayList3.size();
            ChatListEntry.Companion companion = ChatListEntry.Companion;
            arrayList3.addAll(companion.createEmbedEntries(message, state, z2, z4, z5, z6, channel.f(), z8));
            arrayList3.addAll(companion.createStickerEntries(message));
            arrayList3.addAll(companion.createGameInviteEntries(message));
            arrayList3.addAll(companion.createSpotifyListenTogetherEntries(message));
            arrayList3.addAll(companion.createInviteEntries(message, inviteEmbedModel));
            arrayList3.addAll(companion.createGuildTemplateEntries(message));
            arrayList3.addAll(companion.createGiftEntries(message));
            if (z8) {
                arrayList3.add(new ThreadStarterDividerEntry(channel.h(), message.getId()));
                return arrayList3;
            }
            if (z7) {
                arrayList3.addAll(companion.createBotComponentEntries(message, channel.f(), map5.get(Long.valueOf(message.getId())), z4));
            }
            boolean z12 = true;
            if (!message.getReactionsMap().isEmpty()) {
                ThreadMetadata y2 = channel.y();
                boolean z13 = y2 == null || !y2.b();
                if (!PermissionUtils.can(64L, l) || !z13) {
                    z12 = false;
                }
                arrayList3.add(new ReactionsEntry(message, z13, z12, z4));
            }
            if (message.getHasLocalUploads() && !message.isFailed()) {
                String nonce = message.getNonce();
                if (nonce == null) {
                    nonce = "";
                }
                arrayList3.add(new UploadProgressEntry(nonce, message.getChannelId()));
            }
            if (ViewThreadsFeatureFlag.Companion.getINSTANCE().isEnabled() && message.hasThread() && channel2 != null) {
                enableThreadSpine(arrayList3, size);
                arrayList3.addAll(companion.createThreadEmbedEntries(channel, map, message, channel2, threadState, map2, z4, z6));
            }
            if (message.isEphemeralMessage() || (message.isInteraction() && message.isFailed())) {
                arrayList3.add(new EphemeralMessageEntry(message));
            }
            return arrayList3;
        }

        public final Observable<List<ChatListEntry>> getSingleMessage(final Channel channel, final long j) {
            m.checkNotNullParameter(channel, "channel");
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<List<ChatListEntry>> c = Observable.c(companion.getMessages().observeMessagesForChannel(channel.h(), j), MessagesWithMetadata.Companion.get(channel), companion.getGuilds().observeGuildMembers(channel.f()), companion.getGuilds().observeRoles(channel.f()), StoreUserSettings.observeIsAnimatedEmojisEnabled$default(companion.getUserSettings(), false, 1, null), StoreUserSettings.observeIsAutoPlayGifsEnabled$default(companion.getUserSettings(), false, 1, null), companion.getUsers().observeMeId(), companion.getUserSettings().observeIsRenderEmbedsEnabled(), InviteEmbedModel.Companion.observe$default(InviteEmbedModel.Companion, null, null, null, null, 15, null), new Func9<Message, MessagesWithMetadata, Map<Long, ? extends GuildMember>, Map<Long, ? extends GuildRole>, Boolean, Boolean, Long, Boolean, InviteEmbedModel, List<? extends ChatListEntry>>() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModelMessages$Companion$getSingleMessage$1
                @Override // rx.functions.Func9
                public /* bridge */ /* synthetic */ List<? extends ChatListEntry> call(Message message, WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata, Map<Long, ? extends GuildMember> map, Map<Long, ? extends GuildRole> map2, Boolean bool, Boolean bool2, Long l, Boolean bool3, InviteEmbedModel inviteEmbedModel) {
                    return call2(message, messagesWithMetadata, (Map<Long, GuildMember>) map, (Map<Long, GuildRole>) map2, bool, bool2, l, bool3, inviteEmbedModel);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final List<ChatListEntry> call2(Message message, WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Boolean bool, Boolean bool2, Long l, Boolean bool3, InviteEmbedModel inviteEmbedModel) {
                    if (message != null) {
                        WidgetChatListModelMessages.Companion companion2 = WidgetChatListModelMessages.Companion;
                        Channel channel2 = Channel.this;
                        m.checkNotNullExpressionValue(map, "guildMembers");
                        m.checkNotNullExpressionValue(map2, "guildRoles");
                        HashMap hashMap = new HashMap();
                        StoreMessageState.State state = messagesWithMetadata.getMessageState().get(Long.valueOf(j));
                        Map<Long, StoreMessageReplies.MessageState> messageReplyState = messagesWithMetadata.getMessageReplyState();
                        m.checkNotNullExpressionValue(bool, "allowAnimatedEmojis");
                        boolean booleanValue = bool.booleanValue();
                        m.checkNotNullExpressionValue(bool2, "autoPlayGifs");
                        boolean booleanValue2 = bool2.booleanValue();
                        m.checkNotNullExpressionValue(l, "meUserId");
                        long longValue = l.longValue();
                        m.checkNotNullExpressionValue(bool3, "renderEmbeds");
                        boolean booleanValue3 = bool3.booleanValue();
                        HashMap hashMap2 = new HashMap();
                        m.checkNotNullExpressionValue(inviteEmbedModel, "inviteEmbedModel");
                        List<ChatListEntry> asReversed = s.asReversed(WidgetChatListModelMessages.Companion.getMessageItems$default(companion2, channel2, map, map2, hashMap, null, null, message, state, messageReplyState, false, false, null, booleanValue, booleanValue2, booleanValue3, longValue, false, hashMap2, inviteEmbedModel, false, 524288, null));
                        if (asReversed != null) {
                            return asReversed;
                        }
                    }
                    return n.emptyList();
                }
            });
            m.checkNotNullExpressionValue(c, "Observable.combineLatest… ?: emptyList()\n        }");
            return c;
        }

        public final List<ChatListEntry> getThreadStarterMessageItems(Channel channel, Channel channel2, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Map<Long, Integer> map3, Message message, MessagesWithMetadata messagesWithMetadata, boolean z2, boolean z3, boolean z4, long j, boolean z5, Map<Long, ComponentChatListState.ComponentStoreState> map4, InviteEmbedModel inviteEmbedModel) {
            m.checkNotNullParameter(channel2, "channel");
            m.checkNotNullParameter(map, "guildMembers");
            m.checkNotNullParameter(map2, "guildRoles");
            m.checkNotNullParameter(map3, "blockedRelationships");
            m.checkNotNullParameter(message, "message");
            m.checkNotNullParameter(messagesWithMetadata, "messagesWithMetadata");
            m.checkNotNullParameter(map4, "componentStoreState");
            m.checkNotNullParameter(inviteEmbedModel, "inviteEmbedModel");
            Pair<Message, Channel> threadStarterMessageAndChannel = getThreadStarterMessageAndChannel(channel, channel2, message, messagesWithMetadata);
            Message first = threadStarterMessageAndChannel.getFirst();
            return getMessageItems(threadStarterMessageAndChannel.getSecond(), map, map2, map3, null, null, first, messagesWithMetadata.getMessageState().get(Long.valueOf(first.getId())), messagesWithMetadata.getParentChannelMessageReplyState(), false, false, null, z2, z3, z4, j, z5, map4, inviteEmbedModel, true);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChatListModelMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010!\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\n\b\u0002\u0018\u0000 !2\u00020\u0001:\u0001!B\u000f\u0012\u0006\u0010\u001f\u001a\u00020\u0018¢\u0006\u0004\b \u0010\u001eJ\u001b\u0010\u0006\u001a\u00020\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\t\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0003¢\u0006\u0004\b\t\u0010\nR$\u0010\f\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R(\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u00128\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0007R\"\u0010\u0019\u001a\u00020\u00188\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001e¨\u0006\""}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$Items;", "", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "itemList", "", "addItems", "(Ljava/util/List;)V", "item", "addItem", "(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "listItemMostRecentlyAdded", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "getListItemMostRecentlyAdded", "()Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "setListItemMostRecentlyAdded", "(Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V", "", "items", "Ljava/util/List;", "getItems", "()Ljava/util/List;", "setItems", "", "concatCount", "I", "getConcatCount", "()I", "setConcatCount", "(I)V", "size", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Items {
        private static final Companion Companion = new Companion(null);
        @Deprecated
        private static final int LIST_CAPACITY_BUFFER = 10;
        private int concatCount;
        private List<ChatListEntry> items;
        private MGRecyclerDataPayload listItemMostRecentlyAdded;

        /* compiled from: WidgetChatListModelMessages.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$Items$Companion;", "", "", "LIST_CAPACITY_BUFFER", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Items(int i) {
            this.items = new ArrayList(i + 10);
        }

        public final void addItem(ChatListEntry chatListEntry) {
            m.checkNotNullParameter(chatListEntry, "item");
            this.items.add(chatListEntry);
            this.listItemMostRecentlyAdded = chatListEntry;
        }

        public final void addItems(List<? extends ChatListEntry> list) {
            m.checkNotNullParameter(list, "itemList");
            if (!list.isEmpty()) {
                this.items.addAll(list);
                this.listItemMostRecentlyAdded = list.get(list.size() - 1);
            }
        }

        public final int getConcatCount() {
            return this.concatCount;
        }

        public final List<ChatListEntry> getItems() {
            return this.items;
        }

        public final MGRecyclerDataPayload getListItemMostRecentlyAdded() {
            return this.listItemMostRecentlyAdded;
        }

        public final void setConcatCount(int i) {
            this.concatCount = i;
        }

        public final void setItems(List<ChatListEntry> list) {
            m.checkNotNullParameter(list, "<set-?>");
            this.items = list;
        }

        public final void setListItemMostRecentlyAdded(MGRecyclerDataPayload mGRecyclerDataPayload) {
            this.listItemMostRecentlyAdded = mGRecyclerDataPayload;
        }
    }

    /* compiled from: WidgetChatListModelMessages.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u0000 02\u00020\u0001:\u00010B\u008d\u0001\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0016\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0006\u0012\u0016\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\r0\u0006\u0012\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\u000f0\u0006\u0012\u0016\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u0006\u0012\u0016\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u0006¢\u0006\u0004\b.\u0010/J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ \u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\r0\u0006HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000bJ \u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\u000f0\u0006HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000bJ \u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u0006HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u000bJ \u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u0006HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u000bJ¢\u0001\u0010\u001a\u001a\u00020\u00002\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0018\b\u0002\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00062\u0018\b\u0002\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\r0\u00062\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\u000f0\u00062\u0018\b\u0002\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u00062\u0018\b\u0002\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u0006HÆ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u001d\u001a\u00020\u001cHÖ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010 \u001a\u00020\u001fHÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010$\u001a\u00020#2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b'\u0010\u0005R)\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\r0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\u000bR)\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010(\u001a\u0004\b*\u0010\u000bR)\u0010\u0018\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b+\u0010\u000bR)\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\f\u0012\u0004\u0012\u00020\u000f0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010(\u001a\u0004\b,\u0010\u000bR)\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u00110\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010(\u001a\u0004\b-\u0010\u000b¨\u00061"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;", "", "", "Lcom/discord/models/message/Message;", "component1", "()Ljava/util/List;", "", "", "Lcom/discord/primitives/MessageId;", "Lcom/discord/stores/StoreMessageState$State;", "component2", "()Ljava/util/Map;", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component3", "Lcom/discord/stores/StoreThreadMessages$ThreadState;", "component4", "Lcom/discord/stores/StoreMessageReplies$MessageState;", "component5", "component6", "messages", "messageState", "messageThreads", "threadCountsAndLatestMessages", "messageReplyState", "parentChannelMessageReplyState", "copy", "(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getMessages", "Ljava/util/Map;", "getMessageThreads", "getMessageState", "getMessageReplyState", "getThreadCountsAndLatestMessages", "getParentChannelMessageReplyState", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MessagesWithMetadata {
        public static final Companion Companion = new Companion(null);
        private final Map<Long, StoreMessageReplies.MessageState> messageReplyState;
        private final Map<Long, StoreMessageState.State> messageState;
        private final Map<Long, Channel> messageThreads;
        private final List<Message> messages;
        private final Map<Long, StoreMessageReplies.MessageState> parentChannelMessageReplyState;
        private final Map<Long, StoreThreadMessages.ThreadState> threadCountsAndLatestMessages;

        /* compiled from: WidgetChatListModelMessages.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ!\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001b\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u0007\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata$Companion;", "", "", "Lcom/discord/models/message/Message;", "messages", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;", "get", "(Ljava/util/List;)Lrx/Observable;", "Lcom/discord/api/channel/Channel;", "channel", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<MessagesWithMetadata> get(final List<Message> list) {
                m.checkNotNullParameter(list, "messages");
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<MessagesWithMetadata> i = Observable.i(companion.getMessageState().getMessageState(), companion.getChannels().observeThreadsFromMessages(list), companion.getThreadMessages().observeThreadCountAndLatestMessage(), new Func3<Map<Long, ? extends StoreMessageState.State>, Map<Long, ? extends Channel>, Map<Long, ? extends StoreThreadMessages.ThreadState>, MessagesWithMetadata>() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModelMessages$MessagesWithMetadata$Companion$get$1
                    @Override // rx.functions.Func3
                    public /* bridge */ /* synthetic */ WidgetChatListModelMessages.MessagesWithMetadata call(Map<Long, ? extends StoreMessageState.State> map, Map<Long, ? extends Channel> map2, Map<Long, ? extends StoreThreadMessages.ThreadState> map3) {
                        return call2((Map<Long, StoreMessageState.State>) map, (Map<Long, Channel>) map2, (Map<Long, StoreThreadMessages.ThreadState>) map3);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final WidgetChatListModelMessages.MessagesWithMetadata call2(Map<Long, StoreMessageState.State> map, Map<Long, Channel> map2, Map<Long, StoreThreadMessages.ThreadState> map3) {
                        List list2 = list;
                        m.checkNotNullExpressionValue(map, "messageState");
                        m.checkNotNullExpressionValue(map2, "messageThreads");
                        m.checkNotNullExpressionValue(map3, "threadCountsAndLatestMessages");
                        return new WidgetChatListModelMessages.MessagesWithMetadata(list2, map, map2, map3, h0.emptyMap(), h0.emptyMap());
                    }
                });
                m.checkNotNullExpressionValue(i, "Observable\n            .…          )\n            }");
                return i;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final Observable<MessagesWithMetadata> get(final Channel channel) {
                m.checkNotNullParameter(channel, "channel");
                Observable Y = StoreStream.Companion.getMessages().observeMessagesForChannel(channel.h()).Y(new b<List<? extends Message>, Observable<? extends MessagesWithMetadata>>() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModelMessages$MessagesWithMetadata$Companion$get$2

                    /* compiled from: WidgetChatListModelMessages.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0013\u001a\u00020\u00102\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u00002\u0016\u0010\u0007\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u00032\u0016\u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00032\u0016\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\b\u0012\u0004\u0012\u00020\u000b0\u00032\u0016\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\r0\u00032\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\r0\u0003¢\u0006\u0004\b\u0011\u0010\u0012"}, d2 = {"", "Lcom/discord/models/message/Message;", "p1", "", "", "Lcom/discord/primitives/MessageId;", "Lcom/discord/stores/StoreMessageState$State;", "p2", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "p3", "Lcom/discord/stores/StoreThreadMessages$ThreadState;", "p4", "Lcom/discord/stores/StoreMessageReplies$MessageState;", "p5", "p6", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;", "invoke", "(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.chat.list.model.WidgetChatListModelMessages$MessagesWithMetadata$Companion$get$2$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final /* synthetic */ class AnonymousClass1 extends d0.z.d.k implements Function6<List<? extends Message>, Map<Long, ? extends StoreMessageState.State>, Map<Long, ? extends Channel>, Map<Long, ? extends StoreThreadMessages.ThreadState>, Map<Long, ? extends StoreMessageReplies.MessageState>, Map<Long, ? extends StoreMessageReplies.MessageState>, WidgetChatListModelMessages.MessagesWithMetadata> {
                        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                        public AnonymousClass1() {
                            super(6, WidgetChatListModelMessages.MessagesWithMetadata.class, HookHelper.constructorName, "<init>(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", 0);
                        }

                        @Override // kotlin.jvm.functions.Function6
                        public /* bridge */ /* synthetic */ WidgetChatListModelMessages.MessagesWithMetadata invoke(List<? extends Message> list, Map<Long, ? extends StoreMessageState.State> map, Map<Long, ? extends Channel> map2, Map<Long, ? extends StoreThreadMessages.ThreadState> map3, Map<Long, ? extends StoreMessageReplies.MessageState> map4, Map<Long, ? extends StoreMessageReplies.MessageState> map5) {
                            return invoke2((List<Message>) list, (Map<Long, StoreMessageState.State>) map, (Map<Long, Channel>) map2, (Map<Long, StoreThreadMessages.ThreadState>) map3, map4, map5);
                        }

                        /* renamed from: invoke  reason: avoid collision after fix types in other method */
                        public final WidgetChatListModelMessages.MessagesWithMetadata invoke2(List<Message> list, Map<Long, StoreMessageState.State> map, Map<Long, Channel> map2, Map<Long, StoreThreadMessages.ThreadState> map3, Map<Long, ? extends StoreMessageReplies.MessageState> map4, Map<Long, ? extends StoreMessageReplies.MessageState> map5) {
                            m.checkNotNullParameter(list, "p1");
                            m.checkNotNullParameter(map, "p2");
                            m.checkNotNullParameter(map2, "p3");
                            m.checkNotNullParameter(map3, "p4");
                            m.checkNotNullParameter(map4, "p5");
                            m.checkNotNullParameter(map5, "p6");
                            return new WidgetChatListModelMessages.MessagesWithMetadata(list, map, map2, map3, map4, map5);
                        }
                    }

                    /* JADX WARN: Multi-variable type inference failed */
                    /* JADX WARN: Type inference failed for: r1v5, types: [com.discord.widgets.chat.list.model.WidgetChatListModelMessages$sam$rx_functions_Func6$0] */
                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final Observable<? extends WidgetChatListModelMessages.MessagesWithMetadata> call2(List<Message> list) {
                        k kVar = new k(list);
                        StoreStream.Companion companion = StoreStream.Companion;
                        Observable<Map<Long, StoreMessageState.State>> messageState = companion.getMessageState().getMessageState();
                        StoreChannels channels = companion.getChannels();
                        m.checkNotNullExpressionValue(list, "messages");
                        Observable<Map<Long, Channel>> observeThreadsFromMessages = channels.observeThreadsFromMessages(list);
                        Observable<Map<Long, StoreThreadMessages.ThreadState>> observeThreadCountAndLatestMessage = companion.getThreadMessages().observeThreadCountAndLatestMessage();
                        Observable<Map<Long, StoreMessageReplies.MessageState>> observeMessageReferencesForChannel = companion.getRepliedMessages().observeMessageReferencesForChannel(Channel.this.h());
                        Observable<Map<Long, StoreMessageReplies.MessageState>> observeMessageReferencesForChannel2 = companion.getRepliedMessages().observeMessageReferencesForChannel(Channel.this.r());
                        final AnonymousClass1 r1 = AnonymousClass1.INSTANCE;
                        AnonymousClass1 r12 = r1;
                        if (r1 != null) {
                            r12 = new Func6() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModelMessages$sam$rx_functions_Func6$0
                                @Override // rx.functions.Func6
                                public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6) {
                                    return Function6.this.invoke(obj, obj2, obj3, obj4, obj5, obj6);
                                }
                            };
                        }
                        return Observable.f(kVar, messageState, observeThreadsFromMessages, observeThreadCountAndLatestMessage, observeMessageReferencesForChannel, observeMessageReferencesForChannel2, (Func6) r12);
                    }

                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Observable<? extends WidgetChatListModelMessages.MessagesWithMetadata> call(List<? extends Message> list) {
                        return call2((List<Message>) list);
                    }
                });
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …        )\n              }");
                return Y;
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public MessagesWithMetadata(List<Message> list, Map<Long, StoreMessageState.State> map, Map<Long, Channel> map2, Map<Long, StoreThreadMessages.ThreadState> map3, Map<Long, ? extends StoreMessageReplies.MessageState> map4, Map<Long, ? extends StoreMessageReplies.MessageState> map5) {
            m.checkNotNullParameter(list, "messages");
            m.checkNotNullParameter(map, "messageState");
            m.checkNotNullParameter(map2, "messageThreads");
            m.checkNotNullParameter(map3, "threadCountsAndLatestMessages");
            m.checkNotNullParameter(map4, "messageReplyState");
            m.checkNotNullParameter(map5, "parentChannelMessageReplyState");
            this.messages = list;
            this.messageState = map;
            this.messageThreads = map2;
            this.threadCountsAndLatestMessages = map3;
            this.messageReplyState = map4;
            this.parentChannelMessageReplyState = map5;
        }

        public static /* synthetic */ MessagesWithMetadata copy$default(MessagesWithMetadata messagesWithMetadata, List list, Map map, Map map2, Map map3, Map map4, Map map5, int i, Object obj) {
            List<Message> list2 = list;
            if ((i & 1) != 0) {
                list2 = messagesWithMetadata.messages;
            }
            Map<Long, StoreMessageState.State> map6 = map;
            if ((i & 2) != 0) {
                map6 = messagesWithMetadata.messageState;
            }
            Map map7 = map6;
            Map<Long, Channel> map8 = map2;
            if ((i & 4) != 0) {
                map8 = messagesWithMetadata.messageThreads;
            }
            Map map9 = map8;
            Map<Long, StoreThreadMessages.ThreadState> map10 = map3;
            if ((i & 8) != 0) {
                map10 = messagesWithMetadata.threadCountsAndLatestMessages;
            }
            Map map11 = map10;
            Map<Long, StoreMessageReplies.MessageState> map12 = map4;
            if ((i & 16) != 0) {
                map12 = messagesWithMetadata.messageReplyState;
            }
            Map map13 = map12;
            Map<Long, StoreMessageReplies.MessageState> map14 = map5;
            if ((i & 32) != 0) {
                map14 = messagesWithMetadata.parentChannelMessageReplyState;
            }
            return messagesWithMetadata.copy(list2, map7, map9, map11, map13, map14);
        }

        public final List<Message> component1() {
            return this.messages;
        }

        public final Map<Long, StoreMessageState.State> component2() {
            return this.messageState;
        }

        public final Map<Long, Channel> component3() {
            return this.messageThreads;
        }

        public final Map<Long, StoreThreadMessages.ThreadState> component4() {
            return this.threadCountsAndLatestMessages;
        }

        public final Map<Long, StoreMessageReplies.MessageState> component5() {
            return this.messageReplyState;
        }

        public final Map<Long, StoreMessageReplies.MessageState> component6() {
            return this.parentChannelMessageReplyState;
        }

        public final MessagesWithMetadata copy(List<Message> list, Map<Long, StoreMessageState.State> map, Map<Long, Channel> map2, Map<Long, StoreThreadMessages.ThreadState> map3, Map<Long, ? extends StoreMessageReplies.MessageState> map4, Map<Long, ? extends StoreMessageReplies.MessageState> map5) {
            m.checkNotNullParameter(list, "messages");
            m.checkNotNullParameter(map, "messageState");
            m.checkNotNullParameter(map2, "messageThreads");
            m.checkNotNullParameter(map3, "threadCountsAndLatestMessages");
            m.checkNotNullParameter(map4, "messageReplyState");
            m.checkNotNullParameter(map5, "parentChannelMessageReplyState");
            return new MessagesWithMetadata(list, map, map2, map3, map4, map5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MessagesWithMetadata)) {
                return false;
            }
            MessagesWithMetadata messagesWithMetadata = (MessagesWithMetadata) obj;
            return m.areEqual(this.messages, messagesWithMetadata.messages) && m.areEqual(this.messageState, messagesWithMetadata.messageState) && m.areEqual(this.messageThreads, messagesWithMetadata.messageThreads) && m.areEqual(this.threadCountsAndLatestMessages, messagesWithMetadata.threadCountsAndLatestMessages) && m.areEqual(this.messageReplyState, messagesWithMetadata.messageReplyState) && m.areEqual(this.parentChannelMessageReplyState, messagesWithMetadata.parentChannelMessageReplyState);
        }

        public final Map<Long, StoreMessageReplies.MessageState> getMessageReplyState() {
            return this.messageReplyState;
        }

        public final Map<Long, StoreMessageState.State> getMessageState() {
            return this.messageState;
        }

        public final Map<Long, Channel> getMessageThreads() {
            return this.messageThreads;
        }

        public final List<Message> getMessages() {
            return this.messages;
        }

        public final Map<Long, StoreMessageReplies.MessageState> getParentChannelMessageReplyState() {
            return this.parentChannelMessageReplyState;
        }

        public final Map<Long, StoreThreadMessages.ThreadState> getThreadCountsAndLatestMessages() {
            return this.threadCountsAndLatestMessages;
        }

        public int hashCode() {
            List<Message> list = this.messages;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            Map<Long, StoreMessageState.State> map = this.messageState;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Channel> map2 = this.messageThreads;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, StoreThreadMessages.ThreadState> map3 = this.threadCountsAndLatestMessages;
            int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Map<Long, StoreMessageReplies.MessageState> map4 = this.messageReplyState;
            int hashCode5 = (hashCode4 + (map4 != null ? map4.hashCode() : 0)) * 31;
            Map<Long, StoreMessageReplies.MessageState> map5 = this.parentChannelMessageReplyState;
            if (map5 != null) {
                i = map5.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("MessagesWithMetadata(messages=");
            R.append(this.messages);
            R.append(", messageState=");
            R.append(this.messageState);
            R.append(", messageThreads=");
            R.append(this.messageThreads);
            R.append(", threadCountsAndLatestMessages=");
            R.append(this.threadCountsAndLatestMessages);
            R.append(", messageReplyState=");
            R.append(this.messageReplyState);
            R.append(", parentChannelMessageReplyState=");
            return a.L(R, this.parentChannelMessageReplyState, ")");
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public WidgetChatListModelMessages(List<? extends ChatListEntry> list, long j, long j2, Map<Long, GuildMember> map, long j3, Long l) {
        m.checkNotNullParameter(list, "items");
        this.items = list;
        this.oldestMessageId = j;
        this.newestKnownMessageId = j2;
        this.guildMembers = map;
        this.newMessagesMarkerMessageId = j3;
        this.newestSentByUserMessageId = l;
    }

    public final List<ChatListEntry> component1() {
        return this.items;
    }

    public final long component2() {
        return this.oldestMessageId;
    }

    public final long component3() {
        return this.newestKnownMessageId;
    }

    public final Map<Long, GuildMember> component4() {
        return this.guildMembers;
    }

    public final long component5() {
        return this.newMessagesMarkerMessageId;
    }

    public final Long component6() {
        return this.newestSentByUserMessageId;
    }

    public final WidgetChatListModelMessages copy(List<? extends ChatListEntry> list, long j, long j2, Map<Long, GuildMember> map, long j3, Long l) {
        m.checkNotNullParameter(list, "items");
        return new WidgetChatListModelMessages(list, j, j2, map, j3, l);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetChatListModelMessages)) {
            return false;
        }
        WidgetChatListModelMessages widgetChatListModelMessages = (WidgetChatListModelMessages) obj;
        return m.areEqual(this.items, widgetChatListModelMessages.items) && this.oldestMessageId == widgetChatListModelMessages.oldestMessageId && this.newestKnownMessageId == widgetChatListModelMessages.newestKnownMessageId && m.areEqual(this.guildMembers, widgetChatListModelMessages.guildMembers) && this.newMessagesMarkerMessageId == widgetChatListModelMessages.newMessagesMarkerMessageId && m.areEqual(this.newestSentByUserMessageId, widgetChatListModelMessages.newestSentByUserMessageId);
    }

    public final Map<Long, GuildMember> getGuildMembers() {
        return this.guildMembers;
    }

    public final List<ChatListEntry> getItems() {
        return this.items;
    }

    public final long getNewMessagesMarkerMessageId() {
        return this.newMessagesMarkerMessageId;
    }

    public final long getNewestKnownMessageId() {
        return this.newestKnownMessageId;
    }

    public final Long getNewestSentByUserMessageId() {
        return this.newestSentByUserMessageId;
    }

    public final long getOldestMessageId() {
        return this.oldestMessageId;
    }

    public int hashCode() {
        List<ChatListEntry> list = this.items;
        int i = 0;
        int hashCode = list != null ? list.hashCode() : 0;
        int a = (a0.a.a.b.a(this.newestKnownMessageId) + ((a0.a.a.b.a(this.oldestMessageId) + (hashCode * 31)) * 31)) * 31;
        Map<Long, GuildMember> map = this.guildMembers;
        int a2 = (a0.a.a.b.a(this.newMessagesMarkerMessageId) + ((a + (map != null ? map.hashCode() : 0)) * 31)) * 31;
        Long l = this.newestSentByUserMessageId;
        if (l != null) {
            i = l.hashCode();
        }
        return a2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetChatListModelMessages(items=");
        R.append(this.items);
        R.append(", oldestMessageId=");
        R.append(this.oldestMessageId);
        R.append(", newestKnownMessageId=");
        R.append(this.newestKnownMessageId);
        R.append(", guildMembers=");
        R.append(this.guildMembers);
        R.append(", newMessagesMarkerMessageId=");
        R.append(this.newMessagesMarkerMessageId);
        R.append(", newestSentByUserMessageId=");
        return a.F(R, this.newestSentByUserMessageId, ")");
    }
}
