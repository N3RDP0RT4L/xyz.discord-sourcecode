package com.discord.widgets.chat.list.model;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.permission.Permission;
import com.discord.api.thread.ThreadMetadata;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreMessagesLoader;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.GuildWelcomeEntry;
import com.discord.widgets.chat.list.entries.LoadingEntry;
import com.discord.widgets.chat.list.entries.SpacerEntry;
import com.discord.widgets.chat.list.entries.StartOfChatEntry;
import com.discord.widgets.chat.list.entries.StartOfPrivateChatEntry;
import com.discord.widgets.user.UserMutualGuildsManager;
import d0.t.m;
import d0.t.n;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func7;
/* compiled from: WidgetChatListModelTop.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "component1", "()Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "item", "copy", "(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "getItem", HookHelper.constructorName, "(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListModelTop {
    public static final Companion Companion = new Companion(null);
    private final ChatListEntry item;

    /* compiled from: WidgetChatListModelTop.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J5\u0010\n\u001a\u0010\u0012\f\u0012\n\u0018\u00010\bj\u0004\u0018\u0001`\t0\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u001d\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00072\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u001d\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00072\u0006\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0004\b\u0012\u0010\u0010¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/UserId;", "userId", "Lrx/Observable;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "observeGuildMember", "(JJ)Lrx/Observable;", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "getWelcomeEntry", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;", "get", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<ChatListEntry> getWelcomeEntry(final Channel channel) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<Guild> observeGuild = companion.getGuilds().observeGuild(channel.f());
            Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
            Observable<Long> observePermissionsForChannel = companion.getPermissions().observePermissionsForChannel(channel.h());
            Observable q = companion.getChannels().observeDefaultChannel(channel.f()).F(WidgetChatListModelTop$Companion$getWelcomeEntry$1.INSTANCE).q();
            Observable<GuildMember> observeGuildMember = observeGuildMember(channel.f(), channel.q());
            Observable<User> observeUser = companion.getUsers().observeUser(channel.q());
            UserMutualGuildsManager userMutualGuildsManager = new UserMutualGuildsManager(null, null, null, 7, null);
            User a = ChannelUtils.a(channel);
            Observable<ChatListEntry> e = Observable.e(observeGuild, observeMe$default, observePermissionsForChannel, q, observeGuildMember, observeUser, userMutualGuildsManager.observeMutualGuilds(m.listOf(Long.valueOf(a != null ? a.getId() : 0L))), new Func7<Guild, MeUser, Long, Long, GuildMember, User, Map<Long, ? extends List<? extends Guild>>, ChatListEntry>() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModelTop$Companion$getWelcomeEntry$2
                @Override // rx.functions.Func7
                public /* bridge */ /* synthetic */ ChatListEntry call(Guild guild, MeUser meUser, Long l, Long l2, GuildMember guildMember, User user, Map<Long, ? extends List<? extends Guild>> map) {
                    return call2(guild, meUser, l, l2, guildMember, user, (Map<Long, ? extends List<Guild>>) map);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final ChatListEntry call2(Guild guild, MeUser meUser, Long l, Long l2, GuildMember guildMember, User user, Map<Long, ? extends List<Guild>> map) {
                    ChatListEntry startOfChatEntry;
                    boolean z2 = true;
                    boolean z3 = false;
                    boolean z4 = l2 != null && Channel.this.h() == l2.longValue();
                    if (guild == null || !guild.hasIcon()) {
                        z2 = false;
                    }
                    boolean can = PermissionUtils.can(Permission.READ_MESSAGE_HISTORY, l);
                    boolean can2 = PermissionUtils.can(1L, l);
                    boolean can3 = PermissionUtils.can(16L, l);
                    ThreadUtils threadUtils = ThreadUtils.INSTANCE;
                    d0.z.d.m.checkNotNullExpressionValue(meUser, "me");
                    boolean canManageThread = threadUtils.canManageThread(meUser, Channel.this, l);
                    User a2 = ChannelUtils.a(Channel.this);
                    List<Guild> list = map.get(Long.valueOf(a2 != null ? a2.getId() : 0L));
                    if (list == null) {
                        list = n.emptyList();
                    }
                    List<Guild> list2 = list;
                    if (guild != null) {
                        z3 = guild.isOwner(meUser.getId());
                    }
                    if (!ChannelUtils.s(Channel.this) || guild == null || !z4 || !can || (!can2 && z2)) {
                        Integer num = null;
                        if (ChannelUtils.x(Channel.this)) {
                            startOfChatEntry = new StartOfPrivateChatEntry(Channel.this.h(), ChannelUtils.c(Channel.this), Channel.this.A(), IconUtils.getForChannel$default(Channel.this, null, 2, null), ChannelUtils.A(Channel.this), list2);
                        } else {
                            long h = Channel.this.h();
                            String c = ChannelUtils.c(Channel.this);
                            boolean E = ChannelUtils.E(Channel.this);
                            boolean C = ChannelUtils.C(Channel.this);
                            ThreadMetadata y2 = Channel.this.y();
                            if (y2 != null) {
                                num = Integer.valueOf(y2.c());
                            }
                            startOfChatEntry = new StartOfChatEntry(h, c, can, can3, canManageThread, C, num, guildMember, user != null ? GuildMember.Companion.getNickOrUsername(guildMember, user) : "", E);
                        }
                        return startOfChatEntry;
                    }
                    long id2 = guild.getId();
                    String name = guild.getName();
                    if (name == null) {
                        name = "";
                    }
                    return new GuildWelcomeEntry(z3, z2, can2, id2, name);
                }
            });
            d0.z.d.m.checkNotNullExpressionValue(e, "Observable.combineLatest…      )\n        }\n      }");
            return e;
        }

        private final Observable<GuildMember> observeGuildMember(long j, long j2) {
            Observable<GuildMember> observeGuildMember = StoreStream.Companion.getGuilds().observeGuildMember(j, j2);
            d0.z.d.m.checkNotNullExpressionValue(observeGuildMember, "StoreStream.getGuilds().…ldMember(guildId, userId)");
            return ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.leadingEdgeThrottle(observeGuildMember, 1L, TimeUnit.SECONDS));
        }

        public final Observable<WidgetChatListModelTop> get(final Channel channel) {
            d0.z.d.m.checkNotNullParameter(channel, "channel");
            Observable<WidgetChatListModelTop> q = StoreStream.Companion.getMessagesLoader().getMessagesLoadedState(channel.h()).Y(new b<StoreMessagesLoader.ChannelLoadedState, Observable<? extends WidgetChatListModelTop>>() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModelTop$Companion$get$1

                /* compiled from: WidgetChatListModelTop.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/chat/list/entries/ChatListEntry;)Lcom/discord/widgets/chat/list/model/WidgetChatListModelTop;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.list.model.WidgetChatListModelTop$Companion$get$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1<T, R> implements b<ChatListEntry, WidgetChatListModelTop> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    public final WidgetChatListModelTop call(ChatListEntry chatListEntry) {
                        d0.z.d.m.checkNotNullExpressionValue(chatListEntry, "it");
                        return new WidgetChatListModelTop(chatListEntry);
                    }
                }

                public final Observable<? extends WidgetChatListModelTop> call(StoreMessagesLoader.ChannelLoadedState channelLoadedState) {
                    Observable observable;
                    k kVar;
                    if (channelLoadedState.isOldestMessagesLoaded()) {
                        observable = WidgetChatListModelTop.Companion.getWelcomeEntry(Channel.this);
                    } else {
                        if (channelLoadedState.isTouchedSinceLastJump() || !channelLoadedState.isInitialMessagesLoaded()) {
                            kVar = new k(new LoadingEntry());
                        } else {
                            kVar = new k(new SpacerEntry(Channel.this.h()));
                        }
                        observable = kVar;
                    }
                    return observable.F(AnonymousClass1.INSTANCE);
                }
            }).q();
            d0.z.d.m.checkNotNullExpressionValue(q, "StoreStream\n          .g…  .distinctUntilChanged()");
            return q;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetChatListModelTop(ChatListEntry chatListEntry) {
        d0.z.d.m.checkNotNullParameter(chatListEntry, "item");
        this.item = chatListEntry;
    }

    public static /* synthetic */ WidgetChatListModelTop copy$default(WidgetChatListModelTop widgetChatListModelTop, ChatListEntry chatListEntry, int i, Object obj) {
        if ((i & 1) != 0) {
            chatListEntry = widgetChatListModelTop.item;
        }
        return widgetChatListModelTop.copy(chatListEntry);
    }

    public static final Observable<WidgetChatListModelTop> get(Channel channel) {
        return Companion.get(channel);
    }

    public final ChatListEntry component1() {
        return this.item;
    }

    public final WidgetChatListModelTop copy(ChatListEntry chatListEntry) {
        d0.z.d.m.checkNotNullParameter(chatListEntry, "item");
        return new WidgetChatListModelTop(chatListEntry);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof WidgetChatListModelTop) && d0.z.d.m.areEqual(this.item, ((WidgetChatListModelTop) obj).item);
        }
        return true;
    }

    public final ChatListEntry getItem() {
        return this.item;
    }

    public int hashCode() {
        ChatListEntry chatListEntry = this.item;
        if (chatListEntry != null) {
            return chatListEntry.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetChatListModelTop(item=");
        R.append(this.item);
        R.append(")");
        return R.toString();
    }
}
