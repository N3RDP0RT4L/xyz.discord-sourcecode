package com.discord.widgets.chat.list.model;

import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.api.user.User;
import com.discord.api.utcdatetime.UtcDateTime;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.utilities.embed.InviteEmbedModel;
import com.discord.widgets.botuikit.ComponentChatListState;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.model.WidgetChatListModelMessages;
import d0.t.n;
import d0.t.t;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function14;
/* compiled from: WidgetChatListModelMessages.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010#\u001a\u00020 2\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u001a\u0010\t\u001a\u0016\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\b\u0012\u00060\u0007j\u0002`\b0\u00042\u0010\u0010\f\u001a\f\u0012\b\u0012\u00060\u0005j\u0002`\u000b0\n26\u0010\u0010\u001a2\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\b\u0012\u00060\rj\u0002`\u000e \u000f*\u0018\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\b\u0012\u00060\rj\u0002`\u000e\u0018\u00010\u00040\u00042\u0018\u0010\u0011\u001a\u0014 \u000f*\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u000b0\u0005j\u0002`\u000b2\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u00042\u000e\u0010\u0016\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00152\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u00172\n\u0010\u001b\u001a\u00060\u0005j\u0002`\u00062\u0016\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u000b\u0012\u0004\u0012\u00020\u001c0\u00042\u0006\u0010\u001f\u001a\u00020\u001eH\n¢\u0006\u0004\b!\u0010\""}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;", "messagesWithMetadata", "Lcom/discord/api/channel/Channel;", "parentChannel", "", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "blockedRelationships", "", "Lcom/discord/primitives/MessageId;", "blockedExpanded", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "kotlin.jvm.PlatformType", "guildMembers", "newMessagesMarkerMessageId", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "Lcom/discord/api/permission/PermissionBit;", "permissionsForChannel", "", "animateEmojis", "autoPlayGifs", "renderEmbeds", "meUserId", "Lcom/discord/widgets/botuikit/ComponentChatListState$ComponentStoreState;", "componentStoreState", "Lcom/discord/utilities/embed/InviteEmbedModel;", "inviteEmbedModel", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages;", "invoke", "(Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages$MessagesWithMetadata;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/lang/Long;Ljava/util/Map;Ljava/lang/Long;ZZZJLjava/util/Map;Lcom/discord/utilities/embed/InviteEmbedModel;)Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListModelMessages$Companion$get$1 extends o implements Function14<WidgetChatListModelMessages.MessagesWithMetadata, Channel, Map<Long, ? extends Integer>, List<? extends Long>, Map<Long, ? extends GuildMember>, Long, Map<Long, ? extends GuildRole>, Long, Boolean, Boolean, Boolean, Long, Map<Long, ? extends ComponentChatListState.ComponentStoreState>, InviteEmbedModel, WidgetChatListModelMessages> {
    public final /* synthetic */ Channel $channel;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatListModelMessages$Companion$get$1(Channel channel) {
        super(14);
        this.$channel = channel;
    }

    @Override // kotlin.jvm.functions.Function14
    public /* bridge */ /* synthetic */ WidgetChatListModelMessages invoke(WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata, Channel channel, Map<Long, ? extends Integer> map, List<? extends Long> list, Map<Long, ? extends GuildMember> map2, Long l, Map<Long, ? extends GuildRole> map3, Long l2, Boolean bool, Boolean bool2, Boolean bool3, Long l3, Map<Long, ? extends ComponentChatListState.ComponentStoreState> map4, InviteEmbedModel inviteEmbedModel) {
        return invoke(messagesWithMetadata, channel, (Map<Long, Integer>) map, (List<Long>) list, (Map<Long, GuildMember>) map2, l, (Map<Long, GuildRole>) map3, l2, bool.booleanValue(), bool2.booleanValue(), bool3.booleanValue(), l3.longValue(), (Map<Long, ComponentChatListState.ComponentStoreState>) map4, inviteEmbedModel);
    }

    public final WidgetChatListModelMessages invoke(WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata, Channel channel, Map<Long, Integer> map, List<Long> list, Map<Long, GuildMember> map2, Long l, Map<Long, GuildRole> map3, Long l2, boolean z2, boolean z3, boolean z4, long j, Map<Long, ComponentChatListState.ComponentStoreState> map4, InviteEmbedModel inviteEmbedModel) {
        Object obj;
        boolean z5;
        boolean willAddTimestamp;
        long tryAddTimestamp;
        boolean z6;
        Message message;
        boolean z7;
        int i;
        String str;
        Message message2;
        Message message3;
        WidgetChatListModelMessages.Companion companion;
        WidgetChatListModelMessages.Items items;
        WidgetChatListModelMessages.Items items2;
        boolean shouldConcatMessage;
        int addBlockedMessage;
        Pair threadStarterMessageAndChannel;
        WidgetChatListModelMessages$Companion$get$1 widgetChatListModelMessages$Companion$get$1 = this;
        WidgetChatListModelMessages.MessagesWithMetadata messagesWithMetadata2 = messagesWithMetadata;
        Map<Long, Integer> map5 = map;
        m.checkNotNullParameter(messagesWithMetadata2, "messagesWithMetadata");
        m.checkNotNullParameter(map5, "blockedRelationships");
        m.checkNotNullParameter(list, "blockedExpanded");
        m.checkNotNullParameter(map3, "guildRoles");
        m.checkNotNullParameter(map4, "componentStoreState");
        m.checkNotNullParameter(inviteEmbedModel, "inviteEmbedModel");
        WidgetChatListModelMessages.Items items3 = new WidgetChatListModelMessages.Items(messagesWithMetadata.getMessages().size());
        long j2 = 0;
        Message message4 = null;
        Message message5 = null;
        int i2 = 0;
        int i3 = 0;
        boolean z8 = false;
        boolean z9 = false;
        for (Object obj2 : messagesWithMetadata.getMessages()) {
            i2++;
            if (i2 < 0) {
                n.throwIndexOverflow();
            }
            Message message6 = (Message) obj2;
            WidgetChatListModelMessages.Companion companion2 = WidgetChatListModelMessages.Companion;
            UtcDateTime timestamp = message6.getTimestamp();
            willAddTimestamp = companion2.willAddTimestamp(timestamp != null ? timestamp.g() : 0L, j2);
            if (willAddTimestamp) {
                i3 = companion2.addBlockedMessage(items3, message4, i3, z8);
            }
            long id2 = message6.getId();
            UtcDateTime timestamp2 = message6.getTimestamp();
            tryAddTimestamp = companion2.tryAddTimestamp(items3, id2, timestamp2 != null ? timestamp2.g() : 0L, j2);
            boolean z10 = i2 == messagesWithMetadata.getMessages().size() - 1;
            Integer type = message6.getType();
            if (type != null && type.intValue() == 21) {
                threadStarterMessageAndChannel = companion2.getThreadStarterMessageAndChannel(channel, widgetChatListModelMessages$Companion$get$1.$channel, message6, messagesWithMetadata2);
                User author = ((Message) threadStarterMessageAndChannel.getFirst()).getAuthor();
                z6 = map5.containsKey(author != null ? Long.valueOf(author.i()) : null);
            } else {
                User author2 = message6.getAuthor();
                z6 = map5.containsKey(author2 != null ? Long.valueOf(author2.i()) : null);
            }
            if (!z6 || (i3 = i3 + 1) != 1) {
                z7 = z8;
                message = message4;
            } else {
                z7 = list.contains(Long.valueOf(message6.getId()));
                message = message6;
            }
            if (!z6 || z10) {
                addBlockedMessage = companion2.addBlockedMessage(items3, message, i3, z7);
                i = addBlockedMessage;
            } else {
                i = i3;
            }
            boolean z11 = (z6 || !z7) ? z7 : false;
            if (!z6 || z11) {
                shouldConcatMessage = companion2.shouldConcatMessage(items3, message6, message5);
                items3.setConcatCount(shouldConcatMessage ? items3.getConcatCount() + 1 : 0);
                Integer type2 = message6.getType();
                if (type2 != null && type2.intValue() == 21) {
                    Channel channel2 = widgetChatListModelMessages$Companion$get$1.$channel;
                    m.checkNotNullExpressionValue(map2, "guildMembers");
                    message3 = message;
                    WidgetChatListModelMessages.Items items4 = items3;
                    message2 = message6;
                    str = "newMessagesMarkerMessageId";
                    companion = companion2;
                    items4.addItems(companion2.getThreadStarterMessageItems(channel, channel2, map2, map3, map, message6, messagesWithMetadata, z2, z3, z4, j, true, map4, inviteEmbedModel));
                    items = items4;
                } else {
                    message3 = message;
                    items = items3;
                    message2 = message6;
                    str = "newMessagesMarkerMessageId";
                    companion = companion2;
                    Channel channel3 = widgetChatListModelMessages$Companion$get$1.$channel;
                    m.checkNotNullExpressionValue(map2, "guildMembers");
                    items.addItems(WidgetChatListModelMessages.Companion.getMessageItems$default(companion, channel3, map2, map3, map, messagesWithMetadata.getMessageThreads().get(Long.valueOf(message2.getId())), messagesWithMetadata.getThreadCountsAndLatestMessages().get(Long.valueOf(message2.getId())), message2, messagesWithMetadata.getMessageState().get(Long.valueOf(message2.getId())), messagesWithMetadata.getMessageReplyState(), z11, shouldConcatMessage, l2, z2, z3, z4, j, true, map4, inviteEmbedModel, false, 524288, null));
                }
            } else {
                message3 = message;
                items = items3;
                message2 = message6;
                str = "newMessagesMarkerMessageId";
                companion = companion2;
            }
            if (!z9) {
                m.checkNotNullExpressionValue(l, str);
                items2 = items;
                widgetChatListModelMessages$Companion$get$1 = this;
                z9 = companion.tryAddNewMessagesSeparator(items2, l.longValue(), z10, message2.getId(), widgetChatListModelMessages$Companion$get$1.$channel);
            } else {
                items2 = items;
                widgetChatListModelMessages$Companion$get$1 = this;
            }
            messagesWithMetadata2 = messagesWithMetadata;
            map5 = map;
            items3 = items2;
            j2 = tryAddTimestamp;
            i3 = i;
            z8 = z11;
            message4 = message3;
            message5 = message2;
        }
        WidgetChatListModelMessages.Items items5 = items3;
        t.reverse(items5.getItems());
        List<ChatListEntry> items6 = items5.getItems();
        Message message7 = (Message) u.firstOrNull((List<? extends Object>) messagesWithMetadata.getMessages());
        long id3 = message7 != null ? message7.getId() : 0L;
        Message message8 = (Message) u.lastOrNull((List<? extends Object>) messagesWithMetadata.getMessages());
        long id4 = message8 != null ? message8.getId() : 0L;
        m.checkNotNullExpressionValue(l, "newMessagesMarkerMessageId");
        long longValue = l.longValue();
        Iterator<T> it = messagesWithMetadata.getMessages().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            User author3 = ((Message) obj).getAuthor();
            if (author3 == null || author3.i() != j) {
                z5 = false;
                continue;
            } else {
                z5 = true;
                continue;
            }
            if (z5) {
                break;
            }
        }
        Message message9 = (Message) obj;
        return new WidgetChatListModelMessages(items6, id3, id4, map2, longValue, message9 != null ? Long.valueOf(message9.getId()) : null);
    }
}
