package com.discord.widgets.chat.list.model;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.sticker.Sticker;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.User;
import com.discord.stores.StoreMessagesLoader;
import com.discord.stores.StoreStickers;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreThreadDraft;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.chat.list.adapter.WidgetChatListAdapter;
import com.discord.widgets.chat.list.entries.ChatListEntry;
import com.discord.widgets.chat.list.entries.LoadingEntry;
import com.discord.widgets.chat.list.entries.SpacerEntry;
import com.discord.widgets.chat.list.entries.StickerGreetCompactEntry;
import com.discord.widgets.chat.list.entries.StickerGreetEntry;
import com.discord.widgets.chat.list.entries.ThreadDraftFormEntry;
import com.discord.widgets.chat.list.model.WidgetChatListModel;
import d0.t.n0;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
import rx.functions.Func7;
import rx.functions.Func9;
/* compiled from: WidgetChatListModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0019\b\u0086\b\u0018\u0000 I2\u00020\u0001:\u0002JIB\u009f\u0001\u0012\n\u0010 \u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010!\u001a\u00060\u0002j\u0002`\u0006\u0012\b\u0010\"\u001a\u0004\u0018\u00010\b\u0012\n\u0010#\u001a\u00060\u0002j\u0002`\u000b\u0012\u0016\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r\u0012\f\b\u0002\u0010%\u001a\u00060\u0002j\u0002`\u0011\u0012\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013\u0012\u0010\u0010'\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00180\u0017\u0012\f\b\u0002\u0010(\u001a\u00060\u0002j\u0002`\u0011\u0012\f\b\u0002\u0010)\u001a\u00060\u0002j\u0002`\u0011\u0012\u0006\u0010*\u001a\u00020\u001d¢\u0006\u0004\bG\u0010HJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\f\u001a\u00060\u0002j\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\u0005J \u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\rHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0014\u0010\u0012\u001a\u00060\u0002j\u0002`\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0005J\u0016\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0019\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00180\u0017HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0014\u0010\u001b\u001a\u00060\u0002j\u0002`\u0011HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0005J\u0014\u0010\u001c\u001a\u00060\u0002j\u0002`\u0011HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0005J\u0010\u0010\u001e\u001a\u00020\u001dHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ¸\u0001\u0010+\u001a\u00020\u00002\f\b\u0002\u0010 \u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010!\u001a\u00060\u0002j\u0002`\u00062\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\b2\f\b\u0002\u0010#\u001a\u00060\u0002j\u0002`\u000b2\u0018\b\u0002\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r2\f\b\u0002\u0010%\u001a\u00060\u0002j\u0002`\u00112\u000e\b\u0002\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\u0012\b\u0002\u0010'\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00180\u00172\f\b\u0002\u0010(\u001a\u00060\u0002j\u0002`\u00112\f\b\u0002\u0010)\u001a\u00060\u0002j\u0002`\u00112\b\b\u0002\u0010*\u001a\u00020\u001dHÆ\u0001¢\u0006\u0004\b+\u0010,J\u0010\u0010-\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u00100\u001a\u00020/HÖ\u0001¢\u0006\u0004\b0\u00101J\u001a\u00104\u001a\u00020\u001d2\b\u00103\u001a\u0004\u0018\u000102HÖ\u0003¢\u0006\u0004\b4\u00105R\u001d\u0010)\u001a\u00060\u0002j\u0002`\u00118\u0006@\u0006¢\u0006\f\n\u0004\b)\u00106\u001a\u0004\b7\u0010\u0005R\u0019\u0010*\u001a\u00020\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b*\u00108\u001a\u0004\b*\u0010\u001fR \u0010%\u001a\u00060\u0002j\u0002`\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b%\u00106\u001a\u0004\b9\u0010\u0005R \u0010(\u001a\u00060\u0002j\u0002`\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b(\u00106\u001a\u0004\b:\u0010\u0005R\u001e\u0010\"\u001a\u0004\u0018\u00010\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\"\u0010;\u001a\u0004\b<\u0010\nR&\u0010'\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00180\u00178\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b'\u0010=\u001a\u0004\b>\u0010\u001aR\u001c\u0010?\u001a\u00020\u001d8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b?\u00108\u001a\u0004\b?\u0010\u001fR\"\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00140\u00138\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010@\u001a\u0004\bA\u0010\u0016R \u0010#\u001a\u00060\u0002j\u0002`\u000b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b#\u00106\u001a\u0004\bB\u0010\u0005R \u0010 \u001a\u00060\u0002j\u0002`\u00038\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b \u00106\u001a\u0004\bC\u0010\u0005R,\u0010$\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010D\u001a\u0004\bE\u0010\u0010R \u0010!\u001a\u00060\u0002j\u0002`\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b!\u00106\u001a\u0004\bF\u0010\u0005¨\u0006K"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModel;", "Lcom/discord/widgets/chat/list/adapter/WidgetChatListAdapter$Data;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "Lcom/discord/models/guild/Guild;", "component3", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/primitives/GuildId;", "component4", "", "", "component5", "()Ljava/util/Map;", "Lcom/discord/primitives/MessageId;", "component6", "", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "component7", "()Ljava/util/List;", "", "Lcom/discord/primitives/RoleId;", "component8", "()Ljava/util/Set;", "component9", "component10", "", "component11", "()Z", "userId", "channelId", "guild", "guildId", "channelNames", "oldestMessageId", "list", "myRoleIds", "newMessagesMarkerMessageId", "newestKnownMessageId", "isLoadingMessages", "copy", "(JJLcom/discord/models/guild/Guild;JLjava/util/Map;JLjava/util/List;Ljava/util/Set;JJZ)Lcom/discord/widgets/chat/list/model/WidgetChatListModel;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getNewestKnownMessageId", "Z", "getOldestMessageId", "getNewMessagesMarkerMessageId", "Lcom/discord/models/guild/Guild;", "getGuild", "Ljava/util/Set;", "getMyRoleIds", "isSpoilerClickAllowed", "Ljava/util/List;", "getList", "getGuildId", "getUserId", "Ljava/util/Map;", "getChannelNames", "getChannelId", HookHelper.constructorName, "(JJLcom/discord/models/guild/Guild;JLjava/util/Map;JLjava/util/List;Ljava/util/Set;JJZ)V", "Companion", "ChatListState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatListModel implements WidgetChatListAdapter.Data {
    public static final Companion Companion = new Companion(null);
    private static final int MAX_MESSAGES_PER_CHANNEL = 25;
    private static final long WUMPUS_PACK_ID = 847199849233514549L;
    private static final long WUMPUS_WAVE_STICKER_ID = 749054660769218631L;
    private final long channelId;
    private final Map<Long, String> channelNames;
    private final Guild guild;
    private final long guildId;
    private final boolean isLoadingMessages;
    private final boolean isSpoilerClickAllowed;
    private final List<ChatListEntry> list;
    private final Set<Long> myRoleIds;
    private final long newMessagesMarkerMessageId;
    private final long newestKnownMessageId;
    private final long oldestMessageId;
    private final long userId;

    /* compiled from: WidgetChatListModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "DETACHED", "DETACHED_UNTOUCHED", "ATTACHED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum ChatListState {
        DETACHED,
        DETACHED_UNTOUCHED,
        ATTACHED;
        
        public static final Companion Companion = new Companion(null);

        /* compiled from: WidgetChatListModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001b\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState$Companion;", "", "", "channelId", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<ChatListState> get(final long j) {
                Observable Y = StoreStream.Companion.getMessages().observeIsDetached(j).Y(new b<Boolean, Observable<? extends ChatListState>>() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModel$ChatListState$Companion$get$1

                    /* compiled from: WidgetChatListModel.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "kotlin.jvm.PlatformType", "<name for destructuring parameter 0>", "Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;)Lcom/discord/widgets/chat/list/model/WidgetChatListModel$ChatListState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.chat.list.model.WidgetChatListModel$ChatListState$Companion$get$1$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass1<T, R> implements b<StoreMessagesLoader.ChannelLoadedState, WidgetChatListModel.ChatListState> {
                        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                        public final WidgetChatListModel.ChatListState call(StoreMessagesLoader.ChannelLoadedState channelLoadedState) {
                            if (channelLoadedState.component4()) {
                                return WidgetChatListModel.ChatListState.DETACHED;
                            }
                            return WidgetChatListModel.ChatListState.DETACHED_UNTOUCHED;
                        }
                    }

                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Observable<? extends WidgetChatListModel.ChatListState> call(Boolean bool) {
                        return call(bool.booleanValue());
                    }

                    public final Observable<? extends WidgetChatListModel.ChatListState> call(boolean z2) {
                        if (z2) {
                            return (Observable<R>) StoreStream.Companion.getMessagesLoader().getMessagesLoadedState(j).F(AnonymousClass1.INSTANCE).q();
                        }
                        return new k(WidgetChatListModel.ChatListState.ATTACHED);
                    }
                });
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …        }\n              }");
                return Y;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }
    }

    /* compiled from: WidgetChatListModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b*\u0010+J\u001f\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J/\u0010\f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00042\u0006\u0010\b\u001a\u00020\u00022\u000e\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nH\u0002¢\u0006\u0004\b\f\u0010\rJC\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00022\u001a\u0010\u0016\u001a\u0016\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\b\u0012\u00060\u0014j\u0002`\u00150\u0012H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J!\u0010\u001b\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJC\u0010!\u001a&\u0012\f\u0012\n  *\u0004\u0018\u00010\u000e0\u000e  *\u0012\u0012\f\u0012\n  *\u0004\u0018\u00010\u000e0\u000e\u0018\u00010\u00040\u0004*\u00020\u001d2\n\u0010\u001f\u001a\u00060\tj\u0002`\u001eH\u0002¢\u0006\u0004\b!\u0010\"J\u0015\u0010#\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004¢\u0006\u0004\b#\u0010$R\u0016\u0010%\u001a\u00020\u00148\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010'\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010)\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b)\u0010(¨\u0006,"}, d2 = {"Lcom/discord/widgets/chat/list/model/WidgetChatListModel$Companion;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "Lcom/discord/widgets/chat/list/model/WidgetChatListModel;", "getChannel", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "parentChannel", "", "Lcom/discord/primitives/MessageId;", "parentMessageId", "getThreadDraft", "(Lcom/discord/api/channel/Channel;Ljava/lang/Long;)Lrx/Observable;", "Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;", "loadingState", "Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages;", "messages", "", "Lcom/discord/primitives/UserId;", "", "Lcom/discord/primitives/RelationshipType;", "relationships", "", "shouldShowStickerGreet", "(Lcom/discord/stores/StoreMessagesLoader$ChannelLoadedState;Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages;Lcom/discord/api/channel/Channel;Ljava/util/Map;)Z", "Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "getGreetMessageItem", "(Lcom/discord/widgets/chat/list/model/WidgetChatListModelMessages;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/chat/list/entries/ChatListEntry;", "Lcom/discord/stores/StoreMessagesLoader;", "Lcom/discord/primitives/ChannelId;", "channelId", "kotlin.jvm.PlatformType", "observeIsLoadingMessages", "(Lcom/discord/stores/StoreMessagesLoader;J)Lrx/Observable;", "get", "()Lrx/Observable;", "MAX_MESSAGES_PER_CHANNEL", "I", "WUMPUS_PACK_ID", "J", "WUMPUS_WAVE_STICKER_ID", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<WidgetChatListModel> getChannel(final Channel channel) {
            Observable<WidgetChatListModelTop> observable = WidgetChatListModelTop.Companion.get(channel);
            Observable<WidgetChatListModelMessages> observable2 = WidgetChatListModelMessages.Companion.get(channel);
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<WidgetChatListModel> c = Observable.c(observable, observable2, observeIsLoadingMessages(companion.getMessagesLoader(), channel.h()), companion.getChannels().observeNames(), companion.getUsers().observeMeId(), companion.getGuilds().observeComputed(channel.f()), companion.getGuilds().observeGuild(channel.f()), companion.getUserRelationships().observe(), ChatListState.Companion.get(channel.h()), new Func9<WidgetChatListModelTop, WidgetChatListModelMessages, StoreMessagesLoader.ChannelLoadedState, Map<Long, ? extends String>, Long, Map<Long, ? extends GuildMember>, Guild, Map<Long, ? extends Integer>, ChatListState, WidgetChatListModel>() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModel$Companion$getChannel$1
                @Override // rx.functions.Func9
                public /* bridge */ /* synthetic */ WidgetChatListModel call(WidgetChatListModelTop widgetChatListModelTop, WidgetChatListModelMessages widgetChatListModelMessages, StoreMessagesLoader.ChannelLoadedState channelLoadedState, Map<Long, ? extends String> map, Long l, Map<Long, ? extends GuildMember> map2, Guild guild, Map<Long, ? extends Integer> map3, WidgetChatListModel.ChatListState chatListState) {
                    return call2(widgetChatListModelTop, widgetChatListModelMessages, channelLoadedState, (Map<Long, String>) map, l, (Map<Long, GuildMember>) map2, guild, (Map<Long, Integer>) map3, chatListState);
                }

                /* JADX WARN: Code restructure failed: missing block: B:10:0x0053, code lost:
                    r4 = r5.getGreetMessageItem(r27, r1);
                 */
                /* renamed from: call  reason: avoid collision after fix types in other method */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public final com.discord.widgets.chat.list.model.WidgetChatListModel call2(com.discord.widgets.chat.list.model.WidgetChatListModelTop r26, com.discord.widgets.chat.list.model.WidgetChatListModelMessages r27, com.discord.stores.StoreMessagesLoader.ChannelLoadedState r28, java.util.Map<java.lang.Long, java.lang.String> r29, java.lang.Long r30, java.util.Map<java.lang.Long, com.discord.models.member.GuildMember> r31, com.discord.models.guild.Guild r32, java.util.Map<java.lang.Long, java.lang.Integer> r33, com.discord.widgets.chat.list.model.WidgetChatListModel.ChatListState r34) {
                    /*
                        r25 = this;
                        r0 = r25
                        r1 = r27
                        r2 = r28
                        r3 = r30
                        r4 = r33
                        com.discord.widgets.chat.list.model.WidgetChatListModel$Companion r5 = com.discord.widgets.chat.list.model.WidgetChatListModel.Companion
                        java.lang.String r6 = "loadingState"
                        d0.z.d.m.checkNotNullExpressionValue(r2, r6)
                        java.lang.String r6 = "messages"
                        d0.z.d.m.checkNotNullExpressionValue(r1, r6)
                        com.discord.api.channel.Channel r6 = com.discord.api.channel.Channel.this
                        java.lang.String r7 = "relationships"
                        d0.z.d.m.checkNotNullExpressionValue(r4, r7)
                        boolean r4 = com.discord.widgets.chat.list.model.WidgetChatListModel.Companion.access$shouldShowStickerGreet(r5, r2, r1, r6, r4)
                        java.util.List r6 = r27.getItems()
                        int r6 = r6.size()
                        int r6 = r6 + 2
                        if (r4 == 0) goto L2f
                        int r6 = r6 + 1
                    L2f:
                        java.util.ArrayList r15 = new java.util.ArrayList
                        r15.<init>(r6)
                        com.discord.widgets.chat.list.model.WidgetChatListModel$ChatListState r6 = com.discord.widgets.chat.list.model.WidgetChatListModel.ChatListState.DETACHED
                        r7 = r34
                        if (r7 != r6) goto L43
                        com.discord.widgets.chat.list.entries.LoadingEntry r4 = new com.discord.widgets.chat.list.entries.LoadingEntry
                        r4.<init>()
                        r15.add(r4)
                        goto L5e
                    L43:
                        com.discord.widgets.chat.list.entries.SpacerEntry r6 = new com.discord.widgets.chat.list.entries.SpacerEntry
                        com.discord.api.channel.Channel r7 = com.discord.api.channel.Channel.this
                        long r7 = r7.h()
                        r6.<init>(r7)
                        r15.add(r6)
                        if (r4 == 0) goto L5e
                        com.discord.api.channel.Channel r4 = com.discord.api.channel.Channel.this
                        com.discord.widgets.chat.list.entries.ChatListEntry r4 = com.discord.widgets.chat.list.model.WidgetChatListModel.Companion.access$getGreetMessageItem(r5, r1, r4)
                        if (r4 == 0) goto L5e
                        r15.add(r4)
                    L5e:
                        java.util.List r4 = r27.getItems()
                        r15.addAll(r4)
                        com.discord.widgets.chat.list.entries.ChatListEntry r4 = r26.getItem()
                        r15.add(r4)
                        r4 = r31
                        java.lang.Object r4 = r4.get(r3)
                        com.discord.models.member.GuildMember r4 = (com.discord.models.member.GuildMember) r4
                        if (r4 == 0) goto L84
                        java.util.List r4 = r4.getRoles()
                        if (r4 == 0) goto L84
                        java.util.HashSet r5 = new java.util.HashSet
                        r5.<init>(r4)
                        r19 = r5
                        goto L8a
                    L84:
                        java.util.Set r4 = d0.t.n0.emptySet()
                        r19 = r4
                    L8a:
                        com.discord.widgets.chat.list.model.WidgetChatListModel r4 = new com.discord.widgets.chat.list.model.WidgetChatListModel
                        r7 = r4
                        java.lang.String r5 = "myId"
                        d0.z.d.m.checkNotNullExpressionValue(r3, r5)
                        long r8 = r30.longValue()
                        com.discord.api.channel.Channel r3 = com.discord.api.channel.Channel.this
                        long r10 = r3.h()
                        com.discord.api.channel.Channel r3 = com.discord.api.channel.Channel.this
                        long r13 = r3.f()
                        java.lang.String r3 = "channelNames"
                        r5 = r29
                        d0.z.d.m.checkNotNullExpressionValue(r5, r3)
                        long r16 = r27.getOldestMessageId()
                        long r20 = r27.getNewMessagesMarkerMessageId()
                        long r22 = r27.getNewestKnownMessageId()
                        boolean r24 = r28.isLoadingMessages()
                        r12 = r32
                        r1 = r15
                        r15 = r29
                        r18 = r1
                        r7.<init>(r8, r10, r12, r13, r15, r16, r18, r19, r20, r22, r24)
                        return r4
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.chat.list.model.WidgetChatListModel$Companion$getChannel$1.call2(com.discord.widgets.chat.list.model.WidgetChatListModelTop, com.discord.widgets.chat.list.model.WidgetChatListModelMessages, com.discord.stores.StoreMessagesLoader$ChannelLoadedState, java.util.Map, java.lang.Long, java.util.Map, com.discord.models.guild.Guild, java.util.Map, com.discord.widgets.chat.list.model.WidgetChatListModel$ChatListState):com.discord.widgets.chat.list.model.WidgetChatListModel");
                }
            });
            m.checkNotNullExpressionValue(c, "Observable\n            .…          )\n            }");
            return c;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final ChatListEntry getGreetMessageItem(WidgetChatListModelMessages widgetChatListModelMessages, Channel channel) {
            StoreStream.Companion companion = StoreStream.Companion;
            StoreStickers stickers = companion.getStickers();
            Map<Long, Sticker> stickers2 = stickers.getStickers();
            Long valueOf = Long.valueOf((long) WidgetChatListModel.WUMPUS_WAVE_STICKER_ID);
            if (stickers2.get(valueOf) == null) {
                stickers.fetchStickerPack(WidgetChatListModel.WUMPUS_PACK_ID);
            }
            Sticker sticker = stickers.getStickers().get(valueOf);
            if (sticker != null) {
                Guild guild = companion.getGuilds().getGuild(channel.f());
                boolean z2 = false;
                if (((guild != null ? guild.getSystemChannelFlags() : 0) & 8) == 0) {
                    z2 = true;
                }
                if (!z2) {
                    return null;
                }
                if (widgetChatListModelMessages.getItems().isEmpty()) {
                    return new StickerGreetEntry(sticker, channel.h(), ChannelUtils.c(channel), channel.A());
                }
                if (widgetChatListModelMessages.getItems().size() < 25) {
                    return new StickerGreetCompactEntry(sticker, channel.h(), ChannelUtils.c(channel), channel.A());
                }
            }
            return null;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<WidgetChatListModel> getThreadDraft(final Channel channel, final Long l) {
            Observable observable;
            final long f = channel.f();
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<Map<Long, String>> observeNames = companion.getChannels().observeNames();
            Observable<Long> observeMeId = companion.getUsers().observeMeId();
            Observable<Map<Long, GuildMember>> observeComputed = companion.getGuilds().observeComputed(f);
            Observable<Guild> observeGuild = companion.getGuilds().observeGuild(f);
            Observable<StoreThreadDraft.ThreadDraftState> observeDraftState = companion.getThreadDraft().observeDraftState();
            Observable<Long> observePermissionsForChannel = companion.getPermissions().observePermissionsForChannel(channel.h());
            if (l != null) {
                Observable<Message> observeMessagesForChannel = companion.getMessages().observeMessagesForChannel(channel.h(), l.longValue());
                Observable<List<ChatListEntry>> singleMessage = WidgetChatListModelMessages.Companion.getSingleMessage(channel, l.longValue());
                final WidgetChatListModel$Companion$getThreadDraft$1 widgetChatListModel$Companion$getThreadDraft$1 = WidgetChatListModel$Companion$getThreadDraft$1.INSTANCE;
                Object obj = widgetChatListModel$Companion$getThreadDraft$1;
                if (widgetChatListModel$Companion$getThreadDraft$1 != null) {
                    obj = new Func2() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModel$sam$rx_functions_Func2$0
                        @Override // rx.functions.Func2
                        public final /* synthetic */ Object call(Object obj2, Object obj3) {
                            return Function2.this.invoke(obj2, obj3);
                        }
                    };
                }
                observable = Observable.j(observeMessagesForChannel, singleMessage, (Func2) obj);
            } else {
                k kVar = new k(null);
                k kVar2 = new k(new ArrayList());
                final WidgetChatListModel$Companion$getThreadDraft$2 widgetChatListModel$Companion$getThreadDraft$2 = WidgetChatListModel$Companion$getThreadDraft$2.INSTANCE;
                Object obj2 = widgetChatListModel$Companion$getThreadDraft$2;
                if (widgetChatListModel$Companion$getThreadDraft$2 != null) {
                    obj2 = new Func2() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModel$sam$rx_functions_Func2$0
                        @Override // rx.functions.Func2
                        public final /* synthetic */ Object call(Object obj22, Object obj3) {
                            return Function2.this.invoke(obj22, obj3);
                        }
                    };
                }
                observable = Observable.j(kVar, kVar2, (Func2) obj2);
            }
            Observable<WidgetChatListModel> e = Observable.e(observeNames, observeMeId, observeComputed, observeGuild, observeDraftState, observePermissionsForChannel, observable, new Func7<Map<Long, ? extends String>, Long, Map<Long, ? extends GuildMember>, Guild, StoreThreadDraft.ThreadDraftState, Long, Pair<? extends Message, ? extends List<? extends ChatListEntry>>, WidgetChatListModel>() { // from class: com.discord.widgets.chat.list.model.WidgetChatListModel$Companion$getThreadDraft$3
                @Override // rx.functions.Func7
                public /* bridge */ /* synthetic */ WidgetChatListModel call(Map<Long, ? extends String> map, Long l2, Map<Long, ? extends GuildMember> map2, Guild guild, StoreThreadDraft.ThreadDraftState threadDraftState, Long l3, Pair<? extends Message, ? extends List<? extends ChatListEntry>> pair) {
                    return call2((Map<Long, String>) map, l2, (Map<Long, GuildMember>) map2, guild, threadDraftState, l3, (Pair<Message, ? extends List<? extends ChatListEntry>>) pair);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetChatListModel call2(Map<Long, String> map, Long l2, Map<Long, GuildMember> map2, Guild guild, StoreThreadDraft.ThreadDraftState threadDraftState, Long l3, Pair<Message, ? extends List<? extends ChatListEntry>> pair) {
                    List<Long> roles;
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new SpacerEntry(Channel.this.h()));
                    if (threadDraftState.isSending()) {
                        arrayList.add(new LoadingEntry());
                    }
                    arrayList.addAll(pair.getSecond());
                    long h = Channel.this.h();
                    Long l4 = l;
                    long f2 = Channel.this.f();
                    Integer d = Channel.this.d();
                    m.checkNotNullExpressionValue(threadDraftState, "threadDraft");
                    ThreadUtils threadUtils = ThreadUtils.INSTANCE;
                    arrayList.add(new ThreadDraftFormEntry(h, l4, f2, guild, d, threadDraftState, ThreadUtils.canCreatePrivateThread$default(threadUtils, l3, Channel.this, guild, false, 8, null), threadUtils.canCreatePrivateThread(l3, Channel.this, guild, false), threadUtils.canCreatePublicThread(l3, Channel.this, pair.getFirst(), guild)));
                    GuildMember guildMember = map2.get(l2);
                    Set emptySet = (guildMember == null || (roles = guildMember.getRoles()) == null) ? n0.emptySet() : new HashSet(roles);
                    m.checkNotNullExpressionValue(l2, "myId");
                    long longValue = l2.longValue();
                    long j = f;
                    m.checkNotNullExpressionValue(map, "channelNames");
                    return new WidgetChatListModel(longValue, -3L, guild, j, map, 0L, arrayList, emptySet, 0L, 0L, false, 800, null);
                }
            });
            m.checkNotNullExpressionValue(e, "Observable\n          .co…            )\n          }");
            return e;
        }

        private final Observable<StoreMessagesLoader.ChannelLoadedState> observeIsLoadingMessages(StoreMessagesLoader storeMessagesLoader, long j) {
            return storeMessagesLoader.getMessagesLoadedState(j).q();
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final boolean shouldShowStickerGreet(StoreMessagesLoader.ChannelLoadedState channelLoadedState, WidgetChatListModelMessages widgetChatListModelMessages, Channel channel, Map<Long, Integer> map) {
            if (channelLoadedState.isOldestMessagesLoaded() && channelLoadedState.isInitialMessagesLoaded() && channelLoadedState.getNewestSentByUserMessageId() == null && widgetChatListModelMessages.getNewestSentByUserMessageId() == null && widgetChatListModelMessages.getItems().size() < 25 && ChannelUtils.m(channel) && !ChannelUtils.A(channel)) {
                User a = ChannelUtils.a(channel);
                Integer num = map.get(a != null ? Long.valueOf(a.getId()) : null);
                if (num == null || num.intValue() != 2) {
                    return true;
                }
            }
            return false;
        }

        public final Observable<WidgetChatListModel> get() {
            Observable Y = StoreStream.Companion.getChannelsSelected().observeResolvedSelectedChannel().Y(WidgetChatListModel$Companion$get$1.INSTANCE);
            m.checkNotNullExpressionValue(Y, "StoreStream\n          .g…            }\n          }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public WidgetChatListModel(long j, long j2, Guild guild, long j3, Map<Long, String> map, long j4, List<? extends ChatListEntry> list, Set<Long> set, long j5, long j6, boolean z2) {
        m.checkNotNullParameter(map, "channelNames");
        m.checkNotNullParameter(list, "list");
        m.checkNotNullParameter(set, "myRoleIds");
        this.userId = j;
        this.channelId = j2;
        this.guild = guild;
        this.guildId = j3;
        this.channelNames = map;
        this.oldestMessageId = j4;
        this.list = list;
        this.myRoleIds = set;
        this.newMessagesMarkerMessageId = j5;
        this.newestKnownMessageId = j6;
        this.isLoadingMessages = z2;
        this.isSpoilerClickAllowed = true;
    }

    public final long component1() {
        return getUserId();
    }

    public final long component10() {
        return this.newestKnownMessageId;
    }

    public final boolean component11() {
        return this.isLoadingMessages;
    }

    public final long component2() {
        return getChannelId();
    }

    public final Guild component3() {
        return getGuild();
    }

    public final long component4() {
        return getGuildId();
    }

    public final Map<Long, String> component5() {
        return getChannelNames();
    }

    public final long component6() {
        return getOldestMessageId();
    }

    public final List<ChatListEntry> component7() {
        return getList();
    }

    public final Set<Long> component8() {
        return getMyRoleIds();
    }

    public final long component9() {
        return getNewMessagesMarkerMessageId();
    }

    public final WidgetChatListModel copy(long j, long j2, Guild guild, long j3, Map<Long, String> map, long j4, List<? extends ChatListEntry> list, Set<Long> set, long j5, long j6, boolean z2) {
        m.checkNotNullParameter(map, "channelNames");
        m.checkNotNullParameter(list, "list");
        m.checkNotNullParameter(set, "myRoleIds");
        return new WidgetChatListModel(j, j2, guild, j3, map, j4, list, set, j5, j6, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetChatListModel)) {
            return false;
        }
        WidgetChatListModel widgetChatListModel = (WidgetChatListModel) obj;
        return getUserId() == widgetChatListModel.getUserId() && getChannelId() == widgetChatListModel.getChannelId() && m.areEqual(getGuild(), widgetChatListModel.getGuild()) && getGuildId() == widgetChatListModel.getGuildId() && m.areEqual(getChannelNames(), widgetChatListModel.getChannelNames()) && getOldestMessageId() == widgetChatListModel.getOldestMessageId() && m.areEqual(getList(), widgetChatListModel.getList()) && m.areEqual(getMyRoleIds(), widgetChatListModel.getMyRoleIds()) && getNewMessagesMarkerMessageId() == widgetChatListModel.getNewMessagesMarkerMessageId() && this.newestKnownMessageId == widgetChatListModel.newestKnownMessageId && this.isLoadingMessages == widgetChatListModel.isLoadingMessages;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public long getChannelId() {
        return this.channelId;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public Map<Long, String> getChannelNames() {
        return this.channelNames;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public Guild getGuild() {
        return this.guild;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public long getGuildId() {
        return this.guildId;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public List<ChatListEntry> getList() {
        return this.list;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public Set<Long> getMyRoleIds() {
        return this.myRoleIds;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public long getNewMessagesMarkerMessageId() {
        return this.newMessagesMarkerMessageId;
    }

    public final long getNewestKnownMessageId() {
        return this.newestKnownMessageId;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public long getOldestMessageId() {
        return this.oldestMessageId;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public long getUserId() {
        return this.userId;
    }

    public int hashCode() {
        int a = (a0.a.a.b.a(getChannelId()) + (a0.a.a.b.a(getUserId()) * 31)) * 31;
        Guild guild = getGuild();
        int i = 0;
        int a2 = (a0.a.a.b.a(getGuildId()) + ((a + (guild != null ? guild.hashCode() : 0)) * 31)) * 31;
        Map<Long, String> channelNames = getChannelNames();
        int a3 = (a0.a.a.b.a(getOldestMessageId()) + ((a2 + (channelNames != null ? channelNames.hashCode() : 0)) * 31)) * 31;
        List<ChatListEntry> list = getList();
        int hashCode = (a3 + (list != null ? list.hashCode() : 0)) * 31;
        Set<Long> myRoleIds = getMyRoleIds();
        if (myRoleIds != null) {
            i = myRoleIds.hashCode();
        }
        int a4 = (a0.a.a.b.a(this.newestKnownMessageId) + ((a0.a.a.b.a(getNewMessagesMarkerMessageId()) + ((hashCode + i) * 31)) * 31)) * 31;
        boolean z2 = this.isLoadingMessages;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        return a4 + i2;
    }

    public final boolean isLoadingMessages() {
        return this.isLoadingMessages;
    }

    @Override // com.discord.widgets.chat.list.adapter.WidgetChatListAdapter.Data
    public boolean isSpoilerClickAllowed() {
        return this.isSpoilerClickAllowed;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetChatListModel(userId=");
        R.append(getUserId());
        R.append(", channelId=");
        R.append(getChannelId());
        R.append(", guild=");
        R.append(getGuild());
        R.append(", guildId=");
        R.append(getGuildId());
        R.append(", channelNames=");
        R.append(getChannelNames());
        R.append(", oldestMessageId=");
        R.append(getOldestMessageId());
        R.append(", list=");
        R.append(getList());
        R.append(", myRoleIds=");
        R.append(getMyRoleIds());
        R.append(", newMessagesMarkerMessageId=");
        R.append(getNewMessagesMarkerMessageId());
        R.append(", newestKnownMessageId=");
        R.append(this.newestKnownMessageId);
        R.append(", isLoadingMessages=");
        return a.M(R, this.isLoadingMessages, ")");
    }

    public /* synthetic */ WidgetChatListModel(long j, long j2, Guild guild, long j3, Map map, long j4, List list, Set set, long j5, long j6, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, j2, guild, j3, map, (i & 32) != 0 ? 0L : j4, list, set, (i & 256) != 0 ? 0L : j5, (i & 512) != 0 ? 0L : j6, z2);
    }
}
