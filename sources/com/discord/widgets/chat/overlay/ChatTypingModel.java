package com.discord.widgets.chat.overlay;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreSlowMode;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.widgets.chat.overlay.ChatTypingModel;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import j0.k.b;
import j0.l.a.r;
import j0.l.a.u0;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: ChatTypingModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/chat/overlay/ChatTypingModel;", "", HookHelper.constructorName, "()V", "Companion", "Hide", "Typing", "Lcom/discord/widgets/chat/overlay/ChatTypingModel$Typing;", "Lcom/discord/widgets/chat/overlay/ChatTypingModel$Hide;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class ChatTypingModel {
    public static final Companion Companion = new Companion(null);

    /* compiled from: ChatTypingModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0010\r\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001d\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001d\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\b\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\u0007J#\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00042\u0006\u0010\n\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\r\u0010\u0007J\u0013\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/chat/overlay/ChatTypingModel$Companion;", "", "Lcom/discord/api/channel/Channel;", "resolvedChannel", "Lrx/Observable;", "Lcom/discord/widgets/chat/overlay/ChatTypingModel;", "getTypingObservableForChannel", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "parentChannel", "getTypingObservableForDraft", "channel", "", "", "getTypingUsers", "get", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<ChatTypingModel> getTypingObservableForChannel(Channel channel) {
            k kVar = new k(channel);
            Observable<ChatTypingModel> Y = Observable.h0(new r(kVar.j, new u0(ChatTypingModel$Companion$getTypingObservableForChannel$1.INSTANCE))).Y(ChatTypingModel$Companion$getTypingObservableForChannel$2.INSTANCE);
            m.checkNotNullExpressionValue(Y, "Observable.just(resolved…          }\n            }");
            return Y;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<ChatTypingModel> getTypingObservableForDraft(final Channel channel) {
            Observable Y = StoreStream.Companion.getSlowMode().observeCooldownSecs(Long.valueOf(channel.h()), StoreSlowMode.Type.ThreadCreate.INSTANCE).Y(new b<Integer, Observable<? extends ChatTypingModel>>() { // from class: com.discord.widgets.chat.overlay.ChatTypingModel$Companion$getTypingObservableForDraft$1
                public final Observable<? extends ChatTypingModel> call(Integer num) {
                    List emptyList = n.emptyList();
                    int u = Channel.this.u();
                    m.checkNotNullExpressionValue(num, "cooldownSecs");
                    return new k(new ChatTypingModel.Typing(emptyList, u, num.intValue()));
                }
            });
            m.checkNotNullExpressionValue(Y, "StoreStream\n            …ldownSecs))\n            }");
            return Y;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<List<CharSequence>> getTypingUsers(final Channel channel) {
            Observable<List<CharSequence>> q = StoreStream.Companion.getUsersTyping().observeTypingUsers(channel.h()).Y(new b<Set<? extends Long>, Observable<? extends List<? extends String>>>() { // from class: com.discord.widgets.chat.overlay.ChatTypingModel$Companion$getTypingUsers$1

                /* compiled from: ChatTypingModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\u0010\r\u001a\u0016\u0012\u0004\u0012\u00020\n \u0004*\n\u0012\u0004\u0012\u00020\n\u0018\u00010\t0\t2.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u000026\u0010\b\u001a2\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0006j\u0002`\u0007 \u0004*\u0018\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u000b\u0010\f"}, d2 = {"", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "kotlin.jvm.PlatformType", "users", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/stores/ClientGuildMember;", "members", "", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.chat.overlay.ChatTypingModel$Companion$getTypingUsers$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1<T1, T2, R> implements Func2<Map<Long, ? extends User>, Map<Long, ? extends GuildMember>, List<? extends String>> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    @Override // rx.functions.Func2
                    public /* bridge */ /* synthetic */ List<? extends String> call(Map<Long, ? extends User> map, Map<Long, ? extends GuildMember> map2) {
                        return call2(map, (Map<Long, GuildMember>) map2);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final List<String> call2(Map<Long, ? extends User> map, Map<Long, GuildMember> map2) {
                        Collection<? extends User> values = map.values();
                        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(values, 10));
                        for (User user : values) {
                            arrayList.add(GuildMember.Companion.getNickOrUsername((GuildMember) a.e(user, map2), user));
                        }
                        return arrayList;
                    }
                }

                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends List<? extends String>> call(Set<? extends Long> set) {
                    return call2((Set<Long>) set);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends List<String>> call2(Set<Long> set) {
                    StoreStream.Companion companion = StoreStream.Companion;
                    StoreUser users = companion.getUsers();
                    m.checkNotNullExpressionValue(set, "userIds");
                    return Observable.j(users.observeUsers(set), companion.getGuilds().observeComputed(Channel.this.f(), set), AnonymousClass1.INSTANCE);
                }
            }).F(ChatTypingModel$Companion$getTypingUsers$2.INSTANCE).q();
            m.checkNotNullExpressionValue(q, "StoreStream\n          .g…  .distinctUntilChanged()");
            return q;
        }

        public final Observable<ChatTypingModel> get() {
            Observable<ChatTypingModel> q = StoreStream.Companion.getChannelsSelected().observeResolvedSelectedChannel().Y(ChatTypingModel$Companion$get$1.INSTANCE).q();
            m.checkNotNullExpressionValue(q, "StoreStream.getChannelsS…  .distinctUntilChanged()");
            return q;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ChatTypingModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/chat/overlay/ChatTypingModel$Hide;", "Lcom/discord/widgets/chat/overlay/ChatTypingModel;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Hide extends ChatTypingModel {
        public static final Hide INSTANCE = new Hide();

        private Hide() {
            super(null);
        }
    }

    /* compiled from: ChatTypingModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0006\u0012\u0006\u0010\f\u001a\u00020\u0006¢\u0006\u0004\b\u001d\u0010\u001eJ\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ4\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00062\b\b\u0002\u0010\f\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0012\u0010\bJ\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u001a\u0010\bR\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001b\u001a\u0004\b\u001c\u0010\u0005¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/chat/overlay/ChatTypingModel$Typing;", "Lcom/discord/widgets/chat/overlay/ChatTypingModel;", "", "", "component1", "()Ljava/util/List;", "", "component2", "()I", "component3", "typingUsers", "channelRateLimit", "cooldownSecs", "copy", "(Ljava/util/List;II)Lcom/discord/widgets/chat/overlay/ChatTypingModel$Typing;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getChannelRateLimit", "getCooldownSecs", "Ljava/util/List;", "getTypingUsers", HookHelper.constructorName, "(Ljava/util/List;II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Typing extends ChatTypingModel {
        private final int channelRateLimit;
        private final int cooldownSecs;
        private final List<CharSequence> typingUsers;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        /* JADX WARN: Multi-variable type inference failed */
        public Typing(List<? extends CharSequence> list, int i, int i2) {
            super(null);
            m.checkNotNullParameter(list, "typingUsers");
            this.typingUsers = list;
            this.channelRateLimit = i;
            this.cooldownSecs = i2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Typing copy$default(Typing typing, List list, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                list = typing.typingUsers;
            }
            if ((i3 & 2) != 0) {
                i = typing.channelRateLimit;
            }
            if ((i3 & 4) != 0) {
                i2 = typing.cooldownSecs;
            }
            return typing.copy(list, i, i2);
        }

        public final List<CharSequence> component1() {
            return this.typingUsers;
        }

        public final int component2() {
            return this.channelRateLimit;
        }

        public final int component3() {
            return this.cooldownSecs;
        }

        public final Typing copy(List<? extends CharSequence> list, int i, int i2) {
            m.checkNotNullParameter(list, "typingUsers");
            return new Typing(list, i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Typing)) {
                return false;
            }
            Typing typing = (Typing) obj;
            return m.areEqual(this.typingUsers, typing.typingUsers) && this.channelRateLimit == typing.channelRateLimit && this.cooldownSecs == typing.cooldownSecs;
        }

        public final int getChannelRateLimit() {
            return this.channelRateLimit;
        }

        public final int getCooldownSecs() {
            return this.cooldownSecs;
        }

        public final List<CharSequence> getTypingUsers() {
            return this.typingUsers;
        }

        public int hashCode() {
            List<CharSequence> list = this.typingUsers;
            return ((((list != null ? list.hashCode() : 0) * 31) + this.channelRateLimit) * 31) + this.cooldownSecs;
        }

        public String toString() {
            StringBuilder R = a.R("Typing(typingUsers=");
            R.append(this.typingUsers);
            R.append(", channelRateLimit=");
            R.append(this.channelRateLimit);
            R.append(", cooldownSecs=");
            return a.A(R, this.cooldownSecs, ")");
        }
    }

    private ChatTypingModel() {
    }

    public /* synthetic */ ChatTypingModel(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
