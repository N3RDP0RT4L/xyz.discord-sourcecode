package com.discord.widgets.chat.overlay;

import com.discord.widgets.chat.overlay.WidgetChatOverlay;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatOverlay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/overlay/ChatTypingModel;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/widgets/chat/overlay/ChatTypingModel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatOverlay$onViewBoundOrOnResume$4 extends o implements Function1<ChatTypingModel, Unit> {
    public final /* synthetic */ WidgetChatOverlay this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatOverlay$onViewBoundOrOnResume$4(WidgetChatOverlay widgetChatOverlay) {
        super(1);
        this.this$0 = widgetChatOverlay;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ChatTypingModel chatTypingModel) {
        invoke2(chatTypingModel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ChatTypingModel chatTypingModel) {
        WidgetChatOverlay.TypingIndicatorViewHolder access$getTypingIndicatorViewHolder$p = WidgetChatOverlay.access$getTypingIndicatorViewHolder$p(this.this$0);
        m.checkNotNullExpressionValue(chatTypingModel, "it");
        access$getTypingIndicatorViewHolder$p.configureUI(chatTypingModel);
    }
}
