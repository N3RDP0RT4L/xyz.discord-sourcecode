package com.discord.widgets.chat.overlay;

import androidx.core.app.NotificationCompat;
import b.a.k.b;
import b.d.b.a.a;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
/* compiled from: ChatTypingModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0002*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00000\u00002\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "kotlin.jvm.PlatformType", "names", "", NotificationCompat.CATEGORY_CALL, "(Ljava/util/List;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatTypingModel$Companion$getTypingUsers$2<T, R> implements b<List<? extends String>, List<? extends CharSequence>> {
    public static final ChatTypingModel$Companion$getTypingUsers$2 INSTANCE = new ChatTypingModel$Companion$getTypingUsers$2();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ List<? extends CharSequence> call(List<? extends String> list) {
        return call2((List<String>) list);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final List<CharSequence> call2(List<String> list) {
        CharSequence g;
        m.checkNotNullExpressionValue(list, "names");
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
        for (String str : list) {
            g = b.a.k.b.g(a.w("!!", str, "!!"), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
            arrayList.add(g);
        }
        return u.take(arrayList, 4);
    }
}
