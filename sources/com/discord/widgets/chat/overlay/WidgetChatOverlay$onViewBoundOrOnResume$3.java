package com.discord.widgets.chat.overlay;

import com.discord.databinding.WidgetChatOverlayBinding;
import com.discord.widgets.chat.overlay.WidgetChatOverlay;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatOverlay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatOverlay$onViewBoundOrOnResume$3 extends o implements Function1<WidgetChatOverlay.OldMessageModel, Unit> {
    public final /* synthetic */ WidgetChatOverlay this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChatOverlay$onViewBoundOrOnResume$3(WidgetChatOverlay widgetChatOverlay) {
        super(1);
        this.this$0 = widgetChatOverlay;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetChatOverlay.OldMessageModel oldMessageModel) {
        invoke2(oldMessageModel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetChatOverlay.OldMessageModel oldMessageModel) {
        WidgetChatOverlayBinding binding;
        WidgetChatOverlayBinding binding2;
        if (oldMessageModel.isViewingOldMessages()) {
            binding2 = this.this$0.getBinding();
            binding2.f2326b.show();
            return;
        }
        binding = this.this$0.getBinding();
        binding.f2326b.hide();
    }
}
