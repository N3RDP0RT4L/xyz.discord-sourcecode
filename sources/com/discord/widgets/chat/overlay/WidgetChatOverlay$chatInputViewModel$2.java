package com.discord.widgets.chat.overlay;

import com.discord.app.AppViewModel;
import com.discord.widgets.chat.input.ChatInputViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChatOverlay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/chat/input/ChatInputViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatOverlay$chatInputViewModel$2 extends o implements Function0<AppViewModel<ChatInputViewModel.ViewState>> {
    public static final WidgetChatOverlay$chatInputViewModel$2 INSTANCE = new WidgetChatOverlay$chatInputViewModel$2();

    public WidgetChatOverlay$chatInputViewModel$2() {
        super(0);
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<ChatInputViewModel.ViewState> invoke() {
        return new ChatInputViewModel(null, null, null, null, null, null, null, null, null, null, null, null, null, false, null, 32767, null);
    }
}
