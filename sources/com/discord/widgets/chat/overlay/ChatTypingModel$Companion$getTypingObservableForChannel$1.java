package com.discord.widgets.chat.overlay;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: ChatTypingModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0003\u0010\u0007\u001a\u001a\u0012\u0006\b\u0001\u0012\u00020\u0004 \u0001*\f\u0012\u0006\b\u0001\u0012\u00020\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channel", "", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)[Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatTypingModel$Companion$getTypingObservableForChannel$1<T, R> implements b<Channel, Object[]> {
    public static final ChatTypingModel$Companion$getTypingObservableForChannel$1 INSTANCE = new ChatTypingModel$Companion$getTypingObservableForChannel$1();

    public final Object[] call(Channel channel) {
        return channel != null ? new Object[]{Long.valueOf(channel.h()), Long.valueOf(channel.f()), Integer.valueOf(channel.u())} : new Object[0];
    }
}
