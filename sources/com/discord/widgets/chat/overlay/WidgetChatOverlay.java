package com.discord.widgets.chat.overlay;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.e0;
import b.a.i.s4;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetChatOverlayBinding;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.search.SearchUtils;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.sticker.StickerView;
import com.discord.views.typing.TypingDots;
import com.discord.widgets.chat.MessageManager;
import com.discord.widgets.chat.input.AppFlexInputViewModel;
import com.discord.widgets.chat.input.ChatInputViewModel;
import com.discord.widgets.chat.overlay.ChatTypingModel;
import com.lytefast.flexinput.viewmodel.FlexInputState;
import d0.g0.t;
import d0.t.n;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetChatOverlay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0002 !B\u0007¢\u0006\u0004\b\u001f\u0010\nJ#\u0010\u0007\u001a\u00020\u00062\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\f\u0010\rR\u001d\u0010\u0013\u001a\u00020\u000e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u001d\u0010\u0019\u001a\u00020\u00148B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001e\u001a\u00020\u001a8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001b\u0010\u0010\u001a\u0004\b\u001c\u0010\u001d¨\u0006\""}, d2 = {"Lcom/discord/widgets/chat/overlay/WidgetChatOverlay;", "Lcom/discord/app/AppFragment;", "Lkotlin/Pair;", "", "Lcom/lytefast/flexinput/viewmodel/FlexInputState;", "pair", "", "configureStickerSuggestions", "(Lkotlin/Pair;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$TypingIndicatorViewHolder;", "typingIndicatorViewHolder", "Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$TypingIndicatorViewHolder;", "Lcom/discord/widgets/chat/input/ChatInputViewModel;", "chatInputViewModel$delegate", "Lkotlin/Lazy;", "getChatInputViewModel", "()Lcom/discord/widgets/chat/input/ChatInputViewModel;", "chatInputViewModel", "Lcom/discord/databinding/WidgetChatOverlayBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetChatOverlayBinding;", "binding", "Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel$delegate", "getFlexInputViewModel", "()Lcom/discord/widgets/chat/input/AppFlexInputViewModel;", "flexInputViewModel", HookHelper.constructorName, "OldMessageModel", "TypingIndicatorViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatOverlay extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetChatOverlay.class, "binding", "getBinding()Lcom/discord/databinding/WidgetChatOverlayBinding;", 0)};
    private TypingIndicatorViewHolder typingIndicatorViewHolder;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetChatOverlay$binding$2.INSTANCE, null, 2, null);
    private final Lazy flexInputViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(AppFlexInputViewModel.class), new WidgetChatOverlay$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(new WidgetChatOverlay$flexInputViewModel$2(this)));
    private final Lazy chatInputViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(ChatInputViewModel.class), new WidgetChatOverlay$appActivityViewModels$$inlined$activityViewModels$3(this), new e0(WidgetChatOverlay$chatInputViewModel$2.INSTANCE));

    /* compiled from: WidgetChatOverlay.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0082\b\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u001b\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\n\u001a\u00020\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\n\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0014\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0005R\u0019\u0010\n\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\n\u0010\b¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;", "", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "component2", "()Z", "channelId", "isViewingOldMessages", "copy", "(JZ)Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Z", HookHelper.constructorName, "(JZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class OldMessageModel {
        public static final Companion Companion = new Companion(null);
        private final long channelId;
        private final boolean isViewingOldMessages;

        /* compiled from: WidgetChatOverlay.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;", "get", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<OldMessageModel> get() {
                Observable Y = StoreStream.Companion.getChannelsSelected().observeId().q().Y(WidgetChatOverlay$OldMessageModel$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …          }\n            }");
                return Y;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public OldMessageModel(long j, boolean z2) {
            this.channelId = j;
            this.isViewingOldMessages = z2;
        }

        public static /* synthetic */ OldMessageModel copy$default(OldMessageModel oldMessageModel, long j, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                j = oldMessageModel.channelId;
            }
            if ((i & 2) != 0) {
                z2 = oldMessageModel.isViewingOldMessages;
            }
            return oldMessageModel.copy(j, z2);
        }

        public final long component1() {
            return this.channelId;
        }

        public final boolean component2() {
            return this.isViewingOldMessages;
        }

        public final OldMessageModel copy(long j, boolean z2) {
            return new OldMessageModel(j, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OldMessageModel)) {
                return false;
            }
            OldMessageModel oldMessageModel = (OldMessageModel) obj;
            return this.channelId == oldMessageModel.channelId && this.isViewingOldMessages == oldMessageModel.isViewingOldMessages;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public int hashCode() {
            int a = b.a(this.channelId) * 31;
            boolean z2 = this.isViewingOldMessages;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return a + i;
        }

        public final boolean isViewingOldMessages() {
            return this.isViewingOldMessages;
        }

        public String toString() {
            StringBuilder R = a.R("OldMessageModel(channelId=");
            R.append(this.channelId);
            R.append(", isViewingOldMessages=");
            return a.M(R, this.isViewingOldMessages, ")");
        }
    }

    /* compiled from: WidgetChatOverlay.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J'\u0010\r\u001a\u00020\f2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ%\u0010\u0013\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u000f2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\f0\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0015\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0015¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001a¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$TypingIndicatorViewHolder;", "", "Lcom/discord/widgets/chat/overlay/ChatTypingModel$Typing;", "model", "", "configureTyping", "(Lcom/discord/widgets/chat/overlay/ChatTypingModel$Typing;)V", "", "cooldownSecs", "channelRateLimit", "", "hasTypingText", "", "getSlowmodeText", "(IIZ)Ljava/lang/CharSequence;", "Landroid/content/res/Resources;", "resources", "", "typingUsers", "getTypingString", "(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/CharSequence;", "Lcom/discord/widgets/chat/overlay/ChatTypingModel;", "configureUI", "(Lcom/discord/widgets/chat/overlay/ChatTypingModel;)V", "Lcom/discord/databinding/WidgetChatOverlayBinding;", "binding", "Lcom/discord/databinding/WidgetChatOverlayBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetChatOverlayBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class TypingIndicatorViewHolder {
        private final WidgetChatOverlayBinding binding;

        public TypingIndicatorViewHolder(WidgetChatOverlayBinding widgetChatOverlayBinding) {
            m.checkNotNullParameter(widgetChatOverlayBinding, "binding");
            this.binding = widgetChatOverlayBinding;
        }

        private final void configureTyping(ChatTypingModel.Typing typing) {
            int i = 8;
            if (!typing.getTypingUsers().isEmpty() || typing.getChannelRateLimit() > 0) {
                RelativeLayout relativeLayout = this.binding.c;
                m.checkNotNullExpressionValue(relativeLayout, "binding.chatOverlayTyping");
                relativeLayout.setVisibility(0);
                ConstraintLayout constraintLayout = this.binding.a;
                m.checkNotNullExpressionValue(constraintLayout, "binding.root");
                Resources resources = constraintLayout.getResources();
                m.checkNotNullExpressionValue(resources, "binding.root.resources");
                CharSequence typingString = getTypingString(resources, typing.getTypingUsers());
                boolean z2 = true;
                CharSequence slowmodeText = getSlowmodeText(typing.getCooldownSecs(), typing.getChannelRateLimit(), !t.isBlank(typingString));
                TextView textView = this.binding.g;
                m.checkNotNullExpressionValue(textView, "binding.chatTypingUsersTyping");
                ViewExtensions.setTextAndVisibilityBy(textView, typingString);
                TypingDots typingDots = this.binding.d;
                m.checkNotNullExpressionValue(typingDots, "binding.chatOverlayTypingDots");
                typingDots.setVisibility(typing.getTypingUsers().isEmpty() ^ true ? 0 : 8);
                TypingDots typingDots2 = this.binding.d;
                if (!typing.getTypingUsers().isEmpty()) {
                    int i2 = TypingDots.j;
                    typingDots2.a(false);
                } else {
                    typingDots2.b();
                }
                TextView textView2 = this.binding.e;
                m.checkNotNullExpressionValue(textView2, "binding.chatTypingUsersSlowmode");
                ViewExtensions.setTextAndVisibilityBy(textView2, slowmodeText);
                ImageView imageView = this.binding.f;
                m.checkNotNullExpressionValue(imageView, "binding.chatTypingUsersSlowmodeIcon");
                if (typing.getChannelRateLimit() <= 0) {
                    z2 = false;
                }
                if (z2) {
                    i = 0;
                }
                imageView.setVisibility(i);
                return;
            }
            this.binding.d.b();
            RelativeLayout relativeLayout2 = this.binding.c;
            m.checkNotNullExpressionValue(relativeLayout2, "binding.chatOverlayTyping");
            relativeLayout2.setVisibility(8);
        }

        private final CharSequence getSlowmodeText(int i, int i2, boolean z2) {
            if (i > 0) {
                return TimeUtils.toFriendlyStringSimple$default(TimeUtils.INSTANCE, 1000 * i, null, null, 6, null);
            }
            if (i2 <= 0 || z2) {
                return "";
            }
            ConstraintLayout constraintLayout = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout, "binding.root");
            String string = constraintLayout.getResources().getString(R.string.channel_slowmode_desc_short);
            m.checkNotNullExpressionValue(string, "binding.root.resources.g…nnel_slowmode_desc_short)");
            return string;
        }

        private final CharSequence getTypingString(Resources resources, List<? extends CharSequence> list) {
            CharSequence c;
            CharSequence c2;
            CharSequence c3;
            CharSequence c4;
            int size = list.size();
            if (size == 0) {
                return "";
            }
            if (size == 1) {
                c = b.a.k.b.c(resources, R.string.one_user_typing, new Object[]{list.get(0)}, (r4 & 4) != 0 ? b.d.j : null);
                return c;
            } else if (size == 2) {
                c2 = b.a.k.b.c(resources, R.string.two_users_typing, new Object[]{list.get(0), list.get(1)}, (r4 & 4) != 0 ? b.d.j : null);
                return c2;
            } else if (size != 3) {
                c4 = b.a.k.b.c(resources, R.string.several_users_typing, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
                return c4;
            } else {
                c3 = b.a.k.b.c(resources, R.string.three_users_typing, new Object[]{list.get(0), list.get(1), list.get(2)}, (r4 & 4) != 0 ? b.d.j : null);
                return c3;
            }
        }

        public final void configureUI(ChatTypingModel chatTypingModel) {
            m.checkNotNullParameter(chatTypingModel, "model");
            if (chatTypingModel instanceof ChatTypingModel.Hide) {
                RelativeLayout relativeLayout = this.binding.c;
                m.checkNotNullExpressionValue(relativeLayout, "binding.chatOverlayTyping");
                relativeLayout.setVisibility(8);
            } else if (chatTypingModel instanceof ChatTypingModel.Typing) {
                configureTyping((ChatTypingModel.Typing) chatTypingModel);
            }
        }
    }

    public WidgetChatOverlay() {
        super(R.layout.widget_chat_overlay);
    }

    public static final /* synthetic */ TypingIndicatorViewHolder access$getTypingIndicatorViewHolder$p(WidgetChatOverlay widgetChatOverlay) {
        TypingIndicatorViewHolder typingIndicatorViewHolder = widgetChatOverlay.typingIndicatorViewHolder;
        if (typingIndicatorViewHolder == null) {
            m.throwUninitializedPropertyAccessException("typingIndicatorViewHolder");
        }
        return typingIndicatorViewHolder;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureStickerSuggestions(Pair<Boolean, FlexInputState> pair) {
        boolean booleanValue = pair.component1().booleanValue();
        FlexInputState component2 = pair.component2();
        if (!component2.i || booleanValue) {
            s4 s4Var = getBinding().h;
            m.checkNotNullExpressionValue(s4Var, "binding.stickersSuggestions");
            LinearLayout linearLayout = s4Var.a;
            m.checkNotNullExpressionValue(linearLayout, "binding.stickersSuggestions.root");
            linearLayout.setVisibility(8);
            return;
        }
        final List take = u.take(getFlexInputViewModel().getMatchingStickers(component2.a), 4);
        boolean z2 = SearchUtils.INSTANCE.getQueriesFromSearchText(component2.a).size() == 1;
        if (take == null || take.isEmpty()) {
            s4 s4Var2 = getBinding().h;
            m.checkNotNullExpressionValue(s4Var2, "binding.stickersSuggestions");
            LinearLayout linearLayout2 = s4Var2.a;
            m.checkNotNullExpressionValue(linearLayout2, "binding.stickersSuggestions.root");
            linearLayout2.setVisibility(8);
            return;
        }
        StoreStream.Companion.getExpressionSuggestions().trackExpressionSuggestionsDisplayed(component2.a);
        s4 s4Var3 = getBinding().h;
        m.checkNotNullExpressionValue(s4Var3, "binding.stickersSuggestions");
        LinearLayout linearLayout3 = s4Var3.a;
        m.checkNotNullExpressionValue(linearLayout3, "binding.stickersSuggestions.root");
        linearLayout3.setVisibility(0);
        int i = 0;
        for (Object obj : n.listOf((Object[]) new StickerView[]{getBinding().h.f195b, getBinding().h.c, getBinding().h.d, getBinding().h.e})) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            final StickerView stickerView = (StickerView) obj;
            final Sticker sticker = i < take.size() ? (Sticker) take.get(i) : null;
            m.checkNotNullExpressionValue(stickerView, "stickerView");
            stickerView.setVisibility(sticker != null ? 0 : 8);
            if (sticker != null) {
                StickerView.e(stickerView, sticker, null, 2);
                final Sticker sticker2 = sticker;
                final boolean z3 = z2;
                stickerView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.chat.overlay.WidgetChatOverlay$configureStickerSuggestions$$inlined$forEachIndexed$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ChatInputViewModel chatInputViewModel;
                        AppFlexInputViewModel flexInputViewModel;
                        AnalyticsTracker.INSTANCE.expressionSuggestionsSelected(sticker.getId(), StoreStream.Companion.getExpressionSuggestions().getLastSuggestionTrigger());
                        m.checkNotNullExpressionValue(view, "it");
                        Context context = view.getContext();
                        m.checkNotNullExpressionValue(context, "it.context");
                        MessageManager messageManager = new MessageManager(context, null, null, null, null, null, null, null, null, 510, null);
                        chatInputViewModel = this.getChatInputViewModel();
                        chatInputViewModel.sendSticker(Sticker.this, messageManager);
                        flexInputViewModel = this.getFlexInputViewModel();
                        flexInputViewModel.onStickerSuggestionSent(z3);
                    }
                });
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetChatOverlayBinding getBinding() {
        return (WidgetChatOverlayBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ChatInputViewModel getChatInputViewModel() {
        return (ChatInputViewModel) this.chatInputViewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final AppFlexInputViewModel getFlexInputViewModel() {
        return (AppFlexInputViewModel) this.flexInputViewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        WidgetChatOverlayBinding binding = getBinding();
        m.checkNotNullExpressionValue(binding, "binding");
        this.typingIndicatorViewHolder = new TypingIndicatorViewHolder(binding);
        s4 s4Var = getBinding().h;
        m.checkNotNullExpressionValue(s4Var, "binding.stickersSuggestions");
        LinearLayout linearLayout = s4Var.a;
        m.checkNotNullExpressionValue(linearLayout, "binding.stickersSuggestions.root");
        Drawable background = linearLayout.getBackground();
        m.checkNotNullExpressionValue(background, "binding.stickersSuggestions.root.background");
        background.setAlpha(216);
        getBinding().h.f.setOnClickListener(WidgetChatOverlay$onViewBoundOrOnResume$1.INSTANCE);
        getBinding().f2326b.setOnClickListener(WidgetChatOverlay$onViewBoundOrOnResume$2.INSTANCE);
        Observable<OldMessageModel> q = OldMessageModel.Companion.get().q();
        m.checkNotNullExpressionValue(q, "OldMessageModel.get()\n  …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), WidgetChatOverlay.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatOverlay$onViewBoundOrOnResume$3(this));
        Observable q2 = ObservableExtensionsKt.computationLatest(ChatTypingModel.Companion.get()).q();
        m.checkNotNullExpressionValue(q2, "ChatTypingModel\n        …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(q2, this, null, 2, null), WidgetChatOverlay.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatOverlay$onViewBoundOrOnResume$4(this));
        Observable j = Observable.j(StoreStream.Companion.getAutocomplete().observeAutocompleteVisibility().q(), getFlexInputViewModel().observeState().q(), WidgetChatOverlay$onViewBoundOrOnResume$5.INSTANCE);
        m.checkNotNullExpressionValue(j, "Observable.combineLatest…isible, flexInputState) }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(j, this, null, 2, null), WidgetChatOverlay.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChatOverlay$onViewBoundOrOnResume$6(this));
    }
}
