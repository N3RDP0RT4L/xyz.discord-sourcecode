package com.discord.widgets.chat.overlay;

import androidx.core.app.NotificationCompat;
import com.discord.stores.StoreChat;
import com.discord.stores.StoreMessages;
import com.discord.stores.StoreStream;
import com.discord.widgets.chat.overlay.WidgetChatOverlay;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import rx.Observable;
/* compiled from: WidgetChatOverlay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a*\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005 \u0002*\u0014\u0012\u000e\b\u0001\u0012\n \u0002*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00040\u00042\u0018\u0010\u0003\u001a\u0014 \u0002*\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00010\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "kotlin.jvm.PlatformType", "selectedChannelId", "Lrx/Observable;", "Lcom/discord/widgets/chat/overlay/WidgetChatOverlay$OldMessageModel;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Long;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatOverlay$OldMessageModel$Companion$get$1<T, R> implements b<Long, Observable<? extends WidgetChatOverlay.OldMessageModel>> {
    public static final WidgetChatOverlay$OldMessageModel$Companion$get$1 INSTANCE = new WidgetChatOverlay$OldMessageModel$Companion$get$1();

    public final Observable<? extends WidgetChatOverlay.OldMessageModel> call(final Long l) {
        Observable observable;
        if (l.longValue() <= 0) {
            observable = new k(Boolean.FALSE);
        } else {
            StoreStream.Companion companion = StoreStream.Companion;
            StoreMessages messages = companion.getMessages();
            m.checkNotNullExpressionValue(l, "selectedChannelId");
            observable = Observable.j(messages.observeIsDetached(l.longValue()), companion.getChat().observeInteractionState().x(new b<StoreChat.InteractionState, Boolean>() { // from class: com.discord.widgets.chat.overlay.WidgetChatOverlay$OldMessageModel$Companion$get$1$isViewingOldMessagesObs$1
                public final Boolean call(StoreChat.InteractionState interactionState) {
                    long channelId = interactionState.getChannelId();
                    Long l2 = l;
                    return Boolean.valueOf((l2 == null || channelId != l2.longValue() || interactionState.getLastMessageId() == 0) ? false : true);
                }
            }).o(200L, TimeUnit.MILLISECONDS), WidgetChatOverlay$OldMessageModel$Companion$get$1$isViewingOldMessagesObs$2.INSTANCE);
        }
        return (Observable<R>) Observable.m(new k(Boolean.FALSE), observable).q().F(new b<Boolean, WidgetChatOverlay.OldMessageModel>() { // from class: com.discord.widgets.chat.overlay.WidgetChatOverlay$OldMessageModel$Companion$get$1.1
            public final WidgetChatOverlay.OldMessageModel call(Boolean bool) {
                Long l2 = l;
                m.checkNotNullExpressionValue(l2, "selectedChannelId");
                long longValue = l2.longValue();
                m.checkNotNullExpressionValue(bool, "isViewingOldMessages");
                return new WidgetChatOverlay.OldMessageModel(longValue, bool.booleanValue());
            }
        });
    }
}
