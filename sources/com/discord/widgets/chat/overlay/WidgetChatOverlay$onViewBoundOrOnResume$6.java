package com.discord.widgets.chat.overlay;

import com.lytefast.flexinput.viewmodel.FlexInputState;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChatOverlay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lkotlin/Pair;", "", "Lcom/lytefast/flexinput/viewmodel/FlexInputState;", "p1", "", "invoke", "(Lkotlin/Pair;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetChatOverlay$onViewBoundOrOnResume$6 extends k implements Function1<Pair<? extends Boolean, ? extends FlexInputState>, Unit> {
    public WidgetChatOverlay$onViewBoundOrOnResume$6(WidgetChatOverlay widgetChatOverlay) {
        super(1, widgetChatOverlay, WidgetChatOverlay.class, "configureStickerSuggestions", "configureStickerSuggestions(Lkotlin/Pair;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends Boolean, ? extends FlexInputState> pair) {
        invoke2((Pair<Boolean, FlexInputState>) pair);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Pair<Boolean, FlexInputState> pair) {
        m.checkNotNullParameter(pair, "p1");
        ((WidgetChatOverlay) this.receiver).configureStickerSuggestions(pair);
    }
}
