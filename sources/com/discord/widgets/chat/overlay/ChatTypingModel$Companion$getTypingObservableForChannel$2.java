package com.discord.widgets.chat.overlay;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.stores.StoreSlowMode;
import com.discord.stores.StoreStream;
import com.discord.utilities.guilds.GuildVerificationLevelUtils;
import com.discord.widgets.chat.overlay.ChatTypingModel;
import d0.z.d.m;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Func2;
/* compiled from: ChatTypingModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channel", "Lrx/Observable;", "Lcom/discord/widgets/chat/overlay/ChatTypingModel;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ChatTypingModel$Companion$getTypingObservableForChannel$2<T, R> implements b<Channel, Observable<? extends ChatTypingModel>> {
    public static final ChatTypingModel$Companion$getTypingObservableForChannel$2 INSTANCE = new ChatTypingModel$Companion$getTypingObservableForChannel$2();

    public final Observable<? extends ChatTypingModel> call(final Channel channel) {
        if (channel == null) {
            return new k(ChatTypingModel.Hide.INSTANCE);
        }
        return GuildVerificationLevelUtils.observeVerificationLevelTriggered$default(GuildVerificationLevelUtils.INSTANCE, channel.f(), null, null, null, 14, null).Y(new b<GuildVerificationLevel, Observable<? extends ChatTypingModel>>() { // from class: com.discord.widgets.chat.overlay.ChatTypingModel$Companion$getTypingObservableForChannel$2.1
            public final Observable<? extends ChatTypingModel> call(GuildVerificationLevel guildVerificationLevel) {
                Observable typingUsers;
                if (guildVerificationLevel.compareTo(GuildVerificationLevel.NONE) > 0) {
                    return new k(ChatTypingModel.Hide.INSTANCE);
                }
                typingUsers = ChatTypingModel.Companion.getTypingUsers(Channel.this);
                return Observable.j(typingUsers, StoreStream.Companion.getSlowMode().observeCooldownSecs(Long.valueOf(Channel.this.h()), StoreSlowMode.Type.MessageSend.INSTANCE), new Func2<List<? extends CharSequence>, Integer, ChatTypingModel.Typing>() { // from class: com.discord.widgets.chat.overlay.ChatTypingModel.Companion.getTypingObservableForChannel.2.1.1
                    public final ChatTypingModel.Typing call(List<? extends CharSequence> list, Integer num) {
                        m.checkNotNullExpressionValue(list, "typingUsers");
                        int u = Channel.this.u();
                        m.checkNotNullExpressionValue(num, "cooldownSecs");
                        return new ChatTypingModel.Typing(list, u, num.intValue());
                    }
                });
            }
        });
    }
}
