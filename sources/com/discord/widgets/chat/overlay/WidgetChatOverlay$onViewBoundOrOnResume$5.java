package com.discord.widgets.chat.overlay;

import androidx.core.app.NotificationCompat;
import com.lytefast.flexinput.viewmodel.FlexInputState;
import kotlin.Metadata;
import kotlin.Pair;
import rx.functions.Func2;
/* compiled from: WidgetChatOverlay.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001aB\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00030\u0003 \u0001* \u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00000\u0000\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00050\u00052\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "kotlin.jvm.PlatformType", "autocompleteVisible", "Lcom/lytefast/flexinput/viewmodel/FlexInputState;", "flexInputState", "Lkotlin/Pair;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;Lcom/lytefast/flexinput/viewmodel/FlexInputState;)Lkotlin/Pair;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChatOverlay$onViewBoundOrOnResume$5<T1, T2, R> implements Func2<Boolean, FlexInputState, Pair<? extends Boolean, ? extends FlexInputState>> {
    public static final WidgetChatOverlay$onViewBoundOrOnResume$5 INSTANCE = new WidgetChatOverlay$onViewBoundOrOnResume$5();

    public final Pair<Boolean, FlexInputState> call(Boolean bool, FlexInputState flexInputState) {
        return new Pair<>(bool, flexInputState);
    }
}
