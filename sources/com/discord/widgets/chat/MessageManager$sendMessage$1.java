package com.discord.widgets.chat;

import androidx.core.app.NotificationCompat;
import com.discord.models.guild.Guild;
import com.discord.utilities.messagesend.MessageResult;
import com.discord.widgets.chat.MessageManager;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: MessageManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\n \u0001*\u0004\u0018\u00010\u00050\u00052\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/utilities/messagesend/MessageResult;", "kotlin.jvm.PlatformType", "messageResult", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/widgets/chat/MessageManager$MessageSendResult;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/utilities/messagesend/MessageResult;Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/chat/MessageManager$MessageSendResult;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MessageManager$sendMessage$1<T1, T2, R> implements Func2<MessageResult, Guild, MessageManager.MessageSendResult> {
    public static final MessageManager$sendMessage$1 INSTANCE = new MessageManager$sendMessage$1();

    public final MessageManager.MessageSendResult call(MessageResult messageResult, Guild guild) {
        m.checkNotNullExpressionValue(messageResult, "messageResult");
        return new MessageManager.MessageSendResult(messageResult, guild);
    }
}
