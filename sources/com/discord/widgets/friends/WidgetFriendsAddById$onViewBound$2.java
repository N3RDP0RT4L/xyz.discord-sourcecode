package com.discord.widgets.friends;

import android.text.Editable;
import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetFriendsAddByIdBinding;
import com.discord.utilities.logging.Logger;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetFriendsAddById.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/text/Editable;", "editable", "", "invoke", "(Landroid/text/Editable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetFriendsAddById$onViewBound$2 extends o implements Function1<Editable, Unit> {
    public final /* synthetic */ WidgetFriendsAddById this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetFriendsAddById$onViewBound$2(WidgetFriendsAddById widgetFriendsAddById) {
        super(1);
        this.this$0 = widgetFriendsAddById;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Editable editable) {
        invoke2(editable);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Editable editable) {
        WidgetFriendsAddByIdBinding binding;
        m.checkNotNullParameter(editable, "editable");
        try {
            boolean z2 = true;
            if (editable.length() == 0) {
                this.this$0.setInputEditError(null);
            }
            binding = this.this$0.getBinding();
            MaterialButton materialButton = binding.d;
            m.checkNotNullExpressionValue(materialButton, "binding.friendsAddSend");
            if (editable.length() <= 0) {
                z2 = false;
            }
            materialButton.setEnabled(z2);
        } catch (Exception unused) {
            AppLog appLog = AppLog.g;
            StringBuilder R = a.R("Detached: ");
            R.append(this.this$0.isDetached());
            R.append(" Visible: ");
            R.append(this.this$0.isVisible());
            Logger.e$default(appLog, "Failed to update views via TextWatcher", new Exception(R.toString()), null, 4, null);
        }
    }
}
