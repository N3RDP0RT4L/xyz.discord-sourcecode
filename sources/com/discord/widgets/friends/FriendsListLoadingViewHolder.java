package com.discord.widgets.friends;

import andhook.lib.HookHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetFriendsListAdapterItemLoadingBinding;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: WidgetFriendsList.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/friends/FriendsListLoadingViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/databinding/WidgetFriendsListAdapterItemLoadingBinding;", "binding", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetFriendsListAdapterItemLoadingBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FriendsListLoadingViewHolder extends RecyclerView.ViewHolder {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FriendsListLoadingViewHolder(WidgetFriendsListAdapterItemLoadingBinding widgetFriendsListAdapterItemLoadingBinding) {
        super(widgetFriendsListAdapterItemLoadingBinding.a);
        m.checkNotNullParameter(widgetFriendsListAdapterItemLoadingBinding, "binding");
    }
}
