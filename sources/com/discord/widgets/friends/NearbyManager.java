package com.discord.widgets.friends;

import andhook.lib.HookHelper;
import android.os.IBinder;
import androidx.fragment.app.FragmentActivity;
import b.c.a.a0.d;
import b.d.b.a.a;
import b.i.a.f.e.h.j.k;
import b.i.a.f.h.m.j;
import b.i.a.f.h.m.o;
import b.i.a.f.j.b.a;
import b.i.a.f.j.b.b;
import b.i.a.f.j.b.c;
import b.i.a.f.j.b.e.a0;
import b.i.a.f.j.b.e.i;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.widgets.friends.NearbyManager;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageFilter;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.MessagesClient;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeOptions;
import com.google.android.gms.nearby.messages.internal.zzcb;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
/* compiled from: NearbyManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 62\u00020\u0001:\u000267B\u0007¢\u0006\u0004\b5\u0010\rJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\n\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\f\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0010\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0012\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0002¢\u0006\u0004\b\u0012\u0010\u000bJ\u001b\u0010\u0013\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\bH\u0002¢\u0006\u0004\b\u0013\u0010\u000bJ\u0019\u0010\u0014\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u0014\u0010\u000bJ\u0015\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0006J\r\u0010\u0016\u001a\u00020\u0004¢\u0006\u0004\b\u0016\u0010\rJ\r\u0010\u0017\u001a\u00020\u0004¢\u0006\u0004\b\u0017\u0010\rR\u001e\u0010\u0018\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a8F@\u0006¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001dR.\u0010!\u001a\u001a\u0012\b\u0012\u00060\u0007j\u0002`\b0\u001fj\f\u0012\b\u0012\u00060\u0007j\u0002`\b` 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0018\u0010$\u001a\u0004\u0018\u00010#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u0018\u0010&\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\u0018\u0010)\u001a\u0004\u0018\u00010(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b)\u0010*R\u0018\u0010,\u001a\u0004\u0018\u00010+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-R\u0018\u0010/\u001a\u0004\u0018\u00010.8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100R:\u00103\u001a&\u0012\f\u0012\n 2*\u0004\u0018\u00010\u001b0\u001b 2*\u0012\u0012\f\u0012\n 2*\u0004\u0018\u00010\u001b0\u001b\u0018\u000101018\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104¨\u00068"}, d2 = {"Lcom/discord/widgets/friends/NearbyManager;", "", "Landroidx/fragment/app/FragmentActivity;", "fragmentActivity", "", "buildClient", "(Landroidx/fragment/app/FragmentActivity;)V", "", "Lcom/discord/primitives/UserId;", "userId", "setupBroadcaster", "(J)V", "setupListener", "()V", "Lcom/google/android/gms/nearby/messages/Message;", "message", "parseUserId", "(Lcom/google/android/gms/nearby/messages/Message;)Ljava/lang/Long;", "foundUserId", "lostUserId", "initialize", "buildClientAndPublish", "disableNearby", "activateNearby", "meUserId", "Ljava/lang/Long;", "Lrx/Observable;", "Lcom/discord/widgets/friends/NearbyManager$NearbyState;", "getState", "()Lrx/Observable;", "state", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "nearbyUserIds", "Ljava/util/HashSet;", "Lcom/google/android/gms/nearby/messages/SubscribeOptions;", "subscribeOptions", "Lcom/google/android/gms/nearby/messages/SubscribeOptions;", "outboundMessage", "Lcom/google/android/gms/nearby/messages/Message;", "Lcom/google/android/gms/nearby/messages/PublishOptions;", "messagePublishOptions", "Lcom/google/android/gms/nearby/messages/PublishOptions;", "Lcom/google/android/gms/nearby/messages/MessagesClient;", "messagesClient", "Lcom/google/android/gms/nearby/messages/MessagesClient;", "Lcom/google/android/gms/nearby/messages/MessageListener;", "messageListener", "Lcom/google/android/gms/nearby/messages/MessageListener;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "nearbyStateSubject", "Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "Companion", "NearbyState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NearbyManager {
    public static final int CONNECTION_ERROR = 99;
    public static final Companion Companion = new Companion(null);
    public static final int PERMISSION_DENIED = 98;
    private Long meUserId;
    private MessageListener messageListener;
    private PublishOptions messagePublishOptions;
    private MessagesClient messagesClient;
    private Message outboundMessage;
    private SubscribeOptions subscribeOptions;
    private final HashSet<Long> nearbyUserIds = new HashSet<>();
    private final BehaviorSubject<NearbyState> nearbyStateSubject = BehaviorSubject.l0(NearbyState.Uninitialized.INSTANCE);

    /* compiled from: NearbyManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/friends/NearbyManager$Companion;", "", "", "CONNECTION_ERROR", "I", "PERMISSION_DENIED", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: NearbyManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/friends/NearbyManager$NearbyState;", "", HookHelper.constructorName, "()V", "Connected", "Disconnected", "Uninitialized", "Lcom/discord/widgets/friends/NearbyManager$NearbyState$Uninitialized;", "Lcom/discord/widgets/friends/NearbyManager$NearbyState$Disconnected;", "Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class NearbyState {

        /* compiled from: NearbyManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0010\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J$\u0010\b\u001a\u00020\u00002\u0012\b\u0002\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R#\u0010\u0007\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0015\u001a\u0004\b\u0016\u0010\u0006¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;", "Lcom/discord/widgets/friends/NearbyManager$NearbyState;", "", "", "Lcom/discord/primitives/UserId;", "component1", "()Ljava/util/Set;", "nearbyUserIds", "copy", "(Ljava/util/Set;)Lcom/discord/widgets/friends/NearbyManager$NearbyState$Connected;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getNearbyUserIds", HookHelper.constructorName, "(Ljava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Connected extends NearbyState {
            private final Set<Long> nearbyUserIds;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Connected(Set<Long> set) {
                super(null);
                m.checkNotNullParameter(set, "nearbyUserIds");
                this.nearbyUserIds = set;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Connected copy$default(Connected connected, Set set, int i, Object obj) {
                if ((i & 1) != 0) {
                    set = connected.nearbyUserIds;
                }
                return connected.copy(set);
            }

            public final Set<Long> component1() {
                return this.nearbyUserIds;
            }

            public final Connected copy(Set<Long> set) {
                m.checkNotNullParameter(set, "nearbyUserIds");
                return new Connected(set);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Connected) && m.areEqual(this.nearbyUserIds, ((Connected) obj).nearbyUserIds);
                }
                return true;
            }

            public final Set<Long> getNearbyUserIds() {
                return this.nearbyUserIds;
            }

            public int hashCode() {
                Set<Long> set = this.nearbyUserIds;
                if (set != null) {
                    return set.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Connected(nearbyUserIds=");
                R.append(this.nearbyUserIds);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: NearbyManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/friends/NearbyManager$NearbyState$Disconnected;", "Lcom/discord/widgets/friends/NearbyManager$NearbyState;", "", "component1", "()I", ModelAuditLogEntry.CHANGE_KEY_CODE, "copy", "(I)Lcom/discord/widgets/friends/NearbyManager$NearbyState$Disconnected;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getCode", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Disconnected extends NearbyState {
            private final int code;

            public Disconnected(int i) {
                super(null);
                this.code = i;
            }

            public static /* synthetic */ Disconnected copy$default(Disconnected disconnected, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = disconnected.code;
                }
                return disconnected.copy(i);
            }

            public final int component1() {
                return this.code;
            }

            public final Disconnected copy(int i) {
                return new Disconnected(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Disconnected) && this.code == ((Disconnected) obj).code;
                }
                return true;
            }

            public final int getCode() {
                return this.code;
            }

            public int hashCode() {
                return this.code;
            }

            public String toString() {
                return a.A(a.R("Disconnected(code="), this.code, ")");
            }
        }

        /* compiled from: NearbyManager.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/friends/NearbyManager$NearbyState$Uninitialized;", "Lcom/discord/widgets/friends/NearbyManager$NearbyState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends NearbyState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private NearbyState() {
        }

        public /* synthetic */ NearbyState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    private final void buildClient(FragmentActivity fragmentActivity) {
        b.i.a.f.j.b.a aVar = new b.i.a.f.j.b.a(new a.C0118a(), null);
        d.z(fragmentActivity, "Activity must not be null");
        d.z(aVar, "Options must not be null");
        i iVar = new i(fragmentActivity, aVar);
        final k m = iVar.m(new c() { // from class: com.discord.widgets.friends.NearbyManager$buildClient$$inlined$also$lambda$1
            @Override // b.i.a.f.j.b.c
            public void onPermissionChanged(boolean z2) {
                BehaviorSubject behaviorSubject;
                HashSet hashSet;
                BehaviorSubject behaviorSubject2;
                super.onPermissionChanged(z2);
                if (!z2) {
                    behaviorSubject2 = NearbyManager.this.nearbyStateSubject;
                    behaviorSubject2.onNext(new NearbyManager.NearbyState.Disconnected(98));
                    return;
                }
                AnalyticsTracker.nearbyConnected();
                behaviorSubject = NearbyManager.this.nearbyStateSubject;
                hashSet = NearbyManager.this.nearbyUserIds;
                behaviorSubject.onNext(new NearbyManager.NearbyState.Connected(new HashSet(hashSet)));
            }
        });
        iVar.k(m, new a0(m) { // from class: b.i.a.f.j.b.e.n
            public final k a;

            {
                this.a = m;
            }

            @Override // b.i.a.f.j.b.e.a0
            public final void a(f fVar, k kVar) {
                k kVar2 = this.a;
                if (!fVar.A.a(kVar2.c)) {
                    o<k.a, IBinder> oVar = fVar.A;
                    oVar.a.put(kVar2.c, new WeakReference<>(new b.i.a.f.h.m.m(kVar2)));
                }
                zzcb zzcbVar = new zzcb(1, new j(kVar), fVar.A.b(kVar2.c), false, null, null);
                zzcbVar.m = true;
                ((u0) fVar.w()).s(zzcbVar);
            }
        }, new a0(m) { // from class: b.i.a.f.j.b.e.o
            public final k a;

            {
                this.a = m;
            }

            @Override // b.i.a.f.j.b.e.a0
            public final void a(f fVar, k kVar) {
                k kVar2 = this.a;
                Objects.requireNonNull(fVar);
                j jVar = new j(kVar);
                if (!fVar.A.a(kVar2.c)) {
                    jVar.g(new Status(0, null));
                    return;
                }
                zzcb zzcbVar = new zzcb(1, jVar, fVar.A.b(kVar2.c), false, null, null);
                zzcbVar.m = false;
                ((u0) fVar.w()).s(zzcbVar);
                b.i.a.f.h.m.o<k.a, IBinder> oVar = fVar.A;
                oVar.a.remove(kVar2.c);
            }
        });
        this.messagesClient = iVar;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x000c, code lost:
        if (r4 != r0.longValue()) goto L8;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final synchronized void foundUserId(long r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.Long r0 = r3.meUserId     // Catch: java.lang.Throwable -> L2a
            if (r0 != 0) goto L6
            goto Le
        L6:
            long r0 = r0.longValue()     // Catch: java.lang.Throwable -> L2a
            int r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r2 == 0) goto L28
        Le:
            java.util.HashSet<java.lang.Long> r0 = r3.nearbyUserIds     // Catch: java.lang.Throwable -> L2a
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch: java.lang.Throwable -> L2a
            r0.add(r4)     // Catch: java.lang.Throwable -> L2a
            rx.subjects.BehaviorSubject<com.discord.widgets.friends.NearbyManager$NearbyState> r4 = r3.nearbyStateSubject     // Catch: java.lang.Throwable -> L2a
            com.discord.widgets.friends.NearbyManager$NearbyState$Connected r5 = new com.discord.widgets.friends.NearbyManager$NearbyState$Connected     // Catch: java.lang.Throwable -> L2a
            java.util.HashSet r0 = new java.util.HashSet     // Catch: java.lang.Throwable -> L2a
            java.util.HashSet<java.lang.Long> r1 = r3.nearbyUserIds     // Catch: java.lang.Throwable -> L2a
            r0.<init>(r1)     // Catch: java.lang.Throwable -> L2a
            r5.<init>(r0)     // Catch: java.lang.Throwable -> L2a
            r4.onNext(r5)     // Catch: java.lang.Throwable -> L2a
        L28:
            monitor-exit(r3)
            return
        L2a:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.friends.NearbyManager.foundUserId(long):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final synchronized void lostUserId(long j) {
        this.nearbyUserIds.remove(Long.valueOf(j));
        this.nearbyStateSubject.onNext(new NearbyState.Connected(new HashSet(this.nearbyUserIds)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Long parseUserId(Message message) {
        byte[] bArr = message.l;
        m.checkNotNullExpressionValue(bArr, "message.content");
        String str = new String(bArr, d0.g0.c.a);
        if (str.charAt(0) == 'u') {
            try {
                String substring = str.substring(2);
                m.checkNotNullExpressionValue(substring, "(this as java.lang.String).substring(startIndex)");
                return Long.valueOf(Long.parseLong(substring));
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    private final void setupBroadcaster(long j) {
        this.messagePublishOptions = new PublishOptions(Strategy.j, new b() { // from class: com.discord.widgets.friends.NearbyManager$setupBroadcaster$1
            @Override // b.i.a.f.j.b.b
            public void onExpired() {
                super.onExpired();
                NearbyManager.this.activateNearby();
            }
        }, null);
        String s2 = b.d.b.a.a.s("u:", j);
        Charset charset = d0.g0.c.a;
        Objects.requireNonNull(s2, "null cannot be cast to non-null type java.lang.String");
        byte[] bytes = s2.getBytes(charset);
        m.checkNotNullExpressionValue(bytes, "(this as java.lang.String).getBytes(charset)");
        this.outboundMessage = new Message(2, bytes, "", "", Message.j, 0L);
    }

    private final void setupListener() {
        this.messageListener = new MessageListener() { // from class: com.discord.widgets.friends.NearbyManager$setupListener$1
            /* JADX WARN: Code restructure failed: missing block: B:4:0x0005, code lost:
                r4 = r3.this$0.parseUserId(r4);
             */
            @Override // com.google.android.gms.nearby.messages.MessageListener
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public void onFound(com.google.android.gms.nearby.messages.Message r4) {
                /*
                    r3 = this;
                    super.onFound(r4)
                    if (r4 == 0) goto L16
                    com.discord.widgets.friends.NearbyManager r0 = com.discord.widgets.friends.NearbyManager.this
                    java.lang.Long r4 = com.discord.widgets.friends.NearbyManager.access$parseUserId(r0, r4)
                    if (r4 == 0) goto L16
                    com.discord.widgets.friends.NearbyManager r0 = com.discord.widgets.friends.NearbyManager.this
                    long r1 = r4.longValue()
                    com.discord.widgets.friends.NearbyManager.access$foundUserId(r0, r1)
                L16:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.friends.NearbyManager$setupListener$1.onFound(com.google.android.gms.nearby.messages.Message):void");
            }

            /* JADX WARN: Code restructure failed: missing block: B:4:0x0005, code lost:
                r4 = r3.this$0.parseUserId(r4);
             */
            @Override // com.google.android.gms.nearby.messages.MessageListener
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public void onLost(com.google.android.gms.nearby.messages.Message r4) {
                /*
                    r3 = this;
                    super.onLost(r4)
                    if (r4 == 0) goto L16
                    com.discord.widgets.friends.NearbyManager r0 = com.discord.widgets.friends.NearbyManager.this
                    java.lang.Long r4 = com.discord.widgets.friends.NearbyManager.access$parseUserId(r0, r4)
                    if (r4 == 0) goto L16
                    com.discord.widgets.friends.NearbyManager r0 = com.discord.widgets.friends.NearbyManager.this
                    long r1 = r4.longValue()
                    com.discord.widgets.friends.NearbyManager.access$lostUserId(r0, r1)
                L16:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.friends.NearbyManager$setupListener$1.onLost(com.google.android.gms.nearby.messages.Message):void");
            }
        };
        Strategy strategy = Strategy.j;
        this.subscribeOptions = new SubscribeOptions(Strategy.k, MessageFilter.j, null);
    }

    public final void activateNearby() {
        Message message;
        PublishOptions publishOptions;
        MessageListener messageListener;
        SubscribeOptions subscribeOptions;
        MessagesClient messagesClient = this.messagesClient;
        if (messagesClient != null && (message = this.outboundMessage) != null && (publishOptions = this.messagePublishOptions) != null && (messageListener = this.messageListener) != null && (subscribeOptions = this.subscribeOptions) != null) {
            this.nearbyStateSubject.onNext(new NearbyState.Connected(new HashSet(this.nearbyUserIds)));
            messagesClient.f(message, publishOptions);
            messagesClient.g(messageListener, subscribeOptions);
        }
    }

    public final void buildClientAndPublish(FragmentActivity fragmentActivity) {
        m.checkNotNullParameter(fragmentActivity, "fragmentActivity");
        if (this.messagesClient == null) {
            buildClient(fragmentActivity);
        }
        activateNearby();
    }

    public final void disableNearby() {
        Message message;
        MessageListener messageListener;
        MessagesClient messagesClient = this.messagesClient;
        if (messagesClient != null && (message = this.outboundMessage) != null && (messageListener = this.messageListener) != null) {
            messagesClient.h(message);
            messagesClient.i(messageListener);
            this.nearbyUserIds.clear();
            this.nearbyStateSubject.onNext(NearbyState.Uninitialized.INSTANCE);
        }
    }

    public final Observable<NearbyState> getState() {
        BehaviorSubject<NearbyState> behaviorSubject = this.nearbyStateSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "nearbyStateSubject");
        return behaviorSubject;
    }

    public final void initialize(long j) {
        this.meUserId = Long.valueOf(j);
        setupBroadcaster(j);
        setupListener();
    }
}
