package com.discord.widgets.friends;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.o;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetFriendsNearbyBinding;
import com.discord.rlottie.RLottieDrawable;
import com.discord.rlottie.RLottieImageView;
import com.discord.stores.StoreStream;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.captcha.CaptchaErrorBody;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.error.Error;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rest.RestAPIAbortMessages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.captcha.WidgetCaptcha;
import com.discord.widgets.captcha.WidgetCaptchaKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.friends.NearbyManager;
import com.discord.widgets.friends.WidgetFriendsAddUserAdapter;
import com.google.android.material.button.MaterialButton;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import xyz.discord.R;
/* compiled from: WidgetFriendsFindNearby.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0002ABB\u0007¢\u0006\u0004\b@\u0010\u001cJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ+\u0010\u0010\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\r2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0015\u001a\u00020\u00042\n\u0010\u0014\u001a\u00060\u0012j\u0002`\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J#\u0010\u0019\u001a\u00020\u00042\n\u0010\u0014\u001a\u00060\u0012j\u0002`\u00132\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001d\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001d\u0010\u001cJ\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u001e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010#\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020!H\u0016¢\u0006\u0004\b#\u0010$J\u000f\u0010%\u001a\u00020\u0004H\u0016¢\u0006\u0004\b%\u0010\u001cJ\u000f\u0010&\u001a\u00020\u0004H\u0016¢\u0006\u0004\b&\u0010\u001cJ\u000f\u0010'\u001a\u00020\u0004H\u0016¢\u0006\u0004\b'\u0010\u001cR\u001d\u0010-\u001a\u00020(8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,R\u001e\u0010.\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010/R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R\u0016\u00104\u001a\u0002038\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b4\u00105R\u001c\u00108\u001a\b\u0012\u0004\u0012\u000207068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109R\u001d\u0010?\u001a\u00020:8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>¨\u0006C"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsFindNearby;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;)V", "Lcom/discord/utilities/error/Error;", "error", "launchCaptchaFlow", "(Lcom/discord/utilities/error/Error;)V", "", "username", "", "discriminator", "captchaKey", "sendFriendRequest", "(Ljava/lang/String;ILjava/lang/String;)V", "", "Lcom/discord/primitives/UserId;", "userId", "acceptFriendRequest", "(J)V", "", "incomingFriendRequest", "declineFriendRequest", "(JZ)V", "enableScanning", "()V", "updateMeUserIdAndInitNearbyManager", "resultCode", "getErrorMessage", "(Ljava/lang/Integer;)Ljava/lang/String;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onResume", "onPause", "Lcom/discord/widgets/friends/WidgetFriendsFindNearbyViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/friends/WidgetFriendsFindNearbyViewModel;", "viewModel", "meUserId", "Ljava/lang/Long;", "Lcom/discord/widgets/friends/NearbyManager;", "nearbyManager", "Lcom/discord/widgets/friends/NearbyManager;", "Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;", "resultsAdapter", "Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "captchaLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/databinding/WidgetFriendsNearbyBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetFriendsNearbyBinding;", "binding", HookHelper.constructorName, ExifInterface.TAG_MODEL, "ModelProvider", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetFriendsFindNearby extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetFriendsFindNearby.class, "binding", "getBinding()Lcom/discord/databinding/WidgetFriendsNearbyBinding;", 0)};
    private Long meUserId;
    private WidgetFriendsAddUserAdapter resultsAdapter;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetFriendsFindNearby$binding$2.INSTANCE, null, 2, null);
    private final NearbyManager nearbyManager = new NearbyManager();
    private final ActivityResultLauncher<Intent> captchaLauncher = WidgetCaptcha.Companion.registerForResult(this, new WidgetFriendsFindNearby$captchaLauncher$1(this));

    /* compiled from: WidgetFriendsFindNearby.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;", "", HookHelper.constructorName, "()V", "Empty", "Error", "NearbyUsers", "Uninitialized", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$NearbyUsers;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Empty;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Uninitialized;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Model {

        /* compiled from: WidgetFriendsFindNearby.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Empty;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Empty extends Model {
            public static final Empty INSTANCE = new Empty();

            private Empty() {
                super(null);
            }
        }

        /* compiled from: WidgetFriendsFindNearby.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\rHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001b\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;", "", "component1", "()Ljava/lang/Integer;", "errorCode", "copy", "(Ljava/lang/Integer;)Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Error;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getErrorCode", HookHelper.constructorName, "(Ljava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends Model {
            private final Integer errorCode;

            public Error(Integer num) {
                super(null);
                this.errorCode = num;
            }

            public static /* synthetic */ Error copy$default(Error error, Integer num, int i, Object obj) {
                if ((i & 1) != 0) {
                    num = error.errorCode;
                }
                return error.copy(num);
            }

            public final Integer component1() {
                return this.errorCode;
            }

            public final Error copy(Integer num) {
                return new Error(num);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Error) && m.areEqual(this.errorCode, ((Error) obj).errorCode);
                }
                return true;
            }

            public final Integer getErrorCode() {
                return this.errorCode;
            }

            public int hashCode() {
                Integer num = this.errorCode;
                if (num != null) {
                    return num.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.E(a.R("Error(errorCode="), this.errorCode, ")");
            }
        }

        /* compiled from: WidgetFriendsFindNearby.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$NearbyUsers;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;", "", "Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser;", "component1", "()Ljava/util/List;", "items", "copy", "(Ljava/util/List;)Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$NearbyUsers;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class NearbyUsers extends Model {
            private final List<WidgetFriendsAddUserAdapter.ItemUser> items;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public NearbyUsers(List<WidgetFriendsAddUserAdapter.ItemUser> list) {
                super(null);
                m.checkNotNullParameter(list, "items");
                this.items = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ NearbyUsers copy$default(NearbyUsers nearbyUsers, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = nearbyUsers.items;
                }
                return nearbyUsers.copy(list);
            }

            public final List<WidgetFriendsAddUserAdapter.ItemUser> component1() {
                return this.items;
            }

            public final NearbyUsers copy(List<WidgetFriendsAddUserAdapter.ItemUser> list) {
                m.checkNotNullParameter(list, "items");
                return new NearbyUsers(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof NearbyUsers) && m.areEqual(this.items, ((NearbyUsers) obj).items);
                }
                return true;
            }

            public final List<WidgetFriendsAddUserAdapter.ItemUser> getItems() {
                return this.items;
            }

            public int hashCode() {
                List<WidgetFriendsAddUserAdapter.ItemUser> list = this.items;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("NearbyUsers(items="), this.items, ")");
            }
        }

        /* compiled from: WidgetFriendsFindNearby.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$Uninitialized;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends Model {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private Model() {
        }

        public /* synthetic */ Model(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetFriendsFindNearby.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bÂ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0010\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0002H\u0002¢\u0006\u0004\b\b\u0010\tJ!\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u00062\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\u0006¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsFindNearby$ModelProvider;", "", "", "", "Lcom/discord/primitives/UserId;", "userIds", "Lrx/Observable;", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model$NearbyUsers;", "getUserModels", "(Ljava/util/Collection;)Lrx/Observable;", "Lcom/discord/widgets/friends/NearbyManager$NearbyState;", "nearbyStateObservable", "Lcom/discord/widgets/friends/WidgetFriendsFindNearby$Model;", "get", "(Lrx/Observable;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ModelProvider {
        public static final ModelProvider INSTANCE = new ModelProvider();

        private ModelProvider() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Model.NearbyUsers> getUserModels(Collection<Long> collection) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<Model.NearbyUsers> i = Observable.i(companion.getUsers().observeUsers(collection, true), companion.getPresences().observePresencesForUsers(collection), companion.getUserRelationships().observe(collection), WidgetFriendsFindNearby$ModelProvider$getUserModels$1.INSTANCE);
            m.checkNotNullExpressionValue(i, "Observable\n          .co…yUsers(items)\n          }");
            return i;
        }

        public final Observable<Model> get(Observable<NearbyManager.NearbyState> observable) {
            m.checkNotNullParameter(observable, "nearbyStateObservable");
            Observable Y = observable.Y(WidgetFriendsFindNearby$ModelProvider$get$1.INSTANCE);
            m.checkNotNullExpressionValue(Y, "nearbyStateObservable\n  …          }\n            }");
            return Y;
        }
    }

    public WidgetFriendsFindNearby() {
        super(R.layout.widget_friends_nearby);
        WidgetFriendsFindNearby$viewModel$2 widgetFriendsFindNearby$viewModel$2 = WidgetFriendsFindNearby$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetFriendsFindNearbyViewModel.class), new WidgetFriendsFindNearby$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetFriendsFindNearby$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void acceptFriendRequest(long j) {
        Observable addRelationship;
        addRelationship = RestAPI.Companion.getApi().addRelationship("Nearby - Accept Friend Request", j, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null, (r16 & 16) != 0 ? null : null);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(addRelationship, false, 1, null), this, null, 2, null), WidgetFriendsFindNearby.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFriendsFindNearby$acceptFriendRequest$1(this));
    }

    public static final /* synthetic */ WidgetFriendsAddUserAdapter access$getResultsAdapter$p(WidgetFriendsFindNearby widgetFriendsFindNearby) {
        WidgetFriendsAddUserAdapter widgetFriendsAddUserAdapter = widgetFriendsFindNearby.resultsAdapter;
        if (widgetFriendsAddUserAdapter == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        return widgetFriendsAddUserAdapter;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        if (model instanceof Model.Uninitialized) {
            TextView textView = getBinding().g;
            m.checkNotNullExpressionValue(textView, "binding.nearbyFriendsSearchingTitle");
            textView.setText(getString(R.string.add_friend_nearby_title));
            TextView textView2 = getBinding().e;
            m.checkNotNullExpressionValue(textView2, "binding.nearbyFriendsSearchingBody");
            textView2.setText(getString(R.string.add_friend_nearby_body));
            TextView textView3 = getBinding().e;
            TextView textView4 = getBinding().e;
            m.checkNotNullExpressionValue(textView4, "binding.nearbyFriendsSearchingBody");
            textView3.setTextColor(ColorCompat.getThemedColor(textView4, (int) R.attr.colorHeaderSecondary));
            MaterialButton materialButton = getBinding().f2377b;
            m.checkNotNullExpressionValue(materialButton, "binding.nearbyFriendsEnable");
            materialButton.setVisibility(0);
            RecyclerView recyclerView = getBinding().d;
            m.checkNotNullExpressionValue(recyclerView, "binding.nearbyFriendsRecycler");
            recyclerView.setVisibility(8);
            RLottieImageView rLottieImageView = getBinding().f;
            RLottieDrawable rLottieDrawable = rLottieImageView.j;
            if (rLottieDrawable != null) {
                rLottieImageView.l = false;
                if (rLottieImageView.k) {
                    rLottieDrawable.P = false;
                }
            }
        } else if (model instanceof Model.Error) {
            TextView textView5 = getBinding().g;
            m.checkNotNullExpressionValue(textView5, "binding.nearbyFriendsSearchingTitle");
            textView5.setText(getString(R.string.add_friend_nearby_title));
            TextView textView6 = getBinding().e;
            m.checkNotNullExpressionValue(textView6, "binding.nearbyFriendsSearchingBody");
            textView6.setText(getErrorMessage(((Model.Error) model).getErrorCode()));
            TextView textView7 = getBinding().e;
            TextView textView8 = getBinding().e;
            m.checkNotNullExpressionValue(textView8, "binding.nearbyFriendsSearchingBody");
            textView7.setTextColor(ColorCompat.getColor(textView8, (int) R.color.status_red_500));
            MaterialButton materialButton2 = getBinding().f2377b;
            m.checkNotNullExpressionValue(materialButton2, "binding.nearbyFriendsEnable");
            materialButton2.setVisibility(0);
            RecyclerView recyclerView2 = getBinding().d;
            m.checkNotNullExpressionValue(recyclerView2, "binding.nearbyFriendsRecycler");
            recyclerView2.setVisibility(8);
            RLottieImageView rLottieImageView2 = getBinding().f;
            RLottieDrawable rLottieDrawable2 = rLottieImageView2.j;
            if (rLottieDrawable2 != null) {
                rLottieImageView2.l = false;
                if (rLottieImageView2.k) {
                    rLottieDrawable2.P = false;
                }
            }
        } else if (model instanceof Model.Empty) {
            TextView textView9 = getBinding().g;
            m.checkNotNullExpressionValue(textView9, "binding.nearbyFriendsSearchingTitle");
            textView9.setText(getString(R.string.add_friend_nearby_title));
            TextView textView10 = getBinding().e;
            m.checkNotNullExpressionValue(textView10, "binding.nearbyFriendsSearchingBody");
            textView10.setText(getString(R.string.add_friend_nearby_body));
            TextView textView11 = getBinding().e;
            TextView textView12 = getBinding().e;
            m.checkNotNullExpressionValue(textView12, "binding.nearbyFriendsSearchingBody");
            textView11.setTextColor(ColorCompat.getThemedColor(textView12, (int) R.attr.colorHeaderSecondary));
            MaterialButton materialButton3 = getBinding().f2377b;
            m.checkNotNullExpressionValue(materialButton3, "binding.nearbyFriendsEnable");
            materialButton3.setVisibility(8);
            RecyclerView recyclerView3 = getBinding().d;
            m.checkNotNullExpressionValue(recyclerView3, "binding.nearbyFriendsRecycler");
            recyclerView3.setVisibility(8);
            if (!AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
                getBinding().f.b();
            }
        } else if (model instanceof Model.NearbyUsers) {
            TextView textView13 = getBinding().g;
            m.checkNotNullExpressionValue(textView13, "binding.nearbyFriendsSearchingTitle");
            textView13.setText(getString(R.string.add_friend_nearby_found_title));
            TextView textView14 = getBinding().e;
            m.checkNotNullExpressionValue(textView14, "binding.nearbyFriendsSearchingBody");
            textView14.setText(getString(R.string.add_friend_nearby_found_body));
            TextView textView15 = getBinding().e;
            TextView textView16 = getBinding().e;
            m.checkNotNullExpressionValue(textView16, "binding.nearbyFriendsSearchingBody");
            textView15.setTextColor(ColorCompat.getThemedColor(textView16, (int) R.attr.colorHeaderSecondary));
            MaterialButton materialButton4 = getBinding().f2377b;
            m.checkNotNullExpressionValue(materialButton4, "binding.nearbyFriendsEnable");
            materialButton4.setVisibility(8);
            WidgetFriendsAddUserAdapter widgetFriendsAddUserAdapter = this.resultsAdapter;
            if (widgetFriendsAddUserAdapter == null) {
                m.throwUninitializedPropertyAccessException("resultsAdapter");
            }
            widgetFriendsAddUserAdapter.setData(((Model.NearbyUsers) model).getItems());
            RecyclerView recyclerView4 = getBinding().d;
            m.checkNotNullExpressionValue(recyclerView4, "binding.nearbyFriendsRecycler");
            recyclerView4.setVisibility(0);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void declineFriendRequest(long j, boolean z2) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().removeRelationship("Nearby - Remove Friend Request", j), false, 1, null), this, null, 2, null), WidgetFriendsFindNearby.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFriendsFindNearby$declineFriendRequest$1(this, z2 ? R.string.friend_request_ignored : R.string.friend_request_cancelled));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void enableScanning() {
        if (this.meUserId == null) {
            updateMeUserIdAndInitNearbyManager();
        }
        if (this.meUserId != null) {
            NearbyManager nearbyManager = this.nearbyManager;
            FragmentActivity activity = e();
            Objects.requireNonNull(activity, "null cannot be cast to non-null type androidx.fragment.app.FragmentActivity");
            nearbyManager.buildClientAndPublish(activity);
        }
    }

    private final WidgetFriendsNearbyBinding getBinding() {
        return (WidgetFriendsNearbyBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final String getErrorMessage(Integer num) {
        if (num != null && num.intValue() == 99) {
            Context context = getContext();
            if (context != null) {
                return context.getString(R.string.add_friend_nearby_connection_error);
            }
            return null;
        } else if (num != null && num.intValue() == 98) {
            Context context2 = getContext();
            if (context2 != null) {
                return context2.getString(R.string.add_friend_nearby_stopped);
            }
            return null;
        } else {
            Context context3 = getContext();
            if (context3 != null) {
                return context3.getString(R.string.add_friend_nearby_generic_error);
            }
            return null;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetFriendsFindNearbyViewModel getViewModel() {
        return (WidgetFriendsFindNearbyViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchCaptchaFlow(Error error) {
        WidgetCaptcha.Companion.launch(requireContext(), this.captchaLauncher, CaptchaErrorBody.Companion.createFromError(error));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void sendFriendRequest(final String str, final int i, String str2) {
        getViewModel().setUsername(str);
        getViewModel().setDiscriminator(Integer.valueOf(i));
        ObservableExtensionsKt.ui$default(RestAPI.Companion.getApi().sendRelationshipRequest("Nearby - Add Friend Suggestion", str, i, str2), this, null, 2, null).k(o.h(new Action1<Void>() { // from class: com.discord.widgets.friends.WidgetFriendsFindNearby$sendFriendRequest$1
            public final void call(Void r7) {
                CharSequence charSequence;
                Context context = WidgetFriendsFindNearby.this.getContext();
                Context context2 = WidgetFriendsFindNearby.this.getContext();
                if (context2 != null) {
                    charSequence = b.b(context2, R.string.add_friend_confirmation, new Object[]{str}, (r4 & 4) != 0 ? b.C0034b.j : null);
                } else {
                    charSequence = null;
                }
                b.a.d.m.h(context, charSequence, 0, null, 12);
            }
        }, getAppActivity(), new Action1<Error>() { // from class: com.discord.widgets.friends.WidgetFriendsFindNearby$sendFriendRequest$2

            /* compiled from: WidgetFriendsFindNearby.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.friends.WidgetFriendsFindNearby$sendFriendRequest$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends d0.z.d.o implements Function0<Unit> {
                public final /* synthetic */ Error $error;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass1(Error error) {
                    super(0);
                    this.$error = error;
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    Error error = this.$error;
                    m.checkNotNullExpressionValue(error, "error");
                    if (WidgetCaptchaKt.isCaptchaError(error)) {
                        WidgetFriendsFindNearby widgetFriendsFindNearby = WidgetFriendsFindNearby.this;
                        Error error2 = this.$error;
                        m.checkNotNullExpressionValue(error2, "error");
                        widgetFriendsFindNearby.launchCaptchaFlow(error2);
                        return;
                    }
                    RestAPIAbortMessages.ResponseResolver responseResolver = RestAPIAbortMessages.ResponseResolver.INSTANCE;
                    Context context = WidgetFriendsFindNearby.this.getContext();
                    Error error3 = this.$error;
                    m.checkNotNullExpressionValue(error3, "error");
                    Error.Response response = error3.getResponse();
                    m.checkNotNullExpressionValue(response, "error.response");
                    int code = response.getCode();
                    b.a.d.m.h(WidgetFriendsFindNearby.this.getContext(), responseResolver.getRelationshipResponse(context, code, str + MentionUtilsKt.CHANNELS_CHAR + UserUtils.INSTANCE.padDiscriminator(i)), 0, null, 12);
                }
            }

            public final void call(Error error) {
                RestAPIAbortMessages restAPIAbortMessages = RestAPIAbortMessages.INSTANCE;
                m.checkNotNullExpressionValue(error, "error");
                RestAPIAbortMessages.handleAbortCodeOrDefault$default(restAPIAbortMessages, error, new AnonymousClass1(error), null, 4, null);
            }
        }));
    }

    public static /* synthetic */ void sendFriendRequest$default(WidgetFriendsFindNearby widgetFriendsFindNearby, String str, int i, String str2, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            str2 = null;
        }
        widgetFriendsFindNearby.sendFriendRequest(str, i, str2);
    }

    private final void updateMeUserIdAndInitNearbyManager() {
        long id2 = StoreStream.Companion.getUsers().getMe().getId();
        this.meUserId = Long.valueOf(id2);
        this.nearbyManager.initialize(id2);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        this.nearbyManager.disableNearby();
        super.onPause();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        enableScanning();
        AnalyticsTracker.INSTANCE.friendAddViewed("Nearby");
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        updateMeUserIdAndInitNearbyManager();
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().d;
        m.checkNotNullExpressionValue(recyclerView, "binding.nearbyFriendsRecycler");
        WidgetFriendsAddUserAdapter widgetFriendsAddUserAdapter = (WidgetFriendsAddUserAdapter) companion.configure(new WidgetFriendsAddUserAdapter(recyclerView));
        this.resultsAdapter = widgetFriendsAddUserAdapter;
        if (widgetFriendsAddUserAdapter == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        widgetFriendsAddUserAdapter.setSendHandler(new WidgetFriendsFindNearby$onViewBound$1(this));
        WidgetFriendsAddUserAdapter widgetFriendsAddUserAdapter2 = this.resultsAdapter;
        if (widgetFriendsAddUserAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        widgetFriendsAddUserAdapter2.setAcceptHandler(new WidgetFriendsFindNearby$onViewBound$2(this));
        WidgetFriendsAddUserAdapter widgetFriendsAddUserAdapter3 = this.resultsAdapter;
        if (widgetFriendsAddUserAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        widgetFriendsAddUserAdapter3.setDeclineHandler(new WidgetFriendsFindNearby$onViewBound$3(this));
        WidgetFriendsAddUserAdapter widgetFriendsAddUserAdapter4 = this.resultsAdapter;
        if (widgetFriendsAddUserAdapter4 == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        widgetFriendsAddUserAdapter4.setOnItemClick(new WidgetFriendsFindNearby$onViewBound$4(this));
        getBinding().f2377b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsFindNearby$onViewBound$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetFriendsFindNearby.this.enableScanning();
            }
        });
        getBinding().c.setOnClickListener(WidgetFriendsFindNearby$onViewBound$6.INSTANCE);
        getBinding().f.c(R.raw.anim_friends_add_nearby_looking, DimenUtils.dpToPixels(200), DimenUtils.dpToPixels(200));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<R> F = companion.getUserRelationships().observeForType(4).F(j.j);
        Observable<R> F2 = companion.getUserRelationships().observeForType(3).F(j.k);
        v vVar = v.j;
        Object obj = vVar;
        if (vVar != null) {
            obj = new w(vVar);
        }
        Observable j = Observable.j(F, F2, (Func2) obj);
        m.checkNotNullExpressionValue(j, "Observable\n        .comb…erRequestsModel\n        )");
        Observable q = ObservableExtensionsKt.computationLatest(j).q();
        m.checkNotNullExpressionValue(q, "Observable\n        .comb…  .distinctUntilChanged()");
        ObservableExtensionsKt.ui$default(q, this, null, 2, null).k(o.a.g(getContext(), new WidgetFriendsFindNearby$onViewBoundOrOnResume$1(this), null));
        Observable<Model> observable = ModelProvider.INSTANCE.get(this.nearbyManager.getState());
        WidgetFriendsAddUserAdapter widgetFriendsAddUserAdapter = this.resultsAdapter;
        if (widgetFriendsAddUserAdapter == null) {
            m.throwUninitializedPropertyAccessException("resultsAdapter");
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(observable, this, widgetFriendsAddUserAdapter), WidgetFriendsFindNearby.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFriendsFindNearby$onViewBoundOrOnResume$2(this));
    }
}
