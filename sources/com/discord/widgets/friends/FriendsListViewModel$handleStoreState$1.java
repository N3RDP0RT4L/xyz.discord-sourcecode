package com.discord.widgets.friends;

import com.discord.widgets.friends.FriendsListViewModel;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: FriendsListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;", "invoke", "()Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FriendsListViewModel$handleStoreState$1 extends o implements Function0<FriendsListViewModel.ListSections> {
    public final /* synthetic */ Map $relationships;
    public final /* synthetic */ FriendsListViewModel.StoreState $storeState;
    public final /* synthetic */ FriendsListViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FriendsListViewModel$handleStoreState$1(FriendsListViewModel friendsListViewModel, Map map, FriendsListViewModel.StoreState storeState) {
        super(0);
        this.this$0 = friendsListViewModel;
        this.$relationships = map;
        this.$storeState = storeState;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final FriendsListViewModel.ListSections invoke() {
        FriendsListViewModel.ListSections items;
        items = this.this$0.getItems(this.$relationships, this.$storeState.getUsers(), this.$storeState.getPresences(), this.$storeState.getApplicationStreams(), this.$storeState.getShowContactSyncUpsell(), this.$storeState.getFriendSuggestions());
        return items;
    }
}
