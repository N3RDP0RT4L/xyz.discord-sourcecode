package com.discord.widgets.friends;

import d0.t.n0;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: FriendsListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/RelationshipType;", "relationshipType", "", "invoke", "(I)Z", "isPendingInvite"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FriendsListViewModel$getItems$3 extends o implements Function1<Integer, Boolean> {
    public static final FriendsListViewModel$getItems$3 INSTANCE = new FriendsListViewModel$getItems$3();

    public FriendsListViewModel$getItems$3() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(Integer num) {
        return Boolean.valueOf(invoke(num.intValue()));
    }

    public final boolean invoke(int i) {
        return n0.setOf((Object[]) new Integer[]{3, 4}).contains(Integer.valueOf(i));
    }
}
