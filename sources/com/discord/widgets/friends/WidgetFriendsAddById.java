package com.discord.widgets.friends;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import b.a.d.o;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetFriendsAddByIdBinding;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.captcha.CaptchaErrorBody;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rest.RestAPIAbortMessages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.captcha.WidgetCaptcha;
import com.discord.widgets.captcha.WidgetCaptchaKt;
import com.google.android.material.textfield.TextInputLayout;
import d0.g0.s;
import d0.g0.t;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetFriendsAddById.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 -2\u00020\u0001:\u0001-B\u0007¢\u0006\u0004\b,\u0010\u001fJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001b\u0010\b\u001a\u00020\u00042\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ+\u0010\u0010\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000e2\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0019\u0010\u0013\u001a\u00020\u00042\b\u0010\n\u001a\u0004\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010 \u001a\u00020\u0004H\u0016¢\u0006\u0004\b \u0010\u001fJ\u0019\u0010!\u001a\u00020\u00042\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aH\u0016¢\u0006\u0004\b!\u0010\u001dR\u001d\u0010'\u001a\u00020\"8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u001c\u0010*\u001a\b\u0012\u0004\u0012\u00020)0(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+¨\u0006."}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsAddById;", "Lcom/discord/app/AppFragment;", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "", "setInputText", "(Ljava/lang/String;)V", "captchaKey", "extractTargetAndSendFriendRequest", "Lcom/discord/utilities/error/Error;", "error", "launchCaptchaFlow", "(Lcom/discord/utilities/error/Error;)V", "username", "", "discriminator", "sendFriendRequest", "(Ljava/lang/String;ILjava/lang/String;)V", "", "setInputEditError", "(Ljava/lang/CharSequence;)V", "Lcom/discord/models/user/User;", "user", "Landroid/text/SpannableStringBuilder;", "getUsernameIndicatorText", "(Lcom/discord/models/user/User;)Landroid/text/SpannableStringBuilder;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onResume", "()V", "onPause", "hideKeyboard", "Lcom/discord/databinding/WidgetFriendsAddByIdBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetFriendsAddByIdBinding;", "binding", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "captchaLauncher", "Landroidx/activity/result/ActivityResultLauncher;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetFriendsAddById extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetFriendsAddById.class, "binding", "getBinding()Lcom/discord/databinding/WidgetFriendsAddByIdBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final Regex PATTERN_USERNAME = new Regex("^(.*)#(\\d{4})$");
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetFriendsAddById$binding$2.INSTANCE, null, 2, null);
    private final ActivityResultLauncher<Intent> captchaLauncher = WidgetCaptcha.Companion.registerForResult(this, new WidgetFriendsAddById$captchaLauncher$1(this));

    /* compiled from: WidgetFriendsAddById.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\fB\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion;", "", "", "source", "Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;", "extractUsernameAndDiscriminator", "(Ljava/lang/CharSequence;)Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;", "Lkotlin/text/Regex;", "PATTERN_USERNAME", "Lkotlin/text/Regex;", HookHelper.constructorName, "()V", "UserNameDiscriminator", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: WidgetFriendsAddById.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\r\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0007¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()Ljava/lang/Integer;", "username", "discriminator", "copy", "(Ljava/lang/String;Ljava/lang/Integer;)Lcom/discord/widgets/friends/WidgetFriendsAddById$Companion$UserNameDiscriminator;", "toString", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getUsername", "Ljava/lang/Integer;", "getDiscriminator", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class UserNameDiscriminator {
            private final Integer discriminator;
            private final String username;

            public UserNameDiscriminator(String str, Integer num) {
                m.checkNotNullParameter(str, "username");
                this.username = str;
                this.discriminator = num;
            }

            public static /* synthetic */ UserNameDiscriminator copy$default(UserNameDiscriminator userNameDiscriminator, String str, Integer num, int i, Object obj) {
                if ((i & 1) != 0) {
                    str = userNameDiscriminator.username;
                }
                if ((i & 2) != 0) {
                    num = userNameDiscriminator.discriminator;
                }
                return userNameDiscriminator.copy(str, num);
            }

            public final String component1() {
                return this.username;
            }

            public final Integer component2() {
                return this.discriminator;
            }

            public final UserNameDiscriminator copy(String str, Integer num) {
                m.checkNotNullParameter(str, "username");
                return new UserNameDiscriminator(str, num);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof UserNameDiscriminator)) {
                    return false;
                }
                UserNameDiscriminator userNameDiscriminator = (UserNameDiscriminator) obj;
                return m.areEqual(this.username, userNameDiscriminator.username) && m.areEqual(this.discriminator, userNameDiscriminator.discriminator);
            }

            public final Integer getDiscriminator() {
                return this.discriminator;
            }

            public final String getUsername() {
                return this.username;
            }

            public int hashCode() {
                String str = this.username;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                Integer num = this.discriminator;
                if (num != null) {
                    i = num.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("UserNameDiscriminator(username=");
                R.append(this.username);
                R.append(", discriminator=");
                return a.E(R, this.discriminator, ")");
            }
        }

        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final UserNameDiscriminator extractUsernameAndDiscriminator(CharSequence charSequence) {
            List<String> groupValues;
            MatchResult matchEntire = WidgetFriendsAddById.PATTERN_USERNAME.matchEntire(charSequence);
            if (matchEntire == null || (groupValues = matchEntire.getGroupValues()) == null || !(!groupValues.isEmpty())) {
                return new UserNameDiscriminator(charSequence.toString(), null);
            }
            return new UserNameDiscriminator(matchEntire.getGroupValues().get(1), s.toIntOrNull(matchEntire.getGroupValues().get(2)));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetFriendsAddById() {
        super(R.layout.widget_friends_add_by_id);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void extractTargetAndSendFriendRequest(String str) {
        TextInputLayout textInputLayout = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout, "binding.friendsAddTextEditWrap");
        String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
        Companion.UserNameDiscriminator extractUsernameAndDiscriminator = Companion.extractUsernameAndDiscriminator(textOrEmpty);
        if (extractUsernameAndDiscriminator.getDiscriminator() != null) {
            sendFriendRequest(extractUsernameAndDiscriminator.getUsername(), extractUsernameAndDiscriminator.getDiscriminator().intValue(), str);
            return;
        }
        Context context = getContext();
        CharSequence charSequence = null;
        if (context != null) {
            charSequence = b.b(context, R.string.add_friend_error_username_only, new Object[]{extractUsernameAndDiscriminator.getUsername()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        }
        setInputEditError(charSequence);
        AnalyticsTracker.INSTANCE.friendRequestFailed(textOrEmpty, extractUsernameAndDiscriminator.getUsername(), extractUsernameAndDiscriminator.getDiscriminator(), "Invalid Username");
    }

    public static /* synthetic */ void extractTargetAndSendFriendRequest$default(WidgetFriendsAddById widgetFriendsAddById, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        widgetFriendsAddById.extractTargetAndSendFriendRequest(str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetFriendsAddByIdBinding getBinding() {
        return (WidgetFriendsAddByIdBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final SpannableStringBuilder getUsernameIndicatorText(User user) {
        CharSequence c;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        UserUtils userUtils = UserUtils.INSTANCE;
        c = b.c(resources, R.string.self_username_indicator, new Object[]{UserUtils.getUserNameWithDiscriminator$default(userUtils, user, null, null, 3, null)}, (r4 & 4) != 0 ? b.d.j : null);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(c);
        SpannableStringBuilder spannableStringBuilder2 = (SpannableStringBuilder) c;
        spannableStringBuilder.setSpan(new ForegroundColorSpan(ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorHeaderPrimary)), spannableStringBuilder2.length() - UserUtils.getUserNameWithDiscriminator$default(userUtils, user, null, null, 3, null).length(), spannableStringBuilder2.length(), 33);
        return spannableStringBuilder;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchCaptchaFlow(Error error) {
        WidgetCaptcha.Companion.launch(requireContext(), this.captchaLauncher, CaptchaErrorBody.Companion.createFromError(error));
    }

    private final void sendFriendRequest(final String str, final int i, String str2) {
        ObservableExtensionsKt.ui$default(RestAPI.Companion.getApi().sendRelationshipRequest("Search - Add Friend Search", str, i, str2), this, null, 2, null).k(o.h(new Action1<Void>() { // from class: com.discord.widgets.friends.WidgetFriendsAddById$sendFriendRequest$1
            public final void call(Void r8) {
                CharSequence charSequence;
                WidgetFriendsAddByIdBinding binding;
                Context context = WidgetFriendsAddById.this.getContext();
                Context context2 = WidgetFriendsAddById.this.getContext();
                if (context2 != null) {
                    charSequence = b.b(context2, R.string.add_friend_confirmation, new Object[]{str}, (r4 & 4) != 0 ? b.C0034b.j : null);
                } else {
                    charSequence = null;
                }
                b.a.d.m.h(context, charSequence, 0, null, 12);
                binding = WidgetFriendsAddById.this.getBinding();
                TextInputLayout textInputLayout = binding.e;
                m.checkNotNullExpressionValue(textInputLayout, "binding.friendsAddTextEditWrap");
                ViewExtensions.clear(textInputLayout);
                AppFragment.hideKeyboard$default(WidgetFriendsAddById.this, null, 1, null);
            }
        }, getAppActivity(), new Action1<Error>() { // from class: com.discord.widgets.friends.WidgetFriendsAddById$sendFriendRequest$2

            /* compiled from: WidgetFriendsAddById.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.friends.WidgetFriendsAddById$sendFriendRequest$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends d0.z.d.o implements Function0<Unit> {
                public final /* synthetic */ Error $error;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass1(Error error) {
                    super(0);
                    this.$error = error;
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    WidgetFriendsAddByIdBinding binding;
                    Error error = this.$error;
                    m.checkNotNullExpressionValue(error, "error");
                    if (WidgetCaptchaKt.isCaptchaError(error)) {
                        WidgetFriendsAddById widgetFriendsAddById = WidgetFriendsAddById.this;
                        Error error2 = this.$error;
                        m.checkNotNullExpressionValue(error2, "error");
                        widgetFriendsAddById.launchCaptchaFlow(error2);
                        return;
                    }
                    binding = WidgetFriendsAddById.this.getBinding();
                    binding.e.setErrorTextAppearance(R.style.TextAppearance_Error);
                    RestAPIAbortMessages.ResponseResolver responseResolver = RestAPIAbortMessages.ResponseResolver.INSTANCE;
                    Context context = WidgetFriendsAddById.this.getContext();
                    Error error3 = this.$error;
                    m.checkNotNullExpressionValue(error3, "error");
                    Error.Response response = error3.getResponse();
                    m.checkNotNullExpressionValue(response, "error.response");
                    int code = response.getCode();
                    WidgetFriendsAddById.this.setInputEditError(String.valueOf(responseResolver.getRelationshipResponse(context, code, str + UserUtils.INSTANCE.padDiscriminator(i))));
                }
            }

            public final void call(Error error) {
                RestAPIAbortMessages restAPIAbortMessages = RestAPIAbortMessages.INSTANCE;
                m.checkNotNullExpressionValue(error, "error");
                RestAPIAbortMessages.handleAbortCodeOrDefault$default(restAPIAbortMessages, error, new AnonymousClass1(error), null, 4, null);
            }
        }));
    }

    public static /* synthetic */ void sendFriendRequest$default(WidgetFriendsAddById widgetFriendsAddById, String str, int i, String str2, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            str2 = null;
        }
        widgetFriendsAddById.sendFriendRequest(str, i, str2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void setInputEditError(CharSequence charSequence) {
        TextInputLayout textInputLayout = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout, "binding.friendsAddTextEditWrap");
        textInputLayout.setError(charSequence);
        TextInputLayout textInputLayout2 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.friendsAddTextEditWrap");
        textInputLayout2.setErrorEnabled(!(charSequence == null || t.isBlank(charSequence)));
    }

    private final void setInputText(String str) {
        if (!(str == null || t.isBlank(str))) {
            TextInputLayout textInputLayout = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout, "binding.friendsAddTextEditWrap");
            ViewExtensions.setText(textInputLayout, str);
            TextInputLayout textInputLayout2 = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.friendsAddTextEditWrap");
            ViewExtensions.setSelectionEnd(textInputLayout2);
        }
    }

    @Override // com.discord.app.AppFragment
    public void hideKeyboard(View view) {
        super.hideKeyboard(view);
        getBinding().e.clearFocus();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        AppFragment.hideKeyboard$default(this, null, 1, null);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AnalyticsTracker.INSTANCE.friendAddViewed("Id");
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsAddById$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetFriendsAddById.extractTargetAndSendFriendRequest$default(WidgetFriendsAddById.this, null, 1, null);
            }
        });
        TextInputLayout textInputLayout = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout, "binding.friendsAddTextEditWrap");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetFriendsAddById$onViewBound$2(this));
        TextInputLayout textInputLayout2 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.friendsAddTextEditWrap");
        String str = null;
        ViewExtensions.setOnImeActionDone$default(textInputLayout2, false, new WidgetFriendsAddById$onViewBound$3(this), 1, null);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsAddById$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AppFragment.hideKeyboard$default(WidgetFriendsAddById.this, null, 1, null);
            }
        });
        Bundle extras = getMostRecentIntent().getExtras();
        if (extras != null) {
            str = extras.getString("android.intent.extra.TEXT");
        }
        setInputText(str);
        MeUser me2 = StoreStream.Companion.getUsers().getMe();
        TextView textView = getBinding().f2369b;
        m.checkNotNullExpressionValue(textView, "binding.friendAddUsernameIndicator");
        textView.setVisibility(0);
        TextView textView2 = getBinding().f2369b;
        m.checkNotNullExpressionValue(textView2, "binding.friendAddUsernameIndicator");
        textView2.setText(getUsernameIndicatorText(me2));
    }
}
