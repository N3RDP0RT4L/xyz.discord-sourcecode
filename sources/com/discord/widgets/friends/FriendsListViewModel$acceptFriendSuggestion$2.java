package com.discord.widgets.friends;

import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPIAbortMessages;
import com.discord.widgets.captcha.WidgetCaptchaKt;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: FriendsListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FriendsListViewModel$acceptFriendSuggestion$2 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ int $discriminator;
    public final /* synthetic */ String $username;
    public final /* synthetic */ FriendsListViewModel this$0;

    /* compiled from: FriendsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.friends.FriendsListViewModel$acceptFriendSuggestion$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Error $error;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Error error) {
            super(0);
            this.$error = error;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            if (WidgetCaptchaKt.isCaptchaError(this.$error)) {
                FriendsListViewModel$acceptFriendSuggestion$2 friendsListViewModel$acceptFriendSuggestion$2 = FriendsListViewModel$acceptFriendSuggestion$2.this;
                friendsListViewModel$acceptFriendSuggestion$2.this$0.emitCaptchaErrorEvent(this.$error, friendsListViewModel$acceptFriendSuggestion$2.$username, friendsListViewModel$acceptFriendSuggestion$2.$discriminator);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FriendsListViewModel$acceptFriendSuggestion$2(FriendsListViewModel friendsListViewModel, String str, int i) {
        super(1);
        this.this$0 = friendsListViewModel;
        this.$username = str;
        this.$discriminator = i;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        m.checkNotNullParameter(error, "error");
        RestAPIAbortMessages.handleAbortCodeOrDefault$default(RestAPIAbortMessages.INSTANCE, error, new AnonymousClass1(error), null, 4, null);
    }
}
