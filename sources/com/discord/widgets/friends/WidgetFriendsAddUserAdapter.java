package com.discord.widgets.friends;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.WidgetAddFriendUserListItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.user.UserUtils;
import com.discord.views.StatusView;
import com.discord.widgets.friends.WidgetFriendsAddUserAdapter;
import com.discord.widgets.user.search.WidgetGlobalSearchModel;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetFriendsAddUserAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 ,2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003,-.B\u000f\u0012\u0006\u0010)\u001a\u00020(¢\u0006\u0004\b*\u0010+J\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ1\u0010\u0010\u001a\u00020\u000f2\u0010\u0010\r\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\n2\u0010\u0010\u000e\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\n¢\u0006\u0004\b\u0010\u0010\u0011J'\u0010\u0015\u001a\u00020\u000f2\u0018\u0010\u0014\u001a\u0014\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000f0\u0012¢\u0006\u0004\b\u0015\u0010\u0016J%\u0010\u0018\u001a\u00020\u000f2\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u000f0\u0017¢\u0006\u0004\b\u0018\u0010\u0019J+\u0010\u001b\u001a\u00020\u000f2\u001c\u0010\u0014\u001a\u0018\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u000f0\u0012¢\u0006\u0004\b\u001b\u0010\u0016J%\u0010\u001d\u001a\u00020\u000f2\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u000f0\u0017¢\u0006\u0004\b\u001d\u0010\u0019R*\u0010\u001e\u001a\u0016\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR.\u0010\"\u001a\u001a\u0012\b\u0012\u00060\u000bj\u0002`\f0 j\f\u0012\b\u0012\u00060\u000bj\u0002`\f`!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R.\u0010$\u001a\u001a\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010\u001fR(\u0010%\u001a\u0014\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R&\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\u000f0\u00178\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010&R.\u0010'\u001a\u001a\u0012\b\u0012\u00060\u000bj\u0002`\f0 j\f\u0012\b\u0012\u00060\u000bj\u0002`\f`!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010#¨\u0006/"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$UserViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$UserViewHolder;", "", "", "Lcom/discord/primitives/UserId;", "outgoingIds", "incomingIds", "", "addFriendRequestUserIds", "(Ljava/util/Collection;Ljava/util/Collection;)V", "Lkotlin/Function2;", "", "handler", "setSendHandler", "(Lkotlin/jvm/functions/Function2;)V", "Lkotlin/Function1;", "setAcceptHandler", "(Lkotlin/jvm/functions/Function1;)V", "", "setDeclineHandler", "onItemClick", "setOnItemClick", "sendHandler", "Lkotlin/jvm/functions/Function2;", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "incomingRequestUserIds", "Ljava/util/HashSet;", "declineHandler", "acceptHandler", "Lkotlin/jvm/functions/Function1;", "outgoingRequestUserIds", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "Companion", "ItemUser", "UserViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetFriendsAddUserAdapter extends MGRecyclerAdapterSimple<ItemUser> {
    public static final Companion Companion = new Companion(null);
    private static final int TYPE_USER = 1;
    private Function1<? super Long, Unit> acceptHandler;
    private Function2<? super Long, ? super Boolean, Unit> declineHandler;
    private Function2<? super String, ? super Integer, Unit> sendHandler;
    private final HashSet<Long> outgoingRequestUserIds = new HashSet<>();
    private final HashSet<Long> incomingRequestUserIds = new HashSet<>();
    private Function1<? super Long, Unit> onItemClick = WidgetFriendsAddUserAdapter$onItemClick$1.INSTANCE;

    /* compiled from: WidgetFriendsAddUserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$Companion;", "", "", "TYPE_USER", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetFriendsAddUserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$UserViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;", "Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser;)V", "Lcom/discord/databinding/WidgetAddFriendUserListItemBinding;", "binding", "Lcom/discord/databinding/WidgetAddFriendUserListItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UserViewHolder extends MGRecyclerViewHolder<WidgetFriendsAddUserAdapter, ItemUser> {
        private final WidgetAddFriendUserListItemBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public UserViewHolder(WidgetFriendsAddUserAdapter widgetFriendsAddUserAdapter) {
            super((int) R.layout.widget_add_friend_user_list_item, widgetFriendsAddUserAdapter);
            m.checkNotNullParameter(widgetFriendsAddUserAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.add_friend_user_accept_button;
            AppCompatImageView appCompatImageView = (AppCompatImageView) view.findViewById(R.id.add_friend_user_accept_button);
            if (appCompatImageView != null) {
                i = R.id.add_friend_user_check_image;
                AppCompatImageView appCompatImageView2 = (AppCompatImageView) view.findViewById(R.id.add_friend_user_check_image);
                if (appCompatImageView2 != null) {
                    i = R.id.add_friend_user_decline_button;
                    AppCompatImageView appCompatImageView3 = (AppCompatImageView) view.findViewById(R.id.add_friend_user_decline_button);
                    if (appCompatImageView3 != null) {
                        i = R.id.add_friend_user_item_avatar;
                        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.add_friend_user_item_avatar);
                        if (simpleDraweeView != null) {
                            i = R.id.add_friend_user_item_buttons;
                            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.add_friend_user_item_buttons);
                            if (linearLayout != null) {
                                i = R.id.add_friend_user_item_name;
                                TextView textView = (TextView) view.findViewById(R.id.add_friend_user_item_name);
                                if (textView != null) {
                                    i = R.id.add_friend_user_item_name_secondary;
                                    TextView textView2 = (TextView) view.findViewById(R.id.add_friend_user_item_name_secondary);
                                    if (textView2 != null) {
                                        i = R.id.add_friend_user_item_status;
                                        StatusView statusView = (StatusView) view.findViewById(R.id.add_friend_user_item_status);
                                        if (statusView != null) {
                                            i = R.id.add_friend_user_item_text;
                                            LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.add_friend_user_item_text);
                                            if (linearLayout2 != null) {
                                                i = R.id.add_friend_user_send_button;
                                                AppCompatImageView appCompatImageView4 = (AppCompatImageView) view.findViewById(R.id.add_friend_user_send_button);
                                                if (appCompatImageView4 != null) {
                                                    WidgetAddFriendUserListItemBinding widgetAddFriendUserListItemBinding = new WidgetAddFriendUserListItemBinding((ConstraintLayout) view, appCompatImageView, appCompatImageView2, appCompatImageView3, simpleDraweeView, linearLayout, textView, textView2, statusView, linearLayout2, appCompatImageView4);
                                                    m.checkNotNullExpressionValue(widgetAddFriendUserListItemBinding, "WidgetAddFriendUserListItemBinding.bind(itemView)");
                                                    this.binding = widgetAddFriendUserListItemBinding;
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ WidgetFriendsAddUserAdapter access$getAdapter$p(UserViewHolder userViewHolder) {
            return (WidgetFriendsAddUserAdapter) userViewHolder.adapter;
        }

        public void onConfigure(int i, ItemUser itemUser) {
            m.checkNotNullParameter(itemUser, "data");
            super.onConfigure(i, (int) itemUser);
            final User user = itemUser.getUser();
            SimpleDraweeView simpleDraweeView = this.binding.e;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.addFriendUserItemAvatar");
            IconUtils.setIcon$default(simpleDraweeView, user, 0, null, null, null, 60, null);
            TextView textView = this.binding.f;
            m.checkNotNullExpressionValue(textView, "binding.addFriendUserItemName");
            CharSequence charSequence = (CharSequence) u.lastOrNull((List<? extends Object>) itemUser.getAliases());
            if (charSequence == null) {
                charSequence = user.getUsername();
            }
            textView.setText(charSequence);
            TextView textView2 = this.binding.g;
            m.checkNotNullExpressionValue(textView2, "binding.addFriendUserItemNameSecondary");
            textView2.setText(UserUtils.INSTANCE.getDiscriminatorWithPadding(user));
            this.binding.h.setPresence(itemUser.getPresence());
            StatusView statusView = this.binding.h;
            m.checkNotNullExpressionValue(statusView, "binding.addFriendUserItemStatus");
            boolean z2 = true;
            int i2 = 0;
            statusView.setVisibility(itemUser.getPresence() != null ? 0 : 8);
            final long id2 = itemUser.getUser().getId();
            boolean contains = ((WidgetFriendsAddUserAdapter) this.adapter).outgoingRequestUserIds.contains(Long.valueOf(id2));
            final boolean contains2 = ((WidgetFriendsAddUserAdapter) this.adapter).incomingRequestUserIds.contains(Long.valueOf(id2));
            this.binding.f2206b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsAddUserAdapter$UserViewHolder$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1 function1;
                    function1 = WidgetFriendsAddUserAdapter.UserViewHolder.access$getAdapter$p(WidgetFriendsAddUserAdapter.UserViewHolder.this).acceptHandler;
                    if (function1 != null) {
                        Unit unit = (Unit) function1.invoke(Long.valueOf(id2));
                    }
                }
            });
            this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsAddUserAdapter$UserViewHolder$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function2 function2;
                    function2 = WidgetFriendsAddUserAdapter.UserViewHolder.access$getAdapter$p(WidgetFriendsAddUserAdapter.UserViewHolder.this).declineHandler;
                    if (function2 != null) {
                        Unit unit = (Unit) function2.invoke(Long.valueOf(id2), Boolean.valueOf(contains2));
                    }
                }
            });
            this.binding.i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsAddUserAdapter$UserViewHolder$onConfigure$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function2 function2;
                    function2 = WidgetFriendsAddUserAdapter.UserViewHolder.access$getAdapter$p(WidgetFriendsAddUserAdapter.UserViewHolder.this).sendHandler;
                    if (function2 != null) {
                        Unit unit = (Unit) function2.invoke(user.getUsername(), Integer.valueOf(user.getDiscriminator()));
                    }
                }
            });
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsAddUserAdapter$UserViewHolder$onConfigure$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function1 function1;
                    function1 = WidgetFriendsAddUserAdapter.UserViewHolder.access$getAdapter$p(WidgetFriendsAddUserAdapter.UserViewHolder.this).onItemClick;
                    function1.invoke(Long.valueOf(id2));
                }
            });
            AppCompatImageView appCompatImageView = this.binding.c;
            m.checkNotNullExpressionValue(appCompatImageView, "binding.addFriendUserCheckImage");
            appCompatImageView.setVisibility(itemUser.isFriend() ? 0 : 8);
            AppCompatImageView appCompatImageView2 = this.binding.i;
            m.checkNotNullExpressionValue(appCompatImageView2, "binding.addFriendUserSendButton");
            appCompatImageView2.setVisibility(!itemUser.isFriend() && !contains && !contains2 ? 0 : 8);
            AppCompatImageView appCompatImageView3 = this.binding.f2206b;
            m.checkNotNullExpressionValue(appCompatImageView3, "binding.addFriendUserAcceptButton");
            appCompatImageView3.setVisibility(!itemUser.isFriend() && contains2 ? 0 : 8);
            AppCompatImageView appCompatImageView4 = this.binding.d;
            m.checkNotNullExpressionValue(appCompatImageView4, "binding.addFriendUserDeclineButton");
            if (itemUser.isFriend() || (!contains && !contains2)) {
                z2 = false;
            }
            if (!z2) {
                i2 = 8;
            }
            appCompatImageView4.setVisibility(i2);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetFriendsAddUserAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
    }

    public final void addFriendRequestUserIds(Collection<Long> collection, Collection<Long> collection2) {
        m.checkNotNullParameter(collection, "outgoingIds");
        m.checkNotNullParameter(collection2, "incomingIds");
        this.outgoingRequestUserIds.clear();
        this.outgoingRequestUserIds.addAll(collection);
        this.incomingRequestUserIds.clear();
        this.incomingRequestUserIds.addAll(collection2);
        notifyDataSetChanged();
    }

    public final void setAcceptHandler(Function1<? super Long, Unit> function1) {
        m.checkNotNullParameter(function1, "handler");
        this.acceptHandler = function1;
    }

    public final void setDeclineHandler(Function2<? super Long, ? super Boolean, Unit> function2) {
        m.checkNotNullParameter(function2, "handler");
        this.declineHandler = function2;
    }

    public final void setOnItemClick(Function1<? super Long, Unit> function1) {
        m.checkNotNullParameter(function1, "onItemClick");
        this.onItemClick = function1;
    }

    public final void setSendHandler(Function2<? super String, ? super Integer, Unit> function2) {
        m.checkNotNullParameter(function2, "handler");
        this.sendHandler = function2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public UserViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 1) {
            return new UserViewHolder(this);
        }
        throw invalidViewTypeException(i);
    }

    /* compiled from: WidgetFriendsAddUserAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0014\b\u0086\b\u0018\u0000 .2\u00020\u0001:\u0001.B1\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\u0012\u001a\u00020\f¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ@\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\u0012\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001d\u001a\u00020\f2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u0004R\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b\"\u0010\bR\u001c\u0010#\u001a\u00020\u00188\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010\u001aR\u001c\u0010&\u001a\u00020\u00158\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010\u0017R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010)\u001a\u0004\b*\u0010\u000bR\u0019\u0010\u0012\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010+\u001a\u0004\b\u0012\u0010\u000e¨\u0006/"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "", "", "component2", "()Ljava/util/List;", "Lcom/discord/models/presence/Presence;", "component3", "()Lcom/discord/models/presence/Presence;", "", "component4", "()Z", "user", "aliases", "presence", "isFriend", "copy", "(Lcom/discord/models/user/User;Ljava/util/List;Lcom/discord/models/presence/Presence;Z)Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/user/User;", "getUser", "Ljava/util/List;", "getAliases", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/presence/Presence;", "getPresence", "Z", HookHelper.constructorName, "(Lcom/discord/models/user/User;Ljava/util/List;Lcom/discord/models/presence/Presence;Z)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemUser implements MGRecyclerDataPayload {
        public static final Companion Companion = new Companion(null);
        private final List<CharSequence> aliases;
        private final boolean isFriend;
        private final String key;
        private final Presence presence;
        private final int type;
        private final User user;

        /* compiled from: WidgetFriendsAddUserAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser$Companion;", "", "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;", "item", "Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser;", "create", "(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;)Lcom/discord/widgets/friends/WidgetFriendsAddUserAdapter$ItemUser;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final ItemUser create(WidgetGlobalSearchModel.ItemUser itemUser) {
                m.checkNotNullParameter(itemUser, "item");
                return new ItemUser(itemUser.getUser(), itemUser.getAliases(), itemUser.getPresence(), itemUser.isFriend());
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public ItemUser(User user, List<? extends CharSequence> list, Presence presence, boolean z2) {
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(list, "aliases");
            this.user = user;
            this.aliases = list;
            this.presence = presence;
            this.isFriend = z2;
            this.type = 1;
            this.key = String.valueOf(user.getId());
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ItemUser copy$default(ItemUser itemUser, User user, List list, Presence presence, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                user = itemUser.user;
            }
            if ((i & 2) != 0) {
                list = itemUser.aliases;
            }
            if ((i & 4) != 0) {
                presence = itemUser.presence;
            }
            if ((i & 8) != 0) {
                z2 = itemUser.isFriend;
            }
            return itemUser.copy(user, list, presence, z2);
        }

        public final User component1() {
            return this.user;
        }

        public final List<CharSequence> component2() {
            return this.aliases;
        }

        public final Presence component3() {
            return this.presence;
        }

        public final boolean component4() {
            return this.isFriend;
        }

        public final ItemUser copy(User user, List<? extends CharSequence> list, Presence presence, boolean z2) {
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(list, "aliases");
            return new ItemUser(user, list, presence, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ItemUser)) {
                return false;
            }
            ItemUser itemUser = (ItemUser) obj;
            return m.areEqual(this.user, itemUser.user) && m.areEqual(this.aliases, itemUser.aliases) && m.areEqual(this.presence, itemUser.presence) && this.isFriend == itemUser.isFriend;
        }

        public final List<CharSequence> getAliases() {
            return this.aliases;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final Presence getPresence() {
            return this.presence;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public final User getUser() {
            return this.user;
        }

        public int hashCode() {
            User user = this.user;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            List<CharSequence> list = this.aliases;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            Presence presence = this.presence;
            if (presence != null) {
                i = presence.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.isFriend;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isFriend() {
            return this.isFriend;
        }

        public String toString() {
            StringBuilder R = a.R("ItemUser(user=");
            R.append(this.user);
            R.append(", aliases=");
            R.append(this.aliases);
            R.append(", presence=");
            R.append(this.presence);
            R.append(", isFriend=");
            return a.M(R, this.isFriend, ")");
        }

        public /* synthetic */ ItemUser(User user, List list, Presence presence, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(user, list, (i & 4) != 0 ? null : presence, z2);
        }
    }
}
