package com.discord.widgets.friends;

import andhook.lib.HookHelper;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.databinding.WidgetFriendsListAdapterItemFriendBinding;
import com.discord.databinding.WidgetFriendsListAdapterItemHeaderBinding;
import com.discord.databinding.WidgetFriendsListAdapterItemPendingBinding;
import com.discord.databinding.WidgetFriendsListAdapterSuggestedFriendBinding;
import com.discord.databinding.WidgetFriendsListContactSyncUpsellBinding;
import com.discord.databinding.WidgetFriendsListExpandableHeaderBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.presence.PresenceUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.views.StatusView;
import com.discord.widgets.friends.FriendsListViewModel;
import com.discord.widgets.friends.WidgetFriendsListAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.card.MaterialCardView;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetFriendsListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\bABCDEFGHB\u000f\u0012\u0006\u0010>\u001a\u00020=¢\u0006\u0004\b?\u0010@J)\u0010\b\u001a\f\u0012\u0002\b\u0003\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR4\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R(\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\r0\u00148\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR.\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R.\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\"\u0010\u001d\u001a\u0004\b#\u0010\u001f\"\u0004\b$\u0010!R.\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b%\u0010\u001d\u001a\u0004\b&\u0010\u001f\"\u0004\b'\u0010!R(\u0010(\u001a\b\u0012\u0004\u0012\u00020\r0\u00148\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b(\u0010\u0016\u001a\u0004\b)\u0010\u0018\"\u0004\b*\u0010\u001aR.\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b+\u0010\u001d\u001a\u0004\b,\u0010\u001f\"\u0004\b-\u0010!R2\u00100\u001a\u0012\u0012\b\u0012\u00060.j\u0002`/\u0012\u0004\u0012\u00020\r0\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b0\u0010\u001d\u001a\u0004\b1\u0010\u001f\"\u0004\b2\u0010!R(\u00103\u001a\b\u0012\u0004\u0012\u00020\r0\u00148\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b3\u0010\u0016\u001a\u0004\b4\u0010\u0018\"\u0004\b5\u0010\u001aR.\u00106\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\r0\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b6\u0010\u001d\u001a\u0004\b7\u0010\u001f\"\u0004\b8\u0010!R8\u0010:\u001a\u0018\u0012\u0004\u0012\u00020\f\u0012\b\u0012\u00060\u0005j\u0002`9\u0012\u0004\u0012\u00020\r0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b:\u0010\u000f\u001a\u0004\b;\u0010\u0011\"\u0004\b<\u0010\u0013¨\u0006I"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function2;", "Landroid/view/View;", "Lcom/discord/models/user/User;", "", "onClickUserProfile", "Lkotlin/jvm/functions/Function2;", "getOnClickUserProfile", "()Lkotlin/jvm/functions/Function2;", "setOnClickUserProfile", "(Lkotlin/jvm/functions/Function2;)V", "Lkotlin/Function0;", "onClickPendingHeaderExpand", "Lkotlin/jvm/functions/Function0;", "getOnClickPendingHeaderExpand", "()Lkotlin/jvm/functions/Function0;", "setOnClickPendingHeaderExpand", "(Lkotlin/jvm/functions/Function0;)V", "Lkotlin/Function1;", "onClickChat", "Lkotlin/jvm/functions/Function1;", "getOnClickChat", "()Lkotlin/jvm/functions/Function1;", "setOnClickChat", "(Lkotlin/jvm/functions/Function1;)V", "onClickApproveSuggestion", "getOnClickApproveSuggestion", "setOnClickApproveSuggestion", "onClickAcceptFriend", "getOnClickAcceptFriend", "setOnClickAcceptFriend", "onClickContactSyncUpsell", "getOnClickContactSyncUpsell", "setOnClickContactSyncUpsell", "onClickCall", "getOnClickCall", "setOnClickCall", "", "Lcom/discord/primitives/UserId;", "onClickRemoveSuggestion", "getOnClickRemoveSuggestion", "setOnClickRemoveSuggestion", "onClickSuggestedHeaderExpandCollapse", "getOnClickSuggestedHeaderExpandCollapse", "setOnClickSuggestedHeaderExpandCollapse", "onClickContactSyncUpsellLongClick", "getOnClickContactSyncUpsellLongClick", "setOnClickContactSyncUpsellLongClick", "Lcom/discord/primitives/RelationshipType;", "onClickDeclineFriend", "getOnClickDeclineFriend", "setOnClickDeclineFriend", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "Item", "ItemContactSyncUpsell", "ItemHeader", "ItemPendingHeader", "ItemPendingUser", "ItemSuggestedFriend", "ItemSuggestedFriendHeader", "ItemUser", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetFriendsListAdapter extends MGRecyclerAdapterSimple<FriendsListViewModel.Item> {
    private Function0<Unit> onClickSuggestedHeaderExpandCollapse = WidgetFriendsListAdapter$onClickSuggestedHeaderExpandCollapse$1.INSTANCE;
    private Function0<Unit> onClickPendingHeaderExpand = WidgetFriendsListAdapter$onClickPendingHeaderExpand$1.INSTANCE;
    private Function2<? super View, ? super User, Unit> onClickUserProfile = WidgetFriendsListAdapter$onClickUserProfile$1.INSTANCE;
    private Function1<? super User, Unit> onClickCall = WidgetFriendsListAdapter$onClickCall$1.INSTANCE;
    private Function1<? super User, Unit> onClickChat = WidgetFriendsListAdapter$onClickChat$1.INSTANCE;
    private Function1<? super User, Unit> onClickAcceptFriend = WidgetFriendsListAdapter$onClickAcceptFriend$1.INSTANCE;
    private Function2<? super User, ? super Integer, Unit> onClickDeclineFriend = WidgetFriendsListAdapter$onClickDeclineFriend$1.INSTANCE;
    private Function1<? super User, Unit> onClickApproveSuggestion = WidgetFriendsListAdapter$onClickApproveSuggestion$1.INSTANCE;
    private Function1<? super Long, Unit> onClickRemoveSuggestion = WidgetFriendsListAdapter$onClickRemoveSuggestion$1.INSTANCE;
    private Function0<Unit> onClickContactSyncUpsell = WidgetFriendsListAdapter$onClickContactSyncUpsell$1.INSTANCE;
    private Function1<? super View, Unit> onClickContactSyncUpsellLongClick = WidgetFriendsListAdapter$onClickContactSyncUpsellLongClick$1.INSTANCE;

    /* compiled from: WidgetFriendsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u001b\b\u0016\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bB\u0019\b\u0016\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u0006\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "", "layoutResId", "adapter", HookHelper.constructorName, "(ILcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "Landroid/view/View;", "view", "(Landroid/view/View;Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item extends MGRecyclerViewHolder<WidgetFriendsListAdapter, FriendsListViewModel.Item> {
        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Item(@LayoutRes int i, WidgetFriendsListAdapter widgetFriendsListAdapter) {
            super(i, widgetFriendsListAdapter);
            m.checkNotNullParameter(widgetFriendsListAdapter, "adapter");
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Item(View view, WidgetFriendsListAdapter widgetFriendsListAdapter) {
            super(view, widgetFriendsListAdapter);
            m.checkNotNullParameter(view, "view");
            m.checkNotNullParameter(widgetFriendsListAdapter, "adapter");
        }
    }

    /* compiled from: WidgetFriendsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemContactSyncUpsell;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V", "Lcom/discord/databinding/WidgetFriendsListContactSyncUpsellBinding;", "binding", "Lcom/discord/databinding/WidgetFriendsListContactSyncUpsellBinding;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemContactSyncUpsell extends Item {
        private final WidgetFriendsListContactSyncUpsellBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemContactSyncUpsell(WidgetFriendsListAdapter widgetFriendsListAdapter) {
            super((int) R.layout.widget_friends_list_contact_sync_upsell, widgetFriendsListAdapter);
            m.checkNotNullParameter(widgetFriendsListAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.friends_list_contact_sync_upsell_arrow;
            ImageView imageView = (ImageView) view.findViewById(R.id.friends_list_contact_sync_upsell_arrow);
            if (imageView != null) {
                MaterialCardView materialCardView = (MaterialCardView) view;
                i = R.id.friends_list_contact_sync_upsell_icon;
                ImageView imageView2 = (ImageView) view.findViewById(R.id.friends_list_contact_sync_upsell_icon);
                if (imageView2 != null) {
                    i = R.id.friends_list_contact_sync_upsell_subtitle;
                    TextView textView = (TextView) view.findViewById(R.id.friends_list_contact_sync_upsell_subtitle);
                    if (textView != null) {
                        i = R.id.friends_list_contact_sync_upsell_title;
                        TextView textView2 = (TextView) view.findViewById(R.id.friends_list_contact_sync_upsell_title);
                        if (textView2 != null) {
                            WidgetFriendsListContactSyncUpsellBinding widgetFriendsListContactSyncUpsellBinding = new WidgetFriendsListContactSyncUpsellBinding(materialCardView, imageView, materialCardView, imageView2, textView, textView2);
                            m.checkNotNullExpressionValue(widgetFriendsListContactSyncUpsellBinding, "WidgetFriendsListContact…ellBinding.bind(itemView)");
                            this.binding = widgetFriendsListContactSyncUpsellBinding;
                            return;
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ WidgetFriendsListAdapter access$getAdapter$p(ItemContactSyncUpsell itemContactSyncUpsell) {
            return (WidgetFriendsListAdapter) itemContactSyncUpsell.adapter;
        }

        public void onConfigure(int i, FriendsListViewModel.Item item) {
            m.checkNotNullParameter(item, "data");
            FriendsListViewModel.Item.ContactSyncUpsell contactSyncUpsell = (FriendsListViewModel.Item.ContactSyncUpsell) item;
            this.binding.f2375b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemContactSyncUpsell$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetFriendsListAdapter.ItemContactSyncUpsell.access$getAdapter$p(WidgetFriendsListAdapter.ItemContactSyncUpsell.this).getOnClickContactSyncUpsell().invoke();
                }
            });
            this.binding.f2375b.setOnLongClickListener(new View.OnLongClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemContactSyncUpsell$onConfigure$2
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    Function1<View, Unit> onClickContactSyncUpsellLongClick = WidgetFriendsListAdapter.ItemContactSyncUpsell.access$getAdapter$p(WidgetFriendsListAdapter.ItemContactSyncUpsell.this).getOnClickContactSyncUpsellLongClick();
                    m.checkNotNullExpressionValue(view, "it");
                    onClickContactSyncUpsellLongClick.invoke(view);
                    return true;
                }
            });
        }
    }

    /* compiled from: WidgetFriendsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemHeader;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V", "Lcom/discord/databinding/WidgetFriendsListAdapterItemHeaderBinding;", "binding", "Lcom/discord/databinding/WidgetFriendsListAdapterItemHeaderBinding;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemHeader extends Item {
        private final WidgetFriendsListAdapterItemHeaderBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemHeader(WidgetFriendsListAdapter widgetFriendsListAdapter) {
            super((int) R.layout.widget_friends_list_adapter_item_header, widgetFriendsListAdapter);
            m.checkNotNullParameter(widgetFriendsListAdapter, "adapter");
            View view = this.itemView;
            TextView textView = (TextView) view.findViewById(R.id.friends_list_item_header_text);
            if (textView != null) {
                WidgetFriendsListAdapterItemHeaderBinding widgetFriendsListAdapterItemHeaderBinding = new WidgetFriendsListAdapterItemHeaderBinding((FrameLayout) view, textView);
                m.checkNotNullExpressionValue(widgetFriendsListAdapterItemHeaderBinding, "WidgetFriendsListAdapter…derBinding.bind(itemView)");
                this.binding = widgetFriendsListAdapterItemHeaderBinding;
                return;
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.friends_list_item_header_text)));
        }

        public void onConfigure(int i, FriendsListViewModel.Item item) {
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            FriendsListViewModel.Item.Header header = (FriendsListViewModel.Item.Header) item;
            TextView textView = this.binding.f2371b;
            m.checkNotNullExpressionValue(textView, "binding.friendsListItemHeaderText");
            b.m(textView, header.getTitleStringResId(), new Object[]{Integer.valueOf(header.getCount())}, (r4 & 4) != 0 ? b.g.j : null);
        }
    }

    /* compiled from: WidgetFriendsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingHeader;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V", "Lcom/discord/databinding/WidgetFriendsListExpandableHeaderBinding;", "binding", "Lcom/discord/databinding/WidgetFriendsListExpandableHeaderBinding;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemPendingHeader extends Item {
        private final WidgetFriendsListExpandableHeaderBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemPendingHeader(WidgetFriendsListAdapter widgetFriendsListAdapter) {
            super((int) R.layout.widget_friends_list_expandable_header, widgetFriendsListAdapter);
            m.checkNotNullParameter(widgetFriendsListAdapter, "adapter");
            WidgetFriendsListExpandableHeaderBinding a = WidgetFriendsListExpandableHeaderBinding.a(this.itemView);
            m.checkNotNullExpressionValue(a, "WidgetFriendsListExpanda…derBinding.bind(itemView)");
            this.binding = a;
        }

        public static final /* synthetic */ WidgetFriendsListAdapter access$getAdapter$p(ItemPendingHeader itemPendingHeader) {
            return (WidgetFriendsListAdapter) itemPendingHeader.adapter;
        }

        public void onConfigure(int i, FriendsListViewModel.Item item) {
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            FriendsListViewModel.Item.PendingHeader pendingHeader = (FriendsListViewModel.Item.PendingHeader) item;
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.friendsListPendingItemHeaderText");
            b.m(textView, pendingHeader.getTitleStringResId(), new Object[]{Integer.valueOf(pendingHeader.getCount())}, (r4 & 4) != 0 ? b.g.j : null);
            if (pendingHeader.getShowExpandButton()) {
                TextView textView2 = this.binding.f2376b;
                m.checkNotNullExpressionValue(textView2, "binding.friendsListExpandableHeaderButton");
                textView2.setVisibility(0);
                int i2 = pendingHeader.isPendingSectionExpanded() ? R.string.friends_pending_request_expand_collapse : R.string.friends_pending_request_expand;
                TextView textView3 = this.binding.f2376b;
                m.checkNotNullExpressionValue(textView3, "binding.friendsListExpandableHeaderButton");
                b.m(textView3, i2, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
                int i3 = pendingHeader.isPendingSectionExpanded() ? R.drawable.ic_arrow_up_24dp : R.drawable.ic_arrow_right_24dp;
                TextView textView4 = this.binding.f2376b;
                m.checkNotNullExpressionValue(textView4, "binding.friendsListExpandableHeaderButton");
                Drawable drawable = ContextCompat.getDrawable(textView4.getContext(), i3);
                TextView textView5 = this.binding.f2376b;
                m.checkNotNullExpressionValue(textView5, "binding.friendsListExpandableHeaderButton");
                DrawableCompat.setCompoundDrawablesCompat$default(textView5, (Drawable) null, (Drawable) null, drawable, (Drawable) null, 11, (Object) null);
                this.binding.f2376b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemPendingHeader$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetFriendsListAdapter.ItemPendingHeader.access$getAdapter$p(WidgetFriendsListAdapter.ItemPendingHeader.this).getOnClickPendingHeaderExpand().invoke();
                    }
                });
                return;
            }
            TextView textView6 = this.binding.f2376b;
            m.checkNotNullExpressionValue(textView6, "binding.friendsListExpandableHeaderButton");
            textView6.setVisibility(8);
        }
    }

    /* compiled from: WidgetFriendsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemPendingUser;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V", "Lcom/discord/databinding/WidgetFriendsListAdapterItemPendingBinding;", "binding", "Lcom/discord/databinding/WidgetFriendsListAdapterItemPendingBinding;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemPendingUser extends Item {
        private final WidgetFriendsListAdapterItemPendingBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemPendingUser(WidgetFriendsListAdapter widgetFriendsListAdapter) {
            super((int) R.layout.widget_friends_list_adapter_item_pending, widgetFriendsListAdapter);
            m.checkNotNullParameter(widgetFriendsListAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.friends_list_item_accept_button;
            AppCompatImageView appCompatImageView = (AppCompatImageView) view.findViewById(R.id.friends_list_item_accept_button);
            if (appCompatImageView != null) {
                i = R.id.friends_list_item_activity;
                SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) view.findViewById(R.id.friends_list_item_activity);
                if (simpleDraweeSpanTextView != null) {
                    i = R.id.friends_list_item_avatar;
                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.friends_list_item_avatar);
                    if (simpleDraweeView != null) {
                        i = R.id.friends_list_item_buttons_wrap;
                        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.friends_list_item_buttons_wrap);
                        if (linearLayout != null) {
                            i = R.id.friends_list_item_decline_button;
                            AppCompatImageView appCompatImageView2 = (AppCompatImageView) view.findViewById(R.id.friends_list_item_decline_button);
                            if (appCompatImageView2 != null) {
                                i = R.id.friends_list_item_name;
                                TextView textView = (TextView) view.findViewById(R.id.friends_list_item_name);
                                if (textView != null) {
                                    i = R.id.friends_list_item_status;
                                    StatusView statusView = (StatusView) view.findViewById(R.id.friends_list_item_status);
                                    if (statusView != null) {
                                        i = R.id.friends_list_item_text;
                                        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.friends_list_item_text);
                                        if (linearLayout2 != null) {
                                            WidgetFriendsListAdapterItemPendingBinding widgetFriendsListAdapterItemPendingBinding = new WidgetFriendsListAdapterItemPendingBinding((RelativeLayout) view, appCompatImageView, simpleDraweeSpanTextView, simpleDraweeView, linearLayout, appCompatImageView2, textView, statusView, linearLayout2);
                                            m.checkNotNullExpressionValue(widgetFriendsListAdapterItemPendingBinding, "WidgetFriendsListAdapter…ingBinding.bind(itemView)");
                                            this.binding = widgetFriendsListAdapterItemPendingBinding;
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ WidgetFriendsListAdapter access$getAdapter$p(ItemPendingUser itemPendingUser) {
            return (WidgetFriendsListAdapter) itemPendingUser.adapter;
        }

        public void onConfigure(int i, final FriendsListViewModel.Item item) {
            CharSequence d;
            CharSequence d2;
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            final FriendsListViewModel.Item.PendingFriendRequest pendingFriendRequest = (FriendsListViewModel.Item.PendingFriendRequest) item;
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemPendingUser$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function2<View, User, Unit> onClickUserProfile = WidgetFriendsListAdapter.ItemPendingUser.access$getAdapter$p(WidgetFriendsListAdapter.ItemPendingUser.this).getOnClickUserProfile();
                    m.checkNotNullExpressionValue(view, "view");
                    onClickUserProfile.invoke(view, ((FriendsListViewModel.Item.PendingFriendRequest) item).getUser());
                }
            });
            TextView textView = this.binding.f;
            m.checkNotNullExpressionValue(textView, "binding.friendsListItemName");
            textView.setText(pendingFriendRequest.getUser().getUsername());
            this.binding.g.setPresence(pendingFriendRequest.getPresence());
            int relationshipType = pendingFriendRequest.getRelationshipType();
            if (relationshipType == 3) {
                SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.c;
                m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.friendsListItemActivity");
                SimpleDraweeSpanTextView simpleDraweeSpanTextView2 = this.binding.c;
                m.checkNotNullExpressionValue(simpleDraweeSpanTextView2, "binding.friendsListItemActivity");
                d = b.d(simpleDraweeSpanTextView2, R.string.incoming_friend_request, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                ViewExtensions.setTextAndVisibilityBy(simpleDraweeSpanTextView, d);
                AppCompatImageView appCompatImageView = this.binding.f2372b;
                m.checkNotNullExpressionValue(appCompatImageView, "binding.friendsListItemAcceptButton");
                appCompatImageView.setVisibility(0);
            } else if (relationshipType == 4) {
                SimpleDraweeSpanTextView simpleDraweeSpanTextView3 = this.binding.c;
                m.checkNotNullExpressionValue(simpleDraweeSpanTextView3, "binding.friendsListItemActivity");
                SimpleDraweeSpanTextView simpleDraweeSpanTextView4 = this.binding.c;
                m.checkNotNullExpressionValue(simpleDraweeSpanTextView4, "binding.friendsListItemActivity");
                d2 = b.d(simpleDraweeSpanTextView4, R.string.outgoing_friend_request, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                ViewExtensions.setTextAndVisibilityBy(simpleDraweeSpanTextView3, d2);
                AppCompatImageView appCompatImageView2 = this.binding.f2372b;
                m.checkNotNullExpressionValue(appCompatImageView2, "binding.friendsListItemAcceptButton");
                appCompatImageView2.setVisibility(8);
            }
            SimpleDraweeView simpleDraweeView = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.friendsListItemAvatar");
            IconUtils.setIcon$default(simpleDraweeView, pendingFriendRequest.getUser(), R.dimen.avatar_size_standard, null, null, null, 56, null);
            this.binding.f2372b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemPendingUser$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetFriendsListAdapter.ItemPendingUser.access$getAdapter$p(WidgetFriendsListAdapter.ItemPendingUser.this).getOnClickAcceptFriend().invoke(pendingFriendRequest.getUser());
                }
            });
            this.binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemPendingUser$onConfigure$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetFriendsListAdapter.ItemPendingUser.access$getAdapter$p(WidgetFriendsListAdapter.ItemPendingUser.this).getOnClickDeclineFriend().invoke(pendingFriendRequest.getUser(), Integer.valueOf(pendingFriendRequest.getRelationshipType()));
                }
            });
        }
    }

    /* compiled from: WidgetFriendsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemSuggestedFriend;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V", "Lcom/discord/databinding/WidgetFriendsListAdapterSuggestedFriendBinding;", "viewBinding", "Lcom/discord/databinding/WidgetFriendsListAdapterSuggestedFriendBinding;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetFriendsListAdapterSuggestedFriendBinding;Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemSuggestedFriend extends Item {
        private final WidgetFriendsListAdapterSuggestedFriendBinding viewBinding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public ItemSuggestedFriend(com.discord.databinding.WidgetFriendsListAdapterSuggestedFriendBinding r3, com.discord.widgets.friends.WidgetFriendsListAdapter r4) {
            /*
                r2 = this;
                java.lang.String r0 = "viewBinding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                java.lang.String r0 = "adapter"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                com.discord.widgets.friends.SuggestedFriendView r0 = r3.a
                java.lang.String r1 = "viewBinding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r2.<init>(r0, r4)
                r2.viewBinding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.friends.WidgetFriendsListAdapter.ItemSuggestedFriend.<init>(com.discord.databinding.WidgetFriendsListAdapterSuggestedFriendBinding, com.discord.widgets.friends.WidgetFriendsListAdapter):void");
        }

        public static final /* synthetic */ WidgetFriendsListAdapter access$getAdapter$p(ItemSuggestedFriend itemSuggestedFriend) {
            return (WidgetFriendsListAdapter) itemSuggestedFriend.adapter;
        }

        public void onConfigure(int i, final FriendsListViewModel.Item item) {
            m.checkNotNullParameter(item, "data");
            FriendsListViewModel.Item.SuggestedFriend suggestedFriend = (FriendsListViewModel.Item.SuggestedFriend) item;
            this.viewBinding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemSuggestedFriend$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function2<View, User, Unit> onClickUserProfile = WidgetFriendsListAdapter.ItemSuggestedFriend.access$getAdapter$p(WidgetFriendsListAdapter.ItemSuggestedFriend.this).getOnClickUserProfile();
                    m.checkNotNullExpressionValue(view, "view");
                    onClickUserProfile.invoke(view, ((FriendsListViewModel.Item.SuggestedFriend) item).getSuggestion().getUser());
                }
            });
            this.viewBinding.f2373b.setAvatarUrl(suggestedFriend.getSuggestion().getUser().getId(), Integer.valueOf(suggestedFriend.getSuggestion().getUser().getDiscriminator()), suggestedFriend.getSuggestion().getUser().getAvatar());
            this.viewBinding.f2373b.setUsername(suggestedFriend.getSuggestion().getUser().getUsername());
            this.viewBinding.f2373b.setPublicName(suggestedFriend.getSuggestion().getPublicName());
            this.viewBinding.f2373b.setOnSendClicked(new WidgetFriendsListAdapter$ItemSuggestedFriend$onConfigure$2(this, item));
            this.viewBinding.f2373b.setOnDeclineClicked(new WidgetFriendsListAdapter$ItemSuggestedFriend$onConfigure$3(this, item));
        }
    }

    /* compiled from: WidgetFriendsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemSuggestedFriendHeader;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V", "Lcom/discord/databinding/WidgetFriendsListExpandableHeaderBinding;", "binding", "Lcom/discord/databinding/WidgetFriendsListExpandableHeaderBinding;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemSuggestedFriendHeader extends Item {
        private final WidgetFriendsListExpandableHeaderBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemSuggestedFriendHeader(WidgetFriendsListAdapter widgetFriendsListAdapter) {
            super((int) R.layout.widget_friends_list_expandable_header, widgetFriendsListAdapter);
            m.checkNotNullParameter(widgetFriendsListAdapter, "adapter");
            WidgetFriendsListExpandableHeaderBinding a = WidgetFriendsListExpandableHeaderBinding.a(this.itemView);
            m.checkNotNullExpressionValue(a, "WidgetFriendsListExpanda…derBinding.bind(itemView)");
            this.binding = a;
        }

        public static final /* synthetic */ WidgetFriendsListAdapter access$getAdapter$p(ItemSuggestedFriendHeader itemSuggestedFriendHeader) {
            return (WidgetFriendsListAdapter) itemSuggestedFriendHeader.adapter;
        }

        public void onConfigure(int i, FriendsListViewModel.Item item) {
            CharSequence charSequence;
            m.checkNotNullParameter(item, "data");
            FriendsListViewModel.Item.SuggestedFriendsHeader suggestedFriendsHeader = (FriendsListViewModel.Item.SuggestedFriendsHeader) item;
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.friendsListPendingItemHeaderText");
            b.m(textView, R.string.friends_friend_suggestions_header, new Object[0], new WidgetFriendsListAdapter$ItemSuggestedFriendHeader$onConfigure$1(item));
            if (suggestedFriendsHeader.getShowExpandButton()) {
                TextView textView2 = this.binding.f2376b;
                m.checkNotNullExpressionValue(textView2, "binding.friendsListExpandableHeaderButton");
                textView2.setVisibility(0);
                TextView textView3 = this.binding.f2376b;
                m.checkNotNullExpressionValue(textView3, "binding.friendsListExpandableHeaderButton");
                if (suggestedFriendsHeader.isExpanded()) {
                    TextView textView4 = this.binding.f2376b;
                    m.checkNotNullExpressionValue(textView4, "binding.friendsListExpandableHeaderButton");
                    charSequence = b.d(textView4, R.string.friends_pending_request_expand_collapse, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                } else {
                    TextView textView5 = this.binding.f2376b;
                    m.checkNotNullExpressionValue(textView5, "binding.friendsListExpandableHeaderButton");
                    charSequence = b.d(textView5, R.string.friends_pending_request_expand, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                }
                textView3.setText(charSequence);
                int i2 = suggestedFriendsHeader.isExpanded() ? R.drawable.ic_arrow_up_24dp : R.drawable.ic_arrow_right_24dp;
                TextView textView6 = this.binding.f2376b;
                m.checkNotNullExpressionValue(textView6, "binding.friendsListExpandableHeaderButton");
                Drawable drawable = ContextCompat.getDrawable(textView6.getContext(), i2);
                TextView textView7 = this.binding.f2376b;
                m.checkNotNullExpressionValue(textView7, "binding.friendsListExpandableHeaderButton");
                DrawableCompat.setCompoundDrawablesCompat$default(textView7, (Drawable) null, (Drawable) null, drawable, (Drawable) null, 11, (Object) null);
                this.binding.f2376b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemSuggestedFriendHeader$onConfigure$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetFriendsListAdapter.ItemSuggestedFriendHeader.access$getAdapter$p(WidgetFriendsListAdapter.ItemSuggestedFriendHeader.this).getOnClickSuggestedHeaderExpandCollapse().invoke();
                    }
                });
                return;
            }
            TextView textView8 = this.binding.f2376b;
            m.checkNotNullExpressionValue(textView8, "binding.friendsListExpandableHeaderButton");
            textView8.setVisibility(8);
        }
    }

    /* compiled from: WidgetFriendsListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsListAdapter$ItemUser;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "data", "", "onConfigure", "(ILcom/discord/widgets/friends/FriendsListViewModel$Item;)V", "Lcom/discord/databinding/WidgetFriendsListAdapterItemFriendBinding;", "binding", "Lcom/discord/databinding/WidgetFriendsListAdapterItemFriendBinding;", "Lcom/discord/widgets/friends/WidgetFriendsListAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/friends/WidgetFriendsListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ItemUser extends Item {
        private final WidgetFriendsListAdapterItemFriendBinding binding;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ItemUser(WidgetFriendsListAdapter widgetFriendsListAdapter) {
            super((int) R.layout.widget_friends_list_adapter_item_friend, widgetFriendsListAdapter);
            m.checkNotNullParameter(widgetFriendsListAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.friends_list_item_activity;
            SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) view.findViewById(R.id.friends_list_item_activity);
            if (simpleDraweeSpanTextView != null) {
                i = R.id.friends_list_item_avatar;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.friends_list_item_avatar);
                if (simpleDraweeView != null) {
                    i = R.id.friends_list_item_buttons_wrap;
                    LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.friends_list_item_buttons_wrap);
                    if (linearLayout != null) {
                        i = R.id.friends_list_item_call_button;
                        AppCompatImageView appCompatImageView = (AppCompatImageView) view.findViewById(R.id.friends_list_item_call_button);
                        if (appCompatImageView != null) {
                            i = R.id.friends_list_item_chat_button;
                            AppCompatImageView appCompatImageView2 = (AppCompatImageView) view.findViewById(R.id.friends_list_item_chat_button);
                            if (appCompatImageView2 != null) {
                                i = R.id.friends_list_item_name;
                                TextView textView = (TextView) view.findViewById(R.id.friends_list_item_name);
                                if (textView != null) {
                                    i = R.id.friends_list_item_status;
                                    StatusView statusView = (StatusView) view.findViewById(R.id.friends_list_item_status);
                                    if (statusView != null) {
                                        i = R.id.friends_list_item_text;
                                        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.friends_list_item_text);
                                        if (linearLayout2 != null) {
                                            WidgetFriendsListAdapterItemFriendBinding widgetFriendsListAdapterItemFriendBinding = new WidgetFriendsListAdapterItemFriendBinding((RelativeLayout) view, simpleDraweeSpanTextView, simpleDraweeView, linearLayout, appCompatImageView, appCompatImageView2, textView, statusView, linearLayout2);
                                            m.checkNotNullExpressionValue(widgetFriendsListAdapterItemFriendBinding, "WidgetFriendsListAdapter…endBinding.bind(itemView)");
                                            this.binding = widgetFriendsListAdapterItemFriendBinding;
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        public static final /* synthetic */ WidgetFriendsListAdapter access$getAdapter$p(ItemUser itemUser) {
            return (WidgetFriendsListAdapter) itemUser.adapter;
        }

        public void onConfigure(int i, final FriendsListViewModel.Item item) {
            m.checkNotNullParameter(item, "data");
            super.onConfigure(i, (int) item);
            final FriendsListViewModel.Item.Friend friend = (FriendsListViewModel.Item.Friend) item;
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemUser$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function2<View, User, Unit> onClickUserProfile = WidgetFriendsListAdapter.ItemUser.access$getAdapter$p(WidgetFriendsListAdapter.ItemUser.this).getOnClickUserProfile();
                    m.checkNotNullExpressionValue(view, "view");
                    onClickUserProfile.invoke(view, ((FriendsListViewModel.Item.Friend) item).getUser());
                }
            });
            TextView textView = this.binding.f;
            m.checkNotNullExpressionValue(textView, "binding.friendsListItemName");
            textView.setText(friend.getUser().getUsername());
            this.binding.g.setPresence(friend.getPresence());
            Presence presence = friend.getPresence();
            boolean isApplicationStreaming = friend.isApplicationStreaming();
            SimpleDraweeSpanTextView simpleDraweeSpanTextView = this.binding.f2370b;
            m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.friendsListItemActivity");
            PresenceUtils.setPresenceText$default(presence, isApplicationStreaming, simpleDraweeSpanTextView, true, false, 16, null);
            SimpleDraweeView simpleDraweeView = this.binding.c;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.friendsListItemAvatar");
            IconUtils.setIcon$default(simpleDraweeView, friend.getUser(), R.dimen.avatar_size_standard, null, null, null, 56, null);
            this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemUser$onConfigure$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetFriendsListAdapter.ItemUser.access$getAdapter$p(WidgetFriendsListAdapter.ItemUser.this).getOnClickCall().invoke(friend.getUser());
                }
            });
            this.binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.friends.WidgetFriendsListAdapter$ItemUser$onConfigure$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetFriendsListAdapter.ItemUser.access$getAdapter$p(WidgetFriendsListAdapter.ItemUser.this).getOnClickChat().invoke(friend.getUser());
                }
            });
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetFriendsListAdapter(RecyclerView recyclerView) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recycler");
    }

    public final Function1<User, Unit> getOnClickAcceptFriend() {
        return this.onClickAcceptFriend;
    }

    public final Function1<User, Unit> getOnClickApproveSuggestion() {
        return this.onClickApproveSuggestion;
    }

    public final Function1<User, Unit> getOnClickCall() {
        return this.onClickCall;
    }

    public final Function1<User, Unit> getOnClickChat() {
        return this.onClickChat;
    }

    public final Function0<Unit> getOnClickContactSyncUpsell() {
        return this.onClickContactSyncUpsell;
    }

    public final Function1<View, Unit> getOnClickContactSyncUpsellLongClick() {
        return this.onClickContactSyncUpsellLongClick;
    }

    public final Function2<User, Integer, Unit> getOnClickDeclineFriend() {
        return this.onClickDeclineFriend;
    }

    public final Function0<Unit> getOnClickPendingHeaderExpand() {
        return this.onClickPendingHeaderExpand;
    }

    public final Function1<Long, Unit> getOnClickRemoveSuggestion() {
        return this.onClickRemoveSuggestion;
    }

    public final Function0<Unit> getOnClickSuggestedHeaderExpandCollapse() {
        return this.onClickSuggestedHeaderExpandCollapse;
    }

    public final Function2<View, User, Unit> getOnClickUserProfile() {
        return this.onClickUserProfile;
    }

    public final void setOnClickAcceptFriend(Function1<? super User, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClickAcceptFriend = function1;
    }

    public final void setOnClickApproveSuggestion(Function1<? super User, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClickApproveSuggestion = function1;
    }

    public final void setOnClickCall(Function1<? super User, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClickCall = function1;
    }

    public final void setOnClickChat(Function1<? super User, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClickChat = function1;
    }

    public final void setOnClickContactSyncUpsell(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onClickContactSyncUpsell = function0;
    }

    public final void setOnClickContactSyncUpsellLongClick(Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClickContactSyncUpsellLongClick = function1;
    }

    public final void setOnClickDeclineFriend(Function2<? super User, ? super Integer, Unit> function2) {
        m.checkNotNullParameter(function2, "<set-?>");
        this.onClickDeclineFriend = function2;
    }

    public final void setOnClickPendingHeaderExpand(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onClickPendingHeaderExpand = function0;
    }

    public final void setOnClickRemoveSuggestion(Function1<? super Long, Unit> function1) {
        m.checkNotNullParameter(function1, "<set-?>");
        this.onClickRemoveSuggestion = function1;
    }

    public final void setOnClickSuggestedHeaderExpandCollapse(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onClickSuggestedHeaderExpandCollapse = function0;
    }

    public final void setOnClickUserProfile(Function2<? super View, ? super User, Unit> function2) {
        m.checkNotNullParameter(function2, "<set-?>");
        this.onClickUserProfile = function2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<?, FriendsListViewModel.Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        switch (i) {
            case 0:
                return new ItemUser(this);
            case 1:
                return new ItemPendingUser(this);
            case 2:
                return new ItemPendingHeader(this);
            case 3:
                return new ItemHeader(this);
            case 4:
                return new ItemSuggestedFriendHeader(this);
            case 5:
                View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.widget_friends_list_adapter_suggested_friend, viewGroup, false);
                Objects.requireNonNull(inflate, "rootView");
                SuggestedFriendView suggestedFriendView = (SuggestedFriendView) inflate;
                WidgetFriendsListAdapterSuggestedFriendBinding widgetFriendsListAdapterSuggestedFriendBinding = new WidgetFriendsListAdapterSuggestedFriendBinding(suggestedFriendView, suggestedFriendView);
                m.checkNotNullExpressionValue(widgetFriendsListAdapterSuggestedFriendBinding, "WidgetFriendsListAdapter…          false\n        )");
                return new ItemSuggestedFriend(widgetFriendsListAdapterSuggestedFriendBinding, this);
            case 6:
                return new ItemContactSyncUpsell(this);
            default:
                throw invalidViewTypeException(i);
        }
    }
}
