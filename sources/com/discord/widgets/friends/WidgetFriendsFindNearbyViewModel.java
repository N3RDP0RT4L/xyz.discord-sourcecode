package com.discord.widgets.friends;

import andhook.lib.HookHelper;
import com.discord.app.AppViewModel;
import kotlin.Metadata;
import kotlin.Unit;
/* compiled from: WidgetFriendsFindNearbyViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\t\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007¢\u0006\u0004\b\u0011\u0010\u0012R$\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR$\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/friends/WidgetFriendsFindNearbyViewModel;", "Lcom/discord/app/AppViewModel;", "", "", "discriminator", "Ljava/lang/Integer;", "getDiscriminator", "()Ljava/lang/Integer;", "setDiscriminator", "(Ljava/lang/Integer;)V", "", "username", "Ljava/lang/String;", "getUsername", "()Ljava/lang/String;", "setUsername", "(Ljava/lang/String;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetFriendsFindNearbyViewModel extends AppViewModel<Unit> {
    private Integer discriminator;
    private String username;

    public WidgetFriendsFindNearbyViewModel() {
        super(null, 1, null);
    }

    public final Integer getDiscriminator() {
        return this.discriminator;
    }

    public final String getUsername() {
        return this.username;
    }

    public final void setDiscriminator(Integer num) {
        this.discriminator = num;
    }

    public final void setUsername(String str) {
        this.username = str;
    }
}
