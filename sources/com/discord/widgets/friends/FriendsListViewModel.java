package com.discord.widgets.friends;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.api.connectedaccounts.ConnectedAccount;
import com.discord.api.presence.ClientStatus;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.domain.ModelUserRelationship;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.friendsuggestions.FriendSuggestion;
import com.discord.models.presence.Presence;
import com.discord.models.user.User;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreContactSync;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreFriendSuggestions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserConnections;
import com.discord.stores.StoreUserPresence;
import com.discord.stores.StoreUserRelationships;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.collections.SnowflakePartitionMap;
import com.discord.utilities.error.Error;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.friends.FriendsListViewModel;
import d0.t.h0;
import d0.t.i0;
import d0.t.n;
import d0.t.n0;
import d0.t.q;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Ref$ObjectRef;
import rx.Emitter;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Cancellable;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: FriendsListViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¼\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 k2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0006klmnopB+\u0012\u000e\b\u0002\u0010`\u001a\b\u0012\u0004\u0012\u00020\u00170?\u0012\b\b\u0002\u0010Z\u001a\u00020Y\u0012\b\b\u0002\u0010e\u001a\u00020d¢\u0006\u0004\bi\u0010jJ\u0019\u0010\u0006\u001a\u00020\u00052\b\b\u0001\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001f\u0010\u000b\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001b\u0010\u0010\u001a\u00020\u00052\n\u0010\u000f\u001a\u00060\rj\u0002`\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J'\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u001f\u0010\u001f\u001a\u00020\u00052\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u001dH\u0003¢\u0006\u0004\b\u001f\u0010 J\u0019\u0010!\u001a\u00020\u00052\b\b\u0002\u0010\u001e\u001a\u00020\u001dH\u0003¢\u0006\u0004\b!\u0010\"J\u0093\u0001\u00100\u001a\u00020\u001b2\u001a\u0010&\u001a\u0016\u0012\b\u0012\u00060\rj\u0002`$\u0012\b\u0012\u00060\u0003j\u0002`%0#2\u0016\u0010(\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`$\u0012\u0004\u0012\u00020'0#2\u0016\u0010*\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`$\u0012\u0004\u0012\u00020)0#2\u0016\u0010,\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`$\u0012\u0004\u0012\u00020+0#2\u0006\u0010-\u001a\u00020\u001d2\u0016\u0010/\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`$\u0012\u0004\u0012\u00020.0#H\u0002¢\u0006\u0004\b0\u00101J\u001d\u00104\u001a\b\u0012\u0004\u0012\u000203022\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b4\u00105JO\u0010=\u001a\u00020<\"\u0004\b\u0000\u001062\f\u00108\u001a\b\u0012\u0004\u0012\u00028\u0000072\u0016\b\u0002\u0010:\u001a\u0010\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0005\u0018\u0001092\u0012\u0010;\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000509H\u0002¢\u0006\u0004\b=\u0010>J\u0015\u0010A\u001a\b\u0012\u0004\u0012\u00020@0?H\u0007¢\u0006\u0004\bA\u0010BJ!\u0010D\u001a\u00020\u00052\n\u0010C\u001a\u00060\rj\u0002`$2\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\bD\u0010EJ%\u0010G\u001a\u00020\u00052\n\u0010C\u001a\u00060\rj\u0002`$2\n\u0010F\u001a\u00060\u0003j\u0002`%¢\u0006\u0004\bG\u0010HJ)\u0010J\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u00032\n\b\u0002\u0010I\u001a\u0004\u0018\u00010\t¢\u0006\u0004\bJ\u0010KJ\u0019\u0010L\u001a\u00020\u00052\n\u0010C\u001a\u00060\rj\u0002`$¢\u0006\u0004\bL\u0010\u0011J\u0019\u0010M\u001a\u00020\u00052\n\u0010C\u001a\u00060\rj\u0002`$¢\u0006\u0004\bM\u0010\u0011J\u000f\u0010N\u001a\u00020\u0005H\u0007¢\u0006\u0004\bN\u0010OJ\u000f\u0010P\u001a\u00020\u0005H\u0007¢\u0006\u0004\bP\u0010OJ\u000f\u0010Q\u001a\u00020\u0005H\u0007¢\u0006\u0004\bQ\u0010OR\u0016\u0010R\u001a\u00020\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bR\u0010SR:\u0010V\u001a&\u0012\f\u0012\n U*\u0004\u0018\u00010@0@ U*\u0012\u0012\f\u0012\n U*\u0004\u0018\u00010@0@\u0018\u00010T0T8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bV\u0010WR\u0016\u0010\u001c\u001a\u00020\u001b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010XR\u0019\u0010Z\u001a\u00020Y8\u0006@\u0006¢\u0006\f\n\u0004\bZ\u0010[\u001a\u0004\b\\\u0010]R\u0018\u0010^\u001a\u0004\u0018\u00010<8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b^\u0010_R\u001f\u0010`\u001a\b\u0012\u0004\u0012\u00020\u00170?8\u0006@\u0006¢\u0006\f\n\u0004\b`\u0010a\u001a\u0004\bb\u0010BR\u0016\u0010c\u001a\u00020\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bc\u0010SR\u0019\u0010e\u001a\u00020d8\u0006@\u0006¢\u0006\f\n\u0004\be\u0010f\u001a\u0004\bg\u0010h¨\u0006q"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;", "", "stringRes", "", "emitShowToastEvent", "(I)V", "abortCode", "", "username", "emitShowFriendRequestAbortToast", "(ILjava/lang/String;)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "emitLaunchVoiceCallEvent", "(J)V", "Lcom/discord/utilities/error/Error;", "error", "discriminator", "emitCaptchaErrorEvent", "(Lcom/discord/utilities/error/Error;Ljava/lang/String;I)V", "Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V", "Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;", "listSections", "", "showContactSyncIcon", "handleComputedItems", "(Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;Z)V", "generateLoadedItems", "(Z)V", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/primitives/RelationshipType;", "relationships", "Lcom/discord/models/user/User;", "users", "Lcom/discord/models/presence/Presence;", "presences", "Lcom/discord/models/domain/ModelApplicationStream;", "applicationStreams", "showContactSyncUpsell", "Lcom/discord/models/friendsuggestions/FriendSuggestion;", "friendSuggestions", "getItems", "(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/util/Map;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;", "", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "getVisibleItems", "(Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;)Ljava/util/List;", ExifInterface.GPS_DIRECTION_TRUE, "Lkotlin/Function0;", "compute", "Lkotlin/Function1;", "onError", "onSuccess", "Lrx/functions/Cancellable;", "asyncComputeAndHandleOnUiThread", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lrx/functions/Cancellable;", "Lrx/Observable;", "Lcom/discord/widgets/friends/FriendsListViewModel$Event;", "observeEvents", "()Lrx/Observable;", "userId", "acceptFriendRequest", "(JLjava/lang/String;)V", "relationshipType", "removeFriendRequest", "(JI)V", "captchaKey", "acceptFriendSuggestion", "(Ljava/lang/String;ILjava/lang/String;)V", "ignoreSuggestion", "launchVoiceCall", "handleClickPendingHeader", "()V", "handleClickSuggestedHeader", "dismissContactSyncUpsell", "isSuggestedSectionExpanded", "Z", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "getStoreChannels", "()Lcom/discord/stores/StoreChannels;", "computeItemJob", "Lrx/functions/Cancellable;", "storeObservable", "Lrx/Observable;", "getStoreObservable", "isPendingSectionExpanded", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "getRestAPI", "()Lcom/discord/utilities/rest/RestAPI;", HookHelper.constructorName, "(Lrx/Observable;Lcom/discord/stores/StoreChannels;Lcom/discord/utilities/rest/RestAPI;)V", "Companion", "Event", "Item", "ListSections", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class FriendsListViewModel extends AppViewModel<ViewState> {
    private static final int COLLAPSED_ITEM_COUNT = 2;
    public static final Companion Companion = new Companion(null);
    private static final String LOCATION = "Friends List";
    private Cancellable computeItemJob;
    private final PublishSubject<Event> eventSubject;
    private boolean isPendingSectionExpanded;
    private boolean isSuggestedSectionExpanded;
    private ListSections listSections;
    private final RestAPI restAPI;
    private final StoreChannels storeChannels;
    private final Observable<StoreState> storeObservable;

    /* compiled from: FriendsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.friends.FriendsListViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            FriendsListViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: FriendsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u0013\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005R\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;", "observeStores", "()Lrx/Observable;", "", "COLLAPSED_ITEM_COUNT", "I", "", "LOCATION", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<StoreState> observeStores() {
            StoreStream.Companion companion = StoreStream.Companion;
            final StoreChannelsSelected channelsSelected = companion.getChannelsSelected();
            final StoreUserPresence presences = companion.getPresences();
            final StoreUser users = companion.getUsers();
            final StoreUserRelationships userRelationships = companion.getUserRelationships();
            final StoreUserConnections userConnections = companion.getUserConnections();
            final StoreApplicationStreaming applicationStreaming = companion.getApplicationStreaming();
            final StoreExperiments experiments = companion.getExperiments();
            final StoreContactSync contactSync = companion.getContactSync();
            final StoreFriendSuggestions friendSuggestions = companion.getFriendSuggestions();
            Observable<StoreState> F = ObservableExtensionsKt.leadingEdgeThrottle(ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{companion.getChannelsSelected(), companion.getPresences(), companion.getUsers(), companion.getUserRelationships(), companion.getApplicationStreaming(), companion.getUserConnections(), companion.getExperiments(), companion.getContactSync()}, false, null, null, 14, null), 1L, TimeUnit.SECONDS).F(new b<Unit, StoreState>() { // from class: com.discord.widgets.friends.FriendsListViewModel$Companion$observeStores$1
                public final FriendsListViewModel.StoreState call(Unit unit) {
                    boolean z2;
                    boolean z3;
                    boolean z4;
                    long id2 = StoreChannelsSelected.this.getId();
                    SnowflakePartitionMap.CopiablePartitionMap<Presence> presences2 = presences.getPresences();
                    Map<Long, User> users2 = users.getUsers();
                    StoreUserRelationships.UserRelationshipsState relationshipsState = userRelationships.getRelationshipsState();
                    Map<Long, ModelApplicationStream> streamsByUser = applicationStreaming.getStreamsByUser();
                    StoreUserConnections.State<ConnectedAccount> connectedAccounts = userConnections.getConnectedAccounts();
                    boolean z5 = connectedAccounts instanceof Collection;
                    if (!z5 || !connectedAccounts.isEmpty()) {
                        for (ConnectedAccount connectedAccount : connectedAccounts) {
                            if (m.areEqual(connectedAccount.g(), "contacts")) {
                                z2 = true;
                                break;
                            }
                        }
                    }
                    z2 = false;
                    if (!z5 || !connectedAccounts.isEmpty()) {
                        for (ConnectedAccount connectedAccount2 : connectedAccounts) {
                            if (!m.areEqual(connectedAccount2.g(), "contacts") || !connectedAccount2.a()) {
                                z4 = false;
                                continue;
                            } else {
                                z4 = true;
                                continue;
                            }
                            if (z4) {
                                z3 = true;
                                break;
                            }
                        }
                    }
                    z3 = false;
                    Experiment userExperiment = experiments.getUserExperiment("2021-04_contact_sync_android_main", !z2);
                    boolean z6 = userExperiment != null && userExperiment.getBucket() == 1 && !z2;
                    boolean z7 = !contactSync.getFriendsListUpsellDismissed() && (userExperiment != null && userExperiment.getBucket() == 1 && !z3);
                    return new FriendsListViewModel.StoreState(z6 && !z7, z7, id2, relationshipsState, users2, presences2, streamsByUser, (userExperiment == null || userExperiment.getBucket() != 1) ? h0.emptyMap() : friendSuggestions.getFriendSuggestions());
                }
            });
            m.checkNotNullExpressionValue(F, "ObservationDeckProvider\n…            )\n          }");
            return F;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: FriendsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\b\t\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Event;", "", HookHelper.constructorName, "()V", "CaptchaError", "LaunchVoiceCall", "ShowFriendRequestErrorToast", "ShowToast", "Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;", "Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;", "Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;", "Lcom/discord/widgets/friends/FriendsListViewModel$Event$CaptchaError;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0007J\u0010\u0010\u0011\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0011\u0010\nJ\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\n¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Event$CaptchaError;", "Lcom/discord/widgets/friends/FriendsListViewModel$Event;", "Lcom/discord/utilities/error/Error;", "component1", "()Lcom/discord/utilities/error/Error;", "", "component2", "()Ljava/lang/String;", "", "component3", "()I", "error", "username", "discriminator", "copy", "(Lcom/discord/utilities/error/Error;Ljava/lang/String;I)Lcom/discord/widgets/friends/FriendsListViewModel$Event$CaptchaError;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getUsername", "Lcom/discord/utilities/error/Error;", "getError", "I", "getDiscriminator", HookHelper.constructorName, "(Lcom/discord/utilities/error/Error;Ljava/lang/String;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CaptchaError extends Event {
            private final int discriminator;
            private final Error error;
            private final String username;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public CaptchaError(Error error, String str, int i) {
                super(null);
                m.checkNotNullParameter(error, "error");
                m.checkNotNullParameter(str, "username");
                this.error = error;
                this.username = str;
                this.discriminator = i;
            }

            public static /* synthetic */ CaptchaError copy$default(CaptchaError captchaError, Error error, String str, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    error = captchaError.error;
                }
                if ((i2 & 2) != 0) {
                    str = captchaError.username;
                }
                if ((i2 & 4) != 0) {
                    i = captchaError.discriminator;
                }
                return captchaError.copy(error, str, i);
            }

            public final Error component1() {
                return this.error;
            }

            public final String component2() {
                return this.username;
            }

            public final int component3() {
                return this.discriminator;
            }

            public final CaptchaError copy(Error error, String str, int i) {
                m.checkNotNullParameter(error, "error");
                m.checkNotNullParameter(str, "username");
                return new CaptchaError(error, str, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof CaptchaError)) {
                    return false;
                }
                CaptchaError captchaError = (CaptchaError) obj;
                return m.areEqual(this.error, captchaError.error) && m.areEqual(this.username, captchaError.username) && this.discriminator == captchaError.discriminator;
            }

            public final int getDiscriminator() {
                return this.discriminator;
            }

            public final Error getError() {
                return this.error;
            }

            public final String getUsername() {
                return this.username;
            }

            public int hashCode() {
                Error error = this.error;
                int i = 0;
                int hashCode = (error != null ? error.hashCode() : 0) * 31;
                String str = this.username;
                if (str != null) {
                    i = str.hashCode();
                }
                return ((hashCode + i) * 31) + this.discriminator;
            }

            public String toString() {
                StringBuilder R = a.R("CaptchaError(error=");
                R.append(this.error);
                R.append(", username=");
                R.append(this.username);
                R.append(", discriminator=");
                return a.A(R, this.discriminator, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;", "Lcom/discord/widgets/friends/FriendsListViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "channelId", "copy", "(J)Lcom/discord/widgets/friends/FriendsListViewModel$Event$LaunchVoiceCall;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchVoiceCall extends Event {
            private final long channelId;

            public LaunchVoiceCall(long j) {
                super(null);
                this.channelId = j;
            }

            public static /* synthetic */ LaunchVoiceCall copy$default(LaunchVoiceCall launchVoiceCall, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchVoiceCall.channelId;
                }
                return launchVoiceCall.copy(j);
            }

            public final long component1() {
                return this.channelId;
            }

            public final LaunchVoiceCall copy(long j) {
                return new LaunchVoiceCall(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof LaunchVoiceCall) && this.channelId == ((LaunchVoiceCall) obj).channelId;
                }
                return true;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public int hashCode() {
                return a0.a.a.b.a(this.channelId);
            }

            public String toString() {
                return a.B(a.R("LaunchVoiceCall(channelId="), this.channelId, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0013\u001a\u0004\b\u0014\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;", "Lcom/discord/widgets/friends/FriendsListViewModel$Event;", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "abortCode", "username", "copy", "(ILjava/lang/String;)Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowFriendRequestErrorToast;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getUsername", "I", "getAbortCode", HookHelper.constructorName, "(ILjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowFriendRequestErrorToast extends Event {
            private final int abortCode;
            private final String username;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ShowFriendRequestErrorToast(int i, String str) {
                super(null);
                m.checkNotNullParameter(str, "username");
                this.abortCode = i;
                this.username = str;
            }

            public static /* synthetic */ ShowFriendRequestErrorToast copy$default(ShowFriendRequestErrorToast showFriendRequestErrorToast, int i, String str, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showFriendRequestErrorToast.abortCode;
                }
                if ((i2 & 2) != 0) {
                    str = showFriendRequestErrorToast.username;
                }
                return showFriendRequestErrorToast.copy(i, str);
            }

            public final int component1() {
                return this.abortCode;
            }

            public final String component2() {
                return this.username;
            }

            public final ShowFriendRequestErrorToast copy(int i, String str) {
                m.checkNotNullParameter(str, "username");
                return new ShowFriendRequestErrorToast(i, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ShowFriendRequestErrorToast)) {
                    return false;
                }
                ShowFriendRequestErrorToast showFriendRequestErrorToast = (ShowFriendRequestErrorToast) obj;
                return this.abortCode == showFriendRequestErrorToast.abortCode && m.areEqual(this.username, showFriendRequestErrorToast.username);
            }

            public final int getAbortCode() {
                return this.abortCode;
            }

            public final String getUsername() {
                return this.username;
            }

            public int hashCode() {
                int i = this.abortCode * 31;
                String str = this.username;
                return i + (str != null ? str.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("ShowFriendRequestErrorToast(abortCode=");
                R.append(this.abortCode);
                R.append(", username=");
                return a.H(R, this.username, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;", "Lcom/discord/widgets/friends/FriendsListViewModel$Event;", "", "component1", "()I", "stringRes", "copy", "(I)Lcom/discord/widgets/friends/FriendsListViewModel$Event$ShowToast;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getStringRes", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowToast extends Event {
            private final int stringRes;

            public ShowToast(int i) {
                super(null);
                this.stringRes = i;
            }

            public static /* synthetic */ ShowToast copy$default(ShowToast showToast, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showToast.stringRes;
                }
                return showToast.copy(i);
            }

            public final int component1() {
                return this.stringRes;
            }

            public final ShowToast copy(int i) {
                return new ShowToast(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowToast) && this.stringRes == ((ShowToast) obj).stringRes;
                }
                return true;
            }

            public final int getStringRes() {
                return this.stringRes;
            }

            public int hashCode() {
                return this.stringRes;
            }

            public String toString() {
                return a.A(a.R("ShowToast(stringRes="), this.stringRes, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: FriendsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \t2\u00020\u0001:\b\t\n\u000b\f\r\u000e\u000f\u0010B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0007\u0011\u0012\u0013\u0014\u0015\u0016\u0017¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "type", "I", "getType", "()I", HookHelper.constructorName, "(I)V", "Companion", "ContactSyncUpsell", "Friend", Traits.Location.Section.HEADER, "PendingFriendRequest", "PendingHeader", "SuggestedFriend", "SuggestedFriendsHeader", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriendsHeader;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriend;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$ContactSyncUpsell;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item implements MGRecyclerDataPayload {
        public static final Companion Companion = new Companion(null);
        public static final int TYPE_CONTACT_SYNC_UPSELL = 6;
        public static final int TYPE_FRIEND = 0;
        public static final int TYPE_HEADER = 3;
        public static final int TYPE_PENDING_FRIEND = 1;
        public static final int TYPE_PENDING_HEADER = 2;
        public static final int TYPE_SUGGESTED_FRIEND = 5;
        public static final int TYPE_SUGGESTED_FRIEND_HEADER = 4;
        private final int type;

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004¨\u0006\r"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item$Companion;", "", "", "TYPE_CONTACT_SYNC_UPSELL", "I", "TYPE_FRIEND", "TYPE_HEADER", "TYPE_PENDING_FRIEND", "TYPE_PENDING_HEADER", "TYPE_SUGGESTED_FRIEND", "TYPE_SUGGESTED_FRIEND_HEADER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u00022\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\nR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item$ContactSyncUpsell;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "", "component1", "()Z", "dismissed", "copy", "(Z)Lcom/discord/widgets/friends/FriendsListViewModel$Item$ContactSyncUpsell;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Z", "getDismissed", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ContactSyncUpsell extends Item {
            private final boolean dismissed;
            private final String key = String.valueOf(getType());

            public ContactSyncUpsell(boolean z2) {
                super(6, null);
                this.dismissed = z2;
            }

            public static /* synthetic */ ContactSyncUpsell copy$default(ContactSyncUpsell contactSyncUpsell, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = contactSyncUpsell.dismissed;
                }
                return contactSyncUpsell.copy(z2);
            }

            public final boolean component1() {
                return this.dismissed;
            }

            public final ContactSyncUpsell copy(boolean z2) {
                return new ContactSyncUpsell(z2);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ContactSyncUpsell) && this.dismissed == ((ContactSyncUpsell) obj).dismissed;
                }
                return true;
            }

            public final boolean getDismissed() {
                return this.dismissed;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public int hashCode() {
                boolean z2 = this.dismissed;
                if (z2) {
                    return 1;
                }
                return z2 ? 1 : 0;
            }

            public String toString() {
                return a.M(a.R("ContactSyncUpsell(dismissed="), this.dismissed, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b#\u0010$J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000b\u0010\u0004J0\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\b2\b\b\u0002\u0010\u000e\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0019\u001a\u00020\u00022\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u00020\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0013R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\nR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010 \u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010!\u001a\u0004\b\"\u0010\u0007¨\u0006%"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "", "isOnline", "()Z", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/models/presence/Presence;", "component2", "()Lcom/discord/models/presence/Presence;", "component3", "user", "presence", "isApplicationStreaming", "copy", "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;Z)Lcom/discord/widgets/friends/FriendsListViewModel$Item$Friend;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/presence/Presence;", "getPresence", "Z", "Lcom/discord/models/user/User;", "getUser", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Friend extends Item {
            private final boolean isApplicationStreaming;
            private final String key;
            private final Presence presence;
            private final User user;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Friend(User user, Presence presence, boolean z2) {
                super(0, null);
                m.checkNotNullParameter(user, "user");
                this.user = user;
                this.presence = presence;
                this.isApplicationStreaming = z2;
                StringBuilder sb = new StringBuilder();
                sb.append(getType());
                sb.append(user.getId());
                this.key = sb.toString();
            }

            public static /* synthetic */ Friend copy$default(Friend friend, User user, Presence presence, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    user = friend.user;
                }
                if ((i & 2) != 0) {
                    presence = friend.presence;
                }
                if ((i & 4) != 0) {
                    z2 = friend.isApplicationStreaming;
                }
                return friend.copy(user, presence, z2);
            }

            public final User component1() {
                return this.user;
            }

            public final Presence component2() {
                return this.presence;
            }

            public final boolean component3() {
                return this.isApplicationStreaming;
            }

            public final Friend copy(User user, Presence presence, boolean z2) {
                m.checkNotNullParameter(user, "user");
                return new Friend(user, presence, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Friend)) {
                    return false;
                }
                Friend friend = (Friend) obj;
                return m.areEqual(this.user, friend.user) && m.areEqual(this.presence, friend.presence) && this.isApplicationStreaming == friend.isApplicationStreaming;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final Presence getPresence() {
                return this.presence;
            }

            public final User getUser() {
                return this.user;
            }

            public int hashCode() {
                User user = this.user;
                int i = 0;
                int hashCode = (user != null ? user.hashCode() : 0) * 31;
                Presence presence = this.presence;
                if (presence != null) {
                    i = presence.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.isApplicationStreaming;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public final boolean isApplicationStreaming() {
                return this.isApplicationStreaming;
            }

            public final boolean isOnline() {
                return this.presence != null && n0.setOf((Object[]) new ClientStatus[]{ClientStatus.ONLINE, ClientStatus.IDLE, ClientStatus.DND}).contains(this.presence.getStatus());
            }

            public String toString() {
                StringBuilder R = a.R("Friend(user=");
                R.append(this.user);
                R.append(", presence=");
                R.append(this.presence);
                R.append(", isApplicationStreaming=");
                return a.M(R, this.isApplicationStreaming, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0003\u0010\u0006\u001a\u00020\u00022\b\b\u0002\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\n8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\fR\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0016\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "", "component1", "()I", "component2", "titleStringResId", "count", "copy", "(II)Lcom/discord/widgets/friends/FriendsListViewModel$Item$Header;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "I", "getTitleStringResId", "getCount", HookHelper.constructorName, "(II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Header extends Item {
            private final int count;
            private final String key;
            private final int titleStringResId;

            public Header(@StringRes int i, int i2) {
                super(3, null);
                this.titleStringResId = i;
                this.count = i2;
                StringBuilder sb = new StringBuilder();
                sb.append(getType());
                sb.append(i);
                this.key = sb.toString();
            }

            public static /* synthetic */ Header copy$default(Header header, int i, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    i = header.titleStringResId;
                }
                if ((i3 & 2) != 0) {
                    i2 = header.count;
                }
                return header.copy(i, i2);
            }

            public final int component1() {
                return this.titleStringResId;
            }

            public final int component2() {
                return this.count;
            }

            public final Header copy(@StringRes int i, int i2) {
                return new Header(i, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Header)) {
                    return false;
                }
                Header header = (Header) obj;
                return this.titleStringResId == header.titleStringResId && this.count == header.count;
            }

            public final int getCount() {
                return this.count;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final int getTitleStringResId() {
                return this.titleStringResId;
            }

            public int hashCode() {
                return (this.titleStringResId * 31) + this.count;
            }

            public String toString() {
                StringBuilder R = a.R("Header(titleStringResId=");
                R.append(this.titleStringResId);
                R.append(", count=");
                return a.A(R, this.count, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\n\u0010\u000e\u001a\u00060\bj\u0002`\t¢\u0006\u0004\b#\u0010$J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0014\u0010\n\u001a\u00060\bj\u0002`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ4\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\f\b\u0002\u0010\u000e\u001a\u00060\bj\u0002`\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0014\u0010\u000bJ\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u00020\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0013R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\u0007R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001f\u001a\u0004\b \u0010\u0004R\u001d\u0010\u000e\u001a\u00060\bj\u0002`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010!\u001a\u0004\b\"\u0010\u000b¨\u0006%"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/models/presence/Presence;", "component2", "()Lcom/discord/models/presence/Presence;", "", "Lcom/discord/primitives/RelationshipType;", "component3", "()I", "user", "presence", "relationshipType", "copy", "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;I)Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingFriendRequest;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/presence/Presence;", "getPresence", "Lcom/discord/models/user/User;", "getUser", "I", "getRelationshipType", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/models/presence/Presence;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PendingFriendRequest extends Item {
            private final String key;
            private final Presence presence;
            private final int relationshipType;
            private final User user;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public PendingFriendRequest(User user, Presence presence, int i) {
                super(1, null);
                m.checkNotNullParameter(user, "user");
                this.user = user;
                this.presence = presence;
                this.relationshipType = i;
                StringBuilder sb = new StringBuilder();
                sb.append(getType());
                sb.append(user.getId());
                this.key = sb.toString();
            }

            public static /* synthetic */ PendingFriendRequest copy$default(PendingFriendRequest pendingFriendRequest, User user, Presence presence, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    user = pendingFriendRequest.user;
                }
                if ((i2 & 2) != 0) {
                    presence = pendingFriendRequest.presence;
                }
                if ((i2 & 4) != 0) {
                    i = pendingFriendRequest.relationshipType;
                }
                return pendingFriendRequest.copy(user, presence, i);
            }

            public final User component1() {
                return this.user;
            }

            public final Presence component2() {
                return this.presence;
            }

            public final int component3() {
                return this.relationshipType;
            }

            public final PendingFriendRequest copy(User user, Presence presence, int i) {
                m.checkNotNullParameter(user, "user");
                return new PendingFriendRequest(user, presence, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof PendingFriendRequest)) {
                    return false;
                }
                PendingFriendRequest pendingFriendRequest = (PendingFriendRequest) obj;
                return m.areEqual(this.user, pendingFriendRequest.user) && m.areEqual(this.presence, pendingFriendRequest.presence) && this.relationshipType == pendingFriendRequest.relationshipType;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final Presence getPresence() {
                return this.presence;
            }

            public final int getRelationshipType() {
                return this.relationshipType;
            }

            public final User getUser() {
                return this.user;
            }

            public int hashCode() {
                User user = this.user;
                int i = 0;
                int hashCode = (user != null ? user.hashCode() : 0) * 31;
                Presence presence = this.presence;
                if (presence != null) {
                    i = presence.hashCode();
                }
                return ((hashCode + i) * 31) + this.relationshipType;
            }

            public String toString() {
                StringBuilder R = a.R("PendingFriendRequest(user=");
                R.append(this.user);
                R.append(", presence=");
                R.append(this.presence);
                R.append(", relationshipType=");
                return a.A(R, this.relationshipType, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\b\b\u0001\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0006\u0012\u0006\u0010\r\u001a\u00020\u0006¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ8\u0010\u000e\u001a\u00020\u00002\b\b\u0003\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00062\b\b\u0002\u0010\r\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0004J\u001a\u0010\u0016\u001a\u00020\u00062\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\r\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\f\u0010\bR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001c\u0010\u0004R\u001c\u0010\u001d\u001a\u00020\u00108\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010\u0012¨\u0006\""}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "", "component1", "()I", "component2", "", "component3", "()Z", "component4", "titleStringResId", "count", "isPendingSectionExpanded", "showExpandButton", "copy", "(IIZZ)Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowExpandButton", "I", "getTitleStringResId", "getCount", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(IIZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PendingHeader extends Item {
            private final int count;
            private final boolean isPendingSectionExpanded;
            private final String key = String.valueOf(getType());
            private final boolean showExpandButton;
            private final int titleStringResId;

            public PendingHeader(@StringRes int i, int i2, boolean z2, boolean z3) {
                super(2, null);
                this.titleStringResId = i;
                this.count = i2;
                this.isPendingSectionExpanded = z2;
                this.showExpandButton = z3;
            }

            public static /* synthetic */ PendingHeader copy$default(PendingHeader pendingHeader, int i, int i2, boolean z2, boolean z3, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    i = pendingHeader.titleStringResId;
                }
                if ((i3 & 2) != 0) {
                    i2 = pendingHeader.count;
                }
                if ((i3 & 4) != 0) {
                    z2 = pendingHeader.isPendingSectionExpanded;
                }
                if ((i3 & 8) != 0) {
                    z3 = pendingHeader.showExpandButton;
                }
                return pendingHeader.copy(i, i2, z2, z3);
            }

            public final int component1() {
                return this.titleStringResId;
            }

            public final int component2() {
                return this.count;
            }

            public final boolean component3() {
                return this.isPendingSectionExpanded;
            }

            public final boolean component4() {
                return this.showExpandButton;
            }

            public final PendingHeader copy(@StringRes int i, int i2, boolean z2, boolean z3) {
                return new PendingHeader(i, i2, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof PendingHeader)) {
                    return false;
                }
                PendingHeader pendingHeader = (PendingHeader) obj;
                return this.titleStringResId == pendingHeader.titleStringResId && this.count == pendingHeader.count && this.isPendingSectionExpanded == pendingHeader.isPendingSectionExpanded && this.showExpandButton == pendingHeader.showExpandButton;
            }

            public final int getCount() {
                return this.count;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final boolean getShowExpandButton() {
                return this.showExpandButton;
            }

            public final int getTitleStringResId() {
                return this.titleStringResId;
            }

            public int hashCode() {
                int i = ((this.titleStringResId * 31) + this.count) * 31;
                boolean z2 = this.isPendingSectionExpanded;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (i + i3) * 31;
                boolean z3 = this.showExpandButton;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                return i5 + i2;
            }

            public final boolean isPendingSectionExpanded() {
                return this.isPendingSectionExpanded;
            }

            public String toString() {
                StringBuilder R = a.R("PendingHeader(titleStringResId=");
                R.append(this.titleStringResId);
                R.append(", count=");
                R.append(this.count);
                R.append(", isPendingSectionExpanded=");
                R.append(this.isPendingSectionExpanded);
                R.append(", showExpandButton=");
                return a.M(R, this.showExpandButton, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\nR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriend;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "Lcom/discord/models/friendsuggestions/FriendSuggestion;", "component1", "()Lcom/discord/models/friendsuggestions/FriendSuggestion;", "suggestion", "copy", "(Lcom/discord/models/friendsuggestions/FriendSuggestion;)Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriend;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/friendsuggestions/FriendSuggestion;", "getSuggestion", HookHelper.constructorName, "(Lcom/discord/models/friendsuggestions/FriendSuggestion;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SuggestedFriend extends Item {
            private final String key;
            private final FriendSuggestion suggestion;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SuggestedFriend(FriendSuggestion friendSuggestion) {
                super(5, null);
                m.checkNotNullParameter(friendSuggestion, "suggestion");
                this.suggestion = friendSuggestion;
                this.key = getType() + " -- " + friendSuggestion.getUser().getId();
            }

            public static /* synthetic */ SuggestedFriend copy$default(SuggestedFriend suggestedFriend, FriendSuggestion friendSuggestion, int i, Object obj) {
                if ((i & 1) != 0) {
                    friendSuggestion = suggestedFriend.suggestion;
                }
                return suggestedFriend.copy(friendSuggestion);
            }

            public final FriendSuggestion component1() {
                return this.suggestion;
            }

            public final SuggestedFriend copy(FriendSuggestion friendSuggestion) {
                m.checkNotNullParameter(friendSuggestion, "suggestion");
                return new SuggestedFriend(friendSuggestion);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SuggestedFriend) && m.areEqual(this.suggestion, ((SuggestedFriend) obj).suggestion);
                }
                return true;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final FriendSuggestion getSuggestion() {
                return this.suggestion;
            }

            public int hashCode() {
                FriendSuggestion friendSuggestion = this.suggestion;
                if (friendSuggestion != null) {
                    return friendSuggestion.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("SuggestedFriend(suggestion=");
                R.append(this.suggestion);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u001a\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u001c\u0010\u0018\u001a\u00020\u000e8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u0010R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001b\u001a\u0004\b\n\u0010\u0007¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriendsHeader;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "", "component1", "()I", "", "component2", "()Z", "component3", "count", "isExpanded", "showExpandButton", "copy", "(IZZ)Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriendsHeader;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getCount", "key", "Ljava/lang/String;", "getKey", "Z", "getShowExpandButton", HookHelper.constructorName, "(IZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SuggestedFriendsHeader extends Item {
            private final int count;
            private final boolean isExpanded;
            private final String key = String.valueOf(getType());
            private final boolean showExpandButton;

            public SuggestedFriendsHeader(int i, boolean z2, boolean z3) {
                super(4, null);
                this.count = i;
                this.isExpanded = z2;
                this.showExpandButton = z3;
            }

            public static /* synthetic */ SuggestedFriendsHeader copy$default(SuggestedFriendsHeader suggestedFriendsHeader, int i, boolean z2, boolean z3, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = suggestedFriendsHeader.count;
                }
                if ((i2 & 2) != 0) {
                    z2 = suggestedFriendsHeader.isExpanded;
                }
                if ((i2 & 4) != 0) {
                    z3 = suggestedFriendsHeader.showExpandButton;
                }
                return suggestedFriendsHeader.copy(i, z2, z3);
            }

            public final int component1() {
                return this.count;
            }

            public final boolean component2() {
                return this.isExpanded;
            }

            public final boolean component3() {
                return this.showExpandButton;
            }

            public final SuggestedFriendsHeader copy(int i, boolean z2, boolean z3) {
                return new SuggestedFriendsHeader(i, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof SuggestedFriendsHeader)) {
                    return false;
                }
                SuggestedFriendsHeader suggestedFriendsHeader = (SuggestedFriendsHeader) obj;
                return this.count == suggestedFriendsHeader.count && this.isExpanded == suggestedFriendsHeader.isExpanded && this.showExpandButton == suggestedFriendsHeader.showExpandButton;
            }

            public final int getCount() {
                return this.count;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final boolean getShowExpandButton() {
                return this.showExpandButton;
            }

            public int hashCode() {
                int i = this.count * 31;
                boolean z2 = this.isExpanded;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (i + i3) * 31;
                boolean z3 = this.showExpandButton;
                if (!z3) {
                    i2 = z3 ? 1 : 0;
                }
                return i5 + i2;
            }

            public final boolean isExpanded() {
                return this.isExpanded;
            }

            public String toString() {
                StringBuilder R = a.R("SuggestedFriendsHeader(count=");
                R.append(this.count);
                R.append(", isExpanded=");
                R.append(this.isExpanded);
                R.append(", showExpandButton=");
                return a.M(R, this.showExpandButton, ")");
            }
        }

        private Item(int i) {
            this.type = i;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public /* synthetic */ Item(int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(i);
        }
    }

    /* compiled from: FriendsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000f\b\u0082\b\u0018\u00002\u00020\u0001BO\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\t\u0012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u0005\u0012\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\f0\u0005\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u000f¢\u0006\u0004\b.\u0010/J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u0005HÆ\u0003¢\u0006\u0004\b\r\u0010\bJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\f0\u0005HÆ\u0003¢\u0006\u0004\b\u000e\u0010\bJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011Jd\u0010\u0018\u001a\u00020\u00002\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00022\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\t2\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u00052\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\f0\u00052\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u000fHÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010\"\u001a\u00020!2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010$\u001a\u0004\b%\u0010\u0011R\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b'\u0010\bR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010(\u001a\u0004\b)\u0010\u0004R\u001f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\f0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b*\u0010\bR\u001b\u0010\u0014\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010+\u001a\u0004\b,\u0010\u000bR\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b-\u0010\b¨\u00060"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;", "", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriendsHeader;", "component1", "()Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriendsHeader;", "", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriend;", "component2", "()Ljava/util/List;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;", "component3", "()Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "component4", "component5", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$ContactSyncUpsell;", "component6", "()Lcom/discord/widgets/friends/FriendsListViewModel$Item$ContactSyncUpsell;", "suggestionsHeaderItem", "suggestedFriendItems", "pendingHeaderItem", "pendingItems", "friendsItemsWithHeaders", "contactSyncUpsell", "copy", "(Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriendsHeader;Ljava/util/List;Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/friends/FriendsListViewModel$Item$ContactSyncUpsell;)Lcom/discord/widgets/friends/FriendsListViewModel$ListSections;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$ContactSyncUpsell;", "getContactSyncUpsell", "Ljava/util/List;", "getSuggestedFriendItems", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriendsHeader;", "getSuggestionsHeaderItem", "getFriendsItemsWithHeaders", "Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;", "getPendingHeaderItem", "getPendingItems", HookHelper.constructorName, "(Lcom/discord/widgets/friends/FriendsListViewModel$Item$SuggestedFriendsHeader;Ljava/util/List;Lcom/discord/widgets/friends/FriendsListViewModel$Item$PendingHeader;Ljava/util/List;Ljava/util/List;Lcom/discord/widgets/friends/FriendsListViewModel$Item$ContactSyncUpsell;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ListSections {
        private final Item.ContactSyncUpsell contactSyncUpsell;
        private final List<Item> friendsItemsWithHeaders;
        private final Item.PendingHeader pendingHeaderItem;
        private final List<Item> pendingItems;
        private final List<Item.SuggestedFriend> suggestedFriendItems;
        private final Item.SuggestedFriendsHeader suggestionsHeaderItem;

        /* JADX WARN: Multi-variable type inference failed */
        public ListSections(Item.SuggestedFriendsHeader suggestedFriendsHeader, List<Item.SuggestedFriend> list, Item.PendingHeader pendingHeader, List<? extends Item> list2, List<? extends Item> list3, Item.ContactSyncUpsell contactSyncUpsell) {
            m.checkNotNullParameter(list, "suggestedFriendItems");
            m.checkNotNullParameter(list2, "pendingItems");
            m.checkNotNullParameter(list3, "friendsItemsWithHeaders");
            this.suggestionsHeaderItem = suggestedFriendsHeader;
            this.suggestedFriendItems = list;
            this.pendingHeaderItem = pendingHeader;
            this.pendingItems = list2;
            this.friendsItemsWithHeaders = list3;
            this.contactSyncUpsell = contactSyncUpsell;
        }

        public static /* synthetic */ ListSections copy$default(ListSections listSections, Item.SuggestedFriendsHeader suggestedFriendsHeader, List list, Item.PendingHeader pendingHeader, List list2, List list3, Item.ContactSyncUpsell contactSyncUpsell, int i, Object obj) {
            if ((i & 1) != 0) {
                suggestedFriendsHeader = listSections.suggestionsHeaderItem;
            }
            List<Item.SuggestedFriend> list4 = list;
            if ((i & 2) != 0) {
                list4 = listSections.suggestedFriendItems;
            }
            List list5 = list4;
            if ((i & 4) != 0) {
                pendingHeader = listSections.pendingHeaderItem;
            }
            Item.PendingHeader pendingHeader2 = pendingHeader;
            List<Item> list6 = list2;
            if ((i & 8) != 0) {
                list6 = listSections.pendingItems;
            }
            List list7 = list6;
            List<Item> list8 = list3;
            if ((i & 16) != 0) {
                list8 = listSections.friendsItemsWithHeaders;
            }
            List list9 = list8;
            if ((i & 32) != 0) {
                contactSyncUpsell = listSections.contactSyncUpsell;
            }
            return listSections.copy(suggestedFriendsHeader, list5, pendingHeader2, list7, list9, contactSyncUpsell);
        }

        public final Item.SuggestedFriendsHeader component1() {
            return this.suggestionsHeaderItem;
        }

        public final List<Item.SuggestedFriend> component2() {
            return this.suggestedFriendItems;
        }

        public final Item.PendingHeader component3() {
            return this.pendingHeaderItem;
        }

        public final List<Item> component4() {
            return this.pendingItems;
        }

        public final List<Item> component5() {
            return this.friendsItemsWithHeaders;
        }

        public final Item.ContactSyncUpsell component6() {
            return this.contactSyncUpsell;
        }

        public final ListSections copy(Item.SuggestedFriendsHeader suggestedFriendsHeader, List<Item.SuggestedFriend> list, Item.PendingHeader pendingHeader, List<? extends Item> list2, List<? extends Item> list3, Item.ContactSyncUpsell contactSyncUpsell) {
            m.checkNotNullParameter(list, "suggestedFriendItems");
            m.checkNotNullParameter(list2, "pendingItems");
            m.checkNotNullParameter(list3, "friendsItemsWithHeaders");
            return new ListSections(suggestedFriendsHeader, list, pendingHeader, list2, list3, contactSyncUpsell);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ListSections)) {
                return false;
            }
            ListSections listSections = (ListSections) obj;
            return m.areEqual(this.suggestionsHeaderItem, listSections.suggestionsHeaderItem) && m.areEqual(this.suggestedFriendItems, listSections.suggestedFriendItems) && m.areEqual(this.pendingHeaderItem, listSections.pendingHeaderItem) && m.areEqual(this.pendingItems, listSections.pendingItems) && m.areEqual(this.friendsItemsWithHeaders, listSections.friendsItemsWithHeaders) && m.areEqual(this.contactSyncUpsell, listSections.contactSyncUpsell);
        }

        public final Item.ContactSyncUpsell getContactSyncUpsell() {
            return this.contactSyncUpsell;
        }

        public final List<Item> getFriendsItemsWithHeaders() {
            return this.friendsItemsWithHeaders;
        }

        public final Item.PendingHeader getPendingHeaderItem() {
            return this.pendingHeaderItem;
        }

        public final List<Item> getPendingItems() {
            return this.pendingItems;
        }

        public final List<Item.SuggestedFriend> getSuggestedFriendItems() {
            return this.suggestedFriendItems;
        }

        public final Item.SuggestedFriendsHeader getSuggestionsHeaderItem() {
            return this.suggestionsHeaderItem;
        }

        public int hashCode() {
            Item.SuggestedFriendsHeader suggestedFriendsHeader = this.suggestionsHeaderItem;
            int i = 0;
            int hashCode = (suggestedFriendsHeader != null ? suggestedFriendsHeader.hashCode() : 0) * 31;
            List<Item.SuggestedFriend> list = this.suggestedFriendItems;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            Item.PendingHeader pendingHeader = this.pendingHeaderItem;
            int hashCode3 = (hashCode2 + (pendingHeader != null ? pendingHeader.hashCode() : 0)) * 31;
            List<Item> list2 = this.pendingItems;
            int hashCode4 = (hashCode3 + (list2 != null ? list2.hashCode() : 0)) * 31;
            List<Item> list3 = this.friendsItemsWithHeaders;
            int hashCode5 = (hashCode4 + (list3 != null ? list3.hashCode() : 0)) * 31;
            Item.ContactSyncUpsell contactSyncUpsell = this.contactSyncUpsell;
            if (contactSyncUpsell != null) {
                i = contactSyncUpsell.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            StringBuilder R = a.R("ListSections(suggestionsHeaderItem=");
            R.append(this.suggestionsHeaderItem);
            R.append(", suggestedFriendItems=");
            R.append(this.suggestedFriendItems);
            R.append(", pendingHeaderItem=");
            R.append(this.pendingHeaderItem);
            R.append(", pendingItems=");
            R.append(this.pendingItems);
            R.append(", friendsItemsWithHeaders=");
            R.append(this.friendsItemsWithHeaders);
            R.append(", contactSyncUpsell=");
            R.append(this.contactSyncUpsell);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: FriendsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001B\u008b\u0001\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\n\u0010\u001a\u001a\u00060\u0006j\u0002`\u0007\u0012\u0006\u0010\u001b\u001a\u00020\n\u0012\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\r\u0012\u0016\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00120\r\u0012\u0016\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00140\r\u0012\u0016\u0010\u001f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00160\r¢\u0006\u0004\b7\u00108J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0014\u0010\b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ \u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J \u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00120\rHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0011J \u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00140\rHÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0011J \u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00160\rHÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0011J¤\u0001\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u00022\b\b\u0002\u0010\u0019\u001a\u00020\u00022\f\b\u0002\u0010\u001a\u001a\u00060\u0006j\u0002`\u00072\b\b\u0002\u0010\u001b\u001a\u00020\n2\u0018\b\u0002\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\r2\u0018\b\u0002\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00120\r2\u0018\b\u0002\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00140\r2\u0018\b\u0002\u0010\u001f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00160\rHÆ\u0001¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÖ\u0001¢\u0006\u0004\b#\u0010$J\u0010\u0010&\u001a\u00020%HÖ\u0001¢\u0006\u0004\b&\u0010'J\u001a\u0010)\u001a\u00020\u00022\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b)\u0010*R)\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010+\u001a\u0004\b,\u0010\u0011R\u001d\u0010\u001a\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010-\u001a\u0004\b.\u0010\tR\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010/\u001a\u0004\b0\u0010\u0004R)\u0010\u001f\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00160\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010+\u001a\u0004\b1\u0010\u0011R)\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00140\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010+\u001a\u0004\b2\u0010\u0011R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010/\u001a\u0004\b3\u0010\u0004R)\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u00120\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010+\u001a\u0004\b4\u0010\u0011R\u0019\u0010\u001b\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00105\u001a\u0004\b6\u0010\f¨\u00069"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;", "", "", "component1", "()Z", "component2", "", "Lcom/discord/primitives/ChannelId;", "component3", "()J", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", "component4", "()Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component5", "()Ljava/util/Map;", "Lcom/discord/models/presence/Presence;", "component6", "Lcom/discord/models/domain/ModelApplicationStream;", "component7", "Lcom/discord/models/friendsuggestions/FriendSuggestion;", "component8", "showContactSyncIcon", "showContactSyncUpsell", "channelId", "relationshipsState", "users", "presences", "applicationStreams", "friendSuggestions", "copy", "(ZZJLcom/discord/stores/StoreUserRelationships$UserRelationshipsState;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/friends/FriendsListViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getUsers", "J", "getChannelId", "Z", "getShowContactSyncIcon", "getFriendSuggestions", "getApplicationStreams", "getShowContactSyncUpsell", "getPresences", "Lcom/discord/stores/StoreUserRelationships$UserRelationshipsState;", "getRelationshipsState", HookHelper.constructorName, "(ZZJLcom/discord/stores/StoreUserRelationships$UserRelationshipsState;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, ModelApplicationStream> applicationStreams;
        private final long channelId;
        private final Map<Long, FriendSuggestion> friendSuggestions;
        private final Map<Long, Presence> presences;
        private final StoreUserRelationships.UserRelationshipsState relationshipsState;
        private final boolean showContactSyncIcon;
        private final boolean showContactSyncUpsell;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(boolean z2, boolean z3, long j, StoreUserRelationships.UserRelationshipsState userRelationshipsState, Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, ? extends ModelApplicationStream> map3, Map<Long, FriendSuggestion> map4) {
            m.checkNotNullParameter(userRelationshipsState, "relationshipsState");
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(map2, "presences");
            m.checkNotNullParameter(map3, "applicationStreams");
            m.checkNotNullParameter(map4, "friendSuggestions");
            this.showContactSyncIcon = z2;
            this.showContactSyncUpsell = z3;
            this.channelId = j;
            this.relationshipsState = userRelationshipsState;
            this.users = map;
            this.presences = map2;
            this.applicationStreams = map3;
            this.friendSuggestions = map4;
        }

        public final boolean component1() {
            return this.showContactSyncIcon;
        }

        public final boolean component2() {
            return this.showContactSyncUpsell;
        }

        public final long component3() {
            return this.channelId;
        }

        public final StoreUserRelationships.UserRelationshipsState component4() {
            return this.relationshipsState;
        }

        public final Map<Long, User> component5() {
            return this.users;
        }

        public final Map<Long, Presence> component6() {
            return this.presences;
        }

        public final Map<Long, ModelApplicationStream> component7() {
            return this.applicationStreams;
        }

        public final Map<Long, FriendSuggestion> component8() {
            return this.friendSuggestions;
        }

        public final StoreState copy(boolean z2, boolean z3, long j, StoreUserRelationships.UserRelationshipsState userRelationshipsState, Map<Long, ? extends User> map, Map<Long, Presence> map2, Map<Long, ? extends ModelApplicationStream> map3, Map<Long, FriendSuggestion> map4) {
            m.checkNotNullParameter(userRelationshipsState, "relationshipsState");
            m.checkNotNullParameter(map, "users");
            m.checkNotNullParameter(map2, "presences");
            m.checkNotNullParameter(map3, "applicationStreams");
            m.checkNotNullParameter(map4, "friendSuggestions");
            return new StoreState(z2, z3, j, userRelationshipsState, map, map2, map3, map4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return this.showContactSyncIcon == storeState.showContactSyncIcon && this.showContactSyncUpsell == storeState.showContactSyncUpsell && this.channelId == storeState.channelId && m.areEqual(this.relationshipsState, storeState.relationshipsState) && m.areEqual(this.users, storeState.users) && m.areEqual(this.presences, storeState.presences) && m.areEqual(this.applicationStreams, storeState.applicationStreams) && m.areEqual(this.friendSuggestions, storeState.friendSuggestions);
        }

        public final Map<Long, ModelApplicationStream> getApplicationStreams() {
            return this.applicationStreams;
        }

        public final long getChannelId() {
            return this.channelId;
        }

        public final Map<Long, FriendSuggestion> getFriendSuggestions() {
            return this.friendSuggestions;
        }

        public final Map<Long, Presence> getPresences() {
            return this.presences;
        }

        public final StoreUserRelationships.UserRelationshipsState getRelationshipsState() {
            return this.relationshipsState;
        }

        public final boolean getShowContactSyncIcon() {
            return this.showContactSyncIcon;
        }

        public final boolean getShowContactSyncUpsell() {
            return this.showContactSyncUpsell;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            boolean z2 = this.showContactSyncIcon;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.showContactSyncUpsell;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            int a = (a0.a.a.b.a(this.channelId) + ((i4 + i) * 31)) * 31;
            StoreUserRelationships.UserRelationshipsState userRelationshipsState = this.relationshipsState;
            int i5 = 0;
            int hashCode = (a + (userRelationshipsState != null ? userRelationshipsState.hashCode() : 0)) * 31;
            Map<Long, User> map = this.users;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Presence> map2 = this.presences;
            int hashCode3 = (hashCode2 + (map2 != null ? map2.hashCode() : 0)) * 31;
            Map<Long, ModelApplicationStream> map3 = this.applicationStreams;
            int hashCode4 = (hashCode3 + (map3 != null ? map3.hashCode() : 0)) * 31;
            Map<Long, FriendSuggestion> map4 = this.friendSuggestions;
            if (map4 != null) {
                i5 = map4.hashCode();
            }
            return hashCode4 + i5;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(showContactSyncIcon=");
            R.append(this.showContactSyncIcon);
            R.append(", showContactSyncUpsell=");
            R.append(this.showContactSyncUpsell);
            R.append(", channelId=");
            R.append(this.channelId);
            R.append(", relationshipsState=");
            R.append(this.relationshipsState);
            R.append(", users=");
            R.append(this.users);
            R.append(", presences=");
            R.append(this.presences);
            R.append(", applicationStreams=");
            R.append(this.applicationStreams);
            R.append(", friendSuggestions=");
            return a.L(R, this.friendSuggestions, ")");
        }
    }

    /* compiled from: FriendsListViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Empty", "Loaded", "Uninitialized", "Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;", "Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u00022\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;", "Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;", "", "component1", "()Z", "showContactSyncIcon", "copy", "(Z)Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Empty;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowContactSyncIcon", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Empty extends ViewState {
            private final boolean showContactSyncIcon;

            public Empty(boolean z2) {
                super(null);
                this.showContactSyncIcon = z2;
            }

            public static /* synthetic */ Empty copy$default(Empty empty, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = empty.showContactSyncIcon;
                }
                return empty.copy(z2);
            }

            public final boolean component1() {
                return this.showContactSyncIcon;
            }

            public final Empty copy(boolean z2) {
                return new Empty(z2);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Empty) && this.showContactSyncIcon == ((Empty) obj).showContactSyncIcon;
                }
                return true;
            }

            public final boolean getShowContactSyncIcon() {
                return this.showContactSyncIcon;
            }

            public int hashCode() {
                boolean z2 = this.showContactSyncIcon;
                if (z2) {
                    return 1;
                }
                return z2 ? 1 : 0;
            }

            public String toString() {
                return a.M(a.R("Empty(showContactSyncIcon="), this.showContactSyncIcon, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ*\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00022\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\b¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;", "Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;", "", "component1", "()Z", "", "Lcom/discord/widgets/friends/FriendsListViewModel$Item;", "component2", "()Ljava/util/List;", "showContactSyncIcon", "items", "copy", "(ZLjava/util/List;)Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowContactSyncIcon", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(ZLjava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final List<Item> items;
            private final boolean showContactSyncIcon;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(boolean z2, List<? extends Item> list) {
                super(null);
                m.checkNotNullParameter(list, "items");
                this.showContactSyncIcon = z2;
                this.items = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, boolean z2, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = loaded.showContactSyncIcon;
                }
                if ((i & 2) != 0) {
                    list = loaded.items;
                }
                return loaded.copy(z2, list);
            }

            public final boolean component1() {
                return this.showContactSyncIcon;
            }

            public final List<Item> component2() {
                return this.items;
            }

            public final Loaded copy(boolean z2, List<? extends Item> list) {
                m.checkNotNullParameter(list, "items");
                return new Loaded(z2, list);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return this.showContactSyncIcon == loaded.showContactSyncIcon && m.areEqual(this.items, loaded.items);
            }

            public final List<Item> getItems() {
                return this.items;
            }

            public final boolean getShowContactSyncIcon() {
                return this.showContactSyncIcon;
            }

            public int hashCode() {
                boolean z2 = this.showContactSyncIcon;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                int i3 = i * 31;
                List<Item> list = this.items;
                return i3 + (list != null ? list.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(showContactSyncIcon=");
                R.append(this.showContactSyncIcon);
                R.append(", items=");
                return a.K(R, this.items, ")");
            }
        }

        /* compiled from: FriendsListViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/friends/FriendsListViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/friends/FriendsListViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public FriendsListViewModel() {
        this(null, null, null, 7, null);
    }

    public /* synthetic */ FriendsListViewModel(Observable<StoreState> observable, StoreChannels storeChannels, RestAPI restAPI, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? Companion.observeStores() : observable, (i & 2) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 4) != 0 ? RestAPI.Companion.getApi() : restAPI);
    }

    public static /* synthetic */ void acceptFriendSuggestion$default(FriendsListViewModel friendsListViewModel, String str, int i, String str2, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            str2 = null;
        }
        friendsListViewModel.acceptFriendSuggestion(str, i, str2);
    }

    private final <T> Cancellable asyncComputeAndHandleOnUiThread(final Function0<? extends T> function0, Function1<? super Error, Unit> function1, Function1<? super T, Unit> function12) {
        final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        Observable<T> X = Observable.n(new Action1<Emitter<T>>() { // from class: com.discord.widgets.friends.FriendsListViewModel$asyncComputeAndHandleOnUiThread$1
            @Override // rx.functions.Action1
            public /* bridge */ /* synthetic */ void call(Object obj) {
                call((Emitter) ((Emitter) obj));
            }

            /* JADX WARN: Multi-variable type inference failed */
            public final void call(Emitter<T> emitter) {
                emitter.onNext(Function0.this.invoke());
            }
        }, Emitter.BackpressureMode.NONE).X(j0.p.a.a());
        m.checkNotNullExpressionValue(X, "Observable\n        .crea…Schedulers.computation())");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(X, this, null, 2, null), FriendsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new FriendsListViewModel$asyncComputeAndHandleOnUiThread$4(ref$ObjectRef), (r18 & 8) != 0 ? null : new FriendsListViewModel$asyncComputeAndHandleOnUiThread$3(function1), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new FriendsListViewModel$asyncComputeAndHandleOnUiThread$2(function12));
        return new Cancellable() { // from class: com.discord.widgets.friends.FriendsListViewModel$asyncComputeAndHandleOnUiThread$5
            @Override // rx.functions.Cancellable
            public final void cancel() {
                Subscription subscription = (Subscription) Ref$ObjectRef.this.element;
                if (subscription != null) {
                    subscription.unsubscribe();
                }
            }
        };
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Cancellable asyncComputeAndHandleOnUiThread$default(FriendsListViewModel friendsListViewModel, Function0 function0, Function1 function1, Function1 function12, int i, Object obj) {
        if ((i & 2) != 0) {
            function1 = null;
        }
        return friendsListViewModel.asyncComputeAndHandleOnUiThread(function0, function1, function12);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitCaptchaErrorEvent(Error error, String str, int i) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.CaptchaError(error, str, i));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitLaunchVoiceCallEvent(long j) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.LaunchVoiceCall(j));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitShowFriendRequestAbortToast(int i, String str) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowFriendRequestErrorToast(i, str));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitShowToastEvent(@StringRes int i) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.ShowToast(i));
    }

    @MainThread
    private final void generateLoadedItems(boolean z2) {
        Item.SuggestedFriendsHeader suggestionsHeaderItem = this.listSections.getSuggestionsHeaderItem();
        Item.PendingHeader pendingHeader = null;
        Item.SuggestedFriendsHeader copy$default = suggestionsHeaderItem != null ? Item.SuggestedFriendsHeader.copy$default(suggestionsHeaderItem, 0, this.isSuggestedSectionExpanded, false, 5, null) : null;
        Item.PendingHeader pendingHeaderItem = this.listSections.getPendingHeaderItem();
        if (pendingHeaderItem != null) {
            pendingHeader = Item.PendingHeader.copy$default(pendingHeaderItem, 0, 0, this.isPendingSectionExpanded, false, 11, null);
        }
        ListSections copy$default2 = ListSections.copy$default(this.listSections, copy$default, null, pendingHeader, null, null, null, 58, null);
        this.listSections = copy$default2;
        List<Item> visibleItems = getVisibleItems(copy$default2);
        updateViewState(visibleItems.isEmpty() ^ true ? new ViewState.Loaded(z2, visibleItems) : new ViewState.Empty(z2));
    }

    public static /* synthetic */ void generateLoadedItems$default(FriendsListViewModel friendsListViewModel, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        friendsListViewModel.generateLoadedItems(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final ListSections getItems(Map<Long, Integer> map, Map<Long, ? extends User> map2, Map<Long, Presence> map3, Map<Long, ? extends ModelApplicationStream> map4, boolean z2, Map<Long, FriendSuggestion> map5) {
        Item.SuggestedFriendsHeader suggestedFriendsHeader;
        Item.PendingHeader pendingHeader;
        ArrayList arrayList = new ArrayList();
        if (!map5.isEmpty()) {
            for (FriendSuggestion friendSuggestion : map5.values()) {
                arrayList.add(new Item.SuggestedFriend(friendSuggestion));
            }
            if (arrayList.size() > 1) {
                q.sortWith(arrayList, new Comparator() { // from class: com.discord.widgets.friends.FriendsListViewModel$getItems$$inlined$sortBy$1
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return d0.u.a.compareValues(Long.valueOf(((FriendsListViewModel.Item.SuggestedFriend) t).getSuggestion().getUser().getId()), Long.valueOf(((FriendsListViewModel.Item.SuggestedFriend) t2).getSuggestion().getUser().getId()));
                    }
                });
            }
        }
        if (!arrayList.isEmpty()) {
            suggestedFriendsHeader = new Item.SuggestedFriendsHeader(arrayList.size(), false, arrayList.size() > 2);
        } else {
            suggestedFriendsHeader = null;
        }
        FriendsListViewModel$getItems$3 friendsListViewModel$getItems$3 = FriendsListViewModel$getItems$3.INSTANCE;
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        for (Map.Entry entry : i0.asSequence(map)) {
            long longValue = ((Number) entry.getKey()).longValue();
            int intValue = ((Number) entry.getValue()).intValue();
            User user = map2.get(Long.valueOf(longValue));
            int type = ModelUserRelationship.getType(Integer.valueOf(intValue));
            Presence presence = map3.get(Long.valueOf(longValue));
            boolean containsKey = map4.containsKey(Long.valueOf(longValue));
            if (!(user == null || type == 2)) {
                if (FriendsListViewModel$getItems$3.INSTANCE.invoke(type)) {
                    arrayList2.add(new Item.PendingFriendRequest(user, presence, type));
                } else {
                    arrayList3.add(new Item.Friend(user, presence, containsKey));
                }
            }
        }
        List list = u.toList(u.sortedWith(arrayList2, FriendsListViewModel$getItems$sortedPendingItems$1.INSTANCE));
        if (!list.isEmpty()) {
            pendingHeader = new Item.PendingHeader(R.string.friends_pending_request_header, list.size(), false, list.size() > 2);
        } else {
            pendingHeader = null;
        }
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        for (Object obj : arrayList3) {
            if (((Item.Friend) obj).isOnline()) {
                arrayList5.add(obj);
            }
        }
        List list2 = u.toList(u.sortedWith(arrayList5, FriendsListViewModel$getItems$onlineFriendItems$2.INSTANCE));
        if (!list2.isEmpty()) {
            arrayList4.add(new Item.Header(R.string.friends_online_header, list2.size()));
            arrayList4.addAll(list2);
        }
        ArrayList arrayList6 = new ArrayList();
        for (Object obj2 : arrayList3) {
            if (!((Item.Friend) obj2).isOnline()) {
                arrayList6.add(obj2);
            }
        }
        List list3 = u.toList(u.sortedWith(arrayList6, FriendsListViewModel$getItems$offlineFriendItems$2.INSTANCE));
        if (true ^ list3.isEmpty()) {
            arrayList4.add(new Item.Header(R.string.friends_offline_header, list3.size()));
            arrayList4.addAll(list3);
        }
        return new ListSections(suggestedFriendsHeader, arrayList, pendingHeader, list, arrayList4, z2 ? new Item.ContactSyncUpsell(false) : null);
    }

    private final List<Item> getVisibleItems(ListSections listSections) {
        Item.SuggestedFriendsHeader suggestionsHeaderItem = listSections.getSuggestionsHeaderItem();
        List<Item.SuggestedFriend> suggestedFriendItems = listSections.getSuggestedFriendItems();
        Item.PendingHeader pendingHeaderItem = listSections.getPendingHeaderItem();
        List<Item> pendingItems = listSections.getPendingItems();
        List<Item> friendsItemsWithHeaders = listSections.getFriendsItemsWithHeaders();
        ArrayList arrayList = new ArrayList();
        if (listSections.getContactSyncUpsell() != null) {
            arrayList.add(listSections.getContactSyncUpsell());
        }
        if (!this.isSuggestedSectionExpanded) {
            suggestedFriendItems = u.take(suggestedFriendItems, 2);
        }
        if (suggestionsHeaderItem != null && (!suggestedFriendItems.isEmpty())) {
            arrayList.add(suggestionsHeaderItem);
            arrayList.addAll(suggestedFriendItems);
        }
        if (!this.isPendingSectionExpanded) {
            pendingItems = u.take(pendingItems, 2);
        }
        if (pendingHeaderItem != null && (!pendingItems.isEmpty())) {
            arrayList.add(pendingHeaderItem);
            arrayList.addAll(pendingItems);
        }
        arrayList.addAll(friendsItemsWithHeaders);
        return arrayList;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleComputedItems(ListSections listSections, boolean z2) {
        this.listSections = listSections;
        generateLoadedItems(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        Cancellable cancellable = this.computeItemJob;
        if (cancellable != null) {
            cancellable.cancel();
        }
        if (storeState.getRelationshipsState() instanceof StoreUserRelationships.UserRelationshipsState.Loaded) {
            Map<Long, Integer> relationships = ((StoreUserRelationships.UserRelationshipsState.Loaded) storeState.getRelationshipsState()).getRelationships();
            if (relationships.isEmpty()) {
                updateViewState(new ViewState.Empty(storeState.getShowContactSyncIcon() || storeState.getShowContactSyncUpsell()));
            } else {
                this.computeItemJob = asyncComputeAndHandleOnUiThread$default(this, new FriendsListViewModel$handleStoreState$1(this, relationships, storeState), null, new FriendsListViewModel$handleStoreState$2(this, storeState), 2, null);
            }
        } else {
            updateViewState(ViewState.Uninitialized.INSTANCE);
        }
    }

    public final void acceptFriendRequest(long j, String str) {
        Observable addRelationship;
        m.checkNotNullParameter(str, "username");
        addRelationship = this.restAPI.addRelationship(LOCATION, j, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : null, (r16 & 16) != 0 ? null : null);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(addRelationship, false, 1, null), this, null, 2, null), FriendsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new FriendsListViewModel$acceptFriendRequest$2(this, str), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new FriendsListViewModel$acceptFriendRequest$1(this));
    }

    public final void acceptFriendSuggestion(String str, int i, String str2) {
        m.checkNotNullParameter(str, "username");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().sendRelationshipRequest("Friends List - Friend Suggestion", str, i, str2), false, 1, null), this, null, 2, null), FriendsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new FriendsListViewModel$acceptFriendSuggestion$2(this, str, i), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, FriendsListViewModel$acceptFriendSuggestion$1.INSTANCE);
    }

    @MainThread
    public final void dismissContactSyncUpsell() {
        StoreStream.Companion.getContactSync().dismissFriendsListUpsell();
    }

    public final RestAPI getRestAPI() {
        return this.restAPI;
    }

    public final StoreChannels getStoreChannels() {
        return this.storeChannels;
    }

    public final Observable<StoreState> getStoreObservable() {
        return this.storeObservable;
    }

    @MainThread
    public final void handleClickPendingHeader() {
        boolean z2;
        this.isPendingSectionExpanded = !this.isPendingSectionExpanded;
        ViewState viewState = getViewState();
        if (viewState instanceof ViewState.Empty) {
            z2 = ((ViewState.Empty) viewState).getShowContactSyncIcon();
        } else {
            z2 = viewState instanceof ViewState.Loaded ? ((ViewState.Loaded) viewState).getShowContactSyncIcon() : false;
        }
        generateLoadedItems(z2);
    }

    @MainThread
    public final void handleClickSuggestedHeader() {
        boolean z2;
        this.isSuggestedSectionExpanded = !this.isSuggestedSectionExpanded;
        ViewState viewState = getViewState();
        if (viewState instanceof ViewState.Empty) {
            z2 = ((ViewState.Empty) viewState).getShowContactSyncIcon();
        } else {
            z2 = viewState instanceof ViewState.Loaded ? ((ViewState.Loaded) viewState).getShowContactSyncIcon() : false;
        }
        generateLoadedItems(z2);
    }

    public final void ignoreSuggestion(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.ignoreFriendSuggestion(j), false, 1, null), this, null, 2, null), FriendsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, FriendsListViewModel$ignoreSuggestion$1.INSTANCE);
    }

    public final void launchVoiceCall(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.createOrFetchDM(j), false, 1, null), this, null, 2, null), FriendsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new FriendsListViewModel$launchVoiceCall$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new FriendsListViewModel$launchVoiceCall$1(this));
    }

    @MainThread
    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void removeFriendRequest(long j, int i) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restAPI.removeRelationship(LOCATION, j), false, 1, null), this, null, 2, null), FriendsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new FriendsListViewModel$removeFriendRequest$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new FriendsListViewModel$removeFriendRequest$1(this, i == 3 ? R.string.friend_request_ignored : R.string.friend_request_cancelled));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public FriendsListViewModel(Observable<StoreState> observable, StoreChannels storeChannels, RestAPI restAPI) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(observable, "storeObservable");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(restAPI, "restAPI");
        this.storeObservable = observable;
        this.storeChannels = storeChannels;
        this.restAPI = restAPI;
        this.listSections = new ListSections(null, n.emptyList(), null, n.emptyList(), n.emptyList(), null);
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), FriendsListViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
