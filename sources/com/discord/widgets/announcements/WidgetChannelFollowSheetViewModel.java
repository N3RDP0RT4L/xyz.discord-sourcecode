package com.discord.widgets.announcements;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppComponent;
import com.discord.app.AppViewModel;
import com.discord.models.guild.Guild;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetChannelFollowSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u00029:B=\u0012\n\u0010.\u001a\u00060\tj\u0002`\u0018\u0012\n\u0010+\u001a\u00060\tj\u0002`\u0014\u0012\b\b\u0002\u00100\u001a\u00020/\u0012\b\b\u0002\u0010\"\u001a\u00020!\u0012\b\b\u0002\u0010'\u001a\u00020&¢\u0006\u0004\b7\u00108J\u0015\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J)\u0010\f\u001a\u001c\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\t\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\b\u0018\u00010\u0004H\u0002¢\u0006\u0004\b\f\u0010\u0007J\u0017\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0013\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0004¢\u0006\u0004\b\u0013\u0010\u0007J\u0019\u0010\u0016\u001a\u00020\u000f2\n\u0010\u0015\u001a\u00060\tj\u0002`\u0014¢\u0006\u0004\b\u0016\u0010\u0017J\u0019\u0010\u001a\u001a\u00020\u000f2\n\u0010\u0019\u001a\u00060\tj\u0002`\u0018¢\u0006\u0004\b\u001a\u0010\u0017J\u0019\u0010\u001c\u001a\u00020\u000f2\n\u0010\u001b\u001a\u00060\tj\u0002`\u0014¢\u0006\u0004\b\u001c\u0010\u0017RN\u0010\u001f\u001a:\u0012\u0016\u0012\u0014 \u001e*\n\u0018\u00010\tj\u0004\u0018\u0001`\u00180\tj\u0002`\u0018 \u001e*\u001c\u0012\u0016\u0012\u0014 \u001e*\n\u0018\u00010\tj\u0004\u0018\u0001`\u00180\tj\u0002`\u0018\u0018\u00010\u001d0\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u0019\u0010'\u001a\u00020&8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001a\u0010+\u001a\u00060\tj\u0002`\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,RN\u0010-\u001a:\u0012\u0016\u0012\u0014 \u001e*\n\u0018\u00010\tj\u0004\u0018\u0001`\u00140\tj\u0002`\u0014 \u001e*\u001c\u0012\u0016\u0012\u0014 \u001e*\n\u0018\u00010\tj\u0004\u0018\u0001`\u00140\tj\u0002`\u0014\u0018\u00010\u001d0\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010 R\u001a\u0010.\u001a\u00060\tj\u0002`\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b.\u0010,R\u0019\u00100\u001a\u00020/8\u0006@\u0006¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R:\u00105\u001a&\u0012\f\u0012\n \u001e*\u0004\u0018\u00010\u00120\u0012 \u001e*\u0012\u0012\f\u0012\n \u001e*\u0004\u0018\u00010\u00120\u0012\u0018\u000104048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b5\u00106¨\u0006;"}, d2 = {"Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState;", "Lcom/discord/app/AppComponent;", "Lrx/Observable;", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;", "observeViewStateFromStores", "()Lrx/Observable;", "", "", "", "Lcom/discord/api/channel/Channel;", "calculateChannelsWithPermissions", "Lcom/discord/utilities/error/Error;", "error", "", "handleChannelFollowError", "(Lcom/discord/utilities/error/Error;)V", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;", "observeEvents", "Lcom/discord/primitives/ChannelId;", "channelId", "selectChannel", "(J)V", "Lcom/discord/primitives/GuildId;", "guildId", "selectGuild", "webhookChannelId", "followChannel", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "selectedGuildSubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "getStoreChannels", "()Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StorePermissions;", "getStorePermissions", "()Lcom/discord/stores/StorePermissions;", "sourceChannelId", "J", "selectedChannelSubject", "sourceGuildId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "getStoreGuilds", "()Lcom/discord/stores/StoreGuilds;", "Lrx/subjects/PublishSubject;", "eventSubject", "Lrx/subjects/PublishSubject;", HookHelper.constructorName, "(JJLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;)V", "Event", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelFollowSheetViewModel extends AppViewModel<ViewState> implements AppComponent {
    private final PublishSubject<Event> eventSubject;
    private final BehaviorSubject<Long> selectedChannelSubject;
    private final BehaviorSubject<Long> selectedGuildSubject;
    private final long sourceChannelId;
    private final long sourceGuildId;
    private final StoreChannels storeChannels;
    private final StoreGuilds storeGuilds;
    private final StorePermissions storePermissions;

    /* compiled from: WidgetChannelFollowSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;", "viewState", "", "invoke", "(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<ViewState.Loaded, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(ViewState.Loaded loaded) {
            invoke2(loaded);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(ViewState.Loaded loaded) {
            m.checkNotNullParameter(loaded, "viewState");
            WidgetChannelFollowSheetViewModel.this.updateViewState(loaded);
        }
    }

    /* compiled from: WidgetChannelFollowSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;", "", HookHelper.constructorName, "()V", "FollowSuccess", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event$FollowSuccess;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetChannelFollowSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event$FollowSuccess;", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class FollowSuccess extends Event {
            public static final FollowSuccess INSTANCE = new FollowSuccess();

            private FollowSuccess() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetChannelFollowSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Uninitialized", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetChannelFollowSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B_\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0002\u0012\u0010\u0010\u0018\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\n\u0012\u0010\u0010\u0019\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\u000f0\n\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b2\u00103J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u001a\u0010\r\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\nHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0010\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\u000f0\nHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000eJ\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013Jt\u0010\u001b\u001a\u00020\u00002\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00022\u0012\b\u0002\u0010\u0018\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\n2\u0012\b\u0002\u0010\u0019\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\u000f0\n2\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0011HÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010 \u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b \u0010!J\u001a\u0010%\u001a\u00020$2\b\u0010#\u001a\u0004\u0018\u00010\"HÖ\u0003¢\u0006\u0004\b%\u0010&R#\u0010\u0018\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\f0\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010'\u001a\u0004\b(\u0010\u000eR\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b*\u0010\u0004R\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b+\u0010\u0004R\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010,\u001a\u0004\b-\u0010\u0007R#\u0010\u0019\u001a\f\u0012\b\u0012\u00060\u000bj\u0002`\u000f0\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010'\u001a\u0004\b.\u0010\u000eR\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010/\u001a\u0004\b0\u0010\u0013R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010,\u001a\u0004\b1\u0010\u0007¨\u00064"}, d2 = {"Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "component3", "component4", "", "", "Lcom/discord/primitives/GuildId;", "component5", "()Ljava/util/Set;", "Lcom/discord/primitives/ChannelId;", "component6", "", "component7", "()Ljava/lang/Integer;", "sourceChannel", "sourceGuild", "selectedGuild", "selectedChannel", "availableGuilds", "availableChannels", "errorTextRes", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Set;Ljava/util/Set;Ljava/lang/Integer;)Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Set;", "getAvailableGuilds", "Lcom/discord/api/channel/Channel;", "getSelectedChannel", "getSourceChannel", "Lcom/discord/models/guild/Guild;", "getSourceGuild", "getAvailableChannels", "Ljava/lang/Integer;", "getErrorTextRes", "getSelectedGuild", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Set;Ljava/util/Set;Ljava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final Set<Long> availableChannels;
            private final Set<Long> availableGuilds;
            private final Integer errorTextRes;
            private final Channel selectedChannel;
            private final Guild selectedGuild;
            private final Channel sourceChannel;
            private final Guild sourceGuild;

            public /* synthetic */ Loaded(Channel channel, Guild guild, Guild guild2, Channel channel2, Set set, Set set2, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(channel, guild, guild2, channel2, set, set2, (i & 64) != 0 ? null : num);
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, Channel channel, Guild guild, Guild guild2, Channel channel2, Set set, Set set2, Integer num, int i, Object obj) {
                if ((i & 1) != 0) {
                    channel = loaded.sourceChannel;
                }
                if ((i & 2) != 0) {
                    guild = loaded.sourceGuild;
                }
                Guild guild3 = guild;
                if ((i & 4) != 0) {
                    guild2 = loaded.selectedGuild;
                }
                Guild guild4 = guild2;
                if ((i & 8) != 0) {
                    channel2 = loaded.selectedChannel;
                }
                Channel channel3 = channel2;
                Set<Long> set3 = set;
                if ((i & 16) != 0) {
                    set3 = loaded.availableGuilds;
                }
                Set set4 = set3;
                Set<Long> set5 = set2;
                if ((i & 32) != 0) {
                    set5 = loaded.availableChannels;
                }
                Set set6 = set5;
                if ((i & 64) != 0) {
                    num = loaded.errorTextRes;
                }
                return loaded.copy(channel, guild3, guild4, channel3, set4, set6, num);
            }

            public final Channel component1() {
                return this.sourceChannel;
            }

            public final Guild component2() {
                return this.sourceGuild;
            }

            public final Guild component3() {
                return this.selectedGuild;
            }

            public final Channel component4() {
                return this.selectedChannel;
            }

            public final Set<Long> component5() {
                return this.availableGuilds;
            }

            public final Set<Long> component6() {
                return this.availableChannels;
            }

            public final Integer component7() {
                return this.errorTextRes;
            }

            public final Loaded copy(Channel channel, Guild guild, Guild guild2, Channel channel2, Set<Long> set, Set<Long> set2, Integer num) {
                m.checkNotNullParameter(set, "availableGuilds");
                m.checkNotNullParameter(set2, "availableChannels");
                return new Loaded(channel, guild, guild2, channel2, set, set2, num);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(this.sourceChannel, loaded.sourceChannel) && m.areEqual(this.sourceGuild, loaded.sourceGuild) && m.areEqual(this.selectedGuild, loaded.selectedGuild) && m.areEqual(this.selectedChannel, loaded.selectedChannel) && m.areEqual(this.availableGuilds, loaded.availableGuilds) && m.areEqual(this.availableChannels, loaded.availableChannels) && m.areEqual(this.errorTextRes, loaded.errorTextRes);
            }

            public final Set<Long> getAvailableChannels() {
                return this.availableChannels;
            }

            public final Set<Long> getAvailableGuilds() {
                return this.availableGuilds;
            }

            public final Integer getErrorTextRes() {
                return this.errorTextRes;
            }

            public final Channel getSelectedChannel() {
                return this.selectedChannel;
            }

            public final Guild getSelectedGuild() {
                return this.selectedGuild;
            }

            public final Channel getSourceChannel() {
                return this.sourceChannel;
            }

            public final Guild getSourceGuild() {
                return this.sourceGuild;
            }

            public int hashCode() {
                Channel channel = this.sourceChannel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                Guild guild = this.sourceGuild;
                int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
                Guild guild2 = this.selectedGuild;
                int hashCode3 = (hashCode2 + (guild2 != null ? guild2.hashCode() : 0)) * 31;
                Channel channel2 = this.selectedChannel;
                int hashCode4 = (hashCode3 + (channel2 != null ? channel2.hashCode() : 0)) * 31;
                Set<Long> set = this.availableGuilds;
                int hashCode5 = (hashCode4 + (set != null ? set.hashCode() : 0)) * 31;
                Set<Long> set2 = this.availableChannels;
                int hashCode6 = (hashCode5 + (set2 != null ? set2.hashCode() : 0)) * 31;
                Integer num = this.errorTextRes;
                if (num != null) {
                    i = num.hashCode();
                }
                return hashCode6 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(sourceChannel=");
                R.append(this.sourceChannel);
                R.append(", sourceGuild=");
                R.append(this.sourceGuild);
                R.append(", selectedGuild=");
                R.append(this.selectedGuild);
                R.append(", selectedChannel=");
                R.append(this.selectedChannel);
                R.append(", availableGuilds=");
                R.append(this.availableGuilds);
                R.append(", availableChannels=");
                R.append(this.availableChannels);
                R.append(", errorTextRes=");
                return a.E(R, this.errorTextRes, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(Channel channel, Guild guild, Guild guild2, Channel channel2, Set<Long> set, Set<Long> set2, Integer num) {
                super(null);
                m.checkNotNullParameter(set, "availableGuilds");
                m.checkNotNullParameter(set2, "availableChannels");
                this.sourceChannel = channel;
                this.sourceGuild = guild;
                this.selectedGuild = guild2;
                this.selectedChannel = channel2;
                this.availableGuilds = set;
                this.availableChannels = set2;
                this.errorTextRes = num;
            }
        }

        /* compiled from: WidgetChannelFollowSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetChannelFollowSheetViewModel(long j, long j2, StoreGuilds storeGuilds, StoreChannels storeChannels, StorePermissions storePermissions, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, j2, (i & 4) != 0 ? StoreStream.Companion.getGuilds() : storeGuilds, (i & 8) != 0 ? StoreStream.Companion.getChannels() : storeChannels, (i & 16) != 0 ? StoreStream.Companion.getPermissions() : storePermissions);
    }

    private final Observable<Map<Long, List<Channel>>> calculateChannelsWithPermissions() {
        return this.storePermissions.observePermissionsForAllChannels().q().Y(new b<Map<Long, ? extends Long>, Observable<? extends Map<Long, ? extends List<? extends Channel>>>>() { // from class: com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel$calculateChannelsWithPermissions$1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends Map<Long, ? extends List<? extends Channel>>> call(Map<Long, ? extends Long> map) {
                return call2((Map<Long, Long>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends Map<Long, List<Channel>>> call2(final Map<Long, Long> map) {
                return (Observable<R>) WidgetChannelFollowSheetViewModel.this.getStoreChannels().observeGuildAndPrivateChannels().q().F(new b<Map<Long, ? extends Channel>, Map<Long, ? extends List<? extends Channel>>>() { // from class: com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel$calculateChannelsWithPermissions$1.1
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ Map<Long, ? extends List<? extends Channel>> call(Map<Long, ? extends Channel> map2) {
                        return call2((Map<Long, Channel>) map2);
                    }

                    /* JADX WARN: Removed duplicated region for block: B:27:0x0062 A[SYNTHETIC] */
                    /* JADX WARN: Removed duplicated region for block: B:30:0x0012 A[SYNTHETIC] */
                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    /*
                        Code decompiled incorrectly, please refer to instructions dump.
                        To view partially-correct add '--show-bad-code' argument
                    */
                    public final java.util.Map<java.lang.Long, java.util.List<com.discord.api.channel.Channel>> call2(java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r8) {
                        /*
                            r7 = this;
                            java.lang.String r0 = "it"
                            d0.z.d.m.checkNotNullExpressionValue(r8, r0)
                            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
                            r0.<init>()
                            java.util.Set r8 = r8.entrySet()
                            java.util.Iterator r8 = r8.iterator()
                        L12:
                            boolean r1 = r8.hasNext()
                            if (r1 == 0) goto L6e
                            java.lang.Object r1 = r8.next()
                            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
                            java.lang.Object r2 = r1.getKey()
                            java.lang.Number r2 = (java.lang.Number) r2
                            long r2 = r2.longValue()
                            java.lang.Object r4 = r1.getValue()
                            com.discord.api.channel.Channel r4 = (com.discord.api.channel.Channel) r4
                            int r5 = r4.A()
                            r6 = 5
                            if (r5 == r6) goto L3b
                            int r4 = r4.A()
                            if (r4 != 0) goto L5f
                        L3b:
                            r4 = 536870912(0x20000000, double:2.652494739E-315)
                            java.util.Map r6 = r1
                            java.lang.Long r2 = java.lang.Long.valueOf(r2)
                            java.lang.Object r2 = r6.get(r2)
                            java.lang.Long r2 = (java.lang.Long) r2
                            if (r2 == 0) goto L51
                            long r2 = r2.longValue()
                            goto L53
                        L51:
                            r2 = 0
                        L53:
                            java.lang.Long r2 = java.lang.Long.valueOf(r2)
                            boolean r2 = com.discord.utilities.permissions.PermissionUtils.can(r4, r2)
                            if (r2 == 0) goto L5f
                            r2 = 1
                            goto L60
                        L5f:
                            r2 = 0
                        L60:
                            if (r2 == 0) goto L12
                            java.lang.Object r2 = r1.getKey()
                            java.lang.Object r1 = r1.getValue()
                            r0.put(r2, r1)
                            goto L12
                        L6e:
                            java.util.Collection r8 = r0.values()
                            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
                            r0.<init>()
                            java.util.Iterator r8 = r8.iterator()
                        L7b:
                            boolean r1 = r8.hasNext()
                            if (r1 == 0) goto La4
                            java.lang.Object r1 = r8.next()
                            r2 = r1
                            com.discord.api.channel.Channel r2 = (com.discord.api.channel.Channel) r2
                            long r2 = r2.f()
                            java.lang.Long r2 = java.lang.Long.valueOf(r2)
                            java.lang.Object r3 = r0.get(r2)
                            if (r3 != 0) goto L9e
                            java.util.ArrayList r3 = new java.util.ArrayList
                            r3.<init>()
                            r0.put(r2, r3)
                        L9e:
                            java.util.List r3 = (java.util.List) r3
                            r3.add(r1)
                            goto L7b
                        La4:
                            return r0
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel$calculateChannelsWithPermissions$1.AnonymousClass1.call2(java.util.Map):java.util.Map");
                    }
                });
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleChannelFollowError(Error error) {
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        int i = response.getCode() != 30007 ? R.string.follow_modal_fail : R.string.follow_modal_too_many_webhooks;
        ViewState viewState = getViewState();
        Objects.requireNonNull(viewState, "null cannot be cast to non-null type com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel.ViewState.Loaded");
        updateViewState(ViewState.Loaded.copy$default((ViewState.Loaded) viewState, null, null, null, null, null, null, Integer.valueOf(i), 63, null));
    }

    private final Observable<ViewState.Loaded> observeViewStateFromStores() {
        Observable<ViewState.Loaded> g = Observable.g(this.storeGuilds.observeGuild(this.sourceGuildId), this.storeChannels.observeChannel(this.sourceChannelId), calculateChannelsWithPermissions(), this.selectedGuildSubject.Y(new b<Long, Observable<? extends Guild>>() { // from class: com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel$observeViewStateFromStores$1
            public final Observable<? extends Guild> call(Long l) {
                if (l != null) {
                    Observable<Guild> observeGuild = WidgetChannelFollowSheetViewModel.this.getStoreGuilds().observeGuild(l.longValue());
                    if (observeGuild != null) {
                        return observeGuild;
                    }
                }
                return new k(null);
            }
        }), this.selectedChannelSubject.Y(new b<Long, Observable<? extends Channel>>() { // from class: com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel$observeViewStateFromStores$2
            public final Observable<? extends Channel> call(Long l) {
                if (l != null) {
                    Observable<Channel> observeChannel = WidgetChannelFollowSheetViewModel.this.getStoreChannels().observeChannel(l.longValue());
                    if (observeChannel != null) {
                        return observeChannel;
                    }
                }
                return new k(null);
            }
        }), WidgetChannelFollowSheetViewModel$observeViewStateFromStores$3.INSTANCE);
        m.checkNotNullExpressionValue(g, "Observable.combineLatest…  )\n\n      expected\n    }");
        return g;
    }

    public final void followChannel(long j) {
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().createChannelFollower(this.sourceChannelId, new RestAPIParams.ChannelFollowerPost(j)), false, 1, null), this, null, 2, null), WidgetChannelFollowSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetChannelFollowSheetViewModel$followChannel$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetChannelFollowSheetViewModel$followChannel$2(this));
    }

    public final StoreChannels getStoreChannels() {
        return this.storeChannels;
    }

    public final StoreGuilds getStoreGuilds() {
        return this.storeGuilds;
    }

    public final StorePermissions getStorePermissions() {
        return this.storePermissions;
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void selectChannel(long j) {
        this.selectedChannelSubject.onNext(Long.valueOf(j));
    }

    public final void selectGuild(long j) {
        this.selectedGuildSubject.onNext(Long.valueOf(j));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelFollowSheetViewModel(long j, long j2, StoreGuilds storeGuilds, StoreChannels storeChannels, StorePermissions storePermissions) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storePermissions, "storePermissions");
        this.sourceGuildId = j;
        this.sourceChannelId = j2;
        this.storeGuilds = storeGuilds;
        this.storeChannels = storeChannels;
        this.storePermissions = storePermissions;
        this.selectedGuildSubject = BehaviorSubject.l0(null);
        this.selectedChannelSubject = BehaviorSubject.l0(null);
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observeViewStateFromStores()), this, null, 2, null), WidgetChannelFollowSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
