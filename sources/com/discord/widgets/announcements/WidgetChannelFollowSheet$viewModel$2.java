package com.discord.widgets.announcements;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetChannelFollowSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelFollowSheet$viewModel$2 extends o implements Function0<AppViewModel<WidgetChannelFollowSheetViewModel.ViewState>> {
    public final /* synthetic */ WidgetChannelFollowSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetChannelFollowSheet$viewModel$2(WidgetChannelFollowSheet widgetChannelFollowSheet) {
        super(0);
        this.this$0 = widgetChannelFollowSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<WidgetChannelFollowSheetViewModel.ViewState> invoke() {
        Bundle argumentsOrDefault;
        Bundle argumentsOrDefault2;
        argumentsOrDefault = this.this$0.getArgumentsOrDefault();
        long j = argumentsOrDefault.getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID");
        argumentsOrDefault2 = this.this$0.getArgumentsOrDefault();
        return new WidgetChannelFollowSheetViewModel(argumentsOrDefault2.getLong("com.discord.intent.extra.EXTRA_GUILD_ID"), j, null, null, null, 28, null);
    }
}
