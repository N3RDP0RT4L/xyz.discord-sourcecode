package com.discord.widgets.announcements;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.models.guild.Guild;
import com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel;
import d0.t.o;
import d0.t.u;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import rx.functions.Func5;
/* compiled from: WidgetChannelFollowSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000e\u001a\n \u0007*\u0004\u0018\u00010\u000b0\u000b2\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u000222\u0010\b\u001a.\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u0006 \u0007*\u0016\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u0006\u0018\u00010\u00040\u00042\b\u0010\t\u001a\u0004\u0018\u00010\u00002\b\u0010\n\u001a\u0004\u0018\u00010\u0002H\n¢\u0006\u0004\b\f\u0010\r"}, d2 = {"Lcom/discord/models/guild/Guild;", "sourceGuild", "Lcom/discord/api/channel/Channel;", "sourceChannel", "", "", "", "kotlin.jvm.PlatformType", "channelsWithPermissions", "selectedGuild", "selectedChannel", "Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Map;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetChannelFollowSheetViewModel$observeViewStateFromStores$3<T1, T2, T3, T4, T5, R> implements Func5<Guild, Channel, Map<Long, ? extends List<? extends Channel>>, Guild, Channel, WidgetChannelFollowSheetViewModel.ViewState.Loaded> {
    public static final WidgetChannelFollowSheetViewModel$observeViewStateFromStores$3 INSTANCE = new WidgetChannelFollowSheetViewModel$observeViewStateFromStores$3();

    @Override // rx.functions.Func5
    public /* bridge */ /* synthetic */ WidgetChannelFollowSheetViewModel.ViewState.Loaded call(Guild guild, Channel channel, Map<Long, ? extends List<? extends Channel>> map, Guild guild2, Channel channel2) {
        return call2(guild, channel, (Map<Long, ? extends List<Channel>>) map, guild2, channel2);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final WidgetChannelFollowSheetViewModel.ViewState.Loaded call2(Guild guild, Channel channel, Map<Long, ? extends List<Channel>> map, Guild guild2, Channel channel2) {
        Set<Long> keySet = map.keySet();
        List<Channel> flatten = o.flatten(map.values());
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(flatten, 10));
        for (Channel channel3 : flatten) {
            arrayList.add(Long.valueOf(channel3.h()));
        }
        return new WidgetChannelFollowSheetViewModel.ViewState.Loaded(channel, guild, guild2, channel2, keySet, u.toSet(arrayList), null, 64, null);
    }
}
