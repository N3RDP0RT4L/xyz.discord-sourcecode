package com.discord.widgets.announcements;

import com.discord.widgets.announcements.WidgetChannelFollowSheetViewModel;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetChannelFollowSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;", "p1", "", "invoke", "(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetChannelFollowSheet$onResume$1 extends k implements Function1<WidgetChannelFollowSheetViewModel.ViewState.Loaded, Unit> {
    public WidgetChannelFollowSheet$onResume$1(WidgetChannelFollowSheet widgetChannelFollowSheet) {
        super(1, widgetChannelFollowSheet, WidgetChannelFollowSheet.class, "configureUI", "configureUI(Lcom/discord/widgets/announcements/WidgetChannelFollowSheetViewModel$ViewState$Loaded;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetChannelFollowSheetViewModel.ViewState.Loaded loaded) {
        invoke2(loaded);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetChannelFollowSheetViewModel.ViewState.Loaded loaded) {
        m.checkNotNullParameter(loaded, "p1");
        ((WidgetChannelFollowSheet) this.receiver).configureUI(loaded);
    }
}
