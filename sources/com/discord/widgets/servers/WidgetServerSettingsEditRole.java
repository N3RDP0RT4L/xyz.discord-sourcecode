package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.j;
import b.a.d.o;
import b.a.k.b;
import b.d.b.a.a;
import b.k.a.a.f;
import com.discord.api.guild.GuildFeature;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetEditRoleBinding;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.colors.ColorPickerUtils;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.chat.list.NewThreadsPermissionsFeatureFlag;
import com.discord.widgets.guildcommunicationdisabled.start.GuildCommunicationDisabledGuildsFeatureFlag;
import com.discord.widgets.servers.WidgetServerSettingsEditRole;
import com.discord.widgets.stage.start.StageEventsGuildsFeatureFlag;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import d0.z.d.m;
import j0.k.b;
import java.util.Collection;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.functions.Func7;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsEditRole.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 /2\u00020\u0001:\u0002/0B\u0007¢\u0006\u0004\b.\u0010 J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\t\u0010\u0006J\u0017\u0010\n\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\n\u0010\u0006J\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000b\u0010\u0006J\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\f\u0010\u0006J\u001f\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0012\u0010\u0006J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0013\u0010\u0006J'\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u001f\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u001bH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010#\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020!H\u0016¢\u0006\u0004\b#\u0010$R\u001d\u0010*\u001a\u00020%8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-¨\u00061"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;", "data", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;)V", "setupRoleName", "setupColorSetting", "setRoleIcon", "setupHoistAndMentionSettings", "setupPermissionsSettings", "launchColorPicker", "", "everyoneLocked", "", "getLockMessage", "(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;Z)Ljava/lang/String;", "setupMenu", "setupActionBar", "Lcom/discord/views/CheckedSetting;", "setting", "", "permission", "enableSetting", "(Lcom/discord/views/CheckedSetting;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;J)V", "guildId", "Lcom/discord/restapi/RestAPIParams$Role;", "roleParams", "patchRole", "(JLcom/discord/restapi/RestAPIParams$Role;)V", "onViewBoundOrOnResume", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/databinding/WidgetEditRoleBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetEditRoleBinding;", "binding", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEditRole extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsEditRole.class, "binding", "getBinding()Lcom/discord/databinding/WidgetEditRoleBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String DIALOG_TAG_COLOR_PICKER = "DIALOG_TAG_COLOR_PICKER";
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String INTENT_EXTRA_ROLE_ID = "INTENT_EXTRA_ROLE_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsEditRole$binding$2.INSTANCE, null, 2, null);
    private final StatefulViews state = new StatefulViews(R.id.edit_role_name);

    /* compiled from: WidgetServerSettingsEditRole.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\fR\u0016\u0010\u000e\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Companion;", "", "", "guildId", "roleId", "Landroid/content/Context;", "context", "", "launch", "(JJLandroid/content/Context;)V", "", WidgetServerSettingsEditRole.DIALOG_TAG_COLOR_PICKER, "Ljava/lang/String;", "INTENT_EXTRA_GUILD_ID", WidgetServerSettingsEditRole.INTENT_EXTRA_ROLE_ID, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(long j, long j2, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra("INTENT_EXTRA_GUILD_ID", j);
            intent.putExtra(WidgetServerSettingsEditRole.INTENT_EXTRA_ROLE_ID, j2);
            j.d(context, WidgetServerSettingsEditRole.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsEditRole.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0018\b\u0082\b\u0018\u0000 @2\u00020\u0001:\u0002@ABa\u0012\u0006\u0010\u001b\u001a\u00020\u0002\u0012\u0006\u0010\u001c\u001a\u00020\u0006\u0012\u0006\u0010\u001d\u001a\u00020\r\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0010\u0012\u000e\u0010\u001f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0013\u0012\u0006\u0010 \u001a\u00020\u0006\u0012\u0006\u0010!\u001a\u00020\u0002\u0012\u0006\u0010\"\u001a\u00020\u0002\u0012\u0006\u0010#\u001a\u00020\u0002\u0012\u0006\u0010$\u001a\u00020\u0002¢\u0006\u0004\b>\u0010?J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0004J\u0015\u0010\b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0018\u0010\u0014\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0016\u0010\fJ\u0010\u0010\u0017\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0004J\u0010\u0010\u0018\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0004J\u0010\u0010\u0019\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0004J\u0010\u0010\u001a\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0004J~\u0010%\u001a\u00020\u00002\b\b\u0002\u0010\u001b\u001a\u00020\u00022\b\b\u0002\u0010\u001c\u001a\u00020\u00062\b\b\u0002\u0010\u001d\u001a\u00020\r2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00102\u0010\b\u0002\u0010\u001f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00132\b\b\u0002\u0010 \u001a\u00020\u00062\b\b\u0002\u0010!\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020\u00022\b\b\u0002\u0010#\u001a\u00020\u00022\b\b\u0002\u0010$\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010(\u001a\u00020'HÖ\u0001¢\u0006\u0004\b(\u0010)J\u0010\u0010+\u001a\u00020*HÖ\u0001¢\u0006\u0004\b+\u0010,J\u001a\u0010.\u001a\u00020\u00022\b\u0010-\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b.\u0010/R!\u0010\u001f\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00100\u001a\u0004\b1\u0010\u0015R\u0019\u0010\u001d\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00102\u001a\u0004\b3\u0010\u000fR\u0019\u0010$\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u00104\u001a\u0004\b5\u0010\u0004R\u0019\u0010 \u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b \u00106\u001a\u0004\b7\u0010\fR\u0019\u0010\u001c\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00106\u001a\u0004\b8\u0010\fR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00109\u001a\u0004\b:\u0010\u0012R\u0019\u0010#\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b#\u00104\u001a\u0004\b;\u0010\u0004R\u0019\u0010\"\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00104\u001a\u0004\b<\u0010\u0004R\u0019\u0010!\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u00104\u001a\u0004\b!\u0010\u0004R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u00104\u001a\u0004\b=\u0010\u0004¨\u0006B"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;", "", "", "isEveryoneRole", "()Z", "canManage", "", "permission", "isSingular", "(J)Z", "component1", "component2", "()J", "Lcom/discord/api/role/GuildRole;", "component3", "()Lcom/discord/api/role/GuildRole;", "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;", "component4", "()Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;", "Lcom/discord/api/permission/PermissionBit;", "component5", "()Ljava/lang/Long;", "component6", "component7", "component8", "component9", "component10", "owner", "guildId", "role", "manageStatus", "myPermissions", "myPermissionsFromOtherRoles", "isCommunityServer", "useNewThreadPermissions", "hasEventsFeature", "hasGuildCommunicationDisabledFeature", "copy", "(ZJLcom/discord/api/role/GuildRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;Ljava/lang/Long;JZZZZ)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getMyPermissions", "Lcom/discord/api/role/GuildRole;", "getRole", "Z", "getHasGuildCommunicationDisabledFeature", "J", "getMyPermissionsFromOtherRoles", "getGuildId", "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;", "getManageStatus", "getHasEventsFeature", "getUseNewThreadPermissions", "getOwner", HookHelper.constructorName, "(ZJLcom/discord/api/role/GuildRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;Ljava/lang/Long;JZZZZ)V", "Companion", "ManageStatus", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final long guildId;
        private final boolean hasEventsFeature;
        private final boolean hasGuildCommunicationDisabledFeature;
        private final boolean isCommunityServer;
        private final ManageStatus manageStatus;
        private final Long myPermissions;
        private final long myPermissionsFromOtherRoles;
        private final boolean owner;
        private final GuildRole role;
        private final boolean useNewThreadPermissions;

        /* compiled from: WidgetServerSettingsEditRole.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u001e\n\u0002\u0010\t\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001b\u0010\u001cJA\u0010\n\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u00052\u0006\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\n\u0010\u000bJC\u0010\u0014\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f2\u000e\u0010\u0010\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00062\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J%\u0010\u0019\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00180\u00172\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0003¢\u0006\u0004\b\u0019\u0010\u001a¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;", "", "", "", "myRoleIds", "", "Lcom/discord/api/role/GuildRole;", "guildRoles", "ignoreRoleId", "guildId", "computeMyOtherPermissions", "(Ljava/util/Collection;Ljava/util/Map;JJ)J", "", "isOwner", "isElevated", "Lcom/discord/api/permission/PermissionBit;", "myPermissions", "myHighestRole", "role", "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;", "computeManageStatus", "(ZZLjava/lang/Long;Lcom/discord/api/role/GuildRole;Lcom/discord/api/role/GuildRole;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;", "roleId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;", "get", "(JJ)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final ManageStatus computeManageStatus(boolean z2, boolean z3, Long l, GuildRole guildRole, GuildRole guildRole2) {
                boolean can = PermissionUtils.can(8L, l);
                boolean rankIsHigher = RoleUtils.rankIsHigher(guildRole, guildRole2);
                if (z2 || (can && rankIsHigher && z3)) {
                    return ManageStatus.CAN_MANAGE_ADMIN;
                }
                if (RoleUtils.rankIsHigher(guildRole2, guildRole)) {
                    return ManageStatus.LOCKED_HIGHER;
                }
                if (RoleUtils.rankEquals(guildRole, guildRole2)) {
                    return ManageStatus.LOCKED_HIGHEST;
                }
                if (PermissionUtils.can(Permission.MANAGE_ROLES, l) && z3) {
                    return ManageStatus.CAN_MANAGE_CONDITIONAL;
                }
                if (!z3) {
                    return ManageStatus.USER_NOT_ELEVATED;
                }
                return ManageStatus.NO_MANAGE_ROLES_PERMISSION;
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final long computeMyOtherPermissions(Collection<Long> collection, Map<Long, GuildRole> map, long j, long j2) {
                GuildRole guildRole = map.get(Long.valueOf(j2));
                long j3 = 0;
                if (!(guildRole == null || j == j2)) {
                    j3 = guildRole.h() | 0 | guildRole.h();
                }
                for (Number number : collection) {
                    long longValue = number.longValue();
                    GuildRole guildRole2 = map.get(Long.valueOf(longValue));
                    if (!(guildRole2 == null || longValue == j)) {
                        j3 = guildRole2.h() | j3;
                    }
                }
                return j3;
            }

            public final Observable<Model> get(final long j, final long j2) {
                Observable<Model> q = StoreUser.observeMe$default(StoreStream.Companion.getUsers(), false, 1, null).Y(new b<MeUser, Observable<? extends Model>>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$Model$Companion$get$1
                    public final Observable<? extends WidgetServerSettingsEditRole.Model> call(final MeUser meUser) {
                        m.checkNotNullParameter(meUser, "meUser");
                        StoreStream.Companion companion = StoreStream.Companion;
                        return Observable.e(companion.getGuilds().observeGuild(j), companion.getGuilds().observeComputed(j, d0.t.m.listOf(Long.valueOf(meUser.getId()))), companion.getGuilds().observeRoles(j), companion.getPermissions().observePermissionsForGuild(j), NewThreadsPermissionsFeatureFlag.Companion.getINSTANCE().observeEnabled(j), StageEventsGuildsFeatureFlag.Companion.getINSTANCE().observeCanGuildAccessStageEvents(j), GuildCommunicationDisabledGuildsFeatureFlag.Companion.getINSTANCE().observeCanGuildAccessCommunicationDisabled(j), new Func7<Guild, Map<Long, ? extends GuildMember>, Map<Long, ? extends GuildRole>, Long, Boolean, Boolean, Boolean, WidgetServerSettingsEditRole.Model>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$Model$Companion$get$1.1
                            @Override // rx.functions.Func7
                            public /* bridge */ /* synthetic */ WidgetServerSettingsEditRole.Model call(Guild guild, Map<Long, ? extends GuildMember> map, Map<Long, ? extends GuildRole> map2, Long l, Boolean bool, Boolean bool2, Boolean bool3) {
                                return call2(guild, (Map<Long, GuildMember>) map, (Map<Long, GuildRole>) map2, l, bool, bool2, bool3);
                            }

                            /* renamed from: call  reason: avoid collision after fix types in other method */
                            public final WidgetServerSettingsEditRole.Model call2(Guild guild, Map<Long, GuildMember> map, Map<Long, GuildRole> map2, Long l, Boolean bool, Boolean bool2, Boolean bool3) {
                                WidgetServerSettingsEditRole.Model.ManageStatus computeManageStatus;
                                long computeMyOtherPermissions;
                                GuildMember guildMember = map.get(Long.valueOf(meUser.getId()));
                                GuildRole guildRole = map2 != null ? map2.get(Long.valueOf(j2)) : null;
                                if (guildRole == null || guild == null || guildMember == null) {
                                    return null;
                                }
                                GuildRole highestRole = RoleUtils.getHighestRole(map2, guildMember);
                                boolean z2 = guild.getOwnerId() == meUser.getId();
                                boolean isElevated = PermissionUtils.isElevated(meUser.getMfaEnabled(), guild.getMfaLevel());
                                WidgetServerSettingsEditRole.Model.Companion companion2 = WidgetServerSettingsEditRole.Model.Companion;
                                computeManageStatus = companion2.computeManageStatus(z2, isElevated, l, highestRole, guildRole);
                                computeMyOtherPermissions = companion2.computeMyOtherPermissions(guildMember.getRoles(), map2, j2, guild.getId());
                                boolean contains = guild.getFeatures().contains(GuildFeature.COMMUNITY);
                                long id2 = guild.getId();
                                m.checkNotNullExpressionValue(bool, "useNewThreadPermissions");
                                boolean booleanValue = bool.booleanValue();
                                m.checkNotNullExpressionValue(bool2, "hasEventsFeature");
                                boolean booleanValue2 = bool2.booleanValue();
                                m.checkNotNullExpressionValue(bool3, "hasDisabledGuildCommunicationFeature");
                                return new WidgetServerSettingsEditRole.Model(z2, id2, guildRole, computeManageStatus, l, computeMyOtherPermissions, contains, booleanValue, booleanValue2, bool3.booleanValue());
                            }
                        });
                    }
                }).q();
                m.checkNotNullExpressionValue(q, "getUsers()\n            .…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetServerSettingsEditRole.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "NO_MANAGE_ROLES_PERMISSION", "LOCKED_HIGHER", "LOCKED_HIGHEST", "USER_NOT_ELEVATED", "CAN_MANAGE_CONDITIONAL", "CAN_MANAGE_ADMIN", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public enum ManageStatus {
            NO_MANAGE_ROLES_PERMISSION,
            LOCKED_HIGHER,
            LOCKED_HIGHEST,
            USER_NOT_ELEVATED,
            CAN_MANAGE_CONDITIONAL,
            CAN_MANAGE_ADMIN
        }

        public Model(boolean z2, long j, GuildRole guildRole, ManageStatus manageStatus, Long l, long j2, boolean z3, boolean z4, boolean z5, boolean z6) {
            m.checkNotNullParameter(guildRole, "role");
            this.owner = z2;
            this.guildId = j;
            this.role = guildRole;
            this.manageStatus = manageStatus;
            this.myPermissions = l;
            this.myPermissionsFromOtherRoles = j2;
            this.isCommunityServer = z3;
            this.useNewThreadPermissions = z4;
            this.hasEventsFeature = z5;
            this.hasGuildCommunicationDisabledFeature = z6;
        }

        public final boolean canManage() {
            ManageStatus manageStatus = this.manageStatus;
            return manageStatus == ManageStatus.CAN_MANAGE_CONDITIONAL || manageStatus == ManageStatus.CAN_MANAGE_ADMIN;
        }

        public final boolean component1() {
            return this.owner;
        }

        public final boolean component10() {
            return this.hasGuildCommunicationDisabledFeature;
        }

        public final long component2() {
            return this.guildId;
        }

        public final GuildRole component3() {
            return this.role;
        }

        public final ManageStatus component4() {
            return this.manageStatus;
        }

        public final Long component5() {
            return this.myPermissions;
        }

        public final long component6() {
            return this.myPermissionsFromOtherRoles;
        }

        public final boolean component7() {
            return this.isCommunityServer;
        }

        public final boolean component8() {
            return this.useNewThreadPermissions;
        }

        public final boolean component9() {
            return this.hasEventsFeature;
        }

        public final Model copy(boolean z2, long j, GuildRole guildRole, ManageStatus manageStatus, Long l, long j2, boolean z3, boolean z4, boolean z5, boolean z6) {
            m.checkNotNullParameter(guildRole, "role");
            return new Model(z2, j, guildRole, manageStatus, l, j2, z3, z4, z5, z6);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return this.owner == model.owner && this.guildId == model.guildId && m.areEqual(this.role, model.role) && m.areEqual(this.manageStatus, model.manageStatus) && m.areEqual(this.myPermissions, model.myPermissions) && this.myPermissionsFromOtherRoles == model.myPermissionsFromOtherRoles && this.isCommunityServer == model.isCommunityServer && this.useNewThreadPermissions == model.useNewThreadPermissions && this.hasEventsFeature == model.hasEventsFeature && this.hasGuildCommunicationDisabledFeature == model.hasGuildCommunicationDisabledFeature;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final boolean getHasEventsFeature() {
            return this.hasEventsFeature;
        }

        public final boolean getHasGuildCommunicationDisabledFeature() {
            return this.hasGuildCommunicationDisabledFeature;
        }

        public final ManageStatus getManageStatus() {
            return this.manageStatus;
        }

        public final Long getMyPermissions() {
            return this.myPermissions;
        }

        public final long getMyPermissionsFromOtherRoles() {
            return this.myPermissionsFromOtherRoles;
        }

        public final boolean getOwner() {
            return this.owner;
        }

        public final GuildRole getRole() {
            return this.role;
        }

        public final boolean getUseNewThreadPermissions() {
            return this.useNewThreadPermissions;
        }

        public int hashCode() {
            boolean z2 = this.owner;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int a = (a0.a.a.b.a(this.guildId) + (i2 * 31)) * 31;
            GuildRole guildRole = this.role;
            int i4 = 0;
            int hashCode = (a + (guildRole != null ? guildRole.hashCode() : 0)) * 31;
            ManageStatus manageStatus = this.manageStatus;
            int hashCode2 = (hashCode + (manageStatus != null ? manageStatus.hashCode() : 0)) * 31;
            Long l = this.myPermissions;
            if (l != null) {
                i4 = l.hashCode();
            }
            int a2 = (a0.a.a.b.a(this.myPermissionsFromOtherRoles) + ((hashCode2 + i4) * 31)) * 31;
            boolean z3 = this.isCommunityServer;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (a2 + i5) * 31;
            boolean z4 = this.useNewThreadPermissions;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.hasEventsFeature;
            if (z5) {
                z5 = true;
            }
            int i11 = z5 ? 1 : 0;
            int i12 = z5 ? 1 : 0;
            int i13 = (i10 + i11) * 31;
            boolean z6 = this.hasGuildCommunicationDisabledFeature;
            if (!z6) {
                i = z6 ? 1 : 0;
            }
            return i13 + i;
        }

        public final boolean isCommunityServer() {
            return this.isCommunityServer;
        }

        public final boolean isEveryoneRole() {
            return this.role.getId() == this.guildId;
        }

        public final boolean isSingular(long j) {
            return !this.owner && (this.myPermissionsFromOtherRoles & j) != j;
        }

        public String toString() {
            StringBuilder R = a.R("Model(owner=");
            R.append(this.owner);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", role=");
            R.append(this.role);
            R.append(", manageStatus=");
            R.append(this.manageStatus);
            R.append(", myPermissions=");
            R.append(this.myPermissions);
            R.append(", myPermissionsFromOtherRoles=");
            R.append(this.myPermissionsFromOtherRoles);
            R.append(", isCommunityServer=");
            R.append(this.isCommunityServer);
            R.append(", useNewThreadPermissions=");
            R.append(this.useNewThreadPermissions);
            R.append(", hasEventsFeature=");
            R.append(this.hasEventsFeature);
            R.append(", hasGuildCommunicationDisabledFeature=");
            return a.M(R, this.hasGuildCommunicationDisabledFeature, ")");
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            Model.ManageStatus.values();
            int[] iArr = new int[6];
            $EnumSwitchMapping$0 = iArr;
            iArr[Model.ManageStatus.CAN_MANAGE_CONDITIONAL.ordinal()] = 1;
            iArr[Model.ManageStatus.CAN_MANAGE_ADMIN.ordinal()] = 2;
            Model.ManageStatus.values();
            int[] iArr2 = new int[6];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[Model.ManageStatus.NO_MANAGE_ROLES_PERMISSION.ordinal()] = 1;
            iArr2[Model.ManageStatus.LOCKED_HIGHER.ordinal()] = 2;
            iArr2[Model.ManageStatus.LOCKED_HIGHEST.ordinal()] = 3;
            iArr2[Model.ManageStatus.USER_NOT_ELEVATED.ordinal()] = 4;
        }
    }

    public WidgetServerSettingsEditRole() {
        super(R.layout.widget_edit_role);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        if ((model != null ? model.getRole() : null) == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        setupMenu(model);
        setupActionBar(model);
        setupRoleName(model);
        setupHoistAndMentionSettings(model);
        setupPermissionsSettings(model);
        setupColorSetting(model);
        setRoleIcon(model);
        this.state.configureSaveActionView(getBinding().c);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetEditRoleBinding binding;
                WidgetEditRoleBinding binding2;
                binding = WidgetServerSettingsEditRole.this.getBinding();
                TextInputLayout textInputLayout = binding.f2350b;
                m.checkNotNullExpressionValue(textInputLayout, "binding.editRoleName");
                String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
                int length = textOrEmpty.length() - 1;
                int i = 0;
                boolean z2 = false;
                while (i <= length) {
                    boolean z3 = m.compare(textOrEmpty.charAt(!z2 ? i : length), 32) <= 0;
                    if (!z2) {
                        if (!z3) {
                            z2 = true;
                        } else {
                            i++;
                        }
                    } else if (!z3) {
                        break;
                    } else {
                        length--;
                    }
                }
                String obj = textOrEmpty.subSequence(i, length + 1).toString();
                if (obj.length() > 0) {
                    binding2 = WidgetServerSettingsEditRole.this.getBinding();
                    TextInputLayout textInputLayout2 = binding2.f2350b;
                    m.checkNotNullExpressionValue(textInputLayout2, "binding.editRoleName");
                    textInputLayout2.setSelected(false);
                    RestAPIParams.Role createWithRole = RestAPIParams.Role.Companion.createWithRole(model.getRole());
                    createWithRole.setName(obj);
                    WidgetServerSettingsEditRole.this.patchRole(model.getGuildId(), createWithRole);
                    AppFragment.hideKeyboard$default(WidgetServerSettingsEditRole.this, null, 1, null);
                    return;
                }
                b.a.d.m.i(WidgetServerSettingsEditRole.this, R.string.form_label_role_enter_name, 0, 4);
            }
        });
        if (!model.getUseNewThreadPermissions()) {
            CheckedSetting.i(getBinding().O, getString(R.string.role_permissions_send_messages_description), false, 2);
            getBinding().P.setText(getString(R.string.interim_send_messages_in_threads));
            CheckedSetting checkedSetting = getBinding().P;
            e = b.a.k.b.e(this, R.string.interim_role_permissions_send_messages_in_threads_description, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            checkedSetting.h(e, true);
            getBinding().o.setText(getString(R.string.interim_create_public_threads));
            CheckedSetting checkedSetting2 = getBinding().o;
            e2 = b.a.k.b.e(this, R.string.interim_role_permissions_create_public_threads_description, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            checkedSetting2.h(e2, true);
            getBinding().n.setText(getString(R.string.interim_create_private_threads));
            CheckedSetting checkedSetting3 = getBinding().n;
            e3 = b.a.k.b.e(this, R.string.interim_role_permissions_create_private_threads_description, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            checkedSetting3.h(e3, true);
        }
    }

    private final void enableSetting(CheckedSetting checkedSetting, final Model model, final long j) {
        checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$enableSetting$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetEditRoleBinding binding;
                binding = WidgetServerSettingsEditRole.this.getBinding();
                binding.f2350b.clearFocus();
                RestAPIParams.Role createWithRole = RestAPIParams.Role.Companion.createWithRole(model.getRole());
                createWithRole.setPermissions(Long.valueOf(model.getRole().h() ^ j));
                WidgetServerSettingsEditRole.this.patchRole(model.getGuildId(), createWithRole);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetEditRoleBinding getBinding() {
        return (WidgetEditRoleBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getLockMessage(Model model, boolean z2) {
        String str;
        if (!z2 || !model.isEveryoneRole()) {
            Model.ManageStatus manageStatus = model.getManageStatus();
            if (manageStatus != null) {
                int ordinal = manageStatus.ordinal();
                if (ordinal == 0) {
                    str = getString(R.string.help_missing_manage_roles_permission);
                } else if (ordinal == 1) {
                    str = getString(R.string.help_role_locked);
                } else if (ordinal == 2) {
                    str = getString(R.string.help_role_locked_mine);
                } else if (ordinal == 3) {
                    str = getString(R.string.two_fa_guild_mfa_warning_ios);
                }
                m.checkNotNullExpressionValue(str, "when (data.manageStatus)…s)\n      else -> \"\"\n    }");
                return str;
            }
            str = "";
            m.checkNotNullExpressionValue(str, "when (data.manageStatus)…s)\n      else -> \"\"\n    }");
            return str;
        }
        String string = getString(R.string.form_label_disabled_for_everyone);
        m.checkNotNullExpressionValue(string, "getString(R.string.form_…el_disabled_for_everyone)");
        return string;
    }

    public static final void launch(long j, long j2, Context context) {
        Companion.launch(j, j2, context);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchColorPicker(final Model model) {
        ColorPickerDialog buildColorPickerDialog = ColorPickerUtils.INSTANCE.buildColorPickerDialog(requireContext(), R.string.role_color, RoleUtils.getRoleColor$default(model.getRole(), requireContext(), null, 2, null));
        buildColorPickerDialog.k = new f() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$launchColorPicker$1
            @Override // b.k.a.a.f
            public void onColorReset(int i) {
                RestAPIParams.Role createWithRole = RestAPIParams.Role.Companion.createWithRole(model.getRole());
                createWithRole.setColor(0);
                WidgetServerSettingsEditRole.this.patchRole(model.getGuildId(), createWithRole);
            }

            @Override // b.k.a.a.f
            public void onColorSelected(int i, int i2) {
                RestAPIParams.Role createWithRole = RestAPIParams.Role.Companion.createWithRole(model.getRole());
                createWithRole.setColor(Integer.valueOf(ColorCompat.INSTANCE.removeAlphaComponent(i2)));
                WidgetServerSettingsEditRole.this.patchRole(model.getGuildId(), createWithRole);
            }

            @Override // b.k.a.a.f
            public void onDialogDismissed(int i) {
            }
        };
        AppFragment.hideKeyboard$default(this, null, 1, null);
        buildColorPickerDialog.show(getParentFragmentManager(), DIALOG_TAG_COLOR_PICKER);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void patchRole(long j, RestAPIParams.Role role) {
        ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateRole(j, role.getId(), role), false, 1, null), this, null, 2, null).k(o.j(WidgetServerSettingsEditRole$patchRole$1.INSTANCE, getContext(), null, 4));
    }

    private final void setRoleIcon(Model model) {
        if (model.getRole().d() != null) {
            String d = model.getRole().d();
            m.checkNotNull(d);
            if (d.length() > 0) {
                getBinding().L.setRole(model.getRole(), Long.valueOf(model.getGuildId()));
                TextView textView = getBinding().N;
                m.checkNotNullExpressionValue(textView, "binding.roleSettingsRoleIconHeader");
                textView.setVisibility(0);
                View view = getBinding().M;
                m.checkNotNullExpressionValue(view, "binding.roleSettingsRoleIconDivider");
                view.setVisibility(0);
                RelativeLayout relativeLayout = getBinding().t;
                m.checkNotNullExpressionValue(relativeLayout, "binding.roleSettingsIconContainer");
                relativeLayout.setVisibility(0);
                return;
            }
        }
        TextView textView2 = getBinding().N;
        m.checkNotNullExpressionValue(textView2, "binding.roleSettingsRoleIconHeader");
        textView2.setVisibility(8);
        View view2 = getBinding().M;
        m.checkNotNullExpressionValue(view2, "binding.roleSettingsRoleIconDivider");
        view2.setVisibility(8);
        RelativeLayout relativeLayout2 = getBinding().t;
        m.checkNotNullExpressionValue(relativeLayout2, "binding.roleSettingsIconContainer");
        relativeLayout2.setVisibility(8);
    }

    private final void setupActionBar(Model model) {
        int i;
        AppActivity appActivity = getAppActivity();
        Toolbar toolbar = appActivity != null ? appActivity.u : null;
        if (toolbar != null) {
            int roleColor = RoleUtils.getRoleColor(model.getRole(), requireContext(), Integer.valueOf(ColorCompat.getThemedColor(requireContext(), (int) R.attr.color_brand)));
            if (RoleUtils.isDefaultColor(model.getRole())) {
                i = ColorCompat.getThemedColor(this, (int) R.attr.color_brand_460);
            } else {
                Color.colorToHSV(roleColor, r3);
                float[] fArr = {0.0f, 0.0f, fArr[2] * 0.5f};
                i = Color.HSVToColor(fArr);
            }
            boolean isColorDark$default = ColorCompat.isColorDark$default(roleColor, 0.0f, 2, null);
            int color = ColorCompat.getColor(requireContext(), isColorDark$default ? R.color.primary_100 : R.color.primary_500);
            ColorCompat.setStatusBarColor(this, i, isColorDark$default);
            WidgetServerSettingsEditRole$setupActionBar$1 widgetServerSettingsEditRole$setupActionBar$1 = WidgetServerSettingsEditRole$setupActionBar$1.INSTANCE;
            AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
            String string = getString(R.string.form_label_role_settings);
            m.checkNotNullExpressionValue(string, "getString(R.string.form_label_role_settings)");
            setActionBarTitle(widgetServerSettingsEditRole$setupActionBar$1.invoke(string, color));
            setActionBarSubtitle(widgetServerSettingsEditRole$setupActionBar$1.invoke(model.getRole().g(), color));
            toolbar.setBackgroundColor(roleColor);
            Drawable navigationIcon = toolbar.getNavigationIcon();
            if (navigationIcon != null) {
                navigationIcon.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
            }
            Drawable overflowIcon = toolbar.getOverflowIcon();
            if (overflowIcon != null) {
                overflowIcon.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
            }
        }
    }

    private final void setupColorSetting(final Model model) {
        View view = getBinding().p;
        m.checkNotNullExpressionValue(view, "binding.roleSettingsCurrentColorDisplay");
        Drawable drawable = ContextCompat.getDrawable(view.getContext(), R.drawable.drawable_circle_white_1);
        if (drawable != null) {
            drawable.setColorFilter(new PorterDuffColorFilter(RoleUtils.getRoleColor$default(model.getRole(), requireContext(), null, 2, null), PorterDuff.Mode.SRC_ATOP));
            View view2 = getBinding().p;
            m.checkNotNullExpressionValue(view2, "binding.roleSettingsCurrentColorDisplay");
            view2.setBackground(drawable);
        }
        if (!model.canManage() || model.isEveryoneRole()) {
            View view3 = getBinding().k;
            m.checkNotNullExpressionValue(view3, "binding.roleSettingsColorDisabledOverlay");
            view3.setVisibility(0);
            getBinding().k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$setupColorSetting$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view4) {
                    String lockMessage;
                    WidgetServerSettingsEditRole widgetServerSettingsEditRole = WidgetServerSettingsEditRole.this;
                    lockMessage = widgetServerSettingsEditRole.getLockMessage(model, true);
                    b.a.d.m.j(widgetServerSettingsEditRole, lockMessage, 0, 4);
                }
            });
            return;
        }
        getBinding().l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$setupColorSetting$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view4) {
                WidgetServerSettingsEditRole.this.launchColorPicker(model);
            }
        });
        View view4 = getBinding().k;
        m.checkNotNullExpressionValue(view4, "binding.roleSettingsColorDisabledOverlay");
        view4.setVisibility(8);
        getBinding().k.setOnClickListener(null);
    }

    private final void setupHoistAndMentionSettings(final Model model) {
        CheckedSetting checkedSetting = getBinding().f2353s;
        m.checkNotNullExpressionValue(checkedSetting, "binding.roleSettingsHoistCheckedsetting");
        checkedSetting.setChecked(model.getRole().c());
        CheckedSetting checkedSetting2 = getBinding().F;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.roleSettingsMentionableCheckedsetting");
        checkedSetting2.setChecked(model.getRole().f());
        if (!model.canManage() || model.isEveryoneRole()) {
            String lockMessage = getLockMessage(model, true);
            getBinding().f2353s.c(lockMessage);
            getBinding().F.c(lockMessage);
            return;
        }
        getBinding().f2353s.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$setupHoistAndMentionSettings$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetEditRoleBinding binding;
                WidgetEditRoleBinding binding2;
                binding = WidgetServerSettingsEditRole.this.getBinding();
                binding.f2350b.clearFocus();
                RestAPIParams.Role createWithRole = RestAPIParams.Role.Companion.createWithRole(model.getRole());
                binding2 = WidgetServerSettingsEditRole.this.getBinding();
                CheckedSetting checkedSetting3 = binding2.f2353s;
                m.checkNotNullExpressionValue(checkedSetting3, "binding.roleSettingsHoistCheckedsetting");
                createWithRole.setHoist(Boolean.valueOf(!checkedSetting3.isChecked()));
                WidgetServerSettingsEditRole.this.patchRole(model.getGuildId(), createWithRole);
            }
        });
        getBinding().F.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$setupHoistAndMentionSettings$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetEditRoleBinding binding;
                WidgetEditRoleBinding binding2;
                binding = WidgetServerSettingsEditRole.this.getBinding();
                binding.f2350b.clearFocus();
                RestAPIParams.Role createWithRole = RestAPIParams.Role.Companion.createWithRole(model.getRole());
                binding2 = WidgetServerSettingsEditRole.this.getBinding();
                CheckedSetting checkedSetting3 = binding2.F;
                m.checkNotNullExpressionValue(checkedSetting3, "binding.roleSettingsMentionableCheckedsetting");
                createWithRole.setMentionable(Boolean.valueOf(!checkedSetting3.isChecked()));
                WidgetServerSettingsEditRole.this.patchRole(model.getGuildId(), createWithRole);
            }
        });
    }

    private final void setupMenu(final Model model) {
        if (!model.canManage() || model.isEveryoneRole() || model.getRole().e()) {
            AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_empty, null, null, 4, null);
        } else {
            AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_edit_role, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$setupMenu$1
                public final void call(MenuItem menuItem, Context context) {
                    m.checkNotNullParameter(menuItem, "menuItem");
                    if (menuItem.getItemId() == R.id.menu_edit_role_delete) {
                        ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteRole(model.getGuildId(), model.getRole().getId()), false, 1, null), WidgetServerSettingsEditRole.this, null, 2, null).k(o.j(new Action1<Void>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$setupMenu$1.1
                            public final void call(Void r1) {
                                AppActivity appActivity = WidgetServerSettingsEditRole.this.getAppActivity();
                                if (appActivity != null) {
                                    appActivity.onBackPressed();
                                }
                            }
                        }, WidgetServerSettingsEditRole.this.getContext(), null, 4));
                    }
                }
            }, null, 4, null);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:79:0x02e6  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x02e8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void setupPermissionsSettings(com.discord.widgets.servers.WidgetServerSettingsEditRole.Model r18) {
        /*
            Method dump skipped, instructions count: 956
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.WidgetServerSettingsEditRole.setupPermissionsSettings(com.discord.widgets.servers.WidgetServerSettingsEditRole$Model):void");
    }

    private final void setupRoleName(final Model model) {
        TextInputLayout textInputLayout = getBinding().f2350b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.editRoleName");
        StatefulViews statefulViews = this.state;
        TextInputLayout textInputLayout2 = getBinding().f2350b;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.editRoleName");
        ViewExtensions.setText(textInputLayout, (CharSequence) statefulViews.get(textInputLayout2.getId(), model.getRole().g()));
        if (!model.canManage() || model.isEveryoneRole()) {
            View view = getBinding().q;
            m.checkNotNullExpressionValue(view, "binding.roleSettingsEditNameDisabledOverlay");
            view.setVisibility(0);
            getBinding().q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditRole$setupRoleName$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    String lockMessage;
                    WidgetServerSettingsEditRole widgetServerSettingsEditRole = WidgetServerSettingsEditRole.this;
                    lockMessage = widgetServerSettingsEditRole.getLockMessage(model, true);
                    b.a.d.m.j(widgetServerSettingsEditRole, lockMessage, 0, 4);
                }
            });
            return;
        }
        View view2 = getBinding().q;
        m.checkNotNullExpressionValue(view2, "binding.roleSettingsEditNameDisabledOverlay");
        view2.setVisibility(8);
        getBinding().q.setOnClickListener(null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setRetainInstance(true);
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews = this.state;
        FloatingActionButton floatingActionButton = getBinding().c;
        TextInputLayout textInputLayout = getBinding().f2350b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.editRoleName");
        statefulViews.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout);
        FloatingActionButton floatingActionButton2 = getBinding().c;
        m.checkNotNullExpressionValue(floatingActionButton2, "binding.editRoleSave");
        floatingActionButton2.setVisibility(8);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Model.Companion.get(getMostRecentIntent().getLongExtra("INTENT_EXTRA_GUILD_ID", -1L), getMostRecentIntent().getLongExtra(INTENT_EXTRA_ROLE_ID, -1L))), this, null, 2, null), WidgetServerSettingsEditRole.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsEditRole$onViewBoundOrOnResume$1(this));
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Fragment findFragmentByTag = parentFragmentManager.findFragmentByTag(DIALOG_TAG_COLOR_PICKER);
        if (findFragmentByTag != null) {
            parentFragmentManager.beginTransaction().remove(findFragmentByTag).commit();
        }
    }
}
