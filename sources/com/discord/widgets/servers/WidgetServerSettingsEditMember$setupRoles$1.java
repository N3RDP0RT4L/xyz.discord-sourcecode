package com.discord.widgets.servers;

import com.discord.databinding.WidgetServerSettingsEditMemberBinding;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.servers.WidgetServerSettingsEditMember;
import d0.z.d.o;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetServerSettingsEditMember.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/RoleId;", "clickedRoleId", "", "invoke", "(J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEditMember$setupRoles$1 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ WidgetServerSettingsEditMember.Model $data;
    public final /* synthetic */ WidgetServerSettingsEditMember this$0;

    /* compiled from: WidgetServerSettingsEditMember.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.WidgetServerSettingsEditMember$setupRoles$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Void, Unit> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
            invoke2(r1);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Void r1) {
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsEditMember$setupRoles$1(WidgetServerSettingsEditMember widgetServerSettingsEditMember, WidgetServerSettingsEditMember.Model model) {
        super(1);
        this.this$0 = widgetServerSettingsEditMember;
        this.$data = model;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    public final void invoke(long j) {
        WidgetServerSettingsEditMemberBinding binding;
        binding = this.this$0.getBinding();
        binding.e.clearFocus();
        ArrayList arrayList = new ArrayList();
        if (this.$data.getUserRoles().contains(Long.valueOf(j))) {
            for (Long l : this.$data.getUserRoles()) {
                long longValue = l.longValue();
                if (longValue != j) {
                    arrayList.add(Long.valueOf(longValue));
                }
            }
        } else {
            arrayList.addAll(this.$data.getUserRoles());
            arrayList.add(Long.valueOf(j));
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().changeGuildMember(this.$data.getGuild().getId(), this.$data.getUser().getId(), RestAPIParams.GuildMember.Companion.createWithRoles(arrayList)), false, 1, null), this.this$0, null, 2, null), this.this$0.getClass(), (r18 & 2) != 0 ? null : this.this$0.requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, AnonymousClass1.INSTANCE);
    }
}
