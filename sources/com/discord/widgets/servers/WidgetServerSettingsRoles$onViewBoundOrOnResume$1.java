package com.discord.widgets.servers;

import com.discord.widgets.servers.WidgetServerSettingsRoles;
import d0.z.d.k;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetServerSettingsRoles.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;", "p1", "", "invoke", "(Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettingsRoles$onViewBoundOrOnResume$1 extends k implements Function1<WidgetServerSettingsRoles.Model, Unit> {
    public WidgetServerSettingsRoles$onViewBoundOrOnResume$1(WidgetServerSettingsRoles widgetServerSettingsRoles) {
        super(1, widgetServerSettingsRoles, WidgetServerSettingsRoles.class, "configureUI", "configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetServerSettingsRoles.Model model) {
        invoke2(model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetServerSettingsRoles.Model model) {
        ((WidgetServerSettingsRoles) this.receiver).configureUI(model);
    }
}
