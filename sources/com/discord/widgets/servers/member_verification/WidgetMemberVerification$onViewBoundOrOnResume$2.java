package com.discord.widgets.servers.member_verification;

import com.discord.widgets.servers.member_verification.MemberVerificationPendingViewModel;
import com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetMemberVerification.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event;", "event", "", "invoke", "(Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMemberVerification$onViewBoundOrOnResume$2 extends o implements Function1<WidgetMemberVerificationViewModel.Event, Unit> {
    public final /* synthetic */ WidgetMemberVerification this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetMemberVerification$onViewBoundOrOnResume$2(WidgetMemberVerification widgetMemberVerification) {
        super(1);
        this.this$0 = widgetMemberVerification;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetMemberVerificationViewModel.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetMemberVerificationViewModel.Event event) {
        m.checkNotNullParameter(event, "event");
        if (event instanceof WidgetMemberVerificationViewModel.Event.Success) {
            this.this$0.requireActivity().finish();
            WidgetMemberVerificationViewModel.Event.Success success = (WidgetMemberVerificationViewModel.Event.Success) event;
            int ordinal = success.getApplicationStatus().ordinal();
            if (ordinal != 1) {
                if (ordinal == 3 && success.getGuildId() != null && success.getGuildName() != null && success.getLastSeen() == null) {
                    MemberVerificationSuccessDialog.Companion.enqueue(success.getGuildId().longValue());
                }
            } else if (success.getGuildId() != null && success.getGuildName() != null) {
                MemberVerificationPendingDialog.Companion.enqueue(success.getGuildId().longValue(), MemberVerificationPendingViewModel.DialogState.PENDING);
            }
        } else if (event instanceof WidgetMemberVerificationViewModel.Event.Error) {
            b.a.d.m.i(this.this$0, R.string.guild_settings_public_update_failed, 0, 4);
        }
    }
}
