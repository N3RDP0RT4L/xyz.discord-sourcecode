package com.discord.widgets.servers.member_verification.form_fields;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.simpleast.core.node.Node;
import com.discord.utilities.textprocessing.MessageRenderContext;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: MemberVerificationItemTerm.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0010\u0000\n\u0002\b\u0015\b\u0086\b\u0018\u00002\u00020\u0001Bo\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\u0006\u0010\u001a\u001a\u00020\u0005\u0012\u0012\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b\u0012\u0016\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u00050\r\u0012\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00120\r\u0012\u0006\u0010\u001e\u001a\u00020\u0014\u0012\u0006\u0010\u001f\u001a\u00020\u0014\u0012\u0006\u0010 \u001a\u00020\u0014¢\u0006\u0004\b8\u00109J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001c\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ \u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u00050\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u001c\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00120\rHÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0011J\u0010\u0010\u0015\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0014HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0016J\u0088\u0001\u0010!\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00052\u0014\b\u0002\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b2\u0018\b\u0002\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u00050\r2\u0014\b\u0002\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00120\r2\b\b\u0002\u0010\u001e\u001a\u00020\u00142\b\b\u0002\u0010\u001f\u001a\u00020\u00142\b\b\u0002\u0010 \u001a\u00020\u0014HÆ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010#\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b#\u0010\u0007J\u0010\u0010$\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b$\u0010\u0004J\u001a\u0010'\u001a\u00020\u00142\b\u0010&\u001a\u0004\u0018\u00010%HÖ\u0003¢\u0006\u0004\b'\u0010(R\u0019\u0010\u0019\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010)\u001a\u0004\b*\u0010\u0004R%\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010+\u001a\u0004\b,\u0010\fR\u001c\u0010-\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b-\u0010)\u001a\u0004\b.\u0010\u0004R%\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00120\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010/\u001a\u0004\b0\u0010\u0011R\u0019\u0010 \u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b \u00101\u001a\u0004\b \u0010\u0016R\u0019\u0010\u001f\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u00101\u001a\u0004\b\u001f\u0010\u0016R\u0019\u0010\u001a\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00102\u001a\u0004\b3\u0010\u0007R\u0019\u0010\u001e\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00101\u001a\u0004\b4\u0010\u0016R\u001c\u00105\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b5\u00102\u001a\u0004\b6\u0010\u0007R)\u0010\u001c\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u00050\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010/\u001a\u0004\b7\u0010\u0011¨\u0006:"}, d2 = {"Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItemTerm;", "Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItem;", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "component3", "()Ljava/util/List;", "", "", "Lcom/discord/primitives/ChannelId;", "component4", "()Ljava/util/Map;", "Lcom/discord/api/role/GuildRole;", "component5", "", "component6", "()Z", "component7", "component8", "index", "rule", "ast", "channelNames", "roles", "allowAnimatedEmojis", "isFirstItem", "isLastItem", "copy", "(ILjava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;ZZZ)Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItemTerm;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getIndex", "Ljava/util/List;", "getAst", "type", "getType", "Ljava/util/Map;", "getRoles", "Z", "Ljava/lang/String;", "getRule", "getAllowAnimatedEmojis", "key", "getKey", "getChannelNames", HookHelper.constructorName, "(ILjava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;ZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MemberVerificationItemTerm implements MemberVerificationItem {
    private final boolean allowAnimatedEmojis;
    private final List<Node<MessageRenderContext>> ast;
    private final Map<Long, String> channelNames;
    private final int index;
    private final boolean isFirstItem;
    private final boolean isLastItem;
    private final String key;
    private final Map<Long, GuildRole> roles;
    private final String rule;
    private final int type = 3;

    public MemberVerificationItemTerm(int i, String str, List<Node<MessageRenderContext>> list, Map<Long, String> map, Map<Long, GuildRole> map2, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(str, "rule");
        m.checkNotNullParameter(list, "ast");
        m.checkNotNullParameter(map, "channelNames");
        m.checkNotNullParameter(map2, "roles");
        this.index = i;
        this.rule = str;
        this.ast = list;
        this.channelNames = map;
        this.roles = map2;
        this.allowAnimatedEmojis = z2;
        this.isFirstItem = z3;
        this.isLastItem = z4;
        this.key = getType() + i + str;
    }

    public final int component1() {
        return this.index;
    }

    public final String component2() {
        return this.rule;
    }

    public final List<Node<MessageRenderContext>> component3() {
        return this.ast;
    }

    public final Map<Long, String> component4() {
        return this.channelNames;
    }

    public final Map<Long, GuildRole> component5() {
        return this.roles;
    }

    public final boolean component6() {
        return this.allowAnimatedEmojis;
    }

    public final boolean component7() {
        return this.isFirstItem;
    }

    public final boolean component8() {
        return this.isLastItem;
    }

    public final MemberVerificationItemTerm copy(int i, String str, List<Node<MessageRenderContext>> list, Map<Long, String> map, Map<Long, GuildRole> map2, boolean z2, boolean z3, boolean z4) {
        m.checkNotNullParameter(str, "rule");
        m.checkNotNullParameter(list, "ast");
        m.checkNotNullParameter(map, "channelNames");
        m.checkNotNullParameter(map2, "roles");
        return new MemberVerificationItemTerm(i, str, list, map, map2, z2, z3, z4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MemberVerificationItemTerm)) {
            return false;
        }
        MemberVerificationItemTerm memberVerificationItemTerm = (MemberVerificationItemTerm) obj;
        return this.index == memberVerificationItemTerm.index && m.areEqual(this.rule, memberVerificationItemTerm.rule) && m.areEqual(this.ast, memberVerificationItemTerm.ast) && m.areEqual(this.channelNames, memberVerificationItemTerm.channelNames) && m.areEqual(this.roles, memberVerificationItemTerm.roles) && this.allowAnimatedEmojis == memberVerificationItemTerm.allowAnimatedEmojis && this.isFirstItem == memberVerificationItemTerm.isFirstItem && this.isLastItem == memberVerificationItemTerm.isLastItem;
    }

    public final boolean getAllowAnimatedEmojis() {
        return this.allowAnimatedEmojis;
    }

    public final List<Node<MessageRenderContext>> getAst() {
        return this.ast;
    }

    public final Map<Long, String> getChannelNames() {
        return this.channelNames;
    }

    public final int getIndex() {
        return this.index;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final Map<Long, GuildRole> getRoles() {
        return this.roles;
    }

    public final String getRule() {
        return this.rule;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        int i = this.index * 31;
        String str = this.rule;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        List<Node<MessageRenderContext>> list = this.ast;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        Map<Long, String> map = this.channelNames;
        int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
        Map<Long, GuildRole> map2 = this.roles;
        if (map2 != null) {
            i2 = map2.hashCode();
        }
        int i3 = (hashCode3 + i2) * 31;
        boolean z2 = this.allowAnimatedEmojis;
        int i4 = 1;
        if (z2) {
            z2 = true;
        }
        int i5 = z2 ? 1 : 0;
        int i6 = z2 ? 1 : 0;
        int i7 = (i3 + i5) * 31;
        boolean z3 = this.isFirstItem;
        if (z3) {
            z3 = true;
        }
        int i8 = z3 ? 1 : 0;
        int i9 = z3 ? 1 : 0;
        int i10 = (i7 + i8) * 31;
        boolean z4 = this.isLastItem;
        if (!z4) {
            i4 = z4 ? 1 : 0;
        }
        return i10 + i4;
    }

    public final boolean isFirstItem() {
        return this.isFirstItem;
    }

    public final boolean isLastItem() {
        return this.isLastItem;
    }

    public String toString() {
        StringBuilder R = a.R("MemberVerificationItemTerm(index=");
        R.append(this.index);
        R.append(", rule=");
        R.append(this.rule);
        R.append(", ast=");
        R.append(this.ast);
        R.append(", channelNames=");
        R.append(this.channelNames);
        R.append(", roles=");
        R.append(this.roles);
        R.append(", allowAnimatedEmojis=");
        R.append(this.allowAnimatedEmojis);
        R.append(", isFirstItem=");
        R.append(this.isFirstItem);
        R.append(", isLastItem=");
        return a.M(R, this.isLastItem, ")");
    }
}
