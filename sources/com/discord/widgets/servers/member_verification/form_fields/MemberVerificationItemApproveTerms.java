package com.discord.widgets.servers.member_verification.form_fields;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import kotlin.Metadata;
/* compiled from: MemberVerificationItemApproveTerms.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u001a\u0010\u0012\u001a\u00020\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0014\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001c\u0010\u0017\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u000eR\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001b\u001a\u0004\b\t\u0010\u0007¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItemApproveTerms;", "Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItem;", "", "component1", "()I", "", "component2", "()Z", "fieldIndex", "isApproved", "copy", "(IZ)Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItemApproveTerms;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "getFieldIndex", "Z", HookHelper.constructorName, "(IZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MemberVerificationItemApproveTerms implements MemberVerificationItem {
    private final int fieldIndex;
    private final boolean isApproved;
    private final int type = 2;
    private final String key = String.valueOf(getType());

    public MemberVerificationItemApproveTerms(int i, boolean z2) {
        this.fieldIndex = i;
        this.isApproved = z2;
    }

    public static /* synthetic */ MemberVerificationItemApproveTerms copy$default(MemberVerificationItemApproveTerms memberVerificationItemApproveTerms, int i, boolean z2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = memberVerificationItemApproveTerms.fieldIndex;
        }
        if ((i2 & 2) != 0) {
            z2 = memberVerificationItemApproveTerms.isApproved;
        }
        return memberVerificationItemApproveTerms.copy(i, z2);
    }

    public final int component1() {
        return this.fieldIndex;
    }

    public final boolean component2() {
        return this.isApproved;
    }

    public final MemberVerificationItemApproveTerms copy(int i, boolean z2) {
        return new MemberVerificationItemApproveTerms(i, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MemberVerificationItemApproveTerms)) {
            return false;
        }
        MemberVerificationItemApproveTerms memberVerificationItemApproveTerms = (MemberVerificationItemApproveTerms) obj;
        return this.fieldIndex == memberVerificationItemApproveTerms.fieldIndex && this.isApproved == memberVerificationItemApproveTerms.isApproved;
    }

    public final int getFieldIndex() {
        return this.fieldIndex;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        int i = this.fieldIndex * 31;
        boolean z2 = this.isApproved;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        return i + i2;
    }

    public final boolean isApproved() {
        return this.isApproved;
    }

    public String toString() {
        StringBuilder R = a.R("MemberVerificationItemApproveTerms(fieldIndex=");
        R.append(this.fieldIndex);
        R.append(", isApproved=");
        return a.M(R, this.isApproved, ")");
    }
}
