package com.discord.widgets.servers.member_verification.form_fields;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: MemberVerificationItemParagraph.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\f\u0010\u0007J\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0016\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007R\u001c\u0010\u0019\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0019\u0010\u0017\u001a\u0004\b\u001a\u0010\u0007¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItemParagraph;", "Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItem;", "", "component1", "()I", "", "component2", "()Ljava/lang/String;", "fieldIndex", "response", "copy", "(ILjava/lang/String;)Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItemParagraph;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "getFieldIndex", "Ljava/lang/String;", "getResponse", "key", "getKey", HookHelper.constructorName, "(ILjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MemberVerificationItemParagraph implements MemberVerificationItem {
    private final int fieldIndex;
    private final String key;
    private final String response;
    private final int type = 5;

    public MemberVerificationItemParagraph(int i, String str) {
        this.fieldIndex = i;
        this.response = str;
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(getType());
        this.key = sb.toString();
    }

    public static /* synthetic */ MemberVerificationItemParagraph copy$default(MemberVerificationItemParagraph memberVerificationItemParagraph, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = memberVerificationItemParagraph.fieldIndex;
        }
        if ((i2 & 2) != 0) {
            str = memberVerificationItemParagraph.response;
        }
        return memberVerificationItemParagraph.copy(i, str);
    }

    public final int component1() {
        return this.fieldIndex;
    }

    public final String component2() {
        return this.response;
    }

    public final MemberVerificationItemParagraph copy(int i, String str) {
        return new MemberVerificationItemParagraph(i, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MemberVerificationItemParagraph)) {
            return false;
        }
        MemberVerificationItemParagraph memberVerificationItemParagraph = (MemberVerificationItemParagraph) obj;
        return this.fieldIndex == memberVerificationItemParagraph.fieldIndex && m.areEqual(this.response, memberVerificationItemParagraph.response);
    }

    public final int getFieldIndex() {
        return this.fieldIndex;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final String getResponse() {
        return this.response;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        int i = this.fieldIndex * 31;
        String str = this.response;
        return i + (str != null ? str.hashCode() : 0);
    }

    public String toString() {
        StringBuilder R = a.R("MemberVerificationItemParagraph(fieldIndex=");
        R.append(this.fieldIndex);
        R.append(", response=");
        return a.H(R, this.response, ")");
    }
}
