package com.discord.widgets.servers.member_verification.form_fields;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
/* compiled from: MemberVerificationItemMultipleChoice.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b#\u0010$J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ6\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0004J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0018\u001a\u0004\b\u0019\u0010\nR\u001c\u0010\u001a\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001c\u0010\u001d\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010\u0011R\u001f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010 \u001a\u0004\b!\u0010\bR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\"\u0010\u0004¨\u0006%"}, d2 = {"Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItemMultipleChoice;", "Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItem;", "", "component1", "()I", "", "", "component2", "()Ljava/util/List;", "component3", "()Ljava/lang/Integer;", "fieldIndex", "choices", "response", "copy", "(ILjava/util/List;Ljava/lang/Integer;)Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItemMultipleChoice;", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getResponse", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Ljava/util/List;", "getChoices", "getFieldIndex", HookHelper.constructorName, "(ILjava/util/List;Ljava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MemberVerificationItemMultipleChoice implements MemberVerificationItem {
    private final List<String> choices;
    private final int fieldIndex;
    private final String key;
    private final Integer response;
    private final int type = 6;

    public MemberVerificationItemMultipleChoice(int i, List<String> list, Integer num) {
        m.checkNotNullParameter(list, "choices");
        this.fieldIndex = i;
        this.choices = list;
        this.response = num;
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(getType());
        this.key = sb.toString();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ MemberVerificationItemMultipleChoice copy$default(MemberVerificationItemMultipleChoice memberVerificationItemMultipleChoice, int i, List list, Integer num, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = memberVerificationItemMultipleChoice.fieldIndex;
        }
        if ((i2 & 2) != 0) {
            list = memberVerificationItemMultipleChoice.choices;
        }
        if ((i2 & 4) != 0) {
            num = memberVerificationItemMultipleChoice.response;
        }
        return memberVerificationItemMultipleChoice.copy(i, list, num);
    }

    public final int component1() {
        return this.fieldIndex;
    }

    public final List<String> component2() {
        return this.choices;
    }

    public final Integer component3() {
        return this.response;
    }

    public final MemberVerificationItemMultipleChoice copy(int i, List<String> list, Integer num) {
        m.checkNotNullParameter(list, "choices");
        return new MemberVerificationItemMultipleChoice(i, list, num);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MemberVerificationItemMultipleChoice)) {
            return false;
        }
        MemberVerificationItemMultipleChoice memberVerificationItemMultipleChoice = (MemberVerificationItemMultipleChoice) obj;
        return this.fieldIndex == memberVerificationItemMultipleChoice.fieldIndex && m.areEqual(this.choices, memberVerificationItemMultipleChoice.choices) && m.areEqual(this.response, memberVerificationItemMultipleChoice.response);
    }

    public final List<String> getChoices() {
        return this.choices;
    }

    public final int getFieldIndex() {
        return this.fieldIndex;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
    public String getKey() {
        return this.key;
    }

    public final Integer getResponse() {
        return this.response;
    }

    @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
    public int getType() {
        return this.type;
    }

    public int hashCode() {
        int i = this.fieldIndex * 31;
        List<String> list = this.choices;
        int i2 = 0;
        int hashCode = (i + (list != null ? list.hashCode() : 0)) * 31;
        Integer num = this.response;
        if (num != null) {
            i2 = num.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder R = a.R("MemberVerificationItemMultipleChoice(fieldIndex=");
        R.append(this.fieldIndex);
        R.append(", choices=");
        R.append(this.choices);
        R.append(", response=");
        return a.E(R, this.response, ")");
    }
}
