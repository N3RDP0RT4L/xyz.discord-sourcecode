package com.discord.widgets.servers.member_verification.form_fields;

import andhook.lib.HookHelper;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import kotlin.Metadata;
/* compiled from: MemberVerificationItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002¨\u0006\u0003"}, d2 = {"Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public interface MemberVerificationItem extends MGRecyclerDataPayload {
    public static final int APPROVE_TERMS = 2;
    public static final Companion Companion = Companion.$$INSTANCE;
    public static final int HEADER = 0;
    public static final int MULTIPLE_CHOICE = 6;
    public static final int PARAGRAPH = 5;
    public static final int TERM = 3;
    public static final int TERMS_HEADER = 1;
    public static final int TEXT_INPUT = 4;

    /* compiled from: MemberVerificationItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004R\u0016\u0010\t\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\t\u0010\u0004R\u0016\u0010\n\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\u0004¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItem$Companion;", "", "", "TEXT_INPUT", "I", "TERM", "APPROVE_TERMS", "TERMS_HEADER", "PARAGRAPH", "MULTIPLE_CHOICE", "HEADER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        public static final /* synthetic */ Companion $$INSTANCE = new Companion();
        public static final int APPROVE_TERMS = 2;
        public static final int HEADER = 0;
        public static final int MULTIPLE_CHOICE = 6;
        public static final int PARAGRAPH = 5;
        public static final int TERM = 3;
        public static final int TERMS_HEADER = 1;
        public static final int TEXT_INPUT = 4;

        private Companion() {
        }
    }
}
