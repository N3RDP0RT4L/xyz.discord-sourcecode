package com.discord.widgets.servers.member_verification;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelMemberVerificationForm;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.simpleast.core.node.Node;
import com.discord.simpleast.core.parser.Parser;
import com.discord.stores.StoreGuildMemberVerificationForm;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.MessageParseState;
import com.discord.utilities.textprocessing.MessageRenderContext;
import com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel;
import com.discord.widgets.servers.member_verification.form_fields.MemberVerificationItem;
import com.discord.widgets.servers.member_verification.form_fields.MemberVerificationItemApproveTerms;
import com.discord.widgets.servers.member_verification.form_fields.MemberVerificationItemHeader;
import com.discord.widgets.servers.member_verification.form_fields.MemberVerificationItemMultipleChoice;
import com.discord.widgets.servers.member_verification.form_fields.MemberVerificationItemParagraph;
import com.discord.widgets.servers.member_verification.form_fields.MemberVerificationItemTerm;
import com.discord.widgets.servers.member_verification.form_fields.MemberVerificationItemTermsHeader;
import com.discord.widgets.servers.member_verification.form_fields.MemberVerificationItemTextInput;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func6;
import rx.subjects.PublishSubject;
/* compiled from: WidgetMemberVerificationViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ä\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 N2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0005NOPQRBq\u0012\n\u0010?\u001a\u00060\u001aj\u0002`>\u0012\u0006\u0010A\u001a\u00020\u001c\u0012\n\b\u0002\u00103\u001a\u0004\u0018\u000102\u0012\b\b\u0002\u0010J\u001a\u00020I\u0012\b\b\u0002\u0010D\u001a\u00020C\u0012$\b\u0002\u0010<\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0004\u0012\u00020:09j\u0002`;\u0012\u000e\b\u0002\u0010K\u001a\b\u0012\u0004\u0012\u00020\u000b0$¢\u0006\u0004\bL\u0010MJ'\u0010\t\u001a\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0005j\u0002`\b2\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u000bH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\rH\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0013\u0010\u0014Ja\u0010\"\u001a\b\u0012\u0004\u0012\u00020!0\u00152\u000e\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u00152\u0006\u0010\u0018\u001a\u00020\u00122\u0016\u0010\u001d\u001a\u0012\u0012\b\u0012\u00060\u001aj\u0002`\u001b\u0012\u0004\u0012\u00020\u001c0\u00192\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001e0\u00192\u0006\u0010 \u001a\u00020\u0012H\u0002¢\u0006\u0004\b\"\u0010#J\u0013\u0010&\u001a\b\u0012\u0004\u0012\u00020%0$¢\u0006\u0004\b&\u0010'J\r\u0010(\u001a\u00020\r¢\u0006\u0004\b(\u0010\u0011J\u0015\u0010)\u001a\u00020\r2\u0006\u0010\u0018\u001a\u00020\u0012¢\u0006\u0004\b)\u0010*J\u001f\u0010/\u001a\u00020\r2\u0006\u0010,\u001a\u00020+2\b\u0010.\u001a\u0004\u0018\u00010-¢\u0006\u0004\b/\u00100J\u000f\u00101\u001a\u00020\rH\u0014¢\u0006\u0004\b1\u0010\u0011R\u0018\u00103\u001a\u0004\u0018\u0001028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R:\u00107\u001a&\u0012\f\u0012\n 6*\u0004\u0018\u00010%0% 6*\u0012\u0012\f\u0012\n 6*\u0004\u0018\u00010%0%\u0018\u000105058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00108R2\u0010<\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0004\u0012\u00020:09j\u0002`;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R\u001a\u0010?\u001a\u00060\u001aj\u0002`>8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010@R\u0016\u0010A\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010ER$\u0010G\u001a\u0010\u0012\u0004\u0012\u00020+\u0012\u0006\u0012\u0004\u0018\u00010-0F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u0010H¨\u0006S"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState;", "", "rawRuleString", "", "Lcom/discord/simpleast/core/node/Node;", "Lcom/discord/utilities/textprocessing/MessageRenderContext;", "Lcom/discord/widgets/servers/member_verification/AST;", "generateAST", "(Ljava/lang/CharSequence;)Ljava/util/List;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$StoreState;)V", "handleGuildUpdateError", "()V", "", "isFormValid", "()Z", "", "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;", "formFields", "isTermsApproved", "", "", "Lcom/discord/primitives/ChannelId;", "", "channelNames", "Lcom/discord/api/role/GuildRole;", "roles", "allowAnimatedEmojis", "Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItem;", "createFormItems", "(Ljava/util/List;ZLjava/util/Map;Ljava/util/Map;Z)Ljava/util/List;", "Lrx/Observable;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event;", "observeEvents", "()Lrx/Observable;", "applyToJoinGuild", "updateTermsApproval", "(Z)V", "", "key", "", "value", "updateFormValidation", "(ILjava/lang/Object;)V", "onCleared", "Lcom/discord/models/guild/Guild;", "inviteGuild", "Lcom/discord/models/guild/Guild;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/utilities/textprocessing/MessageParseState;", "Lcom/discord/widgets/servers/member_verification/RulesParser;", "rulesParser", "Lcom/discord/simpleast/core/parser/Parser;", "Lcom/discord/primitives/GuildId;", "guildId", "J", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "Ljava/lang/String;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "", "formUserInputDataMap", "Ljava/util/Map;", "Lcom/discord/stores/StoreGuildMemberVerificationForm;", "storeGuildMemberVerificationForm", "storeObservable", HookHelper.constructorName, "(JLjava/lang/String;Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildMemberVerificationForm;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/simpleast/core/parser/Parser;Lrx/Observable;)V", "Companion", "Event", "StoreState", "VerificationType", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMemberVerificationViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final Map<Integer, Object> formUserInputDataMap;
    private final long guildId;
    private final Guild inviteGuild;
    private final String location;
    private final RestAPI restAPI;
    private final Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> rulesParser;

    /* compiled from: WidgetMemberVerificationViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetMemberVerificationViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetMemberVerificationViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ)\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/models/guild/Guild;", "inviteGuild", "Lrx/Observable;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$StoreState;", "observeStores", "(JLcom/discord/models/guild/Guild;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<StoreState> observeStores(long j, final Guild guild) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<StoreState> f = Observable.f(companion.getUsers().observeMe(true), companion.getGuilds().observeGuild(j), companion.getMemberVerificationForms().observeMemberVerificationFormData(j), companion.getChannels().observeNames(), companion.getGuilds().observeRoles(j), StoreUserSettings.observeIsAnimatedEmojisEnabled$default(companion.getUserSettings(), false, 1, null), new Func6<MeUser, Guild, StoreGuildMemberVerificationForm.MemberVerificationFormData, Map<Long, ? extends String>, Map<Long, ? extends GuildRole>, Boolean, StoreState>() { // from class: com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel$Companion$observeStores$1
                @Override // rx.functions.Func6
                public /* bridge */ /* synthetic */ WidgetMemberVerificationViewModel.StoreState call(MeUser meUser, Guild guild2, StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData, Map<Long, ? extends String> map, Map<Long, ? extends GuildRole> map2, Boolean bool) {
                    return call2(meUser, guild2, memberVerificationFormData, (Map<Long, String>) map, (Map<Long, GuildRole>) map2, bool);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final WidgetMemberVerificationViewModel.StoreState call2(MeUser meUser, Guild guild2, StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData, Map<Long, String> map, Map<Long, GuildRole> map2, Boolean bool) {
                    m.checkNotNullExpressionValue(meUser, "me");
                    if (guild2 == null) {
                        guild2 = Guild.this;
                    }
                    m.checkNotNullExpressionValue(map, "channels");
                    m.checkNotNullExpressionValue(map2, "roles");
                    m.checkNotNullExpressionValue(bool, "allowAnimatedEmojis");
                    return new WidgetMemberVerificationViewModel.StoreState(meUser, guild2, memberVerificationFormData, map, map2, bool.booleanValue());
                }
            });
            m.checkNotNullExpressionValue(f, "Observable\n            .…          )\n            }");
            return f;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetMemberVerificationViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event;", "", HookHelper.constructorName, "()V", "Error", "Success", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event$Success;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetMemberVerificationViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event$Error;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends Event {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        /* compiled from: WidgetMemberVerificationViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B3\u0012\u000e\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u000f\u001a\u00020\t\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b#\u0010$J\u0018\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\f\u0010\bJD\u0010\u0011\u001a\u00020\u00002\u0010\b\u0002\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u000f\u001a\u00020\t2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0013\u0010\bJ\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u001a\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR!\u0010\r\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\u0005R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\bR\u0019\u0010\u000f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u000bR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\"\u0010\b¨\u0006%"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event$Success;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event;", "", "Lcom/discord/primitives/GuildId;", "component1", "()Ljava/lang/Long;", "", "component2", "()Ljava/lang/String;", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "component3", "()Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "component4", "guildId", "guildName", "applicationStatus", "lastSeen", "copy", "(Ljava/lang/Long;Ljava/lang/String;Lcom/discord/api/guildjoinrequest/ApplicationStatus;Ljava/lang/String;)Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$Event$Success;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getGuildId", "Ljava/lang/String;", "getGuildName", "Lcom/discord/api/guildjoinrequest/ApplicationStatus;", "getApplicationStatus", "getLastSeen", HookHelper.constructorName, "(Ljava/lang/Long;Ljava/lang/String;Lcom/discord/api/guildjoinrequest/ApplicationStatus;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Success extends Event {
            private final ApplicationStatus applicationStatus;
            private final Long guildId;
            private final String guildName;
            private final String lastSeen;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Success(Long l, String str, ApplicationStatus applicationStatus, String str2) {
                super(null);
                m.checkNotNullParameter(applicationStatus, "applicationStatus");
                this.guildId = l;
                this.guildName = str;
                this.applicationStatus = applicationStatus;
                this.lastSeen = str2;
            }

            public static /* synthetic */ Success copy$default(Success success, Long l, String str, ApplicationStatus applicationStatus, String str2, int i, Object obj) {
                if ((i & 1) != 0) {
                    l = success.guildId;
                }
                if ((i & 2) != 0) {
                    str = success.guildName;
                }
                if ((i & 4) != 0) {
                    applicationStatus = success.applicationStatus;
                }
                if ((i & 8) != 0) {
                    str2 = success.lastSeen;
                }
                return success.copy(l, str, applicationStatus, str2);
            }

            public final Long component1() {
                return this.guildId;
            }

            public final String component2() {
                return this.guildName;
            }

            public final ApplicationStatus component3() {
                return this.applicationStatus;
            }

            public final String component4() {
                return this.lastSeen;
            }

            public final Success copy(Long l, String str, ApplicationStatus applicationStatus, String str2) {
                m.checkNotNullParameter(applicationStatus, "applicationStatus");
                return new Success(l, str, applicationStatus, str2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Success)) {
                    return false;
                }
                Success success = (Success) obj;
                return m.areEqual(this.guildId, success.guildId) && m.areEqual(this.guildName, success.guildName) && m.areEqual(this.applicationStatus, success.applicationStatus) && m.areEqual(this.lastSeen, success.lastSeen);
            }

            public final ApplicationStatus getApplicationStatus() {
                return this.applicationStatus;
            }

            public final Long getGuildId() {
                return this.guildId;
            }

            public final String getGuildName() {
                return this.guildName;
            }

            public final String getLastSeen() {
                return this.lastSeen;
            }

            public int hashCode() {
                Long l = this.guildId;
                int i = 0;
                int hashCode = (l != null ? l.hashCode() : 0) * 31;
                String str = this.guildName;
                int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
                ApplicationStatus applicationStatus = this.applicationStatus;
                int hashCode3 = (hashCode2 + (applicationStatus != null ? applicationStatus.hashCode() : 0)) * 31;
                String str2 = this.lastSeen;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return hashCode3 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Success(guildId=");
                R.append(this.guildId);
                R.append(", guildName=");
                R.append(this.guildName);
                R.append(", applicationStatus=");
                R.append(this.applicationStatus);
                R.append(", lastSeen=");
                return a.H(R, this.lastSeen, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetMemberVerificationViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001BW\u0012\u0006\u0010\u0016\u001a\u00020\u0002\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0018\u001a\u0004\u0018\u00010\b\u0012\u0016\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000b\u0012\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00110\u000b\u0012\u0006\u0010\u001b\u001a\u00020\u0013¢\u0006\u0004\b1\u00102J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ \u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u001c\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00110\u000bHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0010J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015Jl\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0016\u001a\u00020\u00022\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\b2\u0018\b\u0002\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000b2\u0014\b\u0002\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00110\u000b2\b\b\u0002\u0010\u001b\u001a\u00020\u0013HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u001a\u0010$\u001a\u00020\u00132\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\u0007R\u001b\u0010\u0018\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b)\u0010\nR%\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00110\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010*\u001a\u0004\b+\u0010\u0010R\u0019\u0010\u001b\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010,\u001a\u0004\b-\u0010\u0015R)\u0010\u0019\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010*\u001a\u0004\b.\u0010\u0010R\u0019\u0010\u0016\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010/\u001a\u0004\b0\u0010\u0004¨\u00063"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$StoreState;", "", "Lcom/discord/models/user/MeUser;", "component1", "()Lcom/discord/models/user/MeUser;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "component3", "()Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "", "", "Lcom/discord/primitives/ChannelId;", "", "component4", "()Ljava/util/Map;", "Lcom/discord/api/role/GuildRole;", "component5", "", "component6", "()Z", "me", "guild", "memberVerificationFormData", "channels", "roles", "allowAnimatedEmojis", "copy", "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;Ljava/util/Map;Ljava/util/Map;Z)Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$StoreState;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;", "getMemberVerificationFormData", "Ljava/util/Map;", "getRoles", "Z", "getAllowAnimatedEmojis", "getChannels", "Lcom/discord/models/user/MeUser;", "getMe", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildMemberVerificationForm$MemberVerificationFormData;Ljava/util/Map;Ljava/util/Map;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final boolean allowAnimatedEmojis;
        private final Map<Long, String> channels;
        private final Guild guild;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2839me;
        private final StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData;
        private final Map<Long, GuildRole> roles;

        public StoreState(MeUser meUser, Guild guild, StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData, Map<Long, String> map, Map<Long, GuildRole> map2, boolean z2) {
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(map, "channels");
            m.checkNotNullParameter(map2, "roles");
            this.f2839me = meUser;
            this.guild = guild;
            this.memberVerificationFormData = memberVerificationFormData;
            this.channels = map;
            this.roles = map2;
            this.allowAnimatedEmojis = z2;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, MeUser meUser, Guild guild, StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData, Map map, Map map2, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                meUser = storeState.f2839me;
            }
            if ((i & 2) != 0) {
                guild = storeState.guild;
            }
            Guild guild2 = guild;
            if ((i & 4) != 0) {
                memberVerificationFormData = storeState.memberVerificationFormData;
            }
            StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData2 = memberVerificationFormData;
            Map<Long, String> map3 = map;
            if ((i & 8) != 0) {
                map3 = storeState.channels;
            }
            Map map4 = map3;
            Map<Long, GuildRole> map5 = map2;
            if ((i & 16) != 0) {
                map5 = storeState.roles;
            }
            Map map6 = map5;
            if ((i & 32) != 0) {
                z2 = storeState.allowAnimatedEmojis;
            }
            return storeState.copy(meUser, guild2, memberVerificationFormData2, map4, map6, z2);
        }

        public final MeUser component1() {
            return this.f2839me;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final StoreGuildMemberVerificationForm.MemberVerificationFormData component3() {
            return this.memberVerificationFormData;
        }

        public final Map<Long, String> component4() {
            return this.channels;
        }

        public final Map<Long, GuildRole> component5() {
            return this.roles;
        }

        public final boolean component6() {
            return this.allowAnimatedEmojis;
        }

        public final StoreState copy(MeUser meUser, Guild guild, StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData, Map<Long, String> map, Map<Long, GuildRole> map2, boolean z2) {
            m.checkNotNullParameter(meUser, "me");
            m.checkNotNullParameter(map, "channels");
            m.checkNotNullParameter(map2, "roles");
            return new StoreState(meUser, guild, memberVerificationFormData, map, map2, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.f2839me, storeState.f2839me) && m.areEqual(this.guild, storeState.guild) && m.areEqual(this.memberVerificationFormData, storeState.memberVerificationFormData) && m.areEqual(this.channels, storeState.channels) && m.areEqual(this.roles, storeState.roles) && this.allowAnimatedEmojis == storeState.allowAnimatedEmojis;
        }

        public final boolean getAllowAnimatedEmojis() {
            return this.allowAnimatedEmojis;
        }

        public final Map<Long, String> getChannels() {
            return this.channels;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final MeUser getMe() {
            return this.f2839me;
        }

        public final StoreGuildMemberVerificationForm.MemberVerificationFormData getMemberVerificationFormData() {
            return this.memberVerificationFormData;
        }

        public final Map<Long, GuildRole> getRoles() {
            return this.roles;
        }

        public int hashCode() {
            MeUser meUser = this.f2839me;
            int i = 0;
            int hashCode = (meUser != null ? meUser.hashCode() : 0) * 31;
            Guild guild = this.guild;
            int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
            StoreGuildMemberVerificationForm.MemberVerificationFormData memberVerificationFormData = this.memberVerificationFormData;
            int hashCode3 = (hashCode2 + (memberVerificationFormData != null ? memberVerificationFormData.hashCode() : 0)) * 31;
            Map<Long, String> map = this.channels;
            int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map2 = this.roles;
            if (map2 != null) {
                i = map2.hashCode();
            }
            int i2 = (hashCode4 + i) * 31;
            boolean z2 = this.allowAnimatedEmojis;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(me=");
            R.append(this.f2839me);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", memberVerificationFormData=");
            R.append(this.memberVerificationFormData);
            R.append(", channels=");
            R.append(this.channels);
            R.append(", roles=");
            R.append(this.roles);
            R.append(", allowAnimatedEmojis=");
            return a.M(R, this.allowAnimatedEmojis, ")");
        }
    }

    /* compiled from: WidgetMemberVerificationViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$VerificationType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "PHONE", "EMAIL", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum VerificationType {
        PHONE,
        EMAIL
    }

    /* compiled from: WidgetMemberVerificationViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Loaded", "Loading", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Invalid;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetMemberVerificationViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Invalid;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetMemberVerificationViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0016\b\u0086\b\u0018\u00002\u00020\u0001B\u0097\u0001\u0012\u0006\u0010 \u001a\u00020\u0002\u0012\u0006\u0010!\u001a\u00020\u0002\u0012\u0006\u0010\"\u001a\u00020\u0002\u0012\b\u0010#\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010$\u001a\u00020\u0002\u0012\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\b\u0010&\u001a\u0004\u0018\u00010\u000f\u0012\u0016\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0013j\u0002`\u0014\u0012\u0004\u0012\u00020\u00150\u0012\u0012\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00180\u0012\u0012\u0006\u0010)\u001a\u00020\u0002\u0012\u0006\u0010*\u001a\u00020\u0002\u0012\u0006\u0010+\u001a\u00020\u0002\u0012\b\u0010,\u001a\u0004\u0018\u00010\u001d¢\u0006\u0004\bH\u0010IJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\n\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\n\u0010\u0004J\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J \u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\u0013j\u0002`\u0014\u0012\u0004\u0012\u00020\u00150\u0012HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u001c\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00180\u0012HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0017J\u0010\u0010\u001a\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0004J\u0010\u0010\u001b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0004J\u0010\u0010\u001c\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0004J\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u001dHÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001fJº\u0001\u0010-\u001a\u00020\u00002\b\b\u0002\u0010 \u001a\u00020\u00022\b\b\u0002\u0010!\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020\u00022\n\b\u0002\u0010#\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010$\u001a\u00020\u00022\u000e\b\u0002\u0010%\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u000f2\u0018\b\u0002\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0013j\u0002`\u0014\u0012\u0004\u0012\u00020\u00150\u00122\u0014\b\u0002\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00180\u00122\b\b\u0002\u0010)\u001a\u00020\u00022\b\b\u0002\u0010*\u001a\u00020\u00022\b\b\u0002\u0010+\u001a\u00020\u00022\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\u001dHÆ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u0010/\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b/\u00100J\u0010\u00102\u001a\u000201HÖ\u0001¢\u0006\u0004\b2\u00103J\u001a\u00106\u001a\u00020\u00022\b\u00105\u001a\u0004\u0018\u000104HÖ\u0003¢\u0006\u0004\b6\u00107R\u001f\u0010%\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b%\u00108\u001a\u0004\b9\u0010\u000eR\u001b\u0010,\u001a\u0004\u0018\u00010\u001d8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010:\u001a\u0004\b;\u0010\u001fR\u0019\u0010 \u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b \u0010<\u001a\u0004\b \u0010\u0004R\u001b\u0010&\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010=\u001a\u0004\b>\u0010\u0011R\u0019\u0010!\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010<\u001a\u0004\b?\u0010\u0004R\u0019\u0010+\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010<\u001a\u0004\b+\u0010\u0004R\u001b\u0010#\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010@\u001a\u0004\bA\u0010\tR\u0019\u0010*\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010<\u001a\u0004\b*\u0010\u0004R\u0019\u0010)\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b)\u0010<\u001a\u0004\bB\u0010\u0004R%\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00180\u00128\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010C\u001a\u0004\bD\u0010\u0017R\u0019\u0010$\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010<\u001a\u0004\bE\u0010\u0004R)\u0010'\u001a\u0012\u0012\b\u0012\u00060\u0013j\u0002`\u0014\u0012\u0004\u0012\u00020\u00150\u00128\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010C\u001a\u0004\bF\u0010\u0017R\u0019\u0010\"\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010<\u001a\u0004\bG\u0010\u0004¨\u0006J"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Loaded;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState;", "", "component1", "()Z", "component2", "component3", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$VerificationType;", "component4", "()Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$VerificationType;", "component5", "", "Lcom/discord/widgets/servers/member_verification/form_fields/MemberVerificationItem;", "component6", "()Ljava/util/List;", "Lcom/discord/models/domain/ModelMemberVerificationForm;", "component7", "()Lcom/discord/models/domain/ModelMemberVerificationForm;", "", "", "Lcom/discord/primitives/ChannelId;", "", "component8", "()Ljava/util/Map;", "Lcom/discord/api/role/GuildRole;", "component9", "component10", "component11", "component12", "Lcom/discord/models/guild/Guild;", "component13", "()Lcom/discord/models/guild/Guild;", "isRulesListVisible", "submitting", "disabled", "verificationType", "needsAdditionalVerification", "formItems", "form", "channelNames", "roles", "allowAnimatedEmojis", "isFormValid", "isTermsApproved", "guild", "copy", "(ZZZLcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$VerificationType;ZLjava/util/List;Lcom/discord/models/domain/ModelMemberVerificationForm;Ljava/util/Map;Ljava/util/Map;ZZZLcom/discord/models/guild/Guild;)Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Loaded;", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getFormItems", "Lcom/discord/models/guild/Guild;", "getGuild", "Z", "Lcom/discord/models/domain/ModelMemberVerificationForm;", "getForm", "getSubmitting", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$VerificationType;", "getVerificationType", "getAllowAnimatedEmojis", "Ljava/util/Map;", "getRoles", "getNeedsAdditionalVerification", "getChannelNames", "getDisabled", HookHelper.constructorName, "(ZZZLcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$VerificationType;ZLjava/util/List;Lcom/discord/models/domain/ModelMemberVerificationForm;Ljava/util/Map;Ljava/util/Map;ZZZLcom/discord/models/guild/Guild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean allowAnimatedEmojis;
            private final Map<Long, String> channelNames;
            private final boolean disabled;
            private final ModelMemberVerificationForm form;
            private final List<MemberVerificationItem> formItems;
            private final Guild guild;
            private final boolean isFormValid;
            private final boolean isRulesListVisible;
            private final boolean isTermsApproved;
            private final boolean needsAdditionalVerification;
            private final Map<Long, GuildRole> roles;
            private final boolean submitting;
            private final VerificationType verificationType;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(boolean z2, boolean z3, boolean z4, VerificationType verificationType, boolean z5, List<? extends MemberVerificationItem> list, ModelMemberVerificationForm modelMemberVerificationForm, Map<Long, String> map, Map<Long, GuildRole> map2, boolean z6, boolean z7, boolean z8, Guild guild) {
                super(null);
                m.checkNotNullParameter(list, "formItems");
                m.checkNotNullParameter(map, "channelNames");
                m.checkNotNullParameter(map2, "roles");
                this.isRulesListVisible = z2;
                this.submitting = z3;
                this.disabled = z4;
                this.verificationType = verificationType;
                this.needsAdditionalVerification = z5;
                this.formItems = list;
                this.form = modelMemberVerificationForm;
                this.channelNames = map;
                this.roles = map2;
                this.allowAnimatedEmojis = z6;
                this.isFormValid = z7;
                this.isTermsApproved = z8;
                this.guild = guild;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, boolean z2, boolean z3, boolean z4, VerificationType verificationType, boolean z5, List list, ModelMemberVerificationForm modelMemberVerificationForm, Map map, Map map2, boolean z6, boolean z7, boolean z8, Guild guild, int i, Object obj) {
                return loaded.copy((i & 1) != 0 ? loaded.isRulesListVisible : z2, (i & 2) != 0 ? loaded.submitting : z3, (i & 4) != 0 ? loaded.disabled : z4, (i & 8) != 0 ? loaded.verificationType : verificationType, (i & 16) != 0 ? loaded.needsAdditionalVerification : z5, (i & 32) != 0 ? loaded.formItems : list, (i & 64) != 0 ? loaded.form : modelMemberVerificationForm, (i & 128) != 0 ? loaded.channelNames : map, (i & 256) != 0 ? loaded.roles : map2, (i & 512) != 0 ? loaded.allowAnimatedEmojis : z6, (i & 1024) != 0 ? loaded.isFormValid : z7, (i & 2048) != 0 ? loaded.isTermsApproved : z8, (i & 4096) != 0 ? loaded.guild : guild);
            }

            public final boolean component1() {
                return this.isRulesListVisible;
            }

            public final boolean component10() {
                return this.allowAnimatedEmojis;
            }

            public final boolean component11() {
                return this.isFormValid;
            }

            public final boolean component12() {
                return this.isTermsApproved;
            }

            public final Guild component13() {
                return this.guild;
            }

            public final boolean component2() {
                return this.submitting;
            }

            public final boolean component3() {
                return this.disabled;
            }

            public final VerificationType component4() {
                return this.verificationType;
            }

            public final boolean component5() {
                return this.needsAdditionalVerification;
            }

            public final List<MemberVerificationItem> component6() {
                return this.formItems;
            }

            public final ModelMemberVerificationForm component7() {
                return this.form;
            }

            public final Map<Long, String> component8() {
                return this.channelNames;
            }

            public final Map<Long, GuildRole> component9() {
                return this.roles;
            }

            public final Loaded copy(boolean z2, boolean z3, boolean z4, VerificationType verificationType, boolean z5, List<? extends MemberVerificationItem> list, ModelMemberVerificationForm modelMemberVerificationForm, Map<Long, String> map, Map<Long, GuildRole> map2, boolean z6, boolean z7, boolean z8, Guild guild) {
                m.checkNotNullParameter(list, "formItems");
                m.checkNotNullParameter(map, "channelNames");
                m.checkNotNullParameter(map2, "roles");
                return new Loaded(z2, z3, z4, verificationType, z5, list, modelMemberVerificationForm, map, map2, z6, z7, z8, guild);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return this.isRulesListVisible == loaded.isRulesListVisible && this.submitting == loaded.submitting && this.disabled == loaded.disabled && m.areEqual(this.verificationType, loaded.verificationType) && this.needsAdditionalVerification == loaded.needsAdditionalVerification && m.areEqual(this.formItems, loaded.formItems) && m.areEqual(this.form, loaded.form) && m.areEqual(this.channelNames, loaded.channelNames) && m.areEqual(this.roles, loaded.roles) && this.allowAnimatedEmojis == loaded.allowAnimatedEmojis && this.isFormValid == loaded.isFormValid && this.isTermsApproved == loaded.isTermsApproved && m.areEqual(this.guild, loaded.guild);
            }

            public final boolean getAllowAnimatedEmojis() {
                return this.allowAnimatedEmojis;
            }

            public final Map<Long, String> getChannelNames() {
                return this.channelNames;
            }

            public final boolean getDisabled() {
                return this.disabled;
            }

            public final ModelMemberVerificationForm getForm() {
                return this.form;
            }

            public final List<MemberVerificationItem> getFormItems() {
                return this.formItems;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final boolean getNeedsAdditionalVerification() {
                return this.needsAdditionalVerification;
            }

            public final Map<Long, GuildRole> getRoles() {
                return this.roles;
            }

            public final boolean getSubmitting() {
                return this.submitting;
            }

            public final VerificationType getVerificationType() {
                return this.verificationType;
            }

            public int hashCode() {
                boolean z2 = this.isRulesListVisible;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = i2 * 31;
                boolean z3 = this.submitting;
                if (z3) {
                    z3 = true;
                }
                int i5 = z3 ? 1 : 0;
                int i6 = z3 ? 1 : 0;
                int i7 = (i4 + i5) * 31;
                boolean z4 = this.disabled;
                if (z4) {
                    z4 = true;
                }
                int i8 = z4 ? 1 : 0;
                int i9 = z4 ? 1 : 0;
                int i10 = (i7 + i8) * 31;
                VerificationType verificationType = this.verificationType;
                int i11 = 0;
                int hashCode = (i10 + (verificationType != null ? verificationType.hashCode() : 0)) * 31;
                boolean z5 = this.needsAdditionalVerification;
                if (z5) {
                    z5 = true;
                }
                int i12 = z5 ? 1 : 0;
                int i13 = z5 ? 1 : 0;
                int i14 = (hashCode + i12) * 31;
                List<MemberVerificationItem> list = this.formItems;
                int hashCode2 = (i14 + (list != null ? list.hashCode() : 0)) * 31;
                ModelMemberVerificationForm modelMemberVerificationForm = this.form;
                int hashCode3 = (hashCode2 + (modelMemberVerificationForm != null ? modelMemberVerificationForm.hashCode() : 0)) * 31;
                Map<Long, String> map = this.channelNames;
                int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
                Map<Long, GuildRole> map2 = this.roles;
                int hashCode5 = (hashCode4 + (map2 != null ? map2.hashCode() : 0)) * 31;
                boolean z6 = this.allowAnimatedEmojis;
                if (z6) {
                    z6 = true;
                }
                int i15 = z6 ? 1 : 0;
                int i16 = z6 ? 1 : 0;
                int i17 = (hashCode5 + i15) * 31;
                boolean z7 = this.isFormValid;
                if (z7) {
                    z7 = true;
                }
                int i18 = z7 ? 1 : 0;
                int i19 = z7 ? 1 : 0;
                int i20 = (i17 + i18) * 31;
                boolean z8 = this.isTermsApproved;
                if (!z8) {
                    i = z8 ? 1 : 0;
                }
                int i21 = (i20 + i) * 31;
                Guild guild = this.guild;
                if (guild != null) {
                    i11 = guild.hashCode();
                }
                return i21 + i11;
            }

            public final boolean isFormValid() {
                return this.isFormValid;
            }

            public final boolean isRulesListVisible() {
                return this.isRulesListVisible;
            }

            public final boolean isTermsApproved() {
                return this.isTermsApproved;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(isRulesListVisible=");
                R.append(this.isRulesListVisible);
                R.append(", submitting=");
                R.append(this.submitting);
                R.append(", disabled=");
                R.append(this.disabled);
                R.append(", verificationType=");
                R.append(this.verificationType);
                R.append(", needsAdditionalVerification=");
                R.append(this.needsAdditionalVerification);
                R.append(", formItems=");
                R.append(this.formItems);
                R.append(", form=");
                R.append(this.form);
                R.append(", channelNames=");
                R.append(this.channelNames);
                R.append(", roles=");
                R.append(this.roles);
                R.append(", allowAnimatedEmojis=");
                R.append(this.allowAnimatedEmojis);
                R.append(", isFormValid=");
                R.append(this.isFormValid);
                R.append(", isTermsApproved=");
                R.append(this.isTermsApproved);
                R.append(", guild=");
                R.append(this.guild);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetMemberVerificationViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            StoreGuildMemberVerificationForm.FetchStates.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreGuildMemberVerificationForm.FetchStates.FETCHING.ordinal()] = 1;
            iArr[StoreGuildMemberVerificationForm.FetchStates.SUCCEEDED.ordinal()] = 2;
            iArr[StoreGuildMemberVerificationForm.FetchStates.FAILED.ordinal()] = 3;
            ModelMemberVerificationForm.MemberVerificationFieldType.values();
            int[] iArr2 = new int[5];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[ModelMemberVerificationForm.MemberVerificationFieldType.TERMS.ordinal()] = 1;
            iArr2[ModelMemberVerificationForm.MemberVerificationFieldType.TEXT_INPUT.ordinal()] = 2;
            iArr2[ModelMemberVerificationForm.MemberVerificationFieldType.PARAGRAPH.ordinal()] = 3;
            iArr2[ModelMemberVerificationForm.MemberVerificationFieldType.MULTIPLE_CHOICE.ordinal()] = 4;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetMemberVerificationViewModel(long r12, java.lang.String r14, com.discord.models.guild.Guild r15, com.discord.stores.StoreGuildMemberVerificationForm r16, com.discord.utilities.rest.RestAPI r17, com.discord.simpleast.core.parser.Parser r18, rx.Observable r19, int r20, kotlin.jvm.internal.DefaultConstructorMarker r21) {
        /*
            r11 = this;
            r0 = r20 & 4
            r1 = 0
            if (r0 == 0) goto L7
            r6 = r1
            goto L8
        L7:
            r6 = r15
        L8:
            r0 = r20 & 8
            if (r0 == 0) goto L14
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildMemberVerificationForm r0 = r0.getMemberVerificationForms()
            r7 = r0
            goto L16
        L14:
            r7 = r16
        L16:
            r0 = r20 & 16
            if (r0 == 0) goto L22
            com.discord.utilities.rest.RestAPI$Companion r0 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r0 = r0.getApi()
            r8 = r0
            goto L24
        L22:
            r8 = r17
        L24:
            r0 = r20 & 32
            if (r0 == 0) goto L31
            r0 = 1
            r2 = 0
            r3 = 4
            com.discord.simpleast.core.parser.Parser r0 = com.discord.utilities.textprocessing.DiscordParser.createParser$default(r2, r0, r2, r3, r1)
            r9 = r0
            goto L33
        L31:
            r9 = r18
        L33:
            r0 = r20 & 64
            if (r0 == 0) goto L40
            com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel$Companion r0 = com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel.Companion
            r3 = r12
            rx.Observable r0 = r0.observeStores(r12, r6)
            r10 = r0
            goto L43
        L40:
            r3 = r12
            r10 = r19
        L43:
            r2 = r11
            r3 = r12
            r5 = r14
            r2.<init>(r3, r5, r6, r7, r8, r9, r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel.<init>(long, java.lang.String, com.discord.models.guild.Guild, com.discord.stores.StoreGuildMemberVerificationForm, com.discord.utilities.rest.RestAPI, com.discord.simpleast.core.parser.Parser, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final List<MemberVerificationItem> createFormItems(List<ModelMemberVerificationForm.FormField> list, boolean z2, Map<Long, String> map, Map<Long, GuildRole> map2, boolean z3) {
        if (list == null || list.isEmpty()) {
            return n.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (ModelMemberVerificationForm.FormField formField : list) {
            Object obj = this.formUserInputDataMap.get(Integer.valueOf(list.indexOf(formField)));
            int ordinal = formField.getMemberVerificationFieldType().ordinal();
            if (ordinal == 1) {
                List<String> values = formField.getValues();
                if (!values.isEmpty()) {
                    arrayList.add(new MemberVerificationItemTermsHeader());
                    int size = values.size();
                    int i = 0;
                    while (i < size) {
                        int i2 = i + 1;
                        arrayList.add(new MemberVerificationItemTerm(i2, values.get(i), generateAST(values.get(i)), map, map2, z3, i == 0, i == values.size() - 1));
                        i = i2;
                    }
                    arrayList.add(new MemberVerificationItemApproveTerms(list.indexOf(formField), z2));
                }
            } else if (ordinal == 2) {
                arrayList.add(new MemberVerificationItemHeader(formField.getLabel()));
                arrayList.add(new MemberVerificationItemTextInput(list.indexOf(formField), (String) obj));
            } else if (ordinal == 3) {
                arrayList.add(new MemberVerificationItemHeader(formField.getLabel()));
                arrayList.add(new MemberVerificationItemParagraph(list.indexOf(formField), (String) obj));
            } else if (ordinal == 4) {
                arrayList.add(new MemberVerificationItemHeader(formField.getLabel()));
                arrayList.add(new MemberVerificationItemMultipleChoice(list.indexOf(formField), formField.getChoices(), (Integer) obj));
            }
        }
        return arrayList;
    }

    private final List<Node<MessageRenderContext>> generateAST(CharSequence charSequence) {
        return Parser.parse$default(this.rulesParser, charSequence, MessageParseState.Companion.getInitialState(), null, 4, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleGuildUpdateError() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, false, null, false, null, null, null, null, false, false, false, null, 8189, null));
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(Event.Error.INSTANCE);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x007b, code lost:
        if (com.discord.utilities.user.UserUtils.INSTANCE.getHasPhone(r0) == false) goto L44;
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x008a, code lost:
        if (com.discord.utilities.user.UserUtils.INSTANCE.getHasPhone(r0) == false) goto L44;
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x008c, code lost:
        r12 = r2;
        r13 = true;
     */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0093  */
    /* JADX WARN: Removed duplicated region for block: B:60:0x00c3  */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStoreState(com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel.StoreState r22) {
        /*
            Method dump skipped, instructions count: 273
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel.handleStoreState(com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel$StoreState):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x0034, code lost:
        if ((((java.lang.CharSequence) r1).length() == 0) != false) goto L17;
     */
    /* JADX WARN: Removed duplicated region for block: B:23:0x003c A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final boolean isFormValid() {
        /*
            r5 = this;
            java.util.Map<java.lang.Integer, java.lang.Object> r0 = r5.formUserInputDataMap
            boolean r1 = r0.isEmpty()
            r2 = 1
            r3 = 0
            if (r1 == 0) goto Lb
            goto L3d
        Lb:
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L13:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L3d
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r1 = r1.getValue()
            if (r1 == 0) goto L39
            boolean r4 = r1 instanceof java.lang.String
            if (r4 == 0) goto L37
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1
            int r1 = r1.length()
            if (r1 != 0) goto L33
            r1 = 1
            goto L34
        L33:
            r1 = 0
        L34:
            if (r1 == 0) goto L37
            goto L39
        L37:
            r1 = 0
            goto L3a
        L39:
            r1 = 1
        L3a:
            if (r1 == 0) goto L13
            r3 = 1
        L3d:
            r0 = r3 ^ 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel.isFormValid():boolean");
    }

    public final void applyToJoinGuild() {
        List<ModelMemberVerificationForm.FormField> list;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, false, true, false, null, false, null, null, null, null, false, false, false, null, 8189, null));
            ModelMemberVerificationForm form = loaded.getForm();
            if (form == null || (list = form.getFormFields()) == null) {
                list = n.emptyList();
            }
            for (Map.Entry<Integer, Object> entry : this.formUserInputDataMap.entrySet()) {
                int intValue = entry.getKey().intValue();
                Object value = entry.getValue();
                if (value != null) {
                    list.get(intValue).setResponse(value);
                }
            }
            int i = 0;
            for (Object obj : list) {
                i++;
                if (i < 0) {
                    n.throwIndexOverflow();
                }
                if (((ModelMemberVerificationForm.FormField) obj).getMemberVerificationFieldType() == ModelMemberVerificationForm.MemberVerificationFieldType.TERMS) {
                    list.get(i).setResponse(Boolean.valueOf(loaded.isTermsApproved()));
                }
            }
            RestAPI restAPI = this.restAPI;
            long j = this.guildId;
            ModelMemberVerificationForm form2 = loaded.getForm();
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(restAPI.createGuildJoinRequest(j, new RestAPIParams.MemberVerificationForm(list, form2 != null ? form2.getVersion() : null)), false, 1, null), this, null, 2, null), WidgetMemberVerificationViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetMemberVerificationViewModel$applyToJoinGuild$3(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetMemberVerificationViewModel$applyToJoinGuild$4(this, loaded));
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @Override // com.discord.app.AppViewModel, androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        AnalyticsTracker.INSTANCE.dismissModal("Membership Gating", this.location, "dismissed", Long.valueOf(this.guildId));
    }

    public final void updateFormValidation(int i, Object obj) {
        boolean isFormValid;
        this.formUserInputDataMap.put(Integer.valueOf(i), obj);
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null && loaded.isFormValid() != (isFormValid = isFormValid())) {
            boolean isTermsApproved = loaded.isTermsApproved();
            ModelMemberVerificationForm form = loaded.getForm();
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, !isFormValid || !isTermsApproved || loaded.getNeedsAdditionalVerification(), null, false, createFormItems(form != null ? form.getFormFields() : null, isTermsApproved, loaded.getChannelNames(), loaded.getRoles(), loaded.getAllowAnimatedEmojis()), null, null, null, false, isFormValid, false, null, 7131, null));
        }
    }

    public final void updateTermsApproval(boolean z2) {
        ViewState viewState = getViewState();
        List<ModelMemberVerificationForm.FormField> list = null;
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            boolean isFormValid = isFormValid();
            ModelMemberVerificationForm form = loaded.getForm();
            if (form != null) {
                list = form.getFormFields();
            }
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, !isFormValid || !z2 || loaded.getNeedsAdditionalVerification(), null, false, createFormItems(list, z2, loaded.getChannelNames(), loaded.getRoles(), loaded.getAllowAnimatedEmojis()), null, null, null, false, isFormValid, z2, null, 5083, null));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetMemberVerificationViewModel(long j, String str, Guild guild, StoreGuildMemberVerificationForm storeGuildMemberVerificationForm, RestAPI restAPI, Parser<MessageRenderContext, Node<MessageRenderContext>, MessageParseState> parser, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        m.checkNotNullParameter(storeGuildMemberVerificationForm, "storeGuildMemberVerificationForm");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(parser, "rulesParser");
        m.checkNotNullParameter(observable, "storeObservable");
        this.guildId = j;
        this.location = str;
        this.inviteGuild = guild;
        this.restAPI = restAPI;
        this.rulesParser = parser;
        this.eventSubject = PublishSubject.k0();
        this.formUserInputDataMap = new HashMap();
        AnalyticsTracker.openModal("Membership Gating", str, Long.valueOf(j));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetMemberVerificationViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        storeGuildMemberVerificationForm.fetchMemberVerificationForm(j);
    }
}
