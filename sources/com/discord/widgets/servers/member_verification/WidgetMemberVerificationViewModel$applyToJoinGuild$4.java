package com.discord.widgets.servers.member_verification;

import com.discord.models.domain.ModelMemberVerificationFormResponse;
import com.discord.models.guild.Guild;
import com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
/* compiled from: WidgetMemberVerificationViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelMemberVerificationFormResponse;", "it", "", "invoke", "(Lcom/discord/models/domain/ModelMemberVerificationFormResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMemberVerificationViewModel$applyToJoinGuild$4 extends o implements Function1<ModelMemberVerificationFormResponse, Unit> {
    public final /* synthetic */ WidgetMemberVerificationViewModel.ViewState.Loaded $currentViewState;
    public final /* synthetic */ WidgetMemberVerificationViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetMemberVerificationViewModel$applyToJoinGuild$4(WidgetMemberVerificationViewModel widgetMemberVerificationViewModel, WidgetMemberVerificationViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = widgetMemberVerificationViewModel;
        this.$currentViewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ModelMemberVerificationFormResponse modelMemberVerificationFormResponse) {
        invoke2(modelMemberVerificationFormResponse);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(ModelMemberVerificationFormResponse modelMemberVerificationFormResponse) {
        PublishSubject publishSubject;
        m.checkNotNullParameter(modelMemberVerificationFormResponse, "it");
        this.this$0.updateViewState(WidgetMemberVerificationViewModel.ViewState.Loaded.copy$default(this.$currentViewState, false, false, false, null, false, null, null, null, null, false, false, false, null, 8189, null));
        publishSubject = this.this$0.eventSubject;
        Guild guild = this.$currentViewState.getGuild();
        String str = null;
        Long valueOf = guild != null ? Long.valueOf(guild.getId()) : null;
        Guild guild2 = this.$currentViewState.getGuild();
        if (guild2 != null) {
            str = guild2.getName();
        }
        publishSubject.k.onNext(new WidgetMemberVerificationViewModel.Event.Success(valueOf, str, modelMemberVerificationFormResponse.getApplicationStatus(), modelMemberVerificationFormResponse.getLastSeen()));
    }
}
