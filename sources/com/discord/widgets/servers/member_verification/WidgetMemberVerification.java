package com.discord.widgets.servers.member_verification;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.api.guildjoinrequest.ApplicationStatus;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetMemberVerificationBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.discord.views.MemberVerificationAvatarView;
import com.discord.widgets.servers.member_verification.WidgetMemberVerificationViewModel;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetMemberVerification.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 $2\u00020\u0001:\u0001$B\u0007¢\u0006\u0004\b#\u0010\u0013J\u0011\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\r\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\fJ\u0017\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010\"\u001a\u00020\u001d8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!¨\u0006%"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerification;", "Lcom/discord/app/AppFragment;", "Lcom/discord/models/guild/Guild;", "parseInviteGuild", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState;)V", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Loaded;", "configureLoadedUI", "(Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel$ViewState$Loaded;)V", "configureVerificationBanner", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/servers/member_verification/MemberVerificationRulesAdapter;", "rulesAdapter", "Lcom/discord/widgets/servers/member_verification/MemberVerificationRulesAdapter;", "Lcom/discord/databinding/WidgetMemberVerificationBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetMemberVerificationBinding;", "binding", "Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/servers/member_verification/WidgetMemberVerificationViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetMemberVerification extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetMemberVerification.class, "binding", "getBinding()Lcom/discord/databinding/WidgetMemberVerificationBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String INTENT_EXTRA_INVITE_GUILD = "INTENT_EXTRA_INVITE_GUILD";
    private static final String INTENT_EXTRA_LOCATION = "INTENT_EXTRA_LOCATION";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetMemberVerification$binding$2.INSTANCE, null, 2, null);
    private MemberVerificationRulesAdapter rulesAdapter;
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetMemberVerification.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J7\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\tH\u0007¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000f¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/servers/member_verification/WidgetMemberVerification$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "Lcom/discord/models/domain/ModelInvite;", "invite", "", "create", "(Landroid/content/Context;JLjava/lang/String;Lcom/discord/models/domain/ModelInvite;)V", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", WidgetMemberVerification.INTENT_EXTRA_INVITE_GUILD, WidgetMemberVerification.INTENT_EXTRA_LOCATION, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void create$default(Companion companion, Context context, long j, String str, ModelInvite modelInvite, int i, Object obj) {
            if ((i & 8) != 0) {
                modelInvite = null;
            }
            companion.create(context, j, str, modelInvite);
        }

        public final void create(Context context, long j, String str, ModelInvite modelInvite) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
            Intent intent = new Intent();
            intent.putExtra("INTENT_EXTRA_GUILD_ID", j);
            intent.putExtra(WidgetMemberVerification.INTENT_EXTRA_LOCATION, str);
            if ((modelInvite != null ? modelInvite.guild : null) != null) {
                intent.putExtra(WidgetMemberVerification.INTENT_EXTRA_INVITE_GUILD, new Gson().m(modelInvite.guild));
            }
            j.d(context, WidgetMemberVerification.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            ApplicationStatus.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            iArr[ApplicationStatus.PENDING.ordinal()] = 1;
            iArr[ApplicationStatus.APPROVED.ordinal()] = 2;
            WidgetMemberVerificationViewModel.VerificationType.values();
            int[] iArr2 = new int[2];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[WidgetMemberVerificationViewModel.VerificationType.EMAIL.ordinal()] = 1;
            iArr2[WidgetMemberVerificationViewModel.VerificationType.PHONE.ordinal()] = 2;
        }
    }

    public WidgetMemberVerification() {
        super(R.layout.widget_member_verification);
        WidgetMemberVerification$viewModel$2 widgetMemberVerification$viewModel$2 = new WidgetMemberVerification$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetMemberVerificationViewModel.class), new WidgetMemberVerification$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetMemberVerification$viewModel$2));
    }

    private final void configureLoadedUI(WidgetMemberVerificationViewModel.ViewState.Loaded loaded) {
        NestedScrollView nestedScrollView = getBinding().g;
        m.checkNotNullExpressionValue(nestedScrollView, "binding.memberVerificationScrollview");
        int i = 0;
        nestedScrollView.setVisibility(0);
        getBinding().e.setIsLoading(loaded.getSubmitting());
        String str = null;
        DimmerView.setDimmed$default(getBinding().f2465b, false, false, 2, null);
        LoadingButton loadingButton = getBinding().e;
        m.checkNotNullExpressionValue(loadingButton, "binding.memberVerificationRulesConfirm");
        boolean z2 = true;
        loadingButton.setEnabled(!loaded.getDisabled());
        RecyclerView recyclerView = getBinding().f;
        m.checkNotNullExpressionValue(recyclerView, "binding.memberVerificationRulesRecycler");
        recyclerView.setVisibility(loaded.isRulesListVisible() ? 0 : 8);
        MemberVerificationAvatarView memberVerificationAvatarView = getBinding().d;
        Guild guild = loaded.getGuild();
        Objects.requireNonNull(memberVerificationAvatarView);
        if (guild == null || !guild.hasIcon()) {
            z2 = false;
        }
        TextView textView = memberVerificationAvatarView.j.c;
        m.checkNotNullExpressionValue(textView, "binding.memberVerificationText");
        if (!(!z2)) {
            i = 8;
        }
        textView.setVisibility(i);
        if (z2) {
            SimpleDraweeView simpleDraweeView = memberVerificationAvatarView.j.f198b;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.memberVerificationAvatar");
            IconUtils.setIcon$default(simpleDraweeView, IconUtils.getForGuild$default(guild, null, false, null, 14, null), 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
            TextView textView2 = memberVerificationAvatarView.j.c;
            m.checkNotNullExpressionValue(textView2, "binding.memberVerificationText");
            textView2.setText("");
        } else {
            TextView textView3 = memberVerificationAvatarView.j.c;
            m.checkNotNullExpressionValue(textView3, "binding.memberVerificationText");
            if (guild != null) {
                str = guild.getShortName();
            }
            textView3.setText(str);
        }
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.WidgetMemberVerification$configureLoadedUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetMemberVerificationViewModel viewModel;
                viewModel = WidgetMemberVerification.this.getViewModel();
                viewModel.applyToJoinGuild();
            }
        });
        MemberVerificationRulesAdapter memberVerificationRulesAdapter = this.rulesAdapter;
        if (memberVerificationRulesAdapter == null) {
            m.throwUninitializedPropertyAccessException("rulesAdapter");
        }
        memberVerificationRulesAdapter.setData(loaded.getFormItems());
        configureVerificationBanner(loaded);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(WidgetMemberVerificationViewModel.ViewState viewState) {
        if (viewState instanceof WidgetMemberVerificationViewModel.ViewState.Invalid) {
            b.a.d.m.i(this, R.string.default_failure_to_perform_action_message, 0, 4);
            requireActivity().finish();
        } else if (viewState instanceof WidgetMemberVerificationViewModel.ViewState.Loaded) {
            configureLoadedUI((WidgetMemberVerificationViewModel.ViewState.Loaded) viewState);
        } else if (viewState instanceof WidgetMemberVerificationViewModel.ViewState.Loading) {
            NestedScrollView nestedScrollView = getBinding().g;
            m.checkNotNullExpressionValue(nestedScrollView, "binding.memberVerificationScrollview");
            nestedScrollView.setVisibility(8);
            DimmerView.setDimmed$default(getBinding().f2465b, true, false, 2, null);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final void configureVerificationBanner(WidgetMemberVerificationViewModel.ViewState.Loaded loaded) {
        MemberVerificationView memberVerificationView = getBinding().c;
        m.checkNotNullExpressionValue(memberVerificationView, "binding.memberVerificationContainer");
        int i = 0;
        if (!(loaded.getVerificationType() != null)) {
            i = 8;
        }
        memberVerificationView.setVisibility(i);
        WidgetMemberVerificationViewModel.VerificationType verificationType = loaded.getVerificationType();
        if (verificationType != null) {
            int ordinal = verificationType.ordinal();
            if (ordinal == 0) {
                getBinding().c.configure(R.drawable.ic_phone_verification_24dp, R.string.member_verification_form_item_phone_verification_label, !loaded.getNeedsAdditionalVerification(), new WidgetMemberVerification$configureVerificationBanner$2(this));
            } else if (ordinal == 1) {
                getBinding().c.configure(R.drawable.ic_email_verification_24dp, R.string.member_verification_form_item_email_verification_label, !loaded.getNeedsAdditionalVerification(), new WidgetMemberVerification$configureVerificationBanner$1(this));
            }
        }
    }

    public static final void create(Context context, long j, String str, ModelInvite modelInvite) {
        Companion.create(context, j, str, modelInvite);
    }

    private final WidgetMemberVerificationBinding getBinding() {
        return (WidgetMemberVerificationBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetMemberVerificationViewModel getViewModel() {
        return (WidgetMemberVerificationViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Guild parseInviteGuild() {
        String stringExtra = getMostRecentIntent().getStringExtra(INTENT_EXTRA_INVITE_GUILD);
        if (stringExtra == null) {
            return (Guild) stringExtra;
        }
        return (Guild) f.E1(Guild.class).cast(new Gson().g(stringExtra, Guild.class));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        RecyclerView recyclerView = getBinding().f;
        m.checkNotNullExpressionValue(recyclerView, "binding.memberVerificationRulesRecycler");
        this.rulesAdapter = new MemberVerificationRulesAdapter(recyclerView, this);
        getBinding().f.setHasFixedSize(false);
        RecyclerView recyclerView2 = getBinding().f;
        m.checkNotNullExpressionValue(recyclerView2, "binding.memberVerificationRulesRecycler");
        recyclerView2.setNestedScrollingEnabled(false);
        MemberVerificationRulesAdapter memberVerificationRulesAdapter = this.rulesAdapter;
        if (memberVerificationRulesAdapter == null) {
            m.throwUninitializedPropertyAccessException("rulesAdapter");
        }
        memberVerificationRulesAdapter.setOnUpdateRulesApproval(new WidgetMemberVerification$onViewBound$1(this));
        MemberVerificationRulesAdapter memberVerificationRulesAdapter2 = this.rulesAdapter;
        if (memberVerificationRulesAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("rulesAdapter");
        }
        memberVerificationRulesAdapter2.setOnUserInputDataEntered(new WidgetMemberVerification$onViewBound$2(this));
        RecyclerView recyclerView3 = getBinding().f;
        m.checkNotNullExpressionValue(recyclerView3, "binding.memberVerificationRulesRecycler");
        MemberVerificationRulesAdapter memberVerificationRulesAdapter3 = this.rulesAdapter;
        if (memberVerificationRulesAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("rulesAdapter");
        }
        recyclerView3.setAdapter(memberVerificationRulesAdapter3);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetMemberVerification.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetMemberVerification$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetMemberVerification.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetMemberVerification$onViewBoundOrOnResume$2(this));
    }
}
