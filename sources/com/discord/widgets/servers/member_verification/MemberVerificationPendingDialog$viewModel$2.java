package com.discord.widgets.servers.member_verification;

import android.os.Bundle;
import com.discord.app.AppViewModel;
import com.discord.widgets.servers.WidgetServerSettingsChannels;
import com.discord.widgets.servers.member_verification.MemberVerificationPendingViewModel;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: MemberVerificationPendingDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MemberVerificationPendingDialog$viewModel$2 extends o implements Function0<AppViewModel<MemberVerificationPendingViewModel.ViewState>> {
    public final /* synthetic */ MemberVerificationPendingDialog this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MemberVerificationPendingDialog$viewModel$2(MemberVerificationPendingDialog memberVerificationPendingDialog) {
        super(0);
        this.this$0 = memberVerificationPendingDialog;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<MemberVerificationPendingViewModel.ViewState> invoke() {
        Bundle arguments = this.this$0.getArguments();
        Serializable serializable = null;
        Long valueOf = arguments != null ? Long.valueOf(arguments.getLong(WidgetServerSettingsChannels.INTENT_EXTRA_GUILD_ID)) : null;
        Objects.requireNonNull(valueOf, "null cannot be cast to non-null type com.discord.primitives.GuildId /* = kotlin.Long */");
        long longValue = valueOf.longValue();
        Bundle arguments2 = this.this$0.getArguments();
        if (arguments2 != null) {
            serializable = arguments2.getSerializable("INTENT_EXTRA_DIALOG_STATE");
        }
        return new MemberVerificationPendingViewModel(longValue, (MemberVerificationPendingViewModel.DialogState) serializable, null, null, 12, null);
    }
}
