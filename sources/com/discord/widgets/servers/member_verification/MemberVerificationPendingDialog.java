package com.discord.widgets.servers.member_verification;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetMemberVerificationPendingDialogBinding;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.home.WidgetHome;
import com.discord.widgets.servers.member_verification.MemberVerificationPendingViewModel;
import com.google.android.material.button.MaterialButton;
import d0.t.m;
import d0.z.d.a0;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: MemberVerificationPendingDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0007¢\u0006\u0004\b\u0018\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\n\u0010\u000bR\u001d\u0010\u0011\u001a\u00020\f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0017\u001a\u00020\u00128B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingDialog;", "Lcom/discord/app/AppDialog;", "Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel$ViewState;)V", "Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel$ViewState$Loaded;", "configureLoadedUI", "(Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel$ViewState$Loaded;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel;", "viewModel", "Lcom/discord/databinding/WidgetMemberVerificationPendingDialogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetMemberVerificationPendingDialogBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MemberVerificationPendingDialog extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(MemberVerificationPendingDialog.class, "binding", "getBinding()Lcom/discord/databinding/WidgetMemberVerificationPendingDialogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_DIALOG_STATE = "INTENT_EXTRA_DIALOG_STATE";
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, MemberVerificationPendingDialog$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    /* compiled from: MemberVerificationPendingDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J/\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bJ%\u0010\f\u001a\u00020\t2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingDialog$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "supportFragmentManager", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel$DialogState;", "dialogState", "", "show", "(Landroidx/fragment/app/FragmentManager;JLcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel$DialogState;)V", "enqueue", "(JLcom/discord/widgets/servers/member_verification/MemberVerificationPendingViewModel$DialogState;)V", "", MemberVerificationPendingDialog.INTENT_EXTRA_DIALOG_STATE, "Ljava/lang/String;", "INTENT_EXTRA_GUILD_ID", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void enqueue$default(Companion companion, long j, MemberVerificationPendingViewModel.DialogState dialogState, int i, Object obj) {
            if ((i & 2) != 0) {
                dialogState = null;
            }
            companion.enqueue(j, dialogState);
        }

        public static /* synthetic */ void show$default(Companion companion, FragmentManager fragmentManager, long j, MemberVerificationPendingViewModel.DialogState dialogState, int i, Object obj) {
            if ((i & 4) != 0) {
                dialogState = null;
            }
            companion.show(fragmentManager, j, dialogState);
        }

        public final void enqueue(long j, MemberVerificationPendingViewModel.DialogState dialogState) {
            StoreNotices notices = StoreStream.Companion.getNotices();
            String s2 = a.s("MEMBERVERIFICATION-", j);
            notices.requestToShow(new StoreNotices.Notice(s2, null, 0L, 0, false, m.listOf(a0.getOrCreateKotlinClass(WidgetHome.class)), 0L, false, 0L, new MemberVerificationPendingDialog$Companion$enqueue$memberVerificationPendingDialogNotice$1(j, dialogState, notices, s2), 150, null));
        }

        public final void show(FragmentManager fragmentManager, long j, MemberVerificationPendingViewModel.DialogState dialogState) {
            d0.z.d.m.checkNotNullParameter(fragmentManager, "supportFragmentManager");
            MemberVerificationPendingDialog memberVerificationPendingDialog = new MemberVerificationPendingDialog();
            Bundle bundle = new Bundle();
            bundle.putLong("INTENT_EXTRA_GUILD_ID", j);
            bundle.putSerializable(MemberVerificationPendingDialog.INTENT_EXTRA_DIALOG_STATE, dialogState);
            memberVerificationPendingDialog.setArguments(bundle);
            memberVerificationPendingDialog.show(fragmentManager, MemberVerificationPendingDialog.class.getSimpleName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            MemberVerificationPendingViewModel.DialogState.values();
            int[] iArr = new int[5];
            $EnumSwitchMapping$0 = iArr;
            MemberVerificationPendingViewModel.DialogState dialogState = MemberVerificationPendingViewModel.DialogState.CANCEL;
            iArr[dialogState.ordinal()] = 1;
            MemberVerificationPendingViewModel.DialogState dialogState2 = MemberVerificationPendingViewModel.DialogState.LEAVE;
            iArr[dialogState2.ordinal()] = 2;
            MemberVerificationPendingViewModel.DialogState.values();
            int[] iArr2 = new int[5];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[MemberVerificationPendingViewModel.DialogState.PENDING.ordinal()] = 1;
            iArr2[dialogState.ordinal()] = 2;
            iArr2[MemberVerificationPendingViewModel.DialogState.REJECTED.ordinal()] = 3;
            iArr2[dialogState2.ordinal()] = 4;
            iArr2[MemberVerificationPendingViewModel.DialogState.UPGRADE.ordinal()] = 5;
        }
    }

    public MemberVerificationPendingDialog() {
        super(R.layout.widget_member_verification_pending_dialog);
        MemberVerificationPendingDialog$viewModel$2 memberVerificationPendingDialog$viewModel$2 = new MemberVerificationPendingDialog$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(MemberVerificationPendingViewModel.class), new MemberVerificationPendingDialog$appViewModels$$inlined$viewModels$1(f0Var), new h0(memberVerificationPendingDialog$viewModel$2));
    }

    private final void configureLoadedUI(final MemberVerificationPendingViewModel.ViewState.Loaded loaded) {
        int i;
        ImageView imageView = getBinding().f2469b;
        d0.z.d.m.checkNotNullExpressionValue(imageView, "binding.memberVerificationPendingImg");
        boolean z2 = false;
        imageView.setVisibility(loaded.getShowPendingImage() ? 0 : 8);
        TextView textView = getBinding().e;
        d0.z.d.m.checkNotNullExpressionValue(textView, "binding.memberVerificationTertiaryBtn");
        textView.setVisibility(loaded.getShowTertiaryButton() ? 0 : 8);
        int ordinal = loaded.getDialogState().ordinal();
        if (ordinal == 2 || ordinal == 4) {
            i = ColorCompat.getColor(getContext(), (int) R.color.status_red_500);
        } else {
            i = ColorCompat.getThemedColor(getContext(), (int) R.attr.color_brand_500);
        }
        MaterialButton materialButton = getBinding().c;
        d0.z.d.m.checkNotNullExpressionValue(materialButton, "binding.memberVerificationPrimaryBtn");
        materialButton.setBackgroundTintList(ColorStateList.valueOf(i));
        int ordinal2 = loaded.getDialogState().ordinal();
        if (ordinal2 == 0) {
            getBinding().f2469b.setImageResource(R.drawable.ic_community_update);
            TextView textView2 = getBinding().g;
            d0.z.d.m.checkNotNullExpressionValue(textView2, "binding.pendingDialogTitle");
            textView2.setText(getString(R.string.member_verification_warning_update_modal_title));
            TextView textView3 = getBinding().f;
            d0.z.d.m.checkNotNullExpressionValue(textView3, "binding.pendingDialogDesc");
            textView3.setText(getString(R.string.member_verification_warning_update_modal_desc));
            MaterialButton materialButton2 = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(materialButton2, "binding.memberVerificationPrimaryBtn");
            materialButton2.setText(getString(R.string.member_verification_warning_update));
            MaterialButton materialButton3 = getBinding().d;
            d0.z.d.m.checkNotNullExpressionValue(materialButton3, "binding.memberVerificationSecondaryBtn");
            materialButton3.setText(getString(R.string.cancel));
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$11
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Context requireContext = MemberVerificationPendingDialog.this.requireContext();
                    d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
                    UriHandler.directToPlayStore$default(requireContext, null, null, 6, null);
                }
            });
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$12
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingDialog.this.dismiss();
                }
            });
        } else if (ordinal2 == 1) {
            getBinding().f2469b.setImageResource(R.drawable.ic_member_verification_pending);
            TextView textView4 = getBinding().g;
            d0.z.d.m.checkNotNullExpressionValue(textView4, "binding.pendingDialogTitle");
            textView4.setText(getString(R.string.member_verification_pending_application_modal_title));
            TextView textView5 = getBinding().f;
            d0.z.d.m.checkNotNullExpressionValue(textView5, "binding.pendingDialogDesc");
            textView5.setText(getString(R.string.member_verification_pending_application_modal_desc));
            MaterialButton materialButton4 = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(materialButton4, "binding.memberVerificationPrimaryBtn");
            materialButton4.setText(getString(R.string.got_it));
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingDialog.this.dismiss();
                }
            });
            if (loaded.isPreviewEnabled()) {
                MaterialButton materialButton5 = getBinding().d;
                d0.z.d.m.checkNotNullExpressionValue(materialButton5, "binding.memberVerificationSecondaryBtn");
                materialButton5.setVisibility(8);
                TextView textView6 = getBinding().e;
                d0.z.d.m.checkNotNullExpressionValue(textView6, "binding.memberVerificationTertiaryBtn");
                textView6.setVisibility(8);
                return;
            }
            MaterialButton materialButton6 = getBinding().d;
            d0.z.d.m.checkNotNullExpressionValue(materialButton6, "binding.memberVerificationSecondaryBtn");
            materialButton6.setVisibility(0);
            MaterialButton materialButton7 = getBinding().d;
            d0.z.d.m.checkNotNullExpressionValue(materialButton7, "binding.memberVerificationSecondaryBtn");
            materialButton7.setText(getString(R.string.member_verification_cancel_application));
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingViewModel viewModel;
                    viewModel = MemberVerificationPendingDialog.this.getViewModel();
                    viewModel.updateDialogState(MemberVerificationPendingViewModel.DialogState.CANCEL);
                }
            });
            TextView textView7 = getBinding().e;
            d0.z.d.m.checkNotNullExpressionValue(textView7, "binding.memberVerificationTertiaryBtn");
            textView7.setVisibility(0);
            TextView textView8 = getBinding().e;
            d0.z.d.m.checkNotNullExpressionValue(textView8, "binding.memberVerificationTertiaryBtn");
            textView8.setText(getString(R.string.leave_server));
            getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingViewModel viewModel;
                    viewModel = MemberVerificationPendingDialog.this.getViewModel();
                    viewModel.updateDialogState(MemberVerificationPendingViewModel.DialogState.LEAVE);
                }
            });
        } else if (ordinal2 == 2) {
            TextView textView9 = getBinding().g;
            d0.z.d.m.checkNotNullExpressionValue(textView9, "binding.pendingDialogTitle");
            textView9.setText(getString(R.string.member_verification_pending_application_cancel_modal_title));
            TextView textView10 = getBinding().f;
            d0.z.d.m.checkNotNullExpressionValue(textView10, "binding.pendingDialogDesc");
            textView10.setText(getString(R.string.member_verification_pending_application_leave_server_modal_desc));
            MaterialButton materialButton8 = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(materialButton8, "binding.memberVerificationPrimaryBtn");
            materialButton8.setText(getString(R.string.member_verification_pending_application_modal_cancel));
            MaterialButton materialButton9 = getBinding().d;
            d0.z.d.m.checkNotNullExpressionValue(materialButton9, "binding.memberVerificationSecondaryBtn");
            materialButton9.setText(getString(R.string.member_verification_pending_application_modal_dismiss));
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingViewModel viewModel;
                    viewModel = MemberVerificationPendingDialog.this.getViewModel();
                    viewModel.resetGuildJoinRequest();
                    MemberVerificationPendingDialog.this.dismiss();
                }
            });
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingDialog.this.dismiss();
                }
            });
        } else if (ordinal2 == 3) {
            TextView textView11 = getBinding().g;
            d0.z.d.m.checkNotNullExpressionValue(textView11, "binding.pendingDialogTitle");
            textView11.setText(getString(R.string.member_verification_application_rejected_title));
            if (loaded.getRejectionReason() != null) {
                String rejectionReason = loaded.getRejectionReason();
                if (rejectionReason == null || rejectionReason.length() == 0) {
                    z2 = true;
                }
                if (!z2) {
                    TextView textView12 = getBinding().f;
                    d0.z.d.m.checkNotNullExpressionValue(textView12, "binding.pendingDialogDesc");
                    textView12.setText(getString(R.string.member_verification_application_rejected_reason) + " " + loaded.getRejectionReason());
                }
            }
            MaterialButton materialButton10 = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(materialButton10, "binding.memberVerificationPrimaryBtn");
            materialButton10.setText(getString(R.string.okay));
            MaterialButton materialButton11 = getBinding().d;
            d0.z.d.m.checkNotNullExpressionValue(materialButton11, "binding.memberVerificationSecondaryBtn");
            materialButton11.setText(getString(R.string.member_verification_application_reapply));
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$7
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingDialog.this.dismiss();
                }
            });
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$8
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingViewModel viewModel;
                    viewModel = MemberVerificationPendingDialog.this.getViewModel();
                    viewModel.resetGuildJoinRequest();
                    MemberVerificationPendingDialog.this.dismiss();
                }
            });
        } else if (ordinal2 == 4) {
            TextView textView13 = getBinding().g;
            d0.z.d.m.checkNotNullExpressionValue(textView13, "binding.pendingDialogTitle");
            textView13.setText(getString(R.string.member_verification_pending_application_leave_server_modal_title));
            TextView textView14 = getBinding().f;
            d0.z.d.m.checkNotNullExpressionValue(textView14, "binding.pendingDialogDesc");
            textView14.setText(getString(R.string.member_verification_pending_application_leave_server_modal_desc));
            MaterialButton materialButton12 = getBinding().c;
            d0.z.d.m.checkNotNullExpressionValue(materialButton12, "binding.memberVerificationPrimaryBtn");
            materialButton12.setText(getString(R.string.member_verification_pending_application_modal_leave));
            MaterialButton materialButton13 = getBinding().d;
            d0.z.d.m.checkNotNullExpressionValue(materialButton13, "binding.memberVerificationSecondaryBtn");
            materialButton13.setText(getString(R.string.member_verification_pending_application_modal_dismiss));
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$9
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingViewModel viewModel;
                    MemberVerificationPendingViewModel viewModel2;
                    if (loaded.isPreviewEnabled()) {
                        viewModel2 = MemberVerificationPendingDialog.this.getViewModel();
                        viewModel2.deleteGuildJoinRequest();
                    } else {
                        viewModel = MemberVerificationPendingDialog.this.getViewModel();
                        viewModel.leaveGuild();
                    }
                    MemberVerificationPendingDialog.this.dismiss();
                }
            });
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.member_verification.MemberVerificationPendingDialog$configureLoadedUI$10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MemberVerificationPendingDialog.this.dismiss();
                }
            });
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(MemberVerificationPendingViewModel.ViewState viewState) {
        if (viewState instanceof MemberVerificationPendingViewModel.ViewState.Loaded) {
            configureLoadedUI((MemberVerificationPendingViewModel.ViewState.Loaded) viewState);
            return;
        }
        throw new NoWhenBranchMatchedException();
    }

    private final WidgetMemberVerificationPendingDialogBinding getBinding() {
        return (WidgetMemberVerificationPendingDialogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final MemberVerificationPendingViewModel getViewModel() {
        return (MemberVerificationPendingViewModel) this.viewModel$delegate.getValue();
    }

    public static final void show(FragmentManager fragmentManager, long j, MemberVerificationPendingViewModel.DialogState dialogState) {
        Companion.show(fragmentManager, j, dialogState);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<MemberVerificationPendingViewModel.ViewState> q = getViewModel().observeViewState().q();
        d0.z.d.m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), MemberVerificationPendingDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new MemberVerificationPendingDialog$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), MemberVerificationPendingDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new MemberVerificationPendingDialog$onViewBoundOrOnResume$2(this));
    }
}
