package com.discord.widgets.servers;

import b.d.b.a.a;
import com.discord.app.AppLog;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter;
import com.discord.widgets.servers.SettingsChannelListAdapter;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: SettingsChannelListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", "item", "", "invoke", "(Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;)Ljava/lang/Long;", "getChannelId"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsChannelListAdapter$computeChangedPositions$1 extends o implements Function1<CategoricalDragAndDropAdapter.Payload, Long> {
    public static final SettingsChannelListAdapter$computeChangedPositions$1 INSTANCE = new SettingsChannelListAdapter$computeChangedPositions$1();

    public SettingsChannelListAdapter$computeChangedPositions$1() {
        super(1);
    }

    public final Long invoke(CategoricalDragAndDropAdapter.Payload payload) {
        m.checkNotNullParameter(payload, "item");
        int type = payload.getType();
        if (type == 0) {
            return Long.valueOf(((SettingsChannelListAdapter.ChannelItem) payload).getChannel().h());
        }
        if (type == 1) {
            return Long.valueOf(((SettingsChannelListAdapter.CategoryItem) payload).getId());
        }
        AppLog appLog = AppLog.g;
        StringBuilder R = a.R("Invalid type: ");
        R.append(payload.getType());
        Logger.e$default(appLog, R.toString(), null, null, 6, null);
        return null;
    }
}
