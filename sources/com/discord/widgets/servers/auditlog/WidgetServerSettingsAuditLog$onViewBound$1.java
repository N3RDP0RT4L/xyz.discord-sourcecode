package com.discord.widgets.servers.auditlog;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilter;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsAuditLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Landroid/view/Menu;", "kotlin.jvm.PlatformType", "menu", "", NotificationCompat.CATEGORY_CALL, "(Landroid/view/Menu;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsAuditLog$onViewBound$1<T> implements Action1<Menu> {
    public final /* synthetic */ WidgetServerSettingsAuditLog this$0;

    public WidgetServerSettingsAuditLog$onViewBound$1(WidgetServerSettingsAuditLog widgetServerSettingsAuditLog) {
        this.this$0 = widgetServerSettingsAuditLog;
    }

    public final void call(Menu menu) {
        View actionView;
        MenuItem findItem = menu.findItem(R.id.menu_filter);
        if (findItem != null && (actionView = findItem.getActionView()) != null) {
            actionView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog$onViewBound$1.1
                @Override // android.view.View.OnClickListener
                public final void onClick(final View view) {
                    PopupMenu popupMenu = new PopupMenu(new ContextThemeWrapper(WidgetServerSettingsAuditLog$onViewBound$1.this.this$0.getContext(), (int) R.style.AppTheme_PopupMenu), view);
                    popupMenu.getMenuInflater().inflate(R.menu.menu_audit_log_sort, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog.onViewBound.1.1.1
                        @Override // androidx.appcompat.widget.PopupMenu.OnMenuItemClickListener
                        public final boolean onMenuItemClick(MenuItem menuItem) {
                            long j;
                            long j2;
                            m.checkNotNullExpressionValue(menuItem, "it");
                            switch (menuItem.getItemId()) {
                                case R.id.menu_audit_log_sort_actions /* 2131364294 */:
                                    WidgetServerSettingsAuditLogFilter.Companion companion = WidgetServerSettingsAuditLogFilter.Companion;
                                    Context x2 = a.x(view, "view", "view.context");
                                    j = WidgetServerSettingsAuditLog$onViewBound$1.this.this$0.guildId;
                                    companion.show(x2, j, 1);
                                    break;
                                case R.id.menu_audit_log_sort_users /* 2131364295 */:
                                    WidgetServerSettingsAuditLogFilter.Companion companion2 = WidgetServerSettingsAuditLogFilter.Companion;
                                    Context x3 = a.x(view, "view", "view.context");
                                    j2 = WidgetServerSettingsAuditLog$onViewBound$1.this.this$0.guildId;
                                    companion2.show(x3, j2, 0);
                                    break;
                            }
                            return true;
                        }
                    });
                    popupMenu.show();
                }
            });
        }
    }
}
