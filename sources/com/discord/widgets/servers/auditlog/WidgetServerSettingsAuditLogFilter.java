package com.discord.widgets.servers.auditlog;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetAuditLogFilterBinding;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreAuditLog;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.utilities.auditlogs.AuditLogUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.search.SearchUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilter;
import com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilterAdapter;
import com.google.android.material.textfield.TextInputLayout;
import d0.f0.n;
import d0.f0.q;
import d0.g0.t;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlin.sequences.Sequence;
import rx.Observable;
import rx.functions.Func5;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsAuditLogFilter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u0000 )2\u00020\u0001:\u0002)*B\u0007¢\u0006\u0004\b(\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J!\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\u000eR\u0018\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u001e\u0010\u0015\u001a\n\u0018\u00010\u0013j\u0004\u0018\u0001`\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u0018\u0010\u001e\u001a\u0004\u0018\u00010\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001c\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00040 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R:\u0010&\u001a&\u0012\f\u0012\n %*\u0004\u0018\u00010$0$ %*\u0012\u0012\f\u0012\n %*\u0004\u0018\u00010$0$\u0018\u00010#0#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'¨\u0006+"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilter;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilter$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilter$Model;)V", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onViewBoundOrOnResume", "()V", "onPause", "", "filterType", "Ljava/lang/Integer;", "", "Lcom/discord/primitives/GuildId;", "guildId", "Ljava/lang/Long;", "Lcom/discord/databinding/WidgetAuditLogFilterBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetAuditLogFilterBinding;", "binding", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilterAdapter;", "adapter", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilterAdapter;", "Lkotlin/Function0;", "onFilterSelectedCallback", "Lkotlin/jvm/functions/Function0;", "Lrx/subjects/BehaviorSubject;", "", "kotlin.jvm.PlatformType", "filterPublisher", "Lrx/subjects/BehaviorSubject;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsAuditLogFilter extends AppFragment {
    private static final String ARG_FILTER_TYPE = "ARG_FILTER_TYPE";
    private static final String ARG_GUILD_ID = "ARG_GUILD_ID";
    private WidgetServerSettingsAuditLogFilterAdapter adapter;
    private Integer filterType;
    private Long guildId;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsAuditLogFilter.class, "binding", "getBinding()Lcom/discord/databinding/WidgetAuditLogFilterBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsAuditLogFilter$binding$2.INSTANCE, null, 2, null);
    private final BehaviorSubject<String> filterPublisher = BehaviorSubject.l0("");
    private final Function0<Unit> onFilterSelectedCallback = new WidgetServerSettingsAuditLogFilter$onFilterSelectedCallback$1(this);

    /* compiled from: WidgetServerSettingsAuditLogFilter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J)\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilter$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "filterType", "", "show", "(Landroid/content/Context;JI)V", "", WidgetServerSettingsAuditLogFilter.ARG_FILTER_TYPE, "Ljava/lang/String;", WidgetServerSettingsAuditLogFilter.ARG_GUILD_ID, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Context context, long j, int i) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra(WidgetServerSettingsAuditLogFilter.ARG_GUILD_ID, j).putExtra(WidgetServerSettingsAuditLogFilter.ARG_FILTER_TYPE, i);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…_FILTER_TYPE, filterType)");
            j.d(context, WidgetServerSettingsAuditLogFilter.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsAuditLogFilter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\b\u0002\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0015\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\b\u0010\tR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilter$Model;", "", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "listItems", "Ljava/util/List;", "getListItems", "()Ljava/util/List;", HookHelper.constructorName, "(Ljava/util/List;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final List<MGRecyclerDataPayload> listItems;

        /* compiled from: WidgetServerSettingsAuditLogFilter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J+\u0010\b\u001a\u0012\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0002j\u0002`\u00070\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\b\u0010\tJ?\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u00052\u0006\u0010\u000b\u001a\u00020\n2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\r\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005H\u0007¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilter$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "", "Lcom/discord/primitives/UserId;", "getPermissionUserIds", "(J)Lrx/Observable;", "Landroid/content/Context;", "context", "", "filterType", "", "filterPublisher", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilter$Model;", "get", "(Landroid/content/Context;JILrx/Observable;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            private final Observable<Set<Long>> getPermissionUserIds(long j) {
                StoreGuilds guilds = StoreStream.Companion.getGuilds();
                Observable<Set<Long>> q = Observable.i(guilds.observeGuild(j), guilds.observeRoles(j), guilds.observeComputed(j), WidgetServerSettingsAuditLogFilter$Model$Companion$getPermissionUserIds$1.INSTANCE).q();
                m.checkNotNullExpressionValue(q, "Observable\n            .…  .distinctUntilChanged()");
                return q;
            }

            @SuppressLint({"DefaultLocale"})
            public final Observable<Model> get(final Context context, long j, final int i, Observable<String> observable) {
                m.checkNotNullParameter(context, "context");
                m.checkNotNullParameter(observable, "filterPublisher");
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<Model> q = Observable.g(companion.getAuditLog().observeAuditLogState(j), getPermissionUserIds(j).Y(WidgetServerSettingsAuditLogFilter$Model$Companion$get$1.INSTANCE), companion.getGuilds().observeComputed(j), new k(AuditLogUtils.INSTANCE.getALL_ACTION_TYPES()), ObservableExtensionsKt.leadingEdgeThrottle(observable, 100L, TimeUnit.MILLISECONDS).q(), new Func5<StoreAuditLog.AuditLogState, Map<Long, ? extends User>, Map<Long, ? extends GuildMember>, List<? extends Integer>, String, Model>() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilter$Model$Companion$get$2

                    /* compiled from: WidgetServerSettingsAuditLogFilter.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(I)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilter$Model$Companion$get$2$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass1 extends o implements Function1<Integer, Boolean> {
                        public final /* synthetic */ String $filterText;

                        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                        public AnonymousClass1(String str) {
                            super(1);
                            this.$filterText = str;
                        }

                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Boolean invoke(Integer num) {
                            return Boolean.valueOf(invoke(num.intValue()));
                        }

                        public final boolean invoke(int i) {
                            SearchUtils searchUtils = SearchUtils.INSTANCE;
                            String str = this.$filterText;
                            m.checkNotNullExpressionValue(str, "filterText");
                            Objects.requireNonNull(str, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase = str.toLowerCase();
                            m.checkNotNullExpressionValue(lowerCase, "(this as java.lang.String).toLowerCase()");
                            String string = context.getString(AuditLogUtils.INSTANCE.getActionName(i));
                            m.checkNotNullExpressionValue(string, "context.getString(AuditLogUtils.getActionName(it))");
                            Objects.requireNonNull(string, "null cannot be cast to non-null type java.lang.String");
                            String lowerCase2 = string.toLowerCase();
                            m.checkNotNullExpressionValue(lowerCase2, "(this as java.lang.String).toLowerCase()");
                            return searchUtils.fuzzyMatch(lowerCase, lowerCase2);
                        }
                    }

                    /* compiled from: WidgetServerSettingsAuditLogFilter.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilterAdapter$AuditLogActionFilterItem;", "invoke", "(I)Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogFilterAdapter$AuditLogActionFilterItem;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilter$Model$Companion$get$2$2  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass2 extends o implements Function1<Integer, WidgetServerSettingsAuditLogFilterAdapter.AuditLogActionFilterItem> {
                        public final /* synthetic */ StoreAuditLog.AuditLogState $auditLogState;

                        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                        public AnonymousClass2(StoreAuditLog.AuditLogState auditLogState) {
                            super(1);
                            this.$auditLogState = auditLogState;
                        }

                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ WidgetServerSettingsAuditLogFilterAdapter.AuditLogActionFilterItem invoke(Integer num) {
                            return invoke(num.intValue());
                        }

                        public final WidgetServerSettingsAuditLogFilterAdapter.AuditLogActionFilterItem invoke(int i) {
                            String string = context.getString(AuditLogUtils.INSTANCE.getActionName(i));
                            m.checkNotNullExpressionValue(string, "context.getString(AuditLogUtils.getActionName(it))");
                            return new WidgetServerSettingsAuditLogFilterAdapter.AuditLogActionFilterItem(i, string, this.$auditLogState.getFilter().getActionFilter() == i);
                        }
                    }

                    @Override // rx.functions.Func5
                    public /* bridge */ /* synthetic */ WidgetServerSettingsAuditLogFilter.Model call(StoreAuditLog.AuditLogState auditLogState, Map<Long, ? extends User> map, Map<Long, ? extends GuildMember> map2, List<? extends Integer> list, String str) {
                        return call2(auditLogState, map, (Map<Long, GuildMember>) map2, (List<Integer>) list, str);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final WidgetServerSettingsAuditLogFilter.Model call2(StoreAuditLog.AuditLogState auditLogState, Map<Long, ? extends User> map, Map<Long, GuildMember> map2, List<Integer> list, String str) {
                        Sequence sequence;
                        if (i == 0) {
                            m.checkNotNullExpressionValue(str, "filterText");
                            if (t.isBlank(str)) {
                                sequence = u.asSequence(d0.t.m.listOf(new WidgetServerSettingsAuditLogFilterAdapter.AuditLogUserFilterItem(null, null, 0L, context.getString(R.string.guild_settings_filter_all_users), null, auditLogState.getFilter().getUserFilter() == 0)));
                            } else {
                                sequence = n.emptySequence();
                            }
                            return new WidgetServerSettingsAuditLogFilter.Model(q.toList(q.sortedWith(q.plus(sequence, q.map(q.filterNotNull(q.filter(u.asSequence(map.values()), new WidgetServerSettingsAuditLogFilter$Model$Companion$get$2$listItems$1(str))), new WidgetServerSettingsAuditLogFilter$Model$Companion$get$2$listItems$2(map2, auditLogState))), new Comparator() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilter$Model$Companion$get$2$$special$$inlined$sortedBy$1
                                @Override // java.util.Comparator
                                public final int compare(T t, T t2) {
                                    return d0.u.a.compareValues(Boolean.valueOf(!((WidgetServerSettingsAuditLogFilterAdapter.AuditLogUserFilterItem) t).isChecked()), Boolean.valueOf(!((WidgetServerSettingsAuditLogFilterAdapter.AuditLogUserFilterItem) t2).isChecked()));
                                }
                            })));
                        }
                        m.checkNotNullExpressionValue(list, "actions");
                        return new WidgetServerSettingsAuditLogFilter.Model(q.toList(q.sortedWith(q.map(q.filter(u.asSequence(list), new AnonymousClass1(str)), new AnonymousClass2(auditLogState)), new Comparator() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilter$Model$Companion$get$2$$special$$inlined$sortedBy$2
                            @Override // java.util.Comparator
                            public final int compare(T t, T t2) {
                                return d0.u.a.compareValues(Boolean.valueOf(!((WidgetServerSettingsAuditLogFilterAdapter.AuditLogActionFilterItem) t).isChecked()), Boolean.valueOf(!((WidgetServerSettingsAuditLogFilterAdapter.AuditLogActionFilterItem) t2).isChecked()));
                            }
                        })));
                    }
                }).q();
                m.checkNotNullExpressionValue(q, "Observable\n             …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(List<? extends MGRecyclerDataPayload> list) {
            m.checkNotNullParameter(list, "listItems");
            this.listItems = list;
        }

        public final List<MGRecyclerDataPayload> getListItems() {
            return this.listItems;
        }
    }

    public WidgetServerSettingsAuditLogFilter() {
        super(R.layout.widget_audit_log_filter);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        if (this.filterType == null) {
            FragmentActivity activity = e();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        WidgetServerSettingsAuditLogFilterAdapter widgetServerSettingsAuditLogFilterAdapter = this.adapter;
        if (widgetServerSettingsAuditLogFilterAdapter != null) {
            widgetServerSettingsAuditLogFilterAdapter.configure(model.getListItems(), this.onFilterSelectedCallback);
        }
    }

    private final WidgetAuditLogFilterBinding getBinding() {
        return (WidgetAuditLogFilterBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        AppFragment.hideKeyboard$default(this, null, 1, null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        this.filterType = Integer.valueOf(getMostRecentIntent().getIntExtra(ARG_FILTER_TYPE, -1));
        long j = 0;
        this.guildId = Long.valueOf(getMostRecentIntent().getLongExtra(ARG_GUILD_ID, 0L));
        Model.Companion companion = Model.Companion;
        Context requireContext = requireContext();
        Long l = this.guildId;
        if (l != null) {
            j = l.longValue();
        }
        long j2 = j;
        Integer num = this.filterType;
        if (num != null) {
            int intValue = num.intValue();
            BehaviorSubject<String> behaviorSubject = this.filterPublisher;
            m.checkNotNullExpressionValue(behaviorSubject, "filterPublisher");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(companion.get(requireContext, j2, intValue, behaviorSubject)), this, null, 2, null), WidgetServerSettingsAuditLogFilter.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetServerSettingsAuditLogFilter$onViewBoundOrOnResume$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsAuditLogFilter$onViewBoundOrOnResume$2(this));
        }
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.auditLogFilterRecycler");
        WidgetServerSettingsAuditLogFilterAdapter widgetServerSettingsAuditLogFilterAdapter = (WidgetServerSettingsAuditLogFilterAdapter) companion.configure(new WidgetServerSettingsAuditLogFilterAdapter(recyclerView));
        widgetServerSettingsAuditLogFilterAdapter.setOnAuditLogAvatarClicked(new WidgetServerSettingsAuditLogFilter$onViewCreated$$inlined$apply$lambda$1(this));
        this.adapter = widgetServerSettingsAuditLogFilterAdapter;
        setActionBarTitle(R.string.guild_settings_label_audit_log);
        Integer num = this.filterType;
        setActionBarSubtitle((num != null && num.intValue() == 0) ? R.string.guild_settings_filter_user : R.string.guild_settings_filter_action);
        TextInputLayout textInputLayout = getBinding().f2211b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.auditLogFilterInput");
        Integer num2 = this.filterType;
        ViewExtensions.setSingleLineHint(textInputLayout, (num2 != null && num2.intValue() == 0) ? R.string.search_members : R.string.search_actions);
        TextInputLayout textInputLayout2 = getBinding().f2211b;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.auditLogFilterInput");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, this, new WidgetServerSettingsAuditLogFilter$onViewCreated$2(this));
    }
}
