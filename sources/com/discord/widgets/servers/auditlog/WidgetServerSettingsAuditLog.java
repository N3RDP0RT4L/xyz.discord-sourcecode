package com.discord.widgets.servers.auditlog;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.role.GuildRole;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsAuditLogBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelGuildIntegration;
import com.discord.models.domain.ModelWebhook;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.stores.StoreAuditLog;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreStream;
import com.discord.utilities.auditlogs.AuditLogChangeUtils;
import com.discord.utilities.auditlogs.AuditLogUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.WidgetServerSettingsChannels;
import com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog;
import com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogAdapter;
import com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLogFilter;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func6;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsAuditLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0006\u0018\u0000 #2\u00020\u0001:\u0002#$B\u0007¢\u0006\u0004\b\"\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\r\u0010\fR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0012\u001a\u00020\u00118\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0018\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001e\u001a\u00020\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!¨\u0006%"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "onDestroy", "", "loadingAuditLogs", "Z", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogAdapter;", "adapter", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLogAdapter;", "Lcom/discord/databinding/WidgetServerSettingsAuditLogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsAuditLogBinding;", "binding", "", "guildId", "J", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsAuditLog extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsAuditLog.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsAuditLogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int DIRECTION_DOWN = 1;
    private static final String INTENT_EXTRA_GUILD_ID = "GUILD_ID";
    private static final String INTENT_EXTRA_GUILD_NAME = "GUILD_NAME";
    private static final int RESULTS_VIEW_INDEX_LOGS = 0;
    private static final int RESULTS_VIEW_INDEX_NO_LOGS = 1;
    private static final int VIEW_INDEX_LOGS_CONTENT = 1;
    private static final int VIEW_INDEX_LOGS_LOADING = 0;
    private WidgetServerSettingsAuditLogAdapter adapter;
    private long guildId;
    private boolean loadingAuditLogs;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsAuditLog$binding$2.INSTANCE, null, 2, null);
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettingsAuditLog$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetServerSettingsAuditLog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J+\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u000eR\u0016\u0010\u0013\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u000eR\u0016\u0010\u0014\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u000eR\u0016\u0010\u0015\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u000e¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "guildName", "", "create", "(Landroid/content/Context;JLjava/lang/String;)V", "", "DIRECTION_DOWN", "I", WidgetServerSettingsChannels.INTENT_EXTRA_GUILD_ID, "Ljava/lang/String;", "INTENT_EXTRA_GUILD_NAME", "RESULTS_VIEW_INDEX_LOGS", "RESULTS_VIEW_INDEX_NO_LOGS", "VIEW_INDEX_LOGS_CONTENT", "VIEW_INDEX_LOGS_LOADING", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j, String str) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, "guildName");
            StoreStream.Companion.getAnalytics().onGuildSettingsPaneViewed("AUDIT_LOG", j);
            Intent putExtra = new Intent().putExtra(WidgetServerSettingsAuditLog.INTENT_EXTRA_GUILD_ID, j).putExtra(WidgetServerSettingsAuditLog.INTENT_EXTRA_GUILD_NAME, str);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…RA_GUILD_NAME, guildName)");
            j.d(context, WidgetServerSettingsAuditLog.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsAuditLog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model;", "", HookHelper.constructorName, "()V", "Companion", "Loaded", "Loading", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model$Loading;", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Model {
        public static final Companion Companion = new Companion(null);

        /* compiled from: WidgetServerSettingsAuditLog.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J1\u0010\n\u001a\u0004\u0018\u00010\t2\u0016\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\n\u0010\r\u001a\u00060\u0003j\u0002`\f2\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model$Companion;", "", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "channels", "Lcom/discord/models/domain/ModelAuditLogEntry;", "entry", "", "resolveChannelName", "(Ljava/util/Map;Lcom/discord/models/domain/ModelAuditLogEntry;)Ljava/lang/String;", "Lcom/discord/primitives/GuildId;", "guildId", "Landroid/content/Context;", "context", "Lrx/Observable;", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model;", "get", "(JLandroid/content/Context;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {

            @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public final /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    ModelAuditLogEntry.TargetType.values();
                    int[] iArr = new int[15];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[ModelAuditLogEntry.TargetType.CHANNEL.ordinal()] = 1;
                    iArr[ModelAuditLogEntry.TargetType.CHANNEL_OVERWRITE.ordinal()] = 2;
                    iArr[ModelAuditLogEntry.TargetType.USER.ordinal()] = 3;
                    iArr[ModelAuditLogEntry.TargetType.ROLE.ordinal()] = 4;
                    iArr[ModelAuditLogEntry.TargetType.GUILD.ordinal()] = 5;
                    iArr[ModelAuditLogEntry.TargetType.INVITE.ordinal()] = 6;
                    iArr[ModelAuditLogEntry.TargetType.ALL.ordinal()] = 7;
                    iArr[ModelAuditLogEntry.TargetType.WEBHOOK.ordinal()] = 8;
                    iArr[ModelAuditLogEntry.TargetType.EMOJI.ordinal()] = 9;
                    iArr[ModelAuditLogEntry.TargetType.INTEGRATION.ordinal()] = 10;
                    iArr[ModelAuditLogEntry.TargetType.STAGE_INSTANCE.ordinal()] = 11;
                    iArr[ModelAuditLogEntry.TargetType.GUILD_SCHEDULED_EVENT.ordinal()] = 12;
                    iArr[ModelAuditLogEntry.TargetType.THREAD.ordinal()] = 13;
                    iArr[ModelAuditLogEntry.TargetType.UNKNOWN.ordinal()] = 14;
                    iArr[ModelAuditLogEntry.TargetType.STICKER.ordinal()] = 15;
                }
            }

            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            /* JADX WARN: Removed duplicated region for block: B:28:0x0077  */
            /* JADX WARN: Removed duplicated region for block: B:31:0x0080  */
            /* JADX WARN: Removed duplicated region for block: B:43:0x00b0  */
            /* JADX WARN: Removed duplicated region for block: B:46:0x00b5 A[ADDED_TO_REGION] */
            /* JADX WARN: Removed duplicated region for block: B:49:0x00c2  */
            /* JADX WARN: Removed duplicated region for block: B:50:0x00d2  */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public final java.lang.String resolveChannelName(java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r8, com.discord.models.domain.ModelAuditLogEntry r9) {
                /*
                    r7 = this;
                    long r0 = r9.getTargetId()
                    java.lang.Long r0 = java.lang.Long.valueOf(r0)
                    java.lang.Object r8 = r8.get(r0)
                    com.discord.api.channel.Channel r8 = (com.discord.api.channel.Channel) r8
                    r0 = 35
                    r1 = 1
                    if (r8 == 0) goto L36
                    boolean r9 = com.discord.api.channel.ChannelUtils.s(r8)
                    if (r9 != r1) goto L29
                    java.lang.StringBuilder r9 = b.d.b.a.a.O(r0)
                    java.lang.String r8 = com.discord.api.channel.ChannelUtils.c(r8)
                    r9.append(r8)
                    java.lang.String r8 = r9.toString()
                    goto L2f
                L29:
                    if (r9 != 0) goto L30
                    java.lang.String r8 = com.discord.api.channel.ChannelUtils.c(r8)
                L2f:
                    return r8
                L30:
                    kotlin.NoWhenBranchMatchedException r8 = new kotlin.NoWhenBranchMatchedException
                    r8.<init>()
                    throw r8
                L36:
                    int r8 = r9.getActionTypeId()
                    r2 = 12
                    r3 = 0
                    if (r8 != r2) goto Ldb
                    java.util.List r8 = r9.getChanges()
                    java.lang.String r2 = "it"
                    if (r8 == 0) goto L72
                    java.util.Iterator r8 = r8.iterator()
                L4b:
                    boolean r4 = r8.hasNext()
                    if (r4 == 0) goto L68
                    java.lang.Object r4 = r8.next()
                    r5 = r4
                    com.discord.models.domain.ModelAuditLogEntry$Change r5 = (com.discord.models.domain.ModelAuditLogEntry.Change) r5
                    d0.z.d.m.checkNotNullExpressionValue(r5, r2)
                    java.lang.String r5 = r5.getKey()
                    java.lang.String r6 = "name"
                    boolean r5 = d0.z.d.m.areEqual(r5, r6)
                    if (r5 == 0) goto L4b
                    goto L69
                L68:
                    r4 = r3
                L69:
                    com.discord.models.domain.ModelAuditLogEntry$Change r4 = (com.discord.models.domain.ModelAuditLogEntry.Change) r4
                    if (r4 == 0) goto L72
                    java.lang.Object r8 = r4.getOldValue()
                    goto L73
                L72:
                    r8 = r3
                L73:
                    boolean r4 = r8 instanceof java.lang.String
                    if (r4 != 0) goto L78
                    r8 = r3
                L78:
                    java.lang.String r8 = (java.lang.String) r8
                    java.util.List r9 = r9.getChanges()
                    if (r9 == 0) goto Lab
                    java.util.Iterator r9 = r9.iterator()
                L84:
                    boolean r4 = r9.hasNext()
                    if (r4 == 0) goto La1
                    java.lang.Object r4 = r9.next()
                    r5 = r4
                    com.discord.models.domain.ModelAuditLogEntry$Change r5 = (com.discord.models.domain.ModelAuditLogEntry.Change) r5
                    d0.z.d.m.checkNotNullExpressionValue(r5, r2)
                    java.lang.String r5 = r5.getKey()
                    java.lang.String r6 = "type"
                    boolean r5 = d0.z.d.m.areEqual(r5, r6)
                    if (r5 == 0) goto L84
                    goto La2
                La1:
                    r4 = r3
                La2:
                    com.discord.models.domain.ModelAuditLogEntry$Change r4 = (com.discord.models.domain.ModelAuditLogEntry.Change) r4
                    if (r4 == 0) goto Lab
                    java.lang.Object r9 = r4.getOldValue()
                    goto Lac
                Lab:
                    r9 = r3
                Lac:
                    boolean r2 = r9 instanceof java.lang.Long
                    if (r2 != 0) goto Lb1
                    r9 = r3
                Lb1:
                    java.lang.Long r9 = (java.lang.Long) r9
                    if (r8 == 0) goto Ldb
                    if (r9 == 0) goto Ldb
                    long r2 = r9.longValue()
                    int r9 = (int) r2
                    boolean r9 = com.discord.api.channel.ChannelUtils.l(r9)
                    if (r9 != r1) goto Ld2
                    java.lang.StringBuilder r9 = new java.lang.StringBuilder
                    r9.<init>()
                    r9.append(r0)
                    r9.append(r8)
                    java.lang.String r8 = r9.toString()
                    goto Ld4
                Ld2:
                    if (r9 != 0) goto Ld5
                Ld4:
                    return r8
                Ld5:
                    kotlin.NoWhenBranchMatchedException r8 = new kotlin.NoWhenBranchMatchedException
                    r8.<init>()
                    throw r8
                Ldb:
                    return r3
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog.Model.Companion.resolveChannelName(java.util.Map, com.discord.models.domain.ModelAuditLogEntry):java.lang.String");
            }

            public final Observable<Model> get(final long j, final Context context) {
                m.checkNotNullParameter(context, "context");
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<Model> q = Observable.f(companion.getAuditLog().observeAuditLogState(j), companion.getGuilds().observeGuild(j), StoreChannels.observeChannelsForGuild$default(companion.getChannels(), j, null, 2, null), ObservableExtensionsKt.leadingEdgeThrottle(companion.getUsers().observeAllUsers(), 3L, TimeUnit.SECONDS), companion.getGuilds().observeRoles(j), companion.getGuilds().observeComputed(j), new Func6<StoreAuditLog.AuditLogState, Guild, Map<Long, ? extends Channel>, Map<Long, ? extends User>, Map<Long, ? extends GuildRole>, Map<Long, ? extends GuildMember>, Model>() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog$Model$Companion$get$1
                    @Override // rx.functions.Func6
                    public /* bridge */ /* synthetic */ WidgetServerSettingsAuditLog.Model call(StoreAuditLog.AuditLogState auditLogState, Guild guild, Map<Long, ? extends Channel> map, Map<Long, ? extends User> map2, Map<Long, ? extends GuildRole> map3, Map<Long, ? extends GuildMember> map4) {
                        return call2(auditLogState, guild, (Map<Long, Channel>) map, map2, (Map<Long, GuildRole>) map3, (Map<Long, GuildMember>) map4);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final WidgetServerSettingsAuditLog.Model call2(StoreAuditLog.AuditLogState auditLogState, Guild guild, Map<Long, Channel> map, Map<Long, ? extends User> map2, Map<Long, GuildRole> map3, Map<Long, GuildMember> map4) {
                        CharSequence charSequence;
                        CharSequence userNameWithDiscriminator$default;
                        Channel channel;
                        Object obj;
                        Map map5;
                        Map<Long, Channel> map6 = map;
                        if ((auditLogState != null ? auditLogState.getEntries() : null) == null || guild == null) {
                            return WidgetServerSettingsAuditLog.Model.Loading.INSTANCE;
                        }
                        HashMap hashMap = new HashMap();
                        hashMap.putAll(auditLogState.getDeletedTargets());
                        for (ModelAuditLogEntry modelAuditLogEntry : auditLogState.getEntries()) {
                            if (hashMap.get(modelAuditLogEntry.getTargetType()) == null) {
                                ModelAuditLogEntry.TargetType targetType = modelAuditLogEntry.getTargetType();
                                m.checkNotNullExpressionValue(targetType, "entry.targetType");
                                hashMap.put(targetType, new HashMap());
                            }
                            ModelAuditLogEntry.TargetType targetType2 = modelAuditLogEntry.getTargetType();
                            if (targetType2 != null) {
                                switch (targetType2.ordinal()) {
                                    case 0:
                                    case 1:
                                    case 2:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                    case 13:
                                    case 14:
                                        break;
                                    case 3:
                                    case 4:
                                        WidgetServerSettingsAuditLog.Model.Companion companion2 = WidgetServerSettingsAuditLog.Model.Companion;
                                        m.checkNotNullExpressionValue(map6, "channels");
                                        obj = companion2.resolveChannelName(map6, modelAuditLogEntry);
                                        break;
                                    case 5:
                                        User user = map2.get(Long.valueOf(modelAuditLogEntry.getTargetId()));
                                        if (user != null) {
                                            obj = UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user, null, null, 3, null);
                                            break;
                                        }
                                        obj = null;
                                        break;
                                    case 6:
                                        GuildRole guildRole = map3.get(Long.valueOf(modelAuditLogEntry.getTargetId()));
                                        if (guildRole != null) {
                                            obj = guildRole.g();
                                            break;
                                        }
                                        obj = null;
                                        break;
                                    default:
                                        throw new NoWhenBranchMatchedException();
                                }
                                if (!(obj == null || (map5 = (Map) hashMap.get(modelAuditLogEntry.getTargetType())) == null)) {
                                    CharSequence charSequence2 = (CharSequence) map5.put(Long.valueOf(modelAuditLogEntry.getTargetId()), obj);
                                }
                            }
                            obj = null;
                            if (obj == null) {
                                CharSequence charSequence22 = (CharSequence) map5.put(Long.valueOf(modelAuditLogEntry.getTargetId()), obj);
                            }
                        }
                        ModelAuditLogEntry.TargetType targetType3 = ModelAuditLogEntry.TargetType.CHANNEL;
                        if (hashMap.get(targetType3) == null) {
                            hashMap.put(targetType3, new HashMap());
                        }
                        m.checkNotNullExpressionValue(map6, "channels");
                        Iterator<Map.Entry<Long, Channel>> it = map.entrySet().iterator();
                        while (true) {
                            String str = "#";
                            if (it.hasNext()) {
                                Map.Entry<Long, Channel> next = it.next();
                                if (!ChannelUtils.s(next.getValue())) {
                                    str = "";
                                }
                                Map map7 = (Map) hashMap.get(ModelAuditLogEntry.TargetType.CHANNEL);
                                if (map7 != null) {
                                    Long valueOf = Long.valueOf(next.getValue().h());
                                    StringBuilder R = a.R(str);
                                    R.append(ChannelUtils.c(next.getValue()));
                                    CharSequence charSequence3 = (CharSequence) map7.put(valueOf, R.toString());
                                }
                            } else {
                                ModelAuditLogEntry.TargetType targetType4 = ModelAuditLogEntry.TargetType.USER;
                                if (hashMap.get(targetType4) == null) {
                                    hashMap.put(targetType4, new HashMap());
                                }
                                for (Map.Entry<Long, User> entry : auditLogState.getUsers().entrySet()) {
                                    Map map8 = (Map) hashMap.get(ModelAuditLogEntry.TargetType.USER);
                                    if (map8 != null) {
                                        CharSequence charSequence4 = (CharSequence) map8.put(entry.getKey(), UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, entry.getValue(), null, null, 3, null));
                                    }
                                }
                                ModelAuditLogEntry.TargetType targetType5 = ModelAuditLogEntry.TargetType.GUILD;
                                if (hashMap.get(targetType5) == null) {
                                    hashMap.put(targetType5, new HashMap());
                                }
                                Map map9 = (Map) hashMap.get(targetType5);
                                if (map9 != null) {
                                    CharSequence charSequence5 = (CharSequence) map9.put(Long.valueOf(j), guild.getName());
                                }
                                ModelAuditLogEntry.TargetType targetType6 = ModelAuditLogEntry.TargetType.WEBHOOK;
                                if (hashMap.get(targetType6) == null) {
                                    hashMap.put(targetType6, new HashMap());
                                }
                                for (ModelWebhook modelWebhook : auditLogState.getWebhooks()) {
                                    Map map10 = (Map) hashMap.get(ModelAuditLogEntry.TargetType.WEBHOOK);
                                    if (map10 != null) {
                                        CharSequence charSequence6 = (CharSequence) map10.put(Long.valueOf(modelWebhook.getId()), modelWebhook.getName());
                                    }
                                }
                                ModelAuditLogEntry.TargetType targetType7 = ModelAuditLogEntry.TargetType.INTEGRATION;
                                if (hashMap.get(targetType7) == null) {
                                    hashMap.put(targetType7, new HashMap());
                                }
                                for (ModelGuildIntegration modelGuildIntegration : auditLogState.getIntegrations()) {
                                    Map map11 = (Map) hashMap.get(ModelAuditLogEntry.TargetType.INTEGRATION);
                                    if (map11 != null) {
                                        CharSequence charSequence7 = (CharSequence) map11.put(Long.valueOf(modelGuildIntegration.getId()), modelGuildIntegration.getName());
                                    }
                                }
                                ModelAuditLogEntry.TargetType targetType8 = ModelAuditLogEntry.TargetType.GUILD_SCHEDULED_EVENT;
                                if (hashMap.get(targetType8) == null) {
                                    hashMap.put(targetType8, new HashMap());
                                }
                                for (GuildScheduledEvent guildScheduledEvent : auditLogState.getGuildScheduledEvents()) {
                                    Map map12 = (Map) hashMap.get(ModelAuditLogEntry.TargetType.GUILD_SCHEDULED_EVENT);
                                    if (map12 != null) {
                                        CharSequence charSequence8 = (CharSequence) map12.put(Long.valueOf(guildScheduledEvent.i()), guildScheduledEvent.j());
                                    }
                                }
                                ModelAuditLogEntry.TargetType targetType9 = ModelAuditLogEntry.TargetType.THREAD;
                                if (hashMap.get(targetType9) == null) {
                                    hashMap.put(targetType9, new HashMap());
                                }
                                for (Channel channel2 : auditLogState.getThreads()) {
                                    Map map13 = (Map) hashMap.get(ModelAuditLogEntry.TargetType.THREAD);
                                    if (map13 != null) {
                                        CharSequence charSequence9 = (CharSequence) map13.put(Long.valueOf(channel2.h()), channel2.m());
                                    }
                                }
                                boolean z2 = true;
                                ArrayList arrayList = new ArrayList(auditLogState.getEntries().size() + 1);
                                List<ModelAuditLogEntry> entries = auditLogState.getEntries();
                                ArrayList<ModelAuditLogEntry> arrayList2 = new ArrayList();
                                for (Object obj2 : entries) {
                                    if (AuditLogUtils.INSTANCE.getALL_ACTION_TYPES().contains(Integer.valueOf(((ModelAuditLogEntry) obj2).getActionTypeId()))) {
                                        arrayList2.add(obj2);
                                    }
                                }
                                for (ModelAuditLogEntry modelAuditLogEntry2 : arrayList2) {
                                    String str2 = (!(modelAuditLogEntry2.getTargetType() == ModelAuditLogEntry.TargetType.CHANNEL || modelAuditLogEntry2.getTargetType() == ModelAuditLogEntry.TargetType.CHANNEL_OVERWRITE) || (channel = map6.get(Long.valueOf(modelAuditLogEntry2.getTargetId()))) == null || ChannelUtils.s(channel) != z2) ? "" : str;
                                    User user2 = auditLogState.getUsers().get(Long.valueOf(modelAuditLogEntry2.getUserId()));
                                    String str3 = (user2 == null || (userNameWithDiscriminator$default = UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user2, null, null, 3, null)) == null) ? "" : userNameWithDiscriminator$default;
                                    long id2 = modelAuditLogEntry2.getId();
                                    Long selectedItemId = auditLogState.getSelectedItemId();
                                    boolean z3 = selectedItemId != null && id2 == selectedItemId.longValue();
                                    User user3 = auditLogState.getUsers().get(Long.valueOf(modelAuditLogEntry2.getUserId()));
                                    GuildMember guildMember = map4.get(Long.valueOf(modelAuditLogEntry2.getUserId()));
                                    AuditLogUtils auditLogUtils = AuditLogUtils.INSTANCE;
                                    CharSequence headerString = auditLogUtils.getHeaderString(modelAuditLogEntry2, str3, context, hashMap, str2);
                                    AuditLogChangeUtils auditLogChangeUtils = AuditLogChangeUtils.INSTANCE;
                                    arrayList.add(new WidgetServerSettingsAuditLogAdapter.AuditLogEntryItem(modelAuditLogEntry2, z3, user3, guildMember, headerString, auditLogChangeUtils.getChangeSummary(context, modelAuditLogEntry2, hashMap), auditLogChangeUtils.hasChangesToRender(modelAuditLogEntry2), auditLogUtils.getTimestampString(modelAuditLogEntry2, context)));
                                    z2 = true;
                                    map6 = map;
                                }
                                if (auditLogState.isLoading()) {
                                    arrayList.add(new WidgetServerSettingsAuditLogAdapter.AuditLogLoadingItem());
                                }
                                User user4 = map2.get(Long.valueOf(auditLogState.getFilter().getUserFilter()));
                                if (user4 == null || (charSequence = UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user4, null, null, 3, null)) == null) {
                                    charSequence = context.getString(R.string.guild_settings_filter_all_users);
                                    m.checkNotNullExpressionValue(charSequence, "context.getString(R.stri…ettings_filter_all_users)");
                                }
                                String string = context.getString(AuditLogUtils.INSTANCE.getActionName(auditLogState.getFilter().getActionFilter()));
                                m.checkNotNullExpressionValue(string, "context.getString(AuditL…ate.filter.actionFilter))");
                                return new WidgetServerSettingsAuditLog.Model.Loaded(arrayList, charSequence, string);
                            }
                        }
                    }
                }).q();
                m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetServerSettingsAuditLog.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\t\u0018\u00002\u00020\u0001B%\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u000f\u0010\u0010R\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u0019\u0010\t\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\u000e\u0010\f¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model$Loaded;", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model;", "", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "auditLogEntryItems", "Ljava/util/List;", "getAuditLogEntryItems", "()Ljava/util/List;", "", "usernameFilterText", "Ljava/lang/CharSequence;", "getUsernameFilterText", "()Ljava/lang/CharSequence;", "actionFilterText", "getActionFilterText", HookHelper.constructorName, "(Ljava/util/List;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends Model {
            private final CharSequence actionFilterText;
            private final List<MGRecyclerDataPayload> auditLogEntryItems;
            private final CharSequence usernameFilterText;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(List<? extends MGRecyclerDataPayload> list, CharSequence charSequence, CharSequence charSequence2) {
                super(null);
                m.checkNotNullParameter(list, "auditLogEntryItems");
                m.checkNotNullParameter(charSequence, "usernameFilterText");
                m.checkNotNullParameter(charSequence2, "actionFilterText");
                this.auditLogEntryItems = list;
                this.usernameFilterText = charSequence;
                this.actionFilterText = charSequence2;
            }

            public final CharSequence getActionFilterText() {
                return this.actionFilterText;
            }

            public final List<MGRecyclerDataPayload> getAuditLogEntryItems() {
                return this.auditLogEntryItems;
            }

            public final CharSequence getUsernameFilterText() {
                return this.usernameFilterText;
            }
        }

        /* compiled from: WidgetServerSettingsAuditLog.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model$Loading;", "Lcom/discord/widgets/servers/auditlog/WidgetServerSettingsAuditLog$Model;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends Model {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private Model() {
        }

        public /* synthetic */ Model(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetServerSettingsAuditLog() {
        super(R.layout.widget_server_settings_audit_log);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        if (model instanceof Model.Loading) {
            AppViewFlipper appViewFlipper = getBinding().e;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.serverSettingsAuditLogsViewFlipper");
            appViewFlipper.setDisplayedChild(0);
        } else if (model instanceof Model.Loaded) {
            WidgetServerSettingsAuditLogAdapter widgetServerSettingsAuditLogAdapter = this.adapter;
            if (widgetServerSettingsAuditLogAdapter != null) {
                widgetServerSettingsAuditLogAdapter.configure(((Model.Loaded) model).getAuditLogEntryItems());
            }
            AppViewFlipper appViewFlipper2 = getBinding().e;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.serverSettingsAuditLogsViewFlipper");
            appViewFlipper2.setDisplayedChild(1);
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.serverSettingsAuditLogsUserFilter");
            Model.Loaded loaded = (Model.Loaded) model;
            textView.setText(loaded.getUsernameFilterText());
            TextView textView2 = getBinding().f2515b;
            m.checkNotNullExpressionValue(textView2, "binding.serverSettingsAuditLogsActionFilter");
            textView2.setText(loaded.getActionFilterText());
            if (!loaded.getAuditLogEntryItems().isEmpty()) {
                AppViewFlipper appViewFlipper3 = getBinding().f;
                m.checkNotNullExpressionValue(appViewFlipper3, "binding.serverSettingsAuditLogsViewResultsFlipper");
                appViewFlipper3.setDisplayedChild(0);
            } else {
                AppViewFlipper appViewFlipper4 = getBinding().f;
                m.checkNotNullExpressionValue(appViewFlipper4, "binding.serverSettingsAuditLogsViewResultsFlipper");
                appViewFlipper4.setDisplayedChild(1);
            }
            this.loadingAuditLogs = false;
        }
    }

    public static final void create(Context context, long j, String str) {
        Companion.create(context, j, str);
    }

    private final WidgetServerSettingsAuditLogBinding getBinding() {
        return (WidgetServerSettingsAuditLogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        FragmentActivity activity = e();
        if (activity != null && activity.isFinishing()) {
            StoreStream.Companion.getAuditLog().clearState();
        }
        super.onDestroy();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        long longExtra = getMostRecentIntent().getLongExtra(INTENT_EXTRA_GUILD_ID, -1L);
        this.guildId = longExtra;
        if (longExtra == -1) {
            b.a.d.m.g(getContext(), R.string.crash_unexpected, 0, null, 12);
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
            }
        }
        setActionBarTitle(R.string.guild_settings_label_audit_log);
        setActionBarSubtitle(getMostRecentIntent().getStringExtra(INTENT_EXTRA_GUILD_NAME));
        setActionBarOptionsMenu(R.menu.menu_filter, null, new WidgetServerSettingsAuditLog$onViewBound$1(this));
        getBinding().c.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog$onViewBound$2
            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                m.checkNotNullParameter(recyclerView, "recyclerView");
                super.onScrollStateChanged(recyclerView, i);
                if (!recyclerView.canScrollVertically(1)) {
                    StoreStream.Companion.getAuditLog().fetchMoreAuditLogEntries();
                }
            }
        });
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.serverSettingsAuditLogsRecycler");
        WidgetServerSettingsAuditLogAdapter widgetServerSettingsAuditLogAdapter = (WidgetServerSettingsAuditLogAdapter) companion.configure(new WidgetServerSettingsAuditLogAdapter(recyclerView));
        widgetServerSettingsAuditLogAdapter.setOnAuditLogAvatarClicked(new WidgetServerSettingsAuditLog$onViewBound$$inlined$apply$lambda$1(this));
        this.adapter = widgetServerSettingsAuditLogAdapter;
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                long j;
                WidgetServerSettingsAuditLogFilter.Companion companion2 = WidgetServerSettingsAuditLogFilter.Companion;
                Context x2 = a.x(view2, "it", "it.context");
                j = WidgetServerSettingsAuditLog.this.guildId;
                companion2.show(x2, j, 0);
            }
        });
        getBinding().f2515b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog$onViewBound$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                long j;
                WidgetServerSettingsAuditLogFilter.Companion companion2 = WidgetServerSettingsAuditLogFilter.Companion;
                Context x2 = a.x(view2, "it", "it.context");
                j = WidgetServerSettingsAuditLog.this.guildId;
                companion2.show(x2, j, 1);
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Model.Companion.get(this.guildId, requireContext())), this, null, 2, null), WidgetServerSettingsAuditLog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsAuditLog$onViewBoundOrOnResume$1(this));
    }
}
