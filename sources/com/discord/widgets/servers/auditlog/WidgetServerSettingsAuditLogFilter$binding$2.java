package com.discord.widgets.servers.auditlog;

import android.view.View;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetAuditLogFilterBinding;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsAuditLogFilter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetAuditLogFilterBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetAuditLogFilterBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettingsAuditLogFilter$binding$2 extends k implements Function1<View, WidgetAuditLogFilterBinding> {
    public static final WidgetServerSettingsAuditLogFilter$binding$2 INSTANCE = new WidgetServerSettingsAuditLogFilter$binding$2();

    public WidgetServerSettingsAuditLogFilter$binding$2() {
        super(1, WidgetAuditLogFilterBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetAuditLogFilterBinding;", 0);
    }

    public final WidgetAuditLogFilterBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.audit_log_filter_input;
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.audit_log_filter_input);
        if (textInputLayout != null) {
            i = R.id.audit_log_filter_recycler;
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.audit_log_filter_recycler);
            if (recyclerView != null) {
                return new WidgetAuditLogFilterBinding((LinearLayout) view, textInputLayout, recyclerView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
