package com.discord.widgets.servers;

import androidx.core.app.NotificationCompat;
import com.discord.models.domain.ModelNotificationSettings;
import d0.z.d.m;
import j0.k.b;
import java.util.List;
import kotlin.Metadata;
/* compiled from: WidgetServerNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\u0010\b\u001a&\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0012\u0012\f\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00050\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/models/domain/ModelNotificationSettings;", "kotlin.jvm.PlatformType", "it", "", "Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/domain/ModelNotificationSettings;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerNotifications$Companion$getNotificationOverrides$1<T, R> implements b<ModelNotificationSettings, List<ModelNotificationSettings.ChannelOverride>> {
    public static final WidgetServerNotifications$Companion$getNotificationOverrides$1 INSTANCE = new WidgetServerNotifications$Companion$getNotificationOverrides$1();

    public final List<ModelNotificationSettings.ChannelOverride> call(ModelNotificationSettings modelNotificationSettings) {
        m.checkNotNullExpressionValue(modelNotificationSettings, "it");
        return modelNotificationSettings.getChannelOverrides();
    }
}
