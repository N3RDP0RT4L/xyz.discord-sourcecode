package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsRolesBinding;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.mg_recycler.DragAndDropAdapter;
import com.discord.utilities.mg_recycler.DragAndDropHelper;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.WidgetServerSettingsRoles;
import com.discord.widgets.servers.WidgetServerSettingsRolesAdapter;
import d0.g;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func4;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsRoles.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u0000 /2\u00020\u0001:\u0002/0B\u0007¢\u0006\u0004\b.\u0010\u0018J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J+\u0010\f\u001a\u00020\u00042\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u00072\u0006\u0010\u000b\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\f\u0010\rJ#\u0010\u0011\u001a\u00020\u00042\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f2\u0006\u0010\u000b\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0017\u0010\u0018R\u0018\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0018\u0010\u001d\u001a\u0004\u0018\u00010\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u001d\u0010$\u001a\u00020\u001f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001c\u0010&\u001a\u00020%8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)R\u001d\u0010\u0010\u001a\u00020\u000e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-¨\u00061"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsRoles;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;", "data", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;)V", "", "", "", "newPositions", "dataSnapshot", "processRoleDrop", "(Ljava/util/Map;Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "createRole", "(JLcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Landroidx/recyclerview/widget/ItemTouchHelper;", "itemTouchHelper", "Landroidx/recyclerview/widget/ItemTouchHelper;", "Lcom/discord/widgets/servers/WidgetServerSettingsRolesAdapter;", "adapter", "Lcom/discord/widgets/servers/WidgetServerSettingsRolesAdapter;", "Lcom/discord/databinding/WidgetServerSettingsRolesBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsRolesBinding;", "binding", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "guildId$delegate", "Lkotlin/Lazy;", "getGuildId", "()J", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsRoles extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsRoles.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsRolesBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private WidgetServerSettingsRolesAdapter adapter;
    private ItemTouchHelper itemTouchHelper;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsRoles$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetServerSettingsRoles$guildId$2(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettingsRoles$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetServerSettingsRoles.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Companion;", "", "Landroid/content/Context;", "context", "", "guildId", "", "create", "(Landroid/content/Context;J)V", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            StoreStream.Companion.getAnalytics().onGuildSettingsPaneViewed("ROLES", j);
            Intent putExtra = new Intent().putExtra("INTENT_EXTRA_GUILD_ID", j);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…_EXTRA_GUILD_ID, guildId)");
            j.d(context, WidgetServerSettingsRoles.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsRoles.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0012\b\u0086\b\u0018\u0000 *2\u00020\u0001:\u0001*B;\u0012\n\u0010\u0011\u001a\u00060\u0002j\u0002`\u0003\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0013\u001a\u00020\t\u0012\u0006\u0010\u0014\u001a\u00020\t\u0012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\u0004\b(\u0010)J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJ\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JN\u0010\u0016\u001a\u00020\u00002\f\b\u0002\u0010\u0011\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0013\u001a\u00020\t2\b\b\u0002\u0010\u0014\u001a\u00020\t2\u000e\b\u0002\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000e0\rHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0018\u0010\bJ\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001d\u001a\u00020\t2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u0019\u0010\u0013\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001f\u001a\u0004\b \u0010\u000bR\u001d\u0010\u0011\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010!\u001a\u0004\b\"\u0010\u0005R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\bR\u0019\u0010\u0014\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u001f\u001a\u0004\b%\u0010\u000bR\u001f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010&\u001a\u0004\b'\u0010\u0010¨\u0006+"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "", "component3", "()Z", "component4", "", "Lcom/discord/utilities/mg_recycler/DragAndDropAdapter$Payload;", "component5", "()Ljava/util/List;", "guildId", "guildName", "canManageRoles", "elevated", "roleItems", "copy", "(JLjava/lang/String;ZZLjava/util/List;)Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanManageRoles", "J", "getGuildId", "Ljava/lang/String;", "getGuildName", "getElevated", "Ljava/util/List;", "getRoleItems", HookHelper.constructorName, "(JLjava/lang/String;ZZLjava/util/List;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canManageRoles;
        private final boolean elevated;
        private final long guildId;
        private final String guildName;
        private final List<DragAndDropAdapter.Payload> roleItems;

        /* compiled from: WidgetServerSettingsRoles.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model$Companion;", "", "", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsRoles$Model;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(final long j) {
                Observable<Model> q = StoreUser.observeMe$default(StoreStream.Companion.getUsers(), false, 1, null).Y(new b<MeUser, Observable<? extends Model>>() { // from class: com.discord.widgets.servers.WidgetServerSettingsRoles$Model$Companion$get$1
                    public final Observable<? extends WidgetServerSettingsRoles.Model> call(final MeUser meUser) {
                        StoreStream.Companion companion = StoreStream.Companion;
                        return Observable.h(companion.getGuilds().observeGuild(j), companion.getGuilds().observeComputed(j, d0.t.m.listOf(Long.valueOf(meUser.getId()))).F(new b<Map<Long, ? extends GuildMember>, GuildMember>() { // from class: com.discord.widgets.servers.WidgetServerSettingsRoles$Model$Companion$get$1.1
                            @Override // j0.k.b
                            public /* bridge */ /* synthetic */ GuildMember call(Map<Long, ? extends GuildMember> map) {
                                return call2((Map<Long, GuildMember>) map);
                            }

                            /* renamed from: call  reason: avoid collision after fix types in other method */
                            public final GuildMember call2(Map<Long, GuildMember> map) {
                                return map.get(Long.valueOf(MeUser.this.getId()));
                            }
                        }), companion.getPermissions().observePermissionsForGuild(j), companion.getGuilds().observeRoles(j), new Func4<Guild, GuildMember, Long, Map<Long, ? extends GuildRole>, WidgetServerSettingsRoles.Model>() { // from class: com.discord.widgets.servers.WidgetServerSettingsRoles$Model$Companion$get$1.2
                            @Override // rx.functions.Func4
                            public /* bridge */ /* synthetic */ WidgetServerSettingsRoles.Model call(Guild guild, GuildMember guildMember, Long l, Map<Long, ? extends GuildRole> map) {
                                return call2(guild, guildMember, l, (Map<Long, GuildRole>) map);
                            }

                            /* renamed from: call  reason: avoid collision after fix types in other method */
                            public final WidgetServerSettingsRoles.Model call2(Guild guild, GuildMember guildMember, Long l, Map<Long, GuildRole> map) {
                                if (guild == null || guildMember == null || l == null || map == null) {
                                    return null;
                                }
                                boolean can = PermissionUtils.can(Permission.MANAGE_ROLES, l);
                                boolean isElevated = PermissionUtils.isElevated(meUser.getMfaEnabled(), guild.getMfaLevel());
                                GuildRole highestRole = RoleUtils.getHighestRole(map, guildMember);
                                ArrayList<GuildRole> arrayList = new ArrayList(map.values());
                                Collections.sort(arrayList, RoleUtils.getROLE_COMPARATOR());
                                ArrayList arrayList2 = new ArrayList(arrayList.size() + 1);
                                arrayList2.add(new WidgetServerSettingsRolesAdapter.HelpItem(j));
                                for (GuildRole guildRole : arrayList) {
                                    arrayList2.add(new WidgetServerSettingsRolesAdapter.RoleItem(guildRole, j == guildRole.getId(), guild.getOwnerId() != meUser.getId() && !RoleUtils.rankIsHigher(highestRole, guildRole), can, isElevated, j));
                                }
                                return new WidgetServerSettingsRoles.Model(j, guild.getName(), can, isElevated, arrayList2);
                            }
                        });
                    }
                }).q();
                m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(long j, String str, boolean z2, boolean z3, List<? extends DragAndDropAdapter.Payload> list) {
            m.checkNotNullParameter(list, "roleItems");
            this.guildId = j;
            this.guildName = str;
            this.canManageRoles = z2;
            this.elevated = z3;
            this.roleItems = list;
        }

        public static /* synthetic */ Model copy$default(Model model, long j, String str, boolean z2, boolean z3, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                j = model.guildId;
            }
            long j2 = j;
            if ((i & 2) != 0) {
                str = model.guildName;
            }
            String str2 = str;
            if ((i & 4) != 0) {
                z2 = model.canManageRoles;
            }
            boolean z4 = z2;
            if ((i & 8) != 0) {
                z3 = model.elevated;
            }
            boolean z5 = z3;
            List<DragAndDropAdapter.Payload> list2 = list;
            if ((i & 16) != 0) {
                list2 = model.roleItems;
            }
            return model.copy(j2, str2, z4, z5, list2);
        }

        public final long component1() {
            return this.guildId;
        }

        public final String component2() {
            return this.guildName;
        }

        public final boolean component3() {
            return this.canManageRoles;
        }

        public final boolean component4() {
            return this.elevated;
        }

        public final List<DragAndDropAdapter.Payload> component5() {
            return this.roleItems;
        }

        public final Model copy(long j, String str, boolean z2, boolean z3, List<? extends DragAndDropAdapter.Payload> list) {
            m.checkNotNullParameter(list, "roleItems");
            return new Model(j, str, z2, z3, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return this.guildId == model.guildId && m.areEqual(this.guildName, model.guildName) && this.canManageRoles == model.canManageRoles && this.elevated == model.elevated && m.areEqual(this.roleItems, model.roleItems);
        }

        public final boolean getCanManageRoles() {
            return this.canManageRoles;
        }

        public final boolean getElevated() {
            return this.elevated;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final String getGuildName() {
            return this.guildName;
        }

        public final List<DragAndDropAdapter.Payload> getRoleItems() {
            return this.roleItems;
        }

        public int hashCode() {
            int a = a0.a.a.b.a(this.guildId) * 31;
            String str = this.guildName;
            int i = 0;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            boolean z2 = this.canManageRoles;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            boolean z3 = this.elevated;
            if (!z3) {
                i2 = z3 ? 1 : 0;
            }
            int i6 = (i5 + i2) * 31;
            List<DragAndDropAdapter.Payload> list = this.roleItems;
            if (list != null) {
                i = list.hashCode();
            }
            return i6 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guildId=");
            R.append(this.guildId);
            R.append(", guildName=");
            R.append(this.guildName);
            R.append(", canManageRoles=");
            R.append(this.canManageRoles);
            R.append(", elevated=");
            R.append(this.elevated);
            R.append(", roleItems=");
            return a.K(R, this.roleItems, ")");
        }
    }

    public WidgetServerSettingsRoles() {
        super(R.layout.widget_server_settings_roles);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        if ((model != null ? model.getRoleItems() : null) == null || model.getRoleItems().isEmpty()) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.onBackPressed();
                return;
            }
            return;
        }
        setActionBarTitle(R.string.roles);
        setActionBarSubtitle(model.getGuildName());
        WidgetServerSettingsRolesAdapter widgetServerSettingsRolesAdapter = this.adapter;
        if (widgetServerSettingsRolesAdapter != null) {
            widgetServerSettingsRolesAdapter.configure(model.getRoleItems(), new WidgetServerSettingsRoles$configureUI$1(this, model), new WidgetServerSettingsRoles$configureUI$2(this, model));
        }
        if (!model.getCanManageRoles() || !model.getElevated()) {
            getBinding().c.hide();
            getBinding().c.setOnClickListener(null);
            return;
        }
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsRoles$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsRoles.this.createRole(model.getGuildId(), model);
            }
        });
        getBinding().c.show();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void createRole(final long j, Model model) {
        Observable Y = ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().createRole(j), false, 1, null).Y(new b<GuildRole, Observable<? extends GuildRole>>() { // from class: com.discord.widgets.servers.WidgetServerSettingsRoles$createRole$1
            public final Observable<? extends GuildRole> call(GuildRole guildRole) {
                final long a = guildRole.a();
                return (Observable<R>) StoreStream.Companion.getGuilds().observeRoles(j, d0.t.m.listOf(Long.valueOf(a))).F(new b<Map<Long, ? extends GuildRole>, GuildRole>() { // from class: com.discord.widgets.servers.WidgetServerSettingsRoles$createRole$1.1
                    @Override // j0.k.b
                    public /* bridge */ /* synthetic */ GuildRole call(Map<Long, ? extends GuildRole> map) {
                        return call2((Map<Long, GuildRole>) map);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final GuildRole call2(Map<Long, GuildRole> map) {
                        return map.get(Long.valueOf(a));
                    }
                });
            }
        });
        m.checkNotNullExpressionValue(Y, "RestAPI\n        .api\n   … rolesMap[id] }\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout$default(Y, 0L, false, 3, null), this, null, 2, null), WidgetServerSettingsRoles.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsRoles$createRole$2(this, j, model));
    }

    private final WidgetServerSettingsRolesBinding getBinding() {
        return (WidgetServerSettingsRolesBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void processRoleDrop(Map<String, Integer> map, Model model) {
        Set<String> keySet = map.keySet();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = keySet.iterator();
        while (true) {
            RestAPIParams.Role role = null;
            if (it.hasNext()) {
                String str = (String) it.next();
                Integer num = map.get(str);
                if (num != null) {
                    role = RestAPIParams.Role.Companion.createForPosition(Long.parseLong(str), num.intValue());
                }
                if (role != null) {
                    arrayList.add(role);
                }
            } else {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().batchUpdateRole(model.getGuildId(), arrayList), false, 1, null), this, null, 2, null), getBinding().f2559b, 0L, 2, null), WidgetServerSettingsRoles.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetServerSettingsRoles$processRoleDrop$2(this, model), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsRoles$processRoleDrop$1(this));
                return;
            }
        }
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().d;
        m.checkNotNullExpressionValue(recyclerView, "binding.serverSettingsRolesRecycler");
        this.adapter = (WidgetServerSettingsRolesAdapter) companion.configure(new WidgetServerSettingsRolesAdapter(recyclerView));
        ItemTouchHelper itemTouchHelper = this.itemTouchHelper;
        if (!(itemTouchHelper == null || itemTouchHelper == null)) {
            itemTouchHelper.attachToRecyclerView(null);
        }
        WidgetServerSettingsRolesAdapter widgetServerSettingsRolesAdapter = this.adapter;
        m.checkNotNull(widgetServerSettingsRolesAdapter);
        ItemTouchHelper itemTouchHelper2 = new ItemTouchHelper(new DragAndDropHelper(widgetServerSettingsRolesAdapter, 0, 2, null));
        this.itemTouchHelper = itemTouchHelper2;
        if (itemTouchHelper2 != null) {
            itemTouchHelper2.attachToRecyclerView(getBinding().d);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(Model.Companion.get(getGuildId())), this, null, 2, null), WidgetServerSettingsRoles.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsRoles$onViewBoundOrOnResume$1(this));
    }
}
