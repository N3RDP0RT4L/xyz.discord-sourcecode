package com.discord.widgets.servers.guildboost;

import com.discord.models.domain.ModelAppliedGuildBoost;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GuildBoostTransferInProgressViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/models/domain/ModelAppliedGuildBoost;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostTransferInProgressViewModel$transferGuildBoost$3 extends o implements Function1<List<? extends ModelAppliedGuildBoost>, Unit> {
    public final /* synthetic */ GuildBoostTransferInProgressViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildBoostTransferInProgressViewModel$transferGuildBoost$3(GuildBoostTransferInProgressViewModel guildBoostTransferInProgressViewModel) {
        super(1);
        this.this$0 = guildBoostTransferInProgressViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelAppliedGuildBoost> list) {
        invoke2((List<ModelAppliedGuildBoost>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<ModelAppliedGuildBoost> list) {
        this.this$0.handleGuildBoostingCompleted();
    }
}
