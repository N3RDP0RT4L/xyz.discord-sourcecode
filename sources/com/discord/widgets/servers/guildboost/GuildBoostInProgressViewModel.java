package com.discord.widgets.servers.guildboost;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAppliedGuildBoost;
import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.models.guild.Guild;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGuildBoost;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func2;
/* compiled from: GuildBoostInProgressViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003!\"#B-\u0012\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0018\u0012\u000e\b\u0002\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00030\u001d¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\n\u0010\tJ\u000f\u0010\u000b\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u000f\u0010\f\u001a\u00020\u0005H\u0014¢\u0006\u0004\b\f\u0010\tJ'\u0010\u0012\u001a\u00020\u00052\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e2\n\u0010\u0011\u001a\u00060\rj\u0002`\u0010H\u0007¢\u0006\u0004\b\u0012\u0010\u0013R\u001a\u0010\u000f\u001a\u00060\rj\u0002`\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0014R\u0018\u0010\u0016\u001a\u0004\u0018\u00010\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0019\u0010\u0019\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c¨\u0006$"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState;", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$StoreState;)V", "handleGuildBoostingStarted", "()V", "handleGuildBoostingCompleted", "handleGuildBoostingError", "onCleared", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/GuildBoostSlotId;", "slotId", "subscribeToGuildBoost", "(JJ)V", "J", "Lrx/Subscription;", "guildBoostSubscription", "Lrx/Subscription;", "Lcom/discord/stores/StoreGuildBoost;", "storeGuildBoost", "Lcom/discord/stores/StoreGuildBoost;", "getStoreGuildBoost", "()Lcom/discord/stores/StoreGuildBoost;", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(JLcom/discord/stores/StoreGuildBoost;Lrx/Observable;)V", "GuildBoostState", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostInProgressViewModel extends AppViewModel<ViewState> {
    private Subscription guildBoostSubscription;
    private final long guildId;
    private final StoreGuildBoost storeGuildBoost;

    /* compiled from: GuildBoostInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\n \u0003*\u0004\u0018\u00010\u00050\u00052\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00020\u0002H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/stores/StoreGuildBoost$State;", "kotlin.jvm.PlatformType", "guildBoostState", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildBoost$State;)Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T1, T2, R> implements Func2<Guild, StoreGuildBoost.State, StoreState> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final StoreState call(Guild guild, StoreGuildBoost.State state) {
            m.checkNotNullExpressionValue(state, "guildBoostState");
            return new StoreState(guild, state);
        }
    }

    /* compiled from: GuildBoostInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GuildBoostInProgressViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GuildBoostInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "NOT_IN_PROGRESS", "CALL_IN_PROGRESS", "COMPLETED", "ERROR", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum GuildBoostState {
        NOT_IN_PROGRESS,
        CALL_IN_PROGRESS,
        COMPLETED,
        ERROR
    }

    /* compiled from: GuildBoostInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$StoreState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/stores/StoreGuildBoost$State;", "component2", "()Lcom/discord/stores/StoreGuildBoost$State;", "guild", "guildBoostState", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildBoost$State;)Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreGuildBoost$State;", "getGuildBoostState", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildBoost$State;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Guild guild;
        private final StoreGuildBoost.State guildBoostState;

        public StoreState(Guild guild, StoreGuildBoost.State state) {
            m.checkNotNullParameter(state, "guildBoostState");
            this.guild = guild;
            this.guildBoostState = state;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Guild guild, StoreGuildBoost.State state, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = storeState.guild;
            }
            if ((i & 2) != 0) {
                state = storeState.guildBoostState;
            }
            return storeState.copy(guild, state);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final StoreGuildBoost.State component2() {
            return this.guildBoostState;
        }

        public final StoreState copy(Guild guild, StoreGuildBoost.State state) {
            m.checkNotNullParameter(state, "guildBoostState");
            return new StoreState(guild, state);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guild, storeState.guild) && m.areEqual(this.guildBoostState, storeState.guildBoostState);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final StoreGuildBoost.State getGuildBoostState() {
            return this.guildBoostState;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            StoreGuildBoost.State state = this.guildBoostState;
            if (state != null) {
                i = state.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guild=");
            R.append(this.guild);
            R.append(", guildBoostState=");
            R.append(this.guildBoostState);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildBoostInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\t\nB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0002\u000b\f¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState;", "", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "guildBoostState", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "getGuildBoostState", "()Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", HookHelper.constructorName, "(Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;)V", "Loaded", "Uninitialized", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {
        private final GuildBoostState guildBoostState;

        /* compiled from: GuildBoostInProgressViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u000b¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ:\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0017\u0010\nJ\u001a\u0010\u001a\u001a\u00020\u000b2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0018HÖ\u0003¢\u0006\u0004\b\u001a\u0010\u001bR\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001c\u001a\u0004\b\u001d\u0010\nR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\rR\u001c\u0010\u000e\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\"\u001a\u0004\b#\u0010\u0004¨\u0006&"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState$Loaded;", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState;", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "component1", "()Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "", "component3", "()I", "", "component4", "()Z", "guildBoostState", "guild", "subscriptionCount", "canShowConfirmationDialog", "copy", "(Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;Lcom/discord/models/guild/Guild;IZ)Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getSubscriptionCount", "Lcom/discord/models/guild/Guild;", "getGuild", "Z", "getCanShowConfirmationDialog", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "getGuildBoostState", HookHelper.constructorName, "(Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;Lcom/discord/models/guild/Guild;IZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean canShowConfirmationDialog;
            private final Guild guild;
            private final GuildBoostState guildBoostState;
            private final int subscriptionCount;

            public /* synthetic */ Loaded(GuildBoostState guildBoostState, Guild guild, int i, boolean z2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
                this(guildBoostState, guild, i, (i2 & 8) != 0 ? false : z2);
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, GuildBoostState guildBoostState, Guild guild, int i, boolean z2, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    guildBoostState = loaded.getGuildBoostState();
                }
                if ((i2 & 2) != 0) {
                    guild = loaded.guild;
                }
                if ((i2 & 4) != 0) {
                    i = loaded.subscriptionCount;
                }
                if ((i2 & 8) != 0) {
                    z2 = loaded.canShowConfirmationDialog;
                }
                return loaded.copy(guildBoostState, guild, i, z2);
            }

            public final GuildBoostState component1() {
                return getGuildBoostState();
            }

            public final Guild component2() {
                return this.guild;
            }

            public final int component3() {
                return this.subscriptionCount;
            }

            public final boolean component4() {
                return this.canShowConfirmationDialog;
            }

            public final Loaded copy(GuildBoostState guildBoostState, Guild guild, int i, boolean z2) {
                m.checkNotNullParameter(guildBoostState, "guildBoostState");
                return new Loaded(guildBoostState, guild, i, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return m.areEqual(getGuildBoostState(), loaded.getGuildBoostState()) && m.areEqual(this.guild, loaded.guild) && this.subscriptionCount == loaded.subscriptionCount && this.canShowConfirmationDialog == loaded.canShowConfirmationDialog;
            }

            public final boolean getCanShowConfirmationDialog() {
                return this.canShowConfirmationDialog;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            @Override // com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel.ViewState
            public GuildBoostState getGuildBoostState() {
                return this.guildBoostState;
            }

            public final int getSubscriptionCount() {
                return this.subscriptionCount;
            }

            public int hashCode() {
                GuildBoostState guildBoostState = getGuildBoostState();
                int i = 0;
                int hashCode = (guildBoostState != null ? guildBoostState.hashCode() : 0) * 31;
                Guild guild = this.guild;
                if (guild != null) {
                    i = guild.hashCode();
                }
                int i2 = (((hashCode + i) * 31) + this.subscriptionCount) * 31;
                boolean z2 = this.canShowConfirmationDialog;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(guildBoostState=");
                R.append(getGuildBoostState());
                R.append(", guild=");
                R.append(this.guild);
                R.append(", subscriptionCount=");
                R.append(this.subscriptionCount);
                R.append(", canShowConfirmationDialog=");
                return a.M(R, this.canShowConfirmationDialog, ")");
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(GuildBoostState guildBoostState, Guild guild, int i, boolean z2) {
                super(guildBoostState, null);
                m.checkNotNullParameter(guildBoostState, "guildBoostState");
                this.guildBoostState = guildBoostState;
                this.guild = guild;
                this.subscriptionCount = i;
                this.canShowConfirmationDialog = z2;
            }
        }

        /* compiled from: GuildBoostInProgressViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0005\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState;", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "component1", "()Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "guildBoostState", "copy", "(Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;)Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState$Uninitialized;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;", "getGuildBoostState", HookHelper.constructorName, "(Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$GuildBoostState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            private final GuildBoostState guildBoostState;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Uninitialized(GuildBoostState guildBoostState) {
                super(guildBoostState, null);
                m.checkNotNullParameter(guildBoostState, "guildBoostState");
                this.guildBoostState = guildBoostState;
            }

            public static /* synthetic */ Uninitialized copy$default(Uninitialized uninitialized, GuildBoostState guildBoostState, int i, Object obj) {
                if ((i & 1) != 0) {
                    guildBoostState = uninitialized.getGuildBoostState();
                }
                return uninitialized.copy(guildBoostState);
            }

            public final GuildBoostState component1() {
                return getGuildBoostState();
            }

            public final Uninitialized copy(GuildBoostState guildBoostState) {
                m.checkNotNullParameter(guildBoostState, "guildBoostState");
                return new Uninitialized(guildBoostState);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Uninitialized) && m.areEqual(getGuildBoostState(), ((Uninitialized) obj).getGuildBoostState());
                }
                return true;
            }

            @Override // com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel.ViewState
            public GuildBoostState getGuildBoostState() {
                return this.guildBoostState;
            }

            public int hashCode() {
                GuildBoostState guildBoostState = getGuildBoostState();
                if (guildBoostState != null) {
                    return guildBoostState.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("Uninitialized(guildBoostState=");
                R.append(getGuildBoostState());
                R.append(")");
                return R.toString();
            }
        }

        private ViewState(GuildBoostState guildBoostState) {
            this.guildBoostState = guildBoostState;
        }

        public GuildBoostState getGuildBoostState() {
            return this.guildBoostState;
        }

        public /* synthetic */ ViewState(GuildBoostState guildBoostState, DefaultConstructorMarker defaultConstructorMarker) {
            this(guildBoostState);
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GuildBoostInProgressViewModel(long r2, com.discord.stores.StoreGuildBoost r4, rx.Observable r5, int r6, kotlin.jvm.internal.DefaultConstructorMarker r7) {
        /*
            r1 = this;
            r7 = r6 & 2
            if (r7 == 0) goto La
            com.discord.stores.StoreStream$Companion r4 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildBoost r4 = r4.getGuildBoosts()
        La:
            r6 = r6 & 4
            if (r6 == 0) goto L2d
            com.discord.stores.StoreStream$Companion r5 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r6 = r5.getGuilds()
            rx.Observable r6 = r6.observeGuild(r2)
            com.discord.stores.StoreGuildBoost r5 = r5.getGuildBoosts()
            r7 = 1
            r0 = 0
            rx.Observable r5 = com.discord.stores.StoreGuildBoost.observeGuildBoostState$default(r5, r0, r7, r0)
            com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel$1 r7 = com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel.AnonymousClass1.INSTANCE
            rx.Observable r5 = rx.Observable.j(r6, r5, r7)
            java.lang.String r6 = "Observable.combineLatest…guild, guildBoostState) }"
            d0.z.d.m.checkNotNullExpressionValue(r5, r6)
        L2d:
            r1.<init>(r2, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel.<init>(long, com.discord.stores.StoreGuildBoost, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleGuildBoostingCompleted() {
        Object obj;
        this.storeGuildBoost.fetchUserGuildBoostState();
        ViewState requireViewState = requireViewState();
        if (requireViewState instanceof ViewState.Uninitialized) {
            obj = new ViewState.Uninitialized(GuildBoostState.COMPLETED);
        } else if (requireViewState instanceof ViewState.Loaded) {
            ViewState.Loaded loaded = (ViewState.Loaded) requireViewState;
            obj = new ViewState.Loaded(GuildBoostState.COMPLETED, loaded.getGuild(), loaded.getSubscriptionCount(), true);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        updateViewState(obj);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleGuildBoostingError() {
        Object obj;
        ViewState requireViewState = requireViewState();
        if (requireViewState instanceof ViewState.Uninitialized) {
            obj = new ViewState.Uninitialized(GuildBoostState.ERROR);
        } else if (requireViewState instanceof ViewState.Loaded) {
            ViewState.Loaded loaded = (ViewState.Loaded) requireViewState;
            obj = new ViewState.Loaded(GuildBoostState.ERROR, loaded.getGuild(), loaded.getSubscriptionCount(), false, 8, null);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        updateViewState(obj);
    }

    @MainThread
    private final void handleGuildBoostingStarted() {
        Object obj;
        ViewState requireViewState = requireViewState();
        if (requireViewState instanceof ViewState.Uninitialized) {
            obj = new ViewState.Uninitialized(GuildBoostState.CALL_IN_PROGRESS);
        } else if (requireViewState instanceof ViewState.Loaded) {
            ViewState.Loaded loaded = (ViewState.Loaded) requireViewState;
            obj = new ViewState.Loaded(GuildBoostState.CALL_IN_PROGRESS, loaded.getGuild(), loaded.getSubscriptionCount(), false, 8, null);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        updateViewState(obj);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        int i;
        if (storeState.getGuildBoostState() instanceof StoreGuildBoost.State.Loaded) {
            Collection<ModelGuildBoostSlot> values = ((StoreGuildBoost.State.Loaded) storeState.getGuildBoostState()).getBoostSlotMap().values();
            ArrayList arrayList = new ArrayList();
            for (Object obj : values) {
                ModelAppliedGuildBoost premiumGuildSubscription = ((ModelGuildBoostSlot) obj).getPremiumGuildSubscription();
                Long l = null;
                Long valueOf = premiumGuildSubscription != null ? Long.valueOf(premiumGuildSubscription.getGuildId()) : null;
                Guild guild = storeState.getGuild();
                if (guild != null) {
                    l = Long.valueOf(guild.getId());
                }
                if (m.areEqual(valueOf, l)) {
                    arrayList.add(obj);
                }
            }
            i = arrayList.size();
        } else {
            i = 0;
        }
        updateViewState(new ViewState.Loaded(requireViewState().getGuildBoostState(), storeState.getGuild(), i, false, 8, null));
    }

    public final StoreGuildBoost getStoreGuildBoost() {
        return this.storeGuildBoost;
    }

    @Override // com.discord.app.AppViewModel, androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        Subscription subscription = this.guildBoostSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    @MainThread
    public final void subscribeToGuildBoost(long j, long j2) {
        handleGuildBoostingStarted();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().subscribeToGuild(j, new RestAPIParams.GuildBoosting(d0.t.m.listOf(Long.valueOf(j2)))), false, 1, null), this, null, 2, null), StoreGuildBoost.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new GuildBoostInProgressViewModel$subscribeToGuildBoost$1(this), (r18 & 8) != 0 ? null : new GuildBoostInProgressViewModel$subscribeToGuildBoost$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildBoostInProgressViewModel$subscribeToGuildBoost$3(this));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildBoostInProgressViewModel(long j, StoreGuildBoost storeGuildBoost, Observable<StoreState> observable) {
        super(new ViewState.Uninitialized(GuildBoostState.NOT_IN_PROGRESS));
        m.checkNotNullParameter(storeGuildBoost, "storeGuildBoost");
        m.checkNotNullParameter(observable, "storeObservable");
        this.guildId = j;
        this.storeGuildBoost = storeGuildBoost;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), GuildBoostInProgressViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
