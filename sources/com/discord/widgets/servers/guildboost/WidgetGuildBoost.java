package com.discord.widgets.servers.guildboost;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.activity.result.ActivityResultLauncher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import b.a.d.f;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetServerBoostStatusBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.billing.GooglePlayBillingManager;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.premium.GuildBoostUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.simple_pager.SimplePager;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.servers.WidgetServerSettingsChannels;
import com.discord.widgets.servers.guildboost.GuildBoostViewModel;
import com.discord.widgets.settings.premium.WidgetChoosePlan;
import d0.g;
import d0.o;
import d0.t.g0;
import d0.t.n;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGuildBoost.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 B2\u00020\u0001:\u0002BCB\u0007¢\u0006\u0004\bA\u0010\u0019J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000e\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0010\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0010\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001e\u0010\u0019R\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!R\u001e\u0010$\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010#0\"8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b$\u0010%R\u001d\u0010+\u001a\u00020&8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001c\u0010-\u001a\b\u0012\u0004\u0012\u00020,0\"8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b-\u0010%R\u001c\u00100\u001a\b\u0012\u0004\u0012\u00020/0.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b3\u00104R\u001d\u0010:\u001a\u0002058B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b6\u00107\u001a\u0004\b8\u00109R!\u0010@\u001a\u00060;j\u0002`<8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b=\u00107\u001a\u0004\b>\u0010?¨\u0006D"}, d2 = {"Lcom/discord/widgets/servers/guildboost/WidgetGuildBoost;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/guildboost/GuildBoostViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/servers/guildboost/GuildBoostViewModel$ViewState;)V", "", "guildName", "configureToolbar", "(Ljava/lang/String;)V", "", "premiumTier", "subscriptionCount", "configureProgressBar", "(II)V", "configureViewpager", "currentSelected", "configureLevelBubbles", "(I)V", "Lcom/discord/widgets/servers/guildboost/GuildBoostViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/servers/guildboost/GuildBoostViewModel$Event;)V", "fetchData", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "", "wasPagerPageSet", "Z", "", "Landroid/widget/TextView;", "levelText", "Ljava/util/List;", "Lcom/discord/databinding/WidgetServerBoostStatusBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerBoostStatusBinding;", "binding", "Landroid/widget/ImageView;", "levelBackgrounds", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "choosePlanLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/widgets/servers/guildboost/WidgetGuildBoost$PerksPagerAdapter;", "pagerAdapter", "Lcom/discord/widgets/servers/guildboost/WidgetGuildBoost$PerksPagerAdapter;", "Lcom/discord/widgets/servers/guildboost/GuildBoostViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/servers/guildboost/GuildBoostViewModel;", "viewModel", "", "Lcom/discord/primitives/GuildId;", "guildId$delegate", "getGuildId", "()J", "guildId", HookHelper.constructorName, "Companion", "PerksPagerAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildBoost extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildBoost.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerBoostStatusBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "GUILD_ID";
    private static final int VIEW_INDEX_LOADED = 2;
    private static final int VIEW_INDEX_LOADING = 0;
    private static final int VIEW_INDEX_LOAD_FAILED = 1;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildBoost$binding$2.INSTANCE, null, 2, null);
    private final ActivityResultLauncher<Intent> choosePlanLauncher = WidgetChoosePlan.Companion.registerForResult(this, new WidgetGuildBoost$choosePlanLauncher$1(this));
    private final Lazy guildId$delegate = g.lazy(new WidgetGuildBoost$guildId$2(this));
    private List<? extends ImageView> levelBackgrounds;
    private List<? extends TextView> levelText;
    private PerksPagerAdapter pagerAdapter;
    private final Lazy viewModel$delegate;
    private boolean wasPagerPageSet;

    /* compiled from: WidgetGuildBoost.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J#\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000f¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/servers/guildboost/WidgetGuildBoost$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "create", "(Landroid/content/Context;J)V", "", WidgetServerSettingsChannels.INTENT_EXTRA_GUILD_ID, "Ljava/lang/String;", "", "VIEW_INDEX_LOADED", "I", "VIEW_INDEX_LOADING", "VIEW_INDEX_LOAD_FAILED", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra(WidgetGuildBoost.INTENT_EXTRA_GUILD_ID, j);
            m.checkNotNullExpressionValue(putExtra, "Intent().putExtra(INTENT_EXTRA_GUILD_ID, guildId)");
            j.d(context, WidgetGuildBoost.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetGuildBoost.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\b\b\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\"\u001a\u00060 j\u0002`!¢\u0006\u0004\b(\u0010'J\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\r\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u0015\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\"\u0010\u0018\u001a\u00020\u00048\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u0010\"\u0004\b\u001b\u0010\u001cR\"\u0010\u001d\u001a\u00020\u00048\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u0019\u001a\u0004\b\u001e\u0010\u0010\"\u0004\b\u001f\u0010\u001cR&\u0010\"\u001a\u00060 j\u0002`!8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%\"\u0004\b&\u0010'¨\u0006)"}, d2 = {"Lcom/discord/widgets/servers/guildboost/WidgetGuildBoost$PerksPagerAdapter;", "Landroidx/viewpager/widget/PagerAdapter;", "Landroid/view/ViewGroup;", "container", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Landroid/view/View;", "instantiateItem", "(Landroid/view/ViewGroup;I)Landroid/view/View;", "view", "", "any", "", "isViewFromObject", "(Landroid/view/View;Ljava/lang/Object;)Z", "getCount", "()I", "", "destroyItem", "(Landroid/view/ViewGroup;ILjava/lang/Object;)V", "Landroidx/viewpager/widget/ViewPager;", "viewPager", "configureViews", "(Landroidx/viewpager/widget/ViewPager;)V", "premiumTier", "I", "getPremiumTier", "setPremiumTier", "(I)V", "subscriptionCount", "getSubscriptionCount", "setSubscriptionCount", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", "getGuildId", "()J", "setGuildId", "(J)V", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class PerksPagerAdapter extends PagerAdapter {
        private long guildId;
        private int premiumTier;
        private int subscriptionCount;

        public PerksPagerAdapter(long j) {
            this.guildId = j;
        }

        public final void configureViews(ViewPager viewPager) {
            m.checkNotNullParameter(viewPager, "viewPager");
            int childCount = viewPager.getChildCount();
            if (childCount > 0) {
                int i = 0;
                while (true) {
                    int i2 = i + 1;
                    View childAt = viewPager.getChildAt(i);
                    m.checkNotNullExpressionValue(childAt, "getChildAt(index)");
                    Object tag = childAt.getTag();
                    Objects.requireNonNull(tag, "null cannot be cast to non-null type kotlin.Int");
                    ((GuildBoostPerkView) childAt).configure(((Integer) tag).intValue(), this.premiumTier, this.guildId);
                    if (i2 < childCount) {
                        i = i2;
                    } else {
                        return;
                    }
                }
            }
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            m.checkNotNullParameter(viewGroup, "container");
            m.checkNotNullParameter(obj, "view");
            viewGroup.removeView((View) obj);
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public int getCount() {
            return 3;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final int getPremiumTier() {
            return this.premiumTier;
        }

        public final int getSubscriptionCount() {
            return this.subscriptionCount;
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public boolean isViewFromObject(View view, Object obj) {
            m.checkNotNullParameter(view, "view");
            m.checkNotNullParameter(obj, "any");
            return m.areEqual(view, obj);
        }

        public final void setGuildId(long j) {
            this.guildId = j;
        }

        public final void setPremiumTier(int i) {
            this.premiumTier = i;
        }

        public final void setSubscriptionCount(int i) {
            this.subscriptionCount = i;
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public View instantiateItem(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "container");
            int i2 = i + 1;
            Context context = viewGroup.getContext();
            m.checkNotNullExpressionValue(context, "container.context");
            GuildBoostPerkView guildBoostPerkView = new GuildBoostPerkView(context, null, 0, 6, null);
            guildBoostPerkView.configure(i2, this.premiumTier, this.guildId);
            guildBoostPerkView.setTag(Integer.valueOf(i2));
            viewGroup.addView(guildBoostPerkView);
            return guildBoostPerkView;
        }
    }

    public WidgetGuildBoost() {
        super(R.layout.widget_server_boost_status);
        WidgetGuildBoost$viewModel$2 widgetGuildBoost$viewModel$2 = new WidgetGuildBoost$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildBoostViewModel.class), new WidgetGuildBoost$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildBoost$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureLevelBubbles(int i) {
        int i2;
        int i3 = 0;
        while (i3 <= 3) {
            List<? extends ImageView> list = this.levelBackgrounds;
            if (list == null) {
                m.throwUninitializedPropertyAccessException("levelBackgrounds");
            }
            ImageView imageView = list.get(i3);
            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            int i4 = 32;
            layoutParams.height = DimenUtils.dpToPixels(i == i3 ? 32 : 20);
            if (i != i3) {
                i4 = 20;
            }
            layoutParams.width = DimenUtils.dpToPixels(i4);
            imageView.setLayoutParams(layoutParams);
            List<? extends TextView> list2 = this.levelText;
            if (list2 == null) {
                m.throwUninitializedPropertyAccessException("levelText");
            }
            TextView textView = list2.get(i3);
            if (textView != null) {
                if (i == i3) {
                    i2 = ColorCompat.getThemedColor(textView, (int) R.attr.primary_000);
                } else {
                    i2 = ColorCompat.getThemedColor(textView, (int) R.attr.primary_400);
                }
                textView.setTextColor(i2);
            }
            i3++;
        }
    }

    private final void configureProgressBar(int i, int i2) {
        ProgressBar progressBar = getBinding().l.f193b;
        m.checkNotNullExpressionValue(progressBar, "binding.progress.boostBarProgressBar");
        progressBar.setProgress(GuildBoostUtils.INSTANCE.calculateTotalProgress(i, i2));
        ProgressBar progressBar2 = getBinding().l.f193b;
        m.checkNotNullExpressionValue(progressBar2, "binding.progress.boostBarProgressBar");
        boolean z2 = false;
        progressBar2.setContentDescription(getString(i != 0 ? i != 1 ? i != 2 ? i != 3 ? 0 : R.string.premium_guild_tier_3 : R.string.premium_guild_tier_2 : R.string.premium_guild_tier_1 : R.string.premium_guild_header_badge_no_tier));
        ImageView imageView = getBinding().l.c;
        m.checkNotNullExpressionValue(imageView, "binding.progress.boostBarTier0Iv");
        imageView.setEnabled(i2 > 0);
        ImageView imageView2 = getBinding().l.d;
        m.checkNotNullExpressionValue(imageView2, "binding.progress.boostBarTier1Iv");
        imageView2.setEnabled(i >= 1);
        ImageView imageView3 = getBinding().l.f;
        m.checkNotNullExpressionValue(imageView3, "binding.progress.boostBarTier2Iv");
        imageView3.setEnabled(i >= 2);
        ImageView imageView4 = getBinding().l.h;
        m.checkNotNullExpressionValue(imageView4, "binding.progress.boostBarTier3Iv");
        if (i >= 3) {
            z2 = true;
        }
        imageView4.setEnabled(z2);
    }

    private final void configureToolbar(String str) {
        setActionBarTitle(R.string.premium_guild_perks_modal_header);
        setActionBarSubtitle(str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(GuildBoostViewModel.ViewState viewState) {
        if (viewState instanceof GuildBoostViewModel.ViewState.Loading) {
            AppViewFlipper appViewFlipper = getBinding().f2511b;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.boostStatusFlipper");
            appViewFlipper.setDisplayedChild(0);
        } else if (viewState instanceof GuildBoostViewModel.ViewState.Failure) {
            AppViewFlipper appViewFlipper2 = getBinding().f2511b;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.boostStatusFlipper");
            appViewFlipper2.setDisplayedChild(1);
            getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.guildboost.WidgetGuildBoost$configureUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GuildBoostViewModel viewModel;
                    viewModel = WidgetGuildBoost.this.getViewModel();
                    viewModel.retryClicked();
                }
            });
        } else {
            if (viewState instanceof GuildBoostViewModel.ViewState.Loaded) {
                AppViewFlipper appViewFlipper3 = getBinding().f2511b;
                m.checkNotNullExpressionValue(appViewFlipper3, "binding.boostStatusFlipper");
                appViewFlipper3.setDisplayedChild(2);
            }
            GuildBoostViewModel.ViewState.Loaded loaded = (GuildBoostViewModel.ViewState.Loaded) viewState;
            int premiumTier = loaded.getGuild().getPremiumTier();
            int premiumSubscriptionCount = loaded.getGuild().getPremiumSubscriptionCount();
            configureToolbar(loaded.getGuild().getName());
            configureProgressBar(premiumTier, premiumSubscriptionCount);
            configureViewpager(premiumTier, premiumSubscriptionCount);
            getBinding().e.a(loaded.getMeUser().getPremiumTier(), new WidgetGuildBoost$configureUI$2(this));
            getBinding().f.a(loaded.getMeUser().getPremiumTier(), false);
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.boostStatusNumBoosts");
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            textView.setText(StringResourceUtilsKt.getQuantityString(resources, requireContext(), (int) R.plurals.premium_guild_perks_modal_header_subscription_count_subscriptions, premiumSubscriptionCount, Integer.valueOf(premiumSubscriptionCount)));
        }
    }

    private final void configureViewpager(int i, int i2) {
        PerksPagerAdapter perksPagerAdapter = this.pagerAdapter;
        if (perksPagerAdapter == null) {
            m.throwUninitializedPropertyAccessException("pagerAdapter");
        }
        perksPagerAdapter.setPremiumTier(i);
        PerksPagerAdapter perksPagerAdapter2 = this.pagerAdapter;
        if (perksPagerAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("pagerAdapter");
        }
        perksPagerAdapter2.setSubscriptionCount(i2);
        PerksPagerAdapter perksPagerAdapter3 = this.pagerAdapter;
        if (perksPagerAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("pagerAdapter");
        }
        SimplePager simplePager = getBinding().k;
        m.checkNotNullExpressionValue(simplePager, "binding.boostStatusViewpager");
        perksPagerAdapter3.configureViews(simplePager);
        if (!this.wasPagerPageSet) {
            getBinding().k.setCurrentItem(Math.max(0, i - 1), false);
            configureLevelBubbles(Math.max(1, i));
            this.wasPagerPageSet = true;
        }
    }

    public static final void create(Context context, long j) {
        Companion.create(context, j);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void fetchData() {
        StoreStream.Companion companion = StoreStream.Companion;
        companion.getGuildBoosts().fetchUserGuildBoostState();
        companion.getSubscriptions().fetchSubscriptions();
        GooglePlayBillingManager.INSTANCE.queryPurchases();
    }

    private final WidgetServerBoostStatusBinding getBinding() {
        return (WidgetServerBoostStatusBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildBoostViewModel getViewModel() {
        return (GuildBoostViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(GuildBoostViewModel.Event event) {
        CharSequence e;
        if (event instanceof GuildBoostViewModel.Event.LaunchSubscriptionConfirmation) {
            GuildBoostViewModel.Event.LaunchSubscriptionConfirmation launchSubscriptionConfirmation = (GuildBoostViewModel.Event.LaunchSubscriptionConfirmation) event;
            WidgetGuildBoostConfirmation.Companion.create(requireContext(), launchSubscriptionConfirmation.getGuildId(), launchSubscriptionConfirmation.getSlotId());
        } else if (event instanceof GuildBoostViewModel.Event.LaunchPurchaseSubscription) {
            GuildBoostViewModel.Event.LaunchPurchaseSubscription launchPurchaseSubscription = (GuildBoostViewModel.Event.LaunchPurchaseSubscription) event;
            WidgetChoosePlan.Companion.launch(requireContext(), (r16 & 2) != 0 ? null : this.choosePlanLauncher, WidgetChoosePlan.ViewType.BUY_PREMIUM_GUILD, (r16 & 8) != 0 ? null : launchPurchaseSubscription.getOldSkuName(), new Traits.Location("User-Facing Premium Guild Subscription Fullscreen Modal", launchPurchaseSubscription.getSection(), Traits.Location.Obj.BUTTON_CTA, "buy", null, 16, null), (r16 & 32) != 0 ? null : null);
        } else if (event instanceof GuildBoostViewModel.Event.ShowDesktopAlertDialog) {
            WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            String string = getString(R.string.premium_guild_subscription_out_of_slots_title);
            e = b.e(this, R.string.premium_guild_subscription_out_of_slots_purchase_on_desktop, new Object[]{f.a.a(360055386693L, null)}, (r4 & 4) != 0 ? b.a.j : null);
            WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, e, getString(R.string.premium_guild_subscription_header_subscribe_tooltip_close), null, null, null, null, null, null, null, null, 0, null, 16368, null);
        } else if (event instanceof GuildBoostViewModel.Event.UnacknowledgedPurchase) {
            b.a.d.m.i(this, R.string.billing_error_purchase, 0, 4);
            GooglePlayBillingManager.INSTANCE.queryPurchases();
        } else if (event instanceof GuildBoostViewModel.Event.ShowBlockedPlanSwitchAlertDialog) {
            WidgetNoticeDialog.Companion companion2 = WidgetNoticeDialog.Companion;
            FragmentManager parentFragmentManager2 = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
            GuildBoostViewModel.Event.ShowBlockedPlanSwitchAlertDialog showBlockedPlanSwitchAlertDialog = (GuildBoostViewModel.Event.ShowBlockedPlanSwitchAlertDialog) event;
            String string2 = getString(showBlockedPlanSwitchAlertDialog.getHeaderStringRes());
            String string3 = getString(showBlockedPlanSwitchAlertDialog.getBodyStringRes());
            m.checkNotNullExpressionValue(string3, "getString(event.bodyStringRes)");
            WidgetNoticeDialog.Companion.show$default(companion2, parentFragmentManager2, string2, string3, getString(R.string.billing_manage_subscription), getString(R.string.cancel), g0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), new WidgetGuildBoost$handleEvent$1(this))), null, null, null, null, null, null, 0, null, 16320, null);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        this.pagerAdapter = new PerksPagerAdapter(getGuildId());
        getBinding().k.setWrapHeight(true);
        SimplePager simplePager = getBinding().k;
        m.checkNotNullExpressionValue(simplePager, "binding.boostStatusViewpager");
        PerksPagerAdapter perksPagerAdapter = this.pagerAdapter;
        if (perksPagerAdapter == null) {
            m.throwUninitializedPropertyAccessException("pagerAdapter");
        }
        simplePager.setAdapter(perksPagerAdapter);
        getBinding().k.addOnPageChangeListener(new ViewPager.OnPageChangeListener() { // from class: com.discord.widgets.servers.guildboost.WidgetGuildBoost$onViewBound$1
            @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
            public void onPageScrollStateChanged(int i) {
            }

            @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
            public void onPageScrolled(int i, float f, int i2) {
            }

            @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
            public void onPageSelected(int i) {
                WidgetGuildBoost.this.configureLevelBubbles(i + 1);
            }
        });
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_premium_guild, WidgetGuildBoost$onViewBound$2.INSTANCE, null, 4, null);
        LinkifiedTextView linkifiedTextView = getBinding().c;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.boostStatusLearnMore");
        b.m(linkifiedTextView, R.string.premium_guild_perks_modal_blurb_mobile_learn_more, new Object[]{"learnMode"}, WidgetGuildBoost$onViewBound$3.INSTANCE);
        CharSequence i18nPluralString = StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_perks_modal_protip_mobile_numFreeGuildSubscriptions, 2, 2);
        LinkifiedTextView linkifiedTextView2 = getBinding().g;
        m.checkNotNullExpressionValue(linkifiedTextView2, "binding.boostStatusProtip");
        b.m(linkifiedTextView2, R.string.premium_guild_perks_modal_protip, new Object[]{i18nPluralString}, new WidgetGuildBoost$onViewBound$4(this));
        ImageView imageView = getBinding().l.c;
        m.checkNotNullExpressionValue(imageView, "binding.progress.boostBarTier0Iv");
        ImageView imageView2 = getBinding().l.d;
        m.checkNotNullExpressionValue(imageView2, "binding.progress.boostBarTier1Iv");
        ImageView imageView3 = getBinding().l.f;
        m.checkNotNullExpressionValue(imageView3, "binding.progress.boostBarTier2Iv");
        ImageView imageView4 = getBinding().l.h;
        m.checkNotNullExpressionValue(imageView4, "binding.progress.boostBarTier3Iv");
        this.levelBackgrounds = n.listOf((Object[]) new ImageView[]{imageView, imageView2, imageView3, imageView4});
        this.levelText = n.listOf((Object[]) new TextView[]{null, getBinding().l.e, getBinding().l.g, getBinding().l.i});
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.guildboost.WidgetGuildBoost$onViewBound$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GuildBoostViewModel viewModel;
                viewModel = WidgetGuildBoost.this.getViewModel();
                viewModel.subscribeClicked(Traits.Location.Section.HEADER);
            }
        });
        getBinding().j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.guildboost.WidgetGuildBoost$onViewBound$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GuildBoostViewModel viewModel;
                viewModel = WidgetGuildBoost.this.getViewModel();
                viewModel.subscribeClicked(Traits.Location.Section.FOOTER);
            }
        });
        fetchData();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        AppActivity appActivity;
        super.onViewBoundOrOnResume();
        if ((getGuildId() == 0 || getGuildId() == -1) && (appActivity = getAppActivity()) != null) {
            appActivity.finish();
        }
        Observable<GuildBoostViewModel.ViewState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetGuildBoost.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildBoost$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetGuildBoost.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildBoost$onViewBoundOrOnResume$2(this));
    }
}
