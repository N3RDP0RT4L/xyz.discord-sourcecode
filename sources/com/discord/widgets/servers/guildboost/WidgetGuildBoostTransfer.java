package com.discord.widgets.servers.guildboost;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.a.c;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetGuildBoostTransferBinding;
import com.discord.models.domain.ModelAppliedGuildBoost;
import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.guildboost.GuildBoostTransferInProgressViewModel;
import com.google.android.material.button.MaterialButton;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGuildBoostTransfer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0007¢\u0006\u0004\b\u0019\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/servers/guildboost/WidgetGuildBoostTransfer;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;", "state", "", "configureUI", "(Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetGuildBoostTransferBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildBoostTransferBinding;", "binding", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildBoostTransfer extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildBoostTransfer.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildBoostTransferBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_PREVIOUS_GUILD_ID = "PREVIOUS_GUILD_ID";
    private static final String INTENT_EXTRA_SLOT_ID = "SLOT_ID";
    private static final String INTENT_EXTRA_SUBSCRIPTION_ID = "SUBSCRIPTION_ID";
    private static final String INTENT_EXTRA_TARGET_GUILD_ID = "TARGET_GUILD_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildBoostTransfer$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetGuildBoostTransfer.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J;\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u0007\u001a\u00060\u0004j\u0002`\u00052\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\bH\u0007¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000fR\u0016\u0010\u0012\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u000f¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/servers/guildboost/WidgetGuildBoostTransfer$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "previousGuildId", "targetGuildId", "Lcom/discord/models/domain/ModelGuildBoostSlot;", "slot", "", "create", "(Landroid/content/Context;JJLcom/discord/models/domain/ModelGuildBoostSlot;)V", "", "INTENT_EXTRA_PREVIOUS_GUILD_ID", "Ljava/lang/String;", "INTENT_EXTRA_SLOT_ID", "INTENT_EXTRA_SUBSCRIPTION_ID", "INTENT_EXTRA_TARGET_GUILD_ID", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void create$default(Companion companion, Context context, long j, long j2, ModelGuildBoostSlot modelGuildBoostSlot, int i, Object obj) {
            if ((i & 8) != 0) {
                modelGuildBoostSlot = null;
            }
            companion.create(context, j, j2, modelGuildBoostSlot);
        }

        public final void create(Context context, long j, long j2, ModelGuildBoostSlot modelGuildBoostSlot) {
            ModelAppliedGuildBoost premiumGuildSubscription;
            m.checkNotNullParameter(context, "context");
            Long l = null;
            Intent putExtra = new Intent().putExtra(WidgetGuildBoostTransfer.INTENT_EXTRA_PREVIOUS_GUILD_ID, j).putExtra(WidgetGuildBoostTransfer.INTENT_EXTRA_TARGET_GUILD_ID, j2).putExtra(WidgetGuildBoostTransfer.INTENT_EXTRA_SLOT_ID, modelGuildBoostSlot != null ? Long.valueOf(modelGuildBoostSlot.getId()) : null);
            if (!(modelGuildBoostSlot == null || (premiumGuildSubscription = modelGuildBoostSlot.getPremiumGuildSubscription()) == null)) {
                l = Long.valueOf(premiumGuildSubscription.getId());
            }
            Intent putExtra2 = putExtra.putExtra(WidgetGuildBoostTransfer.INTENT_EXTRA_SUBSCRIPTION_ID, l);
            m.checkNotNullExpressionValue(putExtra2, "Intent()\n          .putE…iumGuildSubscription?.id)");
            j.d(context, WidgetGuildBoostTransfer.class, putExtra2);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetGuildBoostTransfer() {
        super(R.layout.widget_guild_boost_transfer);
        WidgetGuildBoostTransfer$viewModel$2 widgetGuildBoostTransfer$viewModel$2 = new WidgetGuildBoostTransfer$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildBoostTransferInProgressViewModel.class), new WidgetGuildBoostTransfer$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildBoostTransfer$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(GuildBoostTransferInProgressViewModel.ViewState viewState) {
        if (viewState instanceof GuildBoostTransferInProgressViewModel.ViewState.ErrorLoading) {
            FragmentActivity activity = e();
            if (activity != null) {
                activity.finish();
            }
        } else if (viewState instanceof GuildBoostTransferInProgressViewModel.ViewState.Loading) {
            DimmerView.setDimmed$default(getBinding().f2386b, true, false, 2, null);
            TextView textView = getBinding().d;
            m.checkNotNullExpressionValue(textView, "binding.guildBoostTransferError");
            textView.setVisibility(4);
        } else if (viewState instanceof GuildBoostTransferInProgressViewModel.ViewState.ErrorTransfer) {
            DimmerView.setDimmed$default(getBinding().f2386b, false, false, 2, null);
            TextView textView2 = getBinding().d;
            m.checkNotNullExpressionValue(textView2, "binding.guildBoostTransferError");
            textView2.setVisibility(0);
        } else if (viewState instanceof GuildBoostTransferInProgressViewModel.ViewState.PreTransfer) {
            GuildBoostTransferInProgressViewModel.ViewState.PreTransfer preTransfer = (GuildBoostTransferInProgressViewModel.ViewState.PreTransfer) viewState;
            DimmerView.setDimmed$default(getBinding().f2386b, preTransfer.isTransferInProgress(), false, 2, null);
            TextView textView3 = getBinding().d;
            m.checkNotNullExpressionValue(textView3, "binding.guildBoostTransferError");
            textView3.setVisibility(4);
            getBinding().e.b(preTransfer.getPreviousGuild(), -1);
            getBinding().h.b(preTransfer.getTargetGuild(), 1);
        } else if (viewState instanceof GuildBoostTransferInProgressViewModel.ViewState.PostTransfer) {
            c.a aVar = c.l;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            GuildBoostTransferInProgressViewModel.ViewState.PostTransfer postTransfer = (GuildBoostTransferInProgressViewModel.ViewState.PostTransfer) viewState;
            aVar.a(parentFragmentManager, requireContext(), postTransfer.getTargetGuild().getName(), postTransfer.getTargetGuildSubscriptionCount() + 1, true, new WidgetGuildBoostTransfer$configureUI$1(this));
        }
    }

    public static final void create(Context context, long j, long j2, ModelGuildBoostSlot modelGuildBoostSlot) {
        Companion.create(context, j, j2, modelGuildBoostSlot);
    }

    private final WidgetGuildBoostTransferBinding getBinding() {
        return (WidgetGuildBoostTransferBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildBoostTransferInProgressViewModel getViewModel() {
        return (GuildBoostTransferInProgressViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        CharSequence e;
        CharSequence e2;
        CharSequence e3;
        CharSequence e4;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.guildBoostTransferConfirmationBlurb");
        e = b.e(this, R.string.premium_guild_subscribe_confirm_transfer_blurb, new Object[]{StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_subscribe_confirm_transfer_blurb_slotCount, 1, 1), StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_subscribe_confirm_transfer_blurb_guildCount, 1, 1)}, (r4 & 4) != 0 ? b.a.j : null);
        textView.setText(e);
        TextView textView2 = getBinding().f;
        m.checkNotNullExpressionValue(textView2, "binding.guildBoostTransferPreviousGuildHeader");
        e2 = b.e(this, R.string.premium_guild_subscribe_confirm_transfer_from_guild, new Object[]{StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_subscribe_confirm_transfer_from_guild_guildCount, 1, 1)}, (r4 & 4) != 0 ? b.a.j : null);
        textView2.setText(e2);
        TextView textView3 = getBinding().i;
        m.checkNotNullExpressionValue(textView3, "binding.guildBoostTransferTargetGuildHeader");
        e3 = b.e(this, R.string.premium_guild_subscribe_confirm_transfer_to_guild, new Object[]{StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_subscribe_confirm_transfer_to_guild_slotCount, 1, 1)}, (r4 & 4) != 0 ? b.a.j : null);
        textView3.setText(e3);
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.guildboost.WidgetGuildBoostTransfer$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GuildBoostTransferInProgressViewModel viewModel;
                viewModel = WidgetGuildBoostTransfer.this.getViewModel();
                viewModel.transferGuildBoost();
            }
        });
        MaterialButton materialButton = getBinding().g;
        m.checkNotNullExpressionValue(materialButton, "binding.guildBoostTransferSelect");
        e4 = b.e(this, R.string.premium_guild_subscribe_transfer_confirm_confirmation, new Object[]{StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_subscribe_transfer_confirm_confirmation_slotCount, 1, 1)}, (r4 & 4) != 0 ? b.a.j : null);
        materialButton.setText(e4);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable q = ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, WidgetGuildBoostTransfer.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildBoostTransfer$onViewBoundOrOnResume$1(this));
    }
}
