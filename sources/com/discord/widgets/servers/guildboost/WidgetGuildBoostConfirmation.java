package com.discord.widgets.servers.guildboost;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.a.a.c;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetServerBoostConfirmationBinding;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.WidgetServerSettingsChannels;
import com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetGuildBoostConfirmation.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 (2\u00020\u0001:\u0001(B\u0007¢\u0006\u0004\b'\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\u0010R!\u0010\u0017\u001a\u00060\u0011j\u0002`\u00128B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001c\u001a\u00020\u00188B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u001a\u0010\u001bR!\u0010 \u001a\u00060\u0011j\u0002`\u001d8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u0014\u001a\u0004\b\u001f\u0010\u0016R\u001d\u0010&\u001a\u00020!8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%¨\u0006)"}, d2 = {"Lcom/discord/widgets/servers/guildboost/WidgetGuildBoostConfirmation;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState;", "viewModelViewState", "", "configureUI", "(Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel$ViewState;)V", "", "guildName", "configureToolbar", "(Ljava/lang/String;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onResume", "()V", "", "Lcom/discord/primitives/GuildBoostSlotId;", "slotId$delegate", "Lkotlin/Lazy;", "getSlotId", "()J", "slotId", "Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/servers/guildboost/GuildBoostInProgressViewModel;", "viewModel", "Lcom/discord/primitives/GuildId;", "guildId$delegate", "getGuildId", "guildId", "Lcom/discord/databinding/WidgetServerBoostConfirmationBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerBoostConfirmationBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildBoostConfirmation extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildBoostConfirmation.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerBoostConfirmationBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "GUILD_ID";
    private static final String INTENT_EXTRA_SLOT_ID = "SLOT_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildBoostConfirmation$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetGuildBoostConfirmation$guildId$2(this));
    private final Lazy slotId$delegate = g.lazy(new WidgetGuildBoostConfirmation$slotId$2(this));
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetGuildBoostConfirmation.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J/\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/servers/guildboost/WidgetGuildBoostConfirmation$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/GuildBoostSlotId;", "slotId", "", "create", "(Landroid/content/Context;JJ)V", "", WidgetServerSettingsChannels.INTENT_EXTRA_GUILD_ID, "Ljava/lang/String;", "INTENT_EXTRA_SLOT_ID", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j, long j2) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra(WidgetGuildBoostConfirmation.INTENT_EXTRA_GUILD_ID, j).putExtra(WidgetGuildBoostConfirmation.INTENT_EXTRA_SLOT_ID, j2);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…NT_EXTRA_SLOT_ID, slotId)");
            j.d(context, WidgetGuildBoostConfirmation.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            GuildBoostInProgressViewModel.GuildBoostState.values();
            int[] iArr = new int[4];
            $EnumSwitchMapping$0 = iArr;
            iArr[GuildBoostInProgressViewModel.GuildBoostState.NOT_IN_PROGRESS.ordinal()] = 1;
            iArr[GuildBoostInProgressViewModel.GuildBoostState.CALL_IN_PROGRESS.ordinal()] = 2;
            iArr[GuildBoostInProgressViewModel.GuildBoostState.ERROR.ordinal()] = 3;
            iArr[GuildBoostInProgressViewModel.GuildBoostState.COMPLETED.ordinal()] = 4;
        }
    }

    public WidgetGuildBoostConfirmation() {
        super(R.layout.widget_server_boost_confirmation);
        WidgetGuildBoostConfirmation$viewModel$2 widgetGuildBoostConfirmation$viewModel$2 = new WidgetGuildBoostConfirmation$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildBoostInProgressViewModel.class), new WidgetGuildBoostConfirmation$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetGuildBoostConfirmation$viewModel$2));
    }

    private final void configureToolbar(String str) {
        setActionBarTitle(R.string.premium_guild_perks_modal_header);
        setActionBarSubtitle(str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(GuildBoostInProgressViewModel.ViewState viewState) {
        if (!(viewState instanceof GuildBoostInProgressViewModel.ViewState.Uninitialized)) {
            Objects.requireNonNull(viewState, "null cannot be cast to non-null type com.discord.widgets.servers.guildboost.GuildBoostInProgressViewModel.ViewState.Loaded");
            GuildBoostInProgressViewModel.ViewState.Loaded loaded = (GuildBoostInProgressViewModel.ViewState.Loaded) viewState;
            if (loaded.getGuild() == null) {
                requireActivity().finish();
                return;
            }
            configureToolbar(loaded.getGuild().getName());
            getBinding().f2510b.b(loaded.getGuild(), 1);
            int ordinal = loaded.getGuildBoostState().ordinal();
            if (ordinal == 0) {
                DimmerView.setDimmed$default(getBinding().f, false, false, 2, null);
                TextView textView = getBinding().d;
                m.checkNotNullExpressionValue(textView, "binding.boostConfirmationError");
                textView.setVisibility(4);
            } else if (ordinal == 1) {
                DimmerView.setDimmed$default(getBinding().f, true, false, 2, null);
                TextView textView2 = getBinding().d;
                m.checkNotNullExpressionValue(textView2, "binding.boostConfirmationError");
                textView2.setVisibility(4);
            } else if (ordinal != 2) {
                if (ordinal == 3) {
                    DimmerView.setDimmed$default(getBinding().f, false, false, 2, null);
                    TextView textView3 = getBinding().d;
                    m.checkNotNullExpressionValue(textView3, "binding.boostConfirmationError");
                    textView3.setVisibility(0);
                }
            } else if (loaded.getCanShowConfirmationDialog()) {
                c.a aVar = c.l;
                FragmentManager parentFragmentManager = getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                aVar.a(parentFragmentManager, requireContext(), loaded.getGuild().getName(), loaded.getSubscriptionCount() + 1, false, new WidgetGuildBoostConfirmation$configureUI$1(this));
            }
        }
    }

    public static final void create(Context context, long j, long j2) {
        Companion.create(context, j, j2);
    }

    private final WidgetServerBoostConfirmationBinding getBinding() {
        return (WidgetServerBoostConfirmationBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getSlotId() {
        return ((Number) this.slotId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildBoostInProgressViewModel getViewModel() {
        return (GuildBoostInProgressViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        AppActivity appActivity;
        super.onResume();
        if ((getGuildId() == 0 || getGuildId() == -1) && (appActivity = getAppActivity()) != null) {
            appActivity.finish();
        }
        Observable q = ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, WidgetGuildBoostConfirmation.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetGuildBoostConfirmation$onResume$1(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        CharSequence e;
        CharSequence e2;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.guildboost.WidgetGuildBoostConfirmation$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GuildBoostInProgressViewModel viewModel;
                long guildId;
                long slotId;
                viewModel = WidgetGuildBoostConfirmation.this.getViewModel();
                guildId = WidgetGuildBoostConfirmation.this.getGuildId();
                slotId = WidgetGuildBoostConfirmation.this.getSlotId();
                viewModel.subscribeToGuildBoost(guildId, slotId);
            }
        });
        MaterialButton materialButton = getBinding().e;
        m.checkNotNullExpressionValue(materialButton, "binding.boostConfirmationSelect");
        e = b.e(this, R.string.premium_guild_subscribe_confirm_confirmation, new Object[]{StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_subscribe_confirm_confirmation_slotCount, 1, 1)}, (r4 & 4) != 0 ? b.a.j : null);
        materialButton.setText(e);
        CharSequence i18nPluralString = StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_subscribe_confirm_cooldown_warning_days, 7, 7);
        CharSequence i18nPluralString2 = StringResourceUtilsKt.getI18nPluralString(requireContext(), R.plurals.premium_guild_subscribe_confirm_cooldown_warning_slotCount, 1, 1);
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.boostConfirmationCooldownWarning");
        e2 = b.e(this, R.string.premium_guild_subscribe_confirm_cooldown_warning, new Object[]{i18nPluralString2, i18nPluralString}, (r4 & 4) != 0 ? b.a.j : null);
        textView.setText(e2);
    }
}
