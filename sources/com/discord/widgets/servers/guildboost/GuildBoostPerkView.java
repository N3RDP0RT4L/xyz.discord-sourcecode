package com.discord.widgets.servers.guildboost;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.databinding.ViewNitroBoostPerksBinding;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.guilds.GuildUtilsKt;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.threads.ThreadUtils;
import com.discord.widgets.servers.guildboost.GuildBoostPerkViewAdapter;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import f0.e0.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildBoostPerkView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0013\b\u0016\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013¢\u0006\u0004\b\u0015\u0010\u0016B\u001f\b\u0016\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017¢\u0006\u0004\b\u0015\u0010\u0019B'\b\u0016\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0017\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0005¢\u0006\u0004\b\u0015\u0010\u001bJ\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J)\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00052\n\u0010\n\u001a\u00060\bj\u0002`\t¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostPerkView;", "Landroid/widget/RelativeLayout;", "", "initialize", "()V", "", "premiumTier", "currentPremiumTier", "", "Lcom/discord/primitives/GuildId;", "guildId", "configure", "(IIJ)V", "Lcom/discord/databinding/ViewNitroBoostPerksBinding;", "binding", "Lcom/discord/databinding/ViewNitroBoostPerksBinding;", "Lcom/discord/widgets/servers/guildboost/GuildBoostPerkViewAdapter;", "adapter", "Lcom/discord/widgets/servers/guildboost/GuildBoostPerkViewAdapter;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostPerkView extends RelativeLayout {
    private GuildBoostPerkViewAdapter adapter;
    private final ViewNitroBoostPerksBinding binding;

    public GuildBoostPerkView(Context context) {
        super(context);
        ViewNitroBoostPerksBinding a = ViewNitroBoostPerksBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "ViewNitroBoostPerksBindi…rom(context), this, true)");
        this.binding = a;
    }

    private final void initialize() {
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = this.binding.c;
        m.checkNotNullExpressionValue(recyclerView, "binding.perksLevelContentsRecycler");
        this.adapter = (GuildBoostPerkViewAdapter) companion.configure(new GuildBoostPerkViewAdapter(recyclerView));
        this.binding.c.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() { // from class: com.discord.widgets.servers.guildboost.GuildBoostPerkView$initialize$1
            @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
            public boolean onInterceptTouchEvent(RecyclerView recyclerView2, MotionEvent motionEvent) {
                m.checkNotNullParameter(recyclerView2, "rv");
                m.checkNotNullParameter(motionEvent, "e");
                return motionEvent.getAction() == 2;
            }

            @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
            public void onRequestDisallowInterceptTouchEvent(boolean z2) {
            }

            @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
            public void onTouchEvent(RecyclerView recyclerView2, MotionEvent motionEvent) {
                m.checkNotNullParameter(recyclerView2, "rv");
                m.checkNotNullParameter(motionEvent, "e");
            }
        });
    }

    public final void configure(int i, int i2, long j) {
        int i3;
        List list;
        CharSequence d;
        CharSequence d2;
        CharSequence d3;
        CharSequence d4;
        CharSequence d5;
        CharSequence d6;
        CharSequence d7;
        CharSequence d8;
        CharSequence d9;
        CharSequence d10;
        CharSequence d11;
        CharSequence d12;
        CharSequence d13;
        CharSequence d14;
        GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem guildBoostPerkViewListItem;
        GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem guildBoostPerkViewListItem2;
        CharSequence d15;
        CharSequence d16;
        CharSequence d17;
        CharSequence d18;
        CharSequence d19;
        CharSequence d20;
        CharSequence d21;
        CharSequence d22;
        CharSequence d23;
        CharSequence d24;
        GuildBoostPerkView$configure$1 guildBoostPerkView$configure$1 = GuildBoostPerkView$configure$1.INSTANCE;
        boolean z2 = i2 >= i;
        if (z2) {
            this.binding.d.setBackgroundResource(R.drawable.drawable_bg_premium_guild_gradient);
        } else {
            this.binding.d.setBackgroundColor(ColorCompat.getThemedColor(this, (int) R.attr.primary_700));
        }
        int i4 = i != 1 ? i != 2 ? i != 3 ? 0 : 14 : 7 : 2;
        TextView textView = this.binding.f;
        m.checkNotNullExpressionValue(textView, "binding.perksLevelHeaderText");
        GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem guildBoostPerkViewListItem3 = null;
        b.m(textView, i != 1 ? i != 2 ? i != 3 ? 0 : R.string.guild_settings_guild_premium_perks_title_tier_3 : R.string.guild_settings_guild_premium_perks_title_tier_2 : R.string.guild_settings_guild_premium_perks_title_tier_1, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        int i5 = i != 1 ? i != 2 ? i != 3 ? 0 : z2 ? R.drawable.ic_perk_tier_3_boosted : R.drawable.ic_perk_tier_3_unboosted : z2 ? R.drawable.ic_perk_tier_2_boosted : R.drawable.ic_perk_tier_2_unboosted : z2 ? R.drawable.ic_perk_tier_1_boosted : R.drawable.ic_perk_tier_1_unboosted;
        TextView textView2 = this.binding.f;
        m.checkNotNullExpressionValue(textView2, "binding.perksLevelHeaderText");
        DrawableCompat.setCompoundDrawablesCompat$default(textView2, i5, 0, 0, 0, 14, (Object) null);
        TextView textView3 = this.binding.f;
        if (z2) {
            i3 = ColorCompat.getColor(getContext(), (int) R.color.white);
        } else {
            i3 = ColorCompat.getThemedColor(getContext(), (int) R.attr.primary_300);
        }
        textView3.setTextColor(i3);
        TextView textView4 = this.binding.e;
        m.checkNotNullExpressionValue(textView4, "binding.perksLevelHeaderBoosts");
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        textView4.setText(StringResourceUtilsKt.getQuantityString(resources, context, (int) R.plurals.guild_settings_premium_guild_tier_requirement_required, i4, Integer.valueOf(i4)));
        TextView textView5 = this.binding.e;
        m.checkNotNullExpressionValue(textView5, "binding.perksLevelHeaderBoosts");
        textView5.setVisibility(z2 ^ true ? 0 : 8);
        CardView cardView = this.binding.g;
        m.checkNotNullExpressionValue(cardView, "binding.perksLevelHeaderUnlocked");
        cardView.setVisibility(z2 ? 0 : 8);
        TextView textView6 = this.binding.f2188b;
        m.checkNotNullExpressionValue(textView6, "binding.perksLevelContentsHeader");
        b.m(textView6, i != 1 ? R.string.guild_settings_guild_premium_perks_previous_perks : R.string.guild_settings_guild_premium_perks_base_perks, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        boolean isThreadsEnabled = ThreadUtils.INSTANCE.isThreadsEnabled(j);
        if (i == 1) {
            GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem[] guildBoostPerkViewListItemArr = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem[7];
            int invoke = guildBoostPerkView$configure$1.invoke(R.drawable.ic_smile_24dp, z2);
            PremiumUtils premiumUtils = PremiumUtils.INSTANCE;
            d = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_emoji, new Object[]{50, Integer.valueOf(premiumUtils.getGuildEmojiMaxCount(1, false))}, (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr[0] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke, d);
            int invoke2 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_sticker_icon_24dp, z2);
            d2 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_sticker, new Object[]{15, Integer.valueOf(premiumUtils.getGuildStickerMaxCount(1, false))}, (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr[1] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke2, d2);
            int invoke3 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_headset_blue_24dp, z2);
            d3 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_audio_quality, new Object[]{Integer.valueOf(GuildUtilsKt.getMaxVoiceBitrateKbps(1, false))}, (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr[2] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke3, d3);
            int invoke4 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_nitro_gifs_24dp, z2);
            d4 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_1_animated_guild_icon, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr[3] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke4, d4);
            int invoke5 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_image_library_24dp, z2);
            d5 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_1_splash, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr[4] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke5, d5);
            int invoke6 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_guild_nitro_perk_stream_24dp, z2);
            d6 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_1_streaming, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr[5] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke6, d6);
            if (isThreadsEnabled) {
                int invoke7 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_clock_24dp, z2);
                d7 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_1_thread_archive, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                guildBoostPerkViewListItem3 = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke7, d7);
            }
            guildBoostPerkViewListItemArr[6] = guildBoostPerkViewListItem3;
            list = u.toMutableList((Collection) u.filterNotNull(n.mutableListOf(guildBoostPerkViewListItemArr)));
        } else if (i == 2) {
            GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem[] guildBoostPerkViewListItemArr2 = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem[9];
            int invoke8 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_smile_24dp, z2);
            PremiumUtils premiumUtils2 = PremiumUtils.INSTANCE;
            d8 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_emoji, new Object[]{50, Integer.valueOf(premiumUtils2.getGuildEmojiMaxCount(2, false))}, (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr2[0] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke8, d8);
            int invoke9 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_sticker_icon_24dp, z2);
            d9 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_sticker, new Object[]{15, Integer.valueOf(premiumUtils2.getGuildStickerMaxCount(2, false))}, (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr2[1] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke9, d9);
            int invoke10 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_headset_blue_24dp, z2);
            d10 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_audio_quality, new Object[]{Integer.valueOf(GuildUtilsKt.getMaxVoiceBitrateKbps(2, false))}, (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr2[2] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke10, d10);
            int invoke11 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_upload_24dp, z2);
            d11 = b.d(this, R.string.file_size_mb, new Object[]{Integer.valueOf(premiumUtils2.getGuildMaxFileSizeMB(2))}, (r4 & 4) != 0 ? b.c.j : null);
            d12 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_upload_limit, new Object[]{d11}, (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr2[3] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke11, d12);
            int invoke12 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_image_library_24dp, z2);
            d13 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_2_banner, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr2[4] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke12, d13);
            int invoke13 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_guild_nitro_perk_stream_24dp, z2);
            d14 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_2_streaming, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr2[5] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke13, d14);
            if (isThreadsEnabled) {
                int invoke14 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_clock_24dp, z2);
                d17 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_2_thread_archive, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                guildBoostPerkViewListItem = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke14, d17);
            } else {
                guildBoostPerkViewListItem = null;
            }
            guildBoostPerkViewListItemArr2[6] = guildBoostPerkViewListItem;
            if (isThreadsEnabled) {
                int invoke15 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_thread_locked, z2);
                d16 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_2_thread_private, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
                guildBoostPerkViewListItem2 = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke15, d16);
            } else {
                guildBoostPerkViewListItem2 = null;
            }
            guildBoostPerkViewListItemArr2[7] = guildBoostPerkViewListItem2;
            int invoke16 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_person_shield_purple_24dp, z2);
            d15 = b.d(this, R.string.premium_guild_guild_role_icons, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            guildBoostPerkViewListItemArr2[8] = new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke16, d15);
            list = u.toMutableList((Collection) u.filterNotNull(n.mutableListOf(guildBoostPerkViewListItemArr2)));
        } else if (i != 3) {
            list = new ArrayList();
        } else {
            int invoke17 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_smile_24dp, z2);
            PremiumUtils premiumUtils3 = PremiumUtils.INSTANCE;
            d18 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_emoji, new Object[]{100, Integer.valueOf(premiumUtils3.getGuildEmojiMaxCount(3, false))}, (r4 & 4) != 0 ? b.c.j : null);
            int invoke18 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_sticker_icon_24dp, z2);
            d19 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_sticker, new Object[]{30, Integer.valueOf(premiumUtils3.getGuildStickerMaxCount(3, false))}, (r4 & 4) != 0 ? b.c.j : null);
            int invoke19 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_headset_blue_24dp, z2);
            d20 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_audio_quality, new Object[]{Integer.valueOf(GuildUtilsKt.getMaxVoiceBitrateKbps(3, false))}, (r4 & 4) != 0 ? b.c.j : null);
            int invoke20 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_upload_24dp, z2);
            d21 = b.d(this, R.string.file_size_mb, new Object[]{Integer.valueOf(premiumUtils3.getGuildMaxFileSizeMB(3))}, (r4 & 4) != 0 ? b.c.j : null);
            d22 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_any_upload_limit, new Object[]{d21}, (r4 & 4) != 0 ? b.c.j : null);
            int invoke21 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_star_24dp, z2);
            d23 = b.d(this, R.string.guild_settings_guild_premium_perk_title_tier_3_vanity_url, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            int invoke22 = guildBoostPerkView$configure$1.invoke(R.drawable.ic_nitro_gifs_24dp, z2);
            d24 = b.d(this, R.string.guild_settings_guild_premium_perk_description_tier_3_animated_guild_banner, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
            list = n.mutableListOf(new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke17, d18), new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke18, d19), new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke19, d20), new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke20, d22), new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke21, d23), new GuildBoostPerkViewAdapter.GuildBoostPerkViewListItem(invoke22, d24));
        }
        GuildBoostPerkViewAdapter guildBoostPerkViewAdapter = this.adapter;
        if (guildBoostPerkViewAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        guildBoostPerkViewAdapter.configure(c.z(list));
    }

    public GuildBoostPerkView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ViewNitroBoostPerksBinding a = ViewNitroBoostPerksBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "ViewNitroBoostPerksBindi…rom(context), this, true)");
        this.binding = a;
        initialize();
    }

    public /* synthetic */ GuildBoostPerkView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    public /* synthetic */ GuildBoostPerkView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildBoostPerkView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        ViewNitroBoostPerksBinding a = ViewNitroBoostPerksBinding.a(LayoutInflater.from(getContext()), this, true);
        m.checkNotNullExpressionValue(a, "ViewNitroBoostPerksBindi…rom(context), this, true)");
        this.binding = a;
        initialize();
    }
}
