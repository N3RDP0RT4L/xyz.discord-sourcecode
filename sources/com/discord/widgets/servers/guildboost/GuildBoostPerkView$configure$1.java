package com.discord.widgets.servers.guildboost;

import androidx.annotation.DrawableRes;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: GuildBoostPerkView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\u0010\u0006\u001a\u00020\u00002\b\b\u0001\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "imageResId", "", "unlocked", "invoke", "(IZ)I", "getImageAsset"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostPerkView$configure$1 extends o implements Function2<Integer, Boolean, Integer> {
    public static final GuildBoostPerkView$configure$1 INSTANCE = new GuildBoostPerkView$configure$1();

    public GuildBoostPerkView$configure$1() {
        super(2);
    }

    public final int invoke(@DrawableRes int i, boolean z2) {
        return z2 ? R.drawable.ic_check_green_24dp : i;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Integer invoke(Integer num, Boolean bool) {
        return Integer.valueOf(invoke(num.intValue(), bool.booleanValue()));
    }
}
