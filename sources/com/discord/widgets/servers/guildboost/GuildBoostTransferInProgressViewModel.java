package com.discord.widgets.servers.guildboost;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAppliedGuildBoost;
import com.discord.models.domain.ModelGuildBoostSlot;
import com.discord.models.guild.Guild;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGuildBoost;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func3;
/* compiled from: GuildBoostTransferInProgressViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002-.B[\u0012\n\u0010 \u001a\u00060\u0016j\u0002`\u001f\u0012\n\u0010\u0018\u001a\u00060\u0016j\u0002`\u0017\u0012\n\u0010\u001d\u001a\u00060\u0016j\u0002`\u001c\u0012\n\u0010'\u001a\u00060\u0016j\u0002`\u001c\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e\u0012\b\b\u0002\u0010#\u001a\u00020\"\u0012\u000e\b\u0002\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00030)¢\u0006\u0004\b+\u0010,J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\n\u0010\tJ\u000f\u0010\u000b\u001a\u00020\u0005H\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u000f\u0010\f\u001a\u00020\u0005H\u0014¢\u0006\u0004\b\f\u0010\tJ\u000f\u0010\r\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\r\u0010\tR\u0019\u0010\u000f\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001d\u0010\u0018\u001a\u00060\u0016j\u0002`\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010\u001d\u001a\u00060\u0016j\u0002`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0019\u001a\u0004\b\u001e\u0010\u001bR\u001d\u0010 \u001a\u00060\u0016j\u0002`\u001f8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\u0019\u001a\u0004\b!\u0010\u001bR\u0019\u0010#\u001a\u00020\"8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u001d\u0010'\u001a\u00060\u0016j\u0002`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010\u0019\u001a\u0004\b(\u0010\u001b¨\u0006/"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$StoreState;)V", "handleGuildBoostingStarted", "()V", "handleGuildBoostingCompleted", "handleGuildBoostingError", "onCleared", "transferGuildBoost", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "getRestAPI", "()Lcom/discord/utilities/rest/RestAPI;", "Lrx/Subscription;", "guildBoostingSubscription", "Lrx/Subscription;", "", "Lcom/discord/primitives/AppliedGuildBoostId;", "boostId", "J", "getBoostId", "()J", "Lcom/discord/primitives/GuildId;", "previousGuildId", "getPreviousGuildId", "Lcom/discord/primitives/GuildBoostSlotId;", "slotId", "getSlotId", "Lcom/discord/stores/StoreGuildBoost;", "storeGuildBoost", "Lcom/discord/stores/StoreGuildBoost;", "getStoreGuildBoost", "()Lcom/discord/stores/StoreGuildBoost;", "targetGuildId", "getTargetGuildId", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(JJJJLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuildBoost;Lrx/Observable;)V", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostTransferInProgressViewModel extends AppViewModel<ViewState> {
    private final long boostId;
    private Subscription guildBoostingSubscription;
    private final long previousGuildId;
    private final RestAPI restAPI;
    private final long slotId;
    private final StoreGuildBoost storeGuildBoost;
    private final long targetGuildId;

    /* compiled from: GuildBoostTransferInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\n \u0004*\u0004\u0018\u00010\u00060\u00062\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\b\u0010\u0002\u001a\u0004\u0018\u00010\u00002\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/models/guild/Guild;", "previousGuild", "targetGuild", "Lcom/discord/stores/StoreGuildBoost$State;", "kotlin.jvm.PlatformType", "guildBoostState", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildBoost$State;)Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.guildboost.GuildBoostTransferInProgressViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T1, T2, T3, R> implements Func3<Guild, Guild, StoreGuildBoost.State, StoreState> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final StoreState call(Guild guild, Guild guild2, StoreGuildBoost.State state) {
            m.checkNotNullExpressionValue(state, "guildBoostState");
            return new StoreState(guild, guild2, state);
        }
    }

    /* compiled from: GuildBoostTransferInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.guildboost.GuildBoostTransferInProgressViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            GuildBoostTransferInProgressViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: GuildBoostTransferInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0006¢\u0006\u0004\b\u001d\u0010\u001eJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ2\u0010\f\u001a\u00020\u00002\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\u000b\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u001b\u0010\n\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001a\u001a\u0004\b\u001c\u0010\u0004¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$StoreState;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "component2", "Lcom/discord/stores/StoreGuildBoost$State;", "component3", "()Lcom/discord/stores/StoreGuildBoost$State;", "previousGuild", "targetGuild", "guildBoostState", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildBoost$State;)Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreGuildBoost$State;", "getGuildBoostState", "Lcom/discord/models/guild/Guild;", "getTargetGuild", "getPreviousGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/Guild;Lcom/discord/stores/StoreGuildBoost$State;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final StoreGuildBoost.State guildBoostState;
        private final Guild previousGuild;
        private final Guild targetGuild;

        public StoreState(Guild guild, Guild guild2, StoreGuildBoost.State state) {
            m.checkNotNullParameter(state, "guildBoostState");
            this.previousGuild = guild;
            this.targetGuild = guild2;
            this.guildBoostState = state;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Guild guild, Guild guild2, StoreGuildBoost.State state, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = storeState.previousGuild;
            }
            if ((i & 2) != 0) {
                guild2 = storeState.targetGuild;
            }
            if ((i & 4) != 0) {
                state = storeState.guildBoostState;
            }
            return storeState.copy(guild, guild2, state);
        }

        public final Guild component1() {
            return this.previousGuild;
        }

        public final Guild component2() {
            return this.targetGuild;
        }

        public final StoreGuildBoost.State component3() {
            return this.guildBoostState;
        }

        public final StoreState copy(Guild guild, Guild guild2, StoreGuildBoost.State state) {
            m.checkNotNullParameter(state, "guildBoostState");
            return new StoreState(guild, guild2, state);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.previousGuild, storeState.previousGuild) && m.areEqual(this.targetGuild, storeState.targetGuild) && m.areEqual(this.guildBoostState, storeState.guildBoostState);
        }

        public final StoreGuildBoost.State getGuildBoostState() {
            return this.guildBoostState;
        }

        public final Guild getPreviousGuild() {
            return this.previousGuild;
        }

        public final Guild getTargetGuild() {
            return this.targetGuild;
        }

        public int hashCode() {
            Guild guild = this.previousGuild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            Guild guild2 = this.targetGuild;
            int hashCode2 = (hashCode + (guild2 != null ? guild2.hashCode() : 0)) * 31;
            StoreGuildBoost.State state = this.guildBoostState;
            if (state != null) {
                i = state.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(previousGuild=");
            R.append(this.previousGuild);
            R.append(", targetGuild=");
            R.append(this.targetGuild);
            R.append(", guildBoostState=");
            R.append(this.guildBoostState);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildBoostTransferInProgressViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\u0004\u0005\u0006\u0007\bB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0005\t\n\u000b\f\r¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;", "", HookHelper.constructorName, "()V", "ErrorLoading", "ErrorTransfer", "Loading", "PostTransfer", "PreTransfer", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$ErrorTransfer;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$ErrorLoading;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$PreTransfer;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$PostTransfer;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: GuildBoostTransferInProgressViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$ErrorLoading;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ErrorLoading extends ViewState {
            public static final ErrorLoading INSTANCE = new ErrorLoading();

            private ErrorLoading() {
                super(null);
            }
        }

        /* compiled from: GuildBoostTransferInProgressViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$ErrorTransfer;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ErrorTransfer extends ViewState {
            public static final ErrorTransfer INSTANCE = new ErrorTransfer();

            private ErrorTransfer() {
                super(null);
            }
        }

        /* compiled from: GuildBoostTransferInProgressViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        /* compiled from: GuildBoostTransferInProgressViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0007J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$PostTransfer;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "component2", "()I", "targetGuild", "targetGuildSubscriptionCount", "copy", "(Lcom/discord/models/guild/Guild;I)Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$PostTransfer;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getTargetGuild", "I", "getTargetGuildSubscriptionCount", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PostTransfer extends ViewState {
            private final Guild targetGuild;
            private final int targetGuildSubscriptionCount;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public PostTransfer(Guild guild, int i) {
                super(null);
                m.checkNotNullParameter(guild, "targetGuild");
                this.targetGuild = guild;
                this.targetGuildSubscriptionCount = i;
            }

            public static /* synthetic */ PostTransfer copy$default(PostTransfer postTransfer, Guild guild, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    guild = postTransfer.targetGuild;
                }
                if ((i2 & 2) != 0) {
                    i = postTransfer.targetGuildSubscriptionCount;
                }
                return postTransfer.copy(guild, i);
            }

            public final Guild component1() {
                return this.targetGuild;
            }

            public final int component2() {
                return this.targetGuildSubscriptionCount;
            }

            public final PostTransfer copy(Guild guild, int i) {
                m.checkNotNullParameter(guild, "targetGuild");
                return new PostTransfer(guild, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof PostTransfer)) {
                    return false;
                }
                PostTransfer postTransfer = (PostTransfer) obj;
                return m.areEqual(this.targetGuild, postTransfer.targetGuild) && this.targetGuildSubscriptionCount == postTransfer.targetGuildSubscriptionCount;
            }

            public final Guild getTargetGuild() {
                return this.targetGuild;
            }

            public final int getTargetGuildSubscriptionCount() {
                return this.targetGuildSubscriptionCount;
            }

            public int hashCode() {
                Guild guild = this.targetGuild;
                return ((guild != null ? guild.hashCode() : 0) * 31) + this.targetGuildSubscriptionCount;
            }

            public String toString() {
                StringBuilder R = a.R("PostTransfer(targetGuild=");
                R.append(this.targetGuild);
                R.append(", targetGuildSubscriptionCount=");
                return a.A(R, this.targetGuildSubscriptionCount, ")");
            }
        }

        /* compiled from: GuildBoostTransferInProgressViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0006\u0012\u0006\u0010\u000f\u001a\u00020\t¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ8\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00062\b\b\u0002\u0010\u000f\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0015\u0010\bJ\u001a\u0010\u0018\u001a\u00020\t2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u000f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001a\u001a\u0004\b\u000f\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001e\u001a\u0004\b\u001f\u0010\b¨\u0006\""}, d2 = {"Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$PreTransfer;", "Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "component2", "", "component3", "()I", "", "component4", "()Z", "previousGuild", "targetGuild", "targetGuildSubscriptionCount", "isTransferInProgress", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/Guild;IZ)Lcom/discord/widgets/servers/guildboost/GuildBoostTransferInProgressViewModel$ViewState$PreTransfer;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/models/guild/Guild;", "getPreviousGuild", "getTargetGuild", "I", "getTargetGuildSubscriptionCount", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/guild/Guild;IZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class PreTransfer extends ViewState {
            private final boolean isTransferInProgress;
            private final Guild previousGuild;
            private final Guild targetGuild;
            private final int targetGuildSubscriptionCount;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public PreTransfer(Guild guild, Guild guild2, int i, boolean z2) {
                super(null);
                m.checkNotNullParameter(guild, "previousGuild");
                m.checkNotNullParameter(guild2, "targetGuild");
                this.previousGuild = guild;
                this.targetGuild = guild2;
                this.targetGuildSubscriptionCount = i;
                this.isTransferInProgress = z2;
            }

            public static /* synthetic */ PreTransfer copy$default(PreTransfer preTransfer, Guild guild, Guild guild2, int i, boolean z2, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    guild = preTransfer.previousGuild;
                }
                if ((i2 & 2) != 0) {
                    guild2 = preTransfer.targetGuild;
                }
                if ((i2 & 4) != 0) {
                    i = preTransfer.targetGuildSubscriptionCount;
                }
                if ((i2 & 8) != 0) {
                    z2 = preTransfer.isTransferInProgress;
                }
                return preTransfer.copy(guild, guild2, i, z2);
            }

            public final Guild component1() {
                return this.previousGuild;
            }

            public final Guild component2() {
                return this.targetGuild;
            }

            public final int component3() {
                return this.targetGuildSubscriptionCount;
            }

            public final boolean component4() {
                return this.isTransferInProgress;
            }

            public final PreTransfer copy(Guild guild, Guild guild2, int i, boolean z2) {
                m.checkNotNullParameter(guild, "previousGuild");
                m.checkNotNullParameter(guild2, "targetGuild");
                return new PreTransfer(guild, guild2, i, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof PreTransfer)) {
                    return false;
                }
                PreTransfer preTransfer = (PreTransfer) obj;
                return m.areEqual(this.previousGuild, preTransfer.previousGuild) && m.areEqual(this.targetGuild, preTransfer.targetGuild) && this.targetGuildSubscriptionCount == preTransfer.targetGuildSubscriptionCount && this.isTransferInProgress == preTransfer.isTransferInProgress;
            }

            public final Guild getPreviousGuild() {
                return this.previousGuild;
            }

            public final Guild getTargetGuild() {
                return this.targetGuild;
            }

            public final int getTargetGuildSubscriptionCount() {
                return this.targetGuildSubscriptionCount;
            }

            public int hashCode() {
                Guild guild = this.previousGuild;
                int i = 0;
                int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
                Guild guild2 = this.targetGuild;
                if (guild2 != null) {
                    i = guild2.hashCode();
                }
                int i2 = (((hashCode + i) * 31) + this.targetGuildSubscriptionCount) * 31;
                boolean z2 = this.isTransferInProgress;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public final boolean isTransferInProgress() {
                return this.isTransferInProgress;
            }

            public String toString() {
                StringBuilder R = a.R("PreTransfer(previousGuild=");
                R.append(this.previousGuild);
                R.append(", targetGuild=");
                R.append(this.targetGuild);
                R.append(", targetGuildSubscriptionCount=");
                R.append(this.targetGuildSubscriptionCount);
                R.append(", isTransferInProgress=");
                return a.M(R, this.isTransferInProgress, ")");
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ GuildBoostTransferInProgressViewModel(long r14, long r16, long r18, long r20, com.discord.utilities.rest.RestAPI r22, com.discord.stores.StoreGuildBoost r23, rx.Observable r24, int r25, kotlin.jvm.internal.DefaultConstructorMarker r26) {
        /*
            r13 = this;
            r0 = r25 & 16
            if (r0 == 0) goto Lc
            com.discord.utilities.rest.RestAPI$Companion r0 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r0 = r0.getApi()
            r10 = r0
            goto Le
        Lc:
            r10 = r22
        Le:
            r0 = r25 & 32
            if (r0 == 0) goto L1a
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildBoost r0 = r0.getGuildBoosts()
            r11 = r0
            goto L1c
        L1a:
            r11 = r23
        L1c:
            r0 = r25 & 64
            if (r0 == 0) goto L4d
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r1 = r0.getGuilds()
            r6 = r18
            rx.Observable r1 = r1.observeGuild(r6)
            com.discord.stores.StoreGuilds r2 = r0.getGuilds()
            r8 = r20
            rx.Observable r2 = r2.observeGuild(r8)
            com.discord.stores.StoreGuildBoost r0 = r0.getGuildBoosts()
            r3 = 1
            r4 = 0
            rx.Observable r0 = com.discord.stores.StoreGuildBoost.observeGuildBoostState$default(r0, r4, r3, r4)
            com.discord.widgets.servers.guildboost.GuildBoostTransferInProgressViewModel$1 r3 = com.discord.widgets.servers.guildboost.GuildBoostTransferInProgressViewModel.AnonymousClass1.INSTANCE
            rx.Observable r0 = rx.Observable.i(r1, r2, r0, r3)
            java.lang.String r1 = "Observable.combineLatest…guildBoostState\n    )\n  }"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r12 = r0
            goto L53
        L4d:
            r6 = r18
            r8 = r20
            r12 = r24
        L53:
            r1 = r13
            r2 = r14
            r4 = r16
            r6 = r18
            r8 = r20
            r1.<init>(r2, r4, r6, r8, r10, r11, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guildboost.GuildBoostTransferInProgressViewModel.<init>(long, long, long, long, com.discord.utilities.rest.RestAPI, com.discord.stores.StoreGuildBoost, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleGuildBoostingCompleted() {
        this.storeGuildBoost.fetchUserGuildBoostState();
        ViewState requireViewState = requireViewState();
        if (requireViewState instanceof ViewState.PreTransfer) {
            ViewState.PreTransfer preTransfer = (ViewState.PreTransfer) requireViewState;
            requireViewState = new ViewState.PostTransfer(preTransfer.getTargetGuild(), preTransfer.getTargetGuildSubscriptionCount());
        }
        updateViewState(requireViewState);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleGuildBoostingError() {
        updateViewState(ViewState.ErrorTransfer.INSTANCE);
    }

    @MainThread
    private final void handleGuildBoostingStarted() {
        Object obj = (ViewState) requireViewState();
        if (obj instanceof ViewState.PreTransfer) {
            obj = ViewState.PreTransfer.copy$default((ViewState.PreTransfer) obj, null, null, 0, true, 7, null);
        }
        updateViewState(obj);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        int i;
        if (storeState.getPreviousGuild() == null || storeState.getTargetGuild() == null) {
            updateViewState(ViewState.ErrorLoading.INSTANCE);
            return;
        }
        if (storeState.getGuildBoostState() instanceof StoreGuildBoost.State.Loaded) {
            Collection<ModelGuildBoostSlot> values = ((StoreGuildBoost.State.Loaded) storeState.getGuildBoostState()).getBoostSlotMap().values();
            ArrayList arrayList = new ArrayList();
            for (Object obj : values) {
                ModelAppliedGuildBoost premiumGuildSubscription = ((ModelGuildBoostSlot) obj).getPremiumGuildSubscription();
                if (premiumGuildSubscription != null && premiumGuildSubscription.getGuildId() == this.targetGuildId) {
                    arrayList.add(obj);
                }
            }
            i = arrayList.size();
        } else {
            i = 0;
        }
        updateViewState(new ViewState.PreTransfer(storeState.getPreviousGuild(), storeState.getTargetGuild(), i, false));
    }

    public final long getBoostId() {
        return this.boostId;
    }

    public final long getPreviousGuildId() {
        return this.previousGuildId;
    }

    public final RestAPI getRestAPI() {
        return this.restAPI;
    }

    public final long getSlotId() {
        return this.slotId;
    }

    public final StoreGuildBoost getStoreGuildBoost() {
        return this.storeGuildBoost;
    }

    public final long getTargetGuildId() {
        return this.targetGuildId;
    }

    @Override // com.discord.app.AppViewModel, androidx.lifecycle.ViewModel
    public void onCleared() {
        Subscription subscription = this.guildBoostingSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        super.onCleared();
    }

    @MainThread
    public final void transferGuildBoost() {
        handleGuildBoostingStarted();
        Observable<R> z2 = this.restAPI.unsubscribeToGuild(this.previousGuildId, this.boostId).z(new b<Void, Observable<? extends List<? extends ModelAppliedGuildBoost>>>() { // from class: com.discord.widgets.servers.guildboost.GuildBoostTransferInProgressViewModel$transferGuildBoost$1
            public final Observable<? extends List<ModelAppliedGuildBoost>> call(Void r6) {
                return GuildBoostTransferInProgressViewModel.this.getRestAPI().subscribeToGuild(GuildBoostTransferInProgressViewModel.this.getTargetGuildId(), new RestAPIParams.GuildBoosting(d0.t.m.listOf(Long.valueOf(GuildBoostTransferInProgressViewModel.this.getSlotId()))));
            }
        });
        m.checkNotNullExpressionValue(z2, "restAPI\n        .unsubsc…              )\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(z2, false, 1, null), this, null, 2, null), GuildBoostTransferInProgressViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new GuildBoostTransferInProgressViewModel$transferGuildBoost$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new GuildBoostTransferInProgressViewModel$transferGuildBoost$3(this));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildBoostTransferInProgressViewModel(long j, long j2, long j3, long j4, RestAPI restAPI, StoreGuildBoost storeGuildBoost, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(storeGuildBoost, "storeGuildBoost");
        m.checkNotNullParameter(observable, "storeObservable");
        this.slotId = j;
        this.boostId = j2;
        this.previousGuildId = j3;
        this.targetGuildId = j4;
        this.restAPI = restAPI;
        this.storeGuildBoost = storeGuildBoost;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), GuildBoostTransferInProgressViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
