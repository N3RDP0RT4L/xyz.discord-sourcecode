package com.discord.widgets.servers.guildboost;

import com.discord.models.domain.ModelAppliedGuildBoost;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GuildBoostInProgressViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/models/domain/ModelAppliedGuildBoost;", "it", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildBoostInProgressViewModel$subscribeToGuildBoost$3 extends o implements Function1<List<? extends ModelAppliedGuildBoost>, Unit> {
    public final /* synthetic */ GuildBoostInProgressViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildBoostInProgressViewModel$subscribeToGuildBoost$3(GuildBoostInProgressViewModel guildBoostInProgressViewModel) {
        super(1);
        this.this$0 = guildBoostInProgressViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelAppliedGuildBoost> list) {
        invoke2((List<ModelAppliedGuildBoost>) list);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(List<ModelAppliedGuildBoost> list) {
        m.checkNotNullParameter(list, "it");
        this.this$0.handleGuildBoostingCompleted();
    }
}
