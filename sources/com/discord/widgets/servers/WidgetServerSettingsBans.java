package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.user.User;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsBanListItemBinding;
import com.discord.databinding.WidgetServerSettingsBansBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelBan;
import com.discord.models.guild.Guild;
import com.discord.models.user.CoreUser;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.locale.LocaleManager;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.servers.WidgetServerSettingsBans;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.textfield.TextInputLayout;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import j0.l.a.o2;
import j0.l.a.r;
import j0.l.e.k;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func2;
import rx.functions.Func3;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsBans.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 02\u00020\u0001:\u0003102B\u0007¢\u0006\u0004\b/\u0010\u0017J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0019\u0010\t\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ#\u0010\u0010\u001a\u00020\u00042\n\u0010\r\u001a\u00060\u000bj\u0002`\f2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\u0017R\u001d\u0010\u001e\u001a\u00020\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u0018\u0010 \u001a\u0004\u0018\u00010\u001f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b \u0010!R:\u0010$\u001a&\u0012\f\u0012\n #*\u0004\u0018\u00010\u00070\u0007 #*\u0012\u0012\f\u0012\n #*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\"0\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u001c\u0010'\u001a\u00020&8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001a\u0010\r\u001a\u00060\u000bj\u0002`\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010+R\u0018\u0010-\u001a\u0004\u0018\u00010,8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.¨\u00063"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsBans;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;)V", "", "guildName", "configureToolbar", "(Ljava/lang/String;)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/models/domain/ModelBan;", "ban", "showConfirmUnbanDialog", "(JLcom/discord/models/domain/ModelBan;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onResume", "()V", "onPause", "Lcom/discord/databinding/WidgetServerSettingsBansBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsBansBinding;", "binding", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter;", "adapter", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "filterPublisher", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "J", "Landroidx/appcompat/app/AlertDialog;", "dialog", "Landroidx/appcompat/app/AlertDialog;", HookHelper.constructorName, "Companion", "Adapter", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsBans extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsBans.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsBansBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "GUILD_ID";
    private static final int VIEW_INDEX_BANS_LOADING = 0;
    private static final int VIEW_INDEX_BAN_LIST = 1;
    private static final int VIEW_INDEX_NO_BANS = 2;
    private static final int VIEW_INDEX_NO_RESULTS = 3;
    private Adapter adapter;
    private AlertDialog dialog;
    private long guildId;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsBans$binding$2.INSTANCE, null, 2, null);
    private final BehaviorSubject<String> filterPublisher = BehaviorSubject.l0("");
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettingsBans$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetServerSettingsBans.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0017B\u000f\u0012\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016J#\u0010\b\u001a\u00060\u0007R\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR0\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f\u0018\u00010\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter$BanListItem;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter$BanListItem;", "Lkotlin/Function1;", "Lcom/discord/models/domain/ModelBan;", "", "onBanSelectedListener", "Lkotlin/jvm/functions/Function1;", "getOnBanSelectedListener", "()Lkotlin/jvm/functions/Function1;", "setOnBanSelectedListener", "(Lkotlin/jvm/functions/Function1;)V", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "BanListItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Adapter extends MGRecyclerAdapterSimple<Model.BanItem> {
        private Function1<? super ModelBan, Unit> onBanSelectedListener;

        /* compiled from: WidgetServerSettingsBans.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter$BanListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter;", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;)V", "Lcom/discord/databinding/WidgetServerSettingsBanListItemBinding;", "binding", "Lcom/discord/databinding/WidgetServerSettingsBanListItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter;Lcom/discord/widgets/servers/WidgetServerSettingsBans$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public final class BanListItem extends MGRecyclerViewHolder<Adapter, Model.BanItem> {
            private final WidgetServerSettingsBanListItemBinding binding;
            public final /* synthetic */ Adapter this$0;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public BanListItem(Adapter adapter, Adapter adapter2) {
                super((int) R.layout.widget_server_settings_ban_list_item, adapter2);
                m.checkNotNullParameter(adapter2, "adapter");
                this.this$0 = adapter;
                View view = this.itemView;
                RelativeLayout relativeLayout = (RelativeLayout) view;
                int i = R.id.ban_list_item_avatar;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.ban_list_item_avatar);
                if (simpleDraweeView != null) {
                    i = R.id.ban_list_item_name;
                    TextView textView = (TextView) view.findViewById(R.id.ban_list_item_name);
                    if (textView != null) {
                        WidgetServerSettingsBanListItemBinding widgetServerSettingsBanListItemBinding = new WidgetServerSettingsBanListItemBinding((RelativeLayout) view, relativeLayout, simpleDraweeView, textView);
                        m.checkNotNullExpressionValue(widgetServerSettingsBanListItemBinding, "WidgetServerSettingsBanL…temBinding.bind(itemView)");
                        this.binding = widgetServerSettingsBanListItemBinding;
                        return;
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
            }

            public static final /* synthetic */ Adapter access$getAdapter$p(BanListItem banListItem) {
                return (Adapter) banListItem.adapter;
            }

            public void onConfigure(int i, final Model.BanItem banItem) {
                User user;
                User user2;
                m.checkNotNullParameter(banItem, "data");
                super.onConfigure(i, (int) banItem);
                TextView textView = this.binding.d;
                m.checkNotNullExpressionValue(textView, "binding.banListItemName");
                ModelBan ban = banItem.getBan();
                CoreUser coreUser = null;
                textView.setText((ban == null || (user2 = ban.getUser()) == null) ? null : user2.r());
                SimpleDraweeView simpleDraweeView = this.binding.c;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.banListItemAvatar");
                ModelBan ban2 = banItem.getBan();
                if (!(ban2 == null || (user = ban2.getUser()) == null)) {
                    coreUser = new CoreUser(user);
                }
                IconUtils.setIcon$default(simpleDraweeView, coreUser, R.dimen.avatar_size_standard, null, null, null, 56, null);
                this.binding.f2519b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsBans$Adapter$BanListItem$onConfigure$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        Function1<ModelBan, Unit> onBanSelectedListener;
                        ModelBan ban3 = banItem.getBan();
                        if (ban3 != null && (onBanSelectedListener = WidgetServerSettingsBans.Adapter.BanListItem.access$getAdapter$p(WidgetServerSettingsBans.Adapter.BanListItem.this).getOnBanSelectedListener()) != null) {
                            onBanSelectedListener.invoke(ban3);
                        }
                    }
                });
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Adapter(RecyclerView recyclerView) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recycler");
        }

        public final Function1<ModelBan, Unit> getOnBanSelectedListener() {
            return this.onBanSelectedListener;
        }

        public final void setOnBanSelectedListener(Function1<? super ModelBan, Unit> function1) {
            this.onBanSelectedListener = function1;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public BanListItem onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            if (i == 1) {
                return new BanListItem(this, this);
            }
            throw invalidViewTypeException(i);
        }
    }

    /* compiled from: WidgetServerSettingsBans.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J#\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000fR\u0016\u0010\u0012\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u000f¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsBans$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "create", "(Landroid/content/Context;J)V", "", WidgetServerSettingsChannels.INTENT_EXTRA_GUILD_ID, "Ljava/lang/String;", "", "VIEW_INDEX_BANS_LOADING", "I", "VIEW_INDEX_BAN_LIST", "VIEW_INDEX_NO_BANS", "VIEW_INDEX_NO_RESULTS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            StoreStream.Companion.getAnalytics().onGuildSettingsPaneViewed("BANS", j);
            Intent putExtra = new Intent().putExtra(WidgetServerSettingsBans.INTENT_EXTRA_GUILD_ID, j);
            m.checkNotNullExpressionValue(putExtra, "Intent().putExtra(INTENT_EXTRA_GUILD_ID, guildId)");
            j.d(context, WidgetServerSettingsBans.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetServerSettingsBans() {
        super(R.layout.widget_server_settings_bans);
    }

    private final void configureToolbar(String str) {
        setActionBarTitle(R.string.bans);
        setActionBarSubtitle(str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(Model model) {
        if ((model != null ? model.getFilteredBannedUsers() : null) == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.onBackPressed();
                return;
            }
            return;
        }
        configureToolbar(model.getGuildName());
        if (model.isLoading()) {
            AppViewFlipper appViewFlipper = getBinding().d;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.serverSettingsBansViewFlipper");
            appViewFlipper.setDisplayedChild(0);
        } else if (model.getTotalBannedUsers() == 0) {
            AppViewFlipper appViewFlipper2 = getBinding().d;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.serverSettingsBansViewFlipper");
            appViewFlipper2.setDisplayedChild(2);
        } else if (model.getFilteredBannedUsers().isEmpty()) {
            AppViewFlipper appViewFlipper3 = getBinding().d;
            m.checkNotNullExpressionValue(appViewFlipper3, "binding.serverSettingsBansViewFlipper");
            appViewFlipper3.setDisplayedChild(3);
        } else {
            AppViewFlipper appViewFlipper4 = getBinding().d;
            m.checkNotNullExpressionValue(appViewFlipper4, "binding.serverSettingsBansViewFlipper");
            appViewFlipper4.setDisplayedChild(1);
        }
        Adapter adapter = this.adapter;
        if (adapter != null) {
            adapter.setOnBanSelectedListener(new WidgetServerSettingsBans$configureUI$1(this, model));
        }
        Adapter adapter2 = this.adapter;
        if (adapter2 != null) {
            adapter2.setData(model.getFilteredBannedUsers());
        }
    }

    public static final void create(Context context, long j) {
        Companion.create(context, j);
    }

    private final WidgetServerSettingsBansBinding getBinding() {
        return (WidgetServerSettingsBansBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showConfirmUnbanDialog(long j, ModelBan modelBan) {
        CharSequence b2;
        String reason = modelBan.getReason();
        if (reason == null) {
            reason = getString(R.string.no_ban_reason);
        }
        m.checkNotNullExpressionValue(reason, "ban.reason ?: getString(R.string.no_ban_reason)");
        StringBuilder sb = new StringBuilder();
        String string = getString(R.string.ban_reason);
        m.checkNotNullExpressionValue(string, "getString(R.string.ban_reason)");
        Locale primaryLocale = new LocaleManager().getPrimaryLocale(getContext());
        Objects.requireNonNull(string, "null cannot be cast to non-null type java.lang.String");
        String upperCase = string.toUpperCase(primaryLocale);
        m.checkNotNullExpressionValue(upperCase, "(this as java.lang.String).toUpperCase(locale)");
        sb.append(upperCase);
        sb.append("\n");
        sb.append(reason);
        String sb2 = sb.toString();
        WidgetNoticeDialog.Builder builder = new WidgetNoticeDialog.Builder(requireContext());
        b2 = b.b(requireContext(), R.string.unban_user_title, new Object[]{modelBan.getUser().r()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        WidgetNoticeDialog.Builder dialogAttrTheme = WidgetNoticeDialog.Builder.setNegativeButton$default(builder.setTitle(b2.toString()).setMessage(sb2), (int) R.string.cancel, (Function1) null, 2, (Object) null).setPositiveButton(R.string.unban, new WidgetServerSettingsBans$showConfirmUnbanDialog$1(this, j, modelBan)).setDialogAttrTheme(R.attr.notice_theme_positive_red);
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        dialogAttrTheme.show(parentFragmentManager);
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        AlertDialog alertDialog = this.dialog;
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        this.dialog = null;
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        BehaviorSubject<String> behaviorSubject = this.filterPublisher;
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.serverSettingsBansSearch");
        behaviorSubject.onNext(ViewExtensions.getTextOrEmpty(textInputLayout));
        TextInputLayout textInputLayout2 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.serverSettingsBansSearch");
        ViewExtensions.addBindedTextWatcher(textInputLayout2, this, new WidgetServerSettingsBans$onResume$1(this));
        Model.Companion companion = Model.Companion;
        long j = this.guildId;
        BehaviorSubject<String> behaviorSubject2 = this.filterPublisher;
        m.checkNotNullExpressionValue(behaviorSubject2, "filterPublisher");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.get(j, behaviorSubject2), this, null, 2, null), WidgetServerSettingsBans.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsBans$onResume$2(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2520b;
        m.checkNotNullExpressionValue(recyclerView, "binding.serverSettingsBansRecycler");
        this.adapter = (Adapter) companion.configure(new Adapter(recyclerView));
        this.guildId = getMostRecentIntent().getLongExtra(INTENT_EXTRA_GUILD_ID, -1L);
    }

    /* compiled from: WidgetServerSettingsBans.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001c\b\u0086\b\u0018\u0000 *2\u00020\u0001:\u0002+*B=\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u0012\n\u0010\u0015\u001a\u00060\tj\u0002`\n\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\r\u0012\u0006\u0010\u0017\u001a\u00020\u0010¢\u0006\u0004\b(\u0010)J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0014\u0010\u000b\u001a\u00060\tj\u0002`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012JP\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0013\u001a\u00020\u00022\u0010\b\u0002\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00052\f\b\u0002\u0010\u0015\u001a\u00060\tj\u0002`\n2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\r2\b\b\u0002\u0010\u0017\u001a\u00020\u0010HÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u001a\u0010\u000fJ\u0010\u0010\u001b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001b\u0010\u0004J\u001a\u0010\u001d\u001a\u00020\u00102\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u001b\u0010\u0016\u001a\u0004\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u001f\u001a\u0004\b \u0010\u000fR!\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010!\u001a\u0004\b\"\u0010\bR\u0019\u0010\u0017\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010#\u001a\u0004\b\u0017\u0010\u0012R\u001d\u0010\u0015\u001a\u00060\tj\u0002`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b%\u0010\fR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b'\u0010\u0004¨\u0006,"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", "", "", "component1", "()I", "", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "component2", "()Ljava/util/List;", "", "Lcom/discord/primitives/GuildId;", "component3", "()J", "", "component4", "()Ljava/lang/String;", "", "component5", "()Z", "totalBannedUsers", "filteredBannedUsers", "guildId", "guildName", "isLoading", "copy", "(ILjava/util/List;JLjava/lang/String;Z)Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getGuildName", "Ljava/util/List;", "getFilteredBannedUsers", "Z", "J", "getGuildId", "I", "getTotalBannedUsers", HookHelper.constructorName, "(ILjava/util/List;JLjava/lang/String;Z)V", "Companion", "BanItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final int TYPE_BANNED_USER = 1;
        private final List<BanItem> filteredBannedUsers;
        private final long guildId;
        private final String guildName;
        private final boolean isLoading;
        private final int totalBannedUsers;
        public static final Companion Companion = new Companion(null);
        private static final ArrayList<BanItem> emptyBansList = new ArrayList<>();

        /* compiled from: WidgetServerSettingsBans.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001f\u0010 J!\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0007\u0010\bJ1\u0010\f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0005H\u0002¢\u0006\u0004\b\f\u0010\rJ\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J/\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0005¢\u0006\u0004\b\u0012\u0010\rJ3\u0010\u0018\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\u000e\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015¢\u0006\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001a\u001a\u00020\u00138\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u001c\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00160\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006!"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "", "getCanManageBans", "(J)Lrx/Observable;", "", "filterPublisher", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", "getBanItems", "(JLrx/Observable;)Lrx/Observable;", "Lcom/discord/models/guild/Guild;", "guild", "createLoading", "(Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", "get", "", "totalBannedUsers", "", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "filteredBannedUsers", "create", "(Lcom/discord/models/guild/Guild;Ljava/lang/Integer;Ljava/util/List;)Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", "TYPE_BANNED_USER", "I", "Ljava/util/ArrayList;", "emptyBansList", "Ljava/util/ArrayList;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Model createLoading(Guild guild) {
                if (guild == null) {
                    return null;
                }
                return new Model(0, Model.emptyBansList, guild.getId(), guild.getName(), true);
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Observable<Model> getBanItems(final long j, final Observable<String> observable) {
                Observable<R> Y = StoreStream.Companion.getBans().observeBans(j).Y(new j0.k.b<Map<Long, ? extends ModelBan>, Observable<? extends Model>>() { // from class: com.discord.widgets.servers.WidgetServerSettingsBans$Model$Companion$getBanItems$1

                    /* compiled from: WidgetServerSettingsBans.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/guild/Guild;", "it", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.servers.WidgetServerSettingsBans$Model$Companion$getBanItems$1$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass1<T, R> implements j0.k.b<Guild, WidgetServerSettingsBans.Model> {
                        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                        public final WidgetServerSettingsBans.Model call(Guild guild) {
                            WidgetServerSettingsBans.Model createLoading;
                            createLoading = WidgetServerSettingsBans.Model.Companion.createLoading(guild);
                            return createLoading;
                        }
                    }

                    /* compiled from: WidgetServerSettingsBans.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\f\u001a\u0004\u0018\u00010\t2\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022*\u0010\b\u001a&\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00060\u0006 \u0003*\u0012\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00070\u0005H\n¢\u0006\u0004\b\n\u0010\u000b"}, d2 = {"Lcom/discord/models/guild/Guild;", "guild", "", "kotlin.jvm.PlatformType", "totalBannedUsers", "", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "", "filteredBannedUsers", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;Ljava/lang/Integer;Ljava/util/List;)Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.servers.WidgetServerSettingsBans$Model$Companion$getBanItems$1$3  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass3<T1, T2, T3, R> implements Func3<Guild, Integer, List<WidgetServerSettingsBans.Model.BanItem>, WidgetServerSettingsBans.Model> {
                        public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

                        public final WidgetServerSettingsBans.Model call(Guild guild, Integer num, List<WidgetServerSettingsBans.Model.BanItem> list) {
                            return WidgetServerSettingsBans.Model.Companion.create(guild, num, list);
                        }
                    }

                    public final Observable<? extends WidgetServerSettingsBans.Model> call(final Map<Long, ? extends ModelBan> map) {
                        if (map == null) {
                            return (Observable<R>) StoreStream.Companion.getGuilds().observeGuild(j).F(AnonymousClass1.INSTANCE);
                        }
                        return Observable.i(StoreStream.Companion.getGuilds().observeGuild(j), new k(Integer.valueOf(map.size())), observable.Y(new j0.k.b<String, Observable<? extends List<WidgetServerSettingsBans.Model.BanItem>>>() { // from class: com.discord.widgets.servers.WidgetServerSettingsBans$Model$Companion$getBanItems$1.2

                            /* compiled from: WidgetServerSettingsBans.kt */
                            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/domain/ModelBan;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/domain/ModelBan;)Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsBans$Model$Companion$getBanItems$1$2$2  reason: invalid class name and collision with other inner class name */
                            /* loaded from: classes2.dex */
                            public static final class C02542<T, R> implements j0.k.b<ModelBan, WidgetServerSettingsBans.Model.BanItem> {
                                public static final C02542 INSTANCE = new C02542();

                                public final WidgetServerSettingsBans.Model.BanItem call(ModelBan modelBan) {
                                    return new WidgetServerSettingsBans.Model.BanItem(modelBan);
                                }
                            }

                            /* compiled from: WidgetServerSettingsBans.kt */
                            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0007\u001a\n \u0001*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0003\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "kotlin.jvm.PlatformType", "banItem1", "banItem2", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;)Ljava/lang/Integer;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsBans$Model$Companion$getBanItems$1$2$3  reason: invalid class name */
                            /* loaded from: classes2.dex */
                            public static final class AnonymousClass3<T1, T2, R> implements Func2<WidgetServerSettingsBans.Model.BanItem, WidgetServerSettingsBans.Model.BanItem, Integer> {
                                public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

                                public final Integer call(WidgetServerSettingsBans.Model.BanItem banItem, WidgetServerSettingsBans.Model.BanItem banItem2) {
                                    User user;
                                    String r;
                                    String str;
                                    User user2;
                                    ModelBan ban = banItem.getBan();
                                    if (ban == null || (user = ban.getUser()) == null || (r = user.r()) == null) {
                                        return null;
                                    }
                                    ModelBan ban2 = banItem2.getBan();
                                    if (ban2 == null || (user2 = ban2.getUser()) == null || (str = user2.r()) == null) {
                                        str = "";
                                    }
                                    return Integer.valueOf(t.compareTo(r, str, true));
                                }
                            }

                            public final Observable<? extends List<WidgetServerSettingsBans.Model.BanItem>> call(final String str) {
                                Observable<R> F = Observable.A(map.values()).x(new j0.k.b<ModelBan, Boolean>() { // from class: com.discord.widgets.servers.WidgetServerSettingsBans.Model.Companion.getBanItems.1.2.1
                                    public final Boolean call(ModelBan modelBan) {
                                        m.checkNotNullExpressionValue(modelBan, "it");
                                        String r = modelBan.getUser().r();
                                        String str2 = str;
                                        m.checkNotNullExpressionValue(str2, "filter");
                                        return Boolean.valueOf(w.contains((CharSequence) r, (CharSequence) str2, true));
                                    }
                                }).F(C02542.INSTANCE);
                                return Observable.h0(new r(F.j, new o2(AnonymousClass3.INSTANCE, 10)));
                            }
                        }), AnonymousClass3.INSTANCE);
                    }
                });
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …          }\n            }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(Y).q();
                m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
                return q;
            }

            private final Observable<Boolean> getCanManageBans(long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable i = Observable.i(companion.getPermissions().observePermissionsForGuild(j), companion.getGuilds().observeGuild(j), StoreUser.observeMe$default(companion.getUsers(), false, 1, null), WidgetServerSettingsBans$Model$Companion$getCanManageBans$1.INSTANCE);
                m.checkNotNullExpressionValue(i, "Observable\n             …        }\n              }");
                Observable<Boolean> q = ObservableExtensionsKt.computationLatest(i).q();
                m.checkNotNullExpressionValue(q, "Observable\n             …  .distinctUntilChanged()");
                return q;
            }

            public final Model create(Guild guild, Integer num, List<BanItem> list) {
                if (guild == null || num == null || list == null) {
                    return null;
                }
                return new Model(num.intValue(), list, guild.getId(), guild.getName(), false);
            }

            public final Observable<Model> get(final long j, final Observable<String> observable) {
                m.checkNotNullParameter(observable, "filterPublisher");
                Observable Y = getCanManageBans(j).Y(new j0.k.b<Boolean, Observable<? extends Model>>() { // from class: com.discord.widgets.servers.WidgetServerSettingsBans$Model$Companion$get$1
                    public final Observable<? extends WidgetServerSettingsBans.Model> call(Boolean bool) {
                        Observable<? extends WidgetServerSettingsBans.Model> banItems;
                        m.checkNotNullExpressionValue(bool, "canManage");
                        if (!bool.booleanValue()) {
                            return new k(null);
                        }
                        banItems = WidgetServerSettingsBans.Model.Companion.getBanItems(j, observable);
                        return banItems;
                    }
                });
                m.checkNotNullExpressionValue(Y, "getCanManageBans(guildId….just(null)\n            }");
                return Y;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(int i, List<BanItem> list, long j, String str, boolean z2) {
            this.totalBannedUsers = i;
            this.filteredBannedUsers = list;
            this.guildId = j;
            this.guildName = str;
            this.isLoading = z2;
        }

        public static /* synthetic */ Model copy$default(Model model, int i, List list, long j, String str, boolean z2, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = model.totalBannedUsers;
            }
            List<BanItem> list2 = list;
            if ((i2 & 2) != 0) {
                list2 = model.filteredBannedUsers;
            }
            List list3 = list2;
            if ((i2 & 4) != 0) {
                j = model.guildId;
            }
            long j2 = j;
            if ((i2 & 8) != 0) {
                str = model.guildName;
            }
            String str2 = str;
            if ((i2 & 16) != 0) {
                z2 = model.isLoading;
            }
            return model.copy(i, list3, j2, str2, z2);
        }

        public final int component1() {
            return this.totalBannedUsers;
        }

        public final List<BanItem> component2() {
            return this.filteredBannedUsers;
        }

        public final long component3() {
            return this.guildId;
        }

        public final String component4() {
            return this.guildName;
        }

        public final boolean component5() {
            return this.isLoading;
        }

        public final Model copy(int i, List<BanItem> list, long j, String str, boolean z2) {
            return new Model(i, list, j, str, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return this.totalBannedUsers == model.totalBannedUsers && m.areEqual(this.filteredBannedUsers, model.filteredBannedUsers) && this.guildId == model.guildId && m.areEqual(this.guildName, model.guildName) && this.isLoading == model.isLoading;
        }

        public final List<BanItem> getFilteredBannedUsers() {
            return this.filteredBannedUsers;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final String getGuildName() {
            return this.guildName;
        }

        public final int getTotalBannedUsers() {
            return this.totalBannedUsers;
        }

        public int hashCode() {
            int i = this.totalBannedUsers * 31;
            List<BanItem> list = this.filteredBannedUsers;
            int i2 = 0;
            int a = (a0.a.a.b.a(this.guildId) + ((i + (list != null ? list.hashCode() : 0)) * 31)) * 31;
            String str = this.guildName;
            if (str != null) {
                i2 = str.hashCode();
            }
            int i3 = (a + i2) * 31;
            boolean z2 = this.isLoading;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            return i3 + i4;
        }

        public final boolean isLoading() {
            return this.isLoading;
        }

        public String toString() {
            StringBuilder R = a.R("Model(totalBannedUsers=");
            R.append(this.totalBannedUsers);
            R.append(", filteredBannedUsers=");
            R.append(this.filteredBannedUsers);
            R.append(", guildId=");
            R.append(this.guildId);
            R.append(", guildName=");
            R.append(this.guildName);
            R.append(", isLoading=");
            return a.M(R, this.isLoading, ")");
        }

        /* compiled from: WidgetServerSettingsBans.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001e\u0010\u001dJ\u0015\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u001c\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\u0005J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\u0014\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u000bR\u001c\u0010\u0017\u001a\u00020\f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u000eR$\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0003\u0010\u001a\u001a\u0004\b\u001b\u0010\u0007\"\u0004\b\u001c\u0010\u001d¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/models/domain/ModelBan;", "ban", "create", "(Lcom/discord/models/domain/ModelBan;)Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model$BanItem;", "component1", "()Lcom/discord/models/domain/ModelBan;", "copy", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "key", "Ljava/lang/String;", "getKey", "type", "I", "getType", "Lcom/discord/models/domain/ModelBan;", "getBan", "setBan", "(Lcom/discord/models/domain/ModelBan;)V", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class BanItem implements MGRecyclerDataPayload {
            private ModelBan ban;
            private final String key;
            private final int type;

            public BanItem() {
                this(null, 1, null);
            }

            public BanItem(ModelBan modelBan) {
                User user;
                this.ban = modelBan;
                this.type = 1;
                this.key = String.valueOf((modelBan == null || (user = modelBan.getUser()) == null) ? null : Long.valueOf(user.i()));
            }

            public static /* synthetic */ BanItem copy$default(BanItem banItem, ModelBan modelBan, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelBan = banItem.ban;
                }
                return banItem.copy(modelBan);
            }

            public final ModelBan component1() {
                return this.ban;
            }

            public final BanItem copy(ModelBan modelBan) {
                return new BanItem(modelBan);
            }

            public final BanItem create(ModelBan modelBan) {
                m.checkNotNullParameter(modelBan, "ban");
                BanItem banItem = new BanItem(null, 1, null);
                banItem.ban = modelBan;
                return banItem;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof BanItem) && m.areEqual(this.ban, ((BanItem) obj).ban);
                }
                return true;
            }

            public final ModelBan getBan() {
                return this.ban;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                ModelBan modelBan = this.ban;
                if (modelBan != null) {
                    return modelBan.hashCode();
                }
                return 0;
            }

            public final void setBan(ModelBan modelBan) {
                this.ban = modelBan;
            }

            public String toString() {
                StringBuilder R = a.R("BanItem(ban=");
                R.append(this.ban);
                R.append(")");
                return R.toString();
            }

            public /* synthetic */ BanItem(ModelBan modelBan, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? null : modelBan);
            }
        }
    }
}
