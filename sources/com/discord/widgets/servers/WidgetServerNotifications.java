package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.i.x1;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.preview.GuildPreview;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetServerNotificationsBinding;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildProfiles;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserGuildSettings;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.SwipeableItemTouchHelper;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import com.discord.widgets.servers.NotificationMuteSettingsView;
import com.discord.widgets.servers.NotificationsOverridesAdapter;
import com.discord.widgets.servers.WidgetServerNotifications;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetServerNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 $2\u00020\u0001:\u0002$%B\u0007¢\u0006\u0004\b#\u0010\bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ'\u0010\r\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0016\u0010\bR\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u001d\u0010\"\u001a\u00020\u001d8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010!¨\u0006&"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerNotifications;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerNotifications$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerNotifications$Model;)V", "configureForHub", "()V", "Lcom/discord/views/CheckedSetting;", "radio", "", "type", "configureRadio", "(Lcom/discord/views/CheckedSetting;ILcom/discord/widgets/servers/WidgetServerNotifications$Model;)V", "Landroidx/recyclerview/widget/ItemTouchHelper;", "createSwipeableItemTouchHelper", "()Landroidx/recyclerview/widget/ItemTouchHelper;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/views/RadioManager;", "notificationSettingsRadioManager", "Lcom/discord/views/RadioManager;", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter;", "overrideAdapter", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter;", "Lcom/discord/databinding/WidgetServerNotificationsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerNotificationsBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerNotifications extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerNotifications.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerNotificationsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerNotifications$binding$2.INSTANCE, null, 2, null);
    private RadioManager notificationSettingsRadioManager;
    private NotificationsOverridesAdapter overrideAdapter;

    /* compiled from: WidgetServerNotifications.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011JW\u0010\t\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 \b*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00060\u0006 \b*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 \b*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00060\u0006\u0018\u00010\u00050\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u000e\u001a\u00020\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerNotifications$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;", "kotlin.jvm.PlatformType", "getNotificationOverrides", "(J)Lrx/Observable;", "Landroid/content/Context;", "context", "", "launch", "(JLandroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<List<NotificationsOverridesAdapter.Item>> getNotificationOverrides(long j) {
            StoreStream.Companion companion = StoreStream.Companion;
            return Observable.i(companion.getUserGuildSettings().observeGuildSettings(j).F(WidgetServerNotifications$Companion$getNotificationOverrides$1.INSTANCE), StoreChannels.observeChannelsForGuild$default(companion.getChannels(), j, null, 2, null), GuildChannelsInfo.Companion.get(j), WidgetServerNotifications$Companion$getNotificationOverrides$2.INSTANCE);
        }

        public final void launch(long j, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra("com.discord.intent.extra.EXTRA_GUILD_ID", j);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE….EXTRA_GUILD_ID, guildId)");
            j.d(context, WidgetServerNotifications.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerNotifications.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0014\b\u0086\b\u0018\u0000 *2\u00020\u0001:\u0001*B!\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b(\u0010)J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001a\u001a\u00020\u00178\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001a\u0010\u001cR\u0019\u0010\u001d\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010\u0015R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010 \u001a\u0004\b!\u0010\nR\u0018\u0010\"\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010$\u001a\u0004\b%\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010&\u001a\u0004\b'\u0010\u0004¨\u0006+"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerNotifications$Model;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/domain/ModelNotificationSettings;", "component2", "()Lcom/discord/models/domain/ModelNotificationSettings;", "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "component3", "()Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "guild", "guildSettings", "guildProfile", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/ModelNotificationSettings;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;)Lcom/discord/widgets/servers/WidgetServerNotifications$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "isAboveNotifyAllSize", "Z", "()Z", "notificationsSetting", "I", "getNotificationsSetting", "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "getGuildProfile", "approximateMemberCount", "Ljava/lang/Integer;", "Lcom/discord/models/domain/ModelNotificationSettings;", "getGuildSettings", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/ModelNotificationSettings;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Integer approximateMemberCount;
        private final Guild guild;
        private final StoreGuildProfiles.GuildProfileData guildProfile;
        private final ModelNotificationSettings guildSettings;
        private final boolean isAboveNotifyAllSize;
        private final int notificationsSetting;

        /* compiled from: WidgetServerNotifications.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J!\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\bJ!\u0010\u000e\u001a\u00020\r2\b\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000b¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerNotifications$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerNotifications$Model;", "get", "(J)Lrx/Observable;", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/domain/ModelNotificationSettings;", "guildSettings", "", "isValid", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/ModelNotificationSettings;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable i = Observable.i(companion.getGuilds().observeGuild(j), companion.getUserGuildSettings().observeGuildSettings(j), companion.getGuildProfiles().observeGuildProfile(j), WidgetServerNotifications$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(i, "Observable\n          .co…  } else null\n          }");
                Observable<Model> q = ObservableExtensionsKt.computationBuffered(i).q();
                m.checkNotNullExpressionValue(q, "Observable\n          .co…  .distinctUntilChanged()");
                return q;
            }

            public final boolean isValid(Guild guild, ModelNotificationSettings modelNotificationSettings) {
                return (guild == null || modelNotificationSettings == null) ? false : true;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Guild guild, ModelNotificationSettings modelNotificationSettings, StoreGuildProfiles.GuildProfileData guildProfileData) {
            int i;
            GuildPreview data;
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(modelNotificationSettings, "guildSettings");
            this.guild = guild;
            this.guildSettings = modelNotificationSettings;
            this.guildProfile = guildProfileData;
            if (modelNotificationSettings.getMessageNotifications() != ModelNotificationSettings.FREQUENCY_UNSET) {
                i = modelNotificationSettings.getMessageNotifications();
            } else {
                i = guild.getDefaultMessageNotifications();
            }
            this.notificationsSetting = i;
            Integer a = (guildProfileData == null || (data = guildProfileData.getData()) == null) ? null : data.a();
            this.approximateMemberCount = a;
            this.isAboveNotifyAllSize = a != null && a.intValue() > 2500;
        }

        public static /* synthetic */ Model copy$default(Model model, Guild guild, ModelNotificationSettings modelNotificationSettings, StoreGuildProfiles.GuildProfileData guildProfileData, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = model.guild;
            }
            if ((i & 2) != 0) {
                modelNotificationSettings = model.guildSettings;
            }
            if ((i & 4) != 0) {
                guildProfileData = model.guildProfile;
            }
            return model.copy(guild, modelNotificationSettings, guildProfileData);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final ModelNotificationSettings component2() {
            return this.guildSettings;
        }

        public final StoreGuildProfiles.GuildProfileData component3() {
            return this.guildProfile;
        }

        public final Model copy(Guild guild, ModelNotificationSettings modelNotificationSettings, StoreGuildProfiles.GuildProfileData guildProfileData) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(modelNotificationSettings, "guildSettings");
            return new Model(guild, modelNotificationSettings, guildProfileData);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.guild, model.guild) && m.areEqual(this.guildSettings, model.guildSettings) && m.areEqual(this.guildProfile, model.guildProfile);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final StoreGuildProfiles.GuildProfileData getGuildProfile() {
            return this.guildProfile;
        }

        public final ModelNotificationSettings getGuildSettings() {
            return this.guildSettings;
        }

        public final int getNotificationsSetting() {
            return this.notificationsSetting;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            ModelNotificationSettings modelNotificationSettings = this.guildSettings;
            int hashCode2 = (hashCode + (modelNotificationSettings != null ? modelNotificationSettings.hashCode() : 0)) * 31;
            StoreGuildProfiles.GuildProfileData guildProfileData = this.guildProfile;
            if (guildProfileData != null) {
                i = guildProfileData.hashCode();
            }
            return hashCode2 + i;
        }

        public final boolean isAboveNotifyAllSize() {
            return this.isAboveNotifyAllSize;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guild=");
            R.append(this.guild);
            R.append(", guildSettings=");
            R.append(this.guildSettings);
            R.append(", guildProfile=");
            R.append(this.guildProfile);
            R.append(")");
            return R.toString();
        }
    }

    public WidgetServerNotifications() {
        super(R.layout.widget_server_notifications);
    }

    public static final /* synthetic */ NotificationsOverridesAdapter access$getOverrideAdapter$p(WidgetServerNotifications widgetServerNotifications) {
        NotificationsOverridesAdapter notificationsOverridesAdapter = widgetServerNotifications.overrideAdapter;
        if (notificationsOverridesAdapter == null) {
            m.throwUninitializedPropertyAccessException("overrideAdapter");
        }
        return notificationsOverridesAdapter;
    }

    private final void configureForHub() {
        View view = getBinding().h;
        m.checkNotNullExpressionValue(view, "binding.serverNotificationsFrequencyDivider");
        view.setVisibility(8);
        LinearLayout linearLayout = getBinding().i;
        m.checkNotNullExpressionValue(linearLayout, "binding.serverNotificationsFrequencyWrap");
        linearLayout.setVisibility(8);
        CheckedSetting checkedSetting = getBinding().d;
        m.checkNotNullExpressionValue(checkedSetting, "binding.serverNotificationsEveryoneSwitch");
        checkedSetting.setVisibility(8);
        View view2 = getBinding().n;
        m.checkNotNullExpressionValue(view2, "binding.serverNotificationsRolesDivider");
        view2.setVisibility(8);
        CheckedSetting checkedSetting2 = getBinding().o;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.serverNotificationsRolesSwitch");
        checkedSetting2.setVisibility(8);
        CheckedSetting checkedSetting3 = getBinding().m;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.serverNotificationsPushSwitch");
        checkedSetting3.setVisibility(8);
        TextView textView = getBinding().l;
        m.checkNotNullExpressionValue(textView, "binding.serverNotificationsOverrideTitle");
        textView.setVisibility(8);
        x1 x1Var = getBinding().f2514b;
        m.checkNotNullExpressionValue(x1Var, "binding.addOverride");
        ConstraintLayout constraintLayout = x1Var.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.addOverride.root");
        constraintLayout.setVisibility(8);
        View view3 = getBinding().k;
        m.checkNotNullExpressionValue(view3, "binding.serverNotificationsOverrideDivider");
        view3.setVisibility(8);
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.guildNotificationsOverrideList");
        recyclerView.setVisibility(8);
    }

    @MainThread
    private final void configureRadio(CheckedSetting checkedSetting, final int i, final Model model) {
        CharSequence e;
        if (model.getNotificationsSetting() == i) {
            RadioManager radioManager = this.notificationSettingsRadioManager;
            if (radioManager == null) {
                m.throwUninitializedPropertyAccessException("notificationSettingsRadioManager");
            }
            radioManager.a(checkedSetting);
        }
        if (!model.isAboveNotifyAllSize() || i != ModelNotificationSettings.FREQUENCY_ALL) {
            int i2 = CheckedSetting.j;
            checkedSetting.h(null, false);
        } else {
            e = b.e(this, R.string.large_guild_notify_all_messages_description, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            int i3 = CheckedSetting.j;
            checkedSetting.h(e, false);
        }
        checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerNotifications$configureRadio$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoreStream.Companion.getUserGuildSettings().setGuildFrequency(a.x(view, "view", "view.context"), WidgetServerNotifications.Model.this.getGuild(), i);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        CharSequence b2;
        CharSequence b3;
        CharSequence b4;
        CharSequence b5;
        CharSequence d;
        CharSequence d2;
        CharSequence d3;
        if (model == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.onBackPressed();
                return;
            }
            return;
        }
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.notification_settings);
        setActionBarSubtitle(model.getGuild().getName());
        ModelNotificationSettings guildSettings = model.getGuildSettings();
        boolean isMuted = guildSettings.isMuted();
        long id2 = model.getGuild().getId();
        View view = getBinding().h;
        m.checkNotNullExpressionValue(view, "binding.serverNotificationsFrequencyDivider");
        view.setVisibility(isMuted ^ true ? 0 : 8);
        LinearLayout linearLayout = getBinding().i;
        m.checkNotNullExpressionValue(linearLayout, "binding.serverNotificationsFrequencyWrap");
        linearLayout.setVisibility(isMuted ^ true ? 0 : 8);
        String muteEndTime = guildSettings.getMuteEndTime();
        b2 = b.b(requireContext(), R.string.form_label_mute_server, new Object[]{model.getGuild().getName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        b3 = b.b(requireContext(), R.string.form_label_unmute_server, new Object[]{model.getGuild().getName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        b4 = b.b(requireContext(), R.string.form_label_mobile_server_muted, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        b5 = b.b(requireContext(), R.string.form_label_mute_server_description, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        getBinding().j.updateView(new NotificationMuteSettingsView.ViewState(isMuted, muteEndTime, b2, b3, b4, R.string.form_label_mobile_server_muted_until, b5), new WidgetServerNotifications$configureUI$onMute$1(this, id2), new WidgetServerNotifications$configureUI$onUnmute$1(this, id2));
        if (model.getGuild().isHub()) {
            configureForHub();
            return;
        }
        getBinding().d.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.servers.WidgetServerNotifications$configureUI$1
            public final void call(Boolean bool) {
                WidgetServerNotificationsBinding binding;
                StoreUserGuildSettings userGuildSettings = StoreStream.Companion.getUserGuildSettings();
                binding = WidgetServerNotifications.this.getBinding();
                CheckedSetting checkedSetting = binding.d;
                m.checkNotNullExpressionValue(checkedSetting, "binding.serverNotificationsEveryoneSwitch");
                Context context = checkedSetting.getContext();
                m.checkNotNullExpressionValue(context, "binding.serverNotificationsEveryoneSwitch.context");
                userGuildSettings.setGuildToggles(context, model.getGuild(), (r16 & 4) != 0 ? null : Boolean.valueOf(!model.getGuildSettings().isSuppressEveryone()), (r16 & 8) != 0 ? null : null, (r16 & 16) != 0 ? null : null, (r16 & 32) != 0 ? null : null);
            }
        });
        CheckedSetting checkedSetting = getBinding().d;
        m.checkNotNullExpressionValue(checkedSetting, "binding.serverNotificationsEveryoneSwitch");
        checkedSetting.setChecked(model.getGuildSettings().isSuppressEveryone());
        CheckedSetting checkedSetting2 = getBinding().d;
        CheckedSetting checkedSetting3 = getBinding().d;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.serverNotificationsEveryoneSwitch");
        d = b.d(checkedSetting3, R.string.form_label_suppress_everyone, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        checkedSetting2.setText(d);
        getBinding().o.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.servers.WidgetServerNotifications$configureUI$2
            public final void call(Boolean bool) {
                WidgetServerNotificationsBinding binding;
                StoreUserGuildSettings userGuildSettings = StoreStream.Companion.getUserGuildSettings();
                binding = WidgetServerNotifications.this.getBinding();
                CheckedSetting checkedSetting4 = binding.o;
                m.checkNotNullExpressionValue(checkedSetting4, "binding.serverNotificationsRolesSwitch");
                Context context = checkedSetting4.getContext();
                m.checkNotNullExpressionValue(context, "binding.serverNotificationsRolesSwitch.context");
                userGuildSettings.setGuildToggles(context, model.getGuild(), (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? null : Boolean.valueOf(!model.getGuildSettings().isSuppressRoles()), (r16 & 16) != 0 ? null : null, (r16 & 32) != 0 ? null : null);
            }
        });
        CheckedSetting checkedSetting4 = getBinding().o;
        m.checkNotNullExpressionValue(checkedSetting4, "binding.serverNotificationsRolesSwitch");
        checkedSetting4.setChecked(model.getGuildSettings().isSuppressRoles());
        CheckedSetting checkedSetting5 = getBinding().o;
        CheckedSetting checkedSetting6 = getBinding().o;
        m.checkNotNullExpressionValue(checkedSetting6, "binding.serverNotificationsRolesSwitch");
        d2 = b.d(checkedSetting6, R.string.form_label_suppress_roles, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        checkedSetting5.setText(d2);
        CheckedSetting checkedSetting7 = getBinding().m;
        m.checkNotNullExpressionValue(checkedSetting7, "binding.serverNotificationsPushSwitch");
        checkedSetting7.setVisibility(model.getGuildSettings().isMuted() ^ true ? 0 : 8);
        CheckedSetting checkedSetting8 = getBinding().m;
        m.checkNotNullExpressionValue(checkedSetting8, "binding.serverNotificationsPushSwitch");
        checkedSetting8.setChecked(model.getGuildSettings().isMobilePush());
        getBinding().m.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.servers.WidgetServerNotifications$configureUI$3
            public final void call(Boolean bool) {
                WidgetServerNotificationsBinding binding;
                StoreUserGuildSettings userGuildSettings = StoreStream.Companion.getUserGuildSettings();
                binding = WidgetServerNotifications.this.getBinding();
                CheckedSetting checkedSetting9 = binding.m;
                m.checkNotNullExpressionValue(checkedSetting9, "binding.serverNotificationsPushSwitch");
                Context context = checkedSetting9.getContext();
                m.checkNotNullExpressionValue(context, "binding.serverNotificationsPushSwitch.context");
                userGuildSettings.setGuildToggles(context, model.getGuild(), null, null, null, Boolean.valueOf(!model.getGuildSettings().isMobilePush()));
            }
        });
        CheckedSetting checkedSetting9 = getBinding().f;
        CheckedSetting checkedSetting10 = getBinding().f;
        m.checkNotNullExpressionValue(checkedSetting10, "binding.serverNotificationsFrequency1Radio");
        d3 = b.d(checkedSetting10, R.string.form_label_only_mentions, new Object[0], (r4 & 4) != 0 ? b.c.j : null);
        checkedSetting9.setText(d3);
        CheckedSetting checkedSetting11 = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting11, "binding.serverNotificationsFrequency0Radio");
        configureRadio(checkedSetting11, ModelNotificationSettings.FREQUENCY_ALL, model);
        CheckedSetting checkedSetting12 = getBinding().f;
        m.checkNotNullExpressionValue(checkedSetting12, "binding.serverNotificationsFrequency1Radio");
        configureRadio(checkedSetting12, ModelNotificationSettings.FREQUENCY_MENTIONS, model);
        CheckedSetting checkedSetting13 = getBinding().g;
        m.checkNotNullExpressionValue(checkedSetting13, "binding.serverNotificationsFrequency2Radio");
        configureRadio(checkedSetting13, ModelNotificationSettings.FREQUENCY_NOTHING, model);
    }

    private final ItemTouchHelper createSwipeableItemTouchHelper() {
        final SwipeableItemTouchHelper.SwipeRevealConfiguration swipeRevealConfiguration = new SwipeableItemTouchHelper.SwipeRevealConfiguration(ColorCompat.getColor(this, (int) R.color.status_red_500), ContextCompat.getDrawable(requireContext(), R.drawable.ic_delete_white_24dp), DimenUtils.dpToPixels(8));
        return new ItemTouchHelper(new SwipeableItemTouchHelper(swipeRevealConfiguration, swipeRevealConfiguration) { // from class: com.discord.widgets.servers.WidgetServerNotifications$createSwipeableItemTouchHelper$1
            @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int i) {
                m.checkNotNullParameter(viewHolder, "viewHolder");
                NotificationsOverridesAdapter.Item item = (NotificationsOverridesAdapter.Item) u.getOrNull(WidgetServerNotifications.access$getOverrideAdapter$p(WidgetServerNotifications.this).getData(), viewHolder.getAdapterPosition());
                if (item != null) {
                    Channel channel = item.getChannel();
                    if (item.getOverrideSettings().isMuted()) {
                        StoreUserGuildSettings.setChannelMuted$default(StoreStream.Companion.getUserGuildSettings(), WidgetServerNotifications.this.requireContext(), channel.h(), false, null, 8, null);
                    }
                    StoreStream.Companion.getUserGuildSettings().setChannelFrequency(WidgetServerNotifications.this.requireContext(), channel, ModelNotificationSettings.FREQUENCY_UNSET);
                }
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerNotificationsBinding getBinding() {
        return (WidgetServerNotificationsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        this.notificationSettingsRadioManager = new RadioManager(n.listOf((Object[]) new CheckedSetting[]{getBinding().e, getBinding().f, getBinding().g}));
        this.overrideAdapter = new NotificationsOverridesAdapter(WidgetServerNotifications$onViewBound$1.INSTANCE);
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.guildNotificationsOverrideList");
        recyclerView.setNestedScrollingEnabled(false);
        RecyclerView recyclerView2 = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView2, "binding.guildNotificationsOverrideList");
        NotificationsOverridesAdapter notificationsOverridesAdapter = this.overrideAdapter;
        if (notificationsOverridesAdapter == null) {
            m.throwUninitializedPropertyAccessException("overrideAdapter");
        }
        recyclerView2.setAdapter(notificationsOverridesAdapter);
        createSwipeableItemTouchHelper().attachToRecyclerView(getBinding().c);
        final long longExtra = getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_GUILD_ID", -1L);
        x1 x1Var = getBinding().f2514b;
        m.checkNotNullExpressionValue(x1Var, "binding.addOverride");
        x1Var.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerNotifications$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetServerNotificationsOverrideSelector.Companion.launch(a.x(view2, "it", "it.context"), longExtra);
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        long longExtra = getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_GUILD_ID", -1L);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(longExtra), this, null, 2, null), WidgetServerNotifications.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerNotifications$onViewBoundOrOnResume$1(this));
        Observable notificationOverrides = Companion.getNotificationOverrides(longExtra);
        m.checkNotNullExpressionValue(notificationOverrides, "getNotificationOverrides(guildId)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationBuffered(notificationOverrides), this, null, 2, null), WidgetServerNotifications.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerNotifications$onViewBoundOrOnResume$2(this));
    }
}
