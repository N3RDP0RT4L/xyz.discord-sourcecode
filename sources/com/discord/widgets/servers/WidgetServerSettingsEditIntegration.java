package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetServerSettingsEditIntegrationBinding;
import com.discord.models.domain.ModelGuildIntegration;
import com.discord.models.guild.Guild;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimmer.DimmerView;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.views.RadioManager;
import com.discord.widgets.servers.WidgetServerSettingsEditIntegration;
import com.discord.widgets.servers.WidgetServerSettingsEditRole;
import d0.g;
import d0.t.n;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsEditIntegration.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\u0018\u0000 .2\u00020\u0001:\u0002./B\u0007¢\u0006\u0004\b-\u0010\u0017J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ%\u0010\u0010\u001a\u00020\u000f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u0006\u0010\u000e\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\u0017R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u001d\u0010!\u001a\u00020\u001c8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R#\u0010&\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R#\u0010)\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010#\u001a\u0004\b(\u0010%R\u0016\u0010*\u001a\u00020\u000f8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010,\u001a\u00020\u000f8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b,\u0010+¨\u00060"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;)V", "", "gracePeriodDays", "getGracePeriodPosition", "(I)I", "", "Lcom/discord/views/CheckedSetting;", "checkedSettings", "stateKey", "Lcom/discord/views/RadioManager;", "setupRadioManager", "(Ljava/util/List;I)Lcom/discord/views/RadioManager;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "onPause", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "Lcom/discord/databinding/WidgetServerSettingsEditIntegrationBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsEditIntegrationBinding;", "binding", "expiryBehaviorRadios$delegate", "Lkotlin/Lazy;", "getExpiryBehaviorRadios", "()Ljava/util/List;", "expiryBehaviorRadios", "gracePeriodRadios$delegate", "getGracePeriodRadios", "gracePeriodRadios", "expiryBehaviorRadioManager", "Lcom/discord/views/RadioManager;", "gracePeriodRadioManager", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEditIntegration extends AppFragment {
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String INTENT_EXTRA_INTEGRATION_ID = "INTENT_EXTRA_INTEGRATION_ID";
    private static final int STATE_KEY_ENABLE_TWITCH_EMOTES = 2131363119;
    private static final int STATE_KEY_EXPIRE_BEHAVIOR = 2131363122;
    private static final int STATE_KEY_GRACE_PERIOD = 2131363123;
    private RadioManager expiryBehaviorRadioManager;
    private RadioManager gracePeriodRadioManager;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsEditIntegration.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsEditIntegrationBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int[] gracePeriodDays = {1, 3, 7, 14, 30};
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsEditIntegration$binding$2.INSTANCE, null, 2, null);
    private final Lazy expiryBehaviorRadios$delegate = g.lazy(new WidgetServerSettingsEditIntegration$expiryBehaviorRadios$2(this));
    private final Lazy gracePeriodRadios$delegate = g.lazy(new WidgetServerSettingsEditIntegration$gracePeriodRadios$2(this));
    private final StatefulViews state = new StatefulViews(R.id.edit_integration_custom_emotes_toggle, R.id.edit_integration_expired_sub_container, R.id.edit_integration_grace_period_container);

    /* compiled from: WidgetServerSettingsEditIntegration.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0015\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018J)\u0010\t\u001a\u00020\b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\rR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u000f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0011R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "integrationId", "Landroid/content/Context;", "context", "", "launch", "(JJLandroid/content/Context;)V", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", WidgetServerSettingsEditIntegration.INTENT_EXTRA_INTEGRATION_ID, "", "STATE_KEY_ENABLE_TWITCH_EMOTES", "I", "STATE_KEY_EXPIRE_BEHAVIOR", "STATE_KEY_GRACE_PERIOD", "", "gracePeriodDays", "[I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(long j, long j2, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra("INTENT_EXTRA_GUILD_ID", j);
            intent.putExtra(WidgetServerSettingsEditIntegration.INTENT_EXTRA_INTEGRATION_ID, j2);
            j.d(context, WidgetServerSettingsEditIntegration.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsEditIntegration.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0082\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B!\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b\u001d\u0010\nR\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007¨\u0006#"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/domain/ModelGuildIntegration;", "component2", "()Lcom/discord/models/domain/ModelGuildIntegration;", "Lcom/discord/api/role/GuildRole;", "component3", "()Lcom/discord/api/role/GuildRole;", "guild", "integration", "role", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/ModelGuildIntegration;Lcom/discord/api/role/GuildRole;)Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/api/role/GuildRole;", "getRole", "Lcom/discord/models/domain/ModelGuildIntegration;", "getIntegration", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/domain/ModelGuildIntegration;Lcom/discord/api/role/GuildRole;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Guild guild;
        private final ModelGuildIntegration integration;
        private final GuildRole role;

        /* compiled from: WidgetServerSettingsEditIntegration.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bJ)\u0010\b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u00062\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "integrationId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;", "get", "(JJ)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j, long j2) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable g = Observable.g(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getGuilds().observeGuild(j), companion.getGuilds().observeRoles(j), companion.getPermissions().observePermissionsForGuild(j), companion.getGuildIntegrations().get(j, j2), WidgetServerSettingsEditIntegration$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(g, "Observable.combineLatest…  }\n          }\n        }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(g).q();
                m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Guild guild, ModelGuildIntegration modelGuildIntegration, GuildRole guildRole) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(modelGuildIntegration, "integration");
            this.guild = guild;
            this.integration = modelGuildIntegration;
            this.role = guildRole;
        }

        public static /* synthetic */ Model copy$default(Model model, Guild guild, ModelGuildIntegration modelGuildIntegration, GuildRole guildRole, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = model.guild;
            }
            if ((i & 2) != 0) {
                modelGuildIntegration = model.integration;
            }
            if ((i & 4) != 0) {
                guildRole = model.role;
            }
            return model.copy(guild, modelGuildIntegration, guildRole);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final ModelGuildIntegration component2() {
            return this.integration;
        }

        public final GuildRole component3() {
            return this.role;
        }

        public final Model copy(Guild guild, ModelGuildIntegration modelGuildIntegration, GuildRole guildRole) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(modelGuildIntegration, "integration");
            return new Model(guild, modelGuildIntegration, guildRole);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.guild, model.guild) && m.areEqual(this.integration, model.integration) && m.areEqual(this.role, model.role);
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final ModelGuildIntegration getIntegration() {
            return this.integration;
        }

        public final GuildRole getRole() {
            return this.role;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            ModelGuildIntegration modelGuildIntegration = this.integration;
            int hashCode2 = (hashCode + (modelGuildIntegration != null ? modelGuildIntegration.hashCode() : 0)) * 31;
            GuildRole guildRole = this.role;
            if (guildRole != null) {
                i = guildRole.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guild=");
            R.append(this.guild);
            R.append(", integration=");
            R.append(this.integration);
            R.append(", role=");
            R.append(this.role);
            R.append(")");
            return R.toString();
        }
    }

    public WidgetServerSettingsEditIntegration() {
        super(R.layout.widget_server_settings_edit_integration);
    }

    public static final /* synthetic */ RadioManager access$getExpiryBehaviorRadioManager$p(WidgetServerSettingsEditIntegration widgetServerSettingsEditIntegration) {
        RadioManager radioManager = widgetServerSettingsEditIntegration.expiryBehaviorRadioManager;
        if (radioManager == null) {
            m.throwUninitializedPropertyAccessException("expiryBehaviorRadioManager");
        }
        return radioManager;
    }

    public static final /* synthetic */ RadioManager access$getGracePeriodRadioManager$p(WidgetServerSettingsEditIntegration widgetServerSettingsEditIntegration) {
        RadioManager radioManager = widgetServerSettingsEditIntegration.gracePeriodRadioManager;
        if (radioManager == null) {
            m.throwUninitializedPropertyAccessException("gracePeriodRadioManager");
        }
        return radioManager;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        CharSequence g;
        if (model == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        setActionBarTitle(R.string.integration_settings);
        setActionBarSubtitle(model.getGuild().getName());
        boolean areEqual = m.areEqual(model.getIntegration().getType(), "twitch");
        TextView textView = getBinding().m;
        m.checkNotNullExpressionValue(textView, "binding.editIntegrationName");
        textView.setText(model.getIntegration().getDisplayName());
        TextView textView2 = getBinding().n;
        m.checkNotNullExpressionValue(textView2, "binding.editIntegrationOwnerName");
        textView2.setText(model.getIntegration().getUser().r());
        getBinding().l.setImageResource(areEqual ? R.drawable.asset_account_sync_twitch : R.drawable.asset_account_sync_youtube);
        getBinding().p.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditIntegration$configureUI$1

            /* compiled from: WidgetServerSettingsEditIntegration.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsEditIntegration$configureUI$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Void, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
                    invoke2(r1);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Void r5) {
                    WidgetServerSettingsEditIntegrationBinding binding;
                    binding = WidgetServerSettingsEditIntegration.this.getBinding();
                    DimmerView.setDimmed$default(binding.f2533b, true, false, 2, null);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Observable ui$default = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().syncIntegration(model.getGuild().getId(), model.getIntegration().getId()), false, 1, null), WidgetServerSettingsEditIntegration.this, null, 2, null);
                Class<?> cls = WidgetServerSettingsEditIntegration.this.getClass();
                m.checkNotNullExpressionValue(view, "view");
                ObservableExtensionsKt.appSubscribe(ui$default, cls, (r18 & 2) != 0 ? null : view.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
            }
        });
        int i = 0;
        View.OnClickListener onClickListener = null;
        DimmerView.setDimmed$default(getBinding().f2533b, model.getIntegration().isSyncing(), false, 2, null);
        TextView textView3 = getBinding().p.f81b;
        m.checkNotNullExpressionValue(textView3, "binding.syncSettings.editIntegrationLastSyncTime");
        WidgetServerSettingsEditIntegrationBinding binding = getBinding();
        m.checkNotNullExpressionValue(binding, "binding");
        CoordinatorLayout coordinatorLayout = binding.a;
        m.checkNotNullExpressionValue(coordinatorLayout, "binding.root");
        Context context = coordinatorLayout.getContext();
        m.checkNotNullExpressionValue(context, "binding.root.context");
        textView3.setText(TimeUtils.toReadableTimeString$default(context, TimeUtils.parseUTCDate(model.getIntegration().getSyncedAt()), null, 4, null));
        TextView textView4 = getBinding().p.c;
        m.checkNotNullExpressionValue(textView4, "binding.syncSettings.edi…ntegrationSubscriberCount");
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        WidgetServerSettingsEditIntegrationBinding binding2 = getBinding();
        m.checkNotNullExpressionValue(binding2, "binding");
        CoordinatorLayout coordinatorLayout2 = binding2.a;
        m.checkNotNullExpressionValue(coordinatorLayout2, "binding.root");
        Context context2 = coordinatorLayout2.getContext();
        m.checkNotNullExpressionValue(context2, "binding.root.context");
        g = b.g(StringResourceUtilsKt.getQuantityString(resources, context2, areEqual ? R.plurals.num_subscribers_subscribers : R.plurals.num_members_subscribers, model.getIntegration().getSubscriberCount(), Integer.valueOf(model.getIntegration().getSubscriberCount())), new Object[0], (r3 & 2) != 0 ? b.e.j : null);
        textView4.setText(g);
        RelativeLayout relativeLayout = getBinding().p.f;
        if (model.getRole() != null) {
            onClickListener = new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditIntegration$configureUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetServerSettingsEditRole.Companion companion = WidgetServerSettingsEditRole.Companion;
                    long id2 = WidgetServerSettingsEditIntegration.Model.this.getGuild().getId();
                    long id3 = WidgetServerSettingsEditIntegration.Model.this.getRole().getId();
                    m.checkNotNullExpressionValue(view, "v");
                    Context context3 = view.getContext();
                    m.checkNotNullExpressionValue(context3, "v.context");
                    companion.launch(id2, id3, context3);
                }
            };
        }
        relativeLayout.setOnClickListener(onClickListener);
        TextView textView5 = getBinding().p.e;
        m.checkNotNullExpressionValue(textView5, "binding.syncSettings.editIntegrationSyncedRole");
        textView5.setText(model.getRole() != null ? model.getRole().g() : "None");
        getBinding().p.e.setTextColor(RoleUtils.getOpaqueColor(model.getRole(), ColorCompat.getColor(this, (int) R.color.primary_300)));
        int intValue = ((Number) this.state.get(R.id.edit_integration_expired_sub_container, Integer.valueOf(model.getIntegration().getExpireBehavior()))).intValue();
        RadioManager radioManager = this.expiryBehaviorRadioManager;
        if (radioManager == null) {
            m.throwUninitializedPropertyAccessException("expiryBehaviorRadioManager");
        }
        radioManager.a(getExpiryBehaviorRadios().get(intValue));
        int intValue2 = ((Number) this.state.get(R.id.edit_integration_grace_period_container, Integer.valueOf(getGracePeriodPosition(model.getIntegration().getExpireGracePeriod())))).intValue();
        RadioManager radioManager2 = this.gracePeriodRadioManager;
        if (radioManager2 == null) {
            m.throwUninitializedPropertyAccessException("gracePeriodRadioManager");
        }
        radioManager2.a(getGracePeriodRadios().get(intValue2));
        LinearLayout linearLayout = getBinding().c;
        m.checkNotNullExpressionValue(linearLayout, "binding.editIntegrationCustomEmotesContainer");
        if (!areEqual) {
            i = 8;
        }
        linearLayout.setVisibility(i);
        CheckedSetting checkedSetting = getBinding().d;
        m.checkNotNullExpressionValue(checkedSetting, "binding.editIntegrationCustomEmotesToggle");
        StatefulViews statefulViews = this.state;
        CheckedSetting checkedSetting2 = getBinding().d;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.editIntegrationCustomEmotesToggle");
        checkedSetting.setChecked(((Boolean) statefulViews.get(checkedSetting2.getId(), Boolean.valueOf(model.getIntegration().isEnableEmoticons()))).booleanValue());
        this.state.configureSaveActionView(getBinding().o);
        getBinding().o.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditIntegration$configureUI$3

            /* compiled from: WidgetServerSettingsEditIntegration.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsEditIntegration$configureUI$3$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Void, Unit> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
                    invoke2(r1);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Void r1) {
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                int[] iArr;
                WidgetServerSettingsEditIntegrationBinding binding3;
                int b2 = WidgetServerSettingsEditIntegration.access$getExpiryBehaviorRadioManager$p(WidgetServerSettingsEditIntegration.this).b();
                iArr = WidgetServerSettingsEditIntegration.gracePeriodDays;
                int i2 = iArr[WidgetServerSettingsEditIntegration.access$getGracePeriodRadioManager$p(WidgetServerSettingsEditIntegration.this).b()];
                binding3 = WidgetServerSettingsEditIntegration.this.getBinding();
                CheckedSetting checkedSetting3 = binding3.d;
                m.checkNotNullExpressionValue(checkedSetting3, "binding.editIntegrationCustomEmotesToggle");
                Observable ui$default = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateGuildIntegration(model.getGuild().getId(), model.getIntegration().getId(), new RestAPIParams.GuildIntegration(b2, i2, checkedSetting3.isChecked())), false, 1, null), WidgetServerSettingsEditIntegration.this, null, 2, null);
                Class<?> cls = WidgetServerSettingsEditIntegration.this.getClass();
                m.checkNotNullExpressionValue(view, "view");
                ObservableExtensionsKt.appSubscribe(ui$default, cls, (r18 & 2) != 0 ? null : view.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, AnonymousClass1.INSTANCE);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsEditIntegrationBinding getBinding() {
        return (WidgetServerSettingsEditIntegrationBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final List<CheckedSetting> getExpiryBehaviorRadios() {
        return (List) this.expiryBehaviorRadios$delegate.getValue();
    }

    private final int getGracePeriodPosition(int i) {
        if (i == 1) {
            return 0;
        }
        if (i == 3) {
            return 1;
        }
        if (i == 7) {
            return 2;
        }
        if (i != 14) {
            return i != 30 ? 0 : 4;
        }
        return 3;
    }

    private final List<CheckedSetting> getGracePeriodRadios() {
        return (List) this.gracePeriodRadios$delegate.getValue();
    }

    private final RadioManager setupRadioManager(List<CheckedSetting> list, final int i) {
        final RadioManager radioManager = new RadioManager(list);
        final int i2 = 0;
        for (Object obj : list) {
            i2++;
            if (i2 < 0) {
                n.throwIndexOverflow();
            }
            final CheckedSetting checkedSetting = (CheckedSetting) obj;
            checkedSetting.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditIntegration$setupRadioManager$$inlined$forEachIndexed$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StatefulViews statefulViews;
                    StatefulViews statefulViews2;
                    WidgetServerSettingsEditIntegrationBinding binding;
                    radioManager.a(CheckedSetting.this);
                    statefulViews = this.state;
                    statefulViews.put(i, Integer.valueOf(i2));
                    statefulViews2 = this.state;
                    binding = this.getBinding();
                    statefulViews2.configureSaveActionView(binding.o);
                }
            });
        }
        return radioManager;
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        StoreStream.Companion.getGuildIntegrations().onIntegrationScreenClosed();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setRetainInstance(true);
        this.expiryBehaviorRadioManager = setupRadioManager(getExpiryBehaviorRadios(), R.id.edit_integration_expired_sub_container);
        this.gracePeriodRadioManager = setupRadioManager(getGracePeriodRadios(), R.id.edit_integration_grace_period_container);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        this.state.setupUnsavedChangesConfirmation(this);
        List<CheckedSetting> gracePeriodRadios = getGracePeriodRadios();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(gracePeriodRadios, 10));
        int i = 0;
        for (Object obj : gracePeriodRadios) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            CheckedSetting checkedSetting = (CheckedSetting) obj;
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            Context context = checkedSetting.getContext();
            m.checkNotNullExpressionValue(context, "radio.context");
            int[] iArr = gracePeriodDays;
            checkedSetting.setText(StringResourceUtilsKt.getQuantityString(resources, context, (int) R.plurals.n_days_days, iArr[i], Integer.valueOf(iArr[i])));
            arrayList.add(Unit.a);
        }
        getBinding().d.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditIntegration$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetServerSettingsEditIntegrationBinding binding;
                StatefulViews statefulViews;
                WidgetServerSettingsEditIntegrationBinding binding2;
                StatefulViews statefulViews2;
                WidgetServerSettingsEditIntegrationBinding binding3;
                binding = WidgetServerSettingsEditIntegration.this.getBinding();
                binding.d.toggle();
                statefulViews = WidgetServerSettingsEditIntegration.this.state;
                binding2 = WidgetServerSettingsEditIntegration.this.getBinding();
                CheckedSetting checkedSetting2 = binding2.d;
                m.checkNotNullExpressionValue(checkedSetting2, "binding.editIntegrationCustomEmotesToggle");
                statefulViews.put(R.id.edit_integration_custom_emotes_toggle, Boolean.valueOf(checkedSetting2.isChecked()));
                statefulViews2 = WidgetServerSettingsEditIntegration.this.state;
                binding3 = WidgetServerSettingsEditIntegration.this.getBinding();
                statefulViews2.configureSaveActionView(binding3.o);
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        long longExtra = getMostRecentIntent().getLongExtra("INTENT_EXTRA_GUILD_ID", -1L);
        long longExtra2 = getMostRecentIntent().getLongExtra(INTENT_EXTRA_INTEGRATION_ID, -1L);
        StoreStream.Companion.getGuildIntegrations().onIntegrationScreenOpened(longExtra);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(longExtra, longExtra2), this, null, 2, null), WidgetServerSettingsEditIntegration.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsEditIntegration$onViewBoundOrOnResume$1(this));
    }
}
