package com.discord.widgets.servers.guild_role_subscription;

import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.widgets.servers.guild_role_subscription.ServerSettingsGuildRoleSubscriptionViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ServerSettingsGuildRoleSubscriptionViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;", "it", "", "invoke", "(Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsGuildRoleSubscriptionViewModel$submitChanges$1 extends o implements Function1<GuildRoleSubscriptionGroupListing, Unit> {
    public final /* synthetic */ ServerSettingsGuildRoleSubscriptionViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ ServerSettingsGuildRoleSubscriptionViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerSettingsGuildRoleSubscriptionViewModel$submitChanges$1(ServerSettingsGuildRoleSubscriptionViewModel serverSettingsGuildRoleSubscriptionViewModel, ServerSettingsGuildRoleSubscriptionViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = serverSettingsGuildRoleSubscriptionViewModel;
        this.$viewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing) {
        invoke2(guildRoleSubscriptionGroupListing);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing) {
        m.checkNotNullParameter(guildRoleSubscriptionGroupListing, "it");
        this.this$0.updateViewState(ServerSettingsGuildRoleSubscriptionViewModel.ViewState.Loaded.copy$default(this.$viewState, false, false, 0L, 4, null));
        this.this$0.emitEvent(ServerSettingsGuildRoleSubscriptionViewModel.Event.SubmitSuccess.INSTANCE);
    }
}
