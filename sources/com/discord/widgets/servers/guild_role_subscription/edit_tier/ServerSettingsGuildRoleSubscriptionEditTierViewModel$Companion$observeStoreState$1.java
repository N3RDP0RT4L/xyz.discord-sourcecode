package com.discord.widgets.servers.guild_role_subscription.edit_tier;

import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildRoleSubscriptions;
import com.discord.stores.StoreGuilds;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Triple;
import kotlin.jvm.functions.Function0;
/* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\n\u001a8\u0012\u0006\u0012\u0004\u0018\u00010\u0001\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00020\u0000H\n¢\u0006\u0004\b\b\u0010\t"}, d2 = {"Lkotlin/Triple;", "Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "invoke", "()Lkotlin/Triple;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion$observeStoreState$1 extends o implements Function0<Triple<? extends StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState, ? extends Map<Long, ? extends Channel>, ? extends Map<Long, ? extends GuildRole>>> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreChannels $storeChannels;
    public final /* synthetic */ StoreGuildRoleSubscriptions $storeGuildRoleSubscriptions;
    public final /* synthetic */ StoreGuilds $storeGuilds;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion$observeStoreState$1(StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, long j, StoreChannels storeChannels, StoreGuilds storeGuilds) {
        super(0);
        this.$storeGuildRoleSubscriptions = storeGuildRoleSubscriptions;
        this.$guildId = j;
        this.$storeChannels = storeChannels;
        this.$storeGuilds = storeGuilds;
    }

    @Override // kotlin.jvm.functions.Function0
    public final Triple<? extends StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState, ? extends Map<Long, ? extends Channel>, ? extends Map<Long, ? extends GuildRole>> invoke() {
        return new Triple<>(this.$storeGuildRoleSubscriptions.getGuildRoleSubscriptionState(this.$guildId), this.$storeChannels.getChannelsForGuild(this.$guildId), this.$storeGuilds.getRoles().get(Long.valueOf(this.$guildId)));
    }
}
