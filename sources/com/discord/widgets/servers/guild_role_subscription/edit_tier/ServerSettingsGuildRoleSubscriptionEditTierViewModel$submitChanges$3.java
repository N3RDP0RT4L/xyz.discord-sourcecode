package com.discord.widgets.servers.guild_role_subscription.edit_tier;

import com.discord.utilities.error.Error;
import com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsGuildRoleSubscriptionEditTierViewModel$submitChanges$3 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ ServerSettingsGuildRoleSubscriptionEditTierViewModel.ViewState.Loaded $viewState;
    public final /* synthetic */ ServerSettingsGuildRoleSubscriptionEditTierViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerSettingsGuildRoleSubscriptionEditTierViewModel$submitChanges$3(ServerSettingsGuildRoleSubscriptionEditTierViewModel serverSettingsGuildRoleSubscriptionEditTierViewModel, ServerSettingsGuildRoleSubscriptionEditTierViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = serverSettingsGuildRoleSubscriptionEditTierViewModel;
        this.$viewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        m.checkNotNullParameter(error, "error");
        this.this$0.updateViewState(ServerSettingsGuildRoleSubscriptionEditTierViewModel.ViewState.Loaded.copy$default(this.$viewState, false, false, null, false, 7, null));
        this.this$0.emitEvent(new ServerSettingsGuildRoleSubscriptionEditTierViewModel.Event.SubmitFailure(error));
    }
}
