package com.discord.widgets.servers.guild_role_subscription.edit_tier;

import com.discord.app.AppViewModel;
import com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetServerSettingsGuildRoleSubscriptionEditTier.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsGuildRoleSubscriptionEditTier$viewModel$2 extends o implements Function0<AppViewModel<ServerSettingsGuildRoleSubscriptionEditTierViewModel.ViewState>> {
    public final /* synthetic */ WidgetServerSettingsGuildRoleSubscriptionEditTier this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsGuildRoleSubscriptionEditTier$viewModel$2(WidgetServerSettingsGuildRoleSubscriptionEditTier widgetServerSettingsGuildRoleSubscriptionEditTier) {
        super(0);
        this.this$0 = widgetServerSettingsGuildRoleSubscriptionEditTier;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<ServerSettingsGuildRoleSubscriptionEditTierViewModel.ViewState> invoke() {
        long guildId;
        long guildRoleSubscriptionTierListingId;
        long guildRoleSubscriptionGroupListingId;
        guildId = this.this$0.getGuildId();
        guildRoleSubscriptionTierListingId = this.this$0.getGuildRoleSubscriptionTierListingId();
        guildRoleSubscriptionGroupListingId = this.this$0.getGuildRoleSubscriptionGroupListingId();
        return new ServerSettingsGuildRoleSubscriptionEditTierViewModel(guildId, guildRoleSubscriptionTierListingId, guildRoleSubscriptionGroupListingId, null, null, null, null, null, null, 504, null);
    }
}
