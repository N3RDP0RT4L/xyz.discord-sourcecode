package com.discord.widgets.servers.guild_role_subscription.edit_tier;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefit;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionBenefitType;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionTierListing;
import com.discord.api.guildrolesubscription.ImageAsset;
import com.discord.api.permission.Permission;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.domain.emoji.Emoji;
import com.discord.models.domain.emoji.EmojiSet;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreEmoji;
import com.discord.stores.StoreGuildRoleSubscriptions;
import com.discord.stores.StoreGuilds;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.channel.GuildChannelIconUtilsKt;
import com.discord.utilities.error.Error;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtilsKt;
import com.discord.widgets.guild_role_subscriptions.tier.model.Benefit;
import com.discord.widgets.guild_role_subscriptions.tier.model.GuildRoleSubscriptionTier;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.PublishSubject;
/* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¸\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 R2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004RSTUBe\u0012\n\u0010;\u001a\u00060\u000ej\u0002`:\u0012\u0006\u0010@\u001a\u00020\u000e\u0012\u0006\u0010A\u001a\u00020\u000e\u0012\b\b\u0002\u0010>\u001a\u00020=\u0012\b\b\u0002\u0010(\u001a\u00020'\u0012\b\b\u0002\u0010J\u001a\u00020I\u0012\b\b\u0002\u0010L\u001a\u00020K\u0012\b\b\u0002\u0010N\u001a\u00020M\u0012\u000e\b\u0002\u0010O\u001a\b\u0012\u0004\u0012\u00020\b0\u0019¢\u0006\u0004\bP\u0010QJ\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\b\n\u0010\u000bJQ\u0010\u0017\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\f2\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u000f\u0012\u0004\u0012\u00020\u00100\r2\u0006\u0010\u0013\u001a\u00020\u00122\u0018\u0010\u0016\u001a\u0014\u0012\b\u0012\u00060\u000ej\u0002`\u0014\u0012\u0004\u0012\u00020\u0015\u0018\u00010\rH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0013\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00030\u0019¢\u0006\u0004\b\u001a\u0010\u001bJ\u0015\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ\r\u0010 \u001a\u00020\u0005¢\u0006\u0004\b \u0010!J\r\u0010\"\u001a\u00020\u0005¢\u0006\u0004\b\"\u0010!J\r\u0010#\u001a\u00020\u0005¢\u0006\u0004\b#\u0010!R\u0018\u0010%\u001a\u0004\u0018\u00010$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010(\u001a\u00020'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b(\u0010)R\u0018\u0010+\u001a\u0004\u0018\u00010*8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b+\u0010,R\u0018\u0010-\u001a\u0004\u0018\u00010$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010&R\u0018\u0010.\u001a\u0004\u0018\u00010$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010&R:\u00101\u001a&\u0012\f\u0012\n 0*\u0004\u0018\u00010\u00030\u0003 0*\u0012\u0012\f\u0012\n 0*\u0004\u0018\u00010\u00030\u0003\u0018\u00010/0/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102R\u0018\u00103\u001a\u0004\u0018\u00010*8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b3\u0010,R\u001e\u00106\u001a\n\u0012\u0004\u0012\u000205\u0018\u0001048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u00107R\u0018\u00108\u001a\u0004\u0018\u00010\u001c8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b8\u00109R\u001a\u0010;\u001a\u00060\u000ej\u0002`:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010@\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b@\u0010<R\u0016\u0010A\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010<R\u001e\u0010C\u001a\n\u0012\u0004\u0012\u00020B\u0018\u0001048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u00107R\u0018\u0010E\u001a\u0004\u0018\u00010D8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bE\u0010FR\u0018\u0010G\u001a\u0004\u0018\u00010$8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bG\u0010&R\u0018\u0010H\u001a\u0004\u0018\u00010D8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bH\u0010F¨\u0006V"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event;", "event", "", "emitEvent", "(Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event;)V", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;)V", "Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState$Loaded;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "channels", "Lcom/discord/models/domain/emoji/EmojiSet;", "emojiSet", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "handleLoadedStoreState", "(Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState$Loaded;Ljava/util/Map;Lcom/discord/models/domain/emoji/EmojiSet;Ljava/util/Map;)V", "Lrx/Observable;", "observeEvents", "()Lrx/Observable;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "guildRoleSubscriptionTier", "onTierUpdated", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;)V", "submitChanges", "()V", "publishTier", "deleteTier", "", "editedMemberIcon", "Ljava/lang/String;", "Lcom/discord/stores/StoreGuildRoleSubscriptions;", "storeGuildRoleSubscriptions", "Lcom/discord/stores/StoreGuildRoleSubscriptions;", "", "editedCanAccessAllChannelsFlag", "Ljava/lang/Boolean;", "editedTierName", "editedTierImage", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "isFullServerGating", "", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$ChannelBenefit;", "editedChannelBenefits", "Ljava/util/List;", "storedGuildRoleSubscriptionTier", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "Lcom/discord/primitives/GuildId;", "guildId", "J", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "Lcom/discord/utilities/rest/RestAPI;", "guildRoleSubscriptionTierListingId", "guildRoleSubscriptionGroupListingId", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/Benefit$IntangibleBenefit;", "editedIntangibleBenefits", "", "editedTierPrice", "Ljava/lang/Integer;", "editedTierDescription", "editedMemberColor", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreEmoji;", "storeEmoji", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "storeStateObservable", HookHelper.constructorName, "(JJJLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuildRoleSubscriptions;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreEmoji;Lcom/discord/stores/StoreGuilds;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsGuildRoleSubscriptionEditTierViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private Boolean editedCanAccessAllChannelsFlag;
    private List<Benefit.ChannelBenefit> editedChannelBenefits;
    private List<Benefit.IntangibleBenefit> editedIntangibleBenefits;
    private Integer editedMemberColor;
    private String editedMemberIcon;
    private String editedTierDescription;
    private String editedTierImage;
    private String editedTierName;
    private Integer editedTierPrice;
    private final PublishSubject<Event> eventSubject;
    private final long guildId;
    private final long guildRoleSubscriptionGroupListingId;
    private final long guildRoleSubscriptionTierListingId;
    private Boolean isFullServerGating;
    private final RestAPI restApi;
    private final StoreGuildRoleSubscriptions storeGuildRoleSubscriptions;
    private GuildRoleSubscriptionTier storedGuildRoleSubscriptionTier;

    /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            ServerSettingsGuildRoleSubscriptionEditTierViewModel serverSettingsGuildRoleSubscriptionEditTierViewModel = ServerSettingsGuildRoleSubscriptionEditTierViewModel.this;
            m.checkNotNullExpressionValue(storeState, "storeState");
            serverSettingsGuildRoleSubscriptionEditTierViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014JI\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreGuildRoleSubscriptions;", "storeGuildRoleSubscriptions", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreEmoji;", "storeEmoji", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lrx/Observable;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreGuildRoleSubscriptions;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreEmoji;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(long j, ObservationDeck observationDeck, StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, StoreChannels storeChannels, StoreEmoji storeEmoji, StoreGuilds storeGuilds) {
            Observable<StoreState> j2 = Observable.j(storeEmoji.getEmojiSet(new StoreEmoji.EmojiContext.Guild(j), false, false), ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeGuildRoleSubscriptions, storeChannels, storeGuilds}, false, null, null, new ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion$observeStoreState$1(storeGuildRoleSubscriptions, j, storeChannels, storeGuilds), 14, null), ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion$observeStoreState$2.INSTANCE);
            m.checkNotNullExpressionValue(j2, "Observable.combineLatest…te, guildRoleMap)\n      }");
            return j2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\u0004\u0005\u0006\u0007\bB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0005\t\n\u000b\f\r¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event;", "", HookHelper.constructorName, "()V", "DeleteFailure", "DeleteSuccess", "StoredStateUpdate", "SubmitFailure", "SubmitSuccess", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$StoredStateUpdate;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$SubmitSuccess;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$SubmitFailure;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$DeleteSuccess;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$DeleteFailure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$DeleteFailure;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event;", "Lcom/discord/utilities/error/Error;", "component1", "()Lcom/discord/utilities/error/Error;", "error", "copy", "(Lcom/discord/utilities/error/Error;)Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$DeleteFailure;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/error/Error;", "getError", HookHelper.constructorName, "(Lcom/discord/utilities/error/Error;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DeleteFailure extends Event {
            private final Error error;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public DeleteFailure(Error error) {
                super(null);
                m.checkNotNullParameter(error, "error");
                this.error = error;
            }

            public static /* synthetic */ DeleteFailure copy$default(DeleteFailure deleteFailure, Error error, int i, Object obj) {
                if ((i & 1) != 0) {
                    error = deleteFailure.error;
                }
                return deleteFailure.copy(error);
            }

            public final Error component1() {
                return this.error;
            }

            public final DeleteFailure copy(Error error) {
                m.checkNotNullParameter(error, "error");
                return new DeleteFailure(error);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof DeleteFailure) && m.areEqual(this.error, ((DeleteFailure) obj).error);
                }
                return true;
            }

            public final Error getError() {
                return this.error;
            }

            public int hashCode() {
                Error error = this.error;
                if (error != null) {
                    return error.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("DeleteFailure(error=");
                R.append(this.error);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$DeleteSuccess;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class DeleteSuccess extends Event {
            public static final DeleteSuccess INSTANCE = new DeleteSuccess();

            private DeleteSuccess() {
                super(null);
            }
        }

        /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\t\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$StoredStateUpdate;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event;", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "component1", "()Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "", "component2", "()Z", "guildRoleSubscriptionTier", "isFullServerGating", "copy", "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;Z)Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$StoredStateUpdate;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;", "getGuildRoleSubscriptionTier", "Z", HookHelper.constructorName, "(Lcom/discord/widgets/guild_role_subscriptions/tier/model/GuildRoleSubscriptionTier;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class StoredStateUpdate extends Event {
            private final GuildRoleSubscriptionTier guildRoleSubscriptionTier;
            private final boolean isFullServerGating;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public StoredStateUpdate(GuildRoleSubscriptionTier guildRoleSubscriptionTier, boolean z2) {
                super(null);
                m.checkNotNullParameter(guildRoleSubscriptionTier, "guildRoleSubscriptionTier");
                this.guildRoleSubscriptionTier = guildRoleSubscriptionTier;
                this.isFullServerGating = z2;
            }

            public static /* synthetic */ StoredStateUpdate copy$default(StoredStateUpdate storedStateUpdate, GuildRoleSubscriptionTier guildRoleSubscriptionTier, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    guildRoleSubscriptionTier = storedStateUpdate.guildRoleSubscriptionTier;
                }
                if ((i & 2) != 0) {
                    z2 = storedStateUpdate.isFullServerGating;
                }
                return storedStateUpdate.copy(guildRoleSubscriptionTier, z2);
            }

            public final GuildRoleSubscriptionTier component1() {
                return this.guildRoleSubscriptionTier;
            }

            public final boolean component2() {
                return this.isFullServerGating;
            }

            public final StoredStateUpdate copy(GuildRoleSubscriptionTier guildRoleSubscriptionTier, boolean z2) {
                m.checkNotNullParameter(guildRoleSubscriptionTier, "guildRoleSubscriptionTier");
                return new StoredStateUpdate(guildRoleSubscriptionTier, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof StoredStateUpdate)) {
                    return false;
                }
                StoredStateUpdate storedStateUpdate = (StoredStateUpdate) obj;
                return m.areEqual(this.guildRoleSubscriptionTier, storedStateUpdate.guildRoleSubscriptionTier) && this.isFullServerGating == storedStateUpdate.isFullServerGating;
            }

            public final GuildRoleSubscriptionTier getGuildRoleSubscriptionTier() {
                return this.guildRoleSubscriptionTier;
            }

            public int hashCode() {
                GuildRoleSubscriptionTier guildRoleSubscriptionTier = this.guildRoleSubscriptionTier;
                int hashCode = (guildRoleSubscriptionTier != null ? guildRoleSubscriptionTier.hashCode() : 0) * 31;
                boolean z2 = this.isFullServerGating;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public final boolean isFullServerGating() {
                return this.isFullServerGating;
            }

            public String toString() {
                StringBuilder R = a.R("StoredStateUpdate(guildRoleSubscriptionTier=");
                R.append(this.guildRoleSubscriptionTier);
                R.append(", isFullServerGating=");
                return a.M(R, this.isFullServerGating, ")");
            }
        }

        /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$SubmitFailure;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event;", "Lcom/discord/utilities/error/Error;", "component1", "()Lcom/discord/utilities/error/Error;", "error", "copy", "(Lcom/discord/utilities/error/Error;)Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$SubmitFailure;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/error/Error;", "getError", HookHelper.constructorName, "(Lcom/discord/utilities/error/Error;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SubmitFailure extends Event {
            private final Error error;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public SubmitFailure(Error error) {
                super(null);
                m.checkNotNullParameter(error, "error");
                this.error = error;
            }

            public static /* synthetic */ SubmitFailure copy$default(SubmitFailure submitFailure, Error error, int i, Object obj) {
                if ((i & 1) != 0) {
                    error = submitFailure.error;
                }
                return submitFailure.copy(error);
            }

            public final Error component1() {
                return this.error;
            }

            public final SubmitFailure copy(Error error) {
                m.checkNotNullParameter(error, "error");
                return new SubmitFailure(error);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof SubmitFailure) && m.areEqual(this.error, ((SubmitFailure) obj).error);
                }
                return true;
            }

            public final Error getError() {
                return this.error;
            }

            public int hashCode() {
                Error error = this.error;
                if (error != null) {
                    return error.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("SubmitFailure(error=");
                R.append(this.error);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event$SubmitSuccess;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SubmitSuccess extends Event {
            public static final SubmitSuccess INSTANCE = new SubmitSuccess();

            private SubmitSuccess() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001BK\u0012\u0016\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002\u0012\u0006\u0010\u0012\u001a\u00020\b\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u000b\u0012\u0018\u0010\u0014\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u0002¢\u0006\u0004\b(\u0010)J \u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\"\u0010\u0010\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0007J\\\u0010\u0015\u001a\u00020\u00002\u0018\b\u0002\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00022\b\b\u0002\u0010\u0012\u001a\u00020\b2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u000b2\u001a\b\u0002\u0010\u0014\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010\u001f\u001a\u00020\u001e2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010!\u001a\u0004\b\"\u0010\rR\u0019\u0010\u0012\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\nR)\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010%\u001a\u0004\b&\u0010\u0007R+\u0010\u0014\u001a\u0014\u0012\b\u0012\u00060\u0003j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010%\u001a\u0004\b'\u0010\u0007¨\u0006*"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;", "", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component1", "()Ljava/util/Map;", "Lcom/discord/models/domain/emoji/EmojiSet;", "component2", "()Lcom/discord/models/domain/emoji/EmojiSet;", "Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;", "component3", "()Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "component4", "channels", "guildEmojis", "guildRoleSubscriptionGroupListingState", "guildRoles", "copy", "(Ljava/util/Map;Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;Ljava/util/Map;)Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;", "getGuildRoleSubscriptionGroupListingState", "Lcom/discord/models/domain/emoji/EmojiSet;", "getGuildEmojis", "Ljava/util/Map;", "getChannels", "getGuildRoles", HookHelper.constructorName, "(Ljava/util/Map;Lcom/discord/models/domain/emoji/EmojiSet;Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Map<Long, Channel> channels;
        private final EmojiSet guildEmojis;
        private final StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupListingState;
        private final Map<Long, GuildRole> guildRoles;

        public StoreState(Map<Long, Channel> map, EmojiSet emojiSet, StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState, Map<Long, GuildRole> map2) {
            m.checkNotNullParameter(map, "channels");
            m.checkNotNullParameter(emojiSet, "guildEmojis");
            this.channels = map;
            this.guildEmojis = emojiSet;
            this.guildRoleSubscriptionGroupListingState = guildRoleSubscriptionGroupState;
            this.guildRoles = map2;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, Map map, EmojiSet emojiSet, StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState, Map map2, int i, Object obj) {
            if ((i & 1) != 0) {
                map = storeState.channels;
            }
            if ((i & 2) != 0) {
                emojiSet = storeState.guildEmojis;
            }
            if ((i & 4) != 0) {
                guildRoleSubscriptionGroupState = storeState.guildRoleSubscriptionGroupListingState;
            }
            if ((i & 8) != 0) {
                map2 = storeState.guildRoles;
            }
            return storeState.copy(map, emojiSet, guildRoleSubscriptionGroupState, map2);
        }

        public final Map<Long, Channel> component1() {
            return this.channels;
        }

        public final EmojiSet component2() {
            return this.guildEmojis;
        }

        public final StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState component3() {
            return this.guildRoleSubscriptionGroupListingState;
        }

        public final Map<Long, GuildRole> component4() {
            return this.guildRoles;
        }

        public final StoreState copy(Map<Long, Channel> map, EmojiSet emojiSet, StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState, Map<Long, GuildRole> map2) {
            m.checkNotNullParameter(map, "channels");
            m.checkNotNullParameter(emojiSet, "guildEmojis");
            return new StoreState(map, emojiSet, guildRoleSubscriptionGroupState, map2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.channels, storeState.channels) && m.areEqual(this.guildEmojis, storeState.guildEmojis) && m.areEqual(this.guildRoleSubscriptionGroupListingState, storeState.guildRoleSubscriptionGroupListingState) && m.areEqual(this.guildRoles, storeState.guildRoles);
        }

        public final Map<Long, Channel> getChannels() {
            return this.channels;
        }

        public final EmojiSet getGuildEmojis() {
            return this.guildEmojis;
        }

        public final StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState getGuildRoleSubscriptionGroupListingState() {
            return this.guildRoleSubscriptionGroupListingState;
        }

        public final Map<Long, GuildRole> getGuildRoles() {
            return this.guildRoles;
        }

        public int hashCode() {
            Map<Long, Channel> map = this.channels;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            EmojiSet emojiSet = this.guildEmojis;
            int hashCode2 = (hashCode + (emojiSet != null ? emojiSet.hashCode() : 0)) * 31;
            StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState = this.guildRoleSubscriptionGroupListingState;
            int hashCode3 = (hashCode2 + (guildRoleSubscriptionGroupState != null ? guildRoleSubscriptionGroupState.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map2 = this.guildRoles;
            if (map2 != null) {
                i = map2.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(channels=");
            R.append(this.channels);
            R.append(", guildEmojis=");
            R.append(this.guildEmojis);
            R.append(", guildRoleSubscriptionGroupListingState=");
            R.append(this.guildRoleSubscriptionGroupListingState);
            R.append(", guildRoles=");
            return a.L(R, this.guildRoles, ")");
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Failed", "Loaded", "Loading", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState$Loaded;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState$Failed;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState$Failed;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Failed extends ViewState {
            public static final Failed INSTANCE = new Failed();

            private Failed() {
                super(null);
            }
        }

        /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\b\b\u0002\u0010\n\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0002\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J:\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\r\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0010\u0010\bJ\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00022\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001a\u001a\u0004\b\r\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u000b\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState$Loaded;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState;", "", "component1", "()Z", "component2", "", "component3", "()Ljava/lang/String;", "component4", "hasChanges", "isPublished", "tierName", "isSubmitting", "copy", "(ZZLjava/lang/String;Z)Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState$Loaded;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getTierName", "Z", "getHasChanges", HookHelper.constructorName, "(ZZLjava/lang/String;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean hasChanges;
            private final boolean isPublished;
            private final boolean isSubmitting;
            private final String tierName;

            public Loaded() {
                this(false, false, null, false, 15, null);
            }

            public /* synthetic */ Loaded(boolean z2, boolean z3, String str, boolean z4, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? false : z3, (i & 4) != 0 ? null : str, (i & 8) != 0 ? false : z4);
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, boolean z2, boolean z3, String str, boolean z4, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = loaded.hasChanges;
                }
                if ((i & 2) != 0) {
                    z3 = loaded.isPublished;
                }
                if ((i & 4) != 0) {
                    str = loaded.tierName;
                }
                if ((i & 8) != 0) {
                    z4 = loaded.isSubmitting;
                }
                return loaded.copy(z2, z3, str, z4);
            }

            public final boolean component1() {
                return this.hasChanges;
            }

            public final boolean component2() {
                return this.isPublished;
            }

            public final String component3() {
                return this.tierName;
            }

            public final boolean component4() {
                return this.isSubmitting;
            }

            public final Loaded copy(boolean z2, boolean z3, String str, boolean z4) {
                return new Loaded(z2, z3, str, z4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return this.hasChanges == loaded.hasChanges && this.isPublished == loaded.isPublished && m.areEqual(this.tierName, loaded.tierName) && this.isSubmitting == loaded.isSubmitting;
            }

            public final boolean getHasChanges() {
                return this.hasChanges;
            }

            public final String getTierName() {
                return this.tierName;
            }

            public int hashCode() {
                boolean z2 = this.hasChanges;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = i2 * 31;
                boolean z3 = this.isPublished;
                if (z3) {
                    z3 = true;
                }
                int i5 = z3 ? 1 : 0;
                int i6 = z3 ? 1 : 0;
                int i7 = (i4 + i5) * 31;
                String str = this.tierName;
                int hashCode = (i7 + (str != null ? str.hashCode() : 0)) * 31;
                boolean z4 = this.isSubmitting;
                if (!z4) {
                    i = z4 ? 1 : 0;
                }
                return hashCode + i;
            }

            public final boolean isPublished() {
                return this.isPublished;
            }

            public final boolean isSubmitting() {
                return this.isSubmitting;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(hasChanges=");
                R.append(this.hasChanges);
                R.append(", isPublished=");
                R.append(this.isPublished);
                R.append(", tierName=");
                R.append(this.tierName);
                R.append(", isSubmitting=");
                return a.M(R, this.isSubmitting, ")");
            }

            public Loaded(boolean z2, boolean z3, String str, boolean z4) {
                super(null);
                this.hasChanges = z2;
                this.isPublished = z3;
                this.tierName = str;
                this.isSubmitting = z4;
            }
        }

        /* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            GuildRoleSubscriptionBenefitType.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[GuildRoleSubscriptionBenefitType.INTANGIBLE.ordinal()] = 1;
            iArr[GuildRoleSubscriptionBenefitType.CHANNEL.ordinal()] = 2;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ ServerSettingsGuildRoleSubscriptionEditTierViewModel(long r16, long r18, long r20, com.discord.utilities.rest.RestAPI r22, com.discord.stores.StoreGuildRoleSubscriptions r23, com.discord.stores.StoreChannels r24, com.discord.stores.StoreEmoji r25, com.discord.stores.StoreGuilds r26, rx.Observable r27, int r28, kotlin.jvm.internal.DefaultConstructorMarker r29) {
        /*
            r15 = this;
            r0 = r28
            r1 = r0 & 8
            if (r1 == 0) goto Le
            com.discord.utilities.rest.RestAPI$Companion r1 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r1 = r1.getApi()
            r9 = r1
            goto L10
        Le:
            r9 = r22
        L10:
            r1 = r0 & 16
            if (r1 == 0) goto L1c
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildRoleSubscriptions r1 = r1.getGuildRoleSubscriptions()
            r10 = r1
            goto L1e
        L1c:
            r10 = r23
        L1e:
            r1 = r0 & 32
            if (r1 == 0) goto L2a
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r1 = r1.getChannels()
            r11 = r1
            goto L2c
        L2a:
            r11 = r24
        L2c:
            r1 = r0 & 64
            if (r1 == 0) goto L38
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreEmoji r1 = r1.getEmojis()
            r12 = r1
            goto L3a
        L38:
            r12 = r25
        L3a:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L46
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r1 = r1.getGuilds()
            r13 = r1
            goto L48
        L46:
            r13 = r26
        L48:
            r0 = r0 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L66
            com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion r0 = com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel.Companion
            com.discord.stores.updates.ObservationDeck r1 = com.discord.stores.updates.ObservationDeckProvider.get()
            r22 = r0
            r23 = r16
            r25 = r1
            r26 = r10
            r27 = r11
            r28 = r12
            r29 = r13
            rx.Observable r0 = com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel.Companion.access$observeStoreState(r22, r23, r25, r26, r27, r28, r29)
            r14 = r0
            goto L68
        L66:
            r14 = r27
        L68:
            r2 = r15
            r3 = r16
            r5 = r18
            r7 = r20
            r2.<init>(r3, r5, r7, r9, r10, r11, r12, r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel.<init>(long, long, long, com.discord.utilities.rest.RestAPI, com.discord.stores.StoreGuildRoleSubscriptions, com.discord.stores.StoreChannels, com.discord.stores.StoreEmoji, com.discord.stores.StoreGuilds, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void emitEvent(Event event) {
        this.eventSubject.k.onNext(event);
    }

    private final void handleLoadedStoreState(StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded loaded, Map<Long, Channel> map, EmojiSet emojiSet, Map<Long, GuildRole> map2) {
        List<GuildRoleSubscriptionTierListing> h;
        Long l;
        Object obj;
        String str;
        boolean z2;
        GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing = loaded.getGuildRoleSubscriptionGroupListing();
        if (!(guildRoleSubscriptionGroupListing == null || (h = guildRoleSubscriptionGroupListing.h()) == null)) {
            Iterator<T> it = h.iterator();
            while (true) {
                l = null;
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((GuildRoleSubscriptionTierListing) obj).c() == this.guildRoleSubscriptionTierListingId) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing = (GuildRoleSubscriptionTierListing) obj;
            if (guildRoleSubscriptionTierListing != null) {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                for (GuildRoleSubscriptionBenefit guildRoleSubscriptionBenefit : guildRoleSubscriptionTierListing.g().a()) {
                    Long b2 = guildRoleSubscriptionBenefit.b();
                    if (b2 == null || (str = String.valueOf(b2.longValue())) == null) {
                        str = guildRoleSubscriptionBenefit.c();
                    }
                    Emoji emoji = emojiSet.emojiIndex.get(str);
                    if (emoji != null) {
                        int ordinal = guildRoleSubscriptionBenefit.f().ordinal();
                        if (ordinal == 1) {
                            Channel channel = map.get(guildRoleSubscriptionBenefit.e());
                            if (channel != null) {
                                arrayList.add(new Benefit.ChannelBenefit(ChannelUtils.c(channel), emoji, guildRoleSubscriptionBenefit.a(), Integer.valueOf(GuildChannelIconUtilsKt.guildChannelIcon(channel)), channel.h()));
                            }
                        } else if (ordinal == 2) {
                            arrayList2.add(new Benefit.IntangibleBenefit(guildRoleSubscriptionBenefit.d(), emoji, guildRoleSubscriptionBenefit.a()));
                        }
                    }
                }
                GuildRole guildRole = map2 != null ? map2.get(Long.valueOf(guildRoleSubscriptionTierListing.h())) : null;
                ViewState requireViewState = requireViewState();
                if (!(requireViewState instanceof ViewState.Loaded)) {
                    requireViewState = null;
                }
                ViewState.Loaded loaded2 = (ViewState.Loaded) requireViewState;
                boolean isSubmitting = loaded2 != null ? loaded2.isSubmitting() : false;
                boolean hasChanges = loaded2 != null ? loaded2.getHasChanges() : false;
                if (!hasChanges) {
                    Integer valueOf = ((guildRole != null ? Integer.valueOf(guildRole.b()) : null) == null || guildRole.b() == 0) ? null : Integer.valueOf(RoleUtils.getOpaqueColor(guildRole));
                    boolean canRole = PermissionUtils.INSTANCE.canRole(Permission.VIEW_CHANNEL, guildRole, null);
                    String e = guildRoleSubscriptionTierListing.e();
                    Long valueOf2 = Long.valueOf(guildRoleSubscriptionTierListing.a());
                    String b3 = guildRoleSubscriptionTierListing.b();
                    ImageAsset d = guildRoleSubscriptionTierListing.d();
                    if (d != null) {
                        l = Long.valueOf(d.a());
                    }
                    GuildRoleSubscriptionTier guildRoleSubscriptionTier = new GuildRoleSubscriptionTier(e, valueOf2, Integer.valueOf(GuildRoleSubscriptionUtilsKt.getPrice(guildRoleSubscriptionTierListing)), valueOf, null, guildRole, null, l, b3, Boolean.valueOf(guildRoleSubscriptionTierListing.f()), Boolean.valueOf(canRole), arrayList, arrayList2, 80, null);
                    Boolean fullServerGatingOverwrite = GuildRoleSubscriptionUtilsKt.getFullServerGatingOverwrite(loaded.getGuildRoleSubscriptionGroupListing(), map2);
                    boolean booleanValue = fullServerGatingOverwrite != null ? fullServerGatingOverwrite.booleanValue() : loaded.getGuildRoleSubscriptionGroupListing().d();
                    this.isFullServerGating = Boolean.valueOf(booleanValue);
                    this.storedGuildRoleSubscriptionTier = guildRoleSubscriptionTier;
                    this.eventSubject.k.onNext(new Event.StoredStateUpdate(guildRoleSubscriptionTier, booleanValue));
                    updateViewState(new ViewState.Loaded(false, guildRoleSubscriptionTierListing.f(), guildRoleSubscriptionTierListing.e(), isSubmitting, 1, null));
                    return;
                }
                updateViewState(new ViewState.Loaded(hasChanges, guildRoleSubscriptionTierListing.f(), null, false, 12, null));
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        if (storeState.getGuildRoleSubscriptionGroupListingState() != null) {
            StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupListingState = storeState.getGuildRoleSubscriptionGroupListingState();
            if (guildRoleSubscriptionGroupListingState instanceof StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loading) {
                updateViewState(ViewState.Loading.INSTANCE);
            } else if (guildRoleSubscriptionGroupListingState instanceof StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded) {
                handleLoadedStoreState((StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded) storeState.getGuildRoleSubscriptionGroupListingState(), storeState.getChannels(), storeState.getGuildEmojis(), storeState.getGuildRoles());
            } else if (guildRoleSubscriptionGroupListingState instanceof StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Failed) {
                updateViewState(ViewState.Failed.INSTANCE);
            }
        }
    }

    public final void deleteTier() {
        ViewState requireViewState = requireViewState();
        if (!(requireViewState instanceof ViewState.Loaded)) {
            requireViewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) requireViewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, null, true, 7, null));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(GuildRoleSubscriptionUtils.INSTANCE.deleteGuildRoleSubscriptionTierListing(this.restApi, this.storeGuildRoleSubscriptions, this.guildId, this.guildRoleSubscriptionGroupListingId, this.guildRoleSubscriptionTierListingId), this, null, 2, null), ServerSettingsGuildRoleSubscriptionEditTierViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new ServerSettingsGuildRoleSubscriptionEditTierViewModel$deleteTier$2(this, loaded), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ServerSettingsGuildRoleSubscriptionEditTierViewModel$deleteTier$1(this, loaded));
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0055  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x005d  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x007c  */
    /* JADX WARN: Removed duplicated region for block: B:39:0x0084  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x008e  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0093  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x009b  */
    /* JADX WARN: Removed duplicated region for block: B:47:0x00a3  */
    /* JADX WARN: Removed duplicated region for block: B:50:0x00ad  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x00b2  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x00ba  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00c2  */
    /* JADX WARN: Removed duplicated region for block: B:58:0x00ca  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x00d2  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x00dc  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x00e1  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x00e9  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x00f1  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x00fb  */
    /* JADX WARN: Removed duplicated region for block: B:71:0x0100  */
    /* JADX WARN: Removed duplicated region for block: B:74:0x0108  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x0110  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x011c  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x013b  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x013d  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void onTierUpdated(com.discord.widgets.guild_role_subscriptions.tier.model.GuildRoleSubscriptionTier r11) {
        /*
            Method dump skipped, instructions count: 338
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel.onTierUpdated(com.discord.widgets.guild_role_subscriptions.tier.model.GuildRoleSubscriptionTier):void");
    }

    public final void publishTier() {
        Observable updateGuildRoleSubscriptionTierListing;
        ViewState requireViewState = requireViewState();
        if (!(requireViewState instanceof ViewState.Loaded)) {
            requireViewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) requireViewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, false, false, null, true, 7, null));
            updateGuildRoleSubscriptionTierListing = GuildRoleSubscriptionUtils.INSTANCE.updateGuildRoleSubscriptionTierListing(this.restApi, this.storeGuildRoleSubscriptions, this.guildId, this.guildRoleSubscriptionGroupListingId, this.guildRoleSubscriptionTierListingId, (r41 & 32) != 0 ? null : null, (r41 & 64) != 0 ? null : null, (r41 & 128) != 0 ? null : null, (r41 & 256) != 0 ? null : null, (r41 & 512) != 0 ? null : null, (r41 & 1024) != 0 ? null : null, (r41 & 2048) != 0 ? null : null, (r41 & 4096) != 0 ? null : null, (r41 & 8192) != 0 ? null : null, (r41 & 16384) != 0 ? null : Boolean.TRUE);
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(updateGuildRoleSubscriptionTierListing, this, null, 2, null), ServerSettingsGuildRoleSubscriptionEditTierViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new ServerSettingsGuildRoleSubscriptionEditTierViewModel$publishTier$2(this, loaded), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ServerSettingsGuildRoleSubscriptionEditTierViewModel$publishTier$1(this, loaded));
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x005b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void submitChanges() {
        /*
            r26 = this;
            r0 = r26
            java.lang.Object r1 = r26.requireViewState()
            boolean r2 = r1 instanceof com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel.ViewState.Loaded
            if (r2 != 0) goto Lb
            r1 = 0
        Lb:
            com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel$ViewState$Loaded r1 = (com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel.ViewState.Loaded) r1
            if (r1 == 0) goto L92
            java.lang.Integer r2 = r0.editedMemberColor
            if (r2 == 0) goto L24
            int r2 = r2.intValue()
            com.discord.utilities.color.ColorCompat r4 = com.discord.utilities.color.ColorCompat.INSTANCE
            int r2 = r4.removeAlphaComponent(r2)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r17 = r2
            goto L26
        L24:
            r17 = 0
        L26:
            com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils r4 = com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils.INSTANCE
            com.discord.utilities.rest.RestAPI r5 = r0.restApi
            com.discord.stores.StoreGuildRoleSubscriptions r6 = r0.storeGuildRoleSubscriptions
            long r7 = r0.guildId
            long r9 = r0.guildRoleSubscriptionGroupListingId
            long r11 = r0.guildRoleSubscriptionTierListingId
            java.lang.String r13 = r0.editedTierName
            java.lang.String r14 = r0.editedTierDescription
            java.lang.String r15 = r0.editedTierImage
            java.lang.Integer r2 = r0.editedTierPrice
            java.lang.String r3 = r0.editedMemberIcon
            r25 = r1
            java.lang.Boolean r1 = r0.editedCanAccessAllChannelsFlag
            r19 = r1
            java.util.List<com.discord.widgets.guild_role_subscriptions.tier.model.Benefit$ChannelBenefit> r1 = r0.editedChannelBenefits
            if (r1 == 0) goto L49
        L46:
            r20 = r1
            goto L54
        L49:
            com.discord.widgets.guild_role_subscriptions.tier.model.GuildRoleSubscriptionTier r1 = r0.storedGuildRoleSubscriptionTier
            if (r1 == 0) goto L52
            java.util.List r1 = r1.getChannelBenefits()
            goto L46
        L52:
            r20 = 0
        L54:
            java.util.List<com.discord.widgets.guild_role_subscriptions.tier.model.Benefit$IntangibleBenefit> r1 = r0.editedIntangibleBenefits
            if (r1 == 0) goto L5b
        L58:
            r21 = r1
            goto L66
        L5b:
            com.discord.widgets.guild_role_subscriptions.tier.model.GuildRoleSubscriptionTier r1 = r0.storedGuildRoleSubscriptionTier
            if (r1 == 0) goto L64
            java.util.List r1 = r1.getIntangibleBenefits()
            goto L58
        L64:
            r21 = 0
        L66:
            r22 = 0
            r23 = 16384(0x4000, float:2.2959E-41)
            r24 = 0
            r16 = r2
            r18 = r3
            rx.Observable r1 = com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtils.updateGuildRoleSubscriptionTierListing$default(r4, r5, r6, r7, r9, r11, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            r2 = 2
            r3 = 0
            rx.Observable r4 = com.discord.utilities.rx.ObservableExtensionsKt.ui$default(r1, r0, r3, r2, r3)
            java.lang.Class<com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel> r5 = com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel.class
            r6 = 0
            r7 = 0
            com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel$submitChanges$2 r11 = new com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel$submitChanges$2
            r1 = r25
            r11.<init>(r0, r1)
            r9 = 0
            r10 = 0
            com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel$submitChanges$3 r8 = new com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel$submitChanges$3
            r8.<init>(r0, r1)
            r12 = 54
            r13 = 0
            com.discord.utilities.rx.ObservableExtensionsKt.appSubscribe$default(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
        L92:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel.submitChanges():void");
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerSettingsGuildRoleSubscriptionEditTierViewModel(long j, long j2, long j3, RestAPI restAPI, StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, StoreChannels storeChannels, StoreEmoji storeEmoji, StoreGuilds storeGuilds, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(storeGuildRoleSubscriptions, "storeGuildRoleSubscriptions");
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeEmoji, "storeEmoji");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.guildId = j;
        this.guildRoleSubscriptionTierListingId = j2;
        this.guildRoleSubscriptionGroupListingId = j3;
        this.restApi = restAPI;
        this.storeGuildRoleSubscriptions = storeGuildRoleSubscriptions;
        this.eventSubject = PublishSubject.k0();
        storeGuildRoleSubscriptions.fetchGuildRoleSubscriptionGroupsForGuild(j);
        Observable<StoreState> q = observable.q();
        m.checkNotNullExpressionValue(q, "storeStateObservable\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), ServerSettingsGuildRoleSubscriptionEditTierViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
