package com.discord.widgets.servers.guild_role_subscription.edit_tier;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.models.domain.emoji.EmojiSet;
import com.discord.stores.StoreGuildRoleSubscriptions;
import com.discord.widgets.servers.guild_role_subscription.edit_tier.ServerSettingsGuildRoleSubscriptionEditTierViewModel;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Triple;
import rx.functions.Func2;
/* compiled from: ServerSettingsGuildRoleSubscriptionEditTierViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000f\u001a\n \u0001*\u0004\u0018\u00010\f0\f2\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002z\u0010\u000b\u001av\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0006j\u0002`\t\u0012\u0004\u0012\u00020\n\u0018\u00010\u0005 \u0001*:\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b0\u0005\u0012\u0016\u0012\u0014\u0012\b\u0012\u00060\u0006j\u0002`\t\u0012\u0004\u0012\u00020\n\u0018\u00010\u0005\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\r\u0010\u000e"}, d2 = {"Lcom/discord/models/domain/emoji/EmojiSet;", "kotlin.jvm.PlatformType", "emojiSet", "Lkotlin/Triple;", "Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "Lcom/discord/primitives/RoleId;", "Lcom/discord/api/role/GuildRole;", "<name for destructuring parameter 1>", "Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/domain/emoji/EmojiSet;Lkotlin/Triple;)Lcom/discord/widgets/servers/guild_role_subscription/edit_tier/ServerSettingsGuildRoleSubscriptionEditTierViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion$observeStoreState$2<T1, T2, R> implements Func2<EmojiSet, Triple<? extends StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState, ? extends Map<Long, ? extends Channel>, ? extends Map<Long, ? extends GuildRole>>, ServerSettingsGuildRoleSubscriptionEditTierViewModel.StoreState> {
    public static final ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion$observeStoreState$2 INSTANCE = new ServerSettingsGuildRoleSubscriptionEditTierViewModel$Companion$observeStoreState$2();

    @Override // rx.functions.Func2
    public /* bridge */ /* synthetic */ ServerSettingsGuildRoleSubscriptionEditTierViewModel.StoreState call(EmojiSet emojiSet, Triple<? extends StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState, ? extends Map<Long, ? extends Channel>, ? extends Map<Long, ? extends GuildRole>> triple) {
        return call2(emojiSet, (Triple<? extends StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState, ? extends Map<Long, Channel>, ? extends Map<Long, GuildRole>>) triple);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final ServerSettingsGuildRoleSubscriptionEditTierViewModel.StoreState call2(EmojiSet emojiSet, Triple<? extends StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState, ? extends Map<Long, Channel>, ? extends Map<Long, GuildRole>> triple) {
        Map<Long, Channel> component2 = triple.component2();
        m.checkNotNullExpressionValue(emojiSet, "emojiSet");
        return new ServerSettingsGuildRoleSubscriptionEditTierViewModel.StoreState(component2, emojiSet, triple.component1(), triple.component3());
    }
}
