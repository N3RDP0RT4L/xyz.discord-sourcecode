package com.discord.widgets.servers.guild_role_subscription;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.app.AppComponent;
import com.discord.databinding.ViewServerSettingsGuildRoleSubscriptionAddTierItemBinding;
import com.discord.databinding.ViewServerSettingsGuildRoleSubscriptionTierItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.recycler.DiffCreator;
import com.discord.widgets.servers.guild_role_subscription.ServerSettingsGuildRoleSubscriptionTierAdapterItem;
import com.discord.widgets.servers.guild_role_subscription.TierViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 %2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002%&B+\u0012\u0006\u0010\"\u001a\u00020!\u0012\u001a\b\u0002\u0010\u001f\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0004\u0012\u00020\u00020\u001e¢\u0006\u0004\b#\u0010$J\u001f\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0010\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0015\u001a\u00020\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012¢\u0006\u0004\b\u0015\u0010\u0016J\u0015\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0018\u0010\u001bR\u001c\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR(\u0010\u001f\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0004\u0012\u00020\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006'"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onBindViewHolder", "(Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder;I)V", "getItemCount", "()I", "getItemViewType", "(I)I", "", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem;", "newItems", "setItems", "(Ljava/util/List;)V", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener;", "itemClickListener", "setItemClickListener", "(Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener;)V", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener;", "items", "Ljava/util/List;", "Lcom/discord/utilities/recycler/DiffCreator;", "diffCreator", "Lcom/discord/utilities/recycler/DiffCreator;", "Lcom/discord/app/AppComponent;", "appComponent", HookHelper.constructorName, "(Lcom/discord/app/AppComponent;Lcom/discord/utilities/recycler/DiffCreator;)V", "Companion", "ItemClickListener", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsGuildRoleSubscriptionTierAdapter extends RecyclerView.Adapter<TierViewHolder> {
    public static final Companion Companion = new Companion(null);
    private static final int VIEW_TYPE_ADD_ITEM = 1;
    private static final int VIEW_TYPE_TIER = 0;
    private final DiffCreator<List<ServerSettingsGuildRoleSubscriptionTierAdapterItem>, TierViewHolder> diffCreator;
    private ItemClickListener itemClickListener;
    private List<? extends ServerSettingsGuildRoleSubscriptionTierAdapterItem> items;

    /* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$Companion;", "", "", "VIEW_TYPE_ADD_ITEM", "I", "VIEW_TYPE_TIER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H&¢\u0006\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener;", "", "", "tierListingId", "", "onTierItemClick", "(J)V", "onAddTierItemClick", "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface ItemClickListener {
        void onAddTierItemClick();

        void onTierItemClick(long j);
    }

    public /* synthetic */ ServerSettingsGuildRoleSubscriptionTierAdapter(AppComponent appComponent, DiffCreator diffCreator, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(appComponent, (i & 2) != 0 ? new DiffCreator(appComponent) : diffCreator);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        ServerSettingsGuildRoleSubscriptionTierAdapterItem serverSettingsGuildRoleSubscriptionTierAdapterItem = this.items.get(i);
        if (serverSettingsGuildRoleSubscriptionTierAdapterItem instanceof ServerSettingsGuildRoleSubscriptionTierAdapterItem.Tier) {
            return 0;
        }
        if (serverSettingsGuildRoleSubscriptionTierAdapterItem instanceof ServerSettingsGuildRoleSubscriptionTierAdapterItem.AddTier) {
            return 1;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final void setItemClickListener(ItemClickListener itemClickListener) {
        m.checkNotNullParameter(itemClickListener, "itemClickListener");
        this.itemClickListener = itemClickListener;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void setItems(List<? extends ServerSettingsGuildRoleSubscriptionTierAdapterItem> list) {
        m.checkNotNullParameter(list, "newItems");
        this.diffCreator.dispatchDiffUpdates(this, new ServerSettingsGuildRoleSubscriptionTierAdapter$setItems$1(this), this.items, list);
    }

    public ServerSettingsGuildRoleSubscriptionTierAdapter(AppComponent appComponent, DiffCreator<List<ServerSettingsGuildRoleSubscriptionTierAdapterItem>, TierViewHolder> diffCreator) {
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(diffCreator, "diffCreator");
        this.diffCreator = diffCreator;
        this.items = n.emptyList();
    }

    public void onBindViewHolder(TierViewHolder tierViewHolder, int i) {
        m.checkNotNullParameter(tierViewHolder, "holder");
        ServerSettingsGuildRoleSubscriptionTierAdapterItem serverSettingsGuildRoleSubscriptionTierAdapterItem = this.items.get(i);
        if ((tierViewHolder instanceof TierViewHolder.TierItemViewHolder) && (serverSettingsGuildRoleSubscriptionTierAdapterItem instanceof ServerSettingsGuildRoleSubscriptionTierAdapterItem.Tier)) {
            TierViewHolder.TierItemViewHolder tierItemViewHolder = (TierViewHolder.TierItemViewHolder) tierViewHolder;
            ServerSettingsGuildRoleSubscriptionTierAdapterItem.Tier tier = (ServerSettingsGuildRoleSubscriptionTierAdapterItem.Tier) serverSettingsGuildRoleSubscriptionTierAdapterItem;
            ItemClickListener itemClickListener = this.itemClickListener;
            if (itemClickListener == null) {
                m.throwUninitializedPropertyAccessException("itemClickListener");
            }
            tierItemViewHolder.configureUI(tier, itemClickListener);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public TierViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        TierViewHolder tierViewHolder;
        CharSequence b2;
        m.checkNotNullParameter(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            View inflate = from.inflate(R.layout.view_server_settings_guild_role_subscription_tier_item, viewGroup, false);
            int i2 = R.id.guild_role_subscription_tier_draft_tag;
            TextView textView = (TextView) inflate.findViewById(R.id.guild_role_subscription_tier_draft_tag);
            if (textView != null) {
                i2 = R.id.guild_role_subscription_tier_edit;
                ImageView imageView = (ImageView) inflate.findViewById(R.id.guild_role_subscription_tier_edit);
                if (imageView != null) {
                    i2 = R.id.guild_role_subscription_tier_icon;
                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.guild_role_subscription_tier_icon);
                    if (simpleDraweeView != null) {
                        i2 = R.id.guild_role_subscription_tier_name;
                        TextView textView2 = (TextView) inflate.findViewById(R.id.guild_role_subscription_tier_name);
                        if (textView2 != null) {
                            i2 = R.id.guild_role_subscription_tier_price;
                            TextView textView3 = (TextView) inflate.findViewById(R.id.guild_role_subscription_tier_price);
                            if (textView3 != null) {
                                ViewServerSettingsGuildRoleSubscriptionTierItemBinding viewServerSettingsGuildRoleSubscriptionTierItemBinding = new ViewServerSettingsGuildRoleSubscriptionTierItemBinding((ConstraintLayout) inflate, textView, imageView, simpleDraweeView, textView2, textView3);
                                m.checkNotNullExpressionValue(viewServerSettingsGuildRoleSubscriptionTierItemBinding, "ViewServerSettingsGuildR…(inflater, parent, false)");
                                tierViewHolder = new TierViewHolder.TierItemViewHolder(viewServerSettingsGuildRoleSubscriptionTierItemBinding);
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
        } else if (i == 1) {
            View inflate2 = from.inflate(R.layout.view_server_settings_guild_role_subscription_add_tier_item, viewGroup, false);
            int i3 = R.id.guild_role_subscription_add_tier_image;
            ImageView imageView2 = (ImageView) inflate2.findViewById(R.id.guild_role_subscription_add_tier_image);
            if (imageView2 != null) {
                i3 = R.id.guild_role_subscription_add_tier_text;
                TextView textView4 = (TextView) inflate2.findViewById(R.id.guild_role_subscription_add_tier_text);
                if (textView4 != null) {
                    ViewServerSettingsGuildRoleSubscriptionAddTierItemBinding viewServerSettingsGuildRoleSubscriptionAddTierItemBinding = new ViewServerSettingsGuildRoleSubscriptionAddTierItemBinding((ConstraintLayout) inflate2, imageView2, textView4);
                    m.checkNotNullExpressionValue(viewServerSettingsGuildRoleSubscriptionAddTierItemBinding, "ViewServerSettingsGuildR…rent, false\n            )");
                    ItemClickListener itemClickListener = this.itemClickListener;
                    if (itemClickListener == null) {
                        m.throwUninitializedPropertyAccessException("itemClickListener");
                    }
                    tierViewHolder = new TierViewHolder.AddTierItemViewHolder(viewServerSettingsGuildRoleSubscriptionAddTierItemBinding, itemClickListener);
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(i3)));
        } else {
            Context context = viewGroup.getContext();
            m.checkNotNullExpressionValue(context, "parent.context");
            b2 = b.b(context, R.string.android_unknown_view_holder, new Object[]{Integer.valueOf(i)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            throw new IllegalArgumentException(b2.toString());
        }
        return tierViewHolder;
    }
}
