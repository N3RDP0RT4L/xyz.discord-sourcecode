package com.discord.widgets.servers.guild_role_subscription;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.databinding.ViewServerSettingsGuildRoleSubscriptionTierItemBinding;
import com.discord.utilities.billing.PremiumUtilsKt;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.widgets.servers.guild_role_subscription.ServerSettingsGuildRoleSubscriptionTierAdapter;
import com.discord.widgets.servers.guild_role_subscription.ServerSettingsGuildRoleSubscriptionTierAdapterItem;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0006\u0007B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005\u0082\u0001\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;)V", "AddTierItemViewHolder", "TierItemViewHolder", "Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder$TierItemViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder$AddTierItemViewHolder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class TierViewHolder extends RecyclerView.ViewHolder {

    /* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder$AddTierItemViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder;", "Lcom/discord/databinding/ViewServerSettingsGuildRoleSubscriptionAddTierItemBinding;", "binding", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener;", "itemClickListener", HookHelper.constructorName, "(Lcom/discord/databinding/ViewServerSettingsGuildRoleSubscriptionAddTierItemBinding;Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AddTierItemViewHolder extends TierViewHolder {
        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public AddTierItemViewHolder(com.discord.databinding.ViewServerSettingsGuildRoleSubscriptionAddTierItemBinding r3, final com.discord.widgets.servers.guild_role_subscription.ServerSettingsGuildRoleSubscriptionTierAdapter.ItemClickListener r4) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                java.lang.String r0 = "itemClickListener"
                d0.z.d.m.checkNotNullParameter(r4, r0)
                androidx.constraintlayout.widget.ConstraintLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                android.view.View r0 = r2.itemView
                com.discord.widgets.servers.guild_role_subscription.TierViewHolder$AddTierItemViewHolder$1 r1 = new com.discord.widgets.servers.guild_role_subscription.TierViewHolder$AddTierItemViewHolder$1
                r1.<init>()
                r0.setOnClickListener(r1)
                android.widget.TextView r3 = r3.f2193b
                r4 = 2131890521(0x7f121159, float:1.9415736E38)
                r3.setText(r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.TierViewHolder.AddTierItemViewHolder.<init>(com.discord.databinding.ViewServerSettingsGuildRoleSubscriptionAddTierItemBinding, com.discord.widgets.servers.guild_role_subscription.ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener):void");
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000f\u0010\u0010J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder$TierItemViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem$Tier;", "tierAdapterItem", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener;", "itemClickListener", "", "configureUI", "(Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem$Tier;Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapter$ItemClickListener;)V", "Lcom/discord/databinding/ViewServerSettingsGuildRoleSubscriptionTierItemBinding;", "binding", "Lcom/discord/databinding/ViewServerSettingsGuildRoleSubscriptionTierItemBinding;", "", "tierImageSizePx", "I", HookHelper.constructorName, "(Lcom/discord/databinding/ViewServerSettingsGuildRoleSubscriptionTierItemBinding;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class TierItemViewHolder extends TierViewHolder {
        public static final Companion Companion = new Companion(null);
        private static final int MAX_TIER_IMAGE_SIZE = 64;
        private final ViewServerSettingsGuildRoleSubscriptionTierItemBinding binding;
        private final int tierImageSizePx;

        /* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/TierViewHolder$TierItemViewHolder$Companion;", "", "", "MAX_TIER_IMAGE_SIZE", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public TierItemViewHolder(com.discord.databinding.ViewServerSettingsGuildRoleSubscriptionTierItemBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                androidx.constraintlayout.widget.ConstraintLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                android.view.View r3 = r2.itemView
                java.lang.String r0 = "itemView"
                d0.z.d.m.checkNotNullExpressionValue(r3, r0)
                android.content.res.Resources r3 = r3.getResources()
                r0 = 2131165457(0x7f070111, float:1.7945132E38)
                int r3 = r3.getDimensionPixelSize(r0)
                r2.tierImageSizePx = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.TierViewHolder.TierItemViewHolder.<init>(com.discord.databinding.ViewServerSettingsGuildRoleSubscriptionTierItemBinding):void");
        }

        public final void configureUI(final ServerSettingsGuildRoleSubscriptionTierAdapterItem.Tier tier, final ServerSettingsGuildRoleSubscriptionTierAdapter.ItemClickListener itemClickListener) {
            CharSequence d;
            m.checkNotNullParameter(tier, "tierAdapterItem");
            m.checkNotNullParameter(itemClickListener, "itemClickListener");
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.guild_role_subscription.TierViewHolder$TierItemViewHolder$configureUI$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ServerSettingsGuildRoleSubscriptionTierAdapter.ItemClickListener.this.onTierItemClick(tier.getTierListingId());
                }
            });
            TextView textView = this.binding.d;
            m.checkNotNullExpressionValue(textView, "binding.guildRoleSubscriptionTierName");
            textView.setText(tier.getTierName());
            TextView textView2 = this.binding.e;
            m.checkNotNullExpressionValue(textView2, "binding.guildRoleSubscriptionTierPrice");
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            int tierPrice = tier.getTierPrice();
            View view2 = this.itemView;
            m.checkNotNullExpressionValue(view2, "itemView");
            Context context = view2.getContext();
            m.checkNotNullExpressionValue(context, "itemView.context");
            CharSequence formattedPriceUsd = PremiumUtilsKt.getFormattedPriceUsd(tierPrice, context);
            int i = 0;
            String str = null;
            d = b.d(view, R.string.billing_price_per_month, new Object[]{formattedPriceUsd}, (r4 & 4) != 0 ? b.c.j : null);
            textView2.setText(d);
            if (tier.getTierImageAssetId() != null) {
                str = IconUtils.INSTANCE.getStoreAssetImage(Long.valueOf(tier.getApplicationId()), String.valueOf(tier.getTierImageAssetId().longValue()), 64);
            }
            SimpleDraweeView simpleDraweeView = this.binding.c;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.guildRoleSubscriptionTierIcon");
            int i2 = this.tierImageSizePx;
            MGImages.setImage$default(simpleDraweeView, str, i2, i2, false, null, null, 112, null);
            TextView textView3 = this.binding.f2194b;
            m.checkNotNullExpressionValue(textView3, "binding.guildRoleSubscriptionTierDraftTag");
            if (!(!tier.isPublished())) {
                i = 8;
            }
            textView3.setVisibility(i);
        }
    }

    private TierViewHolder(View view) {
        super(view);
    }

    public /* synthetic */ TierViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }
}
