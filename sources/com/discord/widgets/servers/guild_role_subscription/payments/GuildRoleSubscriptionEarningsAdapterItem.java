package com.discord.widgets.servers.guild_role_subscription.payments;

import a0.a.a.b;
import andhook.lib.HookHelper;
import androidx.annotation.StringRes;
import b.d.b.a.a;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionTierListing;
import com.discord.utilities.recycler.DiffKeyProvider;
import com.discord.widgets.servers.guild_role_subscription.model.CurrentMonthEarningMetrics;
import com.discord.widgets.servers.guild_role_subscription.model.PayoutStatusMedia;
import com.discord.widgets.servers.guild_role_subscription.model.TotalPayoutsForPeriod;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: GuildRoleSubscriptionEarningsAdapterItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0006\u0004\u0005\u0006\u0007\b\tB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0006\n\u000b\f\r\u000e\u000f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "Lcom/discord/utilities/recycler/DiffKeyProvider;", HookHelper.constructorName, "()V", "CurrentMonthEarningMetricsByTiers", "CurrentMonthEarningMetricsByTiersHeader", "EarningMetricsHistory", "EarningMetricsHistoryHeader", "EarningMetricsItem", "SectionHeader", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$EarningMetricsItem;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$CurrentMonthEarningMetricsByTiers;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$SectionHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$CurrentMonthEarningMetricsByTiersHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$EarningMetricsHistoryHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$EarningMetricsHistory;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class GuildRoleSubscriptionEarningsAdapterItem implements DiffKeyProvider {

    /* compiled from: GuildRoleSubscriptionEarningsAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BE\u0012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\n\u0010\u0013\u001a\u00060\u0006j\u0002`\u0007\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\n\u0012\u0018\u0010\u0015\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\r¢\u0006\u0004\b-\u0010.J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\b\u001a\u00060\u0006j\u0002`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\"\u0010\u0010\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011JV\u0010\u0016\u001a\u00020\u00002\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\f\b\u0002\u0010\u0013\u001a\u00060\u0006j\u0002`\u00072\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\n2\u001a\b\u0002\u0010\u0015\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\rHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010 \u001a\u00020\u001f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dHÖ\u0003¢\u0006\u0004\b \u0010!R\u001d\u0010\u0013\u001a\u00060\u0006j\u0002`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b#\u0010\tR+\u0010\u0015\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u000e\u0012\u0004\u0012\u00020\u000f\u0018\u00010\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b%\u0010\u0011R\u001c\u0010&\u001a\u00020\u00188\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010\u001aR\u001b\u0010\u0014\u001a\u0004\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010)\u001a\u0004\b*\u0010\fR\u001f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010+\u001a\u0004\b,\u0010\u0005¨\u0006/"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$CurrentMonthEarningMetricsByTiers;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;", "component1", "()Ljava/util/List;", "", "Lcom/discord/primitives/ApplicationId;", "component2", "()J", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "component3", "()Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "", "Lcom/discord/primitives/RoleId;", "", "component4", "()Ljava/util/Map;", "tierListings", "applicationId", "totalPayoutsForPeriod", "roleMemberCounts", "copy", "(Ljava/util/List;JLcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;Ljava/util/Map;)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$CurrentMonthEarningMetricsByTiers;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getApplicationId", "Ljava/util/Map;", "getRoleMemberCounts", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "getTotalPayoutsForPeriod", "Ljava/util/List;", "getTierListings", HookHelper.constructorName, "(Ljava/util/List;JLcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CurrentMonthEarningMetricsByTiers extends GuildRoleSubscriptionEarningsAdapterItem {
        private final long applicationId;
        private final String key = "CurrentMonthEarningMetricsByTiers";
        private final Map<Long, Integer> roleMemberCounts;
        private final List<GuildRoleSubscriptionTierListing> tierListings;
        private final TotalPayoutsForPeriod totalPayoutsForPeriod;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public CurrentMonthEarningMetricsByTiers(List<GuildRoleSubscriptionTierListing> list, long j, TotalPayoutsForPeriod totalPayoutsForPeriod, Map<Long, Integer> map) {
            super(null);
            m.checkNotNullParameter(list, "tierListings");
            this.tierListings = list;
            this.applicationId = j;
            this.totalPayoutsForPeriod = totalPayoutsForPeriod;
            this.roleMemberCounts = map;
        }

        public static /* synthetic */ CurrentMonthEarningMetricsByTiers copy$default(CurrentMonthEarningMetricsByTiers currentMonthEarningMetricsByTiers, List list, long j, TotalPayoutsForPeriod totalPayoutsForPeriod, Map map, int i, Object obj) {
            List<GuildRoleSubscriptionTierListing> list2 = list;
            if ((i & 1) != 0) {
                list2 = currentMonthEarningMetricsByTiers.tierListings;
            }
            if ((i & 2) != 0) {
                j = currentMonthEarningMetricsByTiers.applicationId;
            }
            long j2 = j;
            if ((i & 4) != 0) {
                totalPayoutsForPeriod = currentMonthEarningMetricsByTiers.totalPayoutsForPeriod;
            }
            TotalPayoutsForPeriod totalPayoutsForPeriod2 = totalPayoutsForPeriod;
            Map<Long, Integer> map2 = map;
            if ((i & 8) != 0) {
                map2 = currentMonthEarningMetricsByTiers.roleMemberCounts;
            }
            return currentMonthEarningMetricsByTiers.copy(list2, j2, totalPayoutsForPeriod2, map2);
        }

        public final List<GuildRoleSubscriptionTierListing> component1() {
            return this.tierListings;
        }

        public final long component2() {
            return this.applicationId;
        }

        public final TotalPayoutsForPeriod component3() {
            return this.totalPayoutsForPeriod;
        }

        public final Map<Long, Integer> component4() {
            return this.roleMemberCounts;
        }

        public final CurrentMonthEarningMetricsByTiers copy(List<GuildRoleSubscriptionTierListing> list, long j, TotalPayoutsForPeriod totalPayoutsForPeriod, Map<Long, Integer> map) {
            m.checkNotNullParameter(list, "tierListings");
            return new CurrentMonthEarningMetricsByTiers(list, j, totalPayoutsForPeriod, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CurrentMonthEarningMetricsByTiers)) {
                return false;
            }
            CurrentMonthEarningMetricsByTiers currentMonthEarningMetricsByTiers = (CurrentMonthEarningMetricsByTiers) obj;
            return m.areEqual(this.tierListings, currentMonthEarningMetricsByTiers.tierListings) && this.applicationId == currentMonthEarningMetricsByTiers.applicationId && m.areEqual(this.totalPayoutsForPeriod, currentMonthEarningMetricsByTiers.totalPayoutsForPeriod) && m.areEqual(this.roleMemberCounts, currentMonthEarningMetricsByTiers.roleMemberCounts);
        }

        public final long getApplicationId() {
            return this.applicationId;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final Map<Long, Integer> getRoleMemberCounts() {
            return this.roleMemberCounts;
        }

        public final List<GuildRoleSubscriptionTierListing> getTierListings() {
            return this.tierListings;
        }

        public final TotalPayoutsForPeriod getTotalPayoutsForPeriod() {
            return this.totalPayoutsForPeriod;
        }

        public int hashCode() {
            List<GuildRoleSubscriptionTierListing> list = this.tierListings;
            int i = 0;
            int a = (b.a(this.applicationId) + ((list != null ? list.hashCode() : 0) * 31)) * 31;
            TotalPayoutsForPeriod totalPayoutsForPeriod = this.totalPayoutsForPeriod;
            int hashCode = (a + (totalPayoutsForPeriod != null ? totalPayoutsForPeriod.hashCode() : 0)) * 31;
            Map<Long, Integer> map = this.roleMemberCounts;
            if (map != null) {
                i = map.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("CurrentMonthEarningMetricsByTiers(tierListings=");
            R.append(this.tierListings);
            R.append(", applicationId=");
            R.append(this.applicationId);
            R.append(", totalPayoutsForPeriod=");
            R.append(this.totalPayoutsForPeriod);
            R.append(", roleMemberCounts=");
            return a.L(R, this.roleMemberCounts, ")");
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$CurrentMonthEarningMetricsByTiersHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CurrentMonthEarningMetricsByTiersHeader extends GuildRoleSubscriptionEarningsAdapterItem {
        public static final CurrentMonthEarningMetricsByTiersHeader INSTANCE = new CurrentMonthEarningMetricsByTiersHeader();
        private static final String key = "CurrentMonthEarningMetricsByTiersHeader";

        private CurrentMonthEarningMetricsByTiersHeader() {
            super(null);
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return key;
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\b\b\u0002\u0010\u0012\u001a\u00020\f¢\u0006\u0004\b)\u0010*J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ>\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00052\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\u0012\u001a\u00020\fHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u001a\u0010\u001d\u001a\u00020\f2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001bHÖ\u0003¢\u0006\u0004\b\u001d\u0010\u001eR\u001f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001f\u001a\u0004\b \u0010\u000bR\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010!\u001a\u0004\b\"\u0010\u0007R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010#\u001a\u0004\b$\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010%\u001a\u0004\b\u0012\u0010\u000eR\u001c\u0010&\u001a\u00020\u00158\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010\u0017¨\u0006+"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$EarningMetricsHistory;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "Lcom/discord/widgets/servers/guild_role_subscription/model/PayoutStatusMedia;", "component1", "()Lcom/discord/widgets/servers/guild_role_subscription/model/PayoutStatusMedia;", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "component2", "()Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;", "component3", "()Ljava/util/List;", "", "component4", "()Z", "statusMedia", "totalPayoutsForPeriod", "tierListings", "isLastItem", "copy", "(Lcom/discord/widgets/servers/guild_role_subscription/model/PayoutStatusMedia;Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;Ljava/util/List;Z)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$EarningMetricsHistory;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getTierListings", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "getTotalPayoutsForPeriod", "Lcom/discord/widgets/servers/guild_role_subscription/model/PayoutStatusMedia;", "getStatusMedia", "Z", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/widgets/servers/guild_role_subscription/model/PayoutStatusMedia;Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;Ljava/util/List;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EarningMetricsHistory extends GuildRoleSubscriptionEarningsAdapterItem {
        private final boolean isLastItem;
        private final String key;
        private final PayoutStatusMedia statusMedia;
        private final List<GuildRoleSubscriptionTierListing> tierListings;
        private final TotalPayoutsForPeriod totalPayoutsForPeriod;

        public /* synthetic */ EarningMetricsHistory(PayoutStatusMedia payoutStatusMedia, TotalPayoutsForPeriod totalPayoutsForPeriod, List list, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(payoutStatusMedia, totalPayoutsForPeriod, list, (i & 8) != 0 ? false : z2);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ EarningMetricsHistory copy$default(EarningMetricsHistory earningMetricsHistory, PayoutStatusMedia payoutStatusMedia, TotalPayoutsForPeriod totalPayoutsForPeriod, List list, boolean z2, int i, Object obj) {
            if ((i & 1) != 0) {
                payoutStatusMedia = earningMetricsHistory.statusMedia;
            }
            if ((i & 2) != 0) {
                totalPayoutsForPeriod = earningMetricsHistory.totalPayoutsForPeriod;
            }
            if ((i & 4) != 0) {
                list = earningMetricsHistory.tierListings;
            }
            if ((i & 8) != 0) {
                z2 = earningMetricsHistory.isLastItem;
            }
            return earningMetricsHistory.copy(payoutStatusMedia, totalPayoutsForPeriod, list, z2);
        }

        public final PayoutStatusMedia component1() {
            return this.statusMedia;
        }

        public final TotalPayoutsForPeriod component2() {
            return this.totalPayoutsForPeriod;
        }

        public final List<GuildRoleSubscriptionTierListing> component3() {
            return this.tierListings;
        }

        public final boolean component4() {
            return this.isLastItem;
        }

        public final EarningMetricsHistory copy(PayoutStatusMedia payoutStatusMedia, TotalPayoutsForPeriod totalPayoutsForPeriod, List<GuildRoleSubscriptionTierListing> list, boolean z2) {
            m.checkNotNullParameter(payoutStatusMedia, "statusMedia");
            m.checkNotNullParameter(totalPayoutsForPeriod, "totalPayoutsForPeriod");
            m.checkNotNullParameter(list, "tierListings");
            return new EarningMetricsHistory(payoutStatusMedia, totalPayoutsForPeriod, list, z2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EarningMetricsHistory)) {
                return false;
            }
            EarningMetricsHistory earningMetricsHistory = (EarningMetricsHistory) obj;
            return m.areEqual(this.statusMedia, earningMetricsHistory.statusMedia) && m.areEqual(this.totalPayoutsForPeriod, earningMetricsHistory.totalPayoutsForPeriod) && m.areEqual(this.tierListings, earningMetricsHistory.tierListings) && this.isLastItem == earningMetricsHistory.isLastItem;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final PayoutStatusMedia getStatusMedia() {
            return this.statusMedia;
        }

        public final List<GuildRoleSubscriptionTierListing> getTierListings() {
            return this.tierListings;
        }

        public final TotalPayoutsForPeriod getTotalPayoutsForPeriod() {
            return this.totalPayoutsForPeriod;
        }

        public int hashCode() {
            PayoutStatusMedia payoutStatusMedia = this.statusMedia;
            int i = 0;
            int hashCode = (payoutStatusMedia != null ? payoutStatusMedia.hashCode() : 0) * 31;
            TotalPayoutsForPeriod totalPayoutsForPeriod = this.totalPayoutsForPeriod;
            int hashCode2 = (hashCode + (totalPayoutsForPeriod != null ? totalPayoutsForPeriod.hashCode() : 0)) * 31;
            List<GuildRoleSubscriptionTierListing> list = this.tierListings;
            if (list != null) {
                i = list.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z2 = this.isLastItem;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            return i2 + i3;
        }

        public final boolean isLastItem() {
            return this.isLastItem;
        }

        public String toString() {
            StringBuilder R = a.R("EarningMetricsHistory(statusMedia=");
            R.append(this.statusMedia);
            R.append(", totalPayoutsForPeriod=");
            R.append(this.totalPayoutsForPeriod);
            R.append(", tierListings=");
            R.append(this.tierListings);
            R.append(", isLastItem=");
            return a.M(R, this.isLastItem, ")");
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public EarningMetricsHistory(PayoutStatusMedia payoutStatusMedia, TotalPayoutsForPeriod totalPayoutsForPeriod, List<GuildRoleSubscriptionTierListing> list, boolean z2) {
            super(null);
            m.checkNotNullParameter(payoutStatusMedia, "statusMedia");
            m.checkNotNullParameter(totalPayoutsForPeriod, "totalPayoutsForPeriod");
            m.checkNotNullParameter(list, "tierListings");
            this.statusMedia = payoutStatusMedia;
            this.totalPayoutsForPeriod = totalPayoutsForPeriod;
            this.tierListings = list;
            this.isLastItem = z2;
            this.key = totalPayoutsForPeriod.getPeriodStartingAt();
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$EarningMetricsHistoryHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EarningMetricsHistoryHeader extends GuildRoleSubscriptionEarningsAdapterItem {
        public static final EarningMetricsHistoryHeader INSTANCE = new EarningMetricsHistoryHeader();
        private static final String key = "EarningMetricsHistoryHeader";

        private EarningMetricsHistoryHeader() {
            super(null);
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return key;
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u001c\u0010\u001b\u001a\u00020\f8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u000e¨\u0006 "}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$EarningMetricsItem;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;", "component1", "()Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;", "", "component2", "()Ljava/lang/Long;", "currentMonthEarningMetrics", "teamId", "copy", "(Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;Ljava/lang/Long;)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$EarningMetricsItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;", "getCurrentMonthEarningMetrics", "Ljava/lang/Long;", "getTeamId", "key", "Ljava/lang/String;", "getKey", HookHelper.constructorName, "(Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;Ljava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EarningMetricsItem extends GuildRoleSubscriptionEarningsAdapterItem {
        private final CurrentMonthEarningMetrics currentMonthEarningMetrics;
        private final String key = "EarningMetricsItem";
        private final Long teamId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public EarningMetricsItem(CurrentMonthEarningMetrics currentMonthEarningMetrics, Long l) {
            super(null);
            m.checkNotNullParameter(currentMonthEarningMetrics, "currentMonthEarningMetrics");
            this.currentMonthEarningMetrics = currentMonthEarningMetrics;
            this.teamId = l;
        }

        public static /* synthetic */ EarningMetricsItem copy$default(EarningMetricsItem earningMetricsItem, CurrentMonthEarningMetrics currentMonthEarningMetrics, Long l, int i, Object obj) {
            if ((i & 1) != 0) {
                currentMonthEarningMetrics = earningMetricsItem.currentMonthEarningMetrics;
            }
            if ((i & 2) != 0) {
                l = earningMetricsItem.teamId;
            }
            return earningMetricsItem.copy(currentMonthEarningMetrics, l);
        }

        public final CurrentMonthEarningMetrics component1() {
            return this.currentMonthEarningMetrics;
        }

        public final Long component2() {
            return this.teamId;
        }

        public final EarningMetricsItem copy(CurrentMonthEarningMetrics currentMonthEarningMetrics, Long l) {
            m.checkNotNullParameter(currentMonthEarningMetrics, "currentMonthEarningMetrics");
            return new EarningMetricsItem(currentMonthEarningMetrics, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EarningMetricsItem)) {
                return false;
            }
            EarningMetricsItem earningMetricsItem = (EarningMetricsItem) obj;
            return m.areEqual(this.currentMonthEarningMetrics, earningMetricsItem.currentMonthEarningMetrics) && m.areEqual(this.teamId, earningMetricsItem.teamId);
        }

        public final CurrentMonthEarningMetrics getCurrentMonthEarningMetrics() {
            return this.currentMonthEarningMetrics;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final Long getTeamId() {
            return this.teamId;
        }

        public int hashCode() {
            CurrentMonthEarningMetrics currentMonthEarningMetrics = this.currentMonthEarningMetrics;
            int i = 0;
            int hashCode = (currentMonthEarningMetrics != null ? currentMonthEarningMetrics.hashCode() : 0) * 31;
            Long l = this.teamId;
            if (l != null) {
                i = l.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("EarningMetricsItem(currentMonthEarningMetrics=");
            R.append(this.currentMonthEarningMetrics);
            R.append(", teamId=");
            return a.F(R, this.teamId, ")");
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsAdapterItem.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0002\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J$\u0010\b\u001a\u00020\u00002\b\b\u0003\u0010\u0006\u001a\u00020\u00022\b\b\u0003\u0010\u0007\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\n8V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\fR\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\u0006\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0015\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$SectionHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "", "component1", "()I", "component2", "sectionTitleResId", "sectionDescriptionResId", "copy", "(II)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem$SectionHeader;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "getKey", "key", "I", "getSectionDescriptionResId", "getSectionTitleResId", HookHelper.constructorName, "(II)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SectionHeader extends GuildRoleSubscriptionEarningsAdapterItem {
        private final int sectionDescriptionResId;
        private final int sectionTitleResId;

        public SectionHeader(@StringRes int i, @StringRes int i2) {
            super(null);
            this.sectionTitleResId = i;
            this.sectionDescriptionResId = i2;
        }

        public static /* synthetic */ SectionHeader copy$default(SectionHeader sectionHeader, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = sectionHeader.sectionTitleResId;
            }
            if ((i3 & 2) != 0) {
                i2 = sectionHeader.sectionDescriptionResId;
            }
            return sectionHeader.copy(i, i2);
        }

        public final int component1() {
            return this.sectionTitleResId;
        }

        public final int component2() {
            return this.sectionDescriptionResId;
        }

        public final SectionHeader copy(@StringRes int i, @StringRes int i2) {
            return new SectionHeader(i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SectionHeader)) {
                return false;
            }
            SectionHeader sectionHeader = (SectionHeader) obj;
            return this.sectionTitleResId == sectionHeader.sectionTitleResId && this.sectionDescriptionResId == sectionHeader.sectionDescriptionResId;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return String.valueOf(this.sectionTitleResId);
        }

        public final int getSectionDescriptionResId() {
            return this.sectionDescriptionResId;
        }

        public final int getSectionTitleResId() {
            return this.sectionTitleResId;
        }

        public int hashCode() {
            return (this.sectionTitleResId * 31) + this.sectionDescriptionResId;
        }

        public String toString() {
            StringBuilder R = a.R("SectionHeader(sectionTitleResId=");
            R.append(this.sectionTitleResId);
            R.append(", sectionDescriptionResId=");
            return a.A(R, this.sectionDescriptionResId, ")");
        }
    }

    private GuildRoleSubscriptionEarningsAdapterItem() {
    }

    public /* synthetic */ GuildRoleSubscriptionEarningsAdapterItem(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
