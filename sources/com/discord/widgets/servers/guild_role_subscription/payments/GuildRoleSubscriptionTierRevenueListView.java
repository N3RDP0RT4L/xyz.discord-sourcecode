package com.discord.widgets.servers.guild_role_subscription.payments;

import andhook.lib.HookHelper;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionTierListing;
import com.discord.api.guildrolesubscription.ImageAsset;
import com.discord.api.guildrolesubscription.PayoutGroup;
import com.discord.utilities.billing.PremiumUtilsKt;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.widgets.servers.guild_role_subscription.model.TotalPayoutsForPeriod;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.m;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionTierRevenueListView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001f\u0010 B\u001d\b\u0016\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010!¢\u0006\u0004\b\u001f\u0010#B'\b\u0016\u0012\u0006\u0010\u001e\u001a\u00020\u001d\u0012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010!\u0012\b\b\u0002\u0010%\u001a\u00020\u000b¢\u0006\u0004\b\u001f\u0010&JK\u0010\u000e\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u000e\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\u0018\u0010\f\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\tH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ!\u0010\u0010\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J]\u0010\u0016\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\u00122\b\b\u0002\u0010\u0015\u001a\u00020\u00142\u0010\b\u0002\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\u001a\b\u0002\u0010\f\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\t¢\u0006\u0004\b\u0016\u0010\u0017J\r\u0010\u0018\u001a\u00020\r¢\u0006\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001a\u001a\u00020\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001c\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001b¨\u0006'"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionTierRevenueListView;", "Landroid/widget/LinearLayout;", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "totalPayoutsForPeriod", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;", "tierListing", "", "Lcom/discord/primitives/ApplicationId;", "applicationId", "", "Lcom/discord/primitives/RoleId;", "", "guildRoleMemberCounts", "", "setupRevenueMetricsView", "(Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;Ljava/lang/Long;Ljava/util/Map;)V", "setupRevenueHistoryMetricsView", "(Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionTierListing;)V", "", "tierListings", "", "isHistoryList", "configureUI", "(Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;Ljava/util/List;ZLjava/lang/Long;Ljava/util/Map;)V", "toggleVisibility", "()V", "avatarSize", "I", "verticalPadding", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attributeSet", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "attrs", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionTierRevenueListView extends LinearLayout {
    private int avatarSize;
    private final int verticalPadding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionTierRevenueListView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        int dpToPixels = DimenUtils.dpToPixels(8);
        this.verticalPadding = dpToPixels;
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size_small);
        setOrientation(1);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        setPadding(0, dpToPixels, 0, dpToPixels);
    }

    public static /* synthetic */ void configureUI$default(GuildRoleSubscriptionTierRevenueListView guildRoleSubscriptionTierRevenueListView, TotalPayoutsForPeriod totalPayoutsForPeriod, List list, boolean z2, Long l, Map map, int i, Object obj) {
        guildRoleSubscriptionTierRevenueListView.configureUI(totalPayoutsForPeriod, list, (i & 4) != 0 ? false : z2, (i & 8) != 0 ? null : l, (i & 16) != 0 ? null : map);
    }

    private final void setupRevenueHistoryMetricsView(TotalPayoutsForPeriod totalPayoutsForPeriod, GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing) {
        String str;
        Map<Long, PayoutGroup> payoutGroups;
        int i = 0;
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_guild_role_subscription_earning_metrics_history_tier_item, (ViewGroup) this, false);
        addView(inflate);
        int i2 = R.id.tier_metrics_history_description;
        TextView textView = (TextView) inflate.findViewById(R.id.tier_metrics_history_description);
        if (textView != null) {
            i2 = R.id.tier_metrics_history_revenue;
            TextView textView2 = (TextView) inflate.findViewById(R.id.tier_metrics_history_revenue);
            if (textView2 != null) {
                i2 = R.id.tier_metrics_history_subscriber_count;
                TextView textView3 = (TextView) inflate.findViewById(R.id.tier_metrics_history_subscriber_count);
                if (textView3 != null) {
                    ConstraintLayout constraintLayout = (ConstraintLayout) inflate;
                    m.checkNotNullExpressionValue(textView, "tierMetricsHistoryDescription");
                    textView.setText(guildRoleSubscriptionTierListing.e());
                    PayoutGroup payoutGroup = (totalPayoutsForPeriod == null || (payoutGroups = totalPayoutsForPeriod.getPayoutGroups()) == null) ? null : payoutGroups.get(Long.valueOf(guildRoleSubscriptionTierListing.c()));
                    m.checkNotNullExpressionValue(textView3, "tierMetricsHistorySubscriberCount");
                    if (payoutGroup == null || (str = String.valueOf(payoutGroup.c())) == null) {
                        str = "0";
                    }
                    textView3.setText(str);
                    m.checkNotNullExpressionValue(textView2, "tierMetricsHistoryRevenue");
                    if (payoutGroup != null) {
                        i = (int) payoutGroup.a();
                    }
                    Context context = getContext();
                    m.checkNotNullExpressionValue(context, "context");
                    textView2.setText(PremiumUtilsKt.getFormattedPriceUsd(i, context));
                    return;
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }

    private final void setupRevenueMetricsView(TotalPayoutsForPeriod totalPayoutsForPeriod, GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing, Long l, Map<Long, Integer> map) {
        String str;
        Integer num;
        Map<Long, PayoutGroup> payoutGroups;
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.drawable_bg_secondary_bottom_corners));
        int i = 0;
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.view_guild_role_subscription_earning_metrics_tier_item, (ViewGroup) this, false);
        addView(inflate);
        int i2 = R.id.tier_metrics_description;
        TextView textView = (TextView) inflate.findViewById(R.id.tier_metrics_description);
        if (textView != null) {
            i2 = R.id.tier_metrics_description_layout;
            if (((LinearLayout) inflate.findViewById(R.id.tier_metrics_description_layout)) != null) {
                i2 = R.id.tier_metrics_icon;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.tier_metrics_icon);
                if (simpleDraweeView != null) {
                    i2 = R.id.tier_metrics_revenue;
                    TextView textView2 = (TextView) inflate.findViewById(R.id.tier_metrics_revenue);
                    if (textView2 != null) {
                        i2 = R.id.tier_metrics_subscriber_count;
                        TextView textView3 = (TextView) inflate.findViewById(R.id.tier_metrics_subscriber_count);
                        if (textView3 != null) {
                            ConstraintLayout constraintLayout = (ConstraintLayout) inflate;
                            m.checkNotNullExpressionValue(textView, "tierMetricsDescription");
                            textView.setText(guildRoleSubscriptionTierListing.e());
                            PayoutGroup payoutGroup = (totalPayoutsForPeriod == null || (payoutGroups = totalPayoutsForPeriod.getPayoutGroups()) == null) ? null : payoutGroups.get(Long.valueOf(guildRoleSubscriptionTierListing.c()));
                            m.checkNotNullExpressionValue(textView3, "tierMetricsSubscriberCount");
                            if (map == null || (num = map.get(Long.valueOf(guildRoleSubscriptionTierListing.h()))) == null || (str = String.valueOf(num.intValue())) == null) {
                                str = "0";
                            }
                            textView3.setText(str);
                            m.checkNotNullExpressionValue(textView2, "tierMetricsRevenue");
                            if (payoutGroup != null) {
                                i = (int) payoutGroup.a();
                            }
                            Context context = getContext();
                            m.checkNotNullExpressionValue(context, "context");
                            textView2.setText(PremiumUtilsKt.getFormattedPriceUsd(i, context));
                            ImageAsset d = guildRoleSubscriptionTierListing.d();
                            if (d == null || l == null) {
                                simpleDraweeView.setImageDrawable(null);
                                return;
                            }
                            String storeAssetImage = IconUtils.INSTANCE.getStoreAssetImage(l, String.valueOf(d.a()), this.avatarSize);
                            m.checkNotNullExpressionValue(simpleDraweeView, "tierMetricsIcon");
                            int i3 = this.avatarSize;
                            IconUtils.setIcon$default(simpleDraweeView, storeAssetImage, i3, i3, false, null, null, 112, null);
                            return;
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }

    public final void configureUI(TotalPayoutsForPeriod totalPayoutsForPeriod, List<GuildRoleSubscriptionTierListing> list, boolean z2, Long l, Map<Long, Integer> map) {
        m.checkNotNullParameter(list, "tierListings");
        removeAllViews();
        for (GuildRoleSubscriptionTierListing guildRoleSubscriptionTierListing : list) {
            if (z2) {
                setupRevenueHistoryMetricsView(totalPayoutsForPeriod, guildRoleSubscriptionTierListing);
            } else {
                setupRevenueMetricsView(totalPayoutsForPeriod, guildRoleSubscriptionTierListing, l, map);
            }
        }
    }

    public final void toggleVisibility() {
        int i = 0;
        if (!(!(getVisibility() == 0))) {
            i = 8;
        }
        setVisibility(i);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionTierRevenueListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        int dpToPixels = DimenUtils.dpToPixels(8);
        this.verticalPadding = dpToPixels;
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size_small);
        setOrientation(1);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        setPadding(0, dpToPixels, 0, dpToPixels);
    }

    public /* synthetic */ GuildRoleSubscriptionTierRevenueListView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionTierRevenueListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        int dpToPixels = DimenUtils.dpToPixels(8);
        this.verticalPadding = dpToPixels;
        this.avatarSize = getResources().getDimensionPixelSize(R.dimen.avatar_size_small);
        setOrientation(1);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        setPadding(0, dpToPixels, 0, dpToPixels);
    }

    public /* synthetic */ GuildRoleSubscriptionTierRevenueListView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
