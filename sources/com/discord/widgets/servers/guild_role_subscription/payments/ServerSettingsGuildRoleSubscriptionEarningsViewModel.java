package com.discord.widgets.servers.guild_role_subscription.payments;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.application.Application;
import com.discord.api.application.ApplicationType;
import com.discord.api.application.Team;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.api.guildrolesubscription.PayoutGroup;
import com.discord.app.AppViewModel;
import com.discord.stores.StoreGuildRoleMemberCounts;
import com.discord.stores.StoreGuildRoleSubscriptions;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.servers.guild_role_subscription.model.TotalEarningMetrics;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 02\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003012BK\u0012\n\u0010\u0014\u001a\u00060\u0012j\u0002`\u0013\u0012\b\b\u0002\u0010$\u001a\u00020#\u0012\b\b\u0002\u0010'\u001a\u00020&\u0012\b\b\u0002\u0010)\u001a\u00020(\u0012\b\b\u0002\u0010+\u001a\u00020*\u0012\u000e\b\u0002\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00030,¢\u0006\u0004\b.\u0010/J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001d\u0010\u000b\u001a\u00020\u00052\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0014\u001a\u00060\u0012j\u0002`\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0018\u0010\u0019\u001a\u0004\u0018\u00010\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0018\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR(\u0010!\u001a\u0014\u0012\b\u0012\u00060\u0012j\u0002`\u001f\u0012\u0004\u0012\u00020 \u0018\u00010\u001e8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%¨\u00063"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;)V", "", "Lcom/discord/api/guildrolesubscription/PayoutGroup;", "payoutGroupList", "updatePayouts", "(Ljava/util/List;)V", "updateAdapterItemList", "()V", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;", "guildRoleSubscriptionGroupListing", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", "getGuildId", "()J", "Lcom/discord/api/application/Application;", "application", "Lcom/discord/api/application/Application;", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalEarningMetrics;", "totalEarningMetrics", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalEarningMetrics;", "", "Lcom/discord/primitives/RoleId;", "", "guildRoleMemberCounts", "Ljava/util/Map;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/stores/StoreGuildRoleSubscriptions;", "storeGuildRoleSubscriptions", "Lcom/discord/stores/StoreGuildRoleMemberCounts;", "storeGuildRoleMemberCounts", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(JLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuildRoleSubscriptions;Lcom/discord/stores/StoreGuildRoleMemberCounts;Lcom/discord/stores/updates/ObservationDeck;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsGuildRoleSubscriptionEarningsViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private Application application;
    private final long guildId;
    private Map<Long, Integer> guildRoleMemberCounts;
    private GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing;
    private final RestAPI restAPI;
    private TotalEarningMetrics totalEarningMetrics;

    /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/api/guildrolesubscription/PayoutGroup;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<List<? extends PayoutGroup>, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends PayoutGroup> list) {
            invoke2((List<PayoutGroup>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<PayoutGroup> list) {
            ServerSettingsGuildRoleSubscriptionEarningsViewModel serverSettingsGuildRoleSubscriptionEarningsViewModel = ServerSettingsGuildRoleSubscriptionEarningsViewModel.this;
            m.checkNotNullExpressionValue(list, "it");
            serverSettingsGuildRoleSubscriptionEarningsViewModel.updatePayouts(list);
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "it", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3 extends o implements Function1<Error, Unit> {
        public AnonymousClass3() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Error error) {
            invoke2(error);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Error error) {
            m.checkNotNullParameter(error, "it");
            ServerSettingsGuildRoleSubscriptionEarningsViewModel.this.updateViewState(ViewState.Failed.INSTANCE);
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel$4  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass4 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass4() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            ServerSettingsGuildRoleSubscriptionEarningsViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J9\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreGuildRoleSubscriptions;", "storeGuildRoleSubscriptions", "Lcom/discord/stores/StoreGuildRoleMemberCounts;", "storeGuildRoleMemberCounts", "Lrx/Observable;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreGuildRoleSubscriptions;Lcom/discord/stores/StoreGuildRoleMemberCounts;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(long j, ObservationDeck observationDeck, StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, StoreGuildRoleMemberCounts storeGuildRoleMemberCounts) {
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeGuildRoleSubscriptions, storeGuildRoleMemberCounts}, false, null, null, new ServerSettingsGuildRoleSubscriptionEarningsViewModel$Companion$observeStoreState$1(storeGuildRoleSubscriptions, j, storeGuildRoleMemberCounts), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\u0018\u0010\f\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\"\u0010\t\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\nJ8\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\u001a\b\u0002\u0010\f\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R+\u0010\f\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\b\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\u0019\u0010\nR\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;", "", "Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;", "component1", "()Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;", "", "", "Lcom/discord/primitives/RoleId;", "", "component2", "()Ljava/util/Map;", "guildRoleSubscriptionGroupState", "roleMemberCounts", "copy", "(Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;Ljava/util/Map;)Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getRoleMemberCounts", "Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;", "getGuildRoleSubscriptionGroupState", HookHelper.constructorName, "(Lcom/discord/stores/StoreGuildRoleSubscriptions$GuildRoleSubscriptionGroupState;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState;
        private final Map<Long, Integer> roleMemberCounts;

        public StoreState(StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState, Map<Long, Integer> map) {
            this.guildRoleSubscriptionGroupState = guildRoleSubscriptionGroupState;
            this.roleMemberCounts = map;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState, Map map, int i, Object obj) {
            if ((i & 1) != 0) {
                guildRoleSubscriptionGroupState = storeState.guildRoleSubscriptionGroupState;
            }
            if ((i & 2) != 0) {
                map = storeState.roleMemberCounts;
            }
            return storeState.copy(guildRoleSubscriptionGroupState, map);
        }

        public final StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState component1() {
            return this.guildRoleSubscriptionGroupState;
        }

        public final Map<Long, Integer> component2() {
            return this.roleMemberCounts;
        }

        public final StoreState copy(StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState, Map<Long, Integer> map) {
            return new StoreState(guildRoleSubscriptionGroupState, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guildRoleSubscriptionGroupState, storeState.guildRoleSubscriptionGroupState) && m.areEqual(this.roleMemberCounts, storeState.roleMemberCounts);
        }

        public final StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState getGuildRoleSubscriptionGroupState() {
            return this.guildRoleSubscriptionGroupState;
        }

        public final Map<Long, Integer> getRoleMemberCounts() {
            return this.roleMemberCounts;
        }

        public int hashCode() {
            StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState = this.guildRoleSubscriptionGroupState;
            int i = 0;
            int hashCode = (guildRoleSubscriptionGroupState != null ? guildRoleSubscriptionGroupState.hashCode() : 0) * 31;
            Map<Long, Integer> map = this.roleMemberCounts;
            if (map != null) {
                i = map.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guildRoleSubscriptionGroupState=");
            R.append(this.guildRoleSubscriptionGroupState);
            R.append(", roleMemberCounts=");
            return a.L(R, this.roleMemberCounts, ")");
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Failed", "Loaded", "Loading", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState$Loaded;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState$Failed;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState$Failed;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Failed extends ViewState {
            public static final Failed INSTANCE = new Failed();

            private Failed() {
                super(null);
            }
        }

        /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState$Loaded;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState;", "", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "component1", "()Ljava/util/List;", "items", "copy", "(Ljava/util/List;)Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final List<GuildRoleSubscriptionEarningsAdapterItem> items;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Loaded(List<? extends GuildRoleSubscriptionEarningsAdapterItem> list) {
                super(null);
                m.checkNotNullParameter(list, "items");
                this.items = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = loaded.items;
                }
                return loaded.copy(list);
            }

            public final List<GuildRoleSubscriptionEarningsAdapterItem> component1() {
                return this.items;
            }

            public final Loaded copy(List<? extends GuildRoleSubscriptionEarningsAdapterItem> list) {
                m.checkNotNullParameter(list, "items");
                return new Loaded(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Loaded) && m.areEqual(this.items, ((Loaded) obj).items);
                }
                return true;
            }

            public final List<GuildRoleSubscriptionEarningsAdapterItem> getItems() {
                return this.items;
            }

            public int hashCode() {
                List<GuildRoleSubscriptionEarningsAdapterItem> list = this.items;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("Loaded(items="), this.items, ")");
            }
        }

        /* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ ServerSettingsGuildRoleSubscriptionEarningsViewModel(long r10, com.discord.utilities.rest.RestAPI r12, com.discord.stores.StoreGuildRoleSubscriptions r13, com.discord.stores.StoreGuildRoleMemberCounts r14, com.discord.stores.updates.ObservationDeck r15, rx.Observable r16, int r17, kotlin.jvm.internal.DefaultConstructorMarker r18) {
        /*
            r9 = this;
            r0 = r17 & 2
            if (r0 == 0) goto Lc
            com.discord.utilities.rest.RestAPI$Companion r0 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r0 = r0.getApi()
            r4 = r0
            goto Ld
        Lc:
            r4 = r12
        Ld:
            r0 = r17 & 4
            if (r0 == 0) goto L19
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildRoleSubscriptions r0 = r0.getGuildRoleSubscriptions()
            r5 = r0
            goto L1a
        L19:
            r5 = r13
        L1a:
            r0 = r17 & 8
            if (r0 == 0) goto L26
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuildRoleMemberCounts r0 = r0.getGuildRoleMemberCounts()
            r6 = r0
            goto L27
        L26:
            r6 = r14
        L27:
            r0 = r17 & 16
            if (r0 == 0) goto L31
            com.discord.stores.updates.ObservationDeck r0 = com.discord.stores.updates.ObservationDeckProvider.get()
            r7 = r0
            goto L32
        L31:
            r7 = r15
        L32:
            r0 = r17 & 32
            if (r0 == 0) goto L45
            com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel$Companion r0 = com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel.Companion
            r12 = r0
            r13 = r10
            r15 = r7
            r16 = r5
            r17 = r6
            rx.Observable r0 = com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel.Companion.access$observeStoreState(r12, r13, r15, r16, r17)
            r8 = r0
            goto L47
        L45:
            r8 = r16
        L47:
            r1 = r9
            r2 = r10
            r1.<init>(r2, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel.<init>(long, com.discord.utilities.rest.RestAPI, com.discord.stores.StoreGuildRoleSubscriptions, com.discord.stores.StoreGuildRoleMemberCounts, com.discord.stores.updates.ObservationDeck, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        this.guildRoleMemberCounts = storeState.getRoleMemberCounts();
        StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState guildRoleSubscriptionGroupState = storeState.getGuildRoleSubscriptionGroupState();
        if (guildRoleSubscriptionGroupState instanceof StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded) {
            GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing = ((StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Loaded) storeState.getGuildRoleSubscriptionGroupState()).getGuildRoleSubscriptionGroupListing();
            if (guildRoleSubscriptionGroupListing == null) {
                updateViewState(ViewState.Failed.INSTANCE);
                return;
            }
            this.guildRoleSubscriptionGroupListing = guildRoleSubscriptionGroupListing;
            updateAdapterItemList();
        } else if (guildRoleSubscriptionGroupState instanceof StoreGuildRoleSubscriptions.GuildRoleSubscriptionGroupState.Failed) {
            updateViewState(ViewState.Failed.INSTANCE);
        }
    }

    private final void updateAdapterItemList() {
        GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing = this.guildRoleSubscriptionGroupListing;
        TotalEarningMetrics totalEarningMetrics = this.totalEarningMetrics;
        Application application = this.application;
        if (application != null && guildRoleSubscriptionGroupListing != null && totalEarningMetrics != null) {
            long g = application.g();
            Team j = application.j();
            updateViewState(new ViewState.Loaded(GuildRoleSubscriptionEarningsListItemGeneratorKt.generateEarningsAdapterItemList(g, guildRoleSubscriptionGroupListing, totalEarningMetrics, j != null ? Long.valueOf(j.a()) : null, this.guildRoleMemberCounts)));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updatePayouts(List<PayoutGroup> list) {
        this.totalEarningMetrics = GuildRoleSubscriptionPayoutUtils.INSTANCE.aggregateTotalEarningMetrics(list);
        updateAdapterItemList();
    }

    public final long getGuildId() {
        return this.guildId;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerSettingsGuildRoleSubscriptionEarningsViewModel(long j, RestAPI restAPI, StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, StoreGuildRoleMemberCounts storeGuildRoleMemberCounts, ObservationDeck observationDeck, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(storeGuildRoleSubscriptions, "storeGuildRoleSubscriptions");
        m.checkNotNullParameter(storeGuildRoleMemberCounts, "storeGuildRoleMemberCounts");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(observable, "storeObservable");
        this.guildId = j;
        this.restAPI = restAPI;
        storeGuildRoleSubscriptions.fetchGuildRoleSubscriptionGroupsForGuild(j);
        storeGuildRoleMemberCounts.fetchGuildRoleMemberCounts(j);
        Observable<R> z2 = restAPI.getGuildApplications(j, true).z(new b<List<? extends Application>, Observable<? extends List<? extends PayoutGroup>>>() { // from class: com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel.1
            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends List<? extends PayoutGroup>> call(List<? extends Application> list) {
                return call2((List<Application>) list);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends List<PayoutGroup>> call2(List<Application> list) {
                T t;
                boolean z3;
                m.checkNotNullExpressionValue(list, "applications");
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (((Application) t).k() == ApplicationType.GUILD_ROLE_SUBSCRIPTIONS) {
                        z3 = true;
                        continue;
                    } else {
                        z3 = false;
                        continue;
                    }
                    if (z3) {
                        break;
                    }
                }
                Application application = (Application) t;
                if (application == null) {
                    return Observable.w(new IllegalStateException("No application for guild"));
                }
                ServerSettingsGuildRoleSubscriptionEarningsViewModel.this.application = application;
                return ServerSettingsGuildRoleSubscriptionEarningsViewModel.this.restAPI.getPaymentPayoutGroups(application.g());
            }
        });
        m.checkNotNullExpressionValue(z2, "restAPI\n        .getGuil…Application.id)\n        }");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(z2, false, 1, null), this, null, 2, null), ServerSettingsGuildRoleSubscriptionEarningsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new AnonymousClass3(), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), ServerSettingsGuildRoleSubscriptionEarningsViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass4());
    }
}
