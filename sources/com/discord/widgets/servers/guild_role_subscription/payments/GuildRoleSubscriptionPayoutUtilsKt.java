package com.discord.widgets.servers.guild_role_subscription.payments;

import com.discord.api.guildrolesubscription.Payout;
import com.discord.api.guildrolesubscription.PayoutGroup;
import com.discord.api.guildrolesubscription.PayoutGroupStatus;
import com.discord.api.guildrolesubscription.PayoutStatus;
import com.discord.widgets.servers.guild_role_subscription.model.TotalPayoutsForPeriod;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: GuildRoleSubscriptionPayoutUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0001*\u00020\u0000¢\u0006\u0004\b\u0002\u0010\u0003\u001a\u0011\u0010\u0005\u001a\u00020\u0004*\u00020\u0000¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "Lcom/discord/api/guildrolesubscription/PayoutGroupStatus;", "getGroupStatusForPeriod", "(Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;)Lcom/discord/api/guildrolesubscription/PayoutGroupStatus;", "Lcom/discord/api/guildrolesubscription/PayoutStatus;", "getPayoutStatusForPeriod", "(Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;)Lcom/discord/api/guildrolesubscription/PayoutStatus;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionPayoutUtilsKt {
    public static final PayoutGroupStatus getGroupStatusForPeriod(TotalPayoutsForPeriod totalPayoutsForPeriod) {
        Collection<PayoutGroup> values;
        List list;
        PayoutGroup payoutGroup;
        m.checkNotNullParameter(totalPayoutsForPeriod, "$this$getGroupStatusForPeriod");
        Map<Long, PayoutGroup> payoutGroups = totalPayoutsForPeriod.getPayoutGroups();
        if (payoutGroups == null || (values = payoutGroups.values()) == null || (list = u.toList(values)) == null || (payoutGroup = (PayoutGroup) u.getOrNull(list, 0)) == null) {
            return null;
        }
        return payoutGroup.f();
    }

    public static final PayoutStatus getPayoutStatusForPeriod(TotalPayoutsForPeriod totalPayoutsForPeriod) {
        Collection<PayoutGroup> values;
        List list;
        PayoutGroup payoutGroup;
        Payout d;
        PayoutStatus a;
        m.checkNotNullParameter(totalPayoutsForPeriod, "$this$getPayoutStatusForPeriod");
        Map<Long, PayoutGroup> payoutGroups = totalPayoutsForPeriod.getPayoutGroups();
        return (payoutGroups == null || (values = payoutGroups.values()) == null || (list = u.toList(values)) == null || (payoutGroup = (PayoutGroup) u.getOrNull(list, 0)) == null || (d = payoutGroup.d()) == null || (a = d.a()) == null) ? PayoutStatus.UNKNOWN : a;
    }
}
