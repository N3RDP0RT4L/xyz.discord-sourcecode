package com.discord.widgets.servers.guild_role_subscription.payments;

import com.discord.api.guildrolesubscription.GuildRoleSubscriptionGroupListing;
import com.discord.api.guildrolesubscription.GuildRoleSubscriptionTierListing;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionUtilsKt;
import com.discord.widgets.servers.guild_role_subscription.model.TotalEarningMetrics;
import com.discord.widgets.servers.guild_role_subscription.model.TotalPayoutsForPeriod;
import com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsAdapterItem;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionEarningsListItemGenerator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u001aU\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\u00002\u001a\b\u0002\u0010\u000b\u001a\u0014\u0012\b\u0012\u00060\u0000j\u0002`\t\u0012\u0004\u0012\u00020\n\u0018\u00010\b¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0010"}, d2 = {"", "Lcom/discord/primitives/ApplicationId;", "applicationId", "Lcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;", "guildRoleSubscriptionGroupListing", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalEarningMetrics;", "totalEarningMetrics", "teamId", "", "Lcom/discord/primitives/RoleId;", "", "guildRoleMemberCounts", "", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "generateEarningsAdapterItemList", "(JLcom/discord/api/guildrolesubscription/GuildRoleSubscriptionGroupListing;Lcom/discord/widgets/servers/guild_role_subscription/model/TotalEarningMetrics;Ljava/lang/Long;Ljava/util/Map;)Ljava/util/List;", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionEarningsListItemGeneratorKt {
    public static final List<GuildRoleSubscriptionEarningsAdapterItem> generateEarningsAdapterItemList(long j, GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing, TotalEarningMetrics totalEarningMetrics, Long l, Map<Long, Integer> map) {
        m.checkNotNullParameter(guildRoleSubscriptionGroupListing, "guildRoleSubscriptionGroupListing");
        m.checkNotNullParameter(totalEarningMetrics, "totalEarningMetrics");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsItem(GuildRoleSubscriptionPayoutUtils.INSTANCE.calculateEarningMetrics(totalEarningMetrics.getCurrentPeriod(), (TotalPayoutsForPeriod) u.getOrNull(totalEarningMetrics.getPreviousPeriods(), 0)), l));
        List<GuildRoleSubscriptionTierListing> h = guildRoleSubscriptionGroupListing.h();
        if (h == null) {
            h = n.emptyList();
        }
        if (!(h == null || h.isEmpty())) {
            arrayList.add(GuildRoleSubscriptionEarningsAdapterItem.CurrentMonthEarningMetricsByTiersHeader.INSTANCE);
            arrayList.add(new GuildRoleSubscriptionEarningsAdapterItem.CurrentMonthEarningMetricsByTiers(h, j, totalEarningMetrics.getCurrentPeriod(), map));
        }
        if ((h == null || h.isEmpty()) || !(!totalEarningMetrics.getPreviousPeriods().isEmpty())) {
            arrayList.add(new GuildRoleSubscriptionEarningsAdapterItem.SectionHeader(R.string.guild_role_subscription_earnings_earnings_history_header, R.string.guild_role_subscription_earnings_earnings_history_empty));
        } else {
            arrayList.add(new GuildRoleSubscriptionEarningsAdapterItem.SectionHeader(R.string.guild_role_subscription_earnings_earnings_history_header, R.string.guild_role_subscription_earnings_earnings_history_description));
            arrayList.add(GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsHistoryHeader.INSTANCE);
            int i = 0;
            for (Object obj : totalEarningMetrics.getPreviousPeriods()) {
                i++;
                if (i < 0) {
                    n.throwIndexOverflow();
                }
                TotalPayoutsForPeriod totalPayoutsForPeriod = (TotalPayoutsForPeriod) obj;
                arrayList.add(new GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsHistory(GuildRoleSubscriptionUtilsKt.getStatusMedia(GuildRoleSubscriptionPayoutUtilsKt.getPayoutStatusForPeriod(totalPayoutsForPeriod)), totalPayoutsForPeriod, h, i == totalEarningMetrics.getPreviousPeriods().size() - 1));
            }
        }
        return arrayList;
    }

    public static /* synthetic */ List generateEarningsAdapterItemList$default(long j, GuildRoleSubscriptionGroupListing guildRoleSubscriptionGroupListing, TotalEarningMetrics totalEarningMetrics, Long l, Map map, int i, Object obj) {
        if ((i & 16) != 0) {
            map = null;
        }
        return generateEarningsAdapterItemList(j, guildRoleSubscriptionGroupListing, totalEarningMetrics, l, map);
    }
}
