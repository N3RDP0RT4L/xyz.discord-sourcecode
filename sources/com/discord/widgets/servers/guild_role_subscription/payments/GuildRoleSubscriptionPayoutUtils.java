package com.discord.widgets.servers.guild_role_subscription.payments;

import andhook.lib.HookHelper;
import com.discord.api.guildrolesubscription.PayoutGroup;
import com.discord.api.guildrolesubscription.PayoutGroupStatus;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.servers.guild_role_subscription.model.CurrentMonthEarningMetrics;
import com.discord.widgets.servers.guild_role_subscription.model.TotalEarningMetrics;
import com.discord.widgets.servers.guild_role_subscription.model.TotalPayoutsForPeriod;
import d0.g0.t;
import d0.t.h0;
import d0.t.u;
import d0.u.a;
import d0.z.d.m;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: GuildRoleSubscriptionPayoutUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J#\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00022\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u001b\u0010\t\u001a\u00020\b2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u000e\u001a\u00020\r2\b\u0010\u000b\u001a\u0004\u0018\u00010\u00052\b\u0010\f\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u000e\u0010\u000fJ\u0015\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionPayoutUtils;", "", "", "Lcom/discord/api/guildrolesubscription/PayoutGroup;", "payoutGroupList", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;", "aggregatePayoutsByPeriod", "(Ljava/util/List;)Ljava/util/List;", "Lcom/discord/widgets/servers/guild_role_subscription/model/TotalEarningMetrics;", "aggregateTotalEarningMetrics", "(Ljava/util/List;)Lcom/discord/widgets/servers/guild_role_subscription/model/TotalEarningMetrics;", "currentPeriod", "previousPeriod", "Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;", "calculateEarningMetrics", "(Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;Lcom/discord/widgets/servers/guild_role_subscription/model/TotalPayoutsForPeriod;)Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;", "", "teamId", "", "getTeamPayoutSetupUrl", "(J)Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionPayoutUtils {
    public static final GuildRoleSubscriptionPayoutUtils INSTANCE = new GuildRoleSubscriptionPayoutUtils();

    private GuildRoleSubscriptionPayoutUtils() {
    }

    private final List<TotalPayoutsForPeriod> aggregatePayoutsByPeriod(List<PayoutGroup> list) {
        Map map;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (PayoutGroup payoutGroup : list) {
            String e = payoutGroup.e();
            TotalPayoutsForPeriod totalPayoutsForPeriod = (TotalPayoutsForPeriod) linkedHashMap.get(e);
            if (totalPayoutsForPeriod == null) {
                totalPayoutsForPeriod = new TotalPayoutsForPeriod(e, 0L, 0L, null, 14, null);
            }
            long component3 = totalPayoutsForPeriod.component3();
            Map<Long, PayoutGroup> component4 = totalPayoutsForPeriod.component4();
            long a = payoutGroup.a() + component3;
            long c = payoutGroup.c();
            if (component4 == null || (map = h0.toMutableMap(component4)) == null) {
                map = new LinkedHashMap();
            }
            map.put(Long.valueOf(payoutGroup.b()), payoutGroup);
            linkedHashMap.put(e, new TotalPayoutsForPeriod(e, c, a, h0.toMap(map)));
        }
        return u.sortedWith(linkedHashMap.values(), new Comparator() { // from class: com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionPayoutUtils$aggregatePayoutsByPeriod$$inlined$sortedByDescending$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return a.compareValues(((TotalPayoutsForPeriod) t2).getPeriodStartingAt(), ((TotalPayoutsForPeriod) t).getPeriodStartingAt());
            }
        });
    }

    public final TotalEarningMetrics aggregateTotalEarningMetrics(List<PayoutGroup> list) {
        m.checkNotNullParameter(list, "payoutGroupList");
        List<TotalPayoutsForPeriod> aggregatePayoutsByPeriod = aggregatePayoutsByPeriod(list);
        TotalPayoutsForPeriod totalPayoutsForPeriod = (TotalPayoutsForPeriod) u.getOrNull(aggregatePayoutsByPeriod, 0);
        if ((totalPayoutsForPeriod != null ? GuildRoleSubscriptionPayoutUtilsKt.getGroupStatusForPeriod(totalPayoutsForPeriod) : null) == PayoutGroupStatus.OPEN) {
            aggregatePayoutsByPeriod = u.drop(aggregatePayoutsByPeriod, 1);
        } else {
            totalPayoutsForPeriod = null;
        }
        return new TotalEarningMetrics(calculateEarningMetrics(totalPayoutsForPeriod, (TotalPayoutsForPeriod) u.getOrNull(aggregatePayoutsByPeriod, 0)), totalPayoutsForPeriod, aggregatePayoutsByPeriod);
    }

    public final CurrentMonthEarningMetrics calculateEarningMetrics(TotalPayoutsForPeriod totalPayoutsForPeriod, TotalPayoutsForPeriod totalPayoutsForPeriod2) {
        Long l;
        Long l2;
        if (totalPayoutsForPeriod == null) {
            return new CurrentMonthEarningMetrics(0L, null, 0L, null, null, null, 63, null);
        }
        long revenue = totalPayoutsForPeriod.getRevenue();
        long subscriberCount = totalPayoutsForPeriod.getSubscriberCount();
        PayoutGroupStatus groupStatusForPeriod = GuildRoleSubscriptionPayoutUtilsKt.getGroupStatusForPeriod(totalPayoutsForPeriod);
        PayoutGroupStatus payoutGroupStatus = PayoutGroupStatus.PAYOUT_CREATED;
        String periodStartingAt = (groupStatusForPeriod == payoutGroupStatus || totalPayoutsForPeriod2 == null) ? totalPayoutsForPeriod.getPeriodStartingAt() : null;
        if (totalPayoutsForPeriod2 != null) {
            l2 = Long.valueOf((revenue / totalPayoutsForPeriod2.getRevenue()) - 1);
            l = Long.valueOf(subscriberCount - totalPayoutsForPeriod2.getSubscriberCount());
            if (periodStartingAt == null && GuildRoleSubscriptionPayoutUtilsKt.getGroupStatusForPeriod(totalPayoutsForPeriod2) == payoutGroupStatus) {
                periodStartingAt = totalPayoutsForPeriod2.getPeriodStartingAt();
            }
        } else {
            l2 = null;
            l = null;
        }
        if (!(periodStartingAt == null || t.isBlank(periodStartingAt))) {
            periodStartingAt = TimeUtils.addMonthsAndDaysToDate(periodStartingAt, 1, 30);
        }
        return new CurrentMonthEarningMetrics(revenue, l2, subscriberCount, l, periodStartingAt, totalPayoutsForPeriod.getPeriodStartingAt());
    }

    public final String getTeamPayoutSetupUrl(long j) {
        return "http://discord.com/developers/teams/" + j + "/payout-settings";
    }
}
