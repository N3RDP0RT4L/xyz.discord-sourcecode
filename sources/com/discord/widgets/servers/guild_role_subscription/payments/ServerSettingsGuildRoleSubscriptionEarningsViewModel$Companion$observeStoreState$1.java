package com.discord.widgets.servers.guild_role_subscription.payments;

import com.discord.stores.StoreGuildRoleMemberCounts;
import com.discord.stores.StoreGuildRoleSubscriptions;
import com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: ServerSettingsGuildRoleSubscriptionEarningsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;", "invoke", "()Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsGuildRoleSubscriptionEarningsViewModel$Companion$observeStoreState$1 extends o implements Function0<ServerSettingsGuildRoleSubscriptionEarningsViewModel.StoreState> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuildRoleMemberCounts $storeGuildRoleMemberCounts;
    public final /* synthetic */ StoreGuildRoleSubscriptions $storeGuildRoleSubscriptions;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerSettingsGuildRoleSubscriptionEarningsViewModel$Companion$observeStoreState$1(StoreGuildRoleSubscriptions storeGuildRoleSubscriptions, long j, StoreGuildRoleMemberCounts storeGuildRoleMemberCounts) {
        super(0);
        this.$storeGuildRoleSubscriptions = storeGuildRoleSubscriptions;
        this.$guildId = j;
        this.$storeGuildRoleMemberCounts = storeGuildRoleMemberCounts;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ServerSettingsGuildRoleSubscriptionEarningsViewModel.StoreState invoke() {
        return new ServerSettingsGuildRoleSubscriptionEarningsViewModel.StoreState(this.$storeGuildRoleSubscriptions.getGuildRoleSubscriptionState(this.$guildId), this.$storeGuildRoleMemberCounts.getGuildRoleMemberCounts(this.$guildId));
    }
}
