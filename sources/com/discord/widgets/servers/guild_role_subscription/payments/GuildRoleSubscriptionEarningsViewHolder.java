package com.discord.widgets.servers.guild_role_subscription.payments;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.ViewGuildRoleSubscriptionEarningTableHeaderItemBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionRevenueMetricsItemBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionSectionHeaderItemBinding;
import com.discord.utilities.billing.PremiumUtilsKt;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.servers.guild_role_subscription.model.TotalPayoutsForPeriod;
import com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsAdapterItem;
import d0.z.d.m;
import java.text.SimpleDateFormat;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionEarningsViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\u000b\f\r\u000e\u000fB\u0011\b\u0002\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006\u0082\u0001\u0005\u0010\u0011\u0012\u0013\u0014¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "adapterItem", "", "configure", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;)V", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;)V", "CurrentRevenueByTiersViewHolder", "EarningMetricsHistoryListItemViewHolder", "EarningMetricsTableHeader", "EarningsSectionHeader", "RevenueMetricsViewHolder", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$RevenueMetricsViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$CurrentRevenueByTiersViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningsSectionHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningMetricsTableHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningMetricsHistoryListItemViewHolder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class GuildRoleSubscriptionEarningsViewHolder extends RecyclerView.ViewHolder {

    /* compiled from: GuildRoleSubscriptionEarningsViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\n\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\t¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$CurrentRevenueByTiersViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "adapterItem", "", "configure", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;)V", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionTierRevenueListView;", "component1", "()Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionTierRevenueListView;", "view", "copy", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionTierRevenueListView;)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$CurrentRevenueByTiersViewHolder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionTierRevenueListView;", "getView", HookHelper.constructorName, "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionTierRevenueListView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CurrentRevenueByTiersViewHolder extends GuildRoleSubscriptionEarningsViewHolder {
        private final GuildRoleSubscriptionTierRevenueListView view;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public CurrentRevenueByTiersViewHolder(GuildRoleSubscriptionTierRevenueListView guildRoleSubscriptionTierRevenueListView) {
            super(guildRoleSubscriptionTierRevenueListView, null);
            m.checkNotNullParameter(guildRoleSubscriptionTierRevenueListView, "view");
            this.view = guildRoleSubscriptionTierRevenueListView;
        }

        public static /* synthetic */ CurrentRevenueByTiersViewHolder copy$default(CurrentRevenueByTiersViewHolder currentRevenueByTiersViewHolder, GuildRoleSubscriptionTierRevenueListView guildRoleSubscriptionTierRevenueListView, int i, Object obj) {
            if ((i & 1) != 0) {
                guildRoleSubscriptionTierRevenueListView = currentRevenueByTiersViewHolder.view;
            }
            return currentRevenueByTiersViewHolder.copy(guildRoleSubscriptionTierRevenueListView);
        }

        public final GuildRoleSubscriptionTierRevenueListView component1() {
            return this.view;
        }

        @Override // com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder
        public void configure(GuildRoleSubscriptionEarningsAdapterItem guildRoleSubscriptionEarningsAdapterItem) {
            m.checkNotNullParameter(guildRoleSubscriptionEarningsAdapterItem, "adapterItem");
            if (!(guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.CurrentMonthEarningMetricsByTiers)) {
                guildRoleSubscriptionEarningsAdapterItem = null;
            }
            GuildRoleSubscriptionEarningsAdapterItem.CurrentMonthEarningMetricsByTiers currentMonthEarningMetricsByTiers = (GuildRoleSubscriptionEarningsAdapterItem.CurrentMonthEarningMetricsByTiers) guildRoleSubscriptionEarningsAdapterItem;
            if (currentMonthEarningMetricsByTiers != null) {
                GuildRoleSubscriptionTierRevenueListView.configureUI$default(this.view, currentMonthEarningMetricsByTiers.getTotalPayoutsForPeriod(), currentMonthEarningMetricsByTiers.getTierListings(), false, Long.valueOf(currentMonthEarningMetricsByTiers.getApplicationId()), currentMonthEarningMetricsByTiers.getRoleMemberCounts(), 4, null);
            }
        }

        public final CurrentRevenueByTiersViewHolder copy(GuildRoleSubscriptionTierRevenueListView guildRoleSubscriptionTierRevenueListView) {
            m.checkNotNullParameter(guildRoleSubscriptionTierRevenueListView, "view");
            return new CurrentRevenueByTiersViewHolder(guildRoleSubscriptionTierRevenueListView);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof CurrentRevenueByTiersViewHolder) && m.areEqual(this.view, ((CurrentRevenueByTiersViewHolder) obj).view);
            }
            return true;
        }

        public final GuildRoleSubscriptionTierRevenueListView getView() {
            return this.view;
        }

        public int hashCode() {
            GuildRoleSubscriptionTierRevenueListView guildRoleSubscriptionTierRevenueListView = this.view;
            if (guildRoleSubscriptionTierRevenueListView != null) {
                return guildRoleSubscriptionTierRevenueListView.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("CurrentRevenueByTiersViewHolder(view=");
            R.append(this.view);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\b\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fB\u000f\u0012\u0006\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u0019\u0010\n\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u001b\u001a\u0004\b\u001c\u0010\t¨\u0006 "}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningMetricsHistoryListItemViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "adapterItem", "", "configure", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;)V", "Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding;", "component1", "()Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding;", "binding", "copy", "(Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding;)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningMetricsHistoryListItemViewHolder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/text/SimpleDateFormat;", "dateFormat", "Ljava/text/SimpleDateFormat;", "Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding;", "getBinding", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EarningMetricsHistoryListItemViewHolder extends GuildRoleSubscriptionEarningsViewHolder {
        public static final Companion Companion = new Companion(null);
        private static final String DATE_FORMAT = "MMM d";
        private final ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding binding;
        private final SimpleDateFormat dateFormat;

        /* compiled from: GuildRoleSubscriptionEarningsViewHolder.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningMetricsHistoryListItemViewHolder$Companion;", "", "", "DATE_FORMAT", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public EarningMetricsHistoryListItemViewHolder(com.discord.databinding.ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding r5) {
            /*
                r4 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r5, r0)
                androidx.constraintlayout.widget.ConstraintLayout r0 = r5.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r4.<init>(r0, r1)
                r4.binding = r5
                java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat
                com.discord.utilities.locale.LocaleManager r1 = new com.discord.utilities.locale.LocaleManager
                r1.<init>()
                android.view.View r2 = r4.itemView
                java.lang.String r3 = "itemView"
                d0.z.d.m.checkNotNullExpressionValue(r2, r3)
                android.content.Context r2 = r2.getContext()
                java.util.Locale r1 = r1.getPrimaryLocale(r2)
                java.lang.String r2 = "MMM d"
                r0.<init>(r2, r1)
                r4.dateFormat = r0
                androidx.constraintlayout.widget.ConstraintLayout r5 = r5.a
                com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder$EarningMetricsHistoryListItemViewHolder$1 r0 = new com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder$EarningMetricsHistoryListItemViewHolder$1
                r0.<init>()
                r5.setOnClickListener(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder.EarningMetricsHistoryListItemViewHolder.<init>(com.discord.databinding.ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding):void");
        }

        public static /* synthetic */ EarningMetricsHistoryListItemViewHolder copy$default(EarningMetricsHistoryListItemViewHolder earningMetricsHistoryListItemViewHolder, ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding, int i, Object obj) {
            if ((i & 1) != 0) {
                viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding = earningMetricsHistoryListItemViewHolder.binding;
            }
            return earningMetricsHistoryListItemViewHolder.copy(viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding);
        }

        public final ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding component1() {
            return this.binding;
        }

        @Override // com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder
        public void configure(GuildRoleSubscriptionEarningsAdapterItem guildRoleSubscriptionEarningsAdapterItem) {
            GuildRoleSubscriptionEarningsAdapterItem guildRoleSubscriptionEarningsAdapterItem2 = guildRoleSubscriptionEarningsAdapterItem;
            m.checkNotNullParameter(guildRoleSubscriptionEarningsAdapterItem2, "adapterItem");
            if (!(guildRoleSubscriptionEarningsAdapterItem2 instanceof GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsHistory)) {
                guildRoleSubscriptionEarningsAdapterItem2 = null;
            }
            GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsHistory earningMetricsHistory = (GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsHistory) guildRoleSubscriptionEarningsAdapterItem2;
            if (earningMetricsHistory != null) {
                TotalPayoutsForPeriod totalPayoutsForPeriod = earningMetricsHistory.getTotalPayoutsForPeriod();
                TextView textView = this.binding.f2177b;
                m.checkNotNullExpressionValue(textView, "binding.revenueHistoryItemAmount");
                int revenue = (int) totalPayoutsForPeriod.getRevenue();
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "itemView.context");
                textView.setText(PremiumUtilsKt.getFormattedPriceUsd(revenue, context));
                TextView textView2 = this.binding.c;
                m.checkNotNullExpressionValue(textView2, "binding.revenueHistoryItemDescription");
                textView2.setText(TimeUtils.renderUtcDate$default(TimeUtils.INSTANCE, totalPayoutsForPeriod.getPeriodStartingAt(), a.x(this.itemView, "itemView", "itemView.context"), (String) null, this.dateFormat, 0, 20, (Object) null));
                TextView textView3 = this.binding.e;
                m.checkNotNullExpressionValue(textView3, "binding.revenueHistoryItemSubscriberCount");
                textView3.setText(String.valueOf(totalPayoutsForPeriod.getSubscriberCount()));
                GuildRoleSubscriptionTierRevenueListView.configureUI$default(this.binding.f, earningMetricsHistory.getTotalPayoutsForPeriod(), earningMetricsHistory.getTierListings(), true, null, null, 24, null);
                TextView textView4 = this.binding.d;
                m.checkNotNullExpressionValue(textView4, "binding.revenueHistoryItemStatus");
                View view2 = this.itemView;
                m.checkNotNullExpressionValue(view2, "itemView");
                ViewExtensions.setTextAndVisibilityBy(textView4, view2.getContext().getText(earningMetricsHistory.getStatusMedia().getStatusStringRes()));
                TextView textView5 = this.binding.d;
                m.checkNotNullExpressionValue(textView5, "binding.revenueHistoryItemStatus");
                DrawableCompat.setCompoundDrawablesCompat$default(textView5, earningMetricsHistory.getStatusMedia().getIconDrawableRes(), 0, 0, 0, 14, (Object) null);
                if (earningMetricsHistory.isLastItem()) {
                    View view3 = this.itemView;
                    m.checkNotNullExpressionValue(view3, "itemView");
                    View view4 = this.itemView;
                    m.checkNotNullExpressionValue(view4, "itemView");
                    view3.setBackground(ContextCompat.getDrawable(view4.getContext(), R.drawable.drawable_bg_secondary_bottom_corners));
                    return;
                }
                View view5 = this.itemView;
                m.checkNotNullExpressionValue(view5, "itemView");
                view5.setBackgroundColor(ColorCompat.getThemedColor(view5, (int) R.attr.colorBackgroundSecondary));
            }
        }

        public final EarningMetricsHistoryListItemViewHolder copy(ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding) {
            m.checkNotNullParameter(viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding, "binding");
            return new EarningMetricsHistoryListItemViewHolder(viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof EarningMetricsHistoryListItemViewHolder) && m.areEqual(this.binding, ((EarningMetricsHistoryListItemViewHolder) obj).binding);
            }
            return true;
        }

        public final ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding getBinding() {
            return this.binding;
        }

        public int hashCode() {
            ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding = this.binding;
            if (viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding != null) {
                return viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("EarningMetricsHistoryListItemViewHolder(binding=");
            R.append(this.binding);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\n\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\t¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningMetricsTableHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "adapterItem", "", "configure", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;)V", "Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningTableHeaderItemBinding;", "component1", "()Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningTableHeaderItemBinding;", "binding", "copy", "(Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningTableHeaderItemBinding;)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningMetricsTableHeader;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningTableHeaderItemBinding;", "getBinding", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionEarningTableHeaderItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EarningMetricsTableHeader extends GuildRoleSubscriptionEarningsViewHolder {
        private final ViewGuildRoleSubscriptionEarningTableHeaderItemBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public EarningMetricsTableHeader(com.discord.databinding.ViewGuildRoleSubscriptionEarningTableHeaderItemBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                androidx.constraintlayout.widget.ConstraintLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder.EarningMetricsTableHeader.<init>(com.discord.databinding.ViewGuildRoleSubscriptionEarningTableHeaderItemBinding):void");
        }

        public static /* synthetic */ EarningMetricsTableHeader copy$default(EarningMetricsTableHeader earningMetricsTableHeader, ViewGuildRoleSubscriptionEarningTableHeaderItemBinding viewGuildRoleSubscriptionEarningTableHeaderItemBinding, int i, Object obj) {
            if ((i & 1) != 0) {
                viewGuildRoleSubscriptionEarningTableHeaderItemBinding = earningMetricsTableHeader.binding;
            }
            return earningMetricsTableHeader.copy(viewGuildRoleSubscriptionEarningTableHeaderItemBinding);
        }

        public final ViewGuildRoleSubscriptionEarningTableHeaderItemBinding component1() {
            return this.binding;
        }

        @Override // com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder
        public void configure(GuildRoleSubscriptionEarningsAdapterItem guildRoleSubscriptionEarningsAdapterItem) {
            m.checkNotNullParameter(guildRoleSubscriptionEarningsAdapterItem, "adapterItem");
            boolean z2 = guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.CurrentMonthEarningMetricsByTiersHeader;
            int i = z2 ? R.string.guild_role_subscription_earnings_table_tiers : R.string.guild_role_subscription_earnings_table_period;
            TextView textView = this.binding.d;
            if (z2) {
                textView.setText(R.string.guild_role_subscription_earnings_table_tiers);
                textView.setGravity(8388627);
            } else {
                textView.setText(R.string.guild_role_subscription_earnings_table_period);
                textView.setGravity(17);
            }
            this.binding.d.setText(i);
            this.binding.f2176b.setText(R.string.guild_role_subscription_earnings_table_members);
            this.binding.c.setText(R.string.guild_role_subscription_earnings_table_amount);
        }

        public final EarningMetricsTableHeader copy(ViewGuildRoleSubscriptionEarningTableHeaderItemBinding viewGuildRoleSubscriptionEarningTableHeaderItemBinding) {
            m.checkNotNullParameter(viewGuildRoleSubscriptionEarningTableHeaderItemBinding, "binding");
            return new EarningMetricsTableHeader(viewGuildRoleSubscriptionEarningTableHeaderItemBinding);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof EarningMetricsTableHeader) && m.areEqual(this.binding, ((EarningMetricsTableHeader) obj).binding);
            }
            return true;
        }

        public final ViewGuildRoleSubscriptionEarningTableHeaderItemBinding getBinding() {
            return this.binding;
        }

        public int hashCode() {
            ViewGuildRoleSubscriptionEarningTableHeaderItemBinding viewGuildRoleSubscriptionEarningTableHeaderItemBinding = this.binding;
            if (viewGuildRoleSubscriptionEarningTableHeaderItemBinding != null) {
                return viewGuildRoleSubscriptionEarningTableHeaderItemBinding.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("EarningMetricsTableHeader(binding=");
            R.append(this.binding);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\n\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\t¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningsSectionHeader;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "adapterItem", "", "configure", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;)V", "Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;", "component1", "()Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;", "binding", "copy", "(Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$EarningsSectionHeader;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;", "getBinding", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionSectionHeaderItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EarningsSectionHeader extends GuildRoleSubscriptionEarningsViewHolder {
        private final ViewGuildRoleSubscriptionSectionHeaderItemBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public EarningsSectionHeader(com.discord.databinding.ViewGuildRoleSubscriptionSectionHeaderItemBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.LinearLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder.EarningsSectionHeader.<init>(com.discord.databinding.ViewGuildRoleSubscriptionSectionHeaderItemBinding):void");
        }

        public static /* synthetic */ EarningsSectionHeader copy$default(EarningsSectionHeader earningsSectionHeader, ViewGuildRoleSubscriptionSectionHeaderItemBinding viewGuildRoleSubscriptionSectionHeaderItemBinding, int i, Object obj) {
            if ((i & 1) != 0) {
                viewGuildRoleSubscriptionSectionHeaderItemBinding = earningsSectionHeader.binding;
            }
            return earningsSectionHeader.copy(viewGuildRoleSubscriptionSectionHeaderItemBinding);
        }

        public final ViewGuildRoleSubscriptionSectionHeaderItemBinding component1() {
            return this.binding;
        }

        @Override // com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder
        public void configure(GuildRoleSubscriptionEarningsAdapterItem guildRoleSubscriptionEarningsAdapterItem) {
            m.checkNotNullParameter(guildRoleSubscriptionEarningsAdapterItem, "adapterItem");
            if (!(guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.SectionHeader)) {
                guildRoleSubscriptionEarningsAdapterItem = null;
            }
            GuildRoleSubscriptionEarningsAdapterItem.SectionHeader sectionHeader = (GuildRoleSubscriptionEarningsAdapterItem.SectionHeader) guildRoleSubscriptionEarningsAdapterItem;
            if (sectionHeader != null) {
                this.binding.c.setText(sectionHeader.getSectionTitleResId());
                TextView textView = this.binding.f2183b;
                m.checkNotNullExpressionValue(textView, "binding.guildRoleSubscriptionSectionDescription");
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                ViewExtensions.setTextAndVisibilityBy(textView, view.getContext().getString(sectionHeader.getSectionDescriptionResId()));
            }
        }

        public final EarningsSectionHeader copy(ViewGuildRoleSubscriptionSectionHeaderItemBinding viewGuildRoleSubscriptionSectionHeaderItemBinding) {
            m.checkNotNullParameter(viewGuildRoleSubscriptionSectionHeaderItemBinding, "binding");
            return new EarningsSectionHeader(viewGuildRoleSubscriptionSectionHeaderItemBinding);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof EarningsSectionHeader) && m.areEqual(this.binding, ((EarningsSectionHeader) obj).binding);
            }
            return true;
        }

        public final ViewGuildRoleSubscriptionSectionHeaderItemBinding getBinding() {
            return this.binding;
        }

        public int hashCode() {
            ViewGuildRoleSubscriptionSectionHeaderItemBinding viewGuildRoleSubscriptionSectionHeaderItemBinding = this.binding;
            if (viewGuildRoleSubscriptionSectionHeaderItemBinding != null) {
                return viewGuildRoleSubscriptionSectionHeaderItemBinding.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("EarningsSectionHeader(binding=");
            R.append(this.binding);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: GuildRoleSubscriptionEarningsViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\n\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0016\u001a\u00020\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\n\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\t¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$RevenueMetricsViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "adapterItem", "", "configure", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;)V", "Lcom/discord/databinding/ViewGuildRoleSubscriptionRevenueMetricsItemBinding;", "component1", "()Lcom/discord/databinding/ViewGuildRoleSubscriptionRevenueMetricsItemBinding;", "binding", "copy", "(Lcom/discord/databinding/ViewGuildRoleSubscriptionRevenueMetricsItemBinding;)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder$RevenueMetricsViewHolder;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/databinding/ViewGuildRoleSubscriptionRevenueMetricsItemBinding;", "getBinding", HookHelper.constructorName, "(Lcom/discord/databinding/ViewGuildRoleSubscriptionRevenueMetricsItemBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RevenueMetricsViewHolder extends GuildRoleSubscriptionEarningsViewHolder {
        private final ViewGuildRoleSubscriptionRevenueMetricsItemBinding binding;

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public RevenueMetricsViewHolder(com.discord.databinding.ViewGuildRoleSubscriptionRevenueMetricsItemBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                androidx.constraintlayout.widget.ConstraintLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder.RevenueMetricsViewHolder.<init>(com.discord.databinding.ViewGuildRoleSubscriptionRevenueMetricsItemBinding):void");
        }

        public static /* synthetic */ RevenueMetricsViewHolder copy$default(RevenueMetricsViewHolder revenueMetricsViewHolder, ViewGuildRoleSubscriptionRevenueMetricsItemBinding viewGuildRoleSubscriptionRevenueMetricsItemBinding, int i, Object obj) {
            if ((i & 1) != 0) {
                viewGuildRoleSubscriptionRevenueMetricsItemBinding = revenueMetricsViewHolder.binding;
            }
            return revenueMetricsViewHolder.copy(viewGuildRoleSubscriptionRevenueMetricsItemBinding);
        }

        public final ViewGuildRoleSubscriptionRevenueMetricsItemBinding component1() {
            return this.binding;
        }

        @Override // com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder
        public void configure(GuildRoleSubscriptionEarningsAdapterItem guildRoleSubscriptionEarningsAdapterItem) {
            m.checkNotNullParameter(guildRoleSubscriptionEarningsAdapterItem, "adapterItem");
            if (!(guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsItem)) {
                guildRoleSubscriptionEarningsAdapterItem = null;
            }
            GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsItem earningMetricsItem = (GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsItem) guildRoleSubscriptionEarningsAdapterItem;
            if (earningMetricsItem != null) {
                GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics = this.binding.f2182b;
                int revenue = (int) earningMetricsItem.getCurrentMonthEarningMetrics().getRevenue();
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "itemView.context");
                guildRoleSubscriptionRevenueMetrics.configureUI(PremiumUtilsKt.getFormattedPriceUsd(revenue, context).toString(), true, earningMetricsItem.getCurrentMonthEarningMetrics().getMonthOverMonthRevenueChangePercent(), earningMetricsItem.getCurrentMonthEarningMetrics().getRevenueSinceDate());
                GuildRoleSubscriptionRevenueMetrics.configureUI$default(this.binding.d, String.valueOf(earningMetricsItem.getCurrentMonthEarningMetrics().getSubscriberCount()), false, earningMetricsItem.getCurrentMonthEarningMetrics().getMonthOverMonthSubscriberCountChange(), null, 8, null);
                this.binding.c.configureUI(earningMetricsItem.getCurrentMonthEarningMetrics().getNextPaymentDate(), earningMetricsItem.getTeamId());
            }
        }

        public final RevenueMetricsViewHolder copy(ViewGuildRoleSubscriptionRevenueMetricsItemBinding viewGuildRoleSubscriptionRevenueMetricsItemBinding) {
            m.checkNotNullParameter(viewGuildRoleSubscriptionRevenueMetricsItemBinding, "binding");
            return new RevenueMetricsViewHolder(viewGuildRoleSubscriptionRevenueMetricsItemBinding);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof RevenueMetricsViewHolder) && m.areEqual(this.binding, ((RevenueMetricsViewHolder) obj).binding);
            }
            return true;
        }

        public final ViewGuildRoleSubscriptionRevenueMetricsItemBinding getBinding() {
            return this.binding;
        }

        public int hashCode() {
            ViewGuildRoleSubscriptionRevenueMetricsItemBinding viewGuildRoleSubscriptionRevenueMetricsItemBinding = this.binding;
            if (viewGuildRoleSubscriptionRevenueMetricsItemBinding != null) {
                return viewGuildRoleSubscriptionRevenueMetricsItemBinding.hashCode();
            }
            return 0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.ViewHolder
        public String toString() {
            StringBuilder R = a.R("RevenueMetricsViewHolder(binding=");
            R.append(this.binding);
            R.append(")");
            return R.toString();
        }
    }

    private GuildRoleSubscriptionEarningsViewHolder(View view) {
        super(view);
    }

    public abstract void configure(GuildRoleSubscriptionEarningsAdapterItem guildRoleSubscriptionEarningsAdapterItem);

    public /* synthetic */ GuildRoleSubscriptionEarningsViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }
}
