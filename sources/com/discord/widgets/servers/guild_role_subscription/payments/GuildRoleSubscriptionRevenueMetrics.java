package com.discord.widgets.servers.guild_role_subscription.payments;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import b.a.k.b;
import com.discord.databinding.ViewGuildRoleSubscriptionRevenueMetricBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.locale.LocaleManager;
import com.discord.utilities.time.TimeUtils;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.badge.BadgeDrawable;
import d0.g0.t;
import d0.z.d.m;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: GuildRoleSubscriptionRevenueMetrics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u0000 02\u00020\u0001:\u00010B\u0011\b\u0016\u0012\u0006\u0010)\u001a\u00020(¢\u0006\u0004\b*\u0010+B\u001d\b\u0016\u0012\u0006\u0010)\u001a\u00020(\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b*\u0010,B'\b\u0016\u0012\u0006\u0010)\u001a\u00020(\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010.\u001a\u00020-¢\u0006\u0004\b*\u0010/J\u001b\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ!\u0010\u000e\u001a\u00020\u00042\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ!\u0010\u0011\u001a\u00020\u00102\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J9\u0010\u0017\u001a\u00020\u00042\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\b\b\u0002\u0010\u0015\u001a\u00020\u00072\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0013¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u0017\u001a\u00020\u00042\b\u0010\u0019\u001a\u0004\u0018\u00010\u00132\b\u0010\u001a\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\u0017\u0010\u001bR\u0016\u0010\u001c\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0016\u0010%\u001a\u00020$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0016\u0010'\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010\u001d¨\u00061"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionRevenueMetrics;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "Landroid/util/AttributeSet;", "attrs", "", "initialize", "(Landroid/util/AttributeSet;)V", "", "isPercent", "Ljava/text/NumberFormat;", "createNumberFormatter", "(Z)Ljava/text/NumberFormat;", "", "monthOverMonthChange", "configureMonthOverMonthChange", "(Ljava/lang/Long;Z)V", "", "formatMonthOverMonthChangeValue", "(Ljava/lang/Long;Z)Ljava/lang/CharSequence;", "", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "isMonthOverMonthChangePercent", "dateInfo", "configureUI", "(Ljava/lang/String;ZLjava/lang/Long;Ljava/lang/String;)V", "nextPaymentDate", "teamId", "(Ljava/lang/String;Ljava/lang/Long;)V", "percentFormatter", "Ljava/text/NumberFormat;", "Lcom/discord/utilities/locale/LocaleManager;", "localeManager", "Lcom/discord/utilities/locale/LocaleManager;", "Lcom/discord/databinding/ViewGuildRoleSubscriptionRevenueMetricBinding;", "binding", "Lcom/discord/databinding/ViewGuildRoleSubscriptionRevenueMetricBinding;", "Ljava/text/SimpleDateFormat;", "dateFormat", "Ljava/text/SimpleDateFormat;", "numberFormatter", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildRoleSubscriptionRevenueMetrics extends ConstraintLayout {
    public static final Companion Companion = new Companion(null);
    private static final String DATE_FORMAT = "MMM d";
    private static final int MONTHLY_REVENUE_TYPE = 0;
    private static final int NEXT_PAYMENT_TYPE = 2;
    private static final int TOTAL_MEMBERS_TYPE = 1;
    private final ViewGuildRoleSubscriptionRevenueMetricBinding binding;
    private final SimpleDateFormat dateFormat;
    private final LocaleManager localeManager;
    private final NumberFormat numberFormatter;
    private final NumberFormat percentFormatter;

    /* compiled from: GuildRoleSubscriptionRevenueMetrics.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0016\u0010\b\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\u0007R\u0016\u0010\t\u001a\u00020\u00058\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\u0007¨\u0006\f"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionRevenueMetrics$Companion;", "", "", "DATE_FORMAT", "Ljava/lang/String;", "", "MONTHLY_REVENUE_TYPE", "I", "NEXT_PAYMENT_TYPE", "TOTAL_MEMBERS_TYPE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionRevenueMetrics(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionRevenueMetricBinding a = ViewGuildRoleSubscriptionRevenueMetricBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        LocaleManager localeManager = new LocaleManager();
        this.localeManager = localeManager;
        this.numberFormatter = createNumberFormatter(false);
        this.percentFormatter = createNumberFormatter(true);
        this.dateFormat = new SimpleDateFormat(DATE_FORMAT, localeManager.getPrimaryLocale(getContext()));
        initialize$default(this, null, 1, null);
    }

    private final void configureMonthOverMonthChange(Long l, boolean z2) {
        CharSequence formatMonthOverMonthChangeValue = formatMonthOverMonthChangeValue(l, z2);
        LinkifiedTextView linkifiedTextView = this.binding.d;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.metricsFooter");
        linkifiedTextView.setText(formatMonthOverMonthChangeValue);
        ImageView imageView = this.binding.e;
        m.checkNotNullExpressionValue(imageView, "binding.metricsFooterLeadingIcon");
        int i = 0;
        if (!(l != null)) {
            i = 8;
        }
        imageView.setVisibility(i);
        if (l != null) {
            l.longValue();
            if (l.longValue() > 0) {
                ImageView imageView2 = this.binding.e;
                m.checkNotNullExpressionValue(imageView2, "binding.metricsFooterLeadingIcon");
                imageView2.setRotation(0.0f);
                ImageView imageView3 = this.binding.e;
                m.checkNotNullExpressionValue(imageView3, "binding.metricsFooterLeadingIcon");
                ColorCompatKt.tintWithColorResource(imageView3, R.color.status_green_600);
                return;
            }
            ImageView imageView4 = this.binding.e;
            m.checkNotNullExpressionValue(imageView4, "binding.metricsFooterLeadingIcon");
            imageView4.setRotation(180.0f);
            ImageView imageView5 = this.binding.e;
            m.checkNotNullExpressionValue(imageView5, "binding.metricsFooterLeadingIcon");
            ColorCompatKt.tintWithColorResource(imageView5, R.color.status_red);
        }
    }

    public static /* synthetic */ void configureUI$default(GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics, String str, boolean z2, Long l, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        if ((i & 4) != 0) {
            l = null;
        }
        if ((i & 8) != 0) {
            str2 = null;
        }
        guildRoleSubscriptionRevenueMetrics.configureUI(str, z2, l, str2);
    }

    private final NumberFormat createNumberFormatter(boolean z2) {
        NumberFormat numberFormat;
        if (z2) {
            numberFormat = NumberFormat.getNumberInstance(this.localeManager.getPrimaryLocale(getContext()));
        } else {
            numberFormat = NumberFormat.getPercentInstance(this.localeManager.getPrimaryLocale(getContext()));
        }
        DecimalFormat decimalFormat = (DecimalFormat) (!(numberFormat instanceof DecimalFormat) ? null : numberFormat);
        if (decimalFormat != null) {
            decimalFormat.setPositivePrefix(BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX);
            decimalFormat.setNegativePrefix("-");
        }
        m.checkNotNullExpressionValue(numberFormat, "formatter");
        return numberFormat;
    }

    private final CharSequence formatMonthOverMonthChangeValue(Long l, boolean z2) {
        String str;
        CharSequence d;
        if (l == null) {
            String string = getContext().getString(R.string.guild_role_subscription_earnings_metric_trend_empty_text);
            m.checkNotNullExpressionValue(string, "context.getString(R.stri…_metric_trend_empty_text)");
            return string;
        }
        if (z2) {
            str = this.percentFormatter.format(l.longValue());
        } else {
            str = this.numberFormatter.format(l.longValue());
        }
        d = b.d(this, R.string.guild_role_subscription_earnings_metric_trend_description, new Object[]{str}, (r4 & 4) != 0 ? b.c.j : null);
        return d;
    }

    private final void initialize(AttributeSet attributeSet) {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.guild_role_subscription_setup_default_padding);
        setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.drawable_rect_rounded_bg_secondary));
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        int[] iArr = com.discord.R.a.GuildRoleSubscriptionPayoutInfoView;
        m.checkNotNullExpressionValue(iArr, "R.styleable.GuildRoleSubscriptionPayoutInfoView");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "obtainStyledAttributes(attrs, styleable)");
        int i = obtainStyledAttributes.getInt(0, 0);
        if (i == 0) {
            this.binding.f.setText(R.string.guild_role_subscription_earnings_metric_revenue);
        } else if (i == 1) {
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.metricsDescription");
            DrawableCompat.setCompoundDrawablesCompat$default(textView, 0, 0, (int) R.drawable.ic_person_white_a60_24dp, 0, 11, (Object) null);
            this.binding.f.setText(R.string.guild_role_subscription_earnings_metric_subscribers);
            TextView textView2 = this.binding.f2181b;
            m.checkNotNullExpressionValue(textView2, "binding.metricsDate");
            textView2.setVisibility(8);
        } else if (i == 2) {
            this.binding.f.setText(R.string.guild_role_subscription_earnings_metric_next_payment);
            LinkifiedTextView linkifiedTextView = this.binding.d;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.metricsFooter");
            linkifiedTextView.setTextColor(ColorCompat.getThemedColor(linkifiedTextView, (int) R.attr.colorTextLink));
            TextView textView3 = this.binding.f2181b;
            m.checkNotNullExpressionValue(textView3, "binding.metricsDate");
            textView3.setVisibility(8);
            ImageView imageView = this.binding.e;
            m.checkNotNullExpressionValue(imageView, "binding.metricsFooterLeadingIcon");
            imageView.setVisibility(8);
        }
        obtainStyledAttributes.recycle();
    }

    public static /* synthetic */ void initialize$default(GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics, AttributeSet attributeSet, int i, Object obj) {
        if ((i & 1) != 0) {
            attributeSet = null;
        }
        guildRoleSubscriptionRevenueMetrics.initialize(attributeSet);
    }

    public final void configureUI(String str, boolean z2, Long l, String str2) {
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.metricsDescription");
        textView.setText(str);
        CharSequence charSequence = null;
        if (!(str2 == null || t.isBlank(str2))) {
            TimeUtils timeUtils = TimeUtils.INSTANCE;
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            charSequence = b.d(this, R.string.guild_role_subscription_earnings_metric_revenue_since, new Object[]{TimeUtils.renderUtcDate$default(timeUtils, str2, context, (String) null, this.dateFormat, 0, 20, (Object) null)}, (r4 & 4) != 0 ? b.c.j : null);
        }
        TextView textView2 = this.binding.f2181b;
        m.checkNotNullExpressionValue(textView2, "binding.metricsDate");
        ViewExtensions.setTextAndVisibilityBy(textView2, charSequence);
        configureMonthOverMonthChange(l, z2);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionRevenueMetrics(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionRevenueMetricBinding a = ViewGuildRoleSubscriptionRevenueMetricBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        LocaleManager localeManager = new LocaleManager();
        this.localeManager = localeManager;
        this.numberFormatter = createNumberFormatter(false);
        this.percentFormatter = createNumberFormatter(true);
        this.dateFormat = new SimpleDateFormat(DATE_FORMAT, localeManager.getPrimaryLocale(getContext()));
        initialize(attributeSet);
    }

    public final void configureUI(String str, Long l) {
        String str2;
        if (str == null || t.isBlank(str)) {
            str2 = "-";
        } else {
            TimeUtils timeUtils = TimeUtils.INSTANCE;
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            str2 = TimeUtils.renderUtcDate$default(timeUtils, str, context, (String) null, this.dateFormat, 0, 20, (Object) null);
        }
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.metricsDescription");
        ViewExtensions.setTextAndVisibilityBy(textView, str2);
        if (l == null) {
            LinkifiedTextView linkifiedTextView = this.binding.d;
            m.checkNotNullExpressionValue(linkifiedTextView, "binding.metricsFooter");
            linkifiedTextView.setText((CharSequence) null);
            return;
        }
        LinkifiedTextView linkifiedTextView2 = this.binding.d;
        m.checkNotNullExpressionValue(linkifiedTextView2, "binding.metricsFooter");
        b.m(linkifiedTextView2, R.string.guild_role_subscription_earnings_edit_payment_method_link, new Object[]{GuildRoleSubscriptionPayoutUtils.INSTANCE.getTeamPayoutSetupUrl(l.longValue())}, (r4 & 4) != 0 ? b.g.j : null);
    }

    public /* synthetic */ GuildRoleSubscriptionRevenueMetrics(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildRoleSubscriptionRevenueMetrics(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        ViewGuildRoleSubscriptionRevenueMetricBinding a = ViewGuildRoleSubscriptionRevenueMetricBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…ater.from(context), this)");
        this.binding = a;
        LocaleManager localeManager = new LocaleManager();
        this.localeManager = localeManager;
        this.numberFormatter = createNumberFormatter(false);
        this.percentFormatter = createNumberFormatter(true);
        this.dateFormat = new SimpleDateFormat(DATE_FORMAT, localeManager.getPrimaryLocale(getContext()));
        initialize(attributeSet);
    }

    public /* synthetic */ GuildRoleSubscriptionRevenueMetrics(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
