package com.discord.widgets.servers.guild_role_subscription.payments;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetServerSettingsGuildRoleSubscriptionEarningsBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsGuildRoleSubscriptionEarnings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetServerSettingsGuildRoleSubscriptionEarningsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsGuildRoleSubscriptionEarningsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettingsGuildRoleSubscriptionEarnings$binding$2 extends k implements Function1<View, WidgetServerSettingsGuildRoleSubscriptionEarningsBinding> {
    public static final WidgetServerSettingsGuildRoleSubscriptionEarnings$binding$2 INSTANCE = new WidgetServerSettingsGuildRoleSubscriptionEarnings$binding$2();

    public WidgetServerSettingsGuildRoleSubscriptionEarnings$binding$2() {
        super(1, WidgetServerSettingsGuildRoleSubscriptionEarningsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsGuildRoleSubscriptionEarningsBinding;", 0);
    }

    public final WidgetServerSettingsGuildRoleSubscriptionEarningsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.guild_role_subscription_earnings_list;
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.guild_role_subscription_earnings_list);
        if (recyclerView != null) {
            i = R.id.guild_role_subscription_revenue_view_flipper;
            AppViewFlipper appViewFlipper = (AppViewFlipper) view.findViewById(R.id.guild_role_subscription_revenue_view_flipper);
            if (appViewFlipper != null) {
                return new WidgetServerSettingsGuildRoleSubscriptionEarningsBinding((CoordinatorLayout) view, recyclerView, appViewFlipper);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
