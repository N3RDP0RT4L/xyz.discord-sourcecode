package com.discord.widgets.servers.guild_role_subscription.payments;

import com.discord.app.AppViewModel;
import com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetServerSettingsGuildRoleSubscriptionEarnings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsGuildRoleSubscriptionEarnings$viewModel$2 extends o implements Function0<AppViewModel<ServerSettingsGuildRoleSubscriptionEarningsViewModel.ViewState>> {
    public final /* synthetic */ WidgetServerSettingsGuildRoleSubscriptionEarnings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsGuildRoleSubscriptionEarnings$viewModel$2(WidgetServerSettingsGuildRoleSubscriptionEarnings widgetServerSettingsGuildRoleSubscriptionEarnings) {
        super(0);
        this.this$0 = widgetServerSettingsGuildRoleSubscriptionEarnings;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<ServerSettingsGuildRoleSubscriptionEarningsViewModel.ViewState> invoke() {
        long guildId;
        guildId = this.this$0.getGuildId();
        return new ServerSettingsGuildRoleSubscriptionEarningsViewModel(guildId, null, null, null, null, null, 62, null);
    }
}
