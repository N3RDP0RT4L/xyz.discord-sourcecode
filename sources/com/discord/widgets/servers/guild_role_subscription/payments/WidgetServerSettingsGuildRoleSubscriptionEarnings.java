package com.discord.widgets.servers.guild_role_subscription.payments;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetServerSettingsGuildRoleSubscriptionEarningsBinding;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.guild_role_subscription.payments.ServerSettingsGuildRoleSubscriptionEarningsViewModel;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsGuildRoleSubscriptionEarnings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0007¢\u0006\u0004\b!\u0010\fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0017\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u000f\u001a\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u001d\u0010 \u001a\u00020\u001b8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f¨\u0006#"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/WidgetServerSettingsGuildRoleSubscriptionEarnings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel$ViewState;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/servers/guild_role_subscription/payments/ServerSettingsGuildRoleSubscriptionEarningsViewModel;", "viewModel", "", "guildId$delegate", "getGuildId", "()J", "guildId", "Lcom/discord/widgets/servers/guild_role_subscription/payments/WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter;", "adapter", "Lcom/discord/widgets/servers/guild_role_subscription/payments/WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter;", "Lcom/discord/databinding/WidgetServerSettingsGuildRoleSubscriptionEarningsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsGuildRoleSubscriptionEarningsBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsGuildRoleSubscriptionEarnings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsGuildRoleSubscriptionEarnings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsGuildRoleSubscriptionEarningsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int SERVER_SETTINGS_GUILD_ROLE_SUBSCRIPTION_EARNINGS_VIEW_FLIPPER_LOADED_STATE = 1;
    private static final int SERVER_SETTINGS_GUILD_ROLE_SUBSCRIPTION_EARNINGS_VIEW_FLIPPER_LOADING_STATE = 0;
    private WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsGuildRoleSubscriptionEarnings$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetServerSettingsGuildRoleSubscriptionEarnings$guildId$2(this));
    private final Lazy viewModel$delegate;

    /* compiled from: WidgetServerSettingsGuildRoleSubscriptionEarnings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ#\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/WidgetServerSettingsGuildRoleSubscriptionEarnings$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "launch", "(Landroid/content/Context;J)V", "", "SERVER_SETTINGS_GUILD_ROLE_SUBSCRIPTION_EARNINGS_VIEW_FLIPPER_LOADED_STATE", "I", "SERVER_SETTINGS_GUILD_ROLE_SUBSCRIPTION_EARNINGS_VIEW_FLIPPER_LOADING_STATE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra("com.discord.intent.extra.EXTRA_GUILD_ID", j);
            j.d(context, WidgetServerSettingsGuildRoleSubscriptionEarnings.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetServerSettingsGuildRoleSubscriptionEarnings() {
        super(R.layout.widget_server_settings_guild_role_subscription_earnings);
        WidgetServerSettingsGuildRoleSubscriptionEarnings$viewModel$2 widgetServerSettingsGuildRoleSubscriptionEarnings$viewModel$2 = new WidgetServerSettingsGuildRoleSubscriptionEarnings$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(ServerSettingsGuildRoleSubscriptionEarningsViewModel.class), new WidgetServerSettingsGuildRoleSubscriptionEarnings$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetServerSettingsGuildRoleSubscriptionEarnings$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(ServerSettingsGuildRoleSubscriptionEarningsViewModel.ViewState viewState) {
        if (viewState instanceof ServerSettingsGuildRoleSubscriptionEarningsViewModel.ViewState.Loading) {
            AppViewFlipper appViewFlipper = getBinding().c;
            m.checkNotNullExpressionValue(appViewFlipper, "binding.guildRoleSubscriptionRevenueViewFlipper");
            appViewFlipper.setDisplayedChild(0);
        } else if (viewState instanceof ServerSettingsGuildRoleSubscriptionEarningsViewModel.ViewState.Loaded) {
            AppViewFlipper appViewFlipper2 = getBinding().c;
            m.checkNotNullExpressionValue(appViewFlipper2, "binding.guildRoleSubscriptionRevenueViewFlipper");
            appViewFlipper2.setDisplayedChild(1);
            WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter widgetServerSettingsGuildRoleSubscriptionEarningsAdapter = this.adapter;
            if (widgetServerSettingsGuildRoleSubscriptionEarningsAdapter == null) {
                m.throwUninitializedPropertyAccessException("adapter");
            }
            widgetServerSettingsGuildRoleSubscriptionEarningsAdapter.setItems(((ServerSettingsGuildRoleSubscriptionEarningsViewModel.ViewState.Loaded) viewState).getItems());
        } else if (viewState instanceof ServerSettingsGuildRoleSubscriptionEarningsViewModel.ViewState.Failed) {
            b.a.d.m.i(this, R.string.default_failure_to_perform_action_message, 0, 4);
            requireActivity().finish();
        }
    }

    private final WidgetServerSettingsGuildRoleSubscriptionEarningsBinding getBinding() {
        return (WidgetServerSettingsGuildRoleSubscriptionEarningsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    private final ServerSettingsGuildRoleSubscriptionEarningsViewModel getViewModel() {
        return (ServerSettingsGuildRoleSubscriptionEarningsViewModel) this.viewModel$delegate.getValue();
    }

    public static final void launch(Context context, long j) {
        Companion.launch(context, j);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarDisplayHomeAsUpEnabled(true);
        setActionBarTitle(R.string.guild_role_subscription_settings_section_payment);
        this.adapter = new WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter(this, null, 2, null);
        RecyclerView recyclerView = getBinding().f2542b;
        m.checkNotNullExpressionValue(recyclerView, "binding.guildRoleSubscriptionEarningsList");
        WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter widgetServerSettingsGuildRoleSubscriptionEarningsAdapter = this.adapter;
        if (widgetServerSettingsGuildRoleSubscriptionEarningsAdapter == null) {
            m.throwUninitializedPropertyAccessException("adapter");
        }
        recyclerView.setAdapter(widgetServerSettingsGuildRoleSubscriptionEarningsAdapter);
        getBinding().f2542b.addItemDecoration(new GuildRoleSubscriptionEarningItemDecoration(getResources().getDimensionPixelSize(R.dimen.guild_role_subscription_setup_default_padding)));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable q = ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "viewModel.observeViewSta…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, WidgetServerSettingsGuildRoleSubscriptionEarnings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsGuildRoleSubscriptionEarnings$onViewBoundOrOnResume$1(this));
    }
}
