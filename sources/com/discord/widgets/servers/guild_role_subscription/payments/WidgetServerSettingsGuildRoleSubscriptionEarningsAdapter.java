package com.discord.widgets.servers.guild_role_subscription.payments;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import com.discord.app.AppComponent;
import com.discord.databinding.ViewGuildRoleSubscriptionEarningTableHeaderItemBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionRevenueMetricsItemBinding;
import com.discord.databinding.ViewGuildRoleSubscriptionSectionHeaderItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.recycler.DiffCreator;
import com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsAdapterItem;
import com.discord.widgets.servers.guild_role_subscription.payments.GuildRoleSubscriptionEarningsViewHolder;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000  2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001 B+\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u001a\b\u0002\u0010\u0018\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0004\u0012\u00020\u00020\u0017¢\u0006\u0004\b\u001e\u0010\u001fJ\u001f\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0010\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u001b\u0010\u0015\u001a\u00020\u000b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012¢\u0006\u0004\b\u0015\u0010\u0016R(\u0010\u0018\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0004\u0012\u00020\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006!"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;", "holder", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "onBindViewHolder", "(Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsViewHolder;I)V", "getItemCount", "()I", "getItemViewType", "(I)I", "", "Lcom/discord/widgets/servers/guild_role_subscription/payments/GuildRoleSubscriptionEarningsAdapterItem;", "newItems", "setItems", "(Ljava/util/List;)V", "Lcom/discord/utilities/recycler/DiffCreator;", "diffCreator", "Lcom/discord/utilities/recycler/DiffCreator;", "items", "Ljava/util/List;", "Lcom/discord/app/AppComponent;", "appComponent", HookHelper.constructorName, "(Lcom/discord/app/AppComponent;Lcom/discord/utilities/recycler/DiffCreator;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter extends RecyclerView.Adapter<GuildRoleSubscriptionEarningsViewHolder> {
    public static final Companion Companion = new Companion(null);
    private static final int VIEW_TYPE_CURRENT_REVENUE_BY_TIERS = 1;
    private static final int VIEW_TYPE_REVENUE_HISTORY_ITEM = 4;
    private static final int VIEW_TYPE_REVENUE_METRICS = 0;
    public static final int VIEW_TYPE_SECTION_HEADER = 2;
    private static final int VIEW_TYPE_TABLE_HEADER = 3;
    private final DiffCreator<List<GuildRoleSubscriptionEarningsAdapterItem>, GuildRoleSubscriptionEarningsViewHolder> diffCreator;
    private List<? extends GuildRoleSubscriptionEarningsAdapterItem> items;

    /* compiled from: WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\u0004¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/payments/WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter$Companion;", "", "", "VIEW_TYPE_CURRENT_REVENUE_BY_TIERS", "I", "VIEW_TYPE_REVENUE_HISTORY_ITEM", "VIEW_TYPE_REVENUE_METRICS", "VIEW_TYPE_SECTION_HEADER", "VIEW_TYPE_TABLE_HEADER", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter(AppComponent appComponent, DiffCreator diffCreator, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(appComponent, (i & 2) != 0 ? new DiffCreator(appComponent) : diffCreator);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        GuildRoleSubscriptionEarningsAdapterItem guildRoleSubscriptionEarningsAdapterItem = this.items.get(i);
        if (guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsItem) {
            return 0;
        }
        if (guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.CurrentMonthEarningMetricsByTiers) {
            return 1;
        }
        if (guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.SectionHeader) {
            return 2;
        }
        if ((guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.CurrentMonthEarningMetricsByTiersHeader) || (guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsHistoryHeader)) {
            return 3;
        }
        if (guildRoleSubscriptionEarningsAdapterItem instanceof GuildRoleSubscriptionEarningsAdapterItem.EarningMetricsHistory) {
            return 4;
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void setItems(List<? extends GuildRoleSubscriptionEarningsAdapterItem> list) {
        m.checkNotNullParameter(list, "newItems");
        this.diffCreator.dispatchDiffUpdatesAsync(this, new WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter$setItems$1(this), this.items, list);
    }

    public WidgetServerSettingsGuildRoleSubscriptionEarningsAdapter(AppComponent appComponent, DiffCreator<List<GuildRoleSubscriptionEarningsAdapterItem>, GuildRoleSubscriptionEarningsViewHolder> diffCreator) {
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(diffCreator, "diffCreator");
        this.diffCreator = diffCreator;
        this.items = n.emptyList();
    }

    public void onBindViewHolder(GuildRoleSubscriptionEarningsViewHolder guildRoleSubscriptionEarningsViewHolder, int i) {
        m.checkNotNullParameter(guildRoleSubscriptionEarningsViewHolder, "holder");
        guildRoleSubscriptionEarningsViewHolder.configure(this.items.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public GuildRoleSubscriptionEarningsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        CharSequence b2;
        m.checkNotNullParameter(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            View inflate = from.inflate(R.layout.view_guild_role_subscription_revenue_metrics_item, viewGroup, false);
            int i2 = R.id.guild_role_subscription_monthly_revenue;
            GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics = (GuildRoleSubscriptionRevenueMetrics) inflate.findViewById(R.id.guild_role_subscription_monthly_revenue);
            if (guildRoleSubscriptionRevenueMetrics != null) {
                i2 = R.id.guild_role_subscription_next_payment;
                GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics2 = (GuildRoleSubscriptionRevenueMetrics) inflate.findViewById(R.id.guild_role_subscription_next_payment);
                if (guildRoleSubscriptionRevenueMetrics2 != null) {
                    i2 = R.id.guild_role_subscription_total_members;
                    GuildRoleSubscriptionRevenueMetrics guildRoleSubscriptionRevenueMetrics3 = (GuildRoleSubscriptionRevenueMetrics) inflate.findViewById(R.id.guild_role_subscription_total_members);
                    if (guildRoleSubscriptionRevenueMetrics3 != null) {
                        ViewGuildRoleSubscriptionRevenueMetricsItemBinding viewGuildRoleSubscriptionRevenueMetricsItemBinding = new ViewGuildRoleSubscriptionRevenueMetricsItemBinding((ConstraintLayout) inflate, guildRoleSubscriptionRevenueMetrics, guildRoleSubscriptionRevenueMetrics2, guildRoleSubscriptionRevenueMetrics3);
                        m.checkNotNullExpressionValue(viewGuildRoleSubscriptionRevenueMetricsItemBinding, "ViewGuildRoleSubscriptio…(inflater, parent, false)");
                        return new GuildRoleSubscriptionEarningsViewHolder.RevenueMetricsViewHolder(viewGuildRoleSubscriptionRevenueMetricsItemBinding);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
        } else if (i == 1) {
            Context context = viewGroup.getContext();
            m.checkNotNullExpressionValue(context, "parent.context");
            return new GuildRoleSubscriptionEarningsViewHolder.CurrentRevenueByTiersViewHolder(new GuildRoleSubscriptionTierRevenueListView(context));
        } else if (i == 2) {
            ViewGuildRoleSubscriptionSectionHeaderItemBinding a = ViewGuildRoleSubscriptionSectionHeaderItemBinding.a(from, viewGroup, false);
            m.checkNotNullExpressionValue(a, "ViewGuildRoleSubscriptio…(inflater, parent, false)");
            return new GuildRoleSubscriptionEarningsViewHolder.EarningsSectionHeader(a);
        } else if (i == 3) {
            View inflate2 = from.inflate(R.layout.view_guild_role_subscription_earning_table_header_item, viewGroup, false);
            int i3 = R.id.center_header_item;
            TextView textView = (TextView) inflate2.findViewById(R.id.center_header_item);
            if (textView != null) {
                i3 = R.id.end_header_item;
                TextView textView2 = (TextView) inflate2.findViewById(R.id.end_header_item);
                if (textView2 != null) {
                    i3 = R.id.start_header_item;
                    TextView textView3 = (TextView) inflate2.findViewById(R.id.start_header_item);
                    if (textView3 != null) {
                        ViewGuildRoleSubscriptionEarningTableHeaderItemBinding viewGuildRoleSubscriptionEarningTableHeaderItemBinding = new ViewGuildRoleSubscriptionEarningTableHeaderItemBinding((ConstraintLayout) inflate2, textView, textView2, textView3);
                        m.checkNotNullExpressionValue(viewGuildRoleSubscriptionEarningTableHeaderItemBinding, "ViewGuildRoleSubscriptio…(inflater, parent, false)");
                        return new GuildRoleSubscriptionEarningsViewHolder.EarningMetricsTableHeader(viewGuildRoleSubscriptionEarningTableHeaderItemBinding);
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(i3)));
        } else if (i == 4) {
            View inflate3 = from.inflate(R.layout.view_guild_role_subscription_earnings_tier_revenue_history_item, viewGroup, false);
            int i4 = R.id.revenue_history_item_amount;
            TextView textView4 = (TextView) inflate3.findViewById(R.id.revenue_history_item_amount);
            if (textView4 != null) {
                i4 = R.id.revenue_history_item_description;
                TextView textView5 = (TextView) inflate3.findViewById(R.id.revenue_history_item_description);
                if (textView5 != null) {
                    i4 = R.id.revenue_history_item_status;
                    TextView textView6 = (TextView) inflate3.findViewById(R.id.revenue_history_item_status);
                    if (textView6 != null) {
                        i4 = R.id.revenue_history_item_subscriber_count;
                        TextView textView7 = (TextView) inflate3.findViewById(R.id.revenue_history_item_subscriber_count);
                        if (textView7 != null) {
                            i4 = R.id.tier_revenue_list_view;
                            GuildRoleSubscriptionTierRevenueListView guildRoleSubscriptionTierRevenueListView = (GuildRoleSubscriptionTierRevenueListView) inflate3.findViewById(R.id.tier_revenue_list_view);
                            if (guildRoleSubscriptionTierRevenueListView != null) {
                                ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding = new ViewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding((ConstraintLayout) inflate3, textView4, textView5, textView6, textView7, guildRoleSubscriptionTierRevenueListView);
                                m.checkNotNullExpressionValue(viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding, "ViewGuildRoleSubscriptio…(inflater, parent, false)");
                                return new GuildRoleSubscriptionEarningsViewHolder.EarningMetricsHistoryListItemViewHolder(viewGuildRoleSubscriptionEarningsTierRevenueHistoryItemBinding);
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate3.getResources().getResourceName(i4)));
        } else {
            Context context2 = viewGroup.getContext();
            m.checkNotNullExpressionValue(context2, "parent.context");
            b2 = b.b(context2, R.string.android_unknown_view_holder, new Object[]{Integer.valueOf(i)}, (r4 & 4) != 0 ? b.C0034b.j : null);
            throw new IllegalArgumentException(b2.toString());
        }
    }
}
