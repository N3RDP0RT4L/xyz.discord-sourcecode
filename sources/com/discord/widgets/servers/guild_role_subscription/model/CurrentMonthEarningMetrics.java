package com.discord.widgets.servers.guild_role_subscription.model;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
/* compiled from: CurrentMonthEarningMetrics.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001BK\u0012\b\b\u0002\u0010\r\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0006J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\f\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\f\u0010\u000bJT\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0015\u0010\u000bJ\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b \u0010\u0006R\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001f\u001a\u0004\b!\u0010\u0006R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001d\u001a\u0004\b\"\u0010\u0004R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\u000bR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b%\u0010\u000b¨\u0006("}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;", "", "", "component1", "()J", "component2", "()Ljava/lang/Long;", "component3", "component4", "", "component5", "()Ljava/lang/String;", "component6", "revenue", "monthOverMonthRevenueChangePercent", "subscriberCount", "monthOverMonthSubscriberCountChange", "nextPaymentDate", "revenueSinceDate", "copy", "(JLjava/lang/Long;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/servers/guild_role_subscription/model/CurrentMonthEarningMetrics;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getRevenue", "Ljava/lang/Long;", "getMonthOverMonthRevenueChangePercent", "getMonthOverMonthSubscriberCountChange", "getSubscriberCount", "Ljava/lang/String;", "getRevenueSinceDate", "getNextPaymentDate", HookHelper.constructorName, "(JLjava/lang/Long;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CurrentMonthEarningMetrics {
    private final Long monthOverMonthRevenueChangePercent;
    private final Long monthOverMonthSubscriberCountChange;
    private final String nextPaymentDate;
    private final long revenue;
    private final String revenueSinceDate;
    private final long subscriberCount;

    public CurrentMonthEarningMetrics() {
        this(0L, null, 0L, null, null, null, 63, null);
    }

    public CurrentMonthEarningMetrics(long j, Long l, long j2, Long l2, String str, String str2) {
        this.revenue = j;
        this.monthOverMonthRevenueChangePercent = l;
        this.subscriberCount = j2;
        this.monthOverMonthSubscriberCountChange = l2;
        this.nextPaymentDate = str;
        this.revenueSinceDate = str2;
    }

    public final long component1() {
        return this.revenue;
    }

    public final Long component2() {
        return this.monthOverMonthRevenueChangePercent;
    }

    public final long component3() {
        return this.subscriberCount;
    }

    public final Long component4() {
        return this.monthOverMonthSubscriberCountChange;
    }

    public final String component5() {
        return this.nextPaymentDate;
    }

    public final String component6() {
        return this.revenueSinceDate;
    }

    public final CurrentMonthEarningMetrics copy(long j, Long l, long j2, Long l2, String str, String str2) {
        return new CurrentMonthEarningMetrics(j, l, j2, l2, str, str2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CurrentMonthEarningMetrics)) {
            return false;
        }
        CurrentMonthEarningMetrics currentMonthEarningMetrics = (CurrentMonthEarningMetrics) obj;
        return this.revenue == currentMonthEarningMetrics.revenue && m.areEqual(this.monthOverMonthRevenueChangePercent, currentMonthEarningMetrics.monthOverMonthRevenueChangePercent) && this.subscriberCount == currentMonthEarningMetrics.subscriberCount && m.areEqual(this.monthOverMonthSubscriberCountChange, currentMonthEarningMetrics.monthOverMonthSubscriberCountChange) && m.areEqual(this.nextPaymentDate, currentMonthEarningMetrics.nextPaymentDate) && m.areEqual(this.revenueSinceDate, currentMonthEarningMetrics.revenueSinceDate);
    }

    public final Long getMonthOverMonthRevenueChangePercent() {
        return this.monthOverMonthRevenueChangePercent;
    }

    public final Long getMonthOverMonthSubscriberCountChange() {
        return this.monthOverMonthSubscriberCountChange;
    }

    public final String getNextPaymentDate() {
        return this.nextPaymentDate;
    }

    public final long getRevenue() {
        return this.revenue;
    }

    public final String getRevenueSinceDate() {
        return this.revenueSinceDate;
    }

    public final long getSubscriberCount() {
        return this.subscriberCount;
    }

    public int hashCode() {
        int a = b.a(this.revenue) * 31;
        Long l = this.monthOverMonthRevenueChangePercent;
        int i = 0;
        int a2 = (b.a(this.subscriberCount) + ((a + (l != null ? l.hashCode() : 0)) * 31)) * 31;
        Long l2 = this.monthOverMonthSubscriberCountChange;
        int hashCode = (a2 + (l2 != null ? l2.hashCode() : 0)) * 31;
        String str = this.nextPaymentDate;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.revenueSinceDate;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("CurrentMonthEarningMetrics(revenue=");
        R.append(this.revenue);
        R.append(", monthOverMonthRevenueChangePercent=");
        R.append(this.monthOverMonthRevenueChangePercent);
        R.append(", subscriberCount=");
        R.append(this.subscriberCount);
        R.append(", monthOverMonthSubscriberCountChange=");
        R.append(this.monthOverMonthSubscriberCountChange);
        R.append(", nextPaymentDate=");
        R.append(this.nextPaymentDate);
        R.append(", revenueSinceDate=");
        return a.H(R, this.revenueSinceDate, ")");
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ CurrentMonthEarningMetrics(long r10, java.lang.Long r12, long r13, java.lang.Long r15, java.lang.String r16, java.lang.String r17, int r18, kotlin.jvm.internal.DefaultConstructorMarker r19) {
        /*
            r9 = this;
            r0 = r18 & 1
            r1 = 0
            if (r0 == 0) goto L8
            r3 = r1
            goto L9
        L8:
            r3 = r10
        L9:
            r0 = r18 & 2
            r5 = 0
            if (r0 == 0) goto L10
            r0 = r5
            goto L11
        L10:
            r0 = r12
        L11:
            r6 = r18 & 4
            if (r6 == 0) goto L16
            goto L17
        L16:
            r1 = r13
        L17:
            r6 = r18 & 8
            if (r6 == 0) goto L1c
            goto L1d
        L1c:
            r5 = r15
        L1d:
            r6 = r18 & 16
            java.lang.String r7 = ""
            if (r6 == 0) goto L25
            r6 = r7
            goto L27
        L25:
            r6 = r16
        L27:
            r8 = r18 & 32
            if (r8 == 0) goto L2c
            goto L2e
        L2c:
            r7 = r17
        L2e:
            r10 = r9
            r11 = r3
            r13 = r0
            r14 = r1
            r16 = r5
            r17 = r6
            r18 = r7
            r10.<init>(r11, r13, r14, r16, r17, r18)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.guild_role_subscription.model.CurrentMonthEarningMetrics.<init>(long, java.lang.Long, long, java.lang.Long, java.lang.String, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }
}
