package com.discord.widgets.servers.guild_role_subscription;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.utilities.recycler.DiffKeyProvider;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem;", "Lcom/discord/utilities/recycler/DiffKeyProvider;", HookHelper.constructorName, "()V", "AddTier", "Tier", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem$Tier;", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem$AddTier;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class ServerSettingsGuildRoleSubscriptionTierAdapterItem implements DiffKeyProvider {

    /* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem$AddTier;", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem;", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AddTier extends ServerSettingsGuildRoleSubscriptionTierAdapterItem {
        public static final AddTier INSTANCE = new AddTier();
        private static final String key = "AddTierItem";

        private AddTier() {
            super(null);
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return key;
        }
    }

    /* compiled from: ServerSettingsGuildRoleSubscriptionTierAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\u0006\u0010\u0014\u001a\u00020\b\u0012\n\u0010\u0015\u001a\u00060\u0002j\u0002`\u000b\u0012\u0006\u0010\u0016\u001a\u00020\r\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0014\u0010\f\u001a\u00060\u0002j\u0002`\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011JR\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00052\b\b\u0002\u0010\u0014\u001a\u00020\b2\f\b\u0002\u0010\u0015\u001a\u00060\u0002j\u0002`\u000b2\b\b\u0002\u0010\u0016\u001a\u00020\r2\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u0007J\u0010\u0010\u001b\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\u001b\u0010\nJ\u001a\u0010\u001e\u001a\u00020\r2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u0019\u0010\u0014\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010 \u001a\u0004\b!\u0010\nR\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\"\u001a\u0004\b#\u0010\u0011R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010$\u001a\u0004\b%\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b'\u0010\u0007R\u001d\u0010\u0015\u001a\u00060\u0002j\u0002`\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010)\u001a\u0004\b\u0016\u0010\u000fR\u001c\u0010*\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b*\u0010&\u001a\u0004\b+\u0010\u0007¨\u0006."}, d2 = {"Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem$Tier;", "Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem;", "", "component1", "()J", "", "component2", "()Ljava/lang/String;", "", "component3", "()I", "Lcom/discord/primitives/ApplicationId;", "component4", "", "component5", "()Z", "component6", "()Ljava/lang/Long;", "tierListingId", "tierName", "tierPrice", "applicationId", "isPublished", "tierImageAssetId", "copy", "(JLjava/lang/String;IJZLjava/lang/Long;)Lcom/discord/widgets/servers/guild_role_subscription/ServerSettingsGuildRoleSubscriptionTierAdapterItem$Tier;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "getTierPrice", "Ljava/lang/Long;", "getTierImageAssetId", "J", "getTierListingId", "Ljava/lang/String;", "getTierName", "getApplicationId", "Z", "key", "getKey", HookHelper.constructorName, "(JLjava/lang/String;IJZLjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Tier extends ServerSettingsGuildRoleSubscriptionTierAdapterItem {
        private final long applicationId;
        private final boolean isPublished;
        private final String key;
        private final Long tierImageAssetId;
        private final long tierListingId;
        private final String tierName;
        private final int tierPrice;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Tier(long j, String str, int i, long j2, boolean z2, Long l) {
            super(null);
            m.checkNotNullParameter(str, "tierName");
            this.tierListingId = j;
            this.tierName = str;
            this.tierPrice = i;
            this.applicationId = j2;
            this.isPublished = z2;
            this.tierImageAssetId = l;
            this.key = String.valueOf(j);
        }

        public final long component1() {
            return this.tierListingId;
        }

        public final String component2() {
            return this.tierName;
        }

        public final int component3() {
            return this.tierPrice;
        }

        public final long component4() {
            return this.applicationId;
        }

        public final boolean component5() {
            return this.isPublished;
        }

        public final Long component6() {
            return this.tierImageAssetId;
        }

        public final Tier copy(long j, String str, int i, long j2, boolean z2, Long l) {
            m.checkNotNullParameter(str, "tierName");
            return new Tier(j, str, i, j2, z2, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Tier)) {
                return false;
            }
            Tier tier = (Tier) obj;
            return this.tierListingId == tier.tierListingId && m.areEqual(this.tierName, tier.tierName) && this.tierPrice == tier.tierPrice && this.applicationId == tier.applicationId && this.isPublished == tier.isPublished && m.areEqual(this.tierImageAssetId, tier.tierImageAssetId);
        }

        public final long getApplicationId() {
            return this.applicationId;
        }

        @Override // com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final Long getTierImageAssetId() {
            return this.tierImageAssetId;
        }

        public final long getTierListingId() {
            return this.tierListingId;
        }

        public final String getTierName() {
            return this.tierName;
        }

        public final int getTierPrice() {
            return this.tierPrice;
        }

        public int hashCode() {
            int a = b.a(this.tierListingId) * 31;
            String str = this.tierName;
            int i = 0;
            int a2 = (b.a(this.applicationId) + ((((a + (str != null ? str.hashCode() : 0)) * 31) + this.tierPrice) * 31)) * 31;
            boolean z2 = this.isPublished;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (a2 + i2) * 31;
            Long l = this.tierImageAssetId;
            if (l != null) {
                i = l.hashCode();
            }
            return i4 + i;
        }

        public final boolean isPublished() {
            return this.isPublished;
        }

        public String toString() {
            StringBuilder R = a.R("Tier(tierListingId=");
            R.append(this.tierListingId);
            R.append(", tierName=");
            R.append(this.tierName);
            R.append(", tierPrice=");
            R.append(this.tierPrice);
            R.append(", applicationId=");
            R.append(this.applicationId);
            R.append(", isPublished=");
            R.append(this.isPublished);
            R.append(", tierImageAssetId=");
            return a.F(R, this.tierImageAssetId, ")");
        }
    }

    private ServerSettingsGuildRoleSubscriptionTierAdapterItem() {
    }

    public /* synthetic */ ServerSettingsGuildRoleSubscriptionTierAdapterItem(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }
}
