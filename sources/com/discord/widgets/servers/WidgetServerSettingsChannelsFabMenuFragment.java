package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import com.discord.databinding.WidgetServerSettingsChannelsSortFabMenuBinding;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.FloatingButtonMenuInitializer;
import com.discord.widgets.channels.WidgetCreateChannel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Action0;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsChannelsFabMenuFragment.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0007¢\u0006\u0004\b\u0014\u0010\bJ\u0019\u0010\u0004\u001a\u00020\u00022\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u000f\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\t\u0010\bJ\u000f\u0010\n\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\n\u0010\bR\u0018\u0010\f\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\f\u0010\rR\u001d\u0010\u0013\u001a\u00020\u000e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/view/View$OnClickListener;", "onClickListener", "createListener", "(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;", "", "dismiss", "()V", "onResume", "onPause", "Lrx/functions/Action0;", "dismissHandler", "Lrx/functions/Action0;", "Lcom/discord/databinding/WidgetServerSettingsChannelsSortFabMenuBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsChannelsSortFabMenuBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsChannelsFabMenuFragment extends Fragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsChannelsFabMenuFragment.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsChannelsSortFabMenuBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String TAG = "channels fab menu";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsChannelsFabMenuFragment$binding$2.INSTANCE, null, 2, null);
    private Action0 dismissHandler;

    /* compiled from: WidgetServerSettingsChannelsFabMenuFragment.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0007¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\r¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment$Companion;", "", "", "guildId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lrx/functions/Action0;", "dismissHandler", "", "show", "(JLandroidx/fragment/app/FragmentManager;Lrx/functions/Action0;)V", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", "TAG", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(long j, FragmentManager fragmentManager, Action0 action0) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(action0, "dismissHandler");
            WidgetServerSettingsChannelsFabMenuFragment widgetServerSettingsChannelsFabMenuFragment = new WidgetServerSettingsChannelsFabMenuFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("INTENT_EXTRA_GUILD_ID", j);
            widgetServerSettingsChannelsFabMenuFragment.setArguments(bundle);
            widgetServerSettingsChannelsFabMenuFragment.dismissHandler = action0;
            fragmentManager.beginTransaction().setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out).add(R.id.widget_server_settings_channels_container, widgetServerSettingsChannelsFabMenuFragment, WidgetServerSettingsChannelsFabMenuFragment.TAG).addToBackStack(WidgetServerSettingsChannelsFabMenuFragment.TAG).commit();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetServerSettingsChannelsFabMenuFragment() {
        super(R.layout.widget_server_settings_channels_sort_fab_menu);
    }

    private final View.OnClickListener createListener(final View.OnClickListener onClickListener) {
        return new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannelsFabMenuFragment$createListener$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                View.OnClickListener onClickListener2 = onClickListener;
                if (onClickListener2 != null) {
                    onClickListener2.onClick(view);
                }
                WidgetServerSettingsChannelsFabMenuFragment.this.dismiss();
            }
        };
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void dismiss() {
        getParentFragmentManager().beginTransaction().remove(this).commit();
        Action0 action0 = this.dismissHandler;
        if (action0 != null) {
            action0.call();
        }
    }

    private final WidgetServerSettingsChannelsSortFabMenuBinding getBinding() {
        return (WidgetServerSettingsChannelsSortFabMenuBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public static final void show(long j, FragmentManager fragmentManager, Action0 action0) {
        Companion.show(j, fragmentManager, action0);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dismiss();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        WidgetServerSettingsChannelsSortFabMenuBinding binding = getBinding();
        m.checkNotNullExpressionValue(binding, "binding");
        binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannelsFabMenuFragment$onResume$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsChannelsFabMenuFragment.this.dismiss();
            }
        });
        Bundle arguments = getArguments();
        final long j = 0;
        if (arguments != null) {
            j = arguments.getLong("INTENT_EXTRA_GUILD_ID", 0L);
        }
        Context requireContext = requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        FloatingButtonMenuInitializer floatingButtonMenuInitializer = new FloatingButtonMenuInitializer(requireContext, h0.mapOf(o.to(Integer.valueOf((int) R.id.fab_menu_add_category), createListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannelsFabMenuFragment$onResume$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCreateChannel.Companion companion = WidgetCreateChannel.Companion;
                Context requireContext2 = WidgetServerSettingsChannelsFabMenuFragment.this.requireContext();
                m.checkNotNullExpressionValue(requireContext2, "requireContext()");
                WidgetCreateChannel.Companion.show$default(companion, requireContext2, j, 4, null, 8, null);
            }
        })), o.to(Integer.valueOf((int) R.id.fab_menu_add_voice), createListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannelsFabMenuFragment$onResume$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCreateChannel.Companion companion = WidgetCreateChannel.Companion;
                Context requireContext2 = WidgetServerSettingsChannelsFabMenuFragment.this.requireContext();
                m.checkNotNullExpressionValue(requireContext2, "requireContext()");
                WidgetCreateChannel.Companion.show$default(companion, requireContext2, j, 2, null, 8, null);
            }
        })), o.to(Integer.valueOf((int) R.id.fab_menu_add_text), createListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannelsFabMenuFragment$onResume$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCreateChannel.Companion companion = WidgetCreateChannel.Companion;
                Context requireContext2 = WidgetServerSettingsChannelsFabMenuFragment.this.requireContext();
                m.checkNotNullExpressionValue(requireContext2, "requireContext()");
                WidgetCreateChannel.Companion.show$default(companion, requireContext2, j, 0, null, 8, null);
            }
        })), o.to(Integer.valueOf((int) R.id.fab_menu_main), createListener(WidgetServerSettingsChannelsFabMenuFragment$onResume$5.INSTANCE))));
        TableLayout tableLayout = getBinding().c;
        m.checkNotNullExpressionValue(tableLayout, "binding.fabMenuTable");
        FloatingActionButton floatingActionButton = getBinding().f2528b;
        m.checkNotNullExpressionValue(floatingActionButton, "binding.fabMenuMainFab");
        floatingButtonMenuInitializer.initialize(tableLayout, floatingActionButton, new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannelsFabMenuFragment$onResume$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsChannelsFabMenuFragment.this.dismiss();
            }
        });
    }
}
