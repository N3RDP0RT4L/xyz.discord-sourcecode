package com.discord.widgets.servers;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.utilities.channel.GuildChannelsInfo;
import com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import rx.functions.Func3;
/* compiled from: WidgetServerNotificationsOverrideSelector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000e\u001a\u0016\u0012\u0004\u0012\u00020\u000b \u0004*\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n0\n2.\u0010\u0005\u001a*\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003 \u0004*\u0014\u0012\b\u0012\u00060\u0001j\u0002`\u0002\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00000\u00002\u000e\u0010\u0007\u001a\n \u0004*\u0004\u0018\u00010\u00060\u00062\u000e\u0010\t\u001a\n \u0004*\u0004\u0018\u00010\b0\bH\n¢\u0006\u0004\b\f\u0010\r"}, d2 = {"", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "channels", "Lcom/discord/utilities/channel/GuildChannelsInfo;", "guildChannelsInfo", "", "filter", "", "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", NotificationCompat.CATEGORY_CALL, "(Ljava/util/Map;Lcom/discord/utilities/channel/GuildChannelsInfo;Ljava/lang/String;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerNotificationsOverrideSelector$Companion$get$1<T1, T2, T3, R> implements Func3<Map<Long, ? extends Channel>, GuildChannelsInfo, String, List<? extends CategoricalDragAndDropAdapter.Payload>> {
    public static final WidgetServerNotificationsOverrideSelector$Companion$get$1 INSTANCE = new WidgetServerNotificationsOverrideSelector$Companion$get$1();

    @Override // rx.functions.Func3
    public /* bridge */ /* synthetic */ List<? extends CategoricalDragAndDropAdapter.Payload> call(Map<Long, ? extends Channel> map, GuildChannelsInfo guildChannelsInfo, String str) {
        return call2((Map<Long, Channel>) map, guildChannelsInfo, str);
    }

    /* JADX WARN: Code restructure failed: missing block: B:10:0x0039, code lost:
        if (d0.g0.w.contains((java.lang.CharSequence) r1, (java.lang.CharSequence) r13, true) != false) goto L12;
     */
    /* renamed from: call  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.util.List<com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter.Payload> call2(java.util.Map<java.lang.Long, com.discord.api.channel.Channel> r11, com.discord.utilities.channel.GuildChannelsInfo r12, java.lang.String r13) {
        /*
            r10 = this;
            java.lang.String r0 = "channels"
            d0.z.d.m.checkNotNullExpressionValue(r11, r0)
            java.util.List r11 = r12.getSortedVisibleChannels(r11)
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            java.util.Iterator r11 = r11.iterator()
        L12:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L43
            java.lang.Object r0 = r11.next()
            r1 = r0
            com.discord.api.channel.Channel r1 = (com.discord.api.channel.Channel) r1
            boolean r2 = com.discord.api.channel.ChannelUtils.B(r1)
            r3 = 1
            if (r2 != 0) goto L2c
            boolean r2 = com.discord.api.channel.ChannelUtils.k(r1)
            if (r2 == 0) goto L3c
        L2c:
            java.lang.String r1 = com.discord.api.channel.ChannelUtils.c(r1)
            java.lang.String r2 = "filter"
            d0.z.d.m.checkNotNullExpressionValue(r13, r2)
            boolean r1 = d0.g0.w.contains(r1, r13, r3)
            if (r1 == 0) goto L3c
            goto L3d
        L3c:
            r3 = 0
        L3d:
            if (r3 == 0) goto L12
            r12.add(r0)
            goto L12
        L43:
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.Iterator r12 = r12.iterator()
        L4c:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L8f
            java.lang.Object r13 = r12.next()
            r1 = r13
            com.discord.api.channel.Channel r1 = (com.discord.api.channel.Channel) r1
            boolean r13 = com.discord.api.channel.ChannelUtils.k(r1)
            if (r13 == 0) goto L75
            com.discord.widgets.servers.SettingsChannelListAdapter$CategoryItem r13 = new com.discord.widgets.servers.SettingsChannelListAdapter$CategoryItem
            java.lang.String r3 = com.discord.api.channel.ChannelUtils.c(r1)
            long r4 = r1.h()
            int r6 = r1.t()
            r7 = 0
            r8 = 0
            r9 = 0
            r2 = r13
            r2.<init>(r3, r4, r6, r7, r8, r9)
            goto L89
        L75:
            boolean r13 = com.discord.api.channel.ChannelUtils.B(r1)
            if (r13 == 0) goto L88
            com.discord.widgets.servers.SettingsChannelListAdapter$ChannelItem r13 = new com.discord.widgets.servers.SettingsChannelListAdapter$ChannelItem
            r2 = 0
            long r3 = r1.r()
            r5 = 0
            r0 = r13
            r0.<init>(r1, r2, r3, r5)
            goto L89
        L88:
            r13 = 0
        L89:
            if (r13 == 0) goto L4c
            r11.add(r13)
            goto L4c
        L8f:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.WidgetServerNotificationsOverrideSelector$Companion$get$1.call2(java.util.Map, com.discord.utilities.channel.GuildChannelsInfo, java.lang.String):java.util.List");
    }
}
