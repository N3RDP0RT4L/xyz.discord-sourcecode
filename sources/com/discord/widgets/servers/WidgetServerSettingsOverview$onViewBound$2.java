package com.discord.widgets.servers;

import com.discord.api.channel.Channel;
import com.discord.databinding.WidgetServerSettingsOverviewBinding;
import com.discord.stores.StoreStream;
import com.discord.utilities.stateful.StatefulViews;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsOverview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\u0006\u0010\u0004\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "channelId", "", "<anonymous parameter 1>", "", "invoke", "(JLjava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsOverview$onViewBound$2 extends o implements Function2<Long, String, Unit> {
    public final /* synthetic */ WidgetServerSettingsOverview this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsOverview$onViewBound$2(WidgetServerSettingsOverview widgetServerSettingsOverview) {
        super(2);
        this.this$0 = widgetServerSettingsOverview;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Long l, String str) {
        invoke(l.longValue(), str);
        return Unit.a;
    }

    public final void invoke(long j, String str) {
        StatefulViews statefulViews;
        StatefulViews statefulViews2;
        WidgetServerSettingsOverviewBinding binding;
        m.checkNotNullParameter(str, "<anonymous parameter 1>");
        Channel channel = StoreStream.Companion.getChannels().getChannel(j);
        statefulViews = this.this$0.state;
        statefulViews.put(R.id.system_channel, channel);
        statefulViews2 = this.this$0.state;
        binding = this.this$0.getBinding();
        statefulViews2.configureSaveActionView(binding.e);
        this.this$0.configureSystemChannel(channel);
    }
}
