package com.discord.widgets.servers;

import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: SettingsChannelListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\t\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000*\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "(J)Ljava/lang/Long;", "positiveOrNull"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsChannelListAdapter$computeChangedPositions$2 extends o implements Function1<Long, Long> {
    public static final SettingsChannelListAdapter$computeChangedPositions$2 INSTANCE = new SettingsChannelListAdapter$computeChangedPositions$2();

    public SettingsChannelListAdapter$computeChangedPositions$2() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Long invoke(Long l) {
        return invoke(l.longValue());
    }

    public final Long invoke(long j) {
        if (j > 0) {
            return Long.valueOf(j);
        }
        return null;
    }
}
