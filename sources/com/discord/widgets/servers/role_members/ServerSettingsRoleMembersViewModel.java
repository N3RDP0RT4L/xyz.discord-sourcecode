package com.discord.widgets.servers.role_members;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreGatewayConnection;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.channels.permissions.PermissionOwner;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
/* compiled from: ServerSettingsRoleMembersViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 <2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004<=>?Bq\u0012\n\u0010/\u001a\u00060\fj\u0002`.\u0012\n\u0010%\u001a\u00060\fj\u0002`$\u0012\b\b\u0002\u0010\"\u001a\u00020!\u0012\u000e\b\u0002\u00101\u001a\b\u0012\u0004\u0012\u00020\u001a00\u0012\b\b\u0002\u0010,\u001a\u00020+\u0012\b\b\u0002\u00104\u001a\u000203\u0012\b\b\u0002\u00106\u001a\u000205\u0012\b\b\u0002\u00108\u001a\u000207\u0012\u000e\b\u0002\u00109\u001a\b\u0012\u0004\u0012\u00020\u00030\u0017¢\u0006\u0004\b:\u0010;J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J;\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\b2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\fj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000bH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0013\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00130\u0017¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ\u0015\u0010\u001f\u001a\u00020\u00052\u0006\u0010\u001e\u001a\u00020\t¢\u0006\u0004\b\u001f\u0010 R\u0016\u0010\"\u001a\u00020!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u001a\u0010%\u001a\u00060\fj\u0002`$8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b%\u0010&R:\u0010)\u001a&\u0012\f\u0012\n (*\u0004\u0018\u00010\u00130\u0013 (*\u0012\u0012\f\u0012\n (*\u0004\u0018\u00010\u00130\u0013\u0018\u00010'0'8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u001a\u0010/\u001a\u00060\fj\u0002`.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u0010&R\u001c\u00101\u001a\b\u0012\u0004\u0012\u00020\u001a008\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b1\u00102¨\u0006@"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$StoreState;)V", "", "Lcom/discord/models/member/GuildMember;", "guildMembers", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "users", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMemberAdapterItem;", "generateGuildMemberList", "(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$Event;", "event", "emitEvent", "(Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$Event;)V", "Lrx/Observable;", "observeEvents", "()Lrx/Observable;", "", "query", "updateSearchQuery", "(Ljava/lang/String;)V", "guildMember", "removeRoleFromMember", "(Lcom/discord/models/member/GuildMember;)V", "Lcom/discord/stores/StoreGatewayConnection;", "storeGatewayConnection", "Lcom/discord/stores/StoreGatewayConnection;", "Lcom/discord/primitives/RoleId;", "guildRoleId", "J", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "Lcom/discord/utilities/rest/RestAPI;", "restApi", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/subjects/BehaviorSubject;", "searchQuerySubject", "Lrx/subjects/BehaviorSubject;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreUser;", "storeUsers", "storeStateObservable", HookHelper.constructorName, "(JJLcom/discord/stores/StoreGatewayConnection;Lrx/subjects/BehaviorSubject;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreUser;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsRoleMembersViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final long guildId;
    private final long guildRoleId;
    private final RestAPI restApi;
    private final BehaviorSubject<String> searchQuerySubject;
    private final StoreGatewayConnection storeGatewayConnection;

    /* compiled from: ServerSettingsRoleMembersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$StoreState;", "kotlin.jvm.PlatformType", "storeState", "", "invoke", "(Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.role_members.ServerSettingsRoleMembersViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            ServerSettingsRoleMembersViewModel serverSettingsRoleMembersViewModel = ServerSettingsRoleMembersViewModel.this;
            m.checkNotNullExpressionValue(storeState, "storeState");
            serverSettingsRoleMembersViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: ServerSettingsRoleMembersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "query", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.role_members.ServerSettingsRoleMembersViewModel$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<String, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            StoreGatewayConnection.requestGuildMembers$default(ServerSettingsRoleMembersViewModel.this.storeGatewayConnection, ServerSettingsRoleMembersViewModel.this.guildId, str, null, null, 12, null);
        }
    }

    /* compiled from: ServerSettingsRoleMembersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017JE\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010JQ\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/RoleId;", "guildRoleId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lrx/Observable;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$StoreState;", "observeGuildRoleMembers", "(JJLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/updates/ObservationDeck;)Lrx/Observable;", "Lrx/subjects/BehaviorSubject;", "", "searchFilterSubject", "observeStoreState", "(JJLrx/subjects/BehaviorSubject;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUser;Lcom/discord/stores/updates/ObservationDeck;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeGuildRoleMembers(long j, long j2, StoreGuilds storeGuilds, StoreUser storeUser, ObservationDeck observationDeck) {
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeGuilds, storeUser}, false, null, null, new ServerSettingsRoleMembersViewModel$Companion$observeGuildRoleMembers$1(storeGuilds, j, j2, storeUser), 14, null);
        }

        public final Observable<StoreState> observeStoreState(long j, long j2, BehaviorSubject<String> behaviorSubject, StoreGuilds storeGuilds, StoreUser storeUser, ObservationDeck observationDeck) {
            m.checkNotNullParameter(behaviorSubject, "searchFilterSubject");
            m.checkNotNullParameter(storeGuilds, "storeGuilds");
            m.checkNotNullParameter(storeUser, "storeUser");
            m.checkNotNullParameter(observationDeck, "observationDeck");
            Observable<StoreState> j3 = Observable.j(behaviorSubject.q(), observeGuildRoleMembers(j, j2, storeGuilds, storeUser, observationDeck), ServerSettingsRoleMembersViewModel$Companion$observeStoreState$1.INSTANCE);
            m.checkNotNullExpressionValue(j3, "Observable.combineLatest…eredGuildMembers)\n      }");
            return j3;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ServerSettingsRoleMembersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0004B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0001\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$Event;", "", HookHelper.constructorName, "()V", "RemoveMemberFailure", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$Event$RemoveMemberFailure;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: ServerSettingsRoleMembersViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$Event$RemoveMemberFailure;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$Event;", "Lcom/discord/utilities/error/Error;", "component1", "()Lcom/discord/utilities/error/Error;", "error", "copy", "(Lcom/discord/utilities/error/Error;)Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$Event$RemoveMemberFailure;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/utilities/error/Error;", "getError", HookHelper.constructorName, "(Lcom/discord/utilities/error/Error;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class RemoveMemberFailure extends Event {
            private final Error error;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public RemoveMemberFailure(Error error) {
                super(null);
                m.checkNotNullParameter(error, "error");
                this.error = error;
            }

            public static /* synthetic */ RemoveMemberFailure copy$default(RemoveMemberFailure removeMemberFailure, Error error, int i, Object obj) {
                if ((i & 1) != 0) {
                    error = removeMemberFailure.error;
                }
                return removeMemberFailure.copy(error);
            }

            public final Error component1() {
                return this.error;
            }

            public final RemoveMemberFailure copy(Error error) {
                m.checkNotNullParameter(error, "error");
                return new RemoveMemberFailure(error);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof RemoveMemberFailure) && m.areEqual(this.error, ((RemoveMemberFailure) obj).error);
                }
                return true;
            }

            public final Error getError() {
                return this.error;
            }

            public int hashCode() {
                Error error = this.error;
                if (error != null) {
                    return error.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("RemoveMemberFailure(error=");
                R.append(this.error);
                R.append(")");
                return R.toString();
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: ServerSettingsRoleMembersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0016\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0006\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b$\u0010%J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\n\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJF\u0010\u0012\u001a\u00020\u00002\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0018\b\u0002\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00062\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\fHÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b\u001f\u0010\u000eR\u001f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010 \u001a\u0004\b!\u0010\u0005R)\u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\t0\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b#\u0010\u000b¨\u0006&"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$StoreState;", "", "", "Lcom/discord/models/member/GuildMember;", "component1", "()Ljava/util/List;", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/user/User;", "component2", "()Ljava/util/Map;", "Lcom/discord/api/role/GuildRole;", "component3", "()Lcom/discord/api/role/GuildRole;", "guildMembers", "users", "role", "copy", "(Ljava/util/List;Ljava/util/Map;Lcom/discord/api/role/GuildRole;)Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/role/GuildRole;", "getRole", "Ljava/util/List;", "getGuildMembers", "Ljava/util/Map;", "getUsers", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/Map;Lcom/discord/api/role/GuildRole;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final List<GuildMember> guildMembers;
        private final GuildRole role;
        private final Map<Long, User> users;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(List<GuildMember> list, Map<Long, ? extends User> map, GuildRole guildRole) {
            m.checkNotNullParameter(list, "guildMembers");
            m.checkNotNullParameter(map, "users");
            this.guildMembers = list;
            this.users = map;
            this.role = guildRole;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ StoreState copy$default(StoreState storeState, List list, Map map, GuildRole guildRole, int i, Object obj) {
            if ((i & 1) != 0) {
                list = storeState.guildMembers;
            }
            if ((i & 2) != 0) {
                map = storeState.users;
            }
            if ((i & 4) != 0) {
                guildRole = storeState.role;
            }
            return storeState.copy(list, map, guildRole);
        }

        public final List<GuildMember> component1() {
            return this.guildMembers;
        }

        public final Map<Long, User> component2() {
            return this.users;
        }

        public final GuildRole component3() {
            return this.role;
        }

        public final StoreState copy(List<GuildMember> list, Map<Long, ? extends User> map, GuildRole guildRole) {
            m.checkNotNullParameter(list, "guildMembers");
            m.checkNotNullParameter(map, "users");
            return new StoreState(list, map, guildRole);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.guildMembers, storeState.guildMembers) && m.areEqual(this.users, storeState.users) && m.areEqual(this.role, storeState.role);
        }

        public final List<GuildMember> getGuildMembers() {
            return this.guildMembers;
        }

        public final GuildRole getRole() {
            return this.role;
        }

        public final Map<Long, User> getUsers() {
            return this.users;
        }

        public int hashCode() {
            List<GuildMember> list = this.guildMembers;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            Map<Long, User> map = this.users;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            GuildRole guildRole = this.role;
            if (guildRole != null) {
                i = guildRole.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(guildMembers=");
            R.append(this.guildMembers);
            R.append(", users=");
            R.append(this.users);
            R.append(", role=");
            R.append(this.role);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: ServerSettingsRoleMembersViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Loaded", "Loading", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: ServerSettingsRoleMembersViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\b\b\u0002\u0010\f\u001a\u00020\u0002\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ4\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0019\u001a\u00020\u00022\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\f\u0010\u0004R\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\b¨\u0006\""}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState$Loaded;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState;", "", "component1", "()Z", "", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMemberAdapterItem;", "component2", "()Ljava/util/List;", "Lcom/discord/api/role/GuildRole;", "component3", "()Lcom/discord/api/role/GuildRole;", "isUpdating", "memberList", "role", "copy", "(ZLjava/util/List;Lcom/discord/api/role/GuildRole;)Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/role/GuildRole;", "getRole", "Z", "Ljava/util/List;", "getMemberList", HookHelper.constructorName, "(ZLjava/util/List;Lcom/discord/api/role/GuildRole;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final boolean isUpdating;
            private final List<ServerSettingsRoleMemberAdapterItem> memberList;
            private final GuildRole role;

            public /* synthetic */ Loaded(boolean z2, List list, GuildRole guildRole, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? false : z2, list, guildRole);
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Loaded copy$default(Loaded loaded, boolean z2, List list, GuildRole guildRole, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = loaded.isUpdating;
                }
                if ((i & 2) != 0) {
                    list = loaded.memberList;
                }
                if ((i & 4) != 0) {
                    guildRole = loaded.role;
                }
                return loaded.copy(z2, list, guildRole);
            }

            public final boolean component1() {
                return this.isUpdating;
            }

            public final List<ServerSettingsRoleMemberAdapterItem> component2() {
                return this.memberList;
            }

            public final GuildRole component3() {
                return this.role;
            }

            public final Loaded copy(boolean z2, List<ServerSettingsRoleMemberAdapterItem> list, GuildRole guildRole) {
                m.checkNotNullParameter(list, "memberList");
                m.checkNotNullParameter(guildRole, "role");
                return new Loaded(z2, list, guildRole);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return this.isUpdating == loaded.isUpdating && m.areEqual(this.memberList, loaded.memberList) && m.areEqual(this.role, loaded.role);
            }

            public final List<ServerSettingsRoleMemberAdapterItem> getMemberList() {
                return this.memberList;
            }

            public final GuildRole getRole() {
                return this.role;
            }

            public int hashCode() {
                boolean z2 = this.isUpdating;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                int i3 = i * 31;
                List<ServerSettingsRoleMemberAdapterItem> list = this.memberList;
                int i4 = 0;
                int hashCode = (i3 + (list != null ? list.hashCode() : 0)) * 31;
                GuildRole guildRole = this.role;
                if (guildRole != null) {
                    i4 = guildRole.hashCode();
                }
                return hashCode + i4;
            }

            public final boolean isUpdating() {
                return this.isUpdating;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(isUpdating=");
                R.append(this.isUpdating);
                R.append(", memberList=");
                R.append(this.memberList);
                R.append(", role=");
                R.append(this.role);
                R.append(")");
                return R.toString();
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(boolean z2, List<ServerSettingsRoleMemberAdapterItem> list, GuildRole guildRole) {
                super(null);
                m.checkNotNullParameter(list, "memberList");
                m.checkNotNullParameter(guildRole, "role");
                this.isUpdating = z2;
                this.memberList = list;
                this.role = guildRole;
            }
        }

        /* compiled from: ServerSettingsRoleMembersViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState$Loading;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ ServerSettingsRoleMembersViewModel(long r15, long r17, com.discord.stores.StoreGatewayConnection r19, rx.subjects.BehaviorSubject r20, com.discord.utilities.rest.RestAPI r21, com.discord.stores.StoreGuilds r22, com.discord.stores.updates.ObservationDeck r23, com.discord.stores.StoreUser r24, rx.Observable r25, int r26, kotlin.jvm.internal.DefaultConstructorMarker r27) {
        /*
            r14 = this;
            r0 = r26
            r1 = r0 & 4
            if (r1 == 0) goto Le
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGatewayConnection r1 = r1.getGatewaySocket()
            r7 = r1
            goto L10
        Le:
            r7 = r19
        L10:
            r1 = r0 & 8
            if (r1 == 0) goto L21
            java.lang.String r1 = ""
            rx.subjects.BehaviorSubject r1 = rx.subjects.BehaviorSubject.l0(r1)
            java.lang.String r2 = "BehaviorSubject.create(\"\")"
            d0.z.d.m.checkNotNullExpressionValue(r1, r2)
            r8 = r1
            goto L23
        L21:
            r8 = r20
        L23:
            r1 = r0 & 16
            if (r1 == 0) goto L2f
            com.discord.utilities.rest.RestAPI$Companion r1 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r1 = r1.getApi()
            r9 = r1
            goto L31
        L2f:
            r9 = r21
        L31:
            r1 = r0 & 32
            if (r1 == 0) goto L3d
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r1 = r1.getGuilds()
            r10 = r1
            goto L3f
        L3d:
            r10 = r22
        L3f:
            r1 = r0 & 64
            if (r1 == 0) goto L49
            com.discord.stores.updates.ObservationDeck r1 = com.discord.stores.updates.ObservationDeckProvider.get()
            r11 = r1
            goto L4b
        L49:
            r11 = r23
        L4b:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L57
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r1 = r1.getUsers()
            r12 = r1
            goto L59
        L57:
            r12 = r24
        L59:
            r0 = r0 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L73
            com.discord.widgets.servers.role_members.ServerSettingsRoleMembersViewModel$Companion r0 = com.discord.widgets.servers.role_members.ServerSettingsRoleMembersViewModel.Companion
            r19 = r0
            r20 = r15
            r22 = r17
            r24 = r8
            r25 = r10
            r26 = r12
            r27 = r11
            rx.Observable r0 = r19.observeStoreState(r20, r22, r24, r25, r26, r27)
            r13 = r0
            goto L75
        L73:
            r13 = r25
        L75:
            r2 = r14
            r3 = r15
            r5 = r17
            r2.<init>(r3, r5, r7, r8, r9, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.role_members.ServerSettingsRoleMembersViewModel.<init>(long, long, com.discord.stores.StoreGatewayConnection, rx.subjects.BehaviorSubject, com.discord.utilities.rest.RestAPI, com.discord.stores.StoreGuilds, com.discord.stores.updates.ObservationDeck, com.discord.stores.StoreUser, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void emitEvent(Event event) {
        this.eventSubject.k.onNext(event);
    }

    private final List<ServerSettingsRoleMemberAdapterItem> generateGuildMemberList(List<GuildMember> list, Map<Long, ? extends User> map) {
        ArrayList arrayList = new ArrayList();
        for (GuildMember guildMember : list) {
            User user = map.get(Long.valueOf(guildMember.getUserId()));
            if (user != null) {
                Object nick = guildMember.getNick();
                if (nick == null) {
                    nick = UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user, null, null, 3, null);
                }
                arrayList.add(new ServerSettingsRoleMemberAdapterItem(guildMember, new PermissionOwner.Member(user, nick.toString(), false)));
            }
        }
        return arrayList;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        if (storeState.getRole() != null) {
            ViewState requireViewState = requireViewState();
            if (!(requireViewState instanceof ViewState.Loaded)) {
                requireViewState = null;
            }
            ViewState.Loaded loaded = (ViewState.Loaded) requireViewState;
            updateViewState(new ViewState.Loaded(loaded != null ? loaded.isUpdating() : false, generateGuildMemberList(storeState.getGuildMembers(), storeState.getUsers()), storeState.getRole()));
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    public final void removeRoleFromMember(GuildMember guildMember) {
        m.checkNotNullParameter(guildMember, "guildMember");
        ViewState requireViewState = requireViewState();
        if (!(requireViewState instanceof ViewState.Loaded)) {
            requireViewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) requireViewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, true, null, null, 6, null));
            List<Long> mutableList = u.toMutableList((Collection) guildMember.getRoles());
            mutableList.remove(Long.valueOf(this.guildRoleId));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(this.restApi.changeGuildMember(this.guildId, guildMember.getUserId(), RestAPIParams.GuildMember.Companion.createWithRoles(mutableList)), false, 1, null), this, null, 2, null), ServerSettingsRoleMembersViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new ServerSettingsRoleMembersViewModel$removeRoleFromMember$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new ServerSettingsRoleMembersViewModel$removeRoleFromMember$1(this, loaded));
        }
    }

    public final void updateSearchQuery(String str) {
        m.checkNotNullParameter(str, "query");
        this.searchQuerySubject.onNext(str);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerSettingsRoleMembersViewModel(long j, long j2, StoreGatewayConnection storeGatewayConnection, BehaviorSubject<String> behaviorSubject, RestAPI restAPI, StoreGuilds storeGuilds, ObservationDeck observationDeck, StoreUser storeUser, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(storeGatewayConnection, "storeGatewayConnection");
        m.checkNotNullParameter(behaviorSubject, "searchQuerySubject");
        m.checkNotNullParameter(restAPI, "restApi");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(observationDeck, "observationDeck");
        m.checkNotNullParameter(storeUser, "storeUsers");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.guildId = j;
        this.guildRoleId = j2;
        this.storeGatewayConnection = storeGatewayConnection;
        this.searchQuerySubject = behaviorSubject;
        this.restApi = restAPI;
        this.eventSubject = PublishSubject.k0();
        StoreGuilds.Actions.requestRoleMembers(j, j2, this, restAPI, storeGatewayConnection);
        Observable<StoreState> q = observable.q();
        m.checkNotNullExpressionValue(q, "storeStateObservable\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), ServerSettingsRoleMembersViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
        Observable<String> O = behaviorSubject.O(750L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(O, "searchQuerySubject\n     …0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(O), this, null, 2, null), ServerSettingsRoleMembersViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass2());
    }
}
