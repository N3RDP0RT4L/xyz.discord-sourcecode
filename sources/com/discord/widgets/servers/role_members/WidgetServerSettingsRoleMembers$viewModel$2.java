package com.discord.widgets.servers.role_members;

import com.discord.app.AppViewModel;
import com.discord.widgets.servers.role_members.ServerSettingsRoleMembersViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetServerSettingsRoleMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewModel$ViewState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsRoleMembers$viewModel$2 extends o implements Function0<AppViewModel<ServerSettingsRoleMembersViewModel.ViewState>> {
    public final /* synthetic */ WidgetServerSettingsRoleMembers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsRoleMembers$viewModel$2(WidgetServerSettingsRoleMembers widgetServerSettingsRoleMembers) {
        super(0);
        this.this$0 = widgetServerSettingsRoleMembers;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<ServerSettingsRoleMembersViewModel.ViewState> invoke() {
        long guildId;
        long guildRoleId;
        guildId = this.this$0.getGuildId();
        guildRoleId = this.this$0.getGuildRoleId();
        return new ServerSettingsRoleMembersViewModel(guildId, guildRoleId, null, null, null, null, null, null, null, 508, null);
    }
}
