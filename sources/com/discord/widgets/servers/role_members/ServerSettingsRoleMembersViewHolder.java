package com.discord.widgets.servers.role_members;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.RemovablePermissionOwnerViewBinding;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetServerSettingsRoleMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000fJ/\u0010\t\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0018\u0010\b\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0004¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMembersViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMemberAdapterItem;", "adapterItem", "Lkotlin/Function2;", "Lcom/discord/models/member/GuildMember;", "Lcom/discord/models/user/User;", "", "removeMemberClickListener", "configureUI", "(Lcom/discord/widgets/servers/role_members/ServerSettingsRoleMemberAdapterItem;Lkotlin/jvm/functions/Function2;)V", "Lcom/discord/databinding/RemovablePermissionOwnerViewBinding;", "binding", "Lcom/discord/databinding/RemovablePermissionOwnerViewBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/RemovablePermissionOwnerViewBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ServerSettingsRoleMembersViewHolder extends RecyclerView.ViewHolder {
    private final RemovablePermissionOwnerViewBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public ServerSettingsRoleMembersViewHolder(RemovablePermissionOwnerViewBinding removablePermissionOwnerViewBinding) {
        super(removablePermissionOwnerViewBinding.a);
        m.checkNotNullParameter(removablePermissionOwnerViewBinding, "binding");
        this.binding = removablePermissionOwnerViewBinding;
    }

    public final void configureUI(final ServerSettingsRoleMemberAdapterItem serverSettingsRoleMemberAdapterItem, final Function2<? super GuildMember, ? super User, Unit> function2) {
        m.checkNotNullParameter(serverSettingsRoleMemberAdapterItem, "adapterItem");
        m.checkNotNullParameter(function2, "removeMemberClickListener");
        this.binding.f2121b.a(serverSettingsRoleMemberAdapterItem.getPermissionOwner());
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.role_members.ServerSettingsRoleMembersViewHolder$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function2.this.invoke(serverSettingsRoleMemberAdapterItem.getGuildMember(), serverSettingsRoleMemberAdapterItem.getPermissionOwner().getUser());
            }
        });
    }
}
