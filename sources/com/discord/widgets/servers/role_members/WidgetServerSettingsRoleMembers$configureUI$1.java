package com.discord.widgets.servers.role_members;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.FragmentManager;
import b.a.a.e;
import b.a.k.b;
import com.discord.dialogs.SimpleConfirmationDialogArgs;
import com.discord.models.member.GuildMember;
import com.discord.models.user.User;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.servers.role_members.ServerSettingsRoleMembersViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsRoleMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/models/member/GuildMember;", "guildMember", "Lcom/discord/models/user/User;", "user", "", "invoke", "(Lcom/discord/models/member/GuildMember;Lcom/discord/models/user/User;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsRoleMembers$configureUI$1 extends o implements Function2<GuildMember, User, Unit> {
    public final /* synthetic */ ServerSettingsRoleMembersViewModel.ViewState $viewState;
    public final /* synthetic */ WidgetServerSettingsRoleMembers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsRoleMembers$configureUI$1(WidgetServerSettingsRoleMembers widgetServerSettingsRoleMembers, ServerSettingsRoleMembersViewModel.ViewState viewState) {
        super(2);
        this.this$0 = widgetServerSettingsRoleMembers;
        this.$viewState = viewState;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(GuildMember guildMember, User user) {
        invoke2(guildMember, user);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(final GuildMember guildMember, User user) {
        CharSequence b2;
        m.checkNotNullParameter(guildMember, "guildMember");
        m.checkNotNullParameter(user, "user");
        e.c cVar = e.k;
        FragmentManager childFragmentManager = this.this$0.getChildFragmentManager();
        m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        String string = this.this$0.getString(R.string.role_remove_member_confirm_title);
        Context requireContext = this.this$0.requireContext();
        Object[] objArr = new Object[2];
        Object nick = guildMember.getNick();
        if (nick == null) {
            nick = UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, user, null, null, 3, null);
        }
        objArr[0] = nick;
        objArr[1] = ((ServerSettingsRoleMembersViewModel.ViewState.Loaded) this.$viewState).getRole().g();
        b2 = b.b(requireContext, R.string.role_remove_member_confirm_body, objArr, (r4 & 4) != 0 ? b.C0034b.j : null);
        cVar.a(childFragmentManager, new SimpleConfirmationDialogArgs(string, b2.toString(), this.this$0.getString(R.string.remove), this.this$0.getString(R.string.cancel)), new View.OnClickListener() { // from class: com.discord.widgets.servers.role_members.WidgetServerSettingsRoleMembers$configureUI$1.1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ServerSettingsRoleMembersViewModel viewModel;
                viewModel = WidgetServerSettingsRoleMembers$configureUI$1.this.this$0.getViewModel();
                viewModel.removeRoleFromMember(guildMember);
            }
        });
    }
}
