package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.databinding.ViewCategoryOverrideItemBinding;
import com.discord.databinding.ViewChannelOverrideItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelNotificationSettings;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.servers.NotificationsOverridesAdapter;
import d0.j;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: NotificationsOverridesAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003#$%B!\u0012\u0018\u0010\u001d\u001a\u0014\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000f0\u001b¢\u0006\u0004\b!\u0010\"J\u000f\u0010\u0004\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\f\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R6\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0006@GX\u0086\u000e¢\u0006\u0012\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR+\u0010\u001d\u001a\u0014\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000f0\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 ¨\u0006&"}, d2 = {"Lcom/discord/widgets/servers/NotificationsOverridesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder;", "", "getItemCount", "()I", ModelAuditLogEntry.CHANGE_KEY_POSITION, "getItemViewType", "(I)I", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder;", "holder", "", "onBindViewHolder", "(Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder;I)V", "", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;", "value", "data", "Ljava/util/List;", "getData", "()Ljava/util/List;", "setData", "(Ljava/util/List;)V", "Lkotlin/Function2;", "Landroid/view/View;", "onClick", "Lkotlin/jvm/functions/Function2;", "getOnClick", "()Lkotlin/jvm/functions/Function2;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function2;)V", "DiffCallback", "Item", "ViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NotificationsOverridesAdapter extends RecyclerView.Adapter<ViewHolder> {
    private List<Item> data = n.emptyList();
    private final Function2<View, Item, Unit> onClick;

    /* compiled from: NotificationsOverridesAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B#\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\f\u0012\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004J\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u000b\u0010\nR\u001c\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u000f¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/servers/NotificationsOverridesAdapter$DiffCallback;", "Landroidx/recyclerview/widget/DiffUtil$Callback;", "", "getOldListSize", "()I", "getNewListSize", "oldItemPosition", "newItemPosition", "", "areContentsTheSame", "(II)Z", "areItemsTheSame", "", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;", "oldItems", "Ljava/util/List;", "newItems", HookHelper.constructorName, "(Ljava/util/List;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DiffCallback extends DiffUtil.Callback {
        private final List<Item> newItems;
        private final List<Item> oldItems;

        public DiffCallback(List<Item> list, List<Item> list2) {
            m.checkNotNullParameter(list, "newItems");
            m.checkNotNullParameter(list2, "oldItems");
            this.newItems = list;
            this.oldItems = list2;
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public boolean areContentsTheSame(int i, int i2) {
            return m.areEqual(this.newItems.get(i2), this.oldItems.get(i));
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public boolean areItemsTheSame(int i, int i2) {
            return this.newItems.get(i2).getChannel().h() == this.oldItems.get(i).getChannel().h();
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public int getNewListSize() {
            return this.newItems.size();
        }

        @Override // androidx.recyclerview.widget.DiffUtil.Callback
        public int getOldListSize() {
            return this.oldItems.size();
        }
    }

    /* compiled from: NotificationsOverridesAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u001b\u0010\f\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\t\u001a\u0004\b\r\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;", "", "Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;", "overrideSettings", "Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;", "getOverrideSettings", "()Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/api/channel/Channel;", "getChannel", "()Lcom/discord/api/channel/Channel;", "parent", "getParent", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Item {
        private final Channel channel;
        private final ModelNotificationSettings.ChannelOverride overrideSettings;
        private final Channel parent;

        public Item(Channel channel, Channel channel2, ModelNotificationSettings.ChannelOverride channelOverride) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(channelOverride, "overrideSettings");
            this.channel = channel;
            this.parent = channel2;
            this.overrideSettings = channelOverride;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final ModelNotificationSettings.ChannelOverride getOverrideSettings() {
            return this.overrideSettings;
        }

        public final Channel getParent() {
            return this.parent;
        }
    }

    /* compiled from: NotificationsOverridesAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0014\u0015B+\b\u0002\u0012\u0006\u0010\u0011\u001a\u00020\f\u0012\u0018\u0010\r\u001a\u0014\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u000b¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\t\u0010\nR+\u0010\r\u001a\u0014\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010\u0082\u0001\u0002\u0016\u0017¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;", "data", "", "onBind", "(Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;)V", "", "messageNotificationLevel", "messageNotificationToString", "(I)I", "Lkotlin/Function2;", "Landroid/view/View;", "onClick", "Lkotlin/jvm/functions/Function2;", "getOnClick", "()Lkotlin/jvm/functions/Function2;", "itemView", HookHelper.constructorName, "(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V", "CategoryOverridesViewHolder", "ChannelOverridesViewHolder", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder$ChannelOverridesViewHolder;", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder$CategoryOverridesViewHolder;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewHolder extends RecyclerView.ViewHolder {
        private final Function2<View, Item, Unit> onClick;

        /* compiled from: NotificationsOverridesAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0018\u0010\f\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\n¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder$CategoryOverridesViewHolder;", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder;", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;", "data", "", "onBind", "(Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;)V", "Lcom/discord/databinding/ViewCategoryOverrideItemBinding;", "binding", "Lcom/discord/databinding/ViewCategoryOverrideItemBinding;", "Lkotlin/Function2;", "Landroid/view/View;", "onClick", HookHelper.constructorName, "(Lcom/discord/databinding/ViewCategoryOverrideItemBinding;Lkotlin/jvm/functions/Function2;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class CategoryOverridesViewHolder extends ViewHolder {
            private final ViewCategoryOverrideItemBinding binding;

            /* JADX WARN: Illegal instructions before constructor call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public CategoryOverridesViewHolder(com.discord.databinding.ViewCategoryOverrideItemBinding r3, kotlin.jvm.functions.Function2<? super android.view.View, ? super com.discord.widgets.servers.NotificationsOverridesAdapter.Item, kotlin.Unit> r4) {
                /*
                    r2 = this;
                    java.lang.String r0 = "binding"
                    d0.z.d.m.checkNotNullParameter(r3, r0)
                    java.lang.String r0 = "onClick"
                    d0.z.d.m.checkNotNullParameter(r4, r0)
                    androidx.constraintlayout.widget.ConstraintLayout r0 = r3.a
                    java.lang.String r1 = "binding.root"
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    r1 = 0
                    r2.<init>(r0, r4, r1)
                    r2.binding = r3
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.NotificationsOverridesAdapter.ViewHolder.CategoryOverridesViewHolder.<init>(com.discord.databinding.ViewCategoryOverrideItemBinding, kotlin.jvm.functions.Function2):void");
            }

            @Override // com.discord.widgets.servers.NotificationsOverridesAdapter.ViewHolder
            public void onBind(Item item) {
                m.checkNotNullParameter(item, "data");
                super.onBind(item);
                TextView textView = this.binding.f2158b;
                m.checkNotNullExpressionValue(textView, "binding.categoryOverrideName");
                textView.setText(ChannelUtils.c(item.getChannel()));
                int messageNotificationToString = item.getOverrideSettings().isMuted() ? R.string.form_label_muted : messageNotificationToString(item.getOverrideSettings().getMessageNotifications());
                TextView textView2 = this.binding.c;
                m.checkNotNullExpressionValue(textView2, "binding.categoryOverrideStatus");
                b.m(textView2, messageNotificationToString, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            }
        }

        /* compiled from: NotificationsOverridesAdapter.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0018\u0010\f\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\n¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder$ChannelOverridesViewHolder;", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$ViewHolder;", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;", "data", "", "onBind", "(Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;)V", "Lcom/discord/databinding/ViewChannelOverrideItemBinding;", "binding", "Lcom/discord/databinding/ViewChannelOverrideItemBinding;", "Lkotlin/Function2;", "Landroid/view/View;", "onClick", HookHelper.constructorName, "(Lcom/discord/databinding/ViewChannelOverrideItemBinding;Lkotlin/jvm/functions/Function2;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ChannelOverridesViewHolder extends ViewHolder {
            private final ViewChannelOverrideItemBinding binding;

            /* JADX WARN: Illegal instructions before constructor call */
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct add '--show-bad-code' argument
            */
            public ChannelOverridesViewHolder(com.discord.databinding.ViewChannelOverrideItemBinding r3, kotlin.jvm.functions.Function2<? super android.view.View, ? super com.discord.widgets.servers.NotificationsOverridesAdapter.Item, kotlin.Unit> r4) {
                /*
                    r2 = this;
                    java.lang.String r0 = "binding"
                    d0.z.d.m.checkNotNullParameter(r3, r0)
                    java.lang.String r0 = "onClick"
                    d0.z.d.m.checkNotNullParameter(r4, r0)
                    androidx.constraintlayout.widget.ConstraintLayout r0 = r3.a
                    java.lang.String r1 = "binding.root"
                    d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                    r1 = 0
                    r2.<init>(r0, r4, r1)
                    r2.binding = r3
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.NotificationsOverridesAdapter.ViewHolder.ChannelOverridesViewHolder.<init>(com.discord.databinding.ViewChannelOverrideItemBinding, kotlin.jvm.functions.Function2):void");
            }

            @Override // com.discord.widgets.servers.NotificationsOverridesAdapter.ViewHolder
            public void onBind(Item item) {
                m.checkNotNullParameter(item, "data");
                super.onBind(item);
                TextView textView = this.binding.c;
                m.checkNotNullExpressionValue(textView, "binding.channelOverrideName");
                textView.setText(ChannelUtils.c(item.getChannel()));
                TextView textView2 = this.binding.f2159b;
                m.checkNotNullExpressionValue(textView2, "binding.channelOverrideCategoryName");
                Channel parent = item.getParent();
                ViewExtensions.setTextAndVisibilityBy(textView2, parent != null ? ChannelUtils.c(parent) : null);
                int messageNotificationToString = item.getOverrideSettings().isMuted() ? R.string.form_label_muted : messageNotificationToString(item.getOverrideSettings().getMessageNotifications());
                TextView textView3 = this.binding.d;
                m.checkNotNullExpressionValue(textView3, "binding.channelOverrideStatus");
                b.m(textView3, messageNotificationToString, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
            }
        }

        public /* synthetic */ ViewHolder(View view, Function2 function2, DefaultConstructorMarker defaultConstructorMarker) {
            this(view, function2);
        }

        public final Function2<View, Item, Unit> getOnClick() {
            return this.onClick;
        }

        @StringRes
        public final int messageNotificationToString(int i) {
            if (i == ModelNotificationSettings.FREQUENCY_ALL) {
                return R.string.form_label_all_messages;
            }
            if (i == ModelNotificationSettings.FREQUENCY_MENTIONS) {
                return R.string.form_label_only_mentions;
            }
            if (i == ModelNotificationSettings.FREQUENCY_NOTHING) {
                return R.string.form_label_nothing;
            }
            return 0;
        }

        public void onBind(final Item item) {
            m.checkNotNullParameter(item, "data");
            this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.NotificationsOverridesAdapter$ViewHolder$onBind$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function2<View, NotificationsOverridesAdapter.Item, Unit> onClick = NotificationsOverridesAdapter.ViewHolder.this.getOnClick();
                    m.checkNotNullExpressionValue(view, "it");
                    onClick.invoke(view, item);
                }
            });
        }

        /* JADX WARN: Multi-variable type inference failed */
        private ViewHolder(View view, Function2<? super View, ? super Item, Unit> function2) {
            super(view);
            this.onClick = function2;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public NotificationsOverridesAdapter(Function2<? super View, ? super Item, Unit> function2) {
        m.checkNotNullParameter(function2, "onClick");
        this.onClick = function2;
    }

    public final List<Item> getData() {
        return this.data;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.data.get(i).getChannel().A();
    }

    public final Function2<View, Item, Unit> getOnClick() {
        return this.onClick;
    }

    @MainThread
    public final void setData(List<Item> list) {
        m.checkNotNullParameter(list, "value");
        DiffUtil.DiffResult calculateDiff = DiffUtil.calculateDiff(new DiffCallback(list, this.data), true);
        m.checkNotNullExpressionValue(calculateDiff, "DiffUtil.calculateDiff(D…back(value, field), true)");
        this.data = list;
        calculateDiff.dispatchUpdatesTo(this);
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        m.checkNotNullParameter(viewHolder, "holder");
        viewHolder.onBind(this.data.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        int i2 = R.id.navigation_indicator;
        if (!(i == 0 || i == 15)) {
            if (i == 4) {
                View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_category_override_item, viewGroup, false);
                TextView textView = (TextView) inflate.findViewById(R.id.category_override_name);
                if (textView != null) {
                    TextView textView2 = (TextView) inflate.findViewById(R.id.category_override_status);
                    if (textView2 != null) {
                        Guideline guideline = (Guideline) inflate.findViewById(R.id.guideline);
                        if (guideline != null) {
                            ImageView imageView = (ImageView) inflate.findViewById(R.id.navigation_indicator);
                            if (imageView != null) {
                                ViewCategoryOverrideItemBinding viewCategoryOverrideItemBinding = new ViewCategoryOverrideItemBinding((ConstraintLayout) inflate, textView, textView2, guideline, imageView);
                                m.checkNotNullExpressionValue(viewCategoryOverrideItemBinding, "ViewCategoryOverrideItem….context), parent, false)");
                                return new ViewHolder.CategoryOverridesViewHolder(viewCategoryOverrideItemBinding, this.onClick);
                            }
                        } else {
                            i2 = R.id.guideline;
                        }
                    } else {
                        i2 = R.id.category_override_status;
                    }
                } else {
                    i2 = R.id.category_override_name;
                }
                throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
            } else if (i != 5) {
                throw new j(a.v("An operation is not implemented: ", a.q("Type[", i, "] not implemented")));
            }
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_channel_override_item, viewGroup, false);
        TextView textView3 = (TextView) inflate2.findViewById(R.id.channel_override_category_name);
        if (textView3 != null) {
            TextView textView4 = (TextView) inflate2.findViewById(R.id.channel_override_name);
            if (textView4 != null) {
                TextView textView5 = (TextView) inflate2.findViewById(R.id.channel_override_status);
                if (textView5 != null) {
                    Guideline guideline2 = (Guideline) inflate2.findViewById(R.id.guideline);
                    if (guideline2 != null) {
                        ImageView imageView2 = (ImageView) inflate2.findViewById(R.id.navigation_indicator);
                        if (imageView2 != null) {
                            ViewChannelOverrideItemBinding viewChannelOverrideItemBinding = new ViewChannelOverrideItemBinding((ConstraintLayout) inflate2, textView3, textView4, textView5, guideline2, imageView2);
                            m.checkNotNullExpressionValue(viewChannelOverrideItemBinding, "ViewChannelOverrideItemB….context), parent, false)");
                            return new ViewHolder.ChannelOverridesViewHolder(viewChannelOverrideItemBinding, this.onClick);
                        }
                    } else {
                        i2 = R.id.guideline;
                    }
                } else {
                    i2 = R.id.channel_override_status;
                }
            } else {
                i2 = R.id.channel_override_name;
            }
        } else {
            i2 = R.id.channel_override_category_name;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(i2)));
    }
}
