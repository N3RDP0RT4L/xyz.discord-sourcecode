package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.f;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildExplicitContentFilter;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.guild.preview.GuildPreview;
import com.discord.app.AppActivity;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsOverviewAfkTimeoutBinding;
import com.discord.databinding.WidgetServerSettingsOverviewBinding;
import com.discord.dialogs.ImageUploadDialog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildProfiles;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.channels.WidgetChannelSelector;
import com.discord.widgets.servers.WidgetServerSettingsOverview;
import com.discord.widgets.servers.guildboost.WidgetGuildBoost;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.g0.t;
import d0.t.n;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func5;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsOverview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 N2\u00020\u0001:\u0003ONPB\u0007¢\u0006\u0004\bM\u00109J\u0015\u0010\u0004\u001a\u00020\u0003*\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0015\u0010\u0006\u001a\u00020\u0003*\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0005J\u0017\u0010\t\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000b\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\nJ\u0017\u0010\f\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\f\u0010\nJ!\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J3\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\r2\b\u0010\u0012\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J+\u0010\u0019\u001a\u00020\u00032\b\u0010\u0017\u001a\u0004\u0018\u00010\r2\b\u0010\u0018\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ+\u0010\u001d\u001a\u00020\u00032\b\u0010\u001b\u001a\u0004\u0018\u00010\r2\b\u0010\u001c\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u001d\u0010\u001aJ/\u0010\"\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\"\u0010#J\u0019\u0010&\u001a\u00020\u00032\b\u0010%\u001a\u0004\u0018\u00010$H\u0002¢\u0006\u0004\b&\u0010'J\u0019\u0010(\u001a\u00020\u00032\b\u0010%\u001a\u0004\u0018\u00010$H\u0002¢\u0006\u0004\b(\u0010'J1\u0010,\u001a\u00020\u0003*\u00020)2\b\u0010%\u001a\u0004\u0018\u00010$2\b\b\u0001\u0010*\u001a\u00020\u001e2\b\b\u0001\u0010+\u001a\u00020\u001eH\u0002¢\u0006\u0004\b,\u0010-J\u001f\u00101\u001a\u00020\u00032\u0006\u0010/\u001a\u00020.2\u0006\u00100\u001a\u00020\rH\u0016¢\u0006\u0004\b1\u00102J\u001f\u00103\u001a\u00020\u00032\u0006\u0010/\u001a\u00020.2\u0006\u00100\u001a\u00020\rH\u0016¢\u0006\u0004\b3\u00102J\u0017\u00106\u001a\u00020\u00032\u0006\u00105\u001a\u000204H\u0016¢\u0006\u0004\b6\u00107J\u000f\u00108\u001a\u00020\u0003H\u0016¢\u0006\u0004\b8\u00109R\u0016\u0010:\u001a\u00020\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b:\u0010;R\u001c\u0010=\u001a\u00020<8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@R\u0016\u0010B\u001a\u00020A8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bB\u0010CR\u001e\u0010E\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010D8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bE\u0010FR\u001d\u0010L\u001a\u00020G8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bH\u0010I\u001a\u0004\bJ\u0010K¨\u0006Q"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsOverview;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;)V", "configureUpdatedGuild", "Lcom/discord/models/guild/Guild;", "guild", "configureSplashSection", "(Lcom/discord/models/guild/Guild;)V", "configureBannerSection", "configureAnimatedBannerUpsellSection", "", "guildShortName", "iconOriginal", "openAvatarPicker", "(Ljava/lang/String;Ljava/lang/String;)V", "iconUpdated", "", "stateShouldUpdate", "configureIcon", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V", "splashOriginalUrl", "splashUpdatedUrl", "configureSplashImage", "(Ljava/lang/String;Ljava/lang/String;Z)V", "bannerOriginalUrl", "bannerUpdatedUrl", "configureBannerImage", "", "stateId", "activeRadioIndex", "isAboveNotifyAllSize", "configureRadios", "(IIZZ)V", "Lcom/discord/api/channel/Channel;", "selectedChannel", "configureAfkChannel", "(Lcom/discord/api/channel/Channel;)V", "configureSystemChannel", "Landroid/widget/TextView;", "defaultStringRes", "iconRes", "configureChannel", "(Landroid/widget/TextView;Lcom/discord/api/channel/Channel;II)V", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "mimeType", "onImageChosen", "(Landroid/net/Uri;Ljava/lang/String;)V", "onImageCropped", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "hasOpenedAvatarPicker", "Z", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "Lrx/functions/Action1;", "imageSelectedResult", "Lrx/functions/Action1;", "Lcom/discord/databinding/WidgetServerSettingsOverviewBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsOverviewBinding;", "binding", HookHelper.constructorName, "Companion", "AfkBottomSheet", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsOverview extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsOverview.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsOverviewBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String INTENT_EXTRA_OPEN_AVATAR_PICKER = "INTENT_EXTRA_OPEN_AVATAR_PICKER";
    private static final String REQUEST_KEY_AFK_CHANNEL = "REQUEST_KEY_AFK_CHANNEL";
    private static final String REQUEST_KEY_SYSTEM_CHANNEL = "REQUEST_KEY_SYSTEM_CHANNEL";
    private static final int STATE_ID_NOTIFICATION_DEFAULT = 90001;
    private boolean hasOpenedAvatarPicker;
    private Action1<String> imageSelectedResult;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsOverview$binding$2.INSTANCE, null, 2, null);
    private final StatefulViews state = new StatefulViews(R.id.server_settings_overview_icon, R.id.overview_name, R.id.afk_channel, R.id.afk_timeout, R.id.afk_timeout_wrap, R.id.system_channel, R.id.upload_splash, R.id.upload_banner, R.id.system_channel_join, R.id.system_channel_join_replies, R.id.system_channel_boost, STATE_ID_NOTIFICATION_DEFAULT);
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettingsOverview$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetServerSettingsOverview.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0000\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J!\u0010\n\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000bR\u001d\u0010\u0011\u001a\u00020\f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015¨\u0006\u0019"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsOverview$AfkBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "Lcom/discord/databinding/WidgetServerSettingsOverviewAfkTimeoutBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsOverviewAfkTimeoutBinding;", "binding", "", "Landroid/widget/TextView;", "getTimeouts", "()Ljava/util/List;", "timeouts", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class AfkBottomSheet extends AppBottomSheet {
        public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(AfkBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsOverviewAfkTimeoutBinding;", 0)};
        private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsOverview$AfkBottomSheet$binding$2.INSTANCE, null, 2, null);

        public AfkBottomSheet() {
            super(false, 1, null);
        }

        private final WidgetServerSettingsOverviewAfkTimeoutBinding getBinding() {
            return (WidgetServerSettingsOverviewAfkTimeoutBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
        }

        private final List<TextView> getTimeouts() {
            TextView textView = getBinding().f2555b;
            m.checkNotNullExpressionValue(textView, "binding.serverSettingsOverviewAfkTimeout01");
            TextView textView2 = getBinding().c;
            m.checkNotNullExpressionValue(textView2, "binding.serverSettingsOverviewAfkTimeout05");
            TextView textView3 = getBinding().d;
            m.checkNotNullExpressionValue(textView3, "binding.serverSettingsOverviewAfkTimeout15");
            TextView textView4 = getBinding().e;
            m.checkNotNullExpressionValue(textView4, "binding.serverSettingsOverviewAfkTimeout30");
            TextView textView5 = getBinding().f;
            m.checkNotNullExpressionValue(textView5, "binding.serverSettingsOverviewAfkTimeout60");
            return n.listOf((Object[]) new TextView[]{textView, textView2, textView3, textView4, textView5});
        }

        @Override // com.discord.app.AppBottomSheet
        public int getContentViewResId() {
            return R.layout.widget_server_settings_overview_afk_timeout;
        }

        @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
        public void onViewCreated(View view, Bundle bundle) {
            m.checkNotNullParameter(view, "view");
            super.onViewCreated(view, bundle);
            Fragment parentFragment = getParentFragment();
            if (!(parentFragment instanceof WidgetServerSettingsOverview)) {
                parentFragment = null;
            }
            final WidgetServerSettingsOverview widgetServerSettingsOverview = (WidgetServerSettingsOverview) parentFragment;
            if (widgetServerSettingsOverview == null) {
                dismiss();
                return;
            }
            for (TextView textView : getTimeouts()) {
                Object tag = textView.getTag();
                Objects.requireNonNull(tag, "null cannot be cast to non-null type kotlin.String");
                final int parseInt = Integer.parseInt((String) tag);
                Model.Companion companion = Model.Companion;
                Context context = textView.getContext();
                m.checkNotNullExpressionValue(context, "it.context");
                final CharSequence afkTimeout = companion.getAfkTimeout(context, parseInt);
                textView.setText(afkTimeout);
                textView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$AfkBottomSheet$onViewCreated$$inlined$forEach$lambda$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        StatefulViews statefulViews;
                        WidgetServerSettingsOverviewBinding binding;
                        StatefulViews statefulViews2;
                        WidgetServerSettingsOverviewBinding binding2;
                        WidgetServerSettingsOverviewBinding binding3;
                        WidgetServerSettingsOverviewBinding binding4;
                        WidgetServerSettingsOverview widgetServerSettingsOverview2 = widgetServerSettingsOverview;
                        statefulViews = widgetServerSettingsOverview2.state;
                        binding = widgetServerSettingsOverview2.getBinding();
                        TextView textView2 = binding.f2556b.d;
                        m.checkNotNullExpressionValue(textView2, "binding.afk.afkTimeout");
                        statefulViews.put(textView2.getId(), afkTimeout);
                        statefulViews2 = widgetServerSettingsOverview2.state;
                        binding2 = widgetServerSettingsOverview2.getBinding();
                        LinearLayout linearLayout = binding2.f2556b.e;
                        m.checkNotNullExpressionValue(linearLayout, "binding.afk.afkTimeoutWrap");
                        statefulViews2.put(linearLayout.getId(), Integer.valueOf(parseInt));
                        binding3 = widgetServerSettingsOverview2.getBinding();
                        TextView textView3 = binding3.f2556b.d;
                        m.checkNotNullExpressionValue(textView3, "binding.afk.afkTimeout");
                        textView3.setText(afkTimeout);
                        binding4 = widgetServerSettingsOverview2.getBinding();
                        LinearLayout linearLayout2 = binding4.f2556b.e;
                        m.checkNotNullExpressionValue(linearLayout2, "binding.afk.afkTimeoutWrap");
                        linearLayout2.setTag(afkTimeout);
                        this.dismiss();
                    }
                });
            }
        }
    }

    /* compiled from: WidgetServerSettingsOverview.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0015\u0010\u0016J-\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\b\u0002\u0010\b\u001a\u00020\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000eR\u0016\u0010\u0011\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u000eR\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "openAvatarPicker", "", "create", "(Landroid/content/Context;JZ)V", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", WidgetServerSettingsOverview.INTENT_EXTRA_OPEN_AVATAR_PICKER, WidgetServerSettingsOverview.REQUEST_KEY_AFK_CHANNEL, WidgetServerSettingsOverview.REQUEST_KEY_SYSTEM_CHANNEL, "", "STATE_ID_NOTIFICATION_DEFAULT", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void create$default(Companion companion, Context context, long j, boolean z2, int i, Object obj) {
            if ((i & 4) != 0) {
                z2 = false;
            }
            companion.create(context, j, z2);
        }

        public final void create(Context context, long j, boolean z2) {
            m.checkNotNullParameter(context, "context");
            StoreStream.Companion.getAnalytics().onGuildSettingsPaneViewed("OVERVIEW", j);
            Intent putExtra = new Intent().putExtra("INTENT_EXTRA_GUILD_ID", j).putExtra(WidgetServerSettingsOverview.INTENT_EXTRA_OPEN_AVATAR_PICKER, z2);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…PICKER, openAvatarPicker)");
            j.d(context, WidgetServerSettingsOverview.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsOverview.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u0000 !2\u00020\u0001:\u0001!BE\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\u000e\u0010\u001c\u001a\n\u0018\u00010\u001aj\u0004\u0018\u0001`\u001b\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u001d¢\u0006\u0004\b\u001f\u0010 R\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0004\u001a\u0004\b\f\u0010\u0006R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0004\u001a\u0004\b\u0010\u0010\u0006R\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\t\u001a\u0004\b\u0012\u0010\u000bR\u0019\u0010\u0014\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\""}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;", "", "", "canManage", "Z", "getCanManage", "()Z", "Lcom/discord/api/channel/Channel;", "systemChannelModel", "Lcom/discord/api/channel/Channel;", "getSystemChannelModel", "()Lcom/discord/api/channel/Channel;", "isOwner", "", "approximateMemberCount", "Ljava/lang/Integer;", "isAboveNotifyAllSize", "afkChannelModel", "getAfkChannelModel", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/guild/Guild;", "getGuild", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/user/MeUser;", "me", "", "Lcom/discord/api/permission/PermissionBit;", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;", "guildProfile", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Ljava/lang/Long;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Channel afkChannelModel;
        private final Integer approximateMemberCount;
        private final boolean canManage;
        private final Guild guild;
        private final boolean isAboveNotifyAllSize;
        private final boolean isOwner;
        private final Channel systemChannelModel;

        /* compiled from: WidgetServerSettingsOverview.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J!\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\u000e\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;", "get", "(J)Lrx/Observable;", "Landroid/content/Context;", "context", "", "afkTimeout", "", "getAfkTimeout", "(Landroid/content/Context;I)Ljava/lang/CharSequence;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(final long j) {
                Observable<R> Y = StoreStream.Companion.getGuilds().observeGuild(j).Y(new b<Guild, Observable<? extends Model>>() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$Model$Companion$get$1
                    public final Observable<? extends WidgetServerSettingsOverview.Model> call(final Guild guild) {
                        if (guild == null) {
                            return new k(null);
                        }
                        StoreStream.Companion companion = StoreStream.Companion;
                        Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
                        StoreChannels channels = companion.getChannels();
                        Long afkChannelId = guild.getAfkChannelId();
                        long j2 = 0;
                        Observable<Channel> observeChannel = channels.observeChannel(afkChannelId != null ? afkChannelId.longValue() : 0L);
                        StoreChannels channels2 = companion.getChannels();
                        Long systemChannelId = guild.getSystemChannelId();
                        if (systemChannelId != null) {
                            j2 = systemChannelId.longValue();
                        }
                        return Observable.g(observeMe$default, observeChannel, channels2.observeChannel(j2), companion.getPermissions().observePermissionsForGuild(j), companion.getGuildProfiles().observeGuildProfile(j), new Func5<MeUser, Channel, Channel, Long, StoreGuildProfiles.GuildProfileData, WidgetServerSettingsOverview.Model>() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$Model$Companion$get$1.1
                            public final WidgetServerSettingsOverview.Model call(MeUser meUser, Channel channel, Channel channel2, Long l, StoreGuildProfiles.GuildProfileData guildProfileData) {
                                Guild guild2 = Guild.this;
                                m.checkNotNullExpressionValue(meUser, "me");
                                return new WidgetServerSettingsOverview.Model(guild2, meUser, channel, channel2, l, guildProfileData);
                            }
                        });
                    }
                });
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …        }\n              }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(Y).q();
                m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
                return q;
            }

            public final CharSequence getAfkTimeout(Context context, int i) {
                m.checkNotNullParameter(context, "context");
                if (i == 60) {
                    Resources resources = context.getResources();
                    m.checkNotNullExpressionValue(resources, "context.resources");
                    return StringResourceUtilsKt.getQuantityString(resources, context, (int) R.plurals.duration_minutes_minutes, 1, 1);
                } else if (i == 300) {
                    Resources resources2 = context.getResources();
                    m.checkNotNullExpressionValue(resources2, "context.resources");
                    return StringResourceUtilsKt.getQuantityString(resources2, context, (int) R.plurals.duration_minutes_minutes, 5, 5);
                } else if (i == 900) {
                    Resources resources3 = context.getResources();
                    m.checkNotNullExpressionValue(resources3, "context.resources");
                    return StringResourceUtilsKt.getQuantityString(resources3, context, (int) R.plurals.duration_minutes_minutes, 15, 15);
                } else if (i == 1800) {
                    Resources resources4 = context.getResources();
                    m.checkNotNullExpressionValue(resources4, "context.resources");
                    return StringResourceUtilsKt.getQuantityString(resources4, context, (int) R.plurals.duration_minutes_minutes, 30, 30);
                } else if (i != 3600) {
                    return "";
                } else {
                    Resources resources5 = context.getResources();
                    m.checkNotNullExpressionValue(resources5, "context.resources");
                    return StringResourceUtilsKt.getQuantityString(resources5, context, (int) R.plurals.duration_hours_hours, 1, 1);
                }
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Guild guild, MeUser meUser, Channel channel, Channel channel2, Long l, StoreGuildProfiles.GuildProfileData guildProfileData) {
            GuildPreview data;
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(meUser, "me");
            this.guild = guild;
            this.afkChannelModel = channel;
            this.systemChannelModel = channel2;
            boolean isOwner = guild.isOwner(meUser.getId());
            this.isOwner = isOwner;
            boolean z2 = false;
            this.canManage = isOwner || PermissionUtils.canAndIsElevated(32L, l, meUser.getMfaEnabled(), guild.getMfaLevel());
            Integer a = (guildProfileData == null || (data = guildProfileData.getData()) == null) ? null : data.a();
            this.approximateMemberCount = a;
            if (a != null && a.intValue() > 2500) {
                z2 = true;
            }
            this.isAboveNotifyAllSize = z2;
        }

        public final Channel getAfkChannelModel() {
            return this.afkChannelModel;
        }

        public final boolean getCanManage() {
            return this.canManage;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Channel getSystemChannelModel() {
            return this.systemChannelModel;
        }

        public final boolean isAboveNotifyAllSize() {
            return this.isAboveNotifyAllSize;
        }

        public final boolean isOwner() {
            return this.isOwner;
        }
    }

    public WidgetServerSettingsOverview() {
        super(R.layout.widget_server_settings_overview);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureAfkChannel(Channel channel) {
        TextView textView = getBinding().f2556b.f208b;
        m.checkNotNullExpressionValue(textView, "binding.afk.afkChannel");
        configureChannel(textView, channel, R.string.no_afk_channel, R.drawable.ic_channel_voice_16dp);
    }

    private final void configureAnimatedBannerUpsellSection(final Guild guild) {
        int premiumTier = guild.getPremiumTier();
        Integer minimumBoostTierForGuildFeature = PremiumUtils.INSTANCE.getMinimumBoostTierForGuildFeature(GuildFeature.ANIMATED_BANNER);
        boolean z2 = false;
        boolean z3 = premiumTier == (minimumBoostTierForGuildFeature != null ? minimumBoostTierForGuildFeature.intValue() : 0) - 1;
        boolean canHaveAnimatedBanner = guild.canHaveAnimatedBanner();
        LinearLayout linearLayout = getBinding().h.f137b.c;
        m.checkNotNullExpressionValue(linearLayout, "binding.uploadBanner.ani…atedBannerUpsellContainer");
        int i = 8;
        linearLayout.setVisibility(canHaveAnimatedBanner || z3 ? 0 : 8);
        MaterialButton materialButton = getBinding().h.f137b.f130b;
        m.checkNotNullExpressionValue(materialButton, "binding.uploadBanner.ani…nimatedBannerUpsellButton");
        if (z3) {
            i = 0;
        }
        materialButton.setVisibility(i);
        TextView textView = getBinding().h.f137b.d;
        m.checkNotNullExpressionValue(textView, "binding.uploadBanner.ani….animatedBannerUpsellText");
        b.a.k.b.m(textView, canHaveAnimatedBanner ? R.string.guild_settings_animated_banner_try_it : R.string.guild_settings_animated_banner_upsell, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        MaterialButton materialButton2 = getBinding().h.f137b.f130b;
        m.checkNotNullExpressionValue(materialButton2, "binding.uploadBanner.ani…nimatedBannerUpsellButton");
        if (materialButton2.getVisibility() == 0) {
            z2 = true;
        }
        if (z2) {
            getBinding().h.f137b.f130b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureAnimatedBannerUpsellSection$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetGuildBoost.Companion.create(a.x(view, "it", "it.context"), Guild.this.getId());
                }
            });
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureBannerImage(final String str, String str2, boolean z2) {
        if (z2) {
            StatefulViews statefulViews = this.state;
            SimpleDraweeView simpleDraweeView = getBinding().h.e;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.uploadBanner.uploadBanner");
            statefulViews.put(simpleDraweeView.getId(), str2 != null ? str2 : "");
        } else {
            StatefulViews statefulViews2 = this.state;
            SimpleDraweeView simpleDraweeView2 = getBinding().h.e;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.uploadBanner.uploadBanner");
            str2 = (String) statefulViews2.get(simpleDraweeView2.getId(), str);
        }
        getBinding().h.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureBannerImage$1

            /* compiled from: WidgetServerSettingsOverview.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsOverview$configureBannerImage$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends o implements Function0<Unit> {
                public AnonymousClass2() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    WidgetServerSettingsOverview.this.openMediaChooser();
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsOverview.this.imageSelectedResult = new Action1<String>() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureBannerImage$1.1
                    public final void call(String str3) {
                        WidgetServerSettingsOverview$configureBannerImage$1 widgetServerSettingsOverview$configureBannerImage$1 = WidgetServerSettingsOverview$configureBannerImage$1.this;
                        WidgetServerSettingsOverview.this.configureBannerImage(str, str3, true);
                    }
                };
                WidgetServerSettingsOverview.this.requestMedia(new AnonymousClass2());
            }
        });
        SimpleDraweeView simpleDraweeView3 = getBinding().h.e;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.uploadBanner.uploadBanner");
        IconUtils.setIcon$default(simpleDraweeView3, str2, 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
        TextView textView = getBinding().h.d;
        m.checkNotNullExpressionValue(textView, "binding.uploadBanner.ser…verviewUploadBannerRemove");
        int i = 0;
        if (!(!(str2 == null || t.isBlank(str2)))) {
            i = 8;
        }
        textView.setVisibility(i);
        getBinding().h.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureBannerImage$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsOverview.this.configureBannerImage(str, null, true);
            }
        });
        this.state.configureSaveActionView(getBinding().e);
    }

    private final void configureBannerSection(final Guild guild) {
        CharSequence e;
        boolean z2 = true;
        if (guild.canHaveBanner()) {
            getBinding().h.e.setBackgroundResource(R.drawable.drawable_bg_nitro_gradient);
            TextView textView = getBinding().h.h;
            m.checkNotNullExpressionValue(textView, "binding.uploadBanner.uploadBannerNitroTier");
            int i = 8;
            textView.setVisibility(8);
            TextView textView2 = getBinding().h.i;
            m.checkNotNullExpressionValue(textView2, "binding.uploadBanner.uploadBannerUnlock");
            b.a.k.b.m(textView2, R.string.guild_settings_overview_boost_unlocked, new Object[]{ExifInterface.GPS_MEASUREMENT_2D}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView3 = getBinding().h.i;
            m.checkNotNullExpressionValue(textView3, "binding.uploadBanner.uploadBannerUnlock");
            DrawableCompat.setCompoundDrawablesCompat$default(textView3, 0, (int) R.drawable.ic_perk_tier_1_boosted, 0, 0, 13, (Object) null);
            getBinding().h.i.setTextColor(ColorCompat.getColor(requireContext(), (int) R.color.white));
            TextView textView4 = getBinding().h.i;
            m.checkNotNullExpressionValue(textView4, "binding.uploadBanner.uploadBannerUnlock");
            String banner = guild.getBanner();
            if (!(banner == null || banner.length() == 0)) {
                z2 = false;
            }
            if (z2) {
                i = 0;
            }
            textView4.setVisibility(i);
            IconUtils iconUtils = IconUtils.INSTANCE;
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            configureBannerImage(IconUtils.getBannerForGuild$default(iconUtils, guild, displayMetrics != null ? Integer.valueOf(displayMetrics.widthPixels) : null, false, 4, null), null, false);
            return;
        }
        TextView textView5 = getBinding().h.h;
        m.checkNotNullExpressionValue(textView5, "binding.uploadBanner.uploadBannerNitroTier");
        b.a.k.b.m(textView5, R.string.guild_settings_overview_tier_info, new Object[]{ExifInterface.GPS_MEASUREMENT_2D, "tierStatus"}, new WidgetServerSettingsOverview$configureBannerSection$1(guild));
        TextView textView6 = getBinding().h.h;
        m.checkNotNullExpressionValue(textView6, "binding.uploadBanner.uploadBannerNitroTier");
        textView6.setVisibility(0);
        FloatingActionButton floatingActionButton = getBinding().h.f;
        m.checkNotNullExpressionValue(floatingActionButton, "binding.uploadBanner.uploadBannerFab");
        ViewExtensions.setEnabledAlpha$default(floatingActionButton, false, 0.0f, 2, null);
        int premiumSubscriptionCount = 7 - guild.getPremiumSubscriptionCount();
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        CharSequence quantityString = StringResourceUtilsKt.getQuantityString(resources2, requireContext(), (int) R.plurals.guild_settings_overview_boost_unlock_boosts, premiumSubscriptionCount, Integer.valueOf(premiumSubscriptionCount));
        TextView textView7 = getBinding().h.i;
        m.checkNotNullExpressionValue(textView7, "binding.uploadBanner.uploadBannerUnlock");
        e = b.a.k.b.e(this, R.string.guild_settings_overview_boost_unlock, new Object[]{quantityString}, (r4 & 4) != 0 ? b.a.j : null);
        ViewExtensions.setTextAndVisibilityBy(textView7, e);
        TextView textView8 = getBinding().h.i;
        m.checkNotNullExpressionValue(textView8, "binding.uploadBanner.uploadBannerUnlock");
        DrawableCompat.setCompoundDrawablesCompat$default(textView8, 0, (int) R.drawable.ic_perk_lock, 0, 0, 13, (Object) null);
        getBinding().h.i.setTextColor(ColorCompat.getThemedColor(requireContext(), (int) R.attr.primary_300));
        getBinding().h.e.setBackgroundColor(ColorCompat.getThemedColor(requireContext(), (int) R.attr.primary_630));
        getBinding().h.h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureBannerSection$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildBoost.Companion.create(a.x(view, "it", "it.context"), Guild.this.getId());
            }
        });
    }

    private final void configureChannel(TextView textView, Channel channel, @StringRes int i, @DrawableRes int i2) {
        CharSequence charSequence;
        if (channel != null) {
            charSequence = ChannelUtils.c(channel);
        } else {
            Context context = textView.getContext();
            m.checkNotNullExpressionValue(context, "context");
            charSequence = b.a.k.b.b(context, i, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
        }
        textView.setText(charSequence);
        DrawableCompat.setCompoundDrawablesCompat$default(textView, channel == null ? 0 : i2, 0, 0, 0, 14, (Object) null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureIcon(final String str, final String str2, String str3, boolean z2) {
        boolean z3 = true;
        int i = 0;
        if (!this.hasOpenedAvatarPicker && getMostRecentIntent().getBooleanExtra(INTENT_EXTRA_OPEN_AVATAR_PICKER, false)) {
            openAvatarPicker(str, str2);
        }
        if (z2) {
            StatefulViews statefulViews = this.state;
            SimpleDraweeView simpleDraweeView = getBinding().c.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.header.serverSettingsOverviewIcon");
            statefulViews.put(simpleDraweeView.getId(), str3 != null ? str3 : "");
        } else {
            StatefulViews statefulViews2 = this.state;
            SimpleDraweeView simpleDraweeView2 = getBinding().c.d;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.header.serverSettingsOverviewIcon");
            str3 = (String) statefulViews2.get(simpleDraweeView2.getId(), str2);
        }
        getBinding().c.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureIcon$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsOverview.this.openAvatarPicker(str, str2);
            }
        });
        SimpleDraweeView simpleDraweeView3 = getBinding().c.d;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.header.serverSettingsOverviewIcon");
        IconUtils.setIcon$default(simpleDraweeView3, str3, (int) R.dimen.avatar_size_extra_large, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
        if (str3 != null && !t.isBlank(str3)) {
            z3 = false;
        }
        TextView textView = getBinding().c.f110b;
        m.checkNotNullExpressionValue(textView, "binding.header.iconLabel");
        textView.setVisibility(z3 ? 0 : 8);
        TextView textView2 = getBinding().c.e;
        m.checkNotNullExpressionValue(textView2, "binding.header.serverSettingsOverviewIconRemove");
        textView2.setVisibility(z3 ^ true ? 0 : 8);
        getBinding().c.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureIcon$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsOverview.this.configureIcon(str, str2, null, true);
            }
        });
        TextView textView3 = getBinding().c.f;
        m.checkNotNullExpressionValue(textView3, "binding.header.serverSettingsOverviewIconText");
        if (!z3) {
            i = 8;
        }
        textView3.setVisibility(i);
        TextView textView4 = getBinding().c.f;
        m.checkNotNullExpressionValue(textView4, "binding.header.serverSettingsOverviewIconText");
        textView4.setText(str);
        this.state.configureSaveActionView(getBinding().e);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureRadios(final int i, final int i2, final boolean z2, boolean z3) {
        CharSequence e;
        if (z3) {
            this.state.put(i, Integer.valueOf(i2));
        }
        final int i3 = 0;
        for (Object obj : n.listOf((Object[]) new CheckedSetting[]{getBinding().d.f116b, getBinding().d.c})) {
            i3++;
            if (i3 < 0) {
                n.throwIndexOverflow();
            }
            CheckedSetting checkedSetting = (CheckedSetting) obj;
            m.checkNotNullExpressionValue(checkedSetting, "checkedSetting");
            checkedSetting.setChecked(i3 == i2);
            if (!z2 || checkedSetting.getId() != R.id.server_settings_overview_notification_all) {
                checkedSetting.h(null, false);
            } else {
                e = b.a.k.b.e(this, R.string.guild_settings_default_notifications_large_guild_notify_all, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
                checkedSetting.h(e, false);
            }
            checkedSetting.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureRadios$$inlined$forEachIndexed$lambda$1
                public final void call(Boolean bool) {
                    this.configureRadios(i, i3, z2, true);
                }
            });
        }
        this.state.configureSaveActionView(getBinding().e);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureSplashImage(final String str, String str2, boolean z2) {
        if (z2) {
            StatefulViews statefulViews = this.state;
            SimpleDraweeView simpleDraweeView = getBinding().i.e;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.uploadSplash.uploadSplash");
            statefulViews.put(simpleDraweeView.getId(), str2 != null ? str2 : "");
        } else {
            StatefulViews statefulViews2 = this.state;
            SimpleDraweeView simpleDraweeView2 = getBinding().i.e;
            m.checkNotNullExpressionValue(simpleDraweeView2, "binding.uploadSplash.uploadSplash");
            str2 = (String) statefulViews2.get(simpleDraweeView2.getId(), str);
        }
        getBinding().i.f144b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureSplashImage$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsOverview.this.imageSelectedResult = new Action1<String>() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureSplashImage$1.1
                    public final void call(String str3) {
                        WidgetServerSettingsOverview$configureSplashImage$1 widgetServerSettingsOverview$configureSplashImage$1 = WidgetServerSettingsOverview$configureSplashImage$1.this;
                        WidgetServerSettingsOverview.this.configureSplashImage(str, str3, true);
                    }
                };
                WidgetServerSettingsOverview.this.openMediaChooser();
            }
        });
        SimpleDraweeView simpleDraweeView3 = getBinding().i.e;
        m.checkNotNullExpressionValue(simpleDraweeView3, "binding.uploadSplash.uploadSplash");
        IconUtils.setIcon$default(simpleDraweeView3, str2, 0, (Function1) null, (MGImages.ChangeDetector) null, 28, (Object) null);
        TextView textView = getBinding().i.d;
        m.checkNotNullExpressionValue(textView, "binding.uploadSplash.ser…verviewUploadSplashRemove");
        int i = 0;
        if (!(!(str2 == null || t.isBlank(str2)))) {
            i = 8;
        }
        textView.setVisibility(i);
        getBinding().i.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureSplashImage$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsOverview.this.configureSplashImage(str, null, true);
            }
        });
        this.state.configureSaveActionView(getBinding().e);
    }

    private final void configureSplashSection(final Guild guild) {
        CharSequence e;
        boolean z2 = true;
        if (guild.canHaveSplash()) {
            TextView textView = getBinding().i.g;
            m.checkNotNullExpressionValue(textView, "binding.uploadSplash.uploadSplashNitroTier");
            int i = 8;
            textView.setVisibility(8);
            getBinding().i.e.setBackgroundResource(R.drawable.drawable_bg_nitro_gradient);
            TextView textView2 = getBinding().i.h;
            m.checkNotNullExpressionValue(textView2, "binding.uploadSplash.uploadSplashUnlock");
            b.a.k.b.m(textView2, R.string.guild_settings_overview_boost_unlocked, new Object[]{"1"}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView3 = getBinding().i.h;
            m.checkNotNullExpressionValue(textView3, "binding.uploadSplash.uploadSplashUnlock");
            DrawableCompat.setCompoundDrawablesCompat$default(textView3, 0, (int) R.drawable.ic_perk_tier_1_boosted, 0, 0, 13, (Object) null);
            getBinding().i.h.setTextColor(ColorCompat.getColor(requireContext(), (int) R.color.white));
            TextView textView4 = getBinding().i.h;
            m.checkNotNullExpressionValue(textView4, "binding.uploadSplash.uploadSplashUnlock");
            String splash = guild.getSplash();
            if (!(splash == null || splash.length() == 0)) {
                z2 = false;
            }
            if (z2) {
                i = 0;
            }
            textView4.setVisibility(i);
            IconUtils iconUtils = IconUtils.INSTANCE;
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            configureSplashImage(iconUtils.getGuildSplashUrl(guild, displayMetrics != null ? Integer.valueOf(displayMetrics.widthPixels) : null), null, false);
            return;
        }
        TextView textView5 = getBinding().i.g;
        m.checkNotNullExpressionValue(textView5, "binding.uploadSplash.uploadSplashNitroTier");
        b.a.k.b.m(textView5, R.string.guild_settings_overview_tier_info, new Object[]{"1", "tierInfo"}, new WidgetServerSettingsOverview$configureSplashSection$1(guild));
        TextView textView6 = getBinding().i.g;
        m.checkNotNullExpressionValue(textView6, "binding.uploadSplash.uploadSplashNitroTier");
        textView6.setVisibility(0);
        FloatingActionButton floatingActionButton = getBinding().i.c;
        m.checkNotNullExpressionValue(floatingActionButton, "binding.uploadSplash.ser…gsOverviewUploadSplashFab");
        ViewExtensions.setEnabledAlpha$default(floatingActionButton, false, 0.0f, 2, null);
        int premiumSubscriptionCount = 2 - guild.getPremiumSubscriptionCount();
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        CharSequence quantityString = StringResourceUtilsKt.getQuantityString(resources2, requireContext(), (int) R.plurals.guild_settings_overview_boost_unlock_boosts, premiumSubscriptionCount, Integer.valueOf(premiumSubscriptionCount));
        TextView textView7 = getBinding().i.h;
        m.checkNotNullExpressionValue(textView7, "binding.uploadSplash.uploadSplashUnlock");
        e = b.a.k.b.e(this, R.string.guild_settings_overview_boost_unlock, new Object[]{quantityString}, (r4 & 4) != 0 ? b.a.j : null);
        ViewExtensions.setTextAndVisibilityBy(textView7, e);
        TextView textView8 = getBinding().i.h;
        m.checkNotNullExpressionValue(textView8, "binding.uploadSplash.uploadSplashUnlock");
        DrawableCompat.setCompoundDrawablesCompat$default(textView8, 0, (int) R.drawable.ic_perk_lock, 0, 0, 13, (Object) null);
        getBinding().i.h.setTextColor(ColorCompat.getThemedColor(requireContext(), (int) R.attr.primary_300));
        getBinding().i.e.setBackgroundColor(ColorCompat.getThemedColor(requireContext(), (int) R.attr.primary_630));
        getBinding().i.g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureSplashSection$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildBoost.Companion.create(a.x(view, "it", "it.context"), Guild.this.getId());
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureSystemChannel(Channel channel) {
        TextView textView = getBinding().g.f123b;
        m.checkNotNullExpressionValue(textView, "binding.systemChannel.systemChannel");
        configureChannel(textView, channel, R.string.no_system_channel, R.drawable.ic_channel_text_16dp);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        if (model == null || !model.getCanManage()) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        boolean z2 = false;
        configureIcon(model.getGuild().getShortName(), IconUtils.getForGuild$default(model.getGuild(), null, true, null, 10, null), null, false);
        TextInputLayout textInputLayout = getBinding().c.c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.header.overviewName");
        StatefulViews statefulViews = this.state;
        TextInputLayout textInputLayout2 = getBinding().c.c;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.header.overviewName");
        ViewExtensions.setText(textInputLayout, (CharSequence) statefulViews.get(textInputLayout2.getId(), model.getGuild().getName()));
        StatefulViews statefulViews2 = this.state;
        TextView textView = getBinding().f2556b.f208b;
        m.checkNotNullExpressionValue(textView, "binding.afk.afkChannel");
        configureAfkChannel((Channel) statefulViews2.get(textView.getId(), model.getAfkChannelModel()));
        getBinding().f2556b.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelSelector.Companion.launchForVoice(WidgetServerSettingsOverview.this, model.getGuild().getId(), "REQUEST_KEY_AFK_CHANNEL", true, R.string.no_afk_channel);
            }
        });
        TextView textView2 = getBinding().f2556b.d;
        m.checkNotNullExpressionValue(textView2, "binding.afk.afkTimeout");
        StatefulViews statefulViews3 = this.state;
        TextView textView3 = getBinding().f2556b.d;
        m.checkNotNullExpressionValue(textView3, "binding.afk.afkTimeout");
        int id2 = textView3.getId();
        Model.Companion companion = Model.Companion;
        TextView textView4 = getBinding().f2556b.d;
        m.checkNotNullExpressionValue(textView4, "binding.afk.afkTimeout");
        Context context = textView4.getContext();
        m.checkNotNullExpressionValue(context, "binding.afk.afkTimeout.context");
        textView2.setText((CharSequence) statefulViews3.get(id2, companion.getAfkTimeout(context, model.getGuild().getAfkTimeout())));
        getBinding().f2556b.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsOverview.AfkBottomSheet afkBottomSheet = new WidgetServerSettingsOverview.AfkBottomSheet();
                FragmentManager childFragmentManager = WidgetServerSettingsOverview.this.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                afkBottomSheet.show(childFragmentManager, a0.getOrCreateKotlinClass(WidgetServerSettingsOverview.AfkBottomSheet.class).toString());
            }
        });
        LinearLayout linearLayout = getBinding().f2556b.e;
        m.checkNotNullExpressionValue(linearLayout, "binding.afk.afkTimeoutWrap");
        linearLayout.setTag(Integer.valueOf(model.getGuild().getAfkTimeout()));
        StatefulViews statefulViews4 = this.state;
        TextView textView5 = getBinding().g.f123b;
        m.checkNotNullExpressionValue(textView5, "binding.systemChannel.systemChannel");
        configureSystemChannel((Channel) statefulViews4.get(textView5.getId(), model.getSystemChannelModel()));
        getBinding().g.f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelSelector.Companion.launchForText(WidgetServerSettingsOverview.this, model.getGuild().getId(), "REQUEST_KEY_SYSTEM_CHANNEL", true, R.string.no_system_channel);
            }
        });
        LinearLayout linearLayout2 = getBinding().g.f;
        m.checkNotNullExpressionValue(linearLayout2, "binding.systemChannel.systemChannelWrap");
        StatefulViews statefulViews5 = this.state;
        LinearLayout linearLayout3 = getBinding().g.f;
        m.checkNotNullExpressionValue(linearLayout3, "binding.systemChannel.systemChannelWrap");
        linearLayout2.setTag(statefulViews5.get(linearLayout3.getId(), model.getGuild().getSystemChannelId()));
        configureRadios(STATE_ID_NOTIFICATION_DEFAULT, ((Number) this.state.get(STATE_ID_NOTIFICATION_DEFAULT, Integer.valueOf(model.getGuild().getDefaultMessageNotifications()))).intValue(), model.isAboveNotifyAllSize(), false);
        configureSplashSection(model.getGuild());
        configureBannerSection(model.getGuild());
        configureAnimatedBannerUpsellSection(model.getGuild());
        boolean z3 = (model.getGuild().getSystemChannelFlags() & 1) == 0;
        CheckedSetting checkedSetting = getBinding().g.d;
        m.checkNotNullExpressionValue(checkedSetting, "binding.systemChannel.systemChannelJoin");
        StatefulViews statefulViews6 = this.state;
        CheckedSetting checkedSetting2 = getBinding().g.d;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.systemChannel.systemChannelJoin");
        checkedSetting.setChecked(((Boolean) statefulViews6.get(checkedSetting2.getId(), Boolean.valueOf(z3))).booleanValue());
        if (model.getSystemChannelModel() != null) {
            getBinding().g.d.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetServerSettingsOverviewBinding binding;
                    WidgetServerSettingsOverviewBinding binding2;
                    StatefulViews statefulViews7;
                    WidgetServerSettingsOverviewBinding binding3;
                    WidgetServerSettingsOverviewBinding binding4;
                    StatefulViews statefulViews8;
                    WidgetServerSettingsOverviewBinding binding5;
                    binding = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting3 = binding.g.d;
                    m.checkNotNullExpressionValue(checkedSetting3, "binding.systemChannel.systemChannelJoin");
                    binding2 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting4 = binding2.g.d;
                    m.checkNotNullExpressionValue(checkedSetting4, "binding.systemChannel.systemChannelJoin");
                    checkedSetting3.setChecked(!checkedSetting4.isChecked());
                    statefulViews7 = WidgetServerSettingsOverview.this.state;
                    binding3 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting5 = binding3.g.d;
                    m.checkNotNullExpressionValue(checkedSetting5, "binding.systemChannel.systemChannelJoin");
                    int id3 = checkedSetting5.getId();
                    binding4 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting6 = binding4.g.d;
                    m.checkNotNullExpressionValue(checkedSetting6, "binding.systemChannel.systemChannelJoin");
                    statefulViews7.put(id3, Boolean.valueOf(checkedSetting6.isChecked()));
                    statefulViews8 = WidgetServerSettingsOverview.this.state;
                    binding5 = WidgetServerSettingsOverview.this.getBinding();
                    statefulViews8.configureSaveActionView(binding5.e);
                }
            });
        } else {
            CheckedSetting.d(getBinding().g.d, null, 1);
        }
        boolean z4 = (model.getGuild().getSystemChannelFlags() & 2) == 0;
        CheckedSetting checkedSetting3 = getBinding().g.c;
        m.checkNotNullExpressionValue(checkedSetting3, "binding.systemChannel.systemChannelBoost");
        StatefulViews statefulViews7 = this.state;
        CheckedSetting checkedSetting4 = getBinding().g.c;
        m.checkNotNullExpressionValue(checkedSetting4, "binding.systemChannel.systemChannelBoost");
        checkedSetting3.setChecked(((Boolean) statefulViews7.get(checkedSetting4.getId(), Boolean.valueOf(z4))).booleanValue());
        if (model.getSystemChannelModel() != null) {
            getBinding().g.c.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetServerSettingsOverviewBinding binding;
                    WidgetServerSettingsOverviewBinding binding2;
                    StatefulViews statefulViews8;
                    WidgetServerSettingsOverviewBinding binding3;
                    WidgetServerSettingsOverviewBinding binding4;
                    StatefulViews statefulViews9;
                    WidgetServerSettingsOverviewBinding binding5;
                    binding = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting5 = binding.g.c;
                    m.checkNotNullExpressionValue(checkedSetting5, "binding.systemChannel.systemChannelBoost");
                    binding2 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting6 = binding2.g.c;
                    m.checkNotNullExpressionValue(checkedSetting6, "binding.systemChannel.systemChannelBoost");
                    checkedSetting5.setChecked(!checkedSetting6.isChecked());
                    statefulViews8 = WidgetServerSettingsOverview.this.state;
                    binding3 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting7 = binding3.g.c;
                    m.checkNotNullExpressionValue(checkedSetting7, "binding.systemChannel.systemChannelBoost");
                    int id3 = checkedSetting7.getId();
                    binding4 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting8 = binding4.g.c;
                    m.checkNotNullExpressionValue(checkedSetting8, "binding.systemChannel.systemChannelBoost");
                    statefulViews8.put(id3, Boolean.valueOf(checkedSetting8.isChecked()));
                    statefulViews9 = WidgetServerSettingsOverview.this.state;
                    binding5 = WidgetServerSettingsOverview.this.getBinding();
                    statefulViews9.configureSaveActionView(binding5.e);
                }
            });
        } else {
            CheckedSetting.d(getBinding().g.c, null, 1);
        }
        if ((model.getGuild().getSystemChannelFlags() & 8) == 0) {
            z2 = true;
        }
        CheckedSetting checkedSetting5 = getBinding().g.e;
        m.checkNotNullExpressionValue(checkedSetting5, "binding.systemChannel.systemChannelJoinReplies");
        StatefulViews statefulViews8 = this.state;
        CheckedSetting checkedSetting6 = getBinding().g.e;
        m.checkNotNullExpressionValue(checkedSetting6, "binding.systemChannel.systemChannelJoinReplies");
        checkedSetting5.setChecked(((Boolean) statefulViews8.get(checkedSetting6.getId(), Boolean.valueOf(z2))).booleanValue());
        if (model.getSystemChannelModel() != null) {
            getBinding().g.e.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$6
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetServerSettingsOverviewBinding binding;
                    WidgetServerSettingsOverviewBinding binding2;
                    StatefulViews statefulViews9;
                    WidgetServerSettingsOverviewBinding binding3;
                    WidgetServerSettingsOverviewBinding binding4;
                    StatefulViews statefulViews10;
                    WidgetServerSettingsOverviewBinding binding5;
                    binding = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting7 = binding.g.e;
                    m.checkNotNullExpressionValue(checkedSetting7, "binding.systemChannel.systemChannelJoinReplies");
                    binding2 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting8 = binding2.g.e;
                    m.checkNotNullExpressionValue(checkedSetting8, "binding.systemChannel.systemChannelJoinReplies");
                    checkedSetting7.setChecked(!checkedSetting8.isChecked());
                    statefulViews9 = WidgetServerSettingsOverview.this.state;
                    binding3 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting9 = binding3.g.e;
                    m.checkNotNullExpressionValue(checkedSetting9, "binding.systemChannel.systemChannelJoinReplies");
                    int id3 = checkedSetting9.getId();
                    binding4 = WidgetServerSettingsOverview.this.getBinding();
                    CheckedSetting checkedSetting10 = binding4.g.e;
                    m.checkNotNullExpressionValue(checkedSetting10, "binding.systemChannel.systemChannelJoinReplies");
                    statefulViews9.put(id3, Boolean.valueOf(checkedSetting10.isChecked()));
                    statefulViews10 = WidgetServerSettingsOverview.this.state;
                    binding5 = WidgetServerSettingsOverview.this.getBinding();
                    statefulViews10.configureSaveActionView(binding5.e);
                }
            });
        } else {
            CheckedSetting.d(getBinding().g.e, null, 1);
        }
        this.state.configureSaveActionView(getBinding().e);
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$7

            /* compiled from: WidgetServerSettingsOverview.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a*\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004 \u0001*\u0014\u0012\u000e\b\u0001\u0012\n \u0001*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/guild/Guild;", "kotlin.jvm.PlatformType", "updatedGuild", "Lrx/Observable;", "Lcom/discord/models/guild/Guild;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/guild/Guild;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$7$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1<T, R> implements j0.k.b<com.discord.api.guild.Guild, Observable<? extends Guild>> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public final Observable<? extends Guild> call(com.discord.api.guild.Guild guild) {
                    Observable observable = (Observable<R>) StoreStream.Companion.getGuilds().observeGuild(guild.r()).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
                    m.checkNotNullExpressionValue(observable, "filter { it != null }.map { it!! }");
                    return observable;
                }
            }

            /* compiled from: WidgetServerSettingsOverview.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004 \u0001*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/models/guild/Guild;", "kotlin.jvm.PlatformType", "updatedGuild", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$7$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2<T, R> implements j0.k.b<Guild, Observable<? extends WidgetServerSettingsOverview.Model>> {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                public final Observable<? extends WidgetServerSettingsOverview.Model> call(Guild guild) {
                    return WidgetServerSettingsOverview.Model.Companion.get(guild.getId());
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StatefulViews statefulViews9;
                WidgetServerSettingsOverviewBinding binding;
                WidgetServerSettingsOverviewBinding binding2;
                StatefulViews statefulViews10;
                WidgetServerSettingsOverviewBinding binding3;
                WidgetServerSettingsOverviewBinding binding4;
                StatefulViews statefulViews11;
                WidgetServerSettingsOverviewBinding binding5;
                WidgetServerSettingsOverviewBinding binding6;
                StatefulViews statefulViews12;
                WidgetServerSettingsOverviewBinding binding7;
                StatefulViews statefulViews13;
                WidgetServerSettingsOverviewBinding binding8;
                StatefulViews statefulViews14;
                WidgetServerSettingsOverviewBinding binding9;
                StatefulViews statefulViews15;
                StatefulViews statefulViews16;
                WidgetServerSettingsOverviewBinding binding10;
                StatefulViews statefulViews17;
                WidgetServerSettingsOverviewBinding binding11;
                StatefulViews statefulViews18;
                WidgetServerSettingsOverviewBinding binding12;
                StatefulViews statefulViews19;
                WidgetServerSettingsOverviewBinding binding13;
                statefulViews9 = WidgetServerSettingsOverview.this.state;
                binding = WidgetServerSettingsOverview.this.getBinding();
                CheckedSetting checkedSetting7 = binding.g.d;
                m.checkNotNullExpressionValue(checkedSetting7, "binding.systemChannel.systemChannelJoin");
                int id3 = checkedSetting7.getId();
                binding2 = WidgetServerSettingsOverview.this.getBinding();
                CheckedSetting checkedSetting8 = binding2.g.d;
                m.checkNotNullExpressionValue(checkedSetting8, "binding.systemChannel.systemChannelJoin");
                int i = !((Boolean) statefulViews9.get(id3, Boolean.valueOf(checkedSetting8.isChecked()))).booleanValue() ? 1 : 0;
                statefulViews10 = WidgetServerSettingsOverview.this.state;
                binding3 = WidgetServerSettingsOverview.this.getBinding();
                CheckedSetting checkedSetting9 = binding3.g.e;
                m.checkNotNullExpressionValue(checkedSetting9, "binding.systemChannel.systemChannelJoinReplies");
                int id4 = checkedSetting9.getId();
                binding4 = WidgetServerSettingsOverview.this.getBinding();
                CheckedSetting checkedSetting10 = binding4.g.e;
                m.checkNotNullExpressionValue(checkedSetting10, "binding.systemChannel.systemChannelJoinReplies");
                if (!((Boolean) statefulViews10.get(id4, Boolean.valueOf(checkedSetting10.isChecked()))).booleanValue()) {
                    i |= 8;
                }
                statefulViews11 = WidgetServerSettingsOverview.this.state;
                binding5 = WidgetServerSettingsOverview.this.getBinding();
                CheckedSetting checkedSetting11 = binding5.g.c;
                m.checkNotNullExpressionValue(checkedSetting11, "binding.systemChannel.systemChannelBoost");
                int id5 = checkedSetting11.getId();
                binding6 = WidgetServerSettingsOverview.this.getBinding();
                CheckedSetting checkedSetting12 = binding6.g.c;
                m.checkNotNullExpressionValue(checkedSetting12, "binding.systemChannel.systemChannelBoost");
                if (!((Boolean) statefulViews11.get(id5, Boolean.valueOf(checkedSetting12.isChecked()))).booleanValue()) {
                    i |= 2;
                }
                RestAPI apiSerializeNulls = RestAPI.Companion.getApiSerializeNulls();
                long id6 = model.getGuild().getId();
                statefulViews12 = WidgetServerSettingsOverview.this.state;
                binding7 = WidgetServerSettingsOverview.this.getBinding();
                TextView textView6 = binding7.f2556b.f208b;
                m.checkNotNullExpressionValue(textView6, "binding.afk.afkChannel");
                Channel channel = (Channel) statefulViews12.get(textView6.getId(), model.getAfkChannelModel());
                Long valueOf = channel != null ? Long.valueOf(channel.h()) : null;
                statefulViews13 = WidgetServerSettingsOverview.this.state;
                binding8 = WidgetServerSettingsOverview.this.getBinding();
                LinearLayout linearLayout4 = binding8.f2556b.e;
                m.checkNotNullExpressionValue(linearLayout4, "binding.afk.afkTimeoutWrap");
                Integer num = (Integer) statefulViews13.get(linearLayout4.getId(), Integer.valueOf(model.getGuild().getAfkTimeout()));
                statefulViews14 = WidgetServerSettingsOverview.this.state;
                binding9 = WidgetServerSettingsOverview.this.getBinding();
                TextView textView7 = binding9.g.f123b;
                m.checkNotNullExpressionValue(textView7, "binding.systemChannel.systemChannel");
                Channel channel2 = (Channel) statefulViews14.get(textView7.getId(), model.getSystemChannelModel());
                Long valueOf2 = channel2 != null ? Long.valueOf(channel2.h()) : null;
                statefulViews15 = WidgetServerSettingsOverview.this.state;
                Integer num2 = (Integer) statefulViews15.get(90001, Integer.valueOf(model.getGuild().getDefaultMessageNotifications()));
                statefulViews16 = WidgetServerSettingsOverview.this.state;
                binding10 = WidgetServerSettingsOverview.this.getBinding();
                SimpleDraweeView simpleDraweeView = binding10.c.d;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.header.serverSettingsOverviewIcon");
                String str = (String) statefulViews16.get(simpleDraweeView.getId(), IconUtils.getForGuild$default(model.getGuild(), null, false, null, 14, null));
                statefulViews17 = WidgetServerSettingsOverview.this.state;
                binding11 = WidgetServerSettingsOverview.this.getBinding();
                TextInputLayout textInputLayout3 = binding11.c.c;
                m.checkNotNullExpressionValue(textInputLayout3, "binding.header.overviewName");
                String str2 = (String) statefulViews17.get(textInputLayout3.getId(), model.getGuild().getName());
                GuildVerificationLevel verificationLevel = model.getGuild().getVerificationLevel();
                GuildExplicitContentFilter explicitContentFilter = model.getGuild().getExplicitContentFilter();
                statefulViews18 = WidgetServerSettingsOverview.this.state;
                binding12 = WidgetServerSettingsOverview.this.getBinding();
                SimpleDraweeView simpleDraweeView2 = binding12.i.e;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.uploadSplash.uploadSplash");
                statefulViews19 = WidgetServerSettingsOverview.this.state;
                binding13 = WidgetServerSettingsOverview.this.getBinding();
                SimpleDraweeView simpleDraweeView3 = binding13.h.e;
                m.checkNotNullExpressionValue(simpleDraweeView3, "binding.uploadBanner.uploadBanner");
                Observable Y = ObservableExtensionsKt.restSubscribeOn$default(apiSerializeNulls.updateGuild(id6, new RestAPIParams.UpdateGuild(valueOf, num, valueOf2, num2, str, str2, verificationLevel, explicitContentFilter, (String) statefulViews18.get(simpleDraweeView2.getId(), model.getGuild().getSplash()), (String) statefulViews19.get(simpleDraweeView3.getId(), model.getGuild().getBanner()), Integer.valueOf(i), u.toList(model.getGuild().getFeatures()), model.getGuild().getRulesChannelId(), model.getGuild().getPublicUpdatesChannelId(), model.getGuild().getPreferredLocale())), false, 1, null).Y(AnonymousClass1.INSTANCE).Y(AnonymousClass2.INSTANCE);
                m.checkNotNullExpressionValue(Y, "RestAPI\n          .apiSe…atedGuild.id)\n          }");
                ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout$default(Y, 0L, false, 3, null), WidgetServerSettingsOverview.this, null, 2, null).k(b.a.d.o.i(new Action1<WidgetServerSettingsOverview.Model>() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$configureUI$7.3
                    public final void call(WidgetServerSettingsOverview.Model model2) {
                        WidgetServerSettingsOverview.this.configureUpdatedGuild(model2);
                    }
                }, WidgetServerSettingsOverview.this));
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUpdatedGuild(Model model) {
        StatefulViews.clear$default(this.state, false, 1, null);
        AppFragment.hideKeyboard$default(this, null, 1, null);
        getBinding().f.fullScroll(33);
        configureUI(model);
        b.a.d.m.i(this, R.string.server_settings_updated, 0, 4);
    }

    public static final void create(Context context, long j, boolean z2) {
        Companion.create(context, j, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsOverviewBinding getBinding() {
        return (WidgetServerSettingsOverviewBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void openAvatarPicker(final String str, final String str2) {
        this.hasOpenedAvatarPicker = true;
        this.imageSelectedResult = new Action1<String>() { // from class: com.discord.widgets.servers.WidgetServerSettingsOverview$openAvatarPicker$1
            public final void call(String str3) {
                WidgetServerSettingsOverview.this.configureIcon(str, str2, str3, true);
            }
        };
        openMediaChooser();
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onImageChosen(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageChosen(uri, str);
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        MGImages.prepareImageUpload(uri, str, parentFragmentManager, this, this.imageSelectedResult, ImageUploadDialog.PreviewType.GUILD_AVATAR);
    }

    @Override // com.discord.app.AppFragment
    public void onImageCropped(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageCropped(uri, str);
        MGImages.requestDataUrl(getContext(), uri, str, this.imageSelectedResult);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setRetainInstance(true);
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews = this.state;
        FloatingActionButton floatingActionButton = getBinding().e;
        TextInputLayout textInputLayout = getBinding().c.c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.header.overviewName");
        TextView textView = getBinding().f2556b.d;
        m.checkNotNullExpressionValue(textView, "binding.afk.afkTimeout");
        statefulViews.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout, textView);
        StatefulViews statefulViews2 = this.state;
        TextView textView2 = getBinding().f2556b.f208b;
        m.checkNotNullExpressionValue(textView2, "binding.afk.afkChannel");
        TextView textView3 = getBinding().g.f123b;
        m.checkNotNullExpressionValue(textView3, "binding.systemChannel.systemChannel");
        statefulViews2.addOptionalFields(textView2, textView3);
        TextView textView4 = getBinding().c.f110b;
        m.checkNotNullExpressionValue(textView4, "binding.header.iconLabel");
        b.a.k.b.m(textView4, R.string.minimum_size, new Object[]{"128", "128"}, (r4 & 4) != 0 ? b.g.j : null);
        LinkifiedTextView linkifiedTextView = getBinding().i.f;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.uploadSplash.uploadSplashLearnMore");
        f fVar = f.a;
        b.a.k.b.m(linkifiedTextView, R.string.guild_settings_splash_recommend, new Object[]{fVar.a(4415841146391L, null)}, (r4 & 4) != 0 ? b.g.j : null);
        LinkifiedTextView linkifiedTextView2 = getBinding().h.g;
        m.checkNotNullExpressionValue(linkifiedTextView2, "binding.uploadBanner.uploadBannerLearnMore");
        b.a.k.b.m(linkifiedTextView2, R.string.guild_settings_banner_recommend, new Object[]{fVar.a(360028716472L, null)}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().e.hide();
        WidgetChannelSelector.Companion companion = WidgetChannelSelector.Companion;
        companion.registerForResult(this, REQUEST_KEY_AFK_CHANNEL, true, new WidgetServerSettingsOverview$onViewBound$1(this));
        companion.registerForResult(this, REQUEST_KEY_SYSTEM_CHANNEL, true, new WidgetServerSettingsOverview$onViewBound$2(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        setActionBarTitle(R.string.overview);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getMostRecentIntent().getLongExtra("INTENT_EXTRA_GUILD_ID", -1L)), this, null, 2, null), WidgetServerSettingsOverview.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsOverview$onViewBoundOrOnResume$1(this));
    }
}
