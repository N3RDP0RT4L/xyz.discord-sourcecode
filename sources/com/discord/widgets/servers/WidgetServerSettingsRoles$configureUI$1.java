package com.discord.widgets.servers;

import com.discord.api.role.GuildRole;
import com.discord.widgets.servers.WidgetServerSettingsRoles;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetServerSettingsRoles.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/role/GuildRole;", "<name for destructuring parameter 0>", "", "invoke", "(Lcom/discord/api/role/GuildRole;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsRoles$configureUI$1 extends o implements Function1<GuildRole, Unit> {
    public final /* synthetic */ WidgetServerSettingsRoles.Model $data;
    public final /* synthetic */ WidgetServerSettingsRoles this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsRoles$configureUI$1(WidgetServerSettingsRoles widgetServerSettingsRoles, WidgetServerSettingsRoles.Model model) {
        super(1);
        this.this$0 = widgetServerSettingsRoles;
        this.$data = model;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GuildRole guildRole) {
        invoke2(guildRole);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GuildRole guildRole) {
        m.checkNotNullParameter(guildRole, "<name for destructuring parameter 0>");
        WidgetServerSettingsEditRole.Companion.launch(this.$data.getGuildId(), guildRole.a(), this.this$0.requireContext());
    }
}
