package com.discord.widgets.servers;

import androidx.core.app.NotificationCompat;
import kotlin.Metadata;
import rx.functions.Action1;
/* compiled from: WidgetServerSettingsEditRole.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEditRole$patchRole$1<T> implements Action1<Void> {
    public static final WidgetServerSettingsEditRole$patchRole$1 INSTANCE = new WidgetServerSettingsEditRole$patchRole$1();

    public final void call(Void r1) {
    }
}
