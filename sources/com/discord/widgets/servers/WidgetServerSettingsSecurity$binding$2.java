package com.discord.widgets.servers;

import android.view.View;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetServerSettingsSecurityBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsSecurity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetServerSettingsSecurityBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsSecurityBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettingsSecurity$binding$2 extends k implements Function1<View, WidgetServerSettingsSecurityBinding> {
    public static final WidgetServerSettingsSecurity$binding$2 INSTANCE = new WidgetServerSettingsSecurity$binding$2();

    public WidgetServerSettingsSecurity$binding$2() {
        super(1, WidgetServerSettingsSecurityBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsSecurityBinding;", 0);
    }

    public final WidgetServerSettingsSecurityBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.mfa_description_text;
        TextView textView = (TextView) view.findViewById(R.id.mfa_description_text);
        if (textView != null) {
            i = R.id.server_settings_security_label;
            TextView textView2 = (TextView) view.findViewById(R.id.server_settings_security_label);
            if (textView2 != null) {
                i = R.id.server_settings_security_toggle_mfa_button;
                MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.server_settings_security_toggle_mfa_button);
                if (materialButton != null) {
                    return new WidgetServerSettingsSecurityBinding((CoordinatorLayout) view, textView, textView2, materialButton);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
