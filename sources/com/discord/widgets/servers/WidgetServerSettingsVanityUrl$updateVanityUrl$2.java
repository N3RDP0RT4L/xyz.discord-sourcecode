package com.discord.widgets.servers;

import android.widget.TextView;
import com.discord.databinding.WidgetServerSettingsVanityUrlBinding;
import com.discord.utilities.error.Error;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetServerSettingsVanityUrl.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/error/Error;", "error", "", "invoke", "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsVanityUrl$updateVanityUrl$2 extends o implements Function1<Error, Unit> {
    public final /* synthetic */ WidgetServerSettingsVanityUrl this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsVanityUrl$updateVanityUrl$2(WidgetServerSettingsVanityUrl widgetServerSettingsVanityUrl) {
        super(1);
        this.this$0 = widgetServerSettingsVanityUrl;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Error error) {
        invoke2(error);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Error error) {
        WidgetServerSettingsVanityUrlBinding binding;
        m.checkNotNullParameter(error, "error");
        this.this$0.showLoadingUI(false);
        Error.Response response = error.getResponse();
        m.checkNotNullExpressionValue(response, "error.response");
        if (response.getCode() == 50020) {
            error.setShowErrorToasts(false);
            binding = this.this$0.getBinding();
            TextView textView = binding.d;
            m.checkNotNullExpressionValue(textView, "binding.serverSettingsVanityUrlErrorText");
            textView.setVisibility(0);
        }
    }
}
