package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsEmojisBinding;
import com.discord.databinding.WidgetServerSettingsEmojisHeaderBinding;
import com.discord.databinding.WidgetServerSettingsEmojisItemBinding;
import com.discord.databinding.WidgetServerSettingsEmojisSectionBinding;
import com.discord.dialogs.ImageUploadDialog;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.domain.emoji.ModelEmojiGuild;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.CoreUser;
import com.discord.models.user.User;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreEmojiGuild;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.premium.PremiumUtils;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.view.extensions.ImageViewExtensionsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.WidgetServerSettingsEmojis;
import com.discord.widgets.servers.WidgetServerSettingsEmojisEdit;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.g0.t;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsEmojis.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 72\u00020\u0001:\u0004879:B\u0007¢\u0006\u0004\b6\u0010\u0019J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001a\u0010\u0019J\u001f\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\rH\u0016¢\u0006\u0004\b\u001e\u0010\u001fJ\u001f\u0010 \u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\rH\u0016¢\u0006\u0004\b \u0010\u001fR\u001d\u0010&\u001a\u00020!8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R\u001e\u0010(\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010'8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010)R\u0016\u0010+\u001a\u00020*8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b+\u0010,R\u001a\u0010/\u001a\u00060-j\u0002`.8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u00100R\u001c\u00102\u001a\u0002018\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105¨\u0006;"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model;)V", "Landroid/view/View;", "v", "Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "emoji", "launchEditScreen", "(Landroid/view/View;Lcom/discord/models/domain/emoji/ModelEmojiGuild;)V", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "imageBase64", "uploadEmoji", "(Ljava/lang/String;Ljava/lang/String;)V", "guildName", "configureToolbar", "(Ljava/lang/String;)V", "view", "onViewBound", "(Landroid/view/View;)V", "onPause", "()V", "onViewBoundOrOnResume", "Landroid/net/Uri;", NotificationCompat.MessagingStyle.Message.KEY_DATA_URI, "mimeType", "onImageChosen", "(Landroid/net/Uri;Ljava/lang/String;)V", "onImageCropped", "Lcom/discord/databinding/WidgetServerSettingsEmojisBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsEmojisBinding;", "binding", "Lrx/functions/Action1;", "uploadEmojiAction", "Lrx/functions/Action1;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;", "adapter", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", HookHelper.constructorName, "Companion", "Adapter", "Item", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEmojis extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsEmojis.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsEmojisBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final int EMOJI_MAX_FILESIZE_KB = 256;
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final int VIEW_INDEX_CONTENT = 1;
    private Adapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsEmojis$binding$2.INSTANCE, null, 2, null);
    private long guildId = -1;
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettingsEmojis$loggingConfig$1.INSTANCE, 3);
    private Action1<String> uploadEmojiAction;

    /* compiled from: WidgetServerSettingsEmojis.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u001f !\"B\u000f\u0012\u0006\u0010\u001c\u001a\u00020\u001b¢\u0006\u0004\b\u001d\u0010\u001eJ+\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR(\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R4\u0010\u0015\u001a\u0014\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u000b0\u00128\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a¨\u0006#"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lkotlin/Function0;", "", "onUploadEmoji", "Lkotlin/jvm/functions/Function0;", "getOnUploadEmoji", "()Lkotlin/jvm/functions/Function0;", "setOnUploadEmoji", "(Lkotlin/jvm/functions/Function0;)V", "Lkotlin/Function2;", "Landroid/view/View;", "Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "onEmojiItemClicked", "Lkotlin/jvm/functions/Function2;", "getOnEmojiItemClicked", "()Lkotlin/jvm/functions/Function2;", "setOnEmojiItemClicked", "(Lkotlin/jvm/functions/Function2;)V", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;)V", "EmojiEmptyViewHolder", "EmojiHeaderViewHolder", "EmojiItemViewHolder", "EmojiSectionViewHolder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Adapter extends MGRecyclerAdapterSimple<Item> {
        private Function0<Unit> onUploadEmoji = WidgetServerSettingsEmojis$Adapter$onUploadEmoji$1.INSTANCE;
        private Function2<? super View, ? super ModelEmojiGuild, Unit> onEmojiItemClicked = WidgetServerSettingsEmojis$Adapter$onEmojiItemClicked$1.INSTANCE;

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter$EmojiEmptyViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;)V", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiEmptyViewHolder extends MGRecyclerViewHolder<Adapter, Item> {
            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmojiEmptyViewHolder(Adapter adapter) {
                super((int) R.layout.widget_server_settings_emojis_empty, adapter);
                m.checkNotNullParameter(adapter, "adapter");
            }

            public void onConfigure(int i, Item item) {
                m.checkNotNullParameter(item, "data");
            }
        }

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter$EmojiHeaderViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;)V", "Lcom/discord/databinding/WidgetServerSettingsEmojisHeaderBinding;", "binding", "Lcom/discord/databinding/WidgetServerSettingsEmojisHeaderBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiHeaderViewHolder extends MGRecyclerViewHolder<Adapter, Item> {
            private final WidgetServerSettingsEmojisHeaderBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmojiHeaderViewHolder(Adapter adapter) {
                super((int) R.layout.widget_server_settings_emojis_header, adapter);
                m.checkNotNullParameter(adapter, "adapter");
                View view = this.itemView;
                int i = R.id.widget_server_settings_emojis_upload;
                MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.widget_server_settings_emojis_upload);
                if (materialButton != null) {
                    i = R.id.widget_server_settings_emojis_upload_description;
                    TextView textView = (TextView) view.findViewById(R.id.widget_server_settings_emojis_upload_description);
                    if (textView != null) {
                        WidgetServerSettingsEmojisHeaderBinding widgetServerSettingsEmojisHeaderBinding = new WidgetServerSettingsEmojisHeaderBinding((LinearLayout) view, materialButton, textView);
                        m.checkNotNullExpressionValue(widgetServerSettingsEmojisHeaderBinding, "WidgetServerSettingsEmoj…derBinding.bind(itemView)");
                        this.binding = widgetServerSettingsEmojisHeaderBinding;
                        return;
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
            }

            public static final /* synthetic */ Adapter access$getAdapter$p(EmojiHeaderViewHolder emojiHeaderViewHolder) {
                return (Adapter) emojiHeaderViewHolder.adapter;
            }

            public void onConfigure(int i, Item item) {
                CharSequence b2;
                m.checkNotNullParameter(item, "data");
                TextView textView = this.binding.c;
                m.checkNotNullExpressionValue(textView, "binding.widgetServerSett…gsEmojisUploadDescription");
                b2 = b.b(a.x(this.itemView, "itemView", "itemView.context"), R.string.guild_settings_emoji_upload_to_server_message, new Object[]{String.valueOf(((Item.EmojiHeader) item).getEmojiMax()), String.valueOf(256)}, (r4 & 4) != 0 ? b.C0034b.j : null);
                textView.setText(b2);
                this.binding.f2537b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEmojis$Adapter$EmojiHeaderViewHolder$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetServerSettingsEmojis.Adapter.EmojiHeaderViewHolder.access$getAdapter$p(WidgetServerSettingsEmojis.Adapter.EmojiHeaderViewHolder.this).getOnUploadEmoji().invoke();
                    }
                });
            }
        }

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter$EmojiItemViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;)V", "Lcom/discord/databinding/WidgetServerSettingsEmojisItemBinding;", "binding", "Lcom/discord/databinding/WidgetServerSettingsEmojisItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiItemViewHolder extends MGRecyclerViewHolder<Adapter, Item> {
            private final WidgetServerSettingsEmojisItemBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmojiItemViewHolder(Adapter adapter) {
                super((int) R.layout.widget_server_settings_emojis_item, adapter);
                m.checkNotNullParameter(adapter, "adapter");
                View view = this.itemView;
                int i = R.id.server_settings_emojis_avatar;
                SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.server_settings_emojis_avatar);
                if (simpleDraweeView != null) {
                    ConstraintLayout constraintLayout = (ConstraintLayout) view;
                    i = R.id.server_settings_emojis_name;
                    TextView textView = (TextView) view.findViewById(R.id.server_settings_emojis_name);
                    if (textView != null) {
                        i = R.id.server_settings_emojis_nickname;
                        TextView textView2 = (TextView) view.findViewById(R.id.server_settings_emojis_nickname);
                        if (textView2 != null) {
                            i = R.id.server_settings_emojis_overflow;
                            ImageView imageView = (ImageView) view.findViewById(R.id.server_settings_emojis_overflow);
                            if (imageView != null) {
                                i = R.id.server_settings_emojis_username;
                                TextView textView3 = (TextView) view.findViewById(R.id.server_settings_emojis_username);
                                if (textView3 != null) {
                                    i = R.id.server_settings_emojis_username_avatar;
                                    SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) view.findViewById(R.id.server_settings_emojis_username_avatar);
                                    if (simpleDraweeView2 != null) {
                                        WidgetServerSettingsEmojisItemBinding widgetServerSettingsEmojisItemBinding = new WidgetServerSettingsEmojisItemBinding(constraintLayout, simpleDraweeView, constraintLayout, textView, textView2, imageView, textView3, simpleDraweeView2);
                                        m.checkNotNullExpressionValue(widgetServerSettingsEmojisItemBinding, "WidgetServerSettingsEmoj…temBinding.bind(itemView)");
                                        this.binding = widgetServerSettingsEmojisItemBinding;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
            }

            public static final /* synthetic */ Adapter access$getAdapter$p(EmojiItemViewHolder emojiItemViewHolder) {
                return (Adapter) emojiItemViewHolder.adapter;
            }

            public void onConfigure(int i, final Item item) {
                m.checkNotNullParameter(item, "data");
                super.onConfigure(i, (int) item);
                Item.EmojiItem emojiItem = (Item.EmojiItem) item;
                TextView textView = this.binding.d;
                m.checkNotNullExpressionValue(textView, "binding.serverSettingsEmojisName");
                textView.setText(emojiItem.getEmoji().getName());
                TextView textView2 = this.binding.f;
                m.checkNotNullExpressionValue(textView2, "binding.serverSettingsEmojisUsername");
                textView2.setText(UserUtils.getUserNameWithDiscriminator$default(UserUtils.INSTANCE, emojiItem.getUser(), null, null, 3, null));
                GuildMember guildMember = emojiItem.getGuildMember();
                String nick = guildMember != null ? guildMember.getNick() : null;
                TextView textView3 = this.binding.e;
                m.checkNotNullExpressionValue(textView3, "binding.serverSettingsEmojisNickname");
                ViewExtensions.setTextAndVisibilityBy(textView3, nick);
                SimpleDraweeView simpleDraweeView = this.binding.g;
                m.checkNotNullExpressionValue(simpleDraweeView, "binding.serverSettingsEmojisUsernameAvatar");
                IconUtils.setIcon$default(simpleDraweeView, new CoreUser(emojiItem.getEmoji().getUser()), R.dimen.avatar_size_standard, null, null, guildMember, 24, null);
                String imageUri = ModelEmojiCustom.getImageUri(emojiItem.getEmoji().getId(), emojiItem.getEmoji().getAnimated(), 64);
                SimpleDraweeView simpleDraweeView2 = this.binding.f2538b;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.serverSettingsEmojisAvatar");
                MGImages.setImage$default(simpleDraweeView2, imageUri, R.dimen.emoji_size, R.dimen.emoji_size, true, null, null, 96, null);
                SimpleDraweeView simpleDraweeView3 = this.binding.f2538b;
                m.checkNotNullExpressionValue(simpleDraweeView3, "binding.serverSettingsEmojisAvatar");
                ImageViewExtensionsKt.setGrayscale(simpleDraweeView3, !emojiItem.getEmoji().getAvailable());
                SimpleDraweeView simpleDraweeView4 = this.binding.f2538b;
                m.checkNotNullExpressionValue(simpleDraweeView4, "binding.serverSettingsEmojisAvatar");
                simpleDraweeView4.setImageAlpha(emojiItem.getEmoji().getAvailable() ? 255 : 100);
                this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEmojis$Adapter$EmojiItemViewHolder$onConfigure$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        Function2<View, ModelEmojiGuild, Unit> onEmojiItemClicked = WidgetServerSettingsEmojis.Adapter.EmojiItemViewHolder.access$getAdapter$p(WidgetServerSettingsEmojis.Adapter.EmojiItemViewHolder.this).getOnEmojiItemClicked();
                        m.checkNotNullExpressionValue(view, "it");
                        onEmojiItemClicked.invoke(view, ((WidgetServerSettingsEmojis.Item.EmojiItem) item).getEmoji());
                    }
                });
            }
        }

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter$EmojiSectionViewHolder;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;)V", "Lcom/discord/databinding/WidgetServerSettingsEmojisSectionBinding;", "binding", "Lcom/discord/databinding/WidgetServerSettingsEmojisSectionBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiSectionViewHolder extends MGRecyclerViewHolder<Adapter, Item> {
            private final WidgetServerSettingsEmojisSectionBinding binding;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmojiSectionViewHolder(Adapter adapter) {
                super((int) R.layout.widget_server_settings_emojis_section, adapter);
                m.checkNotNullParameter(adapter, "adapter");
                View view = this.itemView;
                TextView textView = (TextView) view.findViewById(R.id.widget_server_settings_emojis_section);
                if (textView != null) {
                    WidgetServerSettingsEmojisSectionBinding widgetServerSettingsEmojisSectionBinding = new WidgetServerSettingsEmojisSectionBinding((LinearLayout) view, textView);
                    m.checkNotNullExpressionValue(widgetServerSettingsEmojisSectionBinding, "WidgetServerSettingsEmoj…ionBinding.bind(itemView)");
                    this.binding = widgetServerSettingsEmojisSectionBinding;
                    return;
                }
                throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.widget_server_settings_emojis_section)));
            }

            public void onConfigure(int i, Item item) {
                CharSequence b2;
                CharSequence b3;
                CharSequence b4;
                m.checkNotNullParameter(item, "data");
                super.onConfigure(i, (int) item);
                Item.EmojiSection emojiSection = (Item.EmojiSection) item;
                int emojiMax = emojiSection.getEmojiMax() - emojiSection.getEmojiLength();
                View view = this.itemView;
                m.checkNotNullExpressionValue(view, "itemView");
                Context context = view.getContext();
                m.checkNotNullExpressionValue(context, "itemView.context");
                Resources resources = context.getResources();
                m.checkNotNullExpressionValue(resources, "itemView.context.resources");
                b2 = b.b(a.x(this.itemView, "itemView", "itemView.context"), R.string.emoji_slots_available, new Object[]{StringResourceUtilsKt.getQuantityString(resources, a.x(this.itemView, "itemView", "itemView.context"), (int) R.plurals.emoji_slots_available_count, emojiMax, Integer.valueOf(emojiMax))}, (r4 & 4) != 0 ? b.C0034b.j : null);
                TextView textView = this.binding.f2539b;
                m.checkNotNullExpressionValue(textView, "binding.widgetServerSettingsEmojisSection");
                Context x2 = a.x(this.itemView, "itemView", "itemView.context");
                b3 = b.b(a.x(this.itemView, "itemView", "itemView.context"), emojiSection.getTitleId(), new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
                b4 = b.b(x2, R.string.emoji_section, new Object[]{b3, b2}, (r4 & 4) != 0 ? b.C0034b.j : null);
                textView.setText(b4);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public Adapter(RecyclerView recyclerView) {
            super(recyclerView, false, 2, null);
            m.checkNotNullParameter(recyclerView, "recycler");
        }

        public final Function2<View, ModelEmojiGuild, Unit> getOnEmojiItemClicked() {
            return this.onEmojiItemClicked;
        }

        public final Function0<Unit> getOnUploadEmoji() {
            return this.onUploadEmoji;
        }

        public final void setOnEmojiItemClicked(Function2<? super View, ? super ModelEmojiGuild, Unit> function2) {
            m.checkNotNullParameter(function2, "<set-?>");
            this.onEmojiItemClicked = function2;
        }

        public final void setOnUploadEmoji(Function0<Unit> function0) {
            m.checkNotNullParameter(function0, "<set-?>");
            this.onUploadEmoji = function0;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public MGRecyclerViewHolder<Adapter, Item> onCreateViewHolder(ViewGroup viewGroup, int i) {
            m.checkNotNullParameter(viewGroup, "parent");
            if (i == 0) {
                return new EmojiItemViewHolder(this);
            }
            if (i == 1) {
                return new EmojiSectionViewHolder(this);
            }
            if (i == 2) {
                return new EmojiHeaderViewHolder(this);
            }
            if (i == 3) {
                return new EmojiEmptyViewHolder(this);
            }
            throw invalidViewTypeException(i);
        }
    }

    /* compiled from: WidgetServerSettingsEmojis.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012J!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\f¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "create", "(Landroid/content/Context;J)V", "", "EMOJI_MAX_FILESIZE_KB", "I", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", "VIEW_INDEX_CONTENT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            StoreStream.Companion.getAnalytics().onGuildSettingsPaneViewed("EMOJIS", j);
            j.d(context, WidgetServerSettingsEmojis.class, new Intent().putExtra("INTENT_EXTRA_GUILD_ID", j));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsEmojis.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00042\u00020\u0001:\u0005\u0004\u0005\u0006\u0007\bB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0004\t\n\u000b\f¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", HookHelper.constructorName, "()V", "Companion", "EmojiEmpty", "EmojiHeader", "EmojiItem", "EmojiSection", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiItem;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiSection;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiHeader;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiEmpty;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Item implements MGRecyclerDataPayload {
        public static final Companion Companion = new Companion(null);
        public static final int TYPE_EMOJI = 0;
        public static final int TYPE_EMPTY = 3;
        public static final int TYPE_HEADER = 2;
        public static final int TYPE_SECTION = 1;

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004¨\u0006\n"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$Companion;", "", "", "TYPE_EMOJI", "I", "TYPE_EMPTY", "TYPE_HEADER", "TYPE_SECTION", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\b\u001a\u00020\u00078\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiEmpty;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "", "type", "I", "getType", "()I", "", "key", "Ljava/lang/String;", "getKey", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiEmpty extends Item {
            public static final EmojiEmpty INSTANCE = new EmojiEmpty();
            private static final int type = 3;
            private static final String key = "EMOJI_EMPTY";

            private EmojiEmpty() {
                super(null);
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return type;
            }
        }

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004R\u001c\u0010\u0014\u001a\u00020\b8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\nR\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiHeader;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "", "component1", "()I", "emojiMax", "copy", "(I)Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiHeader;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "getEmojiMax", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiHeader extends Item {
            private final int emojiMax;
            private final int type = 2;
            private final String key = "EMOJI_HEADER";

            public EmojiHeader(int i) {
                super(null);
                this.emojiMax = i;
            }

            public static /* synthetic */ EmojiHeader copy$default(EmojiHeader emojiHeader, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = emojiHeader.emojiMax;
                }
                return emojiHeader.copy(i);
            }

            public final int component1() {
                return this.emojiMax;
            }

            public final EmojiHeader copy(int i) {
                return new EmojiHeader(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof EmojiHeader) && this.emojiMax == ((EmojiHeader) obj).emojiMax;
                }
                return true;
            }

            public final int getEmojiMax() {
                return this.emojiMax;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                return this.emojiMax;
            }

            public String toString() {
                return a.A(a.R("EmojiHeader(emojiMax="), this.emojiMax, ")");
            }
        }

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\b\u0018\u0000 )2\u00020\u0001:\u0001)B!\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ0\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0019\u001a\u00020\u00182\b\u0010\u0017\u001a\u0004\u0018\u00010\u0016HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u00020\u00138\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u0015R\u001c\u0010\u001e\u001a\u00020\u00108\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b \u0010\u0012R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010!\u001a\u0004\b\"\u0010\u0004R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010#\u001a\u0004\b$\u0010\u0007R\u001b\u0010\r\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010%\u001a\u0004\b&\u0010\n¨\u0006*"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiItem;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "component1", "()Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "Lcom/discord/models/user/User;", "component2", "()Lcom/discord/models/user/User;", "Lcom/discord/models/member/GuildMember;", "component3", "()Lcom/discord/models/member/GuildMember;", "emoji", "user", "guildMember", "copy", "(Lcom/discord/models/domain/emoji/ModelEmojiGuild;Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiItem;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "getEmoji", "Lcom/discord/models/user/User;", "getUser", "Lcom/discord/models/member/GuildMember;", "getGuildMember", HookHelper.constructorName, "(Lcom/discord/models/domain/emoji/ModelEmojiGuild;Lcom/discord/models/user/User;Lcom/discord/models/member/GuildMember;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiItem extends Item {
            public static final Companion Companion = new Companion(null);
            private final ModelEmojiGuild emoji;
            private final GuildMember guildMember;
            private final String key;
            private final int type;
            private final User user;

            /* compiled from: WidgetServerSettingsEmojis.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ-\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0016\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u00070\u0004¢\u0006\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiItem$Companion;", "", "Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "emoji", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "guildMembers", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiItem;", "from", "(Lcom/discord/models/domain/emoji/ModelEmojiGuild;Ljava/util/Map;)Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiItem;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Companion {
                private Companion() {
                }

                public final EmojiItem from(ModelEmojiGuild modelEmojiGuild, Map<Long, GuildMember> map) {
                    m.checkNotNullParameter(modelEmojiGuild, "emoji");
                    m.checkNotNullParameter(map, "guildMembers");
                    CoreUser coreUser = new CoreUser(modelEmojiGuild.getUser());
                    return new EmojiItem(modelEmojiGuild, coreUser, map.get(Long.valueOf(coreUser.getId())));
                }

                public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                    this();
                }
            }

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmojiItem(ModelEmojiGuild modelEmojiGuild, User user, GuildMember guildMember) {
                super(null);
                m.checkNotNullParameter(modelEmojiGuild, "emoji");
                m.checkNotNullParameter(user, "user");
                this.emoji = modelEmojiGuild;
                this.user = user;
                this.guildMember = guildMember;
                this.key = String.valueOf(modelEmojiGuild.getId());
            }

            public static /* synthetic */ EmojiItem copy$default(EmojiItem emojiItem, ModelEmojiGuild modelEmojiGuild, User user, GuildMember guildMember, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelEmojiGuild = emojiItem.emoji;
                }
                if ((i & 2) != 0) {
                    user = emojiItem.user;
                }
                if ((i & 4) != 0) {
                    guildMember = emojiItem.guildMember;
                }
                return emojiItem.copy(modelEmojiGuild, user, guildMember);
            }

            public final ModelEmojiGuild component1() {
                return this.emoji;
            }

            public final User component2() {
                return this.user;
            }

            public final GuildMember component3() {
                return this.guildMember;
            }

            public final EmojiItem copy(ModelEmojiGuild modelEmojiGuild, User user, GuildMember guildMember) {
                m.checkNotNullParameter(modelEmojiGuild, "emoji");
                m.checkNotNullParameter(user, "user");
                return new EmojiItem(modelEmojiGuild, user, guildMember);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EmojiItem)) {
                    return false;
                }
                EmojiItem emojiItem = (EmojiItem) obj;
                return m.areEqual(this.emoji, emojiItem.emoji) && m.areEqual(this.user, emojiItem.user) && m.areEqual(this.guildMember, emojiItem.guildMember);
            }

            public final ModelEmojiGuild getEmoji() {
                return this.emoji;
            }

            public final GuildMember getGuildMember() {
                return this.guildMember;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public final User getUser() {
                return this.user;
            }

            public int hashCode() {
                ModelEmojiGuild modelEmojiGuild = this.emoji;
                int i = 0;
                int hashCode = (modelEmojiGuild != null ? modelEmojiGuild.hashCode() : 0) * 31;
                User user = this.user;
                int hashCode2 = (hashCode + (user != null ? user.hashCode() : 0)) * 31;
                GuildMember guildMember = this.guildMember;
                if (guildMember != null) {
                    i = guildMember.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                StringBuilder R = a.R("EmojiItem(emoji=");
                R.append(this.emoji);
                R.append(", user=");
                R.append(this.user);
                R.append(", guildMember=");
                R.append(this.guildMember);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J.\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u001c\u0010\u0017\u001a\u00020\f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u000eR\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0015\u001a\u0004\b\u001a\u0010\u0004R\u001c\u0010\u001b\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u001b\u0010\u0015\u001a\u0004\b\u001c\u0010\u0004R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u001d\u0010\u0004¨\u0006 "}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiSection;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "", "component1", "()I", "component2", "component3", "emojiMax", "emojiLength", "titleId", "copy", "(III)Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item$EmojiSection;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getTitleId", "key", "Ljava/lang/String;", "getKey", "getEmojiMax", "type", "getType", "getEmojiLength", HookHelper.constructorName, "(III)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiSection extends Item {
            private final int emojiLength;
            private final int emojiMax;
            private final String key;
            private final int titleId;
            private final int type = 1;

            public EmojiSection(int i, int i2, int i3) {
                super(null);
                this.emojiMax = i;
                this.emojiLength = i2;
                this.titleId = i3;
                this.key = String.valueOf(i3);
            }

            public static /* synthetic */ EmojiSection copy$default(EmojiSection emojiSection, int i, int i2, int i3, int i4, Object obj) {
                if ((i4 & 1) != 0) {
                    i = emojiSection.emojiMax;
                }
                if ((i4 & 2) != 0) {
                    i2 = emojiSection.emojiLength;
                }
                if ((i4 & 4) != 0) {
                    i3 = emojiSection.titleId;
                }
                return emojiSection.copy(i, i2, i3);
            }

            public final int component1() {
                return this.emojiMax;
            }

            public final int component2() {
                return this.emojiLength;
            }

            public final int component3() {
                return this.titleId;
            }

            public final EmojiSection copy(int i, int i2, int i3) {
                return new EmojiSection(i, i2, i3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EmojiSection)) {
                    return false;
                }
                EmojiSection emojiSection = (EmojiSection) obj;
                return this.emojiMax == emojiSection.emojiMax && this.emojiLength == emojiSection.emojiLength && this.titleId == emojiSection.titleId;
            }

            public final int getEmojiLength() {
                return this.emojiLength;
            }

            public final int getEmojiMax() {
                return this.emojiMax;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
            public String getKey() {
                return this.key;
            }

            public final int getTitleId() {
                return this.titleId;
            }

            @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
            public int getType() {
                return this.type;
            }

            public int hashCode() {
                return (((this.emojiMax * 31) + this.emojiLength) * 31) + this.titleId;
            }

            public String toString() {
                StringBuilder R = a.R("EmojiSection(emojiMax=");
                R.append(this.emojiMax);
                R.append(", emojiLength=");
                R.append(this.emojiLength);
                R.append(", titleId=");
                return a.A(R, this.titleId, ")");
            }
        }

        private Item() {
        }

        public /* synthetic */ Item(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsEmojis.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0082\b\u0018\u0000 !2\u00020\u0001:\u0002!\"B)\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u000e\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ8\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\u0010\b\u0002\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00052\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0011\u0010\u000bJ\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R!\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\bR\u001b\u0010\u000e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004¨\u0006#"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Item;", "component2", "()Ljava/util/List;", "", "component3", "()Ljava/lang/String;", "guild", "items", "defaultName", "copy", "(Lcom/discord/models/guild/Guild;Ljava/util/List;Ljava/lang/String;)Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", "Ljava/lang/String;", "getDefaultName", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Ljava/util/List;Ljava/lang/String;)V", "Companion", "Permission", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final String defaultName;
        private final Guild guild;
        private final List<Item> items;

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0018\u0010\u0019J#\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u001d\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00052\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\f\u0010\rJ?\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\t2\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u000e2\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\u0012\u0012\u0004\u0012\u00020\u00130\u0011H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J!\u0010\u0017\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0017\u0010\b¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model$Permission;", "canManageEmojisAndStickers", "(J)Lrx/Observable;", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model;", "getGuildEmojis", "(Lcom/discord/models/guild/Guild;)Lrx/Observable;", "", "Lcom/discord/models/domain/emoji/ModelEmojiGuild;", "emojis", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "guildMembers", "create", "(Lcom/discord/models/guild/Guild;Ljava/util/List;Ljava/util/Map;)Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model;", "get", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            private final Observable<Permission> canManageEmojisAndStickers(long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable i = Observable.i(companion.getPermissions().observePermissionsForGuild(j), companion.getGuilds().observeGuild(j), StoreUser.observeMe$default(companion.getUsers(), false, 1, null), WidgetServerSettingsEmojis$Model$Companion$canManageEmojisAndStickers$1.INSTANCE);
                m.checkNotNullExpressionValue(i, "Observable.combineLatest…ld)\n          }\n        }");
                return ObservableExtensionsKt.computationBuffered(i);
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Model create(Guild guild, List<ModelEmojiGuild> list, Map<Long, GuildMember> map) {
                if (list == null) {
                    return new Model(guild, null, null);
                }
                int guildEmojiMaxCount = PremiumUtils.INSTANCE.getGuildEmojiMaxCount(guild);
                Item.EmojiHeader emojiHeader = new Item.EmojiHeader(guildEmojiMaxCount);
                ArrayList<ModelEmojiGuild> arrayList = new ArrayList();
                for (Object obj : list) {
                    if (!((ModelEmojiGuild) obj).getAnimated()) {
                        arrayList.add(obj);
                    }
                }
                ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
                for (ModelEmojiGuild modelEmojiGuild : arrayList) {
                    arrayList2.add(Item.EmojiItem.Companion.from(modelEmojiGuild, map));
                }
                List reversed = u.reversed(arrayList2);
                ArrayList<ModelEmojiGuild> arrayList3 = new ArrayList();
                for (Object obj2 : list) {
                    if (((ModelEmojiGuild) obj2).getAnimated()) {
                        arrayList3.add(obj2);
                    }
                }
                ArrayList arrayList4 = new ArrayList(o.collectionSizeOrDefault(arrayList3, 10));
                for (ModelEmojiGuild modelEmojiGuild2 : arrayList3) {
                    arrayList4.add(Item.EmojiItem.Companion.from(modelEmojiGuild2, map));
                }
                List reversed2 = u.reversed(arrayList4);
                Item.EmojiSection emojiSection = new Item.EmojiSection(guildEmojiMaxCount, reversed.size(), R.string.emoji);
                Item.EmojiSection emojiSection2 = new Item.EmojiSection(guildEmojiMaxCount, reversed2.size(), R.string.animated_emoji);
                ArrayList arrayList5 = new ArrayList(d0.t.m.listOf(emojiHeader));
                if (!reversed.isEmpty()) {
                    arrayList5.addAll(u.plus((Collection) d0.t.m.listOf(emojiSection), (Iterable) reversed));
                }
                if (!reversed2.isEmpty()) {
                    arrayList5.addAll(u.plus((Collection) d0.t.m.listOf(emojiSection2), (Iterable) reversed2));
                }
                if (reversed.isEmpty() && reversed2.isEmpty()) {
                    arrayList5.addAll(d0.t.m.listOf(Item.EmojiEmpty.INSTANCE));
                }
                StringBuilder R = a.R("emoji_");
                R.append(list.size() + 1);
                return new Model(guild, arrayList5, R.toString());
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Observable<Model> getGuildEmojis(Guild guild) {
                StoreStream.Companion companion = StoreStream.Companion;
                final StoreEmojiGuild guildEmojis = companion.getGuildEmojis();
                StoreGuilds guilds = companion.getGuilds();
                final long id2 = guild.getId();
                Observable u = ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{guildEmojis, guilds}, false, null, null, new WidgetServerSettingsEmojis$Model$Companion$getGuildEmojis$1(guild, guildEmojis, guilds), 14, null).u(new Action0() { // from class: com.discord.widgets.servers.WidgetServerSettingsEmojis$Model$Companion$getGuildEmojis$2
                    @Override // rx.functions.Action0
                    public final void call() {
                        StoreEmojiGuild.this.fetchGuildEmoji(id2);
                    }
                });
                m.checkNotNullExpressionValue(u, "ObservationDeckProvider.…etchGuildEmoji(guildId) }");
                return ObservableExtensionsKt.computationBuffered(u);
            }

            public final Observable<Model> get(long j) {
                Observable Y = canManageEmojisAndStickers(j).Y(WidgetServerSettingsEmojis$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(Y, "canManageEmojisAndSticke…          }\n            }");
                return Y;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* compiled from: WidgetServerSettingsEmojis.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0013\u001a\u00020\u00022\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0007¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model$Permission;", "", "", "component1", "()Z", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "canManage", "guild", "copy", "(ZLcom/discord/models/guild/Guild;)Lcom/discord/widgets/servers/WidgetServerSettingsEmojis$Model$Permission;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanManage", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(ZLcom/discord/models/guild/Guild;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Permission {
            private final boolean canManage;
            private final Guild guild;

            public Permission(boolean z2, Guild guild) {
                m.checkNotNullParameter(guild, "guild");
                this.canManage = z2;
                this.guild = guild;
            }

            public static /* synthetic */ Permission copy$default(Permission permission, boolean z2, Guild guild, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = permission.canManage;
                }
                if ((i & 2) != 0) {
                    guild = permission.guild;
                }
                return permission.copy(z2, guild);
            }

            public final boolean component1() {
                return this.canManage;
            }

            public final Guild component2() {
                return this.guild;
            }

            public final Permission copy(boolean z2, Guild guild) {
                m.checkNotNullParameter(guild, "guild");
                return new Permission(z2, guild);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Permission)) {
                    return false;
                }
                Permission permission = (Permission) obj;
                return this.canManage == permission.canManage && m.areEqual(this.guild, permission.guild);
            }

            public final boolean getCanManage() {
                return this.canManage;
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public int hashCode() {
                boolean z2 = this.canManage;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                int i3 = i * 31;
                Guild guild = this.guild;
                return i3 + (guild != null ? guild.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("Permission(canManage=");
                R.append(this.canManage);
                R.append(", guild=");
                R.append(this.guild);
                R.append(")");
                return R.toString();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(Guild guild, List<? extends Item> list, String str) {
            m.checkNotNullParameter(guild, "guild");
            this.guild = guild;
            this.items = list;
            this.defaultName = str;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Model copy$default(Model model, Guild guild, List list, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = model.guild;
            }
            if ((i & 2) != 0) {
                list = model.items;
            }
            if ((i & 4) != 0) {
                str = model.defaultName;
            }
            return model.copy(guild, list, str);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final List<Item> component2() {
            return this.items;
        }

        public final String component3() {
            return this.defaultName;
        }

        public final Model copy(Guild guild, List<? extends Item> list, String str) {
            m.checkNotNullParameter(guild, "guild");
            return new Model(guild, list, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.guild, model.guild) && m.areEqual(this.items, model.items) && m.areEqual(this.defaultName, model.defaultName);
        }

        public final String getDefaultName() {
            return this.defaultName;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final List<Item> getItems() {
            return this.items;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            List<Item> list = this.items;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            String str = this.defaultName;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guild=");
            R.append(this.guild);
            R.append(", items=");
            R.append(this.items);
            R.append(", defaultName=");
            return a.H(R, this.defaultName, ")");
        }
    }

    public WidgetServerSettingsEmojis() {
        super(R.layout.widget_server_settings_emojis);
    }

    private final void configureToolbar(String str) {
        setActionBarTitle(R.string.emoji);
        setActionBarSubtitle(str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        if (model == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.onBackPressed();
                return;
            }
            return;
        }
        configureToolbar(model.getGuild().getName());
        List<Item> items = model.getItems();
        boolean z2 = false;
        if (!(items == null || items.isEmpty())) {
            String defaultName = model.getDefaultName();
            if (defaultName == null || t.isBlank(defaultName)) {
                z2 = true;
            }
            if (!z2) {
                AppViewFlipper appViewFlipper = getBinding().c;
                m.checkNotNullExpressionValue(appViewFlipper, "binding.widgetServerSettingsEmojisViewFlipper");
                appViewFlipper.setDisplayedChild(1);
                this.uploadEmojiAction = new Action1<String>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEmojis$configureUI$1
                    public final void call(String str) {
                        WidgetServerSettingsEmojis widgetServerSettingsEmojis = WidgetServerSettingsEmojis.this;
                        String defaultName2 = model.getDefaultName();
                        m.checkNotNullExpressionValue(str, "dataUrl");
                        widgetServerSettingsEmojis.uploadEmoji(defaultName2, str);
                    }
                };
                Adapter adapter = this.adapter;
                if (adapter == null) {
                    m.throwUninitializedPropertyAccessException("adapter");
                }
                adapter.setData(model.getItems());
                Adapter adapter2 = this.adapter;
                if (adapter2 == null) {
                    m.throwUninitializedPropertyAccessException("adapter");
                }
                adapter2.setOnUploadEmoji(new WidgetServerSettingsEmojis$configureUI$2(this));
                Adapter adapter3 = this.adapter;
                if (adapter3 == null) {
                    m.throwUninitializedPropertyAccessException("adapter");
                }
                adapter3.setOnEmojiItemClicked(new WidgetServerSettingsEmojis$configureUI$3(this));
            }
        }
    }

    private final WidgetServerSettingsEmojisBinding getBinding() {
        return (WidgetServerSettingsEmojisBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void launchEditScreen(View view, ModelEmojiGuild modelEmojiGuild) {
        WidgetServerSettingsEmojisEdit.Companion companion = WidgetServerSettingsEmojisEdit.Companion;
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "v.context");
        companion.create(context, this.guildId, modelEmojiGuild.getId(), modelEmojiGuild.getName());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void uploadEmoji(String str, String str2) {
        ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().postGuildEmoji(this.guildId, new RestAPIParams.PostGuildEmoji(str, str2)), false, 1, null), this, null, 2, null).k(b.a.d.o.i(WidgetServerSettingsEmojis$uploadEmoji$1.INSTANCE, this));
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onImageChosen(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageChosen(uri, str);
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        MGImages.prepareImageUpload(uri, str, parentFragmentManager, this, this.uploadEmojiAction, ImageUploadDialog.PreviewType.EMOJI);
    }

    @Override // com.discord.app.AppFragment
    public void onImageCropped(Uri uri, String str) {
        m.checkNotNullParameter(uri, NotificationCompat.MessagingStyle.Message.KEY_DATA_URI);
        m.checkNotNullParameter(str, "mimeType");
        super.onImageCropped(uri, str);
        MGImages.requestDataUrl(getContext(), uri, str, this.uploadEmojiAction);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        StoreStream.Companion.getGuildEmojis().deactivate();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        this.guildId = getMostRecentIntent().getLongExtra("INTENT_EXTRA_GUILD_ID", -1L);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().f2535b;
        m.checkNotNullExpressionValue(recyclerView, "binding.widgetServerSettingsEmojisRecycler");
        this.adapter = (Adapter) companion.configure(new Adapter(recyclerView));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        StoreStream.Companion.getGuildEmojis().activate(this.guildId);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(this.guildId), this, null, 2, null), WidgetServerSettingsEmojis.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsEmojis$onViewBoundOrOnResume$1(this));
    }
}
