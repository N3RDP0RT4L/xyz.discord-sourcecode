package com.discord.widgets.servers.settings.members;

import android.content.Context;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.api.role.GuildRole;
import com.discord.databinding.WidgetServerSettingsMembersBinding;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.widgets.servers.WidgetServerSettingsEditMember;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\u0010\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0000j\u0002`\u00040\u0003H\n¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"", "Lcom/discord/primitives/UserId;", "userId", "", "Lcom/discord/primitives/RoleId;", "userRoles", "", "invoke", "(JLjava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsMembers$configureUI$2 extends o implements Function2<Long, List<? extends Long>, Unit> {
    public final /* synthetic */ WidgetServerSettingsMembersModel $model;
    public final /* synthetic */ WidgetServerSettingsMembers this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsMembers$configureUI$2(WidgetServerSettingsMembers widgetServerSettingsMembers, WidgetServerSettingsMembersModel widgetServerSettingsMembersModel) {
        super(2);
        this.this$0 = widgetServerSettingsMembers;
        this.$model = widgetServerSettingsMembersModel;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Long l, List<? extends Long> list) {
        invoke(l.longValue(), (List<Long>) list);
        return Unit.a;
    }

    public final void invoke(long j, List<Long> list) {
        WidgetServerSettingsMembersBinding binding;
        m.checkNotNullParameter(list, "userRoles");
        boolean z2 = j == this.$model.getMeUser().getId();
        if (this.$model.getGuild().getOwnerId() != j || z2) {
            GuildRole highestRole = RoleUtils.getHighestRole(this.$model.getRoles(), list);
            if (RoleUtils.rankIsHigher(this.$model.getMyHighestRole(), highestRole) || this.$model.getGuild().isOwner(this.$model.getMeUser().getId()) || z2) {
                WidgetServerSettingsEditMember.Companion companion = WidgetServerSettingsEditMember.Companion;
                long id2 = this.$model.getGuild().getId();
                binding = this.this$0.getBinding();
                m.checkNotNullExpressionValue(binding, "binding");
                CoordinatorLayout coordinatorLayout = binding.a;
                m.checkNotNullExpressionValue(coordinatorLayout, "binding.root");
                Context context = coordinatorLayout.getContext();
                m.checkNotNullExpressionValue(context, "binding.root.context");
                companion.launch(id2, j, context);
            } else if (RoleUtils.rankEquals(this.$model.getMyHighestRole(), highestRole)) {
                b.a.d.m.i(this.this$0, R.string.cannot_manage_same_rank, 0, 4);
            } else {
                b.a.d.m.i(this.this$0, R.string.cannot_manage_higher_rank, 0, 4);
            }
        } else {
            b.a.d.m.i(this.this$0, R.string.cannot_manage_is_owner, 0, 4);
        }
    }
}
