package com.discord.widgets.servers.settings.members;

import com.discord.widgets.servers.settings.members.WidgetServerSettingsMembersModel;
import d0.g0.t;
import d0.z.d.m;
import java.util.Comparator;
import kotlin.Metadata;
/* compiled from: WidgetServerSettingsMembersModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel$MemberItem;", "member1", "member2", "", "compare", "(Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel$MemberItem;Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel$MemberItem;)I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsMembersModel$Companion$sortMembersComparator$1<T> implements Comparator<WidgetServerSettingsMembersModel.MemberItem> {
    public static final WidgetServerSettingsMembersModel$Companion$sortMembersComparator$1 INSTANCE = new WidgetServerSettingsMembersModel$Companion$sortMembersComparator$1();

    public final int compare(WidgetServerSettingsMembersModel.MemberItem memberItem, WidgetServerSettingsMembersModel.MemberItem memberItem2) {
        m.checkNotNullParameter(memberItem, "member1");
        m.checkNotNullParameter(memberItem2, "member2");
        int compareTo = t.compareTo(memberItem.getUserDisplayName(), memberItem2.getUserDisplayName(), true);
        return compareTo != 0 ? compareTo : (memberItem.getUser().getId() > memberItem2.getUser().getId() ? 1 : (memberItem.getUser().getId() == memberItem2.getUser().getId() ? 0 : -1));
    }
}
