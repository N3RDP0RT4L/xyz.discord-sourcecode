package com.discord.widgets.servers.settings.members;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.permissions.ManageUserContext;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.widgets.servers.settings.members.WidgetServerSettingsMembersModel;
import d0.g0.s;
import d0.g0.w;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func7;
/* compiled from: WidgetServerSettingsMembersModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0017\b\u0086\b\u0018\u0000 82\u00020\u0001:\u000289BS\u0012\u0006\u0010\u0017\u001a\u00020\u0002\u0012\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u0012\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u001b\u001a\u00020\u0010\u0012\u0006\u0010\u001c\u001a\u00020\u0013\u0012\u0006\u0010\u001d\u001a\u00020\u0013¢\u0006\u0004\b6\u00107J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0013HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0015Jj\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0017\u001a\u00020\u00022\u0014\b\u0002\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00052\u000e\b\u0002\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00072\b\b\u0002\u0010\u001b\u001a\u00020\u00102\b\b\u0002\u0010\u001c\u001a\u00020\u00132\b\b\u0002\u0010\u001d\u001a\u00020\u0013HÆ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u0010\u0010$\u001a\u00020#HÖ\u0001¢\u0006\u0004\b$\u0010%J\u001a\u0010'\u001a\u00020\u00132\b\u0010&\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b'\u0010(R\u0019\u0010\u0017\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b*\u0010\u0004R\u0019\u0010\u001b\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010+\u001a\u0004\b,\u0010\u0012R%\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010-\u001a\u0004\b.\u0010\tR\u001f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u000b0\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010/\u001a\u0004\b0\u0010\rR\u0019\u0010\u001d\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00101\u001a\u0004\b2\u0010\u0015R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u00103\u001a\u0004\b4\u0010\u000fR\u0019\u0010\u001c\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u00101\u001a\u0004\b5\u0010\u0015¨\u0006:"}, d2 = {"Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "", "Lcom/discord/api/role/GuildRole;", "component2", "()Ljava/util/Map;", "", "Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel$MemberItem;", "component3", "()Ljava/util/List;", "component4", "()Lcom/discord/api/role/GuildRole;", "Lcom/discord/models/user/MeUser;", "component5", "()Lcom/discord/models/user/MeUser;", "", "component6", "()Z", "component7", "guild", "roles", "memberItems", "myHighestRole", "meUser", "canKick", "canManageMembers", "copy", "(Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/List;Lcom/discord/api/role/GuildRole;Lcom/discord/models/user/MeUser;ZZ)Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/models/user/MeUser;", "getMeUser", "Ljava/util/Map;", "getRoles", "Ljava/util/List;", "getMemberItems", "Z", "getCanManageMembers", "Lcom/discord/api/role/GuildRole;", "getMyHighestRole", "getCanKick", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Ljava/util/Map;Ljava/util/List;Lcom/discord/api/role/GuildRole;Lcom/discord/models/user/MeUser;ZZ)V", "Companion", "MemberItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsMembersModel {
    public static final Companion Companion = new Companion(null);
    public static final int MEMBER_LIST_ITEM_TYPE = 1;
    private final boolean canKick;
    private final boolean canManageMembers;
    private final Guild guild;
    private final MeUser meUser;
    private final List<MemberItem> memberItems;
    private final GuildRole myHighestRole;
    private final Map<Long, GuildRole> roles;

    /* compiled from: WidgetServerSettingsMembersModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ7\u0010\b\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0007\u0012\u0004\u0012\u00020\u00050\u00022\u0016\u0010\u0006\u001a\u0012\u0012\b\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0002H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\r\u001a\u0010\u0012\f\u0012\n \f*\u0004\u0018\u00010\u000b0\u000b0\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ9\u0010\u0015\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00140\u00102\u0006\u0010\u000f\u001a\u00020\u00032\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00030\u0010¢\u0006\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel$Companion;", "", "", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/api/role/GuildRole;", "guildRoles", "Lcom/discord/primitives/RoleId;", "sortRoles", "(Ljava/util/Map;)Ljava/util/Map;", "Ljava/util/Comparator;", "Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel$MemberItem;", "kotlin.jvm.PlatformType", "sortMembersComparator", "()Ljava/util/Comparator;", "guildId", "Lrx/Observable;", "", "filterPublisher", "roleFilterPublisher", "Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel;", "get", "(JLrx/Observable;Lrx/Observable;)Lrx/Observable;", "", "MEMBER_LIST_ITEM_TYPE", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Comparator<MemberItem> sortMembersComparator() {
            return WidgetServerSettingsMembersModel$Companion$sortMembersComparator$1.INSTANCE;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Map<Long, GuildRole> sortRoles(Map<Long, GuildRole> map) {
            LinkedHashMap linkedHashMap = new LinkedHashMap(map.size());
            ArrayList arrayList = new ArrayList(map.values());
            Collections.sort(arrayList, RoleUtils.getROLE_COMPARATOR());
            for (Object obj : arrayList) {
                linkedHashMap.put(Long.valueOf(((GuildRole) obj).getId()), obj);
            }
            return linkedHashMap;
        }

        public final Observable<WidgetServerSettingsMembersModel> get(final long j, final Observable<String> observable, final Observable<Long> observable2) {
            m.checkNotNullParameter(observable, "filterPublisher");
            m.checkNotNullParameter(observable2, "roleFilterPublisher");
            Observable<R> Y = StoreStream.Companion.getGuilds().observeComputed(j).Y(new b<Map<Long, ? extends GuildMember>, Observable<? extends WidgetServerSettingsMembersModel>>() { // from class: com.discord.widgets.servers.settings.members.WidgetServerSettingsMembersModel$Companion$get$1
                @Override // j0.k.b
                public /* bridge */ /* synthetic */ Observable<? extends WidgetServerSettingsMembersModel> call(Map<Long, ? extends GuildMember> map) {
                    return call2((Map<Long, GuildMember>) map);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final Observable<? extends WidgetServerSettingsMembersModel> call2(final Map<Long, GuildMember> map) {
                    StoreStream.Companion companion = StoreStream.Companion;
                    return Observable.e(companion.getGuilds().observeGuild(j), companion.getPermissions().observePermissionsForGuild(j), StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getUsers().observeUsers(map.keySet()), companion.getGuilds().observeRoles(j), observable.o(300L, TimeUnit.MILLISECONDS), observable2, new Func7<Guild, Long, MeUser, Map<Long, ? extends User>, Map<Long, ? extends GuildRole>, String, Long, WidgetServerSettingsMembersModel>() { // from class: com.discord.widgets.servers.settings.members.WidgetServerSettingsMembersModel$Companion$get$1.1
                        @Override // rx.functions.Func7
                        public /* bridge */ /* synthetic */ WidgetServerSettingsMembersModel call(Guild guild, Long l, MeUser meUser, Map<Long, ? extends User> map2, Map<Long, ? extends GuildRole> map3, String str, Long l2) {
                            return call2(guild, l, meUser, map2, (Map<Long, GuildRole>) map3, str, l2);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final WidgetServerSettingsMembersModel call2(Guild guild, Long l, MeUser meUser, Map<Long, ? extends User> map2, Map<Long, GuildRole> map3, String str, Long l2) {
                            Comparator sortMembersComparator;
                            Map sortRoles;
                            TreeSet treeSet;
                            String nick;
                            AnonymousClass1<T1, T2, T3, T4, T5, T6, T7, R> r0 = this;
                            String str2 = str;
                            Long l3 = l2;
                            GuildMember guildMember = (GuildMember) map.get(Long.valueOf(meUser.getId()));
                            if (guild == null || l == null || guildMember == null) {
                                return null;
                            }
                            sortMembersComparator = WidgetServerSettingsMembersModel.Companion.sortMembersComparator();
                            TreeSet treeSet2 = new TreeSet(sortMembersComparator);
                            for (User user : map2.values()) {
                                GuildMember guildMember2 = (GuildMember) a.e(user, map);
                                if (guildMember2 != null) {
                                    long id2 = guild.getId();
                                    if ((l3 != null && l2.longValue() == id2) || guildMember2.getRoles().contains(l3)) {
                                        long id3 = user.getId();
                                        m.checkNotNullExpressionValue(str2, "filter");
                                        Long longOrNull = s.toLongOrNull(str);
                                        if ((longOrNull != null && id3 == longOrNull.longValue()) || w.contains((CharSequence) user.getUsername(), (CharSequence) str2, true) || ((nick = guildMember2.getNick()) != null && w.contains((CharSequence) nick, (CharSequence) str2, true))) {
                                            ManageUserContext.Companion companion2 = ManageUserContext.Companion;
                                            m.checkNotNullExpressionValue(meUser, "me");
                                            List<Long> roles = guildMember.getRoles();
                                            List<Long> roles2 = guildMember2.getRoles();
                                            m.checkNotNullExpressionValue(map3, "roles");
                                            treeSet = treeSet2;
                                            ManageUserContext from = companion2.from(guild, meUser, user, roles, roles2, l, map3);
                                            String nickOrUsername = GuildMember.Companion.getNickOrUsername(guildMember2, user);
                                            List<Long> roles3 = guildMember2.getRoles();
                                            ArrayList arrayList = new ArrayList();
                                            for (Number number : roles3) {
                                                GuildRole guildRole = map3.get(Long.valueOf(number.longValue()));
                                                if (guildRole != null) {
                                                    arrayList.add(guildRole);
                                                }
                                            }
                                            treeSet.add(new WidgetServerSettingsMembersModel.MemberItem(user, nickOrUsername, arrayList, from.canManage(), guildMember2));
                                            r0 = this;
                                            str2 = str;
                                            treeSet2 = treeSet;
                                            l3 = l2;
                                        }
                                    }
                                }
                                treeSet = treeSet2;
                                r0 = this;
                                str2 = str;
                                treeSet2 = treeSet;
                                l3 = l2;
                            }
                            WidgetServerSettingsMembersModel.Companion companion3 = WidgetServerSettingsMembersModel.Companion;
                            m.checkNotNullExpressionValue(map3, "roles");
                            sortRoles = companion3.sortRoles(map3);
                            ArrayList arrayList2 = new ArrayList(treeSet2);
                            GuildRole highestRole = RoleUtils.getHighestRole(map3, guildMember);
                            m.checkNotNullExpressionValue(meUser, "me");
                            return new WidgetServerSettingsMembersModel(guild, sortRoles, arrayList2, highestRole, meUser, guild.isOwner(meUser.getId()) || PermissionUtils.canAndIsElevated(2L, l, meUser.getMfaEnabled(), guild.getMfaLevel()), PermissionUtils.canManageGuildMembers(guild.isOwner(meUser.getId()), meUser.getMfaEnabled(), guild.getMfaLevel(), l));
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "StoreStream\n            …          }\n            }");
            Observable<WidgetServerSettingsMembersModel> q = ObservableExtensionsKt.computationLatest(Y).q();
            m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
            return q;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsMembersModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0014\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0005\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\u0015\u001a\u00020\f\u0012\u0006\u0010\u0016\u001a\u00020\u000f¢\u0006\u0004\b/\u00100J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011JH\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\u00022\b\b\u0002\u0010\u0013\u001a\u00020\u00052\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\u0015\u001a\u00020\f2\b\b\u0002\u0010\u0016\u001a\u00020\u000fHÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u0007J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010\u001f\u001a\u00020\f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dHÖ\u0003¢\u0006\u0004\b\u001f\u0010 R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010!\u001a\u0004\b\"\u0010\u000bR\u0019\u0010\u0016\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b$\u0010\u0011R\u0019\u0010\u0013\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010%\u001a\u0004\b&\u0010\u0007R\u0019\u0010\u0015\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010'\u001a\u0004\b\u0015\u0010\u000eR\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010(\u001a\u0004\b)\u0010\u0004R\u001c\u0010*\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b*\u0010%\u001a\u0004\b+\u0010\u0007R\u001c\u0010,\u001a\u00020\u001a8\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010\u001c¨\u00061"}, d2 = {"Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel$MemberItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "", "component2", "()Ljava/lang/String;", "", "Lcom/discord/api/role/GuildRole;", "component3", "()Ljava/util/List;", "", "component4", "()Z", "Lcom/discord/models/member/GuildMember;", "component5", "()Lcom/discord/models/member/GuildMember;", "user", "userDisplayName", "roles", "isManagable", "guildMember", "copy", "(Lcom/discord/models/user/User;Ljava/lang/String;Ljava/util/List;ZLcom/discord/models/member/GuildMember;)Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel$MemberItem;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getRoles", "Lcom/discord/models/member/GuildMember;", "getGuildMember", "Ljava/lang/String;", "getUserDisplayName", "Z", "Lcom/discord/models/user/User;", "getUser", "key", "getKey", "type", "I", "getType", HookHelper.constructorName, "(Lcom/discord/models/user/User;Ljava/lang/String;Ljava/util/List;ZLcom/discord/models/member/GuildMember;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class MemberItem implements MGRecyclerDataPayload {
        private final GuildMember guildMember;
        private final boolean isManagable;
        private final String key;
        private final List<GuildRole> roles;
        private final int type = 1;
        private final User user;
        private final String userDisplayName;

        public MemberItem(User user, String str, List<GuildRole> list, boolean z2, GuildMember guildMember) {
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(str, "userDisplayName");
            m.checkNotNullParameter(list, "roles");
            m.checkNotNullParameter(guildMember, "guildMember");
            this.user = user;
            this.userDisplayName = str;
            this.roles = list;
            this.isManagable = z2;
            this.guildMember = guildMember;
            this.key = String.valueOf(user.getId());
        }

        public static /* synthetic */ MemberItem copy$default(MemberItem memberItem, User user, String str, List list, boolean z2, GuildMember guildMember, int i, Object obj) {
            if ((i & 1) != 0) {
                user = memberItem.user;
            }
            if ((i & 2) != 0) {
                str = memberItem.userDisplayName;
            }
            String str2 = str;
            List<GuildRole> list2 = list;
            if ((i & 4) != 0) {
                list2 = memberItem.roles;
            }
            List list3 = list2;
            if ((i & 8) != 0) {
                z2 = memberItem.isManagable;
            }
            boolean z3 = z2;
            if ((i & 16) != 0) {
                guildMember = memberItem.guildMember;
            }
            return memberItem.copy(user, str2, list3, z3, guildMember);
        }

        public final User component1() {
            return this.user;
        }

        public final String component2() {
            return this.userDisplayName;
        }

        public final List<GuildRole> component3() {
            return this.roles;
        }

        public final boolean component4() {
            return this.isManagable;
        }

        public final GuildMember component5() {
            return this.guildMember;
        }

        public final MemberItem copy(User user, String str, List<GuildRole> list, boolean z2, GuildMember guildMember) {
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(str, "userDisplayName");
            m.checkNotNullParameter(list, "roles");
            m.checkNotNullParameter(guildMember, "guildMember");
            return new MemberItem(user, str, list, z2, guildMember);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MemberItem)) {
                return false;
            }
            MemberItem memberItem = (MemberItem) obj;
            return m.areEqual(this.user, memberItem.user) && m.areEqual(this.userDisplayName, memberItem.userDisplayName) && m.areEqual(this.roles, memberItem.roles) && this.isManagable == memberItem.isManagable && m.areEqual(this.guildMember, memberItem.guildMember);
        }

        public final GuildMember getGuildMember() {
            return this.guildMember;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final List<GuildRole> getRoles() {
            return this.roles;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public final User getUser() {
            return this.user;
        }

        public final String getUserDisplayName() {
            return this.userDisplayName;
        }

        public int hashCode() {
            User user = this.user;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            String str = this.userDisplayName;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            List<GuildRole> list = this.roles;
            int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
            boolean z2 = this.isManagable;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode3 + i2) * 31;
            GuildMember guildMember = this.guildMember;
            if (guildMember != null) {
                i = guildMember.hashCode();
            }
            return i4 + i;
        }

        public final boolean isManagable() {
            return this.isManagable;
        }

        public String toString() {
            StringBuilder R = a.R("MemberItem(user=");
            R.append(this.user);
            R.append(", userDisplayName=");
            R.append(this.userDisplayName);
            R.append(", roles=");
            R.append(this.roles);
            R.append(", isManagable=");
            R.append(this.isManagable);
            R.append(", guildMember=");
            R.append(this.guildMember);
            R.append(")");
            return R.toString();
        }
    }

    public WidgetServerSettingsMembersModel(Guild guild, Map<Long, GuildRole> map, List<MemberItem> list, GuildRole guildRole, MeUser meUser, boolean z2, boolean z3) {
        m.checkNotNullParameter(guild, "guild");
        m.checkNotNullParameter(map, "roles");
        m.checkNotNullParameter(list, "memberItems");
        m.checkNotNullParameter(meUser, "meUser");
        this.guild = guild;
        this.roles = map;
        this.memberItems = list;
        this.myHighestRole = guildRole;
        this.meUser = meUser;
        this.canKick = z2;
        this.canManageMembers = z3;
    }

    public static /* synthetic */ WidgetServerSettingsMembersModel copy$default(WidgetServerSettingsMembersModel widgetServerSettingsMembersModel, Guild guild, Map map, List list, GuildRole guildRole, MeUser meUser, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            guild = widgetServerSettingsMembersModel.guild;
        }
        Map<Long, GuildRole> map2 = map;
        if ((i & 2) != 0) {
            map2 = widgetServerSettingsMembersModel.roles;
        }
        Map map3 = map2;
        List<MemberItem> list2 = list;
        if ((i & 4) != 0) {
            list2 = widgetServerSettingsMembersModel.memberItems;
        }
        List list3 = list2;
        if ((i & 8) != 0) {
            guildRole = widgetServerSettingsMembersModel.myHighestRole;
        }
        GuildRole guildRole2 = guildRole;
        if ((i & 16) != 0) {
            meUser = widgetServerSettingsMembersModel.meUser;
        }
        MeUser meUser2 = meUser;
        if ((i & 32) != 0) {
            z2 = widgetServerSettingsMembersModel.canKick;
        }
        boolean z4 = z2;
        if ((i & 64) != 0) {
            z3 = widgetServerSettingsMembersModel.canManageMembers;
        }
        return widgetServerSettingsMembersModel.copy(guild, map3, list3, guildRole2, meUser2, z4, z3);
    }

    public final Guild component1() {
        return this.guild;
    }

    public final Map<Long, GuildRole> component2() {
        return this.roles;
    }

    public final List<MemberItem> component3() {
        return this.memberItems;
    }

    public final GuildRole component4() {
        return this.myHighestRole;
    }

    public final MeUser component5() {
        return this.meUser;
    }

    public final boolean component6() {
        return this.canKick;
    }

    public final boolean component7() {
        return this.canManageMembers;
    }

    public final WidgetServerSettingsMembersModel copy(Guild guild, Map<Long, GuildRole> map, List<MemberItem> list, GuildRole guildRole, MeUser meUser, boolean z2, boolean z3) {
        m.checkNotNullParameter(guild, "guild");
        m.checkNotNullParameter(map, "roles");
        m.checkNotNullParameter(list, "memberItems");
        m.checkNotNullParameter(meUser, "meUser");
        return new WidgetServerSettingsMembersModel(guild, map, list, guildRole, meUser, z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetServerSettingsMembersModel)) {
            return false;
        }
        WidgetServerSettingsMembersModel widgetServerSettingsMembersModel = (WidgetServerSettingsMembersModel) obj;
        return m.areEqual(this.guild, widgetServerSettingsMembersModel.guild) && m.areEqual(this.roles, widgetServerSettingsMembersModel.roles) && m.areEqual(this.memberItems, widgetServerSettingsMembersModel.memberItems) && m.areEqual(this.myHighestRole, widgetServerSettingsMembersModel.myHighestRole) && m.areEqual(this.meUser, widgetServerSettingsMembersModel.meUser) && this.canKick == widgetServerSettingsMembersModel.canKick && this.canManageMembers == widgetServerSettingsMembersModel.canManageMembers;
    }

    public final boolean getCanKick() {
        return this.canKick;
    }

    public final boolean getCanManageMembers() {
        return this.canManageMembers;
    }

    public final Guild getGuild() {
        return this.guild;
    }

    public final MeUser getMeUser() {
        return this.meUser;
    }

    public final List<MemberItem> getMemberItems() {
        return this.memberItems;
    }

    public final GuildRole getMyHighestRole() {
        return this.myHighestRole;
    }

    public final Map<Long, GuildRole> getRoles() {
        return this.roles;
    }

    public int hashCode() {
        Guild guild = this.guild;
        int i = 0;
        int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
        Map<Long, GuildRole> map = this.roles;
        int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
        List<MemberItem> list = this.memberItems;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        GuildRole guildRole = this.myHighestRole;
        int hashCode4 = (hashCode3 + (guildRole != null ? guildRole.hashCode() : 0)) * 31;
        MeUser meUser = this.meUser;
        if (meUser != null) {
            i = meUser.hashCode();
        }
        int i2 = (hashCode4 + i) * 31;
        boolean z2 = this.canKick;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z3 = this.canManageMembers;
        if (!z3) {
            i3 = z3 ? 1 : 0;
        }
        return i6 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetServerSettingsMembersModel(guild=");
        R.append(this.guild);
        R.append(", roles=");
        R.append(this.roles);
        R.append(", memberItems=");
        R.append(this.memberItems);
        R.append(", myHighestRole=");
        R.append(this.myHighestRole);
        R.append(", meUser=");
        R.append(this.meUser);
        R.append(", canKick=");
        R.append(this.canKick);
        R.append(", canManageMembers=");
        return a.M(R, this.canManageMembers, ")");
    }
}
