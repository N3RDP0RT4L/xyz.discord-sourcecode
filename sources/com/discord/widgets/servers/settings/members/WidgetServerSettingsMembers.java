package com.discord.widgets.servers.settings.members;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.i.i1;
import b.a.i.j1;
import b.d.b.a.a;
import com.discord.api.role.GuildRole;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppViewFlipper;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsMembersBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreStream;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.TextWatcher;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.WidgetServerSettingsChannels;
import com.discord.widgets.user.WidgetPruneUsers;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsMembers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 /2\u00020\u0001:\u0002/0B\u0007¢\u0006\u0004\b.\u0010\u0014J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J3\u0010\r\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\u0016\u0010\f\u001a\u0012\u0012\b\u0012\u00060\u0007j\u0002`\b\u0012\u0004\u0012\u00020\u000b0\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0013\u0010\u0014R$\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\f\u0010\u0015R\u001d\u0010\u001b\u001a\u00020\u00168B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001d\u001a\u00020\u001c8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\"\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00070!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u001d\u0010(\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\"\u0010*\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020)0!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010#R\u0018\u0010,\u001a\u0004\u0018\u00010+8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010-¨\u00061"}, d2 = {"Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembers;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersModel;)V", "", "Lcom/discord/primitives/RoleId;", "everyoneRoleId", "", "Lcom/discord/api/role/GuildRole;", "guildRoles", "setupRolesSpinner", "(JLjava/util/Map;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Ljava/util/Map;", "Lcom/discord/databinding/WidgetServerSettingsMembersBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsMembersBinding;", "binding", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lrx/subjects/Subject;", "roleFilterPublisher", "Lrx/subjects/Subject;", "guildId$delegate", "Lkotlin/Lazy;", "getGuildId", "()J", "guildId", "", "nameFilterPublisher", "Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersAdapter;", "adapter", "Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembersAdapter;", HookHelper.constructorName, "Companion", "RolesSpinnerAdapter", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsMembers extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsMembers.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsMembersBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "GUILD_ID";
    private static final int VIEW_INDEX_MEMBER_LIST = 0;
    private static final int VIEW_INDEX_NO_RESULTS = 1;
    private WidgetServerSettingsMembersAdapter adapter;
    private Map<Long, GuildRole> guildRoles;
    private final Subject<String, String> nameFilterPublisher;
    private final Subject<Long, Long> roleFilterPublisher;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsMembers$binding$2.INSTANCE, null, 2, null);
    private final Lazy guildId$delegate = g.lazy(new WidgetServerSettingsMembers$guildId$2(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettingsMembers$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetServerSettingsMembers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012J!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\r8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u000f¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembers$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "create", "(Landroid/content/Context;J)V", "", WidgetServerSettingsChannels.INTENT_EXTRA_GUILD_ID, "Ljava/lang/String;", "", "VIEW_INDEX_MEMBER_LIST", "I", "VIEW_INDEX_NO_RESULTS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra(WidgetServerSettingsMembers.INTENT_EXTRA_GUILD_ID, j);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…_EXTRA_GUILD_ID, guildId)");
            j.d(context, WidgetServerSettingsMembers.class, putExtra);
            StoreStream.Companion.getAnalytics().onGuildSettingsPaneViewed("MEMBERS", j);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsMembers.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B'\u0012\u0006\u0010\u001d\u001a\u00020\u001c\u0012\u0016\u0010\u001a\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0018j\b\u0012\u0004\u0012\u00020\u0002`\u0019¢\u0006\u0004\b\u001e\u0010\u001fJ\u001f\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\f\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J)\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0006\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0017\u0010\u0016R&\u0010\u001a\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0018j\b\u0012\u0004\u0012\u00020\u0002`\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001b¨\u0006 "}, d2 = {"Lcom/discord/widgets/servers/settings/members/WidgetServerSettingsMembers$RolesSpinnerAdapter;", "Landroid/widget/ArrayAdapter;", "Lcom/discord/api/role/GuildRole;", "Landroid/widget/TextView;", "label", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "configureLabel", "(Landroid/widget/TextView;I)V", "getCount", "()I", "getItem", "(I)Lcom/discord/api/role/GuildRole;", "", "getItemId", "(I)J", "Landroid/view/View;", "convertView", "Landroid/view/ViewGroup;", "parent", "getView", "(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;", "getDropDownView", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "roles", "Ljava/util/ArrayList;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;Ljava/util/ArrayList;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class RolesSpinnerAdapter extends ArrayAdapter<GuildRole> {
        private final ArrayList<GuildRole> roles;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public RolesSpinnerAdapter(Context context, ArrayList<GuildRole> arrayList) {
            super(context, (int) R.layout.server_settings_members_role_spinner_item, arrayList);
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(arrayList, "roles");
            this.roles = arrayList;
        }

        private final void configureLabel(TextView textView, int i) {
            GuildRole item = getItem(i);
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            textView.setTextColor(RoleUtils.getRoleColor$default(item, context, null, 2, null));
            textView.setText(item.g());
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public int getCount() {
            return this.roles.size();
        }

        @Override // android.widget.ArrayAdapter, android.widget.BaseAdapter, android.widget.SpinnerAdapter
        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            j1 j1Var;
            m.checkNotNullParameter(viewGroup, "parent");
            if (view == null) {
                View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.server_settings_members_role_spinner_item_open, (ViewGroup) null, false);
                Objects.requireNonNull(inflate, "rootView");
                TextView textView = (TextView) inflate;
                j1Var = new j1(textView, textView);
            } else {
                TextView textView2 = (TextView) view;
                j1Var = new j1(textView2, textView2);
            }
            m.checkNotNullExpressionValue(j1Var, "if (convertView == null)…bind(convertView)\n      }");
            TextView textView3 = j1Var.f135b;
            m.checkNotNullExpressionValue(textView3, "binding.roleSpinnerItemTextview");
            configureLabel(textView3, i);
            TextView textView4 = j1Var.a;
            m.checkNotNullExpressionValue(textView4, "binding.root");
            return textView4;
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public long getItemId(int i) {
            return i;
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int i, View view, ViewGroup viewGroup) {
            i1 i1Var;
            m.checkNotNullParameter(viewGroup, "parent");
            if (view == null) {
                View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.server_settings_members_role_spinner_item, (ViewGroup) null, false);
                Objects.requireNonNull(inflate, "rootView");
                TextView textView = (TextView) inflate;
                i1Var = new i1(textView, textView);
            } else {
                TextView textView2 = (TextView) view;
                i1Var = new i1(textView2, textView2);
            }
            m.checkNotNullExpressionValue(i1Var, "if (convertView == null)…bind(convertView)\n      }");
            TextView textView3 = i1Var.f128b;
            m.checkNotNullExpressionValue(textView3, "binding.roleSpinnerItemTextview");
            configureLabel(textView3, i);
            TextView textView4 = i1Var.a;
            m.checkNotNullExpressionValue(textView4, "binding.root");
            return textView4;
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public GuildRole getItem(int i) {
            GuildRole guildRole = this.roles.get(i);
            m.checkNotNullExpressionValue(guildRole, "roles[position]");
            return guildRole;
        }
    }

    public WidgetServerSettingsMembers() {
        super(R.layout.widget_server_settings_members);
        BehaviorSubject l0 = BehaviorSubject.l0("");
        m.checkNotNullExpressionValue(l0, "BehaviorSubject.create(\"\")");
        this.nameFilterPublisher = l0;
        BehaviorSubject l02 = BehaviorSubject.l0(-1L);
        m.checkNotNullExpressionValue(l02, "BehaviorSubject.create(-1L)");
        this.roleFilterPublisher = l02;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetServerSettingsMembersModel widgetServerSettingsMembersModel) {
        if (widgetServerSettingsMembersModel == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.onBackPressed();
                return;
            }
            return;
        }
        AppFragment.setActionBarOptionsMenu$default(this, widgetServerSettingsMembersModel.getCanKick() ? R.menu.menu_server_settings_members : R.menu.menu_empty, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.servers.settings.members.WidgetServerSettingsMembers$configureUI$1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_server_settings_members_prune) {
                    WidgetPruneUsers.Companion companion = WidgetPruneUsers.Companion;
                    long id2 = widgetServerSettingsMembersModel.getGuild().getId();
                    FragmentManager parentFragmentManager = WidgetServerSettingsMembers.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion.create(id2, parentFragmentManager);
                }
            }
        }, null, 4, null);
        setActionBarTitle(R.string.member_list);
        setActionBarSubtitle(widgetServerSettingsMembersModel.getGuild().getName());
        AppViewFlipper appViewFlipper = getBinding().e;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.serverSettingsMembersViewFlipper");
        appViewFlipper.setDisplayedChild((!widgetServerSettingsMembersModel.getMemberItems().isEmpty() ? 1 : 0) ^ 1);
        if (!m.areEqual(this.guildRoles, widgetServerSettingsMembersModel.getRoles())) {
            this.guildRoles = widgetServerSettingsMembersModel.getRoles();
            setupRolesSpinner(widgetServerSettingsMembersModel.getGuild().getId(), widgetServerSettingsMembersModel.getRoles());
        }
        WidgetServerSettingsMembersAdapter widgetServerSettingsMembersAdapter = this.adapter;
        if (widgetServerSettingsMembersAdapter != null) {
            widgetServerSettingsMembersAdapter.configure(widgetServerSettingsMembersModel, new WidgetServerSettingsMembers$configureUI$2(this, widgetServerSettingsMembersModel));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsMembersBinding getBinding() {
        return (WidgetServerSettingsMembersBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    private final void setupRolesSpinner(long j, Map<Long, GuildRole> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (GuildRole guildRole : map.values()) {
            if (guildRole.getId() != j) {
                arrayList.add(guildRole);
            } else {
                arrayList.add(0, guildRole);
            }
        }
        final RolesSpinnerAdapter rolesSpinnerAdapter = new RolesSpinnerAdapter(requireContext(), arrayList);
        Spinner spinner = getBinding().d;
        m.checkNotNullExpressionValue(spinner, "binding.serverSettingsMembersRolesSpinner");
        spinner.setAdapter((SpinnerAdapter) rolesSpinnerAdapter);
        Spinner spinner2 = getBinding().d;
        m.checkNotNullExpressionValue(spinner2, "binding.serverSettingsMembersRolesSpinner");
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: com.discord.widgets.servers.settings.members.WidgetServerSettingsMembers$setupRolesSpinner$1
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j2) {
                Subject subject;
                m.checkNotNullParameter(view, "view");
                GuildRole item = rolesSpinnerAdapter.getItem(i);
                subject = WidgetServerSettingsMembers.this.roleFilterPublisher;
                subject.onNext(Long.valueOf(item.getId()));
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().c;
        m.checkNotNullExpressionValue(recyclerView, "binding.serverSettingsMembersRecycler");
        this.adapter = (WidgetServerSettingsMembersAdapter) companion.configure(new WidgetServerSettingsMembersAdapter(recyclerView));
        if (!isRecreated()) {
            this.roleFilterPublisher.onNext(Long.valueOf(getGuildId()));
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        TextWatcher.Companion companion = TextWatcher.Companion;
        TextInputLayout textInputLayout = getBinding().f2553b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.serverSettingsMembersNameSearch");
        EditText editText = textInputLayout.getEditText();
        m.checkNotNull(editText);
        m.checkNotNullExpressionValue(editText, "binding.serverSettingsMembersNameSearch.editText!!");
        companion.addBindedTextWatcher(this, editText, new Action1<String>() { // from class: com.discord.widgets.servers.settings.members.WidgetServerSettingsMembers$onViewBoundOrOnResume$1
            public final void call(String str) {
                Subject subject;
                subject = WidgetServerSettingsMembers.this.nameFilterPublisher;
                subject.onNext(str);
            }
        });
        Subject<String, String> subject = this.nameFilterPublisher;
        TextInputLayout textInputLayout2 = getBinding().f2553b;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.serverSettingsMembersNameSearch");
        EditText editText2 = textInputLayout2.getEditText();
        subject.onNext(String.valueOf(editText2 != null ? editText2.getText() : null));
        Observable<String> o = this.nameFilterPublisher.o(750L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(o, "nameFilterPublisher\n    …0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(o, WidgetServerSettingsMembers.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsMembers$onViewBoundOrOnResume$2(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(WidgetServerSettingsMembersModel.Companion.get(getGuildId(), this.nameFilterPublisher, this.roleFilterPublisher), this, null, 2, null), WidgetServerSettingsMembers.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsMembers$onViewBoundOrOnResume$3(this));
    }
}
