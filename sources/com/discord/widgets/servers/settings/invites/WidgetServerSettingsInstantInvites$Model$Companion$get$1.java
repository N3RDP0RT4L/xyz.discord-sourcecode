package com.discord.widgets.servers.settings.invites;

import com.discord.models.domain.ModelInvite;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreInstantInvites;
import com.discord.widgets.servers.settings.invites.WidgetServerSettingsInstantInvites;
import d0.t.h0;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetServerSettingsInstantInvites.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/servers/settings/invites/WidgetServerSettingsInstantInvites$Model;", "invoke", "()Lcom/discord/widgets/servers/settings/invites/WidgetServerSettingsInstantInvites$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsInstantInvites$Model$Companion$get$1 extends o implements Function0<WidgetServerSettingsInstantInvites.Model> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ StoreGuilds $storeGuilds;
    public final /* synthetic */ StoreInstantInvites $storeInstantInvites;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsInstantInvites$Model$Companion$get$1(StoreGuilds storeGuilds, long j, StoreInstantInvites storeInstantInvites) {
        super(0);
        this.$storeGuilds = storeGuilds;
        this.$guildId = j;
        this.$storeInstantInvites = storeInstantInvites;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetServerSettingsInstantInvites.Model invoke() {
        Guild guild = this.$storeGuilds.getGuild(this.$guildId);
        Map<Long, GuildMember> map = this.$storeGuilds.getMembers().get(Long.valueOf(this.$guildId));
        if (map == null) {
            map = h0.emptyMap();
        }
        Map<String, ModelInvite> invites = this.$storeInstantInvites.getInvites(this.$guildId);
        if (guild == null) {
            return null;
        }
        if (invites == null) {
            return new WidgetServerSettingsInstantInvites.Model(guild, null);
        }
        return new WidgetServerSettingsInstantInvites.Model(guild, WidgetServerSettingsInstantInvites.Model.InviteItem.Companion.createList(invites, this.$guildId, map));
    }
}
