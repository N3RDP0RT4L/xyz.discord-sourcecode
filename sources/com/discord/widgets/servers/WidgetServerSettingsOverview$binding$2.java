package com.discord.widgets.servers;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import b.a.i.f3;
import b.a.i.g3;
import b.a.i.h3;
import b.a.i.i3;
import b.a.i.j3;
import b.a.i.k3;
import b.a.i.u5;
import com.discord.databinding.WidgetServerSettingsOverviewBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.CheckedSetting;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsOverview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetServerSettingsOverviewBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsOverviewBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettingsOverview$binding$2 extends k implements Function1<View, WidgetServerSettingsOverviewBinding> {
    public static final WidgetServerSettingsOverview$binding$2 INSTANCE = new WidgetServerSettingsOverview$binding$2();

    public WidgetServerSettingsOverview$binding$2() {
        super(1, WidgetServerSettingsOverviewBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsOverviewBinding;", 0);
    }

    public final WidgetServerSettingsOverviewBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.afk;
        View findViewById = view.findViewById(R.id.afk);
        if (findViewById != null) {
            int i2 = R.id.afk_channel;
            TextView textView = (TextView) findViewById.findViewById(R.id.afk_channel);
            if (textView != null) {
                i2 = R.id.afk_channel_wrap;
                LinearLayout linearLayout = (LinearLayout) findViewById.findViewById(R.id.afk_channel_wrap);
                if (linearLayout != null) {
                    i2 = R.id.afk_timeout;
                    TextView textView2 = (TextView) findViewById.findViewById(R.id.afk_timeout);
                    if (textView2 != null) {
                        i2 = R.id.afk_timeout_wrap;
                        LinearLayout linearLayout2 = (LinearLayout) findViewById.findViewById(R.id.afk_timeout_wrap);
                        if (linearLayout2 != null) {
                            u5 u5Var = new u5((LinearLayout) findViewById, textView, linearLayout, textView2, linearLayout2);
                            i = R.id.header;
                            View findViewById2 = view.findViewById(R.id.header);
                            if (findViewById2 != null) {
                                int i3 = R.id.icon_label;
                                TextView textView3 = (TextView) findViewById2.findViewById(R.id.icon_label);
                                if (textView3 != null) {
                                    i3 = R.id.overview_name;
                                    TextInputLayout textInputLayout = (TextInputLayout) findViewById2.findViewById(R.id.overview_name);
                                    if (textInputLayout != null) {
                                        i3 = R.id.server_settings_overview_icon;
                                        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById2.findViewById(R.id.server_settings_overview_icon);
                                        if (simpleDraweeView != null) {
                                            ConstraintLayout constraintLayout = (ConstraintLayout) findViewById2;
                                            i3 = R.id.server_settings_overview_icon_remove;
                                            TextView textView4 = (TextView) findViewById2.findViewById(R.id.server_settings_overview_icon_remove);
                                            if (textView4 != null) {
                                                i3 = R.id.server_settings_overview_icon_text;
                                                TextView textView5 = (TextView) findViewById2.findViewById(R.id.server_settings_overview_icon_text);
                                                if (textView5 != null) {
                                                    f3 f3Var = new f3(constraintLayout, textView3, textInputLayout, simpleDraweeView, constraintLayout, textView4, textView5);
                                                    i = R.id.notifications;
                                                    View findViewById3 = view.findViewById(R.id.notifications);
                                                    if (findViewById3 != null) {
                                                        int i4 = R.id.server_settings_overview_notification_all;
                                                        CheckedSetting checkedSetting = (CheckedSetting) findViewById3.findViewById(R.id.server_settings_overview_notification_all);
                                                        if (checkedSetting != null) {
                                                            i4 = R.id.server_settings_overview_notification_only_mentions;
                                                            CheckedSetting checkedSetting2 = (CheckedSetting) findViewById3.findViewById(R.id.server_settings_overview_notification_only_mentions);
                                                            if (checkedSetting2 != null) {
                                                                g3 g3Var = new g3((LinearLayout) findViewById3, checkedSetting, checkedSetting2);
                                                                FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.save);
                                                                if (floatingActionButton != null) {
                                                                    ScrollView scrollView = (ScrollView) view.findViewById(R.id.server_settings_overview_scroll);
                                                                    if (scrollView != null) {
                                                                        View findViewById4 = view.findViewById(R.id.systemChannel);
                                                                        if (findViewById4 != null) {
                                                                            int i5 = R.id.system_channel;
                                                                            TextView textView6 = (TextView) findViewById4.findViewById(R.id.system_channel);
                                                                            if (textView6 != null) {
                                                                                i5 = R.id.system_channel_boost;
                                                                                CheckedSetting checkedSetting3 = (CheckedSetting) findViewById4.findViewById(R.id.system_channel_boost);
                                                                                if (checkedSetting3 != null) {
                                                                                    i5 = R.id.system_channel_join;
                                                                                    CheckedSetting checkedSetting4 = (CheckedSetting) findViewById4.findViewById(R.id.system_channel_join);
                                                                                    if (checkedSetting4 != null) {
                                                                                        i5 = R.id.system_channel_join_replies;
                                                                                        CheckedSetting checkedSetting5 = (CheckedSetting) findViewById4.findViewById(R.id.system_channel_join_replies);
                                                                                        if (checkedSetting5 != null) {
                                                                                            LinearLayout linearLayout3 = (LinearLayout) findViewById4;
                                                                                            h3 h3Var = new h3(linearLayout3, textView6, checkedSetting3, checkedSetting4, checkedSetting5, linearLayout3);
                                                                                            View findViewById5 = view.findViewById(R.id.uploadBanner);
                                                                                            if (findViewById5 != null) {
                                                                                                int i6 = R.id.animated_banner_upsell;
                                                                                                View findViewById6 = findViewById5.findViewById(R.id.animated_banner_upsell);
                                                                                                if (findViewById6 != null) {
                                                                                                    int i7 = R.id.animated_banner_upsell_button;
                                                                                                    MaterialButton materialButton = (MaterialButton) findViewById6.findViewById(R.id.animated_banner_upsell_button);
                                                                                                    if (materialButton != null) {
                                                                                                        LinearLayout linearLayout4 = (LinearLayout) findViewById6;
                                                                                                        TextView textView7 = (TextView) findViewById6.findViewById(R.id.animated_banner_upsell_text);
                                                                                                        if (textView7 != null) {
                                                                                                            i3 i3Var = new i3(linearLayout4, materialButton, linearLayout4, textView7);
                                                                                                            i6 = R.id.server_settings_overview_upload_banner_container;
                                                                                                            FrameLayout frameLayout = (FrameLayout) findViewById5.findViewById(R.id.server_settings_overview_upload_banner_container);
                                                                                                            if (frameLayout != null) {
                                                                                                                i6 = R.id.server_settings_overview_upload_banner_remove;
                                                                                                                TextView textView8 = (TextView) findViewById5.findViewById(R.id.server_settings_overview_upload_banner_remove);
                                                                                                                if (textView8 != null) {
                                                                                                                    i6 = R.id.upload_banner;
                                                                                                                    SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) findViewById5.findViewById(R.id.upload_banner);
                                                                                                                    if (simpleDraweeView2 != null) {
                                                                                                                        i6 = R.id.upload_banner_fab;
                                                                                                                        FloatingActionButton floatingActionButton2 = (FloatingActionButton) findViewById5.findViewById(R.id.upload_banner_fab);
                                                                                                                        if (floatingActionButton2 != null) {
                                                                                                                            i6 = R.id.upload_banner_learn_more;
                                                                                                                            LinkifiedTextView linkifiedTextView = (LinkifiedTextView) findViewById5.findViewById(R.id.upload_banner_learn_more);
                                                                                                                            if (linkifiedTextView != null) {
                                                                                                                                i6 = R.id.upload_banner_nitro_tier;
                                                                                                                                TextView textView9 = (TextView) findViewById5.findViewById(R.id.upload_banner_nitro_tier);
                                                                                                                                if (textView9 != null) {
                                                                                                                                    i6 = R.id.upload_banner_unlock;
                                                                                                                                    TextView textView10 = (TextView) findViewById5.findViewById(R.id.upload_banner_unlock);
                                                                                                                                    if (textView10 != null) {
                                                                                                                                        j3 j3Var = new j3((LinearLayout) findViewById5, i3Var, frameLayout, textView8, simpleDraweeView2, floatingActionButton2, linkifiedTextView, textView9, textView10);
                                                                                                                                        View findViewById7 = view.findViewById(R.id.uploadSplash);
                                                                                                                                        if (findViewById7 != null) {
                                                                                                                                            int i8 = R.id.server_settings_overview_upload_splash_container;
                                                                                                                                            FrameLayout frameLayout2 = (FrameLayout) findViewById7.findViewById(R.id.server_settings_overview_upload_splash_container);
                                                                                                                                            if (frameLayout2 != null) {
                                                                                                                                                i8 = R.id.server_settings_overview_upload_splash_fab;
                                                                                                                                                FloatingActionButton floatingActionButton3 = (FloatingActionButton) findViewById7.findViewById(R.id.server_settings_overview_upload_splash_fab);
                                                                                                                                                if (floatingActionButton3 != null) {
                                                                                                                                                    i8 = R.id.server_settings_overview_upload_splash_remove;
                                                                                                                                                    TextView textView11 = (TextView) findViewById7.findViewById(R.id.server_settings_overview_upload_splash_remove);
                                                                                                                                                    if (textView11 != null) {
                                                                                                                                                        i8 = R.id.upload_splash;
                                                                                                                                                        SimpleDraweeView simpleDraweeView3 = (SimpleDraweeView) findViewById7.findViewById(R.id.upload_splash);
                                                                                                                                                        if (simpleDraweeView3 != null) {
                                                                                                                                                            i8 = R.id.upload_splash_learn_more;
                                                                                                                                                            LinkifiedTextView linkifiedTextView2 = (LinkifiedTextView) findViewById7.findViewById(R.id.upload_splash_learn_more);
                                                                                                                                                            if (linkifiedTextView2 != null) {
                                                                                                                                                                i8 = R.id.upload_splash_nitro_tier;
                                                                                                                                                                TextView textView12 = (TextView) findViewById7.findViewById(R.id.upload_splash_nitro_tier);
                                                                                                                                                                if (textView12 != null) {
                                                                                                                                                                    i8 = R.id.upload_splash_unlock;
                                                                                                                                                                    TextView textView13 = (TextView) findViewById7.findViewById(R.id.upload_splash_unlock);
                                                                                                                                                                    if (textView13 != null) {
                                                                                                                                                                        return new WidgetServerSettingsOverviewBinding((CoordinatorLayout) view, u5Var, f3Var, g3Var, floatingActionButton, scrollView, h3Var, j3Var, new k3((LinearLayout) findViewById7, frameLayout2, floatingActionButton3, textView11, simpleDraweeView3, linkifiedTextView2, textView12, textView13));
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            throw new NullPointerException("Missing required view with ID: ".concat(findViewById7.getResources().getResourceName(i8)));
                                                                                                                                        }
                                                                                                                                        i = R.id.uploadSplash;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        } else {
                                                                                                            i7 = R.id.animated_banner_upsell_text;
                                                                                                        }
                                                                                                    }
                                                                                                    throw new NullPointerException("Missing required view with ID: ".concat(findViewById6.getResources().getResourceName(i7)));
                                                                                                }
                                                                                                throw new NullPointerException("Missing required view with ID: ".concat(findViewById5.getResources().getResourceName(i6)));
                                                                                            }
                                                                                            i = R.id.uploadBanner;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            throw new NullPointerException("Missing required view with ID: ".concat(findViewById4.getResources().getResourceName(i5)));
                                                                        }
                                                                        i = R.id.systemChannel;
                                                                    } else {
                                                                        i = R.id.server_settings_overview_scroll;
                                                                    }
                                                                } else {
                                                                    i = R.id.save;
                                                                }
                                                            }
                                                        }
                                                        throw new NullPointerException("Missing required view with ID: ".concat(findViewById3.getResources().getResourceName(i4)));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                throw new NullPointerException("Missing required view with ID: ".concat(findViewById2.getResources().getResourceName(i3)));
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(i2)));
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
