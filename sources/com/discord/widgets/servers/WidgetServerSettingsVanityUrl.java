package com.discord.widgets.servers;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.guild.VanityUrlResponse;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsVanityUrlBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.servers.WidgetServerSettingsVanityUrl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func4;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsVanityUrl.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 /2\u00020\u0001:\u0002/0B\u0007¢\u0006\u0004\b.\u0010\u001bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J!\u0010\u000b\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u00072\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ#\u0010\u0010\u001a\u00020\u00042\n\u0010\u000f\u001a\u00060\rj\u0002`\u000e2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u001c\u0010 \u001a\u00020\u001f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u001d\u0010)\u001a\u00020$8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b'\u0010(R\u001d\u0010\u000f\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-¨\u00061"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;)V", "", ModelAuditLogEntry.CHANGE_KEY_CODE, "", ModelAuditLogEntry.CHANGE_KEY_USES, "configureInviteCode", "(Ljava/lang/String;I)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "updateVanityUrl", "(JLjava/lang/String;)V", "", "loading", "showLoadingUI", "(Z)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onResume", "()V", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/databinding/WidgetServerSettingsVanityUrlBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsVanityUrlBinding;", "binding", "guildId$delegate", "Lkotlin/Lazy;", "getGuildId", "()J", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsVanityUrl extends AppFragment {
    private static final long ANIMATION_DURATION = 250;
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsVanityUrl.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsVanityUrlBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsVanityUrl$binding$2.INSTANCE, null, 2, null);
    private final StatefulViews state = new StatefulViews(R.id.server_settings_vanity_input);
    private final Lazy guildId$delegate = g.lazy(new WidgetServerSettingsVanityUrl$guildId$2(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettingsVanityUrl$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetServerSettingsVanityUrl.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0013\u0010\u0004\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001b\u0010\b\u001a\u00020\u0003*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ!\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\n2\n\u0010\u000e\u001a\u00060\fj\u0002`\r¢\u0006\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Companion;", "", "Landroid/view/View;", "", "translateToOriginX", "(Landroid/view/View;)V", "", "distance", "translateLeft", "(Landroid/view/View;I)V", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "create", "(Landroid/content/Context;J)V", "ANIMATION_DURATION", "J", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void translateLeft(View view, int i) {
            view.animate().translationXBy(-i).setDuration(WidgetServerSettingsVanityUrl.ANIMATION_DURATION).setInterpolator(new AccelerateDecelerateInterpolator()).start();
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void translateToOriginX(View view) {
            view.animate().translationX(0.0f).setDuration(WidgetServerSettingsVanityUrl.ANIMATION_DURATION).setInterpolator(new AccelerateDecelerateInterpolator()).start();
        }

        public final void create(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            StoreStream.Companion.getAnalytics().onGuildSettingsPaneViewed("VANITY_URL", j);
            Intent putExtra = new Intent().putExtra("INTENT_EXTRA_GUILD_ID", j);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…_EXTRA_GUILD_ID, guildId)");
            j.d(context, WidgetServerSettingsVanityUrl.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsVanityUrl.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\r\b\u0082\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B/\u0012\n\u0010\r\u001a\u00060\u0002j\u0002`\u0003\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0010\u001a\u00020\n¢\u0006\u0004\b \u0010!J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ@\u0010\u0011\u001a\u00020\u00002\f\b\u0002\u0010\r\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0010\u001a\u00020\nHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0013\u0010\bJ\u0010\u0010\u0014\u001a\u00020\nHÖ\u0001¢\u0006\u0004\b\u0014\u0010\fJ\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001b\u0010\u000e\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0019\u001a\u0004\b\u001a\u0010\bR\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0019\u001a\u0004\b\u001b\u0010\bR\u0019\u0010\u0010\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001c\u001a\u0004\b\u001d\u0010\fR\u001d\u0010\r\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\u0005¨\u0006#"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()J", "", "component2", "()Ljava/lang/String;", "component3", "", "component4", "()I", "guildId", "guildName", "vanityUrl", "vanityUrlUses", "copy", "(JLjava/lang/String;Ljava/lang/String;I)Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getGuildName", "getVanityUrl", "I", "getVanityUrlUses", "J", "getGuildId", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;I)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final long guildId;
        private final String guildName;
        private final String vanityUrl;
        private final int vanityUrlUses;

        /* compiled from: WidgetServerSettingsVanityUrl.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(final long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<Model> h = Observable.h(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getPermissions().observePermissionsForGuild(j), companion.getGuilds().observeGuild(j), ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getVanityUrl(j), false, 1, null), new Func4<MeUser, Long, Guild, VanityUrlResponse, Model>() { // from class: com.discord.widgets.servers.WidgetServerSettingsVanityUrl$Model$Companion$get$1
                    public final WidgetServerSettingsVanityUrl.Model call(MeUser meUser, Long l, Guild guild, VanityUrlResponse vanityUrlResponse) {
                        m.checkNotNullParameter(meUser, "meUser");
                        int b2 = vanityUrlResponse.b();
                        if (l == null || guild == null || !guild.canHaveVanityURL()) {
                            return null;
                        }
                        if (guild.isOwner(meUser.getId()) || PermissionUtils.can(32L, l)) {
                            return new WidgetServerSettingsVanityUrl.Model(j, guild.getName(), guild.getVanityUrlCode(), b2);
                        }
                        return null;
                    }
                });
                m.checkNotNullExpressionValue(h, "Observable.combineLatest…ull\n          }\n        }");
                return h;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(long j, String str, String str2, int i) {
            this.guildId = j;
            this.guildName = str;
            this.vanityUrl = str2;
            this.vanityUrlUses = i;
        }

        public static /* synthetic */ Model copy$default(Model model, long j, String str, String str2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = model.guildId;
            }
            long j2 = j;
            if ((i2 & 2) != 0) {
                str = model.guildName;
            }
            String str3 = str;
            if ((i2 & 4) != 0) {
                str2 = model.vanityUrl;
            }
            String str4 = str2;
            if ((i2 & 8) != 0) {
                i = model.vanityUrlUses;
            }
            return model.copy(j2, str3, str4, i);
        }

        public final long component1() {
            return this.guildId;
        }

        public final String component2() {
            return this.guildName;
        }

        public final String component3() {
            return this.vanityUrl;
        }

        public final int component4() {
            return this.vanityUrlUses;
        }

        public final Model copy(long j, String str, String str2, int i) {
            return new Model(j, str, str2, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return this.guildId == model.guildId && m.areEqual(this.guildName, model.guildName) && m.areEqual(this.vanityUrl, model.vanityUrl) && this.vanityUrlUses == model.vanityUrlUses;
        }

        public final long getGuildId() {
            return this.guildId;
        }

        public final String getGuildName() {
            return this.guildName;
        }

        public final String getVanityUrl() {
            return this.vanityUrl;
        }

        public final int getVanityUrlUses() {
            return this.vanityUrlUses;
        }

        public int hashCode() {
            int a = b.a(this.guildId) * 31;
            String str = this.guildName;
            int i = 0;
            int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.vanityUrl;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return ((hashCode + i) * 31) + this.vanityUrlUses;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guildId=");
            R.append(this.guildId);
            R.append(", guildName=");
            R.append(this.guildName);
            R.append(", vanityUrl=");
            R.append(this.vanityUrl);
            R.append(", vanityUrlUses=");
            return a.A(R, this.vanityUrlUses, ")");
        }
    }

    public WidgetServerSettingsVanityUrl() {
        super(R.layout.widget_server_settings_vanity_url);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0077  */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0089  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x0099  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x009f  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00c8  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void configureInviteCode(java.lang.String r10, int r11) {
        /*
            r9 = this;
            com.discord.databinding.WidgetServerSettingsVanityUrlBinding r0 = r9.getBinding()
            com.google.android.material.textfield.TextInputLayout r0 = r0.f2562b
            java.lang.String r1 = "binding.serverSettingsVanityInput"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            com.discord.utilities.stateful.StatefulViews r2 = r9.state
            com.discord.databinding.WidgetServerSettingsVanityUrlBinding r3 = r9.getBinding()
            com.google.android.material.textfield.TextInputLayout r3 = r3.f2562b
            d0.z.d.m.checkNotNullExpressionValue(r3, r1)
            int r1 = r3.getId()
            java.lang.Object r1 = r2.get(r1, r10)
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1
            com.discord.utilities.view.extensions.ViewExtensions.setText(r0, r1)
            com.discord.databinding.WidgetServerSettingsVanityUrlBinding r0 = r9.getBinding()
            com.google.android.material.textfield.TextInputLayout r0 = r0.f2562b
            r0.clearFocus()
            r0 = 4
            r1 = 0
            java.lang.String r2 = "binding.serverSettingsVanityUrlCurrentUrl"
            r3 = 0
            r4 = 1
            if (r10 == 0) goto L58
            int r5 = r10.length()
            if (r5 <= 0) goto L3c
            r5 = 1
            goto L3d
        L3c:
            r5 = 0
        L3d:
            if (r5 != r4) goto L58
            java.lang.String r5 = "https://discord.gg/"
            java.lang.String r5 = b.d.b.a.a.v(r5, r10)
            com.discord.databinding.WidgetServerSettingsVanityUrlBinding r6 = r9.getBinding()
            android.widget.TextView r6 = r6.c
            d0.z.d.m.checkNotNullExpressionValue(r6, r2)
            r7 = 2131895605(0x7f122535, float:1.9426048E38)
            java.lang.Object[] r8 = new java.lang.Object[r4]
            r8[r3] = r5
            b.a.k.b.n(r6, r7, r8, r1, r0)
        L58:
            com.discord.databinding.WidgetServerSettingsVanityUrlBinding r5 = r9.getBinding()
            android.widget.TextView r5 = r5.c
            d0.z.d.m.checkNotNullExpressionValue(r5, r2)
            if (r10 == 0) goto L70
            int r2 = r10.length()
            if (r2 <= 0) goto L6b
            r2 = 1
            goto L6c
        L6b:
            r2 = 0
        L6c:
            if (r2 != r4) goto L70
            r2 = 1
            goto L71
        L70:
            r2 = 0
        L71:
            r6 = 8
            if (r2 == 0) goto L77
            r2 = 0
            goto L79
        L77:
            r2 = 8
        L79:
            r5.setVisibility(r2)
            com.discord.databinding.WidgetServerSettingsVanityUrlBinding r2 = r9.getBinding()
            android.widget.TextView r2 = r2.i
            java.lang.String r5 = "binding.serverSettingsVanityUrlRemove"
            d0.z.d.m.checkNotNullExpressionValue(r2, r5)
            if (r10 == 0) goto L96
            int r5 = r10.length()
            if (r5 <= 0) goto L91
            r5 = 1
            goto L92
        L91:
            r5 = 0
        L92:
            if (r5 != r4) goto L96
            r5 = 1
            goto L97
        L96:
            r5 = 0
        L97:
            if (r5 == 0) goto L9a
            r6 = 0
        L9a:
            r2.setVisibility(r6)
            if (r10 == 0) goto Lc8
            android.content.Context r10 = r9.requireContext()
            r2 = 2131755401(0x7f100189, float:1.914168E38)
            java.lang.Object[] r5 = new java.lang.Object[r4]
            java.lang.Integer r6 = java.lang.Integer.valueOf(r11)
            r5[r3] = r6
            java.lang.CharSequence r10 = com.discord.utilities.resources.StringResourceUtilsKt.getI18nPluralString(r10, r2, r11, r5)
            com.discord.databinding.WidgetServerSettingsVanityUrlBinding r11 = r9.getBinding()
            android.widget.TextView r11 = r11.e
            java.lang.String r2 = "binding.serverSettingsVanityUrlHeader"
            d0.z.d.m.checkNotNullExpressionValue(r11, r2)
            r2 = 2131895602(0x7f122532, float:1.9426042E38)
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r4[r3] = r10
            b.a.k.b.n(r11, r2, r4, r1, r0)
            goto Ld4
        Lc8:
            com.discord.databinding.WidgetServerSettingsVanityUrlBinding r10 = r9.getBinding()
            android.widget.TextView r10 = r10.e
            r11 = 2131895600(0x7f122530, float:1.9426038E38)
            r10.setText(r11)
        Ld4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.WidgetServerSettingsVanityUrl.configureInviteCode(java.lang.String, int):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        if (model == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        showLoadingUI(false);
        setActionBarTitle(R.string.vanity_url);
        setActionBarSubtitle(model.getGuildName());
        configureInviteCode(model.getVanityUrl(), model.getVanityUrlUses());
        this.state.configureSaveActionView(getBinding().j);
        getBinding().j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsVanityUrl$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsVanityUrlBinding binding;
                WidgetServerSettingsVanityUrl widgetServerSettingsVanityUrl = WidgetServerSettingsVanityUrl.this;
                long guildId = model.getGuildId();
                binding = WidgetServerSettingsVanityUrl.this.getBinding();
                TextInputLayout textInputLayout = binding.f2562b;
                m.checkNotNullExpressionValue(textInputLayout, "binding.serverSettingsVanityInput");
                String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
                int length = textOrEmpty.length() - 1;
                int i = 0;
                boolean z2 = false;
                while (i <= length) {
                    boolean z3 = m.compare(textOrEmpty.charAt(!z2 ? i : length), 32) <= 0;
                    if (!z2) {
                        if (!z3) {
                            z2 = true;
                        } else {
                            i++;
                        }
                    } else if (!z3) {
                        break;
                    } else {
                        length--;
                    }
                }
                widgetServerSettingsVanityUrl.updateVanityUrl(guildId, textOrEmpty.subSequence(i, length + 1).toString());
            }
        });
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsVanityUrl$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsVanityUrl.this.updateVanityUrl(model.getGuildId(), "");
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsVanityUrlBinding getBinding() {
        return (WidgetServerSettingsVanityUrlBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final long getGuildId() {
        return ((Number) this.guildId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showLoadingUI(boolean z2) {
        ProgressBar progressBar = getBinding().g;
        m.checkNotNullExpressionValue(progressBar, "binding.serverSettingsVanityUrlLoadingIndicator");
        progressBar.setVisibility(z2 ? 0 : 8);
        TextView textView = getBinding().d;
        m.checkNotNullExpressionValue(textView, "binding.serverSettingsVanityUrlErrorText");
        textView.setVisibility(8);
        TextView textView2 = getBinding().i;
        m.checkNotNullExpressionValue(textView2, "binding.serverSettingsVanityUrlRemove");
        textView2.setEnabled(!z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateVanityUrl(long j, String str) {
        showLoadingUI(true);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateVanityUrl(j, new RestAPIParams.VanityUrl(str)), false, 1, null), this, null, 2, null), WidgetServerSettingsVanityUrl.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetServerSettingsVanityUrl$updateVanityUrl$2(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsVanityUrl$updateVanityUrl$1(this));
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        showLoadingUI(true);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getGuildId()), this, null, 2, null), WidgetServerSettingsVanityUrl.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsVanityUrl$onResume$1(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        TextInputLayout textInputLayout = getBinding().f2562b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.serverSettingsVanityInput");
        ViewExtensions.setOnEditTextFocusChangeListener(textInputLayout, new View.OnFocusChangeListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsVanityUrl$onViewBound$1
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view2, boolean z2) {
                WidgetServerSettingsVanityUrlBinding binding;
                WidgetServerSettingsVanityUrlBinding binding2;
                WidgetServerSettingsVanityUrlBinding binding3;
                WidgetServerSettingsVanityUrlBinding binding4;
                WidgetServerSettingsVanityUrlBinding binding5;
                if (!z2) {
                    WidgetServerSettingsVanityUrl.Companion companion = WidgetServerSettingsVanityUrl.Companion;
                    binding4 = WidgetServerSettingsVanityUrl.this.getBinding();
                    TextView textView = binding4.h;
                    m.checkNotNullExpressionValue(textView, "binding.serverSettingsVanityUrlPrefix");
                    companion.translateToOriginX(textView);
                    binding5 = WidgetServerSettingsVanityUrl.this.getBinding();
                    TextInputLayout textInputLayout2 = binding5.f2562b;
                    m.checkNotNullExpressionValue(textInputLayout2, "binding.serverSettingsVanityInput");
                    companion.translateToOriginX(textInputLayout2);
                    return;
                }
                binding = WidgetServerSettingsVanityUrl.this.getBinding();
                TextView textView2 = binding.h;
                m.checkNotNullExpressionValue(textView2, "binding.serverSettingsVanityUrlPrefix");
                int width = textView2.getWidth();
                WidgetServerSettingsVanityUrl.Companion companion2 = WidgetServerSettingsVanityUrl.Companion;
                binding2 = WidgetServerSettingsVanityUrl.this.getBinding();
                TextView textView3 = binding2.h;
                m.checkNotNullExpressionValue(textView3, "binding.serverSettingsVanityUrlPrefix");
                companion2.translateLeft(textView3, width);
                binding3 = WidgetServerSettingsVanityUrl.this.getBinding();
                TextInputLayout textInputLayout3 = binding3.f2562b;
                m.checkNotNullExpressionValue(textInputLayout3, "binding.serverSettingsVanityInput");
                companion2.translateLeft(textInputLayout3, width);
            }
        });
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsVanityUrl$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetServerSettingsVanityUrlBinding binding;
                WidgetServerSettingsVanityUrl widgetServerSettingsVanityUrl = WidgetServerSettingsVanityUrl.this;
                binding = widgetServerSettingsVanityUrl.getBinding();
                TextInputLayout textInputLayout2 = binding.f2562b;
                m.checkNotNullExpressionValue(textInputLayout2, "binding.serverSettingsVanityInput");
                widgetServerSettingsVanityUrl.showKeyboard(textInputLayout2);
            }
        });
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews = this.state;
        FloatingActionButton floatingActionButton = getBinding().j;
        TextInputLayout textInputLayout2 = getBinding().f2562b;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.serverSettingsVanityInput");
        statefulViews.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout2);
    }
}
