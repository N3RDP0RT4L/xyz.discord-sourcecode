package com.discord.widgets.servers;

import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import androidx.annotation.ColorInt;
import androidx.core.app.NotificationCompat;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetServerSettingsEditRole.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\b\b\u0001\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "", "colorInt", "Landroid/text/SpannableStringBuilder;", "invoke", "(Ljava/lang/String;I)Landroid/text/SpannableStringBuilder;", "getColoredSpan"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEditRole$setupActionBar$1 extends o implements Function2<String, Integer, SpannableStringBuilder> {
    public static final WidgetServerSettingsEditRole$setupActionBar$1 INSTANCE = new WidgetServerSettingsEditRole$setupActionBar$1();

    public WidgetServerSettingsEditRole$setupActionBar$1() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ SpannableStringBuilder invoke(String str, Integer num) {
        return invoke(str, num.intValue());
    }

    public final SpannableStringBuilder invoke(String str, @ColorInt int i) {
        m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(i), 0, spannableStringBuilder.length(), 0);
        return spannableStringBuilder;
    }
}
