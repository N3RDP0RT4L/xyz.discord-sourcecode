package com.discord.widgets.servers;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.databinding.SettingsChannelListCategoryItemBinding;
import com.discord.databinding.SettingsChannelListItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter;
import com.discord.utilities.mg_recycler.DragAndDropHelper;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: SettingsChannelListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 .2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0006/012.3B\u0019\u0012\u0006\u0010*\u001a\u00020)\u0012\b\b\u0002\u0010+\u001a\u00020\u001e¢\u0006\u0004\b,\u0010-J7\u0010\t\u001a\u00020\b2\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00032\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u0002¢\u0006\u0004\b\t\u0010\nJ+\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u000e2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\u0014\u001a\u00020\b2\u0018\u0010\u0013\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00120\u00030\u0011¢\u0006\u0004\b\u0014\u0010\u0015J\u001b\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00050\u0003H\u0016¢\u0006\u0004\b\u0017\u0010\u0018J#\u0010\u001a\u001a\u00020\b2\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00050\u0003H\u0016¢\u0006\u0004\b\u001a\u0010\u001bJ\u001f\u0010\u001f\u001a\u00020\u001e2\u0006\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u001f\u0010 R0\u0010\u0013\u001a\u001c\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00120\u0003\u0012\u0004\u0012\u00020\b\u0018\u00010!8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\"R4\u0010$\u001a\u0014\u0012\b\u0012\u00060\u0004j\u0002`#\u0012\u0004\u0012\u00020\b\u0018\u00010!8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b$\u0010\"\u001a\u0004\b%\u0010&\"\u0004\b'\u0010(¨\u00064"}, d2 = {"Lcom/discord/widgets/servers/SettingsChannelListAdapter;", "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter;", "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", "", "", "", "changedPositions", "changedParentIds", "", "handleChangedPositions", "(Ljava/util/Map;Ljava/util/Map;)V", "Landroid/view/ViewGroup;", "parent", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lrx/functions/Action1;", "Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;", "onPositionUpdateListener", "setPositionUpdateListener", "(Lrx/functions/Action1;)V", "", "computeChangedPositions", "()Ljava/util/Map;", "newPositions", "onNewPositions", "(Ljava/util/Map;)V", "fromPosition", "toPosition", "", "isValidMove", "(II)Z", "Lkotlin/Function1;", "Lkotlin/jvm/functions/Function1;", "Lcom/discord/primitives/ChannelId;", "onClickListener", "getOnClickListener", "()Lkotlin/jvm/functions/Function1;", "setOnClickListener", "(Lkotlin/jvm/functions/Function1;)V", "Landroidx/recyclerview/widget/RecyclerView;", "recycler", "isDraggable", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Z)V", "Companion", "CategoryItem", "CategoryListItem", "ChannelItem", "ChannelListItem", "UpdatedPosition", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsChannelListAdapter extends CategoricalDragAndDropAdapter<CategoricalDragAndDropAdapter.Payload> {
    public static final Companion Companion = new Companion(null);
    public static final int TYPE_CATEGORY = 1;
    public static final int TYPE_CHANNEL = 0;
    private Function1<? super Long, Unit> onClickListener;
    private Function1<? super Map<Long, UpdatedPosition>, Unit> onPositionUpdateListener;

    /* compiled from: SettingsChannelListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001B9\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0013\u001a\u00020\n\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\r\u0012\u0006\u0010\u0016\u001a\u00020\r\u0012\u0006\u0010\u0017\u001a\u00020\r¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000fJN\u0010\u0018\u001a\u00020\u00002\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u0013\u001a\u00020\n2\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\r2\b\b\u0002\u0010\u0016\u001a\u00020\r2\b\b\u0002\u0010\u0017\u001a\u00020\rHÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u001a\u0010\bJ\u0010\u0010\u001b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001b\u0010\u0004J\u001a\u0010\u001e\u001a\u00020\r2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u0016\u0010\u0014\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010 R\u0019\u0010\u0017\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010!\u001a\u0004\b\"\u0010\u000fR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\bR\u001c\u0010%\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b%\u0010 \u001a\u0004\b&\u0010\u0004R\u001c\u0010'\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b'\u0010#\u001a\u0004\b(\u0010\bR\u0019\u0010\u0015\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010!\u001a\u0004\b\u0015\u0010\u000fR\u0019\u0010\u0013\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010)\u001a\u0004\b*\u0010\fR\u0019\u0010\u0016\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010!\u001a\u0004\b+\u0010\u000f¨\u0006."}, d2 = {"Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;", "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", "", "component3", "()I", "getPosition", "", "getCategory", "()Ljava/lang/String;", "component1", "", "component2", "()J", "", "component4", "()Z", "component5", "component6", ModelAuditLogEntry.CHANGE_KEY_NAME, ModelAuditLogEntry.CHANGE_KEY_ID, "pos", "isDraggable", "canManageCategory", "canManageChannelsOfCategory", "copy", "(Ljava/lang/String;JIZZZ)Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "I", "Z", "getCanManageChannelsOfCategory", "Ljava/lang/String;", "getName", "type", "getType", "key", "getKey", "J", "getId", "getCanManageCategory", HookHelper.constructorName, "(Ljava/lang/String;JIZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CategoryItem implements CategoricalDragAndDropAdapter.Payload {
        private final boolean canManageCategory;
        private final boolean canManageChannelsOfCategory;

        /* renamed from: id  reason: collision with root package name */
        private final long f2834id;
        private final boolean isDraggable;
        private final String key;
        private final String name;
        private final int pos;
        private final int type = 1;

        public CategoryItem(String str, long j, int i, boolean z2, boolean z3, boolean z4) {
            this.name = str;
            this.f2834id = j;
            this.pos = i;
            this.isDraggable = z2;
            this.canManageCategory = z3;
            this.canManageChannelsOfCategory = z4;
            this.key = String.valueOf(j);
        }

        private final int component3() {
            return this.pos;
        }

        public static /* synthetic */ CategoryItem copy$default(CategoryItem categoryItem, String str, long j, int i, boolean z2, boolean z3, boolean z4, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = categoryItem.name;
            }
            if ((i2 & 2) != 0) {
                j = categoryItem.f2834id;
            }
            long j2 = j;
            if ((i2 & 4) != 0) {
                i = categoryItem.pos;
            }
            int i3 = i;
            if ((i2 & 8) != 0) {
                z2 = categoryItem.isDraggable;
            }
            boolean z5 = z2;
            if ((i2 & 16) != 0) {
                z3 = categoryItem.canManageCategory;
            }
            boolean z6 = z3;
            if ((i2 & 32) != 0) {
                z4 = categoryItem.canManageChannelsOfCategory;
            }
            return categoryItem.copy(str, j2, i3, z5, z6, z4);
        }

        public final String component1() {
            return this.name;
        }

        public final long component2() {
            return this.f2834id;
        }

        public final boolean component4() {
            return this.isDraggable;
        }

        public final boolean component5() {
            return this.canManageCategory;
        }

        public final boolean component6() {
            return this.canManageChannelsOfCategory;
        }

        public final CategoryItem copy(String str, long j, int i, boolean z2, boolean z3, boolean z4) {
            return new CategoryItem(str, j, i, z2, z3, z4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CategoryItem)) {
                return false;
            }
            CategoryItem categoryItem = (CategoryItem) obj;
            return m.areEqual(this.name, categoryItem.name) && this.f2834id == categoryItem.f2834id && this.pos == categoryItem.pos && this.isDraggable == categoryItem.isDraggable && this.canManageCategory == categoryItem.canManageCategory && this.canManageChannelsOfCategory == categoryItem.canManageChannelsOfCategory;
        }

        public final boolean getCanManageCategory() {
            return this.canManageCategory;
        }

        public final boolean getCanManageChannelsOfCategory() {
            return this.canManageChannelsOfCategory;
        }

        @Override // com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter.Payload
        public String getCategory() {
            return String.valueOf(getType());
        }

        public final long getId() {
            return this.f2834id;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final String getName() {
            return this.name;
        }

        @Override // com.discord.utilities.mg_recycler.DragAndDropAdapter.Payload
        public int getPosition() {
            return this.pos;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            String str = this.name;
            int a = (((b.a(this.f2834id) + ((str != null ? str.hashCode() : 0) * 31)) * 31) + this.pos) * 31;
            boolean z2 = this.isDraggable;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (a + i2) * 31;
            boolean z3 = this.canManageCategory;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.canManageChannelsOfCategory;
            if (!z4) {
                i = z4 ? 1 : 0;
            }
            return i7 + i;
        }

        public final boolean isDraggable() {
            return this.isDraggable;
        }

        public String toString() {
            StringBuilder R = a.R("CategoryItem(name=");
            R.append(this.name);
            R.append(", id=");
            R.append(this.f2834id);
            R.append(", pos=");
            R.append(this.pos);
            R.append(", isDraggable=");
            R.append(this.isDraggable);
            R.append(", canManageCategory=");
            R.append(this.canManageCategory);
            R.append(", canManageChannelsOfCategory=");
            return a.M(R, this.canManageChannelsOfCategory, ")");
        }
    }

    /* compiled from: SettingsChannelListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B\u000f\u0012\u0006\u0010\u0017\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0018\u0010\u0015\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/servers/SettingsChannelListAdapter;", "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", "Lcom/discord/utilities/mg_recycler/DragAndDropHelper$DraggableViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;)V", "", "dragging", "onDragStateChanged", "(Z)V", "canDrag", "()Z", "Lcom/discord/databinding/SettingsChannelListCategoryItemBinding;", "binding", "Lcom/discord/databinding/SettingsChannelListCategoryItemBinding;", "Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;", "categoryItem", "Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/servers/SettingsChannelListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CategoryListItem extends MGRecyclerViewHolder<SettingsChannelListAdapter, CategoricalDragAndDropAdapter.Payload> implements DragAndDropHelper.DraggableViewHolder {
        private final SettingsChannelListCategoryItemBinding binding;
        private CategoryItem categoryItem;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public CategoryListItem(SettingsChannelListAdapter settingsChannelListAdapter) {
            super((int) R.layout.settings_channel_list_category_item, settingsChannelListAdapter);
            m.checkNotNullParameter(settingsChannelListAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.settings_channel_list_category_item_drag;
            ImageView imageView = (ImageView) view.findViewById(R.id.settings_channel_list_category_item_drag);
            if (imageView != null) {
                i = R.id.settings_channel_list_category_item_text;
                TextView textView = (TextView) view.findViewById(R.id.settings_channel_list_category_item_text);
                if (textView != null) {
                    SettingsChannelListCategoryItemBinding settingsChannelListCategoryItemBinding = new SettingsChannelListCategoryItemBinding((RelativeLayout) view, imageView, textView);
                    m.checkNotNullExpressionValue(settingsChannelListCategoryItemBinding, "SettingsChannelListCateg…temBinding.bind(itemView)");
                    this.binding = settingsChannelListCategoryItemBinding;
                    return;
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        @Override // com.discord.utilities.mg_recycler.DragAndDropHelper.DraggableViewHolder
        public boolean canDrag() {
            CategoryItem categoryItem = this.categoryItem;
            return categoryItem != null && categoryItem.isDraggable() && categoryItem.getCanManageCategory();
        }

        @Override // com.discord.utilities.mg_recycler.DragAndDropHelper.DraggableViewHolder
        public void onDragStateChanged(boolean z2) {
        }

        public void onConfigure(int i, CategoricalDragAndDropAdapter.Payload payload) {
            m.checkNotNullParameter(payload, "data");
            super.onConfigure(i, (int) payload);
            final CategoryItem categoryItem = (CategoryItem) payload;
            this.categoryItem = categoryItem;
            if (categoryItem != null) {
                final Function1<Long, Unit> onClickListener = ((SettingsChannelListAdapter) this.adapter).getOnClickListener();
                if (onClickListener != null) {
                    this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.SettingsChannelListAdapter$CategoryListItem$onConfigure$$inlined$let$lambda$1
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            Function1.this.invoke(Long.valueOf(categoryItem.getId()));
                        }
                    });
                }
                TextView textView = this.binding.c;
                m.checkNotNullExpressionValue(textView, "binding.settingsChannelListCategoryItemText");
                String name = categoryItem.getName();
                if (name == null) {
                    View view = this.itemView;
                    m.checkNotNullExpressionValue(view, "itemView");
                    name = view.getContext().getString(R.string.uncategorized);
                }
                textView.setText(name);
                ImageView imageView = this.binding.f2123b;
                m.checkNotNullExpressionValue(imageView, "binding.settingsChannelListCategoryItemDrag");
                imageView.setVisibility(canDrag() ? 0 : 8);
            }
        }
    }

    /* compiled from: SettingsChannelListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0012\u001a\u00020\b\u0012\u0006\u0010\u0013\u001a\u00020\u000b\u0012\u0006\u0010\u0014\u001a\u00020\u000e\u0012\u0006\u0010\u0015\u001a\u00020\u000b¢\u0006\u0004\b*\u0010+J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\u0011\u0010\rJ8\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0012\u001a\u00020\b2\b\b\u0002\u0010\u0013\u001a\u00020\u000b2\b\b\u0002\u0010\u0014\u001a\u00020\u000e2\b\b\u0002\u0010\u0015\u001a\u00020\u000bHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0007J\u0010\u0010\u0019\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u0004J\u001a\u0010\u001c\u001a\u00020\u000b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0014\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u001e\u001a\u0004\b\u001f\u0010\u0010R\u0019\u0010\u0013\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b\u0013\u0010\rR\u001c\u0010!\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010\u0004R\u001c\u0010$\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010\u0007R\u0019\u0010\u0012\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010'\u001a\u0004\b(\u0010\nR\u0019\u0010\u0015\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010 \u001a\u0004\b)\u0010\r¨\u0006,"}, d2 = {"Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;", "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", "", "getPosition", "()I", "", "getCategory", "()Ljava/lang/String;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "", "component3", "()J", "component4", "channel", "isDraggable", "parentId", "canManageCategoryOfChannel", "copy", "(Lcom/discord/api/channel/Channel;ZJZ)Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "J", "getParentId", "Z", "type", "I", "getType", "key", "Ljava/lang/String;", "getKey", "Lcom/discord/api/channel/Channel;", "getChannel", "getCanManageCategoryOfChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZJZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelItem implements CategoricalDragAndDropAdapter.Payload {
        private final boolean canManageCategoryOfChannel;
        private final Channel channel;
        private final boolean isDraggable;
        private final String key;
        private final long parentId;
        private final int type;

        public ChannelItem(Channel channel, boolean z2, long j, boolean z3) {
            m.checkNotNullParameter(channel, "channel");
            this.channel = channel;
            this.isDraggable = z2;
            this.parentId = j;
            this.canManageCategoryOfChannel = z3;
            this.key = String.valueOf(channel.h());
        }

        public static /* synthetic */ ChannelItem copy$default(ChannelItem channelItem, Channel channel, boolean z2, long j, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                channel = channelItem.channel;
            }
            if ((i & 2) != 0) {
                z2 = channelItem.isDraggable;
            }
            boolean z4 = z2;
            if ((i & 4) != 0) {
                j = channelItem.parentId;
            }
            long j2 = j;
            if ((i & 8) != 0) {
                z3 = channelItem.canManageCategoryOfChannel;
            }
            return channelItem.copy(channel, z4, j2, z3);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final boolean component2() {
            return this.isDraggable;
        }

        public final long component3() {
            return this.parentId;
        }

        public final boolean component4() {
            return this.canManageCategoryOfChannel;
        }

        public final ChannelItem copy(Channel channel, boolean z2, long j, boolean z3) {
            m.checkNotNullParameter(channel, "channel");
            return new ChannelItem(channel, z2, j, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ChannelItem)) {
                return false;
            }
            ChannelItem channelItem = (ChannelItem) obj;
            return m.areEqual(this.channel, channelItem.channel) && this.isDraggable == channelItem.isDraggable && this.parentId == channelItem.parentId && this.canManageCategoryOfChannel == channelItem.canManageCategoryOfChannel;
        }

        public final boolean getCanManageCategoryOfChannel() {
            return this.canManageCategoryOfChannel;
        }

        @Override // com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter.Payload
        public String getCategory() {
            return String.valueOf(getType());
        }

        public final Channel getChannel() {
            return this.channel;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.key;
        }

        public final long getParentId() {
            return this.parentId;
        }

        @Override // com.discord.utilities.mg_recycler.DragAndDropAdapter.Payload
        public int getPosition() {
            return this.channel.t();
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return this.type;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            boolean z2 = this.isDraggable;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int a = (b.a(this.parentId) + ((hashCode + i2) * 31)) * 31;
            boolean z3 = this.canManageCategoryOfChannel;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return a + i;
        }

        public final boolean isDraggable() {
            return this.isDraggable;
        }

        public String toString() {
            StringBuilder R = a.R("ChannelItem(channel=");
            R.append(this.channel);
            R.append(", isDraggable=");
            R.append(this.isDraggable);
            R.append(", parentId=");
            R.append(this.parentId);
            R.append(", canManageCategoryOfChannel=");
            return a.M(R, this.canManageCategoryOfChannel, ")");
        }
    }

    /* compiled from: SettingsChannelListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B\u000f\u0012\u0006\u0010\u0017\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u001f\u0010\t\u001a\u00020\b2\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/servers/SettingsChannelListAdapter;", "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", "Lcom/discord/utilities/mg_recycler/DragAndDropHelper$DraggableViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "", "onConfigure", "(ILcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;)V", "", "dragging", "onDragStateChanged", "(Z)V", "canDrag", "()Z", "Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;", "channelItem", "Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;", "Lcom/discord/databinding/SettingsChannelListItemBinding;", "binding", "Lcom/discord/databinding/SettingsChannelListItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/servers/SettingsChannelListAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ChannelListItem extends MGRecyclerViewHolder<SettingsChannelListAdapter, CategoricalDragAndDropAdapter.Payload> implements DragAndDropHelper.DraggableViewHolder {
        private final SettingsChannelListItemBinding binding;
        private ChannelItem channelItem;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public ChannelListItem(SettingsChannelListAdapter settingsChannelListAdapter) {
            super((int) R.layout.settings_channel_list_item, settingsChannelListAdapter);
            m.checkNotNullParameter(settingsChannelListAdapter, "adapter");
            View view = this.itemView;
            int i = R.id.settings_channel_list_item_drag;
            ImageView imageView = (ImageView) view.findViewById(R.id.settings_channel_list_item_drag);
            if (imageView != null) {
                i = R.id.settings_channel_list_item_hash;
                ImageView imageView2 = (ImageView) view.findViewById(R.id.settings_channel_list_item_hash);
                if (imageView2 != null) {
                    i = R.id.settings_channel_list_item_name;
                    TextView textView = (TextView) view.findViewById(R.id.settings_channel_list_item_name);
                    if (textView != null) {
                        i = R.id.settings_channel_list_item_selected_overlay;
                        View findViewById = view.findViewById(R.id.settings_channel_list_item_selected_overlay);
                        if (findViewById != null) {
                            SettingsChannelListItemBinding settingsChannelListItemBinding = new SettingsChannelListItemBinding((RelativeLayout) view, imageView, imageView2, textView, findViewById);
                            m.checkNotNullExpressionValue(settingsChannelListItemBinding, "SettingsChannelListItemBinding.bind(itemView)");
                            this.binding = settingsChannelListItemBinding;
                            return;
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
        }

        @Override // com.discord.utilities.mg_recycler.DragAndDropHelper.DraggableViewHolder
        public boolean canDrag() {
            ChannelItem channelItem = this.channelItem;
            return channelItem != null && channelItem.isDraggable() && channelItem.getCanManageCategoryOfChannel();
        }

        @Override // com.discord.utilities.mg_recycler.DragAndDropHelper.DraggableViewHolder
        public void onDragStateChanged(boolean z2) {
            View view = this.binding.e;
            m.checkNotNullExpressionValue(view, "binding.settingsChannelListItemSelectedOverlay");
            view.setVisibility(z2 ? 0 : 8);
        }

        public void onConfigure(int i, CategoricalDragAndDropAdapter.Payload payload) {
            m.checkNotNullParameter(payload, "data");
            super.onConfigure(i, (int) payload);
            final ChannelItem channelItem = (ChannelItem) payload;
            this.channelItem = channelItem;
            if (channelItem != null) {
                this.itemView.setOnClickListener(null);
                final Function1<Long, Unit> onClickListener = ((SettingsChannelListAdapter) this.adapter).getOnClickListener();
                if (onClickListener != null) {
                    this.itemView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.SettingsChannelListAdapter$ChannelListItem$onConfigure$$inlined$let$lambda$1
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            Function1.this.invoke(Long.valueOf(channelItem.getChannel().h()));
                        }
                    });
                }
                ImageView imageView = this.binding.c;
                int A = channelItem.getChannel().A();
                imageView.setImageResource(A != 2 ? A != 5 ? A != 13 ? R.drawable.ic_channel_text : R.drawable.ic_channel_stage_24dp : R.drawable.ic_channel_announcements : R.drawable.ic_channel_voice);
                TextView textView = this.binding.d;
                m.checkNotNullExpressionValue(textView, "binding.settingsChannelListItemName");
                textView.setText(ChannelUtils.c(channelItem.getChannel()));
                ImageView imageView2 = this.binding.f2124b;
                m.checkNotNullExpressionValue(imageView2, "binding.settingsChannelListItemDrag");
                imageView2.setVisibility(canDrag() ? 0 : 8);
            }
        }
    }

    /* compiled from: SettingsChannelListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/SettingsChannelListAdapter$Companion;", "", "", "TYPE_CATEGORY", "I", "TYPE_CHANNEL", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: SettingsChannelListAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J&\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u001b\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;", "", "", "component1", "()I", "", "component2", "()Ljava/lang/Long;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "parentId", "copy", "(ILjava/lang/Long;)Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getPosition", "Ljava/lang/Long;", "getParentId", HookHelper.constructorName, "(ILjava/lang/Long;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UpdatedPosition {
        private final Long parentId;
        private final int position;

        public UpdatedPosition(int i, Long l) {
            this.position = i;
            this.parentId = l;
        }

        public static /* synthetic */ UpdatedPosition copy$default(UpdatedPosition updatedPosition, int i, Long l, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = updatedPosition.position;
            }
            if ((i2 & 2) != 0) {
                l = updatedPosition.parentId;
            }
            return updatedPosition.copy(i, l);
        }

        public final int component1() {
            return this.position;
        }

        public final Long component2() {
            return this.parentId;
        }

        public final UpdatedPosition copy(int i, Long l) {
            return new UpdatedPosition(i, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof UpdatedPosition)) {
                return false;
            }
            UpdatedPosition updatedPosition = (UpdatedPosition) obj;
            return this.position == updatedPosition.position && m.areEqual(this.parentId, updatedPosition.parentId);
        }

        public final Long getParentId() {
            return this.parentId;
        }

        public final int getPosition() {
            return this.position;
        }

        public int hashCode() {
            int i = this.position * 31;
            Long l = this.parentId;
            return i + (l != null ? l.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("UpdatedPosition(position=");
            R.append(this.position);
            R.append(", parentId=");
            return a.F(R, this.parentId, ")");
        }
    }

    public /* synthetic */ SettingsChannelListAdapter(RecyclerView recyclerView, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(recyclerView, (i & 2) != 0 ? true : z2);
    }

    private final void handleChangedPositions(Map<Long, Integer> map, Map<Long, Long> map2) {
        HashMap hashMap = new HashMap();
        for (Map.Entry<Long, Integer> entry : map.entrySet()) {
            long longValue = entry.getKey().longValue();
            hashMap.put(Long.valueOf(longValue), new UpdatedPosition(entry.getValue().intValue(), map2.get(Long.valueOf(longValue))));
        }
        Function1<? super Map<Long, UpdatedPosition>, Unit> function1 = this.onPositionUpdateListener;
        if (function1 != null) {
            function1.invoke(hashMap);
        }
    }

    @Override // com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter, com.discord.utilities.mg_recycler.DragAndDropAdapter
    public Map<String, Integer> computeChangedPositions() {
        SettingsChannelListAdapter$computeChangedPositions$1 settingsChannelListAdapter$computeChangedPositions$1 = SettingsChannelListAdapter$computeChangedPositions$1.INSTANCE;
        SettingsChannelListAdapter$computeChangedPositions$2 settingsChannelListAdapter$computeChangedPositions$2 = SettingsChannelListAdapter$computeChangedPositions$2.INSTANCE;
        SettingsChannelListAdapter$computeChangedPositions$3 settingsChannelListAdapter$computeChangedPositions$3 = SettingsChannelListAdapter$computeChangedPositions$3.INSTANCE;
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        int i = 0;
        Long l = null;
        for (Object obj : getDataCopy()) {
            i++;
            if (i < 0) {
                n.throwIndexOverflow();
            }
            CategoricalDragAndDropAdapter.Payload payload = (CategoricalDragAndDropAdapter.Payload) obj;
            Long invoke = SettingsChannelListAdapter$computeChangedPositions$1.INSTANCE.invoke(payload);
            long longValue = invoke != null ? invoke.longValue() : 0L;
            if (payload.getType() == 1) {
                l = SettingsChannelListAdapter$computeChangedPositions$2.INSTANCE.invoke(longValue);
            }
            Long invoke2 = payload.getType() != 0 ? null : SettingsChannelListAdapter$computeChangedPositions$2.INSTANCE.invoke(((ChannelItem) payload).getParentId());
            Integer num = getOrigPositions().get(payload.getKey());
            if ((num == null || num.intValue() != i) && SettingsChannelListAdapter$computeChangedPositions$3.INSTANCE.invoke2(payload)) {
                hashMap.put(Long.valueOf(longValue), Integer.valueOf(i));
                if ((!m.areEqual(invoke2, l)) && payload.getType() == 0) {
                    hashMap2.put(Long.valueOf(longValue), Long.valueOf(l != null ? l.longValue() : -1L));
                }
            }
        }
        handleChangedPositions(hashMap, hashMap2);
        return h0.emptyMap();
    }

    public final Function1<Long, Unit> getOnClickListener() {
        return this.onClickListener;
    }

    @Override // com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter, com.discord.utilities.mg_recycler.DragAndDropHelper.Adapter
    public boolean isValidMove(int i, int i2) {
        if (i2 <= 0) {
            return false;
        }
        CategoricalDragAndDropAdapter.Payload payload = (CategoricalDragAndDropAdapter.Payload) getItem(i2 - 1);
        int type = payload.getType();
        if (type == 0) {
            return ((ChannelItem) payload).getCanManageCategoryOfChannel();
        }
        if (type != 1) {
            return false;
        }
        return ((CategoryItem) payload).getCanManageChannelsOfCategory();
    }

    @Override // com.discord.utilities.mg_recycler.DragAndDropAdapter
    public void onNewPositions(Map<String, Integer> map) {
        m.checkNotNullParameter(map, "newPositions");
    }

    public final void setOnClickListener(Function1<? super Long, Unit> function1) {
        this.onClickListener = function1;
    }

    public final void setPositionUpdateListener(Action1<Map<Long, UpdatedPosition>> action1) {
        m.checkNotNullParameter(action1, "onPositionUpdateListener");
        this.onPositionUpdateListener = new SettingsChannelListAdapter$setPositionUpdateListener$1(action1);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsChannelListAdapter(RecyclerView recyclerView, boolean z2) {
        super(recyclerView);
        m.checkNotNullParameter(recyclerView, "recycler");
        if (z2) {
            new ItemTouchHelper(new DragAndDropHelper(this, 0, 2, null)).attachToRecyclerView(recyclerView);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<SettingsChannelListAdapter, CategoricalDragAndDropAdapter.Payload> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        if (i == 0) {
            return new ChannelListItem(this);
        }
        if (i == 1) {
            return new CategoryListItem(this);
        }
        throw invalidViewTypeException(i);
    }
}
