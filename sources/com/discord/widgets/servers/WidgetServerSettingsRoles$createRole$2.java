package com.discord.widgets.servers;

import com.discord.api.role.GuildRole;
import com.discord.widgets.servers.WidgetServerSettingsRoles;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetServerSettingsRoles.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/role/GuildRole;", "createdRole", "", "invoke", "(Lcom/discord/api/role/GuildRole;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsRoles$createRole$2 extends o implements Function1<GuildRole, Unit> {
    public final /* synthetic */ WidgetServerSettingsRoles.Model $dataSnapshot;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ WidgetServerSettingsRoles this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsRoles$createRole$2(WidgetServerSettingsRoles widgetServerSettingsRoles, long j, WidgetServerSettingsRoles.Model model) {
        super(1);
        this.this$0 = widgetServerSettingsRoles;
        this.$guildId = j;
        this.$dataSnapshot = model;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GuildRole guildRole) {
        invoke2(guildRole);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(GuildRole guildRole) {
        if (guildRole != null) {
            WidgetServerSettingsEditRole.Companion.launch(this.$guildId, guildRole.getId(), this.this$0.requireContext());
        } else {
            this.this$0.configureUI(this.$dataSnapshot);
        }
    }
}
