package com.discord.widgets.servers;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.widgets.channels.settings.WidgetTextChannelSettings;
import com.discord.widgets.servers.WidgetServerSettingsChannels;
import com.discord.widgets.voice.settings.WidgetVoiceChannelSettings;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetServerSettingsChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u0001H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "channelId", "", "invoke", "(J)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsChannels$setOnClickListener$2 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ WidgetServerSettingsChannels.Model $model;
    public final /* synthetic */ WidgetServerSettingsChannels this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsChannels$setOnClickListener$2(WidgetServerSettingsChannels widgetServerSettingsChannels, WidgetServerSettingsChannels.Model model) {
        super(1);
        this.this$0 = widgetServerSettingsChannels;
        this.$model = model;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.a;
    }

    public final void invoke(long j) {
        if (PermissionUtils.can(16L, this.$model.getChannelPermissions().get(Long.valueOf(j)))) {
            Channel channel = this.$model.getChannels().get(Long.valueOf(j));
            if (channel == null || !ChannelUtils.t(channel)) {
                WidgetTextChannelSettings.Companion.launch(j, this.this$0.requireContext());
            } else {
                WidgetVoiceChannelSettings.Companion.launch(j, this.this$0.requireContext());
            }
        }
    }
}
