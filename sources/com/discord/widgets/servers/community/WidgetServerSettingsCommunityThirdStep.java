package com.discord.widgets.servers.community;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.e0;
import b.a.d.f;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetServerSettingsCommunitySetupThirdStepBinding;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.ToastManager;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsCommunityThirdStep.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0007¢\u0006\u0004\b\u0019\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\t\u0010\bR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0018\u001a\u00020\u00138B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;", "viewState", "", "configureUI", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V", "onViewBoundOrOnResume", "()V", "onDestroyView", "Lcom/discord/utilities/view/ToastManager;", "toastManager", "Lcom/discord/utilities/view/ToastManager;", "Lcom/discord/databinding/WidgetServerSettingsCommunitySetupThirdStepBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsCommunitySetupThirdStepBinding;", "binding", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsCommunityThirdStep extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsCommunityThirdStep.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsCommunitySetupThirdStepBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsCommunityThirdStep$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetServerSettingsEnableCommunityViewModel.class), new WidgetServerSettingsCommunityThirdStep$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetServerSettingsCommunityThirdStep$viewModel$2.INSTANCE));
    private ToastManager toastManager = new ToastManager();

    /* compiled from: WidgetServerSettingsCommunityThirdStep.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$Companion;", "", "Landroid/content/Context;", "context", "", "create", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetServerSettingsCommunityThirdStep.class, new Intent());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetServerSettingsCommunityThirdStep() {
        super(R.layout.widget_server_settings_community_setup_third_step);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Removed duplicated region for block: B:20:0x0095  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void configureUI(final com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel.ViewState.Loaded r10) {
        /*
            r9 = this;
            com.discord.databinding.WidgetServerSettingsCommunitySetupThirdStepBinding r0 = r9.getBinding()
            com.discord.views.CheckedSetting r0 = r0.e
            java.lang.String r1 = "binding.communitySetting…ficationsToMentionsSwitch"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig r1 = r10.getCommunityGuildConfig()
            boolean r1 = r1.getDefaultMessageNotifications()
            r0.setChecked(r1)
            com.discord.databinding.WidgetServerSettingsCommunitySetupThirdStepBinding r0 = r9.getBinding()
            com.discord.views.CheckedSetting r0 = r0.d
            java.lang.String r1 = "binding.communitySettingManagePermissionsSwitch"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig r1 = r10.getCommunityGuildConfig()
            boolean r1 = r1.getEveryonePermissions()
            r0.setChecked(r1)
            com.discord.databinding.WidgetServerSettingsCommunitySetupThirdStepBinding r0 = r9.getBinding()
            com.discord.views.CheckedSetting r0 = r0.f2532b
            java.lang.String r1 = "binding.communitySettingCommunityGuidelinesSwitch"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig r1 = r10.getCommunityGuildConfig()
            boolean r1 = r1.isPrivacyPolicyAccepted()
            r0.setChecked(r1)
            com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig r0 = r10.getCommunityGuildConfig()
            com.discord.models.guild.Guild r0 = r0.getGuild()
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L56
            int r0 = r0.getDefaultMessageNotifications()
            if (r0 != r2) goto L56
            r0 = 1
            goto L57
        L56:
            r0 = 0
        L57:
            r3 = -1116960071743(0xfffffefbeffddfc1, double:NaN)
            com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig r5 = r10.getCommunityGuildConfig()
            java.util.Map r5 = r5.getRoles()
            r6 = 0
            if (r5 == 0) goto L88
            com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig r7 = r10.getCommunityGuildConfig()
            com.discord.models.guild.Guild r7 = r7.getGuild()
            if (r7 == 0) goto L7a
            long r7 = r7.getId()
            java.lang.Long r7 = java.lang.Long.valueOf(r7)
            goto L7b
        L7a:
            r7 = r6
        L7b:
            java.lang.Object r5 = r5.get(r7)
            com.discord.api.role.GuildRole r5 = (com.discord.api.role.GuildRole) r5
            if (r5 == 0) goto L88
            long r7 = r5.h()
            goto L8a
        L88:
            r7 = 0
        L8a:
            long r3 = r3 & r7
            com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig r5 = r10.getCommunityGuildConfig()
            java.util.Map r5 = r5.getRoles()
            if (r5 == 0) goto Lb8
            com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig r7 = r10.getCommunityGuildConfig()
            com.discord.models.guild.Guild r7 = r7.getGuild()
            if (r7 == 0) goto La7
            long r6 = r7.getId()
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
        La7:
            java.lang.Object r5 = r5.get(r6)
            com.discord.api.role.GuildRole r5 = (com.discord.api.role.GuildRole) r5
            if (r5 == 0) goto Lb8
            long r5 = r5.h()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 != 0) goto Lb8
            r1 = 1
        Lb8:
            com.discord.databinding.WidgetServerSettingsCommunitySetupThirdStepBinding r2 = r9.getBinding()
            com.discord.views.CheckedSetting r2 = r2.e
            com.discord.widgets.servers.community.WidgetServerSettingsCommunityThirdStep$configureUI$1 r3 = new com.discord.widgets.servers.community.WidgetServerSettingsCommunityThirdStep$configureUI$1
            r3.<init>()
            r2.e(r3)
            com.discord.databinding.WidgetServerSettingsCommunitySetupThirdStepBinding r0 = r9.getBinding()
            com.discord.views.CheckedSetting r0 = r0.d
            com.discord.widgets.servers.community.WidgetServerSettingsCommunityThirdStep$configureUI$2 r2 = new com.discord.widgets.servers.community.WidgetServerSettingsCommunityThirdStep$configureUI$2
            r2.<init>()
            r0.e(r2)
            com.discord.databinding.WidgetServerSettingsCommunitySetupThirdStepBinding r10 = r9.getBinding()
            com.discord.views.CheckedSetting r10 = r10.f2532b
            com.discord.widgets.servers.community.WidgetServerSettingsCommunityThirdStep$configureUI$3 r0 = new com.discord.widgets.servers.community.WidgetServerSettingsCommunityThirdStep$configureUI$3
            r0.<init>()
            r10.e(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.servers.community.WidgetServerSettingsCommunityThirdStep.configureUI(com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded):void");
    }

    public static final void create(Context context) {
        Companion.create(context);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsCommunitySetupThirdStepBinding getBinding() {
        return (WidgetServerSettingsCommunitySetupThirdStepBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsEnableCommunityViewModel getViewModel() {
        return (WidgetServerSettingsEnableCommunityViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.toastManager.close();
        super.onDestroyView();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable F = ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null).x(WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$$inlined$filterIs$1.INSTANCE).F(WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$$inlined$filterIs$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
        ObservableExtensionsKt.appSubscribe(F, WidgetServerSettingsCommunityThirdStep.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$1(this));
        CheckedSetting checkedSetting = getBinding().e;
        Context context = getContext();
        checkedSetting.setText(context != null ? b.b(context, R.string.enable_community_modal_default_notifications_label_mobile, new Object[0], new WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$2(this)) : null);
        CheckedSetting checkedSetting2 = getBinding().d;
        Context context2 = getContext();
        checkedSetting2.setText(context2 != null ? b.b(context2, R.string.enable_community_modal_everyone_role_permission_label_mobile, new Object[0], new WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$3(this)) : null);
        LinkifiedTextView linkifiedTextView = getBinding().c;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.communitySettingGuidelines");
        b.m(linkifiedTextView, R.string.community_policy_help, new Object[]{f.a.a(360035969312L, null)}, (r4 & 4) != 0 ? b.g.j : null);
    }
}
