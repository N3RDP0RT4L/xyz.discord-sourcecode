package com.discord.widgets.servers.community;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import b.a.k.b;
import com.discord.R;
import com.discord.databinding.ViewCommunityGetStartedInformationBinding;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: CommunityGetStartedInformationView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\b\b\u0002\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/servers/community/CommunityGetStartedInformationView;", "Landroidx/cardview/widget/CardView;", "Landroid/util/AttributeSet;", "attrs", "", "initAttrs", "(Landroid/util/AttributeSet;)V", "Lcom/discord/databinding/ViewCommunityGetStartedInformationBinding;", "binding", "Lcom/discord/databinding/ViewCommunityGetStartedInformationBinding;", "Landroid/content/Context;", "context", "", "defStyleAttr", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CommunityGetStartedInformationView extends CardView {
    private final ViewCommunityGetStartedInformationBinding binding;

    public CommunityGetStartedInformationView(Context context) {
        this(context, null, 0, 6, null);
    }

    public CommunityGetStartedInformationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ CommunityGetStartedInformationView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    private final void initAttrs(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.a.CommunityGetStartedInformationView, 0, 0);
        m.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…tedInformationView, 0, 0)");
        int resourceId = obtainStyledAttributes.getResourceId(1, 0);
        CharSequence text = obtainStyledAttributes.getText(2);
        CharSequence text2 = obtainStyledAttributes.getText(0);
        obtainStyledAttributes.recycle();
        this.binding.c.setImageDrawable(AppCompatResources.getDrawable(getContext(), resourceId));
        TextView textView = this.binding.d;
        m.checkNotNullExpressionValue(textView, "binding.communityListAdapterItemTextName");
        textView.setText(text);
        TextView textView2 = this.binding.f2165b;
        m.checkNotNullExpressionValue(textView2, "binding.communityListAdapterItemDescription");
        m.checkNotNullExpressionValue(text2, "descriptionText");
        textView2.setText(b.g(text2, new Object[0], new CommunityGetStartedInformationView$initAttrs$1(this)));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public CommunityGetStartedInformationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        LayoutInflater.from(context).inflate(xyz.discord.R.layout.view_community_get_started_information, this);
        int i2 = xyz.discord.R.id.community_guideline;
        Guideline guideline = (Guideline) findViewById(xyz.discord.R.id.community_guideline);
        if (guideline != null) {
            i2 = xyz.discord.R.id.community_list_adapter_item_description;
            TextView textView = (TextView) findViewById(xyz.discord.R.id.community_list_adapter_item_description);
            if (textView != null) {
                i2 = xyz.discord.R.id.community_list_adapter_item_icon;
                ImageView imageView = (ImageView) findViewById(xyz.discord.R.id.community_list_adapter_item_icon);
                if (imageView != null) {
                    i2 = xyz.discord.R.id.community_list_adapter_item_text_header;
                    ConstraintLayout constraintLayout = (ConstraintLayout) findViewById(xyz.discord.R.id.community_list_adapter_item_text_header);
                    if (constraintLayout != null) {
                        i2 = xyz.discord.R.id.community_list_adapter_item_text_icon;
                        RelativeLayout relativeLayout = (RelativeLayout) findViewById(xyz.discord.R.id.community_list_adapter_item_text_icon);
                        if (relativeLayout != null) {
                            i2 = xyz.discord.R.id.community_list_adapter_item_text_name;
                            TextView textView2 = (TextView) findViewById(xyz.discord.R.id.community_list_adapter_item_text_name);
                            if (textView2 != null) {
                                ViewCommunityGetStartedInformationBinding viewCommunityGetStartedInformationBinding = new ViewCommunityGetStartedInformationBinding(this, guideline, textView, imageView, constraintLayout, relativeLayout, textView2);
                                m.checkNotNullExpressionValue(viewCommunityGetStartedInformationBinding, "ViewCommunityGetStartedI…ater.from(context), this)");
                                this.binding = viewCommunityGetStartedInformationBinding;
                                if (attributeSet != null) {
                                    initAttrs(attributeSet);
                                    return;
                                }
                                return;
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(getResources().getResourceName(i2)));
    }
}
