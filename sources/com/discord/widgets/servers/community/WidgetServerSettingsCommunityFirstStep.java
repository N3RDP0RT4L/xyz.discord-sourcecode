package com.discord.widgets.servers.community;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.e0;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.guild.GuildExplicitContentFilter;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetServerSettingsCommunitySetupFirstStepBinding;
import com.discord.models.guild.Guild;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.ToastManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsCommunityFirstStep.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0007¢\u0006\u0004\b\u0019\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\t\u0010\bR\u001d\u0010\u000f\u001a\u00020\n8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u001d\u0010\u0015\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityFirstStep;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;", "viewState", "", "configureUI", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V", "onViewBoundOrOnResume", "()V", "onDestroyView", "Lcom/discord/databinding/WidgetServerSettingsCommunitySetupFirstStepBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsCommunitySetupFirstStepBinding;", "binding", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;", "viewModel", "Lcom/discord/utilities/view/ToastManager;", "toastManager", "Lcom/discord/utilities/view/ToastManager;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsCommunityFirstStep extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsCommunityFirstStep.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsCommunitySetupFirstStepBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsCommunityFirstStep$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetServerSettingsEnableCommunityViewModel.class), new WidgetServerSettingsCommunityFirstStep$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetServerSettingsCommunityFirstStep$viewModel$2.INSTANCE));
    private ToastManager toastManager = new ToastManager();

    /* compiled from: WidgetServerSettingsCommunityFirstStep.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityFirstStep$Companion;", "", "Landroid/content/Context;", "context", "", "create", "(Landroid/content/Context;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetServerSettingsCommunityFirstStep.class, new Intent());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetServerSettingsCommunityFirstStep() {
        super(R.layout.widget_server_settings_community_setup_first_step);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetServerSettingsEnableCommunityViewModel.ViewState.Loaded loaded) {
        CheckedSetting checkedSetting = getBinding().c;
        m.checkNotNullExpressionValue(checkedSetting, "binding.communitySettingsVerifiedEmailedSwitch");
        checkedSetting.setChecked(loaded.getCommunityGuildConfig().getVerificationLevel());
        CheckedSetting checkedSetting2 = getBinding().f2530b;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.communitySettingsScanMessagesSwitch");
        checkedSetting2.setChecked(loaded.getCommunityGuildConfig().getExplicitContentFilter());
        Guild guild = loaded.getCommunityGuildConfig().getGuild();
        final boolean z2 = true;
        final boolean z3 = guild != null && guild.getVerificationLevel().compareTo(GuildVerificationLevel.NONE) > 0;
        if (guild == null || guild.getExplicitContentFilter().compareTo(GuildExplicitContentFilter.NONE) <= 0) {
            z2 = false;
        }
        getBinding().c.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.community.WidgetServerSettingsCommunityFirstStep$configureUI$1

            /* compiled from: WidgetServerSettingsCommunityFirstStep.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "it", "invoke", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.community.WidgetServerSettingsCommunityFirstStep$configureUI$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig, WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig> {
                public AnonymousClass1() {
                    super(1);
                }

                public final WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig invoke(WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig communityGuildConfig) {
                    WidgetServerSettingsCommunitySetupFirstStepBinding binding;
                    WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig copy;
                    m.checkNotNullParameter(communityGuildConfig, "it");
                    binding = WidgetServerSettingsCommunityFirstStep.this.getBinding();
                    CheckedSetting checkedSetting = binding.c;
                    m.checkNotNullExpressionValue(checkedSetting, "binding.communitySettingsVerifiedEmailedSwitch");
                    copy = communityGuildConfig.copy((r26 & 1) != 0 ? communityGuildConfig.rulesChannel : null, (r26 & 2) != 0 ? communityGuildConfig.updatesChannel : null, (r26 & 4) != 0 ? communityGuildConfig.rulesChannelId : null, (r26 & 8) != 0 ? communityGuildConfig.updatesChannelId : null, (r26 & 16) != 0 ? communityGuildConfig.isPrivacyPolicyAccepted : false, (r26 & 32) != 0 ? communityGuildConfig.defaultMessageNotifications : false, (r26 & 64) != 0 ? communityGuildConfig.verificationLevel : !checkedSetting.isChecked(), (r26 & 128) != 0 ? communityGuildConfig.explicitContentFilter : false, (r26 & 256) != 0 ? communityGuildConfig.guild : null, (r26 & 512) != 0 ? communityGuildConfig.everyonePermissions : false, (r26 & 1024) != 0 ? communityGuildConfig.features : null, (r26 & 2048) != 0 ? communityGuildConfig.roles : null);
                    return copy;
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsEnableCommunityViewModel viewModel;
                ToastManager toastManager;
                if (!loaded.getCommunityGuildConfig().getVerificationLevel() || !z3) {
                    viewModel = WidgetServerSettingsCommunityFirstStep.this.getViewModel();
                    viewModel.modifyGuildConfig(new AnonymousClass1());
                    return;
                }
                Context context = WidgetServerSettingsCommunityFirstStep.this.getContext();
                toastManager = WidgetServerSettingsCommunityFirstStep.this.toastManager;
                b.a.d.m.d(context, R.string.enable_community_modal_requirement_satisfied_tooltip, 0, toastManager);
            }
        });
        getBinding().f2530b.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.community.WidgetServerSettingsCommunityFirstStep$configureUI$2

            /* compiled from: WidgetServerSettingsCommunityFirstStep.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "it", "invoke", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.community.WidgetServerSettingsCommunityFirstStep$configureUI$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig, WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig> {
                public AnonymousClass1() {
                    super(1);
                }

                public final WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig invoke(WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig communityGuildConfig) {
                    WidgetServerSettingsCommunitySetupFirstStepBinding binding;
                    WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig copy;
                    m.checkNotNullParameter(communityGuildConfig, "it");
                    binding = WidgetServerSettingsCommunityFirstStep.this.getBinding();
                    CheckedSetting checkedSetting = binding.f2530b;
                    m.checkNotNullExpressionValue(checkedSetting, "binding.communitySettingsScanMessagesSwitch");
                    copy = communityGuildConfig.copy((r26 & 1) != 0 ? communityGuildConfig.rulesChannel : null, (r26 & 2) != 0 ? communityGuildConfig.updatesChannel : null, (r26 & 4) != 0 ? communityGuildConfig.rulesChannelId : null, (r26 & 8) != 0 ? communityGuildConfig.updatesChannelId : null, (r26 & 16) != 0 ? communityGuildConfig.isPrivacyPolicyAccepted : false, (r26 & 32) != 0 ? communityGuildConfig.defaultMessageNotifications : false, (r26 & 64) != 0 ? communityGuildConfig.verificationLevel : false, (r26 & 128) != 0 ? communityGuildConfig.explicitContentFilter : !checkedSetting.isChecked(), (r26 & 256) != 0 ? communityGuildConfig.guild : null, (r26 & 512) != 0 ? communityGuildConfig.everyonePermissions : false, (r26 & 1024) != 0 ? communityGuildConfig.features : null, (r26 & 2048) != 0 ? communityGuildConfig.roles : null);
                    return copy;
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsEnableCommunityViewModel viewModel;
                ToastManager toastManager;
                if (!loaded.getCommunityGuildConfig().getExplicitContentFilter() || !z2) {
                    viewModel = WidgetServerSettingsCommunityFirstStep.this.getViewModel();
                    viewModel.modifyGuildConfig(new AnonymousClass1());
                    return;
                }
                Context context = WidgetServerSettingsCommunityFirstStep.this.getContext();
                toastManager = WidgetServerSettingsCommunityFirstStep.this.toastManager;
                b.a.d.m.d(context, R.string.enable_community_modal_requirement_satisfied_tooltip, 0, toastManager);
            }
        });
    }

    public static final void create(Context context) {
        Companion.create(context);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsCommunitySetupFirstStepBinding getBinding() {
        return (WidgetServerSettingsCommunitySetupFirstStepBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsEnableCommunityViewModel getViewModel() {
        return (WidgetServerSettingsEnableCommunityViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.toastManager.close();
        super.onDestroyView();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable F = ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null).x(WidgetServerSettingsCommunityFirstStep$onViewBoundOrOnResume$$inlined$filterIs$1.INSTANCE).F(WidgetServerSettingsCommunityFirstStep$onViewBoundOrOnResume$$inlined$filterIs$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it is T }.map { it as T }");
        ObservableExtensionsKt.appSubscribe(F, WidgetServerSettingsCommunityFirstStep.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsCommunityFirstStep$onViewBoundOrOnResume$1(this));
    }
}
