package com.discord.widgets.servers.community;

import androidx.fragment.app.FragmentActivity;
import com.discord.widgets.servers.community.WidgetServerSettingsCommunityOverviewViewModel;
import com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunitySteps;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsCommunityOverview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event;", "event", "", "invoke", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2 extends o implements Function1<WidgetServerSettingsCommunityOverviewViewModel.Event, Unit> {
    public final /* synthetic */ WidgetServerSettingsCommunityOverview this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2(WidgetServerSettingsCommunityOverview widgetServerSettingsCommunityOverview) {
        super(1);
        this.this$0 = widgetServerSettingsCommunityOverview;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetServerSettingsCommunityOverviewViewModel.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetServerSettingsCommunityOverviewViewModel.Event event) {
        m.checkNotNullParameter(event, "event");
        if (m.areEqual(event, WidgetServerSettingsCommunityOverviewViewModel.Event.DisableCommunitySuccess.INSTANCE)) {
            this.this$0.requireActivity().finish();
            WidgetServerSettingsEnableCommunitySteps.Companion companion = WidgetServerSettingsEnableCommunitySteps.Companion;
            FragmentActivity requireActivity = this.this$0.requireActivity();
            m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            companion.create(requireActivity, this.this$0.getGuildId());
        } else if (m.areEqual(event, WidgetServerSettingsCommunityOverviewViewModel.Event.SaveSuccess.INSTANCE)) {
            b.a.d.m.i(this.this$0, R.string.server_settings_updated, 0, 4);
        } else if (m.areEqual(event, WidgetServerSettingsCommunityOverviewViewModel.Event.Error.INSTANCE)) {
            b.a.d.m.i(this.this$0, R.string.guild_settings_public_update_failed, 0, 4);
        }
    }
}
