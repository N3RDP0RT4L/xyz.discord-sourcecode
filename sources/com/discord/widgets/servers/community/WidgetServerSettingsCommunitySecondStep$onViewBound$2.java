package com.discord.widgets.servers.community;

import com.discord.api.channel.Channel;
import com.discord.stores.StoreStream;
import com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetServerSettingsCommunitySecondStep.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\b\u001a\u00020\u00052\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\u0006\u0010\u0004\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "channelId", "", "<anonymous parameter 1>", "", "invoke", "(JLjava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsCommunitySecondStep$onViewBound$2 extends o implements Function2<Long, String, Unit> {
    public final /* synthetic */ WidgetServerSettingsCommunitySecondStep this$0;

    /* compiled from: WidgetServerSettingsCommunitySecondStep.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "guildConfig", "invoke", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.community.WidgetServerSettingsCommunitySecondStep$onViewBound$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig, WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig> {
        public final /* synthetic */ Channel $channel;
        public final /* synthetic */ long $channelId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(long j, Channel channel) {
            super(1);
            this.$channelId = j;
            this.$channel = channel;
        }

        public final WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig invoke(WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig communityGuildConfig) {
            WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig copy;
            m.checkNotNullParameter(communityGuildConfig, "guildConfig");
            copy = communityGuildConfig.copy((r26 & 1) != 0 ? communityGuildConfig.rulesChannel : null, (r26 & 2) != 0 ? communityGuildConfig.updatesChannel : this.$channel, (r26 & 4) != 0 ? communityGuildConfig.rulesChannelId : null, (r26 & 8) != 0 ? communityGuildConfig.updatesChannelId : Long.valueOf(this.$channelId), (r26 & 16) != 0 ? communityGuildConfig.isPrivacyPolicyAccepted : false, (r26 & 32) != 0 ? communityGuildConfig.defaultMessageNotifications : false, (r26 & 64) != 0 ? communityGuildConfig.verificationLevel : false, (r26 & 128) != 0 ? communityGuildConfig.explicitContentFilter : false, (r26 & 256) != 0 ? communityGuildConfig.guild : null, (r26 & 512) != 0 ? communityGuildConfig.everyonePermissions : false, (r26 & 1024) != 0 ? communityGuildConfig.features : null, (r26 & 2048) != 0 ? communityGuildConfig.roles : null);
            return copy;
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsCommunitySecondStep$onViewBound$2(WidgetServerSettingsCommunitySecondStep widgetServerSettingsCommunitySecondStep) {
        super(2);
        this.this$0 = widgetServerSettingsCommunitySecondStep;
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(Long l, String str) {
        invoke(l.longValue(), str);
        return Unit.a;
    }

    public final void invoke(long j, String str) {
        WidgetServerSettingsEnableCommunityViewModel viewModel;
        m.checkNotNullParameter(str, "<anonymous parameter 1>");
        Channel channel = StoreStream.Companion.getChannels().getChannel(j);
        viewModel = this.this$0.getViewModel();
        viewModel.modifyGuildConfig(new AnonymousClass1(j, channel));
    }
}
