package com.discord.widgets.servers.community;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.discord.databinding.WidgetServerSettingsEnableCommunityBinding;
import com.discord.views.ScreenTitleView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsEnableCommunity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetServerSettingsEnableCommunityBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsEnableCommunityBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettingsEnableCommunity$binding$2 extends k implements Function1<View, WidgetServerSettingsEnableCommunityBinding> {
    public static final WidgetServerSettingsEnableCommunity$binding$2 INSTANCE = new WidgetServerSettingsEnableCommunity$binding$2();

    public WidgetServerSettingsEnableCommunity$binding$2() {
        super(1, WidgetServerSettingsEnableCommunityBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsEnableCommunityBinding;", 0);
    }

    public final WidgetServerSettingsEnableCommunityBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.community_get_started_header;
        ScreenTitleView screenTitleView = (ScreenTitleView) view.findViewById(R.id.community_get_started_header);
        if (screenTitleView != null) {
            i = R.id.community_get_started_header_image;
            ImageView imageView = (ImageView) view.findViewById(R.id.community_get_started_header_image);
            if (imageView != null) {
                ScrollView scrollView = (ScrollView) view;
                i = R.id.community_header_image;
                RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.community_header_image);
                if (relativeLayout != null) {
                    i = R.id.community_setting_get_started;
                    MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.community_setting_get_started);
                    if (materialButton != null) {
                        return new WidgetServerSettingsEnableCommunityBinding(scrollView, screenTitleView, imageView, scrollView, relativeLayout, materialButton);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
