package com.discord.widgets.servers.community;

import com.discord.databinding.WidgetEnableCommunityStepsBinding;
import com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetServerSettingsEnableCommunitySteps.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;", "it", "", "invoke", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2 extends o implements Function1<WidgetServerSettingsEnableCommunityViewModel.ViewState.Loaded, Unit> {
    public final /* synthetic */ WidgetServerSettingsEnableCommunitySteps this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2(WidgetServerSettingsEnableCommunitySteps widgetServerSettingsEnableCommunitySteps) {
        super(1);
        this.this$0 = widgetServerSettingsEnableCommunitySteps;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetServerSettingsEnableCommunityViewModel.ViewState.Loaded loaded) {
        invoke2(loaded);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetServerSettingsEnableCommunityViewModel.ViewState.Loaded loaded) {
        WidgetEnableCommunityStepsBinding binding;
        WidgetEnableCommunityStepsBinding binding2;
        WidgetEnableCommunityStepsBinding binding3;
        WidgetEnableCommunityStepsBinding binding4;
        m.checkNotNullParameter(loaded, "it");
        int currentPage = loaded.getCurrentPage();
        if (currentPage < 0) {
            this.this$0.requireActivity().finish();
        } else {
            binding4 = this.this$0.getBinding();
            binding4.f2359b.b(currentPage);
        }
        binding = this.this$0.getBinding();
        binding.f2359b.setIsNextButtonEnabled(loaded.getCommunityGuildConfig().getExplicitContentFilter() && loaded.getCommunityGuildConfig().getVerificationLevel());
        binding2 = this.this$0.getBinding();
        binding2.f2359b.setIsDoneButtonEnabled(loaded.getCommunityGuildConfig().isPrivacyPolicyAccepted());
        binding3 = this.this$0.getBinding();
        binding3.f2359b.setIsLoading(loaded.isLoading());
    }
}
