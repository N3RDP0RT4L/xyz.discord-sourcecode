package com.discord.widgets.servers.community;

import com.discord.api.guild.Guild;
import com.discord.api.role.GuildRole;
import com.discord.restapi.RestAPIParams;
import com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.objectweb.asm.Opcodes;
import rx.subjects.PublishSubject;
/* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guild/Guild;", "it", "", "invoke", "(Lcom/discord/api/guild/Guild;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEnableCommunityViewModel$updateGuild$2 extends o implements Function1<Guild, Unit> {
    public final /* synthetic */ WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig $currentConfig;
    public final /* synthetic */ WidgetServerSettingsEnableCommunityViewModel.ViewState.Loaded $currentViewState;
    public final /* synthetic */ WidgetServerSettingsEnableCommunityViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsEnableCommunityViewModel$updateGuild$2(WidgetServerSettingsEnableCommunityViewModel widgetServerSettingsEnableCommunityViewModel, WidgetServerSettingsEnableCommunityViewModel.CommunityGuildConfig communityGuildConfig, WidgetServerSettingsEnableCommunityViewModel.ViewState.Loaded loaded) {
        super(1);
        this.this$0 = widgetServerSettingsEnableCommunityViewModel;
        this.$currentConfig = communityGuildConfig;
        this.$currentViewState = loaded;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Guild guild) {
        invoke2(guild);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Guild guild) {
        PublishSubject publishSubject;
        GuildRole guildRole;
        m.checkNotNullParameter(guild, "it");
        if (this.$currentConfig.getEveryonePermissions()) {
            Map<Long, GuildRole> roles = this.$currentConfig.getRoles();
            this.this$0.patchRole(new RestAPIParams.Role(null, null, null, null, null, Long.valueOf((-1116960071743L) & ((roles == null || (guildRole = roles.get(Long.valueOf(this.this$0.getGuildId()))) == null) ? 0L : guildRole.h())), guild.r(), null, Opcodes.IF_ICMPEQ, null));
            return;
        }
        this.this$0.updateViewState(WidgetServerSettingsEnableCommunityViewModel.ViewState.Loaded.copy$default(this.$currentViewState, 0, false, null, 5, null));
        publishSubject = this.this$0.eventSubject;
        publishSubject.k.onNext(WidgetServerSettingsEnableCommunityViewModel.Event.SaveSuccess.INSTANCE);
    }
}
