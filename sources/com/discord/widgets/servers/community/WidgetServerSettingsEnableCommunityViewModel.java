package com.discord.widgets.servers.community;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.GuildExplicitContentFilter;
import com.discord.api.guild.GuildFeature;
import com.discord.api.guild.GuildVerificationLevel;
import com.discord.api.role.GuildRole;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel;
import d0.t.u;
import d0.z.d.k;
import d0.z.d.m;
import j0.k.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func5;
import rx.subjects.PublishSubject;
/* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 -2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0005.-/01B#\u0012\n\u0010&\u001a\u00060$j\u0002`%\u0012\u000e\b\u0002\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00030\u0011¢\u0006\u0004\b+\u0010,J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\t\u001a\u00020\bH\u0003¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\r\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0013\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011¢\u0006\u0004\b\u0013\u0010\u0014J#\u0010\u0018\u001a\u00020\u00052\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00160\u0015H\u0007¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001aH\u0007¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\u0005H\u0007¢\u0006\u0004\b\u001e\u0010\u0010J\r\u0010\u001f\u001a\u00020\u0005¢\u0006\u0004\b\u001f\u0010\u0010R:\u0010\"\u001a&\u0012\f\u0012\n !*\u0004\u0018\u00010\u00120\u0012 !*\u0012\u0012\f\u0012\n !*\u0004\u0018\u00010\u00120\u0012\u0018\u00010 0 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u001d\u0010&\u001a\u00060$j\u0002`%8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)¨\u00062"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;)V", "Lcom/discord/restapi/RestAPIParams$UpdateGuild;", "getUpdatedGuildParams", "()Lcom/discord/restapi/RestAPIParams$UpdateGuild;", "Lcom/discord/restapi/RestAPIParams$Role;", "roleParams", "patchRole", "(Lcom/discord/restapi/RestAPIParams$Role;)V", "handleGuildUpdateError", "()V", "Lrx/Observable;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Event;", "observeEvents", "()Lrx/Observable;", "Lkotlin/Function1;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "transform", "modifyGuildConfig", "(Lkotlin/jvm/functions/Function1;)V", "", "currentPage", "updateCurrentPage", "(I)V", "goBackToPreviousPage", "updateGuild", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "", "Lcom/discord/primitives/GuildId;", "guildId", "J", "getGuildId", "()J", "storeStateObservable", HookHelper.constructorName, "(JLrx/Observable;)V", "Companion", "CommunityGuildConfig", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEnableCommunityViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private final long guildId;

    /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;", "p1", "", "invoke", "(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<StoreState, Unit> {
        public AnonymousClass1(WidgetServerSettingsEnableCommunityViewModel widgetServerSettingsEnableCommunityViewModel) {
            super(1, widgetServerSettingsEnableCommunityViewModel, WidgetServerSettingsEnableCommunityViewModel.class, "handleStoreState", "handleStoreState(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "p1");
            ((WidgetServerSettingsEnableCommunityViewModel) this.receiver).handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0019\b\u0086\b\u0018\u00002\u00020\u0001B\u008b\u0001\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010 \u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010!\u001a\u00020\n\u0012\u0006\u0010\"\u001a\u00020\n\u0012\u0006\u0010#\u001a\u00020\n\u0012\u0006\u0010$\u001a\u00020\n\u0012\b\u0010%\u001a\u0004\u0018\u00010\u0010\u0012\u0006\u0010&\u001a\u00020\n\u0012\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u0018\u0010(\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0018¢\u0006\u0004\bE\u0010FJ\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\r\u0010\fJ\u0010\u0010\u000e\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000e\u0010\fJ\u0010\u0010\u000f\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000f\u0010\fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u0013\u0010\fJ\u0016\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\"\u0010\u001b\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0018HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJª\u0001\u0010)\u001a\u00020\u00002\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010!\u001a\u00020\n2\b\b\u0002\u0010\"\u001a\u00020\n2\b\b\u0002\u0010#\u001a\u00020\n2\b\b\u0002\u0010$\u001a\u00020\n2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u00102\b\b\u0002\u0010&\u001a\u00020\n2\u000e\b\u0002\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\u001a\b\u0002\u0010(\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0018HÆ\u0001¢\u0006\u0004\b)\u0010*J\u0010\u0010,\u001a\u00020+HÖ\u0001¢\u0006\u0004\b,\u0010-J\u0010\u0010/\u001a\u00020.HÖ\u0001¢\u0006\u0004\b/\u00100J\u001a\u00102\u001a\u00020\n2\b\u00101\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b2\u00103R\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00104\u001a\u0004\b5\u0010\u0004R\u0019\u0010!\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00106\u001a\u0004\b!\u0010\fR\u0019\u0010#\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b#\u00106\u001a\u0004\b7\u0010\fR\u0019\u0010$\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b$\u00106\u001a\u0004\b8\u0010\fR\u001b\u0010%\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b%\u00109\u001a\u0004\b:\u0010\u0012R\u001b\u0010\u001f\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010;\u001a\u0004\b<\u0010\bR\u0019\u0010&\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b&\u00106\u001a\u0004\b=\u0010\fR\u0019\u0010\"\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u00106\u001a\u0004\b>\u0010\fR\u001f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u00150\u00148\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010?\u001a\u0004\b@\u0010\u0017R\u001b\u0010\u001d\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00104\u001a\u0004\bA\u0010\u0004R\u001b\u0010 \u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b \u0010;\u001a\u0004\bB\u0010\bR+\u0010(\u001a\u0014\u0012\b\u0012\u00060\u0006j\u0002`\u0019\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00188\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010C\u001a\u0004\bD\u0010\u001c¨\u0006G"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "component2", "", "component3", "()Ljava/lang/Long;", "component4", "", "component5", "()Z", "component6", "component7", "component8", "Lcom/discord/models/guild/Guild;", "component9", "()Lcom/discord/models/guild/Guild;", "component10", "", "Lcom/discord/api/guild/GuildFeature;", "component11", "()Ljava/util/List;", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/api/role/GuildRole;", "component12", "()Ljava/util/Map;", "rulesChannel", "updatesChannel", "rulesChannelId", "updatesChannelId", "isPrivacyPolicyAccepted", "defaultMessageNotifications", "verificationLevel", "explicitContentFilter", "guild", "everyonePermissions", "features", "roles", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Ljava/lang/Long;Ljava/lang/Long;ZZZZLcom/discord/models/guild/Guild;ZLjava/util/List;Ljava/util/Map;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getUpdatesChannel", "Z", "getVerificationLevel", "getExplicitContentFilter", "Lcom/discord/models/guild/Guild;", "getGuild", "Ljava/lang/Long;", "getRulesChannelId", "getEveryonePermissions", "getDefaultMessageNotifications", "Ljava/util/List;", "getFeatures", "getRulesChannel", "getUpdatesChannelId", "Ljava/util/Map;", "getRoles", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;Ljava/lang/Long;Ljava/lang/Long;ZZZZLcom/discord/models/guild/Guild;ZLjava/util/List;Ljava/util/Map;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CommunityGuildConfig {
        private final boolean defaultMessageNotifications;
        private final boolean everyonePermissions;
        private final boolean explicitContentFilter;
        private final List<GuildFeature> features;
        private final Guild guild;
        private final boolean isPrivacyPolicyAccepted;
        private final Map<Long, GuildRole> roles;
        private final Channel rulesChannel;
        private final Long rulesChannelId;
        private final Channel updatesChannel;
        private final Long updatesChannelId;
        private final boolean verificationLevel;

        /* JADX WARN: Multi-variable type inference failed */
        public CommunityGuildConfig(Channel channel, Channel channel2, Long l, Long l2, boolean z2, boolean z3, boolean z4, boolean z5, Guild guild, boolean z6, List<? extends GuildFeature> list, Map<Long, GuildRole> map) {
            m.checkNotNullParameter(list, "features");
            this.rulesChannel = channel;
            this.updatesChannel = channel2;
            this.rulesChannelId = l;
            this.updatesChannelId = l2;
            this.isPrivacyPolicyAccepted = z2;
            this.defaultMessageNotifications = z3;
            this.verificationLevel = z4;
            this.explicitContentFilter = z5;
            this.guild = guild;
            this.everyonePermissions = z6;
            this.features = list;
            this.roles = map;
        }

        public final Channel component1() {
            return this.rulesChannel;
        }

        public final boolean component10() {
            return this.everyonePermissions;
        }

        public final List<GuildFeature> component11() {
            return this.features;
        }

        public final Map<Long, GuildRole> component12() {
            return this.roles;
        }

        public final Channel component2() {
            return this.updatesChannel;
        }

        public final Long component3() {
            return this.rulesChannelId;
        }

        public final Long component4() {
            return this.updatesChannelId;
        }

        public final boolean component5() {
            return this.isPrivacyPolicyAccepted;
        }

        public final boolean component6() {
            return this.defaultMessageNotifications;
        }

        public final boolean component7() {
            return this.verificationLevel;
        }

        public final boolean component8() {
            return this.explicitContentFilter;
        }

        public final Guild component9() {
            return this.guild;
        }

        public final CommunityGuildConfig copy(Channel channel, Channel channel2, Long l, Long l2, boolean z2, boolean z3, boolean z4, boolean z5, Guild guild, boolean z6, List<? extends GuildFeature> list, Map<Long, GuildRole> map) {
            m.checkNotNullParameter(list, "features");
            return new CommunityGuildConfig(channel, channel2, l, l2, z2, z3, z4, z5, guild, z6, list, map);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CommunityGuildConfig)) {
                return false;
            }
            CommunityGuildConfig communityGuildConfig = (CommunityGuildConfig) obj;
            return m.areEqual(this.rulesChannel, communityGuildConfig.rulesChannel) && m.areEqual(this.updatesChannel, communityGuildConfig.updatesChannel) && m.areEqual(this.rulesChannelId, communityGuildConfig.rulesChannelId) && m.areEqual(this.updatesChannelId, communityGuildConfig.updatesChannelId) && this.isPrivacyPolicyAccepted == communityGuildConfig.isPrivacyPolicyAccepted && this.defaultMessageNotifications == communityGuildConfig.defaultMessageNotifications && this.verificationLevel == communityGuildConfig.verificationLevel && this.explicitContentFilter == communityGuildConfig.explicitContentFilter && m.areEqual(this.guild, communityGuildConfig.guild) && this.everyonePermissions == communityGuildConfig.everyonePermissions && m.areEqual(this.features, communityGuildConfig.features) && m.areEqual(this.roles, communityGuildConfig.roles);
        }

        public final boolean getDefaultMessageNotifications() {
            return this.defaultMessageNotifications;
        }

        public final boolean getEveryonePermissions() {
            return this.everyonePermissions;
        }

        public final boolean getExplicitContentFilter() {
            return this.explicitContentFilter;
        }

        public final List<GuildFeature> getFeatures() {
            return this.features;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final Map<Long, GuildRole> getRoles() {
            return this.roles;
        }

        public final Channel getRulesChannel() {
            return this.rulesChannel;
        }

        public final Long getRulesChannelId() {
            return this.rulesChannelId;
        }

        public final Channel getUpdatesChannel() {
            return this.updatesChannel;
        }

        public final Long getUpdatesChannelId() {
            return this.updatesChannelId;
        }

        public final boolean getVerificationLevel() {
            return this.verificationLevel;
        }

        public int hashCode() {
            Channel channel = this.rulesChannel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            Channel channel2 = this.updatesChannel;
            int hashCode2 = (hashCode + (channel2 != null ? channel2.hashCode() : 0)) * 31;
            Long l = this.rulesChannelId;
            int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
            Long l2 = this.updatesChannelId;
            int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
            boolean z2 = this.isPrivacyPolicyAccepted;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode4 + i3) * 31;
            boolean z3 = this.defaultMessageNotifications;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            boolean z4 = this.verificationLevel;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int i11 = (i8 + i9) * 31;
            boolean z5 = this.explicitContentFilter;
            if (z5) {
                z5 = true;
            }
            int i12 = z5 ? 1 : 0;
            int i13 = z5 ? 1 : 0;
            int i14 = (i11 + i12) * 31;
            Guild guild = this.guild;
            int hashCode5 = (i14 + (guild != null ? guild.hashCode() : 0)) * 31;
            boolean z6 = this.everyonePermissions;
            if (!z6) {
                i2 = z6 ? 1 : 0;
            }
            int i15 = (hashCode5 + i2) * 31;
            List<GuildFeature> list = this.features;
            int hashCode6 = (i15 + (list != null ? list.hashCode() : 0)) * 31;
            Map<Long, GuildRole> map = this.roles;
            if (map != null) {
                i = map.hashCode();
            }
            return hashCode6 + i;
        }

        public final boolean isPrivacyPolicyAccepted() {
            return this.isPrivacyPolicyAccepted;
        }

        public String toString() {
            StringBuilder R = a.R("CommunityGuildConfig(rulesChannel=");
            R.append(this.rulesChannel);
            R.append(", updatesChannel=");
            R.append(this.updatesChannel);
            R.append(", rulesChannelId=");
            R.append(this.rulesChannelId);
            R.append(", updatesChannelId=");
            R.append(this.updatesChannelId);
            R.append(", isPrivacyPolicyAccepted=");
            R.append(this.isPrivacyPolicyAccepted);
            R.append(", defaultMessageNotifications=");
            R.append(this.defaultMessageNotifications);
            R.append(", verificationLevel=");
            R.append(this.verificationLevel);
            R.append(", explicitContentFilter=");
            R.append(this.explicitContentFilter);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", everyonePermissions=");
            R.append(this.everyonePermissions);
            R.append(", features=");
            R.append(this.features);
            R.append(", roles=");
            return a.L(R, this.roles, ")");
        }

        public /* synthetic */ CommunityGuildConfig(Channel channel, Channel channel2, Long l, Long l2, boolean z2, boolean z3, boolean z4, boolean z5, Guild guild, boolean z6, List list, Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(channel, channel2, l, l2, z2, z3, z4, z5, guild, z6, (i & 1024) != 0 ? new ArrayList() : list, map);
        }
    }

    /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012JI\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StorePermissions;", "storePermissions", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lrx/Observable;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final Observable<StoreState> observeStoreState(final long j, StoreGuilds storeGuilds, final StoreChannels storeChannels, final StorePermissions storePermissions, final StoreUser storeUser) {
            Observable Y = storeGuilds.observeGuild(j).Y(new b<Guild, Observable<? extends StoreState>>() { // from class: com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$Companion$observeStoreState$1
                public final Observable<? extends WidgetServerSettingsEnableCommunityViewModel.StoreState> call(final Guild guild) {
                    if (guild == null) {
                        return new j0.l.e.k(WidgetServerSettingsEnableCommunityViewModel.StoreState.Invalid.INSTANCE);
                    }
                    Observable observeMe$default = StoreUser.observeMe$default(StoreUser.this, false, 1, null);
                    Observable<Long> observePermissionsForGuild = storePermissions.observePermissionsForGuild(j);
                    Observable<Map<Long, GuildRole>> observeRoles = StoreStream.Companion.getGuilds().observeRoles(j);
                    StoreChannels storeChannels2 = storeChannels;
                    Long rulesChannelId = guild.getRulesChannelId();
                    long j2 = 0;
                    Observable<Channel> observeChannel = storeChannels2.observeChannel(rulesChannelId != null ? rulesChannelId.longValue() : 0L);
                    StoreChannels storeChannels3 = storeChannels;
                    Long publicUpdatesChannelId = guild.getPublicUpdatesChannelId();
                    if (publicUpdatesChannelId != null) {
                        j2 = publicUpdatesChannelId.longValue();
                    }
                    return Observable.g(observeMe$default, observePermissionsForGuild, observeRoles, observeChannel, storeChannels3.observeChannel(j2), new Func5<MeUser, Long, Map<Long, ? extends GuildRole>, Channel, Channel, WidgetServerSettingsEnableCommunityViewModel.StoreState.Valid>() { // from class: com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunityViewModel$Companion$observeStoreState$1.1
                        @Override // rx.functions.Func5
                        public /* bridge */ /* synthetic */ WidgetServerSettingsEnableCommunityViewModel.StoreState.Valid call(MeUser meUser, Long l, Map<Long, ? extends GuildRole> map, Channel channel, Channel channel2) {
                            return call2(meUser, l, (Map<Long, GuildRole>) map, channel, channel2);
                        }

                        /* renamed from: call  reason: avoid collision after fix types in other method */
                        public final WidgetServerSettingsEnableCommunityViewModel.StoreState.Valid call2(MeUser meUser, Long l, Map<Long, GuildRole> map, Channel channel, Channel channel2) {
                            Guild guild2 = Guild.this;
                            m.checkNotNullExpressionValue(meUser, "me");
                            return new WidgetServerSettingsEnableCommunityViewModel.StoreState.Valid(guild2, meUser, l, map, channel, channel2);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "storeGuilds.observeGuild…      }\n        }\n      }");
            return Y;
        }

        public static /* synthetic */ Observable observeStoreState$default(Companion companion, long j, StoreGuilds storeGuilds, StoreChannels storeChannels, StorePermissions storePermissions, StoreUser storeUser, int i, Object obj) {
            if ((i & 2) != 0) {
                storeGuilds = StoreStream.Companion.getGuilds();
            }
            StoreGuilds storeGuilds2 = storeGuilds;
            if ((i & 4) != 0) {
                storeChannels = StoreStream.Companion.getChannels();
            }
            StoreChannels storeChannels2 = storeChannels;
            if ((i & 8) != 0) {
                storePermissions = StoreStream.Companion.getPermissions();
            }
            StorePermissions storePermissions2 = storePermissions;
            if ((i & 16) != 0) {
                storeUser = StoreStream.Companion.getUsers();
            }
            return companion.observeStoreState(j, storeGuilds2, storeChannels2, storePermissions2, storeUser);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Event;", "", HookHelper.constructorName, "()V", "Error", "SaveSuccess", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Event$SaveSuccess;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Event$Error;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Event$Error;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Error extends Event {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Event$SaveSuccess;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class SaveSuccess extends Event {
            public static final SaveSuccess INSTANCE = new SaveSuccess();

            private SaveSuccess() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Invalid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Invalid;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends StoreState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BU\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u000e\u0010\u0017\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t\u0012\u0018\u0010\u0018\u001a\u0014\u0012\b\u0012\u00060\bj\u0002`\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\f\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0011\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b3\u00104J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\"\u0010\u000f\u001a\u0014\u0012\b\u0012\u00060\bj\u0002`\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0011HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0013Jj\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00052\u0010\b\u0002\u0010\u0017\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\u001a\b\u0002\u0010\u0018\u001a\u0014\u0012\b\u0012\u00060\bj\u0002`\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\f2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00112\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0011HÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u001a\u0010&\u001a\u00020%2\b\u0010$\u001a\u0004\u0018\u00010#HÖ\u0003¢\u0006\u0004\b&\u0010'R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010(\u001a\u0004\b)\u0010\u0013R+\u0010\u0018\u001a\u0014\u0012\b\u0012\u00060\bj\u0002`\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010*\u001a\u0004\b+\u0010\u0010R\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010,\u001a\u0004\b-\u0010\u0004R\u0019\u0010\u0016\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010.\u001a\u0004\b/\u0010\u0007R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010(\u001a\u0004\b0\u0010\u0013R!\u0010\u0017\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u00101\u001a\u0004\b2\u0010\u000b¨\u00065"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/user/MeUser;", "component2", "()Lcom/discord/models/user/MeUser;", "", "Lcom/discord/api/permission/PermissionBit;", "component3", "()Ljava/lang/Long;", "", "Lcom/discord/primitives/GuildId;", "Lcom/discord/api/role/GuildRole;", "component4", "()Ljava/util/Map;", "Lcom/discord/api/channel/Channel;", "component5", "()Lcom/discord/api/channel/Channel;", "component6", "guild", "me", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS, "roles", "rulesChannel", "updatesChannel", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getUpdatesChannel", "Ljava/util/Map;", "getRoles", "Lcom/discord/models/guild/Guild;", "getGuild", "Lcom/discord/models/user/MeUser;", "getMe", "getRulesChannel", "Ljava/lang/Long;", "getPermissions", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/models/user/MeUser;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends StoreState {
            private final Guild guild;

            /* renamed from: me  reason: collision with root package name */
            private final MeUser f2838me;
            private final Long permissions;
            private final Map<Long, GuildRole> roles;
            private final Channel rulesChannel;
            private final Channel updatesChannel;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Valid(Guild guild, MeUser meUser, Long l, Map<Long, GuildRole> map, Channel channel, Channel channel2) {
                super(null);
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(meUser, "me");
                this.guild = guild;
                this.f2838me = meUser;
                this.permissions = l;
                this.roles = map;
                this.rulesChannel = channel;
                this.updatesChannel = channel2;
            }

            public static /* synthetic */ Valid copy$default(Valid valid, Guild guild, MeUser meUser, Long l, Map map, Channel channel, Channel channel2, int i, Object obj) {
                if ((i & 1) != 0) {
                    guild = valid.guild;
                }
                if ((i & 2) != 0) {
                    meUser = valid.f2838me;
                }
                MeUser meUser2 = meUser;
                if ((i & 4) != 0) {
                    l = valid.permissions;
                }
                Long l2 = l;
                Map<Long, GuildRole> map2 = map;
                if ((i & 8) != 0) {
                    map2 = valid.roles;
                }
                Map map3 = map2;
                if ((i & 16) != 0) {
                    channel = valid.rulesChannel;
                }
                Channel channel3 = channel;
                if ((i & 32) != 0) {
                    channel2 = valid.updatesChannel;
                }
                return valid.copy(guild, meUser2, l2, map3, channel3, channel2);
            }

            public final Guild component1() {
                return this.guild;
            }

            public final MeUser component2() {
                return this.f2838me;
            }

            public final Long component3() {
                return this.permissions;
            }

            public final Map<Long, GuildRole> component4() {
                return this.roles;
            }

            public final Channel component5() {
                return this.rulesChannel;
            }

            public final Channel component6() {
                return this.updatesChannel;
            }

            public final Valid copy(Guild guild, MeUser meUser, Long l, Map<Long, GuildRole> map, Channel channel, Channel channel2) {
                m.checkNotNullParameter(guild, "guild");
                m.checkNotNullParameter(meUser, "me");
                return new Valid(guild, meUser, l, map, channel, channel2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.guild, valid.guild) && m.areEqual(this.f2838me, valid.f2838me) && m.areEqual(this.permissions, valid.permissions) && m.areEqual(this.roles, valid.roles) && m.areEqual(this.rulesChannel, valid.rulesChannel) && m.areEqual(this.updatesChannel, valid.updatesChannel);
            }

            public final Guild getGuild() {
                return this.guild;
            }

            public final MeUser getMe() {
                return this.f2838me;
            }

            public final Long getPermissions() {
                return this.permissions;
            }

            public final Map<Long, GuildRole> getRoles() {
                return this.roles;
            }

            public final Channel getRulesChannel() {
                return this.rulesChannel;
            }

            public final Channel getUpdatesChannel() {
                return this.updatesChannel;
            }

            public int hashCode() {
                Guild guild = this.guild;
                int i = 0;
                int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
                MeUser meUser = this.f2838me;
                int hashCode2 = (hashCode + (meUser != null ? meUser.hashCode() : 0)) * 31;
                Long l = this.permissions;
                int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
                Map<Long, GuildRole> map = this.roles;
                int hashCode4 = (hashCode3 + (map != null ? map.hashCode() : 0)) * 31;
                Channel channel = this.rulesChannel;
                int hashCode5 = (hashCode4 + (channel != null ? channel.hashCode() : 0)) * 31;
                Channel channel2 = this.updatesChannel;
                if (channel2 != null) {
                    i = channel2.hashCode();
                }
                return hashCode5 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(guild=");
                R.append(this.guild);
                R.append(", me=");
                R.append(this.f2838me);
                R.append(", permissions=");
                R.append(this.permissions);
                R.append(", roles=");
                R.append(this.roles);
                R.append(", rulesChannel=");
                R.append(this.rulesChannel);
                R.append(", updatesChannel=");
                R.append(this.updatesChannel);
                R.append(")");
                return R.toString();
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Loaded", "Uninitialized", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Invalid;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Invalid;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\b¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0004J\u001a\u0010\u0016\u001a\u00020\u00052\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0018\u001a\u0004\b\f\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\n¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState;", "", "component1", "()I", "", "component2", "()Z", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "component3", "()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "currentPage", "isLoading", "communityGuildConfig", "copy", "(IZLcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "I", "getCurrentPage", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;", "getCommunityGuildConfig", HookHelper.constructorName, "(IZLcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loaded extends ViewState {
            private final CommunityGuildConfig communityGuildConfig;
            private final int currentPage;
            private final boolean isLoading;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(int i, boolean z2, CommunityGuildConfig communityGuildConfig) {
                super(null);
                m.checkNotNullParameter(communityGuildConfig, "communityGuildConfig");
                this.currentPage = i;
                this.isLoading = z2;
                this.communityGuildConfig = communityGuildConfig;
            }

            public static /* synthetic */ Loaded copy$default(Loaded loaded, int i, boolean z2, CommunityGuildConfig communityGuildConfig, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = loaded.currentPage;
                }
                if ((i2 & 2) != 0) {
                    z2 = loaded.isLoading;
                }
                if ((i2 & 4) != 0) {
                    communityGuildConfig = loaded.communityGuildConfig;
                }
                return loaded.copy(i, z2, communityGuildConfig);
            }

            public final int component1() {
                return this.currentPage;
            }

            public final boolean component2() {
                return this.isLoading;
            }

            public final CommunityGuildConfig component3() {
                return this.communityGuildConfig;
            }

            public final Loaded copy(int i, boolean z2, CommunityGuildConfig communityGuildConfig) {
                m.checkNotNullParameter(communityGuildConfig, "communityGuildConfig");
                return new Loaded(i, z2, communityGuildConfig);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Loaded)) {
                    return false;
                }
                Loaded loaded = (Loaded) obj;
                return this.currentPage == loaded.currentPage && this.isLoading == loaded.isLoading && m.areEqual(this.communityGuildConfig, loaded.communityGuildConfig);
            }

            public final CommunityGuildConfig getCommunityGuildConfig() {
                return this.communityGuildConfig;
            }

            public final int getCurrentPage() {
                return this.currentPage;
            }

            public int hashCode() {
                int i = this.currentPage * 31;
                boolean z2 = this.isLoading;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (i + i2) * 31;
                CommunityGuildConfig communityGuildConfig = this.communityGuildConfig;
                return i4 + (communityGuildConfig != null ? communityGuildConfig.hashCode() : 0);
            }

            public final boolean isLoading() {
                return this.isLoading;
            }

            public String toString() {
                StringBuilder R = a.R("Loaded(currentPage=");
                R.append(this.currentPage);
                R.append(", isLoading=");
                R.append(this.isLoading);
                R.append(", communityGuildConfig=");
                R.append(this.communityGuildConfig);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public /* synthetic */ WidgetServerSettingsEnableCommunityViewModel(long j, Observable observable, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i & 2) != 0 ? Companion.observeStoreState$default(Companion, j, null, null, null, null, 30, null) : observable);
    }

    @MainThread
    private final RestAPIParams.UpdateGuild getUpdatedGuildParams() {
        GuildVerificationLevel guildVerificationLevel;
        GuildVerificationLevel guildVerificationLevel2;
        GuildExplicitContentFilter guildExplicitContentFilter;
        GuildExplicitContentFilter guildExplicitContentFilter2;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded == null) {
            return new RestAPIParams.UpdateGuild(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 32767, null);
        }
        CommunityGuildConfig communityGuildConfig = loaded.getCommunityGuildConfig();
        List plus = u.plus((Collection<? extends GuildFeature>) communityGuildConfig.getFeatures(), GuildFeature.COMMUNITY);
        Guild guild = communityGuildConfig.getGuild();
        if (guild == null || (guildVerificationLevel = guild.getVerificationLevel()) == null) {
            guildVerificationLevel = GuildVerificationLevel.NONE;
        }
        GuildVerificationLevel guildVerificationLevel3 = GuildVerificationLevel.NONE;
        if (guildVerificationLevel.compareTo(guildVerificationLevel3) <= 0 || communityGuildConfig.getVerificationLevel()) {
            if (guildVerificationLevel == guildVerificationLevel3 && communityGuildConfig.getVerificationLevel()) {
                guildVerificationLevel = GuildVerificationLevel.LOW;
            }
            guildVerificationLevel2 = guildVerificationLevel;
        } else {
            guildVerificationLevel2 = guildVerificationLevel3;
        }
        Guild guild2 = communityGuildConfig.getGuild();
        if (guild2 == null || (guildExplicitContentFilter = guild2.getExplicitContentFilter()) == null) {
            guildExplicitContentFilter = GuildExplicitContentFilter.NONE;
        }
        GuildExplicitContentFilter guildExplicitContentFilter3 = GuildExplicitContentFilter.NONE;
        if (guildExplicitContentFilter.compareTo(guildExplicitContentFilter3) <= 0 || communityGuildConfig.getExplicitContentFilter()) {
            if (guildExplicitContentFilter == guildExplicitContentFilter3 && communityGuildConfig.getExplicitContentFilter()) {
                guildExplicitContentFilter = GuildExplicitContentFilter.ALL;
            }
            guildExplicitContentFilter2 = guildExplicitContentFilter;
        } else {
            guildExplicitContentFilter2 = guildExplicitContentFilter3;
        }
        boolean defaultMessageNotifications = communityGuildConfig.getDefaultMessageNotifications();
        int i = 1;
        if (!defaultMessageNotifications) {
            if (!defaultMessageNotifications) {
                i = 0;
            } else {
                throw new NoWhenBranchMatchedException();
            }
        }
        return new RestAPIParams.UpdateGuild(null, null, null, Integer.valueOf(i), null, null, guildVerificationLevel2, guildExplicitContentFilter2, null, null, null, plus, communityGuildConfig.getRulesChannelId(), communityGuildConfig.getUpdatesChannelId(), null, 18231, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleGuildUpdateError() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, 0, false, null, 5, null));
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(Event.Error.INSTANCE);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        GuildRole guildRole;
        GuildRole guildRole2;
        if (!(storeState instanceof StoreState.Valid)) {
            updateViewState(ViewState.Invalid.INSTANCE);
            return;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        Guild guild = valid.getGuild();
        MeUser me2 = valid.getMe();
        if (!(guild.isOwner(me2.getId()) || PermissionUtils.canAndIsElevated(32L, valid.getPermissions(), me2.getMfaEnabled(), guild.getMfaLevel()))) {
            updateViewState(ViewState.Invalid.INSTANCE);
            return;
        }
        Channel rulesChannel = valid.getRulesChannel();
        Channel updatesChannel = valid.getUpdatesChannel();
        boolean z2 = guild.getDefaultMessageNotifications() == 1;
        boolean z3 = guild.getVerificationLevel().compareTo(GuildVerificationLevel.NONE) > 0;
        boolean z4 = guild.getExplicitContentFilter().compareTo(GuildExplicitContentFilter.NONE) > 0;
        Map<Long, GuildRole> roles = valid.getRoles();
        long h = (-1116960071743L) & ((roles == null || (guildRole2 = roles.get(Long.valueOf(this.guildId))) == null) ? 0L : guildRole2.h());
        Map<Long, GuildRole> roles2 = valid.getRoles();
        updateViewState(new ViewState.Loaded(0, false, new CommunityGuildConfig(rulesChannel, updatesChannel, 1L, 1L, false, z2, z3, z4, guild, (roles2 == null || (guildRole = roles2.get(Long.valueOf(this.guildId))) == null || h != guildRole.h()) ? false : true, u.toList(guild.getFeatures()), valid.getRoles())));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void patchRole(RestAPIParams.Role role) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            RestAPI api = RestAPI.Companion.getApi();
            long j = this.guildId;
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(api.updateRole(j, j, role), false, 1, null), this, null, 2, null), WidgetServerSettingsEnableCommunityViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetServerSettingsEnableCommunityViewModel$patchRole$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsEnableCommunityViewModel$patchRole$2(this, loaded));
        }
    }

    public final long getGuildId() {
        return this.guildId;
    }

    @MainThread
    public final void goBackToPreviousPage() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateCurrentPage(loaded.getCurrentPage() - 1);
        }
    }

    @MainThread
    public final void modifyGuildConfig(Function1<? super CommunityGuildConfig, CommunityGuildConfig> function1) {
        m.checkNotNullParameter(function1, "transform");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, 0, false, function1.invoke(loaded.getCommunityGuildConfig()), 3, null));
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @MainThread
    public final void updateCurrentPage(int i) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            updateViewState(ViewState.Loaded.copy$default(loaded, i, false, null, 6, null));
        }
    }

    public final void updateGuild() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Loaded)) {
            viewState = null;
        }
        ViewState.Loaded loaded = (ViewState.Loaded) viewState;
        if (loaded != null) {
            CommunityGuildConfig communityGuildConfig = loaded.getCommunityGuildConfig();
            updateViewState(ViewState.Loaded.copy$default(loaded, 0, true, null, 5, null));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().updateGuild(this.guildId, getUpdatedGuildParams()), false, 1, null), this, null, 2, null), WidgetServerSettingsEnableCommunityViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetServerSettingsEnableCommunityViewModel$updateGuild$1(this), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsEnableCommunityViewModel$updateGuild$2(this, communityGuildConfig, loaded));
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsEnableCommunityViewModel(long j, Observable<StoreState> observable) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.guildId = j;
        this.eventSubject = PublishSubject.k0();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetServerSettingsEnableCommunityViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
