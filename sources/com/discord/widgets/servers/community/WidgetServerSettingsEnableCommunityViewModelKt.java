package com.discord.widgets.servers.community;

import kotlin.Metadata;
/* compiled from: WidgetServerSettingsEnableCommunityViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\"\u0016\u0010\u0001\u001a\u00020\u00008\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0001\u0010\u0002\"\u0016\u0010\u0004\u001a\u00020\u00038\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"", "CREATE_NEW_CHANNEL_CONSTANT", "J", "", "COMMUNITY_SETUP_FIRST_STEP", "I", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEnableCommunityViewModelKt {
    public static final int COMMUNITY_SETUP_FIRST_STEP = 1;
    public static final long CREATE_NEW_CHANNEL_CONSTANT = 1;
}
