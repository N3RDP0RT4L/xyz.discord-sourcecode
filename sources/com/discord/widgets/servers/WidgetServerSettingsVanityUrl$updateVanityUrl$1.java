package com.discord.widgets.servers;

import com.discord.api.guild.VanityUrlResponse;
import com.discord.app.AppFragment;
import com.discord.utilities.stateful.StatefulViews;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetServerSettingsVanityUrl.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/guild/VanityUrlResponse;", "<name for destructuring parameter 0>", "", "invoke", "(Lcom/discord/api/guild/VanityUrlResponse;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsVanityUrl$updateVanityUrl$1 extends o implements Function1<VanityUrlResponse, Unit> {
    public final /* synthetic */ WidgetServerSettingsVanityUrl this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsVanityUrl$updateVanityUrl$1(WidgetServerSettingsVanityUrl widgetServerSettingsVanityUrl) {
        super(1);
        this.this$0 = widgetServerSettingsVanityUrl;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(VanityUrlResponse vanityUrlResponse) {
        invoke2(vanityUrlResponse);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(VanityUrlResponse vanityUrlResponse) {
        StatefulViews statefulViews;
        m.checkNotNullParameter(vanityUrlResponse, "<name for destructuring parameter 0>");
        String a = vanityUrlResponse.a();
        int b2 = vanityUrlResponse.b();
        this.this$0.showLoadingUI(false);
        AppFragment.hideKeyboard$default(this.this$0, null, 1, null);
        this.this$0.configureInviteCode(a, b2);
        statefulViews = this.this$0.state;
        StatefulViews.clear$default(statefulViews, false, 1, null);
    }
}
