package com.discord.widgets.servers;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetServerSettingsBinding;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetServerSettingsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettings$binding$2 extends k implements Function1<View, WidgetServerSettingsBinding> {
    public static final WidgetServerSettings$binding$2 INSTANCE = new WidgetServerSettings$binding$2();

    public WidgetServerSettings$binding$2() {
        super(1, WidgetServerSettingsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsBinding;", 0);
    }

    public final WidgetServerSettingsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.server_settings_bans_option;
        TextView textView = (TextView) view.findViewById(R.id.server_settings_bans_option);
        if (textView != null) {
            i = R.id.server_settings_community_overview_option;
            TextView textView2 = (TextView) view.findViewById(R.id.server_settings_community_overview_option);
            if (textView2 != null) {
                i = R.id.server_settings_emojis_option;
                TextView textView3 = (TextView) view.findViewById(R.id.server_settings_emojis_option);
                if (textView3 != null) {
                    i = R.id.server_settings_enable_community_option;
                    TextView textView4 = (TextView) view.findViewById(R.id.server_settings_enable_community_option);
                    if (textView4 != null) {
                        i = R.id.server_settings_guild_role_subscription_basic_info;
                        TextView textView5 = (TextView) view.findViewById(R.id.server_settings_guild_role_subscription_basic_info);
                        if (textView5 != null) {
                            i = R.id.server_settings_guild_role_subscription_earnings;
                            TextView textView6 = (TextView) view.findViewById(R.id.server_settings_guild_role_subscription_earnings);
                            if (textView6 != null) {
                                i = R.id.server_settings_guild_role_subscription_get_started;
                                TextView textView7 = (TextView) view.findViewById(R.id.server_settings_guild_role_subscription_get_started);
                                if (textView7 != null) {
                                    i = R.id.server_settings_guild_role_subscription_tiers;
                                    TextView textView8 = (TextView) view.findViewById(R.id.server_settings_guild_role_subscription_tiers);
                                    if (textView8 != null) {
                                        i = R.id.server_settings_instant_invites_option;
                                        TextView textView9 = (TextView) view.findViewById(R.id.server_settings_instant_invites_option);
                                        if (textView9 != null) {
                                            i = R.id.server_settings_members_option;
                                            TextView textView10 = (TextView) view.findViewById(R.id.server_settings_members_option);
                                            if (textView10 != null) {
                                                i = R.id.server_settings_option_audit_log;
                                                TextView textView11 = (TextView) view.findViewById(R.id.server_settings_option_audit_log);
                                                if (textView11 != null) {
                                                    i = R.id.server_settings_option_channels;
                                                    TextView textView12 = (TextView) view.findViewById(R.id.server_settings_option_channels);
                                                    if (textView12 != null) {
                                                        i = R.id.server_settings_option_integrations;
                                                        TextView textView13 = (TextView) view.findViewById(R.id.server_settings_option_integrations);
                                                        if (textView13 != null) {
                                                            i = R.id.server_settings_option_moderation;
                                                            TextView textView14 = (TextView) view.findViewById(R.id.server_settings_option_moderation);
                                                            if (textView14 != null) {
                                                                i = R.id.server_settings_option_overview;
                                                                TextView textView15 = (TextView) view.findViewById(R.id.server_settings_option_overview);
                                                                if (textView15 != null) {
                                                                    i = R.id.server_settings_option_security;
                                                                    TextView textView16 = (TextView) view.findViewById(R.id.server_settings_option_security);
                                                                    if (textView16 != null) {
                                                                        i = R.id.server_settings_option_vanity_url;
                                                                        TextView textView17 = (TextView) view.findViewById(R.id.server_settings_option_vanity_url);
                                                                        if (textView17 != null) {
                                                                            i = R.id.server_settings_roles_option;
                                                                            TextView textView18 = (TextView) view.findViewById(R.id.server_settings_roles_option);
                                                                            if (textView18 != null) {
                                                                                i = R.id.server_settings_section_community;
                                                                                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.server_settings_section_community);
                                                                                if (linearLayout != null) {
                                                                                    i = R.id.server_settings_section_community_divider;
                                                                                    View findViewById = view.findViewById(R.id.server_settings_section_community_divider);
                                                                                    if (findViewById != null) {
                                                                                        i = R.id.server_settings_section_general_settings;
                                                                                        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.server_settings_section_general_settings);
                                                                                        if (linearLayout2 != null) {
                                                                                            i = R.id.server_settings_section_server_monetization;
                                                                                            LinearLayout linearLayout3 = (LinearLayout) view.findViewById(R.id.server_settings_section_server_monetization);
                                                                                            if (linearLayout3 != null) {
                                                                                                i = R.id.server_settings_section_server_monetization_divider;
                                                                                                View findViewById2 = view.findViewById(R.id.server_settings_section_server_monetization_divider);
                                                                                                if (findViewById2 != null) {
                                                                                                    i = R.id.server_settings_section_user_management;
                                                                                                    LinearLayout linearLayout4 = (LinearLayout) view.findViewById(R.id.server_settings_section_user_management);
                                                                                                    if (linearLayout4 != null) {
                                                                                                        i = R.id.server_settings_section_user_management_divider;
                                                                                                        View findViewById3 = view.findViewById(R.id.server_settings_section_user_management_divider);
                                                                                                        if (findViewById3 != null) {
                                                                                                            i = R.id.server_settings_server_icon;
                                                                                                            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.server_settings_server_icon);
                                                                                                            if (simpleDraweeView != null) {
                                                                                                                i = R.id.server_settings_server_icon_text;
                                                                                                                TextView textView19 = (TextView) view.findViewById(R.id.server_settings_server_icon_text);
                                                                                                                if (textView19 != null) {
                                                                                                                    i = R.id.server_settings_server_name;
                                                                                                                    TextView textView20 = (TextView) view.findViewById(R.id.server_settings_server_name);
                                                                                                                    if (textView20 != null) {
                                                                                                                        return new WidgetServerSettingsBinding((CoordinatorLayout) view, textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8, textView9, textView10, textView11, textView12, textView13, textView14, textView15, textView16, textView17, textView18, linearLayout, findViewById, linearLayout2, linearLayout3, findViewById2, linearLayout4, findViewById3, simpleDraweeView, textView19, textView20);
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
