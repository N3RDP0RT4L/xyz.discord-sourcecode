package com.discord.widgets.servers;

import android.content.Context;
import android.view.View;
import com.discord.widgets.channels.settings.WidgetChannelNotificationSettings;
import com.discord.widgets.servers.NotificationsOverridesAdapter;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
/* compiled from: WidgetServerNotifications.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Landroid/view/View;", "itemView", "Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;", "item", "", "invoke", "(Landroid/view/View;Lcom/discord/widgets/servers/NotificationsOverridesAdapter$Item;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerNotifications$onViewBound$1 extends o implements Function2<View, NotificationsOverridesAdapter.Item, Unit> {
    public static final WidgetServerNotifications$onViewBound$1 INSTANCE = new WidgetServerNotifications$onViewBound$1();

    public WidgetServerNotifications$onViewBound$1() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Unit invoke(View view, NotificationsOverridesAdapter.Item item) {
        invoke2(view, item);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(View view, NotificationsOverridesAdapter.Item item) {
        m.checkNotNullParameter(view, "itemView");
        m.checkNotNullParameter(item, "item");
        WidgetChannelNotificationSettings.Companion companion = WidgetChannelNotificationSettings.Companion;
        Context context = view.getContext();
        m.checkNotNullExpressionValue(context, "itemView.context");
        WidgetChannelNotificationSettings.Companion.launch$default(companion, context, item.getChannel().h(), false, 4, null);
    }
}
