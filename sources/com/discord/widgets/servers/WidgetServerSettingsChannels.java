package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsChannelsBinding;
import com.discord.models.guild.Guild;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.AutocompleteTypes;
import com.discord.widgets.servers.SettingsChannelListAdapter;
import com.discord.widgets.servers.WidgetServerSettingsChannels;
import com.discord.widgets.servers.WidgetServerSettingsChannelsFabMenuFragment;
import com.discord.widgets.servers.WidgetServerSettingsChannelsSortActions;
import d0.f0.q;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.subjects.BehaviorSubject;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsChannels.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 ,2\u00020\u0001:\u0002,-B\u0007¢\u0006\u0004\b+\u0010\u0018J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J/\u0010\r\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00022\u0016\u0010\f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000f\u0010\u0006J\u001b\u0010\u0011\u001a\u00020\u0004*\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001e\u001a\u00020\u00198B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR:\u0010\"\u001a&\u0012\f\u0012\n !*\u0004\u0018\u00010 0  !*\u0012\u0012\f\u0012\n !*\u0004\u0018\u00010 0 \u0018\u00010\u001f0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0018\u0010$\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u001c\u0010'\u001a\u00020&8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*¨\u0006."}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsChannels;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V", "data", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;", "newPositions", "reorderChannels", "(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;Ljava/util/Map;)V", "configureFabVisibility", "Lcom/discord/widgets/servers/SettingsChannelListAdapter;", "setOnClickListener", "(Lcom/discord/widgets/servers/SettingsChannelListAdapter;Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetServerSettingsChannelsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsChannelsBinding;", "binding", "Lrx/subjects/BehaviorSubject;", "", "kotlin.jvm.PlatformType", "channelSortTypeSubject", "Lrx/subjects/BehaviorSubject;", "adapter", "Lcom/discord/widgets/servers/SettingsChannelListAdapter;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsChannels extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsChannels.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsChannelsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    public static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private SettingsChannelListAdapter adapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsChannels$binding$2.INSTANCE, null, 2, null);
    private final BehaviorSubject<Integer> channelSortTypeSubject = BehaviorSubject.l0(-1);
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettingsChannels$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetServerSettingsChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "show", "(Landroid/content/Context;J)V", "", WidgetServerSettingsChannels.INTENT_EXTRA_GUILD_ID, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            StoreStream.Companion.getAnalytics().onGuildSettingsPaneViewed(AutocompleteTypes.CHANNELS, j);
            Intent putExtra = new Intent().putExtra(WidgetServerSettingsChannels.INTENT_EXTRA_GUILD_ID, j);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…_EXTRA_GUILD_ID, guildId)");
            j.d(context, WidgetServerSettingsChannels.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsChannels.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0011\b\u0082\b\u0018\u0000 -2\u00020\u0001:\u0001-BU\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0015\u001a\u00020\t\u0012\u0016\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\f\u0012\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0004\b+\u0010,J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ \u0010\u0010\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J \u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\r0\fHÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0011Jh\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0013\u001a\u00020\u00022\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0015\u001a\u00020\t2\u0018\b\u0002\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\f2\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\r0\fHÆ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u001a\u0010!\u001a\u00020\t2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b!\u0010\"R)\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010#\u001a\u0004\b$\u0010\u0011R\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010%\u001a\u0004\b&\u0010\bR)\u0010\u0016\u001a\u0012\u0012\b\u0012\u00060\rj\u0002`\u000e\u0012\u0004\u0012\u00020\u000f0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010#\u001a\u0004\b'\u0010\u0011R\u0019\u0010\u0015\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010(\u001a\u0004\b\u0015\u0010\u000bR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010)\u001a\u0004\b*\u0010\u0004¨\u0006."}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", "component2", "()Ljava/util/List;", "", "component3", "()Z", "", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "component4", "()Ljava/util/Map;", "component5", "guild", "items", "isSorting", "channels", "channelPermissions", "copy", "(Lcom/discord/models/guild/Guild;Ljava/util/List;ZLjava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/Map;", "getChannelPermissions", "Ljava/util/List;", "getItems", "getChannels", "Z", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Ljava/util/List;ZLjava/util/Map;Ljava/util/Map;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Map<Long, Long> channelPermissions;
        private final Map<Long, Channel> channels;
        private final Guild guild;
        private final boolean isSorting;
        private final List<CategoricalDragAndDropAdapter.Payload> items;

        /* compiled from: WidgetServerSettingsChannels.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0013\u0010\u0014J/\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0004\b\t\u0010\nJ=\u0010\u0011\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\r0\u00102\u0016\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0002j\u0002`\f\u0012\u0004\u0012\u00020\r0\u000b2\u0006\u0010\u000f\u001a\u00020\u0006¢\u0006\u0004\b\u0011\u0010\u0012¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "", "channelTypeObservable", "Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;", "get", "(JLrx/Observable;)Lrx/Observable;", "", "Lcom/discord/primitives/ChannelId;", "Lcom/discord/api/channel/Channel;", "guildChannels", "channelType", "Ljava/util/LinkedHashMap;", "getSortedGuildChannels", "(Ljava/util/Map;I)Ljava/util/LinkedHashMap;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j, Observable<Integer> observable) {
                m.checkNotNullParameter(observable, "channelTypeObservable");
                StoreStream.Companion companion = StoreStream.Companion;
                Observable e = Observable.e(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getPermissions().observePermissionsForGuild(j), companion.getGuilds().observeGuild(j), StoreChannels.observeChannelsForGuild$default(companion.getChannels(), j, null, 2, null), companion.getChannels().observeChannelCategories(j), companion.getPermissions().observeChannelPermissionsForGuild(j), observable, WidgetServerSettingsChannels$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(e, "Observable\n            .…ermissions)\n            }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(e).q();
                m.checkNotNullExpressionValue(q, "Observable\n            .…  .distinctUntilChanged()");
                return q;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public final LinkedHashMap<Long, Channel> getSortedGuildChannels(Map<Long, Channel> map, int i) {
                m.checkNotNullParameter(map, "guildChannels");
                LinkedHashMap<Long, Channel> linkedHashMap = new LinkedHashMap<>();
                for (Object obj : q.sortedWith(q.filter(u.asSequence(map.values()), new WidgetServerSettingsChannels$Model$Companion$getSortedGuildChannels$$inlined$also$lambda$1(map, i)), ChannelUtils.h(Channel.Companion))) {
                    linkedHashMap.put(Long.valueOf(((Channel) obj).h()), obj);
                }
                return linkedHashMap;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(Guild guild, List<? extends CategoricalDragAndDropAdapter.Payload> list, boolean z2, Map<Long, Channel> map, Map<Long, Long> map2) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(list, "items");
            m.checkNotNullParameter(map, "channels");
            m.checkNotNullParameter(map2, "channelPermissions");
            this.guild = guild;
            this.items = list;
            this.isSorting = z2;
            this.channels = map;
            this.channelPermissions = map2;
        }

        public static /* synthetic */ Model copy$default(Model model, Guild guild, List list, boolean z2, Map map, Map map2, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = model.guild;
            }
            List<CategoricalDragAndDropAdapter.Payload> list2 = list;
            if ((i & 2) != 0) {
                list2 = model.items;
            }
            List list3 = list2;
            if ((i & 4) != 0) {
                z2 = model.isSorting;
            }
            boolean z3 = z2;
            Map<Long, Channel> map3 = map;
            if ((i & 8) != 0) {
                map3 = model.channels;
            }
            Map map4 = map3;
            Map<Long, Long> map5 = map2;
            if ((i & 16) != 0) {
                map5 = model.channelPermissions;
            }
            return model.copy(guild, list3, z3, map4, map5);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final List<CategoricalDragAndDropAdapter.Payload> component2() {
            return this.items;
        }

        public final boolean component3() {
            return this.isSorting;
        }

        public final Map<Long, Channel> component4() {
            return this.channels;
        }

        public final Map<Long, Long> component5() {
            return this.channelPermissions;
        }

        public final Model copy(Guild guild, List<? extends CategoricalDragAndDropAdapter.Payload> list, boolean z2, Map<Long, Channel> map, Map<Long, Long> map2) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(list, "items");
            m.checkNotNullParameter(map, "channels");
            m.checkNotNullParameter(map2, "channelPermissions");
            return new Model(guild, list, z2, map, map2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.guild, model.guild) && m.areEqual(this.items, model.items) && this.isSorting == model.isSorting && m.areEqual(this.channels, model.channels) && m.areEqual(this.channelPermissions, model.channelPermissions);
        }

        public final Map<Long, Long> getChannelPermissions() {
            return this.channelPermissions;
        }

        public final Map<Long, Channel> getChannels() {
            return this.channels;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final List<CategoricalDragAndDropAdapter.Payload> getItems() {
            return this.items;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            List<CategoricalDragAndDropAdapter.Payload> list = this.items;
            int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
            boolean z2 = this.isSorting;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode2 + i2) * 31;
            Map<Long, Channel> map = this.channels;
            int hashCode3 = (i4 + (map != null ? map.hashCode() : 0)) * 31;
            Map<Long, Long> map2 = this.channelPermissions;
            if (map2 != null) {
                i = map2.hashCode();
            }
            return hashCode3 + i;
        }

        public final boolean isSorting() {
            return this.isSorting;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guild=");
            R.append(this.guild);
            R.append(", items=");
            R.append(this.items);
            R.append(", isSorting=");
            R.append(this.isSorting);
            R.append(", channels=");
            R.append(this.channels);
            R.append(", channelPermissions=");
            return a.L(R, this.channelPermissions, ")");
        }
    }

    public WidgetServerSettingsChannels() {
        super(R.layout.widget_server_settings_channels);
    }

    private final void configureFabVisibility(final Model model) {
        final WidgetServerSettingsChannels$configureFabVisibility$setFabVisibility$1 widgetServerSettingsChannels$configureFabVisibility$setFabVisibility$1 = new WidgetServerSettingsChannels$configureFabVisibility$setFabVisibility$1(this, model);
        widgetServerSettingsChannels$configureFabVisibility$setFabVisibility$1.invoke();
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannels$configureFabVisibility$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsChannelsBinding binding;
                WidgetServerSettingsChannelsFabMenuFragment.Companion companion = WidgetServerSettingsChannelsFabMenuFragment.Companion;
                long id2 = model.getGuild().getId();
                FragmentManager childFragmentManager = WidgetServerSettingsChannels.this.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                companion.show(id2, childFragmentManager, new Action0() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannels$configureFabVisibility$1.1
                    @Override // rx.functions.Action0
                    public final void call() {
                        widgetServerSettingsChannels$configureFabVisibility$setFabVisibility$1.invoke();
                    }
                });
                binding = WidgetServerSettingsChannels.this.getBinding();
                binding.c.hide();
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Multi-variable type inference failed */
    public final void configureUI(final Model model) {
        if (model == null) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.onBackPressed();
                return;
            }
            return;
        }
        setActionBarDisplayHomeAsUpEnabled(!model.isSorting());
        setActionBarTitle(!model.isSorting() ? R.string.channels : R.string.sorting);
        setActionBarSubtitle(model.getGuild().getName());
        setActionBarOptionsMenu(R.menu.menu_channel_sort, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannels$configureUI$1

            /* compiled from: WidgetServerSettingsChannels.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "channelType", "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsChannels$configureUI$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Integer, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                    invoke(num.intValue());
                    return Unit.a;
                }

                public final void invoke(int i) {
                    BehaviorSubject behaviorSubject;
                    behaviorSubject = WidgetServerSettingsChannels.this.channelSortTypeSubject;
                    behaviorSubject.onNext(Integer.valueOf(i));
                }
            }

            public final void call(MenuItem menuItem, Context context) {
                BehaviorSubject behaviorSubject;
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                switch (menuItem.getItemId()) {
                    case R.id.menu_sort_channel /* 2131364345 */:
                        WidgetServerSettingsChannelsSortActions.Companion companion = WidgetServerSettingsChannelsSortActions.Companion;
                        FragmentManager childFragmentManager = WidgetServerSettingsChannels.this.getChildFragmentManager();
                        m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                        companion.show(childFragmentManager, new AnonymousClass1());
                        return;
                    case R.id.menu_sort_done /* 2131364346 */:
                        behaviorSubject = WidgetServerSettingsChannels.this.channelSortTypeSubject;
                        behaviorSubject.onNext(-1);
                        return;
                    default:
                        return;
                }
            }
        }, new Action1<Menu>() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannels$configureUI$2
            public final void call(Menu menu) {
                MenuItem findItem = menu.findItem(R.id.menu_sort_channel);
                m.checkNotNullExpressionValue(findItem, "menu.findItem(R.id.menu_sort_channel)");
                findItem.setVisible(!WidgetServerSettingsChannels.Model.this.isSorting());
                MenuItem findItem2 = menu.findItem(R.id.menu_sort_done);
                m.checkNotNullExpressionValue(findItem2, "menu.findItem(R.id.menu_sort_done)");
                findItem2.setVisible(WidgetServerSettingsChannels.Model.this.isSorting());
            }
        });
        configureFabVisibility(model);
        SettingsChannelListAdapter settingsChannelListAdapter = this.adapter;
        if (settingsChannelListAdapter != null) {
            settingsChannelListAdapter.setData(model.getItems());
        }
        SettingsChannelListAdapter settingsChannelListAdapter2 = this.adapter;
        if (settingsChannelListAdapter2 != 0) {
            settingsChannelListAdapter2.setPositionUpdateListener(new Action1<Map<Long, ? extends SettingsChannelListAdapter.UpdatedPosition>>() { // from class: com.discord.widgets.servers.WidgetServerSettingsChannels$configureUI$3
                @Override // rx.functions.Action1
                public /* bridge */ /* synthetic */ void call(Map<Long, ? extends SettingsChannelListAdapter.UpdatedPosition> map) {
                    call2((Map<Long, SettingsChannelListAdapter.UpdatedPosition>) map);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final void call2(Map<Long, SettingsChannelListAdapter.UpdatedPosition> map) {
                    WidgetServerSettingsChannels widgetServerSettingsChannels = WidgetServerSettingsChannels.this;
                    WidgetServerSettingsChannels.Model model2 = model;
                    m.checkNotNullExpressionValue(map, "newPositions");
                    widgetServerSettingsChannels.reorderChannels(model2, map);
                }
            });
        }
        SettingsChannelListAdapter settingsChannelListAdapter3 = this.adapter;
        if (settingsChannelListAdapter3 != null) {
            setOnClickListener(settingsChannelListAdapter3, model);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsChannelsBinding getBinding() {
        return (WidgetServerSettingsChannelsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void reorderChannels(Model model, Map<Long, SettingsChannelListAdapter.UpdatedPosition> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<Long, SettingsChannelListAdapter.UpdatedPosition> entry : map.entrySet()) {
            long longValue = entry.getKey().longValue();
            SettingsChannelListAdapter.UpdatedPosition value = entry.getValue();
            arrayList.add(new RestAPIParams.ChannelPosition(longValue, value.getPosition(), value.getParentId()));
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.withDimmer$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApiSerializeNulls().reorderChannels(model.getGuild().getId(), arrayList), false, 1, null), getBinding().f2526b, 0L, 2, null), this, null, 2, null), WidgetServerSettingsChannels.class, (r18 & 2) != 0 ? null : getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new WidgetServerSettingsChannels$reorderChannels$2(this, model), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsChannels$reorderChannels$1(this));
    }

    private final void setOnClickListener(SettingsChannelListAdapter settingsChannelListAdapter, Model model) {
        Function1<? super Long, Unit> function1;
        if (model.isSorting()) {
            function1 = WidgetServerSettingsChannels$setOnClickListener$1.INSTANCE;
        } else {
            function1 = new WidgetServerSettingsChannels$setOnClickListener$2(this, model);
        }
        settingsChannelListAdapter.setOnClickListener(function1);
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().d;
        m.checkNotNullExpressionValue(recyclerView, "binding.serverSettingsChannelsTextRecycler");
        this.adapter = (SettingsChannelListAdapter) companion.configure(new SettingsChannelListAdapter(recyclerView, true));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        long longExtra = getMostRecentIntent().getLongExtra(INTENT_EXTRA_GUILD_ID, -1L);
        Model.Companion companion = Model.Companion;
        BehaviorSubject<Integer> behaviorSubject = this.channelSortTypeSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "channelSortTypeSubject");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.get(longExtra, behaviorSubject), this, null, 2, null), WidgetServerSettingsChannels.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsChannels$onViewBoundOrOnResume$1(this));
    }
}
