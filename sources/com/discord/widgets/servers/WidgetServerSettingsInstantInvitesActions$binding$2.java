package com.discord.widgets.servers;

import android.view.View;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import com.discord.databinding.WidgetServerSettingsInstantInviteActionsBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsInstantInvitesActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetServerSettingsInstantInviteActionsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsInstantInviteActionsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettingsInstantInvitesActions$binding$2 extends k implements Function1<View, WidgetServerSettingsInstantInviteActionsBinding> {
    public static final WidgetServerSettingsInstantInvitesActions$binding$2 INSTANCE = new WidgetServerSettingsInstantInvitesActions$binding$2();

    public WidgetServerSettingsInstantInvitesActions$binding$2() {
        super(1, WidgetServerSettingsInstantInviteActionsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsInstantInviteActionsBinding;", 0);
    }

    public final WidgetServerSettingsInstantInviteActionsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.invite_actions_copy;
        TextView textView = (TextView) view.findViewById(R.id.invite_actions_copy);
        if (textView != null) {
            i = R.id.invite_actions_revoke;
            TextView textView2 = (TextView) view.findViewById(R.id.invite_actions_revoke);
            if (textView2 != null) {
                i = R.id.invite_actions_share;
                TextView textView3 = (TextView) view.findViewById(R.id.invite_actions_share);
                if (textView3 != null) {
                    i = R.id.invite_actions_title;
                    TextView textView4 = (TextView) view.findViewById(R.id.invite_actions_title);
                    if (textView4 != null) {
                        return new WidgetServerSettingsInstantInviteActionsBinding((NestedScrollView) view, textView, textView2, textView3, textView4);
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
