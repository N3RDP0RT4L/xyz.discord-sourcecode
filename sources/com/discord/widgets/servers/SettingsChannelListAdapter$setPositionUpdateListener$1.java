package com.discord.widgets.servers;

import com.discord.widgets.servers.SettingsChannelListAdapter;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.functions.Action1;
/* compiled from: SettingsChannelListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "", "Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;", "map", "", "invoke", "(Ljava/util/Map;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsChannelListAdapter$setPositionUpdateListener$1 extends o implements Function1<Map<Long, ? extends SettingsChannelListAdapter.UpdatedPosition>, Unit> {
    public final /* synthetic */ Action1 $onPositionUpdateListener;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SettingsChannelListAdapter$setPositionUpdateListener$1(Action1 action1) {
        super(1);
        this.$onPositionUpdateListener = action1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Map<Long, ? extends SettingsChannelListAdapter.UpdatedPosition> map) {
        invoke2((Map<Long, SettingsChannelListAdapter.UpdatedPosition>) map);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Map<Long, SettingsChannelListAdapter.UpdatedPosition> map) {
        m.checkNotNullParameter(map, "map");
        this.$onPositionUpdateListener.call(map);
    }
}
