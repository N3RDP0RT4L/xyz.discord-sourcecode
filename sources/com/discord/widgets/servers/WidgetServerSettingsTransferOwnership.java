package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetGuildTransferOwnershipBinding;
import com.discord.models.guild.Guild;
import com.discord.models.user.User;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.CheckedSetting;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsTransferOwnership.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00142\u00020\u0001:\u0002\u0014\u0015B\u0007¢\u0006\u0004\b\u0013\u0010\bJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bR\u0018\u0010\f\u001a\u0004\u0018\u00010\t8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;", "Lcom/discord/app/AppDialog;", "Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;)V", "onViewBoundOrOnResume", "()V", "", "getMfaCode", "()Ljava/lang/String;", "mfaCode", "Lcom/discord/databinding/WidgetGuildTransferOwnershipBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetGuildTransferOwnershipBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsTransferOwnership extends AppDialog {
    private static final String ARG_GUILD_ID = "ARG_GUILD_ID";
    private static final String ARG_USER_ID = "ARG_USER_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsTransferOwnership$binding$2.INSTANCE, null, 2, null);
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsTransferOwnership.class, "binding", "getBinding()Lcom/discord/databinding/WidgetGuildTransferOwnershipBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String TAG = WidgetServerSettingsTransferOwnership.class.getSimpleName();

    /* compiled from: WidgetServerSettingsTransferOwnership.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J-\u0010\n\u001a\u00020\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000eR\u001e\u0010\u0011\u001a\n \u0010*\u0004\u0018\u00010\f0\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u000e¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/UserId;", "userId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "create", "(JJLandroidx/fragment/app/FragmentManager;)V", "", WidgetServerSettingsTransferOwnership.ARG_GUILD_ID, "Ljava/lang/String;", WidgetServerSettingsTransferOwnership.ARG_USER_ID, "kotlin.jvm.PlatformType", "TAG", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(long j, long j2, FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetServerSettingsTransferOwnership widgetServerSettingsTransferOwnership = new WidgetServerSettingsTransferOwnership();
            Bundle I = a.I(WidgetServerSettingsTransferOwnership.ARG_GUILD_ID, j);
            I.putLong(WidgetServerSettingsTransferOwnership.ARG_USER_ID, j2);
            widgetServerSettingsTransferOwnership.setArguments(I);
            widgetServerSettingsTransferOwnership.show(fragmentManager, WidgetServerSettingsTransferOwnership.TAG);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsTransferOwnership.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\b\u0082\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\b\u0012\u0006\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ8\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0019\u001a\u00020\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\u000e\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u000e\u0010\nR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001c\u001a\u0004\b\u001d\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\u000f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001b\u001a\u0004\b\u000f\u0010\n¨\u0006#"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;", "", "Lcom/discord/models/user/User;", "component1", "()Lcom/discord/models/user/User;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "", "component3", "()Z", "component4", "user", "guild", "isMfaEnabled", "isGuildOwner", "copy", "(Lcom/discord/models/user/User;Lcom/discord/models/guild/Guild;ZZ)Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/models/user/User;", "getUser", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/user/User;Lcom/discord/models/guild/Guild;ZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Guild guild;
        private final boolean isGuildOwner;
        private final boolean isMfaEnabled;
        private final User user;

        /* compiled from: WidgetServerSettingsTransferOwnership.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ-\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/UserId;", "userId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;", "get", "(JJ)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j, long j2) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable q = Observable.i(companion.getGuilds().observeGuild(j), companion.getUsers().observeUser(j2), StoreUser.observeMe$default(companion.getUsers(), false, 1, null), WidgetServerSettingsTransferOwnership$Model$Companion$get$1.INSTANCE).q();
                m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
                return ObservableExtensionsKt.computationLatest(q);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(User user, Guild guild, boolean z2, boolean z3) {
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(guild, "guild");
            this.user = user;
            this.guild = guild;
            this.isMfaEnabled = z2;
            this.isGuildOwner = z3;
        }

        public static /* synthetic */ Model copy$default(Model model, User user, Guild guild, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                user = model.user;
            }
            if ((i & 2) != 0) {
                guild = model.guild;
            }
            if ((i & 4) != 0) {
                z2 = model.isMfaEnabled;
            }
            if ((i & 8) != 0) {
                z3 = model.isGuildOwner;
            }
            return model.copy(user, guild, z2, z3);
        }

        public final User component1() {
            return this.user;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final boolean component3() {
            return this.isMfaEnabled;
        }

        public final boolean component4() {
            return this.isGuildOwner;
        }

        public final Model copy(User user, Guild guild, boolean z2, boolean z3) {
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(guild, "guild");
            return new Model(user, guild, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.user, model.user) && m.areEqual(this.guild, model.guild) && this.isMfaEnabled == model.isMfaEnabled && this.isGuildOwner == model.isGuildOwner;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final User getUser() {
            return this.user;
        }

        public int hashCode() {
            User user = this.user;
            int i = 0;
            int hashCode = (user != null ? user.hashCode() : 0) * 31;
            Guild guild = this.guild;
            if (guild != null) {
                i = guild.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.isMfaEnabled;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.isGuildOwner;
            if (!z3) {
                i3 = z3 ? 1 : 0;
            }
            return i6 + i3;
        }

        public final boolean isGuildOwner() {
            return this.isGuildOwner;
        }

        public final boolean isMfaEnabled() {
            return this.isMfaEnabled;
        }

        public String toString() {
            StringBuilder R = a.R("Model(user=");
            R.append(this.user);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", isMfaEnabled=");
            R.append(this.isMfaEnabled);
            R.append(", isGuildOwner=");
            return a.M(R, this.isGuildOwner, ")");
        }
    }

    public WidgetServerSettingsTransferOwnership() {
        super(R.layout.widget_guild_transfer_ownership);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        CharSequence e;
        if (model == null) {
            dismiss();
            return;
        }
        LinearLayout linearLayout = getBinding().f;
        m.checkNotNullExpressionValue(linearLayout, "binding.guildTransferOwnershipMfaWrap");
        linearLayout.setVisibility(model.isMfaEnabled() ? 0 : 8);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsTransferOwnership$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsTransferOwnership.this.dismiss();
            }
        });
        MaterialButton materialButton = getBinding().d;
        m.checkNotNullExpressionValue(materialButton, "binding.guildTransferOwnershipConfirm");
        CheckedSetting checkedSetting = getBinding().f2432b;
        m.checkNotNullExpressionValue(checkedSetting, "binding.guildTransferOwnershipAcknowledgeCheck");
        materialButton.setEnabled(checkedSetting.isChecked());
        CheckedSetting checkedSetting2 = getBinding().f2432b;
        e = b.e(this, R.string.transfer_ownership_acknowledge, new Object[]{model.getUser().getUsername()}, (r4 & 4) != 0 ? b.a.j : null);
        checkedSetting2.setText(e);
        getBinding().f2432b.e(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsTransferOwnership$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetGuildTransferOwnershipBinding binding;
                WidgetGuildTransferOwnershipBinding binding2;
                WidgetGuildTransferOwnershipBinding binding3;
                binding = WidgetServerSettingsTransferOwnership.this.getBinding();
                binding.f2432b.toggle();
                binding2 = WidgetServerSettingsTransferOwnership.this.getBinding();
                MaterialButton materialButton2 = binding2.d;
                m.checkNotNullExpressionValue(materialButton2, "binding.guildTransferOwnershipConfirm");
                binding3 = WidgetServerSettingsTransferOwnership.this.getBinding();
                CheckedSetting checkedSetting3 = binding3.f2432b;
                m.checkNotNullExpressionValue(checkedSetting3, "binding.guildTransferOwnershipAcknowledgeCheck");
                materialButton2.setEnabled(checkedSetting3.isChecked());
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsTransferOwnership$configureUI$3

            /* compiled from: WidgetServerSettingsTransferOwnership.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Ljava/lang/Void;", "it", "", "invoke", "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsTransferOwnership$configureUI$3$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Void, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Void r1) {
                    invoke2(r1);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Void r1) {
                    WidgetServerSettingsTransferOwnership.this.dismiss();
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                String mfaCode;
                RestAPI api = RestAPI.Companion.getApi();
                long id2 = model.getGuild().getId();
                long id3 = model.getUser().getId();
                mfaCode = WidgetServerSettingsTransferOwnership.this.getMfaCode();
                Observable ui$default = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(api.transferGuildOwnership(id2, new RestAPIParams.TransferGuildOwnership(id3, mfaCode)), false, 1, null), WidgetServerSettingsTransferOwnership.this, null, 2, null);
                Class<?> cls = WidgetServerSettingsTransferOwnership.this.getClass();
                m.checkNotNullExpressionValue(view, "view");
                ObservableExtensionsKt.appSubscribe(ui$default, cls, (r18 & 2) != 0 ? null : view.getContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetGuildTransferOwnershipBinding getBinding() {
        return (WidgetGuildTransferOwnershipBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getMfaCode() {
        TextInputLayout textInputLayout = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout, "binding.guildTransferOwnershipMfaCode");
        boolean z2 = true;
        if (textInputLayout.getVisibility() == 0) {
            TextInputLayout textInputLayout2 = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.guildTransferOwnershipMfaCode");
            if (ViewExtensions.getTextOrEmpty(textInputLayout2).length() != 0) {
                z2 = false;
            }
            if (!z2) {
                TextInputLayout textInputLayout3 = getBinding().e;
                m.checkNotNullExpressionValue(textInputLayout3, "binding.guildTransferOwnershipMfaCode");
                return ViewExtensions.getTextOrEmpty(textInputLayout3);
            }
        }
        return null;
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getArgumentsOrDefault().getLong(ARG_GUILD_ID, -1L), getArgumentsOrDefault().getLong(ARG_USER_ID, -1L)), this, null, 2, null), WidgetServerSettingsTransferOwnership.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsTransferOwnership$onViewBoundOrOnResume$1(this));
    }
}
