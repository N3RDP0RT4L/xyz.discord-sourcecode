package com.discord.widgets.servers;

import android.view.View;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import com.discord.databinding.WidgetServerSettingsOverviewAfkTimeoutBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsOverview.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetServerSettingsOverviewAfkTimeoutBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsOverviewAfkTimeoutBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetServerSettingsOverview$AfkBottomSheet$binding$2 extends k implements Function1<View, WidgetServerSettingsOverviewAfkTimeoutBinding> {
    public static final WidgetServerSettingsOverview$AfkBottomSheet$binding$2 INSTANCE = new WidgetServerSettingsOverview$AfkBottomSheet$binding$2();

    public WidgetServerSettingsOverview$AfkBottomSheet$binding$2() {
        super(1, WidgetServerSettingsOverviewAfkTimeoutBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetServerSettingsOverviewAfkTimeoutBinding;", 0);
    }

    public final WidgetServerSettingsOverviewAfkTimeoutBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.guild_actions_overview_header_tv;
        TextView textView = (TextView) view.findViewById(R.id.guild_actions_overview_header_tv);
        if (textView != null) {
            i = R.id.server_settings_overview_afk_timeout_01;
            TextView textView2 = (TextView) view.findViewById(R.id.server_settings_overview_afk_timeout_01);
            if (textView2 != null) {
                i = R.id.server_settings_overview_afk_timeout_05;
                TextView textView3 = (TextView) view.findViewById(R.id.server_settings_overview_afk_timeout_05);
                if (textView3 != null) {
                    i = R.id.server_settings_overview_afk_timeout_15;
                    TextView textView4 = (TextView) view.findViewById(R.id.server_settings_overview_afk_timeout_15);
                    if (textView4 != null) {
                        i = R.id.server_settings_overview_afk_timeout_30;
                        TextView textView5 = (TextView) view.findViewById(R.id.server_settings_overview_afk_timeout_30);
                        if (textView5 != null) {
                            i = R.id.server_settings_overview_afk_timeout_60;
                            TextView textView6 = (TextView) view.findViewById(R.id.server_settings_overview_afk_timeout_60);
                            if (textView6 != null) {
                                return new WidgetServerSettingsOverviewAfkTimeoutBinding((NestedScrollView) view, textView, textView2, textView3, textView4, textView5, textView6);
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
