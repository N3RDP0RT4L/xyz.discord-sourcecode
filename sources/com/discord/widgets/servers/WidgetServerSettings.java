package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.api.guild.GuildFeature;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetServerSettingsBinding;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.permissions.ManageGuildContext;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guild_role_subscriptions.GuildRoleSubscriptionsFeatureFlag;
import com.discord.widgets.servers.WidgetServerDeleteDialog;
import com.discord.widgets.servers.WidgetServerSettings;
import com.discord.widgets.servers.WidgetServerSettingsOverview;
import com.discord.widgets.servers.auditlog.WidgetServerSettingsAuditLog;
import com.discord.widgets.servers.community.WidgetServerSettingsCommunityOverview;
import com.discord.widgets.servers.community.WidgetServerSettingsEnableCommunitySteps;
import com.discord.widgets.servers.guild_role_subscription.WidgetServerSettingsEmptyGuildRoleSubscriptions;
import com.discord.widgets.servers.guild_role_subscription.WidgetServerSettingsGuildRoleSubscriptionTierList;
import com.discord.widgets.servers.guild_role_subscription.WidgetServerSettingsGuildRoleSubscriptions;
import com.discord.widgets.servers.guild_role_subscription.payments.WidgetServerSettingsGuildRoleSubscriptionEarnings;
import com.discord.widgets.servers.settings.invites.WidgetServerSettingsInstantInvites;
import com.discord.widgets.servers.settings.members.WidgetServerSettingsMembers;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetServerSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u001e2\u00020\u0001:\u0002\u001e\u001fB\u0007¢\u0006\u0004\b\u001d\u0010\tJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000e\u0010\tR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0013\u001a\u00020\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b¨\u0006 "}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettings$Model;", "model", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettings$Model;)V", "configureToolbar", "configureSectionsVisibility", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "", "guildId", "J", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/databinding/WidgetServerSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private long guildId;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettings$binding$2.INSTANCE, null, 2, null);
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetServerSettings$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetServerSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettings$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "create", "(Landroid/content/Context;J)V", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            j.d(context, WidgetServerSettings.class, new Intent().putExtra("INTENT_EXTRA_GUILD_ID", j));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0082\b\u0018\u0000 $2\u00020\u0001:\u0001$B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\b\u0012\u0006\u0010\u000f\u001a\u00020\b¢\u0006\u0004\b\"\u0010#J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ8\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\b2\b\b\u0002\u0010\u000f\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u001a\u0010\u0019\u001a\u00020\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u0019\u0010\u000e\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\nR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001b\u001a\u0004\b\u001f\u0010\nR\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010 \u001a\u0004\b!\u0010\u0007¨\u0006%"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettings$Model;", "", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/utilities/permissions/ManageGuildContext;", "component2", "()Lcom/discord/utilities/permissions/ManageGuildContext;", "", "component3", "()Z", "component4", "guild", "manageGuildContext", "canManageGuildRoleSubscriptions", "hasGuildRoleSubscriptions", "copy", "(Lcom/discord/models/guild/Guild;Lcom/discord/utilities/permissions/ManageGuildContext;ZZ)Lcom/discord/widgets/servers/WidgetServerSettings$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanManageGuildRoleSubscriptions", "Lcom/discord/models/guild/Guild;", "getGuild", "getHasGuildRoleSubscriptions", "Lcom/discord/utilities/permissions/ManageGuildContext;", "getManageGuildContext", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;Lcom/discord/utilities/permissions/ManageGuildContext;ZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canManageGuildRoleSubscriptions;
        private final Guild guild;
        private final boolean hasGuildRoleSubscriptions;
        private final ManageGuildContext manageGuildContext;

        /* compiled from: WidgetServerSettings.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettings$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettings$Model;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable f = Observable.f(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getGuilds().observeGuild(j), companion.getPermissions().observePermissionsForGuild(j), companion.getChannels().observeChannelCategories(j), companion.getPermissions().observeChannelPermissionsForGuild(j), GuildRoleSubscriptionsFeatureFlag.Companion.getINSTANCE().observeIsGuildEligibleForRoleSubscriptions(j), WidgetServerSettings$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(f, "Observable\n          .co…            )\n          }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(f).q();
                m.checkNotNullExpressionValue(q, "Observable\n          .co…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(Guild guild, ManageGuildContext manageGuildContext, boolean z2, boolean z3) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(manageGuildContext, "manageGuildContext");
            this.guild = guild;
            this.manageGuildContext = manageGuildContext;
            this.canManageGuildRoleSubscriptions = z2;
            this.hasGuildRoleSubscriptions = z3;
        }

        public static /* synthetic */ Model copy$default(Model model, Guild guild, ManageGuildContext manageGuildContext, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                guild = model.guild;
            }
            if ((i & 2) != 0) {
                manageGuildContext = model.manageGuildContext;
            }
            if ((i & 4) != 0) {
                z2 = model.canManageGuildRoleSubscriptions;
            }
            if ((i & 8) != 0) {
                z3 = model.hasGuildRoleSubscriptions;
            }
            return model.copy(guild, manageGuildContext, z2, z3);
        }

        public final Guild component1() {
            return this.guild;
        }

        public final ManageGuildContext component2() {
            return this.manageGuildContext;
        }

        public final boolean component3() {
            return this.canManageGuildRoleSubscriptions;
        }

        public final boolean component4() {
            return this.hasGuildRoleSubscriptions;
        }

        public final Model copy(Guild guild, ManageGuildContext manageGuildContext, boolean z2, boolean z3) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(manageGuildContext, "manageGuildContext");
            return new Model(guild, manageGuildContext, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.guild, model.guild) && m.areEqual(this.manageGuildContext, model.manageGuildContext) && this.canManageGuildRoleSubscriptions == model.canManageGuildRoleSubscriptions && this.hasGuildRoleSubscriptions == model.hasGuildRoleSubscriptions;
        }

        public final boolean getCanManageGuildRoleSubscriptions() {
            return this.canManageGuildRoleSubscriptions;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final boolean getHasGuildRoleSubscriptions() {
            return this.hasGuildRoleSubscriptions;
        }

        public final ManageGuildContext getManageGuildContext() {
            return this.manageGuildContext;
        }

        public int hashCode() {
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
            ManageGuildContext manageGuildContext = this.manageGuildContext;
            if (manageGuildContext != null) {
                i = manageGuildContext.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z2 = this.canManageGuildRoleSubscriptions;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.hasGuildRoleSubscriptions;
            if (!z3) {
                i3 = z3 ? 1 : 0;
            }
            return i6 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("Model(guild=");
            R.append(this.guild);
            R.append(", manageGuildContext=");
            R.append(this.manageGuildContext);
            R.append(", canManageGuildRoleSubscriptions=");
            R.append(this.canManageGuildRoleSubscriptions);
            R.append(", hasGuildRoleSubscriptions=");
            return a.M(R, this.hasGuildRoleSubscriptions, ")");
        }
    }

    public WidgetServerSettings() {
        super(R.layout.widget_server_settings);
    }

    private final void configureSectionsVisibility() {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        int i = 0;
        boolean z9 = true;
        List<TextView> listOf = n.listOf((Object[]) new TextView[]{getBinding().p, getBinding().m, getBinding().n, getBinding().q, getBinding().r, getBinding().l, getBinding().d});
        List<TextView> listOf2 = n.listOf((Object[]) new TextView[]{getBinding().k, getBinding().f2522s, getBinding().j, getBinding().f2521b});
        List<TextView> listOf3 = n.listOf((Object[]) new TextView[]{getBinding().h, getBinding().f, getBinding().i, getBinding().g});
        LinearLayout linearLayout = getBinding().u;
        m.checkNotNullExpressionValue(linearLayout, "binding.serverSettingsSectionGeneralSettings");
        boolean z10 = listOf instanceof Collection;
        if (!z10 || !listOf.isEmpty()) {
            for (TextView textView : listOf) {
                m.checkNotNullExpressionValue(textView, "view");
                if (textView.getVisibility() == 0) {
                    z8 = true;
                    continue;
                } else {
                    z8 = false;
                    continue;
                }
                if (z8) {
                    z2 = true;
                    break;
                }
            }
        }
        z2 = false;
        linearLayout.setVisibility(z2 ? 0 : 8);
        View view = getBinding().f2523x;
        m.checkNotNullExpressionValue(view, "binding.serverSettingsSectionUserManagementDivider");
        if (!z10 || !listOf.isEmpty()) {
            for (TextView textView2 : listOf) {
                m.checkNotNullExpressionValue(textView2, "view");
                if (textView2.getVisibility() == 0) {
                    z7 = true;
                    continue;
                } else {
                    z7 = false;
                    continue;
                }
                if (z7) {
                    z3 = true;
                    break;
                }
            }
        }
        z3 = false;
        view.setVisibility(z3 ? 0 : 8);
        LinearLayout linearLayout2 = getBinding().w;
        m.checkNotNullExpressionValue(linearLayout2, "binding.serverSettingsSectionUserManagement");
        if (!(listOf2 instanceof Collection) || !listOf2.isEmpty()) {
            for (TextView textView3 : listOf2) {
                m.checkNotNullExpressionValue(textView3, "view");
                if (textView3.getVisibility() == 0) {
                    z6 = true;
                    continue;
                } else {
                    z6 = false;
                    continue;
                }
                if (z6) {
                    z4 = true;
                    break;
                }
            }
        }
        z4 = false;
        linearLayout2.setVisibility(z4 ? 0 : 8);
        LinearLayout linearLayout3 = getBinding().v;
        m.checkNotNullExpressionValue(linearLayout3, "binding.serverSettingsSectionServerMonetization");
        if (!(listOf3 instanceof Collection) || !listOf3.isEmpty()) {
            for (TextView textView4 : listOf3) {
                m.checkNotNullExpressionValue(textView4, "view");
                if (textView4.getVisibility() == 0) {
                    z5 = true;
                    continue;
                } else {
                    z5 = false;
                    continue;
                }
                if (z5) {
                    break;
                }
            }
        }
        z9 = false;
        if (!z9) {
            i = 8;
        }
        linearLayout3.setVisibility(i);
    }

    private final void configureToolbar(Model model) {
        final Guild guild = model.getGuild();
        String forGuild$default = IconUtils.getForGuild$default(guild, null, true, null, 10, null);
        TextView textView = getBinding().A;
        m.checkNotNullExpressionValue(textView, "binding.serverSettingsServerName");
        textView.setText(guild.getName());
        SimpleDraweeView simpleDraweeView = getBinding().f2524y;
        m.checkNotNullExpressionValue(simpleDraweeView, "binding.serverSettingsServerIcon");
        IconUtils.setIcon$default(simpleDraweeView, forGuild$default, (int) R.dimen.avatar_size_large, (Function1) null, (MGImages.ChangeDetector) null, 24, (Object) null);
        TextView textView2 = getBinding().f2525z;
        m.checkNotNullExpressionValue(textView2, "binding.serverSettingsServerIconText");
        int i = 0;
        if (!(forGuild$default == null)) {
            i = 8;
        }
        textView2.setVisibility(i);
        TextView textView3 = getBinding().f2525z;
        m.checkNotNullExpressionValue(textView3, "binding.serverSettingsServerIconText");
        textView3.setText(guild.getShortName());
        AppFragment.setActionBarOptionsMenu$default(this, model.getManageGuildContext().isOwnerWithRequiredMFALevel() ? R.menu.menu_server_settings : R.menu.menu_empty, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureToolbar$1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_server_settings_delete) {
                    WidgetServerDeleteDialog.Companion companion = WidgetServerDeleteDialog.Companion;
                    FragmentManager parentFragmentManager = WidgetServerSettings.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion.show(parentFragmentManager, guild.getId());
                }
            }
        }, null, 4, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        if (model == null || !model.getManageGuildContext().canManage()) {
            FragmentActivity activity = e();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        configureToolbar(model);
        ManageGuildContext manageGuildContext = model.getManageGuildContext();
        TextView textView = getBinding().p;
        m.checkNotNullExpressionValue(textView, "binding.serverSettingsOptionOverview");
        int i = 8;
        textView.setVisibility(manageGuildContext.getCanManageServer() ? 0 : 8);
        getBinding().p.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsOverview.Companion.create$default(WidgetServerSettingsOverview.Companion, a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId(), false, 4, null);
            }
        });
        TextView textView2 = getBinding().o;
        m.checkNotNullExpressionValue(textView2, "binding.serverSettingsOptionModeration");
        textView2.setVisibility(manageGuildContext.getCanManageServer() ? 0 : 8);
        getBinding().o.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsModeration.Companion.launch(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView3 = getBinding().l;
        m.checkNotNullExpressionValue(textView3, "binding.serverSettingsOptionAuditLog");
        textView3.setVisibility(manageGuildContext.getCanViewAuditLogs() ? 0 : 8);
        getBinding().l.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsAuditLog.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId(), WidgetServerSettings.Model.this.getGuild().getName());
            }
        });
        TextView textView4 = getBinding().m;
        m.checkNotNullExpressionValue(textView4, "binding.serverSettingsOptionChannels");
        textView4.setVisibility(manageGuildContext.getCanManageChannels() ? 0 : 8);
        getBinding().m.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsChannels.Companion.show(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView5 = getBinding().q;
        m.checkNotNullExpressionValue(textView5, "binding.serverSettingsOptionSecurity");
        textView5.setVisibility(manageGuildContext.isOwnerWithRequiredMFALevel() ? 0 : 8);
        getBinding().q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsSecurity.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView6 = getBinding().n;
        m.checkNotNullExpressionValue(textView6, "binding.serverSettingsOptionIntegrations");
        textView6.setVisibility(manageGuildContext.getCanManageServer() ? 0 : 8);
        getBinding().n.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsIntegrations.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView7 = getBinding().r;
        m.checkNotNullExpressionValue(textView7, "binding.serverSettingsOptionVanityUrl");
        boolean z2 = true;
        textView7.setVisibility(manageGuildContext.getCanManageServer() && model.getGuild().getFeatures().contains(GuildFeature.VANITY_URL) ? 0 : 8);
        getBinding().r.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsVanityUrl.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        LinearLayout linearLayout = getBinding().t;
        m.checkNotNullExpressionValue(linearLayout, "binding.serverSettingsSectionCommunity");
        linearLayout.setVisibility(manageGuildContext.getCanManageServer() ? 0 : 8);
        TextView textView8 = getBinding().c;
        m.checkNotNullExpressionValue(textView8, "binding.serverSettingsCommunityOverviewOption");
        Set<GuildFeature> features = model.getGuild().getFeatures();
        GuildFeature guildFeature = GuildFeature.COMMUNITY;
        textView8.setVisibility(features.contains(guildFeature) ? 0 : 8);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsCommunityOverview.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView9 = getBinding().e;
        m.checkNotNullExpressionValue(textView9, "binding.serverSettingsEnableCommunityOption");
        textView9.setVisibility(model.getGuild().getFeatures().contains(guildFeature) ^ true ? 0 : 8);
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsEnableCommunitySteps.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        getBinding().k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsMembers.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView10 = getBinding().f2522s;
        m.checkNotNullExpressionValue(textView10, "binding.serverSettingsRolesOption");
        textView10.setVisibility(manageGuildContext.getCanManageRoles() ? 0 : 8);
        getBinding().f2522s.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsRoles.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView11 = getBinding().j;
        m.checkNotNullExpressionValue(textView11, "binding.serverSettingsInstantInvitesOption");
        textView11.setVisibility(manageGuildContext.getCanManageServer() ? 0 : 8);
        getBinding().j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$12
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsInstantInvites.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView12 = getBinding().f2521b;
        m.checkNotNullExpressionValue(textView12, "binding.serverSettingsBansOption");
        textView12.setVisibility(manageGuildContext.getCanManageBans() ? 0 : 8);
        getBinding().f2521b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$13
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsBans.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView13 = getBinding().d;
        m.checkNotNullExpressionValue(textView13, "binding.serverSettingsEmojisOption");
        textView13.setVisibility(manageGuildContext.getCanManageEmojisAndStickers() ? 0 : 8);
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsEmojis.Companion.create(a.x(view, "v", "v.context"), WidgetServerSettings.Model.this.getGuild().getId());
            }
        });
        TextView textView14 = getBinding().h;
        m.checkNotNullExpressionValue(textView14, "binding.serverSettingsGu…oleSubscriptionGetStarted");
        textView14.setVisibility(model.getCanManageGuildRoleSubscriptions() && !model.getHasGuildRoleSubscriptions() ? 0 : 8);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$15
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsEmptyGuildRoleSubscriptions.Companion.launch(WidgetServerSettings.this.requireContext(), model.getGuild().getId());
            }
        });
        if (!model.getCanManageGuildRoleSubscriptions() || !model.getHasGuildRoleSubscriptions()) {
            z2 = false;
        }
        TextView textView15 = getBinding().f;
        m.checkNotNullExpressionValue(textView15, "binding.serverSettingsGu…RoleSubscriptionBasicInfo");
        textView15.setVisibility(z2 ? 0 : 8);
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$16
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsGuildRoleSubscriptions.Companion.launch(WidgetServerSettings.this.requireContext(), model.getGuild().getId());
            }
        });
        TextView textView16 = getBinding().i;
        m.checkNotNullExpressionValue(textView16, "binding.serverSettingsGuildRoleSubscriptionTiers");
        textView16.setVisibility(z2 ? 0 : 8);
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$17
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsGuildRoleSubscriptionTierList.Companion.launch(WidgetServerSettings.this.requireContext(), model.getGuild().getId());
            }
        });
        TextView textView17 = getBinding().g;
        m.checkNotNullExpressionValue(textView17, "binding.serverSettingsGu…dRoleSubscriptionEarnings");
        if (z2) {
            i = 0;
        }
        textView17.setVisibility(i);
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettings$configureUI$18
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsGuildRoleSubscriptionEarnings.Companion.launch(WidgetServerSettings.this.requireContext(), model.getGuild().getId());
            }
        });
        configureSectionsVisibility();
    }

    private final WidgetServerSettingsBinding getBinding() {
        return (WidgetServerSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.server_settings);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        long longExtra = getMostRecentIntent().getLongExtra("INTENT_EXTRA_GUILD_ID", -1L);
        this.guildId = longExtra;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(longExtra), this, null, 2, null), WidgetServerSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettings$onViewBoundOrOnResume$1(this));
    }
}
