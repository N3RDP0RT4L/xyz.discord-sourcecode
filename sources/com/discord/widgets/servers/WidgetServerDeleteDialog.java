package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.o;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetServerDeleteDialogBinding;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guild_delete_feedback.GuildDeleteFeedbackSheetNavigator;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import xyz.discord.R;
/* compiled from: WidgetServerDeleteDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u00142\u00020\u0001:\u0002\u0014\u0015B\u0007¢\u0006\u0004\b\u0013\u0010\fJ\u0013\u0010\u0004\u001a\u00020\u0003*\u00020\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001b\u0010\t\u001a\u00020\u00032\n\u0010\b\u001a\u00060\u0006j\u0002`\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\u000b\u0010\fR\u001d\u0010\u0012\u001a\u00020\r8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerDeleteDialog;", "Lcom/discord/app/AppDialog;", "Lcom/discord/widgets/servers/WidgetServerDeleteDialog$Model;", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerDeleteDialog$Model;)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "afterDeleteGuild", "(J)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetServerDeleteDialogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerDeleteDialogBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerDeleteDialog extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerDeleteDialog.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerDeleteDialogBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_GUILD_ID = "INTENT_GUILD_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerDeleteDialog$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetServerDeleteDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ#\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0007¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerDeleteDialog$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "show", "(Landroidx/fragment/app/FragmentManager;J)V", "", WidgetServerDeleteDialog.INTENT_GUILD_ID, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(FragmentManager fragmentManager, long j) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetServerDeleteDialog widgetServerDeleteDialog = new WidgetServerDeleteDialog();
            Bundle bundle = new Bundle();
            bundle.putLong(WidgetServerDeleteDialog.INTENT_GUILD_ID, j);
            widgetServerDeleteDialog.setArguments(bundle);
            widgetServerDeleteDialog.show(fragmentManager, WidgetServerDeleteDialog.class.getSimpleName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerDeleteDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\b\u0002\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\u0019\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\f\u0010\rR\u001b\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerDeleteDialog$Model;", "", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/models/guild/Guild;", "getGuild", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/user/MeUser;", "me", "Lcom/discord/models/user/MeUser;", "getMe", "()Lcom/discord/models/user/MeUser;", HookHelper.constructorName, "(Lcom/discord/models/user/MeUser;Lcom/discord/models/guild/Guild;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final Guild guild;

        /* renamed from: me  reason: collision with root package name */
        private final MeUser f2835me;

        /* compiled from: WidgetServerDeleteDialog.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ?\u0010\n\u001a&\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006 \u0007*\u0012\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00050\u00052\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0000¢\u0006\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerDeleteDialog$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerDeleteDialog$Model;", "kotlin.jvm.PlatformType", "get$app_productionGoogleRelease", "(J)Lrx/Observable;", "get", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get$app_productionGoogleRelease(long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
                Observable<Guild> observeGuild = companion.getGuilds().observeGuild(j);
                final WidgetServerDeleteDialog$Model$Companion$get$1 widgetServerDeleteDialog$Model$Companion$get$1 = WidgetServerDeleteDialog$Model$Companion$get$1.INSTANCE;
                Object obj = widgetServerDeleteDialog$Model$Companion$get$1;
                if (widgetServerDeleteDialog$Model$Companion$get$1 != null) {
                    obj = new Func2() { // from class: com.discord.widgets.servers.WidgetServerDeleteDialog$sam$rx_functions_Func2$0
                        @Override // rx.functions.Func2
                        public final /* synthetic */ Object call(Object obj2, Object obj3) {
                            return Function2.this.invoke(obj2, obj3);
                        }
                    };
                }
                Observable j2 = Observable.j(observeMe$default, observeGuild, (Func2) obj);
                m.checkNotNullExpressionValue(j2, "Observable.combineLatest…      ::Model\n          )");
                return ObservableExtensionsKt.computationLatest(j2).q();
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(MeUser meUser, Guild guild) {
            m.checkNotNullParameter(meUser, "me");
            this.f2835me = meUser;
            this.guild = guild;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final MeUser getMe() {
            return this.f2835me;
        }
    }

    public WidgetServerDeleteDialog() {
        super(R.layout.widget_server_delete_dialog);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void afterDeleteGuild(long j) {
        dismiss();
        Experiment userExperiment = StoreStream.Companion.getExperiments().getUserExperiment("2020-12_guild_delete_feedback", true);
        if (userExperiment != null && userExperiment.getBucket() == 1) {
            GuildDeleteFeedbackSheetNavigator.INSTANCE.enqueueNotice(j);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        CharSequence charSequence;
        if (model.getGuild() == null) {
            dismiss();
            return;
        }
        TextView textView = getBinding().d;
        m.checkNotNullExpressionValue(textView, "binding.serverSettingsDeleteServerHeader");
        Context context = getContext();
        if (context != null) {
            charSequence = b.b(context, R.string.delete_server_title, new Object[]{model.getGuild().getName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        } else {
            charSequence = null;
        }
        textView.setText(charSequence);
        LinearLayout linearLayout = getBinding().f;
        m.checkNotNullExpressionValue(linearLayout, "binding.serverSettingsDeleteServerMfaWrap");
        linearLayout.setVisibility(model.getMe().getMfaEnabled() ? 0 : 8);
        TextView textView2 = getBinding().g;
        m.checkNotNullExpressionValue(textView2, "binding.serverSettingsDeleteServerText");
        b.m(textView2, R.string.delete_server_body, new Object[]{model.getGuild().getName()}, (r4 & 4) != 0 ? b.g.j : null);
        getBinding().f2512b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerDeleteDialog$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerDeleteDialog.this.dismiss();
            }
        });
        MaterialButton materialButton = getBinding().c;
        m.checkNotNullExpressionValue(materialButton, "binding.serverSettingsDeleteServerConfirm");
        materialButton.setEnabled(model.getGuild().isOwner(model.getMe().getId()));
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerDeleteDialog$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerDeleteDialogBinding binding;
                RestAPI api = RestAPI.Companion.getApi();
                long id2 = model.getGuild().getId();
                binding = WidgetServerDeleteDialog.this.getBinding();
                TextInputLayout textInputLayout = binding.e;
                m.checkNotNullExpressionValue(textInputLayout, "binding.serverSettingsDeleteServerMfaCode");
                ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(api.deleteGuild(id2, new RestAPIParams.DeleteGuild(ViewExtensions.getTextOrEmpty(textInputLayout))), false, 1, null), WidgetServerDeleteDialog.this, null, 2, null).k(o.j(new Action1<Void>() { // from class: com.discord.widgets.servers.WidgetServerDeleteDialog$configureUI$2.1
                    public final void call(Void r4) {
                        WidgetServerDeleteDialog$configureUI$2 widgetServerDeleteDialog$configureUI$2 = WidgetServerDeleteDialog$configureUI$2.this;
                        WidgetServerDeleteDialog.this.afterDeleteGuild(model.getGuild().getId());
                    }
                }, WidgetServerDeleteDialog.this.getContext(), null, 4));
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerDeleteDialogBinding getBinding() {
        return (WidgetServerDeleteDialogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public static final void show(FragmentManager fragmentManager, long j) {
        Companion.show(fragmentManager, j);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<Model> observable = Model.Companion.get$app_productionGoogleRelease(getArgumentsOrDefault().getLong(INTENT_GUILD_ID));
        m.checkNotNullExpressionValue(observable, "Model.get(guildId)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(observable, this, null, 2, null), WidgetServerDeleteDialog.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerDeleteDialog$onViewBoundOrOnResume$1(this));
    }
}
