package com.discord.widgets.servers;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.guild.GuildFeature;
import com.discord.api.role.GuildRole;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetServerSettingsEditMemberBinding;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.user.MeUser;
import com.discord.models.user.User;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.guilds.RoleUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.permissions.ManageUserContext;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guildcommunicationdisabled.start.WidgetDisableGuildCommunication;
import com.discord.widgets.guildcommunicationdisabled.start.WidgetEnableGuildCommunication;
import com.discord.widgets.servers.WidgetServerSettingsEditMember;
import com.discord.widgets.servers.WidgetServerSettingsEditMemberRolesAdapter;
import com.discord.widgets.servers.WidgetServerSettingsTransferOwnership;
import com.discord.widgets.user.WidgetBanUser;
import com.discord.widgets.user.WidgetKickUser;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func6;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsEditMember.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \"2\u00020\u0001:\u0002\"#B\u0007¢\u0006\u0004\b!\u0010\u0014J\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\b\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\r\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0013\u0010\u0014R\u001d\u0010\u001a\u001a\u00020\u00158B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0018\u0010\u001c\u001a\u0004\u0018\u00010\u001b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u0010\u001dR\u0016\u0010\u001f\u001a\u00020\u001e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010 ¨\u0006$"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditMember;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;", "data", "", "configureUI", "(Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;)V", "setupNickname", "setupRoles", "", "nickname", "changeNickname", "(Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;Ljava/lang/String;)V", "onNicknameChangeSuccessful", "(Ljava/lang/String;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/databinding/WidgetServerSettingsEditMemberBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsEditMemberBinding;", "binding", "Lcom/discord/widgets/servers/WidgetServerSettingsEditMemberRolesAdapter;", "rolesAdapter", "Lcom/discord/widgets/servers/WidgetServerSettingsEditMemberRolesAdapter;", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEditMember extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsEditMember.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsEditMemberBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_GUILD_ID = "INTENT_EXTRA_GUILD_ID";
    private static final String INTENT_EXTRA_USER_ID = "INTENT_EXTRA_USER_ID";
    private WidgetServerSettingsEditMemberRolesAdapter rolesAdapter;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsEditMember$binding$2.INSTANCE, null, 2, null);
    private final StatefulViews state = new StatefulViews(R.id.edit_member_nickname);

    /* compiled from: WidgetServerSettingsEditMember.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J-\u0010\n\u001a\u00020\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/UserId;", "userId", "Landroid/content/Context;", "context", "", "launch", "(JJLandroid/content/Context;)V", "", "INTENT_EXTRA_GUILD_ID", "Ljava/lang/String;", WidgetServerSettingsEditMember.INTENT_EXTRA_USER_ID, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(long j, long j2, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.putExtra("INTENT_EXTRA_GUILD_ID", j);
            intent.putExtra(WidgetServerSettingsEditMember.INTENT_EXTRA_USER_ID, j2);
            j.d(context, WidgetServerSettingsEditMember.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetServerSettingsEditMember.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0015\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u001c\b\u0082\b\u0018\u0000 J2\u00020\u0001:\u0001JB{\u0012\n\u0010\u001e\u001a\u00060\u0002j\u0002`\u0003\u0012\u0006\u0010\u001f\u001a\u00020\u0006\u0012\u0006\u0010 \u001a\u00020\t\u0012\u0010\u0010!\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f\u0012\u0006\u0010\"\u001a\u00020\u000f\u0012\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0006\u0010$\u001a\u00020\u0016\u0012\u0006\u0010%\u001a\u00020\u0016\u0012\u0006\u0010&\u001a\u00020\u0016\u0012\u0006\u0010'\u001a\u00020\u0016\u0012\u0006\u0010(\u001a\u00020\u0016\u0012\u0006\u0010)\u001a\u00020\u0016¢\u0006\u0004\bH\u0010IJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\r\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001a\u0010\u0018J\u0010\u0010\u001b\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u0018J\u0010\u0010\u001c\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001c\u0010\u0018J\u0010\u0010\u001d\u001a\u00020\u0016HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u0018J\u009c\u0001\u0010*\u001a\u00020\u00002\f\b\u0002\u0010\u001e\u001a\u00060\u0002j\u0002`\u00032\b\b\u0002\u0010\u001f\u001a\u00020\u00062\b\b\u0002\u0010 \u001a\u00020\t2\u0012\b\u0002\u0010!\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f2\b\b\u0002\u0010\"\u001a\u00020\u000f2\u000e\b\u0002\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\b\b\u0002\u0010$\u001a\u00020\u00162\b\b\u0002\u0010%\u001a\u00020\u00162\b\b\u0002\u0010&\u001a\u00020\u00162\b\b\u0002\u0010'\u001a\u00020\u00162\b\b\u0002\u0010(\u001a\u00020\u00162\b\b\u0002\u0010)\u001a\u00020\u0016HÆ\u0001¢\u0006\u0004\b*\u0010+J\u0010\u0010-\u001a\u00020,HÖ\u0001¢\u0006\u0004\b-\u0010.J\u0010\u00100\u001a\u00020/HÖ\u0001¢\u0006\u0004\b0\u00101J\u001a\u00103\u001a\u00020\u00162\b\u00102\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b3\u00104R\u0019\u0010%\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b%\u00105\u001a\u0004\b6\u0010\u0018R\u0019\u0010'\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b'\u00105\u001a\u0004\b7\u0010\u0018R\u001d\u0010\u001e\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00108\u001a\u0004\b9\u0010\u0005R\u0019\u0010 \u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010:\u001a\u0004\b;\u0010\u000bR\u0019\u0010\"\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010<\u001a\u0004\b=\u0010\u0011R\u0019\u0010$\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b$\u00105\u001a\u0004\b>\u0010\u0018R\u0019\u0010)\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b)\u00105\u001a\u0004\b?\u0010\u0018R\u001f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010@\u001a\u0004\bA\u0010\u0015R\u0019\u0010&\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b&\u00105\u001a\u0004\bB\u0010\u0018R\u0019\u0010(\u001a\u00020\u00168\u0006@\u0006¢\u0006\f\n\u0004\b(\u00105\u001a\u0004\bC\u0010\u0018R#\u0010!\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\f8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010D\u001a\u0004\bE\u0010\u000eR\u0019\u0010\u001f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010F\u001a\u0004\bG\u0010\b¨\u0006K"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;", "", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/models/member/GuildMember;", "component3", "()Lcom/discord/models/member/GuildMember;", "", "component4", "()Ljava/util/Set;", "Lcom/discord/models/user/User;", "component5", "()Lcom/discord/models/user/User;", "", "Lcom/discord/widgets/servers/WidgetServerSettingsEditMemberRolesAdapter$RoleItem;", "component6", "()Ljava/util/List;", "", "component7", "()Z", "component8", "component9", "component10", "component11", "component12", "myId", "guild", "userComputed", "userRoles", "user", "roleItems", "canManage", "canKick", "canBan", "canChangeNickname", "canTransferOwnership", "canDisableCommunication", "copy", "(JLcom/discord/models/guild/Guild;Lcom/discord/models/member/GuildMember;Ljava/util/Set;Lcom/discord/models/user/User;Ljava/util/List;ZZZZZZ)Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanKick", "getCanChangeNickname", "J", "getMyId", "Lcom/discord/models/member/GuildMember;", "getUserComputed", "Lcom/discord/models/user/User;", "getUser", "getCanManage", "getCanDisableCommunication", "Ljava/util/List;", "getRoleItems", "getCanBan", "getCanTransferOwnership", "Ljava/util/Set;", "getUserRoles", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(JLcom/discord/models/guild/Guild;Lcom/discord/models/member/GuildMember;Ljava/util/Set;Lcom/discord/models/user/User;Ljava/util/List;ZZZZZZ)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canBan;
        private final boolean canChangeNickname;
        private final boolean canDisableCommunication;
        private final boolean canKick;
        private final boolean canManage;
        private final boolean canTransferOwnership;
        private final Guild guild;
        private final long myId;
        private final List<WidgetServerSettingsEditMemberRolesAdapter.RoleItem> roleItems;
        private final User user;
        private final GuildMember userComputed;
        private final Set<Long> userRoles;

        /* compiled from: WidgetServerSettingsEditMember.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ-\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u00072\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0005¢\u0006\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model$Companion;", "", "", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/UserId;", "userId", "Lrx/Observable;", "Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;", "get", "(JJ)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(final long j, final long j2) {
                StoreStream.Companion companion = StoreStream.Companion;
                Observable q = Observable.f(StoreUser.observeMe$default(companion.getUsers(), false, 1, null), companion.getUsers().observeUser(j2), companion.getGuilds().observeComputed(j), companion.getGuilds().observeGuild(j), companion.getGuilds().observeRoles(j), companion.getPermissions().observePermissionsForGuild(j), new Func6<MeUser, User, Map<Long, ? extends GuildMember>, Guild, Map<Long, ? extends GuildRole>, Long, Model>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditMember$Model$Companion$get$1
                    @Override // rx.functions.Func6
                    public /* bridge */ /* synthetic */ WidgetServerSettingsEditMember.Model call(MeUser meUser, User user, Map<Long, ? extends GuildMember> map, Guild guild, Map<Long, ? extends GuildRole> map2, Long l) {
                        return call2(meUser, user, (Map<Long, GuildMember>) map, guild, (Map<Long, GuildRole>) map2, l);
                    }

                    /* renamed from: call  reason: avoid collision after fix types in other method */
                    public final WidgetServerSettingsEditMember.Model call2(MeUser meUser, User user, Map<Long, GuildMember> map, Guild guild, Map<Long, GuildRole> map2, Long l) {
                        m.checkNotNullExpressionValue(map2, "guildRoles");
                        LinkedHashMap linkedHashMap = new LinkedHashMap();
                        Iterator<Map.Entry<Long, GuildRole>> it = map2.entrySet().iterator();
                        while (true) {
                            boolean z2 = true;
                            if (!it.hasNext()) {
                                break;
                            }
                            Map.Entry<Long, GuildRole> next = it.next();
                            if (next.getKey().longValue() == j) {
                                z2 = false;
                            }
                            if (z2) {
                                linkedHashMap.put(next.getKey(), next.getValue());
                            }
                        }
                        GuildMember guildMember = map.get(Long.valueOf(meUser.getId()));
                        GuildMember guildMember2 = map.get(Long.valueOf(j2));
                        if (guild == null || user == null || l == null || guildMember == null || guildMember2 == null) {
                            return null;
                        }
                        ManageUserContext.Companion companion2 = ManageUserContext.Companion;
                        m.checkNotNullExpressionValue(meUser, "meUser");
                        ManageUserContext from = companion2.from(guild, meUser, user, guildMember.getRoles(), guildMember2.getRoles(), l, linkedHashMap);
                        HashSet hashSet = new HashSet(guildMember2.getRoles());
                        GuildRole highestRole = RoleUtils.getHighestRole(linkedHashMap, guildMember);
                        ArrayList<GuildRole> arrayList = new ArrayList(linkedHashMap.values());
                        Collections.sort(arrayList, RoleUtils.getROLE_COMPARATOR());
                        ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
                        for (GuildRole guildRole : arrayList) {
                            arrayList2.add(new WidgetServerSettingsEditMemberRolesAdapter.RoleItem(guildRole, hashSet.contains(Long.valueOf(guildRole.getId())), highestRole, meUser.getId() == guild.getOwnerId(), from.getCanManageRoles()));
                        }
                        return new WidgetServerSettingsEditMember.Model(meUser.getId(), guild, guildMember2, hashSet, user, arrayList2, from.canManage(), from.getCanKick(), from.getCanBan(), from.getCanChangeNickname(), meUser.getId() != j2 && meUser.getId() == guild.getOwnerId(), from.getCanDisableCommunication());
                    }
                }).q();
                m.checkNotNullExpressionValue(q, "Observable.combineLatest…  .distinctUntilChanged()");
                return ObservableExtensionsKt.computationLatest(q);
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(long j, Guild guild, GuildMember guildMember, Set<Long> set, User user, List<WidgetServerSettingsEditMemberRolesAdapter.RoleItem> list, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(guildMember, "userComputed");
            m.checkNotNullParameter(set, "userRoles");
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(list, "roleItems");
            this.myId = j;
            this.guild = guild;
            this.userComputed = guildMember;
            this.userRoles = set;
            this.user = user;
            this.roleItems = list;
            this.canManage = z2;
            this.canKick = z3;
            this.canBan = z4;
            this.canChangeNickname = z5;
            this.canTransferOwnership = z6;
            this.canDisableCommunication = z7;
        }

        public final long component1() {
            return this.myId;
        }

        public final boolean component10() {
            return this.canChangeNickname;
        }

        public final boolean component11() {
            return this.canTransferOwnership;
        }

        public final boolean component12() {
            return this.canDisableCommunication;
        }

        public final Guild component2() {
            return this.guild;
        }

        public final GuildMember component3() {
            return this.userComputed;
        }

        public final Set<Long> component4() {
            return this.userRoles;
        }

        public final User component5() {
            return this.user;
        }

        public final List<WidgetServerSettingsEditMemberRolesAdapter.RoleItem> component6() {
            return this.roleItems;
        }

        public final boolean component7() {
            return this.canManage;
        }

        public final boolean component8() {
            return this.canKick;
        }

        public final boolean component9() {
            return this.canBan;
        }

        public final Model copy(long j, Guild guild, GuildMember guildMember, Set<Long> set, User user, List<WidgetServerSettingsEditMemberRolesAdapter.RoleItem> list, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
            m.checkNotNullParameter(guild, "guild");
            m.checkNotNullParameter(guildMember, "userComputed");
            m.checkNotNullParameter(set, "userRoles");
            m.checkNotNullParameter(user, "user");
            m.checkNotNullParameter(list, "roleItems");
            return new Model(j, guild, guildMember, set, user, list, z2, z3, z4, z5, z6, z7);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return this.myId == model.myId && m.areEqual(this.guild, model.guild) && m.areEqual(this.userComputed, model.userComputed) && m.areEqual(this.userRoles, model.userRoles) && m.areEqual(this.user, model.user) && m.areEqual(this.roleItems, model.roleItems) && this.canManage == model.canManage && this.canKick == model.canKick && this.canBan == model.canBan && this.canChangeNickname == model.canChangeNickname && this.canTransferOwnership == model.canTransferOwnership && this.canDisableCommunication == model.canDisableCommunication;
        }

        public final boolean getCanBan() {
            return this.canBan;
        }

        public final boolean getCanChangeNickname() {
            return this.canChangeNickname;
        }

        public final boolean getCanDisableCommunication() {
            return this.canDisableCommunication;
        }

        public final boolean getCanKick() {
            return this.canKick;
        }

        public final boolean getCanManage() {
            return this.canManage;
        }

        public final boolean getCanTransferOwnership() {
            return this.canTransferOwnership;
        }

        public final Guild getGuild() {
            return this.guild;
        }

        public final long getMyId() {
            return this.myId;
        }

        public final List<WidgetServerSettingsEditMemberRolesAdapter.RoleItem> getRoleItems() {
            return this.roleItems;
        }

        public final User getUser() {
            return this.user;
        }

        public final GuildMember getUserComputed() {
            return this.userComputed;
        }

        public final Set<Long> getUserRoles() {
            return this.userRoles;
        }

        public int hashCode() {
            int a = b.a(this.myId) * 31;
            Guild guild = this.guild;
            int i = 0;
            int hashCode = (a + (guild != null ? guild.hashCode() : 0)) * 31;
            GuildMember guildMember = this.userComputed;
            int hashCode2 = (hashCode + (guildMember != null ? guildMember.hashCode() : 0)) * 31;
            Set<Long> set = this.userRoles;
            int hashCode3 = (hashCode2 + (set != null ? set.hashCode() : 0)) * 31;
            User user = this.user;
            int hashCode4 = (hashCode3 + (user != null ? user.hashCode() : 0)) * 31;
            List<WidgetServerSettingsEditMemberRolesAdapter.RoleItem> list = this.roleItems;
            if (list != null) {
                i = list.hashCode();
            }
            int i2 = (hashCode4 + i) * 31;
            boolean z2 = this.canManage;
            int i3 = 1;
            if (z2) {
                z2 = true;
            }
            int i4 = z2 ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = (i2 + i4) * 31;
            boolean z3 = this.canKick;
            if (z3) {
                z3 = true;
            }
            int i7 = z3 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = (i6 + i7) * 31;
            boolean z4 = this.canBan;
            if (z4) {
                z4 = true;
            }
            int i10 = z4 ? 1 : 0;
            int i11 = z4 ? 1 : 0;
            int i12 = (i9 + i10) * 31;
            boolean z5 = this.canChangeNickname;
            if (z5) {
                z5 = true;
            }
            int i13 = z5 ? 1 : 0;
            int i14 = z5 ? 1 : 0;
            int i15 = (i12 + i13) * 31;
            boolean z6 = this.canTransferOwnership;
            if (z6) {
                z6 = true;
            }
            int i16 = z6 ? 1 : 0;
            int i17 = z6 ? 1 : 0;
            int i18 = (i15 + i16) * 31;
            boolean z7 = this.canDisableCommunication;
            if (!z7) {
                i3 = z7 ? 1 : 0;
            }
            return i18 + i3;
        }

        public String toString() {
            StringBuilder R = a.R("Model(myId=");
            R.append(this.myId);
            R.append(", guild=");
            R.append(this.guild);
            R.append(", userComputed=");
            R.append(this.userComputed);
            R.append(", userRoles=");
            R.append(this.userRoles);
            R.append(", user=");
            R.append(this.user);
            R.append(", roleItems=");
            R.append(this.roleItems);
            R.append(", canManage=");
            R.append(this.canManage);
            R.append(", canKick=");
            R.append(this.canKick);
            R.append(", canBan=");
            R.append(this.canBan);
            R.append(", canChangeNickname=");
            R.append(this.canChangeNickname);
            R.append(", canTransferOwnership=");
            R.append(this.canTransferOwnership);
            R.append(", canDisableCommunication=");
            return a.M(R, this.canDisableCommunication, ")");
        }
    }

    public WidgetServerSettingsEditMember() {
        super(R.layout.widget_server_settings_edit_member);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void changeNickname(Model model, String str) {
        if (model.getUser().getId() == model.getMyId()) {
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().changeGuildNickname(model.getGuild().getId(), new RestAPIParams.Nick(str)), false, 1, null), this, null, 2, null), WidgetServerSettingsEditMember.class, (r18 & 2) != 0 ? null : requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsEditMember$changeNickname$1(this, str));
            return;
        }
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().changeGuildMember(model.getGuild().getId(), model.getUser().getId(), RestAPIParams.GuildMember.Companion.createWithNick(str)), false, 1, null), this, null, 2, null), WidgetServerSettingsEditMember.class, (r18 & 2) != 0 ? null : requireContext(), (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsEditMember$changeNickname$2(this, str));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        CharSequence charSequence;
        if (model == null || !model.getCanManage()) {
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
                return;
            }
            return;
        }
        int i = 0;
        boolean z2 = true;
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        Context context = getContext();
        if (context != null) {
            charSequence = b.a.k.b.b(context, R.string.guild_members_header, new Object[]{model.getGuild().getName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
        } else {
            charSequence = null;
        }
        setActionBarTitle(charSequence);
        GuildMember.Companion companion = GuildMember.Companion;
        setActionBarSubtitle(companion.getNickOrUsername(model.getUserComputed(), model.getUser()));
        setupNickname(model);
        setupRoles(model);
        this.state.configureSaveActionView(getBinding().h);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditMember$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetServerSettingsEditMemberBinding binding;
                WidgetServerSettingsEditMemberBinding binding2;
                WidgetServerSettingsEditMemberBinding binding3;
                binding = WidgetServerSettingsEditMember.this.getBinding();
                TextInputLayout textInputLayout = binding.e;
                m.checkNotNullExpressionValue(textInputLayout, "binding.editMemberNickname");
                binding2 = WidgetServerSettingsEditMember.this.getBinding();
                TextInputLayout textInputLayout2 = binding2.e;
                m.checkNotNullExpressionValue(textInputLayout2, "binding.editMemberNickname");
                String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout2);
                int length = textOrEmpty.length() - 1;
                int i2 = 0;
                boolean z3 = false;
                while (i2 <= length) {
                    boolean z4 = m.compare(textOrEmpty.charAt(!z3 ? i2 : length), 32) <= 0;
                    if (!z3) {
                        if (!z4) {
                            z3 = true;
                        } else {
                            i2++;
                        }
                    } else if (!z4) {
                        break;
                    } else {
                        length--;
                    }
                }
                ViewExtensions.setText(textInputLayout, textOrEmpty.subSequence(i2, length + 1).toString());
                WidgetServerSettingsEditMember widgetServerSettingsEditMember = WidgetServerSettingsEditMember.this;
                WidgetServerSettingsEditMember.Model model2 = model;
                binding3 = widgetServerSettingsEditMember.getBinding();
                TextInputLayout textInputLayout3 = binding3.e;
                m.checkNotNullExpressionValue(textInputLayout3, "binding.editMemberNickname");
                widgetServerSettingsEditMember.changeNickname(model2, ViewExtensions.getTextOrEmpty(textInputLayout3));
            }
        });
        final String nickOrUsername = companion.getNickOrUsername(model.getUserComputed(), model.getUser());
        if (model.getCanDisableCommunication()) {
            final boolean isCommunicationDisabled = model.getUserComputed().isCommunicationDisabled();
            TextView textView = getBinding().i;
            m.checkNotNullExpressionValue(textView, "binding.editMemberTimeoutButton");
            b.a.k.b.m(textView, isCommunicationDisabled ? R.string.enable_guild_communication_for_user : R.string.disable_guild_communication_for_user, new Object[]{nickOrUsername}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView2 = getBinding().i;
            m.checkNotNullExpressionValue(textView2, "binding.editMemberTimeoutButton");
            textView2.setVisibility(0);
            getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditMember$configureUI$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    long id2 = model.getUser().getId();
                    long id3 = model.getGuild().getId();
                    if (isCommunicationDisabled) {
                        WidgetEnableGuildCommunication.Companion companion2 = WidgetEnableGuildCommunication.Companion;
                        FragmentManager parentFragmentManager = WidgetServerSettingsEditMember.this.getParentFragmentManager();
                        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                        companion2.launch(id2, id3, parentFragmentManager);
                        return;
                    }
                    WidgetDisableGuildCommunication.Companion.launch(id2, id3, WidgetServerSettingsEditMember.this.requireContext());
                }
            });
        } else {
            TextView textView3 = getBinding().i;
            m.checkNotNullExpressionValue(textView3, "binding.editMemberTimeoutButton");
            textView3.setVisibility(8);
            getBinding().i.setOnClickListener(null);
        }
        if (model.getCanKick()) {
            TextView textView4 = getBinding().d;
            m.checkNotNullExpressionValue(textView4, "binding.editMemberKickButton");
            b.a.k.b.m(textView4, R.string.kick_user, new Object[]{nickOrUsername}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView5 = getBinding().d;
            m.checkNotNullExpressionValue(textView5, "binding.editMemberKickButton");
            textView5.setVisibility(0);
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditMember$configureUI$3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetKickUser.Companion companion2 = WidgetKickUser.Companion;
                    String str = nickOrUsername;
                    long id2 = model.getGuild().getId();
                    long id3 = model.getUser().getId();
                    FragmentManager parentFragmentManager = WidgetServerSettingsEditMember.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion2.launch(str, id2, id3, parentFragmentManager);
                }
            });
        } else {
            TextView textView6 = getBinding().d;
            m.checkNotNullExpressionValue(textView6, "binding.editMemberKickButton");
            textView6.setVisibility(8);
            getBinding().d.setOnClickListener(null);
        }
        if (model.getCanBan()) {
            TextView textView7 = getBinding().c;
            m.checkNotNullExpressionValue(textView7, "binding.editMemberBanButton");
            b.a.k.b.m(textView7, R.string.ban_user, new Object[]{nickOrUsername}, (r4 & 4) != 0 ? b.g.j : null);
            TextView textView8 = getBinding().c;
            m.checkNotNullExpressionValue(textView8, "binding.editMemberBanButton");
            textView8.setVisibility(0);
            getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditMember$configureUI$4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetBanUser.Companion.launch(nickOrUsername, model.getGuild().getId(), model.getUser().getId(), WidgetServerSettingsEditMember.this.getParentFragmentManager());
                }
            });
        } else {
            TextView textView9 = getBinding().c;
            m.checkNotNullExpressionValue(textView9, "binding.editMemberBanButton");
            textView9.setVisibility(8);
            getBinding().c.setOnClickListener(null);
        }
        if (model.getCanTransferOwnership()) {
            TextView textView10 = getBinding().j;
            m.checkNotNullExpressionValue(textView10, "binding.editMemberTransferOwnershipButton");
            textView10.setVisibility(0);
            getBinding().j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEditMember$configureUI$5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    if (model.getGuild().getFeatures().contains(GuildFeature.VERIFIED) || model.getGuild().getFeatures().contains(GuildFeature.PARTNERED)) {
                        b.a.d.m.i(WidgetServerSettingsEditMember.this, R.string.transfer_ownership_protected_guild, 0, 4);
                        return;
                    }
                    WidgetServerSettingsTransferOwnership.Companion companion2 = WidgetServerSettingsTransferOwnership.Companion;
                    long id2 = model.getGuild().getId();
                    long id3 = model.getUser().getId();
                    FragmentManager parentFragmentManager = WidgetServerSettingsEditMember.this.getParentFragmentManager();
                    m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion2.create(id2, id3, parentFragmentManager);
                }
            });
        } else {
            TextView textView11 = getBinding().j;
            m.checkNotNullExpressionValue(textView11, "binding.editMemberTransferOwnershipButton");
            textView11.setVisibility(8);
            getBinding().j.setOnClickListener(null);
        }
        LinearLayout linearLayout = getBinding().f2534b;
        m.checkNotNullExpressionValue(linearLayout, "binding.editMemberAdministrativeContainer");
        if (!model.getCanKick() && !model.getCanBan() && !model.getCanTransferOwnership()) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        linearLayout.setVisibility(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetServerSettingsEditMemberBinding getBinding() {
        return (WidgetServerSettingsEditMemberBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onNicknameChangeSuccessful(String str) {
        AppFragment.hideKeyboard$default(this, null, 1, null);
        b.a.d.m.j(this, str.length() > 0 ? b.a.k.b.e(this, R.string.nickname_changed, new Object[]{str}, (r4 & 4) != 0 ? b.a.j : null) : b.a.k.b.e(this, R.string.nickname_cleared, new Object[0], (r4 & 4) != 0 ? b.a.j : null), 0, 4);
    }

    private final void setupNickname(Model model) {
        String nick = model.getUserComputed().getNick();
        if (nick == null) {
            nick = "";
        }
        if (model.getCanChangeNickname()) {
            TextInputLayout textInputLayout = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout, "binding.editMemberNickname");
            textInputLayout.setEndIconVisible(false);
            TextInputLayout textInputLayout2 = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout2, "binding.editMemberNickname");
            textInputLayout2.setEnabled(true);
        } else {
            TextInputLayout textInputLayout3 = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout3, "binding.editMemberNickname");
            textInputLayout3.setEndIconVisible(true);
            TextInputLayout textInputLayout4 = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout4, "binding.editMemberNickname");
            textInputLayout4.setEnabled(false);
            StatefulViews statefulViews = this.state;
            TextInputLayout textInputLayout5 = getBinding().e;
            m.checkNotNullExpressionValue(textInputLayout5, "binding.editMemberNickname");
            statefulViews.put(textInputLayout5.getId(), nick);
        }
        TextInputLayout textInputLayout6 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout6, "binding.editMemberNickname");
        StatefulViews statefulViews2 = this.state;
        TextInputLayout textInputLayout7 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout7, "binding.editMemberNickname");
        ViewExtensions.setText(textInputLayout6, (CharSequence) statefulViews2.get(textInputLayout7.getId(), nick));
    }

    private final void setupRoles(Model model) {
        WidgetServerSettingsEditMemberRolesAdapter widgetServerSettingsEditMemberRolesAdapter;
        LinearLayout linearLayout = getBinding().f;
        m.checkNotNullExpressionValue(linearLayout, "binding.editMemberRolesContainer");
        linearLayout.setVisibility(model.getRoleItems().isEmpty() ^ true ? 0 : 8);
        if ((!model.getRoleItems().isEmpty()) && (widgetServerSettingsEditMemberRolesAdapter = this.rolesAdapter) != null) {
            widgetServerSettingsEditMemberRolesAdapter.configure(model.getRoleItems(), new WidgetServerSettingsEditMember$setupRoles$1(this, model));
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setRetainInstance(true);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().g;
        m.checkNotNullExpressionValue(recyclerView, "binding.editMemberRolesRecycler");
        this.rolesAdapter = (WidgetServerSettingsEditMemberRolesAdapter) companion.configure(new WidgetServerSettingsEditMemberRolesAdapter(recyclerView));
        RecyclerView recyclerView2 = getBinding().g;
        m.checkNotNullExpressionValue(recyclerView2, "binding.editMemberRolesRecycler");
        recyclerView2.setNestedScrollingEnabled(false);
        getBinding().g.setHasFixedSize(false);
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews = this.state;
        TextInputLayout textInputLayout = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout, "binding.editMemberNickname");
        statefulViews.addOptionalFields(textInputLayout);
        StatefulViews statefulViews2 = this.state;
        FloatingActionButton floatingActionButton = getBinding().h;
        TextInputLayout textInputLayout2 = getBinding().e;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.editMemberNickname");
        statefulViews2.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout2);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        long longExtra = getMostRecentIntent().getLongExtra("INTENT_EXTRA_GUILD_ID", -1L);
        long longExtra2 = getMostRecentIntent().getLongExtra(INTENT_EXTRA_USER_ID, -1L);
        StoreStream.Companion.getGuildSubscriptions().subscribeUser(longExtra, longExtra2);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(longExtra, longExtra2), this, null, 2, null), WidgetServerSettingsEditMember.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetServerSettingsEditMember$onViewBoundOrOnResume$1(this));
    }
}
