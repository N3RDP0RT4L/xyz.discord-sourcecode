package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import b.a.d.o;
import b.a.i.t5;
import com.discord.api.user.User;
import com.discord.databinding.WidgetServerSettingsIntegrationListItemBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.restapi.RestAPIParams;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.views.CheckedSetting;
import com.discord.widgets.servers.WidgetServerSettingsIntegrations;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import kotlin.Metadata;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsIntegrationsListItem.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\u0012\u0006\u0010\u001a\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ3\u0010\u0010\u001a\u00020\u00062\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\u0006\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001f\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0003H\u0014¢\u0006\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0018\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsIntegrationsListItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "Lcom/discord/widgets/servers/WidgetServerSettingsIntegrations$Adapter;", "Lcom/discord/widgets/servers/WidgetServerSettingsIntegrations$Model$IntegrationItem;", "", "isSyncing", "", "showSyncingUI", "(Z)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "integrationId", "isTwitch", "Landroid/content/Context;", "context", "showDisableSyncDialog", "(JJZLandroid/content/Context;)V", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "data", "onConfigure", "(ILcom/discord/widgets/servers/WidgetServerSettingsIntegrations$Model$IntegrationItem;)V", "Lcom/discord/databinding/WidgetServerSettingsIntegrationListItemBinding;", "binding", "Lcom/discord/databinding/WidgetServerSettingsIntegrationListItemBinding;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/servers/WidgetServerSettingsIntegrations$Adapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsIntegrationsListItem extends MGRecyclerViewHolder<WidgetServerSettingsIntegrations.Adapter, WidgetServerSettingsIntegrations.Model.IntegrationItem> {
    private final WidgetServerSettingsIntegrationListItemBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetServerSettingsIntegrationsListItem(WidgetServerSettingsIntegrations.Adapter adapter) {
        super((int) R.layout.widget_server_settings_integration_list_item, adapter);
        m.checkNotNullParameter(adapter, "adapter");
        View view = this.itemView;
        int i = R.id.integration_header_container;
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.integration_header_container);
        if (relativeLayout != null) {
            i = R.id.integration_header_disabled_overlay;
            View findViewById = view.findViewById(R.id.integration_header_disabled_overlay);
            if (findViewById != null) {
                i = R.id.integration_icon;
                ImageView imageView = (ImageView) view.findViewById(R.id.integration_icon);
                if (imageView != null) {
                    i = R.id.integration_name;
                    TextView textView = (TextView) view.findViewById(R.id.integration_name);
                    if (textView != null) {
                        i = R.id.integration_name_container;
                        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.integration_name_container);
                        if (linearLayout != null) {
                            i = R.id.integration_owner_name;
                            TextView textView2 = (TextView) view.findViewById(R.id.integration_owner_name);
                            if (textView2 != null) {
                                i = R.id.integration_settings_icon;
                                ImageView imageView2 = (ImageView) view.findViewById(R.id.integration_settings_icon);
                                if (imageView2 != null) {
                                    i = R.id.integration_sync_switch;
                                    CheckedSetting checkedSetting = (CheckedSetting) view.findViewById(R.id.integration_sync_switch);
                                    if (checkedSetting != null) {
                                        i = R.id.integration_syncing_progress_bar;
                                        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.integration_syncing_progress_bar);
                                        if (progressBar != null) {
                                            WidgetServerSettingsIntegrationListItemBinding widgetServerSettingsIntegrationListItemBinding = new WidgetServerSettingsIntegrationListItemBinding((FrameLayout) view, relativeLayout, findViewById, imageView, textView, linearLayout, textView2, imageView2, checkedSetting, progressBar);
                                            m.checkNotNullExpressionValue(widgetServerSettingsIntegrationListItemBinding, "WidgetServerSettingsInte…temBinding.bind(itemView)");
                                            this.binding = widgetServerSettingsIntegrationListItemBinding;
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public static final /* synthetic */ WidgetServerSettingsIntegrations.Adapter access$getAdapter$p(WidgetServerSettingsIntegrationsListItem widgetServerSettingsIntegrationsListItem) {
        return (WidgetServerSettingsIntegrations.Adapter) widgetServerSettingsIntegrationsListItem.adapter;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showDisableSyncDialog(final long j, final long j2, boolean z2, final Context context) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.widget_server_settings_confirm_disable_integration, (ViewGroup) null, false);
        int i = R.id.server_settings_confirm_disable_integration_body;
        TextView textView = (TextView) inflate.findViewById(R.id.server_settings_confirm_disable_integration_body);
        if (textView != null) {
            i = R.id.server_settings_confirm_disable_integration_cancel;
            MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.server_settings_confirm_disable_integration_cancel);
            if (materialButton != null) {
                i = R.id.server_settings_confirm_disable_integration_confirm;
                MaterialButton materialButton2 = (MaterialButton) inflate.findViewById(R.id.server_settings_confirm_disable_integration_confirm);
                if (materialButton2 != null) {
                    i = R.id.server_settings_confirm_disable_integration_header;
                    TextView textView2 = (TextView) inflate.findViewById(R.id.server_settings_confirm_disable_integration_header);
                    if (textView2 != null) {
                        LinearLayout linearLayout = (LinearLayout) inflate;
                        m.checkNotNullExpressionValue(new t5(linearLayout, textView, materialButton, materialButton2, textView2), "WidgetServerSettingsConf…om(context), null, false)");
                        final AlertDialog create = new AlertDialog.Builder(context).setView(linearLayout).create();
                        m.checkNotNullExpressionValue(create, "AlertDialog.Builder(cont…ew(binding.root).create()");
                        materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsIntegrationsListItem$showDisableSyncDialog$1
                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view) {
                                AlertDialog.this.dismiss();
                            }
                        });
                        materialButton2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsIntegrationsListItem$showDisableSyncDialog$2
                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view) {
                                ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteGuildIntegration(j, j2), false, 1, null)).k(o.j(new Action1<Void>() { // from class: com.discord.widgets.servers.WidgetServerSettingsIntegrationsListItem$showDisableSyncDialog$2.1
                                    public final void call(Void r1) {
                                        create.dismiss();
                                    }
                                }, context, null, 4));
                            }
                        });
                        textView.setText(z2 ? R.string.disable_integration_twitch_body : R.string.disable_integration_youtube_body);
                        create.show();
                        return;
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i)));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showSyncingUI(boolean z2) {
        ImageView imageView = this.binding.g;
        m.checkNotNullExpressionValue(imageView, "binding.integrationSettingsIcon");
        int i = 0;
        imageView.setVisibility(z2 ^ true ? 0 : 8);
        ProgressBar progressBar = this.binding.i;
        m.checkNotNullExpressionValue(progressBar, "binding.integrationSyncingProgressBar");
        progressBar.setVisibility(z2 ? 0 : 8);
        View view = this.binding.c;
        m.checkNotNullExpressionValue(view, "binding.integrationHeaderDisabledOverlay");
        if (!z2) {
            i = 8;
        }
        view.setVisibility(i);
        if (z2) {
            this.binding.f2550b.setOnClickListener(null);
        }
    }

    public void onConfigure(int i, final WidgetServerSettingsIntegrations.Model.IntegrationItem integrationItem) {
        m.checkNotNullParameter(integrationItem, "data");
        super.onConfigure(i, (int) integrationItem);
        final boolean areEqual = m.areEqual(integrationItem.getIntegration().getType(), "twitch");
        boolean isSyncing = integrationItem.getIntegration().isSyncing();
        boolean isEnabled = integrationItem.getIntegration().isEnabled();
        final long id2 = integrationItem.getIntegration().getId();
        if (!isSyncing) {
            this.binding.f2550b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsIntegrationsListItem$onConfigure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetServerSettingsIntegrationsListItem.access$getAdapter$p(WidgetServerSettingsIntegrationsListItem.this).onIntegrationSelected(id2);
                }
            });
        } else {
            this.binding.f2550b.setOnClickListener(null);
        }
        TextView textView = this.binding.e;
        m.checkNotNullExpressionValue(textView, "binding.integrationName");
        textView.setText(integrationItem.getIntegration().getDisplayName());
        TextView textView2 = this.binding.f;
        m.checkNotNullExpressionValue(textView2, "binding.integrationOwnerName");
        User user = integrationItem.getIntegration().getUser();
        textView2.setText(user != null ? user.r() : null);
        this.binding.d.setImageResource(areEqual ? R.drawable.asset_account_sync_twitch : R.drawable.asset_account_sync_youtube);
        View view = this.binding.c;
        m.checkNotNullExpressionValue(view, "binding.integrationHeaderDisabledOverlay");
        int i2 = 0;
        if (!(isSyncing || !isEnabled)) {
            i2 = 8;
        }
        view.setVisibility(i2);
        showSyncingUI(isSyncing);
        CheckedSetting checkedSetting = this.binding.h;
        m.checkNotNullExpressionValue(checkedSetting, "binding.integrationSyncSwitch");
        checkedSetting.setChecked(isEnabled);
        CheckedSetting checkedSetting2 = this.binding.h;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.integrationSyncSwitch");
        checkedSetting2.setEnabled(!isSyncing);
        if (!isSyncing) {
            this.binding.h.setOnCheckedListener(new Action1<Boolean>() { // from class: com.discord.widgets.servers.WidgetServerSettingsIntegrationsListItem$onConfigure$2
                public final void call(Boolean bool) {
                    WidgetServerSettingsIntegrationListItemBinding widgetServerSettingsIntegrationListItemBinding;
                    WidgetServerSettingsIntegrationListItemBinding widgetServerSettingsIntegrationListItemBinding2;
                    m.checkNotNullExpressionValue(bool, "checked");
                    if (bool.booleanValue()) {
                        WidgetServerSettingsIntegrationsListItem widgetServerSettingsIntegrationsListItem = WidgetServerSettingsIntegrationsListItem.this;
                        long guildId = integrationItem.getGuildId();
                        long j = id2;
                        boolean z2 = areEqual;
                        widgetServerSettingsIntegrationListItemBinding2 = WidgetServerSettingsIntegrationsListItem.this.binding;
                        CheckedSetting checkedSetting3 = widgetServerSettingsIntegrationListItemBinding2.h;
                        m.checkNotNullExpressionValue(checkedSetting3, "binding.integrationSyncSwitch");
                        Context context = checkedSetting3.getContext();
                        m.checkNotNullExpressionValue(context, "binding.integrationSyncSwitch.context");
                        widgetServerSettingsIntegrationsListItem.showDisableSyncDialog(guildId, j, z2, context);
                        return;
                    }
                    Observable ui = ObservableExtensionsKt.ui(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().enableIntegration(integrationItem.getGuildId(), new RestAPIParams.EnableIntegration(integrationItem.getIntegration().getType(), String.valueOf(id2))), false, 1, null));
                    Action1<Void> action1 = new Action1<Void>() { // from class: com.discord.widgets.servers.WidgetServerSettingsIntegrationsListItem$onConfigure$2.1
                        public final void call(Void r3) {
                            WidgetServerSettingsIntegrationListItemBinding widgetServerSettingsIntegrationListItemBinding3;
                            WidgetServerSettingsIntegrationListItemBinding widgetServerSettingsIntegrationListItemBinding4;
                            widgetServerSettingsIntegrationListItemBinding3 = WidgetServerSettingsIntegrationsListItem.this.binding;
                            CheckedSetting checkedSetting4 = widgetServerSettingsIntegrationListItemBinding3.h;
                            m.checkNotNullExpressionValue(checkedSetting4, "binding.integrationSyncSwitch");
                            checkedSetting4.setChecked(true);
                            widgetServerSettingsIntegrationListItemBinding4 = WidgetServerSettingsIntegrationsListItem.this.binding;
                            CheckedSetting checkedSetting5 = widgetServerSettingsIntegrationListItemBinding4.h;
                            m.checkNotNullExpressionValue(checkedSetting5, "binding.integrationSyncSwitch");
                            checkedSetting5.setEnabled(false);
                            WidgetServerSettingsIntegrationsListItem.this.showSyncingUI(true);
                        }
                    };
                    widgetServerSettingsIntegrationListItemBinding = WidgetServerSettingsIntegrationsListItem.this.binding;
                    CheckedSetting checkedSetting4 = widgetServerSettingsIntegrationListItemBinding.h;
                    m.checkNotNullExpressionValue(checkedSetting4, "binding.integrationSyncSwitch");
                    ui.k(o.j(action1, checkedSetting4.getContext(), null, 4));
                }
            });
        } else {
            this.binding.h.setOnCheckedListener(null);
        }
    }
}
