package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import androidx.fragment.app.Fragment;
import b.a.d.j;
import b.a.d.o;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetServerSettingsEmojisEditBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.emoji.ModelEmojiGuild;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreEmojiGuild;
import com.discord.stores.StoreStream;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import kotlin.text.Regex;
import rx.functions.Action1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsEmojisEdit.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 $2\u00020\u0001:\u0001$B\u0007¢\u0006\u0004\b#\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\t\u0010\bJ\u000f\u0010\n\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\n\u0010\bJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\f\u0010\u0006J\u0017\u0010\u000e\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0019\u001a\u00020\u00148B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001b\u001a\u00020\u001a8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001a\u0010!\u001a\u00060\u001dj\u0002` 8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\u001fR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010\"¨\u0006%"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojisEdit;", "Lcom/discord/app/AppFragment;", "", "emojiName", "", "configureToolbar", "(Ljava/lang/String;)V", "configureMenu", "()V", "deleteEmoji", "handleSaveAlias", "sanitizedName", "onSaveSuccess", ModelAuditLogEntry.CHANGE_KEY_NAME, "sanitizeEmojiName", "(Ljava/lang/String;)Ljava/lang/String;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/databinding/WidgetServerSettingsEmojisEditBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsEmojisEditBinding;", "binding", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "", "emojiId", "J", "Lcom/discord/primitives/GuildId;", "guildId", "Ljava/lang/String;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsEmojisEdit extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsEmojisEdit.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsEmojisEditBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final Regex EMOJI_RE = new Regex("[^A-Za-z0-9_]");
    private static final String EXTRA_EMOJI_ALIAS = "EXTRA_EMOJI_ALIAS";
    private static final String EXTRA_EMOJI_ID = "EXTRA_EMOJI_ID";
    private static final String EXTRA_GUILD_ID = "EXTRA_GUILD_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsEmojisEdit$binding$2.INSTANCE, null, 2, null);
    private final StatefulViews state = new StatefulViews(R.id.server_settings_emojis_edit_alias);
    private long guildId = -1;
    private long emojiId = -1;
    private String emojiName = "";

    /* compiled from: WidgetServerSettingsEmojisEdit.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J3\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\bH\u0007¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0010\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0011¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsEmojisEdit$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/GuildId;", "guildId", "emojiId", "", "alias", "", "create", "(Landroid/content/Context;JJLjava/lang/String;)V", "Lkotlin/text/Regex;", "EMOJI_RE", "Lkotlin/text/Regex;", WidgetServerSettingsEmojisEdit.EXTRA_EMOJI_ALIAS, "Ljava/lang/String;", WidgetServerSettingsEmojisEdit.EXTRA_EMOJI_ID, WidgetServerSettingsEmojisEdit.EXTRA_GUILD_ID, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(Context context, long j, long j2, String str) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(str, "alias");
            Intent putExtra = new Intent().putExtra(WidgetServerSettingsEmojisEdit.EXTRA_GUILD_ID, j).putExtra(WidgetServerSettingsEmojisEdit.EXTRA_EMOJI_ID, j2).putExtra(WidgetServerSettingsEmojisEdit.EXTRA_EMOJI_ALIAS, str);
            m.checkNotNullExpressionValue(putExtra, "Intent()\n          .putE…EXTRA_EMOJI_ALIAS, alias)");
            j.d(context, WidgetServerSettingsEmojisEdit.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetServerSettingsEmojisEdit() {
        super(R.layout.widget_server_settings_emojis_edit);
    }

    private final void configureMenu() {
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_server_settings_emojis_edit, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEmojisEdit$configureMenu$1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_server_settings_emoji_delete) {
                    WidgetServerSettingsEmojisEdit.this.deleteEmoji();
                }
            }
        }, null, 4, null);
    }

    private final void configureToolbar(String str) {
        setActionBarTitle(R.string.emoji);
        setActionBarSubtitle(str);
    }

    public static final void create(Context context, long j, long j2, String str) {
        Companion.create(context, j, j2, str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void deleteEmoji() {
        AppFragment.hideKeyboard$default(this, null, 1, null);
        StatefulViews.clear$default(this.state, false, 1, null);
        ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteGuildEmoji(this.guildId, this.emojiId), false, 1, null), this, null, 2, null).k(o.i(new Action1<Void>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEmojisEdit$deleteEmoji$1
            public final void call(Void r5) {
                long j;
                long j2;
                StoreEmojiGuild guildEmojis = StoreStream.Companion.getGuildEmojis();
                j = WidgetServerSettingsEmojisEdit.this.guildId;
                j2 = WidgetServerSettingsEmojisEdit.this.emojiId;
                guildEmojis.deleteEmoji(j, j2);
                AppActivity appActivity = WidgetServerSettingsEmojisEdit.this.getAppActivity();
                if (appActivity != null) {
                    appActivity.onBackPressed();
                }
            }
        }, this));
    }

    private final WidgetServerSettingsEmojisEditBinding getBinding() {
        return (WidgetServerSettingsEmojisEditBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleSaveAlias() {
        TextInputLayout textInputLayout = getBinding().f2536b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.serverSettingsEmojisEditAlias");
        final String sanitizeEmojiName = sanitizeEmojiName(ViewExtensions.getTextOrEmpty(textInputLayout));
        ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().patchGuildEmoji(this.guildId, this.emojiId, new RestAPIParams.PatchGuildEmoji(sanitizeEmojiName)), false, 1, null), this, null, 2, null).k(o.i(new Action1<ModelEmojiGuild>() { // from class: com.discord.widgets.servers.WidgetServerSettingsEmojisEdit$handleSaveAlias$1
            public final void call(ModelEmojiGuild modelEmojiGuild) {
                WidgetServerSettingsEmojisEdit.this.onSaveSuccess(sanitizeEmojiName);
            }
        }, this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onSaveSuccess(String str) {
        AppFragment.hideKeyboard$default(this, null, 1, null);
        configureToolbar(str);
        StoreStream.Companion.getGuildEmojis().fetchGuildEmoji(this.guildId);
        StatefulViews statefulViews = this.state;
        TextInputLayout textInputLayout = getBinding().f2536b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.serverSettingsEmojisEditAlias");
        statefulViews.put(textInputLayout.getId(), str);
        TextInputLayout textInputLayout2 = getBinding().f2536b;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.serverSettingsEmojisEditAlias");
        StatefulViews statefulViews2 = this.state;
        TextInputLayout textInputLayout3 = getBinding().f2536b;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.serverSettingsEmojisEditAlias");
        ViewExtensions.setText(textInputLayout2, (CharSequence) statefulViews2.get(textInputLayout3.getId(), str));
        b.a.d.m.j(this, getString(R.string.save_media_success_mobile), 0, 4);
    }

    private final String sanitizeEmojiName(String str) {
        String replace = EMOJI_RE.replace(str, "");
        while (replace.length() < 2) {
            replace = replace + '_';
        }
        return replace;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        this.guildId = getMostRecentIntent().getLongExtra(EXTRA_GUILD_ID, -1L);
        this.emojiId = getMostRecentIntent().getLongExtra(EXTRA_EMOJI_ID, -1L);
        String stringExtra = getMostRecentIntent().getStringExtra(EXTRA_EMOJI_ALIAS);
        if (stringExtra == null) {
            stringExtra = "";
        }
        this.emojiName = stringExtra;
        TextInputLayout textInputLayout = getBinding().f2536b;
        m.checkNotNullExpressionValue(textInputLayout, "binding.serverSettingsEmojisEditAlias");
        StatefulViews statefulViews = this.state;
        TextInputLayout textInputLayout2 = getBinding().f2536b;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.serverSettingsEmojisEditAlias");
        ViewExtensions.setText(textInputLayout, (CharSequence) statefulViews.get(textInputLayout2.getId(), this.emojiName));
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews2 = this.state;
        FloatingActionButton floatingActionButton = getBinding().c;
        TextInputLayout textInputLayout3 = getBinding().f2536b;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.serverSettingsEmojisEditAlias");
        statefulViews2.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout3);
        this.state.configureSaveActionView(getBinding().c);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsEmojisEdit$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetServerSettingsEmojisEdit.this.handleSaveAlias();
            }
        });
        configureToolbar(this.emojiName);
        configureMenu();
        if (!isRecreated()) {
            TextInputLayout textInputLayout4 = getBinding().f2536b;
            m.checkNotNullExpressionValue(textInputLayout4, "binding.serverSettingsEmojisEditAlias");
            showKeyboard(textInputLayout4);
        }
    }
}
