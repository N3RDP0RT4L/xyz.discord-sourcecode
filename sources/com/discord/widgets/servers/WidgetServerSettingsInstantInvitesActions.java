package com.discord.widgets.servers;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.d.o;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetServerSettingsInstantInviteActionsBinding;
import com.discord.models.domain.ModelInvite;
import com.discord.models.invite.InviteUtils;
import com.discord.stores.StoreStream;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import d0.g0.t;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetServerSettingsInstantInvitesActions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u0007¢\u0006\u0004\b\u0012\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\n\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\n\u0010\u000bR\u001d\u0010\u0011\u001a\u00020\f8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvitesActions;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/models/domain/ModelInvite;", "invite", "", "handleInviteRevoked", "(Lcom/discord/models/domain/ModelInvite;)V", "", "getContentViewResId", "()I", "onResume", "()V", "Lcom/discord/databinding/WidgetServerSettingsInstantInviteActionsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetServerSettingsInstantInviteActionsBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetServerSettingsInstantInvitesActions extends AppBottomSheet {
    private static final String ARG_INVITE_CODE = "ARG_INVITE_CODE";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetServerSettingsInstantInvitesActions$binding$2.INSTANCE, null, 2, null);
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetServerSettingsInstantInvitesActions.class, "binding", "getBinding()Lcom/discord/databinding/WidgetServerSettingsInstantInviteActionsBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetServerSettingsInstantInvitesActions.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvitesActions$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "inviteCode", "", "create", "(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V", WidgetServerSettingsInstantInvitesActions.ARG_INVITE_CODE, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void create(FragmentManager fragmentManager, String str) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(str, "inviteCode");
            WidgetServerSettingsInstantInvitesActions widgetServerSettingsInstantInvitesActions = new WidgetServerSettingsInstantInvitesActions();
            Bundle bundle = new Bundle();
            bundle.putString(WidgetServerSettingsInstantInvitesActions.ARG_INVITE_CODE, str);
            widgetServerSettingsInstantInvitesActions.setArguments(bundle);
            widgetServerSettingsInstantInvitesActions.show(fragmentManager, WidgetServerSettingsInstantInvitesActions.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetServerSettingsInstantInvitesActions() {
        super(false, 1, null);
    }

    public static final void create(FragmentManager fragmentManager, String str) {
        Companion.create(fragmentManager, str);
    }

    private final WidgetServerSettingsInstantInviteActionsBinding getBinding() {
        return (WidgetServerSettingsInstantInviteActionsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleInviteRevoked(ModelInvite modelInvite) {
        StoreStream.Companion.getInstantInvites().onInviteRemoved(modelInvite);
        dismiss();
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_server_settings_instant_invite_actions;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        final String string = getArgumentsOrDefault().getString(ARG_INVITE_CODE);
        boolean z2 = false;
        if (string == null || t.isBlank(string)) {
            dismiss();
            return;
        }
        final String createLinkFromCode = InviteUtils.INSTANCE.createLinkFromCode(string, null);
        if (string.length() == 0) {
            z2 = true;
        }
        if (z2) {
            TextView textView = getBinding().e;
            m.checkNotNullExpressionValue(textView, "binding.inviteActionsTitle");
            textView.setVisibility(8);
        } else {
            TextView textView2 = getBinding().e;
            m.checkNotNullExpressionValue(textView2, "binding.inviteActionsTitle");
            textView2.setText(string);
        }
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsInstantInvitesActions$onResume$1

            /* compiled from: WidgetServerSettingsInstantInvitesActions.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/models/domain/ModelInvite;", "p1", "", "invoke", "(Lcom/discord/models/domain/ModelInvite;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.servers.WidgetServerSettingsInstantInvitesActions$onResume$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final /* synthetic */ class AnonymousClass1 extends k implements Function1<ModelInvite, Unit> {
                public AnonymousClass1(WidgetServerSettingsInstantInvitesActions widgetServerSettingsInstantInvitesActions) {
                    super(1, widgetServerSettingsInstantInvitesActions, WidgetServerSettingsInstantInvitesActions.class, "handleInviteRevoked", "handleInviteRevoked(Lcom/discord/models/domain/ModelInvite;)V", 0);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(ModelInvite modelInvite) {
                    invoke2(modelInvite);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(ModelInvite modelInvite) {
                    m.checkNotNullParameter(modelInvite, "p1");
                    ((WidgetServerSettingsInstantInvitesActions) this.receiver).handleInviteRevoked(modelInvite);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Observable ui$default = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().revokeInvite(string), false, 1, null), WidgetServerSettingsInstantInvitesActions.this, null, 2, null);
                final AnonymousClass1 r0 = new AnonymousClass1(WidgetServerSettingsInstantInvitesActions.this);
                ui$default.k(o.h(new Action1() { // from class: com.discord.widgets.servers.WidgetServerSettingsInstantInvitesActions$sam$rx_functions_Action1$0
                    @Override // rx.functions.Action1
                    public final /* synthetic */ void call(Object obj) {
                        m.checkNotNullExpressionValue(Function1.this.invoke(obj), "invoke(...)");
                    }
                }, WidgetServerSettingsInstantInvitesActions.this.getContext(), null));
            }
        });
        getBinding().f2547b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsInstantInvitesActions$onResume$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                b.a.d.m.c(a.x(view, "it", "it.context"), createLinkFromCode, 0, 4);
                WidgetServerSettingsInstantInvitesActions.this.dismiss();
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.servers.WidgetServerSettingsInstantInvitesActions$onResume$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CharSequence e;
                Context x2 = a.x(view, "it", "it.context");
                String str = createLinkFromCode;
                e = b.e(WidgetServerSettingsInstantInvitesActions.this, R.string.share_invite_mobile, new Object[]{str}, (r4 & 4) != 0 ? b.a.j : null);
                IntentUtils.performChooserSendIntent(x2, str, e);
                WidgetServerSettingsInstantInvitesActions.this.dismiss();
            }
        });
    }
}
