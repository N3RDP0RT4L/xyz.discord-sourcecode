package com.discord.widgets.servers;

import com.discord.utilities.mg_recycler.CategoricalDragAndDropAdapter;
import com.discord.widgets.servers.SettingsChannelListAdapter;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: SettingsChannelListAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;", "item", "", "invoke", "(Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;)Z", "getCanManageCategory"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SettingsChannelListAdapter$computeChangedPositions$3 extends o implements Function1<CategoricalDragAndDropAdapter.Payload, Boolean> {
    public static final SettingsChannelListAdapter$computeChangedPositions$3 INSTANCE = new SettingsChannelListAdapter$computeChangedPositions$3();

    public SettingsChannelListAdapter$computeChangedPositions$3() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(CategoricalDragAndDropAdapter.Payload payload) {
        return Boolean.valueOf(invoke2(payload));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(CategoricalDragAndDropAdapter.Payload payload) {
        m.checkNotNullParameter(payload, "item");
        int type = payload.getType();
        if (type == 0) {
            return ((SettingsChannelListAdapter.ChannelItem) payload).getCanManageCategoryOfChannel();
        }
        if (type != 1) {
            return false;
        }
        return ((SettingsChannelListAdapter.CategoryItem) payload).getCanManageCategory();
    }
}
