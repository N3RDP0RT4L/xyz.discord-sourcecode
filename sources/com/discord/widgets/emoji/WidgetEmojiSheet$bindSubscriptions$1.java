package com.discord.widgets.emoji;

import com.discord.utilities.KotlinExtensionsKt;
import com.discord.widgets.emoji.EmojiSheetViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetEmojiSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;", "it", "", "invoke", "(Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEmojiSheet$bindSubscriptions$1 extends o implements Function1<EmojiSheetViewModel.ViewState, Unit> {
    public final /* synthetic */ WidgetEmojiSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetEmojiSheet$bindSubscriptions$1(WidgetEmojiSheet widgetEmojiSheet) {
        super(1);
        this.this$0 = widgetEmojiSheet;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(EmojiSheetViewModel.ViewState viewState) {
        invoke2(viewState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(EmojiSheetViewModel.ViewState viewState) {
        Unit unit;
        m.checkNotNullParameter(viewState, "it");
        if (m.areEqual(viewState, EmojiSheetViewModel.ViewState.Loading.INSTANCE)) {
            this.this$0.showLoading();
            unit = Unit.a;
        } else if (viewState instanceof EmojiSheetViewModel.ViewState.EmojiCustom) {
            this.this$0.configureCustomEmoji((EmojiSheetViewModel.ViewState.EmojiCustom) viewState);
            unit = Unit.a;
        } else if (viewState instanceof EmojiSheetViewModel.ViewState.EmojiUnicode) {
            this.this$0.configureUnicodeEmoji((EmojiSheetViewModel.ViewState.EmojiUnicode) viewState);
            unit = Unit.a;
        } else if (m.areEqual(viewState, EmojiSheetViewModel.ViewState.Invalid.INSTANCE)) {
            this.this$0.dismiss();
            unit = Unit.a;
        } else if (m.areEqual(viewState, EmojiSheetViewModel.ViewState.Dismiss.INSTANCE)) {
            this.this$0.dismiss();
            unit = Unit.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }
}
