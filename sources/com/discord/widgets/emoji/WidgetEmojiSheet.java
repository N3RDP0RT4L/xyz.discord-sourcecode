package com.discord.widgets.emoji;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.guild.GuildFeature;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppViewFlipper;
import com.discord.databinding.WidgetEmojiSheetBinding;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.guild.Guild;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.locale.LocaleManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.input.MentionUtilsKt;
import com.discord.widgets.emoji.EmojiSheetViewModel;
import com.discord.widgets.settings.premium.WidgetSettingsPremium;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import java.text.NumberFormat;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import org.objectweb.asm.Opcodes;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetEmojiSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 >2\u00020\u0001:\u0001>B\u0007¢\u0006\u0004\b=\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0007\u0010\bJ)\u0010\u000e\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\t2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ3\u0010\u0014\u001a\u00020\u00022\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0010\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\t2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u001f\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\t2\u0006\u0010\u0017\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ/\u0010\u001f\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u0012H\u0016¢\u0006\u0004\b!\u0010\"J\u0017\u0010%\u001a\u00020\u00022\u0006\u0010$\u001a\u00020#H\u0016¢\u0006\u0004\b%\u0010&R\u0016\u0010'\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001d\u0010.\u001a\u00020)8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u001d\u00104\u001a\u00020/8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103R\u0016\u00106\u001a\u0002058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00107R\u001d\u0010<\u001a\u0002088B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b9\u00101\u001a\u0004\b:\u0010;¨\u0006?"}, d2 = {"Lcom/discord/widgets/emoji/WidgetEmojiSheet;", "Lcom/discord/app/AppBottomSheet;", "", "showLoading", "()V", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiCustom;", "viewState", "configureCustomEmoji", "(Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiCustom;)V", "", "isUserPremium", "isUserInGuild", "Lcom/discord/models/guild/Guild;", "guild", "configureButtons", "(ZZLcom/discord/models/guild/Guild;)V", "isCurrent", "isPublic", "", "approximateOnline", "configureGuildSection", "(Lcom/discord/models/guild/Guild;ZZLjava/lang/Integer;)V", "canFavorite", "isFavorite", "configureFavorite", "(ZZ)V", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiUnicode;", "configureUnicodeEmoji", "(Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiUnicode;)V", "isCurrentGuild", "isGuildPublic", "getCustomEmojiInfoText", "(ZZZZ)I", "getContentViewResId", "()I", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "emojiSizePx", "I", "Lcom/discord/databinding/WidgetEmojiSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetEmojiSheetBinding;", "binding", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "emojiIdAndType$delegate", "Lkotlin/Lazy;", "getEmojiIdAndType", "()Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "emojiIdAndType", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "imageChangeDetector", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/emoji/EmojiSheetViewModel;", "viewModel", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetEmojiSheet extends AppBottomSheet {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetEmojiSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetEmojiSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_EMOJI_ID_AND_TYPE = "EXTRA_EMOJI_ID_AND_TYPE";
    private static final int FLIPPER_INDEX_CONTENT = 0;
    private static final int FLIPPER_INDEX_LOADING = 1;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetEmojiSheet$binding$2.INSTANCE, null, 2, null);
    private final int emojiSizePx = IconUtils.getMediaProxySize(DimenUtils.dpToPixels(48));
    private final MGImages.DistinctChangeDetector imageChangeDetector = new MGImages.DistinctChangeDetector();
    private final Lazy emojiIdAndType$delegate = g.lazy(new WidgetEmojiSheet$emojiIdAndType$2(this));

    /* compiled from: WidgetEmojiSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u001f\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0015\u0010\f\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\u00020\u00048\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\u00108\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0012¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/emoji/WidgetEmojiSheet$Companion;", "", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "emojiIdAndType", "", "getNoticeName", "(Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;)Ljava/lang/String;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "show", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;)V", "enqueueNotice", "(Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;)V", WidgetEmojiSheet.EXTRA_EMOJI_ID_AND_TYPE, "Ljava/lang/String;", "", "FLIPPER_INDEX_CONTENT", "I", "FLIPPER_INDEX_LOADING", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final String getNoticeName(EmojiNode.EmojiIdAndType emojiIdAndType) {
            String str = "Emoji Sheet: " + emojiIdAndType.toString();
            m.checkNotNullExpressionValue(str, "StringBuilder(\"Emoji She…)\n            .toString()");
            return str;
        }

        public final void enqueueNotice(EmojiNode.EmojiIdAndType emojiIdAndType) {
            m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
            String noticeName = getNoticeName(emojiIdAndType);
            StoreStream.Companion.getNotices().requestToShow(new StoreNotices.Notice(noticeName, null, 0L, 0, false, null, 0L, false, 0L, new WidgetEmojiSheet$Companion$enqueueNotice$showEmojiSheetNotice$1(emojiIdAndType, noticeName), Opcodes.INVOKEVIRTUAL, null));
        }

        public final void show(FragmentManager fragmentManager, EmojiNode.EmojiIdAndType emojiIdAndType) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
            WidgetEmojiSheet widgetEmojiSheet = new WidgetEmojiSheet();
            Bundle bundle = new Bundle();
            bundle.putSerializable(WidgetEmojiSheet.EXTRA_EMOJI_ID_AND_TYPE, emojiIdAndType);
            widgetEmojiSheet.setArguments(bundle);
            widgetEmojiSheet.show(fragmentManager, WidgetEmojiSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetEmojiSheet() {
        super(false, 1, null);
        WidgetEmojiSheet$viewModel$2 widgetEmojiSheet$viewModel$2 = new WidgetEmojiSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(EmojiSheetViewModel.class), new WidgetEmojiSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetEmojiSheet$viewModel$2));
    }

    private final void configureButtons(final boolean z2, final boolean z3, final Guild guild) {
        WidgetEmojiSheetBinding binding = getBinding();
        if (guild == null) {
            FrameLayout frameLayout = binding.f2358b;
            m.checkNotNullExpressionValue(frameLayout, "buttonContainer");
            frameLayout.setVisibility(8);
        } else if (!z2) {
            FrameLayout frameLayout2 = binding.f2358b;
            m.checkNotNullExpressionValue(frameLayout2, "buttonContainer");
            frameLayout2.setVisibility(0);
            MaterialButton materialButton = binding.q;
            m.checkNotNullExpressionValue(materialButton, "premiumBtn");
            materialButton.setVisibility(0);
            MaterialButton materialButton2 = binding.o;
            m.checkNotNullExpressionValue(materialButton2, "joinBtn");
            materialButton2.setVisibility(8);
            binding.q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.emoji.WidgetEmojiSheet$configureButtons$$inlined$with$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetEmojiSheet.this.dismiss();
                    WidgetSettingsPremium.Companion companion = WidgetSettingsPremium.Companion;
                    Context requireContext = WidgetEmojiSheet.this.requireContext();
                    m.checkNotNullExpressionValue(requireContext, "requireContext()");
                    WidgetSettingsPremium.Companion.launch$default(companion, requireContext, null, Traits.Location.Section.EMOJI_SHEET_UPSELL, 2, null);
                }
            });
        } else if (!z3) {
            FrameLayout frameLayout3 = binding.f2358b;
            m.checkNotNullExpressionValue(frameLayout3, "buttonContainer");
            frameLayout3.setVisibility(0);
            MaterialButton materialButton3 = binding.q;
            m.checkNotNullExpressionValue(materialButton3, "premiumBtn");
            materialButton3.setVisibility(8);
            MaterialButton materialButton4 = binding.o;
            m.checkNotNullExpressionValue(materialButton4, "joinBtn");
            materialButton4.setVisibility(0);
            binding.o.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.emoji.WidgetEmojiSheet$configureButtons$$inlined$with$lambda$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    EmojiSheetViewModel viewModel;
                    Guild guild2 = guild;
                    viewModel = WidgetEmojiSheet.this.getViewModel();
                    viewModel.joinGuild(guild2, WidgetEmojiSheet.this);
                }
            });
        } else {
            FrameLayout frameLayout4 = binding.f2358b;
            m.checkNotNullExpressionValue(frameLayout4, "buttonContainer");
            frameLayout4.setVisibility(8);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureCustomEmoji(EmojiSheetViewModel.ViewState.EmojiCustom emojiCustom) {
        WidgetEmojiSheetBinding binding = getBinding();
        m.checkNotNullExpressionValue(binding, "binding");
        AppViewFlipper appViewFlipper = binding.a;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.root");
        ViewParent parent = appViewFlipper.getParent();
        Integer num = null;
        if (!(parent instanceof ViewGroup)) {
            parent = null;
        }
        ViewGroup viewGroup = (ViewGroup) parent;
        if (viewGroup != null) {
            ChangeBounds changeBounds = new ChangeBounds();
            changeBounds.setDuration(150L);
            TransitionManager.beginDelayedTransition(viewGroup, changeBounds);
        }
        EmojiSheetViewModel.Companion.CustomEmojGuildInfo emojiGuildInfo = emojiCustom.getEmojiGuildInfo();
        if (!(emojiGuildInfo instanceof EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Known)) {
            emojiGuildInfo = null;
        }
        EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Known known = (EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Known) emojiGuildInfo;
        boolean isPublic = emojiCustom.getEmojiGuildInfo().isPublic();
        boolean isUserInGuild = emojiCustom.getEmojiGuildInfo().isUserInGuild();
        EmojiNode.EmojiIdAndType.Custom emojiCustom2 = emojiCustom.getEmojiCustom();
        boolean isUserPremium = emojiCustom.isUserPremium();
        boolean isCurrentGuild = emojiCustom.isCurrentGuild();
        WidgetEmojiSheetBinding binding2 = getBinding();
        SimpleDraweeView simpleDraweeView = binding2.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "emojiIv");
        MGImages.setImage$default(simpleDraweeView, ModelEmojiCustom.getImageUri(emojiCustom2.getId(), emojiCustom2.isAnimated(), this.emojiSizePx), 0, 0, false, null, this.imageChangeDetector, 60, null);
        TextView textView = binding2.p;
        m.checkNotNullExpressionValue(textView, "nameTv");
        textView.setText(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + emojiCustom2.getName() + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        TextView textView2 = binding2.c;
        m.checkNotNullExpressionValue(textView2, "emojiInfoTv");
        textView2.setText(getString(getCustomEmojiInfoText(isCurrentGuild, isUserInGuild, isPublic, isUserPremium)));
        configureButtons(isUserPremium, isUserInGuild, known != null ? known.getGuild() : null);
        Guild guild = known != null ? known.getGuild() : null;
        if (known != null) {
            num = known.getApproximateOnline();
        }
        configureGuildSection(guild, isCurrentGuild, isPublic, num);
        configureFavorite(emojiCustom.getCanFavorite(), emojiCustom.isFavorite());
        AppViewFlipper appViewFlipper2 = getBinding().e;
        m.checkNotNullExpressionValue(appViewFlipper2, "binding.emojiSheetFlipper");
        appViewFlipper2.setDisplayedChild(0);
    }

    private final void configureFavorite(boolean z2, final boolean z3) {
        FrameLayout frameLayout = getBinding().g;
        m.checkNotNullExpressionValue(frameLayout, "binding.favoriteContainer");
        frameLayout.setVisibility(z2 ? 0 : 8);
        MaterialButton materialButton = getBinding().f;
        ViewExtensions.fadeBy(materialButton, !z3, 200L);
        materialButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.emoji.WidgetEmojiSheet$configureFavorite$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EmojiSheetViewModel viewModel;
                viewModel = WidgetEmojiSheet.this.getViewModel();
                viewModel.setFavorite(true);
            }
        });
        MaterialButton materialButton2 = getBinding().h;
        ViewExtensions.fadeBy(materialButton2, z3, 200L);
        materialButton2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.emoji.WidgetEmojiSheet$configureFavorite$$inlined$apply$lambda$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EmojiSheetViewModel viewModel;
                viewModel = WidgetEmojiSheet.this.getViewModel();
                viewModel.setFavorite(false);
            }
        });
    }

    private final void configureGuildSection(Guild guild, boolean z2, boolean z3, Integer num) {
        int i;
        CharSequence e;
        CharSequence e2;
        WidgetEmojiSheetBinding binding = getBinding();
        if (guild == null || z2) {
            LinearLayout linearLayout = binding.k;
            m.checkNotNullExpressionValue(linearLayout, "guildContainer");
            linearLayout.setVisibility(8);
            return;
        }
        LinearLayout linearLayout2 = binding.k;
        m.checkNotNullExpressionValue(linearLayout2, "guildContainer");
        linearLayout2.setVisibility(0);
        if (guild.hasIcon()) {
            SimpleDraweeView simpleDraweeView = binding.m;
            m.checkNotNullExpressionValue(simpleDraweeView, "guildIv");
            IconUtils.setIcon$default((ImageView) simpleDraweeView, guild, 0, (MGImages.ChangeDetector) this.imageChangeDetector, true, 4, (Object) null);
        } else {
            binding.j.setBackgroundResource(R.drawable.drawable_circle_black);
            int themedColor = ColorCompat.getThemedColor(requireContext(), (int) R.attr.colorBackgroundSecondary);
            FrameLayout frameLayout = binding.j;
            m.checkNotNullExpressionValue(frameLayout, "guildAvatarWrap");
            frameLayout.setBackgroundTintList(ColorStateList.valueOf(themedColor));
            TextView textView = binding.i;
            m.checkNotNullExpressionValue(textView, "guildAvatarText");
            textView.setText(guild.getShortName());
        }
        if (guild.getFeatures().contains(GuildFeature.PARTNERED)) {
            i = R.drawable.ic_partnered_badge_banner;
        } else {
            i = guild.getFeatures().contains(GuildFeature.VERIFIED) ? R.drawable.ic_verified_badge_banner : 0;
        }
        TextView textView2 = binding.n;
        m.checkNotNullExpressionValue(textView2, "guildNameTv");
        DrawableCompat.setCompoundDrawablesCompat(textView2, i, 0, 0, 0);
        TextView textView3 = binding.n;
        m.checkNotNullExpressionValue(textView3, "guildNameTv");
        textView3.setText(guild.getName());
        int i2 = z3 ? R.string.emoji_popout_public_server : R.string.emoji_popout_private_server;
        String str = null;
        e = b.e(this, i2, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        if (num != null) {
            String format = NumberFormat.getNumberInstance(new LocaleManager().getPrimaryLocale(requireContext())).format(Integer.valueOf(num.intValue()));
            StringBuilder sb = new StringBuilder();
            e2 = b.e(this, R.string.instant_invite_guild_members_online, new Object[]{format}, (r4 & 4) != 0 ? b.a.j : null);
            sb.append(e2.toString());
            sb.append(" • ");
            str = sb.toString();
        }
        if (str == null) {
            str = "";
        }
        TextView textView4 = binding.l;
        m.checkNotNullExpressionValue(textView4, "guildInfoTv");
        textView4.setText(str + e);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUnicodeEmoji(EmojiSheetViewModel.ViewState.EmojiUnicode emojiUnicode) {
        WidgetEmojiSheetBinding binding = getBinding();
        SimpleDraweeView simpleDraweeView = binding.d;
        m.checkNotNullExpressionValue(simpleDraweeView, "emojiIv");
        MGImages.setImage$default(simpleDraweeView, emojiUnicode.getEmojiUnicode().getImageUri(true, this.emojiSizePx, requireContext()), 0, 0, false, null, this.imageChangeDetector, 60, null);
        TextView textView = binding.p;
        m.checkNotNullExpressionValue(textView, "nameTv");
        textView.setText(MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR + emojiUnicode.getEmojiUnicode().getFirstName() + MentionUtilsKt.EMOJIS_AND_STICKERS_CHAR);
        TextView textView2 = binding.c;
        m.checkNotNullExpressionValue(textView2, "emojiInfoTv");
        textView2.setText(getString(R.string.emoji_popout_standard_emoji_description));
        FrameLayout frameLayout = binding.f2358b;
        m.checkNotNullExpressionValue(frameLayout, "buttonContainer");
        frameLayout.setVisibility(8);
        LinearLayout linearLayout = binding.k;
        m.checkNotNullExpressionValue(linearLayout, "guildContainer");
        linearLayout.setVisibility(8);
        configureFavorite(emojiUnicode.getCanFavorite(), emojiUnicode.isFavorite());
        AppViewFlipper appViewFlipper = getBinding().e;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.emojiSheetFlipper");
        appViewFlipper.setDisplayedChild(0);
    }

    private final WidgetEmojiSheetBinding getBinding() {
        return (WidgetEmojiSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final int getCustomEmojiInfoText(boolean z2, boolean z3, boolean z4, boolean z5) {
        return (!z2 || z5) ? (!z2 || !z5) ? (!z3 || z5) ? (!z3 || !z5) ? (!z4 || z5) ? (!z4 || !z5) ? R.string.emoji_popout_premium_unjoined_private_guild_description : R.string.emoji_popout_premium_unjoined_discoverable_guild_description : R.string.emoji_popout_unjoined_discoverable_guild_description : R.string.emoji_popout_premium_joined_guild_description : R.string.emoji_popout_joined_guild_description : R.string.emoji_popout_premium_current_guild_description : R.string.emoji_popout_current_guild_description;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final EmojiNode.EmojiIdAndType getEmojiIdAndType() {
        return (EmojiNode.EmojiIdAndType) this.emojiIdAndType$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final EmojiSheetViewModel getViewModel() {
        return (EmojiSheetViewModel) this.viewModel$delegate.getValue();
    }

    public static final void show(FragmentManager fragmentManager, EmojiNode.EmojiIdAndType emojiIdAndType) {
        Companion.show(fragmentManager, emojiIdAndType);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showLoading() {
        AppViewFlipper appViewFlipper = getBinding().e;
        m.checkNotNullExpressionValue(appViewFlipper, "binding.emojiSheetFlipper");
        appViewFlipper.setDisplayedChild(1);
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetEmojiSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetEmojiSheet$bindSubscriptions$1(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_emoji_sheet;
    }
}
