package com.discord.widgets.emoji;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.guild.GuildFeature;
import com.discord.app.AppViewModel;
import com.discord.models.domain.emoji.ModelEmojiUnicode;
import com.discord.models.guild.Guild;
import com.discord.models.user.MeUser;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreEmoji;
import com.discord.stores.StoreEmojiCustom;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreMediaFavorites;
import com.discord.stores.StoreUser;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.textprocessing.node.EmojiNode;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.emoji.EmojiSheetViewModel;
import com.discord.widgets.guilds.join.GuildJoinHelperKt;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import retrofit2.HttpException;
import rx.Observable;
import rx.functions.Func5;
/* compiled from: EmojiSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 52\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003567Bo\u0012\u0006\u0010#\u001a\u00020\"\u0012\b\b\u0002\u0010 \u001a\u00020\u001f\u0012\b\b\u0002\u0010,\u001a\u00020+\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u001c\u0012\b\b\u0002\u0010/\u001a\u00020.\u0012\b\b\u0002\u0010&\u001a\u00020%\u0012\b\b\u0002\u0010)\u001a\u00020(\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0016\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0019\u0012\u000e\b\u0002\u00102\u001a\b\u0012\u0004\u0012\u00020\u000301¢\u0006\u0004\b3\u00104J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\b\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\b\u0010\u0007J\u0017\u0010\t\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\t\u0010\u0007J\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\n\u0010\u0007J\u0017\u0010\u000b\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u000b\u0010\u0007J\u0015\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010#\u001a\u00020\"8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010&\u001a\u00020%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010)\u001a\u00020(8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u0010/\u001a\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100¨\u00068"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$StoreState;", "storeState", "", "handleStoreState", "(Lcom/discord/widgets/emoji/EmojiSheetViewModel$StoreState;)V", "standardPopoutAnalytics", "customPopoutAnalytics", "handleStoreStateUnicode", "handleStoreStateCustom", "", "favorite", "setFavorite", "(Z)V", "Lcom/discord/models/guild/Guild;", "guild", "Landroidx/fragment/app/Fragment;", "fragment", "joinGuild", "(Lcom/discord/models/guild/Guild;Landroidx/fragment/app/Fragment;)V", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreChannelsSelected;", "Lcom/discord/stores/StoreMediaFavorites;", "storeMediaFavorites", "Lcom/discord/stores/StoreMediaFavorites;", "Lcom/discord/stores/StoreEmojiCustom;", "storeEmojiCustom", "Lcom/discord/stores/StoreEmojiCustom;", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "emojiIdAndType", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/stores/StoreEmoji;", "storeEmoji", "Lcom/discord/stores/StoreEmoji;", "Lcom/discord/stores/StoreAnalytics;", "storeAnalytics", "Lcom/discord/stores/StoreAnalytics;", "Lrx/Observable;", "storeStateObservable", HookHelper.constructorName, "(Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreEmoji;Lcom/discord/stores/StoreEmojiCustom;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreMediaFavorites;Lrx/Observable;)V", "Companion", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class EmojiSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final EmojiNode.EmojiIdAndType emojiIdAndType;
    private final RestAPI restAPI;
    private final StoreAnalytics storeAnalytics;
    private final StoreChannelsSelected storeChannelsSelected;
    private final StoreEmoji storeEmoji;
    private final StoreEmojiCustom storeEmojiCustom;
    private final StoreGuilds storeGuilds;
    private final StoreMediaFavorites storeMediaFavorites;
    private final StoreUser storeUsers;

    /* compiled from: EmojiSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$StoreState;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/widgets/emoji/EmojiSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.emoji.EmojiSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            EmojiSheetViewModel emojiSheetViewModel = EmojiSheetViewModel.this;
            m.checkNotNullExpressionValue(storeState, "it");
            emojiSheetViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: EmojiSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u0019B\t\b\u0002¢\u0006\u0004\b\u0017\u0010\u0018JM\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J7\u0010\u0015\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00140\u00102\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u0015\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion;", "", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "emojiIdAndType", "Lcom/discord/stores/StoreUser;", "storeUsers", "Lcom/discord/stores/StoreGuilds;", "storeGuilds", "Lcom/discord/stores/StoreEmojiCustom;", "storeEmojiCustom", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreMediaFavorites;", "storeMediaFavorites", "Lrx/Observable;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$StoreState;", "observeStoreState", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreEmojiCustom;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreMediaFavorites;)Lrx/Observable;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "getGuildForCustomEmoji", "(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreEmojiCustom;)Lrx/Observable;", HookHelper.constructorName, "()V", "CustomEmojGuildInfo", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: EmojiSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\b\tB\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0004\u0082\u0001\u0002\n\u000b¨\u0006\f"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "", "", "isPublic", "()Z", "isUserInGuild", HookHelper.constructorName, "()V", "Known", "Unknown", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo$Known;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo$Unknown;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static abstract class CustomEmojGuildInfo {

            /* compiled from: EmojiSheetViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ:\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00052\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\tHÆ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u001a\u0010\u0019\u001a\u00020\u00052\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017HÖ\u0003¢\u0006\u0004\b\u0019\u0010\u001aR\u001c\u0010\r\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\r\u0010\u0007R\u001c\u0010\u000e\u001a\u00020\u00058\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u000e\u0010\u0007R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004¨\u0006\""}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo$Known;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "Lcom/discord/models/guild/Guild;", "component1", "()Lcom/discord/models/guild/Guild;", "", "component2", "()Z", "component3", "", "component4", "()Ljava/lang/Integer;", "guild", "isPublic", "isUserInGuild", "approximateOnline", "copy", "(Lcom/discord/models/guild/Guild;ZZLjava/lang/Integer;)Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo$Known;", "", "toString", "()Ljava/lang/String;", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/lang/Integer;", "getApproximateOnline", "Lcom/discord/models/guild/Guild;", "getGuild", HookHelper.constructorName, "(Lcom/discord/models/guild/Guild;ZZLjava/lang/Integer;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Known extends CustomEmojGuildInfo {
                private final Integer approximateOnline;
                private final Guild guild;
                private final boolean isPublic;
                private final boolean isUserInGuild;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public Known(Guild guild, boolean z2, boolean z3, Integer num) {
                    super(null);
                    m.checkNotNullParameter(guild, "guild");
                    this.guild = guild;
                    this.isPublic = z2;
                    this.isUserInGuild = z3;
                    this.approximateOnline = num;
                }

                public static /* synthetic */ Known copy$default(Known known, Guild guild, boolean z2, boolean z3, Integer num, int i, Object obj) {
                    if ((i & 1) != 0) {
                        guild = known.guild;
                    }
                    if ((i & 2) != 0) {
                        z2 = known.isPublic();
                    }
                    if ((i & 4) != 0) {
                        z3 = known.isUserInGuild();
                    }
                    if ((i & 8) != 0) {
                        num = known.approximateOnline;
                    }
                    return known.copy(guild, z2, z3, num);
                }

                public final Guild component1() {
                    return this.guild;
                }

                public final boolean component2() {
                    return isPublic();
                }

                public final boolean component3() {
                    return isUserInGuild();
                }

                public final Integer component4() {
                    return this.approximateOnline;
                }

                public final Known copy(Guild guild, boolean z2, boolean z3, Integer num) {
                    m.checkNotNullParameter(guild, "guild");
                    return new Known(guild, z2, z3, num);
                }

                public boolean equals(Object obj) {
                    if (this == obj) {
                        return true;
                    }
                    if (!(obj instanceof Known)) {
                        return false;
                    }
                    Known known = (Known) obj;
                    return m.areEqual(this.guild, known.guild) && isPublic() == known.isPublic() && isUserInGuild() == known.isUserInGuild() && m.areEqual(this.approximateOnline, known.approximateOnline);
                }

                public final Integer getApproximateOnline() {
                    return this.approximateOnline;
                }

                public final Guild getGuild() {
                    return this.guild;
                }

                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r3v0 */
                /* JADX WARN: Type inference failed for: r3v1 */
                /* JADX WARN: Type inference failed for: r3v2 */
                public int hashCode() {
                    Guild guild = this.guild;
                    int i = 0;
                    int hashCode = (guild != null ? guild.hashCode() : 0) * 31;
                    boolean isPublic = isPublic();
                    ?? r3 = 1;
                    if (isPublic) {
                        isPublic = true;
                    }
                    int i2 = isPublic ? 1 : 0;
                    int i3 = isPublic ? 1 : 0;
                    int i4 = (hashCode + i2) * 31;
                    boolean isUserInGuild = isUserInGuild();
                    if (!isUserInGuild) {
                        r3 = isUserInGuild;
                    }
                    int i5 = (i4 + (r3 == true ? 1 : 0)) * 31;
                    Integer num = this.approximateOnline;
                    if (num != null) {
                        i = num.hashCode();
                    }
                    return i5 + i;
                }

                @Override // com.discord.widgets.emoji.EmojiSheetViewModel.Companion.CustomEmojGuildInfo
                public boolean isPublic() {
                    return this.isPublic;
                }

                @Override // com.discord.widgets.emoji.EmojiSheetViewModel.Companion.CustomEmojGuildInfo
                public boolean isUserInGuild() {
                    return this.isUserInGuild;
                }

                public String toString() {
                    StringBuilder R = a.R("Known(guild=");
                    R.append(this.guild);
                    R.append(", isPublic=");
                    R.append(isPublic());
                    R.append(", isUserInGuild=");
                    R.append(isUserInGuild());
                    R.append(", approximateOnline=");
                    return a.E(R, this.approximateOnline, ")");
                }
            }

            /* compiled from: EmojiSheetViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u001c\u0010\u0003\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0003\u0010\u0005R\u001c\u0010\u0006\u001a\u00020\u00028\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0006\u0010\u0004\u001a\u0004\b\u0006\u0010\u0005¨\u0006\t"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo$Unknown;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "", "isPublic", "Z", "()Z", "isUserInGuild", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
            /* loaded from: classes2.dex */
            public static final class Unknown extends CustomEmojGuildInfo {
                public static final Unknown INSTANCE = new Unknown();
                private static final boolean isPublic = false;
                private static final boolean isUserInGuild = false;

                private Unknown() {
                    super(null);
                }

                @Override // com.discord.widgets.emoji.EmojiSheetViewModel.Companion.CustomEmojGuildInfo
                public boolean isPublic() {
                    return isPublic;
                }

                @Override // com.discord.widgets.emoji.EmojiSheetViewModel.Companion.CustomEmojGuildInfo
                public boolean isUserInGuild() {
                    return isUserInGuild;
                }
            }

            private CustomEmojGuildInfo() {
            }

            public abstract boolean isPublic();

            public abstract boolean isUserInGuild();

            public /* synthetic */ CustomEmojGuildInfo(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        private Companion() {
        }

        private final Observable<CustomEmojGuildInfo> getGuildForCustomEmoji(final RestAPI restAPI, EmojiNode.EmojiIdAndType emojiIdAndType, final StoreGuilds storeGuilds, final StoreEmojiCustom storeEmojiCustom) {
            if (!(emojiIdAndType instanceof EmojiNode.EmojiIdAndType.Custom)) {
                emojiIdAndType = null;
            }
            final EmojiNode.EmojiIdAndType.Custom custom = (EmojiNode.EmojiIdAndType.Custom) emojiIdAndType;
            if (custom != null) {
                Observable<CustomEmojGuildInfo> z2 = Observable.C(new Callable<Guild>() { // from class: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$getGuildForCustomEmoji$1
                    /* JADX WARN: Can't rename method to resolve collision */
                    @Override // java.util.concurrent.Callable
                    public final Guild call() {
                        Object obj;
                        Iterator<T> it = StoreEmojiCustom.this.getAllGuildEmoji().entrySet().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                obj = null;
                                break;
                            }
                            obj = it.next();
                            Map.Entry entry = (Map.Entry) obj;
                            ((Number) entry.getKey()).longValue();
                            if (((Map) entry.getValue()).keySet().contains(Long.valueOf(custom.getId()))) {
                                break;
                            }
                        }
                        Map.Entry entry2 = (Map.Entry) obj;
                        if (entry2 == null) {
                            return null;
                        }
                        long longValue = ((Number) entry2.getKey()).longValue();
                        Map map = (Map) entry2.getValue();
                        return storeGuilds.getGuilds().get(Long.valueOf(longValue));
                    }
                }).z(new b<Guild, Observable<? extends CustomEmojGuildInfo>>() { // from class: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$getGuildForCustomEmoji$2

                    /* compiled from: EmojiSheetViewModel.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/guild/Guild;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/models/guild/Guild;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/guild/Guild;)Lcom/discord/models/guild/Guild;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$getGuildForCustomEmoji$2$1  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass1<T, R> implements b<com.discord.api.guild.Guild, Guild> {
                        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                        public final Guild call(com.discord.api.guild.Guild guild) {
                            m.checkNotNullExpressionValue(guild, "it");
                            return new Guild(guild);
                        }
                    }

                    /* compiled from: EmojiSheetViewModel.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/guild/Guild;", "kotlin.jvm.PlatformType", "responseGuild", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$getGuildForCustomEmoji$2$2  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass2<T, R> implements b<Guild, EmojiSheetViewModel.Companion.CustomEmojGuildInfo> {
                        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                        public final EmojiSheetViewModel.Companion.CustomEmojGuildInfo call(Guild guild) {
                            m.checkNotNullExpressionValue(guild, "responseGuild");
                            return new EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Known(guild, true, true, Integer.valueOf(guild.getApproximatePresenceCount()));
                        }
                    }

                    /* compiled from: EmojiSheetViewModel.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/guild/Guild;", "kotlin.jvm.PlatformType", "it", "Lcom/discord/models/guild/Guild;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/guild/Guild;)Lcom/discord/models/guild/Guild;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$getGuildForCustomEmoji$2$4  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass4<T, R> implements b<com.discord.api.guild.Guild, Guild> {
                        public static final AnonymousClass4 INSTANCE = new AnonymousClass4();

                        public final Guild call(com.discord.api.guild.Guild guild) {
                            m.checkNotNullExpressionValue(guild, "it");
                            return new Guild(guild);
                        }
                    }

                    /* compiled from: EmojiSheetViewModel.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/guild/Guild;", "kotlin.jvm.PlatformType", "responseGuild", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/guild/Guild;)Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$getGuildForCustomEmoji$2$5  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass5<T, R> implements b<Guild, EmojiSheetViewModel.Companion.CustomEmojGuildInfo> {
                        public static final AnonymousClass5 INSTANCE = new AnonymousClass5();

                        public final EmojiSheetViewModel.Companion.CustomEmojGuildInfo call(Guild guild) {
                            m.checkNotNullExpressionValue(guild, "responseGuild");
                            return new EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Known(guild, true, false, Integer.valueOf(guild.getApproximatePresenceCount()));
                        }
                    }

                    /* compiled from: EmojiSheetViewModel.kt */
                    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "throwable", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Throwable;)Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                    /* renamed from: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$getGuildForCustomEmoji$2$6  reason: invalid class name */
                    /* loaded from: classes2.dex */
                    public static final class AnonymousClass6<T, R> implements b<Throwable, EmojiSheetViewModel.Companion.CustomEmojGuildInfo> {
                        public static final AnonymousClass6 INSTANCE = new AnonymousClass6();

                        public final EmojiSheetViewModel.Companion.CustomEmojGuildInfo call(Throwable th) {
                            return EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Unknown.INSTANCE;
                        }
                    }

                    public final Observable<? extends EmojiSheetViewModel.Companion.CustomEmojGuildInfo> call(final Guild guild) {
                        if (guild == null) {
                            return (Observable<R>) RestAPI.this.getEmojiGuild(custom.getId()).F(AnonymousClass4.INSTANCE).F(AnonymousClass5.INSTANCE).L(AnonymousClass6.INSTANCE);
                        }
                        if (guild.getFeatures().contains(GuildFeature.DISCOVERABLE)) {
                            return (Observable<R>) RestAPI.this.getEmojiGuild(custom.getId()).F(AnonymousClass1.INSTANCE).F(AnonymousClass2.INSTANCE).L(new b<Throwable, EmojiSheetViewModel.Companion.CustomEmojGuildInfo>() { // from class: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$getGuildForCustomEmoji$2.3
                                public final EmojiSheetViewModel.Companion.CustomEmojGuildInfo call(Throwable th) {
                                    if (!(th instanceof HttpException) || ((HttpException) th).a() != 404) {
                                        return EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Unknown.INSTANCE;
                                    }
                                    return new EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Known(Guild.this, false, true, null);
                                }
                            });
                        }
                        return new k(new EmojiSheetViewModel.Companion.CustomEmojGuildInfo.Known(guild, false, true, null));
                    }
                });
                m.checkNotNullExpressionValue(z2, "Observable.fromCallable …      }\n        }\n      }");
                return z2;
            }
            k kVar = new k(null);
            m.checkNotNullExpressionValue(kVar, "Observable.just(null)");
            return kVar;
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(RestAPI restAPI, final EmojiNode.EmojiIdAndType emojiIdAndType, StoreUser storeUser, StoreGuilds storeGuilds, StoreEmojiCustom storeEmojiCustom, StoreChannelsSelected storeChannelsSelected, StoreMediaFavorites storeMediaFavorites) {
            Observable<StoreState> g = Observable.g(getGuildForCustomEmoji(restAPI, emojiIdAndType, storeGuilds, storeEmojiCustom), StoreUser.observeMe$default(storeUser, false, 1, null), storeGuilds.observeGuilds(), storeChannelsSelected.observeSelectedChannel(), storeMediaFavorites.observeFavorites(StoreMediaFavorites.Favorite.Companion.getEmojiTypes()), new Func5<CustomEmojGuildInfo, MeUser, Map<Long, ? extends Guild>, Channel, Set<? extends StoreMediaFavorites.Favorite>, StoreState>() { // from class: com.discord.widgets.emoji.EmojiSheetViewModel$Companion$observeStoreState$1
                @Override // rx.functions.Func5
                public /* bridge */ /* synthetic */ EmojiSheetViewModel.StoreState call(EmojiSheetViewModel.Companion.CustomEmojGuildInfo customEmojGuildInfo, MeUser meUser, Map<Long, ? extends Guild> map, Channel channel, Set<? extends StoreMediaFavorites.Favorite> set) {
                    return call2(customEmojGuildInfo, meUser, (Map<Long, Guild>) map, channel, set);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final EmojiSheetViewModel.StoreState call2(EmojiSheetViewModel.Companion.CustomEmojGuildInfo customEmojGuildInfo, MeUser meUser, Map<Long, Guild> map, Channel channel, Set<? extends StoreMediaFavorites.Favorite> set) {
                    m.checkNotNullParameter(meUser, "meUser");
                    m.checkNotNullParameter(map, "guilds");
                    m.checkNotNullParameter(set, "favorites");
                    return new EmojiSheetViewModel.StoreState(customEmojGuildInfo, EmojiNode.EmojiIdAndType.this, UserUtils.INSTANCE.isPremium(meUser), map.keySet(), channel, set);
                }
            });
            m.checkNotNullExpressionValue(g, "Observable.combineLatest…es,\n          )\n        }");
            return g;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: EmojiSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0013\b\u0086\b\u0018\u00002\u00020\u0001BK\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0005\u0012\u0006\u0010\u0017\u001a\u00020\b\u0012\u0010\u0010\u0018\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u000b\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0010\u0012\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00130\u000b¢\u0006\u0004\b1\u00102J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u001a\u0010\u000e\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u000bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u000bHÆ\u0003¢\u0006\u0004\b\u0014\u0010\u000fJ`\u0010\u001b\u001a\u00020\u00002\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00052\b\b\u0002\u0010\u0017\u001a\u00020\b2\u0012\b\u0002\u0010\u0018\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u000b2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00102\u000e\b\u0002\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00130\u000bHÆ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u001dHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u001fJ\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u001a\u0010$\u001a\u00020\b2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u0017\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010&\u001a\u0004\b'\u0010\nR#\u0010\u0018\u001a\f\u0012\b\u0012\u00060\fj\u0002`\r0\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b)\u0010\u000fR\u0019\u0010\u0016\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010*\u001a\u0004\b+\u0010\u0007R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010,\u001a\u0004\b-\u0010\u0012R\u001f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00130\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010(\u001a\u0004\b.\u0010\u000fR\u001b\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010/\u001a\u0004\b0\u0010\u0004¨\u00063"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$StoreState;", "", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "component1", "()Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "component2", "()Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "", "component3", "()Z", "", "", "Lcom/discord/primitives/GuildId;", "component4", "()Ljava/util/Set;", "Lcom/discord/api/channel/Channel;", "component5", "()Lcom/discord/api/channel/Channel;", "Lcom/discord/stores/StoreMediaFavorites$Favorite;", "component6", "customEmojiGuildInfo", "emoji", "meUserIsPremium", "joinedGuildIds", "currentChannel", "favorites", "copy", "(Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;ZLjava/util/Set;Lcom/discord/api/channel/Channel;Ljava/util/Set;)Lcom/discord/widgets/emoji/EmojiSheetViewModel$StoreState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getMeUserIsPremium", "Ljava/util/Set;", "getJoinedGuildIds", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;", "getEmoji", "Lcom/discord/api/channel/Channel;", "getCurrentChannel", "getFavorites", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "getCustomEmojiGuildInfo", HookHelper.constructorName, "(Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType;ZLjava/util/Set;Lcom/discord/api/channel/Channel;Ljava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class StoreState {
        private final Channel currentChannel;
        private final Companion.CustomEmojGuildInfo customEmojiGuildInfo;
        private final EmojiNode.EmojiIdAndType emoji;
        private final Set<StoreMediaFavorites.Favorite> favorites;
        private final Set<Long> joinedGuildIds;
        private final boolean meUserIsPremium;

        /* JADX WARN: Multi-variable type inference failed */
        public StoreState(Companion.CustomEmojGuildInfo customEmojGuildInfo, EmojiNode.EmojiIdAndType emojiIdAndType, boolean z2, Set<Long> set, Channel channel, Set<? extends StoreMediaFavorites.Favorite> set2) {
            m.checkNotNullParameter(emojiIdAndType, "emoji");
            m.checkNotNullParameter(set, "joinedGuildIds");
            m.checkNotNullParameter(set2, "favorites");
            this.customEmojiGuildInfo = customEmojGuildInfo;
            this.emoji = emojiIdAndType;
            this.meUserIsPremium = z2;
            this.joinedGuildIds = set;
            this.currentChannel = channel;
            this.favorites = set2;
        }

        public static /* synthetic */ StoreState copy$default(StoreState storeState, Companion.CustomEmojGuildInfo customEmojGuildInfo, EmojiNode.EmojiIdAndType emojiIdAndType, boolean z2, Set set, Channel channel, Set set2, int i, Object obj) {
            if ((i & 1) != 0) {
                customEmojGuildInfo = storeState.customEmojiGuildInfo;
            }
            if ((i & 2) != 0) {
                emojiIdAndType = storeState.emoji;
            }
            EmojiNode.EmojiIdAndType emojiIdAndType2 = emojiIdAndType;
            if ((i & 4) != 0) {
                z2 = storeState.meUserIsPremium;
            }
            boolean z3 = z2;
            Set<Long> set3 = set;
            if ((i & 8) != 0) {
                set3 = storeState.joinedGuildIds;
            }
            Set set4 = set3;
            if ((i & 16) != 0) {
                channel = storeState.currentChannel;
            }
            Channel channel2 = channel;
            Set<StoreMediaFavorites.Favorite> set5 = set2;
            if ((i & 32) != 0) {
                set5 = storeState.favorites;
            }
            return storeState.copy(customEmojGuildInfo, emojiIdAndType2, z3, set4, channel2, set5);
        }

        public final Companion.CustomEmojGuildInfo component1() {
            return this.customEmojiGuildInfo;
        }

        public final EmojiNode.EmojiIdAndType component2() {
            return this.emoji;
        }

        public final boolean component3() {
            return this.meUserIsPremium;
        }

        public final Set<Long> component4() {
            return this.joinedGuildIds;
        }

        public final Channel component5() {
            return this.currentChannel;
        }

        public final Set<StoreMediaFavorites.Favorite> component6() {
            return this.favorites;
        }

        public final StoreState copy(Companion.CustomEmojGuildInfo customEmojGuildInfo, EmojiNode.EmojiIdAndType emojiIdAndType, boolean z2, Set<Long> set, Channel channel, Set<? extends StoreMediaFavorites.Favorite> set2) {
            m.checkNotNullParameter(emojiIdAndType, "emoji");
            m.checkNotNullParameter(set, "joinedGuildIds");
            m.checkNotNullParameter(set2, "favorites");
            return new StoreState(customEmojGuildInfo, emojiIdAndType, z2, set, channel, set2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StoreState)) {
                return false;
            }
            StoreState storeState = (StoreState) obj;
            return m.areEqual(this.customEmojiGuildInfo, storeState.customEmojiGuildInfo) && m.areEqual(this.emoji, storeState.emoji) && this.meUserIsPremium == storeState.meUserIsPremium && m.areEqual(this.joinedGuildIds, storeState.joinedGuildIds) && m.areEqual(this.currentChannel, storeState.currentChannel) && m.areEqual(this.favorites, storeState.favorites);
        }

        public final Channel getCurrentChannel() {
            return this.currentChannel;
        }

        public final Companion.CustomEmojGuildInfo getCustomEmojiGuildInfo() {
            return this.customEmojiGuildInfo;
        }

        public final EmojiNode.EmojiIdAndType getEmoji() {
            return this.emoji;
        }

        public final Set<StoreMediaFavorites.Favorite> getFavorites() {
            return this.favorites;
        }

        public final Set<Long> getJoinedGuildIds() {
            return this.joinedGuildIds;
        }

        public final boolean getMeUserIsPremium() {
            return this.meUserIsPremium;
        }

        public int hashCode() {
            Companion.CustomEmojGuildInfo customEmojGuildInfo = this.customEmojiGuildInfo;
            int i = 0;
            int hashCode = (customEmojGuildInfo != null ? customEmojGuildInfo.hashCode() : 0) * 31;
            EmojiNode.EmojiIdAndType emojiIdAndType = this.emoji;
            int hashCode2 = (hashCode + (emojiIdAndType != null ? emojiIdAndType.hashCode() : 0)) * 31;
            boolean z2 = this.meUserIsPremium;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode2 + i2) * 31;
            Set<Long> set = this.joinedGuildIds;
            int hashCode3 = (i4 + (set != null ? set.hashCode() : 0)) * 31;
            Channel channel = this.currentChannel;
            int hashCode4 = (hashCode3 + (channel != null ? channel.hashCode() : 0)) * 31;
            Set<StoreMediaFavorites.Favorite> set2 = this.favorites;
            if (set2 != null) {
                i = set2.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("StoreState(customEmojiGuildInfo=");
            R.append(this.customEmojiGuildInfo);
            R.append(", emoji=");
            R.append(this.emoji);
            R.append(", meUserIsPremium=");
            R.append(this.meUserIsPremium);
            R.append(", joinedGuildIds=");
            R.append(this.joinedGuildIds);
            R.append(", currentChannel=");
            R.append(this.currentChannel);
            R.append(", favorites=");
            R.append(this.favorites);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: EmojiSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\u0004\u0005\u0006\u0007\bB\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0005\t\n\u000b\f\r¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Dismiss", "EmojiCustom", "EmojiUnicode", "Invalid", "Loading", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$Invalid;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$Dismiss;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiCustom;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiUnicode;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: EmojiSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$Dismiss;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Dismiss extends ViewState {
            public static final Dismiss INSTANCE = new Dismiss();

            private Dismiss() {
                super(null);
            }
        }

        /* compiled from: EmojiSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\u0006\u0010\u0010\u001a\u00020\b\u0012\u0006\u0010\u0011\u001a\u00020\b\u0012\u0006\u0010\u0012\u001a\u00020\b\u0012\u0006\u0010\u0013\u001a\u00020\b¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000b\u0010\nJ\u0010\u0010\f\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\f\u0010\nJ\u0010\u0010\r\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\r\u0010\nJL\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\b\b\u0002\u0010\u0010\u001a\u00020\b2\b\b\u0002\u0010\u0011\u001a\u00020\b2\b\b\u0002\u0010\u0012\u001a\u00020\b2\b\b\u0002\u0010\u0013\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u001a\u001a\u00020\u0019HÖ\u0001¢\u0006\u0004\b\u001a\u0010\u001bJ\u001a\u0010\u001e\u001a\u00020\b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001cHÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u0019\u0010\u0012\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010 \u001a\u0004\b!\u0010\nR\u0019\u0010\u000f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b\u0010\u0010\nR\u0019\u0010\u0013\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b\u0013\u0010\nR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010$\u001a\u0004\b%\u0010\u0004R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b\u0011\u0010\n¨\u0006("}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiCustom;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;", "component1", "()Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "component2", "()Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "", "component3", "()Z", "component4", "component5", "component6", "emojiCustom", "emojiGuildInfo", "isUserPremium", "isCurrentGuild", "canFavorite", "isFavorite", "copy", "(Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;ZZZZ)Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiCustom;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanFavorite", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;", "getEmojiGuildInfo", "Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;", "getEmojiCustom", HookHelper.constructorName, "(Lcom/discord/utilities/textprocessing/node/EmojiNode$EmojiIdAndType$Custom;Lcom/discord/widgets/emoji/EmojiSheetViewModel$Companion$CustomEmojGuildInfo;ZZZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiCustom extends ViewState {
            private final boolean canFavorite;
            private final EmojiNode.EmojiIdAndType.Custom emojiCustom;
            private final Companion.CustomEmojGuildInfo emojiGuildInfo;
            private final boolean isCurrentGuild;
            private final boolean isFavorite;
            private final boolean isUserPremium;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmojiCustom(EmojiNode.EmojiIdAndType.Custom custom, Companion.CustomEmojGuildInfo customEmojGuildInfo, boolean z2, boolean z3, boolean z4, boolean z5) {
                super(null);
                m.checkNotNullParameter(custom, "emojiCustom");
                m.checkNotNullParameter(customEmojGuildInfo, "emojiGuildInfo");
                this.emojiCustom = custom;
                this.emojiGuildInfo = customEmojGuildInfo;
                this.isUserPremium = z2;
                this.isCurrentGuild = z3;
                this.canFavorite = z4;
                this.isFavorite = z5;
            }

            public static /* synthetic */ EmojiCustom copy$default(EmojiCustom emojiCustom, EmojiNode.EmojiIdAndType.Custom custom, Companion.CustomEmojGuildInfo customEmojGuildInfo, boolean z2, boolean z3, boolean z4, boolean z5, int i, Object obj) {
                if ((i & 1) != 0) {
                    custom = emojiCustom.emojiCustom;
                }
                if ((i & 2) != 0) {
                    customEmojGuildInfo = emojiCustom.emojiGuildInfo;
                }
                Companion.CustomEmojGuildInfo customEmojGuildInfo2 = customEmojGuildInfo;
                if ((i & 4) != 0) {
                    z2 = emojiCustom.isUserPremium;
                }
                boolean z6 = z2;
                if ((i & 8) != 0) {
                    z3 = emojiCustom.isCurrentGuild;
                }
                boolean z7 = z3;
                if ((i & 16) != 0) {
                    z4 = emojiCustom.canFavorite;
                }
                boolean z8 = z4;
                if ((i & 32) != 0) {
                    z5 = emojiCustom.isFavorite;
                }
                return emojiCustom.copy(custom, customEmojGuildInfo2, z6, z7, z8, z5);
            }

            public final EmojiNode.EmojiIdAndType.Custom component1() {
                return this.emojiCustom;
            }

            public final Companion.CustomEmojGuildInfo component2() {
                return this.emojiGuildInfo;
            }

            public final boolean component3() {
                return this.isUserPremium;
            }

            public final boolean component4() {
                return this.isCurrentGuild;
            }

            public final boolean component5() {
                return this.canFavorite;
            }

            public final boolean component6() {
                return this.isFavorite;
            }

            public final EmojiCustom copy(EmojiNode.EmojiIdAndType.Custom custom, Companion.CustomEmojGuildInfo customEmojGuildInfo, boolean z2, boolean z3, boolean z4, boolean z5) {
                m.checkNotNullParameter(custom, "emojiCustom");
                m.checkNotNullParameter(customEmojGuildInfo, "emojiGuildInfo");
                return new EmojiCustom(custom, customEmojGuildInfo, z2, z3, z4, z5);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EmojiCustom)) {
                    return false;
                }
                EmojiCustom emojiCustom = (EmojiCustom) obj;
                return m.areEqual(this.emojiCustom, emojiCustom.emojiCustom) && m.areEqual(this.emojiGuildInfo, emojiCustom.emojiGuildInfo) && this.isUserPremium == emojiCustom.isUserPremium && this.isCurrentGuild == emojiCustom.isCurrentGuild && this.canFavorite == emojiCustom.canFavorite && this.isFavorite == emojiCustom.isFavorite;
            }

            public final boolean getCanFavorite() {
                return this.canFavorite;
            }

            public final EmojiNode.EmojiIdAndType.Custom getEmojiCustom() {
                return this.emojiCustom;
            }

            public final Companion.CustomEmojGuildInfo getEmojiGuildInfo() {
                return this.emojiGuildInfo;
            }

            public int hashCode() {
                EmojiNode.EmojiIdAndType.Custom custom = this.emojiCustom;
                int i = 0;
                int hashCode = (custom != null ? custom.hashCode() : 0) * 31;
                Companion.CustomEmojGuildInfo customEmojGuildInfo = this.emojiGuildInfo;
                if (customEmojGuildInfo != null) {
                    i = customEmojGuildInfo.hashCode();
                }
                int i2 = (hashCode + i) * 31;
                boolean z2 = this.isUserPremium;
                int i3 = 1;
                if (z2) {
                    z2 = true;
                }
                int i4 = z2 ? 1 : 0;
                int i5 = z2 ? 1 : 0;
                int i6 = (i2 + i4) * 31;
                boolean z3 = this.isCurrentGuild;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.canFavorite;
                if (z4) {
                    z4 = true;
                }
                int i10 = z4 ? 1 : 0;
                int i11 = z4 ? 1 : 0;
                int i12 = (i9 + i10) * 31;
                boolean z5 = this.isFavorite;
                if (!z5) {
                    i3 = z5 ? 1 : 0;
                }
                return i12 + i3;
            }

            public final boolean isCurrentGuild() {
                return this.isCurrentGuild;
            }

            public final boolean isFavorite() {
                return this.isFavorite;
            }

            public final boolean isUserPremium() {
                return this.isUserPremium;
            }

            public String toString() {
                StringBuilder R = a.R("EmojiCustom(emojiCustom=");
                R.append(this.emojiCustom);
                R.append(", emojiGuildInfo=");
                R.append(this.emojiGuildInfo);
                R.append(", isUserPremium=");
                R.append(this.isUserPremium);
                R.append(", isCurrentGuild=");
                R.append(this.isCurrentGuild);
                R.append(", canFavorite=");
                R.append(this.canFavorite);
                R.append(", isFavorite=");
                return a.M(R, this.isFavorite, ")");
            }
        }

        /* compiled from: EmojiSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u001a\u0010\u0016\u001a\u00020\u00052\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014HÖ\u0003¢\u0006\u0004\b\u0016\u0010\u0017R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0018\u001a\u0004\b\u000b\u0010\u0007¨\u0006\u001e"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiUnicode;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "component1", "()Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "", "component2", "()Z", "component3", "emojiUnicode", "canFavorite", "isFavorite", "copy", "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;ZZ)Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$EmojiUnicode;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getCanFavorite", "Lcom/discord/models/domain/emoji/ModelEmojiUnicode;", "getEmojiUnicode", HookHelper.constructorName, "(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EmojiUnicode extends ViewState {
            private final boolean canFavorite;
            private final ModelEmojiUnicode emojiUnicode;
            private final boolean isFavorite;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EmojiUnicode(ModelEmojiUnicode modelEmojiUnicode, boolean z2, boolean z3) {
                super(null);
                m.checkNotNullParameter(modelEmojiUnicode, "emojiUnicode");
                this.emojiUnicode = modelEmojiUnicode;
                this.canFavorite = z2;
                this.isFavorite = z3;
            }

            public static /* synthetic */ EmojiUnicode copy$default(EmojiUnicode emojiUnicode, ModelEmojiUnicode modelEmojiUnicode, boolean z2, boolean z3, int i, Object obj) {
                if ((i & 1) != 0) {
                    modelEmojiUnicode = emojiUnicode.emojiUnicode;
                }
                if ((i & 2) != 0) {
                    z2 = emojiUnicode.canFavorite;
                }
                if ((i & 4) != 0) {
                    z3 = emojiUnicode.isFavorite;
                }
                return emojiUnicode.copy(modelEmojiUnicode, z2, z3);
            }

            public final ModelEmojiUnicode component1() {
                return this.emojiUnicode;
            }

            public final boolean component2() {
                return this.canFavorite;
            }

            public final boolean component3() {
                return this.isFavorite;
            }

            public final EmojiUnicode copy(ModelEmojiUnicode modelEmojiUnicode, boolean z2, boolean z3) {
                m.checkNotNullParameter(modelEmojiUnicode, "emojiUnicode");
                return new EmojiUnicode(modelEmojiUnicode, z2, z3);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EmojiUnicode)) {
                    return false;
                }
                EmojiUnicode emojiUnicode = (EmojiUnicode) obj;
                return m.areEqual(this.emojiUnicode, emojiUnicode.emojiUnicode) && this.canFavorite == emojiUnicode.canFavorite && this.isFavorite == emojiUnicode.isFavorite;
            }

            public final boolean getCanFavorite() {
                return this.canFavorite;
            }

            public final ModelEmojiUnicode getEmojiUnicode() {
                return this.emojiUnicode;
            }

            public int hashCode() {
                ModelEmojiUnicode modelEmojiUnicode = this.emojiUnicode;
                int hashCode = (modelEmojiUnicode != null ? modelEmojiUnicode.hashCode() : 0) * 31;
                boolean z2 = this.canFavorite;
                int i = 1;
                if (z2) {
                    z2 = true;
                }
                int i2 = z2 ? 1 : 0;
                int i3 = z2 ? 1 : 0;
                int i4 = (hashCode + i2) * 31;
                boolean z3 = this.isFavorite;
                if (!z3) {
                    i = z3 ? 1 : 0;
                }
                return i4 + i;
            }

            public final boolean isFavorite() {
                return this.isFavorite;
            }

            public String toString() {
                StringBuilder R = a.R("EmojiUnicode(emojiUnicode=");
                R.append(this.emojiUnicode);
                R.append(", canFavorite=");
                R.append(this.canFavorite);
                R.append(", isFavorite=");
                return a.M(R, this.isFavorite, ")");
            }
        }

        /* compiled from: EmojiSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$Invalid;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: EmojiSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState$Loading;", "Lcom/discord/widgets/emoji/EmojiSheetViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Loading extends ViewState {
            public static final Loading INSTANCE = new Loading();

            private Loading() {
                super(null);
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ EmojiSheetViewModel(com.discord.utilities.textprocessing.node.EmojiNode.EmojiIdAndType r10, com.discord.utilities.rest.RestAPI r11, com.discord.stores.StoreEmoji r12, com.discord.stores.StoreEmojiCustom r13, com.discord.stores.StoreAnalytics r14, com.discord.stores.StoreUser r15, com.discord.stores.StoreGuilds r16, com.discord.stores.StoreChannelsSelected r17, com.discord.stores.StoreMediaFavorites r18, rx.Observable r19, int r20, kotlin.jvm.internal.DefaultConstructorMarker r21) {
        /*
            r9 = this;
            r0 = r20
            r1 = r0 & 2
            if (r1 == 0) goto Ld
            com.discord.utilities.rest.RestAPI$Companion r1 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r1 = r1.getApi()
            goto Le
        Ld:
            r1 = r11
        Le:
            r2 = r0 & 4
            if (r2 == 0) goto L19
            com.discord.stores.StoreStream$Companion r2 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreEmoji r2 = r2.getEmojis()
            goto L1a
        L19:
            r2 = r12
        L1a:
            r3 = r0 & 8
            if (r3 == 0) goto L25
            com.discord.stores.StoreStream$Companion r3 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreEmojiCustom r3 = r3.getCustomEmojis()
            goto L26
        L25:
            r3 = r13
        L26:
            r4 = r0 & 16
            if (r4 == 0) goto L31
            com.discord.stores.StoreStream$Companion r4 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreAnalytics r4 = r4.getAnalytics()
            goto L32
        L31:
            r4 = r14
        L32:
            r5 = r0 & 32
            if (r5 == 0) goto L3d
            com.discord.stores.StoreStream$Companion r5 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r5 = r5.getUsers()
            goto L3e
        L3d:
            r5 = r15
        L3e:
            r6 = r0 & 64
            if (r6 == 0) goto L49
            com.discord.stores.StoreStream$Companion r6 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreGuilds r6 = r6.getGuilds()
            goto L4b
        L49:
            r6 = r16
        L4b:
            r7 = r0 & 128(0x80, float:1.794E-43)
            if (r7 == 0) goto L56
            com.discord.stores.StoreStream$Companion r7 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannelsSelected r7 = r7.getChannelsSelected()
            goto L58
        L56:
            r7 = r17
        L58:
            r8 = r0 & 256(0x100, float:3.59E-43)
            if (r8 == 0) goto L63
            com.discord.stores.StoreStream$Companion r8 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreMediaFavorites r8 = r8.getMediaFavorites()
            goto L65
        L63:
            r8 = r18
        L65:
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 == 0) goto L7b
            com.discord.widgets.emoji.EmojiSheetViewModel$Companion r0 = com.discord.widgets.emoji.EmojiSheetViewModel.Companion
            r11 = r0
            r12 = r1
            r13 = r10
            r14 = r5
            r15 = r6
            r16 = r3
            r17 = r7
            r18 = r8
            rx.Observable r0 = com.discord.widgets.emoji.EmojiSheetViewModel.Companion.access$observeStoreState(r11, r12, r13, r14, r15, r16, r17, r18)
            goto L7d
        L7b:
            r0 = r19
        L7d:
            r11 = r9
            r12 = r10
            r13 = r1
            r14 = r2
            r15 = r3
            r16 = r4
            r17 = r5
            r18 = r6
            r19 = r7
            r20 = r8
            r21 = r0
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.emoji.EmojiSheetViewModel.<init>(com.discord.utilities.textprocessing.node.EmojiNode$EmojiIdAndType, com.discord.utilities.rest.RestAPI, com.discord.stores.StoreEmoji, com.discord.stores.StoreEmojiCustom, com.discord.stores.StoreAnalytics, com.discord.stores.StoreUser, com.discord.stores.StoreGuilds, com.discord.stores.StoreChannelsSelected, com.discord.stores.StoreMediaFavorites, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final void customPopoutAnalytics(StoreState storeState) {
        EmojiNode.EmojiIdAndType emoji = storeState.getEmoji();
        Objects.requireNonNull(emoji, "null cannot be cast to non-null type com.discord.utilities.textprocessing.node.EmojiNode.EmojiIdAndType.Custom");
        EmojiNode.EmojiIdAndType.Custom custom = (EmojiNode.EmojiIdAndType.Custom) emoji;
        Companion.CustomEmojGuildInfo customEmojiGuildInfo = storeState.getCustomEmojiGuildInfo();
        m.checkNotNull(customEmojiGuildInfo);
        boolean z2 = !customEmojiGuildInfo.isPublic();
        boolean isUserInGuild = customEmojiGuildInfo.isUserInGuild();
        boolean meUserIsPremium = storeState.getMeUserIsPremium();
        Channel currentChannel = storeState.getCurrentChannel();
        if (currentChannel != null) {
            this.storeAnalytics.openCustomEmojiPopout(currentChannel, custom.getId(), meUserIsPremium, isUserInGuild, z2);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        EmojiNode.EmojiIdAndType emoji = storeState.getEmoji();
        if (emoji instanceof EmojiNode.EmojiIdAndType.Unicode) {
            standardPopoutAnalytics(storeState);
            handleStoreStateUnicode(storeState);
        } else if (emoji instanceof EmojiNode.EmojiIdAndType.Custom) {
            customPopoutAnalytics(storeState);
            handleStoreStateCustom(storeState);
        }
    }

    private final void handleStoreStateCustom(StoreState storeState) {
        boolean z2;
        Guild guild;
        EmojiNode.EmojiIdAndType emoji = storeState.getEmoji();
        Objects.requireNonNull(emoji, "null cannot be cast to non-null type com.discord.utilities.textprocessing.node.EmojiNode.EmojiIdAndType.Custom");
        EmojiNode.EmojiIdAndType.Custom custom = (EmojiNode.EmojiIdAndType.Custom) emoji;
        Companion.CustomEmojGuildInfo customEmojiGuildInfo = storeState.getCustomEmojiGuildInfo();
        m.checkNotNull(customEmojiGuildInfo);
        boolean contains = storeState.getFavorites().contains(new StoreMediaFavorites.Favorite.FavCustomEmoji(custom));
        boolean isUserInGuild = customEmojiGuildInfo.isUserInGuild();
        Channel currentChannel = storeState.getCurrentChannel();
        boolean z3 = false;
        if (currentChannel != null) {
            long f = currentChannel.f();
            Companion.CustomEmojGuildInfo.Known known = (Companion.CustomEmojGuildInfo.Known) (!(customEmojiGuildInfo instanceof Companion.CustomEmojGuildInfo.Known) ? null : customEmojiGuildInfo);
            if (!(known == null || (guild = known.getGuild()) == null || guild.getId() != f)) {
                z3 = true;
            }
            z2 = z3;
        } else {
            z2 = false;
        }
        updateViewState(new ViewState.EmojiCustom(custom, customEmojiGuildInfo, storeState.getMeUserIsPremium(), z2, isUserInGuild, contains));
    }

    private final void handleStoreStateUnicode(StoreState storeState) {
        Object obj;
        EmojiNode.EmojiIdAndType emoji = storeState.getEmoji();
        Objects.requireNonNull(emoji, "null cannot be cast to non-null type com.discord.utilities.textprocessing.node.EmojiNode.EmojiIdAndType.Unicode");
        ModelEmojiUnicode modelEmojiUnicode = this.storeEmoji.getUnicodeEmojisNamesMap().get(((EmojiNode.EmojiIdAndType.Unicode) emoji).getName());
        if (modelEmojiUnicode != null) {
            obj = new ViewState.EmojiUnicode(modelEmojiUnicode, true, storeState.getFavorites().contains(new StoreMediaFavorites.Favorite.FavUnicodeEmoji(modelEmojiUnicode)));
        } else {
            obj = ViewState.Invalid.INSTANCE;
        }
        updateViewState(obj);
    }

    private final void standardPopoutAnalytics(StoreState storeState) {
        Channel currentChannel = storeState.getCurrentChannel();
        if (currentChannel != null) {
            this.storeAnalytics.openUnicodeEmojiPopout(currentChannel);
        }
    }

    public final void joinGuild(Guild guild, Fragment fragment) {
        m.checkNotNullParameter(guild, "guild");
        m.checkNotNullParameter(fragment, "fragment");
        Context requireContext = fragment.requireContext();
        m.checkNotNullExpressionValue(requireContext, "fragment.requireContext()");
        GuildJoinHelperKt.joinGuild(requireContext, guild.getId(), false, (r27 & 8) != 0 ? null : null, (r27 & 16) != 0 ? null : null, (r27 & 32) != 0 ? null : null, EmojiSheetViewModel.class, (r27 & 128) != 0 ? null : null, (r27 & 256) != 0 ? null : null, (r27 & 512) != 0 ? null : null, new EmojiSheetViewModel$joinGuild$1(this));
    }

    public final void setFavorite(boolean z2) {
        Object obj;
        ViewState viewState = getViewState();
        if (viewState != null) {
            if (viewState instanceof ViewState.EmojiCustom) {
                obj = new StoreMediaFavorites.Favorite.FavCustomEmoji(((ViewState.EmojiCustom) viewState).getEmojiCustom());
            } else if (viewState instanceof ViewState.EmojiUnicode) {
                obj = new StoreMediaFavorites.Favorite.FavUnicodeEmoji(((ViewState.EmojiUnicode) viewState).getEmojiUnicode());
            } else {
                return;
            }
            Object exhaustive = KotlinExtensionsKt.getExhaustive(obj);
            if (z2) {
                this.storeMediaFavorites.addFavorite((StoreMediaFavorites.Favorite) exhaustive);
            } else {
                this.storeMediaFavorites.removeFavorite((StoreMediaFavorites.Favorite) exhaustive);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public EmojiSheetViewModel(EmojiNode.EmojiIdAndType emojiIdAndType, RestAPI restAPI, StoreEmoji storeEmoji, StoreEmojiCustom storeEmojiCustom, StoreAnalytics storeAnalytics, StoreUser storeUser, StoreGuilds storeGuilds, StoreChannelsSelected storeChannelsSelected, StoreMediaFavorites storeMediaFavorites, Observable<StoreState> observable) {
        super(ViewState.Loading.INSTANCE);
        m.checkNotNullParameter(emojiIdAndType, "emojiIdAndType");
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(storeEmoji, "storeEmoji");
        m.checkNotNullParameter(storeEmojiCustom, "storeEmojiCustom");
        m.checkNotNullParameter(storeAnalytics, "storeAnalytics");
        m.checkNotNullParameter(storeUser, "storeUsers");
        m.checkNotNullParameter(storeGuilds, "storeGuilds");
        m.checkNotNullParameter(storeChannelsSelected, "storeChannelsSelected");
        m.checkNotNullParameter(storeMediaFavorites, "storeMediaFavorites");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.emojiIdAndType = emojiIdAndType;
        this.restAPI = restAPI;
        this.storeEmoji = storeEmoji;
        this.storeEmojiCustom = storeEmojiCustom;
        this.storeAnalytics = storeAnalytics;
        this.storeUsers = storeUser;
        this.storeGuilds = storeGuilds;
        this.storeChannelsSelected = storeChannelsSelected;
        this.storeMediaFavorites = storeMediaFavorites;
        Observable q = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "storeStateObservable\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, EmojiSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
