package com.discord.widgets.captcha;

import com.discord.app.AppActivity;
import com.discord.utilities.captcha.CaptchaHelper;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetCaptchaBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "token", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCaptchaBottomSheet$openCaptcha$1 extends o implements Function1<String, Unit> {
    public final /* synthetic */ WidgetCaptchaBottomSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCaptchaBottomSheet$openCaptcha$1(WidgetCaptchaBottomSheet widgetCaptchaBottomSheet) {
        super(1);
        this.this$0 = widgetCaptchaBottomSheet;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(String str) {
        invoke2(str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        CaptchaHelper.INSTANCE.setCaptchaToken(str);
        this.this$0.captchaPassed = true;
        AppActivity appActivity = this.this$0.getAppActivity();
        if (appActivity != null) {
            this.this$0.finishIfCaptchaTokenReceived(appActivity);
        }
    }
}
