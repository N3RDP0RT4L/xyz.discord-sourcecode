package com.discord.widgets.feedback;

import andhook.lib.HookHelper;
import androidx.annotation.StringRes;
import b.d.b.a.a;
import com.discord.widgets.voice.feedback.FeedbackIssue;
import com.discord.widgets.voice.feedback.FeedbackRating;
import com.discord.widgets.voice.feedback.PendingFeedback;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: FeedbackSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0002\u0015\u0016J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J#\u0010\u000b\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u00072\b\u0010\n\u001a\u0004\u0018\u00010\tH&¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H&¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH&¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u000fH&¢\u0006\u0004\b\u0014\u0010\u0012¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/feedback/FeedbackSheetViewModel;", "", "Lcom/discord/widgets/voice/feedback/FeedbackRating;", "feedbackRating", "", "selectRating", "(Lcom/discord/widgets/voice/feedback/FeedbackRating;)V", "Lcom/discord/widgets/voice/feedback/FeedbackIssue;", "feedbackIssue", "", "reasonDescription", "selectIssue", "(Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;)V", "submitForm", "()V", "Lrx/Observable;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;", "observeViewState", "()Lrx/Observable;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;", "observeEvents", "Event", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public interface FeedbackSheetViewModel {

    /* compiled from: FeedbackSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class DefaultImpls {
        public static void selectRating(FeedbackSheetViewModel feedbackSheetViewModel, FeedbackRating feedbackRating) {
            m.checkNotNullParameter(feedbackRating, "feedbackRating");
        }
    }

    /* compiled from: FeedbackSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;", "", HookHelper.constructorName, "()V", "NavigateToIssueDetails", "Submitted", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: FeedbackSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0018\u001a\u0004\b\u0019\u0010\u0004¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;", "Lcom/discord/widgets/voice/feedback/PendingFeedback;", "component1", "()Lcom/discord/widgets/voice/feedback/PendingFeedback;", "", "component2", "()Z", "pendingFeedback", "showCxLinkForIssueDetails", "copy", "(Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowCxLinkForIssueDetails", "Lcom/discord/widgets/voice/feedback/PendingFeedback;", "getPendingFeedback", HookHelper.constructorName, "(Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class NavigateToIssueDetails extends Event {
            private final PendingFeedback pendingFeedback;
            private final boolean showCxLinkForIssueDetails;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public NavigateToIssueDetails(PendingFeedback pendingFeedback, boolean z2) {
                super(null);
                m.checkNotNullParameter(pendingFeedback, "pendingFeedback");
                this.pendingFeedback = pendingFeedback;
                this.showCxLinkForIssueDetails = z2;
            }

            public static /* synthetic */ NavigateToIssueDetails copy$default(NavigateToIssueDetails navigateToIssueDetails, PendingFeedback pendingFeedback, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    pendingFeedback = navigateToIssueDetails.pendingFeedback;
                }
                if ((i & 2) != 0) {
                    z2 = navigateToIssueDetails.showCxLinkForIssueDetails;
                }
                return navigateToIssueDetails.copy(pendingFeedback, z2);
            }

            public final PendingFeedback component1() {
                return this.pendingFeedback;
            }

            public final boolean component2() {
                return this.showCxLinkForIssueDetails;
            }

            public final NavigateToIssueDetails copy(PendingFeedback pendingFeedback, boolean z2) {
                m.checkNotNullParameter(pendingFeedback, "pendingFeedback");
                return new NavigateToIssueDetails(pendingFeedback, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof NavigateToIssueDetails)) {
                    return false;
                }
                NavigateToIssueDetails navigateToIssueDetails = (NavigateToIssueDetails) obj;
                return m.areEqual(this.pendingFeedback, navigateToIssueDetails.pendingFeedback) && this.showCxLinkForIssueDetails == navigateToIssueDetails.showCxLinkForIssueDetails;
            }

            public final PendingFeedback getPendingFeedback() {
                return this.pendingFeedback;
            }

            public final boolean getShowCxLinkForIssueDetails() {
                return this.showCxLinkForIssueDetails;
            }

            public int hashCode() {
                PendingFeedback pendingFeedback = this.pendingFeedback;
                int hashCode = (pendingFeedback != null ? pendingFeedback.hashCode() : 0) * 31;
                boolean z2 = this.showCxLinkForIssueDetails;
                if (z2) {
                    z2 = true;
                }
                int i = z2 ? 1 : 0;
                int i2 = z2 ? 1 : 0;
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("NavigateToIssueDetails(pendingFeedback=");
                R.append(this.pendingFeedback);
                R.append(", showCxLinkForIssueDetails=");
                return a.M(R, this.showCxLinkForIssueDetails, ")");
            }
        }

        /* compiled from: FeedbackSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u00022\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;", "", "component1", "()Z", "showConfirmation", "copy", "(Z)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getShowConfirmation", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Submitted extends Event {
            private final boolean showConfirmation;

            public Submitted(boolean z2) {
                super(null);
                this.showConfirmation = z2;
            }

            public static /* synthetic */ Submitted copy$default(Submitted submitted, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = submitted.showConfirmation;
                }
                return submitted.copy(z2);
            }

            public final boolean component1() {
                return this.showConfirmation;
            }

            public final Submitted copy(boolean z2) {
                return new Submitted(z2);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Submitted) && this.showConfirmation == ((Submitted) obj).showConfirmation;
                }
                return true;
            }

            public final boolean getShowConfirmation() {
                return this.showConfirmation;
            }

            public int hashCode() {
                boolean z2 = this.showConfirmation;
                if (z2) {
                    return 1;
                }
                return z2 ? 1 : 0;
            }

            public String toString() {
                return a.M(a.R("Submitted(showConfirmation="), this.showConfirmation, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    Observable<Event> observeEvents();

    Observable<ViewState> observeViewState();

    void selectIssue(FeedbackIssue feedbackIssue, String str);

    void selectRating(FeedbackRating feedbackRating);

    void submitForm();

    /* compiled from: FeedbackSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0001\u0010\u0011\u001a\u00020\t\u0012\n\b\u0003\u0010\u0012\u001a\u0004\u0018\u00010\t\u0012\b\b\u0001\u0010\u0013\u001a\u00020\t¢\u0006\u0004\b+\u0010,J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\f\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000bJJ\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0003\u0010\u0011\u001a\u00020\t2\n\b\u0003\u0010\u0012\u001a\u0004\u0018\u00010\t2\b\b\u0003\u0010\u0013\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0019\u0010\u000bJ\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0004R\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\bR\u001b\u0010\u0012\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\rR\u0019\u0010$\u001a\u00020\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u0019\u0010\u0013\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010(\u001a\u0004\b)\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010(\u001a\u0004\b*\u0010\u000b¨\u0006-"}, d2 = {"Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;", "", "Lcom/discord/widgets/voice/feedback/FeedbackRating;", "component1", "()Lcom/discord/widgets/voice/feedback/FeedbackRating;", "", "Lcom/discord/widgets/voice/feedback/FeedbackIssue;", "component2", "()Ljava/util/List;", "", "component3", "()I", "component4", "()Ljava/lang/Integer;", "component5", "selectedFeedbackRating", "feedbackIssues", "titleTextResId", "promptTextResId", "issuesHeaderTextResId", "copy", "(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;ILjava/lang/Integer;I)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/voice/feedback/FeedbackRating;", "getSelectedFeedbackRating", "Ljava/util/List;", "getFeedbackIssues", "Ljava/lang/Integer;", "getPromptTextResId", "showFaceRatings", "Z", "getShowFaceRatings", "()Z", "I", "getIssuesHeaderTextResId", "getTitleTextResId", HookHelper.constructorName, "(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;ILjava/lang/Integer;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final List<FeedbackIssue> feedbackIssues;
        private final int issuesHeaderTextResId;
        private final Integer promptTextResId;
        private final FeedbackRating selectedFeedbackRating;
        private final boolean showFaceRatings;
        private final int titleTextResId;

        /* JADX WARN: Multi-variable type inference failed */
        public ViewState(FeedbackRating feedbackRating, List<? extends FeedbackIssue> list, @StringRes int i, @StringRes Integer num, @StringRes int i2) {
            m.checkNotNullParameter(feedbackRating, "selectedFeedbackRating");
            m.checkNotNullParameter(list, "feedbackIssues");
            this.selectedFeedbackRating = feedbackRating;
            this.feedbackIssues = list;
            this.titleTextResId = i;
            this.promptTextResId = num;
            this.issuesHeaderTextResId = i2;
            this.showFaceRatings = num != null;
        }

        public static /* synthetic */ ViewState copy$default(ViewState viewState, FeedbackRating feedbackRating, List list, int i, Integer num, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                feedbackRating = viewState.selectedFeedbackRating;
            }
            List<FeedbackIssue> list2 = list;
            if ((i3 & 2) != 0) {
                list2 = viewState.feedbackIssues;
            }
            List list3 = list2;
            if ((i3 & 4) != 0) {
                i = viewState.titleTextResId;
            }
            int i4 = i;
            if ((i3 & 8) != 0) {
                num = viewState.promptTextResId;
            }
            Integer num2 = num;
            if ((i3 & 16) != 0) {
                i2 = viewState.issuesHeaderTextResId;
            }
            return viewState.copy(feedbackRating, list3, i4, num2, i2);
        }

        public final FeedbackRating component1() {
            return this.selectedFeedbackRating;
        }

        public final List<FeedbackIssue> component2() {
            return this.feedbackIssues;
        }

        public final int component3() {
            return this.titleTextResId;
        }

        public final Integer component4() {
            return this.promptTextResId;
        }

        public final int component5() {
            return this.issuesHeaderTextResId;
        }

        public final ViewState copy(FeedbackRating feedbackRating, List<? extends FeedbackIssue> list, @StringRes int i, @StringRes Integer num, @StringRes int i2) {
            m.checkNotNullParameter(feedbackRating, "selectedFeedbackRating");
            m.checkNotNullParameter(list, "feedbackIssues");
            return new ViewState(feedbackRating, list, i, num, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.selectedFeedbackRating, viewState.selectedFeedbackRating) && m.areEqual(this.feedbackIssues, viewState.feedbackIssues) && this.titleTextResId == viewState.titleTextResId && m.areEqual(this.promptTextResId, viewState.promptTextResId) && this.issuesHeaderTextResId == viewState.issuesHeaderTextResId;
        }

        public final List<FeedbackIssue> getFeedbackIssues() {
            return this.feedbackIssues;
        }

        public final int getIssuesHeaderTextResId() {
            return this.issuesHeaderTextResId;
        }

        public final Integer getPromptTextResId() {
            return this.promptTextResId;
        }

        public final FeedbackRating getSelectedFeedbackRating() {
            return this.selectedFeedbackRating;
        }

        public final boolean getShowFaceRatings() {
            return this.showFaceRatings;
        }

        public final int getTitleTextResId() {
            return this.titleTextResId;
        }

        public int hashCode() {
            FeedbackRating feedbackRating = this.selectedFeedbackRating;
            int i = 0;
            int hashCode = (feedbackRating != null ? feedbackRating.hashCode() : 0) * 31;
            List<FeedbackIssue> list = this.feedbackIssues;
            int hashCode2 = (((hashCode + (list != null ? list.hashCode() : 0)) * 31) + this.titleTextResId) * 31;
            Integer num = this.promptTextResId;
            if (num != null) {
                i = num.hashCode();
            }
            return ((hashCode2 + i) * 31) + this.issuesHeaderTextResId;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(selectedFeedbackRating=");
            R.append(this.selectedFeedbackRating);
            R.append(", feedbackIssues=");
            R.append(this.feedbackIssues);
            R.append(", titleTextResId=");
            R.append(this.titleTextResId);
            R.append(", promptTextResId=");
            R.append(this.promptTextResId);
            R.append(", issuesHeaderTextResId=");
            return a.A(R, this.issuesHeaderTextResId, ")");
        }

        public /* synthetic */ ViewState(FeedbackRating feedbackRating, List list, int i, Integer num, int i2, int i3, DefaultConstructorMarker defaultConstructorMarker) {
            this(feedbackRating, list, i, (i3 & 8) != 0 ? null : num, i2);
        }
    }
}
