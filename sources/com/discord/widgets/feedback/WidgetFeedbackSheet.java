package com.discord.widgets.feedback;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetFeedbackSheetBinding;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.feedback.FeedbackSheetViewModel;
import com.discord.widgets.guild_delete_feedback.GuildDeleteFeedbackSheetViewModel;
import com.discord.widgets.voice.feedback.FeedbackRating;
import com.discord.widgets.voice.feedback.FeedbackView;
import com.discord.widgets.voice.feedback.WidgetIssueDetailsForm;
import com.discord.widgets.voice.feedback.call.CallFeedbackSheetViewModel;
import com.discord.widgets.voice.feedback.stream.StreamFeedbackSheetViewModel;
import d0.z.d.a0;
import d0.z.d.m;
import java.io.Serializable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: WidgetFeedbackSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 =2\u00020\u0001:\u0002=>B\u0007¢\u0006\u0004\b<\u0010\u000fJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u0017\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u001b\u001a\u00020\u00188B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR(\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00040\u001c8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u001d\u0010+\u001a\u00020&8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001d\u00100\u001a\u00020,8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b-\u0010(\u001a\u0004\b.\u0010/R\u001d\u00105\u001a\u0002018B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b2\u0010(\u001a\u0004\b3\u00104R\u001d\u0010;\u001a\u0002068B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b7\u00108\u001a\u0004\b9\u0010:¨\u0006?"}, d2 = {"Lcom/discord/widgets/feedback/WidgetFeedbackSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;", "viewState", "", "updateView", "(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;)V", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;)V", "", "getContentViewResId", "()I", "onResume", "()V", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "Landroid/content/DialogInterface;", "dialog", "onCancel", "(Landroid/content/DialogInterface;)V", "Lcom/discord/widgets/feedback/FeedbackSheetViewModel;", "getViewModel", "()Lcom/discord/widgets/feedback/FeedbackSheetViewModel;", "viewModel", "Lkotlin/Function0;", "onDismissed", "Lkotlin/jvm/functions/Function0;", "getOnDismissed", "()Lkotlin/jvm/functions/Function0;", "setOnDismissed", "(Lkotlin/jvm/functions/Function0;)V", "", "isShowingFeedbackIssues", "Z", "Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;", "viewModelStreamFeedbackSheet$delegate", "Lkotlin/Lazy;", "getViewModelStreamFeedbackSheet", "()Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;", "viewModelStreamFeedbackSheet", "Lcom/discord/widgets/guild_delete_feedback/GuildDeleteFeedbackSheetViewModel;", "viewModelGuildDeleteFeedbackSheet$delegate", "getViewModelGuildDeleteFeedbackSheet", "()Lcom/discord/widgets/guild_delete_feedback/GuildDeleteFeedbackSheetViewModel;", "viewModelGuildDeleteFeedbackSheet", "Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;", "viewModelCallFeedbackSheet$delegate", "getViewModelCallFeedbackSheet", "()Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;", "viewModelCallFeedbackSheet", "Lcom/discord/databinding/WidgetFeedbackSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetFeedbackSheetBinding;", "binding", HookHelper.constructorName, "Companion", "FeedbackType", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetFeedbackSheet extends AppBottomSheet {
    public static final String ARG_CALL_FEEDBACK_CONFIG = "ARG_CONFIG";
    public static final String ARG_FEEDBACK_TYPE = "ARG_FEEDBACK_TYPE";
    public static final String ARG_GUILD_DELETE_FEEDBACK_GUILD_ID = "ARG_GUILD_DELETE_FEEDBACK_GUILD_ID";
    public static final String ARG_STREAM_FEEDBACK_MEDIA_SESSION_ID = "ARG_STREAM_FEEDBACK_MEDIA_SESSION_ID";
    public static final String ARG_STREAM_FEEDBACK_STREAM_KEY = "ARG_STREAM_FEEDBACK_STREAM_KEY ";
    private static final long SHEET_DISMISS_DELAY_MS = 600;
    private static final long SHEET_EXPAND_DELAY_MS = 100;
    private boolean isShowingFeedbackIssues;
    private final Lazy viewModelCallFeedbackSheet$delegate;
    private final Lazy viewModelGuildDeleteFeedbackSheet$delegate;
    private final Lazy viewModelStreamFeedbackSheet$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetFeedbackSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetFeedbackSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private Function0<Unit> onDismissed = WidgetFeedbackSheet$onDismissed$1.INSTANCE;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetFeedbackSheet$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetFeedbackSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u00020\u00078\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\n\u0010\tR\u0016\u0010\u000b\u001a\u00020\u00078\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u000b\u0010\tR\u0016\u0010\f\u001a\u00020\u00078\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\f\u0010\tR\u0016\u0010\r\u001a\u00020\u00078\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\r\u0010\tR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0010¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;", "", "Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;", "feedbackType", "Lcom/discord/widgets/feedback/WidgetFeedbackSheet;", "newInstance", "(Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;)Lcom/discord/widgets/feedback/WidgetFeedbackSheet;", "", "ARG_CALL_FEEDBACK_CONFIG", "Ljava/lang/String;", WidgetFeedbackSheet.ARG_FEEDBACK_TYPE, WidgetFeedbackSheet.ARG_GUILD_DELETE_FEEDBACK_GUILD_ID, WidgetFeedbackSheet.ARG_STREAM_FEEDBACK_MEDIA_SESSION_ID, "ARG_STREAM_FEEDBACK_STREAM_KEY", "", "SHEET_DISMISS_DELAY_MS", "J", "SHEET_EXPAND_DELAY_MS", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final WidgetFeedbackSheet newInstance(FeedbackType feedbackType) {
            m.checkNotNullParameter(feedbackType, "feedbackType");
            WidgetFeedbackSheet widgetFeedbackSheet = new WidgetFeedbackSheet();
            Bundle bundle = new Bundle();
            bundle.putSerializable(WidgetFeedbackSheet.ARG_FEEDBACK_TYPE, feedbackType);
            widgetFeedbackSheet.setArguments(bundle);
            return widgetFeedbackSheet;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetFeedbackSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "CALL", "STREAM", "GUILD_DELETE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum FeedbackType {
        CALL,
        STREAM,
        GUILD_DELETE
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            FeedbackType.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[FeedbackType.CALL.ordinal()] = 1;
            iArr[FeedbackType.STREAM.ordinal()] = 2;
            iArr[FeedbackType.GUILD_DELETE.ordinal()] = 3;
        }
    }

    public WidgetFeedbackSheet() {
        super(false, 1, null);
        WidgetFeedbackSheet$viewModelCallFeedbackSheet$2 widgetFeedbackSheet$viewModelCallFeedbackSheet$2 = new WidgetFeedbackSheet$viewModelCallFeedbackSheet$2(this);
        f0 f0Var = new f0(this);
        this.viewModelCallFeedbackSheet$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(CallFeedbackSheetViewModel.class), new WidgetFeedbackSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetFeedbackSheet$viewModelCallFeedbackSheet$2));
        WidgetFeedbackSheet$viewModelStreamFeedbackSheet$2 widgetFeedbackSheet$viewModelStreamFeedbackSheet$2 = new WidgetFeedbackSheet$viewModelStreamFeedbackSheet$2(this);
        f0 f0Var2 = new f0(this);
        this.viewModelStreamFeedbackSheet$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(StreamFeedbackSheetViewModel.class), new WidgetFeedbackSheet$appViewModels$$inlined$viewModels$2(f0Var2), new h0(widgetFeedbackSheet$viewModelStreamFeedbackSheet$2));
        WidgetFeedbackSheet$viewModelGuildDeleteFeedbackSheet$2 widgetFeedbackSheet$viewModelGuildDeleteFeedbackSheet$2 = new WidgetFeedbackSheet$viewModelGuildDeleteFeedbackSheet$2(this);
        f0 f0Var3 = new f0(this);
        this.viewModelGuildDeleteFeedbackSheet$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(GuildDeleteFeedbackSheetViewModel.class), new WidgetFeedbackSheet$appViewModels$$inlined$viewModels$3(f0Var3), new h0(widgetFeedbackSheet$viewModelGuildDeleteFeedbackSheet$2));
    }

    private final WidgetFeedbackSheetBinding getBinding() {
        return (WidgetFeedbackSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final FeedbackSheetViewModel getViewModel() {
        Serializable serializable = requireArguments().getSerializable(ARG_FEEDBACK_TYPE);
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type com.discord.widgets.feedback.WidgetFeedbackSheet.FeedbackType");
        int ordinal = ((FeedbackType) serializable).ordinal();
        if (ordinal == 0) {
            return getViewModelCallFeedbackSheet();
        }
        if (ordinal == 1) {
            return getViewModelStreamFeedbackSheet();
        }
        if (ordinal == 2) {
            return getViewModelGuildDeleteFeedbackSheet();
        }
        throw new NoWhenBranchMatchedException();
    }

    private final CallFeedbackSheetViewModel getViewModelCallFeedbackSheet() {
        return (CallFeedbackSheetViewModel) this.viewModelCallFeedbackSheet$delegate.getValue();
    }

    private final GuildDeleteFeedbackSheetViewModel getViewModelGuildDeleteFeedbackSheet() {
        return (GuildDeleteFeedbackSheetViewModel) this.viewModelGuildDeleteFeedbackSheet$delegate.getValue();
    }

    private final StreamFeedbackSheetViewModel getViewModelStreamFeedbackSheet() {
        return (StreamFeedbackSheetViewModel) this.viewModelStreamFeedbackSheet$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(FeedbackSheetViewModel.Event event) {
        if (event instanceof FeedbackSheetViewModel.Event.Submitted) {
            if (!((FeedbackSheetViewModel.Event.Submitted) event).getShowConfirmation()) {
                requireDialog().cancel();
                return;
            }
            Observable<Long> d02 = Observable.d0(SHEET_DISMISS_DELAY_MS, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable\n             …S, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), WidgetFeedbackSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFeedbackSheet$handleEvent$1(this));
        } else if (event instanceof FeedbackSheetViewModel.Event.NavigateToIssueDetails) {
            WidgetIssueDetailsForm.Companion companion = WidgetIssueDetailsForm.Companion;
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            FeedbackSheetViewModel.Event.NavigateToIssueDetails navigateToIssueDetails = (FeedbackSheetViewModel.Event.NavigateToIssueDetails) event;
            companion.launch(requireContext, navigateToIssueDetails.getPendingFeedback(), navigateToIssueDetails.getShowCxLinkForIssueDetails());
            requireDialog().cancel();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateView(FeedbackSheetViewModel.ViewState viewState) {
        boolean z2 = true;
        if (this.isShowingFeedbackIssues || !(!viewState.getFeedbackIssues().isEmpty())) {
            z2 = false;
        }
        if (z2) {
            if (viewState.getShowFaceRatings()) {
                NestedScrollView nestedScrollView = getBinding().c;
                m.checkNotNullExpressionValue(nestedScrollView, "binding.feedbackSheetContainer");
                updatePeekHeightPx(nestedScrollView.getMeasuredHeight());
            }
            setBottomSheetState(4);
        }
        getBinding().e.setText(viewState.getTitleTextResId());
        FeedbackView feedbackView = getBinding().d;
        Integer promptTextResId = viewState.getPromptTextResId();
        String string = promptTextResId != null ? getString(promptTextResId.intValue()) : null;
        FeedbackRating selectedFeedbackRating = viewState.getSelectedFeedbackRating();
        WidgetFeedbackSheet$updateView$2 widgetFeedbackSheet$updateView$2 = new WidgetFeedbackSheet$updateView$2(this);
        WidgetFeedbackSheet$updateView$3 widgetFeedbackSheet$updateView$3 = new WidgetFeedbackSheet$updateView$3(this);
        WidgetFeedbackSheet$updateView$4 widgetFeedbackSheet$updateView$4 = new WidgetFeedbackSheet$updateView$4(this);
        String string2 = getString(viewState.getIssuesHeaderTextResId());
        m.checkNotNullExpressionValue(string2, "getString(viewState.issuesHeaderTextResId)");
        feedbackView.updateView(string, selectedFeedbackRating, widgetFeedbackSheet$updateView$2, widgetFeedbackSheet$updateView$3, widgetFeedbackSheet$updateView$4, string2, viewState.getFeedbackIssues(), new WidgetFeedbackSheet$updateView$5(this));
        if (z2) {
            Observable<Long> d02 = Observable.d0(100L, TimeUnit.MILLISECONDS);
            m.checkNotNullExpressionValue(d02, "Observable\n          .ti…S, TimeUnit.MILLISECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), WidgetFeedbackSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFeedbackSheet$updateView$6(this));
        }
    }

    @Override // com.discord.app.AppBottomSheet
    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
        super.bindSubscriptions(compositeSubscription);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetFeedbackSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFeedbackSheet$bindSubscriptions$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetFeedbackSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetFeedbackSheet$bindSubscriptions$2(this));
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_feedback_sheet;
    }

    public final Function0<Unit> getOnDismissed() {
        return this.onDismissed;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        super.onCancel(dialogInterface);
        this.onDismissed.invoke();
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        requireDialog().setCanceledOnTouchOutside(true);
        getBinding().f2366b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.feedback.WidgetFeedbackSheet$onResume$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                FeedbackSheetViewModel viewModel;
                viewModel = WidgetFeedbackSheet.this.getViewModel();
                viewModel.submitForm();
            }
        });
    }

    public final void setOnDismissed(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "<set-?>");
        this.onDismissed = function0;
    }
}
