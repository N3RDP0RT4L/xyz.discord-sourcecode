package com.discord.widgets.voice;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import com.discord.api.channel.Channel;
import com.discord.stores.StoreStream;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: VoiceUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceUtils$handleConnectToEventChannel$4 extends o implements Function0<Unit> {
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ FragmentManager $fragmentManager;
    public final /* synthetic */ Function0 $onEventStarted;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceUtils$handleConnectToEventChannel$4(Channel channel, Context context, FragmentManager fragmentManager, Function0 function0) {
        super(0);
        this.$channel = channel;
        this.$context = context;
        this.$fragmentManager = fragmentManager;
        this.$onEventStarted = function0;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream.Companion.getVoiceChannelSelected().selectVoiceChannel(this.$channel.h());
        if (TextInVoiceFeatureFlag.Companion.getINSTANCE().isEnabled(Long.valueOf(this.$channel.f()))) {
            WidgetCallFullscreen.Companion.launch(this.$context, this.$channel.h(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
        } else {
            WidgetVoiceBottomSheet.Companion.show(this.$fragmentManager, this.$channel.h(), true, WidgetVoiceBottomSheet.FeatureContext.HOME);
        }
        this.$onEventStarted.invoke();
    }
}
