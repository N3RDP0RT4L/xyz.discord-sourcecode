package com.discord.widgets.voice;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppComponent;
import com.discord.app.AppPermissionsRequests;
import com.discord.app.AppTransitionActivity;
import com.discord.utilities.stage.StageChannelUtils;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.discord.widgets.user.calls.PrivateCallLauncher;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen;
import com.discord.widgets.voice.fullscreen.WidgetGuildCallOnboardingSheet;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: VoiceUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceUtils$handleCallChannel$1 extends o implements Function0<Unit> {
    public final /* synthetic */ AppComponent $appComponent;
    public final /* synthetic */ AppPermissionsRequests $appPermissionsRequests;
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ FragmentManager $fragmentManager;
    public final /* synthetic */ boolean $hasUserSeenVoiceChannelOnboarding;
    public final /* synthetic */ boolean $isNewUser;
    public final /* synthetic */ Channel $selectedVoiceChannel;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceUtils$handleCallChannel$1(Channel channel, AppPermissionsRequests appPermissionsRequests, AppComponent appComponent, Context context, FragmentManager fragmentManager, boolean z2, boolean z3, Channel channel2) {
        super(0);
        this.$channel = channel;
        this.$appPermissionsRequests = appPermissionsRequests;
        this.$appComponent = appComponent;
        this.$context = context;
        this.$fragmentManager = fragmentManager;
        this.$hasUserSeenVoiceChannelOnboarding = z2;
        this.$isNewUser = z3;
        this.$selectedVoiceChannel = channel2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        if (ChannelUtils.x(this.$channel)) {
            new PrivateCallLauncher(this.$appPermissionsRequests, this.$appComponent, this.$context, this.$fragmentManager).launchVoiceCall(this.$channel.h());
        } else if (ChannelUtils.z(this.$channel)) {
            StageChannelUtils.INSTANCE.connectToStageChannel(this.$channel, this.$context, this.$fragmentManager);
        } else if (!this.$hasUserSeenVoiceChannelOnboarding && this.$isNewUser) {
            WidgetGuildCallOnboardingSheet.Companion.show(this.$fragmentManager, this.$channel.h());
        } else if (TextInVoiceFeatureFlag.Companion.getINSTANCE().isEnabled(Long.valueOf(this.$channel.f()))) {
            Channel channel = this.$selectedVoiceChannel;
            if (channel == null || channel.h() == 0 || ChannelUtils.x(this.$selectedVoiceChannel)) {
                WidgetCallPreviewFullscreen.Companion.launch(this.$context, this.$channel.h(), AppTransitionActivity.Transition.TYPE_SLIDE_VERTICAL_WITH_FADE);
            } else {
                WidgetCallFullscreen.Companion.launch(this.$context, this.$channel.h(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
            }
        } else {
            WidgetVoiceBottomSheet.Companion.show(this.$fragmentManager, this.$channel.h(), true, WidgetVoiceBottomSheet.FeatureContext.HOME);
        }
    }
}
