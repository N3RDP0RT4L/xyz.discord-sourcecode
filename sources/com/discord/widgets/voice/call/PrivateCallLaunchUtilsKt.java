package com.discord.widgets.voice.call;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import b.c.a.a0.d;
import com.discord.app.AppComponent;
import com.discord.app.AppPermissionsRequests;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
/* compiled from: PrivateCallLaunchUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u001aA\u0010\u000e\u001a\u00020\r2\n\u0010\u0002\u001a\u00060\u0000j\u0002`\u00012\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000e\u0010\u000f¨\u0006\u0010"}, d2 = {"", "Lcom/discord/primitives/ChannelId;", "channelId", "", "isVideo", "Lcom/discord/app/AppPermissionsRequests;", "appPermissionsRequests", "Landroid/content/Context;", "context", "Lcom/discord/app/AppComponent;", "appComponent", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "callAndLaunch", "(JZLcom/discord/app/AppPermissionsRequests;Landroid/content/Context;Lcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;)V", "app_productionGoogleRelease"}, k = 2, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PrivateCallLaunchUtilsKt {
    public static final void callAndLaunch(long j, boolean z2, AppPermissionsRequests appPermissionsRequests, Context context, AppComponent appComponent, FragmentManager fragmentManager) {
        m.checkNotNullParameter(appPermissionsRequests, "appPermissionsRequests");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        PrivateCallLaunchUtilsKt$callAndLaunch$1 privateCallLaunchUtilsKt$callAndLaunch$1 = new PrivateCallLaunchUtilsKt$callAndLaunch$1(j, new WeakReference(context), appComponent, context, fragmentManager, z2);
        if (z2) {
            appPermissionsRequests.requestVideoCallPermissions(new PrivateCallLaunchUtilsKt$callAndLaunch$2(privateCallLaunchUtilsKt$callAndLaunch$1));
        } else {
            d.S1(appPermissionsRequests, null, new PrivateCallLaunchUtilsKt$callAndLaunch$3(privateCallLaunchUtilsKt$callAndLaunch$1), 1, null);
        }
    }
}
