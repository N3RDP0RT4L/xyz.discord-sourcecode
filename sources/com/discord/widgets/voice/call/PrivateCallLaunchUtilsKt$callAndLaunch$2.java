package com.discord.widgets.voice.call;

import d0.z.d.k;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: PrivateCallLaunchUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class PrivateCallLaunchUtilsKt$callAndLaunch$2 extends k implements Function0<Unit> {
    public final /* synthetic */ PrivateCallLaunchUtilsKt$callAndLaunch$1 $onPermissionsGranted$1;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PrivateCallLaunchUtilsKt$callAndLaunch$2(PrivateCallLaunchUtilsKt$callAndLaunch$1 privateCallLaunchUtilsKt$callAndLaunch$1) {
        super(0, null, "onPermissionsGranted", "invoke()V", 0);
        this.$onPermissionsGranted$1 = privateCallLaunchUtilsKt$callAndLaunch$1;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.$onPermissionsGranted$1.invoke2();
    }
}
