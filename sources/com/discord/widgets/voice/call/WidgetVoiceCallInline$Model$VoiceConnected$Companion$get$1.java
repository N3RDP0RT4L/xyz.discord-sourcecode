package com.discord.widgets.voice.call;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreStream;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.streams.StreamContextService;
import com.discord.widgets.voice.call.WidgetVoiceCallInline;
import d0.z.d.k;
import d0.z.d.m;
import j0.k.b;
import kotlin.Metadata;
import kotlin.jvm.functions.Function3;
import rx.Observable;
import rx.functions.Func3;
/* compiled from: WidgetVoiceCallInline.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003 \u0004*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "Lcom/discord/widgets/voice/call/WidgetVoiceCallInline$Model$VoiceConnected;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceCallInline$Model$VoiceConnected$Companion$get$1<T, R> implements b<Channel, Observable<? extends WidgetVoiceCallInline.Model.VoiceConnected>> {
    public static final WidgetVoiceCallInline$Model$VoiceConnected$Companion$get$1 INSTANCE = new WidgetVoiceCallInline$Model$VoiceConnected$Companion$get$1();

    /* compiled from: WidgetVoiceCallInline.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\t\u001a\u00020\u00062\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0007\u0010\b"}, d2 = {"Lcom/discord/api/channel/Channel;", "p1", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "p2", "Lcom/discord/utilities/streams/StreamContext;", "p3", "Lcom/discord/widgets/voice/call/WidgetVoiceCallInline$Model$VoiceConnected;", "invoke", "(Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/utilities/streams/StreamContext;)Lcom/discord/widgets/voice/call/WidgetVoiceCallInline$Model$VoiceConnected;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.WidgetVoiceCallInline$Model$VoiceConnected$Companion$get$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends k implements Function3<Channel, MediaEngineConnection.InputMode, StreamContext, WidgetVoiceCallInline.Model.VoiceConnected> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(3, WidgetVoiceCallInline.Model.VoiceConnected.class, HookHelper.constructorName, "<init>(Lcom/discord/api/channel/Channel;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/utilities/streams/StreamContext;)V", 0);
        }

        public final WidgetVoiceCallInline.Model.VoiceConnected invoke(Channel channel, MediaEngineConnection.InputMode inputMode, StreamContext streamContext) {
            m.checkNotNullParameter(channel, "p1");
            m.checkNotNullParameter(inputMode, "p2");
            return new WidgetVoiceCallInline.Model.VoiceConnected(channel, inputMode, streamContext);
        }
    }

    public final Observable<? extends WidgetVoiceCallInline.Model.VoiceConnected> call(Channel channel) {
        if (channel == null) {
            return new j0.l.e.k(null);
        }
        j0.l.e.k kVar = new j0.l.e.k(channel);
        Observable<MediaEngineConnection.InputMode> inputMode = StoreStream.Companion.getMediaSettings().getInputMode();
        Observable<StreamContext> forActiveStream = new StreamContextService(null, null, null, null, null, null, null, null, 255, null).getForActiveStream();
        final AnonymousClass1 r2 = AnonymousClass1.INSTANCE;
        Object obj = r2;
        if (r2 != null) {
            obj = new Func3() { // from class: com.discord.widgets.voice.call.WidgetVoiceCallInline$sam$rx_functions_Func3$0
                @Override // rx.functions.Func3
                public final /* synthetic */ Object call(Object obj2, Object obj3, Object obj4) {
                    return Function3.this.invoke(obj2, obj3, obj4);
                }
            };
        }
        return Observable.i(kVar, inputMode, forActiveStream, (Func3) obj);
    }
}
