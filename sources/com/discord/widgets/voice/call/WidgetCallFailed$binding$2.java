package com.discord.widgets.voice.call;

import android.view.View;
import com.discord.databinding.ViewDialogConfirmationBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetCallFailed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/ViewDialogConfirmationBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/ViewDialogConfirmationBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetCallFailed$binding$2 extends k implements Function1<View, ViewDialogConfirmationBinding> {
    public static final WidgetCallFailed$binding$2 INSTANCE = new WidgetCallFailed$binding$2();

    public WidgetCallFailed$binding$2() {
        super(1, ViewDialogConfirmationBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/ViewDialogConfirmationBinding;", 0);
    }

    public final ViewDialogConfirmationBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        return ViewDialogConfirmationBinding.a(view);
    }
}
