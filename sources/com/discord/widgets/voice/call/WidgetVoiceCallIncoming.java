package com.discord.widgets.voice.call;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.MainThread;
import androidx.core.view.ViewCompat;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.DiscordConnectService;
import com.discord.databinding.WidgetVoiceCallIncomingBinding;
import com.discord.models.user.User;
import com.discord.stores.StoreCalls;
import com.discord.stores.StoreMediaEngine;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.media.AppSound;
import com.discord.utilities.media.AppSoundManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.user.calls.PrivateCallLauncher;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.model.CallModel;
import d0.z.d.m;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetVoiceCallIncoming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\b\u0016\u0018\u00002\u00020\u0001:\u0002\u001f B\u0007¢\u0006\u0004\b\u001e\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\t\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\t\u0010\bJ\u0019\u0010\f\u001a\u00020\u00042\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0014¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000e\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u000e\u0010\bJ\u001f\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u000fH\u0014¢\u0006\u0004\b\u0011\u0010\u0012J\u001b\u0010\u0016\u001a\u00020\u00042\n\u0010\u0015\u001a\u00060\u0013j\u0002`\u0014H\u0015¢\u0006\u0004\b\u0016\u0010\u0017R\u001d\u0010\u001d\u001a\u00020\u00188B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c¨\u0006!"}, d2 = {"Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming;", "Lcom/discord/app/AppFragment;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "onStop", "Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;", "model", "configureUI", "(Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;)V", "onEmptyCallModel", "", "useVideo", "onConnect", "(Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;Z)V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "onDecline", "(J)V", "Lcom/discord/databinding/WidgetVoiceCallIncomingBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetVoiceCallIncomingBinding;", "binding", HookHelper.constructorName, ExifInterface.TAG_MODEL, "SystemCallIncoming", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class WidgetVoiceCallIncoming extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetVoiceCallIncoming.class, "binding", "getBinding()Lcom/discord/databinding/WidgetVoiceCallIncomingBinding;", 0)};
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetVoiceCallIncoming$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetVoiceCallIncoming.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0084\b\u0018\u0000 &2\u00020\u0001:\u0001&B%\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u000e\u001a\u00020\t¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ4\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u000e\u001a\u00020\tHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u0014\u0010\u000bJ\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0017\u0010\u001f\u001a\u00060\u001bj\u0002`\u001c8F@\u0006¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u001eR\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010 \u001a\u0004\b!\u0010\bR\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\"\u001a\u0004\b#\u0010\u000b¨\u0006'"}, d2 = {"Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;", "", "Lcom/discord/widgets/voice/model/CallModel;", "component1", "()Lcom/discord/widgets/voice/model/CallModel;", "", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component2", "()Ljava/util/List;", "", "component3", "()I", "callModel", "privateCallUserListItems", "numIncomingCalls", "copy", "(Lcom/discord/widgets/voice/model/CallModel;Ljava/util/List;I)Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/voice/model/CallModel;", "getCallModel", "", "Lcom/discord/primitives/ChannelId;", "getChannelId", "()J", "channelId", "Ljava/util/List;", "getPrivateCallUserListItems", "I", "getNumIncomingCalls", HookHelper.constructorName, "(Lcom/discord/widgets/voice/model/CallModel;Ljava/util/List;I)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final CallModel callModel;
        private final int numIncomingCalls;
        private final List<StoreVoiceParticipants.VoiceUser> privateCallUserListItems;

        /* compiled from: WidgetVoiceCallIncoming.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0015\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model$Companion;", "", "Lrx/Observable;", "Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;", "get", "()Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get() {
                Observable<R> Y = StoreStream.Companion.getCallsIncoming().observeIncoming().Y(WidgetVoiceCallIncoming$Model$Companion$get$1.INSTANCE);
                m.checkNotNullExpressionValue(Y, "StoreStream\n            …          }\n            }");
                Observable<Model> q = ObservableExtensionsKt.computationLatest(Y).q();
                m.checkNotNullExpressionValue(q, "StoreStream\n            …  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Model(CallModel callModel, List<StoreVoiceParticipants.VoiceUser> list, int i) {
            m.checkNotNullParameter(callModel, "callModel");
            m.checkNotNullParameter(list, "privateCallUserListItems");
            this.callModel = callModel;
            this.privateCallUserListItems = list;
            this.numIncomingCalls = i;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Model copy$default(Model model, CallModel callModel, List list, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                callModel = model.callModel;
            }
            if ((i2 & 2) != 0) {
                list = model.privateCallUserListItems;
            }
            if ((i2 & 4) != 0) {
                i = model.numIncomingCalls;
            }
            return model.copy(callModel, list, i);
        }

        public final CallModel component1() {
            return this.callModel;
        }

        public final List<StoreVoiceParticipants.VoiceUser> component2() {
            return this.privateCallUserListItems;
        }

        public final int component3() {
            return this.numIncomingCalls;
        }

        public final Model copy(CallModel callModel, List<StoreVoiceParticipants.VoiceUser> list, int i) {
            m.checkNotNullParameter(callModel, "callModel");
            m.checkNotNullParameter(list, "privateCallUserListItems");
            return new Model(callModel, list, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.callModel, model.callModel) && m.areEqual(this.privateCallUserListItems, model.privateCallUserListItems) && this.numIncomingCalls == model.numIncomingCalls;
        }

        public final CallModel getCallModel() {
            return this.callModel;
        }

        public final long getChannelId() {
            return this.callModel.getChannel().h();
        }

        public final int getNumIncomingCalls() {
            return this.numIncomingCalls;
        }

        public final List<StoreVoiceParticipants.VoiceUser> getPrivateCallUserListItems() {
            return this.privateCallUserListItems;
        }

        public int hashCode() {
            CallModel callModel = this.callModel;
            int i = 0;
            int hashCode = (callModel != null ? callModel.hashCode() : 0) * 31;
            List<StoreVoiceParticipants.VoiceUser> list = this.privateCallUserListItems;
            if (list != null) {
                i = list.hashCode();
            }
            return ((hashCode + i) * 31) + this.numIncomingCalls;
        }

        public String toString() {
            StringBuilder R = a.R("Model(callModel=");
            R.append(this.callModel);
            R.append(", privateCallUserListItems=");
            R.append(this.privateCallUserListItems);
            R.append(", numIncomingCalls=");
            return a.A(R, this.numIncomingCalls, ")");
        }
    }

    /* compiled from: WidgetVoiceCallIncoming.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u001b\u0010\u000eJ!\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0019\u0010\u000b\u001a\u00020\u00062\b\u0010\n\u001a\u0004\u0018\u00010\tH\u0014¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0006H\u0014¢\u0006\u0004\b\r\u0010\u000eJ\u001b\u0010\u0012\u001a\u00020\u00062\n\u0010\u0011\u001a\u00060\u000fj\u0002`\u0010H\u0014¢\u0006\u0004\b\u0012\u0010\u0013J\u001f\u0010\u0016\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u0014H\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0019\u001a\u00020\u00188\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0019\u0010\u001a¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$SystemCallIncoming;", "Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming;", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;", "model", "configureUI", "(Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;)V", "onEmptyCallModel", "()V", "", "Lcom/discord/primitives/ChannelId;", "channelId", "onDecline", "(J)V", "", "useVideo", "onConnect", "(Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;Z)V", "Ljava/util/concurrent/atomic/AtomicLong;", "cachedChannelId", "Ljava/util/concurrent/atomic/AtomicLong;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class SystemCallIncoming extends WidgetVoiceCallIncoming {
        private AtomicLong cachedChannelId = new AtomicLong(0);

        @Override // com.discord.widgets.voice.call.WidgetVoiceCallIncoming
        public void configureUI(Model model) {
            if ((model != null ? Long.valueOf(model.getChannelId()) : null) != null) {
                this.cachedChannelId.set(model.getChannelId());
            }
            WidgetVoiceCallIncoming.super.configureUI(model);
        }

        @Override // com.discord.widgets.voice.call.WidgetVoiceCallIncoming
        public void onConnect(Model model, boolean z2) {
            m.checkNotNullParameter(model, "model");
            NotificationClient.clear$default(NotificationClient.INSTANCE, model.getChannelId(), requireContext(), false, 4, null);
            DiscordConnectService.j.b(requireContext(), model.getChannelId());
            WidgetCallFullscreen.Companion.launch(requireContext(), model.getChannelId(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
            }
        }

        @Override // com.discord.widgets.voice.call.WidgetVoiceCallIncoming
        public void onDecline(long j) {
            WidgetVoiceCallIncoming.super.onDecline(j);
            AppActivity appActivity = getAppActivity();
            if (appActivity != null) {
                appActivity.finish();
            }
        }

        @Override // com.discord.widgets.voice.call.WidgetVoiceCallIncoming
        public void onEmptyCallModel() {
            long j = this.cachedChannelId.get();
            if (j != 0) {
                AppActivity appActivity = getAppActivity();
                if (appActivity != null) {
                    appActivity.finish();
                }
                NotificationClient.clear$default(NotificationClient.INSTANCE, j, requireContext(), false, 4, null);
            }
        }

        @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
        public void onViewCreated(View view, Bundle bundle) {
            m.checkNotNullParameter(view, "view");
            super.onViewCreated(view, bundle);
            Observable<Long> d02 = Observable.d0(15L, TimeUnit.SECONDS);
            m.checkNotNullExpressionValue(d02, "Observable.timer(15, TimeUnit.SECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(d02, this, null, 2, null), SystemCallIncoming.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetVoiceCallIncoming$SystemCallIncoming$onViewCreated$1(this));
        }
    }

    public WidgetVoiceCallIncoming() {
        super(R.layout.widget_voice_call_incoming);
    }

    private final WidgetVoiceCallIncomingBinding getBinding() {
        return (WidgetVoiceCallIncomingBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public void configureUI(final Model model) {
        User user;
        String str = null;
        if ((model != null ? model.getCallModel() : null) == null) {
            onEmptyCallModel();
            return;
        }
        CallModel component1 = model.component1();
        List<StoreVoiceParticipants.VoiceUser> component2 = model.component2();
        int i = 0;
        AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.voice.call.WidgetVoiceCallIncoming$configureUI$1
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                WidgetVoiceCallIncoming.this.onDecline(model.getChannelId());
                return Boolean.TRUE;
            }
        }, 0, 2, null);
        AppSoundManager.Provider.INSTANCE.get().play(AppSound.Companion.getSOUND_CALL_RINGING());
        TextView textView = getBinding().g;
        m.checkNotNullExpressionValue(textView, "binding.incomingCallStatusPrimary");
        if (ChannelUtils.w(component1.getChannel())) {
            str = ChannelUtils.c(component1.getChannel());
        } else {
            StoreVoiceParticipants.VoiceUser dmRecipient = component1.getDmRecipient();
            if (!(dmRecipient == null || (user = dmRecipient.getUser()) == null)) {
                str = user.getUsername();
            }
        }
        textView.setText(str);
        getBinding().h.setText(component1.isVideoCall() ? R.string.incoming_video_call : R.string.incoming_call);
        getBinding().i.configure(component2);
        getBinding().e.configure(component2);
        LinearLayout linearLayout = getBinding().f2673b;
        m.checkNotNullExpressionValue(linearLayout, "binding.incomingCallAcceptAltContainer");
        if (!component1.isVideoCall()) {
            i = 8;
        }
        linearLayout.setVisibility(i);
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.call.WidgetVoiceCallIncoming$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceCallIncoming.this.onConnect(model, true);
            }
        });
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.call.WidgetVoiceCallIncoming$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceCallIncoming.this.onConnect(model, false);
            }
        });
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.call.WidgetVoiceCallIncoming$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceCallIncoming.this.onDecline(model.getChannelId());
            }
        });
    }

    public void onConnect(Model model, boolean z2) {
        m.checkNotNullParameter(model, "model");
        long channelId = model.getChannelId();
        if (model.getNumIncomingCalls() == 1) {
            Context requireContext = requireContext();
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            PrivateCallLauncher privateCallLauncher = new PrivateCallLauncher(this, this, requireContext, parentFragmentManager);
            if (z2) {
                privateCallLauncher.launchVideoCall(channelId);
            } else {
                privateCallLauncher.launchVoiceCall(channelId);
            }
        } else {
            if (z2) {
                StoreMediaEngine.selectDefaultVideoDevice$default(StoreStream.Companion.getMediaEngine(), null, 1, null);
            }
            StoreStream.Companion.getVoiceChannelSelected().selectVoiceChannel(channelId);
        }
        NotificationClient.clear$default(NotificationClient.INSTANCE, channelId, requireContext(), false, 4, null);
    }

    @MainThread
    public void onDecline(long j) {
        StoreCalls.stopRinging$default(StoreStream.Companion.getCalls(), j, null, 2, null);
        NotificationClient.clear$default(NotificationClient.INSTANCE, j, requireContext(), false, 4, null);
    }

    public void onEmptyCallModel() {
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.finish();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        AppSoundManager.Provider.INSTANCE.get().stop(AppSound.Companion.getSOUND_CALL_RINGING());
        super.onStop();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        FragmentActivity requireActivity = requireActivity();
        m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        Window window = requireActivity.getWindow();
        m.checkNotNullExpressionValue(window, "requireActivity().window");
        m.checkNotNullParameter(window, "window");
        if (Build.VERSION.SDK_INT >= 28) {
            window.getAttributes().layoutInDisplayCutoutMode = 1;
        }
        int color = ColorCompat.getColor(this, (int) R.color.transparent);
        ColorCompat.setStatusBarTranslucent(this);
        ColorCompat.setStatusBarColor((Fragment) this, color, true);
        ViewCompat.setOnApplyWindowInsetsListener((ViewGroup) view, WidgetVoiceCallIncoming$onViewBound$1.INSTANCE);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(), this, null, 2, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetVoiceCallIncoming$onViewBoundOrOnResume$1(this));
    }
}
