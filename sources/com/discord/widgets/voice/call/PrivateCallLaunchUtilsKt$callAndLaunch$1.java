package com.discord.widgets.voice.call;

import android.content.Context;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentManager;
import com.discord.api.channel.Channel;
import com.discord.app.AppComponent;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreCalls;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.widgets.notice.WidgetNoticeNuxOverlay;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.functions.Func2;
import xyz.discord.R;
/* compiled from: PrivateCallLaunchUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "onPermissionsGranted"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PrivateCallLaunchUtilsKt$callAndLaunch$1 extends o implements Function0<Unit> {
    public final /* synthetic */ AppComponent $appComponent;
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ FragmentManager $fragmentManager;
    public final /* synthetic */ boolean $isVideo;
    public final /* synthetic */ WeakReference $weakContext;

    /* compiled from: PrivateCallLaunchUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0006\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$StateChange;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/rtcconnection/RtcConnection$StateChange;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.PrivateCallLaunchUtilsKt$callAndLaunch$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1<T, R> implements b<RtcConnection.StateChange, Boolean> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public final Boolean call(RtcConnection.StateChange stateChange) {
            return Boolean.valueOf(stateChange.a == RtcConnection.State.f.a);
        }
    }

    /* compiled from: PrivateCallLaunchUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "it", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Ljava/lang/Long;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.PrivateCallLaunchUtilsKt$callAndLaunch$1$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2<T, R> implements b<Channel, Long> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public final Long call(Channel channel) {
            return Long.valueOf(channel != null ? channel.h() : -1L);
        }
    }

    /* compiled from: PrivateCallLaunchUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\u0010\u0007\u001a\n \u0001*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0004\u001a\n \u0001*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$StateChange;", "kotlin.jvm.PlatformType", "<anonymous parameter 0>", "", "voiceChannelId", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/rtcconnection/RtcConnection$StateChange;Ljava/lang/Long;)Ljava/lang/Long;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.PrivateCallLaunchUtilsKt$callAndLaunch$1$3  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass3<T1, T2, R> implements Func2<RtcConnection.StateChange, Long, Long> {
        public static final AnonymousClass3 INSTANCE = new AnonymousClass3();

        public final Long call(RtcConnection.StateChange stateChange, Long l) {
            return l;
        }
    }

    /* compiled from: PrivateCallLaunchUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "it", "", "invoke", "(Ljava/lang/Long;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.PrivateCallLaunchUtilsKt$callAndLaunch$1$4  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass4 extends o implements Function1<Long, Boolean> {
        public AnonymousClass4() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Boolean invoke(Long l) {
            return Boolean.valueOf(invoke2(l));
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final boolean invoke2(Long l) {
            return l != null && l.longValue() == PrivateCallLaunchUtilsKt$callAndLaunch$1.this.$channelId;
        }
    }

    /* compiled from: PrivateCallLaunchUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "voiceChannelId", "", "invoke", "(Ljava/lang/Long;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.PrivateCallLaunchUtilsKt$callAndLaunch$1$5  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass5 extends o implements Function1<Long, Unit> {
        public AnonymousClass5() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Long l) {
            invoke2(l);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Long l) {
            Context context;
            if (l.longValue() > 0 && (context = (Context) PrivateCallLaunchUtilsKt$callAndLaunch$1.this.$weakContext.get()) != null) {
                WidgetCallFullscreen.Companion companion = WidgetCallFullscreen.Companion;
                m.checkNotNullExpressionValue(context, "it");
                m.checkNotNullExpressionValue(l, "voiceChannelId");
                companion.launch(context, l.longValue(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
                if (!StoreStream.Companion.getUserSettings().getIsMobileOverlayEnabled()) {
                    WidgetNoticeNuxOverlay.Companion.enqueue();
                }
            }
        }
    }

    /* compiled from: PrivateCallLaunchUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "doCall"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.PrivateCallLaunchUtilsKt$callAndLaunch$1$6  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass6 extends o implements Function0<Unit> {

        /* compiled from: PrivateCallLaunchUtils.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.voice.call.PrivateCallLaunchUtilsKt$callAndLaunch$1$6$1  reason: invalid class name */
        /* loaded from: classes2.dex */
        public static final class AnonymousClass1 extends o implements Function0<Unit> {
            public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

            public AnonymousClass1() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2() {
                StoreStream.Companion.getMediaEngine().selectVideoInputDevice(null);
            }
        }

        public AnonymousClass6() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            StoreCalls calls = StoreStream.Companion.getCalls();
            PrivateCallLaunchUtilsKt$callAndLaunch$1 privateCallLaunchUtilsKt$callAndLaunch$1 = PrivateCallLaunchUtilsKt$callAndLaunch$1.this;
            calls.call(privateCallLaunchUtilsKt$callAndLaunch$1.$appComponent, privateCallLaunchUtilsKt$callAndLaunch$1.$context, privateCallLaunchUtilsKt$callAndLaunch$1.$fragmentManager, privateCallLaunchUtilsKt$callAndLaunch$1.$channelId, AnonymousClass1.INSTANCE);
        }
    }

    /* compiled from: PrivateCallLaunchUtils.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "deviceGUID", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.PrivateCallLaunchUtilsKt$callAndLaunch$1$7  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass7 extends o implements Function1<String, Unit> {
        public final /* synthetic */ AnonymousClass6 $doCall$6;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass7(AnonymousClass6 r2) {
            super(1);
            this.$doCall$6 = r2;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            if (str != null) {
                this.$doCall$6.invoke2();
            } else {
                b.a.d.m.g(PrivateCallLaunchUtilsKt$callAndLaunch$1.this.$context, R.string.no_video_devices, 0, null, 12);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PrivateCallLaunchUtilsKt$callAndLaunch$1(long j, WeakReference weakReference, AppComponent appComponent, Context context, FragmentManager fragmentManager, boolean z2) {
        super(0);
        this.$channelId = j;
        this.$weakContext = weakReference;
        this.$appComponent = appComponent;
        this.$context = context;
        this.$fragmentManager = fragmentManager;
        this.$isVideo = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        StoreStream.Companion companion = StoreStream.Companion;
        Observable Z = Observable.j(companion.getRtcConnection().getConnectionState().x(AnonymousClass1.INSTANCE), companion.getVoiceChannelSelected().observeSelectedChannel().F(AnonymousClass2.INSTANCE), AnonymousClass3.INSTANCE).k(b.a.d.o.c(new AnonymousClass4(), -1L, 250L, TimeUnit.MILLISECONDS)).Z(1);
        m.checkNotNullExpressionValue(Z, "Observable\n        .comb…       )\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(Z), WidgetCallFullscreen.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass5());
        AnonymousClass6 r1 = new AnonymousClass6();
        if (this.$isVideo) {
            companion.getMediaEngine().selectDefaultVideoDevice(new AnonymousClass7(r1));
        } else {
            r1.invoke2();
        }
    }
}
