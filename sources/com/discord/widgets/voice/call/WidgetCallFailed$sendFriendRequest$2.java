package com.discord.widgets.voice.call;

import android.content.Context;
import androidx.core.app.NotificationCompat;
import com.discord.app.AppFragment;
import com.discord.utilities.captcha.CaptchaErrorBody;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPIAbortMessages;
import com.discord.widgets.captcha.WidgetCaptchaBottomSheet;
import com.discord.widgets.captcha.WidgetCaptchaKt;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import rx.functions.Action1;
/* compiled from: WidgetCallFailed.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/utilities/error/Error;", "kotlin.jvm.PlatformType", "error", "", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/utilities/error/Error;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallFailed$sendFriendRequest$2<T> implements Action1<Error> {
    public final /* synthetic */ long $userId;
    public final /* synthetic */ String $username;
    public final /* synthetic */ WidgetCallFailed this$0;

    /* compiled from: WidgetCallFailed.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.call.WidgetCallFailed$sendFriendRequest$2$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public final /* synthetic */ Error $error;

        /* compiled from: WidgetCallFailed.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/app/AppFragment;", "<anonymous parameter 0>", "", "captchaToken", "", "invoke", "(Lcom/discord/app/AppFragment;Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
        /* renamed from: com.discord.widgets.voice.call.WidgetCallFailed$sendFriendRequest$2$1$1  reason: invalid class name and collision with other inner class name */
        /* loaded from: classes2.dex */
        public static final class C02641 extends o implements Function2<AppFragment, String, Unit> {
            public C02641() {
                super(2);
            }

            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(AppFragment appFragment, String str) {
                invoke2(appFragment, str);
                return Unit.a;
            }

            /* renamed from: invoke  reason: avoid collision after fix types in other method */
            public final void invoke2(AppFragment appFragment, String str) {
                m.checkNotNullParameter(appFragment, "<anonymous parameter 0>");
                m.checkNotNullParameter(str, "captchaToken");
                WidgetCallFailed$sendFriendRequest$2 widgetCallFailed$sendFriendRequest$2 = WidgetCallFailed$sendFriendRequest$2.this;
                widgetCallFailed$sendFriendRequest$2.this$0.sendFriendRequest(widgetCallFailed$sendFriendRequest$2.$userId, widgetCallFailed$sendFriendRequest$2.$username, str);
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(Error error) {
            super(0);
            this.$error = error;
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            Error error = this.$error;
            m.checkNotNullExpressionValue(error, "error");
            if (WidgetCaptchaKt.isCaptchaError(error)) {
                WidgetCaptchaBottomSheet.Companion companion = WidgetCaptchaBottomSheet.Companion;
                C02641 r4 = new C02641();
                CaptchaErrorBody.Companion companion2 = CaptchaErrorBody.Companion;
                Error error2 = this.$error;
                m.checkNotNullExpressionValue(error2, "error");
                WidgetCaptchaBottomSheet.Companion.enqueue$default(companion, "Add Friend Captcha", r4, null, companion2.createFromError(error2), 4, null);
                return;
            }
            RestAPIAbortMessages.ResponseResolver responseResolver = RestAPIAbortMessages.ResponseResolver.INSTANCE;
            Context context = WidgetCallFailed$sendFriendRequest$2.this.this$0.getContext();
            Error error3 = this.$error;
            m.checkNotNullExpressionValue(error3, "error");
            Error.Response response = error3.getResponse();
            m.checkNotNullExpressionValue(response, "error.response");
            b.a.d.m.h(WidgetCallFailed$sendFriendRequest$2.this.this$0.getContext(), responseResolver.getRelationshipResponse(context, response.getCode(), WidgetCallFailed$sendFriendRequest$2.this.$username), 0, null, 12);
        }
    }

    public WidgetCallFailed$sendFriendRequest$2(WidgetCallFailed widgetCallFailed, long j, String str) {
        this.this$0 = widgetCallFailed;
        this.$userId = j;
        this.$username = str;
    }

    public final void call(Error error) {
        RestAPIAbortMessages restAPIAbortMessages = RestAPIAbortMessages.INSTANCE;
        m.checkNotNullExpressionValue(error, "error");
        RestAPIAbortMessages.handleAbortCodeOrDefault$default(restAPIAbortMessages, error, new AnonymousClass1(error), null, 4, null);
    }
}
