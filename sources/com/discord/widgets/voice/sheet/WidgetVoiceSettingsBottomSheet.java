package com.discord.widgets.voice.sheet;

import andhook.lib.HookHelper;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildFeature;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetVoiceSettingsBottomSheetBinding;
import com.discord.models.guild.Guild;
import com.discord.utilities.channel.ChannelInviteLaunchUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.channels.settings.WidgetChannelNotificationSettings;
import com.discord.widgets.guildscheduledevent.WidgetGuildScheduledEventListBottomSheet;
import com.discord.widgets.mobile_reports.WidgetMobileReports;
import com.discord.widgets.settings.WidgetSettingsVoice;
import com.discord.widgets.stage.sheet.WidgetStageStartEventBottomSheet;
import com.discord.widgets.stage.start.StageEventsGuildsFeatureFlag;
import com.google.android.material.switchmaterial.SwitchMaterial;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetVoiceSettingsBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\b\u0018\u0000 $2\u00020\u0001:\u0002$%B\u0007¢\u0006\u0004\b#\u0010\u0011J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\b\u0010\tJ!\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\n2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0017\u001a\u00020\u00128B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001d\u001a\u00020\u00188B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001d\u0010\"\u001a\u00020\u001e8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010\u001a\u001a\u0004\b \u0010!¨\u0006&"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;", "viewState", "", "configureUI", "(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;)V", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onResume", "()V", "Lcom/discord/databinding/WidgetVoiceSettingsBottomSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetVoiceSettingsBottomSheetBinding;", "binding", "Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;", "viewModel", "", "channelId$delegate", "getChannelId", "()J", "channelId", HookHelper.constructorName, "Companion", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceSettingsBottomSheet extends AppBottomSheet {
    private static final String ANALYTICS_SOURCE = "Voice Call";
    private static final String ARG_CHANNEL_ID = "ARG_CHANNEL_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetVoiceSettingsBottomSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy channelId$delegate = g.lazy(new WidgetVoiceSettingsBottomSheet$channelId$2(this));
    private final Lazy viewModel$delegate;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetVoiceSettingsBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetVoiceSettingsBottomSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetVoiceSettingsBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ'\u0010\b\u001a\u00020\u00072\u0010\b\u0002\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "show", "(Ljava/lang/Long;Landroidx/fragment/app/FragmentManager;)V", "", "ANALYTICS_SOURCE", "Ljava/lang/String;", WidgetVoiceSettingsBottomSheet.ARG_CHANNEL_ID, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, Long l, FragmentManager fragmentManager, int i, Object obj) {
            if ((i & 1) != 0) {
                l = null;
            }
            companion.show(l, fragmentManager);
        }

        public final void show(Long l, FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetVoiceSettingsBottomSheet widgetVoiceSettingsBottomSheet = new WidgetVoiceSettingsBottomSheet();
            Bundle bundle = new Bundle();
            if (l != null) {
                bundle.putLong(WidgetVoiceSettingsBottomSheet.ARG_CHANNEL_ID, l.longValue());
            }
            widgetVoiceSettingsBottomSheet.setArguments(bundle);
            widgetVoiceSettingsBottomSheet.show(fragmentManager, WidgetVoiceSettingsBottomSheet.class.getName());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetVoiceSettingsBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\bf\u0018\u00002\u00020\u0001R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0004R\u0016\u0010\u000b\u001a\u00020\b8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0016\u0010\r\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u0004R\u0016\u0010\u000f\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0004R\u0018\u0010\u0013\u001a\u0004\u0018\u00010\u00108&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0015\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0004R\u0016\u0010\u0017\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0004¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;", "", "", "getNoiseCancellationEnabled", "()Z", "noiseCancellationEnabled", "getShowVoiceParticipantsToggle", "showVoiceParticipantsToggle", "Lcom/discord/api/channel/Channel;", "getChannel", "()Lcom/discord/api/channel/Channel;", "channel", "getShowStageSettings", "showStageSettings", "getShowInviteItem", "showInviteItem", "Lcom/discord/models/guild/Guild;", "getGuild", "()Lcom/discord/models/guild/Guild;", "guild", "getShowVoiceParticipants", "showVoiceParticipants", "getShowReportItem", "showReportItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface ViewState {
        Channel getChannel();

        Guild getGuild();

        boolean getNoiseCancellationEnabled();

        boolean getShowInviteItem();

        boolean getShowReportItem();

        boolean getShowStageSettings();

        boolean getShowVoiceParticipants();

        boolean getShowVoiceParticipantsToggle();
    }

    public WidgetVoiceSettingsBottomSheet() {
        super(false, 1, null);
        WidgetVoiceSettingsBottomSheet$viewModel$2 widgetVoiceSettingsBottomSheet$viewModel$2 = new WidgetVoiceSettingsBottomSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetVoiceSettingsBottomSheetViewModel.class), new WidgetVoiceSettingsBottomSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetVoiceSettingsBottomSheet$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final ViewState viewState) {
        Guild guild;
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceSettingsBottomSheet.this.dismiss();
                ChannelInviteLaunchUtils.INSTANCE.inviteToChannel(WidgetVoiceSettingsBottomSheet.this, viewState.getChannel(), "Voice Call", (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? null : null);
            }
        });
        TextView textView = getBinding().c;
        m.checkNotNullExpressionValue(textView, "binding.voiceSettingsInvite");
        int i = 8;
        textView.setVisibility(viewState.getShowInviteItem() ? 0 : 8);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceSettingsBottomSheetViewModel viewModel;
                viewModel = WidgetVoiceSettingsBottomSheet.this.getViewModel();
                viewModel.onToggleVoiceParticipantsHidden();
            }
        });
        SwitchMaterial switchMaterial = getBinding().h;
        m.checkNotNullExpressionValue(switchMaterial, "binding.voiceSettingsToggleVideoParticipants");
        switchMaterial.setChecked(viewState.getShowVoiceParticipants());
        SwitchMaterial switchMaterial2 = getBinding().h;
        m.checkNotNullExpressionValue(switchMaterial2, "binding.voiceSettingsToggleVideoParticipants");
        switchMaterial2.setVisibility(viewState.getShowVoiceParticipantsToggle() ? 0 : 8);
        TextView textView2 = getBinding().g;
        m.checkNotNullExpressionValue(textView2, "binding.voiceSettingsStageSettings");
        textView2.setVisibility(viewState.getShowStageSettings() ? 0 : 8);
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                long channelId;
                WidgetVoiceSettingsBottomSheet.this.dismiss();
                WidgetStageStartEventBottomSheet.Companion companion = WidgetStageStartEventBottomSheet.Companion;
                FragmentManager parentFragmentManager = WidgetVoiceSettingsBottomSheet.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                channelId = WidgetVoiceSettingsBottomSheet.this.getChannelId();
                WidgetStageStartEventBottomSheet.Companion.show$default(companion, parentFragmentManager, channelId, null, 4, null);
            }
        });
        TextView textView3 = getBinding().f2678b;
        m.checkNotNullExpressionValue(textView3, "binding.voiceSettingsEvents");
        boolean z2 = true;
        textView3.setVisibility(StageEventsGuildsFeatureFlag.Companion.getINSTANCE().canGuildAccessStageEvents(viewState.getChannel().f()) && ChannelUtils.t(viewState.getChannel()) ? 0 : 8);
        getBinding().f2678b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceSettingsBottomSheet.this.dismiss();
                WidgetGuildScheduledEventListBottomSheet.Companion companion = WidgetGuildScheduledEventListBottomSheet.Companion;
                FragmentManager parentFragmentManager = WidgetVoiceSettingsBottomSheet.this.getParentFragmentManager();
                m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                companion.show(parentFragmentManager, viewState.getChannel().f(), Long.valueOf(viewState.getChannel().h()));
            }
        });
        getBinding().i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceSettingsBottomSheetBinding binding;
                WidgetVoiceSettingsBottomSheet.this.dismiss();
                WidgetSettingsVoice.Companion companion = WidgetSettingsVoice.Companion;
                binding = WidgetVoiceSettingsBottomSheet.this.getBinding();
                TextView textView4 = binding.i;
                m.checkNotNullExpressionValue(textView4, "binding.voiceSettingsVoiceSettings");
                Context context = textView4.getContext();
                m.checkNotNullExpressionValue(context, "binding.voiceSettingsVoiceSettings.context");
                WidgetSettingsVoice.Companion.launch$default(companion, context, null, false, 6, null);
            }
        });
        TextView textView4 = getBinding().e;
        if (!ChannelUtils.z(viewState.getChannel()) || (guild = viewState.getGuild()) == null || !guild.hasFeature(GuildFeature.COMMUNITY)) {
            z2 = false;
        }
        textView4.setVisibility(z2 ? 0 : 8);
        textView4.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet$configureUI$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                long channelId;
                WidgetChannelNotificationSettings.Companion companion = WidgetChannelNotificationSettings.Companion;
                Context x2 = a.x(view, "it", "it.context");
                channelId = WidgetVoiceSettingsBottomSheet.this.getChannelId();
                WidgetChannelNotificationSettings.Companion.launch$default(companion, x2, channelId, false, 4, null);
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet$configureUI$7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceSettingsBottomSheetViewModel viewModel;
                viewModel = WidgetVoiceSettingsBottomSheet.this.getViewModel();
                viewModel.onToggleNoiseCancellation();
            }
        });
        SwitchMaterial switchMaterial3 = getBinding().d;
        m.checkNotNullExpressionValue(switchMaterial3, "binding.voiceSettingsNoiseSuppression");
        switchMaterial3.setChecked(viewState.getNoiseCancellationEnabled());
        TextView textView5 = getBinding().f;
        m.checkNotNullExpressionValue(textView5, "binding.voiceSettingsReport");
        if (viewState.getShowReportItem()) {
            i = 0;
        }
        textView5.setVisibility(i);
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet$configureUI$8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetMobileReports.Companion companion = WidgetMobileReports.Companion;
                Context requireContext = WidgetVoiceSettingsBottomSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                companion.launchStageChannelReport(requireContext, viewState.getChannel().h());
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetVoiceSettingsBottomSheetBinding getBinding() {
        return (WidgetVoiceSettingsBottomSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getChannelId() {
        return ((Number) this.channelId$delegate.getValue()).longValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetVoiceSettingsBottomSheetViewModel getViewModel() {
        return (WidgetVoiceSettingsBottomSheetViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_voice_settings_bottom_sheet;
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetVoiceSettingsBottomSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetVoiceSettingsBottomSheet$onResume$1(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        setBottomSheetState(3);
    }
}
