package com.discord.widgets.voice.sheet;

import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetVoiceBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "invoke", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceBottomSheet$featureContext$2 extends o implements Function0<WidgetVoiceBottomSheet.FeatureContext> {
    public final /* synthetic */ WidgetVoiceBottomSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetVoiceBottomSheet$featureContext$2(WidgetVoiceBottomSheet widgetVoiceBottomSheet) {
        super(0);
        this.this$0 = widgetVoiceBottomSheet;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final WidgetVoiceBottomSheet.FeatureContext invoke() {
        Serializable serializable = this.this$0.requireArguments().getSerializable("ARG_FEATURE_CONTEXT");
        Objects.requireNonNull(serializable, "null cannot be cast to non-null type com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.FeatureContext");
        return (WidgetVoiceBottomSheet.FeatureContext) serializable;
    }
}
