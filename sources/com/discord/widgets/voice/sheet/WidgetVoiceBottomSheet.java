package com.discord.widgets.voice.sheet;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.a.j;
import b.a.a.m;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.r0;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppBottomSheet;
import com.discord.databinding.WidgetVoiceBottomSheetBinding;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreAudioManagerV2;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.channel.ChannelInviteLaunchUtils;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guildscheduledevent.WidgetEndGuildScheduledEventBottomSheet;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.notice.WidgetNoticeNuxOverlay;
import com.discord.widgets.settings.WidgetSettingsVoice;
import com.discord.widgets.voice.controls.AnchoredVoiceControlsView;
import com.discord.widgets.voice.feedback.call.CallFeedbackSheetNavigator;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.model.CameraState;
import com.discord.widgets.voice.sheet.CallParticipantsAdapter;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheetViewModel;
import com.discord.widgets.voice.stream.StreamNavigator;
import com.google.android.material.button.MaterialButton;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: WidgetVoiceBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 Z2\u00020\u0001:\u0005[\\Z]^B\u0007¢\u0006\u0004\bY\u0010\u001aJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000f\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J!\u0010\u0017\u001a\u00020\u00042\b\u0010\u0016\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001b\u0010\u001aJ\u000f\u0010\u001c\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001c\u0010\u001aJ\u000f\u0010\u001d\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001d\u0010\u001aJ\u000f\u0010\u001e\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001e\u0010\u001aJ\u000f\u0010\u001f\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001f\u0010\u001aJ\u000f\u0010!\u001a\u00020 H\u0016¢\u0006\u0004\b!\u0010\"J!\u0010'\u001a\u00020\u00042\u0006\u0010$\u001a\u00020#2\b\u0010&\u001a\u0004\u0018\u00010%H\u0016¢\u0006\u0004\b'\u0010(J)\u0010-\u001a\u00020\u00042\u0006\u0010)\u001a\u00020 2\u0006\u0010*\u001a\u00020 2\b\u0010,\u001a\u0004\u0018\u00010+H\u0016¢\u0006\u0004\b-\u0010.J\u000f\u0010/\u001a\u00020\u0004H\u0016¢\u0006\u0004\b/\u0010\u001aR\u001d\u00105\u001a\u0002008B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b1\u00102\u001a\u0004\b3\u00104R\u0016\u00107\u001a\u0002068\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b7\u00108R\u001d\u0010>\u001a\u0002098B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b:\u0010;\u001a\u0004\b<\u0010=R\u0018\u0010@\u001a\u0004\u0018\u00010?8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b@\u0010AR.\u0010C\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00040B8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bC\u0010D\u001a\u0004\bE\u0010F\"\u0004\bG\u0010HR\u001d\u0010M\u001a\u00020I8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bJ\u0010;\u001a\u0004\bK\u0010LR!\u0010S\u001a\u00060Nj\u0002`O8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bP\u0010;\u001a\u0004\bQ\u0010RR\u001d\u0010X\u001a\u00020T8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bU\u0010;\u001a\u0004\bV\u0010W¨\u0006_"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;", "Lcom/discord/app/AppBottomSheet;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "event", "", "handleEvent", "(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;)V", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;", "viewState", "configureUI", "(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;)V", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "centerContent", "Lcom/discord/api/channel/Channel;", "channel", "configureCenterContent", "(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/api/channel/Channel;)V", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "onStreamPreviewClicked", "(Lcom/discord/utilities/streams/StreamContext;)V", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "bottomContent", "configureBottomContent", "(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;Lcom/discord/api/channel/Channel;)V", "showSuppressedDialog", "()V", "showServerMutedDialog", "showServerDeafenedDialog", "showNoVideoPermissionDialog", "showNoVideoDevicesToast", "showNoScreenSharePermissionDialog", "", "getContentViewResId", "()I", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "requestCode", "resultCode", "Landroid/content/Intent;", "intent", "onActivityResult", "(IILandroid/content/Intent;)V", "onResume", "Lcom/discord/databinding/WidgetVoiceBottomSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetVoiceBottomSheetBinding;", "binding", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;", "participantsAdapter", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;", "", "forwardToFullscreenIfVideoActivated$delegate", "Lkotlin/Lazy;", "getForwardToFullscreenIfVideoActivated", "()Z", "forwardToFullscreenIfVideoActivated", "Lrx/Subscription;", "viewModelEventSubscription", "Lrx/Subscription;", "Lkotlin/Function1;", "onStreamPreviewClickedListener", "Lkotlin/jvm/functions/Function1;", "getOnStreamPreviewClickedListener", "()Lkotlin/jvm/functions/Function1;", "setOnStreamPreviewClickedListener", "(Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "featureContext$delegate", "getFeatureContext", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "featureContext", "", "Lcom/discord/primitives/ChannelId;", "channelId$delegate", "getChannelId", "()J", "channelId", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;", "viewModel", HookHelper.constructorName, "Companion", "BottomContent", "CenterContent", "FeatureContext", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceBottomSheet extends AppBottomSheet {
    private static final String ANALYTICS_SOURCE = "Voice Channel Bottom Sheet";
    private static final String ARG_FEATURE_CONTEXT = "ARG_FEATURE_CONTEXT";
    private static final String ARG_FORWARD_TO_FULLSCREEN_IF_VIDEO_ACTIVATED = "ARG_FORWARD_TO_FULLSCREEN_IF_VIDEO_ACTIVATED";
    private static final String END_EVENT_REQUEST_KEY = "END_EVENT_REQUEST_KEY";
    private CallParticipantsAdapter participantsAdapter;
    private final Lazy viewModel$delegate;
    private Subscription viewModelEventSubscription;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetVoiceBottomSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetVoiceBottomSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetVoiceBottomSheet$binding$2.INSTANCE, null, 2, null);
    private final Lazy channelId$delegate = g.lazy(new WidgetVoiceBottomSheet$channelId$2(this));
    private final Lazy forwardToFullscreenIfVideoActivated$delegate = g.lazy(new WidgetVoiceBottomSheet$forwardToFullscreenIfVideoActivated$2(this));
    private final Lazy featureContext$delegate = g.lazy(new WidgetVoiceBottomSheet$featureContext$2(this));
    private Function1<? super StreamContext, Unit> onStreamPreviewClickedListener = WidgetVoiceBottomSheet$onStreamPreviewClickedListener$1.INSTANCE;

    /* compiled from: WidgetVoiceBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "", HookHelper.constructorName, "()V", "Connect", "Controls", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class BottomContent {

        /* compiled from: WidgetVoiceBottomSheet.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u00022\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0005\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "", "component1", "()Z", "isConnectEnabled", "copy", "(Z)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Connect extends BottomContent {
            private final boolean isConnectEnabled;

            public Connect(boolean z2) {
                super(null);
                this.isConnectEnabled = z2;
            }

            public static /* synthetic */ Connect copy$default(Connect connect, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = connect.isConnectEnabled;
                }
                return connect.copy(z2);
            }

            public final boolean component1() {
                return this.isConnectEnabled;
            }

            public final Connect copy(boolean z2) {
                return new Connect(z2);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Connect) && this.isConnectEnabled == ((Connect) obj).isConnectEnabled;
                }
                return true;
            }

            public int hashCode() {
                boolean z2 = this.isConnectEnabled;
                if (z2) {
                    return 1;
                }
                return z2 ? 1 : 0;
            }

            public final boolean isConnectEnabled() {
                return this.isConnectEnabled;
            }

            public String toString() {
                return a.M(a.R("Connect(isConnectEnabled="), this.isConnectEnabled, ")");
            }
        }

        /* compiled from: WidgetVoiceBottomSheet.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\u0006\u0010\u0012\u001a\u00020\b\u0012\u0006\u0010\u0013\u001a\u00020\u000b\u0012\u0006\u0010\u0014\u001a\u00020\b\u0012\u0006\u0010\u0015\u001a\u00020\b¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000e\u0010\nJ\u0010\u0010\u000f\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u000f\u0010\nJL\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u00052\b\b\u0002\u0010\u0012\u001a\u00020\b2\b\b\u0002\u0010\u0013\u001a\u00020\u000b2\b\b\u0002\u0010\u0014\u001a\u00020\b2\b\b\u0002\u0010\u0015\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010 \u001a\u00020\b2\b\u0010\u001f\u001a\u0004\u0018\u00010\u001eHÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u0012\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b\u0012\u0010\nR\u0019\u0010\u0011\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b$\u0010\u0007R\u0019\u0010\u0010\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010%\u001a\u0004\b&\u0010\u0004R\u0019\u0010\u0013\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010'\u001a\u0004\b(\u0010\rR\u0019\u0010\u0014\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\"\u001a\u0004\b)\u0010\nR\u0019\u0010\u0015\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\"\u001a\u0004\b\u0015\u0010\n¨\u0006,"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "component1", "()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "Lcom/discord/stores/StoreAudioManagerV2$State;", "component2", "()Lcom/discord/stores/StoreAudioManagerV2$State;", "", "component3", "()Z", "Lcom/discord/widgets/voice/model/CameraState;", "component4", "()Lcom/discord/widgets/voice/model/CameraState;", "component5", "component6", "inputMode", "audioManagerState", "isMuted", "cameraState", "showScreenShareSparkle", "isScreensharing", "copy", "(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioManagerV2$State;ZLcom/discord/widgets/voice/model/CameraState;ZZ)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/stores/StoreAudioManagerV2$State;", "getAudioManagerState", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "getInputMode", "Lcom/discord/widgets/voice/model/CameraState;", "getCameraState", "getShowScreenShareSparkle", HookHelper.constructorName, "(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioManagerV2$State;ZLcom/discord/widgets/voice/model/CameraState;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Controls extends BottomContent {
            private final StoreAudioManagerV2.State audioManagerState;
            private final CameraState cameraState;
            private final MediaEngineConnection.InputMode inputMode;
            private final boolean isMuted;
            private final boolean isScreensharing;
            private final boolean showScreenShareSparkle;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Controls(MediaEngineConnection.InputMode inputMode, StoreAudioManagerV2.State state, boolean z2, CameraState cameraState, boolean z3, boolean z4) {
                super(null);
                m.checkNotNullParameter(inputMode, "inputMode");
                m.checkNotNullParameter(state, "audioManagerState");
                m.checkNotNullParameter(cameraState, "cameraState");
                this.inputMode = inputMode;
                this.audioManagerState = state;
                this.isMuted = z2;
                this.cameraState = cameraState;
                this.showScreenShareSparkle = z3;
                this.isScreensharing = z4;
            }

            public static /* synthetic */ Controls copy$default(Controls controls, MediaEngineConnection.InputMode inputMode, StoreAudioManagerV2.State state, boolean z2, CameraState cameraState, boolean z3, boolean z4, int i, Object obj) {
                if ((i & 1) != 0) {
                    inputMode = controls.inputMode;
                }
                if ((i & 2) != 0) {
                    state = controls.audioManagerState;
                }
                StoreAudioManagerV2.State state2 = state;
                if ((i & 4) != 0) {
                    z2 = controls.isMuted;
                }
                boolean z5 = z2;
                if ((i & 8) != 0) {
                    cameraState = controls.cameraState;
                }
                CameraState cameraState2 = cameraState;
                if ((i & 16) != 0) {
                    z3 = controls.showScreenShareSparkle;
                }
                boolean z6 = z3;
                if ((i & 32) != 0) {
                    z4 = controls.isScreensharing;
                }
                return controls.copy(inputMode, state2, z5, cameraState2, z6, z4);
            }

            public final MediaEngineConnection.InputMode component1() {
                return this.inputMode;
            }

            public final StoreAudioManagerV2.State component2() {
                return this.audioManagerState;
            }

            public final boolean component3() {
                return this.isMuted;
            }

            public final CameraState component4() {
                return this.cameraState;
            }

            public final boolean component5() {
                return this.showScreenShareSparkle;
            }

            public final boolean component6() {
                return this.isScreensharing;
            }

            public final Controls copy(MediaEngineConnection.InputMode inputMode, StoreAudioManagerV2.State state, boolean z2, CameraState cameraState, boolean z3, boolean z4) {
                m.checkNotNullParameter(inputMode, "inputMode");
                m.checkNotNullParameter(state, "audioManagerState");
                m.checkNotNullParameter(cameraState, "cameraState");
                return new Controls(inputMode, state, z2, cameraState, z3, z4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Controls)) {
                    return false;
                }
                Controls controls = (Controls) obj;
                return m.areEqual(this.inputMode, controls.inputMode) && m.areEqual(this.audioManagerState, controls.audioManagerState) && this.isMuted == controls.isMuted && m.areEqual(this.cameraState, controls.cameraState) && this.showScreenShareSparkle == controls.showScreenShareSparkle && this.isScreensharing == controls.isScreensharing;
            }

            public final StoreAudioManagerV2.State getAudioManagerState() {
                return this.audioManagerState;
            }

            public final CameraState getCameraState() {
                return this.cameraState;
            }

            public final MediaEngineConnection.InputMode getInputMode() {
                return this.inputMode;
            }

            public final boolean getShowScreenShareSparkle() {
                return this.showScreenShareSparkle;
            }

            public int hashCode() {
                MediaEngineConnection.InputMode inputMode = this.inputMode;
                int i = 0;
                int hashCode = (inputMode != null ? inputMode.hashCode() : 0) * 31;
                StoreAudioManagerV2.State state = this.audioManagerState;
                int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
                boolean z2 = this.isMuted;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode2 + i3) * 31;
                CameraState cameraState = this.cameraState;
                if (cameraState != null) {
                    i = cameraState.hashCode();
                }
                int i6 = (i5 + i) * 31;
                boolean z3 = this.showScreenShareSparkle;
                if (z3) {
                    z3 = true;
                }
                int i7 = z3 ? 1 : 0;
                int i8 = z3 ? 1 : 0;
                int i9 = (i6 + i7) * 31;
                boolean z4 = this.isScreensharing;
                if (!z4) {
                    i2 = z4 ? 1 : 0;
                }
                return i9 + i2;
            }

            public final boolean isMuted() {
                return this.isMuted;
            }

            public final boolean isScreensharing() {
                return this.isScreensharing;
            }

            public String toString() {
                StringBuilder R = a.R("Controls(inputMode=");
                R.append(this.inputMode);
                R.append(", audioManagerState=");
                R.append(this.audioManagerState);
                R.append(", isMuted=");
                R.append(this.isMuted);
                R.append(", cameraState=");
                R.append(this.cameraState);
                R.append(", showScreenShareSparkle=");
                R.append(this.showScreenShareSparkle);
                R.append(", isScreensharing=");
                return a.M(R, this.isScreensharing, ")");
            }
        }

        private BottomContent() {
        }

        public /* synthetic */ BottomContent(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetVoiceBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "", HookHelper.constructorName, "()V", "Empty", "ListItems", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$ListItems;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$Empty;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class CenterContent {

        /* compiled from: WidgetVoiceBottomSheet.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$Empty;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem;", "component1", "()Ljava/util/List;", "items", "copy", "(Ljava/util/List;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$Empty;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Empty extends CenterContent {
            private final List<CallParticipantsAdapter.ListItem> items;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Empty(List<? extends CallParticipantsAdapter.ListItem> list) {
                super(null);
                m.checkNotNullParameter(list, "items");
                this.items = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Empty copy$default(Empty empty, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = empty.items;
                }
                return empty.copy(list);
            }

            public final List<CallParticipantsAdapter.ListItem> component1() {
                return this.items;
            }

            public final Empty copy(List<? extends CallParticipantsAdapter.ListItem> list) {
                m.checkNotNullParameter(list, "items");
                return new Empty(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Empty) && m.areEqual(this.items, ((Empty) obj).items);
                }
                return true;
            }

            public final List<CallParticipantsAdapter.ListItem> getItems() {
                return this.items;
            }

            public int hashCode() {
                List<CallParticipantsAdapter.ListItem> list = this.items;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("Empty(items="), this.items, ")");
            }
        }

        /* compiled from: WidgetVoiceBottomSheet.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$ListItems;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem;", "component1", "()Ljava/util/List;", "items", "copy", "(Ljava/util/List;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$ListItems;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ListItems extends CenterContent {
            private final List<CallParticipantsAdapter.ListItem> items;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public ListItems(List<? extends CallParticipantsAdapter.ListItem> list) {
                super(null);
                m.checkNotNullParameter(list, "items");
                this.items = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ ListItems copy$default(ListItems listItems, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = listItems.items;
                }
                return listItems.copy(list);
            }

            public final List<CallParticipantsAdapter.ListItem> component1() {
                return this.items;
            }

            public final ListItems copy(List<? extends CallParticipantsAdapter.ListItem> list) {
                m.checkNotNullParameter(list, "items");
                return new ListItems(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ListItems) && m.areEqual(this.items, ((ListItems) obj).items);
                }
                return true;
            }

            public final List<CallParticipantsAdapter.ListItem> getItems() {
                return this.items;
            }

            public int hashCode() {
                List<CallParticipantsAdapter.ListItem> list = this.items;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("ListItems(items="), this.items, ")");
            }
        }

        private CenterContent() {
        }

        public /* synthetic */ CenterContent(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetVoiceBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J1\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0011\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0011\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0010R\u0016\u0010\u0013\u001a\u00020\u000e8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0013\u0010\u0010¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "forwardToFullscreenIfVideoActivated", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "featureContext", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;", "show", "(Landroidx/fragment/app/FragmentManager;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;", "", "ANALYTICS_SOURCE", "Ljava/lang/String;", WidgetVoiceBottomSheet.ARG_FEATURE_CONTEXT, WidgetVoiceBottomSheet.ARG_FORWARD_TO_FULLSCREEN_IF_VIDEO_ACTIVATED, WidgetVoiceBottomSheet.END_EVENT_REQUEST_KEY, HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final WidgetVoiceBottomSheet show(FragmentManager fragmentManager, long j, boolean z2, FeatureContext featureContext) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(featureContext, "featureContext");
            WidgetVoiceBottomSheet widgetVoiceBottomSheet = new WidgetVoiceBottomSheet();
            Bundle bundle = new Bundle();
            bundle.putLong("com.discord.intent.extra.EXTRA_CHANNEL_ID", j);
            bundle.putSerializable(WidgetVoiceBottomSheet.ARG_FEATURE_CONTEXT, featureContext);
            bundle.putBoolean(WidgetVoiceBottomSheet.ARG_FORWARD_TO_FULLSCREEN_IF_VIDEO_ACTIVATED, z2);
            widgetVoiceBottomSheet.setArguments(bundle);
            widgetVoiceBottomSheet.show(fragmentManager, WidgetVoiceBottomSheet.class.getSimpleName());
            return widgetVoiceBottomSheet;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetVoiceBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "HOME", "FULLSCREEN_CALL", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum FeatureContext {
        HOME,
        FULLSCREEN_CALL
    }

    /* compiled from: WidgetVoiceBottomSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u0001R\u0016\u0010\u0005\u001a\u00020\u00028&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00068&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u000b\u001a\u00020\u00068&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u0016\u0010\u000f\u001a\u00020\f8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u0011\u001a\u00020\u00108&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0018\u0010\u0013\u001a\u0004\u0018\u00010\u00108&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0018\u0010\u0018\u001a\u0004\u0018\u00010\u00158&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0016\u0010\u001a\u001a\u00020\u00108&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u0012¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$ViewState;", "", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "getCenterContent", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "centerContent", "", "getSubtitle", "()Ljava/lang/String;", "subtitle", "getTitle", "title", "Lcom/discord/api/channel/Channel;", "getChannel", "()Lcom/discord/api/channel/Channel;", "channel", "", "isDeafened", "()Z", "isNoiseCancellationActive", "()Ljava/lang/Boolean;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "getBottomContent", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "bottomContent", "getShowInviteOption", "showInviteOption", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface ViewState {
        BottomContent getBottomContent();

        CenterContent getCenterContent();

        Channel getChannel();

        boolean getShowInviteOption();

        String getSubtitle();

        String getTitle();

        boolean isDeafened();

        Boolean isNoiseCancellationActive();
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            FeatureContext.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[FeatureContext.HOME.ordinal()] = 1;
            iArr[FeatureContext.FULLSCREEN_CALL.ordinal()] = 2;
        }
    }

    public WidgetVoiceBottomSheet() {
        super(false, 1, null);
        WidgetVoiceBottomSheet$viewModel$2 widgetVoiceBottomSheet$viewModel$2 = new WidgetVoiceBottomSheet$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetVoiceBottomSheetViewModel.class), new WidgetVoiceBottomSheet$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetVoiceBottomSheet$viewModel$2));
    }

    private final void configureBottomContent(BottomContent bottomContent, Channel channel) {
        if (bottomContent == null) {
            AnchoredVoiceControlsView anchoredVoiceControlsView = getBinding().f;
            m.checkNotNullExpressionValue(anchoredVoiceControlsView, "binding.voiceBottomSheetControls");
            anchoredVoiceControlsView.setVisibility(8);
            RelativeLayout relativeLayout = getBinding().e;
            m.checkNotNullExpressionValue(relativeLayout, "binding.voiceBottomSheetConnectContainer");
            relativeLayout.setVisibility(8);
            CoordinatorLayout coordinatorLayout = getBinding().i;
            CoordinatorLayout coordinatorLayout2 = getBinding().i;
            m.checkNotNullExpressionValue(coordinatorLayout2, "binding.voiceBottomSheetRoot");
            coordinatorLayout.setBackgroundColor(ColorCompat.getThemedColor(coordinatorLayout2, (int) R.attr.colorBackgroundPrimary));
            return;
        }
        if (bottomContent instanceof BottomContent.Controls) {
            AnchoredVoiceControlsView anchoredVoiceControlsView2 = getBinding().f;
            m.checkNotNullExpressionValue(anchoredVoiceControlsView2, "binding.voiceBottomSheetControls");
            anchoredVoiceControlsView2.setVisibility(0);
            RelativeLayout relativeLayout2 = getBinding().e;
            m.checkNotNullExpressionValue(relativeLayout2, "binding.voiceBottomSheetConnectContainer");
            relativeLayout2.setVisibility(8);
            AnchoredVoiceControlsView anchoredVoiceControlsView3 = getBinding().f;
            BottomContent.Controls controls = (BottomContent.Controls) bottomContent;
            MediaEngineConnection.InputMode inputMode = controls.getInputMode();
            boolean isMuted = controls.isMuted();
            boolean isScreensharing = controls.isScreensharing();
            CameraState cameraState = controls.getCameraState();
            WidgetVoiceBottomSheet$configureBottomContent$1 widgetVoiceBottomSheet$configureBottomContent$1 = new WidgetVoiceBottomSheet$configureBottomContent$1(getViewModel());
            WidgetVoiceBottomSheet$configureBottomContent$2 widgetVoiceBottomSheet$configureBottomContent$2 = new WidgetVoiceBottomSheet$configureBottomContent$2(getViewModel());
            WidgetVoiceBottomSheet$configureBottomContent$3 widgetVoiceBottomSheet$configureBottomContent$3 = new WidgetVoiceBottomSheet$configureBottomContent$3(this);
            WidgetVoiceBottomSheet$configureBottomContent$4 widgetVoiceBottomSheet$configureBottomContent$4 = new WidgetVoiceBottomSheet$configureBottomContent$4(getViewModel());
            WidgetVoiceBottomSheet$configureBottomContent$5 widgetVoiceBottomSheet$configureBottomContent$5 = new WidgetVoiceBottomSheet$configureBottomContent$5(getViewModel());
            boolean showScreenShareSparkle = controls.getShowScreenShareSparkle();
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            anchoredVoiceControlsView3.configureUI(inputMode, controls.getAudioManagerState(), isMuted, isScreensharing, showScreenShareSparkle, cameraState, widgetVoiceBottomSheet$configureBottomContent$1, widgetVoiceBottomSheet$configureBottomContent$2, widgetVoiceBottomSheet$configureBottomContent$3, widgetVoiceBottomSheet$configureBottomContent$4, widgetVoiceBottomSheet$configureBottomContent$5, this, parentFragmentManager, new WidgetVoiceBottomSheet$configureBottomContent$6(this), getChannelId(), channel.f(), getForwardToFullscreenIfVideoActivated(), getFeatureContext());
            CoordinatorLayout coordinatorLayout3 = getBinding().i;
            CoordinatorLayout coordinatorLayout4 = getBinding().i;
            m.checkNotNullExpressionValue(coordinatorLayout4, "binding.voiceBottomSheetRoot");
            coordinatorLayout3.setBackgroundColor(ColorCompat.getThemedColor(coordinatorLayout4, (int) R.attr.colorBackgroundSecondary));
        } else if (bottomContent instanceof BottomContent.Connect) {
            AnchoredVoiceControlsView anchoredVoiceControlsView4 = getBinding().f;
            m.checkNotNullExpressionValue(anchoredVoiceControlsView4, "binding.voiceBottomSheetControls");
            anchoredVoiceControlsView4.setVisibility(4);
            getBinding().f.hidePtt();
            RelativeLayout relativeLayout3 = getBinding().e;
            m.checkNotNullExpressionValue(relativeLayout3, "binding.voiceBottomSheetConnectContainer");
            relativeLayout3.setVisibility(0);
            BottomContent.Connect connect = (BottomContent.Connect) bottomContent;
            if (connect.isConnectEnabled()) {
                getBinding().d.setText(R.string.join_voice_channel_cta);
                MaterialButton materialButton = getBinding().d;
                m.checkNotNullExpressionValue(materialButton, "binding.voiceBottomSheetConnect");
                materialButton.setEnabled(true);
            } else {
                getBinding().d.setText(R.string.channel_locked_short);
                MaterialButton materialButton2 = getBinding().d;
                m.checkNotNullExpressionValue(materialButton2, "binding.voiceBottomSheetConnect");
                materialButton2.setEnabled(false);
            }
            getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet$configureBottomContent$7

                /* compiled from: WidgetVoiceBottomSheet.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet$configureBottomContent$7$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function0<Unit> {
                    public AnonymousClass1() {
                        super(0);
                    }

                    @Override // kotlin.jvm.functions.Function0
                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2() {
                        WidgetVoiceBottomSheetViewModel viewModel;
                        viewModel = WidgetVoiceBottomSheet.this.getViewModel();
                        viewModel.tryConnectToVoice(false);
                    }
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    d.S1(WidgetVoiceBottomSheet.this, null, new AnonymousClass1(), 1, null);
                }
            });
            MaterialButton materialButton3 = getBinding().g;
            m.checkNotNullExpressionValue(materialButton3, "binding.voiceBottomSheetJoinVideo");
            materialButton3.setEnabled(connect.isConnectEnabled());
            getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet$configureBottomContent$8

                /* compiled from: WidgetVoiceBottomSheet.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet$configureBottomContent$8$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1 extends o implements Function0<Unit> {
                    public AnonymousClass1() {
                        super(0);
                    }

                    @Override // kotlin.jvm.functions.Function0
                    /* renamed from: invoke  reason: avoid collision after fix types in other method */
                    public final void invoke2() {
                        WidgetVoiceBottomSheetViewModel viewModel;
                        viewModel = WidgetVoiceBottomSheet.this.getViewModel();
                        viewModel.tryConnectToVoice(true);
                    }
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetVoiceBottomSheet.this.requestVideoCallPermissions(new AnonymousClass1());
                }
            });
            CoordinatorLayout coordinatorLayout5 = getBinding().i;
            CoordinatorLayout coordinatorLayout6 = getBinding().i;
            m.checkNotNullExpressionValue(coordinatorLayout6, "binding.voiceBottomSheetRoot");
            coordinatorLayout5.setBackgroundColor(ColorCompat.getThemedColor(coordinatorLayout6, (int) R.attr.colorBackgroundPrimary));
        }
    }

    private final void configureCenterContent(CenterContent centerContent, Channel channel) {
        int i = 0;
        if (centerContent instanceof CenterContent.ListItems) {
            r0 r0Var = getBinding().f2672b;
            m.checkNotNullExpressionValue(r0Var, "binding.empty");
            LinearLayout linearLayout = r0Var.a;
            m.checkNotNullExpressionValue(linearLayout, "binding.empty.root");
            linearLayout.setVisibility(4);
            RecyclerView recyclerView = getBinding().h;
            m.checkNotNullExpressionValue(recyclerView, "binding.voiceBottomSheetRecycler");
            recyclerView.setVisibility(0);
            CallParticipantsAdapter callParticipantsAdapter = this.participantsAdapter;
            if (callParticipantsAdapter == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter.setData(((CenterContent.ListItems) centerContent).getItems());
            CallParticipantsAdapter callParticipantsAdapter2 = this.participantsAdapter;
            if (callParticipantsAdapter2 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter2.setOnStreamPreviewClicked(new WidgetVoiceBottomSheet$configureCenterContent$1(this));
            CallParticipantsAdapter callParticipantsAdapter3 = this.participantsAdapter;
            if (callParticipantsAdapter3 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter3.setOnVoiceUserClicked(new WidgetVoiceBottomSheet$configureCenterContent$2(this, channel));
            CallParticipantsAdapter callParticipantsAdapter4 = this.participantsAdapter;
            if (callParticipantsAdapter4 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter4.setOnToggleRingingClicked(new WidgetVoiceBottomSheet$configureCenterContent$3(this));
            CallParticipantsAdapter callParticipantsAdapter5 = this.participantsAdapter;
            if (callParticipantsAdapter5 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter5.setOnInviteFriendsClicked(new WidgetVoiceBottomSheet$configureCenterContent$4(this, channel));
            CallParticipantsAdapter callParticipantsAdapter6 = this.participantsAdapter;
            if (callParticipantsAdapter6 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter6.setOnEmbeddedActivityClicked(new WidgetVoiceBottomSheet$configureCenterContent$5(this));
        } else if (centerContent instanceof CenterContent.Empty) {
            r0 r0Var2 = getBinding().f2672b;
            m.checkNotNullExpressionValue(r0Var2, "binding.empty");
            LinearLayout linearLayout2 = r0Var2.a;
            m.checkNotNullExpressionValue(linearLayout2, "binding.empty.root");
            linearLayout2.setVisibility(0);
            RecyclerView recyclerView2 = getBinding().h;
            m.checkNotNullExpressionValue(recyclerView2, "binding.voiceBottomSheetRecycler");
            CenterContent.Empty empty = (CenterContent.Empty) centerContent;
            if (!(!empty.getItems().isEmpty())) {
                i = 8;
            }
            recyclerView2.setVisibility(i);
            CallParticipantsAdapter callParticipantsAdapter7 = this.participantsAdapter;
            if (callParticipantsAdapter7 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter7.setData(empty.getItems());
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final WidgetVoiceBottomSheetViewModel.ViewState viewState) {
        String str;
        TextView textView = getBinding().c.g;
        m.checkNotNullExpressionValue(textView, "binding.header.voiceBottomSheetHeaderTitle");
        textView.setText(viewState.getTitle());
        TextView textView2 = getBinding().c.f;
        m.checkNotNullExpressionValue(textView2, "binding.header.voiceBottomSheetHeaderSubtitle");
        ViewExtensions.setTextAndVisibilityBy(textView2, viewState.getSubtitle());
        ImageView imageView = getBinding().c.f191b;
        m.checkNotNullExpressionValue(imageView, "binding.header.voiceBottomSheetHeaderDeafen");
        imageView.setActivated(viewState.isDeafened());
        ImageView imageView2 = getBinding().c.f191b;
        m.checkNotNullExpressionValue(imageView2, "binding.header.voiceBottomSheetHeaderDeafen");
        if (viewState.isDeafened()) {
            str = getString(R.string.undeafen);
        } else {
            str = getString(R.string.deafen);
        }
        imageView2.setContentDescription(str);
        getBinding().c.f191b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceBottomSheetViewModel viewModel;
                viewModel = WidgetVoiceBottomSheet.this.getViewModel();
                viewModel.onDeafenPressed();
            }
        });
        ImageView imageView3 = getBinding().c.c;
        m.checkNotNullExpressionValue(imageView3, "binding.header.voiceBottomSheetHeaderInvite");
        imageView3.setVisibility(viewState.getShowInviteOption() ? 0 : 8);
        getBinding().c.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChannelInviteLaunchUtils.INSTANCE.inviteToChannel(WidgetVoiceBottomSheet.this, viewState.getChannel(), "Voice Channel Bottom Sheet", (r13 & 8) != 0 ? null : null, (r13 & 16) != 0 ? null : null);
            }
        });
        getBinding().c.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetSettingsVoice.Companion companion = WidgetSettingsVoice.Companion;
                Context requireContext = WidgetVoiceBottomSheet.this.requireContext();
                m.checkNotNullExpressionValue(requireContext, "requireContext()");
                WidgetSettingsVoice.Companion.launch$default(companion, requireContext, null, false, 6, null);
            }
        });
        getBinding().c.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceBottomSheetViewModel viewModel;
                viewModel = WidgetVoiceBottomSheet.this.getViewModel();
                viewModel.onNoiseCancellationPressed();
            }
        });
        if (viewState.isNoiseCancellationActive() != null) {
            if (m.areEqual(viewState.isNoiseCancellationActive(), Boolean.TRUE)) {
                getBinding().c.d.setImageResource(R.drawable.ic_noise_cancellation_active_24dp);
            } else {
                getBinding().c.d.setImageResource(R.drawable.ic_noise_cancellation_disabled_24dp);
            }
            ImageView imageView4 = getBinding().c.d;
            m.checkNotNullExpressionValue(imageView4, "binding.header.voiceBott…etHeaderNoiseCancellation");
            imageView4.setVisibility(0);
        } else {
            ImageView imageView5 = getBinding().c.d;
            m.checkNotNullExpressionValue(imageView5, "binding.header.voiceBott…etHeaderNoiseCancellation");
            imageView5.setVisibility(8);
        }
        configureCenterContent(viewState.getCenterContent(), viewState.getChannel());
        configureBottomContent(viewState.getBottomContent(), viewState.getChannel());
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetVoiceBottomSheetBinding getBinding() {
        return (WidgetVoiceBottomSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final long getChannelId() {
        return ((Number) this.channelId$delegate.getValue()).longValue();
    }

    private final FeatureContext getFeatureContext() {
        return (FeatureContext) this.featureContext$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final boolean getForwardToFullscreenIfVideoActivated() {
        return ((Boolean) this.forwardToFullscreenIfVideoActivated$delegate.getValue()).booleanValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetVoiceBottomSheetViewModel getViewModel() {
        return (WidgetVoiceBottomSheetViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleEvent(WidgetVoiceBottomSheetViewModel.Event event) {
        if (m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ShowSuppressedDialog.INSTANCE)) {
            showSuppressedDialog();
        } else if (m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ShowServerMutedDialog.INSTANCE)) {
            showServerMutedDialog();
        } else if (m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ShowServerDeafenedDialog.INSTANCE)) {
            showServerDeafenedDialog();
        } else if (m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ShowNoVideoPermissionDialog.INSTANCE)) {
            showNoVideoPermissionDialog();
        } else if (m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ShowNoVideoDevicesAvailableToast.INSTANCE)) {
            showNoVideoDevicesToast();
        } else if (m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ShowRequestCameraPermissionsDialog.INSTANCE)) {
            requestVideoCallPermissions(new WidgetVoiceBottomSheet$handleEvent$1(this));
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.ShowCameraCapacityDialog) {
            j.a aVar = j.k;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            aVar.a(parentFragmentManager, ((WidgetVoiceBottomSheetViewModel.Event.ShowCameraCapacityDialog) event).getGuildMaxVideoChannelUsers());
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.ShowNoiseCancellationBottomSheet) {
            WidgetNoiseCancellationBottomSheet.Companion.show(this);
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.LaunchVideoCall) {
            Subscription subscription = this.viewModelEventSubscription;
            if (subscription != null) {
                subscription.unsubscribe();
            }
            dismiss();
            WidgetCallFullscreen.Companion companion = WidgetCallFullscreen.Companion;
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            WidgetVoiceBottomSheetViewModel.Event.LaunchVideoCall launchVideoCall = (WidgetVoiceBottomSheetViewModel.Event.LaunchVideoCall) event;
            companion.launch(requireContext, launchVideoCall.getChannelId(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : launchVideoCall.getAutoTargetStreamKey(), (r14 & 16) != 0 ? null : null);
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.LaunchStageChannel) {
            Subscription subscription2 = this.viewModelEventSubscription;
            if (subscription2 != null) {
                subscription2.unsubscribe();
            }
            dismiss();
            WidgetCallFullscreen.Companion companion2 = WidgetCallFullscreen.Companion;
            Context requireContext2 = requireContext();
            m.checkNotNullExpressionValue(requireContext2, "requireContext()");
            companion2.launch(requireContext2, ((WidgetVoiceBottomSheetViewModel.Event.LaunchStageChannel) event).getChannelId(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
        } else if (m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ShowGuildVideoAtCapacityDialog.INSTANCE)) {
            m.a aVar2 = b.a.a.m.k;
            FragmentManager parentFragmentManager2 = getParentFragmentManager();
            d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
            aVar2.a(parentFragmentManager2);
        } else if (d0.z.d.m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ShowOverlayNux.INSTANCE)) {
            WidgetNoticeNuxOverlay.Companion.enqueue();
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.ShowToast) {
            b.a.d.m.g(requireContext(), ((WidgetVoiceBottomSheetViewModel.Event.ShowToast) event).getToastResId(), 0, null, 12);
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.ShowEventEnd) {
            WidgetEndGuildScheduledEventBottomSheet.Companion companion3 = WidgetEndGuildScheduledEventBottomSheet.Companion;
            FragmentManager parentFragmentManager3 = getParentFragmentManager();
            d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager3, "parentFragmentManager");
            WidgetVoiceBottomSheetViewModel.Event.ShowEventEnd showEventEnd = (WidgetVoiceBottomSheetViewModel.Event.ShowEventEnd) event;
            companion3.show(parentFragmentManager3, END_EVENT_REQUEST_KEY, showEventEnd.getGuildScheduledEvent().h(), showEventEnd.getGuildScheduledEvent().i());
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.EnqueueCallFeedbackSheet) {
            WidgetVoiceBottomSheetViewModel.Event.EnqueueCallFeedbackSheet enqueueCallFeedbackSheet = (WidgetVoiceBottomSheetViewModel.Event.EnqueueCallFeedbackSheet) event;
            CallFeedbackSheetNavigator.INSTANCE.enqueueNotice(enqueueCallFeedbackSheet.getChannelId(), enqueueCallFeedbackSheet.getRtcConnectionId(), enqueueCallFeedbackSheet.getMediaSessionId(), Long.valueOf(enqueueCallFeedbackSheet.getCallDuration()), enqueueCallFeedbackSheet.getTriggerRateDenominator());
        } else if (d0.z.d.m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.Dismiss.INSTANCE)) {
            dismiss();
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.AccessibilityAnnouncement) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            Context requireContext3 = requireContext();
            d0.z.d.m.checkNotNullExpressionValue(requireContext3, "requireContext()");
            String string = getString(((WidgetVoiceBottomSheetViewModel.Event.AccessibilityAnnouncement) event).getMessageResId());
            d0.z.d.m.checkNotNullExpressionValue(string, "getString(event.messageResId)");
            accessibilityUtils.sendAnnouncement(requireContext3, string);
        } else if (event instanceof WidgetVoiceBottomSheetViewModel.Event.ShowNoScreenSharePermissionDialog) {
            showNoScreenSharePermissionDialog();
        } else if (d0.z.d.m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.RequestStartStream.INSTANCE)) {
            StreamNavigator.requestStartStream(this);
        } else if (d0.z.d.m.areEqual(event, WidgetVoiceBottomSheetViewModel.Event.ExpandSheet.INSTANCE)) {
            setBottomSheetState(3);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onStreamPreviewClicked(StreamContext streamContext) {
        int ordinal = getFeatureContext().ordinal();
        if (ordinal == 0) {
            d.S1(this, null, new WidgetVoiceBottomSheet$onStreamPreviewClicked$1(this, streamContext), 1, null);
        } else if (ordinal == 1) {
            dismiss();
            this.onStreamPreviewClickedListener.invoke(streamContext);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final void showNoScreenSharePermissionDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
        d0.z.d.m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        d0.z.d.m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.no_video_permission_dialog_title);
        String string2 = requireContext.getString(R.string.no_screenshare_permission_dialog_body);
        d0.z.d.m.checkNotNullExpressionValue(string2, "context.getString(R.stri…e_permission_dialog_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showNoVideoDevicesToast() {
        b.a.d.m.i(this, R.string.no_video_devices, 0, 4);
    }

    private final void showNoVideoPermissionDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
        d0.z.d.m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        d0.z.d.m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.no_video_permission_dialog_title);
        String string2 = requireContext.getString(R.string.no_video_permission_dialog_body);
        d0.z.d.m.checkNotNullExpressionValue(string2, "context.getString(R.stri…o_permission_dialog_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showServerDeafenedDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
        d0.z.d.m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        d0.z.d.m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.server_deafened_dialog_title);
        String string2 = requireContext.getString(R.string.server_deafened_dialog_body);
        d0.z.d.m.checkNotNullExpressionValue(string2, "context.getString(R.stri…ver_deafened_dialog_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showServerMutedDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
        d0.z.d.m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        d0.z.d.m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.server_muted_dialog_title);
        String string2 = requireContext.getString(R.string.server_muted_dialog_body);
        d0.z.d.m.checkNotNullExpressionValue(string2, "context.getString(R.stri…server_muted_dialog_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showSuppressedDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        d0.z.d.m.checkNotNullExpressionValue(requireContext, "requireContext()");
        d0.z.d.m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        d0.z.d.m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.suppressed);
        String string2 = requireContext.getString(R.string.suppressed_permission_body);
        d0.z.d.m.checkNotNullExpressionValue(string2, "context.getString(R.stri…ppressed_permission_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_voice_bottom_sheet;
    }

    public final Function1<StreamContext, Unit> getOnStreamPreviewClickedListener() {
        return this.onStreamPreviewClickedListener;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        StreamNavigator.handleActivityResult(i, i2, intent, new WidgetVoiceBottomSheet$onActivityResult$1(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().h;
        d0.z.d.m.checkNotNullExpressionValue(recyclerView, "binding.voiceBottomSheetRecycler");
        CallParticipantsAdapter callParticipantsAdapter = (CallParticipantsAdapter) companion.configure(new CallParticipantsAdapter(recyclerView, false, true, 2, null));
        this.participantsAdapter = callParticipantsAdapter;
        if (callParticipantsAdapter == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("participantsAdapter");
        }
        callParticipantsAdapter.setOnEventClicked(new WidgetVoiceBottomSheet$onResume$1(this));
        CallParticipantsAdapter callParticipantsAdapter2 = this.participantsAdapter;
        if (callParticipantsAdapter2 == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("participantsAdapter");
        }
        callParticipantsAdapter2.setOnStartEventClicked(new WidgetVoiceBottomSheet$onResume$2(this));
        getBinding().h.setHasFixedSize(false);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetVoiceBottomSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetVoiceBottomSheet$onResume$3(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetVoiceBottomSheet.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetVoiceBottomSheet$onResume$5(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetVoiceBottomSheet$onResume$4(this));
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        d0.z.d.m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        setBottomSheetState(3);
        getBinding().f.setOnPttPressedListener(new WidgetVoiceBottomSheet$onViewCreated$1(getViewModel()));
        WidgetEndGuildScheduledEventBottomSheet.Companion.registerForResult(this, END_EVENT_REQUEST_KEY, new WidgetVoiceBottomSheet$onViewCreated$2(getViewModel()));
    }

    public final void setOnStreamPreviewClickedListener(Function1<? super StreamContext, Unit> function1) {
        d0.z.d.m.checkNotNullParameter(function1, "<set-?>");
        this.onStreamPreviewClickedListener = function1;
    }
}
