package com.discord.widgets.voice.sheet;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.r0;
import b.a.i.s0;
import com.discord.databinding.WidgetVoiceBottomSheetBinding;
import com.discord.widgets.voice.controls.AnchoredVoiceControlsView;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetVoiceBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetVoiceBottomSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetVoiceBottomSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetVoiceBottomSheet$binding$2 extends k implements Function1<View, WidgetVoiceBottomSheetBinding> {
    public static final WidgetVoiceBottomSheet$binding$2 INSTANCE = new WidgetVoiceBottomSheet$binding$2();

    public WidgetVoiceBottomSheet$binding$2() {
        super(1, WidgetVoiceBottomSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetVoiceBottomSheetBinding;", 0);
    }

    public final WidgetVoiceBottomSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.empty;
        View findViewById = view.findViewById(R.id.empty);
        if (findViewById != null) {
            r0 r0Var = new r0((LinearLayout) findViewById);
            i = R.id.header;
            View findViewById2 = view.findViewById(R.id.header);
            if (findViewById2 != null) {
                int i2 = R.id.title_text_barrier;
                Barrier barrier = (Barrier) findViewById2.findViewById(R.id.title_text_barrier);
                if (barrier != null) {
                    i2 = R.id.voice_bottom_sheet_header_deafen;
                    ImageView imageView = (ImageView) findViewById2.findViewById(R.id.voice_bottom_sheet_header_deafen);
                    if (imageView != null) {
                        i2 = R.id.voice_bottom_sheet_header_icon;
                        ImageView imageView2 = (ImageView) findViewById2.findViewById(R.id.voice_bottom_sheet_header_icon);
                        if (imageView2 != null) {
                            i2 = R.id.voice_bottom_sheet_header_invite;
                            ImageView imageView3 = (ImageView) findViewById2.findViewById(R.id.voice_bottom_sheet_header_invite);
                            if (imageView3 != null) {
                                i2 = R.id.voice_bottom_sheet_header_noise_cancellation;
                                ImageView imageView4 = (ImageView) findViewById2.findViewById(R.id.voice_bottom_sheet_header_noise_cancellation);
                                if (imageView4 != null) {
                                    i2 = R.id.voice_bottom_sheet_header_settings;
                                    ImageView imageView5 = (ImageView) findViewById2.findViewById(R.id.voice_bottom_sheet_header_settings);
                                    if (imageView5 != null) {
                                        i2 = R.id.voice_bottom_sheet_header_subtitle;
                                        TextView textView = (TextView) findViewById2.findViewById(R.id.voice_bottom_sheet_header_subtitle);
                                        if (textView != null) {
                                            i2 = R.id.voice_bottom_sheet_header_title;
                                            TextView textView2 = (TextView) findViewById2.findViewById(R.id.voice_bottom_sheet_header_title);
                                            if (textView2 != null) {
                                                s0 s0Var = new s0((ConstraintLayout) findViewById2, barrier, imageView, imageView2, imageView3, imageView4, imageView5, textView, textView2);
                                                ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.voice_bottom_sheet_bottom_content_container);
                                                if (constraintLayout != null) {
                                                    MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.voice_bottom_sheet_connect);
                                                    if (materialButton != null) {
                                                        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.voice_bottom_sheet_connect_container);
                                                        if (relativeLayout != null) {
                                                            AnchoredVoiceControlsView anchoredVoiceControlsView = (AnchoredVoiceControlsView) view.findViewById(R.id.voice_bottom_sheet_controls);
                                                            if (anchoredVoiceControlsView != null) {
                                                                MaterialButton materialButton2 = (MaterialButton) view.findViewById(R.id.voice_bottom_sheet_join_video);
                                                                if (materialButton2 != null) {
                                                                    RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.voice_bottom_sheet_recycler);
                                                                    if (recyclerView != null) {
                                                                        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) view;
                                                                        return new WidgetVoiceBottomSheetBinding(coordinatorLayout, r0Var, s0Var, constraintLayout, materialButton, relativeLayout, anchoredVoiceControlsView, materialButton2, recyclerView, coordinatorLayout);
                                                                    }
                                                                    i = R.id.voice_bottom_sheet_recycler;
                                                                } else {
                                                                    i = R.id.voice_bottom_sheet_join_video;
                                                                }
                                                            } else {
                                                                i = R.id.voice_bottom_sheet_controls;
                                                            }
                                                        } else {
                                                            i = R.id.voice_bottom_sheet_connect_container;
                                                        }
                                                    } else {
                                                        i = R.id.voice_bottom_sheet_connect;
                                                    }
                                                } else {
                                                    i = R.id.voice_bottom_sheet_bottom_content_container;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(findViewById2.getResources().getResourceName(i2)));
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
