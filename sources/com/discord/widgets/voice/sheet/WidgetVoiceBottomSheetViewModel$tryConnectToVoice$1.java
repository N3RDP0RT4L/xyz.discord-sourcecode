package com.discord.widgets.voice.sheet;

import com.discord.stores.StoreMediaEngine;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheetViewModel;
import d0.l;
import d0.w.h.c;
import d0.w.i.a.e;
import d0.w.i.a.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import org.objectweb.asm.Opcodes;
import rx.subjects.PublishSubject;
/* compiled from: WidgetVoiceBottomSheetViewModel.kt */
@e(c = "com.discord.widgets.voice.sheet.WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1", f = "WidgetVoiceBottomSheetViewModel.kt", l = {Opcodes.IRETURN, 180}, m = "invokeSuspend")
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0004\u001a\u00020\u0001*\u00020\u0000H\u008a@¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lkotlinx/coroutines/CoroutineScope;", "", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1 extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
    public final /* synthetic */ StoreMediaEngine $mediaEngine;
    public int label;
    public final /* synthetic */ WidgetVoiceBottomSheetViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1(WidgetVoiceBottomSheetViewModel widgetVoiceBottomSheetViewModel, StoreMediaEngine storeMediaEngine, Continuation continuation) {
        super(2, continuation);
        this.this$0 = widgetVoiceBottomSheetViewModel;
        this.$mediaEngine = storeMediaEngine;
    }

    @Override // d0.w.i.a.a
    public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
        m.checkNotNullParameter(continuation, "completion");
        return new WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1(this.this$0, this.$mediaEngine, continuation);
    }

    @Override // kotlin.jvm.functions.Function2
    public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
        return ((WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1) create(coroutineScope, continuation)).invokeSuspend(Unit.a);
    }

    @Override // d0.w.i.a.a
    public final Object invokeSuspend(Object obj) {
        PublishSubject publishSubject;
        long j;
        Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
        int i = this.label;
        boolean z2 = true;
        if (i == 0) {
            l.throwOnFailure(obj);
            StoreMediaEngine storeMediaEngine = this.$mediaEngine;
            this.label = 1;
            obj = storeMediaEngine.getDefaultVideoDeviceGUID(this);
            if (obj == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else if (i == 1) {
            l.throwOnFailure(obj);
        } else if (i == 2) {
            l.throwOnFailure(obj);
            return Unit.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (obj == null) {
            z2 = false;
        }
        if (z2) {
            WidgetVoiceBottomSheetViewModel widgetVoiceBottomSheetViewModel = this.this$0;
            j = widgetVoiceBottomSheetViewModel.channelId;
            widgetVoiceBottomSheetViewModel.joinVoiceChannel(j);
            StoreMediaEngine storeMediaEngine2 = this.$mediaEngine;
            this.label = 2;
            if (storeMediaEngine2.selectDefaultVideoDeviceAsync(this) == coroutine_suspended) {
                return coroutine_suspended;
            }
        } else {
            publishSubject = this.this$0.eventSubject;
            publishSubject.k.onNext(WidgetVoiceBottomSheetViewModel.Event.ShowNoVideoDevicesAvailableToast.INSTANCE);
        }
        return Unit.a;
    }
}
