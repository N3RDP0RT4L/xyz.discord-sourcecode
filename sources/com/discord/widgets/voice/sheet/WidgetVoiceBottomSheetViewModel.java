package com.discord.widgets.voice.sheet;

import andhook.lib.HookHelper;
import android.content.Intent;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.ViewModelKt;
import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.embeddedactivities.EmbeddedActivity;
import com.discord.models.guild.Guild;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreApplication;
import com.discord.stores.StoreApplicationStreamPreviews;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreCalls;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreEmbeddedActivities;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreMediaEngine;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettings;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.tooltips.TooltipManager;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.permissions.VideoPermissionsManager;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.voice.VoiceChannelJoinability;
import com.discord.utilities.voice.VoiceEngineServiceController;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.discord.widgets.voice.fullscreen.ParticipantsListItemGenerator;
import com.discord.widgets.voice.model.CallModel;
import com.discord.widgets.voice.model.CameraState;
import com.discord.widgets.voice.sheet.CallParticipantsAdapter;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheetViewModel;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func3;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetVoiceBottomSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000æ\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 w2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0004wxyzBÁ\u0001\u0012\n\u0010\b\u001a\u00060\u0006j\u0002`\u0007\u0012\u0006\u0010f\u001a\u00020\u0003\u0012\b\b\u0002\u0010D\u001a\u00020C\u0012\b\b\u0002\u0010a\u001a\u00020`\u0012\b\b\u0002\u0010A\u001a\u00020@\u0012\b\b\u0002\u0010l\u001a\u00020k\u0012\b\b\u0002\u0010W\u001a\u00020V\u0012\b\b\u0002\u0010Z\u001a\u00020Y\u0012\b\b\u0002\u0010G\u001a\u00020F\u0012\b\b\u0002\u0010;\u001a\u00020:\u0012\b\b\u0002\u0010r\u001a\u00020q\u0012\b\b\u0002\u0010d\u001a\u00020c\u0012\b\b\u0002\u0010>\u001a\u00020=\u0012\b\b\u0002\u00108\u001a\u000207\u0012\b\b\u0002\u0010S\u001a\u00020R\u0012\b\b\u0002\u0010P\u001a\u00020O\u0012\b\b\u0002\u0010i\u001a\u00020h\u0012\u000e\b\u0002\u0010t\u001a\b\u0012\u0004\u0012\u00020\f0\u0019¢\u0006\u0004\bu\u0010vJ\u000f\u0010\u0004\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u001b\u0010\n\u001a\u00020\t2\n\u0010\b\u001a\u00060\u0006j\u0002`\u0007H\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u000e\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\fH\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0012\u0010\u0011J\u000f\u0010\u0013\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0013\u0010\u0011J\u000f\u0010\u0014\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0014\u0010\u0011J\u000f\u0010\u0015\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0015\u0010\u0011J\u0011\u0010\u0017\u001a\u0004\u0018\u00010\u0016H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0013\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001e\u001a\u00020\t2\u0006\u0010\u001d\u001a\u00020\u0003H\u0007¢\u0006\u0004\b\u001e\u0010\u001fJ\u000f\u0010 \u001a\u00020\tH\u0007¢\u0006\u0004\b \u0010\u0011J\u000f\u0010!\u001a\u00020\tH\u0007¢\u0006\u0004\b!\u0010\u0011J\u000f\u0010\"\u001a\u00020\tH\u0007¢\u0006\u0004\b\"\u0010\u0011J\u000f\u0010#\u001a\u00020\tH\u0007¢\u0006\u0004\b#\u0010\u0011J\u000f\u0010$\u001a\u00020\tH\u0007¢\u0006\u0004\b$\u0010\u0011J\u000f\u0010%\u001a\u00020\tH\u0007¢\u0006\u0004\b%\u0010\u0011J\u000f\u0010&\u001a\u00020\tH\u0007¢\u0006\u0004\b&\u0010\u0011J\u0017\u0010)\u001a\u00020\t2\u0006\u0010(\u001a\u00020'H\u0007¢\u0006\u0004\b)\u0010*J\u000f\u0010+\u001a\u00020\tH\u0007¢\u0006\u0004\b+\u0010\u0011J\u0015\u0010.\u001a\u00020\t2\u0006\u0010-\u001a\u00020,¢\u0006\u0004\b.\u0010/J\u0017\u00101\u001a\u00020\t2\u0006\u00100\u001a\u00020\u0003H\u0007¢\u0006\u0004\b1\u0010\u001fJ\u001b\u00105\u001a\u00020\t2\n\u00104\u001a\u000602j\u0002`3H\u0007¢\u0006\u0004\b5\u00106R\u0016\u00108\u001a\u0002078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b8\u00109R\u0016\u0010;\u001a\u00020:8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b;\u0010<R\u0016\u0010>\u001a\u00020=8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b>\u0010?R\u0016\u0010A\u001a\u00020@8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010ER\u0016\u0010G\u001a\u00020F8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bG\u0010HR:\u0010K\u001a&\u0012\f\u0012\n J*\u0004\u0018\u00010\u001a0\u001a J*\u0012\u0012\f\u0012\n J*\u0004\u0018\u00010\u001a0\u001a\u0018\u00010I0I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bK\u0010LR\u0018\u0010M\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bM\u0010NR\u0016\u0010P\u001a\u00020O8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bP\u0010QR\u0016\u0010S\u001a\u00020R8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010TR\u001a\u0010\b\u001a\u00060\u0006j\u0002`\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010UR\u0016\u0010W\u001a\u00020V8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bW\u0010XR\u0016\u0010Z\u001a\u00020Y8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bZ\u0010[R\u0018\u0010\\\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\\\u0010]R\u0016\u0010^\u001a\u00020\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b^\u0010_R\u0016\u0010a\u001a\u00020`8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\ba\u0010bR\u0016\u0010d\u001a\u00020c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bd\u0010eR\u0016\u0010f\u001a\u00020\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bf\u0010_R\u0016\u0010g\u001a\u00020\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bg\u0010_R\u0016\u0010i\u001a\u00020h8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010jR\u0016\u0010l\u001a\u00020k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010mR \u0010o\u001a\f\u0012\b\u0012\u000602j\u0002`30n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bo\u0010pR\u0016\u0010r\u001a\u00020q8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\br\u0010s¨\u0006{"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;", "", "hasVideoPermission", "()Z", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "joinVoiceChannel", "(J)V", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;)V", "emitSuppressedDialogEvent", "()V", "emitServerMutedDialogEvent", "emitServerDeafenedDialogEvent", "emitShowNoVideoPermissionDialogEvent", "emitShowNoScreenSharePermissionDialogEvent", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEventToEnd", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "Lrx/Observable;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "observeEvents", "()Lrx/Observable;", "useVideo", "tryConnectToVoice", "(Z)V", "onMutePressed", "onDeafenPressed", "onNoiseCancellationPressed", "onCameraButtonPressed", "onCameraPermissionsGranted", "onDisconnectPressed", "onDisconnect", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "voiceUser", "onToggleRingingPressed", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V", "onScreenSharePressed", "Landroid/content/Intent;", "intent", "startStream", "(Landroid/content/Intent;)V", "pressed", "onPttPressed", "", "Lcom/discord/primitives/StreamKey;", "streamKey", "onStreamPreviewClicked", "(Ljava/lang/String;)V", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/stores/StoreGuilds;", "guildsStore", "Lcom/discord/stores/StoreGuilds;", "Lcom/discord/utilities/voice/VoiceEngineServiceController;", "voiceEngineServiceController", "Lcom/discord/utilities/voice/VoiceEngineServiceController;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "selectedVoiceChannelStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/stores/StoreCalls;", "callsStore", "Lcom/discord/stores/StoreCalls;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "mostRecentStoreState", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", "Lcom/discord/stores/StoreEmbeddedActivities;", "embeddedActivitiesStore", "Lcom/discord/stores/StoreEmbeddedActivities;", "Lcom/discord/tooltips/TooltipManager;", "tooltipManager", "Lcom/discord/tooltips/TooltipManager;", "J", "Lcom/discord/stores/StoreMediaEngine;", "mediaEngineStore", "Lcom/discord/stores/StoreMediaEngine;", "Lcom/discord/stores/StoreUserSettings;", "userSettingsStore", "Lcom/discord/stores/StoreUserSettings;", "wasConnected", "Ljava/lang/Boolean;", "wasEverConnected", "Z", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "storeApplicationStreamPreviews", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "forwardToFullscreenIfVideoActivated", "wasEverMultiParticipant", "Lcom/discord/stores/StoreApplication;", "applicationStore", "Lcom/discord/stores/StoreApplication;", "Lcom/discord/stores/StoreMediaSettings;", "mediaSettingsStore", "Lcom/discord/stores/StoreMediaSettings;", "", "fetchedPreviews", "Ljava/util/Set;", "Lcom/discord/utilities/permissions/VideoPermissionsManager;", "videoPermissionsManager", "Lcom/discord/utilities/permissions/VideoPermissionsManager;", "storeStateObservable", HookHelper.constructorName, "(JZLcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreCalls;Lcom/discord/stores/StoreGuilds;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;Lcom/discord/stores/StoreEmbeddedActivities;Lcom/discord/stores/StoreApplication;Lrx/Observable;)V", "Companion", "Event", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceBottomSheetViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final StoreApplication applicationStore;
    private final StoreCalls callsStore;
    private final long channelId;
    private final StoreChannels channelStore;
    private final Clock clock;
    private final StoreEmbeddedActivities embeddedActivitiesStore;
    private final PublishSubject<Event> eventSubject;
    private Set<String> fetchedPreviews;
    private final boolean forwardToFullscreenIfVideoActivated;
    private final StoreGuilds guildsStore;
    private final StoreMediaEngine mediaEngineStore;
    private final StoreMediaSettings mediaSettingsStore;
    private StoreState mostRecentStoreState;
    private final StorePermissions permissionsStore;
    private final StoreVoiceChannelSelected selectedVoiceChannelStore;
    private final StoreApplicationStreamPreviews storeApplicationStreamPreviews;
    private final TooltipManager tooltipManager;
    private final StoreUserSettings userSettingsStore;
    private final VideoPermissionsManager videoPermissionsManager;
    private final VoiceEngineServiceController voiceEngineServiceController;
    private Boolean wasConnected;
    private boolean wasEverConnected;
    private boolean wasEverMultiParticipant;

    /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", "storeState", "", "invoke", "(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheetViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            m.checkNotNullParameter(storeState, "storeState");
            WidgetVoiceBottomSheetViewModel.this.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ1\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StoreChannels;", "channelsStore", "Lrx/Observable;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreChannels;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(final long j, final StorePermissions storePermissions, StoreChannels storeChannels) {
            Observable Y = storeChannels.observeChannel(j).Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheetViewModel$Companion$observeStoreState$1

                /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
                @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u000b\u001a\n \u0006*\u0004\u0018\u00010\b0\b2\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u000e\u0010\u0007\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\n¢\u0006\u0004\b\t\u0010\n"}, d2 = {"Lcom/discord/widgets/voice/model/CallModel;", "callModel", "", "Lcom/discord/api/permission/PermissionBit;", "myPermissions", "", "kotlin.jvm.PlatformType", "textInVoiceEnabled", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
                /* renamed from: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheetViewModel$Companion$observeStoreState$1$1  reason: invalid class name */
                /* loaded from: classes2.dex */
                public static final class AnonymousClass1<T1, T2, T3, R> implements Func3<CallModel, Long, Boolean, WidgetVoiceBottomSheetViewModel.StoreState> {
                    public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                    public final WidgetVoiceBottomSheetViewModel.StoreState call(CallModel callModel, Long l, Boolean bool) {
                        if (callModel != null) {
                            boolean z2 = false;
                            if (n0.setOf((Object[]) new Integer[]{2, 13, 1, 3}).contains(Integer.valueOf(callModel.getChannel().A()))) {
                                if (callModel.getVoiceSettings().getNoiseProcessing() == StoreMediaSettings.NoiseProcessing.Cancellation) {
                                    z2 = true;
                                }
                                Boolean valueOf = Boolean.valueOf(z2);
                                m.checkNotNullExpressionValue(bool, "textInVoiceEnabled");
                                return new WidgetVoiceBottomSheetViewModel.StoreState.Valid(callModel, valueOf, l, bool.booleanValue());
                            }
                        }
                        return WidgetVoiceBottomSheetViewModel.StoreState.Invalid.INSTANCE;
                    }
                }

                public final Observable<? extends WidgetVoiceBottomSheetViewModel.StoreState> call(Channel channel) {
                    return Observable.i(CallModel.Companion.get(j), storePermissions.observePermissionsForChannel(j), TextInVoiceFeatureFlag.Companion.getINSTANCE().observeEnabled(channel != null ? Long.valueOf(channel.f()) : null), AnonymousClass1.INSTANCE);
                }
            });
            m.checkNotNullExpressionValue(Y, "channelsStore.observeCha…      }\n        }\n      }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0014\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0014\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&'()*+¨\u0006,"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "", HookHelper.constructorName, "()V", "AccessibilityAnnouncement", "Dismiss", "EnqueueCallFeedbackSheet", "ExpandSheet", "LaunchStageChannel", "LaunchVideoCall", "RequestStartStream", "ShowCameraCapacityDialog", "ShowEventEnd", "ShowGuildVideoAtCapacityDialog", "ShowNoScreenSharePermissionDialog", "ShowNoVideoDevicesAvailableToast", "ShowNoVideoPermissionDialog", "ShowNoiseCancellationBottomSheet", "ShowOverlayNux", "ShowRequestCameraPermissionsDialog", "ShowServerDeafenedDialog", "ShowServerMutedDialog", "ShowSuppressedDialog", "ShowToast", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowSuppressedDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerMutedDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerDeafenedDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoPermissionDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoDevicesAvailableToast;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoScreenSharePermissionDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowRequestCameraPermissionsDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowOverlayNux;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoiseCancellationBottomSheet;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchStageChannel;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Dismiss;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$EnqueueCallFeedbackSheet;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowEventEnd;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$RequestStartStream;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ExpandSheet;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "", "component1", "()I", "messageResId", "copy", "(I)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMessageResId", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AccessibilityAnnouncement extends Event {
            private final int messageResId;

            public AccessibilityAnnouncement(@StringRes int i) {
                super(null);
                this.messageResId = i;
            }

            public static /* synthetic */ AccessibilityAnnouncement copy$default(AccessibilityAnnouncement accessibilityAnnouncement, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = accessibilityAnnouncement.messageResId;
                }
                return accessibilityAnnouncement.copy(i);
            }

            public final int component1() {
                return this.messageResId;
            }

            public final AccessibilityAnnouncement copy(@StringRes int i) {
                return new AccessibilityAnnouncement(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof AccessibilityAnnouncement) && this.messageResId == ((AccessibilityAnnouncement) obj).messageResId;
                }
                return true;
            }

            public final int getMessageResId() {
                return this.messageResId;
            }

            public int hashCode() {
                return this.messageResId;
            }

            public String toString() {
                return a.A(a.R("AccessibilityAnnouncement(messageResId="), this.messageResId, ")");
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Dismiss;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Dismiss extends Event {
            public static final Dismiss INSTANCE = new Dismiss();

            private Dismiss() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\u0011\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007\u0012\u000e\u0010\u0012\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\r¢\u0006\u0004\b&\u0010'J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0018\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJV\u0010\u0015\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\u0010\b\u0002\u0010\u0012\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n2\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\rHÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0017\u0010\tJ\u0010\u0010\u0018\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u0018\u0010\u000fJ\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u001e\u001a\u0004\b\u001f\u0010\u0005R\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b \u0010\u0005R\u0019\u0010\u0014\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010!\u001a\u0004\b\"\u0010\u000fR!\u0010\u0011\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b$\u0010\tR!\u0010\u0012\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b%\u0010\t¨\u0006("}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$EnqueueCallFeedbackSheet;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "Lcom/discord/primitives/RtcConnectionId;", "component2", "()Ljava/lang/String;", "Lcom/discord/primitives/MediaSessionId;", "component3", "component4", "", "component5", "()I", "channelId", "rtcConnectionId", "mediaSessionId", "callDuration", "triggerRateDenominator", "copy", "(JLjava/lang/String;Ljava/lang/String;JI)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$EnqueueCallFeedbackSheet;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getCallDuration", "getChannelId", "I", "getTriggerRateDenominator", "Ljava/lang/String;", "getRtcConnectionId", "getMediaSessionId", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;JI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EnqueueCallFeedbackSheet extends Event {
            private final long callDuration;
            private final long channelId;
            private final String mediaSessionId;
            private final String rtcConnectionId;
            private final int triggerRateDenominator;

            public EnqueueCallFeedbackSheet(long j, String str, String str2, long j2, int i) {
                super(null);
                this.channelId = j;
                this.rtcConnectionId = str;
                this.mediaSessionId = str2;
                this.callDuration = j2;
                this.triggerRateDenominator = i;
            }

            public final long component1() {
                return this.channelId;
            }

            public final String component2() {
                return this.rtcConnectionId;
            }

            public final String component3() {
                return this.mediaSessionId;
            }

            public final long component4() {
                return this.callDuration;
            }

            public final int component5() {
                return this.triggerRateDenominator;
            }

            public final EnqueueCallFeedbackSheet copy(long j, String str, String str2, long j2, int i) {
                return new EnqueueCallFeedbackSheet(j, str, str2, j2, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EnqueueCallFeedbackSheet)) {
                    return false;
                }
                EnqueueCallFeedbackSheet enqueueCallFeedbackSheet = (EnqueueCallFeedbackSheet) obj;
                return this.channelId == enqueueCallFeedbackSheet.channelId && m.areEqual(this.rtcConnectionId, enqueueCallFeedbackSheet.rtcConnectionId) && m.areEqual(this.mediaSessionId, enqueueCallFeedbackSheet.mediaSessionId) && this.callDuration == enqueueCallFeedbackSheet.callDuration && this.triggerRateDenominator == enqueueCallFeedbackSheet.triggerRateDenominator;
            }

            public final long getCallDuration() {
                return this.callDuration;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final String getMediaSessionId() {
                return this.mediaSessionId;
            }

            public final String getRtcConnectionId() {
                return this.rtcConnectionId;
            }

            public final int getTriggerRateDenominator() {
                return this.triggerRateDenominator;
            }

            public int hashCode() {
                int a = a0.a.a.b.a(this.channelId) * 31;
                String str = this.rtcConnectionId;
                int i = 0;
                int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.mediaSessionId;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return ((a0.a.a.b.a(this.callDuration) + ((hashCode + i) * 31)) * 31) + this.triggerRateDenominator;
            }

            public String toString() {
                StringBuilder R = a.R("EnqueueCallFeedbackSheet(channelId=");
                R.append(this.channelId);
                R.append(", rtcConnectionId=");
                R.append(this.rtcConnectionId);
                R.append(", mediaSessionId=");
                R.append(this.mediaSessionId);
                R.append(", callDuration=");
                R.append(this.callDuration);
                R.append(", triggerRateDenominator=");
                return a.A(R, this.triggerRateDenominator, ")");
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ExpandSheet;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ExpandSheet extends Event {
            public static final ExpandSheet INSTANCE = new ExpandSheet();

            private ExpandSheet() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0006\u001a\u00060\u0002j\u0002`\u0003HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchStageChannel;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "channelId", "copy", "(J)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchStageChannel;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", HookHelper.constructorName, "(J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchStageChannel extends Event {
            private final long channelId;

            public LaunchStageChannel(long j) {
                super(null);
                this.channelId = j;
            }

            public static /* synthetic */ LaunchStageChannel copy$default(LaunchStageChannel launchStageChannel, long j, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchStageChannel.channelId;
                }
                return launchStageChannel.copy(j);
            }

            public final long component1() {
                return this.channelId;
            }

            public final LaunchStageChannel copy(long j) {
                return new LaunchStageChannel(j);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof LaunchStageChannel) && this.channelId == ((LaunchStageChannel) obj).channelId;
                }
                return true;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public int hashCode() {
                return a0.a.a.b.a(this.channelId);
            }

            public String toString() {
                return a.B(a.R("LaunchStageChannel(channelId="), this.channelId, ")");
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\n\u0010\n\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ0\u0010\f\u001a\u00020\u00002\f\b\u0002\u0010\n\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u000e\u0010\tJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\n\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R!\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\t¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "Lcom/discord/primitives/StreamKey;", "component2", "()Ljava/lang/String;", "channelId", "autoTargetStreamKey", "copy", "(JLjava/lang/String;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Ljava/lang/String;", "getAutoTargetStreamKey", HookHelper.constructorName, "(JLjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchVideoCall extends Event {
            private final String autoTargetStreamKey;
            private final long channelId;

            public LaunchVideoCall(long j, String str) {
                super(null);
                this.channelId = j;
                this.autoTargetStreamKey = str;
            }

            public static /* synthetic */ LaunchVideoCall copy$default(LaunchVideoCall launchVideoCall, long j, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchVideoCall.channelId;
                }
                if ((i & 2) != 0) {
                    str = launchVideoCall.autoTargetStreamKey;
                }
                return launchVideoCall.copy(j, str);
            }

            public final long component1() {
                return this.channelId;
            }

            public final String component2() {
                return this.autoTargetStreamKey;
            }

            public final LaunchVideoCall copy(long j, String str) {
                return new LaunchVideoCall(j, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof LaunchVideoCall)) {
                    return false;
                }
                LaunchVideoCall launchVideoCall = (LaunchVideoCall) obj;
                return this.channelId == launchVideoCall.channelId && m.areEqual(this.autoTargetStreamKey, launchVideoCall.autoTargetStreamKey);
            }

            public final String getAutoTargetStreamKey() {
                return this.autoTargetStreamKey;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public int hashCode() {
                int a = a0.a.a.b.a(this.channelId) * 31;
                String str = this.autoTargetStreamKey;
                return a + (str != null ? str.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("LaunchVideoCall(channelId=");
                R.append(this.channelId);
                R.append(", autoTargetStreamKey=");
                return a.H(R, this.autoTargetStreamKey, ")");
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$RequestStartStream;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class RequestStartStream extends Event {
            public static final RequestStartStream INSTANCE = new RequestStartStream();

            private RequestStartStream() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "", "component1", "()I", "guildMaxVideoChannelUsers", "copy", "(I)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getGuildMaxVideoChannelUsers", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowCameraCapacityDialog extends Event {
            private final int guildMaxVideoChannelUsers;

            public ShowCameraCapacityDialog(int i) {
                super(null);
                this.guildMaxVideoChannelUsers = i;
            }

            public static /* synthetic */ ShowCameraCapacityDialog copy$default(ShowCameraCapacityDialog showCameraCapacityDialog, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showCameraCapacityDialog.guildMaxVideoChannelUsers;
                }
                return showCameraCapacityDialog.copy(i);
            }

            public final int component1() {
                return this.guildMaxVideoChannelUsers;
            }

            public final ShowCameraCapacityDialog copy(int i) {
                return new ShowCameraCapacityDialog(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowCameraCapacityDialog) && this.guildMaxVideoChannelUsers == ((ShowCameraCapacityDialog) obj).guildMaxVideoChannelUsers;
                }
                return true;
            }

            public final int getGuildMaxVideoChannelUsers() {
                return this.guildMaxVideoChannelUsers;
            }

            public int hashCode() {
                return this.guildMaxVideoChannelUsers;
            }

            public String toString() {
                return a.A(a.R("ShowCameraCapacityDialog(guildMaxVideoChannelUsers="), this.guildMaxVideoChannelUsers, ")");
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004¨\u0006\u0017"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowEventEnd;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component1", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvent", "copy", "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowEventEnd;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getGuildScheduledEvent", HookHelper.constructorName, "(Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowEventEnd extends Event {
            private final GuildScheduledEvent guildScheduledEvent;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public ShowEventEnd(GuildScheduledEvent guildScheduledEvent) {
                super(null);
                m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
                this.guildScheduledEvent = guildScheduledEvent;
            }

            public static /* synthetic */ ShowEventEnd copy$default(ShowEventEnd showEventEnd, GuildScheduledEvent guildScheduledEvent, int i, Object obj) {
                if ((i & 1) != 0) {
                    guildScheduledEvent = showEventEnd.guildScheduledEvent;
                }
                return showEventEnd.copy(guildScheduledEvent);
            }

            public final GuildScheduledEvent component1() {
                return this.guildScheduledEvent;
            }

            public final ShowEventEnd copy(GuildScheduledEvent guildScheduledEvent) {
                m.checkNotNullParameter(guildScheduledEvent, "guildScheduledEvent");
                return new ShowEventEnd(guildScheduledEvent);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowEventEnd) && m.areEqual(this.guildScheduledEvent, ((ShowEventEnd) obj).guildScheduledEvent);
                }
                return true;
            }

            public final GuildScheduledEvent getGuildScheduledEvent() {
                return this.guildScheduledEvent;
            }

            public int hashCode() {
                GuildScheduledEvent guildScheduledEvent = this.guildScheduledEvent;
                if (guildScheduledEvent != null) {
                    return guildScheduledEvent.hashCode();
                }
                return 0;
            }

            public String toString() {
                StringBuilder R = a.R("ShowEventEnd(guildScheduledEvent=");
                R.append(this.guildScheduledEvent);
                R.append(")");
                return R.toString();
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowGuildVideoAtCapacityDialog extends Event {
            public static final ShowGuildVideoAtCapacityDialog INSTANCE = new ShowGuildVideoAtCapacityDialog();

            private ShowGuildVideoAtCapacityDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoScreenSharePermissionDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowNoScreenSharePermissionDialog extends Event {
            public static final ShowNoScreenSharePermissionDialog INSTANCE = new ShowNoScreenSharePermissionDialog();

            private ShowNoScreenSharePermissionDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoDevicesAvailableToast;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowNoVideoDevicesAvailableToast extends Event {
            public static final ShowNoVideoDevicesAvailableToast INSTANCE = new ShowNoVideoDevicesAvailableToast();

            private ShowNoVideoDevicesAvailableToast() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoPermissionDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowNoVideoPermissionDialog extends Event {
            public static final ShowNoVideoPermissionDialog INSTANCE = new ShowNoVideoPermissionDialog();

            private ShowNoVideoPermissionDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoiseCancellationBottomSheet;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowNoiseCancellationBottomSheet extends Event {
            public static final ShowNoiseCancellationBottomSheet INSTANCE = new ShowNoiseCancellationBottomSheet();

            private ShowNoiseCancellationBottomSheet() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowOverlayNux;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowOverlayNux extends Event {
            public static final ShowOverlayNux INSTANCE = new ShowOverlayNux();

            private ShowOverlayNux() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowRequestCameraPermissionsDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowRequestCameraPermissionsDialog extends Event {
            public static final ShowRequestCameraPermissionsDialog INSTANCE = new ShowRequestCameraPermissionsDialog();

            private ShowRequestCameraPermissionsDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerDeafenedDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowServerDeafenedDialog extends Event {
            public static final ShowServerDeafenedDialog INSTANCE = new ShowServerDeafenedDialog();

            private ShowServerDeafenedDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerMutedDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowServerMutedDialog extends Event {
            public static final ShowServerMutedDialog INSTANCE = new ShowServerMutedDialog();

            private ShowServerMutedDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowSuppressedDialog;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowSuppressedDialog extends Event {
            public static final ShowSuppressedDialog INSTANCE = new ShowSuppressedDialog();

            private ShowSuppressedDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;", "", "component1", "()I", "toastResId", "copy", "(I)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getToastResId", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowToast extends Event {
            private final int toastResId;

            public ShowToast(@StringRes int i) {
                super(null);
                this.toastResId = i;
            }

            public static /* synthetic */ ShowToast copy$default(ShowToast showToast, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showToast.toastResId;
                }
                return showToast.copy(i);
            }

            public final int component1() {
                return this.toastResId;
            }

            public final ShowToast copy(@StringRes int i) {
                return new ShowToast(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowToast) && this.toastResId == ((ShowToast) obj).toastResId;
                }
                return true;
            }

            public final int getToastResId() {
                return this.toastResId;
            }

            public int hashCode() {
                return this.toastResId;
            }

            public String toString() {
                return a.A(a.R("ShowToast(toastResId="), this.toastResId, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Invalid;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Invalid;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends StoreState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u000e\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t\u0012\u0006\u0010\u0011\u001a\u00020\u0005¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\f\u0010\rJB\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00052\u0010\b\u0002\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\b\b\u0002\u0010\u0011\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u0014HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u00052\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR!\u0010\u0010\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\u000bR\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010 \u001a\u0004\b!\u0010\u0004R\u001b\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\"\u001a\u0004\b#\u0010\u0007R\u0019\u0010\u0011\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010$\u001a\u0004\b%\u0010\r¨\u0006("}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;", "Lcom/discord/widgets/voice/model/CallModel;", "component1", "()Lcom/discord/widgets/voice/model/CallModel;", "", "component2", "()Ljava/lang/Boolean;", "", "Lcom/discord/api/permission/PermissionBit;", "component3", "()Ljava/lang/Long;", "component4", "()Z", "callModel", "noiseCancellation", "myPermissions", "textInVoiceEnabled", "copy", "(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;Z)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getMyPermissions", "Lcom/discord/widgets/voice/model/CallModel;", "getCallModel", "Ljava/lang/Boolean;", "getNoiseCancellation", "Z", "getTextInVoiceEnabled", HookHelper.constructorName, "(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends StoreState {
            private final CallModel callModel;
            private final Long myPermissions;
            private final Boolean noiseCancellation;
            private final boolean textInVoiceEnabled;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Valid(CallModel callModel, Boolean bool, Long l, boolean z2) {
                super(null);
                m.checkNotNullParameter(callModel, "callModel");
                this.callModel = callModel;
                this.noiseCancellation = bool;
                this.myPermissions = l;
                this.textInVoiceEnabled = z2;
            }

            public static /* synthetic */ Valid copy$default(Valid valid, CallModel callModel, Boolean bool, Long l, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    callModel = valid.callModel;
                }
                if ((i & 2) != 0) {
                    bool = valid.noiseCancellation;
                }
                if ((i & 4) != 0) {
                    l = valid.myPermissions;
                }
                if ((i & 8) != 0) {
                    z2 = valid.textInVoiceEnabled;
                }
                return valid.copy(callModel, bool, l, z2);
            }

            public final CallModel component1() {
                return this.callModel;
            }

            public final Boolean component2() {
                return this.noiseCancellation;
            }

            public final Long component3() {
                return this.myPermissions;
            }

            public final boolean component4() {
                return this.textInVoiceEnabled;
            }

            public final Valid copy(CallModel callModel, Boolean bool, Long l, boolean z2) {
                m.checkNotNullParameter(callModel, "callModel");
                return new Valid(callModel, bool, l, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.callModel, valid.callModel) && m.areEqual(this.noiseCancellation, valid.noiseCancellation) && m.areEqual(this.myPermissions, valid.myPermissions) && this.textInVoiceEnabled == valid.textInVoiceEnabled;
            }

            public final CallModel getCallModel() {
                return this.callModel;
            }

            public final Long getMyPermissions() {
                return this.myPermissions;
            }

            public final Boolean getNoiseCancellation() {
                return this.noiseCancellation;
            }

            public final boolean getTextInVoiceEnabled() {
                return this.textInVoiceEnabled;
            }

            public int hashCode() {
                CallModel callModel = this.callModel;
                int i = 0;
                int hashCode = (callModel != null ? callModel.hashCode() : 0) * 31;
                Boolean bool = this.noiseCancellation;
                int hashCode2 = (hashCode + (bool != null ? bool.hashCode() : 0)) * 31;
                Long l = this.myPermissions;
                if (l != null) {
                    i = l.hashCode();
                }
                int i2 = (hashCode2 + i) * 31;
                boolean z2 = this.textInVoiceEnabled;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                return i2 + i3;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(callModel=");
                R.append(this.callModel);
                R.append(", noiseCancellation=");
                R.append(this.noiseCancellation);
                R.append(", myPermissions=");
                R.append(this.myPermissions);
                R.append(", textInVoiceEnabled=");
                return a.M(R, this.textInVoiceEnabled, ")");
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetVoiceBottomSheetViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0017\u001a\u00020\u0006\u0012\u0006\u0010\u0018\u001a\u00020\t\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\u001a\u001a\u00020\t\u0012\u0006\u0010\u001b\u001a\u00020\u000f\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0012¢\u0006\u0004\b3\u00104J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\f\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000bJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014Jf\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0015\u001a\u00020\u00022\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u0017\u001a\u00020\u00062\b\b\u0002\u0010\u0018\u001a\u00020\t2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\u001a\u001a\u00020\t2\b\b\u0002\u0010\u001b\u001a\u00020\u000f2\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0012HÆ\u0001¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u001f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001f\u0010\u0004J\u0010\u0010!\u001a\u00020 HÖ\u0001¢\u0006\u0004\b!\u0010\"J\u001a\u0010%\u001a\u00020\t2\b\u0010$\u001a\u0004\u0018\u00010#HÖ\u0003¢\u0006\u0004\b%\u0010&R\u001e\u0010\u001c\u001a\u0004\u0018\u00010\u00128\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010'\u001a\u0004\b(\u0010\u0014R\u001c\u0010\u0017\u001a\u00020\u00068\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b*\u0010\bR\u001e\u0010\u0019\u001a\u0004\u0018\u00010\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0019\u0010+\u001a\u0004\b\u0019\u0010\rR\u001e\u0010\u0016\u001a\u0004\u0018\u00010\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0016\u0010,\u001a\u0004\b-\u0010\u0004R\u001c\u0010\u0015\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0015\u0010,\u001a\u0004\b.\u0010\u0004R\u001c\u0010\u001a\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001a\u0010/\u001a\u0004\b\u001a\u0010\u000bR\u001c\u0010\u0018\u001a\u00020\t8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u0018\u0010/\u001a\u0004\b0\u0010\u000bR\u001c\u0010\u001b\u001a\u00020\u000f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u00101\u001a\u0004\b2\u0010\u0011¨\u00065"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$ViewState;", "", "component1", "()Ljava/lang/String;", "component2", "Lcom/discord/api/channel/Channel;", "component3", "()Lcom/discord/api/channel/Channel;", "", "component4", "()Z", "component5", "()Ljava/lang/Boolean;", "component6", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "component7", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "component8", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "title", "subtitle", "channel", "showInviteOption", "isNoiseCancellationActive", "isDeafened", "centerContent", "bottomContent", "copy", "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/channel/Channel;ZLjava/lang/Boolean;ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;", "getBottomContent", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/lang/Boolean;", "Ljava/lang/String;", "getSubtitle", "getTitle", "Z", "getShowInviteOption", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;", "getCenterContent", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Lcom/discord/api/channel/Channel;ZLjava/lang/Boolean;ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState implements WidgetVoiceBottomSheet.ViewState {
        private final WidgetVoiceBottomSheet.BottomContent bottomContent;
        private final WidgetVoiceBottomSheet.CenterContent centerContent;
        private final Channel channel;
        private final boolean isDeafened;
        private final Boolean isNoiseCancellationActive;
        private final boolean showInviteOption;
        private final String subtitle;
        private final String title;

        public ViewState(String str, String str2, Channel channel, boolean z2, Boolean bool, boolean z3, WidgetVoiceBottomSheet.CenterContent centerContent, WidgetVoiceBottomSheet.BottomContent bottomContent) {
            m.checkNotNullParameter(str, "title");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(centerContent, "centerContent");
            this.title = str;
            this.subtitle = str2;
            this.channel = channel;
            this.showInviteOption = z2;
            this.isNoiseCancellationActive = bool;
            this.isDeafened = z3;
            this.centerContent = centerContent;
            this.bottomContent = bottomContent;
        }

        public final String component1() {
            return getTitle();
        }

        public final String component2() {
            return getSubtitle();
        }

        public final Channel component3() {
            return getChannel();
        }

        public final boolean component4() {
            return getShowInviteOption();
        }

        public final Boolean component5() {
            return isNoiseCancellationActive();
        }

        public final boolean component6() {
            return isDeafened();
        }

        public final WidgetVoiceBottomSheet.CenterContent component7() {
            return getCenterContent();
        }

        public final WidgetVoiceBottomSheet.BottomContent component8() {
            return getBottomContent();
        }

        public final ViewState copy(String str, String str2, Channel channel, boolean z2, Boolean bool, boolean z3, WidgetVoiceBottomSheet.CenterContent centerContent, WidgetVoiceBottomSheet.BottomContent bottomContent) {
            m.checkNotNullParameter(str, "title");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(centerContent, "centerContent");
            return new ViewState(str, str2, channel, z2, bool, z3, centerContent, bottomContent);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(getTitle(), viewState.getTitle()) && m.areEqual(getSubtitle(), viewState.getSubtitle()) && m.areEqual(getChannel(), viewState.getChannel()) && getShowInviteOption() == viewState.getShowInviteOption() && m.areEqual(isNoiseCancellationActive(), viewState.isNoiseCancellationActive()) && isDeafened() == viewState.isDeafened() && m.areEqual(getCenterContent(), viewState.getCenterContent()) && m.areEqual(getBottomContent(), viewState.getBottomContent());
        }

        @Override // com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.ViewState
        public WidgetVoiceBottomSheet.BottomContent getBottomContent() {
            return this.bottomContent;
        }

        @Override // com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.ViewState
        public WidgetVoiceBottomSheet.CenterContent getCenterContent() {
            return this.centerContent;
        }

        @Override // com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.ViewState
        public Channel getChannel() {
            return this.channel;
        }

        @Override // com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.ViewState
        public boolean getShowInviteOption() {
            return this.showInviteOption;
        }

        @Override // com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.ViewState
        public String getSubtitle() {
            return this.subtitle;
        }

        @Override // com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.ViewState
        public String getTitle() {
            return this.title;
        }

        public int hashCode() {
            String title = getTitle();
            int i = 0;
            int hashCode = (title != null ? title.hashCode() : 0) * 31;
            String subtitle = getSubtitle();
            int hashCode2 = (hashCode + (subtitle != null ? subtitle.hashCode() : 0)) * 31;
            Channel channel = getChannel();
            int hashCode3 = (hashCode2 + (channel != null ? channel.hashCode() : 0)) * 31;
            boolean showInviteOption = getShowInviteOption();
            int i2 = 1;
            if (showInviteOption) {
                showInviteOption = true;
            }
            int i3 = showInviteOption ? 1 : 0;
            int i4 = showInviteOption ? 1 : 0;
            int i5 = (hashCode3 + i3) * 31;
            Boolean isNoiseCancellationActive = isNoiseCancellationActive();
            int hashCode4 = (i5 + (isNoiseCancellationActive != null ? isNoiseCancellationActive.hashCode() : 0)) * 31;
            boolean isDeafened = isDeafened();
            if (!isDeafened) {
                i2 = isDeafened;
            }
            int i6 = (hashCode4 + i2) * 31;
            WidgetVoiceBottomSheet.CenterContent centerContent = getCenterContent();
            int hashCode5 = (i6 + (centerContent != null ? centerContent.hashCode() : 0)) * 31;
            WidgetVoiceBottomSheet.BottomContent bottomContent = getBottomContent();
            if (bottomContent != null) {
                i = bottomContent.hashCode();
            }
            return hashCode5 + i;
        }

        @Override // com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.ViewState
        public boolean isDeafened() {
            return this.isDeafened;
        }

        @Override // com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.ViewState
        public Boolean isNoiseCancellationActive() {
            return this.isNoiseCancellationActive;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(title=");
            R.append(getTitle());
            R.append(", subtitle=");
            R.append(getSubtitle());
            R.append(", channel=");
            R.append(getChannel());
            R.append(", showInviteOption=");
            R.append(getShowInviteOption());
            R.append(", isNoiseCancellationActive=");
            R.append(isNoiseCancellationActive());
            R.append(", isDeafened=");
            R.append(isDeafened());
            R.append(", centerContent=");
            R.append(getCenterContent());
            R.append(", bottomContent=");
            R.append(getBottomContent());
            R.append(")");
            return R.toString();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            StoreMediaSettings.SelfMuteFailure.values();
            int[] iArr = new int[1];
            $EnumSwitchMapping$0 = iArr;
            iArr[StoreMediaSettings.SelfMuteFailure.CANNOT_USE_VAD.ordinal()] = 1;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public WidgetVoiceBottomSheetViewModel(long r23, boolean r25, com.discord.stores.StoreChannels r26, com.discord.stores.StorePermissions r27, com.discord.stores.StoreVoiceChannelSelected r28, com.discord.stores.StoreMediaSettings r29, com.discord.stores.StoreMediaEngine r30, com.discord.stores.StoreUserSettings r31, com.discord.stores.StoreCalls r32, com.discord.stores.StoreGuilds r33, com.discord.utilities.permissions.VideoPermissionsManager r34, com.discord.stores.StoreApplicationStreamPreviews r35, com.discord.utilities.voice.VoiceEngineServiceController r36, com.discord.utilities.time.Clock r37, com.discord.tooltips.TooltipManager r38, com.discord.stores.StoreEmbeddedActivities r39, com.discord.stores.StoreApplication r40, rx.Observable r41, int r42, kotlin.jvm.internal.DefaultConstructorMarker r43) {
        /*
            Method dump skipped, instructions count: 341
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.sheet.WidgetVoiceBottomSheetViewModel.<init>(long, boolean, com.discord.stores.StoreChannels, com.discord.stores.StorePermissions, com.discord.stores.StoreVoiceChannelSelected, com.discord.stores.StoreMediaSettings, com.discord.stores.StoreMediaEngine, com.discord.stores.StoreUserSettings, com.discord.stores.StoreCalls, com.discord.stores.StoreGuilds, com.discord.utilities.permissions.VideoPermissionsManager, com.discord.stores.StoreApplicationStreamPreviews, com.discord.utilities.voice.VoiceEngineServiceController, com.discord.utilities.time.Clock, com.discord.tooltips.TooltipManager, com.discord.stores.StoreEmbeddedActivities, com.discord.stores.StoreApplication, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final void emitServerDeafenedDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowServerDeafenedDialog.INSTANCE);
    }

    private final void emitServerMutedDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowServerMutedDialog.INSTANCE);
    }

    private final void emitShowNoScreenSharePermissionDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowNoScreenSharePermissionDialog.INSTANCE);
    }

    private final void emitShowNoVideoPermissionDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowNoVideoPermissionDialog.INSTANCE);
    }

    private final void emitSuppressedDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowSuppressedDialog.INSTANCE);
    }

    private final GuildScheduledEvent getGuildScheduledEventToEnd() {
        StoreState storeState = this.mostRecentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            return null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        return GuildScheduledEventUtilities.Companion.getGuildScheduledEventToEndForCall(valid.getCallModel(), valid.getMyPermissions(), this.guildsStore);
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void handleStoreState(StoreState storeState) {
        WidgetVoiceBottomSheet.CenterContent centerContent;
        WidgetVoiceBottomSheet.BottomContent.Connect connect;
        ModelApplicationStream stream;
        this.mostRecentStoreState = storeState;
        if (storeState instanceof StoreState.Valid) {
            StoreState.Valid valid = (StoreState.Valid) storeState;
            boolean z2 = valid.getCallModel().isConnected() && this.forwardToFullscreenIfVideoActivated && (valid.getCallModel().isVideoCall() || valid.getTextInVoiceEnabled());
            if (valid.getCallModel().isConnected() && ChannelUtils.z(valid.getCallModel().getChannel())) {
                this.eventSubject.k.onNext(new Event.LaunchStageChannel(valid.getCallModel().getChannel().h()));
            }
            String str = null;
            if (z2) {
                this.eventSubject.k.onNext(new Event.LaunchVideoCall(valid.getCallModel().getChannel().h(), null));
                return;
            }
            Channel channel = valid.getCallModel().getChannel();
            String c = ChannelUtils.c(channel);
            Guild guild = valid.getCallModel().getGuild();
            String name = guild != null ? guild.getName() : null;
            StoreApplicationStreaming.ActiveApplicationStream activeStream = valid.getCallModel().getActiveStream();
            if (!(activeStream == null || (stream = activeStream.getStream()) == null)) {
                str = stream.getEncodedStreamKey();
            }
            String str2 = str;
            Map<Long, EmbeddedActivity> embeddedActivitiesForChannel = valid.getCallModel().getEmbeddedActivitiesForChannel();
            List<CallParticipantsAdapter.ListItem> createConnectedListItems = ParticipantsListItemGenerator.Companion.createConnectedListItems(valid.getCallModel().getParticipants(), str2, channel, valid.getCallModel(), u.toList(embeddedActivitiesForChannel.values()), valid.getCallModel().getApplications());
            for (Long l : embeddedActivitiesForChannel.keySet()) {
                this.applicationStore.fetchIfNonexisting(l.longValue());
            }
            if (ParticipantsListItemGenerator.Companion.refreshStreams(createConnectedListItems, this.fetchedPreviews, this.storeApplicationStreamPreviews)) {
                centerContent = new WidgetVoiceBottomSheet.CenterContent.ListItems(createConnectedListItems);
            } else {
                centerContent = new WidgetVoiceBottomSheet.CenterContent.Empty(createConnectedListItems);
            }
            WidgetVoiceBottomSheet.CenterContent centerContent2 = centerContent;
            boolean isConnected = valid.getCallModel().isConnected();
            this.wasEverConnected = this.wasEverConnected || isConnected;
            boolean z3 = valid.getCallModel().getVoiceChannelJoinability() != VoiceChannelJoinability.PERMISSIONS_MISSING;
            Boolean bool = this.wasConnected;
            if (bool != null && !bool.booleanValue() && isConnected && valid.getCallModel().isSuppressed()) {
                emitSuppressedDialogEvent();
            }
            this.wasConnected = Boolean.valueOf(isConnected);
            this.wasEverMultiParticipant = this.wasEverMultiParticipant || valid.getCallModel().getNumUsersConnected() > 1;
            if (!isConnected) {
                connect = new WidgetVoiceBottomSheet.BottomContent.Connect(z3);
            } else {
                connect = new WidgetVoiceBottomSheet.BottomContent.Controls(valid.getCallModel().getInputMode(), valid.getCallModel().getAudioManagerState(), valid.getCallModel().isMeMutedByAnySource(), valid.getCallModel().getCameraState(), hasVideoPermission(), valid.getCallModel().isStreaming());
            }
            updateViewState(new ViewState(c, name, valid.getCallModel().getChannel(), valid.getCallModel().canInvite() && !ChannelUtils.z(valid.getCallModel().getChannel()), valid.getNoiseCancellation(), valid.getCallModel().isDeafenedByAnySource(), centerContent2, connect));
        }
    }

    private final boolean hasVideoPermission() {
        StoreState storeState = this.mostRecentStoreState;
        Long l = null;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid == null) {
            return false;
        }
        VideoPermissionsManager videoPermissionsManager = this.videoPermissionsManager;
        Channel channel = valid.getCallModel().getChannel();
        Guild guild = valid.getCallModel().getGuild();
        if (guild != null) {
            l = guild.getAfkChannelId();
        }
        return videoPermissionsManager.hasVideoPermission(channel, l, valid.getMyPermissions());
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void joinVoiceChannel(long j) {
        this.selectedVoiceChannelStore.selectVoiceChannel(j);
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ExpandSheet.INSTANCE);
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @MainThread
    public final void onCameraButtonPressed() {
        if (!hasVideoPermission()) {
            emitShowNoVideoPermissionDialogEvent();
            return;
        }
        WidgetVoiceBottomSheet.BottomContent bottomContent = requireViewState().getBottomContent();
        StoreState.Valid valid = null;
        if (!(bottomContent instanceof WidgetVoiceBottomSheet.BottomContent.Controls)) {
            bottomContent = null;
        }
        WidgetVoiceBottomSheet.BottomContent.Controls controls = (WidgetVoiceBottomSheet.BottomContent.Controls) bottomContent;
        CameraState cameraState = controls != null ? controls.getCameraState() : null;
        if (cameraState != CameraState.CAMERA_DISABLED) {
            if (cameraState == CameraState.CAMERA_ON) {
                this.mediaEngineStore.selectVideoInputDevice(null);
                return;
            }
            StoreState storeState = this.mostRecentStoreState;
            if (storeState instanceof StoreState.Valid) {
                valid = storeState;
            }
            StoreState.Valid valid2 = valid;
            if (valid2 != null) {
                int numUsersConnected = valid2.getCallModel().getNumUsersConnected();
                GuildMaxVideoChannelUsers guildMaxVideoChannelMembers = valid2.getCallModel().getGuildMaxVideoChannelMembers();
                if (guildMaxVideoChannelMembers instanceof GuildMaxVideoChannelUsers.Limited) {
                    GuildMaxVideoChannelUsers.Limited limited = (GuildMaxVideoChannelUsers.Limited) guildMaxVideoChannelMembers;
                    if (numUsersConnected > limited.a()) {
                        this.eventSubject.k.onNext(new Event.ShowCameraCapacityDialog(limited.a()));
                        return;
                    }
                }
                this.eventSubject.k.onNext(Event.ShowRequestCameraPermissionsDialog.INSTANCE);
            }
        }
    }

    @MainThread
    public final void onCameraPermissionsGranted() {
        StoreMediaEngine.selectDefaultVideoDevice$default(this.mediaEngineStore, null, 1, null);
    }

    @MainThread
    public final void onDeafenPressed() {
        CallModel callModel;
        StoreState storeState = this.mostRecentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null && (callModel = valid.getCallModel()) != null) {
            if (callModel.isServerDeafened()) {
                emitServerDeafenedDialogEvent();
                return;
            }
            this.mediaSettingsStore.toggleSelfDeafened();
            this.eventSubject.k.onNext(new Event.AccessibilityAnnouncement(callModel.isDeafenedByAnySource() ? R.string.voice_channel_undeafened : R.string.voice_channel_deafened));
        }
    }

    @MainThread
    public final void onDisconnect() {
        this.selectedVoiceChannelStore.clear();
        this.eventSubject.k.onNext(Event.Dismiss.INSTANCE);
        StoreState storeState = this.mostRecentStoreState;
        String str = null;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null) {
            if (!this.wasEverConnected || this.wasEverMultiParticipant) {
                CallModel callModel = valid.getCallModel();
                RtcConnection.Metadata rtcConnectionMetadata = callModel.getRtcConnectionMetadata();
                PublishSubject<Event> publishSubject = this.eventSubject;
                long j = this.channelId;
                String str2 = rtcConnectionMetadata != null ? rtcConnectionMetadata.a : null;
                if (rtcConnectionMetadata != null) {
                    str = rtcConnectionMetadata.f2753b;
                }
                publishSubject.k.onNext(new Event.EnqueueCallFeedbackSheet(j, str2, str, callModel.getCallDurationMs(this.clock), callModel.getCallFeedbackTriggerRateDenominator()));
            }
        }
    }

    @MainThread
    public final void onDisconnectPressed() {
        GuildScheduledEvent guildScheduledEventToEnd = getGuildScheduledEventToEnd();
        if (guildScheduledEventToEnd != null) {
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new Event.ShowEventEnd(guildScheduledEventToEnd));
            return;
        }
        onDisconnect();
    }

    @MainThread
    public final void onMutePressed() {
        StoreState storeState = this.mostRecentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid != null) {
            if (valid.getCallModel().isSuppressed()) {
                emitSuppressedDialogEvent();
                return;
            } else if (valid.getCallModel().isMuted()) {
                emitServerMutedDialogEvent();
                return;
            }
        }
        StoreMediaSettings.SelfMuteFailure selfMuteFailure = this.mediaSettingsStore.toggleSelfMuted();
        if (selfMuteFailure == null) {
            WidgetVoiceBottomSheet.BottomContent bottomContent = requireViewState().getBottomContent();
            if (bottomContent != null && (bottomContent instanceof WidgetVoiceBottomSheet.BottomContent.Controls)) {
                this.eventSubject.k.onNext(new Event.AccessibilityAnnouncement(((WidgetVoiceBottomSheet.BottomContent.Controls) bottomContent).isMuted() ? R.string.voice_channel_unmuted : R.string.voice_channel_muted));
            }
        } else if (selfMuteFailure.ordinal() == 0) {
            this.eventSubject.k.onNext(new Event.ShowToast(R.string.vad_permission_small));
        }
    }

    @MainThread
    public final void onNoiseCancellationPressed() {
        Boolean isNoiseCancellationActive = requireViewState().isNoiseCancellationActive();
        if (isNoiseCancellationActive != null) {
            boolean booleanValue = isNoiseCancellationActive.booleanValue();
            TooltipManager tooltipManager = this.tooltipManager;
            NoiseCancellationTooltip noiseCancellationTooltip = NoiseCancellationTooltip.INSTANCE;
            if (tooltipManager.b(noiseCancellationTooltip, true)) {
                this.tooltipManager.a(noiseCancellationTooltip);
                PublishSubject<Event> publishSubject = this.eventSubject;
                publishSubject.k.onNext(Event.ShowNoiseCancellationBottomSheet.INSTANCE);
                return;
            }
            this.mediaSettingsStore.toggleNoiseCancellation();
            if (booleanValue) {
                PublishSubject<Event> publishSubject2 = this.eventSubject;
                publishSubject2.k.onNext(new Event.ShowToast(R.string.noise_cancellation_off));
                return;
            }
            PublishSubject<Event> publishSubject3 = this.eventSubject;
            publishSubject3.k.onNext(new Event.ShowToast(R.string.noise_cancellation_on));
        }
    }

    @MainThread
    public final void onPttPressed(boolean z2) {
        this.mediaEngineStore.setPttActive(z2);
    }

    @MainThread
    public final void onScreenSharePressed() {
        ViewState viewState = getViewState();
        WidgetVoiceBottomSheet.BottomContent.Controls controls = null;
        WidgetVoiceBottomSheet.BottomContent bottomContent = viewState != null ? viewState.getBottomContent() : null;
        if (bottomContent instanceof WidgetVoiceBottomSheet.BottomContent.Controls) {
            controls = bottomContent;
        }
        WidgetVoiceBottomSheet.BottomContent.Controls controls2 = controls;
        if (controls2 == null) {
            return;
        }
        if (controls2.isScreensharing()) {
            this.voiceEngineServiceController.stopStream();
        } else if (!hasVideoPermission()) {
            emitShowNoScreenSharePermissionDialogEvent();
        } else {
            this.eventSubject.k.onNext(Event.RequestStartStream.INSTANCE);
        }
    }

    @MainThread
    public final void onStreamPreviewClicked(String str) {
        m.checkNotNullParameter(str, "streamKey");
        StoreState storeState = this.mostRecentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid == null) {
            return;
        }
        if (valid.getCallModel().getVoiceChannelJoinability() == VoiceChannelJoinability.GUILD_VIDEO_AT_CAPACITY) {
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(Event.ShowGuildVideoAtCapacityDialog.INSTANCE);
            return;
        }
        ModelApplicationStream decodeStreamKey = ModelApplicationStream.Companion.decodeStreamKey(str);
        PublishSubject<Event> publishSubject2 = this.eventSubject;
        publishSubject2.k.onNext(new Event.LaunchVideoCall(decodeStreamKey.getChannelId(), str));
    }

    @MainThread
    public final void onToggleRingingPressed(StoreVoiceParticipants.VoiceUser voiceUser) {
        m.checkNotNullParameter(voiceUser, "voiceUser");
        if (voiceUser.isRinging()) {
            this.callsStore.stopRinging(this.channelId, d0.t.m.listOf(Long.valueOf(voiceUser.getUser().getId())));
        } else {
            this.callsStore.ring(this.channelId, d0.t.m.listOf(Long.valueOf(voiceUser.getUser().getId())));
        }
    }

    public final void startStream(Intent intent) {
        m.checkNotNullParameter(intent, "intent");
        this.voiceEngineServiceController.startStream(intent);
    }

    @MainThread
    public final void tryConnectToVoice(boolean z2) {
        StoreState storeState = this.mostRecentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid == null) {
            return;
        }
        if (valid.getCallModel().getVoiceChannelJoinability() == VoiceChannelJoinability.GUILD_VIDEO_AT_CAPACITY) {
            this.eventSubject.k.onNext(Event.ShowGuildVideoAtCapacityDialog.INSTANCE);
            return;
        }
        if (!this.userSettingsStore.getIsMobileOverlayEnabled()) {
            this.eventSubject.k.onNext(Event.ShowOverlayNux.INSTANCE);
        }
        if (!z2) {
            joinVoiceChannel(this.channelId);
        } else if (!hasVideoPermission()) {
            emitShowNoVideoPermissionDialogEvent();
        } else {
            f.H0(ViewModelKt.getViewModelScope(this), null, null, new WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1(this, StoreStream.Companion.getMediaEngine(), null), 3, null);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetVoiceBottomSheetViewModel(long j, boolean z2, StoreChannels storeChannels, StorePermissions storePermissions, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreMediaSettings storeMediaSettings, StoreMediaEngine storeMediaEngine, StoreUserSettings storeUserSettings, StoreCalls storeCalls, StoreGuilds storeGuilds, VideoPermissionsManager videoPermissionsManager, StoreApplicationStreamPreviews storeApplicationStreamPreviews, VoiceEngineServiceController voiceEngineServiceController, Clock clock, TooltipManager tooltipManager, StoreEmbeddedActivities storeEmbeddedActivities, StoreApplication storeApplication, Observable<StoreState> observable) {
        super(null, 1, null);
        m.checkNotNullParameter(storeChannels, "channelStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(storeVoiceChannelSelected, "selectedVoiceChannelStore");
        m.checkNotNullParameter(storeMediaSettings, "mediaSettingsStore");
        m.checkNotNullParameter(storeMediaEngine, "mediaEngineStore");
        m.checkNotNullParameter(storeUserSettings, "userSettingsStore");
        m.checkNotNullParameter(storeCalls, "callsStore");
        m.checkNotNullParameter(storeGuilds, "guildsStore");
        m.checkNotNullParameter(videoPermissionsManager, "videoPermissionsManager");
        m.checkNotNullParameter(storeApplicationStreamPreviews, "storeApplicationStreamPreviews");
        m.checkNotNullParameter(voiceEngineServiceController, "voiceEngineServiceController");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(tooltipManager, "tooltipManager");
        m.checkNotNullParameter(storeEmbeddedActivities, "embeddedActivitiesStore");
        m.checkNotNullParameter(storeApplication, "applicationStore");
        m.checkNotNullParameter(observable, "storeStateObservable");
        this.channelId = j;
        this.forwardToFullscreenIfVideoActivated = z2;
        this.channelStore = storeChannels;
        this.permissionsStore = storePermissions;
        this.selectedVoiceChannelStore = storeVoiceChannelSelected;
        this.mediaSettingsStore = storeMediaSettings;
        this.mediaEngineStore = storeMediaEngine;
        this.userSettingsStore = storeUserSettings;
        this.callsStore = storeCalls;
        this.guildsStore = storeGuilds;
        this.videoPermissionsManager = videoPermissionsManager;
        this.storeApplicationStreamPreviews = storeApplicationStreamPreviews;
        this.voiceEngineServiceController = voiceEngineServiceController;
        this.clock = clock;
        this.tooltipManager = tooltipManager;
        this.embeddedActivitiesStore = storeEmbeddedActivities;
        this.applicationStore = storeApplication;
        this.eventSubject = PublishSubject.k0();
        this.fetchedPreviews = new LinkedHashSet();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetVoiceBottomSheetViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
