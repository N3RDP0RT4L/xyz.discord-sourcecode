package com.discord.widgets.voice.sheet;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetVoiceNoiseCancellationBottomSheetBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetNoiseCancellationBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetVoiceNoiseCancellationBottomSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetVoiceNoiseCancellationBottomSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetNoiseCancellationBottomSheet$binding$2 extends k implements Function1<View, WidgetVoiceNoiseCancellationBottomSheetBinding> {
    public static final WidgetNoiseCancellationBottomSheet$binding$2 INSTANCE = new WidgetNoiseCancellationBottomSheet$binding$2();

    public WidgetNoiseCancellationBottomSheet$binding$2() {
        super(1, WidgetVoiceNoiseCancellationBottomSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetVoiceNoiseCancellationBottomSheetBinding;", 0);
    }

    public final WidgetVoiceNoiseCancellationBottomSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.noise_cancellation_enable_button;
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.noise_cancellation_enable_button);
        if (materialButton != null) {
            i = R.id.noise_cancellation_learn_more;
            TextView textView = (TextView) view.findViewById(R.id.noise_cancellation_learn_more);
            if (textView != null) {
                return new WidgetVoiceNoiseCancellationBottomSheetBinding((LinearLayout) view, materialButton, textView);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
