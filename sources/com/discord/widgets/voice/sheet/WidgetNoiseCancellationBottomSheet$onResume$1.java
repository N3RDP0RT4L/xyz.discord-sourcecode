package com.discord.widgets.voice.sheet;

import com.discord.widgets.voice.sheet.WidgetNoiseCancellationBottomSheetViewModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetNoiseCancellationBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel$Event;", "event", "", "invoke", "(Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetNoiseCancellationBottomSheet$onResume$1 extends o implements Function1<WidgetNoiseCancellationBottomSheetViewModel.Event, Unit> {
    public final /* synthetic */ WidgetNoiseCancellationBottomSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetNoiseCancellationBottomSheet$onResume$1(WidgetNoiseCancellationBottomSheet widgetNoiseCancellationBottomSheet) {
        super(1);
        this.this$0 = widgetNoiseCancellationBottomSheet;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetNoiseCancellationBottomSheetViewModel.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetNoiseCancellationBottomSheetViewModel.Event event) {
        m.checkNotNullParameter(event, "event");
        this.this$0.handleEvent(event);
    }
}
