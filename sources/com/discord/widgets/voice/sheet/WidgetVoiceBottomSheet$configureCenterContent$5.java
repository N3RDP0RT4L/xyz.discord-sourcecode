package com.discord.widgets.voice.sheet;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import com.discord.widgets.notice.WidgetNoticeDialog;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetVoiceBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceBottomSheet$configureCenterContent$5 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetVoiceBottomSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetVoiceBottomSheet$configureCenterContent$5(WidgetVoiceBottomSheet widgetVoiceBottomSheet) {
        super(0);
        this.this$0 = widgetVoiceBottomSheet;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = this.this$0.requireContext();
        m.checkNotNullExpressionValue(requireContext, "requireContext()");
        m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.embedded_activities_desktop_only_modal_title);
        String string2 = requireContext.getString(R.string.embedded_activities_desktop_only_modal_description);
        m.checkNotNullExpressionValue(string2, "context.getString(R.stri…p_only_modal_description)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }
}
