package com.discord.widgets.voice.fullscreen;

import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetCallFullscreenBinding;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetCallFullscreen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()I", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallFullscreen$setUpGridRecycler$layoutManager$1 extends o implements Function0<Integer> {
    public final /* synthetic */ int $layoutManagerOrientation;
    public final /* synthetic */ WidgetCallFullscreen this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCallFullscreen$setUpGridRecycler$layoutManager$1(WidgetCallFullscreen widgetCallFullscreen, int i) {
        super(0);
        this.this$0 = widgetCallFullscreen;
        this.$layoutManagerOrientation = i;
    }

    /* JADX WARN: Type inference failed for: r0v4, types: [int, java.lang.Integer] */
    /* JADX WARN: Type inference failed for: r0v9, types: [int, java.lang.Integer] */
    @Override // kotlin.jvm.functions.Function0
    public final Integer invoke() {
        WidgetCallFullscreenBinding binding;
        WidgetCallFullscreenBinding binding2;
        int i = this.$layoutManagerOrientation;
        if (i == 0) {
            binding = this.this$0.getBinding();
            RecyclerView recyclerView = binding.p;
            m.checkNotNullExpressionValue(recyclerView, "binding.callVideoRecycler");
            return recyclerView.getWidth();
        } else if (i == 1) {
            binding2 = this.this$0.getBinding();
            RecyclerView recyclerView2 = binding2.p;
            m.checkNotNullExpressionValue(recyclerView2, "binding.callVideoRecycler");
            return recyclerView2.getHeight();
        } else {
            throw new IllegalStateException();
        }
    }
}
