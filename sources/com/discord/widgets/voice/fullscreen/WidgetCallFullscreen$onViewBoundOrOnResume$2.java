package com.discord.widgets.voice.fullscreen;

import com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetCallFullscreen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "p1", "", "invoke", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetCallFullscreen$onViewBoundOrOnResume$2 extends k implements Function1<WidgetCallFullscreenViewModel.Event, Unit> {
    public WidgetCallFullscreen$onViewBoundOrOnResume$2(WidgetCallFullscreen widgetCallFullscreen) {
        super(1, widgetCallFullscreen, WidgetCallFullscreen.class, "handleEvent", "handleEvent(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetCallFullscreenViewModel.Event event) {
        invoke2(event);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetCallFullscreenViewModel.Event event) {
        m.checkNotNullParameter(event, "p1");
        ((WidgetCallFullscreen) this.receiver).handleEvent(event);
    }
}
