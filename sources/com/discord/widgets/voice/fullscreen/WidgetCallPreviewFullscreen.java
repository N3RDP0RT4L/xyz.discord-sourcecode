package com.discord.widgets.voice.fullscreen;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewKt;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.RecyclerView;
import b.a.a.m;
import b.a.d.f0;
import b.a.d.h0;
import b.a.d.j;
import b.a.i.o0;
import b.c.a.a0.d;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.app.AppFragment;
import com.discord.app.AppTransitionActivity;
import com.discord.databinding.WidgetCallPreviewFullscreenBinding;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.home.WidgetHomePanelNsfw;
import com.discord.widgets.notice.WidgetNoticeNuxOverlay;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel;
import com.discord.widgets.voice.sheet.CallParticipantsAdapter;
import com.google.android.material.button.MaterialButton;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Subscription;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetCallPreviewFullscreen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 @2\u00020\u0001:\u0001@B\u0007¢\u0006\u0004\b?\u0010\u001bJ\u0013\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u001f\u0010\u000f\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0015\u0010\nJ\u0017\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001a\u001a\u00020\bH\u0002¢\u0006\u0004\b\u001a\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\bH\u0002¢\u0006\u0004\b\u001c\u0010\u001bJ#\u0010\u001f\u001a\u00020\b2\b\b\u0002\u0010\u001d\u001a\u00020\u00162\b\b\u0002\u0010\u001e\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010#\u001a\u00020\b2\u0006\u0010\"\u001a\u00020!H\u0002¢\u0006\u0004\b#\u0010$J\u0017\u0010'\u001a\u00020\b2\u0006\u0010&\u001a\u00020%H\u0016¢\u0006\u0004\b'\u0010(J\u000f\u0010)\u001a\u00020\bH\u0016¢\u0006\u0004\b)\u0010\u001bR\u001d\u0010/\u001a\u00020*8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u0016\u00101\u001a\u0002008\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b1\u00102R\u0018\u00104\u001a\u0004\u0018\u0001038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b4\u00105R\u0018\u00107\u001a\u0004\u0018\u0001068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R\u001d\u0010>\u001a\u0002098B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b:\u0010;\u001a\u0004\b<\u0010=¨\u0006A"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreen;", "Lcom/discord/app/AppFragment;", "", "Lcom/discord/primitives/ChannelId;", "getVoiceChannelId", "()J", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ViewState;", "viewState", "", "configureActionBar", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ViewState;)V", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;", "participantsList", "Lcom/discord/api/channel/Channel;", "channel", "configureParticipants", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;Lcom/discord/api/channel/Channel;)V", "Lcom/discord/utilities/streams/StreamContext;", "streamContext", "onStreamPreviewClicked", "(Lcom/discord/utilities/streams/StreamContext;)V", "configureUI", "", "isHidden", "onNsfwToggle", "(Z)V", "onDenyNsfw", "()V", "transitionActivity", "selectTextChannel", "transition", "finishActivity", "(ZZ)V", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event;", "event", "handleEvent", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel;", "viewModel", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;", "participantsAdapter", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;", "Lrx/Subscription;", "viewModelEventSubscription", "Lrx/Subscription;", "Lcom/discord/widgets/home/WidgetHomePanelNsfw;", "panelNsfw", "Lcom/discord/widgets/home/WidgetHomePanelNsfw;", "Lcom/discord/databinding/WidgetCallPreviewFullscreenBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetCallPreviewFullscreenBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallPreviewFullscreen extends AppFragment {
    private static final String ANALYTICS_SOURCE = "Fullscreen Voice Channel Preview";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetCallPreviewFullscreen$binding$2.INSTANCE, null, 2, null);
    private WidgetHomePanelNsfw panelNsfw;
    private CallParticipantsAdapter participantsAdapter;
    private final Lazy viewModel$delegate;
    private Subscription viewModelEventSubscription;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetCallPreviewFullscreen.class, "binding", "getBinding()Lcom/discord/databinding/WidgetCallPreviewFullscreenBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetCallPreviewFullscreen.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J/\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0004\b\n\u0010\u000bR\u0016\u0010\r\u001a\u00020\f8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreen$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/app/AppTransitionActivity$Transition;", "transition", "", "launch", "(Landroid/content/Context;JLcom/discord/app/AppTransitionActivity$Transition;)V", "", "ANALYTICS_SOURCE", "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void launch$default(Companion companion, Context context, long j, AppTransitionActivity.Transition transition, int i, Object obj) {
            if ((i & 4) != 0) {
                transition = null;
            }
            companion.launch(context, j, transition);
        }

        public final void launch(Context context, long j, AppTransitionActivity.Transition transition) {
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent(context, WidgetCallFullscreen.class);
            intent.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", j);
            intent.putExtra("transition", transition);
            j.d(context, WidgetCallPreviewFullscreen.class, intent);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetCallPreviewFullscreen() {
        super(R.layout.widget_call_preview_fullscreen);
        WidgetCallPreviewFullscreen$viewModel$2 widgetCallPreviewFullscreen$viewModel$2 = new WidgetCallPreviewFullscreen$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetCallPreviewFullscreenViewModel.class), new WidgetCallPreviewFullscreen$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetCallPreviewFullscreen$viewModel$2));
    }

    private final void configureActionBar(final WidgetCallPreviewFullscreenViewModel.ViewState viewState) {
        int i = 0;
        boolean z2 = viewState.getTotalMentionsCount() > 0;
        TextView textView = getBinding().e;
        m.checkNotNullExpressionValue(textView, "binding.callFullscreenMentions");
        textView.setText(String.valueOf(viewState.getTotalMentionsCount()));
        TextView textView2 = getBinding().e;
        m.checkNotNullExpressionValue(textView2, "binding.callFullscreenMentions");
        if (!z2) {
            i = 8;
        }
        textView2.setVisibility(i);
        Toolbar toolbar = getBinding().f2232b;
        m.checkNotNullExpressionValue(toolbar, "binding.actionBarToolbar");
        toolbar.setNavigationIcon(ContextCompat.getDrawable(requireContext(), z2 ? R.drawable.ic_call_toolbar_stage_minimize_cutout : R.drawable.ic_call_toolbar_stage_minimize));
        getBinding().f2232b.setNavigationOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen$configureActionBar$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetCallPreviewFullscreen.finishActivity$default(WidgetCallPreviewFullscreen.this, true, false, 2, null);
            }
        });
        setActionBarOptionsMenu(R.menu.menu_call_preview_fullscreen, null, new Action1<Menu>() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen$configureActionBar$2
            public final void call(Menu menu) {
                MenuItem findItem = menu.findItem(R.id.menu_text_in_voice);
                boolean z3 = viewState.getTextInVoiceEnabled() && viewState.isConnectEnabled();
                m.checkNotNullExpressionValue(findItem, "textInVoiceMenuItem");
                findItem.setVisible(z3);
                if (z3) {
                    View actionView = findItem.getActionView();
                    ImageView imageView = null;
                    TextView textView3 = actionView != null ? (TextView) actionView.findViewById(R.id.text_in_voice_count) : null;
                    View actionView2 = findItem.getActionView();
                    if (actionView2 != null) {
                        imageView = (ImageView) actionView2.findViewById(R.id.text_in_voice_icon);
                    }
                    if (textView3 != null) {
                        ViewKt.setVisible(textView3, false);
                    }
                    if (imageView != null) {
                        imageView.setImageResource(R.drawable.ic_text_in_voice);
                    }
                    findItem.getActionView().setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen$configureActionBar$2.1
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            WidgetCallPreviewFullscreenViewModel viewModel;
                            WidgetCallPreviewFullscreen.this.transitionActivity();
                            viewModel = WidgetCallPreviewFullscreen.this.getViewModel();
                            viewModel.onTextInVoiceTapped();
                        }
                    });
                }
            }
        });
        int color = ColorCompat.getColor(this, (int) R.color.transparent);
        String titleText = viewState.getTitleText();
        if (titleText == null) {
            titleText = "";
        }
        setActionBarTitle(titleText);
        setActionBarTitleColor(-1);
        ColorCompat.setStatusBarTranslucent(this);
        ColorCompat.setStatusBarColor((Fragment) this, color, true);
    }

    private final void configureParticipants(WidgetCallPreviewFullscreenViewModel.ParticipantsList participantsList, Channel channel) {
        int i = 0;
        if (participantsList instanceof WidgetCallPreviewFullscreenViewModel.ParticipantsList.ListItems) {
            o0 o0Var = getBinding().k;
            m.checkNotNullExpressionValue(o0Var, "binding.empty");
            ConstraintLayout constraintLayout = o0Var.a;
            m.checkNotNullExpressionValue(constraintLayout, "binding.empty.root");
            constraintLayout.setVisibility(4);
            RecyclerView recyclerView = getBinding().j;
            m.checkNotNullExpressionValue(recyclerView, "binding.callPreviewVoiceSheetRecycler");
            recyclerView.setVisibility(0);
            CallParticipantsAdapter callParticipantsAdapter = this.participantsAdapter;
            if (callParticipantsAdapter == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter.setData(((WidgetCallPreviewFullscreenViewModel.ParticipantsList.ListItems) participantsList).getItems());
            CallParticipantsAdapter callParticipantsAdapter2 = this.participantsAdapter;
            if (callParticipantsAdapter2 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter2.setOnStreamPreviewClicked(new WidgetCallPreviewFullscreen$configureParticipants$1(this));
            CallParticipantsAdapter callParticipantsAdapter3 = this.participantsAdapter;
            if (callParticipantsAdapter3 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter3.setOnVoiceUserClicked(new WidgetCallPreviewFullscreen$configureParticipants$2(this, channel));
            CallParticipantsAdapter callParticipantsAdapter4 = this.participantsAdapter;
            if (callParticipantsAdapter4 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter4.setOnInviteFriendsClicked(new WidgetCallPreviewFullscreen$configureParticipants$3(this, channel));
            CallParticipantsAdapter callParticipantsAdapter5 = this.participantsAdapter;
            if (callParticipantsAdapter5 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter5.setOnEmbeddedActivityClicked(new WidgetCallPreviewFullscreen$configureParticipants$4(this));
        } else if (participantsList instanceof WidgetCallPreviewFullscreenViewModel.ParticipantsList.Empty) {
            o0 o0Var2 = getBinding().k;
            m.checkNotNullExpressionValue(o0Var2, "binding.empty");
            ConstraintLayout constraintLayout2 = o0Var2.a;
            m.checkNotNullExpressionValue(constraintLayout2, "binding.empty.root");
            constraintLayout2.setVisibility(0);
            RecyclerView recyclerView2 = getBinding().j;
            m.checkNotNullExpressionValue(recyclerView2, "binding.callPreviewVoiceSheetRecycler");
            WidgetCallPreviewFullscreenViewModel.ParticipantsList.Empty empty = (WidgetCallPreviewFullscreenViewModel.ParticipantsList.Empty) participantsList;
            if (!(!empty.getItems().isEmpty())) {
                i = 8;
            }
            recyclerView2.setVisibility(i);
            CallParticipantsAdapter callParticipantsAdapter6 = this.participantsAdapter;
            if (callParticipantsAdapter6 == null) {
                m.throwUninitializedPropertyAccessException("participantsAdapter");
            }
            callParticipantsAdapter6.setData(empty.getItems());
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final void configureUI(WidgetCallPreviewFullscreenViewModel.ViewState viewState) {
        configureActionBar(viewState);
        configureParticipants(viewState.getParticipantsList(), viewState.getVoiceChannel());
        if (viewState.isConnectEnabled()) {
            getBinding().h.setText(R.string.join_voice);
            getBinding().g.setText(R.string.join_muted);
            MaterialButton materialButton = getBinding().h;
            m.checkNotNullExpressionValue(materialButton, "binding.callPreviewJoinVoice");
            materialButton.setEnabled(true);
            MaterialButton materialButton2 = getBinding().g;
            m.checkNotNullExpressionValue(materialButton2, "binding.callPreviewJoinMuted");
            materialButton2.setEnabled(true);
        } else {
            getBinding().h.setText(R.string.channel_locked_short);
            MaterialButton materialButton3 = getBinding().h;
            m.checkNotNullExpressionValue(materialButton3, "binding.callPreviewJoinVoice");
            materialButton3.setEnabled(false);
            getBinding().g.setText(R.string.channel_locked_short);
            MaterialButton materialButton4 = getBinding().g;
            m.checkNotNullExpressionValue(materialButton4, "binding.callPreviewJoinMuted");
            materialButton4.setEnabled(false);
        }
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen$configureUI$1

            /* compiled from: WidgetCallPreviewFullscreen.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen$configureUI$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function0<Unit> {
                public AnonymousClass1() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    WidgetCallPreviewFullscreenViewModel viewModel;
                    viewModel = WidgetCallPreviewFullscreen.this.getViewModel();
                    WidgetCallPreviewFullscreenViewModel.tryConnectToVoice$default(viewModel, false, 1, null);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                d.S1(WidgetCallPreviewFullscreen.this, null, new AnonymousClass1(), 1, null);
            }
        });
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen$configureUI$2

            /* compiled from: WidgetCallPreviewFullscreen.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen$configureUI$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function0<Unit> {
                public AnonymousClass1() {
                    super(0);
                }

                @Override // kotlin.jvm.functions.Function0
                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2() {
                    WidgetCallPreviewFullscreenViewModel viewModel;
                    viewModel = WidgetCallPreviewFullscreen.this.getViewModel();
                    viewModel.tryConnectToVoice(true);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                d.S1(WidgetCallPreviewFullscreen.this, null, new AnonymousClass1(), 1, null);
            }
        });
        WidgetHomePanelNsfw widgetHomePanelNsfw = this.panelNsfw;
        if (widgetHomePanelNsfw != null) {
            widgetHomePanelNsfw.configureUI(viewState.getVoiceChannel().f(), viewState.isChannelNsfw(), viewState.isNsfwUnconsented(), viewState.getNsfwAllowed(), getBinding().i, new WidgetCallPreviewFullscreen$configureUI$3(this), new WidgetCallPreviewFullscreen$configureUI$4(this));
        }
    }

    private final void finishActivity(boolean z2, boolean z3) {
        if (z2) {
            getViewModel().selectTextChannelAfterFinish();
        }
        Subscription subscription = this.viewModelEventSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        requireAppActivity().finish();
        if (z3) {
            requireAppActivity().overridePendingTransition(0, R.anim.anim_slide_out_down);
        }
    }

    public static /* synthetic */ void finishActivity$default(WidgetCallPreviewFullscreen widgetCallPreviewFullscreen, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        if ((i & 2) != 0) {
            z3 = true;
        }
        widgetCallPreviewFullscreen.finishActivity(z2, z3);
    }

    public final WidgetCallPreviewFullscreenBinding getBinding() {
        return (WidgetCallPreviewFullscreenBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final WidgetCallPreviewFullscreenViewModel getViewModel() {
        return (WidgetCallPreviewFullscreenViewModel) this.viewModel$delegate.getValue();
    }

    public final long getVoiceChannelId() {
        return getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", -1L);
    }

    public final void handleEvent(WidgetCallPreviewFullscreenViewModel.Event event) {
        if (m.areEqual(event, WidgetCallPreviewFullscreenViewModel.Event.ShowGuildVideoAtCapacityDialog.INSTANCE)) {
            m.a aVar = b.a.a.m.k;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            aVar.a(parentFragmentManager);
        } else if (d0.z.d.m.areEqual(event, WidgetCallPreviewFullscreenViewModel.Event.ShowOverlayNux.INSTANCE)) {
            WidgetNoticeNuxOverlay.Companion.enqueue();
        } else if (event instanceof WidgetCallPreviewFullscreenViewModel.Event.LaunchVideoCall) {
            finishActivity$default(this, false, false, 1, null);
            WidgetCallPreviewFullscreenViewModel.Event.LaunchVideoCall launchVideoCall = (WidgetCallPreviewFullscreenViewModel.Event.LaunchVideoCall) event;
            WidgetCallFullscreen.Companion.launch(requireContext(), launchVideoCall.getChannelId(), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : launchVideoCall.getAutoTargetStreamKey(), (r14 & 16) != 0 ? null : null);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public static final void launch(Context context, long j, AppTransitionActivity.Transition transition) {
        Companion.launch(context, j, transition);
    }

    public final void onDenyNsfw() {
        finishActivity$default(this, true, false, 2, null);
    }

    public final void onNsfwToggle(boolean z2) {
        ConstraintLayout constraintLayout = getBinding().d;
        d0.z.d.m.checkNotNullExpressionValue(constraintLayout, "binding.callFullscreenBody");
        constraintLayout.setVisibility(z2 ^ true ? 0 : 8);
    }

    public final void onStreamPreviewClicked(StreamContext streamContext) {
        d.S1(this, null, new WidgetCallPreviewFullscreen$onStreamPreviewClicked$1(this, streamContext), 1, null);
    }

    public final void transitionActivity() {
        finishActivity$default(this, false, false, 3, null);
        requireAppActivity().overridePendingTransition(R.anim.activity_slide_horizontal_open_in, R.anim.activity_slide_horizontal_open_out);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        d0.z.d.m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().c, WidgetCallPreviewFullscreen$onViewBound$1.INSTANCE);
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().e, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen$onViewBound$2
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view2, WindowInsetsCompat windowInsetsCompat) {
                d0.z.d.m.checkNotNullExpressionValue(view2, "windowView");
                ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
                WidgetCallFullscreen.Companion companion = WidgetCallFullscreen.Companion;
                Resources resources = WidgetCallPreviewFullscreen.this.getResources();
                d0.z.d.m.checkNotNullExpressionValue(resources, "resources");
                ((ViewGroup.MarginLayoutParams) layoutParams2).leftMargin = DimenUtils.dpToPixels(companion.getUnreadIndicatorMarginLeftDp(resources));
                d0.z.d.m.checkNotNullExpressionValue(windowInsetsCompat, "insets");
                int systemWindowInsetTop = windowInsetsCompat.getSystemWindowInsetTop();
                Resources resources2 = WidgetCallPreviewFullscreen.this.getResources();
                d0.z.d.m.checkNotNullExpressionValue(resources2, "resources");
                ((ViewGroup.MarginLayoutParams) layoutParams2).topMargin = DimenUtils.dpToPixels(companion.getUnreadIndicatorMarginDp(resources2)) + systemWindowInsetTop;
                view2.setLayoutParams(layoutParams2);
                return windowInsetsCompat;
            }
        });
        ConstraintLayout constraintLayout = getBinding().f;
        d0.z.d.m.checkNotNullExpressionValue(constraintLayout, "binding.callFullscreenParent");
        ViewExtensions.setForwardingWindowInsetsListener(constraintLayout);
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().j;
        d0.z.d.m.checkNotNullExpressionValue(recyclerView, "binding.callPreviewVoiceSheetRecycler");
        CallParticipantsAdapter callParticipantsAdapter = (CallParticipantsAdapter) companion.configure(new CallParticipantsAdapter(recyclerView, true, true));
        this.participantsAdapter = callParticipantsAdapter;
        if (callParticipantsAdapter == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("participantsAdapter");
        }
        callParticipantsAdapter.setOnEventClicked(new WidgetCallPreviewFullscreen$onViewBound$3(this));
        CallParticipantsAdapter callParticipantsAdapter2 = this.participantsAdapter;
        if (callParticipantsAdapter2 == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("participantsAdapter");
        }
        callParticipantsAdapter2.setOnStartEventClicked(new WidgetCallPreviewFullscreen$onViewBound$4(this));
        this.panelNsfw = new WidgetHomePanelNsfw(this);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetCallPreviewFullscreen.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetCallPreviewFullscreen$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetCallPreviewFullscreen.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetCallPreviewFullscreen$onViewBoundOrOnResume$3(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetCallPreviewFullscreen$onViewBoundOrOnResume$2(this));
    }
}
