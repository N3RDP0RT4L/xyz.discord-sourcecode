package com.discord.widgets.voice.fullscreen;

import com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetCallFullscreenViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallFullscreenViewModel$toggleRequestToSpeak$1 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetCallFullscreenViewModel this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCallFullscreenViewModel$toggleRequestToSpeak$1(WidgetCallFullscreenViewModel widgetCallFullscreenViewModel) {
        super(0);
        this.this$0 = widgetCallFullscreenViewModel;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WidgetCallFullscreenViewModel.ViewState viewState;
        viewState = this.this$0.getViewState();
        if (!(viewState instanceof WidgetCallFullscreenViewModel.ViewState.Valid)) {
            viewState = null;
        }
        WidgetCallFullscreenViewModel.ViewState.Valid valid = (WidgetCallFullscreenViewModel.ViewState.Valid) viewState;
        if (valid != null) {
            this.this$0.updateViewState((WidgetCallFullscreenViewModel.ViewState) WidgetCallFullscreenViewModel.ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, null, false, false, false, null, null, null, null, false, null, null, null, false, false, 0.0f, 0, false, null, false, false, 0, false, null, null, null, 0, 0, false, false, null, -2097153, 1, null));
        }
    }
}
