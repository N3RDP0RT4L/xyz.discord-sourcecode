package com.discord.widgets.voice.fullscreen;

import b.a.d.m;
import com.discord.api.channel.Channel;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import xyz.discord.R;
/* compiled from: WidgetCallFullscreen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallFullscreen$onViewBound$3 extends o implements Function0<Unit> {
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ WidgetCallFullscreen this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCallFullscreen$onViewBound$3(WidgetCallFullscreen widgetCallFullscreen, Channel channel) {
        super(0);
        this.this$0 = widgetCallFullscreen;
        this.$channel = channel;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        m.g(this.this$0.getContext(), R.string.permission_microphone_denied, 0, null, 12);
        WidgetCallFullscreen widgetCallFullscreen = this.this$0;
        TextInVoiceFeatureFlag instance = TextInVoiceFeatureFlag.Companion.getINSTANCE();
        Channel channel = this.$channel;
        WidgetCallFullscreen.finishActivity$default(widgetCallFullscreen, false, instance.isEnabled(channel != null ? Long.valueOf(channel.f()) : null), 1, null);
    }
}
