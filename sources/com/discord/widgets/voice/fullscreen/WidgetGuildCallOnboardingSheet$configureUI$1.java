package com.discord.widgets.voice.fullscreen;

import android.content.Context;
import b.c.a.a0.d;
import com.discord.widgets.voice.fullscreen.WidgetGuildCallOnboardingSheet;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetGuildCallOnboardingSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildCallOnboardingSheet$configureUI$1 extends o implements Function0<Unit> {
    public final /* synthetic */ WidgetGuildCallOnboardingSheet this$0;

    /* compiled from: WidgetGuildCallOnboardingSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.fullscreen.WidgetGuildCallOnboardingSheet$configureUI$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function0<Unit> {
        public AnonymousClass1() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            WidgetGuildCallOnboardingSheetViewModel viewModel;
            WidgetGuildCallOnboardingSheet.Companion companion = WidgetGuildCallOnboardingSheet.Companion;
            Context requireContext = WidgetGuildCallOnboardingSheet$configureUI$1.this.this$0.requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            companion.markSeenUserVoiceChannelOnboarding(requireContext);
            viewModel = WidgetGuildCallOnboardingSheet$configureUI$1.this.this$0.getViewModel();
            viewModel.onConnectPressed();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetGuildCallOnboardingSheet$configureUI$1(WidgetGuildCallOnboardingSheet widgetGuildCallOnboardingSheet) {
        super(0);
        this.this$0 = widgetGuildCallOnboardingSheet;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        d.S1(this.this$0, null, new AnonymousClass1(), 1, null);
    }
}
