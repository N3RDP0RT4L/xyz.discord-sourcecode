package com.discord.widgets.voice.fullscreen;

import androidx.core.app.NotificationCompat;
import com.discord.api.channel.Channel;
import com.discord.utilities.voice.VoiceChannelJoinability;
import com.discord.widgets.voice.fullscreen.WidgetGuildCallOnboardingSheetViewModel;
import d0.z.d.m;
import kotlin.Metadata;
import rx.functions.Func2;
/* compiled from: WidgetGuildCallOnboardingSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\b\u001a\n \u0003*\u0004\u0018\u00010\u00050\u00052\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00020\u0002H\n¢\u0006\u0004\b\u0006\u0010\u0007"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/utilities/voice/VoiceChannelJoinability;", "kotlin.jvm.PlatformType", "joinability", "Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$StoreState;", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;Lcom/discord/utilities/voice/VoiceChannelJoinability;)Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetGuildCallOnboardingSheetViewModel$observeStoreState$1<T1, T2, R> implements Func2<Channel, VoiceChannelJoinability, WidgetGuildCallOnboardingSheetViewModel.StoreState> {
    public static final WidgetGuildCallOnboardingSheetViewModel$observeStoreState$1 INSTANCE = new WidgetGuildCallOnboardingSheetViewModel$observeStoreState$1();

    public final WidgetGuildCallOnboardingSheetViewModel.StoreState call(Channel channel, VoiceChannelJoinability voiceChannelJoinability) {
        Long valueOf = channel != null ? Long.valueOf(channel.f()) : null;
        m.checkNotNullExpressionValue(voiceChannelJoinability, "joinability");
        return new WidgetGuildCallOnboardingSheetViewModel.StoreState(valueOf, voiceChannelJoinability);
    }
}
