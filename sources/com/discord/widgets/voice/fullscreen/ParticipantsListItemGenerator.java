package com.discord.widgets.voice.fullscreen;

import andhook.lib.HookHelper;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.embeddedactivities.EmbeddedActivity;
import com.discord.models.guild.UserGuildMember;
import com.discord.stores.StoreApplicationStreamPreviews;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.EmbeddedActivityUtilsKt;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventTiming;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilitiesKt;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventsComparator;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.voice.model.CallModel;
import com.discord.widgets.voice.sheet.CallParticipantsAdapter;
import d0.t.o;
import d0.t.u;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ParticipantsListItemGenerator.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\u0018\u0000 \u00042\u00020\u0001:\u0001\u0004B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0005"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/ParticipantsListItemGenerator;", "", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class ParticipantsListItemGenerator {
    public static final Companion Companion = new Companion(null);

    /* compiled from: ParticipantsListItemGenerator.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b(\u0010)J1\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u00072\u0010\b\u0002\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003H\u0002¢\u0006\u0004\b\b\u0010\tJe\u0010\u0019\u001a\u00020\u00182\u0016\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\u000b0\nj\b\u0012\u0004\u0012\u00020\u000b`\f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u00060\u00112\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0015\u0012\u0004\u0012\u00020\u00160\u0011H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJq\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000e2\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0013\u0012\u0004\u0012\u00020\u00060\u00112\u000e\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u001d2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u0012j\u0002`\u0015\u0012\u0004\u0012\u00020\u00160\u0011¢\u0006\u0004\b\u001f\u0010 J5\u0010&\u001a\u00020%2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000e2\u0010\u0010\"\u001a\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030!2\u0006\u0010$\u001a\u00020#¢\u0006\u0004\b&\u0010'¨\u0006*"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/ParticipantsListItemGenerator$Companion;", "", "", "Lcom/discord/primitives/StreamKey;", "mySpectatingStreamKey", "Ljava/util/Comparator;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "Lkotlin/Comparator;", "createUserItemsComparator", "(Ljava/lang/String;)Ljava/util/Comparator;", "Ljava/util/ArrayList;", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem;", "Lkotlin/collections/ArrayList;", "listItems", "", "Lcom/discord/models/embeddedactivities/EmbeddedActivity;", "embeddedActivities", "", "", "Lcom/discord/primitives/UserId;", "voiceParticipants", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/api/application/Application;", "applications", "", "addEmbeddedActivitiesToListItems", "(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)V", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/widgets/voice/model/CallModel;", "callModel", "createConnectedListItems", "(Ljava/util/Map;Ljava/lang/String;Lcom/discord/api/channel/Channel;Lcom/discord/widgets/voice/model/CallModel;Ljava/util/List;Ljava/util/Map;)Ljava/util/List;", "", "fetchedPreviews", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "storeApplicationStreamPreviews", "", "refreshStreams", "(Ljava/util/List;Ljava/util/Set;Lcom/discord/stores/StoreApplicationStreamPreviews;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        private final void addEmbeddedActivitiesToListItems(ArrayList<CallParticipantsAdapter.ListItem> arrayList, List<EmbeddedActivity> list, Map<Long, StoreVoiceParticipants.VoiceUser> map, Map<Long, Application> map2) {
            for (EmbeddedActivity embeddedActivity : list) {
                List<UserGuildMember> activityParticipants = EmbeddedActivityUtilsKt.getActivityParticipants(embeddedActivity, map);
                Application application = map2.get(Long.valueOf(embeddedActivity.getApplicationId()));
                if (application != null) {
                    arrayList.add(new CallParticipantsAdapter.ListItem.EmbeddedActivityItem(embeddedActivity, activityParticipants, com.discord.models.commands.Application.Companion.fromApiApplication(application)));
                }
            }
        }

        private final Comparator<StoreVoiceParticipants.VoiceUser> createUserItemsComparator(final String str) {
            return new Comparator<StoreVoiceParticipants.VoiceUser>() { // from class: com.discord.widgets.voice.fullscreen.ParticipantsListItemGenerator$Companion$createUserItemsComparator$1
                public final int compare(StoreVoiceParticipants.VoiceUser voiceUser, StoreVoiceParticipants.VoiceUser voiceUser2) {
                    boolean z2 = false;
                    boolean z3 = str != null;
                    ModelApplicationStream applicationStream = voiceUser.getApplicationStream();
                    String str2 = null;
                    String encodedStreamKey = applicationStream != null ? applicationStream.getEncodedStreamKey() : null;
                    ModelApplicationStream applicationStream2 = voiceUser2.getApplicationStream();
                    if (applicationStream2 != null) {
                        str2 = applicationStream2.getEncodedStreamKey();
                    }
                    boolean z4 = encodedStreamKey != null;
                    boolean z5 = str2 != null;
                    VoiceState voiceState = voiceUser.getVoiceState();
                    boolean z6 = voiceState != null && voiceState.j();
                    VoiceState voiceState2 = voiceUser2.getVoiceState();
                    boolean z7 = voiceState2 != null && voiceState2.j();
                    boolean z8 = z3 && m.areEqual(encodedStreamKey, str);
                    if (z3 && m.areEqual(str2, str)) {
                        z2 = true;
                    }
                    if (!z8) {
                        if (z2) {
                            return 1;
                        }
                        if (!voiceUser.isMe()) {
                            if (voiceUser2.isMe()) {
                                return 1;
                            }
                            if (!z3 || !m.areEqual(voiceUser.getWatchingStream(), str) || !(!m.areEqual(voiceUser2.getWatchingStream(), str))) {
                                if (z3 && m.areEqual(voiceUser2.getWatchingStream(), str) && (!m.areEqual(voiceUser.getWatchingStream(), str))) {
                                    return 1;
                                }
                                if (!z4 || z5) {
                                    if (!z4 && z5) {
                                        return 1;
                                    }
                                    if (!z6 || z7) {
                                        if (z6 || !z7) {
                                            return UserUtils.INSTANCE.compareUserNames(voiceUser.getUser(), voiceUser2.getUser(), voiceUser.getNickname(), voiceUser2.getNickname());
                                        }
                                        return 1;
                                    }
                                }
                            }
                        }
                    }
                    return -1;
                }
            };
        }

        public static /* synthetic */ Comparator createUserItemsComparator$default(Companion companion, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = null;
            }
            return companion.createUserItemsComparator(str);
        }

        public final List<CallParticipantsAdapter.ListItem> createConnectedListItems(Map<Long, StoreVoiceParticipants.VoiceUser> map, String str, Channel channel, CallModel callModel, List<EmbeddedActivity> list, Map<Long, Application> map2) {
            m.checkNotNullParameter(map, "voiceParticipants");
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(callModel, "callModel");
            m.checkNotNullParameter(list, "embeddedActivities");
            m.checkNotNullParameter(map2, "applications");
            ArrayList<CallParticipantsAdapter.ListItem> arrayList = new ArrayList<>();
            List<GuildScheduledEvent> guildScheduledEvents = callModel.getGuildScheduledEvents();
            ArrayList arrayList2 = new ArrayList();
            Iterator<T> it = guildScheduledEvents.iterator();
            while (true) {
                boolean z2 = false;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                GuildScheduledEventTiming eventTiming = GuildScheduledEventUtilitiesKt.getEventTiming((GuildScheduledEvent) next);
                if (eventTiming.isStartable() || eventTiming == GuildScheduledEventTiming.LIVE) {
                    z2 = true;
                }
                if (z2) {
                    arrayList2.add(next);
                }
            }
            GuildScheduledEvent guildScheduledEvent = (GuildScheduledEvent) u.minWithOrNull(arrayList2, GuildScheduledEventsComparator.INSTANCE);
            if (guildScheduledEvent != null) {
                arrayList.add(new CallParticipantsAdapter.ListItem.Event(guildScheduledEvent, callModel.canManageEvent(), callModel.isConnected()));
            }
            addEmbeddedActivitiesToListItems(arrayList, list, map, map2);
            ArrayList arrayList3 = new ArrayList();
            for (StoreVoiceParticipants.VoiceUser voiceUser : map.values()) {
                if (voiceUser.isConnected() || ChannelUtils.x(channel)) {
                    arrayList3.add(voiceUser);
                }
            }
            boolean z3 = !arrayList3.isEmpty();
            boolean z4 = str != null;
            if (z3) {
                List<StoreVoiceParticipants.VoiceUser> sortedWith = u.sortedWith(arrayList3, createUserItemsComparator(str));
                ArrayList arrayList4 = new ArrayList(o.collectionSizeOrDefault(sortedWith, 10));
                for (StoreVoiceParticipants.VoiceUser voiceUser2 : sortedWith) {
                    arrayList4.add(new CallParticipantsAdapter.ListItem.VoiceUser(voiceUser2, ChannelUtils.x(channel) && !voiceUser2.isConnected(), z4 && m.areEqual(str, voiceUser2.getWatchingStream())));
                }
                arrayList.addAll(arrayList4);
            }
            if ((!arrayList3.isEmpty()) && callModel.canInvite()) {
                arrayList.add(CallParticipantsAdapter.ListItem.Invite.INSTANCE);
            }
            return arrayList;
        }

        public final boolean refreshStreams(List<? extends CallParticipantsAdapter.ListItem> list, Set<String> set, StoreApplicationStreamPreviews storeApplicationStreamPreviews) {
            ModelApplicationStream stream;
            m.checkNotNullParameter(list, "listItems");
            m.checkNotNullParameter(set, "fetchedPreviews");
            m.checkNotNullParameter(storeApplicationStreamPreviews, "storeApplicationStreamPreviews");
            boolean z2 = false;
            for (CallParticipantsAdapter.ListItem listItem : list) {
                if (listItem instanceof CallParticipantsAdapter.ListItem.VoiceUser) {
                    z2 = true;
                    StreamContext streamContext = ((CallParticipantsAdapter.ListItem.VoiceUser) listItem).getParticipant().getStreamContext();
                    String encodedStreamKey = (streamContext == null || (stream = streamContext.getStream()) == null) ? null : stream.getEncodedStreamKey();
                    if (encodedStreamKey != null && !set.contains(encodedStreamKey)) {
                        storeApplicationStreamPreviews.fetchStreamPreviewIfNotFetching(streamContext);
                        set.add(encodedStreamKey);
                    }
                }
            }
            return z2;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }
}
