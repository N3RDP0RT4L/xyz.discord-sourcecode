package com.discord.widgets.voice.fullscreen;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.discord.databinding.WidgetStartCallSheetBinding;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetStartCallSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetStartCallSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetStartCallSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetStartCallSheet$binding$2 extends k implements Function1<View, WidgetStartCallSheetBinding> {
    public static final WidgetStartCallSheet$binding$2 INSTANCE = new WidgetStartCallSheet$binding$2();

    public WidgetStartCallSheet$binding$2() {
        super(1, WidgetStartCallSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetStartCallSheetBinding;", 0);
    }

    public final WidgetStartCallSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.start_call_sheet_video_item;
        TextView textView = (TextView) view.findViewById(R.id.start_call_sheet_video_item);
        if (textView != null) {
            i = R.id.start_call_sheet_voice_item;
            TextView textView2 = (TextView) view.findViewById(R.id.start_call_sheet_voice_item);
            if (textView2 != null) {
                return new WidgetStartCallSheetBinding((LinearLayout) view, textView, textView2);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
