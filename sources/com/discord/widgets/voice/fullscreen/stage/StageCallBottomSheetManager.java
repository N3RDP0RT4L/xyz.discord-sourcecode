package com.discord.widgets.voice.fullscreen.stage;

import andhook.lib.HookHelper;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.stores.StoreStream;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.sheet.WidgetStageAudienceNoticeBottomSheet;
import com.discord.widgets.stage.sheet.WidgetStageModeratorJoinBottomSheet;
import com.discord.widgets.stage.sheet.WidgetStageStartEventBottomSheet;
import com.discord.widgets.stage.start.StageEventsGuildsFeatureFlag;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
/* compiled from: StageCallBottomSheetManager.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\b¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/stage/StageCallBottomSheetManager;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Lcom/discord/api/channel/Channel;", "channel", "", "configureBottomSheet", "(Landroidx/fragment/app/FragmentManager;Lcom/discord/api/channel/Channel;)Z", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageCallBottomSheetManager {
    public final boolean configureBottomSheet(FragmentManager fragmentManager, Channel channel) {
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(channel, "channel");
        StoreStream.Companion companion = StoreStream.Companion;
        Map map = (Map) a.u0(channel, companion.getVoiceStates().get());
        boolean z2 = false;
        if (map != null) {
            StageRoles stageRoles = companion.getStageChannels().m10getMyRolesvisDeB4(channel.h());
            long id2 = companion.getUsers().getMe().getId();
            if (ChannelUtils.z(channel) && stageRoles != null && map.containsKey(Long.valueOf(id2))) {
                if (StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl()) && companion.getStageInstances().getStageInstanceForChannel(channel.h()) == null) {
                    z2 = true;
                }
                if (StageRoles.m24isAudienceimpl(stageRoles.m29unboximpl())) {
                    WidgetStageAudienceNoticeBottomSheet.Companion.show(fragmentManager, channel.h());
                } else if (!StageEventsGuildsFeatureFlag.Companion.getINSTANCE().canGuildAccessStageEvents(channel.f())) {
                    if (z2) {
                        WidgetStageStartEventBottomSheet.Companion.show$default(WidgetStageStartEventBottomSheet.Companion, fragmentManager, channel.h(), null, 4, null);
                    } else if (StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl()) && !StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl())) {
                        WidgetStageModeratorJoinBottomSheet.Companion.show(fragmentManager, channel.h());
                    }
                }
                return true;
            }
        }
        return false;
    }
}
