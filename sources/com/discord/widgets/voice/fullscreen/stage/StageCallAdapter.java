package com.discord.widgets.voice.fullscreen.stage;

import andhook.lib.HookHelper;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.mg_recycler.MGRecyclerAdapterSimple;
import com.discord.utilities.mg_recycler.MGRecyclerViewHolder;
import com.discord.views.calls.VideoCallParticipantView;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import com.discord.widgets.voice.fullscreen.CallParticipant;
import d0.z.d.m;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: StageCallAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 C2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001CB\u0097\u0001\u0012\u0006\u0010@\u001a\u00020?\u0012\u0006\u0010=\u001a\u00020<\u0012\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\b0\u001b\u0012\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\b0\u001b\u0012\u0016\u00105\u001a\u0012\u0012\b\u0012\u000603j\u0002`4\u0012\u0004\u0012\u00020\b0\u001b\u0012\u001e\u0010*\u001a\u001a\u0012\u0004\u0012\u00020'\u0012\u0006\u0012\u0004\u0018\u00010(\u0012\u0004\u0012\u00020\b0&j\u0002`)\u0012\u001e\u00101\u001a\u001a\u0012\u0004\u0012\u00020'\u0012\u0006\u0012\u0004\u0018\u00010(\u0012\u0004\u0012\u00020\b0&j\u0002`)¢\u0006\u0004\bA\u0010BJ#\u0010\t\u001a\u00020\b2\n\u0010\u0005\u001a\u00060\u0003j\u0002`\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\u0002H\u0000¢\u0006\u0004\b\f\u0010\rJ+\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0011H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J#\u0010\u0019\u001a\u00020\b2\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0011¢\u0006\u0004\b\u0019\u0010\u001aR%\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\b0\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0019\u0010\"\u001a\u00020!8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%R1\u0010*\u001a\u001a\u0012\u0004\u0012\u00020'\u0012\u0006\u0012\u0004\u0018\u00010(\u0012\u0004\u0012\u00020\b0&j\u0002`)8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R%\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\b0\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010\u001e\u001a\u0004\b/\u0010 R\u0016\u0010\u0018\u001a\u00020\u00118\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u00100R1\u00101\u001a\u001a\u0012\u0004\u0012\u00020'\u0012\u0006\u0012\u0004\u0018\u00010(\u0012\u0004\u0012\u00020\b0&j\u0002`)8\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010+\u001a\u0004\b2\u0010-R)\u00105\u001a\u0012\u0012\b\u0012\u000603j\u0002`4\u0012\u0004\u0012\u00020\b0\u001b8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010\u001e\u001a\u0004\b6\u0010 R\u0019\u00108\u001a\u0002078\u0006@\u0006¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010;R\u0016\u0010=\u001a\u00020<8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b=\u0010>¨\u0006D"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;", "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem;", "", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/api/channel/Channel;", "channel", "", "openWidgetUserSheet", "(JLcom/discord/api/channel/Channel;)V", "item", "onItemClick$app_productionGoogleRelease", "(Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem;)V", "onItemClick", "Landroid/view/ViewGroup;", "parent", "", "viewType", "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;", "", "data", "numSpeakers", "setData", "(Ljava/util/List;I)V", "Lkotlin/Function1;", "Lcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;", "onMediaParticipantLongClicked", "Lkotlin/jvm/functions/Function1;", "getOnMediaParticipantLongClicked", "()Lkotlin/jvm/functions/Function1;", "Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "spanSizeLookup", "Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "getSpanSizeLookup", "()Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "Lkotlin/Function2;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamFps;", "Lcom/discord/views/calls/StreamQualityCallback;", "onStreamQualityIndicatorShown", "Lkotlin/jvm/functions/Function2;", "getOnStreamQualityIndicatorShown", "()Lkotlin/jvm/functions/Function2;", "onMediaParticipantTapped", "getOnMediaParticipantTapped", "I", "onStreamQualityIndicatorClick", "getOnStreamQualityIndicatorClick", "", "Lcom/discord/primitives/StreamKey;", "onWatchStreamClicked", "getOnWatchStreamClicked", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "itemDecoration", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "getItemDecoration", "()Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", HookHelper.constructorName, "(Landroidx/recyclerview/widget/RecyclerView;Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StageCallAdapter extends MGRecyclerAdapterSimple<StageCallItem> {
    public static final int AUDIENCE_SPAN_COUNT = 3;
    public static final Companion Companion = new Companion(null);
    public static final int SPAN_COUNT = 12;
    public static final int SPEAKER_FULL_SPAN_COUNT = 12;
    public static final int SPEAKER_HALF_SPAN_COUNT = 6;
    public static final int SPEAKER_THIRD_SPAN_COUNT = 4;
    private final FragmentManager fragmentManager;
    private int numSpeakers;
    private final Function1<CallParticipant.UserOrStreamParticipant, Unit> onMediaParticipantLongClicked;
    private final Function1<CallParticipant.UserOrStreamParticipant, Unit> onMediaParticipantTapped;
    private final Function2<VideoCallParticipantView.StreamResolution, VideoCallParticipantView.StreamFps, Unit> onStreamQualityIndicatorClick;
    private final Function2<VideoCallParticipantView.StreamResolution, VideoCallParticipantView.StreamFps, Unit> onStreamQualityIndicatorShown;
    private final Function1<String, Unit> onWatchStreamClicked;
    private final GridLayoutManager.SpanSizeLookup spanSizeLookup = new GridLayoutManager.SpanSizeLookup() { // from class: com.discord.widgets.voice.fullscreen.stage.StageCallAdapter$spanSizeLookup$1
        @Override // androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
        public int getSpanSize(int i) {
            List internalData;
            int i2;
            internalData = StageCallAdapter.this.getInternalData();
            int type = ((StageCallItem) internalData.get(i)).getType();
            if (type == 1) {
                i2 = StageCallAdapter.this.numSpeakers;
                if (i2 != 1) {
                    return i2 != 2 ? 4 : 6;
                }
            } else if (type == 3) {
                return 3;
            }
            return 12;
        }
    };
    private final RecyclerView.ItemDecoration itemDecoration = new RecyclerView.ItemDecoration() { // from class: com.discord.widgets.voice.fullscreen.stage.StageCallAdapter$itemDecoration$1
        @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
        public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
            List internalData;
            int i;
            int i2;
            m.checkNotNullParameter(rect, "outRect");
            m.checkNotNullParameter(view, "view");
            m.checkNotNullParameter(recyclerView, "parent");
            m.checkNotNullParameter(state, "state");
            int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
            if (childAdapterPosition != -1) {
                internalData = StageCallAdapter.this.getInternalData();
                if (((StageCallItem) internalData.get(childAdapterPosition)).getType() == 1) {
                    ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                    Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.recyclerview.widget.GridLayoutManager.LayoutParams");
                    int spanIndex = ((GridLayoutManager.LayoutParams) layoutParams).getSpanIndex();
                    i = StageCallAdapter.this.numSpeakers;
                    if (i == 2 && spanIndex == 0) {
                        rect.right = DimenUtils.dpToPixels(4);
                    } else if (spanIndex == 6) {
                        rect.left = DimenUtils.dpToPixels(4);
                    } else {
                        i2 = StageCallAdapter.this.numSpeakers;
                        if (i2 > 2 && spanIndex == 0) {
                            rect.right = DimenUtils.dpToPixels(5.33f);
                        } else if (spanIndex == 8) {
                            rect.left = DimenUtils.dpToPixels(5.33f);
                        } else if (spanIndex == 4) {
                            rect.set(DimenUtils.dpToPixels(2.67f), 0, DimenUtils.dpToPixels(2.67f), 0);
                        }
                    }
                }
            }
        }
    };

    /* compiled from: StageCallAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004R\u0016\u0010\u0007\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0007\u0010\u0004R\u0016\u0010\b\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\b\u0010\u0004¨\u0006\u000b"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter$Companion;", "", "", "AUDIENCE_SPAN_COUNT", "I", "SPAN_COUNT", "SPEAKER_FULL_SPAN_COUNT", "SPEAKER_HALF_SPAN_COUNT", "SPEAKER_THIRD_SPAN_COUNT", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARN: Multi-variable type inference failed */
    public StageCallAdapter(RecyclerView recyclerView, FragmentManager fragmentManager, Function1<? super CallParticipant.UserOrStreamParticipant, Unit> function1, Function1<? super CallParticipant.UserOrStreamParticipant, Unit> function12, Function1<? super String, Unit> function13, Function2<? super VideoCallParticipantView.StreamResolution, ? super VideoCallParticipantView.StreamFps, Unit> function2, Function2<? super VideoCallParticipantView.StreamResolution, ? super VideoCallParticipantView.StreamFps, Unit> function22) {
        super(recyclerView, false, 2, null);
        m.checkNotNullParameter(recyclerView, "recyclerView");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(function1, "onMediaParticipantTapped");
        m.checkNotNullParameter(function12, "onMediaParticipantLongClicked");
        m.checkNotNullParameter(function13, "onWatchStreamClicked");
        m.checkNotNullParameter(function2, "onStreamQualityIndicatorShown");
        m.checkNotNullParameter(function22, "onStreamQualityIndicatorClick");
        this.fragmentManager = fragmentManager;
        this.onMediaParticipantTapped = function1;
        this.onMediaParticipantLongClicked = function12;
        this.onWatchStreamClicked = function13;
        this.onStreamQualityIndicatorShown = function2;
        this.onStreamQualityIndicatorClick = function22;
    }

    private final void openWidgetUserSheet(long j, Channel channel) {
        WidgetUserSheet.Companion.show$default(WidgetUserSheet.Companion, j, Long.valueOf(channel.h()), this.fragmentManager, Long.valueOf(channel.f()), Boolean.TRUE, null, null, 96, null);
    }

    public final RecyclerView.ItemDecoration getItemDecoration() {
        return this.itemDecoration;
    }

    public final Function1<CallParticipant.UserOrStreamParticipant, Unit> getOnMediaParticipantLongClicked() {
        return this.onMediaParticipantLongClicked;
    }

    public final Function1<CallParticipant.UserOrStreamParticipant, Unit> getOnMediaParticipantTapped() {
        return this.onMediaParticipantTapped;
    }

    public final Function2<VideoCallParticipantView.StreamResolution, VideoCallParticipantView.StreamFps, Unit> getOnStreamQualityIndicatorClick() {
        return this.onStreamQualityIndicatorClick;
    }

    public final Function2<VideoCallParticipantView.StreamResolution, VideoCallParticipantView.StreamFps, Unit> getOnStreamQualityIndicatorShown() {
        return this.onStreamQualityIndicatorShown;
    }

    public final Function1<String, Unit> getOnWatchStreamClicked() {
        return this.onWatchStreamClicked;
    }

    public final GridLayoutManager.SpanSizeLookup getSpanSizeLookup() {
        return this.spanSizeLookup;
    }

    public final void onItemClick$app_productionGoogleRelease(StageCallItem stageCallItem) {
        m.checkNotNullParameter(stageCallItem, "item");
        if (stageCallItem instanceof StageCallParticipantItem) {
            StageCallParticipantItem stageCallParticipantItem = (StageCallParticipantItem) stageCallItem;
            openWidgetUserSheet(stageCallParticipantItem.getVoiceUser().getUser().getId(), stageCallParticipantItem.getChannel());
        }
    }

    public final void setData(List<? extends StageCallItem> list, int i) {
        m.checkNotNullParameter(list, "data");
        this.numSpeakers = i;
        super.setData(list);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public MGRecyclerViewHolder<StageCallAdapter, StageCallItem> onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        switch (i) {
            case 0:
                return new DetailsViewHolder(this);
            case 1:
                return new SpeakerViewHolder(this);
            case 2:
                return new AudienceHeaderViewHolder(this);
            case 3:
                return new AudienceViewHolder(this);
            case 4:
                return new DividerViewHolder(this);
            case 5:
                return new PrestartDetailsViewHolder(this);
            case 6:
                return new MediaViewHolder(this);
            default:
                throw new IllegalStateException(a.p("Invalid view type: ", i));
        }
    }
}
