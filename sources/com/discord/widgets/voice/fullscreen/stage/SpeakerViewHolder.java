package com.discord.widgets.voice.fullscreen.stage;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import b.a.y.k0.g;
import b.a.y.k0.h;
import b.a.y.k0.i;
import com.discord.databinding.WidgetStageChannelSpeakerBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.colors.RepresentativeColorsKt;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.views.calls.StageCallSpeakerView;
import com.discord.widgets.voice.fullscreen.stage.StageCallItem;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import rx.Observable;
import rx.Subscription;
import xyz.discord.R;
/* compiled from: StageCallViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/stage/SpeakerViewHolder;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/voice/fullscreen/stage/StageCallItem;)V", "Lcom/discord/databinding/WidgetStageChannelSpeakerBinding;", "binding", "Lcom/discord/databinding/WidgetStageChannelSpeakerBinding;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class SpeakerViewHolder extends StageCallViewHolder {
    private final WidgetStageChannelSpeakerBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public SpeakerViewHolder(StageCallAdapter stageCallAdapter) {
        super(R.layout.widget_stage_channel_speaker, stageCallAdapter, null);
        m.checkNotNullParameter(stageCallAdapter, "adapter");
        View view = this.itemView;
        StageCallSpeakerView stageCallSpeakerView = (StageCallSpeakerView) view.findViewById(R.id.stage_channel_speaker_container);
        if (stageCallSpeakerView != null) {
            WidgetStageChannelSpeakerBinding widgetStageChannelSpeakerBinding = new WidgetStageChannelSpeakerBinding((ConstraintLayout) view, stageCallSpeakerView);
            m.checkNotNullExpressionValue(widgetStageChannelSpeakerBinding, "WidgetStageChannelSpeakerBinding.bind(itemView)");
            this.binding = widgetStageChannelSpeakerBinding;
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.stage_channel_speaker_container)));
    }

    public void onConfigure(int i, StageCallItem stageCallItem) {
        int i2;
        m.checkNotNullParameter(stageCallItem, "data");
        super.onConfigure(i, (int) stageCallItem);
        StageCallItem.SpeakerItem speakerItem = (StageCallItem.SpeakerItem) stageCallItem;
        StageCallSpeakerView stageCallSpeakerView = this.binding.f2628b;
        m.checkNotNullExpressionValue(stageCallSpeakerView, "binding.stageChannelSpeakerContainer");
        ViewGroup.LayoutParams layoutParams = stageCallSpeakerView.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
        ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
        int speakersPerRow = speakerItem.getSpeakersPerRow();
        int i3 = 0;
        if (speakersPerRow == 1) {
            i2 = 0;
        } else if (speakersPerRow != 2) {
            ConstraintLayout constraintLayout = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout, "binding.root");
            i2 = constraintLayout.getResources().getDimensionPixelSize(R.dimen.speaker_height_three_up);
        } else {
            ConstraintLayout constraintLayout2 = this.binding.a;
            m.checkNotNullExpressionValue(constraintLayout2, "binding.root");
            i2 = constraintLayout2.getResources().getDimensionPixelSize(R.dimen.speaker_height_two_up);
        }
        ((ViewGroup.MarginLayoutParams) layoutParams2).height = i2;
        layoutParams2.dimensionRatio = speakerItem.getSpeakersPerRow() != 1 ? null : "H,16:9";
        stageCallSpeakerView.setLayoutParams(layoutParams2);
        StageCallSpeakerView stageCallSpeakerView2 = this.binding.f2628b;
        StoreVoiceParticipants.VoiceUser voiceUser = speakerItem.getVoiceUser();
        boolean isModerator = speakerItem.isModerator();
        boolean isBlocked = speakerItem.isBlocked();
        Objects.requireNonNull(stageCallSpeakerView2);
        m.checkNotNullParameter(voiceUser, "voiceUser");
        String colorId = RepresentativeColorsKt.getColorId(voiceUser.getUser());
        stageCallSpeakerView2.j.f194b.setOnBitmapLoadedListener(new g(colorId));
        stageCallSpeakerView2.j.h.setPulsing(voiceUser.isSpeaking());
        stageCallSpeakerView2.j.f194b.a(voiceUser, R.dimen.avatar_size_hero);
        stageCallSpeakerView2.j.f194b.setRingMargin(DimenUtils.dpToPixels(2));
        ImageView imageView = stageCallSpeakerView2.j.f;
        m.checkNotNullExpressionValue(imageView, "binding.stageChannelSpeakerMuteIndicator");
        imageView.setVisibility(voiceUser.isMuted() && !voiceUser.isDeafened() ? 0 : 8);
        ImageView imageView2 = stageCallSpeakerView2.j.d;
        m.checkNotNullExpressionValue(imageView2, "binding.stageChannelSpeakerDeafenIndicator");
        imageView2.setVisibility(voiceUser.isDeafened() ? 0 : 8);
        ImageView imageView3 = stageCallSpeakerView2.j.e;
        m.checkNotNullExpressionValue(imageView3, "binding.stageChannelSpeakerModIndicator");
        imageView3.setVisibility(isModerator ? 0 : 8);
        TextView textView = stageCallSpeakerView2.j.g;
        m.checkNotNullExpressionValue(textView, "binding.stageChannelSpeakerName");
        textView.setText(voiceUser.getDisplayName());
        ImageView imageView4 = stageCallSpeakerView2.j.c;
        m.checkNotNullExpressionValue(imageView4, "binding.stageChannelSpeakerBlocked");
        if (!isBlocked) {
            i3 = 8;
        }
        imageView4.setVisibility(i3);
        Subscription subscription = stageCallSpeakerView2.k;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        Observable<R> F = RepresentativeColorsKt.getUserRepresentativeColors().observeRepresentativeColor(colorId).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable Z = F.Z(1);
        m.checkNotNullExpressionValue(Z, "UserRepresentativeColors…erNull()\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(Z, StageCallSpeakerView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new h(stageCallSpeakerView2), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new i(stageCallSpeakerView2));
    }
}
