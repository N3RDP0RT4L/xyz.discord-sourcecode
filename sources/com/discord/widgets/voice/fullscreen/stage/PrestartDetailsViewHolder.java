package com.discord.widgets.voice.fullscreen.stage;

import andhook.lib.HookHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.discord.databinding.WidgetStageChannelPrestartDetailsBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.widgets.voice.fullscreen.stage.StageCallItem;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: StageCallViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/stage/PrestartDetailsViewHolder;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/voice/fullscreen/stage/StageCallItem;)V", "Lcom/discord/databinding/WidgetStageChannelPrestartDetailsBinding;", "binding", "Lcom/discord/databinding/WidgetStageChannelPrestartDetailsBinding;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PrestartDetailsViewHolder extends StageCallViewHolder {
    private final WidgetStageChannelPrestartDetailsBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PrestartDetailsViewHolder(StageCallAdapter stageCallAdapter) {
        super(R.layout.widget_stage_channel_prestart_details, stageCallAdapter, null);
        m.checkNotNullParameter(stageCallAdapter, "adapter");
        View view = this.itemView;
        int i = R.id.subtitle;
        TextView textView = (TextView) view.findViewById(R.id.subtitle);
        if (textView != null) {
            i = R.id.title;
            TextView textView2 = (TextView) view.findViewById(R.id.title);
            if (textView2 != null) {
                WidgetStageChannelPrestartDetailsBinding widgetStageChannelPrestartDetailsBinding = new WidgetStageChannelPrestartDetailsBinding((ConstraintLayout) view, textView, textView2);
                m.checkNotNullExpressionValue(widgetStageChannelPrestartDetailsBinding, "WidgetStageChannelPresta…ilsBinding.bind(itemView)");
                this.binding = widgetStageChannelPrestartDetailsBinding;
                return;
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    public void onConfigure(int i, StageCallItem stageCallItem) {
        int i2;
        m.checkNotNullParameter(stageCallItem, "data");
        super.onConfigure(i, (int) stageCallItem);
        StageCallItem.PreStartDetailsItem preStartDetailsItem = (StageCallItem.PreStartDetailsItem) stageCallItem;
        TextView textView = this.binding.c;
        m.checkNotNullExpressionValue(textView, "binding.title");
        textView.setText(preStartDetailsItem.getTitle());
        TextView textView2 = this.binding.f2627b;
        m.checkNotNullExpressionValue(textView2, "binding.subtitle");
        textView2.setText(preStartDetailsItem.getSubtitle());
        boolean z2 = true;
        boolean z3 = preStartDetailsItem.getNumSpeakers() > 0;
        if (preStartDetailsItem.getNumAudience() <= 0) {
            z2 = false;
        }
        ConstraintLayout constraintLayout = this.binding.a;
        m.checkNotNullExpressionValue(constraintLayout, "binding.root");
        ViewGroup.LayoutParams layoutParams = constraintLayout.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        if (z3 || !z2) {
            i2 = DimenUtils.dpToPixels(0);
        } else {
            i2 = DimenUtils.dpToPixels(24);
        }
        marginLayoutParams.bottomMargin = i2;
        constraintLayout.setLayoutParams(marginLayoutParams);
    }
}
