package com.discord.widgets.voice.fullscreen.stage;

import andhook.lib.HookHelper;
import android.view.View;
import androidx.cardview.widget.CardView;
import com.discord.databinding.WidgetStageChannelSpeakerMediaBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.calls.VideoCallParticipantView;
import com.discord.widgets.voice.fullscreen.grid.VideoCallGridAdapter;
import com.discord.widgets.voice.fullscreen.stage.StageCallItem;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import xyz.discord.R;
/* compiled from: StageCallViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0014¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/stage/MediaViewHolder;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallViewHolder;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "Lcom/discord/widgets/voice/fullscreen/stage/StageCallItem;", "data", "", "onConfigure", "(ILcom/discord/widgets/voice/fullscreen/stage/StageCallItem;)V", "Lcom/discord/databinding/WidgetStageChannelSpeakerMediaBinding;", "binding", "Lcom/discord/databinding/WidgetStageChannelSpeakerMediaBinding;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class MediaViewHolder extends StageCallViewHolder {
    private final WidgetStageChannelSpeakerMediaBinding binding;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public MediaViewHolder(StageCallAdapter stageCallAdapter) {
        super(R.layout.widget_stage_channel_speaker_media, stageCallAdapter, null);
        m.checkNotNullParameter(stageCallAdapter, "adapter");
        View view = this.itemView;
        VideoCallParticipantView videoCallParticipantView = (VideoCallParticipantView) view.findViewById(R.id.stage_channel_media_speaker_container);
        if (videoCallParticipantView != null) {
            WidgetStageChannelSpeakerMediaBinding widgetStageChannelSpeakerMediaBinding = new WidgetStageChannelSpeakerMediaBinding((CardView) view, videoCallParticipantView);
            m.checkNotNullExpressionValue(widgetStageChannelSpeakerMediaBinding, "WidgetStageChannelSpeake…diaBinding.bind(itemView)");
            this.binding = widgetStageChannelSpeakerMediaBinding;
            return;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(R.id.stage_channel_media_speaker_container)));
    }

    public static final /* synthetic */ StageCallAdapter access$getAdapter$p(MediaViewHolder mediaViewHolder) {
        return (StageCallAdapter) mediaViewHolder.adapter;
    }

    public void onConfigure(int i, StageCallItem stageCallItem) {
        m.checkNotNullParameter(stageCallItem, "data");
        super.onConfigure(i, (int) stageCallItem);
        final StageCallItem.MediaItem mediaItem = (StageCallItem.MediaItem) stageCallItem;
        VideoCallParticipantView videoCallParticipantView = this.binding.f2629b;
        m.checkNotNullExpressionValue(videoCallParticipantView, "binding.stageChannelMediaSpeakerContainer");
        videoCallParticipantView.c(mediaItem.getParticipantData(), null, false, new VideoCallGridAdapter.CallUiInsets(0, 0, 0, 0), true);
        videoCallParticipantView.setOnWatchStreamClicked(((StageCallAdapter) this.adapter).getOnWatchStreamClicked());
        videoCallParticipantView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.fullscreen.stage.MediaViewHolder$onConfigure$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MediaViewHolder.access$getAdapter$p(MediaViewHolder.this).getOnMediaParticipantTapped().invoke(mediaItem.getUserOrStreamParticipant());
            }
        });
        ViewExtensions.setOnLongClickListenerConsumeClick(videoCallParticipantView, new MediaViewHolder$onConfigure$2(this, mediaItem));
        Function2<VideoCallParticipantView.StreamResolution, VideoCallParticipantView.StreamFps, Unit> onStreamQualityIndicatorShown = ((StageCallAdapter) this.adapter).getOnStreamQualityIndicatorShown();
        Function2<VideoCallParticipantView.StreamResolution, VideoCallParticipantView.StreamFps, Unit> onStreamQualityIndicatorClick = ((StageCallAdapter) this.adapter).getOnStreamQualityIndicatorClick();
        m.checkNotNullParameter(onStreamQualityIndicatorShown, "onShown");
        m.checkNotNullParameter(onStreamQualityIndicatorClick, "onClicked");
        videoCallParticipantView.f2811y = onStreamQualityIndicatorShown;
        videoCallParticipantView.f2810x = onStreamQualityIndicatorClick;
    }
}
