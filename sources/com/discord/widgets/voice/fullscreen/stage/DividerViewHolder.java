package com.discord.widgets.voice.fullscreen.stage;

import andhook.lib.HookHelper;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: StageCallViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/stage/DividerViewHolder;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallViewHolder;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;", "adapter", HookHelper.constructorName, "(Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DividerViewHolder extends StageCallViewHolder {
    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public DividerViewHolder(StageCallAdapter stageCallAdapter) {
        super(R.layout.widget_stage_channel_divider, stageCallAdapter, null);
        m.checkNotNullParameter(stageCallAdapter, "adapter");
    }
}
