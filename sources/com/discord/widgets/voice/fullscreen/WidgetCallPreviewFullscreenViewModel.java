package com.discord.widgets.voice.fullscreen;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.user.NsfwAllowance;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.embeddedactivities.EmbeddedActivity;
import com.discord.models.user.MeUser;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import com.discord.stores.StoreApplication;
import com.discord.stores.StoreApplicationStreamPreviews;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreMentions;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.StoreUserSettings;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.voice.VoiceChannelJoinability;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel;
import com.discord.widgets.voice.model.CallModel;
import com.discord.widgets.voice.sheet.CallParticipantsAdapter;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func5;
import rx.subjects.PublishSubject;
/* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¦\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 I2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0005IJKLMB\u0087\u0001\u0012\n\u0010A\u001a\u00060\u0003j\u0002`\u0004\u0012\b\b\u0002\u0010'\u001a\u00020&\u0012\b\b\u0002\u0010?\u001a\u00020>\u0012\b\b\u0002\u0010D\u001a\u00020C\u0012\b\b\u0002\u00103\u001a\u000202\u0012\b\b\u0002\u0010<\u001a\u00020;\u0012\b\b\u0002\u0010$\u001a\u00020#\u0012\b\b\u0002\u00109\u001a\u000208\u0012\b\b\u0002\u0010!\u001a\u00020 \u0012\b\b\u0002\u00100\u001a\u00020/\u0012\u000e\b\u0002\u0010F\u001a\b\u0012\u0004\u0012\u00020\t0\r\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\bG\u0010HJ\u001b\u0010\u0007\u001a\u00020\u00062\n\u0010\u0005\u001a\u00060\u0003j\u0002`\u0004H\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0013\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\u0004\b\u000f\u0010\u0010J\r\u0010\u0011\u001a\u00020\u0006¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\u0006H\u0007¢\u0006\u0004\b\u0013\u0010\u0012J\u0019\u0010\u0016\u001a\u00020\u00062\b\b\u0002\u0010\u0015\u001a\u00020\u0014H\u0007¢\u0006\u0004\b\u0016\u0010\u0017J\u001b\u0010\u001b\u001a\u00020\u00062\n\u0010\u001a\u001a\u00060\u0018j\u0002`\u0019H\u0007¢\u0006\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001e\u001a\u00020\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010!\u001a\u00020 8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010$\u001a\u00020#8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010'\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R:\u0010+\u001a&\u0012\f\u0012\n **\u0004\u0018\u00010\u000e0\u000e **\u0012\u0012\f\u0012\n **\u0004\u0018\u00010\u000e0\u000e\u0018\u00010)0)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u0018\u0010-\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u0016\u00103\u001a\u0002028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00104R \u00106\u001a\f\u0012\b\u0012\u00060\u0018j\u0002`\u0019058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b6\u00107R\u0016\u00109\u001a\u0002088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b9\u0010:R\u0016\u0010<\u001a\u00020;8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b<\u0010=R\u0016\u0010?\u001a\u00020>8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b?\u0010@R\u001a\u0010A\u001a\u00060\u0003j\u0002`\u00048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bA\u0010BR\u0016\u0010D\u001a\u00020C8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010E¨\u0006N"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ViewState;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "joinVoiceChannel", "(J)V", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;", "storeState", "handleStoreState", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;)V", "Lrx/Observable;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event;", "observeEvents", "()Lrx/Observable;", "selectTextChannelAfterFinish", "()V", "onTextInVoiceTapped", "", "muted", "tryConnectToVoice", "(Z)V", "", "Lcom/discord/primitives/StreamKey;", "streamKey", "onStreamPreviewClicked", "(Ljava/lang/String;)V", "Lcom/discord/stores/StoreMediaSettings;", "mediaSettingsStore", "Lcom/discord/stores/StoreMediaSettings;", "Lcom/discord/stores/StoreApplication;", "storeApplication", "Lcom/discord/stores/StoreApplication;", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreChannelsSelected;", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "eventSubject", "Lrx/subjects/PublishSubject;", "mostRecentStoreState", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreUser;", "Lcom/discord/stores/StoreUserSettings;", "storeUserSettings", "Lcom/discord/stores/StoreUserSettings;", "", "fetchedPreviews", "Ljava/util/Set;", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "storeApplicationStreamPreviews", "Lcom/discord/stores/StoreApplicationStreamPreviews;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "storeVoiceChannelSelected", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/StoreMentions;", "storeMentions", "Lcom/discord/stores/StoreMentions;", "voiceChannelId", "J", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreNavigation;", "storeStateObservable", HookHelper.constructorName, "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreMentions;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/stores/StoreApplication;Lcom/discord/stores/StoreUser;Lrx/Observable;Lcom/discord/stores/StoreMediaSettings;)V", "Companion", "Event", "ParticipantsList", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallPreviewFullscreenViewModel extends AppViewModel<ViewState> {
    public static final Companion Companion = new Companion(null);
    private final PublishSubject<Event> eventSubject;
    private Set<String> fetchedPreviews;
    private final StoreMediaSettings mediaSettingsStore;
    private StoreState mostRecentStoreState;
    private final StoreApplication storeApplication;
    private final StoreApplicationStreamPreviews storeApplicationStreamPreviews;
    private final StoreChannels storeChannels;
    private final StoreChannelsSelected storeChannelsSelected;
    private final StoreMentions storeMentions;
    private final StoreNavigation storeNavigation;
    private final StoreUser storeUser;
    private final StoreUserSettings storeUserSettings;
    private final StoreVoiceChannelSelected storeVoiceChannelSelected;
    private long voiceChannelId;

    /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<StoreState, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(StoreState storeState) {
            invoke2(storeState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(StoreState storeState) {
            WidgetCallPreviewFullscreenViewModel widgetCallPreviewFullscreenViewModel = WidgetCallPreviewFullscreenViewModel.this;
            m.checkNotNullExpressionValue(storeState, "it");
            widgetCallPreviewFullscreenViewModel.handleStoreState(storeState);
        }
    }

    /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012JA\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "voiceChannelId", "Lcom/discord/stores/StoreUser;", "storeUser", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannelsSelected;", "storeChannelsSelected", "Lcom/discord/stores/StoreMentions;", "storeMentions", "Lrx/Observable;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;", "observeStoreState", "(JLcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreMentions;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<StoreState> observeStoreState(final long j, final StoreUser storeUser, StoreChannels storeChannels, final StoreChannelsSelected storeChannelsSelected, final StoreMentions storeMentions) {
            Observable Y = storeChannels.observeChannel(j).Y(new b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel$Companion$observeStoreState$1
                public final Observable<? extends WidgetCallPreviewFullscreenViewModel.StoreState> call(final Channel channel) {
                    Long l = null;
                    Observable observeMe$default = StoreUser.observeMe$default(StoreUser.this, false, 1, null);
                    Observable<Channel> observeSelectedChannel = storeChannelsSelected.observeSelectedChannel();
                    TextInVoiceFeatureFlag instance = TextInVoiceFeatureFlag.Companion.getINSTANCE();
                    if (channel != null) {
                        l = Long.valueOf(channel.f());
                    }
                    return Observable.g(observeMe$default, observeSelectedChannel, instance.observeEnabled(l), storeMentions.observeTotalMentions(), CallModel.Companion.get(j), new Func5<MeUser, Channel, Boolean, Integer, CallModel, WidgetCallPreviewFullscreenViewModel.StoreState>() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel$Companion$observeStoreState$1.1
                        public final WidgetCallPreviewFullscreenViewModel.StoreState call(MeUser meUser, Channel channel2, Boolean bool, Integer num, CallModel callModel) {
                            if (callModel == null || Channel.this == null || channel2 == null) {
                                return WidgetCallPreviewFullscreenViewModel.StoreState.Invalid.INSTANCE;
                            }
                            boolean z2 = !StoreStream.Companion.getGuildsNsfw().isGuildNsfwGateAgreed(Channel.this.f());
                            boolean o = Channel.this.o();
                            NsfwAllowance nsfwAllowance = meUser != null ? meUser.getNsfwAllowance() : null;
                            Channel channel3 = Channel.this;
                            m.checkNotNullExpressionValue(bool, "isTextInVoiceEnabled");
                            boolean booleanValue = bool.booleanValue();
                            m.checkNotNullExpressionValue(num, "totalMentionsCount");
                            return new WidgetCallPreviewFullscreenViewModel.StoreState.Valid(channel3, channel2, booleanValue, num.intValue(), z2, o, nsfwAllowance, callModel);
                        }
                    });
                }
            });
            m.checkNotNullExpressionValue(Y, "storeChannels.observeCha…  }\n          }\n        }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event;", "", HookHelper.constructorName, "()V", "LaunchVideoCall", "ShowGuildVideoAtCapacityDialog", "ShowOverlayNux", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event$ShowGuildVideoAtCapacityDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event$ShowOverlayNux;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event$LaunchVideoCall;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B#\u0012\n\u0010\n\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ0\u0010\f\u001a\u00020\u00002\f\b\u0002\u0010\n\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u000e\u0010\tJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\n\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R!\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0019\u001a\u0004\b\u001a\u0010\t¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event$LaunchVideoCall;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "Lcom/discord/primitives/StreamKey;", "component2", "()Ljava/lang/String;", "channelId", "autoTargetStreamKey", "copy", "(JLjava/lang/String;)Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event$LaunchVideoCall;", "toString", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getChannelId", "Ljava/lang/String;", "getAutoTargetStreamKey", HookHelper.constructorName, "(JLjava/lang/String;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class LaunchVideoCall extends Event {
            private final String autoTargetStreamKey;
            private final long channelId;

            public LaunchVideoCall(long j, String str) {
                super(null);
                this.channelId = j;
                this.autoTargetStreamKey = str;
            }

            public static /* synthetic */ LaunchVideoCall copy$default(LaunchVideoCall launchVideoCall, long j, String str, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = launchVideoCall.channelId;
                }
                if ((i & 2) != 0) {
                    str = launchVideoCall.autoTargetStreamKey;
                }
                return launchVideoCall.copy(j, str);
            }

            public final long component1() {
                return this.channelId;
            }

            public final String component2() {
                return this.autoTargetStreamKey;
            }

            public final LaunchVideoCall copy(long j, String str) {
                return new LaunchVideoCall(j, str);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof LaunchVideoCall)) {
                    return false;
                }
                LaunchVideoCall launchVideoCall = (LaunchVideoCall) obj;
                return this.channelId == launchVideoCall.channelId && m.areEqual(this.autoTargetStreamKey, launchVideoCall.autoTargetStreamKey);
            }

            public final String getAutoTargetStreamKey() {
                return this.autoTargetStreamKey;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public int hashCode() {
                int a = a0.a.a.b.a(this.channelId) * 31;
                String str = this.autoTargetStreamKey;
                return a + (str != null ? str.hashCode() : 0);
            }

            public String toString() {
                StringBuilder R = a.R("LaunchVideoCall(channelId=");
                R.append(this.channelId);
                R.append(", autoTargetStreamKey=");
                return a.H(R, this.autoTargetStreamKey, ")");
            }
        }

        /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event$ShowGuildVideoAtCapacityDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowGuildVideoAtCapacityDialog extends Event {
            public static final ShowGuildVideoAtCapacityDialog INSTANCE = new ShowGuildVideoAtCapacityDialog();

            private ShowGuildVideoAtCapacityDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event$ShowOverlayNux;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowOverlayNux extends Event {
            public static final ShowOverlayNux INSTANCE = new ShowOverlayNux();

            private ShowOverlayNux() {
                super(null);
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;", "", HookHelper.constructorName, "()V", "Empty", "ListItems", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList$ListItems;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList$Empty;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ParticipantsList {

        /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList$Empty;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;", "", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem;", "component1", "()Ljava/util/List;", "items", "copy", "(Ljava/util/List;)Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList$Empty;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Empty extends ParticipantsList {
            private final List<CallParticipantsAdapter.ListItem> items;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public Empty(List<? extends CallParticipantsAdapter.ListItem> list) {
                super(null);
                m.checkNotNullParameter(list, "items");
                this.items = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Empty copy$default(Empty empty, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = empty.items;
                }
                return empty.copy(list);
            }

            public final List<CallParticipantsAdapter.ListItem> component1() {
                return this.items;
            }

            public final Empty copy(List<? extends CallParticipantsAdapter.ListItem> list) {
                m.checkNotNullParameter(list, "items");
                return new Empty(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof Empty) && m.areEqual(this.items, ((Empty) obj).items);
                }
                return true;
            }

            public final List<CallParticipantsAdapter.ListItem> getItems() {
                return this.items;
            }

            public int hashCode() {
                List<CallParticipantsAdapter.ListItem> list = this.items;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("Empty(items="), this.items, ")");
            }
        }

        /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J \u0010\u0007\u001a\u00020\u00002\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0001¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0014\u001a\u0004\b\u0015\u0010\u0005¨\u0006\u0018"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList$ListItems;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;", "", "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem;", "component1", "()Ljava/util/List;", "items", "copy", "(Ljava/util/List;)Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList$ListItems;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getItems", HookHelper.constructorName, "(Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ListItems extends ParticipantsList {
            private final List<CallParticipantsAdapter.ListItem> items;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            /* JADX WARN: Multi-variable type inference failed */
            public ListItems(List<? extends CallParticipantsAdapter.ListItem> list) {
                super(null);
                m.checkNotNullParameter(list, "items");
                this.items = list;
            }

            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ ListItems copy$default(ListItems listItems, List list, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = listItems.items;
                }
                return listItems.copy(list);
            }

            public final List<CallParticipantsAdapter.ListItem> component1() {
                return this.items;
            }

            public final ListItems copy(List<? extends CallParticipantsAdapter.ListItem> list) {
                m.checkNotNullParameter(list, "items");
                return new ListItems(list);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ListItems) && m.areEqual(this.items, ((ListItems) obj).items);
                }
                return true;
            }

            public final List<CallParticipantsAdapter.ListItem> getItems() {
                return this.items;
            }

            public int hashCode() {
                List<CallParticipantsAdapter.ListItem> list = this.items;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public String toString() {
                return a.K(a.R("ListItems(items="), this.items, ")");
            }
        }

        private ParticipantsList() {
        }

        public /* synthetic */ ParticipantsList(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState$Invalid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState$Valid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState$Invalid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends StoreState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0006\u0012\u0006\u0010\u0017\u001a\u00020\t\u0012\u0006\u0010\u0018\u001a\u00020\u0006\u0012\u0006\u0010\u0019\u001a\u00020\u0006\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u000e\u0012\u0006\u0010\u001b\u001a\u00020\u0011¢\u0006\u0004\b0\u00101J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\f\u0010\bJ\u0010\u0010\r\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\r\u0010\bJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013Jb\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00022\b\b\u0002\u0010\u0016\u001a\u00020\u00062\b\b\u0002\u0010\u0017\u001a\u00020\t2\b\b\u0002\u0010\u0018\u001a\u00020\u00062\b\b\u0002\u0010\u0019\u001a\u00020\u00062\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u0010\u001b\u001a\u00020\u0011HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\u001eHÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b!\u0010\u000bJ\u001a\u0010$\u001a\u00020\u00062\b\u0010#\u001a\u0004\u0018\u00010\"HÖ\u0003¢\u0006\u0004\b$\u0010%R\u0019\u0010\u0016\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b\u0016\u0010\bR\u0019\u0010\u0019\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010&\u001a\u0004\b\u0019\u0010\bR\u0019\u0010\u0015\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010'\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u0017\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010)\u001a\u0004\b*\u0010\u000bR\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010+\u001a\u0004\b,\u0010\u0010R\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010'\u001a\u0004\b-\u0010\u0004R\u0019\u0010\u0018\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010&\u001a\u0004\b\u0018\u0010\bR\u0019\u0010\u001b\u001a\u00020\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010.\u001a\u0004\b/\u0010\u0013¨\u00062"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState$Valid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState;", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "component2", "", "component3", "()Z", "", "component4", "()I", "component5", "component6", "Lcom/discord/api/user/NsfwAllowance;", "component7", "()Lcom/discord/api/user/NsfwAllowance;", "Lcom/discord/widgets/voice/model/CallModel;", "component8", "()Lcom/discord/widgets/voice/model/CallModel;", "voiceChannel", "selectedTextChannel", "isTextInVoiceEnabled", "totalMentionsCount", "isNsfwUnconsented", "isChannelNsfw", "nsfwAllowed", "callModel", "copy", "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;ZIZZLcom/discord/api/user/NsfwAllowance;Lcom/discord/widgets/voice/model/CallModel;)Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$StoreState$Valid;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Lcom/discord/api/channel/Channel;", "getSelectedTextChannel", "I", "getTotalMentionsCount", "Lcom/discord/api/user/NsfwAllowance;", "getNsfwAllowed", "getVoiceChannel", "Lcom/discord/widgets/voice/model/CallModel;", "getCallModel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;Lcom/discord/api/channel/Channel;ZIZZLcom/discord/api/user/NsfwAllowance;Lcom/discord/widgets/voice/model/CallModel;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends StoreState {
            private final CallModel callModel;
            private final boolean isChannelNsfw;
            private final boolean isNsfwUnconsented;
            private final boolean isTextInVoiceEnabled;
            private final NsfwAllowance nsfwAllowed;
            private final Channel selectedTextChannel;
            private final int totalMentionsCount;
            private final Channel voiceChannel;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public Valid(Channel channel, Channel channel2, boolean z2, int i, boolean z3, boolean z4, NsfwAllowance nsfwAllowance, CallModel callModel) {
                super(null);
                m.checkNotNullParameter(channel, "voiceChannel");
                m.checkNotNullParameter(channel2, "selectedTextChannel");
                m.checkNotNullParameter(callModel, "callModel");
                this.voiceChannel = channel;
                this.selectedTextChannel = channel2;
                this.isTextInVoiceEnabled = z2;
                this.totalMentionsCount = i;
                this.isNsfwUnconsented = z3;
                this.isChannelNsfw = z4;
                this.nsfwAllowed = nsfwAllowance;
                this.callModel = callModel;
            }

            public final Channel component1() {
                return this.voiceChannel;
            }

            public final Channel component2() {
                return this.selectedTextChannel;
            }

            public final boolean component3() {
                return this.isTextInVoiceEnabled;
            }

            public final int component4() {
                return this.totalMentionsCount;
            }

            public final boolean component5() {
                return this.isNsfwUnconsented;
            }

            public final boolean component6() {
                return this.isChannelNsfw;
            }

            public final NsfwAllowance component7() {
                return this.nsfwAllowed;
            }

            public final CallModel component8() {
                return this.callModel;
            }

            public final Valid copy(Channel channel, Channel channel2, boolean z2, int i, boolean z3, boolean z4, NsfwAllowance nsfwAllowance, CallModel callModel) {
                m.checkNotNullParameter(channel, "voiceChannel");
                m.checkNotNullParameter(channel2, "selectedTextChannel");
                m.checkNotNullParameter(callModel, "callModel");
                return new Valid(channel, channel2, z2, i, z3, z4, nsfwAllowance, callModel);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.voiceChannel, valid.voiceChannel) && m.areEqual(this.selectedTextChannel, valid.selectedTextChannel) && this.isTextInVoiceEnabled == valid.isTextInVoiceEnabled && this.totalMentionsCount == valid.totalMentionsCount && this.isNsfwUnconsented == valid.isNsfwUnconsented && this.isChannelNsfw == valid.isChannelNsfw && m.areEqual(this.nsfwAllowed, valid.nsfwAllowed) && m.areEqual(this.callModel, valid.callModel);
            }

            public final CallModel getCallModel() {
                return this.callModel;
            }

            public final NsfwAllowance getNsfwAllowed() {
                return this.nsfwAllowed;
            }

            public final Channel getSelectedTextChannel() {
                return this.selectedTextChannel;
            }

            public final int getTotalMentionsCount() {
                return this.totalMentionsCount;
            }

            public final Channel getVoiceChannel() {
                return this.voiceChannel;
            }

            public int hashCode() {
                Channel channel = this.voiceChannel;
                int i = 0;
                int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
                Channel channel2 = this.selectedTextChannel;
                int hashCode2 = (hashCode + (channel2 != null ? channel2.hashCode() : 0)) * 31;
                boolean z2 = this.isTextInVoiceEnabled;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (((hashCode2 + i3) * 31) + this.totalMentionsCount) * 31;
                boolean z3 = this.isNsfwUnconsented;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (i5 + i6) * 31;
                boolean z4 = this.isChannelNsfw;
                if (!z4) {
                    i2 = z4 ? 1 : 0;
                }
                int i9 = (i8 + i2) * 31;
                NsfwAllowance nsfwAllowance = this.nsfwAllowed;
                int hashCode3 = (i9 + (nsfwAllowance != null ? nsfwAllowance.hashCode() : 0)) * 31;
                CallModel callModel = this.callModel;
                if (callModel != null) {
                    i = callModel.hashCode();
                }
                return hashCode3 + i;
            }

            public final boolean isChannelNsfw() {
                return this.isChannelNsfw;
            }

            public final boolean isNsfwUnconsented() {
                return this.isNsfwUnconsented;
            }

            public final boolean isTextInVoiceEnabled() {
                return this.isTextInVoiceEnabled;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(voiceChannel=");
                R.append(this.voiceChannel);
                R.append(", selectedTextChannel=");
                R.append(this.selectedTextChannel);
                R.append(", isTextInVoiceEnabled=");
                R.append(this.isTextInVoiceEnabled);
                R.append(", totalMentionsCount=");
                R.append(this.totalMentionsCount);
                R.append(", isNsfwUnconsented=");
                R.append(this.isNsfwUnconsented);
                R.append(", isChannelNsfw=");
                R.append(this.isChannelNsfw);
                R.append(", nsfwAllowed=");
                R.append(this.nsfwAllowed);
                R.append(", callModel=");
                R.append(this.callModel);
                R.append(")");
                return R.toString();
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetCallPreviewFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\"\b\u0086\b\u0018\u00002\u00020\u0001B[\u0012\u0006\u0010\u0018\u001a\u00020\u0002\u0012\u0006\u0010\u0019\u001a\u00020\u0005\u0012\u0006\u0010\u001a\u001a\u00020\u0005\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\u001c\u001a\u00020\f\u0012\u0006\u0010\u001d\u001a\u00020\u0005\u0012\u0006\u0010\u001e\u001a\u00020\u0010\u0012\u0006\u0010\u001f\u001a\u00020\u0005\u0012\u0006\u0010 \u001a\u00020\u0005\u0012\b\u0010!\u001a\u0004\u0018\u00010\u0015¢\u0006\u0004\b5\u00106J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0007J\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0007J\u0010\u0010\u0014\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0014\u0010\u0007J\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017Jx\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0018\u001a\u00020\u00022\b\b\u0002\u0010\u0019\u001a\u00020\u00052\b\b\u0002\u0010\u001a\u001a\u00020\u00052\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\u001c\u001a\u00020\f2\b\b\u0002\u0010\u001d\u001a\u00020\u00052\b\b\u0002\u0010\u001e\u001a\u00020\u00102\b\b\u0002\u0010\u001f\u001a\u00020\u00052\b\b\u0002\u0010 \u001a\u00020\u00052\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\u0015HÆ\u0001¢\u0006\u0004\b\"\u0010#J\u0010\u0010$\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b$\u0010\u000bJ\u0010\u0010%\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b%\u0010\u000eJ\u001a\u0010'\u001a\u00020\u00052\b\u0010&\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b'\u0010(R\u0019\u0010\u001e\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010)\u001a\u0004\b*\u0010\u0012R\u0019\u0010\u001a\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010+\u001a\u0004\b,\u0010\u0007R\u0019\u0010\u001c\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010-\u001a\u0004\b.\u0010\u000eR\u001b\u0010\u001b\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010/\u001a\u0004\b0\u0010\u000bR\u0019\u0010 \u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b \u0010+\u001a\u0004\b \u0010\u0007R\u0019\u0010\u001f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010+\u001a\u0004\b\u001f\u0010\u0007R\u001b\u0010!\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b!\u00101\u001a\u0004\b2\u0010\u0017R\u0019\u0010\u001d\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010+\u001a\u0004\b\u001d\u0010\u0007R\u0019\u0010\u0019\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010+\u001a\u0004\b\u0019\u0010\u0007R\u0019\u0010\u0018\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u00103\u001a\u0004\b4\u0010\u0004¨\u00067"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ViewState;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "component3", "", "component4", "()Ljava/lang/String;", "", "component5", "()I", "component6", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;", "component7", "()Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;", "component8", "component9", "Lcom/discord/api/user/NsfwAllowance;", "component10", "()Lcom/discord/api/user/NsfwAllowance;", "voiceChannel", "isTextInVoiceChannelSelected", "textInVoiceEnabled", "titleText", "totalMentionsCount", "isConnectEnabled", "participantsList", "isNsfwUnconsented", "isChannelNsfw", "nsfwAllowed", "copy", "(Lcom/discord/api/channel/Channel;ZZLjava/lang/String;IZLcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;ZZLcom/discord/api/user/NsfwAllowance;)Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ViewState;", "toString", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;", "getParticipantsList", "Z", "getTextInVoiceEnabled", "I", "getTotalMentionsCount", "Ljava/lang/String;", "getTitleText", "Lcom/discord/api/user/NsfwAllowance;", "getNsfwAllowed", "Lcom/discord/api/channel/Channel;", "getVoiceChannel", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZZLjava/lang/String;IZLcom/discord/widgets/voice/fullscreen/WidgetCallPreviewFullscreenViewModel$ParticipantsList;ZZLcom/discord/api/user/NsfwAllowance;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ViewState {
        private final boolean isChannelNsfw;
        private final boolean isConnectEnabled;
        private final boolean isNsfwUnconsented;
        private final boolean isTextInVoiceChannelSelected;
        private final NsfwAllowance nsfwAllowed;
        private final ParticipantsList participantsList;
        private final boolean textInVoiceEnabled;
        private final String titleText;
        private final int totalMentionsCount;
        private final Channel voiceChannel;

        public ViewState(Channel channel, boolean z2, boolean z3, String str, int i, boolean z4, ParticipantsList participantsList, boolean z5, boolean z6, NsfwAllowance nsfwAllowance) {
            m.checkNotNullParameter(channel, "voiceChannel");
            m.checkNotNullParameter(participantsList, "participantsList");
            this.voiceChannel = channel;
            this.isTextInVoiceChannelSelected = z2;
            this.textInVoiceEnabled = z3;
            this.titleText = str;
            this.totalMentionsCount = i;
            this.isConnectEnabled = z4;
            this.participantsList = participantsList;
            this.isNsfwUnconsented = z5;
            this.isChannelNsfw = z6;
            this.nsfwAllowed = nsfwAllowance;
        }

        public final Channel component1() {
            return this.voiceChannel;
        }

        public final NsfwAllowance component10() {
            return this.nsfwAllowed;
        }

        public final boolean component2() {
            return this.isTextInVoiceChannelSelected;
        }

        public final boolean component3() {
            return this.textInVoiceEnabled;
        }

        public final String component4() {
            return this.titleText;
        }

        public final int component5() {
            return this.totalMentionsCount;
        }

        public final boolean component6() {
            return this.isConnectEnabled;
        }

        public final ParticipantsList component7() {
            return this.participantsList;
        }

        public final boolean component8() {
            return this.isNsfwUnconsented;
        }

        public final boolean component9() {
            return this.isChannelNsfw;
        }

        public final ViewState copy(Channel channel, boolean z2, boolean z3, String str, int i, boolean z4, ParticipantsList participantsList, boolean z5, boolean z6, NsfwAllowance nsfwAllowance) {
            m.checkNotNullParameter(channel, "voiceChannel");
            m.checkNotNullParameter(participantsList, "participantsList");
            return new ViewState(channel, z2, z3, str, i, z4, participantsList, z5, z6, nsfwAllowance);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ViewState)) {
                return false;
            }
            ViewState viewState = (ViewState) obj;
            return m.areEqual(this.voiceChannel, viewState.voiceChannel) && this.isTextInVoiceChannelSelected == viewState.isTextInVoiceChannelSelected && this.textInVoiceEnabled == viewState.textInVoiceEnabled && m.areEqual(this.titleText, viewState.titleText) && this.totalMentionsCount == viewState.totalMentionsCount && this.isConnectEnabled == viewState.isConnectEnabled && m.areEqual(this.participantsList, viewState.participantsList) && this.isNsfwUnconsented == viewState.isNsfwUnconsented && this.isChannelNsfw == viewState.isChannelNsfw && m.areEqual(this.nsfwAllowed, viewState.nsfwAllowed);
        }

        public final NsfwAllowance getNsfwAllowed() {
            return this.nsfwAllowed;
        }

        public final ParticipantsList getParticipantsList() {
            return this.participantsList;
        }

        public final boolean getTextInVoiceEnabled() {
            return this.textInVoiceEnabled;
        }

        public final String getTitleText() {
            return this.titleText;
        }

        public final int getTotalMentionsCount() {
            return this.totalMentionsCount;
        }

        public final Channel getVoiceChannel() {
            return this.voiceChannel;
        }

        public int hashCode() {
            Channel channel = this.voiceChannel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            boolean z2 = this.isTextInVoiceChannelSelected;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            boolean z3 = this.textInVoiceEnabled;
            if (z3) {
                z3 = true;
            }
            int i6 = z3 ? 1 : 0;
            int i7 = z3 ? 1 : 0;
            int i8 = (i5 + i6) * 31;
            String str = this.titleText;
            int hashCode2 = (((i8 + (str != null ? str.hashCode() : 0)) * 31) + this.totalMentionsCount) * 31;
            boolean z4 = this.isConnectEnabled;
            if (z4) {
                z4 = true;
            }
            int i9 = z4 ? 1 : 0;
            int i10 = z4 ? 1 : 0;
            int i11 = (hashCode2 + i9) * 31;
            ParticipantsList participantsList = this.participantsList;
            int hashCode3 = (i11 + (participantsList != null ? participantsList.hashCode() : 0)) * 31;
            boolean z5 = this.isNsfwUnconsented;
            if (z5) {
                z5 = true;
            }
            int i12 = z5 ? 1 : 0;
            int i13 = z5 ? 1 : 0;
            int i14 = (hashCode3 + i12) * 31;
            boolean z6 = this.isChannelNsfw;
            if (!z6) {
                i2 = z6 ? 1 : 0;
            }
            int i15 = (i14 + i2) * 31;
            NsfwAllowance nsfwAllowance = this.nsfwAllowed;
            if (nsfwAllowance != null) {
                i = nsfwAllowance.hashCode();
            }
            return i15 + i;
        }

        public final boolean isChannelNsfw() {
            return this.isChannelNsfw;
        }

        public final boolean isConnectEnabled() {
            return this.isConnectEnabled;
        }

        public final boolean isNsfwUnconsented() {
            return this.isNsfwUnconsented;
        }

        public final boolean isTextInVoiceChannelSelected() {
            return this.isTextInVoiceChannelSelected;
        }

        public String toString() {
            StringBuilder R = a.R("ViewState(voiceChannel=");
            R.append(this.voiceChannel);
            R.append(", isTextInVoiceChannelSelected=");
            R.append(this.isTextInVoiceChannelSelected);
            R.append(", textInVoiceEnabled=");
            R.append(this.textInVoiceEnabled);
            R.append(", titleText=");
            R.append(this.titleText);
            R.append(", totalMentionsCount=");
            R.append(this.totalMentionsCount);
            R.append(", isConnectEnabled=");
            R.append(this.isConnectEnabled);
            R.append(", participantsList=");
            R.append(this.participantsList);
            R.append(", isNsfwUnconsented=");
            R.append(this.isNsfwUnconsented);
            R.append(", isChannelNsfw=");
            R.append(this.isChannelNsfw);
            R.append(", nsfwAllowed=");
            R.append(this.nsfwAllowed);
            R.append(")");
            return R.toString();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetCallPreviewFullscreenViewModel(long r17, com.discord.stores.StoreChannels r19, com.discord.stores.StoreMentions r20, com.discord.stores.StoreNavigation r21, com.discord.stores.StoreUserSettings r22, com.discord.stores.StoreVoiceChannelSelected r23, com.discord.stores.StoreChannelsSelected r24, com.discord.stores.StoreApplicationStreamPreviews r25, com.discord.stores.StoreApplication r26, com.discord.stores.StoreUser r27, rx.Observable r28, com.discord.stores.StoreMediaSettings r29, int r30, kotlin.jvm.internal.DefaultConstructorMarker r31) {
        /*
            r16 = this;
            r0 = r30
            r1 = r0 & 2
            if (r1 == 0) goto Le
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r1 = r1.getChannels()
            r5 = r1
            goto L10
        Le:
            r5 = r19
        L10:
            r1 = r0 & 4
            if (r1 == 0) goto L1c
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreMentions r1 = r1.getMentions()
            r6 = r1
            goto L1e
        L1c:
            r6 = r20
        L1e:
            r1 = r0 & 8
            if (r1 == 0) goto L2a
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreNavigation r1 = r1.getNavigation()
            r7 = r1
            goto L2c
        L2a:
            r7 = r21
        L2c:
            r1 = r0 & 16
            if (r1 == 0) goto L38
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUserSettings r1 = r1.getUserSettings()
            r8 = r1
            goto L3a
        L38:
            r8 = r22
        L3a:
            r1 = r0 & 32
            if (r1 == 0) goto L46
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreVoiceChannelSelected r1 = r1.getVoiceChannelSelected()
            r9 = r1
            goto L48
        L46:
            r9 = r23
        L48:
            r1 = r0 & 64
            if (r1 == 0) goto L54
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannelsSelected r1 = r1.getChannelsSelected()
            r10 = r1
            goto L56
        L54:
            r10 = r24
        L56:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L62
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreApplicationStreamPreviews r1 = r1.getApplicationStreamPreviews()
            r11 = r1
            goto L64
        L62:
            r11 = r25
        L64:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L70
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreApplication r1 = r1.getApplication()
            r12 = r1
            goto L72
        L70:
            r12 = r26
        L72:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L7e
            com.discord.stores.StoreStream$Companion r1 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreUser r1 = r1.getUsers()
            r13 = r1
            goto L80
        L7e:
            r13 = r27
        L80:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L98
            com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel$Companion r1 = com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel.Companion
            r19 = r1
            r20 = r17
            r22 = r13
            r23 = r5
            r24 = r10
            r25 = r6
            rx.Observable r1 = com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel.Companion.access$observeStoreState(r19, r20, r22, r23, r24, r25)
            r14 = r1
            goto L9a
        L98:
            r14 = r28
        L9a:
            r0 = r0 & 2048(0x800, float:2.87E-42)
            if (r0 == 0) goto La6
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreMediaSettings r0 = r0.getMediaSettings()
            r15 = r0
            goto La8
        La6:
            r15 = r29
        La8:
            r2 = r16
            r3 = r17
            r2.<init>(r3, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreenViewModel.<init>(long, com.discord.stores.StoreChannels, com.discord.stores.StoreMentions, com.discord.stores.StoreNavigation, com.discord.stores.StoreUserSettings, com.discord.stores.StoreVoiceChannelSelected, com.discord.stores.StoreChannelsSelected, com.discord.stores.StoreApplicationStreamPreviews, com.discord.stores.StoreApplication, com.discord.stores.StoreUser, rx.Observable, com.discord.stores.StoreMediaSettings, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleStoreState(StoreState storeState) {
        ParticipantsList participantsList;
        ModelApplicationStream stream;
        if (storeState instanceof StoreState.Valid) {
            this.mostRecentStoreState = storeState;
            StoreState.Valid valid = (StoreState.Valid) storeState;
            Map<Long, EmbeddedActivity> embeddedActivitiesForChannel = valid.getCallModel().getEmbeddedActivitiesForChannel();
            StoreApplicationStreaming.ActiveApplicationStream activeStream = valid.getCallModel().getActiveStream();
            List<CallParticipantsAdapter.ListItem> createConnectedListItems = ParticipantsListItemGenerator.Companion.createConnectedListItems(valid.getCallModel().getParticipants(), (activeStream == null || (stream = activeStream.getStream()) == null) ? null : stream.getEncodedStreamKey(), valid.getVoiceChannel(), valid.getCallModel(), u.toList(embeddedActivitiesForChannel.values()), valid.getCallModel().getApplications());
            for (Long l : embeddedActivitiesForChannel.keySet()) {
                this.storeApplication.fetchIfNonexisting(l.longValue());
            }
            if (ParticipantsListItemGenerator.Companion.refreshStreams(createConnectedListItems, this.fetchedPreviews, this.storeApplicationStreamPreviews)) {
                participantsList = new ParticipantsList.ListItems(createConnectedListItems);
            } else {
                participantsList = new ParticipantsList.Empty(createConnectedListItems);
            }
            updateViewState(new ViewState(valid.getVoiceChannel(), ChannelUtils.E(valid.getSelectedTextChannel()), valid.isTextInVoiceEnabled(), ChannelUtils.c(valid.getVoiceChannel()), valid.getTotalMentionsCount(), valid.getCallModel().getVoiceChannelJoinability() != VoiceChannelJoinability.PERMISSIONS_MISSING, participantsList, valid.isNsfwUnconsented(), valid.isChannelNsfw(), valid.getNsfwAllowed()));
        }
    }

    @MainThread
    private final void joinVoiceChannel(long j) {
        this.storeVoiceChannelSelected.selectVoiceChannel(j);
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.LaunchVideoCall(this.voiceChannelId, null));
    }

    public static /* synthetic */ void tryConnectToVoice$default(WidgetCallPreviewFullscreenViewModel widgetCallPreviewFullscreenViewModel, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        widgetCallPreviewFullscreenViewModel.tryConnectToVoice(z2);
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @MainThread
    public final void onStreamPreviewClicked(String str) {
        m.checkNotNullParameter(str, "streamKey");
        StoreState storeState = this.mostRecentStoreState;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid == null) {
            return;
        }
        if (valid.getCallModel().getVoiceChannelJoinability() == VoiceChannelJoinability.GUILD_VIDEO_AT_CAPACITY) {
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(Event.ShowGuildVideoAtCapacityDialog.INSTANCE);
            return;
        }
        ModelApplicationStream decodeStreamKey = ModelApplicationStream.Companion.decodeStreamKey(str);
        PublishSubject<Event> publishSubject2 = this.eventSubject;
        publishSubject2.k.onNext(new Event.LaunchVideoCall(decodeStreamKey.getChannelId(), str));
    }

    @MainThread
    public final void onTextInVoiceTapped() {
        ViewState viewState = getViewState();
        ChannelSelector.selectChannel$default(ChannelSelector.Companion.getInstance(), viewState != null ? viewState.getVoiceChannel() : null, null, SelectedChannelAnalyticsLocation.TEXT_IN_VOICE, 2, null);
        StoreNavigation.setNavigationPanelAction$default(this.storeNavigation, StoreNavigation.PanelAction.CLOSE, null, 2, null);
    }

    public final void selectTextChannelAfterFinish() {
        Channel voiceChannel;
        ViewState viewState = getViewState();
        if (viewState != null && (voiceChannel = viewState.getVoiceChannel()) != null) {
            long f = voiceChannel.f();
            if (viewState.isTextInVoiceChannelSelected()) {
                ChannelSelector.Companion.getInstance().selectPreviousChannel(f);
            }
        }
    }

    @MainThread
    public final void tryConnectToVoice(boolean z2) {
        StoreState storeState = this.mostRecentStoreState;
        if (storeState instanceof StoreState.Valid) {
            if (((StoreState.Valid) storeState).getCallModel().getVoiceChannelJoinability() == VoiceChannelJoinability.GUILD_VIDEO_AT_CAPACITY) {
                this.eventSubject.k.onNext(Event.ShowGuildVideoAtCapacityDialog.INSTANCE);
                return;
            }
            if (!this.storeUserSettings.getIsMobileOverlayEnabled()) {
                this.eventSubject.k.onNext(Event.ShowOverlayNux.INSTANCE);
            }
            if (z2) {
                this.mediaSettingsStore.setSelfMuted(true);
            }
            joinVoiceChannel(this.voiceChannelId);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCallPreviewFullscreenViewModel(long j, StoreChannels storeChannels, StoreMentions storeMentions, StoreNavigation storeNavigation, StoreUserSettings storeUserSettings, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreChannelsSelected storeChannelsSelected, StoreApplicationStreamPreviews storeApplicationStreamPreviews, StoreApplication storeApplication, StoreUser storeUser, Observable<StoreState> observable, StoreMediaSettings storeMediaSettings) {
        super(null, 1, null);
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeMentions, "storeMentions");
        m.checkNotNullParameter(storeNavigation, "storeNavigation");
        m.checkNotNullParameter(storeUserSettings, "storeUserSettings");
        m.checkNotNullParameter(storeVoiceChannelSelected, "storeVoiceChannelSelected");
        m.checkNotNullParameter(storeChannelsSelected, "storeChannelsSelected");
        m.checkNotNullParameter(storeApplicationStreamPreviews, "storeApplicationStreamPreviews");
        m.checkNotNullParameter(storeApplication, "storeApplication");
        m.checkNotNullParameter(storeUser, "storeUser");
        m.checkNotNullParameter(observable, "storeStateObservable");
        m.checkNotNullParameter(storeMediaSettings, "mediaSettingsStore");
        this.voiceChannelId = j;
        this.storeChannels = storeChannels;
        this.storeMentions = storeMentions;
        this.storeNavigation = storeNavigation;
        this.storeUserSettings = storeUserSettings;
        this.storeVoiceChannelSelected = storeVoiceChannelSelected;
        this.storeChannelsSelected = storeChannelsSelected;
        this.storeApplicationStreamPreviews = storeApplicationStreamPreviews;
        this.storeApplication = storeApplication;
        this.storeUser = storeUser;
        this.mediaSettingsStore = storeMediaSettings;
        this.eventSubject = PublishSubject.k0();
        this.fetchedPreviews = new LinkedHashSet();
        Observable q = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null).q();
        m.checkNotNullExpressionValue(q, "storeStateObservable\n   …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(q, WidgetCallPreviewFullscreenViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
