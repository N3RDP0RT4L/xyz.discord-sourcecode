package com.discord.widgets.voice.fullscreen;

import androidx.fragment.app.FragmentManager;
import com.discord.api.channel.Channel;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetCallPreviewFullscreen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "clickedUser", "", "invoke", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallPreviewFullscreen$configureParticipants$2 extends o implements Function1<StoreVoiceParticipants.VoiceUser, Unit> {
    public final /* synthetic */ Channel $channel;
    public final /* synthetic */ WidgetCallPreviewFullscreen this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCallPreviewFullscreen$configureParticipants$2(WidgetCallPreviewFullscreen widgetCallPreviewFullscreen, Channel channel) {
        super(1);
        this.this$0 = widgetCallPreviewFullscreen;
        this.$channel = channel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(StoreVoiceParticipants.VoiceUser voiceUser) {
        invoke2(voiceUser);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(StoreVoiceParticipants.VoiceUser voiceUser) {
        m.checkNotNullParameter(voiceUser, "clickedUser");
        WidgetUserSheet.Companion companion = WidgetUserSheet.Companion;
        long id2 = voiceUser.getUser().getId();
        Long valueOf = Long.valueOf(this.$channel.h());
        FragmentManager childFragmentManager = this.this$0.getChildFragmentManager();
        m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        WidgetUserSheet.Companion.show$default(companion, id2, valueOf, childFragmentManager, Long.valueOf(this.$channel.f()), Boolean.TRUE, null, null, 96, null);
    }
}
