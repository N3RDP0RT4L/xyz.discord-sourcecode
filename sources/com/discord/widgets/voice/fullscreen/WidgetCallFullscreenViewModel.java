package com.discord.widgets.voice.fullscreen;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Intent;
import androidx.annotation.MainThread;
import androidx.annotation.StringRes;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import b.d.b.a.a;
import co.discord.media_engine.VideoInputDeviceDescription;
import co.discord.media_engine.VideoInputDeviceFacing;
import com.discord.api.application.ApplicationAsset;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.voice.state.VoiceState;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.guild.Guild;
import com.discord.models.user.User;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.VideoMetadata;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.rtcconnection.socket.io.Payloads;
import com.discord.stores.SelectedChannelAnalyticsLocation;
import com.discord.stores.StoreAnalytics;
import com.discord.stores.StoreApplication;
import com.discord.stores.StoreApplicationAssets;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreAudioManagerV2;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreChannelsSelected;
import com.discord.stores.StoreConnectivity;
import com.discord.stores.StoreExperiments;
import com.discord.stores.StoreGuildSelected;
import com.discord.stores.StoreMediaEngine;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreMentions;
import com.discord.stores.StoreNavigation;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreReadStates;
import com.discord.stores.StoreStageChannels;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreStreamRtcConnection;
import com.discord.stores.StoreTabsNavigation;
import com.discord.stores.StoreUserSettings;
import com.discord.stores.StoreVideoStreams;
import com.discord.stores.StoreVoiceChannelSelected;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.channel.ChannelSelector;
import com.discord.utilities.permissions.VideoPermissionsManager;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.time.Clock;
import com.discord.utilities.user.UserUtils;
import com.discord.utilities.video.VideoPlayerIdleDetector;
import com.discord.utilities.voice.PerceptualVolumeUtils;
import com.discord.utilities.voice.VoiceEngineServiceController;
import com.discord.views.calls.VideoCallParticipantView;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.model.StageCallModel;
import com.discord.widgets.voice.controls.VoiceControlsOutputSelectorState;
import com.discord.widgets.voice.fullscreen.CallParticipant;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel;
import com.discord.widgets.voice.model.CallModel;
import com.discord.widgets.voice.model.CameraState;
import d0.g;
import d0.g0.t;
import d0.t.n;
import d0.t.n0;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function14;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.webrtc.RendererCommon;
import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.subjects.PublishSubject;
import xyz.discord.R;
/* compiled from: WidgetCallFullscreenViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ê\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u000e\u008d\u0002\u008e\u0002\u008f\u0002\u0090\u0002\u0091\u0002\u0092\u0002\u0093\u0002BÑ\u0002\u0012\n\u0010+\u001a\u00060\tj\u0002`*\u0012\n\b\u0002\u0010Ù\u0001\u001a\u00030Ø\u0001\u0012\n\b\u0002\u0010\u0085\u0002\u001a\u00030\u0084\u0002\u0012\n\b\u0002\u0010¼\u0001\u001a\u00030»\u0001\u0012\n\b\u0002\u0010î\u0001\u001a\u00030í\u0001\u0012\n\b\u0002\u0010¿\u0001\u001a\u00030¾\u0001\u0012\n\b\u0002\u0010¤\u0001\u001a\u00030£\u0001\u0012\n\b\u0002\u0010§\u0001\u001a\u00030¦\u0001\u0012\n\b\u0002\u0010Î\u0001\u001a\u00030Í\u0001\u0012\n\b\u0002\u0010ñ\u0001\u001a\u00030ð\u0001\u0012\n\b\u0002\u0010\u0082\u0002\u001a\u00030\u0081\u0002\u0012\n\b\u0002\u0010Ü\u0001\u001a\u00030Û\u0001\u0012\n\b\u0002\u0010Õ\u0001\u001a\u00030Ô\u0001\u0012\n\b\u0002\u0010ô\u0001\u001a\u00030ó\u0001\u0012\n\b\u0002\u0010¡\u0001\u001a\u00030 \u0001\u0012\n\b\u0002\u0010ú\u0001\u001a\u00030ù\u0001\u0012\n\b\u0002\u0010ß\u0001\u001a\u00030Þ\u0001\u0012\n\b\u0002\u0010Ñ\u0001\u001a\u00030Ð\u0001\u0012\n\b\u0002\u0010¹\u0001\u001a\u00030¸\u0001\u0012\n\b\u0002\u0010â\u0001\u001a\u00030á\u0001\u0012\n\b\u0002\u0010\u0088\u0002\u001a\u00030\u0087\u0002\u0012\n\b\u0002\u0010ü\u0001\u001a\u00030¸\u0001\u0012\n\b\u0002\u0010°\u0001\u001a\u00030¯\u0001\u0012\n\b\u0002\u0010þ\u0001\u001a\u00030ý\u0001\u0012\n\b\u0002\u0010Â\u0001\u001a\u00030Á\u0001\u0012\n\b\u0002\u0010³\u0001\u001a\u00030²\u0001\u0012\u000e\u0010O\u001a\n\u0018\u00010#j\u0004\u0018\u0001`<¢\u0006\u0006\b\u008b\u0002\u0010\u008c\u0002J\u001d\u0010\u0007\u001a\u00020\u00062\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0002¢\u0006\u0004\b\u0007\u0010\bJ7\u0010\u0012\u001a\u0004\u0018\u00010\u00112\n\u0010\u000b\u001a\u00060\tj\u0002`\n2\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u000fH\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0015\u001a\u00020\u0014H\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0017\u001a\u00020\u0014H\u0003¢\u0006\u0004\b\u0017\u0010\u0016J\u000f\u0010\u0018\u001a\u00020\u0014H\u0003¢\u0006\u0004\b\u0018\u0010\u0016J\u000f\u0010\u0019\u001a\u00020\u0014H\u0003¢\u0006\u0004\b\u0019\u0010\u0016J\u001d\u0010\u001c\u001a\u00020\u00142\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0003H\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0015\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0003¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010!\u001a\u00020\u00142\u0006\u0010 \u001a\u00020\u0006H\u0003¢\u0006\u0004\b!\u0010\"J\u0013\u0010$\u001a\u00020#*\u00020\u0004H\u0002¢\u0006\u0004\b$\u0010%J\u000f\u0010&\u001a\u00020\u0006H\u0002¢\u0006\u0004\b&\u0010'JO\u00101\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010)\u001a\u00020(2\n\u0010+\u001a\u00060\tj\u0002`*2\b\u0010-\u001a\u0004\u0018\u00010,2\b\u0010/\u001a\u0004\u0018\u00010.2\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u00100\u001a\u00020\u0006H\u0002¢\u0006\u0004\b1\u00102J!\u00104\u001a\u00020\u00042\u0006\u00103\u001a\u00020\u00042\b\u0010/\u001a\u0004\u0018\u00010.H\u0002¢\u0006\u0004\b4\u00105J\u0017\u00109\u001a\u0002082\u0006\u00107\u001a\u000206H\u0002¢\u0006\u0004\b9\u0010:J;\u0010@\u001a\u0012\u0012\u0004\u0012\u00020(0>j\b\u0012\u0004\u0012\u00020(`?2\b\b\u0002\u0010;\u001a\u00020\u00062\u0010\b\u0002\u0010=\u001a\n\u0018\u00010#j\u0004\u0018\u0001`<H\u0002¢\u0006\u0004\b@\u0010AJ\u0017\u0010D\u001a\u00020\u00062\u0006\u0010C\u001a\u00020BH\u0002¢\u0006\u0004\bD\u0010EJ\u001d\u0010F\u001a\b\u0012\u0004\u0012\u00020(0\u00032\u0006\u0010C\u001a\u00020BH\u0002¢\u0006\u0004\bF\u0010GJ\u000f\u0010H\u001a\u00020\u0014H\u0002¢\u0006\u0004\bH\u0010\u0016J\u000f\u0010I\u001a\u00020\u0014H\u0002¢\u0006\u0004\bI\u0010\u0016J\u000f\u0010J\u001a\u00020\u0014H\u0002¢\u0006\u0004\bJ\u0010\u0016J\u000f\u0010K\u001a\u00020\u0014H\u0002¢\u0006\u0004\bK\u0010\u0016J\u000f\u0010L\u001a\u00020\u0014H\u0002¢\u0006\u0004\bL\u0010\u0016J\u000f\u0010M\u001a\u00020\u0014H\u0002¢\u0006\u0004\bM\u0010\u0016J\u000f\u0010N\u001a\u00020\u0014H\u0003¢\u0006\u0004\bN\u0010\u0016J-\u0010S\u001a\u00020\u00142\n\u0010O\u001a\u00060#j\u0002`<2\b\u0010P\u001a\u0004\u0018\u00010#2\u0006\u0010R\u001a\u00020QH\u0002¢\u0006\u0004\bS\u0010TJ\u0017\u0010W\u001a\u00020\u00062\u0006\u0010V\u001a\u00020UH\u0002¢\u0006\u0004\bW\u0010XJ\u0015\u0010[\u001a\b\u0012\u0004\u0012\u00020Z0YH\u0002¢\u0006\u0004\b[\u0010\\J\u0013\u0010^\u001a\b\u0012\u0004\u0012\u00020]0Y¢\u0006\u0004\b^\u0010\\J\u0019\u0010_\u001a\u00020\u00142\n\u0010+\u001a\u00060\tj\u0002`*¢\u0006\u0004\b_\u0010`J\r\u0010a\u001a\u00020\u0014¢\u0006\u0004\ba\u0010\u0016J\u0017\u0010c\u001a\u00020\u00142\u0006\u0010b\u001a\u00020QH\u0007¢\u0006\u0004\bc\u0010dJ\u0017\u0010f\u001a\u00020\u00142\u0006\u0010e\u001a\u00020\u0002H\u0014¢\u0006\u0004\bf\u0010gJ\u000f\u0010h\u001a\u00020\u0014H\u0007¢\u0006\u0004\bh\u0010\u0016J\u000f\u0010i\u001a\u00020\u0014H\u0007¢\u0006\u0004\bi\u0010\u0016J\u0017\u0010k\u001a\u00020\u00142\u0006\u0010j\u001a\u00020ZH\u0007¢\u0006\u0004\bk\u0010lJ\u000f\u0010m\u001a\u00020\u0014H\u0007¢\u0006\u0004\bm\u0010\u0016J\u0017\u0010p\u001a\u00020\u00142\u0006\u0010o\u001a\u00020nH\u0007¢\u0006\u0004\bp\u0010qJ\u000f\u0010r\u001a\u00020\u0014H\u0007¢\u0006\u0004\br\u0010\u0016J\u001b\u0010s\u001a\u00020\u00142\n\u0010O\u001a\u00060#j\u0002`<H\u0007¢\u0006\u0004\bs\u0010tJ\u001b\u0010u\u001a\u00020\u00142\n\u0010O\u001a\u00060#j\u0002`<H\u0007¢\u0006\u0004\bu\u0010tJ\u000f\u0010v\u001a\u00020\u0014H\u0007¢\u0006\u0004\bv\u0010\u0016J\u000f\u0010w\u001a\u00020\u0014H\u0007¢\u0006\u0004\bw\u0010\u0016J\u000f\u0010x\u001a\u00020\u0014H\u0007¢\u0006\u0004\bx\u0010\u0016J\u000f\u0010y\u001a\u00020\u0014H\u0007¢\u0006\u0004\by\u0010\u0016J\u000f\u0010z\u001a\u00020\u0014H\u0007¢\u0006\u0004\bz\u0010\u0016J\u000f\u0010{\u001a\u00020\u0014H\u0007¢\u0006\u0004\b{\u0010\u0016J\u0017\u0010|\u001a\u00020\u00142\u0006\u00103\u001a\u00020\u0004H\u0007¢\u0006\u0004\b|\u0010}J\u0017\u0010~\u001a\u00020\u00142\u0006\u0010)\u001a\u00020\u0004H\u0007¢\u0006\u0004\b~\u0010}J\u0019\u0010\u0080\u0001\u001a\u00020\u00142\u0006\u0010\u007f\u001a\u00020\u0006H\u0007¢\u0006\u0005\b\u0080\u0001\u0010\"J\u0011\u0010\u0081\u0001\u001a\u00020\u0014H\u0007¢\u0006\u0005\b\u0081\u0001\u0010\u0016J\u0011\u0010\u0082\u0001\u001a\u00020\u0014H\u0007¢\u0006\u0005\b\u0082\u0001\u0010\u0016J(\u0010\u0087\u0001\u001a\u00020\u00142\b\u0010\u0084\u0001\u001a\u00030\u0083\u00012\n\u0010\u0086\u0001\u001a\u0005\u0018\u00010\u0085\u0001H\u0007¢\u0006\u0006\b\u0087\u0001\u0010\u0088\u0001J(\u0010\u0089\u0001\u001a\u00020\u00142\b\u0010\u0084\u0001\u001a\u00030\u0083\u00012\n\u0010\u0086\u0001\u001a\u0005\u0018\u00010\u0085\u0001H\u0007¢\u0006\u0006\b\u0089\u0001\u0010\u0088\u0001J\u0011\u0010\u008a\u0001\u001a\u00020\u0014H\u0007¢\u0006\u0005\b\u008a\u0001\u0010\u0016J\u001a\u0010\u008c\u0001\u001a\u00020\u00142\u0007\u0010\u008b\u0001\u001a\u00020#H\u0007¢\u0006\u0005\b\u008c\u0001\u0010tJ#\u0010\u0090\u0001\u001a\u00020\u00142\b\u0010\u008e\u0001\u001a\u00030\u008d\u00012\u0007\u0010\u008f\u0001\u001a\u00020\u0006¢\u0006\u0006\b\u0090\u0001\u0010\u0091\u0001J\u001d\u0010\u0094\u0001\u001a\u00020\u00142\f\u0010\u0093\u0001\u001a\u00070\tj\u0003`\u0092\u0001¢\u0006\u0005\b\u0094\u0001\u0010`J\u000f\u0010\u0095\u0001\u001a\u00020\u0014¢\u0006\u0005\b\u0095\u0001\u0010\u0016J\u001d\u0010\u0096\u0001\u001a\u00020\u00142\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0003¢\u0006\u0005\b\u0096\u0001\u0010\u001dJ\u0011\u0010\u0097\u0001\u001a\u00020\u0014H\u0015¢\u0006\u0005\b\u0097\u0001\u0010\u0016J\u000f\u0010\u0098\u0001\u001a\u00020\u0014¢\u0006\u0005\b\u0098\u0001\u0010\u0016J\u0011\u0010\u0099\u0001\u001a\u00020\u0014H\u0007¢\u0006\u0005\b\u0099\u0001\u0010\u0016J\u0011\u0010\u009a\u0001\u001a\u00020\u0014H\u0007¢\u0006\u0005\b\u009a\u0001\u0010\u0016J\u0011\u0010\u009b\u0001\u001a\u00020\u0014H\u0007¢\u0006\u0005\b\u009b\u0001\u0010\u0016J&\u0010\u009e\u0001\u001a\u00020\u00022\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010\u00022\u0007\u0010\u009d\u0001\u001a\u00020\u0002H\u0014¢\u0006\u0006\b\u009e\u0001\u0010\u009f\u0001R\u001a\u0010¡\u0001\u001a\u00030 \u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¡\u0001\u0010¢\u0001R\u001a\u0010¤\u0001\u001a\u00030£\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¤\u0001\u0010¥\u0001R\u001a\u0010§\u0001\u001a\u00030¦\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b§\u0001\u0010¨\u0001R\"\u0010ª\u0001\u001a\u000b\u0018\u00010#j\u0005\u0018\u0001`©\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bª\u0001\u0010«\u0001R\u0019\u0010¬\u0001\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b¬\u0001\u0010\u00ad\u0001R!\u0010®\u0001\u001a\n\u0018\u00010#j\u0004\u0018\u0001`<8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b®\u0001\u0010«\u0001R\u001a\u0010°\u0001\u001a\u00030¯\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b°\u0001\u0010±\u0001R\u001a\u0010³\u0001\u001a\u00030²\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b³\u0001\u0010´\u0001R\u001c\u0010¶\u0001\u001a\u0005\u0018\u00010µ\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b¶\u0001\u0010·\u0001R\u001a\u0010¹\u0001\u001a\u00030¸\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¹\u0001\u0010º\u0001R\u001a\u0010¼\u0001\u001a\u00030»\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¼\u0001\u0010½\u0001R\u001a\u0010¿\u0001\u001a\u00030¾\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¿\u0001\u0010À\u0001R\u001a\u0010Â\u0001\u001a\u00030Á\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÂ\u0001\u0010Ã\u0001R\u001a\u0010Å\u0001\u001a\u00030Ä\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÅ\u0001\u0010Æ\u0001RZ\u0010É\u0001\u001aC\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u001a È\u0001*\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00030\u0003 È\u0001* \u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u001a È\u0001*\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u00030\u0003\u0018\u00010Ç\u00010Ç\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÉ\u0001\u0010Ê\u0001R\u001b\u0010Ë\u0001\u001a\u0004\u0018\u00010Z8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bË\u0001\u0010Ì\u0001R\u001a\u0010Î\u0001\u001a\u00030Í\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÎ\u0001\u0010Ï\u0001R\u001a\u0010Ñ\u0001\u001a\u00030Ð\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÑ\u0001\u0010Ò\u0001RB\u0010Ó\u0001\u001a+\u0012\r\u0012\u000b È\u0001*\u0004\u0018\u00010]0] È\u0001*\u0014\u0012\r\u0012\u000b È\u0001*\u0004\u0018\u00010]0]\u0018\u00010Ç\u00010Ç\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÓ\u0001\u0010Ê\u0001R\u001a\u0010Õ\u0001\u001a\u00030Ô\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÕ\u0001\u0010Ö\u0001R\u001b\u0010+\u001a\u00060\tj\u0002`*8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b+\u0010×\u0001R\u001a\u0010Ù\u0001\u001a\u00030Ø\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÙ\u0001\u0010Ú\u0001R\u001a\u0010Ü\u0001\u001a\u00030Û\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÜ\u0001\u0010Ý\u0001R\u001a\u0010ß\u0001\u001a\u00030Þ\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bß\u0001\u0010à\u0001R\u001a\u0010â\u0001\u001a\u00030á\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bâ\u0001\u0010ã\u0001R'\u0010ä\u0001\u001a\u00020\u00068\u0006@\u0006X\u0086\u000e¢\u0006\u0016\n\u0006\bä\u0001\u0010\u00ad\u0001\u001a\u0005\bå\u0001\u0010'\"\u0005\bæ\u0001\u0010\"R\u0019\u0010ç\u0001\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bç\u0001\u0010\u00ad\u0001R\u001b\u0010è\u0001\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bè\u0001\u0010é\u0001R\u001c\u0010ë\u0001\u001a\u0005\u0018\u00010ê\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bë\u0001\u0010ì\u0001R\u001a\u0010î\u0001\u001a\u00030í\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bî\u0001\u0010ï\u0001R\u001a\u0010ñ\u0001\u001a\u00030ð\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bñ\u0001\u0010ò\u0001R\u001a\u0010ô\u0001\u001a\u00030ó\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bô\u0001\u0010õ\u0001R\u001f\u0010ö\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bö\u0001\u0010÷\u0001R\u001c\u0010ø\u0001\u001a\u0005\u0018\u00010ê\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bø\u0001\u0010ì\u0001R\u001a\u0010ú\u0001\u001a\u00030ù\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bú\u0001\u0010û\u0001R\u001a\u0010ü\u0001\u001a\u00030¸\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bü\u0001\u0010º\u0001R\u001a\u0010þ\u0001\u001a\u00030ý\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bþ\u0001\u0010ÿ\u0001R\u0019\u0010b\u001a\u0004\u0018\u00010Q8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bb\u0010\u0080\u0002R\u001a\u0010\u0082\u0002\u001a\u00030\u0081\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0082\u0002\u0010\u0083\u0002R\u001a\u0010\u0085\u0002\u001a\u00030\u0084\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0085\u0002\u0010\u0086\u0002R\u001a\u0010\u0088\u0002\u001a\u00030\u0087\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0088\u0002\u0010\u0089\u0002R\u0019\u0010\u008a\u0002\u001a\u00020\u00068\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u008a\u0002\u0010\u00ad\u0001¨\u0006\u0094\u0002"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;", "", "Lcom/discord/widgets/voice/fullscreen/CallParticipant;", "callParticipants", "", "isOneOnOneMeCall", "(Ljava/util/List;)Z", "", "Lcom/discord/primitives/UserId;", "myUserId", "Lco/discord/media_engine/VideoInputDeviceDescription;", "selectedVideoInputDevice", "isStreaming", "Lcom/discord/widgets/voice/model/CameraState;", "cameraState", "Lcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;", "computePipParticipant", "(JLco/discord/media_engine/VideoInputDeviceDescription;ZLcom/discord/widgets/voice/model/CameraState;)Lcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;", "", "startTapForwardingJob", "()V", "cancelTapForwardingJob", "clearFocusedVideoParticipant", "stopWatchingStreamIfEnded", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;", "visibleVideoParticipants", "setOffScreenVideoParticipants", "(Ljava/util/List;)V", "computeVisibleVideoParticipants", "()Ljava/util/List;", "isIdle", "onIdleStateChanged", "(Z)V", "", "getParticipantFocusKey", "(Lcom/discord/widgets/voice/fullscreen/CallParticipant;)Ljava/lang/String;", "hasVideoPermission", "()Z", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "participant", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "activeApplicationStream", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "streamQuality", "showLabels", "createVideoGridEntriesForParticipant", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;JLcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection$Quality;Lco/discord/media_engine/VideoInputDeviceDescription;Z)Ljava/util/List;", "callParticipant", "addStreamQualityMetadata", "(Lcom/discord/widgets/voice/fullscreen/CallParticipant;Lcom/discord/rtcconnection/RtcConnection$Quality;)Lcom/discord/widgets/voice/fullscreen/CallParticipant;", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;", "applicationStreamState", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;", "getParticipantStreamState", "(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;", "prioritizeSpectators", "Lcom/discord/primitives/StreamKey;", "mySpectatingStreamKey", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "createUserItemsComparator", "(ZLjava/lang/String;)Ljava/util/Comparator;", "Lcom/discord/widgets/voice/model/CallModel;", "callModel", "shouldShowMoreAudioOutputs", "(Lcom/discord/widgets/voice/model/CallModel;)Z", "createPrivateCallParticipantListItems", "(Lcom/discord/widgets/voice/model/CallModel;)Ljava/util/List;", "emitSuppressedDialogEvent", "emitServerMutedDialogEvent", "emitShowNoVideoPermissionDialogEvent", "emitShowNoScreenSharePermissionDialogEvent", "emitShowNoVadPermissionDialogEvent", "emitServerDeafenedDialogEvent", "stopWatchingStream", "streamKey", "mediaSessionId", "", "triggerRateDenominator", "enqueueStreamFeedbackSheet", "(Ljava/lang/String;Ljava/lang/String;I)V", "Lcom/discord/models/domain/ModelApplicationStream;", "stream", "isStreamFocused", "(Lcom/discord/models/domain/ModelApplicationStream;)Z", "Lrx/Observable;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;", "observeStoreState", "()Lrx/Observable;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "observeEvents", "setTargetChannelId", "(J)V", "disableControlFading", "bottomSheetState", "handleBottomSheetState", "(I)V", "viewState", "updateViewState", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V", "stopIdle", "startIdle", "storeState", "handleStoreState", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;)V", "onScreenShareClick", "Landroid/content/Intent;", "intent", "startScreenShare", "(Landroid/content/Intent;)V", "stopScreenShare", "targetStream", "(Ljava/lang/String;)V", "targetAndFocusStream", "tryConnectToVoice", "disconnect", "toggleCameraPressed", "onCameraPermissionsGranted", "switchCameraInputPressed", "onDeafenPressed", "onGridParticipantTapped", "(Lcom/discord/widgets/voice/fullscreen/CallParticipant;)V", "onGridParticipantLongPressed", "isPressed", "onPushToTalkPressed", "onEmptyStateTapped", "onShowParticipantsPressed", "Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution;", "resolution", "Lcom/discord/views/calls/VideoCallParticipantView$StreamFps;", "fps", "onStreamQualityIndicatorShown", "(Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution;Lcom/discord/views/calls/VideoCallParticipantView$StreamFps;)V", "onStreamQualityIndicatorClicked", "onTextInVoiceTapped", "participantKey", "focusVideoParticipant", "", "perceptualVolume", "fromUser", "onStreamPerceptualVolumeChanged", "(FZ)V", "Lcom/discord/primitives/GuildId;", "guildId", "onVisitCommunityButtonClicked", "selectTextChannelAfterFinish", "updateOffScreenParticipantsFromScroll", "onCleared", "requestStopWatchingStreamFromUserInput", "onMuteClicked", "moveMeToAudience", "toggleRequestToSpeak", "previousViewState", "pendingViewState", "modifyPendingViewState", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;", "Lcom/discord/utilities/voice/VoiceEngineServiceController;", "voiceEngineServiceController", "Lcom/discord/utilities/voice/VoiceEngineServiceController;", "Lcom/discord/stores/StoreUserSettings;", "userSettingsStore", "Lcom/discord/stores/StoreUserSettings;", "Lcom/discord/stores/StoreApplicationStreaming;", "applicationStreamingStore", "Lcom/discord/stores/StoreApplicationStreaming;", "Lcom/discord/widgets/voice/fullscreen/ParticipantKey;", "focusedVideoParticipantKey", "Ljava/lang/String;", "logStreamQualityIndicatorViewed", "Z", "autotargetStreamKey", "Lcom/discord/stores/StoreExperiments;", "experimentsStore", "Lcom/discord/stores/StoreExperiments;", "Lcom/discord/stores/StoreApplicationAssets;", "applicationAssetsStore", "Lcom/discord/stores/StoreApplicationAssets;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;", "lastParticipantTap", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;", "Lcom/discord/stores/StoreAnalytics;", "analyticsStore", "Lcom/discord/stores/StoreAnalytics;", "Lcom/discord/stores/StoreVoiceChannelSelected;", "selectedVoiceChannelStore", "Lcom/discord/stores/StoreVoiceChannelSelected;", "Lcom/discord/stores/StoreTabsNavigation;", "tabsNavigationStore", "Lcom/discord/stores/StoreTabsNavigation;", "Lcom/discord/stores/StoreApplication;", "applicationStore", "Lcom/discord/stores/StoreApplication;", "Lcom/discord/utilities/video/VideoPlayerIdleDetector;", "videoPlayerIdleDetector", "Lcom/discord/utilities/video/VideoPlayerIdleDetector;", "Lrx/subjects/PublishSubject;", "kotlin.jvm.PlatformType", "offScreenSubject", "Lrx/subjects/PublishSubject;", "mostRecentStoreState", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;", "Lcom/discord/stores/StoreMediaEngine;", "mediaEngineStore", "Lcom/discord/stores/StoreMediaEngine;", "Lcom/discord/stores/StoreMentions;", "mentionsStore", "Lcom/discord/stores/StoreMentions;", "eventSubject", "Lrx/Scheduler;", "backgroundThreadScheduler", "Lrx/Scheduler;", "J", "Lcom/discord/stores/StoreChannels;", "storeChannels", "Lcom/discord/stores/StoreChannels;", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/discord/stores/StoreAudioManagerV2;", "audioManagerStore", "Lcom/discord/stores/StoreAudioManagerV2;", "Lcom/discord/stores/StoreConnectivity;", "connectivityStore", "Lcom/discord/stores/StoreConnectivity;", "showStageCallBottomSheet", "getShowStageCallBottomSheet", "setShowStageCallBottomSheet", "wasEverMultiParticipant", "startedAsVideo", "Ljava/lang/Boolean;", "Lrx/Subscription;", "storeObservableSubscription", "Lrx/Subscription;", "Lcom/discord/stores/StoreGuildSelected;", "guildSelectedStore", "Lcom/discord/stores/StoreGuildSelected;", "Lcom/discord/stores/StoreMediaSettings;", "mediaSettingsStore", "Lcom/discord/stores/StoreMediaSettings;", "Lcom/discord/utilities/permissions/VideoPermissionsManager;", "videoPermissionsManager", "Lcom/discord/utilities/permissions/VideoPermissionsManager;", "allVideoParticipants", "Ljava/util/List;", "forwardVideoGridInteractionSubscription", "Lcom/discord/stores/StoreStreamRtcConnection;", "streamRtcConnectionStore", "Lcom/discord/stores/StoreStreamRtcConnection;", "storeAnalytics", "Lcom/discord/stores/StoreChannelsSelected;", "channelsSelectedStore", "Lcom/discord/stores/StoreChannelsSelected;", "Ljava/lang/Integer;", "Lcom/discord/stores/StorePermissions;", "permissionsStore", "Lcom/discord/stores/StorePermissions;", "Lcom/discord/stores/StoreNavigation;", "storeNavigation", "Lcom/discord/stores/StoreNavigation;", "Lcom/discord/stores/StoreStageChannels;", "stageStore", "Lcom/discord/stores/StoreStageChannels;", "wasEverConnected", HookHelper.constructorName, "(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StorePermissions;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreAudioManagerV2;Lcom/discord/stores/StoreMentions;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreConnectivity;Lcom/discord/stores/StoreStageChannels;Lcom/discord/stores/StoreAnalytics;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreApplication;Lcom/discord/stores/StoreApplicationAssets;Ljava/lang/String;)V", "DisplayMode", "Event", "MenuItem", "OverlayStatus", "ParticipantTap", "StoreState", "ViewState", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallFullscreenViewModel extends AppViewModel<ViewState> {
    private List<? extends CallParticipant> allVideoParticipants;
    private final StoreAnalytics analyticsStore;
    private final StoreApplicationAssets applicationAssetsStore;
    private final StoreApplication applicationStore;
    private final StoreApplicationStreaming applicationStreamingStore;
    private final StoreAudioManagerV2 audioManagerStore;
    private String autotargetStreamKey;
    private final Scheduler backgroundThreadScheduler;
    private Integer bottomSheetState;
    private long channelId;
    private final StoreChannelsSelected channelsSelectedStore;
    private final Clock clock;
    private final StoreConnectivity connectivityStore;
    private final PublishSubject<Event> eventSubject;
    private final StoreExperiments experimentsStore;
    private String focusedVideoParticipantKey;
    private Subscription forwardVideoGridInteractionSubscription;
    private final StoreGuildSelected guildSelectedStore;
    private ParticipantTap lastParticipantTap;
    private boolean logStreamQualityIndicatorViewed;
    private final StoreMediaEngine mediaEngineStore;
    private final StoreMediaSettings mediaSettingsStore;
    private final StoreMentions mentionsStore;
    private StoreState mostRecentStoreState;
    private final PublishSubject<List<VideoCallParticipantView.ParticipantData>> offScreenSubject;
    private final StorePermissions permissionsStore;
    private final StoreVoiceChannelSelected selectedVoiceChannelStore;
    private boolean showStageCallBottomSheet;
    private final StoreStageChannels stageStore;
    private Boolean startedAsVideo;
    private final StoreAnalytics storeAnalytics;
    private final StoreChannels storeChannels;
    private final StoreNavigation storeNavigation;
    private Subscription storeObservableSubscription;
    private final StoreStreamRtcConnection streamRtcConnectionStore;
    private final StoreTabsNavigation tabsNavigationStore;
    private final StoreUserSettings userSettingsStore;
    private final VideoPermissionsManager videoPermissionsManager;
    private final VideoPlayerIdleDetector videoPlayerIdleDetector;
    private final VoiceEngineServiceController voiceEngineServiceController;
    private boolean wasEverConnected;
    private boolean wasEverMultiParticipant;

    /* compiled from: WidgetCallFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<List<? extends VideoCallParticipantView.ParticipantData>, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(List<? extends VideoCallParticipantView.ParticipantData> list) {
            invoke2((List<VideoCallParticipantView.ParticipantData>) list);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(List<VideoCallParticipantView.ParticipantData> list) {
            WidgetCallFullscreenViewModel widgetCallFullscreenViewModel = WidgetCallFullscreenViewModel.this;
            m.checkNotNullExpressionValue(list, "it");
            widgetCallFullscreenViewModel.setOffScreenVideoParticipants(list);
        }
    }

    /* compiled from: WidgetCallFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "GRID", "STAGE", "PRIVATE_CALL_PARTICIPANTS", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum DisplayMode {
        GRID,
        STAGE,
        PRIVATE_CALL_PARTICIPANTS
    }

    /* compiled from: WidgetCallFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0011\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0011\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%¨\u0006&"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "", HookHelper.constructorName, "()V", "AccessibilityAnnouncement", "EnqueueCallFeedbackSheet", "EnqueueStreamFeedbackSheet", "NavigateToPremiumSettings", "OnIdleStateChanged", "RequestStartStream", "ShowActivitiesDesktopOnlyDialog", "ShowCameraCapacityDialog", "ShowGuildVideoAtCapacityDialog", "ShowNoScreenSharePermissionDialog", "ShowNoVadPermissionDialog", "ShowNoVideoPermissionDialog", "ShowRequestCameraPermissionsDialog", "ShowServerDeafenedDialog", "ShowServerMutedDialog", "ShowSuppressedDialog", "ShowUserSheet", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowSuppressedDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerMutedDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerDeafenedDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVideoPermissionDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVadPermissionDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoScreenSharePermissionDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowGuildVideoAtCapacityDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowRequestCameraPermissionsDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueCallFeedbackSheet;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$OnIdleStateChanged;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$RequestStartStream;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$NavigateToPremiumSettings;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowActivitiesDesktopOnlyDialog;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class Event {

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0003\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "", "component1", "()I", "messageResId", "copy", "(I)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getMessageResId", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class AccessibilityAnnouncement extends Event {
            private final int messageResId;

            public AccessibilityAnnouncement(@StringRes int i) {
                super(null);
                this.messageResId = i;
            }

            public static /* synthetic */ AccessibilityAnnouncement copy$default(AccessibilityAnnouncement accessibilityAnnouncement, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = accessibilityAnnouncement.messageResId;
                }
                return accessibilityAnnouncement.copy(i);
            }

            public final int component1() {
                return this.messageResId;
            }

            public final AccessibilityAnnouncement copy(@StringRes int i) {
                return new AccessibilityAnnouncement(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof AccessibilityAnnouncement) && this.messageResId == ((AccessibilityAnnouncement) obj).messageResId;
                }
                return true;
            }

            public final int getMessageResId() {
                return this.messageResId;
            }

            public int hashCode() {
                return this.messageResId;
            }

            public String toString() {
                return a.A(a.R("AccessibilityAnnouncement(messageResId="), this.messageResId, ")");
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\n\u0010\u0010\u001a\u00060\u0002j\u0002`\u0003\u0012\u000e\u0010\u0011\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007\u0012\u000e\u0010\u0012\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\r¢\u0006\u0004\b&\u0010'J\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0018\u0010\b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0018\u0010\u000b\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\tJ\u0010\u0010\f\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\f\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\rHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJV\u0010\u0015\u001a\u00020\u00002\f\b\u0002\u0010\u0010\u001a\u00060\u0002j\u0002`\u00032\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\u0010\b\u0002\u0010\u0012\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n2\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\rHÆ\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0017\u0010\tJ\u0010\u0010\u0018\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u0018\u0010\u000fJ\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0014\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u001e\u001a\u0004\b\u001f\u0010\u000fR\u001d\u0010\u0010\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010 \u001a\u0004\b!\u0010\u0005R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b\"\u0010\u0005R!\u0010\u0012\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010#\u001a\u0004\b$\u0010\tR!\u0010\u0011\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010#\u001a\u0004\b%\u0010\t¨\u0006("}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueCallFeedbackSheet;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "", "Lcom/discord/primitives/ChannelId;", "component1", "()J", "", "Lcom/discord/primitives/RtcConnectionId;", "component2", "()Ljava/lang/String;", "Lcom/discord/primitives/MediaSessionId;", "component3", "component4", "", "component5", "()I", "channelId", "rtcConnectionId", "mediaSessionId", "callDuration", "triggerRateDenominator", "copy", "(JLjava/lang/String;Ljava/lang/String;JI)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueCallFeedbackSheet;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getTriggerRateDenominator", "J", "getChannelId", "getCallDuration", "Ljava/lang/String;", "getMediaSessionId", "getRtcConnectionId", HookHelper.constructorName, "(JLjava/lang/String;Ljava/lang/String;JI)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EnqueueCallFeedbackSheet extends Event {
            private final long callDuration;
            private final long channelId;
            private final String mediaSessionId;
            private final String rtcConnectionId;
            private final int triggerRateDenominator;

            public EnqueueCallFeedbackSheet(long j, String str, String str2, long j2, int i) {
                super(null);
                this.channelId = j;
                this.rtcConnectionId = str;
                this.mediaSessionId = str2;
                this.callDuration = j2;
                this.triggerRateDenominator = i;
            }

            public final long component1() {
                return this.channelId;
            }

            public final String component2() {
                return this.rtcConnectionId;
            }

            public final String component3() {
                return this.mediaSessionId;
            }

            public final long component4() {
                return this.callDuration;
            }

            public final int component5() {
                return this.triggerRateDenominator;
            }

            public final EnqueueCallFeedbackSheet copy(long j, String str, String str2, long j2, int i) {
                return new EnqueueCallFeedbackSheet(j, str, str2, j2, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EnqueueCallFeedbackSheet)) {
                    return false;
                }
                EnqueueCallFeedbackSheet enqueueCallFeedbackSheet = (EnqueueCallFeedbackSheet) obj;
                return this.channelId == enqueueCallFeedbackSheet.channelId && m.areEqual(this.rtcConnectionId, enqueueCallFeedbackSheet.rtcConnectionId) && m.areEqual(this.mediaSessionId, enqueueCallFeedbackSheet.mediaSessionId) && this.callDuration == enqueueCallFeedbackSheet.callDuration && this.triggerRateDenominator == enqueueCallFeedbackSheet.triggerRateDenominator;
            }

            public final long getCallDuration() {
                return this.callDuration;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final String getMediaSessionId() {
                return this.mediaSessionId;
            }

            public final String getRtcConnectionId() {
                return this.rtcConnectionId;
            }

            public final int getTriggerRateDenominator() {
                return this.triggerRateDenominator;
            }

            public int hashCode() {
                int a = b.a(this.channelId) * 31;
                String str = this.rtcConnectionId;
                int i = 0;
                int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
                String str2 = this.mediaSessionId;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return ((b.a(this.callDuration) + ((hashCode + i) * 31)) * 31) + this.triggerRateDenominator;
            }

            public String toString() {
                StringBuilder R = a.R("EnqueueCallFeedbackSheet(channelId=");
                R.append(this.channelId);
                R.append(", rtcConnectionId=");
                R.append(this.rtcConnectionId);
                R.append(", mediaSessionId=");
                R.append(this.mediaSessionId);
                R.append(", callDuration=");
                R.append(this.callDuration);
                R.append(", triggerRateDenominator=");
                return a.A(R, this.triggerRateDenominator, ")");
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\n\u0010\n\u001a\u00060\u0002j\u0002`\u0003\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\f\u001a\u00020\u0007¢\u0006\u0004\b\u001b\u0010\u001cJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0005J\u0010\u0010\b\u001a\u00020\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ4\u0010\r\u001a\u00020\u00002\f\b\u0002\u0010\n\u001a\u00060\u0002j\u0002`\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\f\u001a\u00020\u0007HÆ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0005J\u0010\u0010\u0010\u001a\u00020\u0007HÖ\u0001¢\u0006\u0004\b\u0010\u0010\tJ\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u001b\u0010\u000b\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0005R\u001d\u0010\n\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0018\u0010\u0005R\u0019\u0010\f\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\t¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "", "Lcom/discord/primitives/StreamKey;", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()I", "streamKey", "mediaSessionId", "triggerRateDenominator", "copy", "(Ljava/lang/String;Ljava/lang/String;I)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getMediaSessionId", "getStreamKey", "I", "getTriggerRateDenominator", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class EnqueueStreamFeedbackSheet extends Event {
            private final String mediaSessionId;
            private final String streamKey;
            private final int triggerRateDenominator;

            /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
            public EnqueueStreamFeedbackSheet(String str, String str2, int i) {
                super(null);
                m.checkNotNullParameter(str, "streamKey");
                this.streamKey = str;
                this.mediaSessionId = str2;
                this.triggerRateDenominator = i;
            }

            public static /* synthetic */ EnqueueStreamFeedbackSheet copy$default(EnqueueStreamFeedbackSheet enqueueStreamFeedbackSheet, String str, String str2, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    str = enqueueStreamFeedbackSheet.streamKey;
                }
                if ((i2 & 2) != 0) {
                    str2 = enqueueStreamFeedbackSheet.mediaSessionId;
                }
                if ((i2 & 4) != 0) {
                    i = enqueueStreamFeedbackSheet.triggerRateDenominator;
                }
                return enqueueStreamFeedbackSheet.copy(str, str2, i);
            }

            public final String component1() {
                return this.streamKey;
            }

            public final String component2() {
                return this.mediaSessionId;
            }

            public final int component3() {
                return this.triggerRateDenominator;
            }

            public final EnqueueStreamFeedbackSheet copy(String str, String str2, int i) {
                m.checkNotNullParameter(str, "streamKey");
                return new EnqueueStreamFeedbackSheet(str, str2, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof EnqueueStreamFeedbackSheet)) {
                    return false;
                }
                EnqueueStreamFeedbackSheet enqueueStreamFeedbackSheet = (EnqueueStreamFeedbackSheet) obj;
                return m.areEqual(this.streamKey, enqueueStreamFeedbackSheet.streamKey) && m.areEqual(this.mediaSessionId, enqueueStreamFeedbackSheet.mediaSessionId) && this.triggerRateDenominator == enqueueStreamFeedbackSheet.triggerRateDenominator;
            }

            public final String getMediaSessionId() {
                return this.mediaSessionId;
            }

            public final String getStreamKey() {
                return this.streamKey;
            }

            public final int getTriggerRateDenominator() {
                return this.triggerRateDenominator;
            }

            public int hashCode() {
                String str = this.streamKey;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                String str2 = this.mediaSessionId;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return ((hashCode + i) * 31) + this.triggerRateDenominator;
            }

            public String toString() {
                StringBuilder R = a.R("EnqueueStreamFeedbackSheet(streamKey=");
                R.append(this.streamKey);
                R.append(", mediaSessionId=");
                R.append(this.mediaSessionId);
                R.append(", triggerRateDenominator=");
                return a.A(R, this.triggerRateDenominator, ")");
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$NavigateToPremiumSettings;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class NavigateToPremiumSettings extends Event {
            public static final NavigateToPremiumSettings INSTANCE = new NavigateToPremiumSettings();

            private NavigateToPremiumSettings() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u00022\b\u0010\u000f\u001a\u0004\u0018\u00010\u000eHÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0005\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$OnIdleStateChanged;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "", "component1", "()Z", "isIdle", "copy", "(Z)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$OnIdleStateChanged;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class OnIdleStateChanged extends Event {
            private final boolean isIdle;

            public OnIdleStateChanged(boolean z2) {
                super(null);
                this.isIdle = z2;
            }

            public static /* synthetic */ OnIdleStateChanged copy$default(OnIdleStateChanged onIdleStateChanged, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    z2 = onIdleStateChanged.isIdle;
                }
                return onIdleStateChanged.copy(z2);
            }

            public final boolean component1() {
                return this.isIdle;
            }

            public final OnIdleStateChanged copy(boolean z2) {
                return new OnIdleStateChanged(z2);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof OnIdleStateChanged) && this.isIdle == ((OnIdleStateChanged) obj).isIdle;
                }
                return true;
            }

            public int hashCode() {
                boolean z2 = this.isIdle;
                if (z2) {
                    return 1;
                }
                return z2 ? 1 : 0;
            }

            public final boolean isIdle() {
                return this.isIdle;
            }

            public String toString() {
                return a.M(a.R("OnIdleStateChanged(isIdle="), this.isIdle, ")");
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$RequestStartStream;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class RequestStartStream extends Event {
            public static final RequestStartStream INSTANCE = new RequestStartStream();

            private RequestStartStream() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowActivitiesDesktopOnlyDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowActivitiesDesktopOnlyDialog extends Event {
            public static final ShowActivitiesDesktopOnlyDialog INSTANCE = new ShowActivitiesDesktopOnlyDialog();

            private ShowActivitiesDesktopOnlyDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000b\u0010\u0004J\u001a\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fHÖ\u0003¢\u0006\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0005\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010\u0011\u001a\u0004\b\u0012\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "", "component1", "()I", "guildMaxVideoChannelUsers", "copy", "(I)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getGuildMaxVideoChannelUsers", HookHelper.constructorName, "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowCameraCapacityDialog extends Event {
            private final int guildMaxVideoChannelUsers;

            public ShowCameraCapacityDialog(int i) {
                super(null);
                this.guildMaxVideoChannelUsers = i;
            }

            public static /* synthetic */ ShowCameraCapacityDialog copy$default(ShowCameraCapacityDialog showCameraCapacityDialog, int i, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    i = showCameraCapacityDialog.guildMaxVideoChannelUsers;
                }
                return showCameraCapacityDialog.copy(i);
            }

            public final int component1() {
                return this.guildMaxVideoChannelUsers;
            }

            public final ShowCameraCapacityDialog copy(int i) {
                return new ShowCameraCapacityDialog(i);
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof ShowCameraCapacityDialog) && this.guildMaxVideoChannelUsers == ((ShowCameraCapacityDialog) obj).guildMaxVideoChannelUsers;
                }
                return true;
            }

            public final int getGuildMaxVideoChannelUsers() {
                return this.guildMaxVideoChannelUsers;
            }

            public int hashCode() {
                return this.guildMaxVideoChannelUsers;
            }

            public String toString() {
                return a.A(a.R("ShowCameraCapacityDialog(guildMaxVideoChannelUsers="), this.guildMaxVideoChannelUsers, ")");
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowGuildVideoAtCapacityDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowGuildVideoAtCapacityDialog extends Event {
            public static final ShowGuildVideoAtCapacityDialog INSTANCE = new ShowGuildVideoAtCapacityDialog();

            private ShowGuildVideoAtCapacityDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoScreenSharePermissionDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowNoScreenSharePermissionDialog extends Event {
            public static final ShowNoScreenSharePermissionDialog INSTANCE = new ShowNoScreenSharePermissionDialog();

            private ShowNoScreenSharePermissionDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVadPermissionDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowNoVadPermissionDialog extends Event {
            public static final ShowNoVadPermissionDialog INSTANCE = new ShowNoVadPermissionDialog();

            private ShowNoVadPermissionDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVideoPermissionDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowNoVideoPermissionDialog extends Event {
            public static final ShowNoVideoPermissionDialog INSTANCE = new ShowNoVideoPermissionDialog();

            private ShowNoVideoPermissionDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowRequestCameraPermissionsDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowRequestCameraPermissionsDialog extends Event {
            public static final ShowRequestCameraPermissionsDialog INSTANCE = new ShowRequestCameraPermissionsDialog();

            private ShowRequestCameraPermissionsDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerDeafenedDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowServerDeafenedDialog extends Event {
            public static final ShowServerDeafenedDialog INSTANCE = new ShowServerDeafenedDialog();

            private ShowServerDeafenedDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerMutedDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowServerMutedDialog extends Event {
            public static final ShowServerMutedDialog INSTANCE = new ShowServerMutedDialog();

            private ShowServerMutedDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowSuppressedDialog;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowSuppressedDialog extends Event {
            public static final ShowSuppressedDialog INSTANCE = new ShowSuppressedDialog();

            private ShowSuppressedDialog() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\n\u0010\b\u001a\u00060\u0002j\u0002`\u0003\u0012\n\u0010\t\u001a\u00060\u0002j\u0002`\u0006¢\u0006\u0004\b\u001a\u0010\u001bJ\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0014\u0010\u0007\u001a\u00060\u0002j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0005J,\u0010\n\u001a\u00020\u00002\f\b\u0002\u0010\b\u001a\u00060\u0002j\u0002`\u00032\f\b\u0002\u0010\t\u001a\u00060\u0002j\u0002`\u0006HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\b\u001a\u00060\u0002j\u0002`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u001d\u0010\t\u001a\u00060\u0002j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0019\u0010\u0005¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "", "Lcom/discord/primitives/UserId;", "component1", "()J", "Lcom/discord/primitives/ChannelId;", "component2", "userId", "channelId", "copy", "(JJ)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getUserId", "getChannelId", HookHelper.constructorName, "(JJ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class ShowUserSheet extends Event {
            private final long channelId;
            private final long userId;

            public ShowUserSheet(long j, long j2) {
                super(null);
                this.userId = j;
                this.channelId = j2;
            }

            public static /* synthetic */ ShowUserSheet copy$default(ShowUserSheet showUserSheet, long j, long j2, int i, Object obj) {
                if ((i & 1) != 0) {
                    j = showUserSheet.userId;
                }
                if ((i & 2) != 0) {
                    j2 = showUserSheet.channelId;
                }
                return showUserSheet.copy(j, j2);
            }

            public final long component1() {
                return this.userId;
            }

            public final long component2() {
                return this.channelId;
            }

            public final ShowUserSheet copy(long j, long j2) {
                return new ShowUserSheet(j, j2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ShowUserSheet)) {
                    return false;
                }
                ShowUserSheet showUserSheet = (ShowUserSheet) obj;
                return this.userId == showUserSheet.userId && this.channelId == showUserSheet.channelId;
            }

            public final long getChannelId() {
                return this.channelId;
            }

            public final long getUserId() {
                return this.userId;
            }

            public int hashCode() {
                return b.a(this.channelId) + (b.a(this.userId) * 31);
            }

            public String toString() {
                StringBuilder R = a.R("ShowUserSheet(userId=");
                R.append(this.userId);
                R.append(", channelId=");
                return a.B(R, this.channelId, ")");
            }
        }

        private Event() {
        }

        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetCallFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$MenuItem;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "LAUNCH_OVERLAY", "CHANNEL_SETTINGS", "VOICE_SETTINGS", "SWITCH_CAMERA", "SHOW_PARTICIPANT_LIST", "TEXT_IN_VOICE", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum MenuItem {
        LAUNCH_OVERLAY,
        CHANNEL_SETTINGS,
        VOICE_SETTINGS,
        SWITCH_CAMERA,
        SHOW_PARTICIPANT_LIST,
        TEXT_IN_VOICE
    }

    /* compiled from: WidgetCallFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "DISABLED", "ENABLED", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public enum OverlayStatus {
        DISABLED,
        ENABLED
    }

    /* compiled from: WidgetCallFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0082\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\n\u0010\n\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ(\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\f\b\u0002\u0010\n\u001a\u00060\u0005j\u0002`\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\r\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u001a\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0013\u0010\u0014R\u001d\u0010\n\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0015\u001a\u0004\b\u0016\u0010\bR\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;", "", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/Timestamp;", "component2", "()J", "participantFocusKey", "timestamp", "copy", "(Ljava/lang/String;J)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getTimestamp", "Ljava/lang/String;", "getParticipantFocusKey", HookHelper.constructorName, "(Ljava/lang/String;J)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ParticipantTap {
        private final String participantFocusKey;
        private final long timestamp;

        public ParticipantTap(String str, long j) {
            m.checkNotNullParameter(str, "participantFocusKey");
            this.participantFocusKey = str;
            this.timestamp = j;
        }

        public static /* synthetic */ ParticipantTap copy$default(ParticipantTap participantTap, String str, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                str = participantTap.participantFocusKey;
            }
            if ((i & 2) != 0) {
                j = participantTap.timestamp;
            }
            return participantTap.copy(str, j);
        }

        public final String component1() {
            return this.participantFocusKey;
        }

        public final long component2() {
            return this.timestamp;
        }

        public final ParticipantTap copy(String str, long j) {
            m.checkNotNullParameter(str, "participantFocusKey");
            return new ParticipantTap(str, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ParticipantTap)) {
                return false;
            }
            ParticipantTap participantTap = (ParticipantTap) obj;
            return m.areEqual(this.participantFocusKey, participantTap.participantFocusKey) && this.timestamp == participantTap.timestamp;
        }

        public final String getParticipantFocusKey() {
            return this.participantFocusKey;
        }

        public final long getTimestamp() {
            return this.timestamp;
        }

        public int hashCode() {
            String str = this.participantFocusKey;
            return b.a(this.timestamp) + ((str != null ? str.hashCode() : 0) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("ParticipantTap(participantFocusKey=");
            R.append(this.participantFocusKey);
            R.append(", timestamp=");
            return a.B(R, this.timestamp, ")");
        }
    }

    /* compiled from: WidgetCallFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0004\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0002\u0006\u0007¨\u0006\b"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;", "", HookHelper.constructorName, "()V", "Invalid", "Valid", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Invalid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class StoreState {

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Invalid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends StoreState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u001e\b\u0086\b\u0018\u00002\u00020\u0001B¢\u0001\u0012\u0006\u0010*\u001a\u00020\u0002\u0012\b\u0010+\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010,\u001a\u0004\u0018\u00010\b\u0012\u000e\u0010-\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f\u0012\u0006\u0010.\u001a\u00020\u000f\u0012\b\u0010/\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u00100\u001a\u00020\u0015\u0012\u0006\u00101\u001a\u00020\u0018\u0012\b\u00102\u001a\u0004\u0018\u00010\u001b\u0012\u0006\u00103\u001a\u00020\b\u0012\u0006\u00104\u001a\u00020\b\u0012\u0006\u00105\u001a\u00020\b\u0012\u0006\u00106\u001a\u00020\u0015\u0012\u0006\u00107\u001a\u00020\u0015\u0012\u0016\u00108\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`&\u0012\u0004\u0012\u00020'0%ø\u0001\u0000¢\u0006\u0004\b\\\u0010]J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0018\u0010\r\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0019\u001a\u00020\u0018HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u001b\u0010\u001e\u001a\u0004\u0018\u00010\u001bHÆ\u0003ø\u0001\u0000ø\u0001\u0001ø\u0001\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001f\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b!\u0010 J\u0010\u0010\"\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\"\u0010 J\u0010\u0010#\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b#\u0010\u0017J\u0010\u0010$\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b$\u0010\u0017J \u0010(\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`&\u0012\u0004\u0012\u00020'0%HÆ\u0003¢\u0006\u0004\b(\u0010)JÌ\u0001\u0010;\u001a\u00020\u00002\b\b\u0002\u0010*\u001a\u00020\u00022\n\b\u0002\u0010+\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010,\u001a\u0004\u0018\u00010\b2\u0010\b\u0002\u0010-\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f2\b\b\u0002\u0010.\u001a\u00020\u000f2\n\b\u0002\u0010/\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u00100\u001a\u00020\u00152\b\b\u0002\u00101\u001a\u00020\u00182\n\b\u0002\u00102\u001a\u0004\u0018\u00010\u001b2\b\b\u0002\u00103\u001a\u00020\b2\b\b\u0002\u00104\u001a\u00020\b2\b\b\u0002\u00105\u001a\u00020\b2\b\b\u0002\u00106\u001a\u00020\u00152\b\b\u0002\u00107\u001a\u00020\u00152\u0018\b\u0002\u00108\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`&\u0012\u0004\u0012\u00020'0%HÆ\u0001ø\u0001\u0000ø\u0001\u0002¢\u0006\u0004\b9\u0010:J\u0010\u0010=\u001a\u00020<HÖ\u0001¢\u0006\u0004\b=\u0010>J\u0010\u0010?\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b?\u0010\u0017J\u001a\u0010B\u001a\u00020\b2\b\u0010A\u001a\u0004\u0018\u00010@HÖ\u0003¢\u0006\u0004\bB\u0010CR\u0019\u00103\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b3\u0010D\u001a\u0004\bE\u0010 R\u0019\u00105\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b5\u0010D\u001a\u0004\b5\u0010 R\u0019\u00100\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b0\u0010F\u001a\u0004\bG\u0010\u0017R\u0019\u00106\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b6\u0010F\u001a\u0004\bH\u0010\u0017R$\u00102\u001a\u0004\u0018\u00010\u001b8\u0006@\u0006ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\f\n\u0004\b2\u0010I\u001a\u0004\bJ\u0010\u001dR\u0019\u00107\u001a\u00020\u00158\u0006@\u0006¢\u0006\f\n\u0004\b7\u0010F\u001a\u0004\bK\u0010\u0017R!\u0010-\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\f8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010L\u001a\u0004\bM\u0010\u000eR\u0019\u00104\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b4\u0010D\u001a\u0004\b4\u0010 R\u001b\u0010/\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\f\n\u0004\b/\u0010N\u001a\u0004\bO\u0010\u0014R\u0019\u00101\u001a\u00020\u00188\u0006@\u0006¢\u0006\f\n\u0004\b1\u0010P\u001a\u0004\bQ\u0010\u001aR\u001b\u0010+\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010R\u001a\u0004\bS\u0010\u0007R\u001b\u0010,\u001a\u0004\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b,\u0010T\u001a\u0004\bU\u0010\nR\u0019\u0010*\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010V\u001a\u0004\bW\u0010\u0004R\u0019\u0010.\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b.\u0010X\u001a\u0004\bY\u0010\u0011R)\u00108\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`&\u0012\u0004\u0012\u00020'0%8\u0006@\u0006¢\u0006\f\n\u0004\b8\u0010Z\u001a\u0004\b[\u0010)\u0082\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b¡\u001e0\u0001¨\u0006^"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;", "Lcom/discord/widgets/voice/model/CallModel;", "component1", "()Lcom/discord/widgets/voice/model/CallModel;", "Lcom/discord/widgets/stage/model/StageCallModel;", "component2", "()Lcom/discord/widgets/stage/model/StageCallModel;", "", "component3", "()Ljava/lang/Boolean;", "", "Lcom/discord/api/permission/PermissionBit;", "component4", "()Ljava/lang/Long;", "", "component5", "()F", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "component6", "()Lcom/discord/rtcconnection/RtcConnection$Quality;", "", "component7", "()I", "Lcom/discord/stores/StoreConnectivity$DelayedState;", "component8", "()Lcom/discord/stores/StoreConnectivity$DelayedState;", "Lcom/discord/widgets/stage/StageRoles;", "component9-twRsX-0", "()Lcom/discord/widgets/stage/StageRoles;", "component9", "component10", "()Z", "component11", "component12", "component13", "component14", "", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/api/application/ApplicationAsset;", "component15", "()Ljava/util/Map;", "callModel", "stageCallModel", "noiseCancellation", "myPermissions", "streamVolume", "streamQuality", "mentionCount", "connectivityState", "myStageRoles", "stopOffscreenVideo", "isTextInVoiceEnabled", "isTextInVoiceChannelSelected", "mentionsCount", "unreadsCount", "embeddedAppBackgrounds", "copy-CrE1NWc", "(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/stage/model/StageCallModel;Ljava/lang/Boolean;Ljava/lang/Long;FLcom/discord/rtcconnection/RtcConnection$Quality;ILcom/discord/stores/StoreConnectivity$DelayedState;Lcom/discord/widgets/stage/StageRoles;ZZZIILjava/util/Map;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;", "copy", "", "toString", "()Ljava/lang/String;", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getStopOffscreenVideo", "I", "getMentionCount", "getMentionsCount", "Lcom/discord/widgets/stage/StageRoles;", "getMyStageRoles-twRsX-0", "getUnreadsCount", "Ljava/lang/Long;", "getMyPermissions", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "getStreamQuality", "Lcom/discord/stores/StoreConnectivity$DelayedState;", "getConnectivityState", "Lcom/discord/widgets/stage/model/StageCallModel;", "getStageCallModel", "Ljava/lang/Boolean;", "getNoiseCancellation", "Lcom/discord/widgets/voice/model/CallModel;", "getCallModel", "F", "getStreamVolume", "Ljava/util/Map;", "getEmbeddedAppBackgrounds", HookHelper.constructorName, "(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/stage/model/StageCallModel;Ljava/lang/Boolean;Ljava/lang/Long;FLcom/discord/rtcconnection/RtcConnection$Quality;ILcom/discord/stores/StoreConnectivity$DelayedState;Lcom/discord/widgets/stage/StageRoles;ZZZIILjava/util/Map;Lkotlin/jvm/internal/DefaultConstructorMarker;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends StoreState {
            private final CallModel callModel;
            private final StoreConnectivity.DelayedState connectivityState;
            private final Map<Long, ApplicationAsset> embeddedAppBackgrounds;
            private final boolean isTextInVoiceChannelSelected;
            private final boolean isTextInVoiceEnabled;
            private final int mentionCount;
            private final int mentionsCount;
            private final Long myPermissions;
            private final StageRoles myStageRoles;
            private final Boolean noiseCancellation;
            private final StageCallModel stageCallModel;
            private final boolean stopOffscreenVideo;
            private final RtcConnection.Quality streamQuality;
            private final float streamVolume;
            private final int unreadsCount;

            public /* synthetic */ Valid(CallModel callModel, StageCallModel stageCallModel, Boolean bool, Long l, float f, RtcConnection.Quality quality, int i, StoreConnectivity.DelayedState delayedState, StageRoles stageRoles, boolean z2, boolean z3, boolean z4, int i2, int i3, Map map, DefaultConstructorMarker defaultConstructorMarker) {
                this(callModel, stageCallModel, bool, l, f, quality, i, delayedState, stageRoles, z2, z3, z4, i2, i3, map);
            }

            public final CallModel component1() {
                return this.callModel;
            }

            public final boolean component10() {
                return this.stopOffscreenVideo;
            }

            public final boolean component11() {
                return this.isTextInVoiceEnabled;
            }

            public final boolean component12() {
                return this.isTextInVoiceChannelSelected;
            }

            public final int component13() {
                return this.mentionsCount;
            }

            public final int component14() {
                return this.unreadsCount;
            }

            public final Map<Long, ApplicationAsset> component15() {
                return this.embeddedAppBackgrounds;
            }

            public final StageCallModel component2() {
                return this.stageCallModel;
            }

            public final Boolean component3() {
                return this.noiseCancellation;
            }

            public final Long component4() {
                return this.myPermissions;
            }

            public final float component5() {
                return this.streamVolume;
            }

            public final RtcConnection.Quality component6() {
                return this.streamQuality;
            }

            public final int component7() {
                return this.mentionCount;
            }

            public final StoreConnectivity.DelayedState component8() {
                return this.connectivityState;
            }

            /* renamed from: component9-twRsX-0  reason: not valid java name */
            public final StageRoles m60component9twRsX0() {
                return this.myStageRoles;
            }

            /* renamed from: copy-CrE1NWc  reason: not valid java name */
            public final Valid m61copyCrE1NWc(CallModel callModel, StageCallModel stageCallModel, Boolean bool, Long l, float f, RtcConnection.Quality quality, int i, StoreConnectivity.DelayedState delayedState, StageRoles stageRoles, boolean z2, boolean z3, boolean z4, int i2, int i3, Map<Long, ApplicationAsset> map) {
                m.checkNotNullParameter(callModel, "callModel");
                m.checkNotNullParameter(delayedState, "connectivityState");
                m.checkNotNullParameter(map, "embeddedAppBackgrounds");
                return new Valid(callModel, stageCallModel, bool, l, f, quality, i, delayedState, stageRoles, z2, z3, z4, i2, i3, map);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.callModel, valid.callModel) && m.areEqual(this.stageCallModel, valid.stageCallModel) && m.areEqual(this.noiseCancellation, valid.noiseCancellation) && m.areEqual(this.myPermissions, valid.myPermissions) && Float.compare(this.streamVolume, valid.streamVolume) == 0 && m.areEqual(this.streamQuality, valid.streamQuality) && this.mentionCount == valid.mentionCount && m.areEqual(this.connectivityState, valid.connectivityState) && m.areEqual(this.myStageRoles, valid.myStageRoles) && this.stopOffscreenVideo == valid.stopOffscreenVideo && this.isTextInVoiceEnabled == valid.isTextInVoiceEnabled && this.isTextInVoiceChannelSelected == valid.isTextInVoiceChannelSelected && this.mentionsCount == valid.mentionsCount && this.unreadsCount == valid.unreadsCount && m.areEqual(this.embeddedAppBackgrounds, valid.embeddedAppBackgrounds);
            }

            public final CallModel getCallModel() {
                return this.callModel;
            }

            public final StoreConnectivity.DelayedState getConnectivityState() {
                return this.connectivityState;
            }

            public final Map<Long, ApplicationAsset> getEmbeddedAppBackgrounds() {
                return this.embeddedAppBackgrounds;
            }

            public final int getMentionCount() {
                return this.mentionCount;
            }

            public final int getMentionsCount() {
                return this.mentionsCount;
            }

            public final Long getMyPermissions() {
                return this.myPermissions;
            }

            /* renamed from: getMyStageRoles-twRsX-0  reason: not valid java name */
            public final StageRoles m62getMyStageRolestwRsX0() {
                return this.myStageRoles;
            }

            public final Boolean getNoiseCancellation() {
                return this.noiseCancellation;
            }

            public final StageCallModel getStageCallModel() {
                return this.stageCallModel;
            }

            public final boolean getStopOffscreenVideo() {
                return this.stopOffscreenVideo;
            }

            public final RtcConnection.Quality getStreamQuality() {
                return this.streamQuality;
            }

            public final float getStreamVolume() {
                return this.streamVolume;
            }

            public final int getUnreadsCount() {
                return this.unreadsCount;
            }

            public int hashCode() {
                CallModel callModel = this.callModel;
                int i = 0;
                int hashCode = (callModel != null ? callModel.hashCode() : 0) * 31;
                StageCallModel stageCallModel = this.stageCallModel;
                int hashCode2 = (hashCode + (stageCallModel != null ? stageCallModel.hashCode() : 0)) * 31;
                Boolean bool = this.noiseCancellation;
                int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
                Long l = this.myPermissions;
                int floatToIntBits = (Float.floatToIntBits(this.streamVolume) + ((hashCode3 + (l != null ? l.hashCode() : 0)) * 31)) * 31;
                RtcConnection.Quality quality = this.streamQuality;
                int hashCode4 = (((floatToIntBits + (quality != null ? quality.hashCode() : 0)) * 31) + this.mentionCount) * 31;
                StoreConnectivity.DelayedState delayedState = this.connectivityState;
                int hashCode5 = (hashCode4 + (delayedState != null ? delayedState.hashCode() : 0)) * 31;
                StageRoles stageRoles = this.myStageRoles;
                int hashCode6 = (hashCode5 + (stageRoles != null ? stageRoles.hashCode() : 0)) * 31;
                boolean z2 = this.stopOffscreenVideo;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode6 + i3) * 31;
                boolean z3 = this.isTextInVoiceEnabled;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (i5 + i6) * 31;
                boolean z4 = this.isTextInVoiceChannelSelected;
                if (!z4) {
                    i2 = z4 ? 1 : 0;
                }
                int i9 = (((((i8 + i2) * 31) + this.mentionsCount) * 31) + this.unreadsCount) * 31;
                Map<Long, ApplicationAsset> map = this.embeddedAppBackgrounds;
                if (map != null) {
                    i = map.hashCode();
                }
                return i9 + i;
            }

            public final boolean isTextInVoiceChannelSelected() {
                return this.isTextInVoiceChannelSelected;
            }

            public final boolean isTextInVoiceEnabled() {
                return this.isTextInVoiceEnabled;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(callModel=");
                R.append(this.callModel);
                R.append(", stageCallModel=");
                R.append(this.stageCallModel);
                R.append(", noiseCancellation=");
                R.append(this.noiseCancellation);
                R.append(", myPermissions=");
                R.append(this.myPermissions);
                R.append(", streamVolume=");
                R.append(this.streamVolume);
                R.append(", streamQuality=");
                R.append(this.streamQuality);
                R.append(", mentionCount=");
                R.append(this.mentionCount);
                R.append(", connectivityState=");
                R.append(this.connectivityState);
                R.append(", myStageRoles=");
                R.append(this.myStageRoles);
                R.append(", stopOffscreenVideo=");
                R.append(this.stopOffscreenVideo);
                R.append(", isTextInVoiceEnabled=");
                R.append(this.isTextInVoiceEnabled);
                R.append(", isTextInVoiceChannelSelected=");
                R.append(this.isTextInVoiceChannelSelected);
                R.append(", mentionsCount=");
                R.append(this.mentionsCount);
                R.append(", unreadsCount=");
                R.append(this.unreadsCount);
                R.append(", embeddedAppBackgrounds=");
                return a.L(R, this.embeddedAppBackgrounds, ")");
            }

            private Valid(CallModel callModel, StageCallModel stageCallModel, Boolean bool, Long l, float f, RtcConnection.Quality quality, int i, StoreConnectivity.DelayedState delayedState, StageRoles stageRoles, boolean z2, boolean z3, boolean z4, int i2, int i3, Map<Long, ApplicationAsset> map) {
                super(null);
                this.callModel = callModel;
                this.stageCallModel = stageCallModel;
                this.noiseCancellation = bool;
                this.myPermissions = l;
                this.streamVolume = f;
                this.streamQuality = quality;
                this.mentionCount = i;
                this.connectivityState = delayedState;
                this.myStageRoles = stageRoles;
                this.stopOffscreenVideo = z2;
                this.isTextInVoiceEnabled = z3;
                this.isTextInVoiceChannelSelected = z4;
                this.mentionsCount = i2;
                this.unreadsCount = i3;
                this.embeddedAppBackgrounds = map;
            }
        }

        private StoreState() {
        }

        public /* synthetic */ StoreState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetCallFullscreenViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0004\u0005\u0006B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003\u0082\u0001\u0003\u0007\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;", "", HookHelper.constructorName, "()V", "Invalid", "Uninitialized", "Valid", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Invalid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static abstract class ViewState {

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Invalid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Invalid extends ViewState {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Uninitialized;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Uninitialized extends ViewState {
            public static final Uninitialized INSTANCE = new Uninitialized();

            private Uninitialized() {
                super(null);
            }
        }

        /* compiled from: WidgetCallFullscreenViewModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000¶\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010$\n\u0002\b'\n\u0002\u0010\u0000\n\u0002\bC\b\u0086\b\u0018\u00002\u00020\u0001BÂ\u0002\u0012\u0006\u0010S\u001a\u00020\u000f\u0012\b\u0010T\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010U\u001a\u00020\u0015\u0012\f\u0010V\u001a\b\u0012\u0004\u0012\u00020\u00180\b\u0012\u0006\u0010W\u001a\u00020\u0002\u0012\u0006\u0010X\u001a\u00020\u0002\u0012\u0006\u0010Y\u001a\u00020\u0002\u0012\u0006\u0010Z\u001a\u00020\u001f\u0012\u0006\u0010[\u001a\u00020\"\u0012\u0006\u0010\\\u001a\u00020%\u0012\b\u0010]\u001a\u0004\u0018\u00010(\u0012\u0006\u0010^\u001a\u00020\u0002\u0012\b\u0010_\u001a\u0004\u0018\u00010,\u0012\b\u0010`\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010a\u001a\b\u0012\u0004\u0012\u0002010\b\u0012\u0006\u0010b\u001a\u00020\u0002\u0012\u0006\u0010c\u001a\u00020\u0002\u0012\u0006\u0010d\u001a\u000205\u0012\u0006\u0010e\u001a\u000208\u0012\u0006\u0010f\u001a\u00020\u0002\u0012\b\u0010g\u001a\u0004\u0018\u00010<\u0012\u0006\u0010h\u001a\u00020\u0002\u0012\u0006\u0010i\u001a\u00020\u0002\u0012\u0006\u0010j\u001a\u000208\u0012\u0006\u0010k\u001a\u00020\u0002\u0012\u000e\u0010l\u001a\n\u0018\u00010Dj\u0004\u0018\u0001`E\u0012\b\u0010m\u001a\u0004\u0018\u00010H\u0012\b\u0010n\u001a\u0004\u0018\u00010H\u0012\u0006\u0010o\u001a\u000208\u0012\u0006\u0010p\u001a\u000208\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010q\u001a\u00020\u0002\u0012\u0012\u0010r\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020,0Pø\u0001\u0000¢\u0006\u0006\b¹\u0001\u0010º\u0001JM\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00022\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\b2\u0006\u0010\n\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÆ\u0003¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\bHÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001d\u0010\u001cJ\u0010\u0010\u001e\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u001e\u0010\u001cJ\u0010\u0010 \u001a\u00020\u001fHÆ\u0003¢\u0006\u0004\b \u0010!J\u0010\u0010#\u001a\u00020\"HÆ\u0003¢\u0006\u0004\b#\u0010$J\u0010\u0010&\u001a\u00020%HÆ\u0003¢\u0006\u0004\b&\u0010'J\u0012\u0010)\u001a\u0004\u0018\u00010(HÆ\u0003¢\u0006\u0004\b)\u0010*J\u0010\u0010+\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b+\u0010\u001cJ\u0012\u0010-\u001a\u0004\u0018\u00010,HÆ\u0003¢\u0006\u0004\b-\u0010.J\u0012\u0010/\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b/\u00100J\u0016\u00102\u001a\b\u0012\u0004\u0012\u0002010\bHÆ\u0003¢\u0006\u0004\b2\u0010\u001aJ\u0010\u00103\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b3\u0010\u001cJ\u0010\u00104\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b4\u0010\u001cJ\u0010\u00106\u001a\u000205HÆ\u0003¢\u0006\u0004\b6\u00107J\u0010\u00109\u001a\u000208HÆ\u0003¢\u0006\u0004\b9\u0010:J\u0010\u0010;\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b;\u0010\u001cJ\u001b\u0010?\u001a\u0004\u0018\u00010<HÆ\u0003ø\u0001\u0000ø\u0001\u0001ø\u0001\u0002¢\u0006\u0004\b=\u0010>J\u0010\u0010@\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b@\u0010\u001cJ\u0010\u0010A\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\bA\u0010\u001cJ\u0010\u0010B\u001a\u000208HÆ\u0003¢\u0006\u0004\bB\u0010:J\u0010\u0010C\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\bC\u0010\u001cJ\u0018\u0010F\u001a\n\u0018\u00010Dj\u0004\u0018\u0001`EHÆ\u0003¢\u0006\u0004\bF\u0010GJ\u0012\u0010I\u001a\u0004\u0018\u00010HHÆ\u0003¢\u0006\u0004\bI\u0010JJ\u0012\u0010K\u001a\u0004\u0018\u00010HHÆ\u0003¢\u0006\u0004\bK\u0010JJ\u0010\u0010L\u001a\u000208HÆ\u0003¢\u0006\u0004\bL\u0010:J\u0010\u0010M\u001a\u000208HÆ\u0003¢\u0006\u0004\bM\u0010:J\u0010\u0010N\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\bN\u0010\u001cJ\u0010\u0010O\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\bO\u0010\u001cJ\u001c\u0010Q\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020,0PHÆ\u0003¢\u0006\u0004\bQ\u0010RJ\u008e\u0003\u0010u\u001a\u00020\u00002\b\b\u0002\u0010S\u001a\u00020\u000f2\n\b\u0002\u0010T\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010U\u001a\u00020\u00152\u000e\b\u0002\u0010V\u001a\b\u0012\u0004\u0012\u00020\u00180\b2\b\b\u0002\u0010W\u001a\u00020\u00022\b\b\u0002\u0010X\u001a\u00020\u00022\b\b\u0002\u0010Y\u001a\u00020\u00022\b\b\u0002\u0010Z\u001a\u00020\u001f2\b\b\u0002\u0010[\u001a\u00020\"2\b\b\u0002\u0010\\\u001a\u00020%2\n\b\u0002\u0010]\u001a\u0004\u0018\u00010(2\b\b\u0002\u0010^\u001a\u00020\u00022\n\b\u0002\u0010_\u001a\u0004\u0018\u00010,2\n\b\u0002\u0010`\u001a\u0004\u0018\u00010\u00022\u000e\b\u0002\u0010a\u001a\b\u0012\u0004\u0012\u0002010\b2\b\b\u0002\u0010b\u001a\u00020\u00022\b\b\u0002\u0010c\u001a\u00020\u00022\b\b\u0002\u0010d\u001a\u0002052\b\b\u0002\u0010e\u001a\u0002082\b\b\u0002\u0010f\u001a\u00020\u00022\n\b\u0002\u0010g\u001a\u0004\u0018\u00010<2\b\b\u0002\u0010h\u001a\u00020\u00022\b\b\u0002\u0010i\u001a\u00020\u00022\b\b\u0002\u0010j\u001a\u0002082\b\b\u0002\u0010k\u001a\u00020\u00022\u0010\b\u0002\u0010l\u001a\n\u0018\u00010Dj\u0004\u0018\u0001`E2\n\b\u0002\u0010m\u001a\u0004\u0018\u00010H2\n\b\u0002\u0010n\u001a\u0004\u0018\u00010H2\b\b\u0002\u0010o\u001a\u0002082\b\b\u0002\u0010p\u001a\u0002082\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010q\u001a\u00020\u00022\u0014\b\u0002\u0010r\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020,0PHÆ\u0001ø\u0001\u0000ø\u0001\u0002¢\u0006\u0004\bs\u0010tJ\u0010\u0010v\u001a\u00020(HÖ\u0001¢\u0006\u0004\bv\u0010*J\u0010\u0010w\u001a\u000208HÖ\u0001¢\u0006\u0004\bw\u0010:J\u001a\u0010z\u001a\u00020\u00022\b\u0010y\u001a\u0004\u0018\u00010xHÖ\u0003¢\u0006\u0004\bz\u0010{R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010|\u001a\u0004\b\n\u0010\u001cR\u001f\u0010V\u001a\b\u0012\u0004\u0012\u00020\u00180\b8\u0006@\u0006¢\u0006\f\n\u0004\bV\u0010}\u001a\u0004\b~\u0010\u001aR&\u0010r\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020,0P8\u0006@\u0006¢\u0006\r\n\u0004\br\u0010\u007f\u001a\u0005\b\u0080\u0001\u0010RR\u001b\u0010e\u001a\u0002088\u0006@\u0006¢\u0006\u000e\n\u0005\be\u0010\u0081\u0001\u001a\u0005\b\u0082\u0001\u0010:R\u001d\u0010n\u001a\u0004\u0018\u00010H8\u0006@\u0006¢\u0006\u000e\n\u0005\bn\u0010\u0083\u0001\u001a\u0005\b\u0084\u0001\u0010JR\u0019\u0010i\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bi\u0010|\u001a\u0004\bi\u0010\u001cR\u001b\u0010j\u001a\u0002088\u0006@\u0006¢\u0006\u000e\n\u0005\bj\u0010\u0081\u0001\u001a\u0005\b\u0085\u0001\u0010:R\u001c\u0010\u0086\u0001\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0086\u0001\u0010|\u001a\u0005\b\u0086\u0001\u0010\u001cR\u001b\u0010S\u001a\u00020\u000f8\u0006@\u0006¢\u0006\u000e\n\u0005\bS\u0010\u0087\u0001\u001a\u0005\b\u0088\u0001\u0010\u0011R\u001a\u0010c\u001a\u00020\u00028\u0006@\u0006¢\u0006\r\n\u0004\bc\u0010|\u001a\u0005\b\u0089\u0001\u0010\u001cR#\u0010l\u001a\n\u0018\u00010Dj\u0004\u0018\u0001`E8\u0006@\u0006¢\u0006\u000e\n\u0005\bl\u0010\u008a\u0001\u001a\u0005\b\u008b\u0001\u0010GR\u001b\u0010U\u001a\u00020\u00158\u0006@\u0006¢\u0006\u000e\n\u0005\bU\u0010\u008c\u0001\u001a\u0005\b\u008d\u0001\u0010\u0017R\u001c\u0010\u008e\u0001\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008e\u0001\u0010|\u001a\u0005\b\u008e\u0001\u0010\u001cR\u0019\u0010h\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bh\u0010|\u001a\u0004\bh\u0010\u001cR\u001c\u0010\u008f\u0001\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u008f\u0001\u0010|\u001a\u0005\b\u008f\u0001\u0010\u001cR\u001d\u0010_\u001a\u0004\u0018\u00010,8\u0006@\u0006¢\u0006\u000e\n\u0005\b_\u0010\u0090\u0001\u001a\u0005\b\u0091\u0001\u0010.R\u001c\u0010\u0092\u0001\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0092\u0001\u0010|\u001a\u0005\b\u0093\u0001\u0010\u001cR\u001a\u0010b\u001a\u00020\u00028\u0006@\u0006¢\u0006\r\n\u0004\bb\u0010|\u001a\u0005\b\u0094\u0001\u0010\u001cR\u001c\u0010\u0095\u0001\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0095\u0001\u0010|\u001a\u0005\b\u0095\u0001\u0010\u001cR\u001d\u0010T\u001a\u0004\u0018\u00010\u00128\u0006@\u0006¢\u0006\u000e\n\u0005\bT\u0010\u0096\u0001\u001a\u0005\b\u0097\u0001\u0010\u0014R\u001b\u0010Z\u001a\u00020\u001f8\u0006@\u0006¢\u0006\u000e\n\u0005\bZ\u0010\u0098\u0001\u001a\u0005\b\u0099\u0001\u0010!R\u001b\u0010d\u001a\u0002058\u0006@\u0006¢\u0006\u000e\n\u0005\bd\u0010\u009a\u0001\u001a\u0005\b\u009b\u0001\u00107R\u001c\u0010\u009c\u0001\u001a\u00020\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b\u009c\u0001\u0010|\u001a\u0005\b\u009d\u0001\u0010\u001cR(\u0010¢\u0001\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8F@\u0006X\u0086\u0084\u0002¢\u0006\u0010\n\u0006\b\u009e\u0001\u0010\u009f\u0001\u001a\u0006\b \u0001\u0010¡\u0001R\u0019\u0010Y\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bY\u0010|\u001a\u0004\bY\u0010\u001cR#\u0010¥\u0001\u001a\u0004\u0018\u00010(8F@\u0006X\u0086\u0084\u0002¢\u0006\u000f\n\u0006\b£\u0001\u0010\u009f\u0001\u001a\u0005\b¤\u0001\u0010*R\u001a\u0010k\u001a\u00020\u00028\u0006@\u0006¢\u0006\r\n\u0004\bk\u0010|\u001a\u0005\b¦\u0001\u0010\u001cR\u001b\u0010p\u001a\u0002088\u0006@\u0006¢\u0006\u000e\n\u0005\bp\u0010\u0081\u0001\u001a\u0005\b§\u0001\u0010:R\u001b\u0010[\u001a\u00020\"8\u0006@\u0006¢\u0006\u000e\n\u0005\b[\u0010¨\u0001\u001a\u0005\b©\u0001\u0010$R\u0019\u0010W\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bW\u0010|\u001a\u0004\bW\u0010\u001cR\u0019\u0010X\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010|\u001a\u0004\bX\u0010\u001cR\u001a\u0010f\u001a\u00020\u00028\u0006@\u0006¢\u0006\r\n\u0004\bf\u0010|\u001a\u0005\bª\u0001\u0010\u001cR&\u0010g\u001a\u0004\u0018\u00010<8\u0006@\u0006ø\u0001\u0000ø\u0001\u0002ø\u0001\u0001¢\u0006\u000e\n\u0005\bg\u0010«\u0001\u001a\u0005\b¬\u0001\u0010>R\u0019\u0010q\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\bq\u0010|\u001a\u0004\bq\u0010\u001cR\u001d\u0010`\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\u000e\n\u0005\b`\u0010\u00ad\u0001\u001a\u0005\b®\u0001\u00100R\u001b\u0010o\u001a\u0002088\u0006@\u0006¢\u0006\u000e\n\u0005\bo\u0010\u0081\u0001\u001a\u0005\b¯\u0001\u0010:R \u0010a\u001a\b\u0012\u0004\u0012\u0002010\b8\u0006@\u0006¢\u0006\r\n\u0004\ba\u0010}\u001a\u0005\b°\u0001\u0010\u001aR\u001d\u0010m\u001a\u0004\u0018\u00010H8\u0006@\u0006¢\u0006\u000e\n\u0005\bm\u0010\u0083\u0001\u001a\u0005\b±\u0001\u0010JR\u0019\u0010^\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b^\u0010|\u001a\u0004\b^\u0010\u001cR#\u0010´\u0001\u001a\u0004\u0018\u00010(8F@\u0006X\u0086\u0084\u0002¢\u0006\u000f\n\u0006\b²\u0001\u0010\u009f\u0001\u001a\u0005\b³\u0001\u0010*R\u001d\u0010]\u001a\u0004\u0018\u00010(8\u0006@\u0006¢\u0006\u000e\n\u0005\b]\u0010µ\u0001\u001a\u0005\b¶\u0001\u0010*R\u001b\u0010\\\u001a\u00020%8\u0006@\u0006¢\u0006\u000e\n\u0005\b\\\u0010·\u0001\u001a\u0005\b¸\u0001\u0010'\u0082\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b¡\u001e0\u0001¨\u0006»\u0001"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;", "", "canManageChannel", "isConnected", "isStageChannel", "Lco/discord/media_engine/VideoInputDeviceDescription;", "selectedVideoInputDevice", "", "availableVideoInputDevices", "isTextInVoiceEnabled", "", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$MenuItem;", "computeMenuItems", "(ZZZLco/discord/media_engine/VideoInputDeviceDescription;Ljava/util/List;Z)Ljava/util/Set;", "Lcom/discord/widgets/voice/model/CallModel;", "component1", "()Lcom/discord/widgets/voice/model/CallModel;", "Lcom/discord/widgets/stage/model/StageCallModel;", "component2", "()Lcom/discord/widgets/stage/model/StageCallModel;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;", "component3", "()Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;", "Lcom/discord/widgets/voice/fullscreen/CallParticipant;", "component4", "()Ljava/util/List;", "component5", "()Z", "component6", "component7", "Lcom/discord/widgets/voice/model/CameraState;", "component8", "()Lcom/discord/widgets/voice/model/CameraState;", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;", "component9", "()Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;", "Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;", "component10", "()Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;", "", "component11", "()Ljava/lang/String;", "component12", "Lcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;", "component13", "()Lcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;", "component14", "()Ljava/lang/Boolean;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component15", "component16", "component17", "", "component18", "()F", "", "component19", "()I", "component20", "Lcom/discord/widgets/stage/StageRoles;", "component21-twRsX-0", "()Lcom/discord/widgets/stage/StageRoles;", "component21", "component22", "component23", "component24", "component25", "", "Lcom/discord/api/permission/PermissionBit;", "component26", "()Ljava/lang/Long;", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component27", "()Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component28", "component29", "component30", "component31", "component32", "", "component33", "()Ljava/util/Map;", "callModel", "stageCallModel", "overlayStatus", "visibleVideoParticipants", "isIdle", "isControlFadingDisabled", "isSwitchCameraButtonVisible", "cameraState", "displayMode", "outputSelectorState", "focusedParticipantKey", "isAnyoneUsingVideo", "pipParticipant", "noiseCancellation", "privateCallUserListItems", "showParticipantsHiddenView", "startedAsVideo", "perceptualStreamVolume", "mentionCount", "showLowConnectivityBar", "stageRoles", "isUpdatingRequestToSpeak", "isMovingToAudience", "requestingToSpeakCount", "stopOffscreenVideo", "channelPermissions", "startableEvent", "activeEvent", "textInVoiceMentionCount", "textInVoiceUnreadCount", "isTextInVoiceChannelSelected", "allVideoParticipants", "copy-6O6tUOw", "(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/stage/model/StageCallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;Ljava/lang/Boolean;Ljava/util/List;ZZFIZLcom/discord/widgets/stage/StageRoles;ZZIZLjava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;IIZZLjava/util/Map;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;", "copy", "toString", "hashCode", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "Ljava/util/List;", "getVisibleVideoParticipants", "Ljava/util/Map;", "getAllVideoParticipants", "I", "getMentionCount", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "getActiveEvent", "getRequestingToSpeakCount", "isVideoCallGridVisible", "Lcom/discord/widgets/voice/model/CallModel;", "getCallModel", "getStartedAsVideo", "Ljava/lang/Long;", "getChannelPermissions", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;", "getOverlayStatus", "isDeafened", "isPushToTalk", "Lcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;", "getPipParticipant", "showControls", "getShowControls", "getShowParticipantsHiddenView", "isStreamFocused", "Lcom/discord/widgets/stage/model/StageCallModel;", "getStageCallModel", "Lcom/discord/widgets/voice/model/CameraState;", "getCameraState", "F", "getPerceptualStreamVolume", "showStreamVolume", "getShowStreamVolume", "menuItems$delegate", "Lkotlin/Lazy;", "getMenuItems", "()Ljava/util/Set;", "menuItems", "titleText$delegate", "getTitleText", "titleText", "getStopOffscreenVideo", "getTextInVoiceUnreadCount", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;", "getDisplayMode", "getShowLowConnectivityBar", "Lcom/discord/widgets/stage/StageRoles;", "getStageRoles-twRsX-0", "Ljava/lang/Boolean;", "getNoiseCancellation", "getTextInVoiceMentionCount", "getPrivateCallUserListItems", "getStartableEvent", "analyticsVideoLayout$delegate", "getAnalyticsVideoLayout", "analyticsVideoLayout", "Ljava/lang/String;", "getFocusedParticipantKey", "Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;", "getOutputSelectorState", HookHelper.constructorName, "(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/stage/model/StageCallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;Ljava/lang/Boolean;Ljava/util/List;ZZFIZLcom/discord/widgets/stage/StageRoles;ZZIZLjava/lang/Long;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;IIZZLjava/util/Map;Lkotlin/jvm/internal/DefaultConstructorMarker;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Valid extends ViewState {
            private final GuildScheduledEvent activeEvent;
            private final Map<String, CallParticipant.UserOrStreamParticipant> allVideoParticipants;
            private final Lazy analyticsVideoLayout$delegate;
            private final CallModel callModel;
            private final CameraState cameraState;
            private final Long channelPermissions;
            private final DisplayMode displayMode;
            private final String focusedParticipantKey;
            private final boolean isAnyoneUsingVideo;
            private final boolean isControlFadingDisabled;
            private final boolean isDeafened;
            private final boolean isIdle;
            private final boolean isMovingToAudience;
            private final boolean isPushToTalk;
            private final boolean isStreamFocused;
            private final boolean isSwitchCameraButtonVisible;
            private final boolean isTextInVoiceChannelSelected;
            private final boolean isTextInVoiceEnabled;
            private final boolean isUpdatingRequestToSpeak;
            private final boolean isVideoCallGridVisible;
            private final int mentionCount;
            private final Lazy menuItems$delegate;
            private final Boolean noiseCancellation;
            private final VoiceControlsOutputSelectorState outputSelectorState;
            private final OverlayStatus overlayStatus;
            private final float perceptualStreamVolume;
            private final CallParticipant.UserOrStreamParticipant pipParticipant;
            private final List<StoreVoiceParticipants.VoiceUser> privateCallUserListItems;
            private final int requestingToSpeakCount;
            private final boolean showControls;
            private final boolean showLowConnectivityBar;
            private final boolean showParticipantsHiddenView;
            private final boolean showStreamVolume;
            private final StageCallModel stageCallModel;
            private final StageRoles stageRoles;
            private final GuildScheduledEvent startableEvent;
            private final boolean startedAsVideo;
            private final boolean stopOffscreenVideo;
            private final int textInVoiceMentionCount;
            private final int textInVoiceUnreadCount;
            private final Lazy titleText$delegate;
            private final List<CallParticipant> visibleVideoParticipants;

            public /* synthetic */ Valid(CallModel callModel, StageCallModel stageCallModel, OverlayStatus overlayStatus, List list, boolean z2, boolean z3, boolean z4, CameraState cameraState, DisplayMode displayMode, VoiceControlsOutputSelectorState voiceControlsOutputSelectorState, String str, boolean z5, CallParticipant.UserOrStreamParticipant userOrStreamParticipant, Boolean bool, List list2, boolean z6, boolean z7, float f, int i, boolean z8, StageRoles stageRoles, boolean z9, boolean z10, int i2, boolean z11, Long l, GuildScheduledEvent guildScheduledEvent, GuildScheduledEvent guildScheduledEvent2, int i3, int i4, boolean z12, boolean z13, Map map, DefaultConstructorMarker defaultConstructorMarker) {
                this(callModel, stageCallModel, overlayStatus, list, z2, z3, z4, cameraState, displayMode, voiceControlsOutputSelectorState, str, z5, userOrStreamParticipant, bool, list2, z6, z7, f, i, z8, stageRoles, z9, z10, i2, z11, l, guildScheduledEvent, guildScheduledEvent2, i3, i4, z12, z13, map);
            }

            /* JADX INFO: Access modifiers changed from: private */
            public final Set<MenuItem> computeMenuItems(boolean z2, boolean z3, boolean z4, VideoInputDeviceDescription videoInputDeviceDescription, List<VideoInputDeviceDescription> list, boolean z5) {
                HashSet hashSetOf = n0.hashSetOf(MenuItem.VOICE_SETTINGS);
                if (z2) {
                    hashSetOf.add(MenuItem.CHANNEL_SETTINGS);
                }
                if (z3 && StoreStream.Companion.getUserSettings().getIsMobileOverlayEnabled()) {
                    hashSetOf.add(MenuItem.LAUNCH_OVERLAY);
                }
                if (videoInputDeviceDescription != null && list.size() > 1) {
                    hashSetOf.add(MenuItem.SWITCH_CAMERA);
                }
                if (!z4 && z5) {
                    hashSetOf.add(MenuItem.TEXT_IN_VOICE);
                }
                if (!z4) {
                    hashSetOf.add(MenuItem.SHOW_PARTICIPANT_LIST);
                }
                return hashSetOf;
            }

            /* JADX WARN: Multi-variable type inference failed */
            /* renamed from: copy-6O6tUOw$default  reason: not valid java name */
            public static /* synthetic */ Valid m63copy6O6tUOw$default(Valid valid, CallModel callModel, StageCallModel stageCallModel, OverlayStatus overlayStatus, List list, boolean z2, boolean z3, boolean z4, CameraState cameraState, DisplayMode displayMode, VoiceControlsOutputSelectorState voiceControlsOutputSelectorState, String str, boolean z5, CallParticipant.UserOrStreamParticipant userOrStreamParticipant, Boolean bool, List list2, boolean z6, boolean z7, float f, int i, boolean z8, StageRoles stageRoles, boolean z9, boolean z10, int i2, boolean z11, Long l, GuildScheduledEvent guildScheduledEvent, GuildScheduledEvent guildScheduledEvent2, int i3, int i4, boolean z12, boolean z13, Map map, int i5, int i6, Object obj) {
                return valid.m65copy6O6tUOw((i5 & 1) != 0 ? valid.callModel : callModel, (i5 & 2) != 0 ? valid.stageCallModel : stageCallModel, (i5 & 4) != 0 ? valid.overlayStatus : overlayStatus, (i5 & 8) != 0 ? valid.visibleVideoParticipants : list, (i5 & 16) != 0 ? valid.isIdle : z2, (i5 & 32) != 0 ? valid.isControlFadingDisabled : z3, (i5 & 64) != 0 ? valid.isSwitchCameraButtonVisible : z4, (i5 & 128) != 0 ? valid.cameraState : cameraState, (i5 & 256) != 0 ? valid.displayMode : displayMode, (i5 & 512) != 0 ? valid.outputSelectorState : voiceControlsOutputSelectorState, (i5 & 1024) != 0 ? valid.focusedParticipantKey : str, (i5 & 2048) != 0 ? valid.isAnyoneUsingVideo : z5, (i5 & 4096) != 0 ? valid.pipParticipant : userOrStreamParticipant, (i5 & 8192) != 0 ? valid.noiseCancellation : bool, (i5 & 16384) != 0 ? valid.privateCallUserListItems : list2, (i5 & 32768) != 0 ? valid.showParticipantsHiddenView : z6, (i5 & 65536) != 0 ? valid.startedAsVideo : z7, (i5 & 131072) != 0 ? valid.perceptualStreamVolume : f, (i5 & 262144) != 0 ? valid.mentionCount : i, (i5 & 524288) != 0 ? valid.showLowConnectivityBar : z8, (i5 & 1048576) != 0 ? valid.stageRoles : stageRoles, (i5 & 2097152) != 0 ? valid.isUpdatingRequestToSpeak : z9, (i5 & 4194304) != 0 ? valid.isMovingToAudience : z10, (i5 & 8388608) != 0 ? valid.requestingToSpeakCount : i2, (i5 & 16777216) != 0 ? valid.stopOffscreenVideo : z11, (i5 & 33554432) != 0 ? valid.channelPermissions : l, (i5 & 67108864) != 0 ? valid.startableEvent : guildScheduledEvent, (i5 & 134217728) != 0 ? valid.activeEvent : guildScheduledEvent2, (i5 & 268435456) != 0 ? valid.textInVoiceMentionCount : i3, (i5 & 536870912) != 0 ? valid.textInVoiceUnreadCount : i4, (i5 & BasicMeasure.EXACTLY) != 0 ? valid.isTextInVoiceEnabled : z12, (i5 & Integer.MIN_VALUE) != 0 ? valid.isTextInVoiceChannelSelected : z13, (i6 & 1) != 0 ? valid.allVideoParticipants : map);
            }

            public final CallModel component1() {
                return this.callModel;
            }

            public final VoiceControlsOutputSelectorState component10() {
                return this.outputSelectorState;
            }

            public final String component11() {
                return this.focusedParticipantKey;
            }

            public final boolean component12() {
                return this.isAnyoneUsingVideo;
            }

            public final CallParticipant.UserOrStreamParticipant component13() {
                return this.pipParticipant;
            }

            public final Boolean component14() {
                return this.noiseCancellation;
            }

            public final List<StoreVoiceParticipants.VoiceUser> component15() {
                return this.privateCallUserListItems;
            }

            public final boolean component16() {
                return this.showParticipantsHiddenView;
            }

            public final boolean component17() {
                return this.startedAsVideo;
            }

            public final float component18() {
                return this.perceptualStreamVolume;
            }

            public final int component19() {
                return this.mentionCount;
            }

            public final StageCallModel component2() {
                return this.stageCallModel;
            }

            public final boolean component20() {
                return this.showLowConnectivityBar;
            }

            /* renamed from: component21-twRsX-0  reason: not valid java name */
            public final StageRoles m64component21twRsX0() {
                return this.stageRoles;
            }

            public final boolean component22() {
                return this.isUpdatingRequestToSpeak;
            }

            public final boolean component23() {
                return this.isMovingToAudience;
            }

            public final int component24() {
                return this.requestingToSpeakCount;
            }

            public final boolean component25() {
                return this.stopOffscreenVideo;
            }

            public final Long component26() {
                return this.channelPermissions;
            }

            public final GuildScheduledEvent component27() {
                return this.startableEvent;
            }

            public final GuildScheduledEvent component28() {
                return this.activeEvent;
            }

            public final int component29() {
                return this.textInVoiceMentionCount;
            }

            public final OverlayStatus component3() {
                return this.overlayStatus;
            }

            public final int component30() {
                return this.textInVoiceUnreadCount;
            }

            public final boolean component31() {
                return this.isTextInVoiceEnabled;
            }

            public final boolean component32() {
                return this.isTextInVoiceChannelSelected;
            }

            public final Map<String, CallParticipant.UserOrStreamParticipant> component33() {
                return this.allVideoParticipants;
            }

            public final List<CallParticipant> component4() {
                return this.visibleVideoParticipants;
            }

            public final boolean component5() {
                return this.isIdle;
            }

            public final boolean component6() {
                return this.isControlFadingDisabled;
            }

            public final boolean component7() {
                return this.isSwitchCameraButtonVisible;
            }

            public final CameraState component8() {
                return this.cameraState;
            }

            public final DisplayMode component9() {
                return this.displayMode;
            }

            /* renamed from: copy-6O6tUOw  reason: not valid java name */
            public final Valid m65copy6O6tUOw(CallModel callModel, StageCallModel stageCallModel, OverlayStatus overlayStatus, List<? extends CallParticipant> list, boolean z2, boolean z3, boolean z4, CameraState cameraState, DisplayMode displayMode, VoiceControlsOutputSelectorState voiceControlsOutputSelectorState, String str, boolean z5, CallParticipant.UserOrStreamParticipant userOrStreamParticipant, Boolean bool, List<StoreVoiceParticipants.VoiceUser> list2, boolean z6, boolean z7, float f, int i, boolean z8, StageRoles stageRoles, boolean z9, boolean z10, int i2, boolean z11, Long l, GuildScheduledEvent guildScheduledEvent, GuildScheduledEvent guildScheduledEvent2, int i3, int i4, boolean z12, boolean z13, Map<String, CallParticipant.UserOrStreamParticipant> map) {
                m.checkNotNullParameter(callModel, "callModel");
                m.checkNotNullParameter(overlayStatus, "overlayStatus");
                m.checkNotNullParameter(list, "visibleVideoParticipants");
                m.checkNotNullParameter(cameraState, "cameraState");
                m.checkNotNullParameter(displayMode, "displayMode");
                m.checkNotNullParameter(voiceControlsOutputSelectorState, "outputSelectorState");
                m.checkNotNullParameter(list2, "privateCallUserListItems");
                m.checkNotNullParameter(map, "allVideoParticipants");
                return new Valid(callModel, stageCallModel, overlayStatus, list, z2, z3, z4, cameraState, displayMode, voiceControlsOutputSelectorState, str, z5, userOrStreamParticipant, bool, list2, z6, z7, f, i, z8, stageRoles, z9, z10, i2, z11, l, guildScheduledEvent, guildScheduledEvent2, i3, i4, z12, z13, map);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return m.areEqual(this.callModel, valid.callModel) && m.areEqual(this.stageCallModel, valid.stageCallModel) && m.areEqual(this.overlayStatus, valid.overlayStatus) && m.areEqual(this.visibleVideoParticipants, valid.visibleVideoParticipants) && this.isIdle == valid.isIdle && this.isControlFadingDisabled == valid.isControlFadingDisabled && this.isSwitchCameraButtonVisible == valid.isSwitchCameraButtonVisible && m.areEqual(this.cameraState, valid.cameraState) && m.areEqual(this.displayMode, valid.displayMode) && m.areEqual(this.outputSelectorState, valid.outputSelectorState) && m.areEqual(this.focusedParticipantKey, valid.focusedParticipantKey) && this.isAnyoneUsingVideo == valid.isAnyoneUsingVideo && m.areEqual(this.pipParticipant, valid.pipParticipant) && m.areEqual(this.noiseCancellation, valid.noiseCancellation) && m.areEqual(this.privateCallUserListItems, valid.privateCallUserListItems) && this.showParticipantsHiddenView == valid.showParticipantsHiddenView && this.startedAsVideo == valid.startedAsVideo && Float.compare(this.perceptualStreamVolume, valid.perceptualStreamVolume) == 0 && this.mentionCount == valid.mentionCount && this.showLowConnectivityBar == valid.showLowConnectivityBar && m.areEqual(this.stageRoles, valid.stageRoles) && this.isUpdatingRequestToSpeak == valid.isUpdatingRequestToSpeak && this.isMovingToAudience == valid.isMovingToAudience && this.requestingToSpeakCount == valid.requestingToSpeakCount && this.stopOffscreenVideo == valid.stopOffscreenVideo && m.areEqual(this.channelPermissions, valid.channelPermissions) && m.areEqual(this.startableEvent, valid.startableEvent) && m.areEqual(this.activeEvent, valid.activeEvent) && this.textInVoiceMentionCount == valid.textInVoiceMentionCount && this.textInVoiceUnreadCount == valid.textInVoiceUnreadCount && this.isTextInVoiceEnabled == valid.isTextInVoiceEnabled && this.isTextInVoiceChannelSelected == valid.isTextInVoiceChannelSelected && m.areEqual(this.allVideoParticipants, valid.allVideoParticipants);
            }

            public final GuildScheduledEvent getActiveEvent() {
                return this.activeEvent;
            }

            public final Map<String, CallParticipant.UserOrStreamParticipant> getAllVideoParticipants() {
                return this.allVideoParticipants;
            }

            public final String getAnalyticsVideoLayout() {
                return (String) this.analyticsVideoLayout$delegate.getValue();
            }

            public final CallModel getCallModel() {
                return this.callModel;
            }

            public final CameraState getCameraState() {
                return this.cameraState;
            }

            public final Long getChannelPermissions() {
                return this.channelPermissions;
            }

            public final DisplayMode getDisplayMode() {
                return this.displayMode;
            }

            public final String getFocusedParticipantKey() {
                return this.focusedParticipantKey;
            }

            public final int getMentionCount() {
                return this.mentionCount;
            }

            public final Set<MenuItem> getMenuItems() {
                return (Set) this.menuItems$delegate.getValue();
            }

            public final Boolean getNoiseCancellation() {
                return this.noiseCancellation;
            }

            public final VoiceControlsOutputSelectorState getOutputSelectorState() {
                return this.outputSelectorState;
            }

            public final OverlayStatus getOverlayStatus() {
                return this.overlayStatus;
            }

            public final float getPerceptualStreamVolume() {
                return this.perceptualStreamVolume;
            }

            public final CallParticipant.UserOrStreamParticipant getPipParticipant() {
                return this.pipParticipant;
            }

            public final List<StoreVoiceParticipants.VoiceUser> getPrivateCallUserListItems() {
                return this.privateCallUserListItems;
            }

            public final int getRequestingToSpeakCount() {
                return this.requestingToSpeakCount;
            }

            public final boolean getShowControls() {
                return this.showControls;
            }

            public final boolean getShowLowConnectivityBar() {
                return this.showLowConnectivityBar;
            }

            public final boolean getShowParticipantsHiddenView() {
                return this.showParticipantsHiddenView;
            }

            public final boolean getShowStreamVolume() {
                return this.showStreamVolume;
            }

            public final StageCallModel getStageCallModel() {
                return this.stageCallModel;
            }

            /* renamed from: getStageRoles-twRsX-0  reason: not valid java name */
            public final StageRoles m66getStageRolestwRsX0() {
                return this.stageRoles;
            }

            public final GuildScheduledEvent getStartableEvent() {
                return this.startableEvent;
            }

            public final boolean getStartedAsVideo() {
                return this.startedAsVideo;
            }

            public final boolean getStopOffscreenVideo() {
                return this.stopOffscreenVideo;
            }

            public final int getTextInVoiceMentionCount() {
                return this.textInVoiceMentionCount;
            }

            public final int getTextInVoiceUnreadCount() {
                return this.textInVoiceUnreadCount;
            }

            public final String getTitleText() {
                return (String) this.titleText$delegate.getValue();
            }

            public final List<CallParticipant> getVisibleVideoParticipants() {
                return this.visibleVideoParticipants;
            }

            public int hashCode() {
                CallModel callModel = this.callModel;
                int i = 0;
                int hashCode = (callModel != null ? callModel.hashCode() : 0) * 31;
                StageCallModel stageCallModel = this.stageCallModel;
                int hashCode2 = (hashCode + (stageCallModel != null ? stageCallModel.hashCode() : 0)) * 31;
                OverlayStatus overlayStatus = this.overlayStatus;
                int hashCode3 = (hashCode2 + (overlayStatus != null ? overlayStatus.hashCode() : 0)) * 31;
                List<CallParticipant> list = this.visibleVideoParticipants;
                int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
                boolean z2 = this.isIdle;
                int i2 = 1;
                if (z2) {
                    z2 = true;
                }
                int i3 = z2 ? 1 : 0;
                int i4 = z2 ? 1 : 0;
                int i5 = (hashCode4 + i3) * 31;
                boolean z3 = this.isControlFadingDisabled;
                if (z3) {
                    z3 = true;
                }
                int i6 = z3 ? 1 : 0;
                int i7 = z3 ? 1 : 0;
                int i8 = (i5 + i6) * 31;
                boolean z4 = this.isSwitchCameraButtonVisible;
                if (z4) {
                    z4 = true;
                }
                int i9 = z4 ? 1 : 0;
                int i10 = z4 ? 1 : 0;
                int i11 = (i8 + i9) * 31;
                CameraState cameraState = this.cameraState;
                int hashCode5 = (i11 + (cameraState != null ? cameraState.hashCode() : 0)) * 31;
                DisplayMode displayMode = this.displayMode;
                int hashCode6 = (hashCode5 + (displayMode != null ? displayMode.hashCode() : 0)) * 31;
                VoiceControlsOutputSelectorState voiceControlsOutputSelectorState = this.outputSelectorState;
                int hashCode7 = (hashCode6 + (voiceControlsOutputSelectorState != null ? voiceControlsOutputSelectorState.hashCode() : 0)) * 31;
                String str = this.focusedParticipantKey;
                int hashCode8 = (hashCode7 + (str != null ? str.hashCode() : 0)) * 31;
                boolean z5 = this.isAnyoneUsingVideo;
                if (z5) {
                    z5 = true;
                }
                int i12 = z5 ? 1 : 0;
                int i13 = z5 ? 1 : 0;
                int i14 = (hashCode8 + i12) * 31;
                CallParticipant.UserOrStreamParticipant userOrStreamParticipant = this.pipParticipant;
                int hashCode9 = (i14 + (userOrStreamParticipant != null ? userOrStreamParticipant.hashCode() : 0)) * 31;
                Boolean bool = this.noiseCancellation;
                int hashCode10 = (hashCode9 + (bool != null ? bool.hashCode() : 0)) * 31;
                List<StoreVoiceParticipants.VoiceUser> list2 = this.privateCallUserListItems;
                int hashCode11 = (hashCode10 + (list2 != null ? list2.hashCode() : 0)) * 31;
                boolean z6 = this.showParticipantsHiddenView;
                if (z6) {
                    z6 = true;
                }
                int i15 = z6 ? 1 : 0;
                int i16 = z6 ? 1 : 0;
                int i17 = (hashCode11 + i15) * 31;
                boolean z7 = this.startedAsVideo;
                if (z7) {
                    z7 = true;
                }
                int i18 = z7 ? 1 : 0;
                int i19 = z7 ? 1 : 0;
                int floatToIntBits = (((Float.floatToIntBits(this.perceptualStreamVolume) + ((i17 + i18) * 31)) * 31) + this.mentionCount) * 31;
                boolean z8 = this.showLowConnectivityBar;
                if (z8) {
                    z8 = true;
                }
                int i20 = z8 ? 1 : 0;
                int i21 = z8 ? 1 : 0;
                int i22 = (floatToIntBits + i20) * 31;
                StageRoles stageRoles = this.stageRoles;
                int hashCode12 = (i22 + (stageRoles != null ? stageRoles.hashCode() : 0)) * 31;
                boolean z9 = this.isUpdatingRequestToSpeak;
                if (z9) {
                    z9 = true;
                }
                int i23 = z9 ? 1 : 0;
                int i24 = z9 ? 1 : 0;
                int i25 = (hashCode12 + i23) * 31;
                boolean z10 = this.isMovingToAudience;
                if (z10) {
                    z10 = true;
                }
                int i26 = z10 ? 1 : 0;
                int i27 = z10 ? 1 : 0;
                int i28 = (((i25 + i26) * 31) + this.requestingToSpeakCount) * 31;
                boolean z11 = this.stopOffscreenVideo;
                if (z11) {
                    z11 = true;
                }
                int i29 = z11 ? 1 : 0;
                int i30 = z11 ? 1 : 0;
                int i31 = (i28 + i29) * 31;
                Long l = this.channelPermissions;
                int hashCode13 = (i31 + (l != null ? l.hashCode() : 0)) * 31;
                GuildScheduledEvent guildScheduledEvent = this.startableEvent;
                int hashCode14 = (hashCode13 + (guildScheduledEvent != null ? guildScheduledEvent.hashCode() : 0)) * 31;
                GuildScheduledEvent guildScheduledEvent2 = this.activeEvent;
                int hashCode15 = (((((hashCode14 + (guildScheduledEvent2 != null ? guildScheduledEvent2.hashCode() : 0)) * 31) + this.textInVoiceMentionCount) * 31) + this.textInVoiceUnreadCount) * 31;
                boolean z12 = this.isTextInVoiceEnabled;
                if (z12) {
                    z12 = true;
                }
                int i32 = z12 ? 1 : 0;
                int i33 = z12 ? 1 : 0;
                int i34 = (hashCode15 + i32) * 31;
                boolean z13 = this.isTextInVoiceChannelSelected;
                if (!z13) {
                    i2 = z13 ? 1 : 0;
                }
                int i35 = (i34 + i2) * 31;
                Map<String, CallParticipant.UserOrStreamParticipant> map = this.allVideoParticipants;
                if (map != null) {
                    i = map.hashCode();
                }
                return i35 + i;
            }

            public final boolean isAnyoneUsingVideo() {
                return this.isAnyoneUsingVideo;
            }

            public final boolean isControlFadingDisabled() {
                return this.isControlFadingDisabled;
            }

            public final boolean isDeafened() {
                return this.isDeafened;
            }

            public final boolean isIdle() {
                return this.isIdle;
            }

            public final boolean isMovingToAudience() {
                return this.isMovingToAudience;
            }

            public final boolean isPushToTalk() {
                return this.isPushToTalk;
            }

            public final boolean isStreamFocused() {
                return this.isStreamFocused;
            }

            public final boolean isSwitchCameraButtonVisible() {
                return this.isSwitchCameraButtonVisible;
            }

            public final boolean isTextInVoiceChannelSelected() {
                return this.isTextInVoiceChannelSelected;
            }

            public final boolean isTextInVoiceEnabled() {
                return this.isTextInVoiceEnabled;
            }

            public final boolean isUpdatingRequestToSpeak() {
                return this.isUpdatingRequestToSpeak;
            }

            public final boolean isVideoCallGridVisible() {
                return this.isVideoCallGridVisible;
            }

            public String toString() {
                StringBuilder R = a.R("Valid(callModel=");
                R.append(this.callModel);
                R.append(", stageCallModel=");
                R.append(this.stageCallModel);
                R.append(", overlayStatus=");
                R.append(this.overlayStatus);
                R.append(", visibleVideoParticipants=");
                R.append(this.visibleVideoParticipants);
                R.append(", isIdle=");
                R.append(this.isIdle);
                R.append(", isControlFadingDisabled=");
                R.append(this.isControlFadingDisabled);
                R.append(", isSwitchCameraButtonVisible=");
                R.append(this.isSwitchCameraButtonVisible);
                R.append(", cameraState=");
                R.append(this.cameraState);
                R.append(", displayMode=");
                R.append(this.displayMode);
                R.append(", outputSelectorState=");
                R.append(this.outputSelectorState);
                R.append(", focusedParticipantKey=");
                R.append(this.focusedParticipantKey);
                R.append(", isAnyoneUsingVideo=");
                R.append(this.isAnyoneUsingVideo);
                R.append(", pipParticipant=");
                R.append(this.pipParticipant);
                R.append(", noiseCancellation=");
                R.append(this.noiseCancellation);
                R.append(", privateCallUserListItems=");
                R.append(this.privateCallUserListItems);
                R.append(", showParticipantsHiddenView=");
                R.append(this.showParticipantsHiddenView);
                R.append(", startedAsVideo=");
                R.append(this.startedAsVideo);
                R.append(", perceptualStreamVolume=");
                R.append(this.perceptualStreamVolume);
                R.append(", mentionCount=");
                R.append(this.mentionCount);
                R.append(", showLowConnectivityBar=");
                R.append(this.showLowConnectivityBar);
                R.append(", stageRoles=");
                R.append(this.stageRoles);
                R.append(", isUpdatingRequestToSpeak=");
                R.append(this.isUpdatingRequestToSpeak);
                R.append(", isMovingToAudience=");
                R.append(this.isMovingToAudience);
                R.append(", requestingToSpeakCount=");
                R.append(this.requestingToSpeakCount);
                R.append(", stopOffscreenVideo=");
                R.append(this.stopOffscreenVideo);
                R.append(", channelPermissions=");
                R.append(this.channelPermissions);
                R.append(", startableEvent=");
                R.append(this.startableEvent);
                R.append(", activeEvent=");
                R.append(this.activeEvent);
                R.append(", textInVoiceMentionCount=");
                R.append(this.textInVoiceMentionCount);
                R.append(", textInVoiceUnreadCount=");
                R.append(this.textInVoiceUnreadCount);
                R.append(", isTextInVoiceEnabled=");
                R.append(this.isTextInVoiceEnabled);
                R.append(", isTextInVoiceChannelSelected=");
                R.append(this.isTextInVoiceChannelSelected);
                R.append(", allVideoParticipants=");
                return a.L(R, this.allVideoParticipants, ")");
            }

            /* JADX WARN: Multi-variable type inference failed */
            private Valid(CallModel callModel, StageCallModel stageCallModel, OverlayStatus overlayStatus, List<? extends CallParticipant> list, boolean z2, boolean z3, boolean z4, CameraState cameraState, DisplayMode displayMode, VoiceControlsOutputSelectorState voiceControlsOutputSelectorState, String str, boolean z5, CallParticipant.UserOrStreamParticipant userOrStreamParticipant, Boolean bool, List<StoreVoiceParticipants.VoiceUser> list2, boolean z6, boolean z7, float f, int i, boolean z8, StageRoles stageRoles, boolean z9, boolean z10, int i2, boolean z11, Long l, GuildScheduledEvent guildScheduledEvent, GuildScheduledEvent guildScheduledEvent2, int i3, int i4, boolean z12, boolean z13, Map<String, CallParticipant.UserOrStreamParticipant> map) {
                super(null);
                this.callModel = callModel;
                this.stageCallModel = stageCallModel;
                this.overlayStatus = overlayStatus;
                this.visibleVideoParticipants = list;
                this.isIdle = z2;
                this.isControlFadingDisabled = z3;
                this.isSwitchCameraButtonVisible = z4;
                this.cameraState = cameraState;
                this.displayMode = displayMode;
                this.outputSelectorState = voiceControlsOutputSelectorState;
                this.focusedParticipantKey = str;
                this.isAnyoneUsingVideo = z5;
                this.pipParticipant = userOrStreamParticipant;
                this.noiseCancellation = bool;
                this.privateCallUserListItems = list2;
                this.showParticipantsHiddenView = z6;
                this.startedAsVideo = z7;
                this.perceptualStreamVolume = f;
                this.mentionCount = i;
                this.showLowConnectivityBar = z8;
                this.stageRoles = stageRoles;
                this.isUpdatingRequestToSpeak = z9;
                this.isMovingToAudience = z10;
                this.requestingToSpeakCount = i2;
                this.stopOffscreenVideo = z11;
                this.channelPermissions = l;
                this.startableEvent = guildScheduledEvent;
                this.activeEvent = guildScheduledEvent2;
                this.textInVoiceMentionCount = i3;
                this.textInVoiceUnreadCount = i4;
                this.isTextInVoiceEnabled = z12;
                this.isTextInVoiceChannelSelected = z13;
                this.allVideoParticipants = map;
                this.menuItems$delegate = g.lazy(new WidgetCallFullscreenViewModel$ViewState$Valid$menuItems$2(this));
                this.titleText$delegate = g.lazy(new WidgetCallFullscreenViewModel$ViewState$Valid$titleText$2(this));
                boolean z14 = false;
                this.isStreamFocused = str != null && t.endsWith$default(str, "STREAM", false, 2, null);
                boolean z15 = (list.isEmpty() ^ true) && !callModel.isStreaming() && displayMode == DisplayMode.GRID;
                this.isVideoCallGridVisible = z15;
                this.isPushToTalk = callModel.getInputMode() == MediaEngineConnection.InputMode.PUSH_TO_TALK;
                this.showControls = !z15 || !z2;
                if (callModel.getActiveStream() != null && !callModel.isStreaming()) {
                    z14 = true;
                }
                this.showStreamVolume = z14;
                this.analyticsVideoLayout$delegate = g.lazy(new WidgetCallFullscreenViewModel$ViewState$Valid$analyticsVideoLayout$2(this));
                this.isDeafened = callModel.isDeafenedByAnySource();
            }
        }

        private ViewState() {
        }

        public /* synthetic */ ViewState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;
        public static final /* synthetic */ int[] $EnumSwitchMapping$2;
        public static final /* synthetic */ int[] $EnumSwitchMapping$3;
        public static final /* synthetic */ int[] $EnumSwitchMapping$4;
        public static final /* synthetic */ int[] $EnumSwitchMapping$5;
        public static final /* synthetic */ int[] $EnumSwitchMapping$6;

        static {
            DisplayMode.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            iArr[DisplayMode.GRID.ordinal()] = 1;
            iArr[DisplayMode.PRIVATE_CALL_PARTICIPANTS.ordinal()] = 2;
            VideoCallParticipantView.ParticipantData.Type.values();
            int[] iArr2 = new int[2];
            $EnumSwitchMapping$1 = iArr2;
            VideoCallParticipantView.ParticipantData.Type type = VideoCallParticipantView.ParticipantData.Type.DEFAULT;
            iArr2[type.ordinal()] = 1;
            VideoCallParticipantView.ParticipantData.Type type2 = VideoCallParticipantView.ParticipantData.Type.APPLICATION_STREAMING;
            iArr2[type2.ordinal()] = 2;
            DiscordAudioManager.DeviceTypes.values();
            int[] iArr3 = new int[6];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[DiscordAudioManager.DeviceTypes.BLUETOOTH_HEADSET.ordinal()] = 1;
            iArr3[DiscordAudioManager.DeviceTypes.SPEAKERPHONE.ordinal()] = 2;
            VideoCallParticipantView.ParticipantData.Type.values();
            int[] iArr4 = new int[2];
            $EnumSwitchMapping$3 = iArr4;
            iArr4[type2.ordinal()] = 1;
            iArr4[type.ordinal()] = 2;
            VideoCallParticipantView.ParticipantData.Type.values();
            int[] iArr5 = new int[2];
            $EnumSwitchMapping$4 = iArr5;
            iArr5[type2.ordinal()] = 1;
            iArr5[type.ordinal()] = 2;
            StoreApplicationStreaming.ActiveApplicationStream.State.values();
            int[] iArr6 = new int[6];
            $EnumSwitchMapping$5 = iArr6;
            iArr6[StoreApplicationStreaming.ActiveApplicationStream.State.CONNECTING.ordinal()] = 1;
            iArr6[StoreApplicationStreaming.ActiveApplicationStream.State.RECONNECTING.ordinal()] = 2;
            iArr6[StoreApplicationStreaming.ActiveApplicationStream.State.ACTIVE.ordinal()] = 3;
            iArr6[StoreApplicationStreaming.ActiveApplicationStream.State.PAUSED.ordinal()] = 4;
            iArr6[StoreApplicationStreaming.ActiveApplicationStream.State.DENIED_FULL.ordinal()] = 5;
            iArr6[StoreApplicationStreaming.ActiveApplicationStream.State.ENDED.ordinal()] = 6;
            StoreMediaSettings.SelfMuteFailure.values();
            int[] iArr7 = new int[1];
            $EnumSwitchMapping$6 = iArr7;
            iArr7[StoreMediaSettings.SelfMuteFailure.CANNOT_USE_VAD.ordinal()] = 1;
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetCallFullscreenViewModel(long r32, com.discord.stores.StoreChannels r34, com.discord.stores.StoreNavigation r35, com.discord.stores.StoreVoiceChannelSelected r36, com.discord.stores.StoreGuildSelected r37, com.discord.stores.StoreTabsNavigation r38, com.discord.stores.StoreUserSettings r39, com.discord.stores.StoreApplicationStreaming r40, com.discord.stores.StoreMediaEngine r41, com.discord.stores.StoreMediaSettings r42, com.discord.stores.StorePermissions r43, com.discord.utilities.time.Clock r44, rx.Scheduler r45, com.discord.utilities.permissions.VideoPermissionsManager r46, com.discord.utilities.voice.VoiceEngineServiceController r47, com.discord.stores.StoreStreamRtcConnection r48, com.discord.stores.StoreAudioManagerV2 r49, com.discord.stores.StoreMentions r50, com.discord.stores.StoreAnalytics r51, com.discord.stores.StoreConnectivity r52, com.discord.stores.StoreStageChannels r53, com.discord.stores.StoreAnalytics r54, com.discord.stores.StoreExperiments r55, com.discord.stores.StoreChannelsSelected r56, com.discord.stores.StoreApplication r57, com.discord.stores.StoreApplicationAssets r58, java.lang.String r59, int r60, kotlin.jvm.internal.DefaultConstructorMarker r61) {
        /*
            Method dump skipped, instructions count: 390
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel.<init>(long, com.discord.stores.StoreChannels, com.discord.stores.StoreNavigation, com.discord.stores.StoreVoiceChannelSelected, com.discord.stores.StoreGuildSelected, com.discord.stores.StoreTabsNavigation, com.discord.stores.StoreUserSettings, com.discord.stores.StoreApplicationStreaming, com.discord.stores.StoreMediaEngine, com.discord.stores.StoreMediaSettings, com.discord.stores.StorePermissions, com.discord.utilities.time.Clock, rx.Scheduler, com.discord.utilities.permissions.VideoPermissionsManager, com.discord.utilities.voice.VoiceEngineServiceController, com.discord.stores.StoreStreamRtcConnection, com.discord.stores.StoreAudioManagerV2, com.discord.stores.StoreMentions, com.discord.stores.StoreAnalytics, com.discord.stores.StoreConnectivity, com.discord.stores.StoreStageChannels, com.discord.stores.StoreAnalytics, com.discord.stores.StoreExperiments, com.discord.stores.StoreChannelsSelected, com.discord.stores.StoreApplication, com.discord.stores.StoreApplicationAssets, java.lang.String, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    private final CallParticipant addStreamQualityMetadata(CallParticipant callParticipant, RtcConnection.Quality quality) {
        boolean z2 = callParticipant instanceof CallParticipant.UserOrStreamParticipant;
        if (z2 || (callParticipant instanceof CallParticipant.EmbeddedActivityParticipant)) {
            return callParticipant;
        }
        if (z2) {
            CallParticipant.UserOrStreamParticipant userOrStreamParticipant = (CallParticipant.UserOrStreamParticipant) callParticipant;
            VideoCallParticipantView.ParticipantData participantData = userOrStreamParticipant.getParticipantData();
            StoreVideoStreams.UserStreams streams = participantData.f2812b.getStreams();
            VideoCallParticipantView.StreamFps streamFps = null;
            VideoMetadata applicationStreamMetadata = streams != null ? streams.getApplicationStreamMetadata() : null;
            if (applicationStreamMetadata != null) {
                m.checkNotNullParameter(applicationStreamMetadata, "metadata");
                VideoCallParticipantView.StreamResolution aVar = applicationStreamMetadata.e == Payloads.ResolutionType.Source ? VideoCallParticipantView.StreamResolution.b.a : new VideoCallParticipantView.StreamResolution.a(applicationStreamMetadata.c);
                Integer num = applicationStreamMetadata.d;
                if (num != null) {
                    streamFps = new VideoCallParticipantView.StreamFps(num.intValue());
                }
                participantData = VideoCallParticipantView.ParticipantData.a(participantData, null, false, null, null, null, null, false, false, new VideoCallParticipantView.ParticipantData.a(aVar.b() || (streamFps != null && streamFps.a()), aVar, streamFps, quality == RtcConnection.Quality.BAD), 255);
            }
            return userOrStreamParticipant.copy(participantData);
        }
        throw new NoWhenBranchMatchedException();
    }

    @MainThread
    private final void cancelTapForwardingJob() {
        Subscription subscription = this.forwardVideoGridInteractionSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.forwardVideoGridInteractionSubscription = null;
    }

    @MainThread
    private final void clearFocusedVideoParticipant() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            this.focusedVideoParticipantKey = null;
            stopWatchingStreamIfEnded();
            updateViewState((ViewState) ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, computeVisibleVideoParticipants(), false, false, false, null, null, null, this.focusedVideoParticipantKey, false, computePipParticipant(valid.getCallModel().getMyId(), valid.getCallModel().getSelectedVideoDevice(), valid.getCallModel().isStreaming(), valid.getCameraState()), null, null, false, false, 0.0f, 0, false, null, false, false, 0, false, null, null, null, 0, 0, false, false, null, -5129, 1, null));
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:50:0x004c A[EDGE_INSN: B:50:0x004c->B:20:0x004c ?: BREAK  , SYNTHETIC] */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final com.discord.widgets.voice.fullscreen.CallParticipant.UserOrStreamParticipant computePipParticipant(long r18, co.discord.media_engine.VideoInputDeviceDescription r20, boolean r21, com.discord.widgets.voice.model.CameraState r22) {
        /*
            r17 = this;
            r0 = r17
            java.util.List<? extends com.discord.widgets.voice.fullscreen.CallParticipant> r1 = r0.allVideoParticipants
            boolean r1 = r1.isEmpty()
            r2 = 0
            if (r1 == 0) goto Lc
            return r2
        Lc:
            java.util.List<? extends com.discord.widgets.voice.fullscreen.CallParticipant> r1 = r0.allVideoParticipants
            java.util.Iterator r1 = r1.iterator()
        L12:
            boolean r3 = r1.hasNext()
            r4 = 0
            r5 = 1
            if (r3 == 0) goto L4b
            java.lang.Object r3 = r1.next()
            r6 = r3
            com.discord.widgets.voice.fullscreen.CallParticipant r6 = (com.discord.widgets.voice.fullscreen.CallParticipant) r6
            boolean r7 = r6 instanceof com.discord.widgets.voice.fullscreen.CallParticipant.UserOrStreamParticipant
            if (r7 == 0) goto L47
            com.discord.widgets.voice.fullscreen.CallParticipant$UserOrStreamParticipant r6 = (com.discord.widgets.voice.fullscreen.CallParticipant.UserOrStreamParticipant) r6
            com.discord.views.calls.VideoCallParticipantView$ParticipantData r7 = r6.getParticipantData()
            com.discord.stores.StoreVoiceParticipants$VoiceUser r7 = r7.f2812b
            com.discord.api.voice.state.VoiceState r7 = r7.getVoiceState()
            if (r7 == 0) goto L47
            boolean r7 = r7.j()
            if (r7 != r5) goto L47
            com.discord.views.calls.VideoCallParticipantView$ParticipantData r6 = r6.getParticipantData()
            com.discord.stores.StoreVoiceParticipants$VoiceUser r6 = r6.f2812b
            boolean r6 = r6.isMe()
            if (r6 == 0) goto L47
            r6 = 1
            goto L48
        L47:
            r6 = 0
        L48:
            if (r6 == 0) goto L12
            goto L4c
        L4b:
            r3 = r2
        L4c:
            com.discord.widgets.voice.fullscreen.CallParticipant r3 = (com.discord.widgets.voice.fullscreen.CallParticipant) r3
            if (r21 == 0) goto L58
            com.discord.widgets.voice.model.CameraState r1 = com.discord.widgets.voice.model.CameraState.CAMERA_ON
            r6 = r22
            if (r6 != r1) goto L58
            r1 = 1
            goto L59
        L58:
            r1 = 0
        L59:
            java.lang.String r6 = r0.focusedVideoParticipantKey
            java.lang.String r7 = java.lang.String.valueOf(r18)
            boolean r7 = d0.z.d.m.areEqual(r6, r7)
            if (r6 == 0) goto L69
            if (r7 != 0) goto L69
            r6 = 1
            goto L6a
        L69:
            r6 = 0
        L6a:
            java.util.List<? extends com.discord.widgets.voice.fullscreen.CallParticipant> r7 = r0.allVideoParticipants
            boolean r7 = r0.isOneOnOneMeCall(r7)
            if (r7 != 0) goto L79
            if (r6 != 0) goto L79
            if (r1 == 0) goto L77
            goto L79
        L77:
            r1 = 0
            goto L7a
        L79:
            r1 = 1
        L7a:
            if (r3 == 0) goto La8
            boolean r6 = r3 instanceof com.discord.widgets.voice.fullscreen.CallParticipant.UserOrStreamParticipant
            if (r6 == 0) goto La8
            if (r1 == 0) goto La8
            if (r20 == 0) goto L88
            co.discord.media_engine.VideoInputDeviceFacing r2 = r20.getFacing()
        L88:
            co.discord.media_engine.VideoInputDeviceFacing r1 = co.discord.media_engine.VideoInputDeviceFacing.Front
            if (r2 != r1) goto L8e
            r8 = 1
            goto L8f
        L8e:
            r8 = 0
        L8f:
            com.discord.widgets.voice.fullscreen.CallParticipant$UserOrStreamParticipant r3 = (com.discord.widgets.voice.fullscreen.CallParticipant.UserOrStreamParticipant) r3
            com.discord.views.calls.VideoCallParticipantView$ParticipantData r6 = r3.getParticipantData()
            r7 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 445(0x1bd, float:6.24E-43)
            com.discord.views.calls.VideoCallParticipantView$ParticipantData r1 = com.discord.views.calls.VideoCallParticipantView.ParticipantData.a(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16)
            com.discord.widgets.voice.fullscreen.CallParticipant$UserOrStreamParticipant r1 = r3.copy(r1)
            return r1
        La8:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel.computePipParticipant(long, co.discord.media_engine.VideoInputDeviceDescription, boolean, com.discord.widgets.voice.model.CameraState):com.discord.widgets.voice.fullscreen.CallParticipant$UserOrStreamParticipant");
    }

    @MainThread
    private final List<CallParticipant> computeVisibleVideoParticipants() {
        boolean z2;
        Object obj;
        CallParticipant.UserOrStreamParticipant userOrStreamParticipant;
        String str = this.focusedVideoParticipantKey;
        Object obj2 = null;
        if (str != null) {
            Iterator<T> it = this.allVideoParticipants.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (m.areEqual(getParticipantFocusKey((CallParticipant) obj), str)) {
                    break;
                }
            }
            CallParticipant callParticipant = (CallParticipant) obj;
            if (callParticipant != null) {
                if (callParticipant instanceof CallParticipant.UserOrStreamParticipant) {
                    CallParticipant.UserOrStreamParticipant userOrStreamParticipant2 = (CallParticipant.UserOrStreamParticipant) callParticipant;
                    VideoCallParticipantView.ParticipantData participantData = userOrStreamParticipant2.getParticipantData();
                    RendererCommon.ScalingType scalingType = participantData.d;
                    RendererCommon.ScalingType scalingType2 = participantData.e;
                    RendererCommon.ScalingType scalingType3 = RendererCommon.ScalingType.SCALE_ASPECT_FIT;
                    userOrStreamParticipant = userOrStreamParticipant2;
                    if (!(scalingType == scalingType3 && scalingType2 == scalingType3)) {
                        userOrStreamParticipant = userOrStreamParticipant2.copy(VideoCallParticipantView.ParticipantData.a(userOrStreamParticipant2.getParticipantData(), null, false, scalingType3, scalingType3, null, null, false, false, null, 499));
                    }
                } else {
                    boolean z3 = callParticipant instanceof CallParticipant.EmbeddedActivityParticipant;
                    userOrStreamParticipant = callParticipant;
                    if (!z3) {
                        throw new NoWhenBranchMatchedException();
                    }
                }
                return d0.t.m.listOf(userOrStreamParticipant);
            }
            this.focusedVideoParticipantKey = null;
            stopWatchingStreamIfEnded();
        }
        if (isOneOnOneMeCall(this.allVideoParticipants)) {
            Iterator<T> it2 = this.allVideoParticipants.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                CallParticipant callParticipant2 = (CallParticipant) next;
                if ((callParticipant2 instanceof CallParticipant.EmbeddedActivityParticipant) || ((callParticipant2 instanceof CallParticipant.UserOrStreamParticipant) && !((CallParticipant.UserOrStreamParticipant) callParticipant2).getParticipantData().f2812b.isMe())) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    obj2 = next;
                    break;
                }
            }
            CallParticipant callParticipant3 = (CallParticipant) obj2;
            if (callParticipant3 != null) {
                return d0.t.m.listOf(callParticipant3);
            }
        }
        return this.allVideoParticipants;
    }

    private final List<StoreVoiceParticipants.VoiceUser> createPrivateCallParticipantListItems(CallModel callModel) {
        return u.toList(callModel.getParticipants().values());
    }

    private final Comparator<StoreVoiceParticipants.VoiceUser> createUserItemsComparator(final boolean z2, final String str) {
        return new Comparator<StoreVoiceParticipants.VoiceUser>() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$createUserItemsComparator$1
            public final int compare(StoreVoiceParticipants.VoiceUser voiceUser, StoreVoiceParticipants.VoiceUser voiceUser2) {
                boolean z3 = false;
                boolean z4 = str != null;
                ModelApplicationStream applicationStream = voiceUser.getApplicationStream();
                String str2 = null;
                String encodedStreamKey = applicationStream != null ? applicationStream.getEncodedStreamKey() : null;
                ModelApplicationStream applicationStream2 = voiceUser2.getApplicationStream();
                if (applicationStream2 != null) {
                    str2 = applicationStream2.getEncodedStreamKey();
                }
                boolean z5 = encodedStreamKey != null;
                boolean z6 = str2 != null;
                VoiceState voiceState = voiceUser.getVoiceState();
                boolean z7 = voiceState != null && voiceState.j();
                VoiceState voiceState2 = voiceUser2.getVoiceState();
                boolean z8 = voiceState2 != null && voiceState2.j();
                boolean z9 = z4 && m.areEqual(encodedStreamKey, str);
                if (z4 && m.areEqual(str2, str)) {
                    z3 = true;
                }
                boolean z10 = z2;
                if (!z10 || !z9) {
                    if (z10 && z3) {
                        return 1;
                    }
                    if (!z10 || !z4 || !m.areEqual(voiceUser.getWatchingStream(), str) || !(!m.areEqual(voiceUser2.getWatchingStream(), str))) {
                        if (z2 && z4 && m.areEqual(voiceUser2.getWatchingStream(), str) && (!m.areEqual(voiceUser.getWatchingStream(), str))) {
                            return 1;
                        }
                        if (!z5 || z6) {
                            if (!z5 && z6) {
                                return 1;
                            }
                            if (!z7 || z8) {
                                if (z7 || !z8) {
                                    return UserUtils.INSTANCE.compareUserNames(voiceUser.getUser(), voiceUser2.getUser(), voiceUser.getNickname(), voiceUser2.getNickname());
                                }
                                return 1;
                            }
                        }
                    }
                }
                return -1;
            }
        };
    }

    public static /* synthetic */ Comparator createUserItemsComparator$default(WidgetCallFullscreenViewModel widgetCallFullscreenViewModel, boolean z2, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = false;
        }
        if ((i & 2) != 0) {
            str = null;
        }
        return widgetCallFullscreenViewModel.createUserItemsComparator(z2, str);
    }

    private final List<CallParticipant> createVideoGridEntriesForParticipant(StoreVoiceParticipants.VoiceUser voiceUser, long j, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, RtcConnection.Quality quality, VideoInputDeviceDescription videoInputDeviceDescription, boolean z2) {
        VoiceState voiceState;
        VideoCallParticipantView.ParticipantData.ApplicationStreamState applicationStreamState;
        ModelApplicationStream stream;
        ArrayList<CallParticipant> arrayList = new ArrayList();
        if (!(voiceUser.isConnected() || voiceUser.isRinging())) {
            return n.emptyList();
        }
        String str = null;
        boolean z3 = false;
        arrayList.add(new CallParticipant.UserOrStreamParticipant(new VideoCallParticipantView.ParticipantData(voiceUser, voiceUser.isMe() && ((videoInputDeviceDescription != null ? videoInputDeviceDescription.getFacing() : null) == VideoInputDeviceFacing.Front), null, null, null, null, z2, false, null, 444)));
        if (voiceUser.getApplicationStream() == null || (voiceState = voiceUser.getVoiceState()) == null || !voiceState.i()) {
            if (activeApplicationStream != null) {
                ModelApplicationStream stream2 = activeApplicationStream.getStream();
                if (voiceUser.getUser().getId() == stream2.getOwnerId()) {
                    z3 = true;
                }
                if (z3) {
                    VideoCallParticipantView.ParticipantData.ApplicationStreamState participantStreamState = getParticipantStreamState(activeApplicationStream.getState());
                    RendererCommon.ScalingType scalingType = RendererCommon.ScalingType.SCALE_ASPECT_FIT;
                    VideoCallParticipantView.ParticipantData participantData = new VideoCallParticipantView.ParticipantData(voiceUser, false, scalingType, scalingType, participantStreamState, VideoCallParticipantView.ParticipantData.Type.APPLICATION_STREAMING, z2, false, null, 384);
                    if (isStreamFocused(stream2)) {
                        arrayList.add(new CallParticipant.UserOrStreamParticipant(participantData));
                    } else {
                        stopWatchingStreamIfEnded();
                    }
                }
            }
        } else if (voiceUser.getApplicationStream().getChannelId() != j) {
            return arrayList;
        } else {
            if (!(activeApplicationStream == null || (stream = activeApplicationStream.getStream()) == null)) {
                str = stream.getEncodedStreamKey();
            }
            if (m.areEqual(str, voiceUser.getApplicationStream().getEncodedStreamKey())) {
                applicationStreamState = getParticipantStreamState(activeApplicationStream.getState());
            } else {
                applicationStreamState = VideoCallParticipantView.ParticipantData.ApplicationStreamState.INACTIVE;
            }
            RendererCommon.ScalingType scalingType2 = RendererCommon.ScalingType.SCALE_ASPECT_FIT;
            arrayList.add(new CallParticipant.UserOrStreamParticipant(new VideoCallParticipantView.ParticipantData(voiceUser, false, scalingType2, scalingType2, applicationStreamState, VideoCallParticipantView.ParticipantData.Type.APPLICATION_STREAMING, z2, false, null, 384)));
        }
        ArrayList arrayList2 = new ArrayList(d0.t.o.collectionSizeOrDefault(arrayList, 10));
        for (CallParticipant callParticipant : arrayList) {
            arrayList2.add(addStreamQualityMetadata(callParticipant, quality));
        }
        return arrayList2;
    }

    private final void emitServerDeafenedDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowServerDeafenedDialog.INSTANCE);
    }

    private final void emitServerMutedDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowServerMutedDialog.INSTANCE);
    }

    private final void emitShowNoScreenSharePermissionDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowNoScreenSharePermissionDialog.INSTANCE);
    }

    private final void emitShowNoVadPermissionDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowNoVadPermissionDialog.INSTANCE);
    }

    private final void emitShowNoVideoPermissionDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowNoVideoPermissionDialog.INSTANCE);
    }

    private final void emitSuppressedDialogEvent() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(Event.ShowSuppressedDialog.INSTANCE);
    }

    private final void enqueueStreamFeedbackSheet(String str, String str2, int i) {
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.EnqueueStreamFeedbackSheet(str, str2, i));
    }

    private final String getParticipantFocusKey(CallParticipant callParticipant) {
        if (callParticipant instanceof CallParticipant.UserOrStreamParticipant) {
            CallParticipant.UserOrStreamParticipant userOrStreamParticipant = (CallParticipant.UserOrStreamParticipant) callParticipant;
            long id2 = userOrStreamParticipant.getParticipantData().f2812b.getUser().getId();
            int ordinal = userOrStreamParticipant.getParticipantData().g.ordinal();
            if (ordinal == 0) {
                return String.valueOf(id2);
            }
            if (ordinal == 1) {
                return id2 + "--STREAM";
            }
            throw new NoWhenBranchMatchedException();
        } else if (callParticipant instanceof CallParticipant.EmbeddedActivityParticipant) {
            return String.valueOf(((CallParticipant.EmbeddedActivityParticipant) callParticipant).getApplication().getId());
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    private final VideoCallParticipantView.ParticipantData.ApplicationStreamState getParticipantStreamState(StoreApplicationStreaming.ActiveApplicationStream.State state) {
        int ordinal = state.ordinal();
        if (ordinal != 0) {
            if (ordinal == 1) {
                return VideoCallParticipantView.ParticipantData.ApplicationStreamState.ACTIVE;
            }
            if (ordinal != 2) {
                if (ordinal == 3) {
                    return VideoCallParticipantView.ParticipantData.ApplicationStreamState.ENDED;
                }
                if (ordinal == 4) {
                    return VideoCallParticipantView.ParticipantData.ApplicationStreamState.PAUSED;
                }
                if (ordinal == 5) {
                    return VideoCallParticipantView.ParticipantData.ApplicationStreamState.INACTIVE;
                }
                throw new NoWhenBranchMatchedException();
            }
        }
        return VideoCallParticipantView.ParticipantData.ApplicationStreamState.CONNECTING;
    }

    private final boolean hasVideoPermission() {
        StoreState storeState = this.mostRecentStoreState;
        Long l = null;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid = (StoreState.Valid) storeState;
        if (valid == null) {
            return false;
        }
        VideoPermissionsManager videoPermissionsManager = this.videoPermissionsManager;
        Channel channel = valid.getCallModel().getChannel();
        Guild guild = valid.getCallModel().getGuild();
        if (guild != null) {
            l = guild.getAfkChannelId();
        }
        return videoPermissionsManager.hasVideoPermission(channel, l, valid.getMyPermissions());
    }

    private final boolean isOneOnOneMeCall(List<? extends CallParticipant> list) {
        boolean z2;
        boolean z3;
        if (list.size() == 2) {
            if (!list.isEmpty()) {
                for (CallParticipant callParticipant : list) {
                    if (!(callParticipant instanceof CallParticipant.UserOrStreamParticipant) || !((CallParticipant.UserOrStreamParticipant) callParticipant).getParticipantData().f2812b.isMe()) {
                        z3 = false;
                        continue;
                    } else {
                        z3 = true;
                        continue;
                    }
                    if (z3) {
                        z2 = true;
                        break;
                    }
                }
            }
            z2 = false;
            if (z2) {
                return true;
            }
        }
        return false;
    }

    private final boolean isStreamFocused(ModelApplicationStream modelApplicationStream) {
        String str = this.focusedVideoParticipantKey;
        return m.areEqual(str, modelApplicationStream.getOwnerId() + "--STREAM");
    }

    private final Observable<StoreState> observeStoreState() {
        Observable Y = this.storeChannels.observeChannel(this.channelId).Y(new j0.k.b<Channel, Observable<? extends StoreState>>() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$observeStoreState$1

            /* compiled from: WidgetCallFullscreenViewModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\"\u001a\u00020\u001f2\b\u0010\u0001\u001a\u0004\u0018\u00010\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u000e\u0010\u0006\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00052\u000e\u0010\t\u001a\n \b*\u0004\u0018\u00010\u00070\u00072\b\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\b\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0015\u001a\u00020\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u00162\u0016\u0010\u001a\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0019\u0012\u0004\u0012\u00020\f0\u00182\u0006\u0010\u001b\u001a\u00020\f2\u0016\u0010\u001e\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u001c\u0012\u0004\u0012\u00020\u001d0\u0018H\n¢\u0006\u0004\b \u0010!"}, d2 = {"Lcom/discord/widgets/voice/model/CallModel;", "callModel", "Lcom/discord/widgets/stage/model/StageCallModel;", "stageCallModel", "", "Lcom/discord/api/permission/PermissionBit;", "myPermissions", "", "kotlin.jvm.PlatformType", "streamVolume", "Lcom/discord/rtcconnection/RtcConnection$Quality;", "streamQuality", "", "mentionCount", "Lcom/discord/stores/StoreConnectivity$DelayedState;", "connectivityState", "Lcom/discord/widgets/stage/StageRoles;", "stageRoles", "Lcom/discord/models/experiments/domain/Experiment;", "stopOffscreenVideoExperiment", "", "isTextInVoiceEnabled", "Lcom/discord/api/channel/Channel;", "selectedTextChannel", "", "Lcom/discord/primitives/ChannelId;", "mentionsMap", "textInVoiceUnreadsCount", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/api/application/ApplicationAsset;", "embeddedAppBackgrounds", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;", "invoke", "(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/stage/model/StageCallModel;Ljava/lang/Long;Ljava/lang/Float;Lcom/discord/rtcconnection/RtcConnection$Quality;ILcom/discord/stores/StoreConnectivity$DelayedState;Lcom/discord/widgets/stage/StageRoles;Lcom/discord/models/experiments/domain/Experiment;ZLcom/discord/api/channel/Channel;Ljava/util/Map;ILjava/util/Map;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$observeStoreState$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function14<CallModel, StageCallModel, Long, Float, RtcConnection.Quality, Integer, StoreConnectivity.DelayedState, StageRoles, Experiment, Boolean, Channel, Map<Long, ? extends Integer>, Integer, Map<Long, ? extends ApplicationAsset>, WidgetCallFullscreenViewModel.StoreState> {
                public AnonymousClass1() {
                    super(14);
                }

                @Override // kotlin.jvm.functions.Function14
                public /* bridge */ /* synthetic */ WidgetCallFullscreenViewModel.StoreState invoke(CallModel callModel, StageCallModel stageCallModel, Long l, Float f, RtcConnection.Quality quality, Integer num, StoreConnectivity.DelayedState delayedState, StageRoles stageRoles, Experiment experiment, Boolean bool, Channel channel, Map<Long, ? extends Integer> map, Integer num2, Map<Long, ? extends ApplicationAsset> map2) {
                    return invoke(callModel, stageCallModel, l, f, quality, num.intValue(), delayedState, stageRoles, experiment, bool.booleanValue(), channel, (Map<Long, Integer>) map, num2.intValue(), (Map<Long, ApplicationAsset>) map2);
                }

                public final WidgetCallFullscreenViewModel.StoreState invoke(CallModel callModel, StageCallModel stageCallModel, Long l, Float f, RtcConnection.Quality quality, int i, StoreConnectivity.DelayedState delayedState, StageRoles stageRoles, Experiment experiment, boolean z2, Channel channel, Map<Long, Integer> map, int i2, Map<Long, ApplicationAsset> map2) {
                    long j;
                    m.checkNotNullParameter(delayedState, "connectivityState");
                    m.checkNotNullParameter(map, "mentionsMap");
                    m.checkNotNullParameter(map2, "embeddedAppBackgrounds");
                    if (callModel == null) {
                        return WidgetCallFullscreenViewModel.StoreState.Invalid.INSTANCE;
                    }
                    Boolean valueOf = Boolean.valueOf(callModel.getVoiceSettings().getNoiseProcessing() == StoreMediaSettings.NoiseProcessing.Cancellation);
                    m.checkNotNullExpressionValue(f, "streamVolume");
                    float floatValue = f.floatValue();
                    boolean z3 = experiment != null && experiment.getBucket() == 1;
                    boolean z4 = channel != null && ChannelUtils.E(channel);
                    j = WidgetCallFullscreenViewModel.this.channelId;
                    Integer num = map.get(Long.valueOf(j));
                    return new WidgetCallFullscreenViewModel.StoreState.Valid(callModel, stageCallModel, valueOf, l, floatValue, quality, i, delayedState, stageRoles, z3, z2, z4, num != null ? num.intValue() : 0, i2, map2, null);
                }
            }

            public final Observable<? extends WidgetCallFullscreenViewModel.StoreState> call(Channel channel) {
                long j;
                long j2;
                StorePermissions storePermissions;
                long j3;
                StoreStreamRtcConnection storeStreamRtcConnection;
                StoreStreamRtcConnection storeStreamRtcConnection2;
                StoreMentions storeMentions;
                StoreConnectivity storeConnectivity;
                StoreStageChannels storeStageChannels;
                long j4;
                StoreExperiments storeExperiments;
                StoreChannelsSelected storeChannelsSelected;
                long j5;
                StoreApplicationAssets storeApplicationAssets;
                CallModel.Companion companion = CallModel.Companion;
                j = WidgetCallFullscreenViewModel.this.channelId;
                Observable<CallModel> observable = companion.get(j);
                StageCallModel.Companion companion2 = StageCallModel.Companion;
                j2 = WidgetCallFullscreenViewModel.this.channelId;
                Observable<StageCallModel> observeStageCallModel = companion2.observeStageCallModel(j2);
                storePermissions = WidgetCallFullscreenViewModel.this.permissionsStore;
                j3 = WidgetCallFullscreenViewModel.this.channelId;
                Observable<Long> observePermissionsForChannel = storePermissions.observePermissionsForChannel(j3);
                storeStreamRtcConnection = WidgetCallFullscreenViewModel.this.streamRtcConnectionStore;
                Observable<Float> q = storeStreamRtcConnection.observeStreamVolume().q();
                m.checkNotNullExpressionValue(q, "streamRtcConnectionStore…().distinctUntilChanged()");
                storeStreamRtcConnection2 = WidgetCallFullscreenViewModel.this.streamRtcConnectionStore;
                Observable<RtcConnection.Quality> q2 = storeStreamRtcConnection2.observeConnectionQuality().q();
                m.checkNotNullExpressionValue(q2, "streamRtcConnectionStore…().distinctUntilChanged()");
                storeMentions = WidgetCallFullscreenViewModel.this.mentionsStore;
                Observable<Integer> observeTotalMentions = storeMentions.observeTotalMentions();
                storeConnectivity = WidgetCallFullscreenViewModel.this.connectivityStore;
                Observable<StoreConnectivity.DelayedState> observeState = storeConnectivity.observeState();
                storeStageChannels = WidgetCallFullscreenViewModel.this.stageStore;
                j4 = WidgetCallFullscreenViewModel.this.channelId;
                Observable<StageRoles> q3 = storeStageChannels.observeMyRoles(j4).q();
                m.checkNotNullExpressionValue(q3, "stageStore.observeMyRole…d).distinctUntilChanged()");
                storeExperiments = WidgetCallFullscreenViewModel.this.experimentsStore;
                Observable<Experiment> observeUserExperiment = storeExperiments.observeUserExperiment("2021-03_stop_offscreen_video_streams", true);
                Observable<Boolean> observeEnabled = TextInVoiceFeatureFlag.Companion.getINSTANCE().observeEnabled(channel != null ? Long.valueOf(channel.f()) : null);
                storeChannelsSelected = WidgetCallFullscreenViewModel.this.channelsSelectedStore;
                Observable<Channel> observeSelectedChannel = storeChannelsSelected.observeSelectedChannel();
                StoreStream.Companion companion3 = StoreStream.Companion;
                Observable<Map<Long, Integer>> observeMentionCounts = companion3.getMentions().observeMentionCounts();
                StoreReadStates readStates = companion3.getReadStates();
                j5 = WidgetCallFullscreenViewModel.this.channelId;
                Observable<Integer> observeUnreadCountForChannel = readStates.observeUnreadCountForChannel(j5);
                storeApplicationAssets = WidgetCallFullscreenViewModel.this.applicationAssetsStore;
                return ObservableCombineLatestOverloadsKt.combineLatest(observable, observeStageCallModel, observePermissionsForChannel, q, q2, observeTotalMentions, observeState, q3, observeUserExperiment, observeEnabled, observeSelectedChannel, observeMentionCounts, observeUnreadCountForChannel, storeApplicationAssets.observeEmbeddedAppBackgrounds(), new AnonymousClass1());
            }
        });
        m.checkNotNullExpressionValue(Y, "storeChannels.observeCha…d\n        }\n      }\n    }");
        return Y;
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void onIdleStateChanged(boolean z2) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && z2 != valid.isIdle()) {
            updateViewState((ViewState) ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, null, z2, false, false, null, null, null, null, false, null, null, null, false, false, 0.0f, 0, false, null, false, false, 0, false, null, null, null, 0, 0, false, false, null, -17, 1, null));
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(new Event.OnIdleStateChanged(z2));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    @MainThread
    public final void setOffScreenVideoParticipants(List<VideoCallParticipantView.ParticipantData> list) {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && valid.getStopOffscreenVideo()) {
            ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
            for (VideoCallParticipantView.ParticipantData participantData : list) {
                arrayList.add(participantData.a);
            }
            HashSet hashSet = u.toHashSet(arrayList);
            for (CallParticipant callParticipant : computeVisibleVideoParticipants()) {
                if (callParticipant instanceof CallParticipant.UserOrStreamParticipant) {
                    VideoCallParticipantView.ParticipantData participantData2 = ((CallParticipant.UserOrStreamParticipant) callParticipant).getParticipantData();
                    if (participantData2.g == VideoCallParticipantView.ParticipantData.Type.DEFAULT) {
                        this.mediaSettingsStore.setUserOffScreen(participantData2.f2812b.getUser().getId(), !hashSet.contains(participantData2.a));
                    }
                }
            }
        }
    }

    private final boolean shouldShowMoreAudioOutputs(CallModel callModel) {
        boolean z2;
        List<DiscordAudioManager.AudioDevice> audioDevices = callModel.getAudioManagerState().getAudioDevices();
        if (!(audioDevices instanceof Collection) || !audioDevices.isEmpty()) {
            for (DiscordAudioManager.AudioDevice audioDevice : audioDevices) {
                if (audioDevice.a != DiscordAudioManager.DeviceTypes.BLUETOOTH_HEADSET || !audioDevice.f2761b) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    return true;
                }
            }
        }
        return false;
    }

    @MainThread
    private final void startTapForwardingJob() {
        cancelTapForwardingJob();
        Observable<Long> e02 = Observable.e0(255L, TimeUnit.MILLISECONDS, this.backgroundThreadScheduler);
        m.checkNotNullExpressionValue(e02, "Observable\n        .time…ackgroundThreadScheduler)");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(e02, this, null, 2, null), WidgetCallFullscreenViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetCallFullscreenViewModel$startTapForwardingJob$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetCallFullscreenViewModel$startTapForwardingJob$2(this));
    }

    @MainThread
    private final void stopWatchingStream() {
        StoreApplicationStreaming.ActiveApplicationStream activeStream;
        ModelApplicationStream stream;
        ViewState requireViewState = requireViewState();
        if (!(requireViewState instanceof ViewState.Valid)) {
            requireViewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) requireViewState;
        if (valid != null && (activeStream = valid.getCallModel().getActiveStream()) != null && (stream = activeStream.getStream()) != null) {
            this.applicationStreamingStore.stopStream(stream.getEncodedStreamKey());
            CallModel callModel = valid.getCallModel();
            if (isStreamFocused(stream)) {
                this.focusedVideoParticipantKey = null;
                updateViewState((ViewState) ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, computeVisibleVideoParticipants(), false, false, false, null, null, null, this.focusedVideoParticipantKey, false, computePipParticipant(callModel.getMyId(), callModel.getSelectedVideoDevice(), callModel.isStreaming(), valid.getCameraState()), null, null, false, false, 0.0f, 0, false, null, false, false, 0, false, null, null, null, 0, 0, false, false, null, -5129, 1, null));
            }
        }
    }

    @MainThread
    private final void stopWatchingStreamIfEnded() {
        ViewState viewState = getViewState();
        StoreApplicationStreaming.ActiveApplicationStream.State state = null;
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            StoreApplicationStreaming.ActiveApplicationStream activeStream = valid.getCallModel().getActiveStream();
            StoreApplicationStreaming.ActiveApplicationStream activeStream2 = valid.getCallModel().getActiveStream();
            if (activeStream2 != null) {
                activeStream2.getStream();
            }
            if (activeStream != null) {
                state = activeStream.getState();
            }
            if (state == StoreApplicationStreaming.ActiveApplicationStream.State.ENDED) {
                stopWatchingStream();
            }
        }
    }

    public final void disableControlFading() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && !valid.isControlFadingDisabled()) {
            updateViewState((ViewState) ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, null, false, true, false, null, null, null, null, false, null, null, null, false, false, 0.0f, 0, false, null, false, false, 0, false, null, null, null, 0, 0, false, false, null, -33, 1, null));
        }
    }

    @MainThread
    public final void disconnect() {
        this.videoPlayerIdleDetector.onPreventIdle();
        this.selectedVoiceChannelStore.clear();
        if (!this.wasEverConnected || this.wasEverMultiParticipant) {
            ViewState viewState = getViewState();
            String str = null;
            if (!(viewState instanceof ViewState.Valid)) {
                viewState = null;
            }
            ViewState.Valid valid = (ViewState.Valid) viewState;
            if (valid != null) {
                CallModel callModel = valid.getCallModel();
                RtcConnection.Metadata rtcConnectionMetadata = callModel.getRtcConnectionMetadata();
                PublishSubject<Event> publishSubject = this.eventSubject;
                long j = this.channelId;
                String str2 = rtcConnectionMetadata != null ? rtcConnectionMetadata.a : null;
                if (rtcConnectionMetadata != null) {
                    str = rtcConnectionMetadata.f2753b;
                }
                publishSubject.k.onNext(new Event.EnqueueCallFeedbackSheet(j, str2, str, callModel.getCallDurationMs(this.clock), callModel.getCallFeedbackTriggerRateDenominator()));
            }
        }
    }

    @MainThread
    public final void focusVideoParticipant(String str) {
        m.checkNotNullParameter(str, "participantKey");
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            if (!isOneOnOneMeCall(this.allVideoParticipants)) {
                this.focusedVideoParticipantKey = str;
            }
            updateViewState((ViewState) ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, computeVisibleVideoParticipants(), false, false, false, null, null, null, this.focusedVideoParticipantKey, false, computePipParticipant(valid.getCallModel().getMyId(), valid.getCallModel().getSelectedVideoDevice(), valid.getCallModel().isStreaming(), valid.getCameraState()), null, null, false, false, 0.0f, 0, false, null, false, false, 0, false, null, null, null, 0, 0, false, false, null, -5129, 1, null));
        }
    }

    public final boolean getShowStageCallBottomSheet() {
        return this.showStageCallBottomSheet;
    }

    @MainThread
    public final void handleBottomSheetState(int i) {
        this.bottomSheetState = Integer.valueOf(i);
    }

    /* JADX WARN: Code restructure failed: missing block: B:112:0x02be, code lost:
        if (r0 != null) goto L115;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:278:0x0554  */
    /* JADX WARN: Removed duplicated region for block: B:279:0x055b  */
    /* JADX WARN: Removed duplicated region for block: B:283:0x0580  */
    /* JADX WARN: Removed duplicated region for block: B:289:0x05a9  */
    /* JADX WARN: Removed duplicated region for block: B:325:0x0592 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:327:0x05c0 A[SYNTHETIC] */
    @androidx.annotation.MainThread
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleStoreState(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel.StoreState r49) {
        /*
            Method dump skipped, instructions count: 1519
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel.handleStoreState(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$StoreState):void");
    }

    @MainThread
    public final void moveMeToAudience() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && !valid.isMovingToAudience()) {
            updateViewState((ViewState) ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, null, false, false, false, null, null, null, null, false, null, null, null, false, false, 0.0f, 0, false, null, false, true, 0, false, null, null, null, 0, 0, false, false, null, -4194305, 1, null));
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApiSerializeNulls().setMeSuppressed(valid.getCallModel().getChannel(), true), false, 1, null), this, null, 2, null), WidgetCallFullscreenViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : new WidgetCallFullscreenViewModel$moveMeToAudience$1(this), WidgetCallFullscreenViewModel$moveMeToAudience$2.INSTANCE);
        }
    }

    public final Observable<Event> observeEvents() {
        PublishSubject<Event> publishSubject = this.eventSubject;
        m.checkNotNullExpressionValue(publishSubject, "eventSubject");
        return publishSubject;
    }

    @MainThread
    public final void onCameraPermissionsGranted() {
        StoreMediaEngine.selectDefaultVideoDevice$default(this.mediaEngineStore, null, 1, null);
        PublishSubject<Event> publishSubject = this.eventSubject;
        publishSubject.k.onNext(new Event.AccessibilityAnnouncement(R.string.camera_a11y_turned_on));
    }

    @Override // com.discord.app.AppViewModel, androidx.lifecycle.ViewModel
    @MainThread
    public void onCleared() {
        super.onCleared();
        this.videoPlayerIdleDetector.dispose();
    }

    @MainThread
    public final void onDeafenPressed() {
        CallModel callModel;
        this.videoPlayerIdleDetector.onPreventIdle();
        StoreState storeState = this.mostRecentStoreState;
        ViewState.Valid valid = null;
        if (!(storeState instanceof StoreState.Valid)) {
            storeState = null;
        }
        StoreState.Valid valid2 = (StoreState.Valid) storeState;
        if (valid2 == null || (callModel = valid2.getCallModel()) == null || !callModel.isServerDeafened()) {
            this.mediaSettingsStore.toggleSelfDeafened();
            ViewState viewState = getViewState();
            if (viewState instanceof ViewState.Valid) {
                valid = viewState;
            }
            ViewState.Valid valid3 = valid;
            if (valid3 != null) {
                this.eventSubject.k.onNext(new Event.AccessibilityAnnouncement(valid3.isDeafened() ? R.string.voice_channel_undeafened : R.string.voice_channel_deafened));
                return;
            }
            return;
        }
        emitServerDeafenedDialogEvent();
    }

    @MainThread
    public final void onEmptyStateTapped() {
        startTapForwardingJob();
    }

    @MainThread
    public final void onGridParticipantLongPressed(CallParticipant callParticipant) {
        m.checkNotNullParameter(callParticipant, "participant");
        if (callParticipant instanceof CallParticipant.UserOrStreamParticipant) {
            CallParticipant.UserOrStreamParticipant userOrStreamParticipant = (CallParticipant.UserOrStreamParticipant) callParticipant;
            if (userOrStreamParticipant.getParticipantData().g == VideoCallParticipantView.ParticipantData.Type.DEFAULT) {
                PublishSubject<Event> publishSubject = this.eventSubject;
                publishSubject.k.onNext(new Event.ShowUserSheet(userOrStreamParticipant.getParticipantData().f2812b.getUser().getId(), this.channelId));
            }
        }
    }

    @MainThread
    public final void onGridParticipantTapped(CallParticipant callParticipant) {
        m.checkNotNullParameter(callParticipant, "callParticipant");
        if (callParticipant instanceof CallParticipant.EmbeddedActivityParticipant) {
            this.eventSubject.k.onNext(Event.ShowActivitiesDesktopOnlyDialog.INSTANCE);
        }
        String participantFocusKey = getParticipantFocusKey(callParticipant);
        ParticipantTap participantTap = new ParticipantTap(participantFocusKey, this.clock.currentTimeMillis());
        ParticipantTap participantTap2 = this.lastParticipantTap;
        this.lastParticipantTap = participantTap;
        if (participantTap2 == null || (!m.areEqual(participantTap2.getParticipantFocusKey(), participantFocusKey))) {
            startTapForwardingJob();
            return;
        }
        if (!(participantTap.getTimestamp() - participantTap2.getTimestamp() <= 255) || !(callParticipant instanceof CallParticipant.UserOrStreamParticipant)) {
            startTapForwardingJob();
            return;
        }
        if (!m.areEqual(this.focusedVideoParticipantKey, participantFocusKey)) {
            focusVideoParticipant(participantFocusKey);
        } else {
            clearFocusedVideoParticipant();
        }
        cancelTapForwardingJob();
        this.lastParticipantTap = null;
    }

    @MainThread
    public final void onMuteClicked() {
        this.videoPlayerIdleDetector.onPreventIdle();
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            CallModel callModel = valid.getCallModel();
            if (callModel.isSuppressed()) {
                emitSuppressedDialogEvent();
                return;
            } else if (callModel.isMuted()) {
                emitServerMutedDialogEvent();
                return;
            }
        }
        StoreMediaSettings.SelfMuteFailure selfMuteFailure = this.mediaSettingsStore.toggleSelfMuted();
        if (selfMuteFailure == null) {
            if (valid != null) {
                boolean isMeMutedByAnySource = valid.getCallModel().isMeMutedByAnySource();
                this.eventSubject.k.onNext(new Event.AccessibilityAnnouncement(isMeMutedByAnySource ? R.string.voice_channel_unmuted : R.string.voice_channel_muted));
            }
        } else if (selfMuteFailure.ordinal() == 0) {
            emitShowNoVadPermissionDialogEvent();
        }
    }

    @MainThread
    public final void onPushToTalkPressed(boolean z2) {
        if (!z2) {
            startIdle();
        } else {
            this.videoPlayerIdleDetector.endIdleDetection();
        }
        this.mediaEngineStore.setPttActive(z2);
    }

    @MainThread
    public final void onScreenShareClick() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid == null) {
            return;
        }
        if (valid.getCallModel().isStreaming()) {
            stopScreenShare();
        } else if (!hasVideoPermission()) {
            emitShowNoScreenSharePermissionDialogEvent();
        } else {
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(Event.RequestStartStream.INSTANCE);
        }
    }

    @MainThread
    public final void onShowParticipantsPressed() {
        this.mediaSettingsStore.updateVoiceParticipantsHidden(false);
    }

    public final void onStreamPerceptualVolumeChanged(float f, boolean z2) {
        if (z2) {
            this.streamRtcConnectionStore.updateStreamVolume(PerceptualVolumeUtils.INSTANCE.perceptualToAmplitude(f, 300.0f));
            this.audioManagerStore.updateMediaVolume(f / 300.0f);
        }
    }

    @MainThread
    public final void onStreamQualityIndicatorClicked(VideoCallParticipantView.StreamResolution streamResolution, VideoCallParticipantView.StreamFps streamFps) {
        m.checkNotNullParameter(streamResolution, "resolution");
        if (streamResolution.b() || (streamFps != null && streamFps.a())) {
            PublishSubject<Event> publishSubject = this.eventSubject;
            publishSubject.k.onNext(Event.NavigateToPremiumSettings.INSTANCE);
        }
    }

    @MainThread
    public final void onStreamQualityIndicatorShown(VideoCallParticipantView.StreamResolution streamResolution, VideoCallParticipantView.StreamFps streamFps) {
        m.checkNotNullParameter(streamResolution, "resolution");
        boolean b2 = streamResolution.b();
        Boolean valueOf = streamFps != null ? Boolean.valueOf(streamFps.a()) : null;
        if (!this.logStreamQualityIndicatorViewed) {
            return;
        }
        if (b2 || m.areEqual(valueOf, Boolean.TRUE)) {
            this.logStreamQualityIndicatorViewed = false;
            this.storeAnalytics.streamQualityIndicatorViewed(b2, valueOf);
        }
    }

    @MainThread
    public final void onTextInVoiceTapped() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null) {
            ChannelSelector.selectChannel$default(ChannelSelector.Companion.getInstance(), valid.getCallModel().getChannel(), null, SelectedChannelAnalyticsLocation.TEXT_IN_VOICE, 2, null);
            StoreNavigation.setNavigationPanelAction$default(this.storeNavigation, StoreNavigation.PanelAction.CLOSE, null, 2, null);
        }
    }

    public final void onVisitCommunityButtonClicked(long j) {
        this.guildSelectedStore.set(j);
        StoreTabsNavigation.selectHomeTab$default(this.tabsNavigationStore, StoreNavigation.PanelAction.CLOSE, false, 2, null);
    }

    public final void requestStopWatchingStreamFromUserInput() {
        StoreApplicationStreaming.ActiveApplicationStream activeStream;
        ModelApplicationStream stream;
        stopWatchingStream();
        ViewState requireViewState = requireViewState();
        String str = null;
        if (!(requireViewState instanceof ViewState.Valid)) {
            requireViewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) requireViewState;
        if (valid != null && (activeStream = valid.getCallModel().getActiveStream()) != null && (stream = activeStream.getStream()) != null) {
            CallModel callModel = valid.getCallModel();
            String encodedStreamKey = stream.getEncodedStreamKey();
            RtcConnection.Metadata rtcConnectionMetadata = callModel.getRtcConnectionMetadata();
            if (rtcConnectionMetadata != null) {
                str = rtcConnectionMetadata.f2753b;
            }
            enqueueStreamFeedbackSheet(encodedStreamKey, str, callModel.getStreamFeedbackTriggerRateDenominator());
        }
    }

    public final void selectTextChannelAfterFinish() {
        Guild guild;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && valid.isTextInVoiceEnabled() && valid.isTextInVoiceChannelSelected() && (guild = valid.getCallModel().getGuild()) != null) {
            ChannelSelector.Companion.getInstance().selectPreviousChannel(guild.getId());
        }
    }

    public final void setShowStageCallBottomSheet(boolean z2) {
        this.showStageCallBottomSheet = z2;
    }

    public final void setTargetChannelId(long j) {
        Subscription subscription = this.storeObservableSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        this.channelId = j;
        Observable<StoreState> q = observeStoreState().q();
        m.checkNotNullExpressionValue(q, "observeStoreState()\n    …  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(q), this, null, 2, null), WidgetCallFullscreenViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetCallFullscreenViewModel$setTargetChannelId$1(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetCallFullscreenViewModel$setTargetChannelId$2(this));
    }

    @MainThread
    public final void startIdle() {
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && !valid.isControlFadingDisabled() && valid.getDisplayMode() == DisplayMode.GRID) {
            Integer num = this.bottomSheetState;
            if (num == null || num.intValue() != 3) {
                this.videoPlayerIdleDetector.beginIdleDetection();
            }
        }
    }

    @MainThread
    public final void startScreenShare(Intent intent) {
        m.checkNotNullParameter(intent, "intent");
        this.videoPlayerIdleDetector.onPreventIdle();
        this.voiceEngineServiceController.startStream(intent);
    }

    @MainThread
    public final void stopIdle() {
        this.videoPlayerIdleDetector.endIdleDetection();
    }

    @MainThread
    public final void stopScreenShare() {
        StoreApplicationStreaming.ActiveApplicationStream activeStream;
        ModelApplicationStream stream;
        this.videoPlayerIdleDetector.onPreventIdle();
        this.voiceEngineServiceController.stopStream();
        ViewState viewState = getViewState();
        String str = null;
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && (activeStream = valid.getCallModel().getActiveStream()) != null && (stream = activeStream.getStream()) != null) {
            CallModel callModel = valid.getCallModel();
            String encodedStreamKey = stream.getEncodedStreamKey();
            RtcConnection.Metadata rtcConnectionMetadata = callModel.getRtcConnectionMetadata();
            if (rtcConnectionMetadata != null) {
                str = rtcConnectionMetadata.f2753b;
            }
            enqueueStreamFeedbackSheet(encodedStreamKey, str, callModel.getStreamFeedbackTriggerRateDenominator());
        }
    }

    @MainThread
    public final void switchCameraInputPressed() {
        this.videoPlayerIdleDetector.onPreventIdle();
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && valid.getCameraState() != CameraState.CAMERA_DISABLED) {
            if (valid.getCameraState() == CameraState.CAMERA_ON) {
                this.mediaEngineStore.cycleVideoInputDevice();
            }
        }
    }

    @MainThread
    public final void targetAndFocusStream(String str) {
        m.checkNotNullParameter(str, "streamKey");
        targetStream(str);
        long ownerId = ModelApplicationStream.Companion.decodeStreamKey(str).getOwnerId();
        focusVideoParticipant(ownerId + "--STREAM");
    }

    @MainThread
    public final void targetStream(String str) {
        m.checkNotNullParameter(str, "streamKey");
        StoreApplicationStreaming.targetStream$default(this.applicationStreamingStore, str, false, 2, null);
    }

    @MainThread
    public final void toggleCameraPressed() {
        this.videoPlayerIdleDetector.onPreventIdle();
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && valid.getCameraState() != CameraState.CAMERA_DISABLED) {
            if (!hasVideoPermission()) {
                emitShowNoVideoPermissionDialogEvent();
                return;
            }
            if (valid.getCameraState() == CameraState.CAMERA_ON) {
                this.mediaEngineStore.selectVideoInputDevice(null);
                this.eventSubject.k.onNext(new Event.AccessibilityAnnouncement(R.string.camera_a11y_turned_off));
                return;
            }
            int numUsersConnected = valid.getCallModel().getNumUsersConnected();
            GuildMaxVideoChannelUsers guildMaxVideoChannelMembers = valid.getCallModel().getGuildMaxVideoChannelMembers();
            if (guildMaxVideoChannelMembers instanceof GuildMaxVideoChannelUsers.Limited) {
                GuildMaxVideoChannelUsers.Limited limited = (GuildMaxVideoChannelUsers.Limited) guildMaxVideoChannelMembers;
                if (numUsersConnected > limited.a()) {
                    this.eventSubject.k.onNext(new Event.ShowCameraCapacityDialog(limited.a()));
                    return;
                }
            }
            this.eventSubject.k.onNext(Event.ShowRequestCameraPermissionsDialog.INSTANCE);
        }
    }

    @MainThread
    public final void toggleRequestToSpeak() {
        Observable<Void> observable;
        ViewState viewState = getViewState();
        if (!(viewState instanceof ViewState.Valid)) {
            viewState = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState;
        if (valid != null && !valid.isUpdatingRequestToSpeak()) {
            updateViewState((ViewState) ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, null, false, false, false, null, null, null, null, false, null, null, null, false, false, 0.0f, 0, false, null, true, false, 0, false, null, null, null, 0, 0, false, false, null, -2097153, 1, null));
            Channel channel = valid.getCallModel().getChannel();
            boolean z2 = !valid.getCallModel().isMyHandRaised();
            if (!z2) {
                observable = RestAPI.Companion.getApiSerializeNulls().setMeSuppressed(channel, true);
            } else {
                observable = RestAPI.requestToSpeak$default(RestAPI.Companion.getApi(), channel, null, 2, null);
            }
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(observable, false, 1, null), this, null, 2, null), WidgetCallFullscreenViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : new WidgetCallFullscreenViewModel$toggleRequestToSpeak$1(this), new WidgetCallFullscreenViewModel$toggleRequestToSpeak$2(z2, channel));
        }
    }

    @MainThread
    public final void tryConnectToVoice() {
        this.selectedVoiceChannelStore.selectVoiceChannel(this.channelId);
    }

    public final void updateOffScreenParticipantsFromScroll(List<VideoCallParticipantView.ParticipantData> list) {
        m.checkNotNullParameter(list, "visibleVideoParticipants");
        this.offScreenSubject.k.onNext(list);
    }

    public ViewState modifyPendingViewState(ViewState viewState, ViewState viewState2) {
        m.checkNotNullParameter(viewState2, "pendingViewState");
        if (!(viewState instanceof ViewState.Valid) || !(viewState2 instanceof ViewState.Valid)) {
            return viewState2;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState2;
        boolean isIdle = valid.isIdle();
        List<CallParticipant> visibleVideoParticipants = valid.getVisibleVideoParticipants();
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(visibleVideoParticipants, 10));
        for (Object obj : visibleVideoParticipants) {
            boolean z2 = false;
            boolean z3 = !isIdle && valid.getVisibleVideoParticipants().size() != 1;
            if (this.focusedVideoParticipantKey != null) {
                z2 = true;
            }
            if (obj instanceof CallParticipant.UserOrStreamParticipant) {
                CallParticipant.UserOrStreamParticipant userOrStreamParticipant = (CallParticipant.UserOrStreamParticipant) obj;
                if (userOrStreamParticipant.getParticipantData().h != z3 || userOrStreamParticipant.getParticipantData().i != z2) {
                    obj = userOrStreamParticipant.copy(VideoCallParticipantView.ParticipantData.a(userOrStreamParticipant.getParticipantData(), null, false, null, null, null, null, z3, z2, null, 319));
                }
            }
            arrayList.add(obj);
        }
        return ViewState.Valid.m63copy6O6tUOw$default(valid, null, null, null, arrayList, false, false, false, null, null, null, null, false, null, null, null, false, false, 0.0f, 0, false, null, false, false, 0, false, null, null, null, 0, 0, false, false, null, -9, 1, null);
    }

    public void updateViewState(ViewState viewState) {
        Object obj;
        StoreVoiceParticipants.VoiceUser voiceUser;
        User user;
        CallModel callModel;
        m.checkNotNullParameter(viewState, "viewState");
        ViewState viewState2 = getViewState();
        if (!(viewState2 instanceof ViewState.Valid)) {
            viewState2 = null;
        }
        ViewState.Valid valid = (ViewState.Valid) viewState2;
        String analyticsVideoLayout = valid != null ? valid.getAnalyticsVideoLayout() : null;
        DisplayMode displayMode = valid != null ? valid.getDisplayMode() : null;
        super.updateViewState((WidgetCallFullscreenViewModel) viewState);
        if (viewState instanceof ViewState.Valid) {
            ViewState.Valid valid2 = (ViewState.Valid) viewState;
            String analyticsVideoLayout2 = valid2.getAnalyticsVideoLayout();
            if (analyticsVideoLayout2 != null && (!m.areEqual(analyticsVideoLayout2, analyticsVideoLayout))) {
                long myId = valid2.getCallModel().getMyId();
                ViewState viewState3 = getViewState();
                if (!(viewState3 instanceof ViewState.Valid)) {
                    viewState3 = null;
                }
                ViewState.Valid valid3 = (ViewState.Valid) viewState3;
                this.analyticsStore.trackVideoLayoutToggled(analyticsVideoLayout2, myId, (valid3 == null || (callModel = valid3.getCallModel()) == null) ? null : callModel.getChannel());
            }
            DisplayMode displayMode2 = valid2.getDisplayMode();
            if (displayMode2 != displayMode) {
                int ordinal = displayMode2.ordinal();
                if (ordinal == 0) {
                    startIdle();
                } else if (ordinal == 2) {
                    this.videoPlayerIdleDetector.endIdleDetection();
                }
            }
            String focusedParticipantKey = valid2.getFocusedParticipantKey();
            Iterator<T> it = valid2.getVisibleVideoParticipants().iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (m.areEqual(getParticipantFocusKey((CallParticipant) obj), focusedParticipantKey)) {
                    break;
                }
            }
            CallParticipant callParticipant = (CallParticipant) obj;
            if (callParticipant != null ? callParticipant instanceof CallParticipant.UserOrStreamParticipant : true) {
                CallParticipant.UserOrStreamParticipant userOrStreamParticipant = (CallParticipant.UserOrStreamParticipant) callParticipant;
                VideoCallParticipantView.ParticipantData participantData = userOrStreamParticipant != null ? userOrStreamParticipant.getParticipantData() : null;
                Long valueOf = (participantData == null || (voiceUser = participantData.f2812b) == null || (user = voiceUser.getUser()) == null) ? null : Long.valueOf(user.getId());
                VideoCallParticipantView.ParticipantData.Type type = participantData != null ? participantData.g : null;
                if (type == null) {
                    this.streamRtcConnectionStore.updateFocusedParticipant(null);
                    RtcConnection rtcConnection$app_productionGoogleRelease = StoreStream.Companion.getRtcConnection().getRtcConnection$app_productionGoogleRelease();
                    if (rtcConnection$app_productionGoogleRelease != null) {
                        rtcConnection$app_productionGoogleRelease.w(null);
                        return;
                    }
                    return;
                }
                int ordinal2 = type.ordinal();
                if (ordinal2 == 0) {
                    this.streamRtcConnectionStore.updateFocusedParticipant(null);
                    RtcConnection rtcConnection$app_productionGoogleRelease2 = StoreStream.Companion.getRtcConnection().getRtcConnection$app_productionGoogleRelease();
                    if (rtcConnection$app_productionGoogleRelease2 != null) {
                        rtcConnection$app_productionGoogleRelease2.w(valueOf);
                    }
                    if (valueOf != null) {
                        this.mediaSettingsStore.setUserOffScreen(valueOf.longValue(), false);
                    }
                } else if (ordinal2 == 1) {
                    this.streamRtcConnectionStore.updateFocusedParticipant(valueOf);
                    RtcConnection rtcConnection$app_productionGoogleRelease3 = StoreStream.Companion.getRtcConnection().getRtcConnection$app_productionGoogleRelease();
                    if (rtcConnection$app_productionGoogleRelease3 != null) {
                        rtcConnection$app_productionGoogleRelease3.w(null);
                    }
                }
            }
        } else {
            this.streamRtcConnectionStore.updateFocusedParticipant(null);
            RtcConnection rtcConnection$app_productionGoogleRelease4 = StoreStream.Companion.getRtcConnection().getRtcConnection$app_productionGoogleRelease();
            if (rtcConnection$app_productionGoogleRelease4 != null) {
                rtcConnection$app_productionGoogleRelease4.w(null);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCallFullscreenViewModel(long j, StoreChannels storeChannels, StoreNavigation storeNavigation, StoreVoiceChannelSelected storeVoiceChannelSelected, StoreGuildSelected storeGuildSelected, StoreTabsNavigation storeTabsNavigation, StoreUserSettings storeUserSettings, StoreApplicationStreaming storeApplicationStreaming, StoreMediaEngine storeMediaEngine, StoreMediaSettings storeMediaSettings, StorePermissions storePermissions, Clock clock, Scheduler scheduler, VideoPermissionsManager videoPermissionsManager, VoiceEngineServiceController voiceEngineServiceController, StoreStreamRtcConnection storeStreamRtcConnection, StoreAudioManagerV2 storeAudioManagerV2, StoreMentions storeMentions, StoreAnalytics storeAnalytics, StoreConnectivity storeConnectivity, StoreStageChannels storeStageChannels, StoreAnalytics storeAnalytics2, StoreExperiments storeExperiments, StoreChannelsSelected storeChannelsSelected, StoreApplication storeApplication, StoreApplicationAssets storeApplicationAssets, String str) {
        super(ViewState.Uninitialized.INSTANCE);
        m.checkNotNullParameter(storeChannels, "storeChannels");
        m.checkNotNullParameter(storeNavigation, "storeNavigation");
        m.checkNotNullParameter(storeVoiceChannelSelected, "selectedVoiceChannelStore");
        m.checkNotNullParameter(storeGuildSelected, "guildSelectedStore");
        m.checkNotNullParameter(storeTabsNavigation, "tabsNavigationStore");
        m.checkNotNullParameter(storeUserSettings, "userSettingsStore");
        m.checkNotNullParameter(storeApplicationStreaming, "applicationStreamingStore");
        m.checkNotNullParameter(storeMediaEngine, "mediaEngineStore");
        m.checkNotNullParameter(storeMediaSettings, "mediaSettingsStore");
        m.checkNotNullParameter(storePermissions, "permissionsStore");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(scheduler, "backgroundThreadScheduler");
        m.checkNotNullParameter(videoPermissionsManager, "videoPermissionsManager");
        m.checkNotNullParameter(voiceEngineServiceController, "voiceEngineServiceController");
        m.checkNotNullParameter(storeStreamRtcConnection, "streamRtcConnectionStore");
        m.checkNotNullParameter(storeAudioManagerV2, "audioManagerStore");
        m.checkNotNullParameter(storeMentions, "mentionsStore");
        m.checkNotNullParameter(storeAnalytics, "analyticsStore");
        m.checkNotNullParameter(storeConnectivity, "connectivityStore");
        m.checkNotNullParameter(storeStageChannels, "stageStore");
        m.checkNotNullParameter(storeAnalytics2, "storeAnalytics");
        m.checkNotNullParameter(storeExperiments, "experimentsStore");
        m.checkNotNullParameter(storeChannelsSelected, "channelsSelectedStore");
        m.checkNotNullParameter(storeApplication, "applicationStore");
        m.checkNotNullParameter(storeApplicationAssets, "applicationAssetsStore");
        this.channelId = j;
        this.storeChannels = storeChannels;
        this.storeNavigation = storeNavigation;
        this.selectedVoiceChannelStore = storeVoiceChannelSelected;
        this.guildSelectedStore = storeGuildSelected;
        this.tabsNavigationStore = storeTabsNavigation;
        this.userSettingsStore = storeUserSettings;
        this.applicationStreamingStore = storeApplicationStreaming;
        this.mediaEngineStore = storeMediaEngine;
        this.mediaSettingsStore = storeMediaSettings;
        this.permissionsStore = storePermissions;
        this.clock = clock;
        this.backgroundThreadScheduler = scheduler;
        this.videoPermissionsManager = videoPermissionsManager;
        this.voiceEngineServiceController = voiceEngineServiceController;
        this.streamRtcConnectionStore = storeStreamRtcConnection;
        this.audioManagerStore = storeAudioManagerV2;
        this.mentionsStore = storeMentions;
        this.analyticsStore = storeAnalytics;
        this.connectivityStore = storeConnectivity;
        this.stageStore = storeStageChannels;
        this.storeAnalytics = storeAnalytics2;
        this.experimentsStore = storeExperiments;
        this.channelsSelectedStore = storeChannelsSelected;
        this.applicationStore = storeApplication;
        this.applicationAssetsStore = storeApplicationAssets;
        this.showStageCallBottomSheet = j != StoreStream.Companion.getVoiceChannelSelected().getSelectedVoiceChannelId();
        this.logStreamQualityIndicatorViewed = true;
        this.eventSubject = PublishSubject.k0();
        PublishSubject<List<VideoCallParticipantView.ParticipantData>> k0 = PublishSubject.k0();
        this.offScreenSubject = k0;
        this.videoPlayerIdleDetector = new VideoPlayerIdleDetector(0L, null, null, new WidgetCallFullscreenViewModel$videoPlayerIdleDetector$1(this), 7, null);
        this.allVideoParticipants = n.emptyList();
        if (str != null) {
            this.autotargetStreamKey = str;
            storeVoiceChannelSelected.selectVoiceChannel(ModelApplicationStream.Companion.decodeStreamKey(str).getChannelId());
        }
        Observable<List<VideoCallParticipantView.ParticipantData>> o = k0.o(1000L, TimeUnit.MILLISECONDS);
        m.checkNotNullExpressionValue(o, "offScreenSubject\n       …0, TimeUnit.MILLISECONDS)");
        ObservableExtensionsKt.appSubscribe(o, WidgetCallFullscreenViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1());
    }
}
