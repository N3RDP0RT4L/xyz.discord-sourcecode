package com.discord.widgets.voice.fullscreen.grid;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.core.view.DisplayCutoutCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.i.u1;
import b.d.b.a.a;
import com.discord.databinding.VideoCallGridItemEmbeddedActivityBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.views.PileView;
import com.discord.views.calls.VideoCallParticipantView;
import com.discord.widgets.voice.fullscreen.CallParticipant;
import com.discord.widgets.voice.fullscreen.grid.VideoCallGridViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: VideoCallGridAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 L2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002MLB\u008f\u0001\u0012\u0012\u0010/\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00100.\u0012\u0012\u00107\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00100.\u0012\u0016\u00103\u001a\u0012\u0012\b\u0012\u000601j\u0002`2\u0012\u0004\u0012\u00020\u00100.\u0012\u001e\u0010B\u001a\u001a\u0012\u0004\u0012\u00020?\u0012\u0006\u0012\u0004\u0018\u00010@\u0012\u0004\u0012\u00020\u00100>j\u0002`A\u0012\u001e\u0010F\u001a\u001a\u0012\u0004\u0012\u00020?\u0012\u0006\u0012\u0004\u0018\u00010@\u0012\u0004\u0012\u00020\u00100>j\u0002`A\u0012\u0006\u0010D\u001a\u00020\n¢\u0006\u0004\bJ\u0010KJ+\u0010\b\u001a\u00020\u00072\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\r\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u001b\u0010\u0011\u001a\u00020\u00102\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0004\b\u0011\u0010\u0012J\u001f\u0010\u0017\u001a\u00020\u00102\b\u0010\u0014\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0016\u001a\u00020\u0015¢\u0006\u0004\b\u0017\u0010\u0018J-\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\n2\u0006\u0010\u001b\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\n¢\u0006\u0004\b\u001d\u0010\u001eJ\u0015\u0010 \u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020\u0015¢\u0006\u0004\b \u0010!J\u0017\u0010\"\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\"\u0010#J\u001f\u0010'\u001a\u00020\u00022\u0006\u0010%\u001a\u00020$2\u0006\u0010&\u001a\u00020\nH\u0016¢\u0006\u0004\b'\u0010(J\u000f\u0010)\u001a\u00020\nH\u0016¢\u0006\u0004\b)\u0010*J\u001f\u0010,\u001a\u00020\u00102\u0006\u0010+\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b,\u0010-R\"\u0010/\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00100.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100R&\u00103\u001a\u0012\u0012\b\u0012\u000601j\u0002`2\u0012\u0004\u0012\u00020\u00100.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b3\u00100R\u0016\u00105\u001a\u0002048\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b5\u00106R\"\u00107\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00100.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b7\u00100R\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u00108R\u0019\u0010:\u001a\u0002098\u0006@\u0006¢\u0006\f\n\u0004\b:\u0010;\u001a\u0004\b<\u0010=R.\u0010B\u001a\u001a\u0012\u0004\u0012\u00020?\u0012\u0006\u0012\u0004\u0018\u00010@\u0012\u0004\u0012\u00020\u00100>j\u0002`A8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bB\u0010CR\u0016\u0010D\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bD\u0010ER.\u0010F\u001a\u001a\u0012\u0004\u0012\u00020?\u0012\u0006\u0012\u0004\u0018\u00010@\u0012\u0004\u0012\u00020\u00100>j\u0002`A8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010CR\u001c\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000f\u0010GR\u0016\u0010H\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bH\u0010ER\u0016\u0010\u001f\u001a\u00020\u00158\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001f\u0010I¨\u0006N"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;", "", "Lcom/discord/widgets/voice/fullscreen/CallParticipant;", "oldData", "newData", "Landroidx/recyclerview/widget/DiffUtil$Callback;", "getDiffUtilCallback", "(Ljava/util/List;Ljava/util/List;)Landroidx/recyclerview/widget/DiffUtil$Callback;", "", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "getItemId", "(I)J", "data", "", "setData", "(Ljava/util/List;)V", "Landroidx/core/view/DisplayCutoutCompat;", "displayCutout", "", "isLandscape", "setDisplayCutout", "(Landroidx/core/view/DisplayCutoutCompat;Z)V", "top", "bottom", "left", "right", "setInsetsForAvoidingCallUiOverlap", "(IIII)V", "controlsVisible", "notifyCallControlsVisibilityChanged", "(Z)V", "getItemViewType", "(I)I", "Landroid/view/ViewGroup;", "parent", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;", "getItemCount", "()I", "holder", "onBindViewHolder", "(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;I)V", "Lkotlin/Function1;", "onParticipantTapped", "Lkotlin/jvm/functions/Function1;", "", "Lcom/discord/primitives/StreamKey;", "onWatchStreamClicked", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;", "callUiInsets", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;", "onParticipantLongClicked", "Landroidx/core/view/DisplayCutoutCompat;", "Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "spanSizeLookup", "Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "getSpanSizeLookup", "()Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;", "Lkotlin/Function2;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamFps;", "Lcom/discord/views/calls/StreamQualityCallback;", "onStreamQualityIndicatorShown", "Lkotlin/jvm/functions/Function2;", "spanCount", "I", "onStreamQualityIndicatorClick", "Ljava/util/List;", "embeddedActivityParticipantsCount", "Z", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;I)V", "Companion", "CallUiInsets", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VideoCallGridAdapter extends RecyclerView.Adapter<VideoCallGridViewHolder> {
    public static final Companion Companion = new Companion(null);
    private static final int EMBEDDED_ACTIVITY_VIEW_TYPE = 1;
    private static final int USER_OR_STREAM_VIEW_TYPE = 0;
    private boolean controlsVisible;
    private DisplayCutoutCompat displayCutout;
    private int embeddedActivityParticipantsCount;
    private final Function1<CallParticipant, Unit> onParticipantLongClicked;
    private final Function1<CallParticipant, Unit> onParticipantTapped;
    private final Function2<VideoCallParticipantView.StreamResolution, VideoCallParticipantView.StreamFps, Unit> onStreamQualityIndicatorClick;
    private final Function2<VideoCallParticipantView.StreamResolution, VideoCallParticipantView.StreamFps, Unit> onStreamQualityIndicatorShown;
    private final Function1<String, Unit> onWatchStreamClicked;
    private final int spanCount;
    private List<? extends CallParticipant> data = n.emptyList();
    private CallUiInsets callUiInsets = new CallUiInsets(0, 0, 0, 0);
    private final GridLayoutManager.SpanSizeLookup spanSizeLookup = new GridLayoutManager.SpanSizeLookup() { // from class: com.discord.widgets.voice.fullscreen.grid.VideoCallGridAdapter$spanSizeLookup$1
        @Override // androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
        public int getSpanSize(int i) {
            List list;
            if (VideoCallGridAdapter.this.getItemCount() == 2) {
                return 2;
            }
            if (VideoCallGridAdapter.this.getItemCount() % 2 == 1) {
                list = VideoCallGridAdapter.this.data;
                if (i == n.getLastIndex(list)) {
                    return 2;
                }
            }
            return 1;
        }
    };

    /* compiled from: VideoCallGridAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0004J8\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0016\u001a\u0004\b\u0018\u0010\u0004R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0019\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;", "", "", "component1", "()I", "component2", "component3", "component4", "top", "bottom", "left", "right", "copy", "(IIII)Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getBottom", "getRight", "getTop", "getLeft", HookHelper.constructorName, "(IIII)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class CallUiInsets {
        private final int bottom;
        private final int left;
        private final int right;
        private final int top;

        public CallUiInsets(int i, int i2, int i3, int i4) {
            this.top = i;
            this.bottom = i2;
            this.left = i3;
            this.right = i4;
        }

        public static /* synthetic */ CallUiInsets copy$default(CallUiInsets callUiInsets, int i, int i2, int i3, int i4, int i5, Object obj) {
            if ((i5 & 1) != 0) {
                i = callUiInsets.top;
            }
            if ((i5 & 2) != 0) {
                i2 = callUiInsets.bottom;
            }
            if ((i5 & 4) != 0) {
                i3 = callUiInsets.left;
            }
            if ((i5 & 8) != 0) {
                i4 = callUiInsets.right;
            }
            return callUiInsets.copy(i, i2, i3, i4);
        }

        public final int component1() {
            return this.top;
        }

        public final int component2() {
            return this.bottom;
        }

        public final int component3() {
            return this.left;
        }

        public final int component4() {
            return this.right;
        }

        public final CallUiInsets copy(int i, int i2, int i3, int i4) {
            return new CallUiInsets(i, i2, i3, i4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CallUiInsets)) {
                return false;
            }
            CallUiInsets callUiInsets = (CallUiInsets) obj;
            return this.top == callUiInsets.top && this.bottom == callUiInsets.bottom && this.left == callUiInsets.left && this.right == callUiInsets.right;
        }

        public final int getBottom() {
            return this.bottom;
        }

        public final int getLeft() {
            return this.left;
        }

        public final int getRight() {
            return this.right;
        }

        public final int getTop() {
            return this.top;
        }

        public int hashCode() {
            return (((((this.top * 31) + this.bottom) * 31) + this.left) * 31) + this.right;
        }

        public String toString() {
            StringBuilder R = a.R("CallUiInsets(top=");
            R.append(this.top);
            R.append(", bottom=");
            R.append(this.bottom);
            R.append(", left=");
            R.append(this.left);
            R.append(", right=");
            return a.A(R, this.right, ")");
        }
    }

    /* compiled from: VideoCallGridAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$Companion;", "", "", "EMBEDDED_ACTIVITY_VIEW_TYPE", "I", "USER_OR_STREAM_VIEW_TYPE", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            VideoCallParticipantView.ParticipantData.Type.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[VideoCallParticipantView.ParticipantData.Type.DEFAULT.ordinal()] = 1;
            iArr[VideoCallParticipantView.ParticipantData.Type.APPLICATION_STREAMING.ordinal()] = 2;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public VideoCallGridAdapter(Function1<? super CallParticipant, Unit> function1, Function1<? super CallParticipant, Unit> function12, Function1<? super String, Unit> function13, Function2<? super VideoCallParticipantView.StreamResolution, ? super VideoCallParticipantView.StreamFps, Unit> function2, Function2<? super VideoCallParticipantView.StreamResolution, ? super VideoCallParticipantView.StreamFps, Unit> function22, int i) {
        m.checkNotNullParameter(function1, "onParticipantTapped");
        m.checkNotNullParameter(function12, "onParticipantLongClicked");
        m.checkNotNullParameter(function13, "onWatchStreamClicked");
        m.checkNotNullParameter(function2, "onStreamQualityIndicatorShown");
        m.checkNotNullParameter(function22, "onStreamQualityIndicatorClick");
        this.onParticipantTapped = function1;
        this.onParticipantLongClicked = function12;
        this.onWatchStreamClicked = function13;
        this.onStreamQualityIndicatorShown = function2;
        this.onStreamQualityIndicatorClick = function22;
        this.spanCount = i;
    }

    private final DiffUtil.Callback getDiffUtilCallback(final List<? extends CallParticipant> list, final List<? extends CallParticipant> list2) {
        return new DiffUtil.Callback() { // from class: com.discord.widgets.voice.fullscreen.grid.VideoCallGridAdapter$getDiffUtilCallback$1
            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public boolean areContentsTheSame(int i, int i2) {
                return m.areEqual((CallParticipant) list.get(i), (CallParticipant) list2.get(i2));
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public boolean areItemsTheSame(int i, int i2) {
                return m.areEqual(((CallParticipant) list.get(i)).getId(), ((CallParticipant) list2.get(i2)).getId());
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public int getNewListSize() {
                return list2.size();
            }

            @Override // androidx.recyclerview.widget.DiffUtil.Callback
            public int getOldListSize() {
                return list.size();
            }
        };
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        CallParticipant callParticipant = this.data.get(i);
        if (callParticipant instanceof CallParticipant.UserOrStreamParticipant) {
            VideoCallParticipantView.ParticipantData participantData = ((CallParticipant.UserOrStreamParticipant) callParticipant).getParticipantData();
            int ordinal = participantData.g.ordinal();
            if (ordinal == 0) {
                return participantData.f2812b.getUser().getId();
            }
            if (ordinal == 1) {
                return participantData.f2812b.getUser().getId() + 1;
            }
            throw new NoWhenBranchMatchedException();
        } else if (callParticipant instanceof CallParticipant.EmbeddedActivityParticipant) {
            return ((CallParticipant.EmbeddedActivityParticipant) callParticipant).getApplication().getId();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        CallParticipant callParticipant = this.data.get(i);
        if (callParticipant instanceof CallParticipant.UserOrStreamParticipant) {
            return 0;
        }
        if (callParticipant instanceof CallParticipant.EmbeddedActivityParticipant) {
            return 1;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final GridLayoutManager.SpanSizeLookup getSpanSizeLookup() {
        return this.spanSizeLookup;
    }

    public final void notifyCallControlsVisibilityChanged(boolean z2) {
        this.controlsVisible = z2;
        notifyDataSetChanged();
    }

    public final void setData(List<? extends CallParticipant> list) {
        m.checkNotNullParameter(list, "data");
        DiffUtil.DiffResult calculateDiff = DiffUtil.calculateDiff(getDiffUtilCallback(this.data, list), true);
        m.checkNotNullExpressionValue(calculateDiff, "DiffUtil.calculateDiff(g…a, newData = data), true)");
        this.data = list;
        calculateDiff.dispatchUpdatesTo(this);
        int i = 0;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (CallParticipant callParticipant : list) {
                if ((callParticipant instanceof CallParticipant.EmbeddedActivityParticipant) && (i = i + 1) < 0) {
                    n.throwCountOverflow();
                }
            }
        }
        this.embeddedActivityParticipantsCount = i;
    }

    public final void setDisplayCutout(DisplayCutoutCompat displayCutoutCompat, boolean z2) {
        this.displayCutout = displayCutoutCompat;
        if (z2) {
            notifyDataSetChanged();
        } else {
            notifyItemRangeChanged(0, this.spanCount);
        }
    }

    public final void setInsetsForAvoidingCallUiOverlap(int i, int i2, int i3, int i4) {
        this.callUiInsets = new CallUiInsets(i, i2, i3, i4);
        notifyDataSetChanged();
    }

    public void onBindViewHolder(VideoCallGridViewHolder videoCallGridViewHolder, int i) {
        m.checkNotNullParameter(videoCallGridViewHolder, "holder");
        if (videoCallGridViewHolder instanceof VideoCallGridViewHolder.UserOrStream) {
            VideoCallGridViewHolder.UserOrStream userOrStream = (VideoCallGridViewHolder.UserOrStream) videoCallGridViewHolder;
            CallParticipant callParticipant = this.data.get(i);
            Objects.requireNonNull(callParticipant, "null cannot be cast to non-null type com.discord.widgets.voice.fullscreen.CallParticipant.UserOrStreamParticipant");
            userOrStream.configure((CallParticipant.UserOrStreamParticipant) callParticipant, this.onParticipantTapped, this.onParticipantLongClicked, this.onWatchStreamClicked, this.displayCutout, this.spanCount, this.onStreamQualityIndicatorShown, this.onStreamQualityIndicatorClick, this.callUiInsets, this.controlsVisible);
        } else if (videoCallGridViewHolder instanceof VideoCallGridViewHolder.EmbeddedActivity) {
            CallParticipant callParticipant2 = this.data.get(i);
            Objects.requireNonNull(callParticipant2, "null cannot be cast to non-null type com.discord.widgets.voice.fullscreen.CallParticipant.EmbeddedActivityParticipant");
            ((VideoCallGridViewHolder.EmbeddedActivity) videoCallGridViewHolder).configure((CallParticipant.EmbeddedActivityParticipant) callParticipant2, this.onParticipantTapped);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public VideoCallGridViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            View inflate = from.inflate(R.layout.video_call_grid_item, viewGroup, false);
            Objects.requireNonNull(inflate, "rootView");
            VideoCallParticipantView videoCallParticipantView = (VideoCallParticipantView) inflate;
            m.checkNotNullExpressionValue(new u1(videoCallParticipantView), "VideoCallGridItemBinding…rent, false\n            )");
            m.checkNotNullExpressionValue(videoCallParticipantView, "VideoCallGridItemBinding… false\n            ).root");
            return new VideoCallGridViewHolder.UserOrStream(videoCallParticipantView);
        } else if (i == 1) {
            View inflate2 = from.inflate(R.layout.video_call_grid_item_embedded_activity, viewGroup, false);
            int i2 = R.id.activity_participant_avatars;
            PileView pileView = (PileView) inflate2.findViewById(R.id.activity_participant_avatars);
            if (pileView != null) {
                i2 = R.id.activity_preview_subtitle;
                TextView textView = (TextView) inflate2.findViewById(R.id.activity_preview_subtitle);
                if (textView != null) {
                    i2 = R.id.activity_preview_title;
                    TextView textView2 = (TextView) inflate2.findViewById(R.id.activity_preview_title);
                    if (textView2 != null) {
                        i2 = R.id.app_background_image;
                        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate2.findViewById(R.id.app_background_image);
                        if (simpleDraweeView != null) {
                            i2 = R.id.app_background_overlay;
                            View findViewById = inflate2.findViewById(R.id.app_background_overlay);
                            if (findViewById != null) {
                                VideoCallGridItemEmbeddedActivityBinding videoCallGridItemEmbeddedActivityBinding = new VideoCallGridItemEmbeddedActivityBinding((FrameLayout) inflate2, pileView, textView, textView2, simpleDraweeView, findViewById);
                                m.checkNotNullExpressionValue(videoCallGridItemEmbeddedActivityBinding, "VideoCallGridItemEmbedde…tInflater, parent, false)");
                                return new VideoCallGridViewHolder.EmbeddedActivity(videoCallGridItemEmbeddedActivityBinding);
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(i2)));
        } else {
            throw new IllegalStateException(a.p("invalid view type: ", i));
        }
    }
}
