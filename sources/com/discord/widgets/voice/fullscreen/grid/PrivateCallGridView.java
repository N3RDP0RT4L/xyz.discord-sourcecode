package com.discord.widgets.voice.fullscreen.grid;

import andhook.lib.HookHelper;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.core.view.ViewGroupKt;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.collections.CollectionExtensionsKt;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.views.VoiceUserView;
import com.google.android.material.badge.BadgeDrawable;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: PrivateCallGridView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 /2\u00020\u0001:\u0001/B\u0011\b\u0016\u0012\u0006\u0010)\u001a\u00020(¢\u0006\u0004\b*\u0010+B\u001d\b\u0016\u0012\u0006\u0010)\u001a\u00020(\u0012\n\b\u0002\u0010-\u001a\u0004\u0018\u00010,¢\u0006\u0004\b*\u0010.J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u000f\u0010\n\u001a\u00020\tH\u0002¢\u0006\u0004\b\n\u0010\u000bJ#\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0011\u001a\u00020\u00102\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J'\u0010\u0016\u001a\u0004\u0018\u00010\u00152\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\f2\u0006\u0010\u0014\u001a\u00020\u0013H\u0003¢\u0006\u0004\b\u0016\u0010\u0017J'\u0010\u0018\u001a\u0004\u0018\u00010\u00152\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\f2\u0006\u0010\u0014\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0018\u0010\u0017J\u000f\u0010\u0019\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u000f\u0010\u001b\u001a\u00020\u0013H\u0002¢\u0006\u0004\b\u001b\u0010\u001aJ\u0013\u0010\u001d\u001a\u00020\u0010*\u00020\u001cH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u001b\u0010\u001f\u001a\u00020\t2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\f¢\u0006\u0004\b\u001f\u0010 R\u001c\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\r\u0010!R:\u0010&\u001a&\u0012\b\u0012\u00060#j\u0002`$\u0012\u0004\u0012\u00020\u00040\"j\u0012\u0012\b\u0012\u00060#j\u0002`$\u0012\u0004\u0012\u00020\u0004`%8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u0010'¨\u00060"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/grid/PrivateCallGridView;", "Landroid/widget/TableLayout;", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "callUserItem", "Lcom/discord/views/VoiceUserView;", "getAndUpdateCallUserView", "(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)Lcom/discord/views/VoiceUserView;", "createCallUserView", "()Lcom/discord/views/VoiceUserView;", "", "removeViewsAndSubviews", "()V", "", "callUsers", "getVisibleCallUsers", "(Ljava/util/List;)Ljava/util/List;", "", "isOverflowingParticipants", "(Ljava/util/List;)Z", "", "rowIndex", "Landroid/view/View;", "getOverflowParticipantView", "(Ljava/util/List;I)Landroid/view/View;", "getOneOnOneSpacerView", "getColumnSize", "()I", "getMaxShownParticipants", "Landroid/content/res/Resources;", "isLandscape", "(Landroid/content/res/Resources;)Z", "configure", "(Ljava/util/List;)V", "Ljava/util/List;", "Ljava/util/LinkedHashMap;", "", "Lcom/discord/primitives/UserId;", "Lkotlin/collections/LinkedHashMap;", "callUserViews", "Ljava/util/LinkedHashMap;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class PrivateCallGridView extends TableLayout {
    private static final Companion Companion = new Companion(null);
    @Deprecated
    private static final int PARTICIPANT_AVATAR_SIZE_DP = 92;
    @Deprecated
    private static final int PARTICIPANT_MARGIN = 2;
    @Deprecated
    private static final int PARTICIPANT_RING_MARGIN = 6;
    private final LinkedHashMap<Long, VoiceUserView> callUserViews;
    private List<StoreVoiceParticipants.VoiceUser> callUsers;

    /* compiled from: PrivateCallGridView.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0082\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\u0003\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004R\u0016\u0010\u0006\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0006\u0010\u0004¨\u0006\t"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/grid/PrivateCallGridView$Companion;", "", "", "PARTICIPANT_AVATAR_SIZE_DP", "I", "PARTICIPANT_MARGIN", "PARTICIPANT_RING_MARGIN", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PrivateCallGridView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        this.callUsers = n.emptyList();
        this.callUserViews = new LinkedHashMap<>();
    }

    private final VoiceUserView createCallUserView() {
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        VoiceUserView voiceUserView = new VoiceUserView(context, null, 0, 6);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams();
        int dpToPixels = DimenUtils.dpToPixels(2);
        layoutParams.setMargins(dpToPixels, dpToPixels, dpToPixels, dpToPixels);
        voiceUserView.setLayoutParams(layoutParams);
        voiceUserView.setAnimateAvatarWhenRinging(true);
        voiceUserView.setFadeWhenDisconnected(true);
        voiceUserView.setAvatarSize(DimenUtils.dpToPixels(92));
        voiceUserView.setRingMargin(DimenUtils.dpToPixels(6));
        return voiceUserView;
    }

    private final VoiceUserView getAndUpdateCallUserView(StoreVoiceParticipants.VoiceUser voiceUser) {
        LinkedHashMap<Long, VoiceUserView> linkedHashMap = this.callUserViews;
        Long valueOf = Long.valueOf(voiceUser.getUser().getId());
        VoiceUserView voiceUserView = linkedHashMap.get(valueOf);
        if (voiceUserView == null) {
            voiceUserView = createCallUserView();
            linkedHashMap.put(valueOf, voiceUserView);
        }
        VoiceUserView voiceUserView2 = voiceUserView;
        int i = VoiceUserView.j;
        voiceUserView2.a(voiceUser, R.dimen.avatar_size_unrestricted);
        voiceUserView2.setSelected(false);
        return voiceUserView2;
    }

    private final int getColumnSize() {
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        return isLandscape(resources) ? 6 : 3;
    }

    private final int getMaxShownParticipants() {
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        return isLandscape(resources) ? 6 : 9;
    }

    private final View getOneOnOneSpacerView(List<StoreVoiceParticipants.VoiceUser> list, int i) {
        if (!(i == 0 && list.size() == 2)) {
            return null;
        }
        View inflate = TableLayout.inflate(getContext(), R.layout.view_private_call_grid_spacer, null);
        Objects.requireNonNull(inflate, "null cannot be cast to non-null type android.widget.TextView");
        return (TextView) inflate;
    }

    @SuppressLint({"SetTextI18n"})
    private final View getOverflowParticipantView(List<StoreVoiceParticipants.VoiceUser> list, int i) {
        boolean z2 = true;
        if (i != ((int) Math.ceil(getMaxShownParticipants() / getColumnSize())) - 1) {
            z2 = false;
        }
        if (!z2 || !isOverflowingParticipants(list)) {
            return null;
        }
        View inflate = TableLayout.inflate(getContext(), R.layout.view_private_call_grid_overflow, null);
        Objects.requireNonNull(inflate, "null cannot be cast to non-null type android.widget.TextView");
        TextView textView = (TextView) inflate;
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(DimenUtils.dpToPixels(92), DimenUtils.dpToPixels(92));
        int dpToPixels = DimenUtils.dpToPixels(2) * 4;
        layoutParams.setMargins(dpToPixels, dpToPixels, dpToPixels, dpToPixels);
        textView.setLayoutParams(layoutParams);
        textView.setText(BadgeDrawable.DEFAULT_EXCEED_MAX_BADGE_NUMBER_SUFFIX + String.valueOf(list.size() - getMaxShownParticipants()));
        return textView;
    }

    private final List<StoreVoiceParticipants.VoiceUser> getVisibleCallUsers(List<StoreVoiceParticipants.VoiceUser> list) {
        return isOverflowingParticipants(list) ? u.take(list, getMaxShownParticipants() - 1) : list;
    }

    private final boolean isLandscape(Resources resources) {
        return resources.getConfiguration().orientation == 2;
    }

    private final boolean isOverflowingParticipants(List<StoreVoiceParticipants.VoiceUser> list) {
        return list.size() > getMaxShownParticipants();
    }

    private final void removeViewsAndSubviews() {
        for (View view : ViewGroupKt.getChildren(this)) {
            Objects.requireNonNull(view, "null cannot be cast to non-null type android.widget.TableRow");
            ((TableRow) view).removeAllViews();
        }
        removeAllViews();
    }

    public final void configure(List<StoreVoiceParticipants.VoiceUser> list) {
        m.checkNotNullParameter(list, "callUsers");
        if (!CollectionExtensionsKt.equals(this.callUsers, list, PrivateCallGridView$configure$callUserOrderChanged$1.INSTANCE)) {
            removeViewsAndSubviews();
            int i = 0;
            for (Object obj : u.chunked(getVisibleCallUsers(list), getColumnSize())) {
                i++;
                if (i < 0) {
                    n.throwIndexOverflow();
                }
                TableRow tableRow = new TableRow(getContext());
                tableRow.setGravity(17);
                for (StoreVoiceParticipants.VoiceUser voiceUser : (List) obj) {
                    tableRow.addView(getAndUpdateCallUserView(voiceUser));
                }
                View overflowParticipantView = getOverflowParticipantView(list, i);
                if (overflowParticipantView != null) {
                    tableRow.addView(overflowParticipantView);
                }
                View oneOnOneSpacerView = getOneOnOneSpacerView(list, i);
                if (oneOnOneSpacerView != null) {
                    tableRow.addView(oneOnOneSpacerView, 1);
                }
                addView(tableRow);
            }
        } else {
            for (StoreVoiceParticipants.VoiceUser voiceUser2 : getVisibleCallUsers(list)) {
                getAndUpdateCallUserView(voiceUser2);
            }
        }
        this.callUsers = list;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public PrivateCallGridView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        this.callUsers = n.emptyList();
        this.callUserViews = new LinkedHashMap<>();
    }

    public /* synthetic */ PrivateCallGridView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }
}
