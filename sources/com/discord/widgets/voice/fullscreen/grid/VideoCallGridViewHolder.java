package com.discord.widgets.voice.fullscreen.grid;

import andhook.lib.HookHelper;
import android.content.res.Resources;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.core.view.DisplayCutoutCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.a.k.b;
import b.a.y.a0;
import com.discord.api.application.ApplicationAsset;
import com.discord.databinding.VideoCallGridItemEmbeddedActivityBinding;
import com.discord.models.guild.UserGuildMember;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.icon.IconUtils;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.PileView;
import com.discord.views.calls.VideoCallParticipantView;
import com.discord.widgets.voice.fullscreen.CallParticipant;
import com.discord.widgets.voice.fullscreen.grid.VideoCallGridAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: VideoCallGridViewHolder.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0006\u0007B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005\u0082\u0001\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View;", "itemView", HookHelper.constructorName, "(Landroid/view/View;)V", "EmbeddedActivity", "UserOrStream", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder$UserOrStream;", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder$EmbeddedActivity;", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public abstract class VideoCallGridViewHolder extends RecyclerView.ViewHolder {

    /* compiled from: VideoCallGridViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0012\u0010\u0013J)\u0010\b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004¢\u0006\u0004\b\b\u0010\tR\u0016\u0010\u000b\u001a\u00020\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0019\u0010\u000e\u001a\u00020\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder$EmbeddedActivity;", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;", "Lcom/discord/widgets/voice/fullscreen/CallParticipant$EmbeddedActivityParticipant;", "callParticipant", "Lkotlin/Function1;", "Lcom/discord/widgets/voice/fullscreen/CallParticipant;", "", "onTapped", "configure", "(Lcom/discord/widgets/voice/fullscreen/CallParticipant$EmbeddedActivityParticipant;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "distinctChangeDetector", "Lcom/discord/utilities/images/MGImages$DistinctChangeDetector;", "Lcom/discord/databinding/VideoCallGridItemEmbeddedActivityBinding;", "binding", "Lcom/discord/databinding/VideoCallGridItemEmbeddedActivityBinding;", "getBinding", "()Lcom/discord/databinding/VideoCallGridItemEmbeddedActivityBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/VideoCallGridItemEmbeddedActivityBinding;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class EmbeddedActivity extends VideoCallGridViewHolder {
        private final VideoCallGridItemEmbeddedActivityBinding binding;
        private final MGImages.DistinctChangeDetector distinctChangeDetector = new MGImages.DistinctChangeDetector();

        /* JADX WARN: Illegal instructions before constructor call */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public EmbeddedActivity(com.discord.databinding.VideoCallGridItemEmbeddedActivityBinding r3) {
            /*
                r2 = this;
                java.lang.String r0 = "binding"
                d0.z.d.m.checkNotNullParameter(r3, r0)
                android.widget.FrameLayout r0 = r3.a
                java.lang.String r1 = "binding.root"
                d0.z.d.m.checkNotNullExpressionValue(r0, r1)
                r1 = 0
                r2.<init>(r0, r1)
                r2.binding = r3
                com.discord.utilities.images.MGImages$DistinctChangeDetector r3 = new com.discord.utilities.images.MGImages$DistinctChangeDetector
                r3.<init>()
                r2.distinctChangeDetector = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.grid.VideoCallGridViewHolder.EmbeddedActivity.<init>(com.discord.databinding.VideoCallGridItemEmbeddedActivityBinding):void");
        }

        public final void configure(final CallParticipant.EmbeddedActivityParticipant embeddedActivityParticipant, final Function1<? super CallParticipant, Unit> function1) {
            m.checkNotNullParameter(embeddedActivityParticipant, "callParticipant");
            m.checkNotNullParameter(function1, "onTapped");
            ApplicationAsset backgroundAsset = embeddedActivityParticipant.getBackgroundAsset();
            String assetImage = backgroundAsset != null ? IconUtils.INSTANCE.getAssetImage(Long.valueOf(embeddedActivityParticipant.getApplication().getId()), String.valueOf(backgroundAsset.a()), 1024) : null;
            SimpleDraweeView simpleDraweeView = this.binding.d;
            m.checkNotNullExpressionValue(simpleDraweeView, "binding.appBackgroundImage");
            boolean z2 = true;
            int i = 8;
            simpleDraweeView.setVisibility(assetImage != null ? 0 : 8);
            View view = this.binding.e;
            m.checkNotNullExpressionValue(view, "binding.appBackgroundOverlay");
            if (assetImage == null) {
                z2 = false;
            }
            if (z2) {
                i = 0;
            }
            view.setVisibility(i);
            if (assetImage != null) {
                MGImages mGImages = MGImages.INSTANCE;
                SimpleDraweeView simpleDraweeView2 = this.binding.d;
                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.appBackgroundImage");
                Uri parse = Uri.parse(assetImage);
                m.checkNotNullExpressionValue(parse, "Uri.parse(backgroundAssetUrl)");
                mGImages.setImage(simpleDraweeView2, parse, this.distinctChangeDetector);
            }
            this.binding.a.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.fullscreen.grid.VideoCallGridViewHolder$EmbeddedActivity$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    Function1.this.invoke(embeddedActivityParticipant);
                }
            });
            FrameLayout frameLayout = this.binding.a;
            m.checkNotNullExpressionValue(frameLayout, "binding.root");
            Resources resources = frameLayout.getResources();
            TextView textView = this.binding.c;
            m.checkNotNullExpressionValue(textView, "binding.activityPreviewTitle");
            CharSequence name = embeddedActivityParticipant.getEmbeddedActivity().getName();
            if (name == null) {
                m.checkNotNullExpressionValue(resources, "resources");
                name = b.c(resources, R.string.embedded_activities_unknown_activity_name, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
            }
            textView.setText(name);
            List<UserGuildMember> participantsInActivity = embeddedActivityParticipant.getParticipantsInActivity();
            int dpToPixels = DimenUtils.dpToPixels(16);
            m.checkNotNullParameter(participantsInActivity, "userGuildMembers");
            ArrayList<String> arrayList = new ArrayList(o.collectionSizeOrDefault(participantsInActivity, 10));
            for (UserGuildMember userGuildMember : participantsInActivity) {
                arrayList.add(IconUtils.INSTANCE.getForGuildMemberOrUser(userGuildMember.getUser(), userGuildMember.getGuildMember(), Integer.valueOf(dpToPixels), false));
            }
            ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(arrayList, 10));
            for (String str : arrayList) {
                arrayList2.add(new PileView.c(new a0(str), null));
            }
            this.binding.f2152b.setItems(arrayList2);
        }

        public final VideoCallGridItemEmbeddedActivityBinding getBinding() {
            return this.binding;
        }
    }

    /* compiled from: VideoCallGridViewHolder.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001f\u0010 J·\u0001\u0010\u001a\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0016\u0010\u000b\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u00060\u00042\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u001e\u0010\u0014\u001a\u001a\u0012\u0004\u0012\u00020\u0011\u0012\u0006\u0012\u0004\u0018\u00010\u0012\u0012\u0004\u0012\u00020\u00060\u0010j\u0002`\u00132\u001e\u0010\u0015\u001a\u001a\u0012\u0004\u0012\u00020\u0011\u0012\u0006\u0012\u0004\u0018\u00010\u0012\u0012\u0004\u0012\u00020\u00060\u0010j\u0002`\u00132\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0018¢\u0006\u0004\b\u001a\u0010\u001bR\u0016\u0010\u001d\u001a\u00020\u001c8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\u001e¨\u0006!"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder$UserOrStream;", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;", "Lcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;", "callParticipant", "Lkotlin/Function1;", "Lcom/discord/widgets/voice/fullscreen/CallParticipant;", "", "onTapped", "onLongClicked", "", "Lcom/discord/primitives/StreamKey;", "onWatchStreamClicked", "Landroidx/core/view/DisplayCutoutCompat;", "displayCutout", "", "spanCount", "Lkotlin/Function2;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamResolution;", "Lcom/discord/views/calls/VideoCallParticipantView$StreamFps;", "Lcom/discord/views/calls/StreamQualityCallback;", "onStreamQualityIndicatorShown", "onStreamQualityIndicatorClicked", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;", "callUiInsets", "", "controlsVisible", "configure", "(Lcom/discord/widgets/voice/fullscreen/CallParticipant$UserOrStreamParticipant;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Landroidx/core/view/DisplayCutoutCompat;ILkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$CallUiInsets;Z)V", "Lcom/discord/views/calls/VideoCallParticipantView;", "videoCallParticipantView", "Lcom/discord/views/calls/VideoCallParticipantView;", HookHelper.constructorName, "(Lcom/discord/views/calls/VideoCallParticipantView;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class UserOrStream extends VideoCallGridViewHolder {
        private final VideoCallParticipantView videoCallParticipantView;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public UserOrStream(VideoCallParticipantView videoCallParticipantView) {
            super(videoCallParticipantView, null);
            m.checkNotNullParameter(videoCallParticipantView, "videoCallParticipantView");
            this.videoCallParticipantView = videoCallParticipantView;
        }

        public final void configure(final CallParticipant.UserOrStreamParticipant userOrStreamParticipant, final Function1<? super CallParticipant, Unit> function1, Function1<? super CallParticipant, Unit> function12, Function1<? super String, Unit> function13, DisplayCutoutCompat displayCutoutCompat, int i, Function2<? super VideoCallParticipantView.StreamResolution, ? super VideoCallParticipantView.StreamFps, Unit> function2, Function2<? super VideoCallParticipantView.StreamResolution, ? super VideoCallParticipantView.StreamFps, Unit> function22, VideoCallGridAdapter.CallUiInsets callUiInsets, boolean z2) {
            m.checkNotNullParameter(userOrStreamParticipant, "callParticipant");
            m.checkNotNullParameter(function1, "onTapped");
            m.checkNotNullParameter(function12, "onLongClicked");
            m.checkNotNullParameter(function13, "onWatchStreamClicked");
            m.checkNotNullParameter(function2, "onStreamQualityIndicatorShown");
            m.checkNotNullParameter(function22, "onStreamQualityIndicatorClicked");
            m.checkNotNullParameter(callUiInsets, "callUiInsets");
            View view = this.itemView;
            m.checkNotNullExpressionValue(view, "itemView");
            Resources resources = view.getResources();
            m.checkNotNullExpressionValue(resources, "itemView.resources");
            this.videoCallParticipantView.c(userOrStreamParticipant.getParticipantData(), displayCutoutCompat, resources.getConfiguration().orientation != 2 ? getAdapterPosition() < i : getAdapterPosition() % i == 0, callUiInsets, z2);
            this.videoCallParticipantView.setOnWatchStreamClicked(function13);
            this.videoCallParticipantView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.fullscreen.grid.VideoCallGridViewHolder$UserOrStream$configure$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    Function1.this.invoke(userOrStreamParticipant);
                }
            });
            ViewExtensions.setOnLongClickListenerConsumeClick(this.videoCallParticipantView, new VideoCallGridViewHolder$UserOrStream$configure$2(function12, userOrStreamParticipant));
            VideoCallParticipantView videoCallParticipantView = this.videoCallParticipantView;
            Objects.requireNonNull(videoCallParticipantView);
            m.checkNotNullParameter(function2, "onShown");
            m.checkNotNullParameter(function22, "onClicked");
            videoCallParticipantView.f2811y = function2;
            videoCallParticipantView.f2810x = function22;
        }
    }

    private VideoCallGridViewHolder(View view) {
        super(view);
    }

    public /* synthetic */ VideoCallGridViewHolder(View view, DefaultConstructorMarker defaultConstructorMarker) {
        this(view);
    }
}
