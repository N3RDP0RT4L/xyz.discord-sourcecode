package com.discord.widgets.voice.fullscreen;

import android.widget.TextView;
import com.discord.databinding.WidgetCallFullscreenBinding;
import com.discord.utilities.time.TimeUtils;
import com.discord.widgets.voice.model.CallModel;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetCallFullscreen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Ljava/lang/Long;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallFullscreen$configureConnectionStatusText$1 extends o implements Function1<Long, Unit> {
    public final /* synthetic */ CallModel $callModel;
    public final /* synthetic */ WidgetCallFullscreen this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCallFullscreen$configureConnectionStatusText$1(WidgetCallFullscreen widgetCallFullscreen, CallModel callModel) {
        super(1);
        this.this$0 = widgetCallFullscreen;
        this.$callModel = callModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke2(l);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Long l) {
        WidgetCallFullscreenBinding binding;
        binding = this.this$0.getBinding();
        TextView textView = binding.f2229s.g;
        m.checkNotNullExpressionValue(textView, "binding.privateCall.privateCallStatusDuration");
        textView.setText(TimeUtils.toFriendlyString$default(TimeUtils.INSTANCE, this.$callModel.getTimeConnectedMs(), 0L, null, null, 14, null));
    }
}
