package com.discord.widgets.voice.fullscreen;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.DisplayCutoutCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewKt;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import b.a.a.j;
import b.a.a.m;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.i;
import b.a.i.j;
import b.a.j.a;
import b.a.v.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.permission.Permission;
import com.discord.app.AppActivity;
import com.discord.app.AppFragment;
import com.discord.app.AppLog;
import com.discord.app.AppTransitionActivity;
import com.discord.databinding.WidgetCallFullscreenBinding;
import com.discord.floating_view_manager.FloatingViewGravity;
import com.discord.models.guild.Guild;
import com.discord.models.user.User;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.tooltips.DefaultTooltipCreator;
import com.discord.tooltips.TooltipManager;
import com.discord.utilities.KotlinExtensionsKt;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.device.DeviceUtils;
import com.discord.utilities.dimen.DimenUtils;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.mg_recycler.MGRecyclerAdapter;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.press.OnPressListener;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.streams.StreamContext;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.viewstub.LazyViewStubDelegate;
import com.discord.utilities.voice.DiscordOverlayService;
import com.discord.views.calls.VideoCallParticipantView;
import com.discord.widgets.guildscheduledevent.GuildEventPromptListView;
import com.discord.widgets.notice.WidgetNoticeDialog;
import com.discord.widgets.settings.premium.WidgetSettingsPremium;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.stage.start.StageEventsGuildsFeatureFlag;
import com.discord.widgets.status.WidgetGlobalStatusIndicatorState;
import com.discord.widgets.user.usersheet.WidgetUserSheet;
import com.discord.widgets.voice.controls.VoiceControlsSheetSwipeTooltip;
import com.discord.widgets.voice.controls.VoiceControlsSheetView;
import com.discord.widgets.voice.feedback.call.CallFeedbackSheetNavigator;
import com.discord.widgets.voice.feedback.stream.StreamFeedbackSheetNavigator;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel;
import com.discord.widgets.voice.fullscreen.grid.VideoCallGridAdapter;
import com.discord.widgets.voice.fullscreen.grid.VideoCallGridLayoutManager;
import com.discord.widgets.voice.fullscreen.stage.StageCallAdapter;
import com.discord.widgets.voice.fullscreen.stage.StageCallBottomSheetManager;
import com.discord.widgets.voice.fullscreen.stage.StageCallItem;
import com.discord.widgets.voice.fullscreen.stage.StageCallParticipantItem;
import com.discord.widgets.voice.model.CallModel;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import com.discord.widgets.voice.sheet.WidgetVoiceSettingsBottomSheet;
import com.discord.widgets.voice.stream.StreamNavigator;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import d0.t.c0;
import d0.t.n;
import d0.t.u;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetCallFullscreen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0091\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0005*\u0001h\u0018\u0000 \u009c\u00012\u00020\u0001:\u0002\u009c\u0001B\b¢\u0006\u0005\b\u009b\u0001\u0010\u0010J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\f\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u000e\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u000e\u0010\rJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0011\u0010\u0010J\u0017\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0016\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0016\u0010\rJ\u0017\u0010\u0017\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0017\u0010\rJ\u0017\u0010\u0018\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0018\u0010\rJ\u0017\u0010\u0019\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\u0019\u0010\rJ\u0017\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u001eH\u0002¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\"\u0010\u0010J!\u0010%\u001a\u00020\u00042\b\b\u0002\u0010#\u001a\u00020\u00122\u0006\u0010$\u001a\u00020\u0012H\u0002¢\u0006\u0004\b%\u0010&J\u0017\u0010'\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u000bH\u0002¢\u0006\u0004\b'\u0010\rJ\u0017\u0010(\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u000bH\u0002¢\u0006\u0004\b(\u0010\rJ\u000f\u0010)\u001a\u00020\u0004H\u0002¢\u0006\u0004\b)\u0010\u0010J\u000f\u0010*\u001a\u00020\u0004H\u0002¢\u0006\u0004\b*\u0010\u0010J\u001d\u0010/\u001a\b\u0012\u0004\u0012\u00020.0-2\u0006\u0010,\u001a\u00020+H\u0002¢\u0006\u0004\b/\u00100J\u0017\u00101\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b1\u0010\u0006J\u000f\u00102\u001a\u00020\u0004H\u0002¢\u0006\u0004\b2\u0010\u0010J\u000f\u00103\u001a\u00020\u0004H\u0002¢\u0006\u0004\b3\u0010\u0010J\u000f\u00104\u001a\u00020\u0004H\u0002¢\u0006\u0004\b4\u0010\u0010J\u000f\u00105\u001a\u00020\u0004H\u0002¢\u0006\u0004\b5\u0010\u0010J\u000f\u00106\u001a\u00020\u0004H\u0002¢\u0006\u0004\b6\u0010\u0010J\u000f\u00107\u001a\u00020\u0004H\u0002¢\u0006\u0004\b7\u0010\u0010J\u000f\u00108\u001a\u00020\u0004H\u0002¢\u0006\u0004\b8\u0010\u0010J\u0017\u0010;\u001a\u00020\u00042\u0006\u0010:\u001a\u000209H\u0002¢\u0006\u0004\b;\u0010<J\u000f\u0010=\u001a\u00020\u0004H\u0002¢\u0006\u0004\b=\u0010\u0010J\u000f\u0010>\u001a\u00020\u0004H\u0002¢\u0006\u0004\b>\u0010\u0010J\u001b\u0010B\u001a\u00020\u00122\n\u0010A\u001a\u00060?j\u0002`@H\u0002¢\u0006\u0004\bB\u0010CJ\u0013\u0010E\u001a\u00060?j\u0002`DH\u0002¢\u0006\u0004\bE\u0010FJ\u000f\u0010G\u001a\u00020\u0012H\u0002¢\u0006\u0004\bG\u0010HJ\u000f\u0010I\u001a\u00020\u0004H\u0016¢\u0006\u0004\bI\u0010\u0010J-\u0010P\u001a\u0004\u0018\u00010O2\u0006\u0010K\u001a\u00020J2\b\u0010L\u001a\u0004\u0018\u00010\u001e2\b\u0010N\u001a\u0004\u0018\u00010MH\u0016¢\u0006\u0004\bP\u0010QJ\u0017\u0010S\u001a\u00020\u00042\u0006\u0010R\u001a\u00020OH\u0016¢\u0006\u0004\bS\u0010TJ\u000f\u0010U\u001a\u00020\u0004H\u0016¢\u0006\u0004\bU\u0010\u0010J\u000f\u0010V\u001a\u00020\u0004H\u0016¢\u0006\u0004\bV\u0010\u0010J)\u0010\\\u001a\u00020\u00042\u0006\u0010X\u001a\u00020W2\u0006\u0010Y\u001a\u00020W2\b\u0010[\u001a\u0004\u0018\u00010ZH\u0016¢\u0006\u0004\b\\\u0010]J\u0015\u0010_\u001a\u00020\u00042\u0006\u0010:\u001a\u00020^¢\u0006\u0004\b_\u0010`R\u001c\u0010c\u001a\b\u0012\u0004\u0012\u00020b0a8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bc\u0010dR\u0016\u0010f\u001a\u00020e8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\bf\u0010gR\u0016\u0010i\u001a\u00020h8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010jR\u0016\u0010l\u001a\u00020k8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bl\u0010mR\u0018\u0010o\u001a\u0004\u0018\u00010n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bo\u0010pR\u0016\u0010r\u001a\u00020q8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\br\u0010sR\u0016\u0010t\u001a\u00020\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bt\u0010uR\u001d\u0010{\u001a\u00020v8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bw\u0010x\u001a\u0004\by\u0010zR\u0018\u0010|\u001a\u0004\u0018\u00010W8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b|\u0010}R\u0017\u0010\u007f\u001a\u00020~8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u007f\u0010\u0080\u0001R\u001a\u0010\u0081\u0001\u001a\u0004\u0018\u00010W8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u0081\u0001\u0010}R\"\u0010\u0086\u0001\u001a\u00030\u0082\u00018B@\u0002X\u0082\u0084\u0002¢\u0006\u000f\n\u0005\b\u0083\u0001\u0010x\u001a\u0006\b\u0084\u0001\u0010\u0085\u0001R!\u0010\u0003\u001a\u00020\u00028B@\u0002X\u0082\u0084\u0002¢\u0006\u0010\n\u0006\b\u0087\u0001\u0010\u0088\u0001\u001a\u0006\b\u0089\u0001\u0010\u008a\u0001R\u001c\u0010\u008c\u0001\u001a\u0005\u0018\u00010\u008b\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u008c\u0001\u0010\u008d\u0001R\u001a\u0010\u008f\u0001\u001a\u00030\u008e\u00018\u0002@\u0002X\u0082.¢\u0006\b\n\u0006\b\u008f\u0001\u0010\u0090\u0001R\u001a\u0010\u0092\u0001\u001a\u00030\u0091\u00018\u0002@\u0002X\u0082.¢\u0006\b\n\u0006\b\u0092\u0001\u0010\u0093\u0001R\u001a\u0010\u0095\u0001\u001a\u00030\u0094\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0095\u0001\u0010\u0096\u0001R#\u0010\u0099\u0001\u001a\f\u0012\u0005\u0012\u00030\u0098\u0001\u0018\u00010\u0097\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u0099\u0001\u0010\u009a\u0001¨\u0006\u009d\u0001"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;", "Lcom/discord/app/AppFragment;", "Lcom/discord/databinding/WidgetCallFullscreenBinding;", "binding", "", "onViewBindingDestroy", "(Lcom/discord/databinding/WidgetCallFullscreenBinding;)V", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;", "viewState", "configureUI", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;", "configureValidUI", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V", "configureBottomControls", "showControls", "()V", "hideControls", "", "showTooltip", "configureSwipeTooltip", "(Z)V", "configureStageUi", "configureEventPrompt", "configureGridUi", "configurePrivateCallParticipantsUi", "Lcom/discord/widgets/voice/model/CallModel;", "callModel", "configureConnectionStatusText", "(Lcom/discord/widgets/voice/model/CallModel;)V", "Landroid/view/ViewGroup;", "viewGroup", "initializeSystemUiListeners", "(Landroid/view/ViewGroup;)V", "transitionActivity", "disconnect", "transition", "finishActivity", "(ZZ)V", "configureActionBar", "configureMenu", "setUpStageRecycler", "setUpGridRecycler", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;", "layoutManager", "", "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;", "getVisibleParticipants", "(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;)Ljava/util/List;", "destroyAllRenderers", "enableWakeLock", "showSuppressedDialog", "showServerMutedDialog", "showServerDeafenedDialog", "showNoVideoPermissionDialog", "showNoVadPermissionDialog", "showNoScreenSharePermissionDialog", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$OnIdleStateChanged;", "event", "handleIdleStateChanged", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$OnIdleStateChanged;)V", "collapseBottomSheet", "setVoiceControlsSheetPeekHeight", "", "Lcom/discord/primitives/UserId;", "userId", "isStageUserVisible", "(J)Z", "Lcom/discord/primitives/ChannelId;", "getChannelId", "()J", "isPortraitMode", "()Z", "onDestroy", "Landroid/view/LayoutInflater;", "inflater", "container", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/view/View;", "onCreateView", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "onPause", "", "requestCode", "resultCode", "Landroid/content/Intent;", "data", "onActivityResult", "(IILandroid/content/Intent;)V", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;", "handleEvent", "(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;)V", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Lcom/discord/widgets/voice/controls/VoiceControlsSheetView;", "bottomSheetBehavior", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;", "gridAdapter", "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;", "com/discord/widgets/voice/fullscreen/WidgetCallFullscreen$stageSpeakerPillManagingScrollListener$1", "stageSpeakerPillManagingScrollListener", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$stageSpeakerPillManagingScrollListener$1;", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;", "bottomSheetCallback", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;", "Landroidx/core/view/WindowInsetsCompat;", "systemWindowInsets", "Landroidx/core/view/WindowInsetsCompat;", "Lcom/discord/tooltips/TooltipManager;", "tooltipManager", "Lcom/discord/tooltips/TooltipManager;", "hasVideoCallGridChildrenChanged", "Z", "Lcom/discord/utilities/views/viewstub/LazyViewStubDelegate;", "eventPromptOverlay$delegate", "Lkotlin/Lazy;", "getEventPromptOverlay", "()Lcom/discord/utilities/views/viewstub/LazyViewStubDelegate;", "eventPromptOverlay", "lastUnreadsCount", "Ljava/lang/Integer;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallBottomSheetManager;", "stageCallBottomSheetManager", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallBottomSheetManager;", "lastMentionsCount", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;", "viewModel", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetCallFullscreenBinding;", "Lrx/Subscription;", "connectedTimerSubscription", "Lrx/Subscription;", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;", "stageAdapter", "Lcom/discord/widgets/voice/fullscreen/stage/StageCallAdapter;", "Landroidx/recyclerview/widget/GridLayoutManager;", "stageLayoutManager", "Landroidx/recyclerview/widget/GridLayoutManager;", "Lcom/discord/tooltips/DefaultTooltipCreator;", "defaultTooltipCreator", "Lcom/discord/tooltips/DefaultTooltipCreator;", "", "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$MenuItem;", "lastMenuItems", "Ljava/util/Set;", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallFullscreen extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetCallFullscreen.class, "binding", "getBinding()Lcom/discord/databinding/WidgetCallFullscreenBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String END_EVENT_REQUEST_KEY = "END_EVENT_REQUEST_KEY";
    private static final String END_STAGE_REQUEST_KEY = "END_STAGE_REQUEST_KEY";
    private static final String INTENT_EXTRA_CONNECT_ON_LAUNCH = "INTENT_EXTRA_CONNECT_ON_LAUNCH";
    public static final int MAX_SPEAKERS_PER_ROW = 3;
    private BottomSheetBehavior<VoiceControlsSheetView> bottomSheetBehavior;
    private Subscription connectedTimerSubscription;
    private final DefaultTooltipCreator defaultTooltipCreator;
    private final Lazy eventPromptOverlay$delegate;
    private VideoCallGridAdapter gridAdapter;
    private boolean hasVideoCallGridChildrenChanged;
    private Integer lastMentionsCount;
    private Set<? extends WidgetCallFullscreenViewModel.MenuItem> lastMenuItems;
    private Integer lastUnreadsCount;
    private StageCallAdapter stageAdapter;
    private final StageCallBottomSheetManager stageCallBottomSheetManager;
    private GridLayoutManager stageLayoutManager;
    private WindowInsetsCompat systemWindowInsets;
    private final TooltipManager tooltipManager;
    private final Lazy viewModel$delegate;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding(this, WidgetCallFullscreen$binding$2.INSTANCE, new WidgetCallFullscreen$binding$3(this));
    private final BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$bottomSheetCallback$1
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
        public void onSlide(View view, float f) {
            WidgetCallFullscreenViewModel viewModel;
            m.checkNotNullParameter(view, "bottomSheet");
            viewModel = WidgetCallFullscreen.this.getViewModel();
            viewModel.stopIdle();
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
        public void onStateChanged(View view, int i) {
            WidgetCallFullscreenBinding binding;
            WidgetCallFullscreenViewModel viewModel;
            WidgetCallFullscreenViewModel viewModel2;
            TooltipManager tooltipManager;
            WidgetCallFullscreenViewModel viewModel3;
            m.checkNotNullParameter(view, "bottomSheet");
            binding = WidgetCallFullscreen.this.getBinding();
            binding.f2231y.handleSheetState(i);
            viewModel = WidgetCallFullscreen.this.getViewModel();
            viewModel.handleBottomSheetState(i);
            if (i == 3) {
                viewModel2 = WidgetCallFullscreen.this.getViewModel();
                viewModel2.stopIdle();
                tooltipManager = WidgetCallFullscreen.this.tooltipManager;
                tooltipManager.a(VoiceControlsSheetSwipeTooltip.INSTANCE);
            } else if (i == 4) {
                viewModel3 = WidgetCallFullscreen.this.getViewModel();
                viewModel3.startIdle();
            }
        }
    };
    private final WidgetCallFullscreen$stageSpeakerPillManagingScrollListener$1 stageSpeakerPillManagingScrollListener = new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$stageSpeakerPillManagingScrollListener$1
        private List<StoreVoiceParticipants.VoiceUser> activeSpeakers = n.emptyList();

        public final List<StoreVoiceParticipants.VoiceUser> getActiveSpeakers() {
            return this.activeSpeakers;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            WidgetCallFullscreenBinding binding;
            boolean isStageUserVisible;
            m.checkNotNullParameter(recyclerView, "recyclerView");
            List<StoreVoiceParticipants.VoiceUser> list = this.activeSpeakers;
            boolean z2 = true;
            int i3 = 0;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                for (StoreVoiceParticipants.VoiceUser voiceUser : list) {
                    isStageUserVisible = WidgetCallFullscreen.this.isStageUserVisible(voiceUser.getUser().getId());
                    if (!isStageUserVisible) {
                        break;
                    }
                }
            }
            z2 = false;
            binding = WidgetCallFullscreen.this.getBinding();
            ConstraintLayout constraintLayout = binding.t;
            m.checkNotNullExpressionValue(constraintLayout, "binding.stageCallSpeakingChip");
            if (!z2) {
                i3 = 8;
            }
            constraintLayout.setVisibility(i3);
        }

        public final void setActiveSpeakers(List<StoreVoiceParticipants.VoiceUser> list) {
            m.checkNotNullParameter(list, "<set-?>");
            this.activeSpeakers = list;
        }
    };

    /* compiled from: WidgetCallFullscreen.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\f\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001d\u0010\u001eJK\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\b\b\u0002\u0010\b\u001a\u00020\u00072\u0010\b\u0002\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\n2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\fH\u0007¢\u0006\u0004\b\u000f\u0010\u0010J\u0015\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0014\u0010\u0015J\u0015\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u0011¢\u0006\u0004\b\u0016\u0010\u0015R\u0016\u0010\u0017\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0016\u0010\u0019\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0018R\u0016\u0010\u001a\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0018R\u0016\u0010\u001b\u001a\u00020\u00138\u0000@\u0000X\u0080T¢\u0006\u0006\n\u0004\b\u001b\u0010\u001c¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;", "", "Landroid/content/Context;", "context", "", "Lcom/discord/primitives/ChannelId;", "channelId", "", "connectOnLaunch", "", "Lcom/discord/primitives/StreamKey;", "streamKey", "Lcom/discord/app/AppTransitionActivity$Transition;", "transition", "", "launch", "(Landroid/content/Context;JZLjava/lang/String;Lcom/discord/app/AppTransitionActivity$Transition;)V", "Landroid/content/res/Resources;", "resources", "", "getUnreadIndicatorMarginLeftDp", "(Landroid/content/res/Resources;)I", "getUnreadIndicatorMarginDp", WidgetCallFullscreen.END_EVENT_REQUEST_KEY, "Ljava/lang/String;", WidgetCallFullscreen.END_STAGE_REQUEST_KEY, WidgetCallFullscreen.INTENT_EXTRA_CONNECT_ON_LAUNCH, "MAX_SPEAKERS_PER_ROW", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final int getUnreadIndicatorMarginDp(Resources resources) {
            m.checkNotNullParameter(resources, "resources");
            return resources.getConfiguration().orientation != 2 ? DeviceUtils.INSTANCE.isTablet(resources) ? 36 : 32 : DeviceUtils.INSTANCE.isTablet(resources) ? 36 : 28;
        }

        public final int getUnreadIndicatorMarginLeftDp(Resources resources) {
            m.checkNotNullParameter(resources, "resources");
            if (resources.getConfiguration().orientation != 2) {
                if (DeviceUtils.INSTANCE.isTablet(resources)) {
                    return 40;
                }
            } else if (DeviceUtils.INSTANCE.isTablet(resources)) {
                return 40;
            }
            return 32;
        }

        public final void launch(Context context, long j, boolean z2, String str, AppTransitionActivity.Transition transition) {
            ActivityOptionsCompat activityOptions;
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent(context, AppActivity.Call.class);
            intent.putExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", j);
            intent.putExtra("com.discord.intent.extra.EXTRA_STREAM_KEY", str);
            intent.putExtra(WidgetCallFullscreen.INTENT_EXTRA_CONNECT_ON_LAUNCH, z2);
            context.startActivity(intent, (transition == null || (activityOptions = transition.toActivityOptions(context)) == null) ? null : activityOptions.toBundle());
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            WidgetCallFullscreenViewModel.DisplayMode.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            WidgetCallFullscreenViewModel.DisplayMode displayMode = WidgetCallFullscreenViewModel.DisplayMode.STAGE;
            iArr[displayMode.ordinal()] = 1;
            WidgetCallFullscreenViewModel.DisplayMode displayMode2 = WidgetCallFullscreenViewModel.DisplayMode.GRID;
            iArr[displayMode2.ordinal()] = 2;
            WidgetCallFullscreenViewModel.DisplayMode.values();
            int[] iArr2 = new int[3];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[displayMode.ordinal()] = 1;
            iArr2[displayMode2.ordinal()] = 2;
            iArr2[WidgetCallFullscreenViewModel.DisplayMode.PRIVATE_CALL_PARTICIPANTS.ordinal()] = 3;
        }
    }

    /* JADX WARN: Type inference failed for: r0v8, types: [com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$stageSpeakerPillManagingScrollListener$1] */
    public WidgetCallFullscreen() {
        super(R.layout.widget_call_fullscreen);
        WidgetCallFullscreen$viewModel$2 widgetCallFullscreen$viewModel$2 = new WidgetCallFullscreen$viewModel$2(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetCallFullscreenViewModel.class), new WidgetCallFullscreen$appViewModels$$inlined$viewModels$1(new f0(this)), new h0(widgetCallFullscreen$viewModel$2));
        AppLog appLog = AppLog.g;
        m.checkNotNullParameter(appLog, "logger");
        WeakReference<b.a.j.a> weakReference = a.b.a;
        TooltipManager tooltipManager = null;
        b.a.j.a aVar = weakReference != null ? weakReference.get() : null;
        if (aVar == null) {
            aVar = new b.a.j.a(appLog);
            a.b.a = new WeakReference<>(aVar);
        }
        b.a.j.a aVar2 = aVar;
        TooltipManager.a aVar3 = TooltipManager.a.d;
        m.checkNotNullParameter(aVar2, "floatingViewManager");
        WeakReference<TooltipManager> weakReference2 = TooltipManager.a.a;
        tooltipManager = weakReference2 != null ? weakReference2.get() : tooltipManager;
        if (tooltipManager == null) {
            tooltipManager = new TooltipManager((b.a.v.a) TooltipManager.a.f2787b.getValue(), (Set) TooltipManager.a.c.getValue(), 0, aVar2, 4);
            TooltipManager.a.a = new WeakReference<>(tooltipManager);
        }
        this.tooltipManager = tooltipManager;
        this.defaultTooltipCreator = new DefaultTooltipCreator(tooltipManager);
        this.stageCallBottomSheetManager = new StageCallBottomSheetManager();
        this.eventPromptOverlay$delegate = LazyViewStubDelegate.Companion.lazyViewStub(new WidgetCallFullscreen$eventPromptOverlay$2(this));
    }

    public static final /* synthetic */ VideoCallGridAdapter access$getGridAdapter$p(WidgetCallFullscreen widgetCallFullscreen) {
        VideoCallGridAdapter videoCallGridAdapter = widgetCallFullscreen.gridAdapter;
        if (videoCallGridAdapter == null) {
            m.throwUninitializedPropertyAccessException("gridAdapter");
        }
        return videoCallGridAdapter;
    }

    private final void collapseBottomSheet() {
        BottomSheetBehavior<VoiceControlsSheetView> bottomSheetBehavior = this.bottomSheetBehavior;
        if (bottomSheetBehavior == null) {
            m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        bottomSheetBehavior.setState(4);
    }

    /* JADX WARN: Removed duplicated region for block: B:41:0x00f9  */
    /* JADX WARN: Removed duplicated region for block: B:42:0x00fb  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x010b  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x010d  */
    /* JADX WARN: Removed duplicated region for block: B:49:0x0114 A[ADDED_TO_REGION] */
    /* JADX WARN: Removed duplicated region for block: B:53:0x0143  */
    /* JADX WARN: Removed duplicated region for block: B:61:0x0196  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x0198  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x01ae  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x01be  */
    /* JADX WARN: Removed duplicated region for block: B:80:0x022c  */
    /* JADX WARN: Removed duplicated region for block: B:81:0x022e  */
    /* JADX WARN: Removed duplicated region for block: B:83:0x023f  */
    /* JADX WARN: Removed duplicated region for block: B:85:0x0242  */
    /* JADX WARN: Removed duplicated region for block: B:88:0x0248  */
    /* JADX WARN: Removed duplicated region for block: B:91:0x025b  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureActionBar(final com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel.ViewState.Valid r29) {
        /*
            Method dump skipped, instructions count: 632
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen.configureActionBar(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$ViewState$Valid):void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:25:0x0109, code lost:
        if ((r1.getVisibility() == 0) == false) goto L27;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureBottomControls(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel.ViewState.Valid r32) {
        /*
            Method dump skipped, instructions count: 280
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen.configureBottomControls(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$ViewState$Valid):void");
    }

    private final void configureConnectionStatusText(CallModel callModel) {
        Subscription subscription = this.connectedTimerSubscription;
        if (subscription == null || subscription.isUnsubscribed()) {
            Observable<Long> D = Observable.D(0L, 1L, TimeUnit.SECONDS);
            m.checkNotNullExpressionValue(D, "Observable\n          .in…0L, 1L, TimeUnit.SECONDS)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(D, this, null, 2, null), WidgetCallFullscreen.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : new WidgetCallFullscreen$configureConnectionStatusText$2(this), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetCallFullscreen$configureConnectionStatusText$1(this, callModel));
        }
    }

    private final void configureEventPrompt(WidgetCallFullscreenViewModel.ViewState.Valid valid) {
        Guild guild = valid.getCallModel().getGuild();
        WidgetCallFullscreen$configureEventPrompt$onCreateEventClick$1 widgetCallFullscreen$configureEventPrompt$onCreateEventClick$1 = new WidgetCallFullscreen$configureEventPrompt$onCreateEventClick$1(this, guild);
        GuildScheduledEvent startableEvent = valid.getStartableEvent();
        WidgetCallFullscreen$configureEventPrompt$onStartEvent$1 widgetCallFullscreen$configureEventPrompt$onStartEvent$1 = new WidgetCallFullscreen$configureEventPrompt$onStartEvent$1(this, startableEvent);
        WidgetCallFullscreen$configureEventPrompt$onStartStage$1 widgetCallFullscreen$configureEventPrompt$onStartStage$1 = new WidgetCallFullscreen$configureEventPrompt$onStartStage$1(this);
        GuildEventPromptListView guildEventPromptListView = (GuildEventPromptListView) getEventPromptOverlay().getMaybeView();
        if (guildEventPromptListView != null) {
            GuildEventPromptListView.ScheduledEventData scheduledEventData = (startableEvent != null && GuildScheduledEventUtilities.Companion.canStartEventInChannel(valid.getCallModel().getChannel(), valid.getChannelPermissions())) ? new GuildEventPromptListView.ScheduledEventData(startableEvent, widgetCallFullscreen$configureEventPrompt$onStartEvent$1) : null;
            if (guild == null || valid.getStageCallModel() == null || !StageEventsGuildsFeatureFlag.Companion.getINSTANCE().canGuildAccessStageEvents(guild.getId()) || !PermissionUtils.can(Permission.START_STAGE_EVENT, valid.getChannelPermissions())) {
                widgetCallFullscreen$configureEventPrompt$onCreateEventClick$1 = null;
            }
            if (valid.getStageCallModel() == null || !StageRoles.m26isModeratorimpl(valid.getStageCallModel().m36getMyStageRoles1LxfuJo())) {
                widgetCallFullscreen$configureEventPrompt$onStartStage$1 = null;
            }
            guildEventPromptListView.configure(scheduledEventData, widgetCallFullscreen$configureEventPrompt$onCreateEventClick$1, widgetCallFullscreen$configureEventPrompt$onStartStage$1);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:56:0x019d  */
    /* JADX WARN: Removed duplicated region for block: B:59:0x01a7  */
    /* JADX WARN: Removed duplicated region for block: B:62:0x01b0  */
    /* JADX WARN: Removed duplicated region for block: B:63:0x01b2  */
    /* JADX WARN: Removed duplicated region for block: B:66:0x01bd  */
    /* JADX WARN: Removed duplicated region for block: B:67:0x01bf  */
    /* JADX WARN: Removed duplicated region for block: B:70:0x01c4  */
    /* JADX WARN: Removed duplicated region for block: B:81:0x01f9  */
    /* JADX WARN: Removed duplicated region for block: B:84:0x0214  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureGridUi(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel.ViewState.Valid r14) {
        /*
            Method dump skipped, instructions count: 555
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen.configureGridUi(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$ViewState$Valid):void");
    }

    private final void configureMenu(final WidgetCallFullscreenViewModel.ViewState.Valid valid) {
        if (m.areEqual(valid.getMenuItems(), this.lastMenuItems)) {
            int textInVoiceMentionCount = valid.getTextInVoiceMentionCount();
            Integer num = this.lastMentionsCount;
            if (num != null && textInVoiceMentionCount == num.intValue()) {
                int textInVoiceUnreadCount = valid.getTextInVoiceUnreadCount();
                Integer num2 = this.lastUnreadsCount;
                if (num2 != null && textInVoiceUnreadCount == num2.intValue()) {
                    return;
                }
            }
        }
        this.lastMenuItems = valid.getMenuItems();
        this.lastMentionsCount = Integer.valueOf(valid.getTextInVoiceMentionCount());
        this.lastUnreadsCount = Integer.valueOf(valid.getTextInVoiceUnreadCount());
        setActionBarOptionsMenu(R.menu.menu_call_fullscreen, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$configureMenu$1

            /* compiled from: WidgetCallFullscreen.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/streams/StreamContext;", "streamContext", "", "invoke", "(Lcom/discord/utilities/streams/StreamContext;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$configureMenu$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<StreamContext, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(StreamContext streamContext) {
                    invoke2(streamContext);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(StreamContext streamContext) {
                    WidgetCallFullscreenViewModel viewModel;
                    m.checkNotNullParameter(streamContext, "streamContext");
                    viewModel = WidgetCallFullscreen.this.getViewModel();
                    viewModel.targetAndFocusStream(streamContext.getStream().getEncodedStreamKey());
                }
            }

            public final void call(MenuItem menuItem, Context context) {
                WidgetCallFullscreenViewModel viewModel;
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                switch (menuItem.getItemId()) {
                    case R.id.menu_call_overlay_launcher /* 2131364298 */:
                        DiscordOverlayService.Companion companion = DiscordOverlayService.Companion;
                        m.checkNotNullExpressionValue(context, "context");
                        companion.launchForConnect(context);
                        return;
                    case R.id.menu_call_switch_camera /* 2131364299 */:
                        viewModel = WidgetCallFullscreen.this.getViewModel();
                        viewModel.switchCameraInputPressed();
                        return;
                    case R.id.menu_call_video_list /* 2131364300 */:
                        WidgetVoiceBottomSheet.Companion companion2 = WidgetVoiceBottomSheet.Companion;
                        FragmentManager parentFragmentManager = WidgetCallFullscreen.this.getParentFragmentManager();
                        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                        companion2.show(parentFragmentManager, valid.getCallModel().getChannel().h(), false, WidgetVoiceBottomSheet.FeatureContext.FULLSCREEN_CALL).setOnStreamPreviewClickedListener(new AnonymousClass1());
                        return;
                    case R.id.menu_call_voice_settings /* 2131364301 */:
                        WidgetVoiceSettingsBottomSheet.Companion companion3 = WidgetVoiceSettingsBottomSheet.Companion;
                        Long valueOf = Long.valueOf(valid.getCallModel().getChannel().h());
                        FragmentManager parentFragmentManager2 = WidgetCallFullscreen.this.getParentFragmentManager();
                        m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
                        companion3.show(valueOf, parentFragmentManager2);
                        return;
                    default:
                        return;
                }
            }
        }, new Action1<Menu>() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$configureMenu$2
            public final void call(Menu menu) {
                MenuItem findItem = menu.findItem(R.id.menu_text_in_voice);
                boolean contains = valid.getMenuItems().contains(WidgetCallFullscreenViewModel.MenuItem.TEXT_IN_VOICE);
                m.checkNotNullExpressionValue(findItem, "textInVoiceMenuItem");
                findItem.setVisible(contains);
                if (contains) {
                    View actionView = findItem.getActionView();
                    ImageView imageView = null;
                    TextView textView = actionView != null ? (TextView) actionView.findViewById(R.id.text_in_voice_count) : null;
                    View actionView2 = findItem.getActionView();
                    if (actionView2 != null) {
                        imageView = (ImageView) actionView2.findViewById(R.id.text_in_voice_icon);
                    }
                    int textInVoiceMentionCount2 = valid.getTextInVoiceMentionCount();
                    int textInVoiceUnreadCount2 = valid.getTextInVoiceUnreadCount();
                    if (textInVoiceMentionCount2 > 0) {
                        if (textView != null) {
                            ViewKt.setVisible(textView, true);
                        }
                        if (imageView != null) {
                            imageView.setImageResource(R.drawable.ic_text_in_voice_cutout);
                        }
                        if (textView != null) {
                            textView.setBackground(ContextCompat.getDrawable(WidgetCallFullscreen.this.requireContext(), R.drawable.drawable_circle_red));
                        }
                        if (textView != null) {
                            textView.setText(String.valueOf(Math.min(99, textInVoiceMentionCount2)));
                        }
                        if (textView != null) {
                            textView.setTextColor(ContextCompat.getColor(WidgetCallFullscreen.this.requireContext(), R.color.white));
                        }
                    } else if (textInVoiceUnreadCount2 > 0) {
                        if (textView != null) {
                            ViewKt.setVisible(textView, true);
                        }
                        if (imageView != null) {
                            imageView.setImageResource(R.drawable.ic_text_in_voice_cutout);
                        }
                        if (textView != null) {
                            textView.setBackground(ContextCompat.getDrawable(WidgetCallFullscreen.this.requireContext(), R.drawable.drawable_circle_white));
                        }
                        if (textView != null) {
                            textView.setText(String.valueOf(Math.min(99, textInVoiceUnreadCount2)));
                        }
                        if (textView != null) {
                            textView.setTextColor(ContextCompat.getColor(WidgetCallFullscreen.this.requireContext(), R.color.black));
                        }
                    } else {
                        if (imageView != null) {
                            imageView.setImageResource(R.drawable.ic_text_in_voice);
                        }
                        if (textView != null) {
                            ViewKt.setVisible(textView, false);
                        }
                    }
                    findItem.getActionView().setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$configureMenu$2.1
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view) {
                            WidgetCallFullscreenViewModel viewModel;
                            WidgetCallFullscreen.this.transitionActivity();
                            viewModel = WidgetCallFullscreen.this.getViewModel();
                            viewModel.onTextInVoiceTapped();
                        }
                    });
                }
                MenuItem findItem2 = menu.findItem(R.id.menu_call_switch_camera);
                m.checkNotNullExpressionValue(findItem2, "switchCameraMenuItem");
                findItem2.setVisible(valid.getMenuItems().contains(WidgetCallFullscreenViewModel.MenuItem.SWITCH_CAMERA));
                MenuItem findItem3 = menu.findItem(R.id.menu_call_voice_settings);
                m.checkNotNullExpressionValue(findItem3, "voiceSettingsMenuItem");
                findItem3.setVisible(valid.getMenuItems().contains(WidgetCallFullscreenViewModel.MenuItem.VOICE_SETTINGS));
                MenuItem findItem4 = menu.findItem(R.id.menu_call_overlay_launcher);
                m.checkNotNullExpressionValue(findItem4, "overlayLauncherMenuItem");
                findItem4.setVisible(valid.getMenuItems().contains(WidgetCallFullscreenViewModel.MenuItem.LAUNCH_OVERLAY));
                MenuItem findItem5 = menu.findItem(R.id.menu_call_video_list);
                m.checkNotNullExpressionValue(findItem5, "listMenuItem");
                findItem5.setVisible(valid.getMenuItems().contains(WidgetCallFullscreenViewModel.MenuItem.SHOW_PARTICIPANT_LIST));
            }
        });
    }

    private final void configurePrivateCallParticipantsUi(WidgetCallFullscreenViewModel.ViewState.Valid valid) {
        String str;
        LinearLayout linearLayout = getBinding().j;
        m.checkNotNullExpressionValue(linearLayout, "binding.callParticipantsHidden");
        int i = 0;
        linearLayout.setVisibility(valid.getShowParticipantsHiddenView() ? 0 : 8);
        RelativeLayout relativeLayout = getBinding().f2229s.d;
        m.checkNotNullExpressionValue(relativeLayout, "binding.privateCall.privateCallContainer");
        relativeLayout.setVisibility(0);
        getBinding().f2229s.f152b.configure(valid.getPrivateCallUserListItems());
        TextView textView = getBinding().f2229s.c;
        m.checkNotNullExpressionValue(textView, "binding.privateCall.priv…CallConnectivityStatusBar");
        if (!valid.getShowLowConnectivityBar()) {
            i = 8;
        }
        textView.setVisibility(i);
        getBinding().f2229s.f.configure(valid.getPrivateCallUserListItems());
        VideoCallParticipantView videoCallParticipantView = getBinding().l;
        m.checkNotNullExpressionValue(videoCallParticipantView, "binding.callPip");
        videoCallParticipantView.setVisibility(8);
        RecyclerView recyclerView = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView, "binding.callVideoRecycler");
        recyclerView.setVisibility(8);
        RecyclerView recyclerView2 = getBinding().m;
        m.checkNotNullExpressionValue(recyclerView2, "binding.callStageRecycler");
        recyclerView2.setVisibility(8);
        ConstraintLayout constraintLayout = getBinding().t;
        m.checkNotNullExpressionValue(constraintLayout, "binding.stageCallSpeakingChip");
        constraintLayout.setVisibility(8);
        configureMenu(valid);
        configureActionBar(valid);
        enableWakeLock();
        getBinding().f2229s.i.setText(valid.getCallModel().isConnected() ? R.string.ongoing_call : R.string.voice_status_not_connected);
        TextView textView2 = getBinding().f2229s.h;
        m.checkNotNullExpressionValue(textView2, "binding.privateCall.privateCallStatusPrimary");
        if (ChannelUtils.w(valid.getCallModel().getChannel())) {
            str = ChannelUtils.c(valid.getCallModel().getChannel());
        } else {
            StoreVoiceParticipants.VoiceUser dmRecipient = valid.getCallModel().getDmRecipient();
            str = dmRecipient != null ? dmRecipient.getDisplayName() : null;
        }
        textView2.setText(str);
        configureConnectionStatusText(valid.getCallModel());
    }

    /* JADX WARN: Removed duplicated region for block: B:34:0x00e7  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00f4  */
    /* JADX WARN: Removed duplicated region for block: B:45:0x018e  */
    /* JADX WARN: Removed duplicated region for block: B:51:0x01cf  */
    /* JADX WARN: Removed duplicated region for block: B:54:0x01df  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x0215  */
    /* JADX WARN: Removed duplicated region for block: B:69:0x027a  */
    /* JADX WARN: Removed duplicated region for block: B:72:0x0295  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void configureStageUi(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel.ViewState.Valid r17) {
        /*
            Method dump skipped, instructions count: 701
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen.configureStageUi(com.discord.widgets.voice.fullscreen.WidgetCallFullscreenViewModel$ViewState$Valid):void");
    }

    private final void configureSwipeTooltip(boolean z2) {
        ViewBinding viewBinding;
        if (z2) {
            DefaultTooltipCreator defaultTooltipCreator = this.defaultTooltipCreator;
            View view = getBinding().f2231y;
            m.checkNotNullExpressionValue(view, "binding.voiceControlsSheetView");
            TooltipManager.b bVar = VoiceControlsSheetSwipeTooltip.INSTANCE;
            String string = getResources().getString(R.string.voice_controls_sheet_tooltip_swipe_up);
            m.checkNotNullExpressionValue(string, "resources.getString(R.st…s_sheet_tooltip_swipe_up)");
            int dpToPixels = DimenUtils.dpToPixels(-12);
            Observable<R> F = getUnsubscribeSignal().F(WidgetCallFullscreen$configureSwipeTooltip$1.INSTANCE);
            m.checkNotNullExpressionValue(F, "this.unsubscribeSignal.map { }");
            FloatingViewGravity floatingViewGravity = FloatingViewGravity.TOP;
            Objects.requireNonNull(defaultTooltipCreator);
            m.checkNotNullParameter(view, "anchorView");
            m.checkNotNullParameter(string, "tooltipText");
            m.checkNotNullParameter(bVar, "tooltip");
            m.checkNotNullParameter(floatingViewGravity, "tooltipGravity");
            m.checkNotNullParameter(F, "componentPausedObservable");
            if (defaultTooltipCreator.a.b(bVar, true)) {
                LayoutInflater from = LayoutInflater.from(view.getContext());
                if (floatingViewGravity == floatingViewGravity) {
                    View rootView = view.getRootView();
                    Objects.requireNonNull(rootView, "null cannot be cast to non-null type android.view.ViewGroup");
                    View inflate = from.inflate(R.layout.default_tooltip_view_top, (ViewGroup) rootView, false);
                    TextView textView = (TextView) inflate.findViewById(R.id.default_tooltip_text);
                    if (textView != null) {
                        viewBinding = new j((ConstraintLayout) inflate, textView);
                        m.checkNotNullExpressionValue(textView, "defaultTooltipText");
                        textView.setText(string);
                        m.checkNotNullExpressionValue(viewBinding, "DefaultTooltipViewTopBin…ext = tooltipText\n      }");
                    } else {
                        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(R.id.default_tooltip_text)));
                    }
                } else {
                    View rootView2 = view.getRootView();
                    Objects.requireNonNull(rootView2, "null cannot be cast to non-null type android.view.ViewGroup");
                    View inflate2 = from.inflate(R.layout.default_tooltip_view_bottom, (ViewGroup) rootView2, false);
                    TextView textView2 = (TextView) inflate2.findViewById(R.id.default_tooltip_text);
                    if (textView2 != null) {
                        viewBinding = new i((ConstraintLayout) inflate2, textView2);
                        m.checkNotNullExpressionValue(textView2, "defaultTooltipText");
                        textView2.setText(string);
                        m.checkNotNullExpressionValue(viewBinding, "DefaultTooltipViewBottom…ext = tooltipText\n      }");
                    } else {
                        throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(R.id.default_tooltip_text)));
                    }
                }
                viewBinding.getRoot().setOnClickListener(new b(defaultTooltipCreator, bVar));
                TooltipManager tooltipManager = defaultTooltipCreator.a;
                View root = viewBinding.getRoot();
                m.checkNotNullExpressionValue(root, "tooltipView.root");
                tooltipManager.d(view, root, bVar, floatingViewGravity, 0, dpToPixels, true, F);
                return;
            }
            return;
        }
        this.tooltipManager.c(VoiceControlsSheetSwipeTooltip.INSTANCE);
    }

    public final void configureUI(WidgetCallFullscreenViewModel.ViewState viewState) {
        if (m.areEqual(viewState, WidgetCallFullscreenViewModel.ViewState.Invalid.INSTANCE)) {
            requireActivity().finish();
        } else if (viewState instanceof WidgetCallFullscreenViewModel.ViewState.Valid) {
            configureValidUI((WidgetCallFullscreenViewModel.ViewState.Valid) viewState);
        }
    }

    private final void configureValidUI(WidgetCallFullscreenViewModel.ViewState.Valid valid) {
        configureBottomControls(valid);
        ConstraintLayout constraintLayout = getBinding().a;
        int ordinal = valid.getDisplayMode().ordinal();
        int i = R.color.primary_dark_800;
        if (ordinal != 0) {
            if (ordinal != 1) {
                i = R.color.black;
            }
        } else if (!valid.isTextInVoiceEnabled()) {
            ConstraintLayout constraintLayout2 = getBinding().a;
            m.checkNotNullExpressionValue(constraintLayout2, "binding.root");
            i = DrawableCompat.getThemedDrawableRes(constraintLayout2, (int) R.attr.colorBackgroundPrimary, 0);
        }
        constraintLayout.setBackgroundResource(i);
        int ordinal2 = valid.getDisplayMode().ordinal();
        if (ordinal2 == 0) {
            configureGridUi(valid);
        } else if (ordinal2 == 1) {
            configureStageUi(valid);
        } else if (ordinal2 == 2) {
            configurePrivateCallParticipantsUi(valid);
        }
    }

    private final void destroyAllRenderers(WidgetCallFullscreenBinding widgetCallFullscreenBinding) {
        VideoCallParticipantView.d(widgetCallFullscreenBinding.l, null, null, false, null, false, 30);
        RecyclerView recyclerView = widgetCallFullscreenBinding.p;
        m.checkNotNullExpressionValue(recyclerView, "binding.callVideoRecycler");
        int childCount = recyclerView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = widgetCallFullscreenBinding.p.getChildAt(i);
            if (childAt instanceof VideoCallParticipantView) {
                VideoCallParticipantView.d((VideoCallParticipantView) childAt, null, null, false, null, false, 30);
            }
        }
    }

    private final void enableWakeLock() {
        FragmentActivity requireActivity = requireActivity();
        m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        requireActivity.getWindow().addFlags(128);
    }

    public final void finishActivity(boolean z2, boolean z3) {
        getViewModel().selectTextChannelAfterFinish();
        if (z2) {
            getViewModel().disconnect();
        }
        requireActivity().finish();
        if (z3) {
            requireAppActivity().overridePendingTransition(0, R.anim.anim_slide_out_down);
        }
    }

    public static /* synthetic */ void finishActivity$default(WidgetCallFullscreen widgetCallFullscreen, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        widgetCallFullscreen.finishActivity(z2, z3);
    }

    public final WidgetCallFullscreenBinding getBinding() {
        return (WidgetCallFullscreenBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    public final long getChannelId() {
        return getMostRecentIntent().getLongExtra("com.discord.intent.extra.EXTRA_CHANNEL_ID", -1L);
    }

    private final LazyViewStubDelegate getEventPromptOverlay() {
        return (LazyViewStubDelegate) this.eventPromptOverlay$delegate.getValue();
    }

    public final WidgetCallFullscreenViewModel getViewModel() {
        return (WidgetCallFullscreenViewModel) this.viewModel$delegate.getValue();
    }

    public final List<VideoCallParticipantView.ParticipantData> getVisibleParticipants(VideoCallGridLayoutManager videoCallGridLayoutManager) {
        IntRange intRange = new IntRange(videoCallGridLayoutManager.findFirstVisibleItemPosition(), videoCallGridLayoutManager.findLastVisibleItemPosition());
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(intRange, 10));
        Iterator<Integer> it = intRange.iterator();
        while (it.hasNext()) {
            View childAt = getBinding().p.getChildAt(((c0) it).nextInt());
            arrayList.add(childAt instanceof VideoCallParticipantView ? ((VideoCallParticipantView) childAt).getData() : null);
        }
        return u.filterNotNull(arrayList);
    }

    private final void handleIdleStateChanged(WidgetCallFullscreenViewModel.Event.OnIdleStateChanged onIdleStateChanged) {
        if (onIdleStateChanged.isIdle()) {
            BottomSheetBehavior<VoiceControlsSheetView> bottomSheetBehavior = this.bottomSheetBehavior;
            if (bottomSheetBehavior == null) {
                m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
            }
            if (bottomSheetBehavior.getState() == 3) {
                collapseBottomSheet();
            }
        }
    }

    private final void hideControls() {
        ViewExtensions.fadeBy(getBinding().f2231y, false, 200L);
        VideoCallGridAdapter videoCallGridAdapter = this.gridAdapter;
        if (videoCallGridAdapter == null) {
            m.throwUninitializedPropertyAccessException("gridAdapter");
        }
        videoCallGridAdapter.notifyCallControlsVisibilityChanged(false);
        FragmentActivity requireActivity = requireActivity();
        m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        Window window = requireActivity.getWindow();
        m.checkNotNullExpressionValue(window, "requireActivity().window");
        RecyclerView recyclerView = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView, "binding.callVideoRecycler");
        m.checkNotNullParameter(window, "window");
        m.checkNotNullParameter(recyclerView, "view");
        if (Build.VERSION.SDK_INT >= 30) {
            window.setDecorFitsSystemWindows(true);
        }
        recyclerView.setSystemUiVisibility(5894);
    }

    private final void initializeSystemUiListeners(ViewGroup viewGroup) {
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().f2231y, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$initializeSystemUiListeners$1
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetCallFullscreen.this.systemWindowInsets = windowInsetsCompat;
                WidgetCallFullscreen.this.setVoiceControlsSheetPeekHeight();
                m.checkNotNullExpressionValue(view, "view");
                m.checkNotNullExpressionValue(windowInsetsCompat, "insets");
                view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), WidgetCallFullscreen.this.getResources().getDimensionPixelSize(R.dimen.voice_controls_sheet_bottom_padding) + windowInsetsCompat.getSystemWindowInsetBottom());
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                marginLayoutParams.leftMargin = windowInsetsCompat.getSystemWindowInsetLeft();
                marginLayoutParams.rightMargin = windowInsetsCompat.getSystemWindowInsetRight();
                view.setLayoutParams(marginLayoutParams);
                return windowInsetsCompat;
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().c, WidgetCallFullscreen$initializeSystemUiListeners$2.INSTANCE);
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().h, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$initializeSystemUiListeners$3
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                m.checkNotNullExpressionValue(view, "view");
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
                WidgetCallFullscreen.Companion companion = WidgetCallFullscreen.Companion;
                Resources resources = WidgetCallFullscreen.this.getResources();
                m.checkNotNullExpressionValue(resources, "resources");
                ((ViewGroup.MarginLayoutParams) layoutParams2).leftMargin = DimenUtils.dpToPixels(companion.getUnreadIndicatorMarginLeftDp(resources));
                m.checkNotNullExpressionValue(windowInsetsCompat, "insets");
                int systemWindowInsetTop = windowInsetsCompat.getSystemWindowInsetTop();
                Resources resources2 = WidgetCallFullscreen.this.getResources();
                m.checkNotNullExpressionValue(resources2, "resources");
                ((ViewGroup.MarginLayoutParams) layoutParams2).topMargin = DimenUtils.dpToPixels(companion.getUnreadIndicatorMarginDp(resources2)) + systemWindowInsetTop;
                view.setLayoutParams(layoutParams2);
                return windowInsetsCompat;
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(getBinding().d, WidgetCallFullscreen$initializeSystemUiListeners$4.INSTANCE);
        CoordinatorLayout coordinatorLayout = getBinding().f;
        m.checkNotNullExpressionValue(coordinatorLayout, "binding.callControlsSheetContainer");
        ViewExtensions.setForwardingWindowInsetsListener(coordinatorLayout);
        ConstraintLayout constraintLayout = getBinding().i;
        m.checkNotNullExpressionValue(constraintLayout, "binding.callNonVideoContainer");
        ViewExtensions.setForwardingWindowInsetsListener(constraintLayout);
        ViewCompat.setOnApplyWindowInsetsListener(viewGroup, new OnApplyWindowInsetsListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$initializeSystemUiListeners$5
            @Override // androidx.core.view.OnApplyWindowInsetsListener
            public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                WidgetCallFullscreenBinding binding;
                WidgetCallFullscreenBinding binding2;
                WidgetCallFullscreenBinding binding3;
                WidgetCallFullscreenBinding binding4;
                m.checkNotNullExpressionValue(windowInsetsCompat, "insets");
                DisplayCutoutCompat displayCutout = windowInsetsCompat.getDisplayCutout();
                VideoCallGridAdapter access$getGridAdapter$p = WidgetCallFullscreen.access$getGridAdapter$p(WidgetCallFullscreen.this);
                Resources resources = WidgetCallFullscreen.this.getResources();
                m.checkNotNullExpressionValue(resources, "resources");
                int i = 0;
                access$getGridAdapter$p.setDisplayCutout(displayCutout, resources.getConfiguration().orientation == 2);
                int safeInsetLeft = displayCutout != null ? displayCutout.getSafeInsetLeft() : 0;
                int safeInsetTop = displayCutout != null ? displayCutout.getSafeInsetTop() : 0;
                int safeInsetRight = displayCutout != null ? displayCutout.getSafeInsetRight() : 0;
                if (displayCutout != null) {
                    i = displayCutout.getSafeInsetBottom();
                }
                WindowInsetsCompat build = new WindowInsetsCompat.Builder().setSystemWindowInsets(Insets.of(Math.max(windowInsetsCompat.getSystemWindowInsetLeft(), safeInsetLeft), Math.max(windowInsetsCompat.getSystemWindowInsetTop(), safeInsetTop), Math.max(windowInsetsCompat.getSystemWindowInsetRight(), safeInsetRight), Math.max(windowInsetsCompat.getSystemWindowInsetBottom(), i))).build();
                m.checkNotNullExpressionValue(build, "WindowInsetsCompat.Build…        )\n      ).build()");
                VideoCallGridAdapter access$getGridAdapter$p2 = WidgetCallFullscreen.access$getGridAdapter$p(WidgetCallFullscreen.this);
                binding = WidgetCallFullscreen.this.getBinding();
                AppBarLayout appBarLayout = binding.c;
                m.checkNotNullExpressionValue(appBarLayout, "binding.actionBarToolbarLayout");
                access$getGridAdapter$p2.setInsetsForAvoidingCallUiOverlap(appBarLayout.getMeasuredHeight(), windowInsetsCompat.getSystemWindowInsetBottom(), windowInsetsCompat.getSystemWindowInsetLeft(), windowInsetsCompat.getSystemWindowInsetRight());
                binding2 = WidgetCallFullscreen.this.getBinding();
                ViewCompat.dispatchApplyWindowInsets(binding2.f, build);
                binding3 = WidgetCallFullscreen.this.getBinding();
                ViewCompat.dispatchApplyWindowInsets(binding3.i, build);
                binding4 = WidgetCallFullscreen.this.getBinding();
                RecyclerView recyclerView = binding4.m;
                m.checkNotNullExpressionValue(recyclerView, "binding.callStageRecycler");
                recyclerView.setPadding(recyclerView.getPaddingLeft(), WidgetCallFullscreen.this.getResources().getDimensionPixelOffset(R.dimen.stage_details_top_padding) + build.getSystemWindowInsetTop(), recyclerView.getPaddingRight(), recyclerView.getPaddingBottom());
                return build;
            }
        });
    }

    private final boolean isPortraitMode() {
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        return resources.getConfiguration().orientation == 1;
    }

    public final boolean isStageUserVisible(long j) {
        StoreVoiceParticipants.VoiceUser voiceUser;
        User user;
        RecyclerView recyclerView = getBinding().m;
        m.checkNotNullExpressionValue(recyclerView, "binding.callStageRecycler");
        if (!(recyclerView.getVisibility() == 0)) {
            return false;
        }
        GridLayoutManager gridLayoutManager = this.stageLayoutManager;
        if (gridLayoutManager == null) {
            m.throwUninitializedPropertyAccessException("stageLayoutManager");
        }
        int findFirstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();
        int findLastVisibleItemPosition = gridLayoutManager.findLastVisibleItemPosition();
        if (findFirstVisibleItemPosition != -1 && findLastVisibleItemPosition != -1) {
            StageCallAdapter stageCallAdapter = this.stageAdapter;
            if (stageCallAdapter == null) {
                m.throwUninitializedPropertyAccessException("stageAdapter");
            }
            if (findFirstVisibleItemPosition <= findLastVisibleItemPosition) {
                while (true) {
                    StageCallItem item = stageCallAdapter.getItem(findFirstVisibleItemPosition);
                    if (!(item instanceof StageCallParticipantItem)) {
                        item = null;
                    }
                    StageCallParticipantItem stageCallParticipantItem = (StageCallParticipantItem) item;
                    if (stageCallParticipantItem == null || (voiceUser = stageCallParticipantItem.getVoiceUser()) == null || (user = voiceUser.getUser()) == null || user.getId() != j) {
                        if (findFirstVisibleItemPosition == findLastVisibleItemPosition) {
                            break;
                        }
                        findFirstVisibleItemPosition++;
                    } else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static final void launch(Context context, long j, boolean z2, String str, AppTransitionActivity.Transition transition) {
        Companion.launch(context, j, z2, str, transition);
    }

    public final void onViewBindingDestroy(WidgetCallFullscreenBinding widgetCallFullscreenBinding) {
        BottomSheetBehavior<VoiceControlsSheetView> bottomSheetBehavior = this.bottomSheetBehavior;
        if (bottomSheetBehavior == null) {
            m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        bottomSheetBehavior.removeBottomSheetCallback(this.bottomSheetCallback);
        destroyAllRenderers(widgetCallFullscreenBinding);
    }

    private final void setUpGridRecycler() {
        VideoCallGridAdapter videoCallGridAdapter = new VideoCallGridAdapter(new WidgetCallFullscreen$setUpGridRecycler$1(this), new WidgetCallFullscreen$setUpGridRecycler$2(this), new WidgetCallFullscreen$setUpGridRecycler$3(this), new WidgetCallFullscreen$setUpGridRecycler$4(getViewModel()), new WidgetCallFullscreen$setUpGridRecycler$5(getViewModel()), 2);
        this.gridAdapter = videoCallGridAdapter;
        if (videoCallGridAdapter == null) {
            m.throwUninitializedPropertyAccessException("gridAdapter");
        }
        videoCallGridAdapter.setHasStableIds(true);
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        int i = resources.getConfiguration().orientation != 2 ? 1 : 0;
        WidgetCallFullscreen$setUpGridRecycler$layoutManager$1 widgetCallFullscreen$setUpGridRecycler$layoutManager$1 = new WidgetCallFullscreen$setUpGridRecycler$layoutManager$1(this, i);
        WidgetCallFullscreen$setUpGridRecycler$layoutManager$2 widgetCallFullscreen$setUpGridRecycler$layoutManager$2 = new WidgetCallFullscreen$setUpGridRecycler$layoutManager$2(this);
        RecyclerView recyclerView = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView, "binding.callVideoRecycler");
        Context context = recyclerView.getContext();
        m.checkNotNullExpressionValue(context, "binding.callVideoRecycler.context");
        final VideoCallGridLayoutManager videoCallGridLayoutManager = new VideoCallGridLayoutManager(widgetCallFullscreen$setUpGridRecycler$layoutManager$1, 2, widgetCallFullscreen$setUpGridRecycler$layoutManager$2, i, context);
        getBinding().p.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$setUpGridRecycler$6
            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrollStateChanged(RecyclerView recyclerView2, int i2) {
                WidgetCallFullscreenViewModel viewModel;
                List<VideoCallParticipantView.ParticipantData> visibleParticipants;
                m.checkNotNullParameter(recyclerView2, "recyclerView");
                if (i2 == 0) {
                    viewModel = WidgetCallFullscreen.this.getViewModel();
                    visibleParticipants = WidgetCallFullscreen.this.getVisibleParticipants(videoCallGridLayoutManager);
                    viewModel.updateOffScreenParticipantsFromScroll(visibleParticipants);
                }
            }
        });
        getBinding().p.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$setUpGridRecycler$7
            @Override // androidx.recyclerview.widget.RecyclerView.OnChildAttachStateChangeListener
            public void onChildViewAttachedToWindow(View view) {
                m.checkNotNullParameter(view, "view");
                WidgetCallFullscreen.this.hasVideoCallGridChildrenChanged = true;
            }

            @Override // androidx.recyclerview.widget.RecyclerView.OnChildAttachStateChangeListener
            public void onChildViewDetachedFromWindow(View view) {
                m.checkNotNullParameter(view, "view");
                WidgetCallFullscreen.this.hasVideoCallGridChildrenChanged = true;
            }
        });
        VideoCallGridAdapter videoCallGridAdapter2 = this.gridAdapter;
        if (videoCallGridAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("gridAdapter");
        }
        videoCallGridLayoutManager.setSpanSizeLookup(videoCallGridAdapter2.getSpanSizeLookup());
        RecyclerView recyclerView2 = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView2, "binding.callVideoRecycler");
        recyclerView2.setLayoutManager(videoCallGridLayoutManager);
        RecyclerView recyclerView3 = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView3, "binding.callVideoRecycler");
        recyclerView3.setItemAnimator(null);
        RecyclerView recyclerView4 = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView4, "binding.callVideoRecycler");
        VideoCallGridAdapter videoCallGridAdapter3 = this.gridAdapter;
        if (videoCallGridAdapter3 == null) {
            m.throwUninitializedPropertyAccessException("gridAdapter");
        }
        recyclerView4.setAdapter(videoCallGridAdapter3);
    }

    private final void setUpStageRecycler() {
        MGRecyclerAdapter.Companion companion = MGRecyclerAdapter.Companion;
        RecyclerView recyclerView = getBinding().m;
        m.checkNotNullExpressionValue(recyclerView, "binding.callStageRecycler");
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        this.stageAdapter = (StageCallAdapter) companion.configure(new StageCallAdapter(recyclerView, parentFragmentManager, WidgetCallFullscreen$setUpStageRecycler$1.INSTANCE, new WidgetCallFullscreen$setUpStageRecycler$2(this), new WidgetCallFullscreen$setUpStageRecycler$3(this), new WidgetCallFullscreen$setUpStageRecycler$4(getViewModel()), new WidgetCallFullscreen$setUpStageRecycler$5(getViewModel())));
        RecyclerView recyclerView2 = getBinding().m;
        m.checkNotNullExpressionValue(recyclerView2, "binding.callStageRecycler");
        GridLayoutManager gridLayoutManager = new GridLayoutManager(recyclerView2.getContext(), 12);
        StageCallAdapter stageCallAdapter = this.stageAdapter;
        if (stageCallAdapter == null) {
            m.throwUninitializedPropertyAccessException("stageAdapter");
        }
        gridLayoutManager.setSpanSizeLookup(stageCallAdapter.getSpanSizeLookup());
        this.stageLayoutManager = gridLayoutManager;
        RecyclerView recyclerView3 = getBinding().m;
        GridLayoutManager gridLayoutManager2 = this.stageLayoutManager;
        if (gridLayoutManager2 == null) {
            m.throwUninitializedPropertyAccessException("stageLayoutManager");
        }
        recyclerView3.setLayoutManager(gridLayoutManager2);
        StageCallAdapter stageCallAdapter2 = this.stageAdapter;
        if (stageCallAdapter2 == null) {
            m.throwUninitializedPropertyAccessException("stageAdapter");
        }
        recyclerView3.addItemDecoration(stageCallAdapter2.getItemDecoration());
        recyclerView3.addOnScrollListener(this.stageSpeakerPillManagingScrollListener);
    }

    public final void setVoiceControlsSheetPeekHeight() {
        WindowInsetsCompat windowInsetsCompat = this.systemWindowInsets;
        int systemWindowInsetBottom = windowInsetsCompat != null ? windowInsetsCompat.getSystemWindowInsetBottom() : 0;
        int peekHeight = getBinding().f2231y.getPeekHeight();
        BottomSheetBehavior<VoiceControlsSheetView> bottomSheetBehavior = this.bottomSheetBehavior;
        if (bottomSheetBehavior == null) {
            m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        int i = systemWindowInsetBottom + peekHeight;
        bottomSheetBehavior.setPeekHeight(i);
        RecyclerView recyclerView = getBinding().m;
        m.checkNotNullExpressionValue(recyclerView, "binding.callStageRecycler");
        recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getPaddingTop(), recyclerView.getPaddingRight(), i);
        ConstraintLayout constraintLayout = getBinding().o;
        m.checkNotNullExpressionValue(constraintLayout, "binding.callStreamingActive");
        constraintLayout.setPadding(constraintLayout.getPaddingLeft(), constraintLayout.getPaddingTop(), constraintLayout.getPaddingRight(), peekHeight);
        LinearLayout linearLayout = getBinding().f2229s.e;
        m.checkNotNullExpressionValue(linearLayout, "binding.privateCall.privateCallContainerContent");
        linearLayout.setPadding(linearLayout.getPaddingLeft(), linearLayout.getPaddingTop(), linearLayout.getPaddingRight(), peekHeight);
    }

    private final void showControls() {
        ViewExtensions.fadeBy(getBinding().f2231y, true, 200L);
        VideoCallGridAdapter videoCallGridAdapter = this.gridAdapter;
        if (videoCallGridAdapter == null) {
            m.throwUninitializedPropertyAccessException("gridAdapter");
        }
        videoCallGridAdapter.notifyCallControlsVisibilityChanged(true);
        FragmentActivity requireActivity = requireActivity();
        m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        Window window = requireActivity.getWindow();
        m.checkNotNullExpressionValue(window, "requireActivity().window");
        RecyclerView recyclerView = getBinding().p;
        m.checkNotNullExpressionValue(recyclerView, "binding.callVideoRecycler");
        m.checkNotNullParameter(window, "window");
        m.checkNotNullParameter(recyclerView, "view");
        if (Build.VERSION.SDK_INT >= 30) {
            window.setDecorFitsSystemWindows(true);
        }
        recyclerView.setSystemUiVisibility(1792);
    }

    private final void showNoScreenSharePermissionDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.no_video_permission_dialog_title);
        String string2 = requireContext.getString(R.string.no_screenshare_permission_dialog_body);
        m.checkNotNullExpressionValue(string2, "context.getString(R.stri…e_permission_dialog_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showNoVadPermissionDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.vad_permission_title);
        String string2 = requireContext.getString(R.string.vad_permission_body);
        m.checkNotNullExpressionValue(string2, "context.getString(R.string.vad_permission_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showNoVideoPermissionDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.no_video_permission_dialog_title);
        String string2 = requireContext.getString(R.string.no_video_permission_dialog_body);
        m.checkNotNullExpressionValue(string2, "context.getString(R.stri…o_permission_dialog_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showServerDeafenedDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.server_deafened_dialog_title);
        String string2 = requireContext.getString(R.string.server_deafened_dialog_body);
        m.checkNotNullExpressionValue(string2, "context.getString(R.stri…ver_deafened_dialog_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showServerMutedDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.server_muted_dialog_title);
        String string2 = requireContext.getString(R.string.server_muted_dialog_body);
        m.checkNotNullExpressionValue(string2, "context.getString(R.stri…server_muted_dialog_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    private final void showSuppressedDialog() {
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        Context requireContext = requireContext();
        m.checkNotNullParameter(parentFragmentManager, "fragmentManager");
        m.checkNotNullParameter(requireContext, "context");
        WidgetNoticeDialog.Companion companion = WidgetNoticeDialog.Companion;
        String string = requireContext.getString(R.string.suppressed);
        String string2 = requireContext.getString(R.string.suppressed_permission_body);
        m.checkNotNullExpressionValue(string2, "context.getString(R.stri…ppressed_permission_body)");
        WidgetNoticeDialog.Companion.show$default(companion, parentFragmentManager, string, string2, requireContext.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
    }

    public final void transitionActivity() {
        requireAppActivity().finish();
        requireAppActivity().overridePendingTransition(R.anim.activity_slide_horizontal_open_in, R.anim.activity_slide_horizontal_open_out);
    }

    public final void handleEvent(WidgetCallFullscreenViewModel.Event event) {
        Unit unit;
        m.checkNotNullParameter(event, "event");
        if (m.areEqual(event, WidgetCallFullscreenViewModel.Event.ShowSuppressedDialog.INSTANCE)) {
            showSuppressedDialog();
            unit = Unit.a;
        } else if (m.areEqual(event, WidgetCallFullscreenViewModel.Event.ShowServerMutedDialog.INSTANCE)) {
            showServerMutedDialog();
            unit = Unit.a;
        } else if (m.areEqual(event, WidgetCallFullscreenViewModel.Event.ShowServerDeafenedDialog.INSTANCE)) {
            showServerDeafenedDialog();
            unit = Unit.a;
        } else if (m.areEqual(event, WidgetCallFullscreenViewModel.Event.ShowNoVideoPermissionDialog.INSTANCE)) {
            showNoVideoPermissionDialog();
            unit = Unit.a;
        } else if (m.areEqual(event, WidgetCallFullscreenViewModel.Event.ShowNoVadPermissionDialog.INSTANCE)) {
            showNoVadPermissionDialog();
            unit = Unit.a;
        } else if (m.areEqual(event, WidgetCallFullscreenViewModel.Event.ShowGuildVideoAtCapacityDialog.INSTANCE)) {
            m.a aVar = b.a.a.m.k;
            FragmentManager parentFragmentManager = getParentFragmentManager();
            d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
            aVar.a(parentFragmentManager);
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.ShowCameraCapacityDialog) {
            j.a aVar2 = b.a.a.j.k;
            FragmentManager parentFragmentManager2 = getParentFragmentManager();
            d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager2, "parentFragmentManager");
            aVar2.a(parentFragmentManager2, ((WidgetCallFullscreenViewModel.Event.ShowCameraCapacityDialog) event).getGuildMaxVideoChannelUsers());
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.ShowUserSheet) {
            WidgetUserSheet.Companion companion = WidgetUserSheet.Companion;
            WidgetCallFullscreenViewModel.Event.ShowUserSheet showUserSheet = (WidgetCallFullscreenViewModel.Event.ShowUserSheet) event;
            long userId = showUserSheet.getUserId();
            Long valueOf = Long.valueOf(showUserSheet.getChannelId());
            FragmentManager parentFragmentManager3 = getParentFragmentManager();
            d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager3, "parentFragmentManager");
            WidgetUserSheet.Companion.show$default(companion, userId, valueOf, parentFragmentManager3, null, Boolean.TRUE, WidgetUserSheet.StreamPreviewClickBehavior.TARGET_AND_DISMISS, null, 72, null);
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.ShowRequestCameraPermissionsDialog) {
            requestVideoCallPermissions(new WidgetCallFullscreen$handleEvent$1(this));
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.AccessibilityAnnouncement) {
            AccessibilityUtils accessibilityUtils = AccessibilityUtils.INSTANCE;
            Context requireContext = requireContext();
            String string = getString(((WidgetCallFullscreenViewModel.Event.AccessibilityAnnouncement) event).getMessageResId());
            d0.z.d.m.checkNotNullExpressionValue(string, "getString(event.messageResId)");
            accessibilityUtils.sendAnnouncement(requireContext, string);
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.EnqueueStreamFeedbackSheet) {
            WidgetCallFullscreenViewModel.Event.EnqueueStreamFeedbackSheet enqueueStreamFeedbackSheet = (WidgetCallFullscreenViewModel.Event.EnqueueStreamFeedbackSheet) event;
            StreamFeedbackSheetNavigator.INSTANCE.enqueueNotice(enqueueStreamFeedbackSheet.getStreamKey(), enqueueStreamFeedbackSheet.getMediaSessionId(), enqueueStreamFeedbackSheet.getTriggerRateDenominator());
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.EnqueueCallFeedbackSheet) {
            WidgetCallFullscreenViewModel.Event.EnqueueCallFeedbackSheet enqueueCallFeedbackSheet = (WidgetCallFullscreenViewModel.Event.EnqueueCallFeedbackSheet) event;
            CallFeedbackSheetNavigator.INSTANCE.enqueueNotice(enqueueCallFeedbackSheet.getChannelId(), enqueueCallFeedbackSheet.getRtcConnectionId(), enqueueCallFeedbackSheet.getMediaSessionId(), Long.valueOf(enqueueCallFeedbackSheet.getCallDuration()), enqueueCallFeedbackSheet.getTriggerRateDenominator());
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.ShowNoScreenSharePermissionDialog) {
            showNoScreenSharePermissionDialog();
            unit = Unit.a;
        } else if (d0.z.d.m.areEqual(event, WidgetCallFullscreenViewModel.Event.RequestStartStream.INSTANCE)) {
            BottomSheetBehavior<VoiceControlsSheetView> bottomSheetBehavior = this.bottomSheetBehavior;
            if (bottomSheetBehavior == null) {
                d0.z.d.m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
            }
            bottomSheetBehavior.setState(4);
            StreamNavigator.requestStartStream(this);
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.OnIdleStateChanged) {
            handleIdleStateChanged((WidgetCallFullscreenViewModel.Event.OnIdleStateChanged) event);
            unit = Unit.a;
        } else if (event instanceof WidgetCallFullscreenViewModel.Event.NavigateToPremiumSettings) {
            WidgetSettingsPremium.Companion.launch$default(WidgetSettingsPremium.Companion, requireContext(), null, null, 6, null);
            unit = Unit.a;
        } else if (d0.z.d.m.areEqual(event, WidgetCallFullscreenViewModel.Event.ShowActivitiesDesktopOnlyDialog.INSTANCE)) {
            FragmentManager parentFragmentManager4 = getParentFragmentManager();
            d0.z.d.m.checkNotNullExpressionValue(parentFragmentManager4, "parentFragmentManager");
            Context requireContext2 = requireContext();
            d0.z.d.m.checkNotNullParameter(parentFragmentManager4, "fragmentManager");
            d0.z.d.m.checkNotNullParameter(requireContext2, "context");
            WidgetNoticeDialog.Companion companion2 = WidgetNoticeDialog.Companion;
            String string2 = requireContext2.getString(R.string.embedded_activities_desktop_only_modal_title);
            String string3 = requireContext2.getString(R.string.embedded_activities_desktop_only_modal_description);
            d0.z.d.m.checkNotNullExpressionValue(string3, "context.getString(R.stri…p_only_modal_description)");
            WidgetNoticeDialog.Companion.show$default(companion2, parentFragmentManager4, string2, string3, requireContext2.getString(R.string.okay), null, null, null, null, null, null, null, null, 0, null, 16368, null);
            unit = Unit.a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        KotlinExtensionsKt.getExhaustive(unit);
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        StreamNavigator.handleActivityResult(i, i2, intent, new WidgetCallFullscreen$onActivityResult$1(this));
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        d0.z.d.m.checkNotNullParameter(layoutInflater, "inflater");
        FragmentActivity requireActivity = requireActivity();
        d0.z.d.m.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        Window window = requireActivity.getWindow();
        d0.z.d.m.checkNotNullExpressionValue(window, "requireActivity().window");
        d0.z.d.m.checkNotNullParameter(window, "window");
        if (Build.VERSION.SDK_INT >= 28) {
            window.getAttributes().layoutInDisplayCutoutMode = 1;
        }
        getViewModel().setTargetChannelId(getChannelId());
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        Subscription subscription = this.connectedTimerSubscription;
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        WidgetGlobalStatusIndicatorState.updateState$default(WidgetGlobalStatusIndicatorState.Provider.get(), false, false, false, 3, null);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        d0.z.d.m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        ViewGroup viewGroup = (ViewGroup) view;
        initializeSystemUiListeners(viewGroup);
        Context context = viewGroup.getContext();
        d0.z.d.m.checkNotNullExpressionValue(context, "view.context");
        setActionBarDisplayHomeAsUpEnabled(true, Integer.valueOf(DrawableCompat.getThemedDrawableRes$default(context, (int) R.attr.ic_action_bar_down, 0, 2, (Object) null)), Integer.valueOf((int) R.string.dismiss));
        AppTransitionActivity.a aVar = requireAppActivity().k;
        setUpGridRecycler();
        setUpStageRecycler();
        ViewCompat.setAccessibilityDelegate(view, new AccessibilityDelegateCompat() { // from class: com.discord.widgets.voice.fullscreen.WidgetCallFullscreen$onViewBound$1
            @Override // androidx.core.view.AccessibilityDelegateCompat
            public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup2, View view2, AccessibilityEvent accessibilityEvent) {
                WidgetCallFullscreenViewModel viewModel;
                Integer valueOf = accessibilityEvent != null ? Integer.valueOf(accessibilityEvent.getEventType()) : null;
                if ((valueOf != null && valueOf.intValue() == 8) || (valueOf != null && valueOf.intValue() == 32768)) {
                    viewModel = WidgetCallFullscreen.this.getViewModel();
                    viewModel.disableControlFading();
                }
                return super.onRequestSendAccessibilityEvent(viewGroup2, view2, accessibilityEvent);
            }
        });
        BottomSheetBehavior<VoiceControlsSheetView> from = BottomSheetBehavior.from(getBinding().f2231y);
        d0.z.d.m.checkNotNullExpressionValue(from, "BottomSheetBehavior.from…g.voiceControlsSheetView)");
        this.bottomSheetBehavior = from;
        if (from == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        from.setGestureInsetBottomIgnored(true);
        BottomSheetBehavior<VoiceControlsSheetView> bottomSheetBehavior = this.bottomSheetBehavior;
        if (bottomSheetBehavior == null) {
            d0.z.d.m.throwUninitializedPropertyAccessException("bottomSheetBehavior");
        }
        bottomSheetBehavior.addBottomSheetCallback(this.bottomSheetCallback);
        VoiceControlsSheetView voiceControlsSheetView = getBinding().f2231y;
        d0.z.d.m.checkNotNullExpressionValue(voiceControlsSheetView, "binding.voiceControlsSheetView");
        ViewExtensions.addOnHeightChangedListener(voiceControlsSheetView, new WidgetCallFullscreen$onViewBound$2(this));
        OnPressListener onPressListener = new OnPressListener(new WidgetCallFullscreen$onViewBound$listener$1(getViewModel()));
        getBinding().g.setOnTouchListener(onPressListener);
        getBinding().f2231y.setOnPTTListener(onPressListener);
        Channel channel = StoreStream.Companion.getChannels().getChannel(getChannelId());
        if (channel != null && ChannelUtils.z(channel)) {
            requireAppActivity().k = AppTransitionActivity.Transition.TYPE_SLIDE_VERTICAL_WITH_FADE.getAnimations();
        }
        if (!getMostRecentIntent().getBooleanExtra(INTENT_EXTRA_CONNECT_ON_LAUNCH, false)) {
            return;
        }
        if (channel == null || !ChannelUtils.z(channel)) {
            requestMicrophone(new WidgetCallFullscreen$onViewBound$3(this, channel), new WidgetCallFullscreen$onViewBound$4(this));
        } else {
            getViewModel().tryConnectToVoice();
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        WidgetGlobalStatusIndicatorState.updateState$default(WidgetGlobalStatusIndicatorState.Provider.get(), false, false, true, 3, null);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetCallFullscreen.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetCallFullscreen$onViewBoundOrOnResume$1(this));
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeEvents(), this, null, 2, null), WidgetCallFullscreen.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetCallFullscreen$onViewBoundOrOnResume$2(this));
    }
}
