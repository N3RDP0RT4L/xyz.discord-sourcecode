package com.discord.widgets.voice.fullscreen;

import android.view.ViewStub;
import com.discord.databinding.WidgetCallFullscreenBinding;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetCallFullscreen.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Landroid/view/ViewStub;", "invoke", "()Landroid/view/ViewStub;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetCallFullscreen$eventPromptOverlay$2 extends o implements Function0<ViewStub> {
    public final /* synthetic */ WidgetCallFullscreen this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetCallFullscreen$eventPromptOverlay$2(WidgetCallFullscreen widgetCallFullscreen) {
        super(0);
        this.this$0 = widgetCallFullscreen;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ViewStub invoke() {
        WidgetCallFullscreenBinding binding;
        binding = this.this$0.getBinding();
        ViewStub viewStub = binding.r;
        m.checkNotNullExpressionValue(viewStub, "binding.eventPromptOverlayStub");
        return viewStub;
    }
}
