package com.discord.widgets.voice.controls;

import android.view.View;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import com.discord.databinding.WidgetScreenShareNfxSheetBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetScreenShareNfxSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetScreenShareNfxSheetBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetScreenShareNfxSheetBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetScreenShareNfxSheet$binding$2 extends k implements Function1<View, WidgetScreenShareNfxSheetBinding> {
    public static final WidgetScreenShareNfxSheet$binding$2 INSTANCE = new WidgetScreenShareNfxSheet$binding$2();

    public WidgetScreenShareNfxSheet$binding$2() {
        super(1, WidgetScreenShareNfxSheetBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetScreenShareNfxSheetBinding;", 0);
    }

    public final WidgetScreenShareNfxSheetBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.screen_share_nfx_cancel_button;
        TextView textView = (TextView) view.findViewById(R.id.screen_share_nfx_cancel_button);
        if (textView != null) {
            i = R.id.screen_share_nfx_cta;
            MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.screen_share_nfx_cta);
            if (materialButton != null) {
                return new WidgetScreenShareNfxSheetBinding((NestedScrollView) view, textView, materialButton);
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
