package com.discord.widgets.voice.controls;

import android.content.Context;
import android.content.Intent;
import com.discord.utilities.voice.VoiceEngineServiceController;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetScreenShareNfxSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/content/Intent;", "it", "", "invoke", "(Landroid/content/Intent;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetScreenShareNfxSheet$onActivityResult$1 extends o implements Function1<Intent, Unit> {
    public final /* synthetic */ WidgetScreenShareNfxSheet this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetScreenShareNfxSheet$onActivityResult$1(WidgetScreenShareNfxSheet widgetScreenShareNfxSheet) {
        super(1);
        this.this$0 = widgetScreenShareNfxSheet;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Intent intent) {
        invoke2(intent);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Intent intent) {
        m.checkNotNullParameter(intent, "it");
        VoiceEngineServiceController.Companion.getINSTANCE().startStream(intent);
        if (!this.this$0.requireAppActivity().h(a0.getOrCreateKotlinClass(WidgetCallFullscreen.class))) {
            WidgetCallFullscreen.Companion companion = WidgetCallFullscreen.Companion;
            Context requireContext = this.this$0.requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            companion.launch(requireContext, this.this$0.requireArguments().getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID"), (r14 & 4) != 0 ? false : false, (r14 & 8) != 0 ? null : null, (r14 & 16) != 0 ? null : null);
        }
        this.this$0.dismiss();
    }
}
