package com.discord.widgets.voice.controls;

import androidx.core.app.NotificationCompat;
import j0.k.b;
import kotlin.Metadata;
import kotlin.Unit;
/* compiled from: AnchoredVoiceControlsView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Ljava/lang/Void;", "kotlin.jvm.PlatformType", "it", "", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Void;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AnchoredVoiceControlsView$configureScreenShareButtonSparkle$2<T, R> implements b<Void, Unit> {
    public static final AnchoredVoiceControlsView$configureScreenShareButtonSparkle$2 INSTANCE = new AnchoredVoiceControlsView$configureScreenShareButtonSparkle$2();

    @Override // j0.k.b
    public /* bridge */ /* synthetic */ Unit call(Void r1) {
        call2(r1);
        return Unit.a;
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final void call2(Void r1) {
    }
}
