package com.discord.widgets.voice.controls;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import b.a.k.b;
import com.discord.api.channel.ChannelUtils;
import com.discord.databinding.VoiceControlsSheetViewBinding;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.color.ColorCompatKt;
import com.discord.utilities.display.DisplayUtils;
import com.discord.utilities.press.OnPressListener;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.views.NumericBadgingView;
import com.discord.views.calls.VolumeSliderView;
import com.discord.widgets.stage.StageRoles;
import com.discord.widgets.voice.model.CallModel;
import com.google.android.material.textview.MaterialTextView;
import d0.a0.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: VoiceControlsSheetView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0010\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\u0006\u0010/\u001a\u00020.¢\u0006\u0004\bO\u0010PB\u001d\b\u0016\u0012\u0006\u0010/\u001a\u00020.\u0012\n\b\u0002\u0010R\u001a\u0004\u0018\u00010Q¢\u0006\u0004\bO\u0010SB'\b\u0016\u0012\u0006\u0010/\u001a\u00020.\u0012\n\b\u0002\u0010R\u001a\u0004\u0018\u00010Q\u0012\b\b\u0002\u0010T\u001a\u00020\u0017¢\u0006\u0004\bO\u0010UJ-\u0010\t\u001a\u00020\u00052\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002ø\u0001\u0000ø\u0001\u0001¢\u0006\u0004\b\u0007\u0010\bJ-\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J-\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00112\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0014\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0015\u0010\u0016JE\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00112\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\r2\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002ø\u0001\u0000ø\u0001\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ5\u0010!\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00112\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002ø\u0001\u0000ø\u0001\u0001¢\u0006\u0004\b\u001f\u0010 J-\u0010#\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00112\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b#\u0010\u0016J-\u0010&\u001a\u00020\u00052\u0006\u0010$\u001a\u00020\u00112\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b&\u0010\u0016J9\u0010,\u001a\u00020\u00052\u0006\u0010'\u001a\u00020\r2\u0006\u0010)\u001a\u00020(2\u0018\u0010+\u001a\u0014\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00050*H\u0002¢\u0006\u0004\b,\u0010-J\u001f\u00101\u001a\u00020\u00172\u0006\u0010/\u001a\u00020.2\u0006\u00100\u001a\u00020\rH\u0002¢\u0006\u0004\b1\u00102J\u001f\u00103\u001a\u00020\u00172\u0006\u0010/\u001a\u00020.2\u0006\u00100\u001a\u00020\rH\u0002¢\u0006\u0004\b3\u00102J\u000f\u00104\u001a\u00020\u0005H\u0014¢\u0006\u0004\b4\u00105J\r\u00106\u001a\u00020\u0017¢\u0006\u0004\b6\u00107J\u0015\u0010:\u001a\u00020\u00052\u0006\u00109\u001a\u000208¢\u0006\u0004\b:\u0010;J£\u0002\u0010F\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\r2\u0006\u0010<\u001a\u00020\r2\u0006\u0010=\u001a\u00020\r2\u0006\u0010>\u001a\u00020\r2\u0006\u0010?\u001a\u00020\r2\u0006\u0010@\u001a\u00020\r2\f\u0010A\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010B\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010'\u001a\u00020\r2\u0006\u0010)\u001a\u00020(2\u0018\u0010+\u001a\u0014\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00050*2\f\u0010C\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0018\u001a\u00020\u00172\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0019\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002ø\u0001\u0000ø\u0001\u0001¢\u0006\u0004\bD\u0010EJ\u0015\u0010H\u001a\u00020\u00052\u0006\u0010G\u001a\u00020\u0017¢\u0006\u0004\bH\u0010IR\u0016\u0010J\u001a\u00020\r8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bJ\u0010KR\u0016\u0010M\u001a\u00020L8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bM\u0010N\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b¡\u001e0\u0001¨\u0006V"}, d2 = {"Lcom/discord/widgets/voice/controls/VoiceControlsSheetView;", "Landroid/widget/LinearLayout;", "Lcom/discord/widgets/stage/StageRoles;", "stageRoles", "Lkotlin/Function0;", "", "onMoveToAudienceClick", "configureMoveToAudienceButton-yox5PQY", "(Lcom/discord/widgets/stage/StageRoles;Lkotlin/jvm/functions/Function0;)V", "configureMoveToAudienceButton", "Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;", "outputSelectorState", "onAudioOutputClick", "", "showInTopRow", "configureOutputSelectors", "(Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Lkotlin/jvm/functions/Function0;Z)V", "Lcom/discord/widgets/voice/model/CallModel;", "model", "onVideoClick", "isVideoEnabledForCall", "configureVideoButton", "(Lcom/discord/widgets/voice/model/CallModel;Lkotlin/jvm/functions/Function0;Z)V", "", "requestingToSpeakCount", "isUpdatingRequestToSpeak", "onRaiseHandClick", "configureRaiseHandButton-fw_bWyM", "(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/stage/StageRoles;IZLkotlin/jvm/functions/Function0;)V", "configureRaiseHandButton", "onMuteClick", "configureMuteButton-P2fzehM", "(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/stage/StageRoles;Lkotlin/jvm/functions/Function0;)V", "configureMuteButton", "onScreenshareClick", "configureScreenshareButtons", "callModel", "onInviteClick", "configureInviteButtons", "showStreamVolume", "", "perceptualStreamVolume", "Lkotlin/Function2;", "onStreamVolumeChange", "configureStreamVolume", "(ZFLkotlin/jvm/functions/Function2;)V", "Landroid/content/Context;", "context", "isActive", "getDefaultButtonIconTint", "(Landroid/content/Context;Z)I", "getDefaultButtonBackgroundTint", "onAttachedToWindow", "()V", "getPeekHeight", "()I", "Lcom/discord/utilities/press/OnPressListener;", "listener", "setOnPTTListener", "(Lcom/discord/utilities/press/OnPressListener;)V", "isPttEnabled", "isDeafened", "startedAsVideo", "showStopWatching", "showDisconnect", "onStopWatchingClick", "onDisconnectClick", "onDeafenPressed", "configureUI-3jxq49Y", "(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;ZZZZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ZFLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ZLcom/discord/widgets/stage/StageRoles;)V", "configureUI", "bottomSheetState", "handleSheetState", "(I)V", "isGestureNavigationEnabled", "Z", "Lcom/discord/databinding/VoiceControlsSheetViewBinding;", "binding", "Lcom/discord/databinding/VoiceControlsSheetViewBinding;", HookHelper.constructorName, "(Landroid/content/Context;)V", "Landroid/util/AttributeSet;", "attrs", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceControlsSheetView extends LinearLayout {
    private final VoiceControlsSheetViewBinding binding;
    private boolean isGestureNavigationEnabled;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceControlsSheetView(Context context) {
        super(context);
        m.checkNotNullParameter(context, "context");
        VoiceControlsSheetViewBinding a = VoiceControlsSheetViewBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "VoiceControlsSheetViewBi…ater.from(context), this)");
        this.binding = a;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        this.isGestureNavigationEnabled = DisplayUtils.isGestureNavigationEnabled(resources);
        CardView cardView = a.r;
        m.checkNotNullExpressionValue(cardView, "binding.secondaryActionsCard");
        cardView.setVisibility(this.isGestureNavigationEnabled ? 0 : 4);
    }

    private final void configureInviteButtons(CallModel callModel, final Function0<Unit> function0, boolean z2) {
        this.binding.h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureInviteButtons$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        this.binding.i.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureInviteButtons$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        ImageView imageView = this.binding.h;
        m.checkNotNullExpressionValue(imageView, "binding.inviteButton");
        boolean z3 = true;
        int i = 8;
        imageView.setVisibility(callModel.canInvite() && z2 ? 0 : 8);
        TextView textView = this.binding.i;
        m.checkNotNullExpressionValue(textView, "binding.inviteSecondaryButton");
        if (!callModel.canInvite() || z2) {
            z3 = false;
        }
        if (z3) {
            i = 0;
        }
        textView.setVisibility(i);
        ImageView imageView2 = this.binding.h;
        m.checkNotNullExpressionValue(imageView2, "binding.inviteButton");
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        imageView2.setBackgroundTintList(ColorStateList.valueOf(getDefaultButtonBackgroundTint(context, false)));
    }

    /* renamed from: configureMoveToAudienceButton-yox5PQY  reason: not valid java name */
    private final void m55configureMoveToAudienceButtonyox5PQY(final StageRoles stageRoles, final Function0<Unit> function0) {
        ImageView imageView = this.binding.j;
        int i = 0;
        boolean z2 = true;
        if (stageRoles == null || !StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl()) || StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl())) {
            z2 = false;
        }
        if (!z2) {
            i = 8;
        }
        imageView.setVisibility(i);
        ImageView imageView2 = this.binding.j;
        m.checkNotNullExpressionValue(imageView2, "binding.moveToAudienceButton");
        imageView2.setBackgroundTintList(ColorStateList.valueOf(ColorCompat.getColor(imageView.getContext(), (int) R.color.white_alpha_24)));
        imageView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureMoveToAudienceButton$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                function0.invoke();
            }
        });
    }

    /* renamed from: configureMuteButton-P2fzehM  reason: not valid java name */
    private final void m56configureMuteButtonP2fzehM(CallModel callModel, StageRoles stageRoles, final Function0<Unit> function0) {
        CharSequence b2;
        if (stageRoles == null || StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl())) {
            ImageView imageView = this.binding.k;
            m.checkNotNullExpressionValue(imageView, "binding.muteButton");
            imageView.setVisibility(0);
            boolean isMeMutedByAnySource = callModel.isMeMutedByAnySource();
            Context context = getContext();
            m.checkNotNullExpressionValue(context, "context");
            int defaultButtonBackgroundTint = getDefaultButtonBackgroundTint(context, isMeMutedByAnySource);
            ImageView imageView2 = this.binding.k;
            m.checkNotNullExpressionValue(imageView2, "binding.muteButton");
            imageView2.setBackgroundTintList(ColorStateList.valueOf(defaultButtonBackgroundTint));
            this.binding.k.setImageResource(isMeMutedByAnySource ? R.drawable.ic_mic_mute_red_strike_24dp : R.drawable.ic_mic_white_24dp);
            this.binding.k.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureMuteButton$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    Function0.this.invoke();
                }
            });
            ImageView imageView3 = this.binding.k;
            m.checkNotNullExpressionValue(imageView3, "binding.muteButton");
            Context context2 = getContext();
            m.checkNotNullExpressionValue(context2, "context");
            b2 = b.b(context2, isMeMutedByAnySource ? R.string.unmute : R.string.mute, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            imageView3.setContentDescription(b2);
            return;
        }
        ImageView imageView4 = this.binding.k;
        m.checkNotNullExpressionValue(imageView4, "binding.muteButton");
        imageView4.setVisibility(8);
    }

    private final void configureOutputSelectors(VoiceControlsOutputSelectorState voiceControlsOutputSelectorState, final Function0<Unit> function0, boolean z2) {
        FrameLayout frameLayout = this.binding.c;
        m.checkNotNullExpressionValue(frameLayout, "binding.audioOutputContainer");
        int i = 0;
        frameLayout.setVisibility(z2 ? 0 : 8);
        this.binding.f2200b.setImageDrawable(ContextCompat.getDrawable(getContext(), voiceControlsOutputSelectorState.getAudioOutputIconRes()));
        ImageView imageView = this.binding.f2200b;
        m.checkNotNullExpressionValue(imageView, "binding.audioOutputButton");
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        imageView.setBackgroundTintList(ColorStateList.valueOf(getDefaultButtonBackgroundTint(context, voiceControlsOutputSelectorState.isButtonActive())));
        TextView textView = this.binding.e;
        m.checkNotNullExpressionValue(textView, "binding.audioOutputSecondaryButton");
        textView.setVisibility(z2 ^ true ? 0 : 8);
        TextView textView2 = this.binding.e;
        m.checkNotNullExpressionValue(textView2, "binding.audioOutputSecondaryButton");
        ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView2, voiceControlsOutputSelectorState.getAudioOutputIconRes(), 0, 0, 0, 14, null);
        Context context2 = getContext();
        m.checkNotNullExpressionValue(context2, "context");
        int defaultButtonIconTint = getDefaultButtonIconTint(context2, voiceControlsOutputSelectorState.isButtonActive());
        ImageView imageView2 = this.binding.f2200b;
        m.checkNotNullExpressionValue(imageView2, "binding.audioOutputButton");
        ColorCompatKt.tintWithColor(imageView2, defaultButtonIconTint);
        ImageView imageView3 = this.binding.d;
        m.checkNotNullExpressionValue(imageView3, "binding.audioOutputMore");
        ColorCompatKt.tintWithColor(imageView3, defaultButtonIconTint);
        ImageView imageView4 = this.binding.d;
        m.checkNotNullExpressionValue(imageView4, "binding.audioOutputMore");
        if (!voiceControlsOutputSelectorState.getShowMoreOptions()) {
            i = 8;
        }
        imageView4.setVisibility(i);
        this.binding.f2200b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureOutputSelectors$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        this.binding.e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureOutputSelectors$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    /* renamed from: configureRaiseHandButton-fw_bWyM  reason: not valid java name */
    private final void m57configureRaiseHandButtonfw_bWyM(final CallModel callModel, final StageRoles stageRoles, int i, final boolean z2, final Function0<Unit> function0) {
        int i2;
        if (stageRoles == null || (!StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl()) && !StageRoles.m24isAudienceimpl(stageRoles.m29unboximpl()))) {
            NumericBadgingView numericBadgingView = this.binding.n;
            m.checkNotNullExpressionValue(numericBadgingView, "binding.raiseHandBadge");
            numericBadgingView.setVisibility(8);
            return;
        }
        NumericBadgingView numericBadgingView2 = this.binding.n;
        boolean z3 = false;
        numericBadgingView2.setVisibility(0);
        if (!StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl())) {
            i = 0;
        }
        numericBadgingView2.setBadgeNumber(i);
        ImageView imageView = this.binding.o;
        if (StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl())) {
            imageView.setContentDescription(imageView.getResources().getString(R.string.request_to_speak_area_title));
            ViewExtensions.setEnabledAndAlpha$default(imageView, true, 0.0f, 2, null);
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_stage_raised_hand_list));
        } else {
            Resources resources = imageView.getResources();
            if (!callModel.canRequestToSpeak()) {
                i2 = R.string.audience_raise_hand_no_permission;
            } else {
                i2 = callModel.isMyHandRaised() ? R.string.audience_lower_hand : R.string.audience_raise_hand_cta;
            }
            imageView.setContentDescription(resources.getString(i2));
            ViewExtensions.setEnabledAndAlpha(imageView, !z2 && callModel.canRequestToSpeak(), 0.2f);
            imageView.setImageDrawable(ContextCompat.getDrawable(imageView.getContext(), R.drawable.ic_stage_raised_hand));
        }
        if (callModel.isMyHandRaised() && !StageRoles.m26isModeratorimpl(stageRoles.m29unboximpl())) {
            z3 = true;
        }
        Context context = imageView.getContext();
        m.checkNotNullExpressionValue(context, "context");
        int defaultButtonBackgroundTint = getDefaultButtonBackgroundTint(context, z3);
        Context context2 = imageView.getContext();
        m.checkNotNullExpressionValue(context2, "context");
        int defaultButtonIconTint = getDefaultButtonIconTint(context2, z3);
        imageView.setBackgroundTintList(ColorStateList.valueOf(defaultButtonBackgroundTint));
        ColorCompatKt.tintWithColor(imageView, defaultButtonIconTint);
        imageView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureRaiseHandButton$$inlined$apply$lambda$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                function0.invoke();
            }
        });
    }

    private final void configureScreenshareButtons(CallModel callModel, final Function0<Unit> function0, boolean z2) {
        boolean z3 = true;
        boolean z4 = !ChannelUtils.z(callModel.getChannel());
        boolean isStreaming = callModel.isStreaming();
        int i = isStreaming ? R.string.stop_streaming : R.string.mobile_stream_screen_share;
        ImageView imageView = this.binding.p;
        m.checkNotNullExpressionValue(imageView, "binding.screenShareButton");
        int i2 = 0;
        imageView.setVisibility(z4 && z2 ? 0 : 8);
        int i3 = isStreaming ? R.drawable.ic_mobile_screenshare_end_24dp : R.drawable.ic_mobile_screenshare_24dp;
        int i4 = isStreaming ? ViewCompat.MEASURED_STATE_MASK : -1;
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        int defaultButtonBackgroundTint = getDefaultButtonBackgroundTint(context, isStreaming);
        this.binding.p.setImageResource(i3);
        ImageView imageView2 = this.binding.p;
        m.checkNotNullExpressionValue(imageView2, "binding.screenShareButton");
        imageView2.setBackgroundTintList(ColorStateList.valueOf(defaultButtonBackgroundTint));
        ImageView imageView3 = this.binding.p;
        m.checkNotNullExpressionValue(imageView3, "binding.screenShareButton");
        imageView3.setImageTintList(ColorStateList.valueOf(i4));
        this.binding.p.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureScreenshareButtons$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        TextView textView = this.binding.q;
        m.checkNotNullExpressionValue(textView, "binding.screenShareSecondaryButton");
        if (!z4 || z2) {
            z3 = false;
        }
        if (!z3) {
            i2 = 8;
        }
        textView.setVisibility(i2);
        this.binding.q.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureScreenshareButtons$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        TextView textView2 = this.binding.q;
        m.checkNotNullExpressionValue(textView2, "binding.screenShareSecondaryButton");
        ViewExtensions.setCompoundDrawableWithIntrinsicBounds$default(textView2, i3, 0, 0, 0, 14, null);
        this.binding.q.setText(i);
    }

    private final void configureStreamVolume(boolean z2, float f, Function2<? super Float, ? super Boolean, Unit> function2) {
        TextView textView = this.binding.u;
        m.checkNotNullExpressionValue(textView, "binding.streamVolumeLabel");
        int i = 8;
        textView.setVisibility(z2 ? 0 : 8);
        VolumeSliderView volumeSliderView = this.binding.v;
        m.checkNotNullExpressionValue(volumeSliderView, "binding.streamVolumeSlider");
        if (z2) {
            i = 0;
        }
        volumeSliderView.setVisibility(i);
        VolumeSliderView volumeSliderView2 = this.binding.v;
        int roundToInt = a.roundToInt(f);
        SeekBar seekBar = volumeSliderView2.j.d;
        m.checkNotNullExpressionValue(seekBar, "binding.volumeSliderSeekBar");
        seekBar.setProgress(roundToInt);
        this.binding.v.setOnVolumeChange(function2);
    }

    private final void configureVideoButton(CallModel callModel, final Function0<Unit> function0, boolean z2) {
        boolean z3 = true;
        int i = 0;
        boolean z4 = callModel.getSelectedVideoDevice() != null;
        boolean z5 = !callModel.getVideoDevices().isEmpty();
        ImageView imageView = this.binding.w;
        m.checkNotNullExpressionValue(imageView, "binding.videoButton");
        if (!z5 || !z2) {
            z3 = false;
        }
        if (!z3) {
            i = 8;
        }
        imageView.setVisibility(i);
        int i2 = z4 ? ViewCompat.MEASURED_STATE_MASK : -1;
        ImageView imageView2 = this.binding.w;
        m.checkNotNullExpressionValue(imageView2, "binding.videoButton");
        imageView2.setImageTintList(ColorStateList.valueOf(i2));
        Context context = getContext();
        m.checkNotNullExpressionValue(context, "context");
        int defaultButtonBackgroundTint = getDefaultButtonBackgroundTint(context, z4);
        ImageView imageView3 = this.binding.w;
        m.checkNotNullExpressionValue(imageView3, "binding.videoButton");
        imageView3.setBackgroundTintList(ColorStateList.valueOf(defaultButtonBackgroundTint));
        this.binding.w.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureVideoButton$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    private final int getDefaultButtonBackgroundTint(Context context, boolean z2) {
        if (z2) {
            return ColorCompat.getColor(context, (int) R.color.white);
        }
        if (!z2) {
            return ColorCompat.getColor(context, (int) R.color.white_alpha_24);
        }
        throw new NoWhenBranchMatchedException();
    }

    private final int getDefaultButtonIconTint(Context context, boolean z2) {
        if (!z2) {
            return ColorCompat.getColor(context, (int) R.color.white);
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.call_controls_active_button_icon_color, typedValue, true);
        return ColorCompat.getColor(context, typedValue.resourceId);
    }

    /* renamed from: configureUI-3jxq49Y  reason: not valid java name */
    public final void m58configureUI3jxq49Y(CallModel callModel, VoiceControlsOutputSelectorState voiceControlsOutputSelectorState, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, final boolean z7, final Function0<Unit> function0, final Function0<Unit> function02, Function0<Unit> function03, Function0<Unit> function04, Function0<Unit> function05, Function0<Unit> function06, Function0<Unit> function07, boolean z8, float f, Function2<? super Float, ? super Boolean, Unit> function2, final Function0<Unit> function08, int i, Function0<Unit> function09, Function0<Unit> function010, boolean z9, final StageRoles stageRoles) {
        int i2;
        m.checkNotNullParameter(callModel, "model");
        m.checkNotNullParameter(voiceControlsOutputSelectorState, "outputSelectorState");
        m.checkNotNullParameter(function0, "onStopWatchingClick");
        m.checkNotNullParameter(function02, "onDisconnectClick");
        m.checkNotNullParameter(function03, "onAudioOutputClick");
        m.checkNotNullParameter(function04, "onVideoClick");
        m.checkNotNullParameter(function05, "onMuteClick");
        m.checkNotNullParameter(function06, "onInviteClick");
        m.checkNotNullParameter(function07, "onScreenshareClick");
        m.checkNotNullParameter(function2, "onStreamVolumeChange");
        m.checkNotNullParameter(function08, "onDeafenPressed");
        m.checkNotNullParameter(function09, "onRaiseHandClick");
        m.checkNotNullParameter(function010, "onMoveToAudienceClick");
        ImageView imageView = this.binding.t;
        m.checkNotNullExpressionValue(imageView, "binding.stopWatchingButton");
        imageView.setVisibility(z6 ? 0 : 8);
        this.binding.t.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        if (stageRoles == null || StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl())) {
            MaterialTextView materialTextView = this.binding.f2201s;
            m.checkNotNullExpressionValue(materialTextView, "binding.stageDisconnectButtonLarge");
            i2 = 8;
            materialTextView.setVisibility(8);
            ImageView imageView2 = this.binding.g;
            imageView2.setImageResource(stageRoles == null ? R.drawable.ic_call_disconnect_24dp : R.drawable.ic_stage_leave_20dp);
            imageView2.setVisibility(z7 ? 0 : 8);
            imageView2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureUI$$inlined$apply$lambda$2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    function02.invoke();
                }
            });
            m.checkNotNullExpressionValue(imageView2, "binding.disconnectButton…sconnectClick() }\n      }");
        } else {
            ImageView imageView3 = this.binding.g;
            m.checkNotNullExpressionValue(imageView3, "binding.disconnectButton");
            imageView3.setVisibility(8);
            MaterialTextView materialTextView2 = this.binding.f2201s;
            materialTextView2.setVisibility(z7 ? 0 : 8);
            materialTextView2.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureUI$$inlined$apply$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    function02.invoke();
                }
            });
            m.checkNotNullExpressionValue(materialTextView2, "binding.stageDisconnectB…Click()\n        }\n      }");
            i2 = 8;
        }
        Button button = this.binding.m;
        m.checkNotNullExpressionValue(button, "binding.pushToTalkButton");
        if ((stageRoles == null || StageRoles.m27isSpeakerimpl(stageRoles.m29unboximpl())) && z3) {
            i2 = 0;
        }
        button.setVisibility(i2);
        TextView textView = this.binding.f;
        m.checkNotNullExpressionValue(textView, "binding.deafenSecondaryButton");
        b.m(textView, z4 ? R.string.undeafen : R.string.deafen, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        this.binding.f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.VoiceControlsSheetView$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        TextView textView2 = this.binding.f;
        m.checkNotNullExpressionValue(textView2, "binding.deafenSecondaryButton");
        textView2.setActivated(z4);
        m55configureMoveToAudienceButtonyox5PQY(stageRoles, function010);
        m57configureRaiseHandButtonfw_bWyM(callModel, stageRoles, i, z9, function09);
        m56configureMuteButtonP2fzehM(callModel, stageRoles, function05);
        configureOutputSelectors(voiceControlsOutputSelectorState, function03, !z5 && !ChannelUtils.z(callModel.getChannel()));
        configureVideoButton(callModel, function04, z2);
        configureScreenshareButtons(callModel, function07, z5);
        configureStreamVolume(z8, f, function2);
        configureInviteButtons(callModel, function06, ChannelUtils.z(callModel.getChannel()));
    }

    public final int getPeekHeight() {
        LinearLayout linearLayout = this.binding.l;
        m.checkNotNullExpressionValue(linearLayout, "binding.peekContainer");
        return linearLayout.getMeasuredHeight();
    }

    public final void handleSheetState(int i) {
        CardView cardView = this.binding.r;
        m.checkNotNullExpressionValue(cardView, "binding.secondaryActionsCard");
        int i2 = 4;
        if (i != 4 || this.isGestureNavigationEnabled) {
            i2 = 0;
        }
        cardView.setVisibility(i2);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        int i;
        super.onAttachedToWindow();
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        if (resources.getConfiguration().orientation == 1) {
            Resources resources2 = getResources();
            m.checkNotNullExpressionValue(resources2, "resources");
            i = resources2.getDisplayMetrics().widthPixels;
        } else {
            Resources resources3 = getResources();
            m.checkNotNullExpressionValue(resources3, "resources");
            i = resources3.getDisplayMetrics().heightPixels;
        }
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = i;
        setLayoutParams(layoutParams);
    }

    public final void setOnPTTListener(OnPressListener onPressListener) {
        m.checkNotNullParameter(onPressListener, "listener");
        this.binding.m.setOnTouchListener(onPressListener);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceControlsSheetView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m.checkNotNullParameter(context, "context");
        VoiceControlsSheetViewBinding a = VoiceControlsSheetViewBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "VoiceControlsSheetViewBi…ater.from(context), this)");
        this.binding = a;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        this.isGestureNavigationEnabled = DisplayUtils.isGestureNavigationEnabled(resources);
        CardView cardView = a.r;
        m.checkNotNullExpressionValue(cardView, "binding.secondaryActionsCard");
        cardView.setVisibility(this.isGestureNavigationEnabled ? 0 : 4);
    }

    public /* synthetic */ VoiceControlsSheetView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceControlsSheetView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        VoiceControlsSheetViewBinding a = VoiceControlsSheetViewBinding.a(LayoutInflater.from(getContext()), this);
        m.checkNotNullExpressionValue(a, "VoiceControlsSheetViewBi…ater.from(context), this)");
        this.binding = a;
        Resources resources = getResources();
        m.checkNotNullExpressionValue(resources, "resources");
        this.isGestureNavigationEnabled = DisplayUtils.isGestureNavigationEnabled(resources);
        CardView cardView = a.r;
        m.checkNotNullExpressionValue(cardView, "binding.secondaryActionsCard");
        cardView.setVisibility(this.isGestureNavigationEnabled ? 0 : 4);
    }

    public /* synthetic */ VoiceControlsSheetView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
