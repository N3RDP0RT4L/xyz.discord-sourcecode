package com.discord.widgets.voice.controls;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.d.b.a.a;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppTransitionActivity;
import com.discord.databinding.WidgetScreenShareNfxSheetBinding;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.discord.widgets.voice.fullscreen.WidgetCallPreviewFullscreen;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import com.discord.widgets.voice.stream.StreamNavigator;
import d0.z.d.m;
import java.io.Serializable;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetScreenShareNfxSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 (2\u00020\u0001:\u0002()B\u0007¢\u0006\u0004\b'\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0013\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0013\u0010\n\u001a\u00060\u0005j\u0002`\tH\u0002¢\u0006\u0004\b\n\u0010\bJ\u0011\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0002¢\u0006\u0004\b\f\u0010\rJ\u000f\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u000f\u0010\u0010J)\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u000e2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J!\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00172\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ\u0017\u0010\u001f\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001dH\u0016¢\u0006\u0004\b\u001f\u0010 R\u001d\u0010&\u001a\u00020!8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\"\u0010#\u001a\u0004\b$\u0010%¨\u0006*"}, d2 = {"Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;", "Lcom/discord/app/AppBottomSheet;", "", "maybeNavigateToVoiceBottomSheet", "()V", "", "Lcom/discord/primitives/ChannelId;", "getChannelId", "()J", "Lcom/discord/primitives/GuildId;", "getGuildId", "Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;", "getVoiceBottomSheetParams", "()Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;", "", "getContentViewResId", "()I", "requestCode", "resultCode", "Landroid/content/Intent;", "intent", "onActivityResult", "(IILandroid/content/Intent;)V", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "Landroid/content/DialogInterface;", "dialog", "onCancel", "(Landroid/content/DialogInterface;)V", "Lcom/discord/databinding/WidgetScreenShareNfxSheetBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetScreenShareNfxSheetBinding;", "binding", HookHelper.constructorName, "Companion", "VoiceBottomSheetParams", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetScreenShareNfxSheet extends AppBottomSheet {
    private static final String ARG_VOICE_BOTTOM_SHEET_PARAMS = "ARG_VOICE_BOTTOM_SHEET_PARAMS";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetScreenShareNfxSheet$binding$2.INSTANCE, null, 2, null);
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetScreenShareNfxSheet.class, "binding", "getBinding()Lcom/discord/databinding/WidgetScreenShareNfxSheetBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetScreenShareNfxSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0014\u0010\u0015J9\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u00072\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\t¢\u0006\u0004\b\f\u0010\rJ\r\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013¨\u0006\u0016"}, d2 = {"Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;", "voiceBottomSheetParams", "", "show", "(Landroidx/fragment/app/FragmentManager;JJLcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;)V", "", "canShow", "()Z", "", WidgetScreenShareNfxSheet.ARG_VOICE_BOTTOM_SHEET_PARAMS, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, FragmentManager fragmentManager, long j, long j2, VoiceBottomSheetParams voiceBottomSheetParams, int i, Object obj) {
            if ((i & 8) != 0) {
                voiceBottomSheetParams = null;
            }
            companion.show(fragmentManager, j, j2, voiceBottomSheetParams);
        }

        public final boolean canShow() {
            return !SharedPreferencesProvider.INSTANCE.get().getBoolean("CACHE_KEY_SCREEN_SHARE_NFX_SHEET_SHOWN", false);
        }

        public final void show(FragmentManager fragmentManager, long j, long j2, VoiceBottomSheetParams voiceBottomSheetParams) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            WidgetScreenShareNfxSheet widgetScreenShareNfxSheet = new WidgetScreenShareNfxSheet();
            Bundle I = a.I("com.discord.intent.extra.EXTRA_CHANNEL_ID", j);
            I.putLong("com.discord.intent.extra.EXTRA_GUILD_ID", j2);
            I.putSerializable(WidgetScreenShareNfxSheet.ARG_VOICE_BOTTOM_SHEET_PARAMS, voiceBottomSheetParams);
            widgetScreenShareNfxSheet.setArguments(I);
            widgetScreenShareNfxSheet.show(fragmentManager, WidgetScreenShareNfxSheet.class.getName());
            SharedPreferences.Editor edit = SharedPreferencesProvider.INSTANCE.get().edit();
            m.checkNotNullExpressionValue(edit, "editor");
            edit.putBoolean("CACHE_KEY_SCREEN_SHARE_NFX_SHEET_SHOWN", true);
            edit.apply();
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetScreenShareNfxSheet.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u001a\u0010\u001bJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u0010\u001a\u00020\u000fHÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u001a\u0010\u0014\u001a\u00020\u00022\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004R\u0019\u0010\t\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0018\u001a\u0004\b\u0019\u0010\u0007¨\u0006\u001c"}, d2 = {"Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;", "Ljava/io/Serializable;", "", "component1", "()Z", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "component2", "()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "forwardToFullscreenIfVideoActivated", "featureContext", "copy", "(ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "Z", "getForwardToFullscreenIfVideoActivated", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "getFeatureContext", HookHelper.constructorName, "(ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class VoiceBottomSheetParams implements Serializable {
        private final WidgetVoiceBottomSheet.FeatureContext featureContext;
        private final boolean forwardToFullscreenIfVideoActivated;

        public VoiceBottomSheetParams(boolean z2, WidgetVoiceBottomSheet.FeatureContext featureContext) {
            m.checkNotNullParameter(featureContext, "featureContext");
            this.forwardToFullscreenIfVideoActivated = z2;
            this.featureContext = featureContext;
        }

        public static /* synthetic */ VoiceBottomSheetParams copy$default(VoiceBottomSheetParams voiceBottomSheetParams, boolean z2, WidgetVoiceBottomSheet.FeatureContext featureContext, int i, Object obj) {
            if ((i & 1) != 0) {
                z2 = voiceBottomSheetParams.forwardToFullscreenIfVideoActivated;
            }
            if ((i & 2) != 0) {
                featureContext = voiceBottomSheetParams.featureContext;
            }
            return voiceBottomSheetParams.copy(z2, featureContext);
        }

        public final boolean component1() {
            return this.forwardToFullscreenIfVideoActivated;
        }

        public final WidgetVoiceBottomSheet.FeatureContext component2() {
            return this.featureContext;
        }

        public final VoiceBottomSheetParams copy(boolean z2, WidgetVoiceBottomSheet.FeatureContext featureContext) {
            m.checkNotNullParameter(featureContext, "featureContext");
            return new VoiceBottomSheetParams(z2, featureContext);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VoiceBottomSheetParams)) {
                return false;
            }
            VoiceBottomSheetParams voiceBottomSheetParams = (VoiceBottomSheetParams) obj;
            return this.forwardToFullscreenIfVideoActivated == voiceBottomSheetParams.forwardToFullscreenIfVideoActivated && m.areEqual(this.featureContext, voiceBottomSheetParams.featureContext);
        }

        public final WidgetVoiceBottomSheet.FeatureContext getFeatureContext() {
            return this.featureContext;
        }

        public final boolean getForwardToFullscreenIfVideoActivated() {
            return this.forwardToFullscreenIfVideoActivated;
        }

        public int hashCode() {
            boolean z2 = this.forwardToFullscreenIfVideoActivated;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            int i3 = i * 31;
            WidgetVoiceBottomSheet.FeatureContext featureContext = this.featureContext;
            return i3 + (featureContext != null ? featureContext.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("VoiceBottomSheetParams(forwardToFullscreenIfVideoActivated=");
            R.append(this.forwardToFullscreenIfVideoActivated);
            R.append(", featureContext=");
            R.append(this.featureContext);
            R.append(")");
            return R.toString();
        }
    }

    public WidgetScreenShareNfxSheet() {
        super(false, 1, null);
    }

    private final WidgetScreenShareNfxSheetBinding getBinding() {
        return (WidgetScreenShareNfxSheetBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final long getChannelId() {
        return requireArguments().getLong("com.discord.intent.extra.EXTRA_CHANNEL_ID");
    }

    private final long getGuildId() {
        return requireArguments().getLong("com.discord.intent.extra.EXTRA_GUILD_ID");
    }

    private final VoiceBottomSheetParams getVoiceBottomSheetParams() {
        return (VoiceBottomSheetParams) requireArguments().getSerializable(ARG_VOICE_BOTTOM_SHEET_PARAMS);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void maybeNavigateToVoiceBottomSheet() {
        VoiceBottomSheetParams voiceBottomSheetParams = getVoiceBottomSheetParams();
        if (voiceBottomSheetParams == null) {
            return;
        }
        if (TextInVoiceFeatureFlag.Companion.getINSTANCE().isEnabled(Long.valueOf(getGuildId()))) {
            WidgetCallPreviewFullscreen.Companion companion = WidgetCallPreviewFullscreen.Companion;
            Context requireContext = requireContext();
            m.checkNotNullExpressionValue(requireContext, "requireContext()");
            companion.launch(requireContext, getChannelId(), AppTransitionActivity.Transition.TYPE_SLIDE_VERTICAL_WITH_FADE);
            return;
        }
        WidgetVoiceBottomSheet.Companion companion2 = WidgetVoiceBottomSheet.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion2.show(parentFragmentManager, getChannelId(), voiceBottomSheetParams.getForwardToFullscreenIfVideoActivated(), voiceBottomSheetParams.getFeatureContext());
    }

    @Override // com.discord.app.AppBottomSheet
    public int getContentViewResId() {
        return R.layout.widget_screen_share_nfx_sheet;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        StreamNavigator.handleActivityResult(i, i2, intent, new WidgetScreenShareNfxSheet$onActivityResult$1(this));
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnCancelListener
    public void onCancel(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        super.onCancel(dialogInterface);
        maybeNavigateToVoiceBottomSheet();
    }

    @Override // com.discord.app.AppBottomSheet, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.WidgetScreenShareNfxSheet$onViewCreated$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                StreamNavigator.requestStartStream(WidgetScreenShareNfxSheet.this);
            }
        });
        getBinding().f2497b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.WidgetScreenShareNfxSheet$onViewCreated$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetScreenShareNfxSheet.this.dismiss();
                WidgetScreenShareNfxSheet.this.maybeNavigateToVoiceBottomSheet();
            }
        });
    }
}
