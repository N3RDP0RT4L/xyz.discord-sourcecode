package com.discord.widgets.voice.controls;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentManager;
import b.a.j.a;
import com.discord.app.AppComponent;
import com.discord.app.AppLog;
import com.discord.databinding.AnchoredVoiceControlsViewBinding;
import com.discord.floating_view_manager.FloatingViewGravity;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreAudioManagerV2;
import com.discord.tooltips.SparkleView;
import com.discord.tooltips.TooltipManager;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.press.OnPressListener;
import com.discord.widgets.voice.controls.WidgetScreenShareNfxSheet;
import com.discord.widgets.voice.model.CameraState;
import com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet;
import com.google.android.material.button.MaterialButton;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import xyz.discord.R;
/* compiled from: AnchoredVoiceControlsView.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B'\b\u0007\u0012\u0006\u00102\u001a\u000201\u0012\n\b\u0002\u00104\u001a\u0004\u0018\u000103\u0012\b\b\u0002\u00106\u001a\u000205¢\u0006\u0004\b7\u00108Jk\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\n\u0010\n\u001a\u00060\bj\u0002`\t2\n\u0010\f\u001a\u00060\bj\u0002`\u000b2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000e2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010H\u0002¢\u0006\u0004\b\u0014\u0010\u0015JÉ\u0001\u0010#\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001d2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00062\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\n\u0010\n\u001a\u00060\bj\u0002`\t2\n\u0010\f\u001a\u00060\bj\u0002`\u000b2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b#\u0010$J\r\u0010%\u001a\u00020\u0011¢\u0006\u0004\b%\u0010&J!\u0010)\u001a\u00020\u00112\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00110'¢\u0006\u0004\b)\u0010*R\u0016\u0010,\u001a\u00020+8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b,\u0010-R\u0016\u0010/\u001a\u00020.8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b/\u00100¨\u00069"}, d2 = {"Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;", "Landroid/widget/FrameLayout;", "", "showSparkle", "Lcom/discord/app/AppComponent;", "appComponent", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "forwardToFullscreenIfVideoActivated", "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;", "featureContext", "Lkotlin/Function0;", "", "onNavigateToScreenShareNfxSheet", "onScreenSharePressed", "configureScreenShareButtonSparkle", "(ZLcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;JJZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "inputMode", "Lcom/discord/stores/StoreAudioManagerV2$State;", "audioManagerState", "isMuted", "isScreensharing", "showScreenShareSparkle", "Lcom/discord/widgets/voice/model/CameraState;", "cameraState", "onMutePressed", "onSpeakerButtonPressed", "onCameraButtonPressed", "onDisconnectPressed", "configureUI", "(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioManagerV2$State;ZZZLcom/discord/widgets/voice/model/CameraState;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;JJZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)V", "hidePtt", "()V", "Lkotlin/Function1;", "onPttPressed", "setOnPttPressedListener", "(Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/tooltips/TooltipManager;", "tooltipManager", "Lcom/discord/tooltips/TooltipManager;", "Lcom/discord/databinding/AnchoredVoiceControlsViewBinding;", "binding", "Lcom/discord/databinding/AnchoredVoiceControlsViewBinding;", "Landroid/content/Context;", "context", "Landroid/util/AttributeSet;", "attrs", "", "defStyleAttr", HookHelper.constructorName, "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AnchoredVoiceControlsView extends FrameLayout {
    private final AnchoredVoiceControlsViewBinding binding;
    private final TooltipManager tooltipManager;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            MediaEngineConnection.InputMode.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[MediaEngineConnection.InputMode.PUSH_TO_TALK.ordinal()] = 1;
        }
    }

    public AnchoredVoiceControlsView(Context context) {
        this(context, null, 0, 6, null);
    }

    public AnchoredVoiceControlsView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    public /* synthetic */ AnchoredVoiceControlsView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    private final void configureScreenShareButtonSparkle(boolean z2, AppComponent appComponent, final FragmentManager fragmentManager, final long j, final long j2, final boolean z3, final WidgetVoiceBottomSheet.FeatureContext featureContext, final Function0<Unit> function0, final Function0<Unit> function02) {
        if (z2) {
            TooltipManager tooltipManager = this.tooltipManager;
            TooltipManager.b bVar = ScreenShareButtonSparkleTooltip.INSTANCE;
            Objects.requireNonNull(tooltipManager);
            m.checkNotNullParameter(bVar, "tooltip");
            if (!(tooltipManager.a.get(bVar.getTooltipName()) != null)) {
                ImageView imageView = this.binding.f;
                m.checkNotNullExpressionValue(imageView, "binding.voiceFullscreenControlsScreenshare");
                Context context = imageView.getContext();
                m.checkNotNullExpressionValue(context, "binding.voiceFullscreenControlsScreenshare.context");
                SparkleView sparkleView = new SparkleView(context, null);
                sparkleView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        TooltipManager tooltipManager2;
                        tooltipManager2 = AnchoredVoiceControlsView.this.tooltipManager;
                        tooltipManager2.a(ScreenShareButtonSparkleTooltip.INSTANCE);
                        WidgetScreenShareNfxSheet.Companion companion = WidgetScreenShareNfxSheet.Companion;
                        if (companion.canShow()) {
                            companion.show(fragmentManager, j, j2, new WidgetScreenShareNfxSheet.VoiceBottomSheetParams(z3, featureContext));
                            function0.invoke();
                            return;
                        }
                        function02.invoke();
                    }
                });
                TooltipManager tooltipManager2 = this.tooltipManager;
                View view = this.binding.f;
                m.checkNotNullExpressionValue(view, "binding.voiceFullscreenControlsScreenshare");
                FloatingViewGravity floatingViewGravity = FloatingViewGravity.CENTER;
                Observable<R> F = appComponent.getUnsubscribeSignal().F(AnchoredVoiceControlsView$configureScreenShareButtonSparkle$2.INSTANCE);
                m.checkNotNullExpressionValue(F, "appComponent.unsubscribeSignal.map { Unit }");
                tooltipManager2.d(view, sparkleView, bVar, floatingViewGravity, 0, 0, false, F);
                return;
            }
        }
        if (!z2) {
            this.tooltipManager.c(ScreenShareButtonSparkleTooltip.INSTANCE);
        }
    }

    public final void configureUI(MediaEngineConnection.InputMode inputMode, StoreAudioManagerV2.State state, boolean z2, boolean z3, boolean z4, CameraState cameraState, final Function0<Unit> function0, final Function0<Unit> function02, final Function0<Unit> function03, final Function0<Unit> function04, final Function0<Unit> function05, AppComponent appComponent, FragmentManager fragmentManager, Function0<Unit> function06, long j, long j2, boolean z5, WidgetVoiceBottomSheet.FeatureContext featureContext) {
        String str;
        m.checkNotNullParameter(inputMode, "inputMode");
        m.checkNotNullParameter(state, "audioManagerState");
        m.checkNotNullParameter(cameraState, "cameraState");
        m.checkNotNullParameter(function0, "onMutePressed");
        m.checkNotNullParameter(function02, "onScreenSharePressed");
        m.checkNotNullParameter(function03, "onSpeakerButtonPressed");
        m.checkNotNullParameter(function04, "onCameraButtonPressed");
        m.checkNotNullParameter(function05, "onDisconnectPressed");
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(function06, "onNavigateToScreenShareNfxSheet");
        m.checkNotNullParameter(featureContext, "featureContext");
        MaterialButton materialButton = this.binding.e;
        m.checkNotNullExpressionValue(materialButton, "binding.voiceFullscreenControlsPtt");
        materialButton.setVisibility(inputMode.ordinal() != 1 ? 8 : 0);
        this.binding.f2077b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.AnchoredVoiceControlsView$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        ImageView imageView = this.binding.f2077b;
        m.checkNotNullExpressionValue(imageView, "binding.voiceFullscreenControlsCamera");
        imageView.setVisibility(cameraState != CameraState.CAMERA_DISABLED ? 0 : 8);
        if (cameraState == CameraState.CAMERA_ON) {
            ImageView imageView2 = this.binding.f2077b;
            m.checkNotNullExpressionValue(imageView2, "binding.voiceFullscreenControlsCamera");
            imageView2.setImageTintList(ColorStateList.valueOf(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorInteractiveActive)));
            ImageView imageView3 = this.binding.f2077b;
            m.checkNotNullExpressionValue(imageView3, "binding.voiceFullscreenControlsCamera");
            imageView3.setContentDescription(getContext().getString(R.string.camera_on));
        } else {
            ImageView imageView4 = this.binding.f2077b;
            m.checkNotNullExpressionValue(imageView4, "binding.voiceFullscreenControlsCamera");
            imageView4.setImageTintList(ColorStateList.valueOf(ColorCompat.getThemedColor(getContext(), (int) R.attr.colorInteractiveNormal)));
            ImageView imageView5 = this.binding.f2077b;
            m.checkNotNullExpressionValue(imageView5, "binding.voiceFullscreenControlsCamera");
            imageView5.setContentDescription(getContext().getString(R.string.camera_off));
        }
        if (state.getActiveAudioDevice() == DiscordAudioManager.DeviceTypes.BLUETOOTH_HEADSET) {
            this.binding.g.setImageResource(R.drawable.ic_audio_output_bluetooth_white_24dp);
        } else {
            this.binding.g.setImageResource(R.drawable.ic_audio_output_white_24dp);
        }
        if (state.getActiveAudioDevice() == DiscordAudioManager.DeviceTypes.EARPIECE || state.getActiveAudioDevice() == DiscordAudioManager.DeviceTypes.WIRED_HEADSET) {
            ImageView imageView6 = this.binding.g;
            m.checkNotNullExpressionValue(imageView6, "binding.voiceFullscreenControlsSpeaker");
            int themedColor = ColorCompat.getThemedColor(imageView6.getContext(), (int) R.attr.colorInteractiveNormal);
            ImageView imageView7 = this.binding.g;
            m.checkNotNullExpressionValue(imageView7, "binding.voiceFullscreenControlsSpeaker");
            imageView7.setImageTintList(ColorStateList.valueOf(themedColor));
        } else {
            ImageView imageView8 = this.binding.g;
            m.checkNotNullExpressionValue(imageView8, "binding.voiceFullscreenControlsSpeaker");
            int themedColor2 = ColorCompat.getThemedColor(imageView8.getContext(), (int) R.attr.colorInteractiveActive);
            ImageView imageView9 = this.binding.g;
            m.checkNotNullExpressionValue(imageView9, "binding.voiceFullscreenControlsSpeaker");
            imageView9.setImageTintList(ColorStateList.valueOf(themedColor2));
        }
        ImageView imageView10 = this.binding.g;
        m.checkNotNullExpressionValue(imageView10, "binding.voiceFullscreenControlsSpeaker");
        imageView10.setActivated(state.getActiveAudioDevice() == DiscordAudioManager.DeviceTypes.SPEAKERPHONE);
        this.binding.g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.AnchoredVoiceControlsView$configureUI$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        ImageView imageView11 = this.binding.d;
        m.checkNotNullExpressionValue(imageView11, "binding.voiceFullscreenControlsMuteState");
        imageView11.setActivated(z2);
        ImageView imageView12 = this.binding.d;
        m.checkNotNullExpressionValue(imageView12, "binding.voiceFullscreenControlsMuteState");
        imageView12.setContentDescription(getContext().getString(z2 ? R.string.unmute : R.string.mute));
        this.binding.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.AnchoredVoiceControlsView$configureUI$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        ImageView imageView13 = this.binding.f;
        m.checkNotNullExpressionValue(imageView13, "binding.voiceFullscreenControlsScreenshare");
        imageView13.setActivated(z3);
        this.binding.f.setImageResource(z3 ? R.drawable.ic_mobile_screenshare_end_24dp : R.drawable.ic_mobile_screenshare_24dp);
        ImageView imageView14 = this.binding.f;
        m.checkNotNullExpressionValue(imageView14, "binding.voiceFullscreenControlsScreenshare");
        Context context = getContext();
        if (context != null) {
            str = context.getString(z3 ? R.string.stop_streaming : R.string.screenshare_screen);
        } else {
            str = null;
        }
        imageView14.setContentDescription(str);
        this.binding.f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.AnchoredVoiceControlsView$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
        configureScreenShareButtonSparkle(z4, appComponent, fragmentManager, j, j2, z5, featureContext, function06, function02);
        this.binding.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.controls.AnchoredVoiceControlsView$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Function0.this.invoke();
            }
        });
    }

    public final void hidePtt() {
        MaterialButton materialButton = this.binding.e;
        m.checkNotNullExpressionValue(materialButton, "binding.voiceFullscreenControlsPtt");
        materialButton.setVisibility(8);
    }

    public final void setOnPttPressedListener(Function1<? super Boolean, Unit> function1) {
        m.checkNotNullParameter(function1, "onPttPressed");
        this.binding.e.setOnTouchListener(new OnPressListener(new AnchoredVoiceControlsView$setOnPttPressedListener$1(function1)));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public AnchoredVoiceControlsView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m.checkNotNullParameter(context, "context");
        View inflate = LayoutInflater.from(context).inflate(R.layout.anchored_voice_controls_view, (ViewGroup) this, false);
        addView(inflate);
        int i2 = R.id.voice_fullscreen_controls_camera;
        ImageView imageView = (ImageView) inflate.findViewById(R.id.voice_fullscreen_controls_camera);
        if (imageView != null) {
            i2 = R.id.voice_fullscreen_controls_controls_wrap;
            LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.voice_fullscreen_controls_controls_wrap);
            if (linearLayout != null) {
                i2 = R.id.voice_fullscreen_controls_disconnect;
                ImageView imageView2 = (ImageView) inflate.findViewById(R.id.voice_fullscreen_controls_disconnect);
                if (imageView2 != null) {
                    i2 = R.id.voice_fullscreen_controls_mute_state;
                    ImageView imageView3 = (ImageView) inflate.findViewById(R.id.voice_fullscreen_controls_mute_state);
                    if (imageView3 != null) {
                        i2 = R.id.voice_fullscreen_controls_ptt;
                        MaterialButton materialButton = (MaterialButton) inflate.findViewById(R.id.voice_fullscreen_controls_ptt);
                        if (materialButton != null) {
                            i2 = R.id.voice_fullscreen_controls_screenshare;
                            ImageView imageView4 = (ImageView) inflate.findViewById(R.id.voice_fullscreen_controls_screenshare);
                            if (imageView4 != null) {
                                i2 = R.id.voice_fullscreen_controls_speaker;
                                ImageView imageView5 = (ImageView) inflate.findViewById(R.id.voice_fullscreen_controls_speaker);
                                if (imageView5 != null) {
                                    AnchoredVoiceControlsViewBinding anchoredVoiceControlsViewBinding = new AnchoredVoiceControlsViewBinding((LinearLayout) inflate, imageView, linearLayout, imageView2, imageView3, materialButton, imageView4, imageView5);
                                    m.checkNotNullExpressionValue(anchoredVoiceControlsViewBinding, "AnchoredVoiceControlsVie…rom(context), this, true)");
                                    this.binding = anchoredVoiceControlsViewBinding;
                                    AppLog appLog = AppLog.g;
                                    m.checkNotNullParameter(appLog, "logger");
                                    WeakReference<a> weakReference = a.b.a;
                                    TooltipManager tooltipManager = null;
                                    a aVar = weakReference != null ? weakReference.get() : null;
                                    if (aVar == null) {
                                        aVar = new a(appLog);
                                        a.b.a = new WeakReference<>(aVar);
                                    }
                                    a aVar2 = aVar;
                                    TooltipManager.a aVar3 = TooltipManager.a.d;
                                    m.checkNotNullParameter(aVar2, "floatingViewManager");
                                    WeakReference<TooltipManager> weakReference2 = TooltipManager.a.a;
                                    tooltipManager = weakReference2 != null ? weakReference2.get() : tooltipManager;
                                    if (tooltipManager == null) {
                                        tooltipManager = new TooltipManager((b.a.v.a) TooltipManager.a.f2787b.getValue(), (Set) TooltipManager.a.c.getValue(), 0, aVar2, 4);
                                        TooltipManager.a.a = new WeakReference<>(tooltipManager);
                                    }
                                    this.tooltipManager = tooltipManager;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(inflate.getResources().getResourceName(i2)));
    }
}
