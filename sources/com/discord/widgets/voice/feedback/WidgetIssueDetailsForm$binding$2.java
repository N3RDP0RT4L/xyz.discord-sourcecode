package com.discord.widgets.voice.feedback;

import android.view.View;
import androidx.core.widget.NestedScrollView;
import com.discord.databinding.WidgetIssueDetailsFormBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetIssueDetailsForm.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetIssueDetailsFormBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetIssueDetailsFormBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetIssueDetailsForm$binding$2 extends k implements Function1<View, WidgetIssueDetailsFormBinding> {
    public static final WidgetIssueDetailsForm$binding$2 INSTANCE = new WidgetIssueDetailsForm$binding$2();

    public WidgetIssueDetailsForm$binding$2() {
        super(1, WidgetIssueDetailsFormBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetIssueDetailsFormBinding;", 0);
    }

    public final WidgetIssueDetailsFormBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.issue_details_cx_prompt;
        LinkifiedTextView linkifiedTextView = (LinkifiedTextView) view.findViewById(R.id.issue_details_cx_prompt);
        if (linkifiedTextView != null) {
            i = R.id.issue_details_input;
            TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.issue_details_input);
            if (textInputLayout != null) {
                i = R.id.issue_details_submit_button;
                MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.issue_details_submit_button);
                if (materialButton != null) {
                    return new WidgetIssueDetailsFormBinding((NestedScrollView) view, linkifiedTextView, textInputLayout, materialButton);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
