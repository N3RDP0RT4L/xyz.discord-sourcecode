package com.discord.widgets.voice.feedback.stream;

import androidx.core.app.NotificationCompat;
import com.discord.models.experiments.domain.Experiment;
import com.discord.widgets.voice.feedback.stream.StreamFeedbackSheetViewModel;
import j0.k.b;
import kotlin.Metadata;
/* compiled from: StreamFeedbackSheetViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/experiments/domain/Experiment;", "experiment", "Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/experiments/domain/Experiment;)Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class StreamFeedbackSheetViewModel$Companion$observeStoreState$1<T, R> implements b<Experiment, StreamFeedbackSheetViewModel.StoreState> {
    public static final StreamFeedbackSheetViewModel$Companion$observeStoreState$1 INSTANCE = new StreamFeedbackSheetViewModel$Companion$observeStoreState$1();

    public final StreamFeedbackSheetViewModel.StoreState call(Experiment experiment) {
        boolean z2 = true;
        if (experiment == null || experiment.getBucket() != 1) {
            z2 = false;
        }
        return new StreamFeedbackSheetViewModel.StoreState(z2);
    }
}
