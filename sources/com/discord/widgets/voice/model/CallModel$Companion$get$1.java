package com.discord.widgets.voice.model;

import androidx.core.app.NotificationCompat;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.models.embeddedactivities.EmbeddedActivity;
import com.discord.models.experiments.domain.Experiment;
import com.discord.models.guild.Guild;
import com.discord.rtcconnection.RtcConnection;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreGuildScheduledEvents;
import com.discord.stores.StoreStream;
import com.discord.utilities.rx.ObservableCombineLatestOverloadsKt;
import com.discord.utilities.voice.VoiceChannelJoinability;
import com.discord.utilities.voice.VoiceChannelJoinabilityUtils;
import com.discord.widgets.voice.model.CallModel;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.e.k;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function10;
import rx.Observable;
/* compiled from: CallModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0007\u001a\u001e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003 \u0004*\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lrx/Observable;", "Lcom/discord/widgets/voice/model/CallModel;", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CallModel$Companion$get$1<T, R> implements b<Channel, Observable<? extends CallModel>> {
    public final /* synthetic */ long $channelId;

    public CallModel$Companion$get$1(long j) {
        this.$channelId = j;
    }

    public final Observable<? extends CallModel> call(final Channel channel) {
        if (channel == null) {
            return new k(null);
        }
        return (Observable<R>) StoreStream.Companion.getEmbeddedActivities().observeEmbeddedActivitiesForChannel(this.$channelId).Y(new b<Map<Long, ? extends EmbeddedActivity>, Observable<? extends CallModel>>() { // from class: com.discord.widgets.voice.model.CallModel$Companion$get$1.1

            /* compiled from: CallModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/experiments/domain/Experiment;", "experiment", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/experiments/domain/Experiment;)Ljava/lang/Integer;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.voice.model.CallModel$Companion$get$1$1$1  reason: invalid class name and collision with other inner class name */
            /* loaded from: classes2.dex */
            public static final class C02661<T, R> implements b<Experiment, Integer> {
                public static final C02661 INSTANCE = new C02661();

                public final Integer call(Experiment experiment) {
                    return Integer.valueOf(experiment != null ? experiment.getBucket() : 0);
                }
            }

            /* compiled from: CallModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/models/experiments/domain/Experiment;", "experiment", "", "kotlin.jvm.PlatformType", NotificationCompat.CATEGORY_CALL, "(Lcom/discord/models/experiments/domain/Experiment;)Ljava/lang/Integer;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.voice.model.CallModel$Companion$get$1$1$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2<T, R> implements b<Experiment, Integer> {
                public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

                public final Integer call(Experiment experiment) {
                    return Integer.valueOf(experiment != null ? experiment.getBucket() : 0);
                }
            }

            /* compiled from: CallModel.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u0001\u001a\u00020\u00002\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u000e\u0010\b\u001a\n \u0007*\u0004\u0018\u00010\u00060\u00062\u000e\u0010\t\u001a\n \u0007*\u0004\u0018\u00010\u00060\u00062\u0006\u0010\u000b\u001a\u00020\n2\b\u0010\r\u001a\u0004\u0018\u00010\f2\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u000ej\u0002`\u0015\u0012\u0004\u0012\u00020\u00160\u0014H\n¢\u0006\u0004\b\u0019\u0010\u001a"}, d2 = {"Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;", "chunk", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "activeStream", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "rtcConnectionAnalyticsInfo", "", "kotlin.jvm.PlatformType", "callFeedbackTriggerRateDenominator", "streamFeedbackTriggerRateDenominator", "Lcom/discord/utilities/voice/VoiceChannelJoinability;", "voiceChannelJoinability", "Lcom/discord/models/guild/Guild;", "guild", "", "Lcom/discord/primitives/ChannelId;", "selectedVoiceChannelId", "", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvents", "", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/api/application/Application;", "applications", "Lcom/discord/widgets/voice/model/CallModel;", "invoke", "(Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection$Metadata;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/utilities/voice/VoiceChannelJoinability;Lcom/discord/models/guild/Guild;JLjava/util/List;Ljava/util/Map;)Lcom/discord/widgets/voice/model/CallModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.voice.model.CallModel$Companion$get$1$1$3  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass3 extends o implements Function10<CallModel.Companion.Chunk, StoreApplicationStreaming.ActiveApplicationStream, RtcConnection.Metadata, Integer, Integer, VoiceChannelJoinability, Guild, Long, List<? extends GuildScheduledEvent>, Map<Long, ? extends Application>, CallModel> {
                public final /* synthetic */ Map $embeddedActivities;

                /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
                public AnonymousClass3(Map map) {
                    super(10);
                    this.$embeddedActivities = map;
                }

                @Override // kotlin.jvm.functions.Function10
                public /* bridge */ /* synthetic */ CallModel invoke(CallModel.Companion.Chunk chunk, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, RtcConnection.Metadata metadata, Integer num, Integer num2, VoiceChannelJoinability voiceChannelJoinability, Guild guild, Long l, List<? extends GuildScheduledEvent> list, Map<Long, ? extends Application> map) {
                    return invoke(chunk, activeApplicationStream, metadata, num, num2, voiceChannelJoinability, guild, l.longValue(), (List<GuildScheduledEvent>) list, (Map<Long, Application>) map);
                }

                /* JADX WARN: Removed duplicated region for block: B:17:0x00a2  */
                /* JADX WARN: Removed duplicated region for block: B:23:0x00a5 A[SYNTHETIC] */
                /*
                    Code decompiled incorrectly, please refer to instructions dump.
                    To view partially-correct add '--show-bad-code' argument
                */
                public final com.discord.widgets.voice.model.CallModel invoke(com.discord.widgets.voice.model.CallModel.Companion.Chunk r27, com.discord.stores.StoreApplicationStreaming.ActiveApplicationStream r28, com.discord.rtcconnection.RtcConnection.Metadata r29, java.lang.Integer r30, java.lang.Integer r31, com.discord.utilities.voice.VoiceChannelJoinability r32, com.discord.models.guild.Guild r33, long r34, java.util.List<com.discord.api.guildscheduledevent.GuildScheduledEvent> r36, java.util.Map<java.lang.Long, com.discord.api.application.Application> r37) {
                    /*
                        r26 = this;
                        r0 = r26
                        java.lang.String r1 = "chunk"
                        r2 = r27
                        d0.z.d.m.checkNotNullParameter(r2, r1)
                        java.lang.String r1 = "voiceChannelJoinability"
                        r15 = r32
                        d0.z.d.m.checkNotNullParameter(r15, r1)
                        java.lang.String r1 = "guildScheduledEvents"
                        r3 = r36
                        d0.z.d.m.checkNotNullParameter(r3, r1)
                        java.lang.String r1 = "applications"
                        r14 = r37
                        d0.z.d.m.checkNotNullParameter(r14, r1)
                        com.discord.widgets.voice.model.CallModel$Companion r1 = com.discord.widgets.voice.model.CallModel.Companion
                        com.discord.widgets.voice.model.CallModel$Companion$get$1$1 r4 = com.discord.widgets.voice.model.CallModel$Companion$get$1.AnonymousClass1.this
                        com.discord.api.channel.Channel r4 = r2
                        long r5 = r27.getMyUserId()
                        long r7 = r27.getTimeConnectedMs()
                        com.discord.stores.StoreMediaSettings$VoiceConfiguration r9 = r27.getVoiceConfig()
                        java.util.Map r10 = r27.getVoiceParticipants()
                        java.lang.Long r11 = r27.getChannelPermissions()
                        java.util.List r12 = r27.getVideoDevices()
                        com.discord.widgets.voice.model.CallModel$Companion$get$1$1 r13 = com.discord.widgets.voice.model.CallModel$Companion$get$1.AnonymousClass1.this
                        com.discord.api.channel.Channel r13 = r2
                        long r16 = r13.h()
                        r18 = 0
                        int r19 = (r16 > r34 ? 1 : (r16 == r34 ? 0 : -1))
                        if (r19 != 0) goto L4d
                        r16 = 1
                        goto L4f
                    L4d:
                        r16 = 0
                    L4f:
                        co.discord.media_engine.VideoInputDeviceDescription r17 = r27.getSelectedVideoDevice()
                        java.lang.String r13 = "callFeedbackTriggerRateDenominator"
                        r2 = r30
                        d0.z.d.m.checkNotNullExpressionValue(r2, r13)
                        int r20 = r30.intValue()
                        java.lang.String r2 = "streamFeedbackTriggerRateDenominator"
                        r13 = r31
                        d0.z.d.m.checkNotNullExpressionValue(r13, r2)
                        int r21 = r31.intValue()
                        com.discord.stores.StoreAudioManagerV2$State r23 = r27.getAudioManagerState()
                        java.util.ArrayList r13 = new java.util.ArrayList
                        r13.<init>()
                        java.util.Iterator r2 = r36.iterator()
                    L76:
                        boolean r3 = r2.hasNext()
                        if (r3 == 0) goto Lac
                        java.lang.Object r3 = r2.next()
                        r22 = r3
                        com.discord.api.guildscheduledevent.GuildScheduledEvent r22 = (com.discord.api.guildscheduledevent.GuildScheduledEvent) r22
                        java.lang.Long r22 = r22.b()
                        if (r22 != 0) goto L8d
                        r27 = r2
                        goto L9f
                    L8d:
                        long r24 = r22.longValue()
                        r27 = r2
                        com.discord.widgets.voice.model.CallModel$Companion$get$1$1 r2 = com.discord.widgets.voice.model.CallModel$Companion$get$1.AnonymousClass1.this
                        com.discord.widgets.voice.model.CallModel$Companion$get$1 r2 = com.discord.widgets.voice.model.CallModel$Companion$get$1.this
                        long r14 = r2.$channelId
                        int r2 = (r24 > r14 ? 1 : (r24 == r14 ? 0 : -1))
                        if (r2 != 0) goto L9f
                        r2 = 1
                        goto La0
                    L9f:
                        r2 = 0
                    La0:
                        if (r2 == 0) goto La5
                        r13.add(r3)
                    La5:
                        r2 = r27
                        r15 = r32
                        r14 = r37
                        goto L76
                    Lac:
                        java.util.Map r2 = r0.$embeddedActivities
                        r22 = r2
                        java.lang.String r3 = "embeddedActivities"
                        d0.z.d.m.checkNotNullExpressionValue(r2, r3)
                        r2 = r1
                        r3 = r4
                        r4 = r33
                        r1 = r13
                        r13 = r16
                        r14 = r17
                        r15 = r28
                        r16 = r29
                        r17 = r20
                        r18 = r21
                        r19 = r23
                        r20 = r32
                        r21 = r1
                        r23 = r37
                        com.discord.widgets.voice.model.CallModel r1 = com.discord.widgets.voice.model.CallModel.Companion.access$create(r2, r3, r4, r5, r7, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)
                        return r1
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.voice.model.CallModel$Companion$get$1.AnonymousClass1.AnonymousClass3.invoke(com.discord.widgets.voice.model.CallModel$Companion$Chunk, com.discord.stores.StoreApplicationStreaming$ActiveApplicationStream, com.discord.rtcconnection.RtcConnection$Metadata, java.lang.Integer, java.lang.Integer, com.discord.utilities.voice.VoiceChannelJoinability, com.discord.models.guild.Guild, long, java.util.List, java.util.Map):com.discord.widgets.voice.model.CallModel");
                }
            }

            @Override // j0.k.b
            public /* bridge */ /* synthetic */ Observable<? extends CallModel> call(Map<Long, ? extends EmbeddedActivity> map) {
                return call2((Map<Long, EmbeddedActivity>) map);
            }

            /* renamed from: call  reason: avoid collision after fix types in other method */
            public final Observable<? extends CallModel> call2(Map<Long, EmbeddedActivity> map) {
                Observable observeChunk;
                Observable observeJoinability;
                Set<Long> keySet = map.keySet();
                observeChunk = CallModel.Companion.observeChunk(channel);
                StoreStream.Companion companion = StoreStream.Companion;
                Observable<StoreApplicationStreaming.ActiveApplicationStream> observeActiveStream = companion.getApplicationStreaming().observeActiveStream();
                Observable<RtcConnection.Metadata> observeRtcConnectionMetadata = companion.getRtcConnection().observeRtcConnectionMetadata();
                Observable<R> F = companion.getExperiments().observeUserExperiment("2019-12_android_call_feedback_sheet_sample_rate_denominator", true).F(C02661.INSTANCE);
                m.checkNotNullExpressionValue(F, "StoreStream\n            …experiment?.bucket ?: 0 }");
                Observable<R> F2 = companion.getExperiments().observeUserExperiment("2020-09_stream_feedback_sheet_sample_rate_denominator", true).F(AnonymousClass2.INSTANCE);
                m.checkNotNullExpressionValue(F2, "StoreStream\n            …experiment?.bucket ?: 0 }");
                observeJoinability = VoiceChannelJoinabilityUtils.INSTANCE.observeJoinability(CallModel$Companion$get$1.this.$channelId, (r19 & 2) != 0 ? StoreStream.Companion.getChannels() : null, (r19 & 4) != 0 ? StoreStream.Companion.getGuilds() : null, (r19 & 8) != 0 ? StoreStream.Companion.getPermissions() : null, (r19 & 16) != 0 ? StoreStream.Companion.getVoiceStates() : null, (r19 & 32) != 0 ? StoreStream.Companion.getVoiceChannelSelected() : null, (r19 & 64) != 0 ? StoreStream.Companion.getStageInstances() : null);
                return ObservableCombineLatestOverloadsKt.combineLatest(observeChunk, observeActiveStream, observeRtcConnectionMetadata, F, F2, observeJoinability, companion.getGuilds().observeFromChannelId(CallModel$Companion$get$1.this.$channelId), companion.getVoiceChannelSelected().observeSelectedVoiceChannelId(), StoreGuildScheduledEvents.observeGuildScheduledEvents$default(companion.getGuildScheduledEvents(), channel.f(), false, 2, null), companion.getApplication().observeApplications(keySet), new AnonymousClass3(map));
            }
        });
    }
}
