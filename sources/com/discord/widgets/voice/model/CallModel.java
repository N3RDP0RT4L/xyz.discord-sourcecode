package com.discord.widgets.voice.model;

import a0.a.a.b;
import andhook.lib.HookHelper;
import b.c.a.a0.d;
import b.d.b.a.a;
import co.discord.media_engine.VideoInputDeviceDescription;
import com.discord.api.application.Application;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.api.guild.GuildMaxVideoChannelUsers;
import com.discord.api.guildscheduledevent.GuildScheduledEvent;
import com.discord.api.permission.Permission;
import com.discord.api.stageinstance.StageInstance;
import com.discord.api.voice.state.VoiceState;
import com.discord.models.domain.ModelApplicationStream;
import com.discord.models.embeddedactivities.EmbeddedActivity;
import com.discord.models.guild.Guild;
import com.discord.rtcconnection.RtcConnection;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.stores.StoreApplicationStreaming;
import com.discord.stores.StoreAudioManagerV2;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreVoiceParticipants;
import com.discord.utilities.guildscheduledevent.GuildScheduledEventUtilities;
import com.discord.utilities.permissions.PermissionUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.time.Clock;
import com.discord.utilities.voice.VoiceChannelJoinability;
import com.discord.widgets.voice.model.CallModel;
import d0.z.d.m;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.functions.Func8;
/* compiled from: CallModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¼\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0019\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u001f\b\u0086\b\u0018\u0000 \u009a\u00012\u00020\u0001:\u0002\u009a\u0001B\u0089\u0002\u0012\u0016\u0010T\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`+\u0012\u0004\u0012\u00020&0*\u0012\n\u0010U\u001a\u00060\bj\u0002`+\u0012\u0006\u0010V\u001a\u00020\b\u0012\f\u0010W\u001a\b\u0012\u0004\u0012\u00020201\u0012\b\u0010X\u001a\u0004\u0018\u000102\u0012\u0006\u0010Y\u001a\u000207\u0012\b\u0010Z\u001a\u0004\u0018\u00010:\u0012\u0006\u0010[\u001a\u00020\u0005\u0012\u0006\u0010\\\u001a\u00020\u0005\u0012\b\u0010]\u001a\u0004\u0018\u00010?\u0012\u0006\u0010^\u001a\u00020B\u0012\u0006\u0010_\u001a\u00020E\u0012\b\u0010`\u001a\u0004\u0018\u00010H\u0012\u0006\u0010a\u001a\u00020\u0010\u0012\u0006\u0010b\u001a\u00020\u0010\u0012\f\u0010c\u001a\b\u0012\u0004\u0012\u00020M01\u0012\u0016\u0010d\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`O\u0012\u0004\u0012\u00020P0*\u0012\u0016\u0010e\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`O\u0012\u0004\u0012\u00020R0*\u0012\u0006\u0010f\u001a\u00020\u0002\u0012\u0006\u0010g\u001a\u00020\u0005\u0012\u000e\u0010h\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t\u0012\u0006\u0010i\u001a\u00020\f¢\u0006\u0006\b\u0098\u0001\u0010\u0099\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÂ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÂ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\tHÂ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÂ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\r\u0010\u000f\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0004J\r\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b\u0011\u0010\u0012J\r\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0014\u0010\u0015J\r\u0010\u0016\u001a\u00020\f¢\u0006\u0004\b\u0016\u0010\u000eJ\r\u0010\u0017\u001a\u00020\u0010¢\u0006\u0004\b\u0017\u0010\u0012J\r\u0010\u0018\u001a\u00020\u0010¢\u0006\u0004\b\u0018\u0010\u0012J\r\u0010\u0019\u001a\u00020\u0010¢\u0006\u0004\b\u0019\u0010\u0012J\r\u0010\u001a\u001a\u00020\u0010¢\u0006\u0004\b\u001a\u0010\u0012J\r\u0010\u001b\u001a\u00020\u0010¢\u0006\u0004\b\u001b\u0010\u0012J\r\u0010\u001c\u001a\u00020\u0010¢\u0006\u0004\b\u001c\u0010\u0012J\r\u0010\u001d\u001a\u00020\u0005¢\u0006\u0004\b\u001d\u0010\u0007J\u0015\u0010\u001e\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t¢\u0006\u0004\b\u001e\u0010\u000bJ\u0015\u0010!\u001a\u00020\b2\u0006\u0010 \u001a\u00020\u001f¢\u0006\u0004\b!\u0010\"J\r\u0010#\u001a\u00020\u0010¢\u0006\u0004\b#\u0010\u0012J\r\u0010$\u001a\u00020\u0010¢\u0006\u0004\b$\u0010\u0012J\r\u0010%\u001a\u00020\u0010¢\u0006\u0004\b%\u0010\u0012J\u000f\u0010'\u001a\u0004\u0018\u00010&¢\u0006\u0004\b'\u0010(J\r\u0010)\u001a\u00020\u0010¢\u0006\u0004\b)\u0010\u0012J \u0010,\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`+\u0012\u0004\u0012\u00020&0*HÆ\u0003¢\u0006\u0004\b,\u0010-J\u0014\u0010.\u001a\u00060\bj\u0002`+HÆ\u0003¢\u0006\u0004\b.\u0010/J\u0010\u00100\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b0\u0010/J\u0016\u00103\u001a\b\u0012\u0004\u0012\u00020201HÆ\u0003¢\u0006\u0004\b3\u00104J\u0012\u00105\u001a\u0004\u0018\u000102HÆ\u0003¢\u0006\u0004\b5\u00106J\u0010\u00108\u001a\u000207HÆ\u0003¢\u0006\u0004\b8\u00109J\u0012\u0010;\u001a\u0004\u0018\u00010:HÆ\u0003¢\u0006\u0004\b;\u0010<J\u0010\u0010=\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b=\u0010\u0007J\u0010\u0010>\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b>\u0010\u0007J\u0012\u0010@\u001a\u0004\u0018\u00010?HÆ\u0003¢\u0006\u0004\b@\u0010AJ\u0010\u0010C\u001a\u00020BHÆ\u0003¢\u0006\u0004\bC\u0010DJ\u0010\u0010F\u001a\u00020EHÆ\u0003¢\u0006\u0004\bF\u0010GJ\u0012\u0010I\u001a\u0004\u0018\u00010HHÆ\u0003¢\u0006\u0004\bI\u0010JJ\u0010\u0010K\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\bK\u0010\u0012J\u0010\u0010L\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\bL\u0010\u0012J\u0016\u0010N\u001a\b\u0012\u0004\u0012\u00020M01HÆ\u0003¢\u0006\u0004\bN\u00104J \u0010Q\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`O\u0012\u0004\u0012\u00020P0*HÆ\u0003¢\u0006\u0004\bQ\u0010-J \u0010S\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`O\u0012\u0004\u0012\u00020R0*HÆ\u0003¢\u0006\u0004\bS\u0010-J¼\u0002\u0010j\u001a\u00020\u00002\u0018\b\u0002\u0010T\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`+\u0012\u0004\u0012\u00020&0*2\f\b\u0002\u0010U\u001a\u00060\bj\u0002`+2\b\b\u0002\u0010V\u001a\u00020\b2\u000e\b\u0002\u0010W\u001a\b\u0012\u0004\u0012\u000202012\n\b\u0002\u0010X\u001a\u0004\u0018\u0001022\b\b\u0002\u0010Y\u001a\u0002072\n\b\u0002\u0010Z\u001a\u0004\u0018\u00010:2\b\b\u0002\u0010[\u001a\u00020\u00052\b\b\u0002\u0010\\\u001a\u00020\u00052\n\b\u0002\u0010]\u001a\u0004\u0018\u00010?2\b\b\u0002\u0010^\u001a\u00020B2\b\b\u0002\u0010_\u001a\u00020E2\n\b\u0002\u0010`\u001a\u0004\u0018\u00010H2\b\b\u0002\u0010a\u001a\u00020\u00102\b\b\u0002\u0010b\u001a\u00020\u00102\u000e\b\u0002\u0010c\u001a\b\u0012\u0004\u0012\u00020M012\u0018\b\u0002\u0010d\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`O\u0012\u0004\u0012\u00020P0*2\u0018\b\u0002\u0010e\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`O\u0012\u0004\u0012\u00020R0*2\b\b\u0002\u0010f\u001a\u00020\u00022\b\b\u0002\u0010g\u001a\u00020\u00052\u0010\b\u0002\u0010h\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t2\b\b\u0002\u0010i\u001a\u00020\fHÆ\u0001¢\u0006\u0004\bj\u0010kJ\u0010\u0010m\u001a\u00020lHÖ\u0001¢\u0006\u0004\bm\u0010nJ\u0010\u0010o\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\bo\u0010\u0007J\u001a\u0010q\u001a\u00020\u00102\b\u0010p\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\bq\u0010rR\u0019\u0010Y\u001a\u0002078\u0006@\u0006¢\u0006\f\n\u0004\bY\u0010s\u001a\u0004\bt\u00109R\u0019\u0010[\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b[\u0010u\u001a\u0004\bv\u0010\u0007R)\u0010d\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`O\u0012\u0004\u0012\u00020P0*8\u0006@\u0006¢\u0006\f\n\u0004\bd\u0010w\u001a\u0004\bx\u0010-R\u0016\u0010i\u001a\u00020\f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bi\u0010yR\u001b\u0010X\u001a\u0004\u0018\u0001028\u0006@\u0006¢\u0006\f\n\u0004\bX\u0010z\u001a\u0004\b{\u00106R\u001a\u0010}\u001a\u00020|8\u0006@\u0006¢\u0006\r\n\u0004\b}\u0010~\u001a\u0005\b\u007f\u0010\u0080\u0001R!\u0010c\u001a\b\u0012\u0004\u0012\u00020M018\u0006@\u0006¢\u0006\u000e\n\u0005\bc\u0010\u0081\u0001\u001a\u0005\b\u0082\u0001\u00104R\u001a\u0010b\u001a\u00020\u00108\u0006@\u0006¢\u0006\r\n\u0005\bb\u0010\u0083\u0001\u001a\u0004\bb\u0010\u0012R*\u0010e\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`O\u0012\u0004\u0012\u00020R0*8\u0006@\u0006¢\u0006\r\n\u0004\be\u0010w\u001a\u0005\b\u0084\u0001\u0010-R\u001b\u0010_\u001a\u00020E8\u0006@\u0006¢\u0006\u000e\n\u0005\b_\u0010\u0085\u0001\u001a\u0005\b\u0086\u0001\u0010GR\u001a\u0010a\u001a\u00020\u00108\u0006@\u0006¢\u0006\r\n\u0005\ba\u0010\u0083\u0001\u001a\u0004\ba\u0010\u0012R\u0017\u0010f\u001a\u00020\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\bf\u0010\u0087\u0001R\u001d\u0010]\u001a\u0004\u0018\u00010?8\u0006@\u0006¢\u0006\u000e\n\u0005\b]\u0010\u0088\u0001\u001a\u0005\b\u0089\u0001\u0010AR\u0016\u0010g\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bg\u0010uR\u001f\u0010U\u001a\u00060\bj\u0002`+8\u0006@\u0006¢\u0006\u000e\n\u0005\bU\u0010\u008a\u0001\u001a\u0005\b\u008b\u0001\u0010/R\u001d\u0010`\u001a\u0004\u0018\u00010H8\u0006@\u0006¢\u0006\u000e\n\u0005\b`\u0010\u008c\u0001\u001a\u0005\b\u008d\u0001\u0010JR\u001b\u0010V\u001a\u00020\b8\u0006@\u0006¢\u0006\u000e\n\u0005\bV\u0010\u008a\u0001\u001a\u0005\b\u008e\u0001\u0010/R!\u0010W\u001a\b\u0012\u0004\u0012\u000202018\u0006@\u0006¢\u0006\u000e\n\u0005\bW\u0010\u0081\u0001\u001a\u0005\b\u008f\u0001\u00104R\u001b\u0010^\u001a\u00020B8\u0006@\u0006¢\u0006\u000e\n\u0005\b^\u0010\u0090\u0001\u001a\u0005\b\u0091\u0001\u0010DR\u001f\u0010h\u001a\n\u0018\u00010\bj\u0004\u0018\u0001`\t8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\bh\u0010\u0092\u0001R\u001d\u0010Z\u001a\u0004\u0018\u00010:8\u0006@\u0006¢\u0006\u000e\n\u0005\bZ\u0010\u0093\u0001\u001a\u0005\b\u0094\u0001\u0010<R*\u0010T\u001a\u0012\u0012\b\u0012\u00060\bj\u0002`+\u0012\u0004\u0012\u00020&0*8\u0006@\u0006¢\u0006\r\n\u0004\bT\u0010w\u001a\u0005\b\u0095\u0001\u0010-R\u001a\u0010\\\u001a\u00020\u00058\u0006@\u0006¢\u0006\r\n\u0004\b\\\u0010u\u001a\u0005\b\u0096\u0001\u0010\u0007R\u001d\u0010\u0097\u0001\u001a\u00020\u00108\u0006@\u0006¢\u0006\u000f\n\u0006\b\u0097\u0001\u0010\u0083\u0001\u001a\u0005\b\u0097\u0001\u0010\u0012¨\u0006\u009b\u0001"}, d2 = {"Lcom/discord/widgets/voice/model/CallModel;", "", "Lcom/discord/api/channel/Channel;", "component19", "()Lcom/discord/api/channel/Channel;", "", "component20", "()I", "", "Lcom/discord/api/permission/PermissionBit;", "component21", "()Ljava/lang/Long;", "Lcom/discord/stores/StoreAudioManagerV2$State;", "component22", "()Lcom/discord/stores/StoreAudioManagerV2$State;", "getChannel", "", "isConnected", "()Z", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "getInputMode", "()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "getAudioManagerState", "isMeMutedByAnySource", "isMuted", "isSuppressed", "isSelfDeafened", "isServerDeafened", "isDeafenedByAnySource", "getNumUsersConnected", "getChannelPermissions", "Lcom/discord/utilities/time/Clock;", "clock", "getCallDurationMs", "(Lcom/discord/utilities/time/Clock;)J", "canRequestToSpeak", "canInvite", "canManageEvent", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "getDmRecipient", "()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "isStreaming", "", "Lcom/discord/primitives/UserId;", "component1", "()Ljava/util/Map;", "component2", "()J", "component3", "", "Lco/discord/media_engine/VideoInputDeviceDescription;", "component4", "()Ljava/util/List;", "component5", "()Lco/discord/media_engine/VideoInputDeviceDescription;", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "component6", "()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "component7", "()Lcom/discord/rtcconnection/RtcConnection$Metadata;", "component8", "component9", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "component10", "()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "Lcom/discord/utilities/voice/VoiceChannelJoinability;", "component11", "()Lcom/discord/utilities/voice/VoiceChannelJoinability;", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "component12", "()Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "Lcom/discord/models/guild/Guild;", "component13", "()Lcom/discord/models/guild/Guild;", "component14", "component15", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "component16", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/models/embeddedactivities/EmbeddedActivity;", "component17", "Lcom/discord/api/application/Application;", "component18", "participants", "myId", "timeConnectedMs", "videoDevices", "selectedVideoDevice", "voiceSettings", "rtcConnectionMetadata", "callFeedbackTriggerRateDenominator", "streamFeedbackTriggerRateDenominator", "activeStream", "voiceChannelJoinability", "guildMaxVideoChannelMembers", "guild", "isChannelSelected", "isMyHandRaised", "guildScheduledEvents", "embeddedActivitiesForChannel", "applications", "channel", "numUsersConnected", "channelPermissions", "audioManagerState", "copy", "(Ljava/util/Map;JJLjava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/rtcconnection/RtcConnection$Metadata;IILcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/utilities/voice/VoiceChannelJoinability;Lcom/discord/api/guild/GuildMaxVideoChannelUsers;Lcom/discord/models/guild/Guild;ZZLjava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/discord/api/channel/Channel;ILjava/lang/Long;Lcom/discord/stores/StoreAudioManagerV2$State;)Lcom/discord/widgets/voice/model/CallModel;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "getVoiceSettings", "I", "getCallFeedbackTriggerRateDenominator", "Ljava/util/Map;", "getEmbeddedActivitiesForChannel", "Lcom/discord/stores/StoreAudioManagerV2$State;", "Lco/discord/media_engine/VideoInputDeviceDescription;", "getSelectedVideoDevice", "Lcom/discord/widgets/voice/model/CameraState;", "cameraState", "Lcom/discord/widgets/voice/model/CameraState;", "getCameraState", "()Lcom/discord/widgets/voice/model/CameraState;", "Ljava/util/List;", "getGuildScheduledEvents", "Z", "getApplications", "Lcom/discord/api/guild/GuildMaxVideoChannelUsers;", "getGuildMaxVideoChannelMembers", "Lcom/discord/api/channel/Channel;", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "getActiveStream", "J", "getMyId", "Lcom/discord/models/guild/Guild;", "getGuild", "getTimeConnectedMs", "getVideoDevices", "Lcom/discord/utilities/voice/VoiceChannelJoinability;", "getVoiceChannelJoinability", "Ljava/lang/Long;", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "getRtcConnectionMetadata", "getParticipants", "getStreamFeedbackTriggerRateDenominator", "isVideoCall", HookHelper.constructorName, "(Ljava/util/Map;JJLjava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/rtcconnection/RtcConnection$Metadata;IILcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/utilities/voice/VoiceChannelJoinability;Lcom/discord/api/guild/GuildMaxVideoChannelUsers;Lcom/discord/models/guild/Guild;ZZLjava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/discord/api/channel/Channel;ILjava/lang/Long;Lcom/discord/stores/StoreAudioManagerV2$State;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class CallModel {
    public static final Companion Companion = new Companion(null);
    private final StoreApplicationStreaming.ActiveApplicationStream activeStream;
    private final Map<Long, Application> applications;
    private final StoreAudioManagerV2.State audioManagerState;
    private final int callFeedbackTriggerRateDenominator;
    private final CameraState cameraState;
    private final Channel channel;
    private final Long channelPermissions;
    private final Map<Long, EmbeddedActivity> embeddedActivitiesForChannel;
    private final Guild guild;
    private final GuildMaxVideoChannelUsers guildMaxVideoChannelMembers;
    private final List<GuildScheduledEvent> guildScheduledEvents;
    private final boolean isChannelSelected;
    private final boolean isMyHandRaised;
    private final boolean isVideoCall;
    private final long myId;
    private final int numUsersConnected;
    private final Map<Long, StoreVoiceParticipants.VoiceUser> participants;
    private final RtcConnection.Metadata rtcConnectionMetadata;
    private final VideoInputDeviceDescription selectedVideoDevice;
    private final int streamFeedbackTriggerRateDenominator;
    private final long timeConnectedMs;
    private final List<VideoInputDeviceDescription> videoDevices;
    private final VoiceChannelJoinability voiceChannelJoinability;
    private final StoreMediaSettings.VoiceConfiguration voiceSettings;

    /* compiled from: CallModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001:\u00016B\t\b\u0002¢\u0006\u0004\b4\u00105J÷\u0001\u0010*\u001a\u00020)2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\u0010\b\u001a\u00060\u0006j\u0002`\u00072\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\n2\u0016\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\r0\f2\u000e\u0010\u0010\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u000f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0015\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u00122\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\b\u0010\u001a\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020 2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\"0\u00112\u0016\u0010&\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`$\u0012\u0004\u0012\u00020%0\f2\u0016\u0010(\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`$\u0012\u0004\u0012\u00020'0\fH\u0002¢\u0006\u0004\b*\u0010+J\u001d\u0010.\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b.\u0010/J!\u00102\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010)0,2\n\u00101\u001a\u00060\u0006j\u0002`0¢\u0006\u0004\b2\u00103¨\u00067"}, d2 = {"Lcom/discord/widgets/voice/model/CallModel$Companion;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/models/guild/Guild;", "guild", "", "Lcom/discord/primitives/UserId;", "myUserId", "timeConnectedMs", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "voiceConfig", "", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "voiceParticipants", "Lcom/discord/api/permission/PermissionBit;", "channelPermissions", "", "Lco/discord/media_engine/VideoInputDeviceDescription;", "videoDevices", "", "isChannelSelected", "selectedVideoDevice", "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;", "activeStream", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "rtcConnectionMetadata", "", "callFeedbackTriggerRateDenominator", "streamFeedbackTriggerRateDenominator", "Lcom/discord/stores/StoreAudioManagerV2$State;", "audioManagerState", "Lcom/discord/utilities/voice/VoiceChannelJoinability;", "voiceChannelJoinability", "Lcom/discord/api/guildscheduledevent/GuildScheduledEvent;", "guildScheduledEvents", "Lcom/discord/primitives/ApplicationId;", "Lcom/discord/models/embeddedactivities/EmbeddedActivity;", "embeddedActivitiesForChannel", "Lcom/discord/api/application/Application;", "applications", "Lcom/discord/widgets/voice/model/CallModel;", "create", "(Lcom/discord/api/channel/Channel;Lcom/discord/models/guild/Guild;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;ZLco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/rtcconnection/RtcConnection$Metadata;IILcom/discord/stores/StoreAudioManagerV2$State;Lcom/discord/utilities/voice/VoiceChannelJoinability;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/voice/model/CallModel;", "Lrx/Observable;", "Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;", "observeChunk", "(Lcom/discord/api/channel/Channel;)Lrx/Observable;", "Lcom/discord/primitives/ChannelId;", "channelId", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "Chunk", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {

        /* compiled from: CallModel.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0016\b\u0082\b\u0018\u00002\u00020\u0001Bs\u0012\u0006\u0010\u001d\u001a\u00020\u0002\u0012\n\u0010\u001e\u001a\u00060\u0005j\u0002`\u0006\u0012\u0006\u0010\u001f\u001a\u00020\u0005\u0012\u0006\u0010 \u001a\u00020\n\u0012\u0016\u0010!\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r\u0012\u000e\u0010\"\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0011\u0012\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014\u0012\b\u0010$\u001a\u0004\u0018\u00010\u0015\u0012\u0006\u0010%\u001a\u00020\u001a¢\u0006\u0004\bC\u0010DJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0014\u0010\u0007\u001a\u00060\u0005j\u0002`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ \u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\rHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010J\u0018\u0010\u0012\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0011HÆ\u0003¢\u0006\u0004\b\u0012\u0010\u0013J\u0016\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014HÆ\u0003¢\u0006\u0004\b\u0016\u0010\u0017J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u0015HÆ\u0003¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u001b\u001a\u00020\u001aHÆ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u008e\u0001\u0010&\u001a\u00020\u00002\b\b\u0002\u0010\u001d\u001a\u00020\u00022\f\b\u0002\u0010\u001e\u001a\u00060\u0005j\u0002`\u00062\b\b\u0002\u0010\u001f\u001a\u00020\u00052\b\b\u0002\u0010 \u001a\u00020\n2\u0018\b\u0002\u0010!\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r2\u0010\b\u0002\u0010\"\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00112\u000e\b\u0002\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\n\b\u0002\u0010$\u001a\u0004\u0018\u00010\u00152\b\b\u0002\u0010%\u001a\u00020\u001aHÆ\u0001¢\u0006\u0004\b&\u0010'J\u0010\u0010)\u001a\u00020(HÖ\u0001¢\u0006\u0004\b)\u0010*J\u0010\u0010,\u001a\u00020+HÖ\u0001¢\u0006\u0004\b,\u0010-J\u001a\u00100\u001a\u00020/2\b\u0010.\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b0\u00101R\u0019\u0010\u001d\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00102\u001a\u0004\b3\u0010\u0004R)\u0010!\u001a\u0012\u0012\b\u0012\u00060\u0005j\u0002`\u0006\u0012\u0004\u0012\u00020\u000e0\r8\u0006@\u0006¢\u0006\f\n\u0004\b!\u00104\u001a\u0004\b5\u0010\u0010R\u001f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00150\u00148\u0006@\u0006¢\u0006\f\n\u0004\b#\u00106\u001a\u0004\b7\u0010\u0017R\u0019\u0010 \u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b \u00108\u001a\u0004\b9\u0010\fR\u0019\u0010\u001f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u001f\u0010:\u001a\u0004\b;\u0010\bR!\u0010\"\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010<\u001a\u0004\b=\u0010\u0013R\u001b\u0010$\u001a\u0004\u0018\u00010\u00158\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010>\u001a\u0004\b?\u0010\u0019R\u001d\u0010\u001e\u001a\u00060\u0005j\u0002`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u0010:\u001a\u0004\b@\u0010\bR\u0019\u0010%\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b%\u0010A\u001a\u0004\bB\u0010\u001c¨\u0006E"}, d2 = {"Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "Lcom/discord/primitives/UserId;", "component2", "()J", "component3", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "component4", "()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "", "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;", "component5", "()Ljava/util/Map;", "Lcom/discord/api/permission/PermissionBit;", "component6", "()Ljava/lang/Long;", "", "Lco/discord/media_engine/VideoInputDeviceDescription;", "component7", "()Ljava/util/List;", "component8", "()Lco/discord/media_engine/VideoInputDeviceDescription;", "Lcom/discord/stores/StoreAudioManagerV2$State;", "component9", "()Lcom/discord/stores/StoreAudioManagerV2$State;", "channel", "myUserId", "timeConnectedMs", "voiceConfig", "voiceParticipants", "channelPermissions", "videoDevices", "selectedVideoDevice", "audioManagerState", "copy", "(Lcom/discord/api/channel/Channel;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreAudioManagerV2$State;)Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Ljava/util/Map;", "getVoiceParticipants", "Ljava/util/List;", "getVideoDevices", "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;", "getVoiceConfig", "J", "getTimeConnectedMs", "Ljava/lang/Long;", "getChannelPermissions", "Lco/discord/media_engine/VideoInputDeviceDescription;", "getSelectedVideoDevice", "getMyUserId", "Lcom/discord/stores/StoreAudioManagerV2$State;", "getAudioManagerState", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreAudioManagerV2$State;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Chunk {
            private final StoreAudioManagerV2.State audioManagerState;
            private final Channel channel;
            private final Long channelPermissions;
            private final long myUserId;
            private final VideoInputDeviceDescription selectedVideoDevice;
            private final long timeConnectedMs;
            private final List<VideoInputDeviceDescription> videoDevices;
            private final StoreMediaSettings.VoiceConfiguration voiceConfig;
            private final Map<Long, StoreVoiceParticipants.VoiceUser> voiceParticipants;

            public Chunk(Channel channel, long j, long j2, StoreMediaSettings.VoiceConfiguration voiceConfiguration, Map<Long, StoreVoiceParticipants.VoiceUser> map, Long l, List<VideoInputDeviceDescription> list, VideoInputDeviceDescription videoInputDeviceDescription, StoreAudioManagerV2.State state) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(voiceConfiguration, "voiceConfig");
                m.checkNotNullParameter(map, "voiceParticipants");
                m.checkNotNullParameter(list, "videoDevices");
                m.checkNotNullParameter(state, "audioManagerState");
                this.channel = channel;
                this.myUserId = j;
                this.timeConnectedMs = j2;
                this.voiceConfig = voiceConfiguration;
                this.voiceParticipants = map;
                this.channelPermissions = l;
                this.videoDevices = list;
                this.selectedVideoDevice = videoInputDeviceDescription;
                this.audioManagerState = state;
            }

            public final Channel component1() {
                return this.channel;
            }

            public final long component2() {
                return this.myUserId;
            }

            public final long component3() {
                return this.timeConnectedMs;
            }

            public final StoreMediaSettings.VoiceConfiguration component4() {
                return this.voiceConfig;
            }

            public final Map<Long, StoreVoiceParticipants.VoiceUser> component5() {
                return this.voiceParticipants;
            }

            public final Long component6() {
                return this.channelPermissions;
            }

            public final List<VideoInputDeviceDescription> component7() {
                return this.videoDevices;
            }

            public final VideoInputDeviceDescription component8() {
                return this.selectedVideoDevice;
            }

            public final StoreAudioManagerV2.State component9() {
                return this.audioManagerState;
            }

            public final Chunk copy(Channel channel, long j, long j2, StoreMediaSettings.VoiceConfiguration voiceConfiguration, Map<Long, StoreVoiceParticipants.VoiceUser> map, Long l, List<VideoInputDeviceDescription> list, VideoInputDeviceDescription videoInputDeviceDescription, StoreAudioManagerV2.State state) {
                m.checkNotNullParameter(channel, "channel");
                m.checkNotNullParameter(voiceConfiguration, "voiceConfig");
                m.checkNotNullParameter(map, "voiceParticipants");
                m.checkNotNullParameter(list, "videoDevices");
                m.checkNotNullParameter(state, "audioManagerState");
                return new Chunk(channel, j, j2, voiceConfiguration, map, l, list, videoInputDeviceDescription, state);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Chunk)) {
                    return false;
                }
                Chunk chunk = (Chunk) obj;
                return m.areEqual(this.channel, chunk.channel) && this.myUserId == chunk.myUserId && this.timeConnectedMs == chunk.timeConnectedMs && m.areEqual(this.voiceConfig, chunk.voiceConfig) && m.areEqual(this.voiceParticipants, chunk.voiceParticipants) && m.areEqual(this.channelPermissions, chunk.channelPermissions) && m.areEqual(this.videoDevices, chunk.videoDevices) && m.areEqual(this.selectedVideoDevice, chunk.selectedVideoDevice) && m.areEqual(this.audioManagerState, chunk.audioManagerState);
            }

            public final StoreAudioManagerV2.State getAudioManagerState() {
                return this.audioManagerState;
            }

            public final Channel getChannel() {
                return this.channel;
            }

            public final Long getChannelPermissions() {
                return this.channelPermissions;
            }

            public final long getMyUserId() {
                return this.myUserId;
            }

            public final VideoInputDeviceDescription getSelectedVideoDevice() {
                return this.selectedVideoDevice;
            }

            public final long getTimeConnectedMs() {
                return this.timeConnectedMs;
            }

            public final List<VideoInputDeviceDescription> getVideoDevices() {
                return this.videoDevices;
            }

            public final StoreMediaSettings.VoiceConfiguration getVoiceConfig() {
                return this.voiceConfig;
            }

            public final Map<Long, StoreVoiceParticipants.VoiceUser> getVoiceParticipants() {
                return this.voiceParticipants;
            }

            public int hashCode() {
                Channel channel = this.channel;
                int i = 0;
                int hashCode = channel != null ? channel.hashCode() : 0;
                int a = (b.a(this.timeConnectedMs) + ((b.a(this.myUserId) + (hashCode * 31)) * 31)) * 31;
                StoreMediaSettings.VoiceConfiguration voiceConfiguration = this.voiceConfig;
                int hashCode2 = (a + (voiceConfiguration != null ? voiceConfiguration.hashCode() : 0)) * 31;
                Map<Long, StoreVoiceParticipants.VoiceUser> map = this.voiceParticipants;
                int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
                Long l = this.channelPermissions;
                int hashCode4 = (hashCode3 + (l != null ? l.hashCode() : 0)) * 31;
                List<VideoInputDeviceDescription> list = this.videoDevices;
                int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
                VideoInputDeviceDescription videoInputDeviceDescription = this.selectedVideoDevice;
                int hashCode6 = (hashCode5 + (videoInputDeviceDescription != null ? videoInputDeviceDescription.hashCode() : 0)) * 31;
                StoreAudioManagerV2.State state = this.audioManagerState;
                if (state != null) {
                    i = state.hashCode();
                }
                return hashCode6 + i;
            }

            public String toString() {
                StringBuilder R = a.R("Chunk(channel=");
                R.append(this.channel);
                R.append(", myUserId=");
                R.append(this.myUserId);
                R.append(", timeConnectedMs=");
                R.append(this.timeConnectedMs);
                R.append(", voiceConfig=");
                R.append(this.voiceConfig);
                R.append(", voiceParticipants=");
                R.append(this.voiceParticipants);
                R.append(", channelPermissions=");
                R.append(this.channelPermissions);
                R.append(", videoDevices=");
                R.append(this.videoDevices);
                R.append(", selectedVideoDevice=");
                R.append(this.selectedVideoDevice);
                R.append(", audioManagerState=");
                R.append(this.audioManagerState);
                R.append(")");
                return R.toString();
            }
        }

        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final CallModel create(Channel channel, Guild guild, long j, long j2, StoreMediaSettings.VoiceConfiguration voiceConfiguration, Map<Long, StoreVoiceParticipants.VoiceUser> map, Long l, List<VideoInputDeviceDescription> list, boolean z2, VideoInputDeviceDescription videoInputDeviceDescription, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, RtcConnection.Metadata metadata, int i, int i2, StoreAudioManagerV2.State state, VoiceChannelJoinability voiceChannelJoinability, List<GuildScheduledEvent> list2, Map<Long, EmbeddedActivity> map2, Map<Long, Application> map3) {
            int i3;
            GuildMaxVideoChannelUsers guildMaxVideoChannelUsers;
            if (map.isEmpty()) {
                i3 = 0;
            } else {
                int i4 = 0;
                for (Map.Entry<Long, StoreVoiceParticipants.VoiceUser> entry : map.entrySet()) {
                    if (entry.getValue().isConnected()) {
                        i4++;
                    }
                }
                i3 = i4;
            }
            StoreVoiceParticipants.VoiceUser voiceUser = map.get(Long.valueOf(j));
            boolean z3 = voiceUser != null && voiceUser.isRequestingToSpeak();
            if (guild == null || (guildMaxVideoChannelUsers = guild.getMaxVideoChannelUsers()) == null) {
                guildMaxVideoChannelUsers = GuildMaxVideoChannelUsers.Unlimited.INSTANCE;
            }
            return new CallModel(map, j, j2, list, videoInputDeviceDescription, voiceConfiguration, metadata, i, i2, activeApplicationStream, voiceChannelJoinability, guildMaxVideoChannelUsers, guild, z2, z3, list2, map2, map3, channel, i3, l, state);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final Observable<Chunk> observeChunk(final Channel channel) {
            StoreStream.Companion companion = StoreStream.Companion;
            Observable<Chunk> d = Observable.d(companion.getUsers().observeMeId(), companion.getVoiceChannelSelected().observeTimeSelectedMs(), companion.getMediaSettings().getVoiceConfiguration(), ObservableExtensionsKt.leadingEdgeThrottle(companion.getVoiceParticipants().get(channel.h()), 250L, TimeUnit.MILLISECONDS), companion.getPermissions().observePermissionsForChannel(channel.h()), companion.getMediaEngine().getVideoInputDevices(), companion.getMediaEngine().getSelectedVideoInputDevice(), companion.getAudioManagerV2().observeAudioManagerState(), new Func8<Long, Long, StoreMediaSettings.VoiceConfiguration, Map<Long, ? extends StoreVoiceParticipants.VoiceUser>, Long, List<? extends VideoInputDeviceDescription>, VideoInputDeviceDescription, StoreAudioManagerV2.State, Chunk>() { // from class: com.discord.widgets.voice.model.CallModel$Companion$observeChunk$1
                @Override // rx.functions.Func8
                public /* bridge */ /* synthetic */ CallModel.Companion.Chunk call(Long l, Long l2, StoreMediaSettings.VoiceConfiguration voiceConfiguration, Map<Long, ? extends StoreVoiceParticipants.VoiceUser> map, Long l3, List<? extends VideoInputDeviceDescription> list, VideoInputDeviceDescription videoInputDeviceDescription, StoreAudioManagerV2.State state) {
                    return call2(l, l2, voiceConfiguration, (Map<Long, StoreVoiceParticipants.VoiceUser>) map, l3, (List<VideoInputDeviceDescription>) list, videoInputDeviceDescription, state);
                }

                /* renamed from: call  reason: avoid collision after fix types in other method */
                public final CallModel.Companion.Chunk call2(Long l, Long l2, StoreMediaSettings.VoiceConfiguration voiceConfiguration, Map<Long, StoreVoiceParticipants.VoiceUser> map, Long l3, List<VideoInputDeviceDescription> list, VideoInputDeviceDescription videoInputDeviceDescription, StoreAudioManagerV2.State state) {
                    Channel channel2 = Channel.this;
                    m.checkNotNullExpressionValue(l, "myUserId");
                    long longValue = l.longValue();
                    m.checkNotNullExpressionValue(l2, "timeConnectedMs");
                    long longValue2 = l2.longValue();
                    m.checkNotNullExpressionValue(voiceConfiguration, "voiceConfig");
                    m.checkNotNullExpressionValue(map, "voiceParticipants");
                    m.checkNotNullExpressionValue(list, "videoDevices");
                    m.checkNotNullExpressionValue(state, "audioManagerState");
                    return new CallModel.Companion.Chunk(channel2, longValue, longValue2, voiceConfiguration, map, l3, list, videoInputDeviceDescription, state);
                }
            });
            m.checkNotNullExpressionValue(d, "Observable\n          .co…            )\n          }");
            return d;
        }

        public final Observable<CallModel> get(long j) {
            Observable Y = StoreStream.Companion.getChannels().observeChannel(j).Y(new CallModel$Companion$get$1(j));
            m.checkNotNullExpressionValue(Y, "StoreStream\n          .g…            }\n          }");
            return Y;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public CallModel(Map<Long, StoreVoiceParticipants.VoiceUser> map, long j, long j2, List<VideoInputDeviceDescription> list, VideoInputDeviceDescription videoInputDeviceDescription, StoreMediaSettings.VoiceConfiguration voiceConfiguration, RtcConnection.Metadata metadata, int i, int i2, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, VoiceChannelJoinability voiceChannelJoinability, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, Guild guild, boolean z2, boolean z3, List<GuildScheduledEvent> list2, Map<Long, EmbeddedActivity> map2, Map<Long, Application> map3, Channel channel, int i3, Long l, StoreAudioManagerV2.State state) {
        CameraState cameraState;
        boolean z4;
        boolean z5;
        m.checkNotNullParameter(map, "participants");
        m.checkNotNullParameter(list, "videoDevices");
        m.checkNotNullParameter(voiceConfiguration, "voiceSettings");
        m.checkNotNullParameter(voiceChannelJoinability, "voiceChannelJoinability");
        m.checkNotNullParameter(guildMaxVideoChannelUsers, "guildMaxVideoChannelMembers");
        m.checkNotNullParameter(list2, "guildScheduledEvents");
        m.checkNotNullParameter(map2, "embeddedActivitiesForChannel");
        m.checkNotNullParameter(map3, "applications");
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(state, "audioManagerState");
        this.participants = map;
        this.myId = j;
        this.timeConnectedMs = j2;
        this.videoDevices = list;
        this.selectedVideoDevice = videoInputDeviceDescription;
        this.voiceSettings = voiceConfiguration;
        this.rtcConnectionMetadata = metadata;
        this.callFeedbackTriggerRateDenominator = i;
        this.streamFeedbackTriggerRateDenominator = i2;
        this.activeStream = activeApplicationStream;
        this.voiceChannelJoinability = voiceChannelJoinability;
        this.guildMaxVideoChannelMembers = guildMaxVideoChannelUsers;
        this.guild = guild;
        this.isChannelSelected = z2;
        this.isMyHandRaised = z3;
        this.guildScheduledEvents = list2;
        this.embeddedActivitiesForChannel = map2;
        this.applications = map3;
        this.channel = channel;
        this.numUsersConnected = i3;
        this.channelPermissions = l;
        this.audioManagerState = state;
        if (videoInputDeviceDescription != null) {
            cameraState = CameraState.CAMERA_ON;
        } else {
            cameraState = CameraState.CAMERA_OFF;
        }
        this.cameraState = cameraState;
        boolean z6 = true;
        if (!isStreaming()) {
            if (!map.isEmpty()) {
                for (Map.Entry<Long, StoreVoiceParticipants.VoiceUser> entry : map.entrySet()) {
                    VoiceState voiceState = entry.getValue().getVoiceState();
                    if (voiceState != null) {
                        z5 = voiceState.j();
                        continue;
                    } else {
                        z5 = false;
                        continue;
                    }
                    if (z5) {
                        z4 = true;
                        break;
                    }
                }
            }
            z4 = false;
            if (!z4) {
                z6 = false;
            }
        }
        this.isVideoCall = z6;
    }

    private final Channel component19() {
        return this.channel;
    }

    private final int component20() {
        return this.numUsersConnected;
    }

    private final Long component21() {
        return this.channelPermissions;
    }

    private final StoreAudioManagerV2.State component22() {
        return this.audioManagerState;
    }

    public final boolean canInvite() {
        boolean x2 = ChannelUtils.x(this.channel);
        boolean z2 = ChannelUtils.t(this.channel) && PermissionUtils.can(1L, this.channelPermissions);
        StageInstance stageInstanceForChannel = StoreStream.Companion.getStageInstances().getStageInstanceForChannel(this.channel.h());
        return x2 || z2 || ((stageInstanceForChannel != null ? stageInstanceForChannel.d() : null) != null);
    }

    public final boolean canManageEvent() {
        return GuildScheduledEventUtilities.Companion.canStartEventInChannel(this.channel, this.channelPermissions);
    }

    public final boolean canRequestToSpeak() {
        return ChannelUtils.z(this.channel) && PermissionUtils.can(Permission.REQUEST_TO_SPEAK, this.channelPermissions);
    }

    public final Map<Long, StoreVoiceParticipants.VoiceUser> component1() {
        return this.participants;
    }

    public final StoreApplicationStreaming.ActiveApplicationStream component10() {
        return this.activeStream;
    }

    public final VoiceChannelJoinability component11() {
        return this.voiceChannelJoinability;
    }

    public final GuildMaxVideoChannelUsers component12() {
        return this.guildMaxVideoChannelMembers;
    }

    public final Guild component13() {
        return this.guild;
    }

    public final boolean component14() {
        return this.isChannelSelected;
    }

    public final boolean component15() {
        return this.isMyHandRaised;
    }

    public final List<GuildScheduledEvent> component16() {
        return this.guildScheduledEvents;
    }

    public final Map<Long, EmbeddedActivity> component17() {
        return this.embeddedActivitiesForChannel;
    }

    public final Map<Long, Application> component18() {
        return this.applications;
    }

    public final long component2() {
        return this.myId;
    }

    public final long component3() {
        return this.timeConnectedMs;
    }

    public final List<VideoInputDeviceDescription> component4() {
        return this.videoDevices;
    }

    public final VideoInputDeviceDescription component5() {
        return this.selectedVideoDevice;
    }

    public final StoreMediaSettings.VoiceConfiguration component6() {
        return this.voiceSettings;
    }

    public final RtcConnection.Metadata component7() {
        return this.rtcConnectionMetadata;
    }

    public final int component8() {
        return this.callFeedbackTriggerRateDenominator;
    }

    public final int component9() {
        return this.streamFeedbackTriggerRateDenominator;
    }

    public final CallModel copy(Map<Long, StoreVoiceParticipants.VoiceUser> map, long j, long j2, List<VideoInputDeviceDescription> list, VideoInputDeviceDescription videoInputDeviceDescription, StoreMediaSettings.VoiceConfiguration voiceConfiguration, RtcConnection.Metadata metadata, int i, int i2, StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream, VoiceChannelJoinability voiceChannelJoinability, GuildMaxVideoChannelUsers guildMaxVideoChannelUsers, Guild guild, boolean z2, boolean z3, List<GuildScheduledEvent> list2, Map<Long, EmbeddedActivity> map2, Map<Long, Application> map3, Channel channel, int i3, Long l, StoreAudioManagerV2.State state) {
        m.checkNotNullParameter(map, "participants");
        m.checkNotNullParameter(list, "videoDevices");
        m.checkNotNullParameter(voiceConfiguration, "voiceSettings");
        m.checkNotNullParameter(voiceChannelJoinability, "voiceChannelJoinability");
        m.checkNotNullParameter(guildMaxVideoChannelUsers, "guildMaxVideoChannelMembers");
        m.checkNotNullParameter(list2, "guildScheduledEvents");
        m.checkNotNullParameter(map2, "embeddedActivitiesForChannel");
        m.checkNotNullParameter(map3, "applications");
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(state, "audioManagerState");
        return new CallModel(map, j, j2, list, videoInputDeviceDescription, voiceConfiguration, metadata, i, i2, activeApplicationStream, voiceChannelJoinability, guildMaxVideoChannelUsers, guild, z2, z3, list2, map2, map3, channel, i3, l, state);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CallModel)) {
            return false;
        }
        CallModel callModel = (CallModel) obj;
        return m.areEqual(this.participants, callModel.participants) && this.myId == callModel.myId && this.timeConnectedMs == callModel.timeConnectedMs && m.areEqual(this.videoDevices, callModel.videoDevices) && m.areEqual(this.selectedVideoDevice, callModel.selectedVideoDevice) && m.areEqual(this.voiceSettings, callModel.voiceSettings) && m.areEqual(this.rtcConnectionMetadata, callModel.rtcConnectionMetadata) && this.callFeedbackTriggerRateDenominator == callModel.callFeedbackTriggerRateDenominator && this.streamFeedbackTriggerRateDenominator == callModel.streamFeedbackTriggerRateDenominator && m.areEqual(this.activeStream, callModel.activeStream) && m.areEqual(this.voiceChannelJoinability, callModel.voiceChannelJoinability) && m.areEqual(this.guildMaxVideoChannelMembers, callModel.guildMaxVideoChannelMembers) && m.areEqual(this.guild, callModel.guild) && this.isChannelSelected == callModel.isChannelSelected && this.isMyHandRaised == callModel.isMyHandRaised && m.areEqual(this.guildScheduledEvents, callModel.guildScheduledEvents) && m.areEqual(this.embeddedActivitiesForChannel, callModel.embeddedActivitiesForChannel) && m.areEqual(this.applications, callModel.applications) && m.areEqual(this.channel, callModel.channel) && this.numUsersConnected == callModel.numUsersConnected && m.areEqual(this.channelPermissions, callModel.channelPermissions) && m.areEqual(this.audioManagerState, callModel.audioManagerState);
    }

    public final StoreApplicationStreaming.ActiveApplicationStream getActiveStream() {
        return this.activeStream;
    }

    public final Map<Long, Application> getApplications() {
        return this.applications;
    }

    public final StoreAudioManagerV2.State getAudioManagerState() {
        return this.audioManagerState;
    }

    public final long getCallDurationMs(Clock clock) {
        m.checkNotNullParameter(clock, "clock");
        return clock.currentTimeMillis() - this.timeConnectedMs;
    }

    public final int getCallFeedbackTriggerRateDenominator() {
        return this.callFeedbackTriggerRateDenominator;
    }

    public final CameraState getCameraState() {
        return this.cameraState;
    }

    public final Channel getChannel() {
        return this.channel;
    }

    public final Long getChannelPermissions() {
        return this.channelPermissions;
    }

    public final StoreVoiceParticipants.VoiceUser getDmRecipient() {
        boolean z2;
        Object obj = null;
        if (this.channel.A() != 1) {
            return null;
        }
        Iterator<T> it = this.participants.values().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((StoreVoiceParticipants.VoiceUser) next).getUser().getId() != this.myId) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                obj = next;
                break;
            }
        }
        return (StoreVoiceParticipants.VoiceUser) obj;
    }

    public final Map<Long, EmbeddedActivity> getEmbeddedActivitiesForChannel() {
        return this.embeddedActivitiesForChannel;
    }

    public final Guild getGuild() {
        return this.guild;
    }

    public final GuildMaxVideoChannelUsers getGuildMaxVideoChannelMembers() {
        return this.guildMaxVideoChannelMembers;
    }

    public final List<GuildScheduledEvent> getGuildScheduledEvents() {
        return this.guildScheduledEvents;
    }

    public final MediaEngineConnection.InputMode getInputMode() {
        return this.voiceSettings.getInputMode();
    }

    public final long getMyId() {
        return this.myId;
    }

    public final int getNumUsersConnected() {
        return this.numUsersConnected;
    }

    public final Map<Long, StoreVoiceParticipants.VoiceUser> getParticipants() {
        return this.participants;
    }

    public final RtcConnection.Metadata getRtcConnectionMetadata() {
        return this.rtcConnectionMetadata;
    }

    public final VideoInputDeviceDescription getSelectedVideoDevice() {
        return this.selectedVideoDevice;
    }

    public final int getStreamFeedbackTriggerRateDenominator() {
        return this.streamFeedbackTriggerRateDenominator;
    }

    public final long getTimeConnectedMs() {
        return this.timeConnectedMs;
    }

    public final List<VideoInputDeviceDescription> getVideoDevices() {
        return this.videoDevices;
    }

    public final VoiceChannelJoinability getVoiceChannelJoinability() {
        return this.voiceChannelJoinability;
    }

    public final StoreMediaSettings.VoiceConfiguration getVoiceSettings() {
        return this.voiceSettings;
    }

    public int hashCode() {
        Map<Long, StoreVoiceParticipants.VoiceUser> map = this.participants;
        int i = 0;
        int hashCode = map != null ? map.hashCode() : 0;
        int a = (b.a(this.timeConnectedMs) + ((b.a(this.myId) + (hashCode * 31)) * 31)) * 31;
        List<VideoInputDeviceDescription> list = this.videoDevices;
        int hashCode2 = (a + (list != null ? list.hashCode() : 0)) * 31;
        VideoInputDeviceDescription videoInputDeviceDescription = this.selectedVideoDevice;
        int hashCode3 = (hashCode2 + (videoInputDeviceDescription != null ? videoInputDeviceDescription.hashCode() : 0)) * 31;
        StoreMediaSettings.VoiceConfiguration voiceConfiguration = this.voiceSettings;
        int hashCode4 = (hashCode3 + (voiceConfiguration != null ? voiceConfiguration.hashCode() : 0)) * 31;
        RtcConnection.Metadata metadata = this.rtcConnectionMetadata;
        int hashCode5 = (((((hashCode4 + (metadata != null ? metadata.hashCode() : 0)) * 31) + this.callFeedbackTriggerRateDenominator) * 31) + this.streamFeedbackTriggerRateDenominator) * 31;
        StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream = this.activeStream;
        int hashCode6 = (hashCode5 + (activeApplicationStream != null ? activeApplicationStream.hashCode() : 0)) * 31;
        VoiceChannelJoinability voiceChannelJoinability = this.voiceChannelJoinability;
        int hashCode7 = (hashCode6 + (voiceChannelJoinability != null ? voiceChannelJoinability.hashCode() : 0)) * 31;
        GuildMaxVideoChannelUsers guildMaxVideoChannelUsers = this.guildMaxVideoChannelMembers;
        int hashCode8 = (hashCode7 + (guildMaxVideoChannelUsers != null ? guildMaxVideoChannelUsers.hashCode() : 0)) * 31;
        Guild guild = this.guild;
        int hashCode9 = (hashCode8 + (guild != null ? guild.hashCode() : 0)) * 31;
        boolean z2 = this.isChannelSelected;
        int i2 = 1;
        if (z2) {
            z2 = true;
        }
        int i3 = z2 ? 1 : 0;
        int i4 = z2 ? 1 : 0;
        int i5 = (hashCode9 + i3) * 31;
        boolean z3 = this.isMyHandRaised;
        if (!z3) {
            i2 = z3 ? 1 : 0;
        }
        int i6 = (i5 + i2) * 31;
        List<GuildScheduledEvent> list2 = this.guildScheduledEvents;
        int hashCode10 = (i6 + (list2 != null ? list2.hashCode() : 0)) * 31;
        Map<Long, EmbeddedActivity> map2 = this.embeddedActivitiesForChannel;
        int hashCode11 = (hashCode10 + (map2 != null ? map2.hashCode() : 0)) * 31;
        Map<Long, Application> map3 = this.applications;
        int hashCode12 = (hashCode11 + (map3 != null ? map3.hashCode() : 0)) * 31;
        Channel channel = this.channel;
        int hashCode13 = (((hashCode12 + (channel != null ? channel.hashCode() : 0)) * 31) + this.numUsersConnected) * 31;
        Long l = this.channelPermissions;
        int hashCode14 = (hashCode13 + (l != null ? l.hashCode() : 0)) * 31;
        StoreAudioManagerV2.State state = this.audioManagerState;
        if (state != null) {
            i = state.hashCode();
        }
        return hashCode14 + i;
    }

    public final boolean isChannelSelected() {
        return this.isChannelSelected;
    }

    public final boolean isConnected() {
        StoreVoiceParticipants.VoiceUser voiceUser = this.participants.get(Long.valueOf(this.myId));
        if (voiceUser != null) {
            return voiceUser.isConnected();
        }
        return false;
    }

    public final boolean isDeafenedByAnySource() {
        return isSelfDeafened() || isServerDeafened();
    }

    public final boolean isMeMutedByAnySource() {
        VoiceState voiceState;
        StoreVoiceParticipants.VoiceUser voiceUser = this.participants.get(Long.valueOf(this.myId));
        if (voiceUser == null || (voiceState = voiceUser.getVoiceState()) == null) {
            return false;
        }
        return d.V0(voiceState);
    }

    public final boolean isMuted() {
        VoiceState voiceState;
        StoreVoiceParticipants.VoiceUser voiceUser = this.participants.get(Long.valueOf(this.myId));
        if (voiceUser == null || (voiceState = voiceUser.getVoiceState()) == null) {
            return false;
        }
        return voiceState.e();
    }

    public final boolean isMyHandRaised() {
        return this.isMyHandRaised;
    }

    public final boolean isSelfDeafened() {
        return this.voiceSettings.isSelfDeafened();
    }

    public final boolean isServerDeafened() {
        VoiceState voiceState;
        StoreVoiceParticipants.VoiceUser voiceUser = this.participants.get(Long.valueOf(this.myId));
        if (voiceUser == null || (voiceState = voiceUser.getVoiceState()) == null) {
            return false;
        }
        return voiceState.b();
    }

    public final boolean isStreaming() {
        ModelApplicationStream stream;
        long j = this.myId;
        StoreApplicationStreaming.ActiveApplicationStream activeApplicationStream = this.activeStream;
        return (activeApplicationStream == null || (stream = activeApplicationStream.getStream()) == null || j != stream.getOwnerId()) ? false : true;
    }

    public final boolean isSuppressed() {
        VoiceState voiceState;
        StoreVoiceParticipants.VoiceUser voiceUser = this.participants.get(Long.valueOf(this.myId));
        if (voiceUser == null || (voiceState = voiceUser.getVoiceState()) == null) {
            return false;
        }
        return voiceState.l();
    }

    public final boolean isVideoCall() {
        return this.isVideoCall;
    }

    public String toString() {
        StringBuilder R = a.R("CallModel(participants=");
        R.append(this.participants);
        R.append(", myId=");
        R.append(this.myId);
        R.append(", timeConnectedMs=");
        R.append(this.timeConnectedMs);
        R.append(", videoDevices=");
        R.append(this.videoDevices);
        R.append(", selectedVideoDevice=");
        R.append(this.selectedVideoDevice);
        R.append(", voiceSettings=");
        R.append(this.voiceSettings);
        R.append(", rtcConnectionMetadata=");
        R.append(this.rtcConnectionMetadata);
        R.append(", callFeedbackTriggerRateDenominator=");
        R.append(this.callFeedbackTriggerRateDenominator);
        R.append(", streamFeedbackTriggerRateDenominator=");
        R.append(this.streamFeedbackTriggerRateDenominator);
        R.append(", activeStream=");
        R.append(this.activeStream);
        R.append(", voiceChannelJoinability=");
        R.append(this.voiceChannelJoinability);
        R.append(", guildMaxVideoChannelMembers=");
        R.append(this.guildMaxVideoChannelMembers);
        R.append(", guild=");
        R.append(this.guild);
        R.append(", isChannelSelected=");
        R.append(this.isChannelSelected);
        R.append(", isMyHandRaised=");
        R.append(this.isMyHandRaised);
        R.append(", guildScheduledEvents=");
        R.append(this.guildScheduledEvents);
        R.append(", embeddedActivitiesForChannel=");
        R.append(this.embeddedActivitiesForChannel);
        R.append(", applications=");
        R.append(this.applications);
        R.append(", channel=");
        R.append(this.channel);
        R.append(", numUsersConnected=");
        R.append(this.numUsersConnected);
        R.append(", channelPermissions=");
        R.append(this.channelPermissions);
        R.append(", audioManagerState=");
        R.append(this.audioManagerState);
        R.append(")");
        return R.toString();
    }
}
