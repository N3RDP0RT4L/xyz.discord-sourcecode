package com.discord.widgets.voice.settings;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.discord.databinding.WidgetVoiceChannelSettingsBinding;
import com.discord.views.CheckedSetting;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetVoiceChannelSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetVoiceChannelSettingsBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetVoiceChannelSettingsBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetVoiceChannelSettings$binding$2 extends k implements Function1<View, WidgetVoiceChannelSettingsBinding> {
    public static final WidgetVoiceChannelSettings$binding$2 INSTANCE = new WidgetVoiceChannelSettings$binding$2();

    public WidgetVoiceChannelSettings$binding$2() {
        super(1, WidgetVoiceChannelSettingsBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetVoiceChannelSettingsBinding;", 0);
    }

    public final WidgetVoiceChannelSettingsBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.bitrate_divider;
        View findViewById = view.findViewById(R.id.bitrate_divider);
        if (findViewById != null) {
            i = R.id.bitrate_seekbar;
            SeekBar seekBar = (SeekBar) view.findViewById(R.id.bitrate_seekbar);
            if (seekBar != null) {
                i = R.id.bitrate_title;
                TextView textView = (TextView) view.findViewById(R.id.bitrate_title);
                if (textView != null) {
                    i = R.id.channel_settings_edit_name;
                    TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.channel_settings_edit_name);
                    if (textInputLayout != null) {
                        i = R.id.channel_settings_edit_topic;
                        TextInputLayout textInputLayout2 = (TextInputLayout) view.findViewById(R.id.channel_settings_edit_topic);
                        if (textInputLayout2 != null) {
                            i = R.id.channel_settings_nsfw;
                            CheckedSetting checkedSetting = (CheckedSetting) view.findViewById(R.id.channel_settings_nsfw);
                            if (checkedSetting != null) {
                                i = R.id.channel_settings_permissions;
                                TextView textView2 = (TextView) view.findViewById(R.id.channel_settings_permissions);
                                if (textView2 != null) {
                                    i = R.id.channel_settings_region_override;
                                    TextView textView3 = (TextView) view.findViewById(R.id.channel_settings_region_override);
                                    if (textView3 != null) {
                                        i = R.id.channel_settings_region_override_container;
                                        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.channel_settings_region_override_container);
                                        if (linearLayout != null) {
                                            i = R.id.channel_settings_region_override_help;
                                            TextView textView4 = (TextView) view.findViewById(R.id.channel_settings_region_override_help);
                                            if (textView4 != null) {
                                                i = R.id.channel_settings_save;
                                                FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.channel_settings_save);
                                                if (floatingActionButton != null) {
                                                    i = R.id.channel_settings_section_privacy_safety;
                                                    LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.channel_settings_section_privacy_safety);
                                                    if (linearLayout2 != null) {
                                                        i = R.id.channel_settings_section_user_management;
                                                        LinearLayout linearLayout3 = (LinearLayout) view.findViewById(R.id.channel_settings_section_user_management);
                                                        if (linearLayout3 != null) {
                                                            i = R.id.current_bitrate_display;
                                                            TextView textView5 = (TextView) view.findViewById(R.id.current_bitrate_display);
                                                            if (textView5 != null) {
                                                                i = R.id.current_user_limit_display;
                                                                TextView textView6 = (TextView) view.findViewById(R.id.current_user_limit_display);
                                                                if (textView6 != null) {
                                                                    i = R.id.region_override_divider;
                                                                    View findViewById2 = view.findViewById(R.id.region_override_divider);
                                                                    if (findViewById2 != null) {
                                                                        i = R.id.settings_bitrate;
                                                                        LinearLayout linearLayout4 = (LinearLayout) view.findViewById(R.id.settings_bitrate);
                                                                        if (linearLayout4 != null) {
                                                                            i = R.id.settings_bitrate_help;
                                                                            TextView textView7 = (TextView) view.findViewById(R.id.settings_bitrate_help);
                                                                            if (textView7 != null) {
                                                                                i = R.id.settings_user_limit;
                                                                                LinearLayout linearLayout5 = (LinearLayout) view.findViewById(R.id.settings_user_limit);
                                                                                if (linearLayout5 != null) {
                                                                                    i = R.id.settings_user_limit_help;
                                                                                    TextView textView8 = (TextView) view.findViewById(R.id.settings_user_limit_help);
                                                                                    if (textView8 != null) {
                                                                                        i = R.id.user_limit_divider;
                                                                                        View findViewById3 = view.findViewById(R.id.user_limit_divider);
                                                                                        if (findViewById3 != null) {
                                                                                            i = R.id.user_limit_seekbar;
                                                                                            SeekBar seekBar2 = (SeekBar) view.findViewById(R.id.user_limit_seekbar);
                                                                                            if (seekBar2 != null) {
                                                                                                i = R.id.user_limit_title;
                                                                                                TextView textView9 = (TextView) view.findViewById(R.id.user_limit_title);
                                                                                                if (textView9 != null) {
                                                                                                    return new WidgetVoiceChannelSettingsBinding((CoordinatorLayout) view, findViewById, seekBar, textView, textInputLayout, textInputLayout2, checkedSetting, textView2, textView3, linearLayout, textView4, floatingActionButton, linearLayout2, linearLayout3, textView5, textView6, findViewById2, linearLayout4, textView7, linearLayout5, textView8, findViewById3, seekBar2, textView9);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
