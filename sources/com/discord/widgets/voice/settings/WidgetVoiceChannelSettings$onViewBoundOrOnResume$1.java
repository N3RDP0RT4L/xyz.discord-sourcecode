package com.discord.widgets.voice.settings;

import com.discord.widgets.voice.settings.WidgetVoiceChannelSettings;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetVoiceChannelSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Model;", "it", "", "invoke", "(Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceChannelSettings$onViewBoundOrOnResume$1 extends o implements Function1<WidgetVoiceChannelSettings.Model, Unit> {
    public final /* synthetic */ WidgetVoiceChannelSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetVoiceChannelSettings$onViewBoundOrOnResume$1(WidgetVoiceChannelSettings widgetVoiceChannelSettings) {
        super(1);
        this.this$0 = widgetVoiceChannelSettings;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WidgetVoiceChannelSettings.Model model) {
        invoke2(model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WidgetVoiceChannelSettings.Model model) {
        this.this$0.configureUI(model);
    }
}
