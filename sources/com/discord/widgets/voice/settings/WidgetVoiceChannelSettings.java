package com.discord.widgets.voice.settings;

import a0.a.a.b;
import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import b.a.a.n;
import b.a.d.j;
import b.a.i.n4;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetVoiceChannelSettingsBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelVoiceRegion;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuildVoiceRegions;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StorePermissions;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.updates.ObservationDeckProvider;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$3;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$4;
import com.discord.utilities.stateful.StatefulViews;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.voice.Bitrate;
import com.discord.views.CheckedSetting;
import com.discord.widgets.channels.permissions.WidgetChannelSettingsPermissionsOverview;
import com.discord.widgets.chat.list.TextInVoiceFeatureFlag;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import rx.functions.Action2;
import xyz.discord.R;
/* compiled from: WidgetVoiceChannelSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 62\u00020\u0001:\u000267B\u0007¢\u0006\u0004\b5\u0010+J\u0015\u0010\u0004\u001a\u00020\u0003*\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\n\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\n\u0010\tJ\u0017\u0010\r\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u000bH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u001d\u0010\u001b\u001a\u00020\u00032\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00190\u0018H\u0002¢\u0006\u0004\b\u001b\u0010\u001cJW\u0010$\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020\u00112\u0006\u0010 \u001a\u00020\u000f2\b\u0010!\u001a\u0004\u0018\u00010\u00112\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\b\u0010#\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0004\b$\u0010%J\u0017\u0010(\u001a\u00020\u00032\u0006\u0010'\u001a\u00020&H\u0016¢\u0006\u0004\b(\u0010)J\u000f\u0010*\u001a\u00020\u0003H\u0016¢\u0006\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.R\u001d\u00104\u001a\u00020/8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b0\u00101\u001a\u0004\b2\u00103¨\u00068"}, d2 = {"Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Model;", "", "configureUI", "(Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Model;)V", "", "isVisible", "configureUserLimitVisibility", "(Z)V", "configureRegionOverrideVisibility", "Lcom/discord/api/channel/Channel;", "channel", "confirmDelete", "(Lcom/discord/api/channel/Channel;)V", "", ModelAuditLogEntry.CHANGE_KEY_BITRATE, "", "getBitrateDisplayString", "(I)Ljava/lang/String;", "userLimit", "", "getUserLimitDisplayString", "(I)Ljava/lang/CharSequence;", "", "Lcom/discord/models/domain/ModelVoiceRegion;", "regions", "showRegionDialog", "(Ljava/util/List;)V", "", "channelId", "channelName", "channelType", "channelTopic", ModelAuditLogEntry.CHANGE_KEY_NSFW, "rtcRegion", "saveChannel", "(JLjava/lang/String;ILjava/lang/String;Ljava/lang/Boolean;IILjava/lang/String;)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Lcom/discord/utilities/stateful/StatefulViews;", "state", "Lcom/discord/utilities/stateful/StatefulViews;", "Lcom/discord/databinding/WidgetVoiceChannelSettingsBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetVoiceChannelSettingsBinding;", "binding", HookHelper.constructorName, "Companion", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceChannelSettings extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetVoiceChannelSettings.class, "binding", "getBinding()Lcom/discord/databinding/WidgetVoiceChannelSettingsBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String INTENT_EXTRA_CHANNEL_ID = "INTENT_EXTRA_CHANNEL_ID";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetVoiceChannelSettings$binding$2.INSTANCE, null, 2, null);
    private final StatefulViews state = new StatefulViews(R.id.channel_settings_edit_name, R.id.channel_settings_edit_topic, R.id.current_user_limit_display, R.id.current_bitrate_display, R.id.channel_settings_region_override, R.id.channel_settings_nsfw);

    /* compiled from: WidgetVoiceChannelSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0007\u0010\bR\u0016\u0010\n\u001a\u00020\t8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Companion;", "", "", "channelId", "Landroid/content/Context;", "context", "", "launch", "(JLandroid/content/Context;)V", "", WidgetVoiceChannelSettings.INTENT_EXTRA_CHANNEL_ID, "Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(long j, Context context) {
            m.checkNotNullParameter(context, "context");
            Intent putExtra = new Intent().putExtra(WidgetVoiceChannelSettings.INTENT_EXTRA_CHANNEL_ID, j);
            m.checkNotNullExpressionValue(putExtra, "Intent().putExtra(INTENT…RA_CHANNEL_ID, channelId)");
            j.d(context, WidgetVoiceChannelSettings.class, putExtra);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: WidgetVoiceChannelSettings.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u0015\b\u0082\b\u0018\u0000 /2\u00020\u0001:\u0001/B=\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0005\u0012\u0006\u0010\u0015\u001a\u00020\u0005\u0012\u0006\u0010\u0016\u001a\u00020\t\u0012\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f\u0012\u0006\u0010\u0018\u001a\u00020\u0010¢\u0006\u0004\b-\u0010.J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J\u0010\u0010\n\u001a\u00020\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÆ\u0003¢\u0006\u0004\b\u0011\u0010\u0012JR\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00052\b\b\u0002\u0010\u0015\u001a\u00020\u00052\b\b\u0002\u0010\u0016\u001a\u00020\t2\u000e\b\u0002\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f2\b\b\u0002\u0010\u0018\u001a\u00020\u0010HÆ\u0001¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u001c\u001a\u00020\u001bHÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\u001e\u0010\u000bJ\u001a\u0010 \u001a\u00020\u00052\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b \u0010!R\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\"\u001a\u0004\b#\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b%\u0010\u0007R\u0019\u0010\u0016\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010&\u001a\u0004\b'\u0010\u000bR\u0019\u0010\u0018\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0018\u0010(\u001a\u0004\b)\u0010\u0012R\u0019\u0010\u0014\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010$\u001a\u0004\b*\u0010\u0007R\u001f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010+\u001a\u0004\b,\u0010\u000f¨\u00060"}, d2 = {"Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Model;", "", "Lcom/discord/api/channel/Channel;", "component1", "()Lcom/discord/api/channel/Channel;", "", "component2", "()Z", "component3", "", "component4", "()I", "", "Lcom/discord/models/domain/ModelVoiceRegion;", "component5", "()Ljava/util/List;", "", "component6", "()J", "channel", "canManageChannel", "canManagePermissions", "maxBitrate", "regions", "guildID", "copy", "(Lcom/discord/api/channel/Channel;ZZILjava/util/List;J)Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Model;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/api/channel/Channel;", "getChannel", "Z", "getCanManagePermissions", "I", "getMaxBitrate", "J", "getGuildID", "getCanManageChannel", "Ljava/util/List;", "getRegions", HookHelper.constructorName, "(Lcom/discord/api/channel/Channel;ZZILjava/util/List;J)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        public static final Companion Companion = new Companion(null);
        private final boolean canManageChannel;
        private final boolean canManagePermissions;
        private final Channel channel;
        private final long guildID;
        private final int maxBitrate;
        private final List<ModelVoiceRegion> regions;

        /* compiled from: WidgetVoiceChannelSettings.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Model$Companion;", "", "", "channelId", "Lrx/Observable;", "Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings$Model;", "get", "(J)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes2.dex */
        public static final class Companion {
            private Companion() {
            }

            public final Observable<Model> get(long j) {
                StoreStream.Companion companion = StoreStream.Companion;
                StoreChannels channels = companion.getChannels();
                StoreGuilds guilds = companion.getGuilds();
                StoreUser users = companion.getUsers();
                StorePermissions permissions = companion.getPermissions();
                StoreGuildVoiceRegions guildVoiceRegions = companion.getGuildVoiceRegions();
                Observable<Model> q = ObservableExtensionsKt.computationLatest(ObservationDeck.connectRx$default(ObservationDeckProvider.get(), new ObservationDeck.UpdateSource[]{channels, guilds, StoreUser.Companion.getMeUpdate(), permissions, guildVoiceRegions, companion.getExperiments()}, false, null, null, new WidgetVoiceChannelSettings$Model$Companion$get$1(channels, j, guilds, permissions, users, guildVoiceRegions), 14, null)).q();
                m.checkNotNullExpressionValue(q, "ObservationDeckProvider.…  .distinctUntilChanged()");
                return q;
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        /* JADX WARN: Multi-variable type inference failed */
        public Model(Channel channel, boolean z2, boolean z3, int i, List<? extends ModelVoiceRegion> list, long j) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(list, "regions");
            this.channel = channel;
            this.canManageChannel = z2;
            this.canManagePermissions = z3;
            this.maxBitrate = i;
            this.regions = list;
            this.guildID = j;
        }

        public static /* synthetic */ Model copy$default(Model model, Channel channel, boolean z2, boolean z3, int i, List list, long j, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                channel = model.channel;
            }
            if ((i2 & 2) != 0) {
                z2 = model.canManageChannel;
            }
            boolean z4 = z2;
            if ((i2 & 4) != 0) {
                z3 = model.canManagePermissions;
            }
            boolean z5 = z3;
            if ((i2 & 8) != 0) {
                i = model.maxBitrate;
            }
            int i3 = i;
            List<ModelVoiceRegion> list2 = list;
            if ((i2 & 16) != 0) {
                list2 = model.regions;
            }
            List list3 = list2;
            if ((i2 & 32) != 0) {
                j = model.guildID;
            }
            return model.copy(channel, z4, z5, i3, list3, j);
        }

        public final Channel component1() {
            return this.channel;
        }

        public final boolean component2() {
            return this.canManageChannel;
        }

        public final boolean component3() {
            return this.canManagePermissions;
        }

        public final int component4() {
            return this.maxBitrate;
        }

        public final List<ModelVoiceRegion> component5() {
            return this.regions;
        }

        public final long component6() {
            return this.guildID;
        }

        public final Model copy(Channel channel, boolean z2, boolean z3, int i, List<? extends ModelVoiceRegion> list, long j) {
            m.checkNotNullParameter(channel, "channel");
            m.checkNotNullParameter(list, "regions");
            return new Model(channel, z2, z3, i, list, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.channel, model.channel) && this.canManageChannel == model.canManageChannel && this.canManagePermissions == model.canManagePermissions && this.maxBitrate == model.maxBitrate && m.areEqual(this.regions, model.regions) && this.guildID == model.guildID;
        }

        public final boolean getCanManageChannel() {
            return this.canManageChannel;
        }

        public final boolean getCanManagePermissions() {
            return this.canManagePermissions;
        }

        public final Channel getChannel() {
            return this.channel;
        }

        public final long getGuildID() {
            return this.guildID;
        }

        public final int getMaxBitrate() {
            return this.maxBitrate;
        }

        public final List<ModelVoiceRegion> getRegions() {
            return this.regions;
        }

        public int hashCode() {
            Channel channel = this.channel;
            int i = 0;
            int hashCode = (channel != null ? channel.hashCode() : 0) * 31;
            boolean z2 = this.canManageChannel;
            int i2 = 1;
            if (z2) {
                z2 = true;
            }
            int i3 = z2 ? 1 : 0;
            int i4 = z2 ? 1 : 0;
            int i5 = (hashCode + i3) * 31;
            boolean z3 = this.canManagePermissions;
            if (!z3) {
                i2 = z3 ? 1 : 0;
            }
            int i6 = (((i5 + i2) * 31) + this.maxBitrate) * 31;
            List<ModelVoiceRegion> list = this.regions;
            if (list != null) {
                i = list.hashCode();
            }
            return b.a(this.guildID) + ((i6 + i) * 31);
        }

        public String toString() {
            StringBuilder R = a.R("Model(channel=");
            R.append(this.channel);
            R.append(", canManageChannel=");
            R.append(this.canManageChannel);
            R.append(", canManagePermissions=");
            R.append(this.canManagePermissions);
            R.append(", maxBitrate=");
            R.append(this.maxBitrate);
            R.append(", regions=");
            R.append(this.regions);
            R.append(", guildID=");
            return a.B(R, this.guildID, ")");
        }
    }

    public WidgetVoiceChannelSettings() {
        super(R.layout.widget_voice_channel_settings);
    }

    private final void configureRegionOverrideVisibility(boolean z2) {
        View view = getBinding().o;
        m.checkNotNullExpressionValue(view, "binding.regionOverrideDivider");
        int i = 0;
        view.setVisibility(z2 ? 0 : 8);
        LinearLayout linearLayout = getBinding().h;
        m.checkNotNullExpressionValue(linearLayout, "binding.channelSettingsRegionOverrideContainer");
        linearLayout.setVisibility(z2 ? 0 : 8);
        TextView textView = getBinding().i;
        m.checkNotNullExpressionValue(textView, "binding.channelSettingsRegionOverrideHelp");
        if (!z2) {
            i = 8;
        }
        textView.setVisibility(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final Model model) {
        Object obj;
        String str;
        if (model == null || (!model.getCanManageChannel() && !model.getCanManagePermissions())) {
            FragmentActivity activity = e();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        boolean z2 = true;
        this.state.clear(true);
        AppFragment.setActionBarDisplayHomeAsUpEnabled$default(this, false, 1, null);
        setActionBarTitle(R.string.channel_settings);
        setActionBarSubtitle(ChannelUtils.e(model.getChannel(), requireContext(), false, 2));
        AppFragment.setActionBarOptionsMenu$default(this, R.menu.menu_voice_channel_settings, new Action2<MenuItem, Context>() { // from class: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$configureUI$1
            public final void call(MenuItem menuItem, Context context) {
                m.checkNotNullExpressionValue(menuItem, "menuItem");
                if (menuItem.getItemId() == R.id.menu_channel_settings_delete) {
                    WidgetVoiceChannelSettings.this.confirmDelete(model.getChannel());
                }
            }
        }, null, 4, null);
        TextView textView = getBinding().n;
        m.checkNotNullExpressionValue(textView, "binding.currentUserLimitDisplay");
        StatefulViews statefulViews = this.state;
        TextView textView2 = getBinding().n;
        m.checkNotNullExpressionValue(textView2, "binding.currentUserLimitDisplay");
        textView.setText((CharSequence) statefulViews.get(textView2.getId(), getUserLimitDisplayString(model.getChannel().B())));
        TextView textView3 = getBinding().r;
        m.checkNotNullExpressionValue(textView3, "binding.settingsUserLimitHelp");
        b.a.k.b.m(textView3, R.string.form_help_user_limit, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        SeekBar seekBar = getBinding().t;
        m.checkNotNullExpressionValue(seekBar, "binding.userLimitSeekbar");
        seekBar.setProgress(model.getChannel().B());
        getBinding().t.setOnSeekBarChangeListener(new b.a.y.j() { // from class: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$configureUI$2
            @Override // b.a.y.j, android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar2, int i, boolean z3) {
                WidgetVoiceChannelSettingsBinding binding;
                CharSequence userLimitDisplayString;
                m.checkNotNullParameter(seekBar2, "seekBar");
                binding = WidgetVoiceChannelSettings.this.getBinding();
                TextView textView4 = binding.n;
                m.checkNotNullExpressionValue(textView4, "binding.currentUserLimitDisplay");
                userLimitDisplayString = WidgetVoiceChannelSettings.this.getUserLimitDisplayString(i);
                textView4.setText(userLimitDisplayString);
            }
        });
        configureUserLimitVisibility(model.getChannel().A() == 2);
        int c = model.getChannel().c() / 1000;
        TextView textView4 = getBinding().m;
        m.checkNotNullExpressionValue(textView4, "binding.currentBitrateDisplay");
        StatefulViews statefulViews2 = this.state;
        TextView textView5 = getBinding().m;
        m.checkNotNullExpressionValue(textView5, "binding.currentBitrateDisplay");
        textView4.setText((CharSequence) statefulViews2.get(textView5.getId(), getBitrateDisplayString(c)));
        TextView textView6 = getBinding().p;
        m.checkNotNullExpressionValue(textView6, "binding.settingsBitrateHelp");
        b.a.k.b.m(textView6, R.string.form_help_bitrate, new Object[]{"64"}, (r4 & 4) != 0 ? b.g.j : null);
        SeekBar seekBar2 = getBinding().f2675b;
        m.checkNotNullExpressionValue(seekBar2, "binding.bitrateSeekbar");
        int maxBitrate = model.getMaxBitrate();
        Bitrate bitrate = Bitrate.MIN;
        seekBar2.setMax(maxBitrate - bitrate.getKbps());
        SeekBar seekBar3 = getBinding().f2675b;
        m.checkNotNullExpressionValue(seekBar3, "binding.bitrateSeekbar");
        seekBar3.setProgress(c - bitrate.getKbps());
        getBinding().f2675b.setOnSeekBarChangeListener(new b.a.y.j() { // from class: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$configureUI$3
            @Override // b.a.y.j, android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar4, int i, boolean z3) {
                WidgetVoiceChannelSettingsBinding binding;
                String bitrateDisplayString;
                m.checkNotNullParameter(seekBar4, "seekBar");
                binding = WidgetVoiceChannelSettings.this.getBinding();
                TextView textView7 = binding.m;
                m.checkNotNullExpressionValue(textView7, "binding.currentBitrateDisplay");
                bitrateDisplayString = WidgetVoiceChannelSettings.this.getBitrateDisplayString(Bitrate.MIN.getKbps() + i);
                textView7.setText(bitrateDisplayString);
            }
        });
        StatefulViews statefulViews3 = this.state;
        TextView textView7 = getBinding().g;
        m.checkNotNullExpressionValue(textView7, "binding.channelSettingsRegionOverride");
        String str2 = (String) statefulViews3.get(textView7.getId(), model.getChannel().x());
        Iterator<T> it = model.getRegions().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (m.areEqual(((ModelVoiceRegion) obj).getId(), str2)) {
                break;
            }
        }
        ModelVoiceRegion modelVoiceRegion = (ModelVoiceRegion) obj;
        if (modelVoiceRegion == null || (str = modelVoiceRegion.getName()) == null) {
            str = getString(R.string.automatic_region);
            m.checkNotNullExpressionValue(str, "getString(R.string.automatic_region)");
        }
        TextView textView8 = getBinding().g;
        m.checkNotNullExpressionValue(textView8, "binding.channelSettingsRegionOverride");
        textView8.setText(str);
        TextView textView9 = getBinding().i;
        m.checkNotNullExpressionValue(textView9, "binding.channelSettingsRegionOverrideHelp");
        b.a.k.b.m(textView9, R.string.form_help_region_override, new Object[0], (r4 & 4) != 0 ? b.g.j : null);
        getBinding().h.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$configureUI$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetVoiceChannelSettings.this.showRegionDialog(model.getRegions());
            }
        });
        configureRegionOverrideVisibility(model.getCanManageChannel());
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.channelSettingsEditName");
        StatefulViews statefulViews4 = this.state;
        TextInputLayout textInputLayout2 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.channelSettingsEditName");
        ViewExtensions.setText(textInputLayout, (CharSequence) statefulViews4.get(textInputLayout2.getId(), ChannelUtils.c(model.getChannel())));
        TextInputLayout textInputLayout3 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.channelSettingsEditTopic");
        int i = 8;
        textInputLayout3.setVisibility(8);
        TextInputLayout textInputLayout4 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.channelSettingsEditTopic");
        StatefulViews statefulViews5 = this.state;
        TextInputLayout textInputLayout5 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout5, "binding.channelSettingsEditTopic");
        int id2 = textInputLayout5.getId();
        String z3 = model.getChannel().z();
        if (z3 == null) {
            z3 = "";
        }
        ViewExtensions.setText(textInputLayout4, (CharSequence) statefulViews5.get(id2, z3));
        LinearLayout linearLayout = getBinding().l;
        m.checkNotNullExpressionValue(linearLayout, "binding.channelSettingsSectionUserManagement");
        linearLayout.setVisibility(model.getCanManagePermissions() ? 0 : 8);
        getBinding().f.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$configureUI$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WidgetChannelSettingsPermissionsOverview.Companion.launch(WidgetVoiceChannelSettings.this.requireContext(), model.getChannel().h());
            }
        });
        LinearLayout linearLayout2 = getBinding().k;
        m.checkNotNullExpressionValue(linearLayout2, "binding.channelSettingsSectionPrivacySafety");
        if (!model.getCanManageChannel() || !ChannelUtils.E(model.getChannel()) || !TextInVoiceFeatureFlag.Companion.getINSTANCE().isEnabled(Long.valueOf(model.getChannel().f()))) {
            z2 = false;
        }
        linearLayout2.setVisibility(z2 ? 0 : 8);
        CheckedSetting checkedSetting = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting, "binding.channelSettingsNsfw");
        if (model.getCanManageChannel()) {
            i = 0;
        }
        checkedSetting.setVisibility(i);
        StatefulViews statefulViews6 = this.state;
        CheckedSetting checkedSetting2 = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting2, "binding.channelSettingsNsfw");
        getBinding().e.g(((Boolean) statefulViews6.get(checkedSetting2.getId(), Boolean.valueOf(model.getChannel().o()))).booleanValue(), false);
        getBinding().j.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$configureUI$6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StatefulViews statefulViews7;
                WidgetVoiceChannelSettingsBinding binding;
                StatefulViews statefulViews8;
                WidgetVoiceChannelSettingsBinding binding2;
                WidgetVoiceChannelSettingsBinding binding3;
                WidgetVoiceChannelSettingsBinding binding4;
                WidgetVoiceChannelSettingsBinding binding5;
                StatefulViews statefulViews9;
                WidgetVoiceChannelSettingsBinding binding6;
                WidgetVoiceChannelSettings widgetVoiceChannelSettings = WidgetVoiceChannelSettings.this;
                long h = model.getChannel().h();
                statefulViews7 = WidgetVoiceChannelSettings.this.state;
                binding = WidgetVoiceChannelSettings.this.getBinding();
                TextInputLayout textInputLayout6 = binding.c;
                m.checkNotNullExpressionValue(textInputLayout6, "binding.channelSettingsEditName");
                String str3 = (String) statefulViews7.get(textInputLayout6.getId(), ChannelUtils.c(model.getChannel()));
                statefulViews8 = WidgetVoiceChannelSettings.this.state;
                binding2 = WidgetVoiceChannelSettings.this.getBinding();
                TextInputLayout textInputLayout7 = binding2.d;
                m.checkNotNullExpressionValue(textInputLayout7, "binding.channelSettingsEditTopic");
                int id3 = textInputLayout7.getId();
                String z4 = model.getChannel().z();
                if (z4 == null) {
                    z4 = "";
                }
                String str4 = (String) statefulViews8.get(id3, z4);
                binding3 = WidgetVoiceChannelSettings.this.getBinding();
                CheckedSetting checkedSetting3 = binding3.e;
                m.checkNotNullExpressionValue(checkedSetting3, "binding.channelSettingsNsfw");
                Boolean valueOf = Boolean.valueOf(checkedSetting3.isChecked());
                int A = model.getChannel().A();
                binding4 = WidgetVoiceChannelSettings.this.getBinding();
                SeekBar seekBar4 = binding4.t;
                m.checkNotNullExpressionValue(seekBar4, "binding.userLimitSeekbar");
                int progress = seekBar4.getProgress();
                binding5 = WidgetVoiceChannelSettings.this.getBinding();
                SeekBar seekBar5 = binding5.f2675b;
                m.checkNotNullExpressionValue(seekBar5, "binding.bitrateSeekbar");
                int progress2 = seekBar5.getProgress();
                statefulViews9 = WidgetVoiceChannelSettings.this.state;
                binding6 = WidgetVoiceChannelSettings.this.getBinding();
                TextView textView10 = binding6.g;
                m.checkNotNullExpressionValue(textView10, "binding.channelSettingsRegionOverride");
                widgetVoiceChannelSettings.saveChannel(h, str3, A, str4, valueOf, progress, (Bitrate.MIN.getKbps() + progress2) * 1000, (String) statefulViews9.get(textView10.getId(), model.getChannel().x()));
            }
        });
        this.state.configureSaveActionView(getBinding().j);
    }

    private final void configureUserLimitVisibility(boolean z2) {
        View view = getBinding().f2676s;
        m.checkNotNullExpressionValue(view, "binding.userLimitDivider");
        int i = 0;
        view.setVisibility(z2 ? 0 : 8);
        TextView textView = getBinding().u;
        m.checkNotNullExpressionValue(textView, "binding.userLimitTitle");
        textView.setVisibility(z2 ? 0 : 8);
        LinearLayout linearLayout = getBinding().q;
        m.checkNotNullExpressionValue(linearLayout, "binding.settingsUserLimit");
        linearLayout.setVisibility(z2 ? 0 : 8);
        TextView textView2 = getBinding().r;
        m.checkNotNullExpressionValue(textView2, "binding.settingsUserLimitHelp");
        if (!z2) {
            i = 8;
        }
        textView2.setVisibility(i);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void confirmDelete(final Channel channel) {
        n4 a = n4.a(LayoutInflater.from(getContext()), null, false);
        m.checkNotNullExpressionValue(a, "WidgetChannelSettingsDel…om(context), null, false)");
        LinearLayout linearLayout = a.a;
        m.checkNotNullExpressionValue(linearLayout, "binding.root");
        final AlertDialog create = new AlertDialog.Builder(linearLayout.getContext()).setView(a.a).create();
        m.checkNotNullExpressionValue(create, "AlertDialog.Builder(bind…ew(binding.root).create()");
        a.c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$confirmDelete$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AlertDialog.this.dismiss();
            }
        });
        a.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$confirmDelete$2

            /* compiled from: WidgetVoiceChannelSettings.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.voice.settings.WidgetVoiceChannelSettings$confirmDelete$2$2  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass2 extends o implements Function1<Channel, Unit> {
                public AnonymousClass2() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
                    invoke2(channel);
                    return Unit.a;
                }

                /* renamed from: invoke  reason: avoid collision after fix types in other method */
                public final void invoke2(Channel channel) {
                    m.checkNotNullParameter(channel, "channel");
                    Integer b2 = ChannelUtils.b(channel);
                    if (b2 != null) {
                        b.a.d.m.i(WidgetVoiceChannelSettings.this, b2.intValue(), 0, 4);
                    }
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Observable ui$default = ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().deleteChannel(channel.h()), false, 1, null), WidgetVoiceChannelSettings.this, null, 2, null);
                m.checkNotNullExpressionValue(view, "v");
                ObservableExtensionsKt.appSubscribe(ui$default, (r18 & 1) != 0 ? null : view.getContext(), "javaClass", (r18 & 4) != 0 ? null : null, new AnonymousClass2(), (r18 & 16) != 0 ? null : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$3.INSTANCE : null, (r18 & 64) != 0 ? ObservableExtensionsKt$appSubscribe$4.INSTANCE : null);
            }
        });
        TextView textView = a.f164b;
        m.checkNotNullExpressionValue(textView, "binding.channelSettingsDeleteBody");
        LinearLayout linearLayout2 = a.a;
        m.checkNotNullExpressionValue(linearLayout2, "binding.root");
        Context context = linearLayout2.getContext();
        m.checkNotNullExpressionValue(context, "binding.root.context");
        b.a.k.b.m(textView, R.string.delete_channel_body, new Object[]{ChannelUtils.e(channel, context, false, 2)}, (r4 & 4) != 0 ? b.g.j : null);
        create.show();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetVoiceChannelSettingsBinding getBinding() {
        return (WidgetVoiceChannelSettingsBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final String getBitrateDisplayString(int i) {
        return i + " Kbps";
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final CharSequence getUserLimitDisplayString(int i) {
        CharSequence c;
        if (i == 0) {
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            c = b.a.k.b.c(resources, R.string.no_user_limit, new Object[0], (r4 & 4) != 0 ? b.d.j : null);
            return c;
        }
        Resources resources2 = getResources();
        m.checkNotNullExpressionValue(resources2, "resources");
        return StringResourceUtilsKt.getQuantityString(resources2, requireContext(), (int) R.plurals.num_users_num, i, Integer.valueOf(i));
    }

    public static final void launch(long j, Context context) {
        Companion.launch(j, context);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void saveChannel(long j, String str, int i, String str2, Boolean bool, int i2, int i3, String str3) {
        ObservableExtensionsKt.ui$default(RestAPI.Companion.getApiSerializeNulls().editVoiceChannel(j, str, str2, bool, Integer.valueOf(i), Integer.valueOf(i3), Integer.valueOf(i2), str3), this, null, 2, null).k(b.a.d.o.a.g(getContext(), new WidgetVoiceChannelSettings$saveChannel$1(this), null));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void showRegionDialog(List<? extends ModelVoiceRegion> list) {
        CharSequence e;
        CharSequence e2;
        n.a aVar = n.k;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        e = b.a.k.b.e(this, R.string.form_label_region_override, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        e2 = b.a.k.b.e(this, R.string.automatic_region, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
        List listOf = d0.t.m.listOf(e2);
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(list, 10));
        for (ModelVoiceRegion modelVoiceRegion : list) {
            arrayList.add(modelVoiceRegion.getName());
        }
        Object[] array = u.plus((Collection) listOf, (Iterable) arrayList).toArray(new CharSequence[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        aVar.a(parentFragmentManager, e, (CharSequence[]) array, new WidgetVoiceChannelSettings$showRegionDialog$2(this, list));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setRetainInstance(true);
        this.state.setupUnsavedChangesConfirmation(this);
        StatefulViews statefulViews = this.state;
        TextInputLayout textInputLayout = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout, "binding.channelSettingsEditTopic");
        statefulViews.addOptionalFields(textInputLayout);
        StatefulViews statefulViews2 = this.state;
        TextView textView = getBinding().g;
        m.checkNotNullExpressionValue(textView, "binding.channelSettingsRegionOverride");
        statefulViews2.addOptionalFields(textView);
        StatefulViews statefulViews3 = this.state;
        FloatingActionButton floatingActionButton = getBinding().j;
        TextInputLayout textInputLayout2 = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout2, "binding.channelSettingsEditName");
        TextInputLayout textInputLayout3 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout3, "binding.channelSettingsEditTopic");
        TextView textView2 = getBinding().n;
        m.checkNotNullExpressionValue(textView2, "binding.currentUserLimitDisplay");
        TextView textView3 = getBinding().m;
        m.checkNotNullExpressionValue(textView3, "binding.currentBitrateDisplay");
        CheckedSetting checkedSetting = getBinding().e;
        m.checkNotNullExpressionValue(checkedSetting, "binding.channelSettingsNsfw");
        statefulViews3.setupTextWatcherWithSaveAction(this, floatingActionButton, textInputLayout2, textInputLayout3, textView2, textView3, checkedSetting);
        TextInputLayout textInputLayout4 = getBinding().d;
        m.checkNotNullExpressionValue(textInputLayout4, "binding.channelSettingsEditTopic");
        ViewExtensions.interceptScrollWhenInsideScrollable(textInputLayout4);
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(Model.Companion.get(getMostRecentIntent().getLongExtra(INTENT_EXTRA_CHANNEL_ID, -1L)), this, null, 2, null), WidgetVoiceChannelSettings.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetVoiceChannelSettings$onViewBoundOrOnResume$1(this));
    }
}
