package com.discord.widgets.voice.settings;

import android.widget.TextView;
import b.a.k.b;
import com.discord.databinding.WidgetVoiceChannelSettingsBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelVoiceRegion;
import com.discord.utilities.stateful.StatefulViews;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetVoiceChannelSettings.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", ModelAuditLogEntry.CHANGE_KEY_POSITION, "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetVoiceChannelSettings$showRegionDialog$2 extends o implements Function1<Integer, Unit> {
    public final /* synthetic */ List $regions;
    public final /* synthetic */ WidgetVoiceChannelSettings this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetVoiceChannelSettings$showRegionDialog$2(WidgetVoiceChannelSettings widgetVoiceChannelSettings, List list) {
        super(1);
        this.this$0 = widgetVoiceChannelSettings;
        this.$regions = list;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke(num.intValue());
        return Unit.a;
    }

    public final void invoke(int i) {
        StatefulViews statefulViews;
        WidgetVoiceChannelSettingsBinding binding;
        StatefulViews statefulViews2;
        WidgetVoiceChannelSettingsBinding binding2;
        WidgetVoiceChannelSettingsBinding binding3;
        StatefulViews statefulViews3;
        WidgetVoiceChannelSettingsBinding binding4;
        WidgetVoiceChannelSettingsBinding binding5;
        CharSequence e;
        if (i == 0) {
            statefulViews3 = this.this$0.state;
            binding4 = this.this$0.getBinding();
            TextView textView = binding4.g;
            m.checkNotNullExpressionValue(textView, "binding.channelSettingsRegionOverride");
            statefulViews3.put(textView.getId(), null);
            binding5 = this.this$0.getBinding();
            TextView textView2 = binding5.g;
            m.checkNotNullExpressionValue(textView2, "binding.channelSettingsRegionOverride");
            e = b.e(this.this$0, R.string.automatic_region, new Object[0], (r4 & 4) != 0 ? b.a.j : null);
            textView2.setText(e);
        } else {
            statefulViews2 = this.this$0.state;
            binding2 = this.this$0.getBinding();
            TextView textView3 = binding2.g;
            m.checkNotNullExpressionValue(textView3, "binding.channelSettingsRegionOverride");
            int i2 = i - 1;
            statefulViews2.put(textView3.getId(), ((ModelVoiceRegion) this.$regions.get(i2)).getId());
            binding3 = this.this$0.getBinding();
            TextView textView4 = binding3.g;
            m.checkNotNullExpressionValue(textView4, "binding.channelSettingsRegionOverride");
            textView4.setText(((ModelVoiceRegion) this.$regions.get(i2)).getName());
        }
        statefulViews = this.this$0.state;
        binding = this.this$0.getBinding();
        statefulViews.configureSaveActionView(binding.j);
    }
}
