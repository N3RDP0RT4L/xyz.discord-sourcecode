package com.discord.widgets.voice;

import andhook.lib.HookHelper;
import android.content.Context;
import androidx.fragment.app.FragmentManager;
import b.c.a.a0.d;
import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.app.AppBottomSheet;
import com.discord.app.AppComponent;
import com.discord.app.AppFragment;
import com.discord.app.AppPermissionsRequests;
import com.discord.stores.StoreStream;
import com.discord.utilities.guilds.MemberVerificationUtils;
import com.discord.utilities.user.UserUtils;
import com.discord.widgets.stage.StageChannelJoinHelper;
import com.discord.widgets.voice.fullscreen.WidgetGuildCallOnboardingSheet;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: VoiceUtils.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0007¢\u0006\u0004\b\u0007\u0010\u000bJ7\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0012H\u0007¢\u0006\u0004\b\u0007\u0010\u0014J/\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0018J/\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\t2\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u0019J=\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u0015H\u0007¢\u0006\u0004\b\u0017\u0010\u001a¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/voice/VoiceUtils;", "", "Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/app/AppBottomSheet;", "appBottomSheet", "", "handleCallChannel", "(Lcom/discord/api/channel/Channel;Lcom/discord/app/AppBottomSheet;)V", "Lcom/discord/app/AppFragment;", "appFragment", "(Lcom/discord/api/channel/Channel;Lcom/discord/app/AppFragment;)V", "Lcom/discord/app/AppComponent;", "appComponent", "Lcom/discord/app/AppPermissionsRequests;", "appPermissionsRequests", "Landroid/content/Context;", "context", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "(Lcom/discord/api/channel/Channel;Lcom/discord/app/AppComponent;Lcom/discord/app/AppPermissionsRequests;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V", "Lkotlin/Function0;", "onEventStarted", "handleConnectToEventChannel", "(Lcom/discord/api/channel/Channel;Lcom/discord/app/AppBottomSheet;Lkotlin/jvm/functions/Function0;)V", "(Lcom/discord/api/channel/Channel;Lcom/discord/app/AppFragment;Lkotlin/jvm/functions/Function0;)V", "(Lcom/discord/api/channel/Channel;Lcom/discord/app/AppPermissionsRequests;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class VoiceUtils {
    public static final VoiceUtils INSTANCE = new VoiceUtils();

    private VoiceUtils() {
    }

    public static final void handleCallChannel(Channel channel, AppBottomSheet appBottomSheet) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(appBottomSheet, "appBottomSheet");
        Context requireContext = appBottomSheet.requireContext();
        m.checkNotNullExpressionValue(requireContext, "appBottomSheet.requireContext()");
        FragmentManager parentFragmentManager = appBottomSheet.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "appBottomSheet.parentFragmentManager");
        handleCallChannel(channel, appBottomSheet, appBottomSheet, requireContext, parentFragmentManager);
    }

    public static final void handleConnectToEventChannel(Channel channel, AppBottomSheet appBottomSheet, Function0<Unit> function0) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(appBottomSheet, "appBottomSheet");
        m.checkNotNullParameter(function0, "onEventStarted");
        Context requireContext = appBottomSheet.requireContext();
        m.checkNotNullExpressionValue(requireContext, "appBottomSheet.requireContext()");
        FragmentManager parentFragmentManager = appBottomSheet.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "appBottomSheet.parentFragmentManager");
        handleConnectToEventChannel(channel, appBottomSheet, requireContext, parentFragmentManager, function0);
    }

    public static /* synthetic */ void handleConnectToEventChannel$default(Channel channel, AppBottomSheet appBottomSheet, Function0 function0, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = VoiceUtils$handleConnectToEventChannel$1.INSTANCE;
        }
        handleConnectToEventChannel(channel, appBottomSheet, function0);
    }

    public static /* synthetic */ void handleConnectToEventChannel$default(Channel channel, AppFragment appFragment, Function0 function0, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = VoiceUtils$handleConnectToEventChannel$2.INSTANCE;
        }
        handleConnectToEventChannel(channel, appFragment, function0);
    }

    public static final void handleCallChannel(Channel channel, AppFragment appFragment) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(appFragment, "appFragment");
        Context requireContext = appFragment.requireContext();
        FragmentManager parentFragmentManager = appFragment.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "appFragment.parentFragmentManager");
        handleCallChannel(channel, appFragment, appFragment, requireContext, parentFragmentManager);
    }

    public static final void handleConnectToEventChannel(Channel channel, AppFragment appFragment, Function0<Unit> function0) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(appFragment, "appFragment");
        m.checkNotNullParameter(function0, "onEventStarted");
        Context requireContext = appFragment.requireContext();
        FragmentManager parentFragmentManager = appFragment.getParentFragmentManager();
        m.checkNotNullExpressionValue(parentFragmentManager, "appFragment.parentFragmentManager");
        handleConnectToEventChannel(channel, appFragment, requireContext, parentFragmentManager, function0);
    }

    public static final void handleCallChannel(Channel channel, AppComponent appComponent, AppPermissionsRequests appPermissionsRequests, Context context, FragmentManager fragmentManager) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(appComponent, "appComponent");
        m.checkNotNullParameter(appPermissionsRequests, "appPermissionsRequests");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        UserUtils userUtils = UserUtils.INSTANCE;
        StoreStream.Companion companion = StoreStream.Companion;
        boolean isNewUser$default = UserUtils.isNewUser$default(userUtils, companion.getUsers().getMe(), null, 1, null);
        Channel selectedVoiceChannel = companion.getVoiceChannelSelected().getSelectedVoiceChannel();
        MemberVerificationUtils.maybeShowVerificationGate$default(MemberVerificationUtils.INSTANCE, context, fragmentManager, channel.f(), "Guild Voice", null, null, new VoiceUtils$handleCallChannel$1(channel, appPermissionsRequests, appComponent, context, fragmentManager, WidgetGuildCallOnboardingSheet.Companion.hasUserSeenVoiceChannelOnboarding(context), isNewUser$default, selectedVoiceChannel), 48, null);
    }

    public static final void handleConnectToEventChannel(Channel channel, AppPermissionsRequests appPermissionsRequests, Context context, FragmentManager fragmentManager, Function0<Unit> function0) {
        m.checkNotNullParameter(channel, "channel");
        m.checkNotNullParameter(appPermissionsRequests, "appPermissionsRequests");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(fragmentManager, "fragmentManager");
        m.checkNotNullParameter(function0, "onEventStarted");
        if (ChannelUtils.z(channel)) {
            StageChannelJoinHelper.connectToStage$default(StageChannelJoinHelper.INSTANCE, context, fragmentManager, channel.h(), false, false, null, null, null, null, new VoiceUtils$handleConnectToEventChannel$3(appPermissionsRequests, channel, function0), 504, null);
        } else if (ChannelUtils.E(channel)) {
            d.S1(appPermissionsRequests, null, new VoiceUtils$handleConnectToEventChannel$4(channel, context, fragmentManager, function0), 1, null);
        }
    }
}
