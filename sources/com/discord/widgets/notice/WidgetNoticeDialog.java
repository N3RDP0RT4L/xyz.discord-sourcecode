package com.discord.widgets.notice;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.AttrRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppActivity;
import com.discord.app.AppDialog;
import com.discord.app.AppLog;
import com.discord.databinding.WidgetNoticeDialogBinding;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.google.android.material.button.MaterialButton;
import d0.g0.t;
import d0.o;
import d0.t.h0;
import d0.z.d.m;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetNoticeDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 12\u00020\u0001:\u0003231B\u0007¢\u0006\u0004\b/\u00100J)\u0010\u0007\u001a\u0004\u0018\u00010\u0005*\u0004\u0018\u00010\u00022\u0006\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0019\u0010\r\u001a\u00020\f2\n\u0010\u000b\u001a\u00060\tj\u0002`\n¢\u0006\u0004\b\r\u0010\u000eJ\u0019\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0004\b\u0011\u0010\u0012J\u0017\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0014\u001a\u00020\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J\u0017\u0010\u0019\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u0017H\u0016¢\u0006\u0004\b\u0019\u0010\u001aR<\u0010\u001d\u001a\u001c\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\f0\u001c\u0018\u00010\u001b8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001d\u0010(\u001a\u00020#8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R*\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010)8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0019\u0010*\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.¨\u00064"}, d2 = {"Lcom/discord/widgets/notice/WidgetNoticeDialog;", "Lcom/discord/app/AppDialog;", "Landroid/os/Bundle;", "", "key", "", "index", "insertLayoutInBundle", "(Landroid/os/Bundle;Ljava/lang/String;I)Ljava/lang/Integer;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "exception", "", "logOnStartError", "(Ljava/lang/Exception;)V", "savedInstanceState", "Landroid/app/Dialog;", "onCreateDialog", "(Landroid/os/Bundle;)Landroid/app/Dialog;", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Landroid/content/DialogInterface;", "dialog", "onDismiss", "(Landroid/content/DialogInterface;)V", "", "Lkotlin/Function1;", "listenerMap", "Ljava/util/Map;", "getListenerMap", "()Ljava/util/Map;", "setListenerMap", "(Ljava/util/Map;)V", "Lcom/discord/databinding/WidgetNoticeDialogBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetNoticeDialogBinding;", "binding", "Lkotlin/Function0;", "Lkotlin/jvm/functions/Function0;", "getOnDismiss", "()Lkotlin/jvm/functions/Function0;", "setOnDismiss", "(Lkotlin/jvm/functions/Function0;)V", HookHelper.constructorName, "()V", "Companion", "ActionLinkMovementMethod", "Builder", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetNoticeDialog extends AppDialog {
    private static final String ARG_ABOVE_LAYOUT_ID = "above_layout_id";
    private static final String ARG_BELOW_LAYOUT_ID = "below_layout_id";
    private static final String ARG_BODY_TEXT = "body_text";
    private static final String ARG_BODY_TEXT_ALIGNMENT = "body_text_alignment";
    private static final String ARG_CANCELABLE = "cancelable";
    private static final String ARG_CANCEL_TEXT = "cancel_text";
    private static final String ARG_HEADER_TEXT = "header_text";
    private static final String ARG_NOTICE_TYPE = "notice_type";
    private static final String ARG_OK_TEXT = "ok_text";
    private static final String ARG_STACK_TRACE_CALLER_FRAME = "stack_trace_caller_frame";
    private static final String ARG_THEME_ID = "theme_id";
    public static final int CANCEL_BUTTON = 2131364483;
    public static final int OK_BUTTON = 2131364488;
    public static final int ON_SHOW = 0;
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetNoticeDialog$binding$2.INSTANCE, null, 2, null);
    private Map<Integer, ? extends Function1<? super View, Unit>> listenerMap;
    private Function0<Unit> onDismiss;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetNoticeDialog.class, "binding", "getBinding()Lcom/discord/databinding/WidgetNoticeDialogBinding;", 0)};
    public static final Companion Companion = new Companion(null);

    /* compiled from: WidgetNoticeDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¢\u0006\u0004\b\t\u0010\nR\u001c\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lcom/discord/widgets/notice/WidgetNoticeDialog$ActionLinkMovementMethod;", "Landroid/text/method/LinkMovementMethod;", "Landroid/widget/TextView;", "widget", "Landroid/text/Spannable;", "buffer", "Landroid/view/MotionEvent;", "event", "", "onTouchEvent", "(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z", "Lkotlin/Function0;", "", "linkAction", "Lkotlin/jvm/functions/Function0;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function0;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class ActionLinkMovementMethod extends LinkMovementMethod {
        private final Function0<Unit> linkAction;

        public ActionLinkMovementMethod(Function0<Unit> function0) {
            m.checkNotNullParameter(function0, "linkAction");
            this.linkAction = function0;
        }

        @Override // android.text.method.LinkMovementMethod, android.text.method.ScrollingMovementMethod, android.text.method.BaseMovementMethod, android.text.method.MovementMethod
        public boolean onTouchEvent(TextView textView, Spannable spannable, MotionEvent motionEvent) {
            m.checkNotNullParameter(textView, "widget");
            m.checkNotNullParameter(spannable, "buffer");
            m.checkNotNullParameter(motionEvent, "event");
            boolean onTouchEvent = super.onTouchEvent(textView, spannable, motionEvent);
            if (motionEvent.getAction() == 1) {
                this.linkAction.invoke();
            }
            return onTouchEvent;
        }
    }

    /* compiled from: WidgetNoticeDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010+\u001a\u00020*¢\u0006\u0004\b1\u00102J\u0015\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\u0017\u0010\u0004\u001a\u00020\u00002\b\b\u0001\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u0004\u0010\bJ\u0015\u0010\t\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\t\u0010\u0005J\u0017\u0010\t\u001a\u00020\u00002\b\b\u0001\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\t\u0010\bJ+\u0010\u000f\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\n2\u0014\b\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b¢\u0006\u0004\b\u000f\u0010\u0010J-\u0010\u000f\u001a\u00020\u00002\b\b\u0001\u0010\u0007\u001a\u00020\u00062\u0014\b\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b¢\u0006\u0004\b\u000f\u0010\u0011J\u0017\u0010\u0013\u001a\u00020\u00002\b\b\u0001\u0010\u0012\u001a\u00020\u0006¢\u0006\u0004\b\u0013\u0010\bJ+\u0010\u0015\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\n2\u0014\b\u0002\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b¢\u0006\u0004\b\u0015\u0010\u0010J-\u0010\u0015\u001a\u00020\u00002\b\b\u0001\u0010\u0007\u001a\u00020\u00062\u0014\b\u0002\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b¢\u0006\u0004\b\u0015\u0010\u0011J\u0015\u0010\u0017\u001a\u00020\u00002\u0006\u0010\u0016\u001a\u00020\n¢\u0006\u0004\b\u0017\u0010\u0018J\u0015\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u0019¢\u0006\u0004\b\u001b\u0010\u001cJ\u0015\u0010\u001f\u001a\u00020\r2\u0006\u0010\u001e\u001a\u00020\u001d¢\u0006\u0004\b\u001f\u0010 R\u0016\u0010!\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b!\u0010\"R\u0016\u0010#\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b#\u0010$R\"\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000e\u0010%R\u0018\u0010&\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\"\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u0010%R\u0016\u0010(\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b(\u0010)R\u0019\u0010+\u001a\u00020*8\u0006@\u0006¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u0018\u0010\u0016\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0016\u0010\"R\u0016\u0010/\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b/\u0010)R\u0016\u00100\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u0010\"¨\u00063"}, d2 = {"Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;", "", "", NotificationCompat.MessagingStyle.Message.KEY_TEXT, "setTitle", "(Ljava/lang/CharSequence;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;", "", "stringResId", "(I)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;", "setMessage", "", "Lkotlin/Function1;", "Landroid/view/View;", "", "onConfirm", "setPositiveButton", "(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;", "(ILkotlin/jvm/functions/Function1;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;", "theme", "setDialogAttrTheme", "onCancel", "setNegativeButton", "tag", "setTag", "(Ljava/lang/String;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;", "", WidgetNoticeDialog.ARG_CANCELABLE, "setCancelable", "(Z)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "show", "(Landroidx/fragment/app/FragmentManager;)V", "confirmText", "Ljava/lang/String;", "isCancelable", "Z", "Lkotlin/jvm/functions/Function1;", "themeId", "Ljava/lang/Integer;", "message", "Ljava/lang/CharSequence;", "Landroid/content/Context;", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "title", "cancelText", HookHelper.constructorName, "(Landroid/content/Context;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Builder {
        private final Context context;
        private boolean isCancelable;
        private String tag;
        private Integer themeId;
        private CharSequence title = "";
        private CharSequence message = "";
        private String confirmText = "";
        private String cancelText = "";
        private Function1<? super View, Unit> onConfirm = WidgetNoticeDialog$Builder$onConfirm$1.INSTANCE;
        private Function1<? super View, Unit> onCancel = WidgetNoticeDialog$Builder$onCancel$1.INSTANCE;

        public Builder(Context context) {
            m.checkNotNullParameter(context, "context");
            this.context = context;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Builder setNegativeButton$default(Builder builder, String str, Function1 function1, int i, Object obj) {
            if ((i & 2) != 0) {
                function1 = WidgetNoticeDialog$Builder$setNegativeButton$1.INSTANCE;
            }
            return builder.setNegativeButton(str, function1);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Builder setPositiveButton$default(Builder builder, String str, Function1 function1, int i, Object obj) {
            if ((i & 2) != 0) {
                function1 = WidgetNoticeDialog$Builder$setPositiveButton$1.INSTANCE;
            }
            return builder.setPositiveButton(str, function1);
        }

        public final Context getContext() {
            return this.context;
        }

        public final Builder setCancelable(boolean z2) {
            this.isCancelable = z2;
            return this;
        }

        public final Builder setDialogAttrTheme(@AttrRes int i) {
            this.themeId = Integer.valueOf(i);
            return this;
        }

        public final Builder setMessage(CharSequence charSequence) {
            m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            this.message = charSequence;
            return this;
        }

        public final Builder setNegativeButton(String str, Function1<? super View, Unit> function1) {
            m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            m.checkNotNullParameter(function1, "onCancel");
            this.cancelText = str;
            this.onCancel = function1;
            return this;
        }

        public final Builder setPositiveButton(String str, Function1<? super View, Unit> function1) {
            m.checkNotNullParameter(str, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            m.checkNotNullParameter(function1, "onConfirm");
            this.confirmText = str;
            this.onConfirm = function1;
            return this;
        }

        public final Builder setTag(String str) {
            m.checkNotNullParameter(str, "tag");
            this.tag = str;
            return this;
        }

        public final Builder setTitle(CharSequence charSequence) {
            m.checkNotNullParameter(charSequence, NotificationCompat.MessagingStyle.Message.KEY_TEXT);
            this.title = charSequence;
            return this;
        }

        public final void show(FragmentManager fragmentManager) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            Companion.show$default(WidgetNoticeDialog.Companion, fragmentManager, this.title, this.message, this.confirmText, this.cancelText, h0.mapOf(o.to(Integer.valueOf((int) R.id.OK_BUTTON), this.onConfirm), o.to(Integer.valueOf((int) R.id.CANCEL_BUTTON), this.onCancel)), null, null, null, this.themeId, null, null, 0, null, 15808, null);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Builder setNegativeButton$default(Builder builder, int i, Function1 function1, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                function1 = WidgetNoticeDialog$Builder$setNegativeButton$2.INSTANCE;
            }
            return builder.setNegativeButton(i, function1);
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Builder setPositiveButton$default(Builder builder, int i, Function1 function1, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                function1 = WidgetNoticeDialog$Builder$setPositiveButton$2.INSTANCE;
            }
            return builder.setPositiveButton(i, function1);
        }

        public final Builder setMessage(@StringRes int i) {
            CharSequence b2;
            b2 = b.b(this.context, i, new Object[0], (r4 & 4) != 0 ? b.C0034b.j : null);
            this.message = b2;
            return this;
        }

        public final Builder setTitle(@StringRes int i) {
            String string = this.context.getString(i);
            m.checkNotNullExpressionValue(string, "context.getString(stringResId)");
            this.title = string;
            return this;
        }

        public final Builder setNegativeButton(@StringRes int i, Function1<? super View, Unit> function1) {
            m.checkNotNullParameter(function1, "onCancel");
            String string = this.context.getString(i);
            m.checkNotNullExpressionValue(string, "context.getString(stringResId)");
            this.cancelText = string;
            this.onCancel = function1;
            return this;
        }

        public final Builder setPositiveButton(@StringRes int i, Function1<? super View, Unit> function1) {
            m.checkNotNullParameter(function1, "onConfirm");
            String string = this.context.getString(i);
            m.checkNotNullExpressionValue(string, "context.getString(stringResId)");
            this.confirmText = string;
            this.onConfirm = function1;
            return this;
        }
    }

    /* compiled from: WidgetNoticeDialog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0017\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b6\u00107JÉ\u0001\u0010\u001b\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0006\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00042\"\b\u0002\u0010\u000e\u001a\u001c\u0012\u0004\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u0018\u00010\t2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\n\b\u0003\u0010\u0011\u001a\u0004\u0018\u00010\n2\n\b\u0003\u0010\u0012\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00142\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00162\b\b\u0002\u0010\u0018\u001a\u00020\n2\u0010\b\u0002\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u0019H\u0007¢\u0006\u0004\b\u001b\u0010\u001cJe\u0010$\u001a\u00020\r2\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u00042\u0018\u0010\"\u001a\u0014\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\r0 2\u0010\b\u0002\u0010#\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u00192\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0014¢\u0006\u0004\b$\u0010%R\u0016\u0010&\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010(\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b(\u0010'R\u0016\u0010)\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b)\u0010'R\u0016\u0010*\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b*\u0010'R\u0016\u0010+\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b+\u0010'R\u0016\u0010,\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b,\u0010'R\u0016\u0010-\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b-\u0010'R\u0016\u0010.\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b.\u0010'R\u0016\u0010/\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b/\u0010'R\u0016\u00100\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b0\u0010'R\u0016\u00101\u001a\u00020\u00168\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b1\u0010'R\u0016\u00102\u001a\u00020\n8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b2\u00103R\u0016\u00104\u001a\u00020\n8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b4\u00103R\u0016\u00105\u001a\u00020\n8\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b5\u00103¨\u00068"}, d2 = {"Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;", "", "Landroidx/fragment/app/FragmentManager;", "fragmentManager", "", "headerText", "bodyText", "goText", "cancelText", "", "", "Lkotlin/Function1;", "Landroid/view/View;", "", "listenerMap", "Lcom/discord/stores/StoreNotices$Dialog$Type;", "type", "aboveLayoutId", "belowLayoutId", "dialogTheme", "", WidgetNoticeDialog.ARG_CANCELABLE, "", "tag", "bodyTextAlignment", "Lkotlin/Function0;", "onDismiss", "show", "(Landroidx/fragment/app/FragmentManager;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;ILkotlin/jvm/functions/Function0;)V", "Lcom/discord/app/AppActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "hintText", "Lkotlin/Function2;", "Landroid/content/Context;", "onOKClicked", "onCancelClicked", "showInputModal", "(Lcom/discord/app/AppActivity;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Ljava/lang/Boolean;)V", "ARG_ABOVE_LAYOUT_ID", "Ljava/lang/String;", "ARG_BELOW_LAYOUT_ID", "ARG_BODY_TEXT", "ARG_BODY_TEXT_ALIGNMENT", "ARG_CANCELABLE", "ARG_CANCEL_TEXT", "ARG_HEADER_TEXT", "ARG_NOTICE_TYPE", "ARG_OK_TEXT", "ARG_STACK_TRACE_CALLER_FRAME", "ARG_THEME_ID", "CANCEL_BUTTON", "I", "OK_BUTTON", "ON_SHOW", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public static /* synthetic */ void show$default(Companion companion, FragmentManager fragmentManager, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, Map map, StoreNotices.Dialog.Type type, Integer num, Integer num2, Integer num3, Boolean bool, String str, int i, Function0 function0, int i2, Object obj) {
            companion.show(fragmentManager, (i2 & 2) != 0 ? null : charSequence, charSequence2, charSequence3, (i2 & 16) != 0 ? null : charSequence4, (i2 & 32) != 0 ? null : map, (i2 & 64) != 0 ? null : type, (i2 & 128) != 0 ? null : num, (i2 & 256) != 0 ? null : num2, (i2 & 512) != 0 ? null : num3, (i2 & 1024) != 0 ? null : bool, (i2 & 2048) != 0 ? null : str, (i2 & 4096) != 0 ? 2 : i, (i2 & 8192) != 0 ? null : function0);
        }

        public final void show(FragmentManager fragmentManager, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, Map<Integer, ? extends Function1<? super View, Unit>> map, StoreNotices.Dialog.Type type, @LayoutRes Integer num, @LayoutRes Integer num2, Integer num3, Boolean bool, String str, int i, Function0<Unit> function0) {
            m.checkNotNullParameter(fragmentManager, "fragmentManager");
            m.checkNotNullParameter(charSequence2, "bodyText");
            WidgetNoticeDialog widgetNoticeDialog = new WidgetNoticeDialog();
            widgetNoticeDialog.setListenerMap(map);
            widgetNoticeDialog.setOnDismiss(function0);
            Bundle bundle = new Bundle();
            bundle.putCharSequence(WidgetNoticeDialog.ARG_HEADER_TEXT, charSequence);
            bundle.putCharSequence(WidgetNoticeDialog.ARG_BODY_TEXT, charSequence2);
            bundle.putCharSequence(WidgetNoticeDialog.ARG_OK_TEXT, charSequence3);
            bundle.putCharSequence(WidgetNoticeDialog.ARG_CANCEL_TEXT, charSequence4);
            if (type != null) {
                bundle.putString(WidgetNoticeDialog.ARG_NOTICE_TYPE, type.name());
            }
            if (num != null) {
                bundle.putInt(WidgetNoticeDialog.ARG_ABOVE_LAYOUT_ID, num.intValue());
            }
            if (num2 != null) {
                bundle.putInt(WidgetNoticeDialog.ARG_BELOW_LAYOUT_ID, num2.intValue());
            }
            if (num3 != null) {
                bundle.putInt(WidgetNoticeDialog.ARG_THEME_ID, num3.intValue());
            }
            if (bool != null) {
                bundle.putBoolean(WidgetNoticeDialog.ARG_CANCELABLE, bool.booleanValue());
            }
            bundle.putInt(WidgetNoticeDialog.ARG_BODY_TEXT_ALIGNMENT, i);
            Thread currentThread = Thread.currentThread();
            m.checkNotNullExpressionValue(currentThread, "Thread.currentThread()");
            StackTraceElement[] stackTrace = currentThread.getStackTrace();
            String stackTraceElement = stackTrace.length > 4 ? stackTrace[4].toString() : "";
            m.checkNotNullExpressionValue(stackTraceElement, "if (stacktrace.size > 4)…ace[4].toString() else \"\"");
            bundle.putString(WidgetNoticeDialog.ARG_STACK_TRACE_CALLER_FRAME, stackTraceElement);
            widgetNoticeDialog.setArguments(bundle);
            if (str == null) {
                str = WidgetNoticeDialog.class.getSimpleName();
            }
            widgetNoticeDialog.show(fragmentManager, str);
        }

        public final void showInputModal(AppActivity appActivity, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Function2<? super Context, ? super String, Unit> function2, Function0<Unit> function0, Boolean bool) {
            m.checkNotNullParameter(appActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
            m.checkNotNullParameter(charSequence, "headerText");
            m.checkNotNullParameter(charSequence2, "bodyText");
            m.checkNotNullParameter(charSequence3, "hintText");
            m.checkNotNullParameter(function2, "onOKClicked");
            HashMap hashMap = new HashMap();
            hashMap.put(Integer.valueOf((int) R.id.OK_BUTTON), new WidgetNoticeDialog$Companion$showInputModal$1(function2));
            if (function0 != null) {
                hashMap.put(Integer.valueOf((int) R.id.CANCEL_BUTTON), new WidgetNoticeDialog$Companion$showInputModal$2(function0));
            }
            hashMap.put(0, new WidgetNoticeDialog$Companion$showInputModal$3(charSequence3));
            FragmentManager supportFragmentManager = appActivity.getSupportFragmentManager();
            m.checkNotNullExpressionValue(supportFragmentManager, "activity.supportFragmentManager");
            show$default(this, supportFragmentManager, charSequence, charSequence2, appActivity.getString(R.string.confirm), appActivity.getString(R.string.cancel), hashMap, null, null, Integer.valueOf((int) R.layout.view_input_modal), null, bool, null, 0, null, 15040, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetNoticeDialog() {
        super(R.layout.widget_notice_dialog);
    }

    private final WidgetNoticeDialogBinding getBinding() {
        return (WidgetNoticeDialogBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final Integer insertLayoutInBundle(Bundle bundle, String str, int i) {
        if (bundle == null) {
            return null;
        }
        Integer valueOf = Integer.valueOf(bundle.getInt(str));
        if (!(valueOf.intValue() != 0)) {
            valueOf = null;
        }
        if (valueOf == null) {
            return null;
        }
        getBinding().f2477b.addView(getLayoutInflater().inflate(valueOf.intValue(), (ViewGroup) getBinding().f2477b, false), i);
        return valueOf;
    }

    public static /* synthetic */ Integer insertLayoutInBundle$default(WidgetNoticeDialog widgetNoticeDialog, Bundle bundle, String str, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return widgetNoticeDialog.insertLayoutInBundle(bundle, str, i);
    }

    public static final void show(FragmentManager fragmentManager, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, CharSequence charSequence4, Map<Integer, ? extends Function1<? super View, Unit>> map, StoreNotices.Dialog.Type type, @LayoutRes Integer num, @LayoutRes Integer num2, Integer num3, Boolean bool, String str, int i, Function0<Unit> function0) {
        Companion.show(fragmentManager, charSequence, charSequence2, charSequence3, charSequence4, map, type, num, num2, num3, bool, str, i, function0);
    }

    public final Map<Integer, Function1<View, Unit>> getListenerMap() {
        return this.listenerMap;
    }

    public final Function0<Unit> getOnDismiss() {
        return this.onDismiss;
    }

    public final void logOnStartError(Exception exc) {
        m.checkNotNullParameter(exc, "exception");
        Bundle arguments = getArguments();
        Serializable serializable = arguments != null ? arguments.getSerializable(ARG_STACK_TRACE_CALLER_FRAME) : null;
        AppLog appLog = AppLog.g;
        Logger.e$default(appLog, "failed to start WidgetNoticeDialog from " + serializable, exc, null, 4, null);
    }

    @Override // com.discord.app.AppDialog, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        Resources.Theme theme;
        Bundle arguments = getArguments();
        if (arguments != null) {
            TypedValue typedValue = new TypedValue();
            Context context = getContext();
            if (!(context == null || (theme = context.getTheme()) == null)) {
                theme.resolveAttribute(arguments.getInt(ARG_THEME_ID, R.attr.dialogTheme), typedValue, true);
            }
            setStyle(1, typedValue.resourceId);
        }
        return super.onCreateDialog(bundle);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        m.checkNotNullParameter(dialogInterface, "dialog");
        Function0<Unit> function0 = this.onDismiss;
        if (function0 != null) {
            function0.invoke();
        }
        super.onDismiss(dialogInterface);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(final View view) {
        Map<Integer, ? extends Function1<? super View, Unit>> map;
        Function1<? super View, Unit> function1;
        String string;
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        Bundle arguments = getArguments();
        setCancelable(arguments != null ? arguments.getBoolean(ARG_CANCELABLE, true) : true);
        Bundle arguments2 = getArguments();
        if (!(arguments2 == null || (string = arguments2.getString(ARG_NOTICE_TYPE)) == null)) {
            StoreNotices notices = StoreStream.Companion.getNotices();
            m.checkNotNullExpressionValue(string, "it");
            notices.markDialogSeen(string);
        }
        Bundle arguments3 = getArguments();
        CharSequence charSequence = null;
        CharSequence charSequence2 = arguments3 != null ? arguments3.getCharSequence(ARG_HEADER_TEXT) : null;
        TextView textView = getBinding().e;
        m.checkNotNullExpressionValue(textView, "binding.noticeHeader");
        textView.setText(charSequence2);
        LinearLayout linearLayout = getBinding().f;
        m.checkNotNullExpressionValue(linearLayout, "binding.noticeHeaderContainer");
        linearLayout.setVisibility((charSequence2 == null || t.isBlank(charSequence2)) ^ true ? 0 : 8);
        LinkifiedTextView linkifiedTextView = getBinding().c;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.noticeBodyText");
        Bundle arguments4 = getArguments();
        linkifiedTextView.setText(arguments4 != null ? arguments4.getCharSequence(ARG_BODY_TEXT) : null);
        if (isCancelable()) {
            LinkifiedTextView linkifiedTextView2 = getBinding().c;
            m.checkNotNullExpressionValue(linkifiedTextView2, "binding.noticeBodyText");
            linkifiedTextView2.setMovementMethod(new ActionLinkMovementMethod(new WidgetNoticeDialog$onViewBound$2(this)));
        }
        LinkifiedTextView linkifiedTextView3 = getBinding().c;
        m.checkNotNullExpressionValue(linkifiedTextView3, "binding.noticeBodyText");
        Bundle arguments5 = getArguments();
        linkifiedTextView3.setTextAlignment(arguments5 != null ? arguments5.getInt(ARG_BODY_TEXT_ALIGNMENT) : 2);
        MaterialButton materialButton = getBinding().g;
        m.checkNotNullExpressionValue(materialButton, "binding.noticeOk");
        Bundle arguments6 = getArguments();
        ViewExtensions.setTextAndVisibilityBy(materialButton, arguments6 != null ? arguments6.getCharSequence(ARG_OK_TEXT) : null);
        Bundle arguments7 = getArguments();
        if (arguments7 != null) {
            charSequence = arguments7.getCharSequence(ARG_CANCEL_TEXT);
        }
        MaterialButton materialButton2 = getBinding().d;
        m.checkNotNullExpressionValue(materialButton2, "binding.noticeCancel");
        ViewExtensions.setTextAndVisibilityBy(materialButton2, charSequence);
        Bundle arguments8 = getArguments();
        if (arguments8 != null) {
            insertLayoutInBundle(arguments8, ARG_ABOVE_LAYOUT_ID, 0);
        }
        Bundle arguments9 = getArguments();
        if (arguments9 != null) {
            LinearLayout linearLayout2 = getBinding().f2477b;
            m.checkNotNullExpressionValue(linearLayout2, "binding.noticeBodyContainer");
            insertLayoutInBundle(arguments9, ARG_BELOW_LAYOUT_ID, linearLayout2.getChildCount());
        }
        getBinding().g.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.notice.WidgetNoticeDialog$onViewBound$4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetNoticeDialog.this.dismiss();
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.notice.WidgetNoticeDialog$onViewBound$5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetNoticeDialog.this.dismiss();
            }
        });
        Map<Integer, ? extends Function1<? super View, Unit>> map2 = this.listenerMap;
        if (map2 != null) {
            for (final Map.Entry<Integer, ? extends Function1<? super View, Unit>> entry : map2.entrySet()) {
                View findViewById = view.findViewById(entry.getKey().intValue());
                if (findViewById != null) {
                    findViewById.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.notice.WidgetNoticeDialog$onViewBound$$inlined$forEach$lambda$1
                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view2) {
                            ((Function1) entry.getValue()).invoke(view);
                            this.dismiss();
                        }
                    });
                }
            }
        }
        if (!(getContext() == null || (map = this.listenerMap) == null || (function1 = map.get(0)) == null)) {
            function1.invoke(view);
        }
    }

    public final void setListenerMap(Map<Integer, ? extends Function1<? super View, Unit>> map) {
        this.listenerMap = map;
    }

    public final void setOnDismiss(Function0<Unit> function0) {
        this.onDismiss = function0;
    }
}
