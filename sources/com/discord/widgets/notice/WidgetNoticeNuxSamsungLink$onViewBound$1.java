package com.discord.widgets.notice;

import com.discord.models.domain.ModelOAuth2Token;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetNoticeNuxSamsungLink.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0007\u001a\u00020\u00042\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"", "Lcom/discord/models/domain/ModelOAuth2Token;", "kotlin.jvm.PlatformType", "authedApps", "", "invoke", "(Ljava/util/List;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetNoticeNuxSamsungLink$onViewBound$1 extends o implements Function1<List<? extends ModelOAuth2Token>, Unit> {
    public final /* synthetic */ WidgetNoticeNuxSamsungLink this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetNoticeNuxSamsungLink$onViewBound$1(WidgetNoticeNuxSamsungLink widgetNoticeNuxSamsungLink) {
        super(1);
        this.this$0 = widgetNoticeNuxSamsungLink;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends ModelOAuth2Token> list) {
        invoke2((List<ModelOAuth2Token>) list);
        return Unit.a;
    }

    /* JADX WARN: Removed duplicated region for block: B:25:0x0051 A[EDGE_INSN: B:25:0x0051->B:22:0x0051 ?: BREAK  , SYNTHETIC] */
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void invoke2(java.util.List<com.discord.models.domain.ModelOAuth2Token> r6) {
        /*
            r5 = this;
            java.lang.String r0 = "authedApps"
            d0.z.d.m.checkNotNullExpressionValue(r6, r0)
            boolean r0 = r6 instanceof java.util.Collection
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L13
            boolean r0 = r6.isEmpty()
            if (r0 == 0) goto L13
        L11:
            r1 = 0
            goto L51
        L13:
            java.util.Iterator r6 = r6.iterator()
        L17:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L11
            java.lang.Object r0 = r6.next()
            com.discord.models.domain.ModelOAuth2Token r0 = (com.discord.models.domain.ModelOAuth2Token) r0
            com.discord.api.application.Application r0 = r0.getApplication()
            long r3 = r0.g()
            java.lang.String r0 = java.lang.String.valueOf(r3)
            java.lang.String r3 = "591317049637339146"
            boolean r0 = d0.z.d.m.areEqual(r0, r3)
            if (r0 == 0) goto L4e
            int r0 = android.os.Build.VERSION.SDK_INT
            r3 = 28
            if (r0 < r3) goto L49
            java.lang.String r0 = android.os.Build.MANUFACTURER
            java.lang.String r3 = "samsung"
            boolean r0 = d0.g0.t.equals(r0, r3, r1)
            if (r0 == 0) goto L49
            r0 = 1
            goto L4a
        L49:
            r0 = 0
        L4a:
            if (r0 == 0) goto L4e
            r0 = 1
            goto L4f
        L4e:
            r0 = 0
        L4f:
            if (r0 == 0) goto L17
        L51:
            if (r1 == 0) goto L58
            com.discord.widgets.notice.WidgetNoticeNuxSamsungLink r6 = r5.this$0
            r6.dismiss()
        L58:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.notice.WidgetNoticeNuxSamsungLink$onViewBound$1.invoke2(java.util.List):void");
    }
}
