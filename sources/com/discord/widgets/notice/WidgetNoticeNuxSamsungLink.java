package com.discord.widgets.notice;

import andhook.lib.HookHelper;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.app.AppDialog;
import com.discord.databinding.WidgetNoticeNuxSamsungLinkBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetNoticeNuxSamsungLink.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u0007¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u001d\u0010\f\u001a\u00020\u00078B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/notice/WidgetNoticeNuxSamsungLink;", "Lcom/discord/app/AppDialog;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/databinding/WidgetNoticeNuxSamsungLinkBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetNoticeNuxSamsungLinkBinding;", "binding", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetNoticeNuxSamsungLink extends AppDialog {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetNoticeNuxSamsungLink.class, "binding", "getBinding()Lcom/discord/databinding/WidgetNoticeNuxSamsungLinkBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String NOTICE_NAME = "NUX/SamsungLink";
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetNoticeNuxSamsungLink$binding$2.INSTANCE, null, 2, null);

    /* compiled from: WidgetNoticeNuxSamsungLink.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u001d\u0010\t\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u000b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\f\u0010\r¨\u0006\u000f"}, d2 = {"Lcom/discord/widgets/notice/WidgetNoticeNuxSamsungLink$Companion;", "", "", "internalEnqueue", "()V", "Landroid/content/Context;", "context", "Lcom/discord/utilities/time/Clock;", "clock", "enqueue", "(Landroid/content/Context;Lcom/discord/utilities/time/Clock;)V", "", "NOTICE_NAME", "Ljava/lang/String;", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void internalEnqueue() {
            StoreStream.Companion.getNotices().requestToShow(new StoreNotices.Notice(WidgetNoticeNuxSamsungLink.NOTICE_NAME, null, 0L, 0, true, null, 0L, false, RecyclerView.FOREVER_NS, WidgetNoticeNuxSamsungLink$Companion$internalEnqueue$notice$1.INSTANCE, 230, null));
        }

        /* JADX WARN: Code restructure failed: missing block: B:11:0x002e, code lost:
            if (r11.getPackageManager().getPackageInfo("com.samsung.android.game.gametools", 0) != null) goto L12;
         */
        /* JADX WARN: Code restructure failed: missing block: B:12:0x0030, code lost:
            r11 = true;
         */
        /* JADX WARN: Code restructure failed: missing block: B:15:0x003e, code lost:
            if (r11.getPackageManager().getPackageInfo("com.samsung.android.game.gamehome", 0) == null) goto L13;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final void enqueue(android.content.Context r11, com.discord.utilities.time.Clock r12) {
            /*
                r10 = this;
                java.lang.String r0 = "context"
                d0.z.d.m.checkNotNullParameter(r11, r0)
                java.lang.String r1 = "clock"
                d0.z.d.m.checkNotNullParameter(r12, r1)
                int r1 = android.os.Build.VERSION.SDK_INT
                r2 = 28
                r3 = 1
                r4 = 0
                if (r1 < r2) goto L1e
                java.lang.String r1 = android.os.Build.MANUFACTURER
                java.lang.String r2 = "samsung"
                boolean r1 = d0.g0.t.equals(r1, r2, r3)
                if (r1 == 0) goto L1e
                r1 = 1
                goto L1f
            L1e:
                r1 = 0
            L1f:
                if (r1 == 0) goto L81
                d0.z.d.m.checkNotNullParameter(r11, r0)
                android.content.pm.PackageManager r0 = r11.getPackageManager()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L34
                java.lang.String r1 = "com.samsung.android.game.gametools"
                android.content.pm.PackageInfo r11 = r0.getPackageInfo(r1, r4)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L34
                if (r11 == 0) goto L32
            L30:
                r11 = 1
                goto L41
            L32:
                r11 = 0
                goto L41
            L34:
                android.content.pm.PackageManager r11 = r11.getPackageManager()     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L32
                java.lang.String r0 = "com.samsung.android.game.gamehome"
                android.content.pm.PackageInfo r11 = r11.getPackageInfo(r0, r4)     // Catch: android.content.pm.PackageManager.NameNotFoundException -> L32
                if (r11 == 0) goto L32
                goto L30
            L41:
                if (r11 != 0) goto L44
                goto L81
            L44:
                com.discord.stores.StoreStream$Companion r11 = com.discord.stores.StoreStream.Companion
                com.discord.stores.StoreAuthentication r0 = r11.getAuthentication()
                rx.Observable r0 = r0.observeIsAuthed$app_productionGoogleRelease()
                com.discord.stores.StoreUser r11 = r11.getUsers()
                r1 = 0
                rx.Observable r11 = com.discord.stores.StoreUser.observeMe$default(r11, r4, r3, r1)
                com.discord.widgets.notice.WidgetNoticeNuxSamsungLink$Companion$enqueue$1 r1 = com.discord.widgets.notice.WidgetNoticeNuxSamsungLink$Companion$enqueue$1.INSTANCE
                rx.Observable r2 = rx.Observable.j(r0, r11, r1)
                java.lang.String r11 = "Observable\n          .co…hed to meUser\n          }"
                d0.z.d.m.checkNotNullExpressionValue(r2, r11)
                r3 = 0
                r5 = 0
                r6 = 3
                r7 = 0
                rx.Observable r11 = com.discord.utilities.rx.ObservableExtensionsKt.takeSingleUntilTimeout$default(r2, r3, r5, r6, r7)
                rx.Observable r0 = com.discord.utilities.rx.ObservableExtensionsKt.computationLatest(r11)
                java.lang.Class<com.discord.widgets.notice.WidgetNoticeNuxSamsungLink> r1 = com.discord.widgets.notice.WidgetNoticeNuxSamsungLink.class
                r2 = 0
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                com.discord.widgets.notice.WidgetNoticeNuxSamsungLink$Companion$enqueue$2 r7 = new com.discord.widgets.notice.WidgetNoticeNuxSamsungLink$Companion$enqueue$2
                r7.<init>(r12)
                r8 = 62
                r9 = 0
                com.discord.utilities.rx.ObservableExtensionsKt.appSubscribe$default(r0, r1, r2, r3, r4, r5, r6, r7, r8, r9)
            L81:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.notice.WidgetNoticeNuxSamsungLink.Companion.enqueue(android.content.Context, com.discord.utilities.time.Clock):void");
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetNoticeNuxSamsungLink() {
        super(R.layout.widget_notice_nux_samsung_link);
    }

    private final WidgetNoticeNuxSamsungLinkBinding getBinding() {
        return (WidgetNoticeNuxSamsungLinkBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppDialog
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        StoreStream.Companion.getNotices().markDialogSeen(NOTICE_NAME);
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.takeSingleUntilTimeout$default(ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.restSubscribeOn$default(RestAPI.Companion.getApi().getOAuthTokens(), false, 1, null)), 0L, false, 1, null), this, null, 2, null), WidgetNoticeNuxSamsungLink.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetNoticeNuxSamsungLink$onViewBound$1(this));
        getBinding().c.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.notice.WidgetNoticeNuxSamsungLink$onViewBound$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetNoticeNuxSamsungLink widgetNoticeNuxSamsungLink = WidgetNoticeNuxSamsungLink.this;
                Uri oauth2Authorize = IntentUtils.RouteBuilders.Uris.INSTANCE.getOauth2Authorize();
                m.checkNotNullParameter(oauth2Authorize, "oAuthUri");
                widgetNoticeNuxSamsungLink.startActivity(new Intent("android.intent.action.VIEW", oauth2Authorize.buildUpon().appendQueryParameter("client_id", "591317049637339146").appendQueryParameter("prompt", "consent").appendQueryParameter("response_type", ModelAuditLogEntry.CHANGE_KEY_CODE).appendQueryParameter("scope", "identify activities.read activities.write").build()));
                WidgetNoticeNuxSamsungLink.this.dismiss();
            }
        });
        getBinding().f2479b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.notice.WidgetNoticeNuxSamsungLink$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetNoticeNuxSamsungLink.this.dismiss();
            }
        });
    }
}
