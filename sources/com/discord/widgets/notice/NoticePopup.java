package com.discord.widgets.notice;

import andhook.lib.HookHelper;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import androidx.core.view.ViewCompat;
import b.p.a.b;
import b.p.a.g;
import b.p.a.i;
import com.discord.api.sticker.Sticker;
import com.discord.app.AppComponent;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.widgets.home.WidgetHome;
import d0.e0.c;
import d0.z.d.a0;
import d0.z.d.m;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: NoticePopup.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b,\u0010-J)\u0010\b\u001a\u0004\u0018\u00010\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0019\u0010\u000b\u001a\u00020\u00052\b\u0010\n\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010JÝ\u0001\u0010(\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\r2\b\u0010\u0013\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00122\b\u0010\u0015\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00162\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00162\u0010\b\u0002\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001a2\n\b\u0002\u0010\u001d\u001a\u0004\u0018\u00010\r2\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u00162\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\u00022\u0016\b\u0002\u0010#\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\"0!0\u001a2\u0014\b\u0002\u0010&\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020\u00050$2\u0012\u0010'\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020\u00050$¢\u0006\u0004\b(\u0010)R\u0016\u0010*\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b*\u0010+¨\u0006."}, d2 = {"Lcom/discord/widgets/notice/NoticePopup;", "", "", "autoDismissPeriodSecs", "Lkotlin/Function0;", "", "onEnd", "Landroid/animation/ValueAnimator;", "getAutoDismissAnimator", "(Ljava/lang/Integer;Lkotlin/jvm/functions/Function0;)Landroid/animation/ValueAnimator;", "autoDismissAnimator", "cancelCountdown", "(Landroid/animation/ValueAnimator;)V", "", ModelAuditLogEntry.CHANGE_KEY_NAME, "dismiss", "(Ljava/lang/String;)V", "noticeName", "", "noticeTitle", "noticeSubtitle", "noticeBody", "Landroid/graphics/drawable/Drawable;", "noticeBodyBackgroundDrawable", "noticeBodyImageUrl", "noticeBodyImageDrawable", "", "Lcom/discord/api/sticker/Sticker;", "noticeStickers", "noticeIconUrl", "noticeIconResId", "noticeIconTopRight", "noticeAutoDismissPeriodSecs", "Ld0/e0/c;", "Lcom/discord/app/AppComponent;", "validScreens", "Lkotlin/Function1;", "Landroid/view/View;", "onClickTopRightIcon", "onClick", "enqueue", "(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Landroid/graphics/drawable/Drawable;Ljava/lang/Integer;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "DEFAULT_AUTO_DISMISS_PERIOD_SECONDS", "I", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NoticePopup {
    private static final int DEFAULT_AUTO_DISMISS_PERIOD_SECONDS = 5;
    public static final NoticePopup INSTANCE = new NoticePopup();

    private NoticePopup() {
    }

    public final void cancelCountdown(ValueAnimator valueAnimator) {
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        if (valueAnimator != null) {
            valueAnimator.end();
        }
    }

    public final void dismiss(String str) {
        ViewGroup viewGroup;
        b bVar;
        WeakReference<ViewGroup> weakReference = i.a;
        if (weakReference != null && (viewGroup = weakReference.get()) != null) {
            m.checkNotNullExpressionValue(viewGroup, "it");
            int childCount = viewGroup.getChildCount();
            if (childCount >= 0) {
                int i = 0;
                while (true) {
                    if (viewGroup.getChildAt(i) instanceof b) {
                        View childAt = viewGroup.getChildAt(i);
                        Objects.requireNonNull(childAt, "null cannot be cast to non-null type com.tapadoo.alerter.Alert");
                        bVar = (b) childAt;
                    } else {
                        bVar = null;
                    }
                    if (!(bVar == null || bVar.getWindowToken() == null)) {
                        ViewCompat.animate(bVar).alpha(0.0f).withEndAction(new g(bVar));
                    }
                    if (i == childCount) {
                        break;
                    }
                    i++;
                }
            }
        }
        StoreNotices.markSeen$default(StoreStream.Companion.getNotices(), str, 0L, 2, null);
    }

    public static /* synthetic */ void enqueue$default(NoticePopup noticePopup, String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Drawable drawable, String str2, Drawable drawable2, List list, String str3, Integer num, Drawable drawable3, Integer num2, List list2, Function1 function1, Function1 function12, int i, Object obj) {
        noticePopup.enqueue(str, charSequence, (i & 4) != 0 ? null : charSequence2, charSequence3, (i & 16) != 0 ? null : drawable, (i & 32) != 0 ? null : str2, (i & 64) != 0 ? null : drawable2, (i & 128) != 0 ? null : list, (i & 256) != 0 ? null : str3, (i & 512) != 0 ? null : num, (i & 1024) != 0 ? null : drawable3, (i & 2048) != 0 ? 5 : num2, (i & 4096) != 0 ? d0.t.m.listOf(a0.getOrCreateKotlinClass(WidgetHome.class)) : list2, (i & 8192) != 0 ? NoticePopup$enqueue$1.INSTANCE : function1, function12);
    }

    public final ValueAnimator getAutoDismissAnimator(Integer num, final Function0<Unit> function0) {
        if (num == null) {
            return null;
        }
        ValueAnimator ofInt = ObjectAnimator.ofInt(0, 1);
        Animator.AnimatorListener noticePopup$getAutoDismissAnimator$animatorListener$1 = new Animator.AnimatorListener() { // from class: com.discord.widgets.notice.NoticePopup$getAutoDismissAnimator$animatorListener$1
            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                if (animator != null) {
                    animator.removeListener(this);
                }
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                Function0.this.invoke();
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
            }
        };
        m.checkNotNullExpressionValue(ofInt, "animator");
        ofInt.setInterpolator(new LinearInterpolator());
        ofInt.setDuration(num.intValue() * 1000);
        ofInt.addListener(noticePopup$getAutoDismissAnimator$animatorListener$1);
        ofInt.start();
        return ofInt;
    }

    public final void enqueue(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Drawable drawable, String str2, Drawable drawable2, List<Sticker> list, String str3, Integer num, Drawable drawable3, Integer num2, List<? extends c<? extends AppComponent>> list2, Function1<? super View, Unit> function1, Function1<? super View, Unit> function12) {
        m.checkNotNullParameter(str, "noticeName");
        m.checkNotNullParameter(list2, "validScreens");
        m.checkNotNullParameter(function1, "onClickTopRightIcon");
        m.checkNotNullParameter(function12, "onClick");
        StoreStream.Companion.getNotices().requestToShow(new StoreNotices.Notice(str, null, 0L, 1, false, list2, 1000L, false, 0L, new NoticePopup$enqueue$notice$1(function12, str, num2, str3, num, str2, drawable2, drawable, charSequence, charSequence2, charSequence3, drawable3, list, function1), 150, null));
    }
}
