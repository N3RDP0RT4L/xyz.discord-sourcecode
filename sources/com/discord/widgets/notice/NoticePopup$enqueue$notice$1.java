package com.discord.widgets.notice;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentActivity;
import b.p.a.a;
import b.p.a.b;
import b.p.a.g;
import b.p.a.h;
import b.p.a.i;
import b.p.a.j;
import b.p.a.l;
import com.discord.api.sticker.Sticker;
import com.discord.databinding.WidgetNoticePopupBinding;
import com.discord.stores.StoreNotices;
import com.discord.stores.StoreStream;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.images.MGImages;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.utilities.view.text.SimpleDraweeSpanTextView;
import com.discord.views.sticker.StickerView;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import d0.g0.t;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: NoticePopup.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroidx/fragment/app/FragmentActivity;", ActivityChooserModel.ATTRIBUTE_ACTIVITY, "", "invoke", "(Landroidx/fragment/app/FragmentActivity;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NoticePopup$enqueue$notice$1 extends o implements Function1<FragmentActivity, Boolean> {
    public final /* synthetic */ Integer $noticeAutoDismissPeriodSecs;
    public final /* synthetic */ CharSequence $noticeBody;
    public final /* synthetic */ Drawable $noticeBodyBackgroundDrawable;
    public final /* synthetic */ Drawable $noticeBodyImageDrawable;
    public final /* synthetic */ String $noticeBodyImageUrl;
    public final /* synthetic */ Integer $noticeIconResId;
    public final /* synthetic */ Drawable $noticeIconTopRight;
    public final /* synthetic */ String $noticeIconUrl;
    public final /* synthetic */ String $noticeName;
    public final /* synthetic */ List $noticeStickers;
    public final /* synthetic */ CharSequence $noticeSubtitle;
    public final /* synthetic */ CharSequence $noticeTitle;
    public final /* synthetic */ Function1 $onClick;
    public final /* synthetic */ Function1 $onClickTopRightIcon;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NoticePopup$enqueue$notice$1(Function1 function1, String str, Integer num, String str2, Integer num2, String str3, Drawable drawable, Drawable drawable2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Drawable drawable3, List list, Function1 function12) {
        super(1);
        this.$onClick = function1;
        this.$noticeName = str;
        this.$noticeAutoDismissPeriodSecs = num;
        this.$noticeIconUrl = str2;
        this.$noticeIconResId = num2;
        this.$noticeBodyImageUrl = str3;
        this.$noticeBodyImageDrawable = drawable;
        this.$noticeBodyBackgroundDrawable = drawable2;
        this.$noticeTitle = charSequence;
        this.$noticeSubtitle = charSequence2;
        this.$noticeBody = charSequence3;
        this.$noticeIconTopRight = drawable3;
        this.$noticeStickers = list;
        this.$onClickTopRightIcon = function12;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(final FragmentActivity fragmentActivity) {
        b bVar;
        View view;
        ViewGroup viewGroup;
        final ValueAnimator autoDismissAnimator;
        Sticker sticker;
        Window window;
        int childCount;
        b bVar2;
        Window window2;
        m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        m.checkNotNullParameter(fragmentActivity, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        i iVar = new i(null);
        View decorView = (fragmentActivity == null || (window2 = fragmentActivity.getWindow()) == null) ? null : window2.getDecorView();
        if (!(decorView instanceof ViewGroup)) {
            decorView = null;
        }
        ViewGroup viewGroup2 = (ViewGroup) decorView;
        int i = 0;
        if (viewGroup2 != null && (childCount = viewGroup2.getChildCount()) >= 0) {
            int i2 = 0;
            while (true) {
                if (viewGroup2.getChildAt(i2) instanceof b) {
                    View childAt = viewGroup2.getChildAt(i2);
                    Objects.requireNonNull(childAt, "null cannot be cast to non-null type com.tapadoo.alerter.Alert");
                    bVar2 = (b) childAt;
                } else {
                    bVar2 = null;
                }
                if (!(bVar2 == null || bVar2.getWindowToken() == null)) {
                    ViewCompat.animate(bVar2).alpha(0.0f).withEndAction(new g(bVar2));
                }
                if (i2 == childCount) {
                    break;
                }
                i2++;
            }
        }
        if (fragmentActivity == null || (window = fragmentActivity.getWindow()) == null) {
            bVar = null;
        } else {
            View decorView2 = window.getDecorView();
            Objects.requireNonNull(decorView2, "null cannot be cast to non-null type android.view.ViewGroup");
            i.a = new WeakReference<>((ViewGroup) decorView2);
            View decorView3 = window.getDecorView();
            m.checkNotNullExpressionValue(decorView3, "it.decorView");
            Context context = decorView3.getContext();
            m.checkNotNullExpressionValue(context, "it.decorView.context");
            bVar = new b(context, R.layout.widget_notice_popup, null, 0, 12);
        }
        iVar.f1969b = bVar;
        int themedColor = ColorCompat.getThemedColor(fragmentActivity, (int) R.attr.colorBackgroundTertiary);
        b bVar3 = iVar.f1969b;
        if (bVar3 != null) {
            bVar3.setAlertBackgroundColor(themedColor);
        }
        b bVar4 = iVar.f1969b;
        if (bVar4 != null) {
            LinearLayout linearLayout = (LinearLayout) bVar4.c(com.tapadoo.alerter.R.d.llAlertBackground);
            m.checkNotNullExpressionValue(linearLayout, "it");
            linearLayout.setOnTouchListener(new l(linearLayout, new a(bVar4)));
        }
        b bVar5 = iVar.f1969b;
        if (bVar5 != null) {
            bVar5.setVibrationEnabled(false);
        }
        b bVar6 = iVar.f1969b;
        if (bVar6 != null) {
            bVar6.setEnableInfiniteDuration(true);
        }
        b bVar7 = iVar.f1969b;
        if (bVar7 != null) {
            Animation loadAnimation = AnimationUtils.loadAnimation(bVar7.getContext(), R.anim.anim_slide_in_down);
            m.checkNotNullExpressionValue(loadAnimation, "AnimationUtils.loadAnima…lert?.context, animation)");
            bVar7.setEnterAnimation$alerter_release(loadAnimation);
        }
        View.OnClickListener onClickListener = new View.OnClickListener() { // from class: com.discord.widgets.notice.NoticePopup$enqueue$notice$1.1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                Function1 function1 = NoticePopup$enqueue$notice$1.this.$onClick;
                m.checkNotNullExpressionValue(view2, "view");
                function1.invoke(view2);
                NoticePopup.INSTANCE.dismiss(NoticePopup$enqueue$notice$1.this.$noticeName);
            }
        };
        m.checkNotNullParameter(onClickListener, "onClickListener");
        b bVar8 = iVar.f1969b;
        if (bVar8 != null) {
            bVar8.setOnClickListener(onClickListener);
        }
        j jVar = new j() { // from class: com.discord.widgets.notice.NoticePopup$enqueue$notice$1.2
            @Override // b.p.a.j
            public final void onHide() {
                StoreNotices.markSeen$default(StoreStream.Companion.getNotices(), NoticePopup$enqueue$notice$1.this.$noticeName, 0L, 2, null);
            }
        };
        m.checkNotNullParameter(jVar, "listener");
        b bVar9 = iVar.f1969b;
        if (bVar9 != null) {
            bVar9.setOnHideListener$alerter_release(jVar);
        }
        b bVar10 = iVar.f1969b;
        final View layoutContainer = bVar10 != null ? bVar10.getLayoutContainer() : null;
        if (layoutContainer != null) {
            autoDismissAnimator = NoticePopup.INSTANCE.getAutoDismissAnimator(this.$noticeAutoDismissPeriodSecs, new NoticePopup$enqueue$notice$1$$special$$inlined$also$lambda$1(this, fragmentActivity));
            layoutContainer.setOnTouchListener(new View.OnTouchListener() { // from class: com.discord.widgets.notice.NoticePopup$enqueue$notice$1$3$1
                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view2, MotionEvent motionEvent) {
                    m.checkNotNullExpressionValue(motionEvent, "event");
                    if (motionEvent.getAction() != 0) {
                        return false;
                    }
                    NoticePopup.INSTANCE.cancelCountdown(autoDismissAnimator);
                    return false;
                }
            });
            int i3 = R.id.popup_attachment;
            SimpleDraweeView simpleDraweeView = (SimpleDraweeView) layoutContainer.findViewById(R.id.popup_attachment);
            if (simpleDraweeView != null) {
                i3 = R.id.popup_body;
                SimpleDraweeSpanTextView simpleDraweeSpanTextView = (SimpleDraweeSpanTextView) layoutContainer.findViewById(R.id.popup_body);
                if (simpleDraweeSpanTextView != null) {
                    i3 = R.id.popup_close_btn;
                    ImageView imageView = (ImageView) layoutContainer.findViewById(R.id.popup_close_btn);
                    if (imageView != null) {
                        i3 = R.id.popup_icon;
                        SimpleDraweeView simpleDraweeView2 = (SimpleDraweeView) layoutContainer.findViewById(R.id.popup_icon);
                        if (simpleDraweeView2 != null) {
                            i3 = R.id.popup_sticker;
                            StickerView stickerView = (StickerView) layoutContainer.findViewById(R.id.popup_sticker);
                            if (stickerView != null) {
                                i3 = R.id.popup_subtitle;
                                TextView textView = (TextView) layoutContainer.findViewById(R.id.popup_subtitle);
                                if (textView != null) {
                                    i3 = R.id.popup_title;
                                    TextView textView2 = (TextView) layoutContainer.findViewById(R.id.popup_title);
                                    if (textView2 != null) {
                                        i3 = R.id.popup_title_wrap;
                                        RelativeLayout relativeLayout = (RelativeLayout) layoutContainer.findViewById(R.id.popup_title_wrap);
                                        if (relativeLayout != null) {
                                            final WidgetNoticePopupBinding widgetNoticePopupBinding = new WidgetNoticePopupBinding((RelativeLayout) layoutContainer, simpleDraweeView, simpleDraweeSpanTextView, imageView, simpleDraweeView2, stickerView, textView, textView2, relativeLayout);
                                            m.checkNotNullExpressionValue(widgetNoticePopupBinding, "WidgetNoticePopupBinding.bind(view)");
                                            if (this.$noticeIconUrl != null) {
                                                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.popupIcon");
                                                MGImages.setImage$default(simpleDraweeView2, this.$noticeIconUrl, 0, 0, false, null, null, 124, null);
                                            } else if (this.$noticeIconResId != null) {
                                                MGImages mGImages = MGImages.INSTANCE;
                                                m.checkNotNullExpressionValue(simpleDraweeView2, "binding.popupIcon");
                                                MGImages.setImage$default(mGImages, simpleDraweeView2, this.$noticeIconResId.intValue(), (MGImages.ChangeDetector) null, 4, (Object) null);
                                            }
                                            m.checkNotNullExpressionValue(simpleDraweeView, "binding.popupAttachment");
                                            simpleDraweeView.setVisibility(this.$noticeBodyImageUrl != null || this.$noticeBodyImageDrawable != null ? 0 : 8);
                                            m.checkNotNullExpressionValue(simpleDraweeView, "binding.popupAttachment");
                                            simpleDraweeView.setBackground(this.$noticeBodyBackgroundDrawable);
                                            if (this.$noticeBodyImageUrl != null) {
                                                m.checkNotNullExpressionValue(simpleDraweeView, "binding.popupAttachment");
                                                MGImages.setImage$default(simpleDraweeView, this.$noticeBodyImageUrl, 0, 0, false, null, null, 124, null);
                                            } else if (this.$noticeBodyImageDrawable != null) {
                                                MGImages mGImages2 = MGImages.INSTANCE;
                                                m.checkNotNullExpressionValue(simpleDraweeView, "binding.popupAttachment");
                                                MGImages.setImage$default(mGImages2, simpleDraweeView, this.$noticeBodyImageDrawable, (MGImages.ChangeDetector) null, 4, (Object) null);
                                            }
                                            m.checkNotNullExpressionValue(textView2, "binding.popupTitle");
                                            textView2.setText(this.$noticeTitle);
                                            m.checkNotNullExpressionValue(textView, "binding.popupSubtitle");
                                            ViewExtensions.setTextAndVisibilityBy(textView, this.$noticeSubtitle);
                                            CharSequence charSequence = this.$noticeBody;
                                            if (charSequence instanceof DraweeSpanStringBuilder) {
                                                simpleDraweeSpanTextView.setDraweeSpanStringBuilder((DraweeSpanStringBuilder) charSequence);
                                            } else {
                                                m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.popupBody");
                                                simpleDraweeSpanTextView.setText(this.$noticeBody);
                                            }
                                            m.checkNotNullExpressionValue(simpleDraweeSpanTextView, "binding.popupBody");
                                            CharSequence charSequence2 = this.$noticeBody;
                                            simpleDraweeSpanTextView.setVisibility(charSequence2 != null && (t.isBlank(charSequence2) ^ true) ? 0 : 8);
                                            simpleDraweeSpanTextView.post(new Runnable() { // from class: com.discord.widgets.notice.NoticePopup$enqueue$notice$1$3$2
                                                @Override // java.lang.Runnable
                                                public final void run() {
                                                    SimpleDraweeSpanTextView simpleDraweeSpanTextView2 = WidgetNoticePopupBinding.this.c;
                                                    m.checkNotNullExpressionValue(simpleDraweeSpanTextView2, "binding.popupBody");
                                                    Layout layout = simpleDraweeSpanTextView2.getLayout();
                                                    int lineCount = layout != null ? layout.getLineCount() : 0;
                                                    SimpleDraweeSpanTextView simpleDraweeSpanTextView3 = WidgetNoticePopupBinding.this.c;
                                                    m.checkNotNullExpressionValue(simpleDraweeSpanTextView3, "binding.popupBody");
                                                    if (lineCount > simpleDraweeSpanTextView3.getMaxLines()) {
                                                        SimpleDraweeSpanTextView simpleDraweeSpanTextView4 = WidgetNoticePopupBinding.this.c;
                                                        m.checkNotNullExpressionValue(simpleDraweeSpanTextView4, "binding.popupBody");
                                                        Layout layout2 = simpleDraweeSpanTextView4.getLayout();
                                                        SimpleDraweeSpanTextView simpleDraweeSpanTextView5 = WidgetNoticePopupBinding.this.c;
                                                        m.checkNotNullExpressionValue(simpleDraweeSpanTextView5, "binding.popupBody");
                                                        int lineEnd = layout2.getLineEnd(simpleDraweeSpanTextView5.getMaxLines() - 1);
                                                        SimpleDraweeSpanTextView simpleDraweeSpanTextView6 = WidgetNoticePopupBinding.this.c;
                                                        m.checkNotNullExpressionValue(simpleDraweeSpanTextView6, "binding.popupBody");
                                                        CharSequence subSequence = simpleDraweeSpanTextView6.getText().subSequence(0, lineEnd - 1);
                                                        SimpleDraweeSpanTextView simpleDraweeSpanTextView7 = WidgetNoticePopupBinding.this.c;
                                                        m.checkNotNullExpressionValue(simpleDraweeSpanTextView7, "binding.popupBody");
                                                        simpleDraweeSpanTextView7.setText(new SpannableStringBuilder(subSequence).append((CharSequence) "…"));
                                                    }
                                                }
                                            });
                                            Drawable drawable = this.$noticeIconTopRight;
                                            if (drawable == null) {
                                                drawable = ContextCompat.getDrawable(fragmentActivity, DrawableCompat.getThemedDrawableRes$default(fragmentActivity, (int) R.attr.ic_settings, 0, 2, (Object) null));
                                            }
                                            m.checkNotNullExpressionValue(stickerView, "binding.popupSticker");
                                            List list = this.$noticeStickers;
                                            if (!(list != null && !list.isEmpty())) {
                                                i = 8;
                                            }
                                            stickerView.setVisibility(i);
                                            List list2 = this.$noticeStickers;
                                            if (list2 == null || (sticker = (Sticker) u.firstOrNull((List<? extends Object>) list2)) == null) {
                                                view = null;
                                            } else {
                                                view = null;
                                                StickerView.e(stickerView, sticker, null, 2);
                                            }
                                            imageView.setImageDrawable(drawable);
                                            imageView.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.notice.NoticePopup$enqueue$notice$1$$special$$inlined$also$lambda$2
                                                @Override // android.view.View.OnClickListener
                                                public final void onClick(View view2) {
                                                    NoticePopup.INSTANCE.dismiss(this.$noticeName);
                                                    this.$onClickTopRightIcon.invoke(layoutContainer);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            throw new NullPointerException("Missing required view with ID: ".concat(layoutContainer.getResources().getResourceName(i3)));
        }
        view = null;
        WeakReference<ViewGroup> weakReference = i.a;
        if (!(weakReference == null || (viewGroup = weakReference.get()) == null)) {
            new Handler(Looper.getMainLooper()).post(new h(viewGroup, iVar));
        }
        b bVar11 = iVar.f1969b;
        View findViewById = bVar11 != null ? bVar11.findViewById(R.id.llAlertBackground) : view;
        if (findViewById != null) {
            ViewCompat.setElevation(findViewById, fragmentActivity.getResources().getDimension(R.dimen.app_elevation));
        }
        return true;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(FragmentActivity fragmentActivity) {
        return Boolean.valueOf(invoke2(fragmentActivity));
    }
}
