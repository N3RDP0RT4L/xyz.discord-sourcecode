package com.discord.widgets.notice;

import android.content.Context;
import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.models.user.User;
import com.discord.widgets.notice.NoticePopupChannel;
import d0.z.d.k;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.functions.Function9;
/* compiled from: NoticePopupChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0019\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0016\u0010\f\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0004\u0012\u00020\u000b0\b2\u000e\u0010\u000e\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\r2\u0006\u0010\u0010\u001a\u00020\u000f2\u0016\u0010\u0012\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\r\u0012\u0004\u0012\u00020\u00110\b2\u0016\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\tj\u0002`\u0013\u0012\u0004\u0012\u00020\u00140\b¢\u0006\u0004\b\u0017\u0010\u0018"}, d2 = {"Landroid/content/Context;", "p1", "Lcom/discord/models/message/Message;", "p2", "Lcom/discord/models/guild/Guild;", "p3", "Lcom/discord/api/channel/Channel;", "p4", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "p5", "Lcom/discord/primitives/ChannelId;", "p6", "Lcom/discord/models/user/User;", "p7", "", "p8", "Lcom/discord/primitives/GuildId;", "Lcom/discord/api/role/GuildRole;", "p9", "Lcom/discord/widgets/notice/NoticePopupChannel$Model;", "invoke", "(Landroid/content/Context;Lcom/discord/models/message/Message;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/user/User;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class NoticePopupChannel$enqueue$1 extends k implements Function9<Context, Message, Guild, Channel, Map<Long, ? extends GuildMember>, Long, User, Map<Long, ? extends String>, Map<Long, ? extends GuildRole>, NoticePopupChannel.Model> {
    public NoticePopupChannel$enqueue$1(NoticePopupChannel noticePopupChannel) {
        super(9, noticePopupChannel, NoticePopupChannel.class, "createModel", "createModel(Landroid/content/Context;Lcom/discord/models/message/Message;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/user/User;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;", 0);
    }

    @Override // kotlin.jvm.functions.Function9
    public /* bridge */ /* synthetic */ NoticePopupChannel.Model invoke(Context context, Message message, Guild guild, Channel channel, Map<Long, ? extends GuildMember> map, Long l, User user, Map<Long, ? extends String> map2, Map<Long, ? extends GuildRole> map3) {
        return invoke2(context, message, guild, channel, (Map<Long, GuildMember>) map, l, user, (Map<Long, String>) map2, (Map<Long, GuildRole>) map3);
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final NoticePopupChannel.Model invoke2(Context context, Message message, Guild guild, Channel channel, Map<Long, GuildMember> map, Long l, User user, Map<Long, String> map2, Map<Long, GuildRole> map3) {
        NoticePopupChannel.Model createModel;
        m.checkNotNullParameter(context, "p1");
        m.checkNotNullParameter(message, "p2");
        m.checkNotNullParameter(map, "p5");
        m.checkNotNullParameter(user, "p7");
        m.checkNotNullParameter(map2, "p8");
        m.checkNotNullParameter(map3, "p9");
        createModel = ((NoticePopupChannel) this.receiver).createModel(context, message, guild, channel, map, l, user, map2, map3);
        return createModel;
    }
}
