package com.discord.widgets.notice;

import andhook.lib.HookHelper;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.exifinterface.media.ExifInterface;
import b.d.b.a.a;
import com.discord.api.channel.Channel;
import com.discord.api.role.GuildRole;
import com.discord.api.sticker.Sticker;
import com.discord.models.guild.Guild;
import com.discord.models.member.GuildMember;
import com.discord.models.message.Message;
import com.discord.stores.StoreGuilds;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUser;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.facebook.drawee.span.DraweeSpanStringBuilder;
import d0.z.d.m;
import j0.l.e.k;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function9;
import rx.Observable;
import rx.functions.Func9;
/* compiled from: NoticePopupChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001$B\t\b\u0002¢\u0006\u0004\b\"\u0010#J\u0095\u0001\u0010\u0019\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\t\u001a\u0004\u0018\u00010\b2\u0016\u0010\u000e\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\f\u0012\u0004\u0012\u00020\r0\n2\u000e\u0010\u0010\u001a\n\u0018\u00010\u000bj\u0004\u0018\u0001`\u000f2\u0006\u0010\u0012\u001a\u00020\u00112\u0016\u0010\u0014\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u000f\u0012\u0004\u0012\u00020\u00130\n2\u0016\u0010\u0017\u001a\u0012\u0012\b\u0012\u00060\u000bj\u0002`\u0015\u0012\u0004\u0012\u00020\u00160\nH\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ9\u0010 \u001a\u00020\u001e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u0005\u001a\u00020\u00042\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u001c¢\u0006\u0004\b \u0010!¨\u0006%"}, d2 = {"Lcom/discord/widgets/notice/NoticePopupChannel;", "", "Landroid/content/Context;", "context", "Lcom/discord/models/message/Message;", "message", "Lcom/discord/models/guild/Guild;", "guild", "Lcom/discord/api/channel/Channel;", "channel", "", "", "Lcom/discord/primitives/UserId;", "Lcom/discord/models/member/GuildMember;", "members", "Lcom/discord/primitives/ChannelId;", "selectedChannel", "Lcom/discord/models/user/User;", "meUser", "", "channelNames", "Lcom/discord/primitives/GuildId;", "Lcom/discord/api/role/GuildRole;", "roles", "Lcom/discord/widgets/notice/NoticePopupChannel$Model;", "createModel", "(Landroid/content/Context;Lcom/discord/models/message/Message;Lcom/discord/models/guild/Guild;Lcom/discord/api/channel/Channel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/user/User;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;", "noticeName", "Lkotlin/Function1;", "Landroid/view/View;", "", "onClick", "enqueue", "(Landroid/content/Context;Ljava/lang/String;Lcom/discord/models/message/Message;Lkotlin/jvm/functions/Function1;)V", HookHelper.constructorName, "()V", ExifInterface.TAG_MODEL, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NoticePopupChannel {
    public static final NoticePopupChannel INSTANCE = new NoticePopupChannel();

    /* compiled from: NoticePopupChannel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u001a\b\u0082\b\u0018\u00002\u00020\u0001Bw\u0012\u0006\u0010\u001b\u001a\u00020\u0002\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u001d\u001a\u00020\u0006\u0012\b\u0010\u001e\u001a\u0004\u0018\u00010\t\u0012\b\u0010\u001f\u001a\u0004\u0018\u00010\f\u0012\b\u0010 \u001a\u0004\u0018\u00010\t\u0012\b\u0010!\u001a\u0004\u0018\u00010\f\u0012\b\u0010\"\u001a\u0004\u0018\u00010\f\u0012\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012\u0012\u000e\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017¢\u0006\u0004\bD\u0010EJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u000bJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u000eJ\u0012\u0010\u0011\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\u0011\u0010\u000eJ\u001c\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012HÆ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0018\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017HÆ\u0003¢\u0006\u0004\b\u0019\u0010\u001aJ\u0094\u0001\u0010%\u001a\u00020\u00002\b\b\u0002\u0010\u001b\u001a\u00020\u00022\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\u001d\u001a\u00020\u00062\n\b\u0002\u0010\u001e\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010!\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\f2\u0014\b\u0002\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00122\u0010\b\u0002\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017HÆ\u0001¢\u0006\u0004\b%\u0010&J\u0010\u0010'\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b'\u0010\u000bJ\u0010\u0010)\u001a\u00020(HÖ\u0001¢\u0006\u0004\b)\u0010*J\u001a\u0010-\u001a\u00020,2\b\u0010+\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b-\u0010.R\u001b\u0010\"\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\"\u0010/\u001a\u0004\b0\u0010\u000eR\u0019\u0010\u001d\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u00101\u001a\u0004\b2\u0010\bR\u001b\u0010\u001e\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001e\u00103\u001a\u0004\b4\u0010\u000bR!\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u00178\u0006@\u0006¢\u0006\f\n\u0004\b$\u00105\u001a\u0004\b6\u0010\u001aR$\u0010 \u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b \u00103\u001a\u0004\b7\u0010\u000b\"\u0004\b8\u00109R\u0019\u0010\u001b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010:\u001a\u0004\b;\u0010\u0004R$\u0010\u001f\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001f\u0010/\u001a\u0004\b<\u0010\u000e\"\u0004\b=\u0010>R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001c\u0010:\u001a\u0004\b?\u0010\u0004R%\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00128\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010@\u001a\u0004\bA\u0010\u0016R$\u0010!\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b!\u0010/\u001a\u0004\bB\u0010\u000e\"\u0004\bC\u0010>¨\u0006F"}, d2 = {"Lcom/discord/widgets/notice/NoticePopupChannel$Model;", "", "", "component1", "()Ljava/lang/CharSequence;", "component2", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "component3", "()Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "", "component4", "()Ljava/lang/String;", "Landroid/graphics/drawable/Drawable;", "component5", "()Landroid/graphics/drawable/Drawable;", "component6", "component7", "component8", "Lkotlin/Function1;", "Landroid/view/View;", "", "component9", "()Lkotlin/jvm/functions/Function1;", "", "Lcom/discord/api/sticker/Sticker;", "component10", "()Ljava/util/List;", "noticeTitle", "noticeSubtitle", "noticeBody", "noticeIconUrl", "noticeBodyBackgroundDrawable", "noticeBodyImageUrl", "noticeBodyImageDrawable", "noticeIconTopRight", "onClickTopRightIcon", "noticeStickers", "copy", "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;Ljava/util/List;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Landroid/graphics/drawable/Drawable;", "getNoticeIconTopRight", "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;", "getNoticeBody", "Ljava/lang/String;", "getNoticeIconUrl", "Ljava/util/List;", "getNoticeStickers", "getNoticeBodyImageUrl", "setNoticeBodyImageUrl", "(Ljava/lang/String;)V", "Ljava/lang/CharSequence;", "getNoticeTitle", "getNoticeBodyBackgroundDrawable", "setNoticeBodyBackgroundDrawable", "(Landroid/graphics/drawable/Drawable;)V", "getNoticeSubtitle", "Lkotlin/jvm/functions/Function1;", "getOnClickTopRightIcon", "getNoticeBodyImageDrawable", "setNoticeBodyImageDrawable", HookHelper.constructorName, "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;Ljava/util/List;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Model {
        private final DraweeSpanStringBuilder noticeBody;
        private Drawable noticeBodyBackgroundDrawable;
        private Drawable noticeBodyImageDrawable;
        private String noticeBodyImageUrl;
        private final Drawable noticeIconTopRight;
        private final String noticeIconUrl;
        private final List<Sticker> noticeStickers;
        private final CharSequence noticeSubtitle;
        private final CharSequence noticeTitle;
        private final Function1<View, Unit> onClickTopRightIcon;

        /* JADX WARN: Multi-variable type inference failed */
        public Model(CharSequence charSequence, CharSequence charSequence2, DraweeSpanStringBuilder draweeSpanStringBuilder, String str, Drawable drawable, String str2, Drawable drawable2, Drawable drawable3, Function1<? super View, Unit> function1, List<Sticker> list) {
            m.checkNotNullParameter(charSequence, "noticeTitle");
            m.checkNotNullParameter(draweeSpanStringBuilder, "noticeBody");
            m.checkNotNullParameter(function1, "onClickTopRightIcon");
            this.noticeTitle = charSequence;
            this.noticeSubtitle = charSequence2;
            this.noticeBody = draweeSpanStringBuilder;
            this.noticeIconUrl = str;
            this.noticeBodyBackgroundDrawable = drawable;
            this.noticeBodyImageUrl = str2;
            this.noticeBodyImageDrawable = drawable2;
            this.noticeIconTopRight = drawable3;
            this.onClickTopRightIcon = function1;
            this.noticeStickers = list;
        }

        public final CharSequence component1() {
            return this.noticeTitle;
        }

        public final List<Sticker> component10() {
            return this.noticeStickers;
        }

        public final CharSequence component2() {
            return this.noticeSubtitle;
        }

        public final DraweeSpanStringBuilder component3() {
            return this.noticeBody;
        }

        public final String component4() {
            return this.noticeIconUrl;
        }

        public final Drawable component5() {
            return this.noticeBodyBackgroundDrawable;
        }

        public final String component6() {
            return this.noticeBodyImageUrl;
        }

        public final Drawable component7() {
            return this.noticeBodyImageDrawable;
        }

        public final Drawable component8() {
            return this.noticeIconTopRight;
        }

        public final Function1<View, Unit> component9() {
            return this.onClickTopRightIcon;
        }

        public final Model copy(CharSequence charSequence, CharSequence charSequence2, DraweeSpanStringBuilder draweeSpanStringBuilder, String str, Drawable drawable, String str2, Drawable drawable2, Drawable drawable3, Function1<? super View, Unit> function1, List<Sticker> list) {
            m.checkNotNullParameter(charSequence, "noticeTitle");
            m.checkNotNullParameter(draweeSpanStringBuilder, "noticeBody");
            m.checkNotNullParameter(function1, "onClickTopRightIcon");
            return new Model(charSequence, charSequence2, draweeSpanStringBuilder, str, drawable, str2, drawable2, drawable3, function1, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Model)) {
                return false;
            }
            Model model = (Model) obj;
            return m.areEqual(this.noticeTitle, model.noticeTitle) && m.areEqual(this.noticeSubtitle, model.noticeSubtitle) && m.areEqual(this.noticeBody, model.noticeBody) && m.areEqual(this.noticeIconUrl, model.noticeIconUrl) && m.areEqual(this.noticeBodyBackgroundDrawable, model.noticeBodyBackgroundDrawable) && m.areEqual(this.noticeBodyImageUrl, model.noticeBodyImageUrl) && m.areEqual(this.noticeBodyImageDrawable, model.noticeBodyImageDrawable) && m.areEqual(this.noticeIconTopRight, model.noticeIconTopRight) && m.areEqual(this.onClickTopRightIcon, model.onClickTopRightIcon) && m.areEqual(this.noticeStickers, model.noticeStickers);
        }

        public final DraweeSpanStringBuilder getNoticeBody() {
            return this.noticeBody;
        }

        public final Drawable getNoticeBodyBackgroundDrawable() {
            return this.noticeBodyBackgroundDrawable;
        }

        public final Drawable getNoticeBodyImageDrawable() {
            return this.noticeBodyImageDrawable;
        }

        public final String getNoticeBodyImageUrl() {
            return this.noticeBodyImageUrl;
        }

        public final Drawable getNoticeIconTopRight() {
            return this.noticeIconTopRight;
        }

        public final String getNoticeIconUrl() {
            return this.noticeIconUrl;
        }

        public final List<Sticker> getNoticeStickers() {
            return this.noticeStickers;
        }

        public final CharSequence getNoticeSubtitle() {
            return this.noticeSubtitle;
        }

        public final CharSequence getNoticeTitle() {
            return this.noticeTitle;
        }

        public final Function1<View, Unit> getOnClickTopRightIcon() {
            return this.onClickTopRightIcon;
        }

        public int hashCode() {
            CharSequence charSequence = this.noticeTitle;
            int i = 0;
            int hashCode = (charSequence != null ? charSequence.hashCode() : 0) * 31;
            CharSequence charSequence2 = this.noticeSubtitle;
            int hashCode2 = (hashCode + (charSequence2 != null ? charSequence2.hashCode() : 0)) * 31;
            DraweeSpanStringBuilder draweeSpanStringBuilder = this.noticeBody;
            int hashCode3 = (hashCode2 + (draweeSpanStringBuilder != null ? draweeSpanStringBuilder.hashCode() : 0)) * 31;
            String str = this.noticeIconUrl;
            int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
            Drawable drawable = this.noticeBodyBackgroundDrawable;
            int hashCode5 = (hashCode4 + (drawable != null ? drawable.hashCode() : 0)) * 31;
            String str2 = this.noticeBodyImageUrl;
            int hashCode6 = (hashCode5 + (str2 != null ? str2.hashCode() : 0)) * 31;
            Drawable drawable2 = this.noticeBodyImageDrawable;
            int hashCode7 = (hashCode6 + (drawable2 != null ? drawable2.hashCode() : 0)) * 31;
            Drawable drawable3 = this.noticeIconTopRight;
            int hashCode8 = (hashCode7 + (drawable3 != null ? drawable3.hashCode() : 0)) * 31;
            Function1<View, Unit> function1 = this.onClickTopRightIcon;
            int hashCode9 = (hashCode8 + (function1 != null ? function1.hashCode() : 0)) * 31;
            List<Sticker> list = this.noticeStickers;
            if (list != null) {
                i = list.hashCode();
            }
            return hashCode9 + i;
        }

        public final void setNoticeBodyBackgroundDrawable(Drawable drawable) {
            this.noticeBodyBackgroundDrawable = drawable;
        }

        public final void setNoticeBodyImageDrawable(Drawable drawable) {
            this.noticeBodyImageDrawable = drawable;
        }

        public final void setNoticeBodyImageUrl(String str) {
            this.noticeBodyImageUrl = str;
        }

        public String toString() {
            StringBuilder R = a.R("Model(noticeTitle=");
            R.append(this.noticeTitle);
            R.append(", noticeSubtitle=");
            R.append(this.noticeSubtitle);
            R.append(", noticeBody=");
            R.append((Object) this.noticeBody);
            R.append(", noticeIconUrl=");
            R.append(this.noticeIconUrl);
            R.append(", noticeBodyBackgroundDrawable=");
            R.append(this.noticeBodyBackgroundDrawable);
            R.append(", noticeBodyImageUrl=");
            R.append(this.noticeBodyImageUrl);
            R.append(", noticeBodyImageDrawable=");
            R.append(this.noticeBodyImageDrawable);
            R.append(", noticeIconTopRight=");
            R.append(this.noticeIconTopRight);
            R.append(", onClickTopRightIcon=");
            R.append(this.onClickTopRightIcon);
            R.append(", noticeStickers=");
            return a.K(R, this.noticeStickers, ")");
        }
    }

    private NoticePopupChannel() {
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:11:0x0076, code lost:
        if (r0 != null) goto L14;
     */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r16v10 */
    /* JADX WARN: Type inference failed for: r16v3, types: [android.graphics.drawable.Drawable] */
    /* JADX WARN: Type inference failed for: r18v4 */
    /* JADX WARN: Type inference failed for: r18v5 */
    /* JADX WARN: Type inference failed for: r9v1, types: [java.lang.Object, java.lang.Integer] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final com.discord.widgets.notice.NoticePopupChannel.Model createModel(android.content.Context r28, com.discord.models.message.Message r29, com.discord.models.guild.Guild r30, com.discord.api.channel.Channel r31, java.util.Map<java.lang.Long, com.discord.models.member.GuildMember> r32, java.lang.Long r33, com.discord.models.user.User r34, java.util.Map<java.lang.Long, java.lang.String> r35, java.util.Map<java.lang.Long, com.discord.api.role.GuildRole> r36) {
        /*
            Method dump skipped, instructions count: 549
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.notice.NoticePopupChannel.createModel(android.content.Context, com.discord.models.message.Message, com.discord.models.guild.Guild, com.discord.api.channel.Channel, java.util.Map, java.lang.Long, com.discord.models.user.User, java.util.Map, java.util.Map):com.discord.widgets.notice.NoticePopupChannel$Model");
    }

    public final void enqueue(Context context, String str, Message message, Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(str, "noticeName");
        m.checkNotNullParameter(message, "message");
        m.checkNotNullParameter(function1, "onClick");
        k kVar = new k(context);
        k kVar2 = new k(message);
        StoreStream.Companion companion = StoreStream.Companion;
        Observable<Guild> observeFromChannelId = companion.getGuilds().observeFromChannelId(message.getChannelId());
        Observable<Channel> observeChannel = companion.getChannels().observeChannel(message.getChannelId());
        StoreGuilds guilds = companion.getGuilds();
        Long guildId = message.getGuildId();
        long j = 0;
        Observable<Map<Long, GuildMember>> observeComputed = guilds.observeComputed(guildId != null ? guildId.longValue() : 0L);
        Observable<Long> observeId = companion.getChannelsSelected().observeId();
        Observable observeMe$default = StoreUser.observeMe$default(companion.getUsers(), false, 1, null);
        Observable<Map<Long, String>> observeNames = companion.getChannels().observeNames();
        StoreGuilds guilds2 = companion.getGuilds();
        Long guildId2 = message.getGuildId();
        if (guildId2 != null) {
            j = guildId2.longValue();
        }
        Observable<Map<Long, GuildRole>> observeRoles = guilds2.observeRoles(j);
        final NoticePopupChannel$enqueue$1 noticePopupChannel$enqueue$1 = new NoticePopupChannel$enqueue$1(this);
        Observable c = Observable.c(kVar, kVar2, observeFromChannelId, observeChannel, observeComputed, observeId, observeMe$default, observeNames, observeRoles, new Func9() { // from class: com.discord.widgets.notice.NoticePopupChannel$sam$rx_functions_Func9$0
            @Override // rx.functions.Func9
            public final /* synthetic */ Object call(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, Object obj8, Object obj9) {
                return Function9.this.invoke(obj, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9);
            }
        });
        m.checkNotNullExpressionValue(c, "Observable\n        .comb…is::createModel\n        )");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.computationLatest(ObservableExtensionsKt.takeSingleUntilTimeout$default(c, 0L, false, 3, null))), NoticePopupChannel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new NoticePopupChannel$enqueue$2(str, function1));
    }
}
