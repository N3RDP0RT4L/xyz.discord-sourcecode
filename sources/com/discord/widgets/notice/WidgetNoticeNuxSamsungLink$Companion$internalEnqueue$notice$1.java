package com.discord.widgets.notice;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.discord.utilities.analytics.AnalyticsTracker;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetNoticeNuxSamsungLink.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroidx/fragment/app/FragmentActivity;", "it", "", "invoke", "(Landroidx/fragment/app/FragmentActivity;)Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetNoticeNuxSamsungLink$Companion$internalEnqueue$notice$1 extends o implements Function1<FragmentActivity, Boolean> {
    public static final WidgetNoticeNuxSamsungLink$Companion$internalEnqueue$notice$1 INSTANCE = new WidgetNoticeNuxSamsungLink$Companion$internalEnqueue$notice$1();

    public WidgetNoticeNuxSamsungLink$Companion$internalEnqueue$notice$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Boolean invoke(FragmentActivity fragmentActivity) {
        return Boolean.valueOf(invoke2(fragmentActivity));
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final boolean invoke2(FragmentActivity fragmentActivity) {
        m.checkNotNullParameter(fragmentActivity, "it");
        WidgetNoticeNuxSamsungLink widgetNoticeNuxSamsungLink = new WidgetNoticeNuxSamsungLink();
        FragmentManager supportFragmentManager = fragmentActivity.getSupportFragmentManager();
        m.checkNotNullExpressionValue(supportFragmentManager, "it.supportFragmentManager");
        widgetNoticeNuxSamsungLink.show(supportFragmentManager, WidgetNoticeNuxSamsungLink.class.getName());
        AnalyticsTracker.openModal$default("Mobile Samsung Link Upsell", "", null, 4, null);
        return true;
    }
}
