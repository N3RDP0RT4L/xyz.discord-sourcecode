package com.discord.widgets.notice;

import com.discord.widgets.home.WidgetHome;
import com.discord.widgets.notice.NoticePopupChannel;
import d0.t.m;
import d0.z.d.a0;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: NoticePopupChannel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/notice/NoticePopupChannel$Model;", "model", "", "invoke", "(Lcom/discord/widgets/notice/NoticePopupChannel$Model;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class NoticePopupChannel$enqueue$2 extends o implements Function1<NoticePopupChannel.Model, Unit> {
    public final /* synthetic */ String $noticeName;
    public final /* synthetic */ Function1 $onClick;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public NoticePopupChannel$enqueue$2(String str, Function1 function1) {
        super(1);
        this.$noticeName = str;
        this.$onClick = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(NoticePopupChannel.Model model) {
        invoke2(model);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(NoticePopupChannel.Model model) {
        if (model != null) {
            NoticePopup noticePopup = NoticePopup.INSTANCE;
            String str = this.$noticeName;
            CharSequence noticeTitle = model.getNoticeTitle();
            CharSequence noticeSubtitle = model.getNoticeSubtitle();
            noticePopup.enqueue(str, noticeTitle, (r35 & 4) != 0 ? null : noticeSubtitle, model.getNoticeBody(), (r35 & 16) != 0 ? null : model.getNoticeBodyBackgroundDrawable(), (r35 & 32) != 0 ? null : model.getNoticeBodyImageUrl(), (r35 & 64) != 0 ? null : model.getNoticeBodyImageDrawable(), (r35 & 128) != 0 ? null : model.getNoticeStickers(), (r35 & 256) != 0 ? null : model.getNoticeIconUrl(), (r35 & 512) != 0 ? null : null, (r35 & 1024) != 0 ? null : model.getNoticeIconTopRight(), (r35 & 2048) != 0 ? 5 : null, (r35 & 4096) != 0 ? m.listOf(a0.getOrCreateKotlinClass(WidgetHome.class)) : null, (r35 & 8192) != 0 ? NoticePopup$enqueue$1.INSTANCE : model.getOnClickTopRightIcon(), this.$onClick);
        }
    }
}
