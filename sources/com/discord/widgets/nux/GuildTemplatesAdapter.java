package com.discord.widgets.nux;

import andhook.lib.HookHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import com.discord.databinding.NuxGuildTemplateCardViewHolderBinding;
import com.discord.databinding.NuxGuildTemplateTextViewHolderBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.nux.GuildTemplateViewType;
import com.google.android.material.card.MaterialCardView;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildTemplatesAdapter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\"B\u000f\u0012\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b \u0010!J\u001f\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\n\u001a\u00020\u00052\u0006\u0010\t\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0013\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R6\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u00178\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001f¨\u0006#"}, d2 = {"Lcom/discord/widgets/nux/GuildTemplatesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/ViewGroup;", "parent", "", "viewType", "onCreateViewHolder", "(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", ModelAuditLogEntry.CHANGE_KEY_POSITION, "getItemViewType", "(I)I", "holder", "", "onBindViewHolder", "(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V", "getItemCount", "()I", "Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;", "callbacks", "Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;", "getCallbacks", "()Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;", "", "Lcom/discord/widgets/nux/GuildTemplateViewType;", "value", "items", "Ljava/util/List;", "getItems", "()Ljava/util/List;", "setItems", "(Ljava/util/List;)V", HookHelper.constructorName, "(Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;)V", "Callbacks", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildTemplatesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Callbacks callbacks;
    private List<? extends GuildTemplateViewType> items = n.emptyList();

    /* compiled from: GuildTemplatesAdapter.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;", "", "Lcom/discord/widgets/nux/GuildTemplate;", "guildTemplate", "", "onTemplateClick", "(Lcom/discord/widgets/nux/GuildTemplate;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public interface Callbacks {
        void onTemplateClick(GuildTemplate guildTemplate);
    }

    public GuildTemplatesAdapter(Callbacks callbacks) {
        m.checkNotNullParameter(callbacks, "callbacks");
        this.callbacks = callbacks;
    }

    public final Callbacks getCallbacks() {
        return this.callbacks;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.items.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return this.items.get(i).getViewType();
    }

    public final List<GuildTemplateViewType> getItems() {
        return this.items;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        m.checkNotNullParameter(viewHolder, "holder");
        GuildTemplateViewType guildTemplateViewType = this.items.get(i);
        if (guildTemplateViewType instanceof GuildTemplateViewType.Template) {
            if (!(viewHolder instanceof GuildTemplateCardViewHolder)) {
                viewHolder = null;
            }
            GuildTemplateCardViewHolder guildTemplateCardViewHolder = (GuildTemplateCardViewHolder) viewHolder;
            if (guildTemplateCardViewHolder != null) {
                guildTemplateCardViewHolder.bind(((GuildTemplateViewType.Template) guildTemplateViewType).getGuildTemplate());
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        m.checkNotNullParameter(viewGroup, "parent");
        GuildTemplateViewType.Companion companion = GuildTemplateViewType.Companion;
        if (i == companion.getTEXT()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.nux_guild_template_text_view_holder, viewGroup, false);
            Objects.requireNonNull(inflate, "rootView");
            NuxGuildTemplateTextViewHolderBinding nuxGuildTemplateTextViewHolderBinding = new NuxGuildTemplateTextViewHolderBinding((TextView) inflate);
            m.checkNotNullExpressionValue(nuxGuildTemplateTextViewHolderBinding, "NuxGuildTemplateTextView…          false\n        )");
            return new GuildTemplateTextViewHolder(nuxGuildTemplateTextViewHolderBinding);
        } else if (i == companion.getTEMPLATE()) {
            View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.nux_guild_template_card_view_holder, viewGroup, false);
            MaterialCardView materialCardView = (MaterialCardView) inflate2;
            TextView textView = (TextView) inflate2.findViewById(R.id.nux_guild_template_text);
            if (textView != null) {
                NuxGuildTemplateCardViewHolderBinding nuxGuildTemplateCardViewHolderBinding = new NuxGuildTemplateCardViewHolderBinding((MaterialCardView) inflate2, materialCardView, textView);
                m.checkNotNullExpressionValue(nuxGuildTemplateCardViewHolderBinding, "NuxGuildTemplateCardView…          false\n        )");
                return new GuildTemplateCardViewHolder(nuxGuildTemplateCardViewHolderBinding, this.callbacks);
            }
            throw new NullPointerException("Missing required view with ID: ".concat(inflate2.getResources().getResourceName(R.id.nux_guild_template_text)));
        } else {
            throw new IllegalStateException(a.p("Missing view type for ", i));
        }
    }

    public final void setItems(List<? extends GuildTemplateViewType> list) {
        m.checkNotNullParameter(list, "value");
        this.items = u.plus((Collection) n.listOf((Object[]) new GuildTemplateViewType[]{new GuildTemplateViewType.Template(GuildTemplate.CreateMyOwn), GuildTemplateViewType.Text.INSTANCE}), (Iterable) list);
        notifyDataSetChanged();
    }
}
