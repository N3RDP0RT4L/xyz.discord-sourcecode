package com.discord.widgets.nux;

import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.guilds.create.CreateGuildTrigger;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetGuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\r\b\u0087\b\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\b\u0012\u0006\u0010\u0012\u001a\u00020\u000b\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0002¢\u0006\u0004\b-\u0010.J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÆ\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u000e\u0010\u0004JB\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\b2\b\b\u0002\u0010\u0012\u001a\u00020\u000b2\b\b\u0002\u0010\u0013\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0007J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u00022\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aHÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0019J \u0010#\u001a\u00020\"2\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b#\u0010$R\u0019\u0010\u0011\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010%\u001a\u0004\b&\u0010\nR\u0019\u0010\u0013\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010'\u001a\u0004\b(\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010'\u001a\u0004\b\u000f\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\u000b8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010)\u001a\u0004\b*\u0010\rR\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010+\u001a\u0004\b,\u0010\u0007¨\u0006/"}, d2 = {"Lcom/discord/widgets/nux/GuildCreateArgs;", "Landroid/os/Parcelable;", "", "component1", "()Z", "", "component2", "()Ljava/lang/String;", "Lcom/discord/widgets/guilds/create/CreateGuildTrigger;", "component3", "()Lcom/discord/widgets/guilds/create/CreateGuildTrigger;", "Lcom/discord/widgets/nux/GuildTemplateArgs;", "component4", "()Lcom/discord/widgets/nux/GuildTemplateArgs;", "component5", "isNux", ModelAuditLogEntry.CHANGE_KEY_LOCATION, "trigger", "guildTemplate", "closeWithResult", "copy", "(ZLjava/lang/String;Lcom/discord/widgets/guilds/create/CreateGuildTrigger;Lcom/discord/widgets/nux/GuildTemplateArgs;Z)Lcom/discord/widgets/nux/GuildCreateArgs;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Lcom/discord/widgets/guilds/create/CreateGuildTrigger;", "getTrigger", "Z", "getCloseWithResult", "Lcom/discord/widgets/nux/GuildTemplateArgs;", "getGuildTemplate", "Ljava/lang/String;", "getLocation", HookHelper.constructorName, "(ZLjava/lang/String;Lcom/discord/widgets/guilds/create/CreateGuildTrigger;Lcom/discord/widgets/nux/GuildTemplateArgs;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildCreateArgs implements Parcelable {
    public static final Parcelable.Creator<GuildCreateArgs> CREATOR = new Creator();
    private final boolean closeWithResult;
    private final GuildTemplateArgs guildTemplate;
    private final boolean isNux;
    private final String location;
    private final CreateGuildTrigger trigger;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static class Creator implements Parcelable.Creator<GuildCreateArgs> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final GuildCreateArgs createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "in");
            return new GuildCreateArgs(parcel.readInt() != 0, parcel.readString(), (CreateGuildTrigger) Enum.valueOf(CreateGuildTrigger.class, parcel.readString()), GuildTemplateArgs.CREATOR.createFromParcel(parcel), parcel.readInt() != 0);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final GuildCreateArgs[] newArray(int i) {
            return new GuildCreateArgs[i];
        }
    }

    public GuildCreateArgs(boolean z2, String str, CreateGuildTrigger createGuildTrigger, GuildTemplateArgs guildTemplateArgs, boolean z3) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        m.checkNotNullParameter(createGuildTrigger, "trigger");
        m.checkNotNullParameter(guildTemplateArgs, "guildTemplate");
        this.isNux = z2;
        this.location = str;
        this.trigger = createGuildTrigger;
        this.guildTemplate = guildTemplateArgs;
        this.closeWithResult = z3;
    }

    public static /* synthetic */ GuildCreateArgs copy$default(GuildCreateArgs guildCreateArgs, boolean z2, String str, CreateGuildTrigger createGuildTrigger, GuildTemplateArgs guildTemplateArgs, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = guildCreateArgs.isNux;
        }
        if ((i & 2) != 0) {
            str = guildCreateArgs.location;
        }
        String str2 = str;
        if ((i & 4) != 0) {
            createGuildTrigger = guildCreateArgs.trigger;
        }
        CreateGuildTrigger createGuildTrigger2 = createGuildTrigger;
        if ((i & 8) != 0) {
            guildTemplateArgs = guildCreateArgs.guildTemplate;
        }
        GuildTemplateArgs guildTemplateArgs2 = guildTemplateArgs;
        if ((i & 16) != 0) {
            z3 = guildCreateArgs.closeWithResult;
        }
        return guildCreateArgs.copy(z2, str2, createGuildTrigger2, guildTemplateArgs2, z3);
    }

    public final boolean component1() {
        return this.isNux;
    }

    public final String component2() {
        return this.location;
    }

    public final CreateGuildTrigger component3() {
        return this.trigger;
    }

    public final GuildTemplateArgs component4() {
        return this.guildTemplate;
    }

    public final boolean component5() {
        return this.closeWithResult;
    }

    public final GuildCreateArgs copy(boolean z2, String str, CreateGuildTrigger createGuildTrigger, GuildTemplateArgs guildTemplateArgs, boolean z3) {
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_LOCATION);
        m.checkNotNullParameter(createGuildTrigger, "trigger");
        m.checkNotNullParameter(guildTemplateArgs, "guildTemplate");
        return new GuildCreateArgs(z2, str, createGuildTrigger, guildTemplateArgs, z3);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildCreateArgs)) {
            return false;
        }
        GuildCreateArgs guildCreateArgs = (GuildCreateArgs) obj;
        return this.isNux == guildCreateArgs.isNux && m.areEqual(this.location, guildCreateArgs.location) && m.areEqual(this.trigger, guildCreateArgs.trigger) && m.areEqual(this.guildTemplate, guildCreateArgs.guildTemplate) && this.closeWithResult == guildCreateArgs.closeWithResult;
    }

    public final boolean getCloseWithResult() {
        return this.closeWithResult;
    }

    public final GuildTemplateArgs getGuildTemplate() {
        return this.guildTemplate;
    }

    public final String getLocation() {
        return this.location;
    }

    public final CreateGuildTrigger getTrigger() {
        return this.trigger;
    }

    public int hashCode() {
        boolean z2 = this.isNux;
        int i = 1;
        if (z2) {
            z2 = true;
        }
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        int i4 = i2 * 31;
        String str = this.location;
        int i5 = 0;
        int hashCode = (i4 + (str != null ? str.hashCode() : 0)) * 31;
        CreateGuildTrigger createGuildTrigger = this.trigger;
        int hashCode2 = (hashCode + (createGuildTrigger != null ? createGuildTrigger.hashCode() : 0)) * 31;
        GuildTemplateArgs guildTemplateArgs = this.guildTemplate;
        if (guildTemplateArgs != null) {
            i5 = guildTemplateArgs.hashCode();
        }
        int i6 = (hashCode2 + i5) * 31;
        boolean z3 = this.closeWithResult;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return i6 + i;
    }

    public final boolean isNux() {
        return this.isNux;
    }

    public String toString() {
        StringBuilder R = a.R("GuildCreateArgs(isNux=");
        R.append(this.isNux);
        R.append(", location=");
        R.append(this.location);
        R.append(", trigger=");
        R.append(this.trigger);
        R.append(", guildTemplate=");
        R.append(this.guildTemplate);
        R.append(", closeWithResult=");
        return a.M(R, this.closeWithResult, ")");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeInt(this.isNux ? 1 : 0);
        parcel.writeString(this.location);
        parcel.writeString(this.trigger.name());
        this.guildTemplate.writeToParcel(parcel, 0);
        parcel.writeInt(this.closeWithResult ? 1 : 0);
    }

    public /* synthetic */ GuildCreateArgs(boolean z2, String str, CreateGuildTrigger createGuildTrigger, GuildTemplateArgs guildTemplateArgs, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(z2, str, createGuildTrigger, guildTemplateArgs, (i & 16) != 0 ? false : z3);
    }
}
