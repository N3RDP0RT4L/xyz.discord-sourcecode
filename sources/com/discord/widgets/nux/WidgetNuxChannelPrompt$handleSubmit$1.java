package com.discord.widgets.nux;

import b.a.k.b;
import com.discord.analytics.generated.events.network_action.TrackNetworkActionChannelCreate;
import com.discord.analytics.generated.traits.TrackNetworkMetadataReceiver;
import com.discord.api.channel.Channel;
import com.discord.api.permission.PermissionOverwrite;
import com.discord.databinding.WidgetNuxChannelPromptBinding;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.error.Error;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.functions.Action1;
import xyz.discord.R;
/* compiled from: WidgetNuxChannelPrompt.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "it", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetNuxChannelPrompt$handleSubmit$1 extends o implements Function1<Channel, Unit> {
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ WidgetNuxChannelPrompt this$0;

    /* compiled from: WidgetNuxChannelPrompt.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/channel/Channel;", "channel", "Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "invoke", "(Lcom/discord/api/channel/Channel;)Lcom/discord/analytics/generated/traits/TrackNetworkMetadataReceiver;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.nux.WidgetNuxChannelPrompt$handleSubmit$1$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass1 extends o implements Function1<Channel, TrackNetworkMetadataReceiver> {
        public AnonymousClass1() {
            super(1);
        }

        public final TrackNetworkMetadataReceiver invoke(Channel channel) {
            List<PermissionOverwrite> s2;
            return new TrackNetworkActionChannelCreate((channel == null || (s2 = channel.s()) == null) ? null : Boolean.valueOf(!s2.isEmpty()), channel != null ? Long.valueOf(channel.A()) : null, channel != null ? Long.valueOf(channel.h()) : null, channel != null ? Long.valueOf(channel.r()) : null, Long.valueOf(WidgetNuxChannelPrompt$handleSubmit$1.this.$guildId));
        }
    }

    /* compiled from: WidgetNuxChannelPrompt.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/api/channel/Channel;", "kotlin.jvm.PlatformType", "it", "", "invoke", "(Lcom/discord/api/channel/Channel;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.nux.WidgetNuxChannelPrompt$handleSubmit$1$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final class AnonymousClass2 extends o implements Function1<Channel, Unit> {
        public AnonymousClass2() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
            invoke2(channel);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(Channel channel) {
            WidgetNuxChannelPrompt$handleSubmit$1 widgetNuxChannelPrompt$handleSubmit$1 = WidgetNuxChannelPrompt$handleSubmit$1.this;
            widgetNuxChannelPrompt$handleSubmit$1.this$0.finishActivity(widgetNuxChannelPrompt$handleSubmit$1.$guildId);
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetNuxChannelPrompt$handleSubmit$1(WidgetNuxChannelPrompt widgetNuxChannelPrompt, long j) {
        super(1);
        this.this$0 = widgetNuxChannelPrompt;
        this.$guildId = j;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Channel channel) {
        invoke2(channel);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Channel channel) {
        WidgetNuxChannelPromptBinding binding;
        CharSequence e;
        WidgetNuxChannelPromptBinding binding2;
        if (channel == null) {
            binding2 = this.this$0.getBinding();
            binding2.f2481b.setIsLoading(false);
            return;
        }
        binding = this.this$0.getBinding();
        TextInputLayout textInputLayout = binding.g;
        m.checkNotNullExpressionValue(textInputLayout, "binding.nufChannelPromptTopicWrap");
        String textOrEmpty = ViewExtensions.getTextOrEmpty(textInputLayout);
        Long valueOf = Long.valueOf(channel.r());
        e = b.e(this.this$0, R.string.nuf_channel_prompt_channel_topic_template, new Object[]{textOrEmpty}, (r4 & 4) != 0 ? b.a.j : null);
        ObservableExtensionsKt.ui$default(ObservableExtensionsKt.restSubscribeOn$default(RestCallStateKt.logNetworkAction(RestAPI.Companion.getApi().createGuildChannel(this.$guildId, new RestAPIParams.CreateGuildChannel(0, null, textOrEmpty, valueOf, null, e.toString())), new AnonymousClass1()), false, 1, null), this.this$0, null, 2, null).k(b.a.d.o.a.g(this.this$0.requireContext(), new AnonymousClass2(), new Action1<Error>() { // from class: com.discord.widgets.nux.WidgetNuxChannelPrompt$handleSubmit$1.3
            public final void call(Error error) {
                WidgetNuxChannelPrompt widgetNuxChannelPrompt = WidgetNuxChannelPrompt$handleSubmit$1.this.this$0;
                m.checkNotNullExpressionValue(error, "error");
                widgetNuxChannelPrompt.handleError(error);
            }
        }));
    }
}
