package com.discord.widgets.nux;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.WidgetNuxGuildTemplateBinding;
import com.google.android.material.button.MaterialButton;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetGuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetNuxGuildTemplateBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetNuxGuildTemplateBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetGuildTemplates$binding$2 extends k implements Function1<View, WidgetNuxGuildTemplateBinding> {
    public static final WidgetGuildTemplates$binding$2 INSTANCE = new WidgetGuildTemplates$binding$2();

    public WidgetGuildTemplates$binding$2() {
        super(1, WidgetNuxGuildTemplateBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetNuxGuildTemplateBinding;", 0);
    }

    public final WidgetNuxGuildTemplateBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.nux_guild_template_action_join;
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.nux_guild_template_action_join);
        if (materialButton != null) {
            i = R.id.nux_guild_template_section_bottom;
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.nux_guild_template_section_bottom);
            if (linearLayout != null) {
                i = R.id.nux_guild_template_section_custom_label;
                TextView textView = (TextView) view.findViewById(R.id.nux_guild_template_section_custom_label);
                if (textView != null) {
                    i = R.id.nux_guild_template_section_title;
                    TextView textView2 = (TextView) view.findViewById(R.id.nux_guild_template_section_title);
                    if (textView2 != null) {
                        i = R.id.recycler_view;
                        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
                        if (recyclerView != null) {
                            return new WidgetNuxGuildTemplateBinding((LinearLayout) view, materialButton, linearLayout, textView, textView2, recyclerView);
                        }
                    }
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
