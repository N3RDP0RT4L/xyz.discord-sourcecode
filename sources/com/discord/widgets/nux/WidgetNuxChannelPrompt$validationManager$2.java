package com.discord.widgets.nux;

import com.discord.databinding.WidgetNuxChannelPromptBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.view.validators.Input;
import com.discord.utilities.view.validators.InputValidator;
import com.discord.utilities.view.validators.ValidationManager;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetNuxChannelPrompt.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"Lcom/discord/utilities/view/validators/ValidationManager;", "invoke", "()Lcom/discord/utilities/view/validators/ValidationManager;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetNuxChannelPrompt$validationManager$2 extends o implements Function0<ValidationManager> {
    public final /* synthetic */ WidgetNuxChannelPrompt this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetNuxChannelPrompt$validationManager$2(WidgetNuxChannelPrompt widgetNuxChannelPrompt) {
        super(0);
        this.this$0 = widgetNuxChannelPrompt;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final ValidationManager invoke() {
        WidgetNuxChannelPromptBinding binding;
        binding = this.this$0.getBinding();
        TextInputLayout textInputLayout = binding.g;
        m.checkNotNullExpressionValue(textInputLayout, "binding.nufChannelPromptTopicWrap");
        return new ValidationManager(new Input.TextInputLayoutInput(ModelAuditLogEntry.CHANGE_KEY_NAME, textInputLayout, new InputValidator[0]));
    }
}
