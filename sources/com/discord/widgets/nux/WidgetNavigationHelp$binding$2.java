package com.discord.widgets.nux;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.viewpager2.widget.ViewPager2;
import com.discord.databinding.WidgetNavigationHelpBinding;
import com.google.android.material.tabs.TabLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetNavigationHelp.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetNavigationHelpBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetNavigationHelpBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetNavigationHelp$binding$2 extends k implements Function1<View, WidgetNavigationHelpBinding> {
    public static final WidgetNavigationHelp$binding$2 INSTANCE = new WidgetNavigationHelp$binding$2();

    public WidgetNavigationHelp$binding$2() {
        super(1, WidgetNavigationHelpBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetNavigationHelpBinding;", 0);
    }

    public final WidgetNavigationHelpBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.navigation_help_close;
        ImageView imageView = (ImageView) view.findViewById(R.id.navigation_help_close);
        if (imageView != null) {
            i = R.id.navigation_help_indicators;
            TabLayout tabLayout = (TabLayout) view.findViewById(R.id.navigation_help_indicators);
            if (tabLayout != null) {
                i = R.id.navigation_help_nux_pager;
                ViewPager2 viewPager2 = (ViewPager2) view.findViewById(R.id.navigation_help_nux_pager);
                if (viewPager2 != null) {
                    return new WidgetNavigationHelpBinding((LinearLayout) view, imageView, tabLayout, viewPager2);
                }
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
