package com.discord.widgets.nux;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.j;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.databinding.WidgetNuxGuildTemplateBinding;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.widgets.guilds.create.CreateGuildTrigger;
import com.discord.widgets.guilds.create.StockGuildTemplate;
import com.discord.widgets.guilds.create.WidgetCreationIntent;
import com.discord.widgets.guilds.create.WidgetGuildCreate;
import com.discord.widgets.nux.GuildTemplateViewType;
import com.discord.widgets.nux.GuildTemplatesAdapter;
import d0.g;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.functions.Func0;
import xyz.discord.R;
/* compiled from: WidgetGuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\b\u0016\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u0007¢\u0006\u0004\b\u001d\u0010\tJ\u001f\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\nH\u0016¢\u0006\u0004\b\f\u0010\rR\u0016\u0010\u000f\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0016\u001a\u00020\u00118B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001d\u0010\u001c\u001a\u00020\u00178B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001b¨\u0006\u001f"}, d2 = {"Lcom/discord/widgets/nux/WidgetGuildTemplates;", "Lcom/discord/app/AppFragment;", "", "fromStep", "toStep", "", "trackPostRegistrationTransition", "(Ljava/lang/String;Ljava/lang/String;)V", "trackPostRegistrationSkip", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "Lcom/discord/widgets/nux/GuildTemplatesAdapter;", "adapter", "Lcom/discord/widgets/nux/GuildTemplatesAdapter;", "Lcom/discord/databinding/WidgetNuxGuildTemplateBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetNuxGuildTemplateBinding;", "binding", "Lcom/discord/widgets/nux/GuildCreateArgs;", "args$delegate", "Lkotlin/Lazy;", "getArgs", "()Lcom/discord/widgets/nux/GuildCreateArgs;", "args", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public class WidgetGuildTemplates extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetGuildTemplates.class, "binding", "getBinding()Lcom/discord/databinding/WidgetNuxGuildTemplateBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final Lazy args$delegate = g.lazy(new WidgetGuildTemplates$$special$$inlined$args$1(this, "intent_args_key"));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetGuildTemplates$binding$2.INSTANCE, null, 2, null);
    private final GuildTemplatesAdapter adapter = new GuildTemplatesAdapter(new GuildTemplatesAdapter.Callbacks() { // from class: com.discord.widgets.nux.WidgetGuildTemplates$adapter$1
        @Override // com.discord.widgets.nux.GuildTemplatesAdapter.Callbacks
        public void onTemplateClick(GuildTemplate guildTemplate) {
            GuildCreateArgs args;
            GuildCreateArgs args2;
            GuildCreateArgs args3;
            GuildCreateArgs args4;
            m.checkNotNullParameter(guildTemplate, "guildTemplate");
            StockGuildTemplate templateType = guildTemplate.getTemplateType();
            boolean z2 = guildTemplate.getTemplateType() == StockGuildTemplate.CREATE;
            String string = WidgetGuildTemplates.this.getString(R.string.guild_create_title);
            args = WidgetGuildTemplates.this.getArgs();
            String location = args.getLocation();
            args2 = WidgetGuildTemplates.this.getArgs();
            WidgetGuildCreate.Options options = new WidgetGuildCreate.Options(location, templateType, z2, string, args2.getCloseWithResult());
            args3 = WidgetGuildTemplates.this.getArgs();
            if (args3.getGuildTemplate().getSkipCreationIntent()) {
                AnalyticsTracker.openModal$default("Create Guild Step 2", options.getAnalyticsLocation(), null, 4, null);
                WidgetGuildCreate.Companion.showFragment(WidgetGuildTemplates.this, options);
                return;
            }
            WidgetGuildTemplates.this.trackPostRegistrationTransition(GuildTemplateAnalytics.STEP_GUILD_TEMPLATE, GuildTemplateAnalytics.STEP_CREATION_INTENT);
            WidgetCreationIntent.Companion companion = WidgetCreationIntent.Companion;
            WidgetGuildTemplates widgetGuildTemplates = WidgetGuildTemplates.this;
            args4 = widgetGuildTemplates.getArgs();
            companion.show(widgetGuildTemplates, args4.getTrigger(), options);
        }
    });

    /* compiled from: WidgetGuildTemplates.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ%\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\t\u0010\r¨\u0006\u0010"}, d2 = {"Lcom/discord/widgets/nux/WidgetGuildTemplates$Companion;", "", "Landroid/content/Context;", "context", "Lcom/discord/widgets/guilds/create/CreateGuildTrigger;", "trigger", "", "isNux", "", "launch", "(Landroid/content/Context;Lcom/discord/widgets/guilds/create/CreateGuildTrigger;Z)V", "Lcom/discord/widgets/nux/GuildCreateArgs;", "args", "(Landroid/content/Context;Lcom/discord/widgets/nux/GuildCreateArgs;)V", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void launch(Context context, CreateGuildTrigger createGuildTrigger, boolean z2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(createGuildTrigger, "trigger");
            String str = z2 ? GuildTemplateAnalytics.STEP_GUILD_TEMPLATE : GuildTemplateAnalytics.IN_APP_LOCATION_TEMPLATE;
            String string = context.getString(R.string.guild_template_selector_title);
            m.checkNotNullExpressionValue(string, "context.getString(R.stri…_template_selector_title)");
            String string2 = context.getString(R.string.guild_template_selector_description);
            m.checkNotNullExpressionValue(string2, "context.getString(R.stri…ate_selector_description)");
            launch(context, new GuildCreateArgs(z2, str, createGuildTrigger, new GuildTemplateArgs(string, string2, null, false, false, 28, null), false, 16, null));
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final void launch(Context context, GuildCreateArgs guildCreateArgs) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(guildCreateArgs, "args");
            j.d(context, WidgetGuildTemplates.class, guildCreateArgs);
        }
    }

    public WidgetGuildTemplates() {
        super(R.layout.widget_nux_guild_template);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final GuildCreateArgs getArgs() {
        return (GuildCreateArgs) this.args$delegate.getValue();
    }

    private final WidgetNuxGuildTemplateBinding getBinding() {
        return (WidgetNuxGuildTemplateBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void trackPostRegistrationSkip() {
        if (getArgs().isNux()) {
            GuildTemplateAnalytics.INSTANCE.postRegistrationSkip$app_productionGoogleRelease(GuildTemplateAnalytics.STEP_GUILD_TEMPLATE);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void trackPostRegistrationTransition(String str, String str2) {
        if (getArgs().isNux()) {
            GuildTemplateAnalytics.INSTANCE.postRegistrationTransition$app_productionGoogleRelease(str, str2);
        }
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        trackPostRegistrationTransition(GuildTemplateAnalytics.STEP_REGISTRATION, GuildTemplateAnalytics.STEP_GUILD_TEMPLATE);
        int i = 0;
        AppFragment.setOnBackPressed$default(this, new Func0<Boolean>() { // from class: com.discord.widgets.nux.WidgetGuildTemplates$onViewBound$1
            @Override // rx.functions.Func0, java.util.concurrent.Callable
            public final Boolean call() {
                WidgetGuildTemplates.this.trackPostRegistrationSkip();
                return Boolean.FALSE;
            }
        }, 0, 2, null);
        RecyclerView recyclerView = getBinding().f;
        m.checkNotNullExpressionValue(recyclerView, "binding.recyclerView");
        recyclerView.setAdapter(this.adapter);
        TextView textView = getBinding().e;
        m.checkNotNullExpressionValue(textView, "binding.nuxGuildTemplateSectionTitle");
        textView.setText(getArgs().getGuildTemplate().getTitle());
        TextView textView2 = getBinding().d;
        m.checkNotNullExpressionValue(textView2, "binding.nuxGuildTemplateSectionCustomLabel");
        textView2.setText(getArgs().getGuildTemplate().getSubtitle());
        GuildTemplatesAdapter guildTemplatesAdapter = this.adapter;
        List<GuildTemplate> templates = getArgs().getGuildTemplate().getTemplates();
        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(templates, 10));
        for (GuildTemplate guildTemplate : templates) {
            arrayList.add(new GuildTemplateViewType.Template(guildTemplate));
        }
        guildTemplatesAdapter.setItems(arrayList);
        getBinding().f2482b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.nux.WidgetGuildTemplates$onViewBound$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetGuildTemplates.this.trackPostRegistrationTransition(GuildTemplateAnalytics.STEP_GUILD_TEMPLATE, GuildTemplateAnalytics.STEP_GUILD_JOIN);
                WidgetNuxPostRegistrationJoin.Companion.show(WidgetGuildTemplates.this.requireContext());
                FragmentActivity activity = WidgetGuildTemplates.this.e();
                if (activity != null) {
                    activity.finish();
                }
            }
        });
        LinearLayout linearLayout = getBinding().c;
        m.checkNotNullExpressionValue(linearLayout, "binding.nuxGuildTemplateSectionBottom");
        if (!getArgs().getGuildTemplate().getShowInvitePrompt()) {
            i = 8;
        }
        linearLayout.setVisibility(i);
    }
}
