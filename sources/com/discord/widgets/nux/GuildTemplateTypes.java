package com.discord.widgets.nux;

import andhook.lib.HookHelper;
import d0.t.n;
import java.util.List;
import kotlin.Metadata;
/* compiled from: GuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\n\u0010\u000bR\u001f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007R\u001f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\u0005\u001a\u0004\b\t\u0010\u0007¨\u0006\f"}, d2 = {"Lcom/discord/widgets/nux/GuildTemplateTypes;", "", "", "Lcom/discord/widgets/nux/GuildTemplate;", "HUB", "Ljava/util/List;", "getHUB", "()Ljava/util/List;", "NUX", "getNUX", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildTemplateTypes {
    public static final GuildTemplateTypes INSTANCE = new GuildTemplateTypes();
    private static final List<GuildTemplate> NUX = n.listOf((Object[]) new GuildTemplate[]{GuildTemplate.Gaming, GuildTemplate.SchoolClub, GuildTemplate.StudyGroup, GuildTemplate.Friends, GuildTemplate.ArtistsAndCreators, GuildTemplate.LocalCommunity});
    private static final List<GuildTemplate> HUB = n.listOf((Object[]) new GuildTemplate[]{GuildTemplate.HubStudyGroup, GuildTemplate.HubSchoolClub, GuildTemplate.Class, GuildTemplate.Social, GuildTemplate.SubjectOrMajor, GuildTemplate.Dorm});

    private GuildTemplateTypes() {
    }

    public final List<GuildTemplate> getHUB() {
        return HUB;
    }

    public final List<GuildTemplate> getNUX() {
        return NUX;
    }
}
