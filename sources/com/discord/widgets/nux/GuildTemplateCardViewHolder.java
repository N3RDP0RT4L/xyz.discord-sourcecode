package com.discord.widgets.nux;

import andhook.lib.HookHelper;
import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.discord.databinding.NuxGuildTemplateCardViewHolderBinding;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.widgets.nux.GuildTemplatesAdapter;
import d0.z.d.m;
import kotlin.Metadata;
import xyz.discord.R;
/* compiled from: GuildTemplateViewHolders.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\u0011\u0010\u0012J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/nux/GuildTemplateCardViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/discord/widgets/nux/GuildTemplate;", "guildTemplate", "", "bind", "(Lcom/discord/widgets/nux/GuildTemplate;)V", "Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;", "callbacks", "Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;", "getCallbacks", "()Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;", "Lcom/discord/databinding/NuxGuildTemplateCardViewHolderBinding;", "binding", "Lcom/discord/databinding/NuxGuildTemplateCardViewHolderBinding;", "getBinding", "()Lcom/discord/databinding/NuxGuildTemplateCardViewHolderBinding;", HookHelper.constructorName, "(Lcom/discord/databinding/NuxGuildTemplateCardViewHolderBinding;Lcom/discord/widgets/nux/GuildTemplatesAdapter$Callbacks;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildTemplateCardViewHolder extends RecyclerView.ViewHolder {
    private final NuxGuildTemplateCardViewHolderBinding binding;
    private final GuildTemplatesAdapter.Callbacks callbacks;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GuildTemplateCardViewHolder(NuxGuildTemplateCardViewHolderBinding nuxGuildTemplateCardViewHolderBinding, GuildTemplatesAdapter.Callbacks callbacks) {
        super(nuxGuildTemplateCardViewHolderBinding.a);
        m.checkNotNullParameter(nuxGuildTemplateCardViewHolderBinding, "binding");
        m.checkNotNullParameter(callbacks, "callbacks");
        this.binding = nuxGuildTemplateCardViewHolderBinding;
        this.callbacks = callbacks;
    }

    public final void bind(final GuildTemplate guildTemplate) {
        m.checkNotNullParameter(guildTemplate, "guildTemplate");
        TextView textView = this.binding.c;
        textView.setText(guildTemplate.getTitleRes());
        DrawableCompat.setCompoundDrawablesCompat(textView, guildTemplate.getDrawableRes(), 0, (int) R.drawable.icon_carrot, 0);
        this.binding.f2115b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.nux.GuildTemplateCardViewHolder$bind$2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GuildTemplateCardViewHolder.this.getCallbacks().onTemplateClick(guildTemplate);
            }
        });
    }

    public final NuxGuildTemplateCardViewHolderBinding getBinding() {
        return this.binding;
    }

    public final GuildTemplatesAdapter.Callbacks getCallbacks() {
        return this.callbacks;
    }
}
