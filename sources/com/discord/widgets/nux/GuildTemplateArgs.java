package com.discord.widgets.nux;

import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetGuildTemplates.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\r\b\u0087\b\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\b\b\u0002\u0010\u0011\u001a\u00020\n\u0012\b\b\u0002\u0010\u0012\u001a\u00020\n¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\r\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\r\u0010\fJH\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\b\b\u0002\u0010\u0011\u001a\u00020\n2\b\b\u0002\u0010\u0012\u001a\u00020\nHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0015\u0010\u0004J\u0010\u0010\u0017\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u001a\u0010\u001b\u001a\u00020\n2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0019HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\u001d\u0010\u0018J \u0010\"\u001a\u00020!2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u0016HÖ\u0001¢\u0006\u0004\b\"\u0010#R\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010$\u001a\u0004\b%\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010$\u001a\u0004\b&\u0010\u0004R\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010'\u001a\u0004\b(\u0010\tR\u0019\u0010\u0011\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010)\u001a\u0004\b*\u0010\fR\u0019\u0010\u0012\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010)\u001a\u0004\b+\u0010\f¨\u0006."}, d2 = {"Lcom/discord/widgets/nux/GuildTemplateArgs;", "Landroid/os/Parcelable;", "", "component1", "()Ljava/lang/String;", "component2", "", "Lcom/discord/widgets/nux/GuildTemplate;", "component3", "()Ljava/util/List;", "", "component4", "()Z", "component5", "title", "subtitle", "templates", "showInvitePrompt", "skipCreationIntent", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZ)Lcom/discord/widgets/nux/GuildTemplateArgs;", "toString", "", "hashCode", "()I", "", "other", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "Ljava/lang/String;", "getSubtitle", "getTitle", "Ljava/util/List;", "getTemplates", "Z", "getShowInvitePrompt", "getSkipCreationIntent", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZ)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class GuildTemplateArgs implements Parcelable {
    public static final Parcelable.Creator<GuildTemplateArgs> CREATOR = new Creator();
    private final boolean showInvitePrompt;
    private final boolean skipCreationIntent;
    private final String subtitle;
    private final List<GuildTemplate> templates;
    private final String title;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static class Creator implements Parcelable.Creator<GuildTemplateArgs> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final GuildTemplateArgs createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "in");
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            while (readInt != 0) {
                arrayList.add((GuildTemplate) Enum.valueOf(GuildTemplate.class, parcel.readString()));
                readInt--;
            }
            boolean z2 = false;
            boolean z3 = parcel.readInt() != 0;
            if (parcel.readInt() != 0) {
                z2 = true;
            }
            return new GuildTemplateArgs(readString, readString2, arrayList, z3, z2);
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final GuildTemplateArgs[] newArray(int i) {
            return new GuildTemplateArgs[i];
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public GuildTemplateArgs(String str, String str2, List<? extends GuildTemplate> list, boolean z2, boolean z3) {
        m.checkNotNullParameter(str, "title");
        m.checkNotNullParameter(str2, "subtitle");
        m.checkNotNullParameter(list, "templates");
        this.title = str;
        this.subtitle = str2;
        this.templates = list;
        this.showInvitePrompt = z2;
        this.skipCreationIntent = z3;
    }

    public static /* synthetic */ GuildTemplateArgs copy$default(GuildTemplateArgs guildTemplateArgs, String str, String str2, List list, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = guildTemplateArgs.title;
        }
        if ((i & 2) != 0) {
            str2 = guildTemplateArgs.subtitle;
        }
        String str3 = str2;
        List<GuildTemplate> list2 = list;
        if ((i & 4) != 0) {
            list2 = guildTemplateArgs.templates;
        }
        List list3 = list2;
        if ((i & 8) != 0) {
            z2 = guildTemplateArgs.showInvitePrompt;
        }
        boolean z4 = z2;
        if ((i & 16) != 0) {
            z3 = guildTemplateArgs.skipCreationIntent;
        }
        return guildTemplateArgs.copy(str, str3, list3, z4, z3);
    }

    public final String component1() {
        return this.title;
    }

    public final String component2() {
        return this.subtitle;
    }

    public final List<GuildTemplate> component3() {
        return this.templates;
    }

    public final boolean component4() {
        return this.showInvitePrompt;
    }

    public final boolean component5() {
        return this.skipCreationIntent;
    }

    public final GuildTemplateArgs copy(String str, String str2, List<? extends GuildTemplate> list, boolean z2, boolean z3) {
        m.checkNotNullParameter(str, "title");
        m.checkNotNullParameter(str2, "subtitle");
        m.checkNotNullParameter(list, "templates");
        return new GuildTemplateArgs(str, str2, list, z2, z3);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuildTemplateArgs)) {
            return false;
        }
        GuildTemplateArgs guildTemplateArgs = (GuildTemplateArgs) obj;
        return m.areEqual(this.title, guildTemplateArgs.title) && m.areEqual(this.subtitle, guildTemplateArgs.subtitle) && m.areEqual(this.templates, guildTemplateArgs.templates) && this.showInvitePrompt == guildTemplateArgs.showInvitePrompt && this.skipCreationIntent == guildTemplateArgs.skipCreationIntent;
    }

    public final boolean getShowInvitePrompt() {
        return this.showInvitePrompt;
    }

    public final boolean getSkipCreationIntent() {
        return this.skipCreationIntent;
    }

    public final String getSubtitle() {
        return this.subtitle;
    }

    public final List<GuildTemplate> getTemplates() {
        return this.templates;
    }

    public final String getTitle() {
        return this.title;
    }

    public int hashCode() {
        String str = this.title;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.subtitle;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        List<GuildTemplate> list = this.templates;
        if (list != null) {
            i = list.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z2 = this.showInvitePrompt;
        int i3 = 1;
        if (z2) {
            z2 = true;
        }
        int i4 = z2 ? 1 : 0;
        int i5 = z2 ? 1 : 0;
        int i6 = (i2 + i4) * 31;
        boolean z3 = this.skipCreationIntent;
        if (!z3) {
            i3 = z3 ? 1 : 0;
        }
        return i6 + i3;
    }

    public String toString() {
        StringBuilder R = a.R("GuildTemplateArgs(title=");
        R.append(this.title);
        R.append(", subtitle=");
        R.append(this.subtitle);
        R.append(", templates=");
        R.append(this.templates);
        R.append(", showInvitePrompt=");
        R.append(this.showInvitePrompt);
        R.append(", skipCreationIntent=");
        return a.M(R, this.skipCreationIntent, ")");
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeString(this.title);
        parcel.writeString(this.subtitle);
        List<GuildTemplate> list = this.templates;
        parcel.writeInt(list.size());
        for (GuildTemplate guildTemplate : list) {
            parcel.writeString(guildTemplate.name());
        }
        parcel.writeInt(this.showInvitePrompt ? 1 : 0);
        parcel.writeInt(this.skipCreationIntent ? 1 : 0);
    }

    public /* synthetic */ GuildTemplateArgs(String str, String str2, List<GuildTemplate> list, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, str2, (i & 4) != 0 ? GuildTemplateTypes.INSTANCE.getNUX() : list, (i & 8) != 0 ? true : z2, (i & 16) != 0 ? false : z3);
    }
}
