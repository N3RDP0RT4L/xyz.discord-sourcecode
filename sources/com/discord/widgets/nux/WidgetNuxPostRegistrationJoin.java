package com.discord.widgets.nux;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import androidx.fragment.app.FragmentActivity;
import b.a.d.j;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.view.extensions.ViewExtensions;
import com.discord.widgets.guilds.join.WidgetGuildJoin;
import com.google.android.material.textfield.TextInputLayout;
import d0.g0.t;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import xyz.discord.R;
/* compiled from: WidgetNuxPostRegistrationJoin.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0007¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;", "Lcom/discord/widgets/guilds/join/WidgetGuildJoin;", "Landroid/view/View;", "view", "", "onViewBound", "(Landroid/view/View;)V", HookHelper.constructorName, "()V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetNuxPostRegistrationJoin extends WidgetGuildJoin {
    public static final Companion Companion = new Companion(null);
    private static final String NUX_FLOW_TYPE = "Mobile NUX Post Reg";
    private static final String NUX_STEP = "Ask to join";

    /* compiled from: WidgetNuxPostRegistrationJoin.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$Companion;", "", "Landroid/content/Context;", "context", "", "show", "(Landroid/content/Context;)V", "", "NUX_FLOW_TYPE", "Ljava/lang/String;", "NUX_STEP", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void show(Context context) {
            m.checkNotNullParameter(context, "context");
            j.e(context, WidgetNuxPostRegistrationJoin.class, null, 4);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetNuxPostRegistrationJoin() {
        super(R.layout.widget_nux_post_registration);
    }

    @Override // com.discord.widgets.guilds.join.WidgetGuildJoin, com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        setActionBarDisplayHomeAsUpEnabled(false);
        AnalyticsTracker.newUserOnboarding$default(AnalyticsTracker.INSTANCE, NUX_FLOW_TYPE, GuildTemplateAnalytics.STEP_REGISTRATION, NUX_STEP, null, false, 24, null);
        final long currentTimeMillis = ClockFactory.get().currentTimeMillis();
        getBinding().f2401b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.nux.WidgetNuxPostRegistrationJoin$onViewBound$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TextInputLayout textInputLayout = WidgetNuxPostRegistrationJoin.this.getBinding().c;
                m.checkNotNullExpressionValue(textInputLayout, "binding.guildJoinInvite");
                if (t.isBlank(ViewExtensions.getTextOrEmpty(textInputLayout))) {
                    AnalyticsTracker.INSTANCE.newUserOnboarding("Mobile NUX Post Reg", "Ask to join", "Friend List", Long.valueOf(currentTimeMillis), true);
                    FragmentActivity activity = WidgetNuxPostRegistrationJoin.this.e();
                    if (activity != null) {
                        activity.onBackPressed();
                        return;
                    }
                    return;
                }
                AnalyticsTracker.INSTANCE.newUserOnboarding("Mobile NUX Post Reg", "Ask to join", "Accept Instant Invite", Long.valueOf(currentTimeMillis), false);
                WidgetNuxPostRegistrationJoin.this.handleGuildJoin();
                FragmentActivity activity2 = WidgetNuxPostRegistrationJoin.this.e();
                if (activity2 != null) {
                    activity2.finish();
                }
            }
        });
        TextInputLayout textInputLayout = getBinding().c;
        m.checkNotNullExpressionValue(textInputLayout, "binding.guildJoinInvite");
        ViewExtensions.addBindedTextWatcher(textInputLayout, this, new WidgetNuxPostRegistrationJoin$onViewBound$2(this));
    }
}
