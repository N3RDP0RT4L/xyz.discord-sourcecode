package com.discord.widgets.hubs;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import b.a.i.j5;
import b.a.i.k5;
import b.a.i.z4;
import com.discord.databinding.WidgetHubEmailFlowBinding;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.views.LoadingButton;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetHubEmailFlow.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Landroid/view/View;", "p1", "Lcom/discord/databinding/WidgetHubEmailFlowBinding;", "invoke", "(Landroid/view/View;)Lcom/discord/databinding/WidgetHubEmailFlowBinding;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final /* synthetic */ class WidgetHubEmailFlow$binding$2 extends k implements Function1<View, WidgetHubEmailFlowBinding> {
    public static final WidgetHubEmailFlow$binding$2 INSTANCE = new WidgetHubEmailFlow$binding$2();

    public WidgetHubEmailFlow$binding$2() {
        super(1, WidgetHubEmailFlowBinding.class, "bind", "bind(Landroid/view/View;)Lcom/discord/databinding/WidgetHubEmailFlowBinding;", 0);
    }

    public final WidgetHubEmailFlowBinding invoke(View view) {
        m.checkNotNullParameter(view, "p1");
        int i = R.id.discord_hub_email_button_layout;
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.discord_hub_email_button_layout);
        if (linearLayout != null) {
            i = R.id.discord_hub_email_confirmation;
            View findViewById = view.findViewById(R.id.discord_hub_email_confirmation);
            if (findViewById != null) {
                int i2 = R.id.discord_hub_email_confirmation_different;
                LinkifiedTextView linkifiedTextView = (LinkifiedTextView) findViewById.findViewById(R.id.discord_hub_email_confirmation_different);
                if (linkifiedTextView != null) {
                    i2 = R.id.discord_hub_email_confirmation_header_title;
                    TextView textView = (TextView) findViewById.findViewById(R.id.discord_hub_email_confirmation_header_title);
                    if (textView != null) {
                        i2 = R.id.discord_hub_email_confirmation_resend;
                        LinkifiedTextView linkifiedTextView2 = (LinkifiedTextView) findViewById.findViewById(R.id.discord_hub_email_confirmation_resend);
                        if (linkifiedTextView2 != null) {
                            j5 j5Var = new j5((NestedScrollView) findViewById, linkifiedTextView, textView, linkifiedTextView2);
                            View findViewById2 = view.findViewById(R.id.discord_hub_email_input);
                            if (findViewById2 != null) {
                                int i3 = R.id.discord_hub_email_header_description;
                                LinkifiedTextView linkifiedTextView3 = (LinkifiedTextView) findViewById2.findViewById(R.id.discord_hub_email_header_description);
                                if (linkifiedTextView3 != null) {
                                    i3 = R.id.discord_hub_email_header_description_email_input;
                                    TextInputEditText textInputEditText = (TextInputEditText) findViewById2.findViewById(R.id.discord_hub_email_header_description_email_input);
                                    if (textInputEditText != null) {
                                        i3 = R.id.discord_hub_email_header_description_email_input_layout;
                                        TextInputLayout textInputLayout = (TextInputLayout) findViewById2.findViewById(R.id.discord_hub_email_header_description_email_input_layout);
                                        if (textInputLayout != null) {
                                            i3 = R.id.discord_hub_email_header_image;
                                            ImageView imageView = (ImageView) findViewById2.findViewById(R.id.discord_hub_email_header_image);
                                            if (imageView != null) {
                                                i3 = R.id.discord_hub_email_header_title;
                                                TextView textView2 = (TextView) findViewById2.findViewById(R.id.discord_hub_email_header_title);
                                                if (textView2 != null) {
                                                    i3 = R.id.discord_hub_email_label;
                                                    TextView textView3 = (TextView) findViewById2.findViewById(R.id.discord_hub_email_label);
                                                    if (textView3 != null) {
                                                        z4 z4Var = new z4((NestedScrollView) findViewById2, linkifiedTextView3, textInputEditText, textInputLayout, imageView, textView2, textView3);
                                                        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.discord_hub_email_no);
                                                        if (materialButton != null) {
                                                            LoadingButton loadingButton = (LoadingButton) view.findViewById(R.id.discord_hub_email_yes);
                                                            if (loadingButton != null) {
                                                                View findViewById3 = view.findViewById(R.id.discord_hub_waitlist);
                                                                if (findViewById3 != null) {
                                                                    TextView textView4 = (TextView) findViewById3.findViewById(R.id.hub_waitlist_description);
                                                                    if (textView4 != null) {
                                                                        return new WidgetHubEmailFlowBinding((LinearLayout) view, linearLayout, j5Var, z4Var, materialButton, loadingButton, new k5((NestedScrollView) findViewById3, textView4));
                                                                    }
                                                                    throw new NullPointerException("Missing required view with ID: ".concat(findViewById3.getResources().getResourceName(R.id.hub_waitlist_description)));
                                                                }
                                                                i = R.id.discord_hub_waitlist;
                                                            } else {
                                                                i = R.id.discord_hub_email_yes;
                                                            }
                                                        } else {
                                                            i = R.id.discord_hub_email_no;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                throw new NullPointerException("Missing required view with ID: ".concat(findViewById2.getResources().getResourceName(i3)));
                            }
                            i = R.id.discord_hub_email_input;
                        }
                    }
                }
                throw new NullPointerException("Missing required view with ID: ".concat(findViewById.getResources().getResourceName(i2)));
            }
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }
}
