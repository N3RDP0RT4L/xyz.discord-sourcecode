package com.discord.widgets.hubs;

import com.discord.api.directory.DirectoryEntryGuild;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetHubAddServer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002¨\u0006\u0003"}, d2 = {"", "invoke", "()V", "com/discord/widgets/hubs/WidgetHubAddServer$onServerClickListener$1$2$1", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubAddServer$onServerClickListener$1$$special$$inlined$let$lambda$1 extends o implements Function0<Unit> {
    public final /* synthetic */ DirectoryEntryGuild $directoryEntry;
    public final /* synthetic */ HubAddServerState $state$inlined;
    public final /* synthetic */ WidgetHubAddServer$onServerClickListener$1 this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubAddServer$onServerClickListener$1$$special$$inlined$let$lambda$1(DirectoryEntryGuild directoryEntryGuild, WidgetHubAddServer$onServerClickListener$1 widgetHubAddServer$onServerClickListener$1, HubAddServerState hubAddServerState) {
        super(0);
        this.$directoryEntry = directoryEntryGuild;
        this.this$0 = widgetHubAddServer$onServerClickListener$1;
        this.$state$inlined = hubAddServerState;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        this.this$0.this$0.getViewModel().removeGuild(this.this$0.$guildId, this.$directoryEntry.c());
    }
}
