package com.discord.widgets.hubs;

import androidx.fragment.app.FragmentActivity;
import com.discord.stores.utilities.Success;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHubAddName.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/utilities/Success;", "Ljava/lang/Void;", "it", "", "invoke", "(Lcom/discord/stores/utilities/Success;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubAddName$configureUI$2 extends o implements Function1<Success<? extends Void>, Unit> {
    public final /* synthetic */ WidgetHubAddName this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubAddName$configureUI$2(WidgetHubAddName widgetHubAddName) {
        super(1);
        this.this$0 = widgetHubAddName;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Success<? extends Void> success) {
        invoke2((Success<Void>) success);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Success<Void> success) {
        m.checkNotNullParameter(success, "it");
        FragmentActivity activity = this.this$0.e();
        if (activity != null) {
            activity.onBackPressed();
        }
        WidgetHubAddName widgetHubAddName = this.this$0;
        widgetHubAddName.hideKeyboard(widgetHubAddName.getView());
    }
}
