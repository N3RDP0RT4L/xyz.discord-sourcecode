package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import b.d.b.a.a;
import com.discord.api.hubs.EmailVerification;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallState;
import d0.g;
import d0.z.d.m;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubEmailViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0002\u0012\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\t\u0012\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0004\b+\u0010,J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0018\u0010\u0007\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\tHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0003¢\u0006\u0004\b\u000e\u0010\u000fJJ\u0010\u0014\u001a\u00020\u00002\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00022\u0010\b\u0002\u0010\u0011\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00062\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\t2\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\r0\fHÆ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0004J\u0010\u0010\u0018\u001a\u00020\u0017HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u001a\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001c\u0010\u001dR!\u0010\u0011\u001a\n\u0018\u00010\u0005j\u0004\u0018\u0001`\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u001e\u001a\u0004\b\u001f\u0010\bR\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\r0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b!\u0010\u000fR\u001b\u0010\u0010\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\"\u001a\u0004\b#\u0010\u0004R\u001b\u0010\u0012\u001a\u0004\u0018\u00010\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010$\u001a\u0004\b%\u0010\u000bR\u001d\u0010*\u001a\u00020\u001b8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b&\u0010'\u001a\u0004\b(\u0010)¨\u0006-"}, d2 = {"Lcom/discord/widgets/hubs/HubEmailState;", "", "", "component1", "()Ljava/lang/String;", "", "Lcom/discord/primitives/GuildId;", "component2", "()Ljava/lang/Long;", "Lcom/discord/widgets/hubs/HubWaitlistResult;", "component3", "()Lcom/discord/widgets/hubs/HubWaitlistResult;", "Lcom/discord/stores/utilities/RestCallState;", "Lcom/discord/api/hubs/EmailVerification;", "component4", "()Lcom/discord/stores/utilities/RestCallState;", NotificationCompat.CATEGORY_EMAIL, "selectedGuildId", "waitlistResult", "verifyEmailAsync", "copy", "(Ljava/lang/String;Ljava/lang/Long;Lcom/discord/widgets/hubs/HubWaitlistResult;Lcom/discord/stores/utilities/RestCallState;)Lcom/discord/widgets/hubs/HubEmailState;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getSelectedGuildId", "Lcom/discord/stores/utilities/RestCallState;", "getVerifyEmailAsync", "Ljava/lang/String;", "getEmail", "Lcom/discord/widgets/hubs/HubWaitlistResult;", "getWaitlistResult", "hasMultipleDomains$delegate", "Lkotlin/Lazy;", "getHasMultipleDomains", "()Z", "hasMultipleDomains", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/Long;Lcom/discord/widgets/hubs/HubWaitlistResult;Lcom/discord/stores/utilities/RestCallState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubEmailState {
    private final String email;
    private final Lazy hasMultipleDomains$delegate;
    private final Long selectedGuildId;
    private final RestCallState<EmailVerification> verifyEmailAsync;
    private final HubWaitlistResult waitlistResult;

    public HubEmailState() {
        this(null, null, null, null, 15, null);
    }

    public HubEmailState(String str, Long l, HubWaitlistResult hubWaitlistResult, RestCallState<EmailVerification> restCallState) {
        m.checkNotNullParameter(restCallState, "verifyEmailAsync");
        this.email = str;
        this.selectedGuildId = l;
        this.waitlistResult = hubWaitlistResult;
        this.verifyEmailAsync = restCallState;
        this.hasMultipleDomains$delegate = g.lazy(new HubEmailState$hasMultipleDomains$2(this));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ HubEmailState copy$default(HubEmailState hubEmailState, String str, Long l, HubWaitlistResult hubWaitlistResult, RestCallState restCallState, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hubEmailState.email;
        }
        if ((i & 2) != 0) {
            l = hubEmailState.selectedGuildId;
        }
        if ((i & 4) != 0) {
            hubWaitlistResult = hubEmailState.waitlistResult;
        }
        if ((i & 8) != 0) {
            restCallState = hubEmailState.verifyEmailAsync;
        }
        return hubEmailState.copy(str, l, hubWaitlistResult, restCallState);
    }

    public final String component1() {
        return this.email;
    }

    public final Long component2() {
        return this.selectedGuildId;
    }

    public final HubWaitlistResult component3() {
        return this.waitlistResult;
    }

    public final RestCallState<EmailVerification> component4() {
        return this.verifyEmailAsync;
    }

    public final HubEmailState copy(String str, Long l, HubWaitlistResult hubWaitlistResult, RestCallState<EmailVerification> restCallState) {
        m.checkNotNullParameter(restCallState, "verifyEmailAsync");
        return new HubEmailState(str, l, hubWaitlistResult, restCallState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HubEmailState)) {
            return false;
        }
        HubEmailState hubEmailState = (HubEmailState) obj;
        return m.areEqual(this.email, hubEmailState.email) && m.areEqual(this.selectedGuildId, hubEmailState.selectedGuildId) && m.areEqual(this.waitlistResult, hubEmailState.waitlistResult) && m.areEqual(this.verifyEmailAsync, hubEmailState.verifyEmailAsync);
    }

    public final String getEmail() {
        return this.email;
    }

    public final boolean getHasMultipleDomains() {
        return ((Boolean) this.hasMultipleDomains$delegate.getValue()).booleanValue();
    }

    public final Long getSelectedGuildId() {
        return this.selectedGuildId;
    }

    public final RestCallState<EmailVerification> getVerifyEmailAsync() {
        return this.verifyEmailAsync;
    }

    public final HubWaitlistResult getWaitlistResult() {
        return this.waitlistResult;
    }

    public int hashCode() {
        String str = this.email;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Long l = this.selectedGuildId;
        int hashCode2 = (hashCode + (l != null ? l.hashCode() : 0)) * 31;
        HubWaitlistResult hubWaitlistResult = this.waitlistResult;
        int hashCode3 = (hashCode2 + (hubWaitlistResult != null ? hubWaitlistResult.hashCode() : 0)) * 31;
        RestCallState<EmailVerification> restCallState = this.verifyEmailAsync;
        if (restCallState != null) {
            i = restCallState.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        StringBuilder R = a.R("HubEmailState(email=");
        R.append(this.email);
        R.append(", selectedGuildId=");
        R.append(this.selectedGuildId);
        R.append(", waitlistResult=");
        R.append(this.waitlistResult);
        R.append(", verifyEmailAsync=");
        R.append(this.verifyEmailAsync);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ HubEmailState(String str, Long l, HubWaitlistResult hubWaitlistResult, RestCallState restCallState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : l, (i & 4) != 0 ? null : hubWaitlistResult, (i & 8) != 0 ? Default.INSTANCE : restCallState);
    }
}
