package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import android.os.Parcel;
import android.os.Parcelable;
import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubEmailViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\b\u0087\b\u0018\u00002\u00020\u0001B%\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0002\u0012\b\b\u0002\u0010\f\u001a\u00020\u0005\u0012\b\b\u0002\u0010\r\u001a\u00020\b¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ.\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\bHÆ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0010\u0010\u0004J\u0010\u0010\u0011\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0007J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0017\u0010\u0007J \u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\f\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001e\u001a\u0004\b\u001f\u0010\u0007R\u0019\u0010\r\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010 \u001a\u0004\b!\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\"\u001a\u0004\b#\u0010\u0004¨\u0006&"}, d2 = {"Lcom/discord/widgets/hubs/HubEmailArgs;", "Landroid/os/Parcelable;", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", "Lcom/discord/widgets/hubs/HubEmailEntryPoint;", "component3", "()Lcom/discord/widgets/hubs/HubEmailEntryPoint;", "guildName", "guildMemberCount", "entryPoint", "copy", "(Ljava/lang/String;ILcom/discord/widgets/hubs/HubEmailEntryPoint;)Lcom/discord/widgets/hubs/HubEmailArgs;", "toString", "hashCode", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "describeContents", "Landroid/os/Parcel;", "parcel", "flags", "", "writeToParcel", "(Landroid/os/Parcel;I)V", "I", "getGuildMemberCount", "Lcom/discord/widgets/hubs/HubEmailEntryPoint;", "getEntryPoint", "Ljava/lang/String;", "getGuildName", HookHelper.constructorName, "(Ljava/lang/String;ILcom/discord/widgets/hubs/HubEmailEntryPoint;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubEmailArgs implements Parcelable {
    public static final Parcelable.Creator<HubEmailArgs> CREATOR = new Creator();
    private final HubEmailEntryPoint entryPoint;
    private final int guildMemberCount;
    private final String guildName;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static class Creator implements Parcelable.Creator<HubEmailArgs> {
        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final HubEmailArgs createFromParcel(Parcel parcel) {
            m.checkNotNullParameter(parcel, "in");
            return new HubEmailArgs(parcel.readString(), parcel.readInt(), (HubEmailEntryPoint) Enum.valueOf(HubEmailEntryPoint.class, parcel.readString()));
        }

        /* JADX WARN: Can't rename method to resolve collision */
        @Override // android.os.Parcelable.Creator
        public final HubEmailArgs[] newArray(int i) {
            return new HubEmailArgs[i];
        }
    }

    public HubEmailArgs() {
        this(null, 0, null, 7, null);
    }

    public HubEmailArgs(String str, int i, HubEmailEntryPoint hubEmailEntryPoint) {
        m.checkNotNullParameter(str, "guildName");
        m.checkNotNullParameter(hubEmailEntryPoint, "entryPoint");
        this.guildName = str;
        this.guildMemberCount = i;
        this.entryPoint = hubEmailEntryPoint;
    }

    public static /* synthetic */ HubEmailArgs copy$default(HubEmailArgs hubEmailArgs, String str, int i, HubEmailEntryPoint hubEmailEntryPoint, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = hubEmailArgs.guildName;
        }
        if ((i2 & 2) != 0) {
            i = hubEmailArgs.guildMemberCount;
        }
        if ((i2 & 4) != 0) {
            hubEmailEntryPoint = hubEmailArgs.entryPoint;
        }
        return hubEmailArgs.copy(str, i, hubEmailEntryPoint);
    }

    public final String component1() {
        return this.guildName;
    }

    public final int component2() {
        return this.guildMemberCount;
    }

    public final HubEmailEntryPoint component3() {
        return this.entryPoint;
    }

    public final HubEmailArgs copy(String str, int i, HubEmailEntryPoint hubEmailEntryPoint) {
        m.checkNotNullParameter(str, "guildName");
        m.checkNotNullParameter(hubEmailEntryPoint, "entryPoint");
        return new HubEmailArgs(str, i, hubEmailEntryPoint);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HubEmailArgs)) {
            return false;
        }
        HubEmailArgs hubEmailArgs = (HubEmailArgs) obj;
        return m.areEqual(this.guildName, hubEmailArgs.guildName) && this.guildMemberCount == hubEmailArgs.guildMemberCount && m.areEqual(this.entryPoint, hubEmailArgs.entryPoint);
    }

    public final HubEmailEntryPoint getEntryPoint() {
        return this.entryPoint;
    }

    public final int getGuildMemberCount() {
        return this.guildMemberCount;
    }

    public final String getGuildName() {
        return this.guildName;
    }

    public int hashCode() {
        String str = this.guildName;
        int i = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.guildMemberCount) * 31;
        HubEmailEntryPoint hubEmailEntryPoint = this.entryPoint;
        if (hubEmailEntryPoint != null) {
            i = hubEmailEntryPoint.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("HubEmailArgs(guildName=");
        R.append(this.guildName);
        R.append(", guildMemberCount=");
        R.append(this.guildMemberCount);
        R.append(", entryPoint=");
        R.append(this.entryPoint);
        R.append(")");
        return R.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        m.checkNotNullParameter(parcel, "parcel");
        parcel.writeString(this.guildName);
        parcel.writeInt(this.guildMemberCount);
        parcel.writeString(this.entryPoint.name());
    }

    public /* synthetic */ HubEmailArgs(String str, int i, HubEmailEntryPoint hubEmailEntryPoint, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? 0 : i, (i2 & 4) != 0 ? HubEmailEntryPoint.Default : hubEmailEntryPoint);
    }
}
