package com.discord.widgets.hubs;

import com.discord.api.hubs.EmailVerification;
import com.discord.api.hubs.GuildInfo;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetHubEmailViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()Z", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class HubEmailState$hasMultipleDomains$2 extends o implements Function0<Boolean> {
    public final /* synthetic */ HubEmailState this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public HubEmailState$hasMultipleDomains$2(HubEmailState hubEmailState) {
        super(0);
        this.this$0 = hubEmailState;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final Boolean invoke2() {
        List<GuildInfo> a;
        EmailVerification invoke = this.this$0.getVerifyEmailAsync().invoke();
        return (invoke == null || (a = invoke.a()) == null || !(a.isEmpty() ^ true)) ? null : 1;
    }
}
