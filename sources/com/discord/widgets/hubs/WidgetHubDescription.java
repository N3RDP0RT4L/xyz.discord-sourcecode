package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetHubDescriptionBinding;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.stores.utilities.Loading;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.discord.views.ScreenTitleView;
import com.discord.widgets.hubs.WidgetRadioSelectorBottomSheet;
import com.google.android.material.textfield.TextInputEditText;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetHubDescription.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b-\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\tH\u0016¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\r\u0010\bR\u0016\u0010\u0011\u001a\u00020\u000e8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0017\u001a\u00020\u00128F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u001d\u0010\u001d\u001a\u00020\u00188F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u001d\u0010\"\u001a\u00020\u001e8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001f\u0010\u001a\u001a\u0004\b \u0010!R\u001c\u0010$\u001a\u00020#8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R\u001d\u0010,\u001a\u00020(8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b)\u0010\u001a\u001a\u0004\b*\u0010+¨\u0006."}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubDescription;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/hubs/HubDescriptionState;", "state", "", "configureUI", "(Lcom/discord/widgets/hubs/HubDescriptionState;)V", "maybeAddServer", "()V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/hubs/RadioSelectorItems;", "getSelectorArgs", "()Lcom/discord/widgets/hubs/RadioSelectorItems;", "selectorArgs", "Lcom/discord/databinding/WidgetHubDescriptionBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetHubDescriptionBinding;", "binding", "Lcom/discord/widgets/hubs/HubDescriptionArgs;", "args$delegate", "Lkotlin/Lazy;", "getArgs", "()Lcom/discord/widgets/hubs/HubDescriptionArgs;", "args", "Lcom/discord/widgets/hubs/WidgetHubDescriptionViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/hubs/WidgetHubDescriptionViewModel;", "viewModel", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager$delegate", "getValidationManager", "()Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubDescription extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetHubDescription.class, "binding", "getBinding()Lcom/discord/databinding/WidgetHubDescriptionBinding;", 0)};
    private final Lazy viewModel$delegate;
    private final Lazy args$delegate = g.lazy(new WidgetHubDescription$$special$$inlined$args$1(this, "intent_args_key"));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetHubDescription$binding$2.INSTANCE, null, 2, null);
    private final Lazy validationManager$delegate = g.lazy(new WidgetHubDescription$validationManager$2(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, new WidgetHubDescription$loggingConfig$1(this), 3);

    public WidgetHubDescription() {
        super(R.layout.widget_hub_description);
        WidgetHubDescription$viewModel$2 widgetHubDescription$viewModel$2 = new WidgetHubDescription$viewModel$2(this);
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetHubDescriptionViewModel.class), new WidgetHubDescription$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetHubDescription$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final HubDescriptionState hubDescriptionState) {
        CharSequence e;
        int i;
        String str;
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return");
            ScreenTitleView screenTitleView = getBinding().h;
            e = b.e(this, getArgs().isEditing() ? R.string.hub_entry_update_description_title : R.string.hub_customize_existing_guild_title, new Object[]{hubDescriptionState.getGuildName()}, (r4 & 4) != 0 ? b.a.j : null);
            screenTitleView.setTitle(e);
            LoadingButton loadingButton = getBinding().f2447b;
            loadingButton.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.WidgetHubDescription$configureUI$$inlined$apply$lambda$1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WidgetHubDescription.this.maybeAddServer();
                }
            });
            loadingButton.setIsLoading(hubDescriptionState.getAddServerAsync() instanceof Loading);
            loadingButton.setText(getString(getArgs().isEditing() ? R.string.save : R.string.hub_add_server_cta));
            TextView textView = getBinding().c;
            DirectoryEntryCategory selectedCategory = hubDescriptionState.getSelectedCategory();
            if (selectedCategory != null) {
                selectedCategory.getTitleRes();
                i = R.attr.colorTextNormal;
            } else {
                i = R.attr.colorTextMuted;
            }
            textView.setTextColor(ColorCompat.getThemedColor(context, i));
            TextView textView2 = getBinding().c;
            m.checkNotNullExpressionValue(textView2, "binding.category");
            DirectoryEntryCategory selectedCategory2 = hubDescriptionState.getSelectedCategory();
            if (selectedCategory2 == null || (str = selectedCategory2.getTitle(context)) == null) {
                str = getString(R.string.categories);
            }
            textView2.setText(str);
            RestCallStateKt.handleResponse$default(hubDescriptionState.getAddServerAsync(), context, null, null, new WidgetHubDescription$configureUI$3(this), 6, null);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final RadioSelectorItems getSelectorArgs() {
        String string = requireContext().getString(R.string.categories);
        m.checkNotNullExpressionValue(string, "requireContext().getString(R.string.categories)");
        return new RadioSelectorItems(string, (List) getViewModel().withViewState(new WidgetHubDescription$selectorArgs$1(this)));
    }

    private final ValidationManager getValidationManager() {
        return (ValidationManager) this.validationManager$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void maybeAddServer() {
        if (getValidationManager().validate(true)) {
            WidgetHubDescriptionViewModel viewModel = getViewModel();
            long channelId = getArgs().getChannelId();
            long guildId = getArgs().getGuildId();
            TextInputEditText textInputEditText = getBinding().f;
            m.checkNotNullExpressionValue(textInputEditText, "binding.description");
            viewModel.addServer(channelId, guildId, String.valueOf(textInputEditText.getText()));
        }
    }

    public final HubDescriptionArgs getArgs() {
        return (HubDescriptionArgs) this.args$delegate.getValue();
    }

    public final WidgetHubDescriptionBinding getBinding() {
        return (WidgetHubDescriptionBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    public final WidgetHubDescriptionViewModel getViewModel() {
        return (WidgetHubDescriptionViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        getBinding().f.setText(getArgs().getDescription());
        getBinding().e.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.WidgetHubDescription$onViewBound$1

            /* compiled from: WidgetHubDescription.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", ModelAuditLogEntry.CHANGE_KEY_ID, "", "invoke", "(I)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.hubs.WidgetHubDescription$onViewBound$1$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<Integer, Unit> {
                public AnonymousClass1() {
                    super(1);
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                    invoke(num.intValue());
                    return Unit.a;
                }

                public final void invoke(int i) {
                    WidgetHubDescription.this.getViewModel().setCategory(i);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RadioSelectorItems selectorArgs;
                WidgetRadioSelectorBottomSheet.Companion companion = WidgetRadioSelectorBottomSheet.Companion;
                FragmentManager childFragmentManager = WidgetHubDescription.this.getChildFragmentManager();
                m.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
                selectorArgs = WidgetHubDescription.this.getSelectorArgs();
                companion.show(childFragmentManager, selectorArgs, new AnonymousClass1());
            }
        });
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null)), WidgetHubDescription.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHubDescription$onViewBoundOrOnResume$1(this));
    }
}
