package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.hubs.DirectoryEntryCategory;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.StoreChannels;
import com.discord.stores.StoreGuilds;
import com.discord.stores.updates.ObservationDeck;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import d0.z.d.a;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
/* compiled from: WidgetHubDescriptionViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 )2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001)BK\u0012\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f\u0012\n\u0010\u0012\u001a\u00060\u000ej\u0002`\u0011\u0012\u0006\u0010!\u001a\u00020 \u0012\b\u0010$\u001a\u0004\u0018\u00010\n\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u001a\u0012\u000e\b\u0002\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00020%¢\u0006\u0004\b'\u0010(J\u0019\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\f\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rJ-\u0010\u0015\u001a\u00020\u00042\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f2\n\u0010\u0012\u001a\u00060\u000ej\u0002`\u00112\u0006\u0010\u0014\u001a\u00020\u0013¢\u0006\u0004\b\u0015\u0010\u0016R\u001d\u0010\u0012\u001a\u00060\u000ej\u0002`\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001b\u001a\u00020\u001a8\u0006@\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u001d\u0010\u0010\u001a\u00060\u000ej\u0002`\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0017\u001a\u0004\b\u001f\u0010\u0019R\u0019\u0010!\u001a\u00020 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b!\u0010#¨\u0006*"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubDescriptionViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/hubs/HubDescriptionState;", "state", "", "handleStoreUpdate", "(Lcom/discord/widgets/hubs/HubDescriptionState;)Lkotlin/Unit;", "Lcom/discord/models/hubs/DirectoryEntryCategory;", "getCategory", "()Lcom/discord/models/hubs/DirectoryEntryCategory;", "", "key", "setCategory", "(I)Lkotlin/Unit;", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "", ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION, "addServer", "(JJLjava/lang/String;)V", "J", "getGuildId", "()J", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "getRestAPI", "()Lcom/discord/utilities/rest/RestAPI;", "getChannelId", "", "isEditing", "Z", "()Z", "primaryCategoryId", "Lrx/Observable;", "storeObservable", HookHelper.constructorName, "(JJZLjava/lang/Integer;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V", "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubDescriptionViewModel extends AppViewModel<HubDescriptionState> {
    public static final Companion Companion = new Companion(null);
    private final long channelId;
    private final long guildId;
    private final boolean isEditing;
    private final RestAPI restAPI;

    /* compiled from: WidgetHubDescriptionViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/hubs/HubDescriptionState;", "p1", "", "invoke", "(Lcom/discord/widgets/hubs/HubDescriptionState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.widgets.hubs.WidgetHubDescriptionViewModel$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static final /* synthetic */ class AnonymousClass1 extends a implements Function1<HubDescriptionState, Unit> {
        public AnonymousClass1(WidgetHubDescriptionViewModel widgetHubDescriptionViewModel) {
            super(1, widgetHubDescriptionViewModel, WidgetHubDescriptionViewModel.class, "handleStoreUpdate", "handleStoreUpdate(Lcom/discord/widgets/hubs/HubDescriptionState;)Lkotlin/Unit;", 8);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(HubDescriptionState hubDescriptionState) {
            invoke2(hubDescriptionState);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(HubDescriptionState hubDescriptionState) {
            m.checkNotNullParameter(hubDescriptionState, "p1");
            ((WidgetHubDescriptionViewModel) this.receiver).handleStoreUpdate(hubDescriptionState);
        }
    }

    /* compiled from: WidgetHubDescriptionViewModel.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012JC\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\n\u0010\u0006\u001a\u00060\u0002j\u0002`\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubDescriptionViewModel$Companion;", "", "", "Lcom/discord/primitives/ChannelId;", "channelId", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/stores/updates/ObservationDeck;", "observationDeck", "Lcom/discord/stores/StoreChannels;", "channelStore", "Lcom/discord/stores/StoreGuilds;", "guildStore", "Lrx/Observable;", "Lcom/discord/widgets/hubs/HubDescriptionState;", "observeStores", "(JJLcom/discord/stores/updates/ObservationDeck;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final Observable<HubDescriptionState> observeStores(long j, long j2, ObservationDeck observationDeck, StoreChannels storeChannels, StoreGuilds storeGuilds) {
            m.checkNotNullParameter(observationDeck, "observationDeck");
            m.checkNotNullParameter(storeChannels, "channelStore");
            m.checkNotNullParameter(storeGuilds, "guildStore");
            return ObservationDeck.connectRx$default(observationDeck, new ObservationDeck.UpdateSource[]{storeChannels, storeGuilds}, false, null, null, new WidgetHubDescriptionViewModel$Companion$observeStores$1(storeChannels, j, storeGuilds, j2), 14, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* JADX WARN: Illegal instructions before constructor call */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ WidgetHubDescriptionViewModel(long r18, long r20, boolean r22, java.lang.Integer r23, com.discord.utilities.rest.RestAPI r24, rx.Observable r25, int r26, kotlin.jvm.internal.DefaultConstructorMarker r27) {
        /*
            r17 = this;
            r0 = r26 & 16
            if (r0 == 0) goto Lc
            com.discord.utilities.rest.RestAPI$Companion r0 = com.discord.utilities.rest.RestAPI.Companion
            com.discord.utilities.rest.RestAPI r0 = r0.getApi()
            r8 = r0
            goto Le
        Lc:
            r8 = r24
        Le:
            r0 = r26 & 32
            if (r0 == 0) goto L2c
            com.discord.widgets.hubs.WidgetHubDescriptionViewModel$Companion r9 = com.discord.widgets.hubs.WidgetHubDescriptionViewModel.Companion
            com.discord.stores.updates.ObservationDeck r14 = com.discord.stores.updates.ObservationDeckProvider.get()
            com.discord.stores.StoreStream$Companion r0 = com.discord.stores.StoreStream.Companion
            com.discord.stores.StoreChannels r15 = r0.getChannels()
            com.discord.stores.StoreGuilds r16 = r0.getGuilds()
            r10 = r18
            r12 = r20
            rx.Observable r0 = r9.observeStores(r10, r12, r14, r15, r16)
            r9 = r0
            goto L2e
        L2c:
            r9 = r25
        L2e:
            r1 = r17
            r2 = r18
            r4 = r20
            r6 = r22
            r7 = r23
            r1.<init>(r2, r4, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.hubs.WidgetHubDescriptionViewModel.<init>(long, long, boolean, java.lang.Integer, com.discord.utilities.rest.RestAPI, rx.Observable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final Unit handleStoreUpdate(HubDescriptionState hubDescriptionState) {
        HubDescriptionState viewState = getViewState();
        if (viewState == null) {
            return null;
        }
        updateViewState(HubDescriptionState.copy$default(viewState, hubDescriptionState.getChannel(), null, hubDescriptionState.getGuildName(), null, 10, null));
        return Unit.a;
    }

    public final void addServer(long j, long j2, String str) {
        DirectoryEntryCategory selectedCategory;
        Observable<DirectoryEntryGuild> observable;
        m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
        HubDescriptionState viewState = getViewState();
        if (viewState != null && (selectedCategory = viewState.getSelectedCategory()) != null) {
            if (this.isEditing) {
                observable = this.restAPI.modifyServerInHub(j, j2, new RestAPIParams.AddServerBody(str, selectedCategory.getKey()));
            } else {
                observable = RestCallStateKt.logNetworkAction(this.restAPI.addServerToHub(j, j2, new RestAPIParams.AddServerBody(str, selectedCategory.getKey())), new WidgetHubDescriptionViewModel$addServer$1(j, j2, selectedCategory));
            }
            RestCallStateKt.executeRequest(observable, new WidgetHubDescriptionViewModel$addServer$2(this, viewState));
        }
    }

    public final DirectoryEntryCategory getCategory() {
        HubDescriptionState viewState = getViewState();
        if (viewState != null) {
            return viewState.getSelectedCategory();
        }
        return null;
    }

    public final long getChannelId() {
        return this.channelId;
    }

    public final long getGuildId() {
        return this.guildId;
    }

    public final RestAPI getRestAPI() {
        return this.restAPI;
    }

    public final boolean isEditing() {
        return this.isEditing;
    }

    public final Unit setCategory(int i) {
        HubDescriptionState viewState = getViewState();
        if (viewState == null) {
            return null;
        }
        updateViewState(HubDescriptionState.copy$default(viewState, null, Integer.valueOf(i), null, null, 13, null));
        return Unit.a;
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubDescriptionViewModel(long j, long j2, boolean z2, Integer num, RestAPI restAPI, Observable<HubDescriptionState> observable) {
        super(new HubDescriptionState(null, num, null, null, 13, null));
        m.checkNotNullParameter(restAPI, "restAPI");
        m.checkNotNullParameter(observable, "storeObservable");
        this.channelId = j;
        this.guildId = j2;
        this.isEditing = z2;
        this.restAPI = restAPI;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(ObservableExtensionsKt.computationLatest(observable), this, null, 2, null), WidgetHubDescriptionViewModel.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new AnonymousClass1(this));
    }
}
