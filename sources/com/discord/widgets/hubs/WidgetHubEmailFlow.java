package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.TextView;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.app.NotificationCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import b.a.d.f0;
import b.a.d.h0;
import b.a.i.j5;
import b.a.i.k5;
import b.a.i.z4;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.api.hubs.EmailVerification;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetHubEmailFlowBinding;
import com.discord.stores.StoreNux;
import com.discord.stores.StoreStream;
import com.discord.stores.utilities.Loading;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.stores.utilities.Success;
import com.discord.utilities.resources.StringResourceUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.discord.utilities.view.validators.ValidationManager;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.views.LoadingButton;
import com.discord.widgets.guilds.create.CreateGuildTrigger;
import com.discord.widgets.nux.WidgetGuildTemplates;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import d0.g;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import rx.Observable;
import xyz.discord.R;
/* compiled from: WidgetHubEmailFlow.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 52\u00020\u0001:\u00015B\u0007¢\u0006\u0004\b4\u0010\bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0007\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u0007\u0010\bJ!\u0010\r\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u000f\u0010\bR\u001d\u0010\u0015\u001a\u00020\u00108B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R$\u0010\u0019\u001a\u0010\u0012\f\u0012\n \u0018*\u0004\u0018\u00010\u00170\u00170\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001c\u001a\u00020\u001b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001fR\u0016\u0010#\u001a\u00020 8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\"R\u001d\u0010(\u001a\u00020$8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b%\u0010\u0012\u001a\u0004\b&\u0010'R\u001d\u0010-\u001a\u00020)8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b*\u0010\u0012\u001a\u0004\b+\u0010,R\u001d\u00103\u001a\u00020.8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\b/\u00100\u001a\u0004\b1\u00102¨\u00066"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubEmailFlow;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/hubs/HubEmailState;", "state", "", "updateView", "(Lcom/discord/widgets/hubs/HubEmailState;)V", "verifyEmail", "()V", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onViewBoundOrOnResume", "Lcom/discord/widgets/hubs/HubEmailArgs;", "args$delegate", "Lkotlin/Lazy;", "getArgs", "()Lcom/discord/widgets/hubs/HubEmailArgs;", "args", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "activityResultHandler", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "", "getEmail", "()Ljava/lang/String;", NotificationCompat.CATEGORY_EMAIL, "Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager$delegate", "getValidationManager", "()Lcom/discord/utilities/view/validators/ValidationManager;", "validationManager", "Lcom/discord/widgets/hubs/WidgetHubEmailViewModel;", "viewModel$delegate", "getViewModel", "()Lcom/discord/widgets/hubs/WidgetHubEmailViewModel;", "viewModel", "Lcom/discord/databinding/WidgetHubEmailFlowBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetHubEmailFlowBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEmailFlow extends AppFragment {
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetHubEmailFlow.class, "binding", "getBinding()Lcom/discord/databinding/WidgetHubEmailFlowBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private static final String NAME = WidgetHubEmailFlow.class.getName();
    private final ActivityResultLauncher<Intent> activityResultHandler;
    private final Lazy viewModel$delegate;
    private final Lazy args$delegate = g.lazy(new WidgetHubEmailFlow$$special$$inlined$args$1(this, "intent_args_key"));
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetHubEmailFlow$binding$2.INSTANCE, null, 2, null);
    private final Lazy validationManager$delegate = g.lazy(new WidgetHubEmailFlow$validationManager$2(this));
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, new WidgetHubEmailFlow$loggingConfig$1(this), 3);

    /* compiled from: WidgetHubEmailFlow.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tR!\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubEmailFlow$Companion;", "", "", "kotlin.jvm.PlatformType", "NAME", "Ljava/lang/String;", "getNAME", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final String getNAME() {
            return WidgetHubEmailFlow.NAME;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            HubEmailEntryPoint.values();
            int[] iArr = new int[3];
            $EnumSwitchMapping$0 = iArr;
            HubEmailEntryPoint hubEmailEntryPoint = HubEmailEntryPoint.Invite;
            iArr[hubEmailEntryPoint.ordinal()] = 1;
            HubEmailEntryPoint hubEmailEntryPoint2 = HubEmailEntryPoint.Onboarding;
            iArr[hubEmailEntryPoint2.ordinal()] = 2;
            HubEmailEntryPoint.values();
            int[] iArr2 = new int[3];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[hubEmailEntryPoint.ordinal()] = 1;
            iArr2[hubEmailEntryPoint2.ordinal()] = 2;
        }
    }

    public WidgetHubEmailFlow() {
        super(R.layout.widget_hub_email_flow);
        ActivityResultLauncher<Intent> registerForActivityResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.hubs.WidgetHubEmailFlow$activityResultHandler$1
            public final void onActivityResult(ActivityResult activityResult) {
                Intent data;
                WidgetHubEmailViewModel viewModel;
                if (activityResult != null && (data = activityResult.getData()) != null) {
                    if (!(activityResult.getResultCode() == -1)) {
                        data = null;
                    }
                    if (data != null) {
                        m.checkNotNullParameter(data, "$this$getArgsFromIntent");
                        MultipleDomainResult multipleDomainResult = (MultipleDomainResult) data.getParcelableExtra("intent_args_key");
                        if (multipleDomainResult == null) {
                            return;
                        }
                        if (multipleDomainResult instanceof HubWaitlistResult) {
                            viewModel = WidgetHubEmailFlow.this.getViewModel();
                            viewModel.setHubWaitlistResult((HubWaitlistResult) multipleDomainResult);
                        } else if (multipleDomainResult instanceof AuthenticationResult) {
                            StoreStream.Companion.getGuildSelected().set(((AuthenticationResult) multipleDomainResult).getGuildId());
                            FragmentActivity activity = WidgetHubEmailFlow.this.e();
                            if (activity != null) {
                                activity.finish();
                            }
                        } else {
                            FragmentActivity activity2 = WidgetHubEmailFlow.this.e();
                            if (activity2 != null) {
                                activity2.finish();
                            }
                        }
                    }
                }
            }
        });
        m.checkNotNullExpressionValue(registerForActivityResult, "registerForActivityResul…  }\n            }\n      }");
        this.activityResultHandler = registerForActivityResult;
        WidgetHubEmailFlow$viewModel$2 widgetHubEmailFlow$viewModel$2 = WidgetHubEmailFlow$viewModel$2.INSTANCE;
        f0 f0Var = new f0(this);
        this.viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetHubEmailViewModel.class), new WidgetHubEmailFlow$appViewModels$$inlined$viewModels$1(f0Var), new h0(widgetHubEmailFlow$viewModel$2));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final HubEmailArgs getArgs() {
        return (HubEmailArgs) this.args$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetHubEmailFlowBinding getBinding() {
        return (WidgetHubEmailFlowBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    private final String getEmail() {
        TextInputEditText textInputEditText = getBinding().c.c;
        m.checkNotNullExpressionValue(textInputEditText, "binding.discordHubEmailI…aderDescriptionEmailInput");
        return String.valueOf(textInputEditText.getText());
    }

    private final ValidationManager getValidationManager() {
        return (ValidationManager) this.validationManager$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final WidgetHubEmailViewModel getViewModel() {
        return (WidgetHubEmailViewModel) this.viewModel$delegate.getValue();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void updateView(HubEmailState hubEmailState) {
        CharSequence e;
        View.OnClickListener onClickListener;
        String str;
        String school;
        CharSequence e2;
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return");
            EmailVerification invoke = hubEmailState.getVerifyEmailAsync().invoke();
            int i = 0;
            boolean z2 = invoke != null && invoke.b();
            boolean z3 = !(hubEmailState.getVerifyEmailAsync() instanceof Success) && hubEmailState.getWaitlistResult() == null;
            EmailVerification invoke2 = hubEmailState.getVerifyEmailAsync().invoke();
            boolean z4 = (invoke2 != null && !invoke2.b()) || hubEmailState.getWaitlistResult() != null;
            TextInputEditText textInputEditText = getBinding().c.c;
            m.checkNotNullExpressionValue(textInputEditText, "binding.discordHubEmailI…aderDescriptionEmailInput");
            String valueOf = String.valueOf(textInputEditText.getText());
            z4 z4Var = getBinding().c;
            m.checkNotNullExpressionValue(z4Var, "binding.discordHubEmailInput");
            NestedScrollView nestedScrollView = z4Var.a;
            m.checkNotNullExpressionValue(nestedScrollView, "binding.discordHubEmailInput.root");
            nestedScrollView.setVisibility(z3 ? 0 : 8);
            MaterialButton materialButton = getBinding().d;
            m.checkNotNullExpressionValue(materialButton, "binding.discordHubEmailNo");
            materialButton.setVisibility(z3 ? 0 : 8);
            j5 j5Var = getBinding().f2450b;
            m.checkNotNullExpressionValue(j5Var, "binding.discordHubEmailConfirmation");
            NestedScrollView nestedScrollView2 = j5Var.a;
            m.checkNotNullExpressionValue(nestedScrollView2, "binding.discordHubEmailConfirmation.root");
            nestedScrollView2.setVisibility(z2 ? 0 : 8);
            TextView textView = getBinding().f2450b.c;
            m.checkNotNullExpressionValue(textView, "binding.discordHubEmailC…ilConfirmationHeaderTitle");
            e = b.e(this, R.string.member_verification_email_confirmation_title, new Object[]{valueOf}, (r4 & 4) != 0 ? b.a.j : null);
            textView.setText(e);
            k5 k5Var = getBinding().f;
            m.checkNotNullExpressionValue(k5Var, "binding.discordHubWaitlist");
            NestedScrollView nestedScrollView3 = k5Var.a;
            m.checkNotNullExpressionValue(nestedScrollView3, "binding.discordHubWaitlist.root");
            nestedScrollView3.setVisibility(z4 ? 0 : 8);
            HubWaitlistResult waitlistResult = hubEmailState.getWaitlistResult();
            if (!(waitlistResult == null || (school = waitlistResult.getSchool()) == null)) {
                TextView textView2 = getBinding().f.f146b;
                m.checkNotNullExpressionValue(textView2, "binding.discordHubWaitlist.hubWaitlistDescription");
                e2 = b.e(this, R.string.hub_waitlist_modal_joined_description, new Object[]{school}, (r4 & 4) != 0 ? b.a.j : null);
                textView2.setText(e2);
            }
            LoadingButton loadingButton = getBinding().e;
            if (z4) {
                onClickListener = new View.OnClickListener() { // from class: com.discord.widgets.hubs.WidgetHubEmailFlow$updateView$2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        FragmentActivity activity = WidgetHubEmailFlow.this.e();
                        if (activity != null) {
                            activity.finish();
                        }
                    }
                };
            } else {
                onClickListener = new View.OnClickListener() { // from class: com.discord.widgets.hubs.WidgetHubEmailFlow$updateView$3
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WidgetHubEmailFlow.this.verifyEmail();
                    }
                };
            }
            loadingButton.setOnClickListener(onClickListener);
            LoadingButton loadingButton2 = getBinding().e;
            m.checkNotNullExpressionValue(loadingButton2, "binding.discordHubEmailYes");
            if (!(!z2)) {
                i = 8;
            }
            loadingButton2.setVisibility(i);
            LoadingButton loadingButton3 = getBinding().e;
            if (z4) {
                str = getString(R.string.hub_add_server_confirmation_button);
            } else {
                str = getString(R.string.hub_email_connection_content_button);
            }
            loadingButton3.setText(str);
            getBinding().e.setIsLoading(hubEmailState.getVerifyEmailAsync() instanceof Loading);
            RestCallStateKt.handleResponse$default(hubEmailState.getVerifyEmailAsync(), context, null, null, new WidgetHubEmailFlow$updateView$4(this, hubEmailState, context, valueOf), 6, null);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void verifyEmail() {
        int i = 0;
        boolean validate$default = ValidationManager.validate$default(getValidationManager(), false, 1, null);
        TextView textView = getBinding().c.f;
        m.checkNotNullExpressionValue(textView, "binding.discordHubEmailInput.discordHubEmailLabel");
        if (!validate$default) {
            i = 8;
        }
        textView.setVisibility(i);
        if (validate$default) {
            getViewModel().submitEmail(getEmail());
            hideKeyboard(getView());
        }
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        Observable<HubEmailState> q = getViewModel().observeViewState().q();
        m.checkNotNullExpressionValue(q, "viewModel\n        .obser…  .distinctUntilChanged()");
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(q, this, null, 2, null), WidgetHubEmailFlow.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHubEmailFlow$onViewBoundOrOnResume$1(this));
    }

    @Override // com.discord.app.AppFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        CharSequence charSequence;
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        getBinding().f2450b.d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.WidgetHubEmailFlow$onViewCreated$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetHubEmailFlow.this.verifyEmail();
            }
        });
        getBinding().d.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.WidgetHubEmailFlow$onViewCreated$2

            /* compiled from: WidgetHubEmailFlow.kt */
            @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0018\u0002\n\u0002\b\u0004\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/stores/StoreNux$NuxState;", "it", "invoke", "(Lcom/discord/stores/StoreNux$NuxState;)Lcom/discord/stores/StoreNux$NuxState;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
            /* renamed from: com.discord.widgets.hubs.WidgetHubEmailFlow$onViewCreated$2$1  reason: invalid class name */
            /* loaded from: classes2.dex */
            public static final class AnonymousClass1 extends o implements Function1<StoreNux.NuxState, StoreNux.NuxState> {
                public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

                public AnonymousClass1() {
                    super(1);
                }

                public final StoreNux.NuxState invoke(StoreNux.NuxState nuxState) {
                    m.checkNotNullParameter(nuxState, "it");
                    return StoreNux.NuxState.copy$default(nuxState, false, false, false, false, false, null, 62, null);
                }
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                HubEmailArgs args;
                FragmentActivity activity = WidgetHubEmailFlow.this.e();
                if (activity != null) {
                    m.checkNotNullExpressionValue(activity, "activity ?: return@setOnClickListener");
                    args = WidgetHubEmailFlow.this.getArgs();
                    if (args.getEntryPoint() == HubEmailEntryPoint.Onboarding) {
                        StoreStream.Companion.getNux().updateNux(AnonymousClass1.INSTANCE);
                        WidgetGuildTemplates.Companion.launch(activity, CreateGuildTrigger.NUF, true);
                    }
                    activity.finish();
                }
            }
        });
        getBinding().f2450b.f139b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.WidgetHubEmailFlow$onViewCreated$3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                WidgetHubEmailViewModel viewModel;
                viewModel = WidgetHubEmailFlow.this.getViewModel();
                viewModel.reset();
            }
        });
        TextView textView = getBinding().c.e;
        m.checkNotNullExpressionValue(textView, "binding.discordHubEmailI…iscordHubEmailHeaderTitle");
        int ordinal = getArgs().getEntryPoint().ordinal();
        if (ordinal == 0) {
            charSequence = getString(R.string.hub_email_connection_nux_header);
        } else if (ordinal != 1) {
            charSequence = getString(R.string.hub_waitlist_modal_join_header);
        } else {
            Context context = view.getContext();
            m.checkNotNullExpressionValue(context, "view.context");
            charSequence = b.e(this, R.string.hub_email_connection_invite_header, new Object[]{StringResourceUtilsKt.getI18nPluralString(context, R.plurals.hub_email_connection_invite_header_count, getArgs().getGuildMemberCount(), Integer.valueOf(getArgs().getGuildMemberCount())), getArgs().getGuildName()}, (r4 & 4) != 0 ? b.a.j : null);
        }
        textView.setText(charSequence);
        int ordinal2 = getArgs().getEntryPoint().ordinal();
        String string = getString(ordinal2 != 0 ? ordinal2 != 1 ? R.string.hub_waitlist_modal_join_subheader : R.string.hub_email_connection_content_description : R.string.hub_email_connection_nux_content_description);
        m.checkNotNullExpressionValue(string, "getString(\n        when …subheader\n        }\n    )");
        CharSequence e = b.e(this, R.string.learn_more_link, new Object[0], new WidgetHubEmailFlow$onViewCreated$learnMore$1(this));
        LinkifiedTextView linkifiedTextView = getBinding().c.f237b;
        m.checkNotNullExpressionValue(linkifiedTextView, "binding.discordHubEmailI…HubEmailHeaderDescription");
        linkifiedTextView.setText(new SpannableStringBuilder(string).append((CharSequence) " ").append(e));
    }
}
