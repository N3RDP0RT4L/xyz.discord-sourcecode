package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import android.view.View;
import com.discord.databinding.WidgetRadioSelectorViewHolderBinding;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetRadioSelectorBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B#\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\f¢\u0006\u0004\b\u0012\u0010\u0013J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u00020\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR%\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/widgets/hubs/RadioSelectorViewHolder;", "Lcom/discord/utilities/views/SimpleRecyclerAdapter$ViewHolder;", "Lcom/discord/widgets/hubs/RadioSelectorItem;", "data", "", "bind", "(Lcom/discord/widgets/hubs/RadioSelectorItem;)V", "Lcom/discord/databinding/WidgetRadioSelectorViewHolderBinding;", "binding", "Lcom/discord/databinding/WidgetRadioSelectorViewHolderBinding;", "getBinding", "()Lcom/discord/databinding/WidgetRadioSelectorViewHolderBinding;", "Lkotlin/Function1;", "", "onSelected", "Lkotlin/jvm/functions/Function1;", "getOnSelected", "()Lkotlin/jvm/functions/Function1;", HookHelper.constructorName, "(Lcom/discord/databinding/WidgetRadioSelectorViewHolderBinding;Lkotlin/jvm/functions/Function1;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class RadioSelectorViewHolder extends SimpleRecyclerAdapter.ViewHolder<RadioSelectorItem> {
    private final WidgetRadioSelectorViewHolderBinding binding;
    private final Function1<Integer, Unit> onSelected;

    /* JADX WARN: Illegal instructions before constructor call */
    /* JADX WARN: Multi-variable type inference failed */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public RadioSelectorViewHolder(com.discord.databinding.WidgetRadioSelectorViewHolderBinding r3, kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> r4) {
        /*
            r2 = this;
            java.lang.String r0 = "binding"
            d0.z.d.m.checkNotNullParameter(r3, r0)
            java.lang.String r0 = "onSelected"
            d0.z.d.m.checkNotNullParameter(r4, r0)
            com.discord.views.IconRow r0 = r3.a
            java.lang.String r1 = "binding.root"
            d0.z.d.m.checkNotNullExpressionValue(r0, r1)
            r2.<init>(r0)
            r2.binding = r3
            r2.onSelected = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.widgets.hubs.RadioSelectorViewHolder.<init>(com.discord.databinding.WidgetRadioSelectorViewHolderBinding, kotlin.jvm.functions.Function1):void");
    }

    public final WidgetRadioSelectorViewHolderBinding getBinding() {
        return this.binding;
    }

    public final Function1<Integer, Unit> getOnSelected() {
        return this.onSelected;
    }

    public void bind(final RadioSelectorItem radioSelectorItem) {
        m.checkNotNullParameter(radioSelectorItem, "data");
        this.binding.f2495b.setText(radioSelectorItem.getText());
        this.binding.f2495b.setImageRes(Integer.valueOf(radioSelectorItem.getSelected() ? R.drawable.ic_check_brand_16dp : 0));
        this.binding.f2495b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.RadioSelectorViewHolder$bind$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RadioSelectorViewHolder.this.getOnSelected().invoke(Integer.valueOf(radioSelectorItem.getId()));
            }
        });
    }
}
