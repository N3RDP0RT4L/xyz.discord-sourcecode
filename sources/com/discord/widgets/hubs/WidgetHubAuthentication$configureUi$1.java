package com.discord.widgets.hubs;

import androidx.fragment.app.FragmentActivity;
import b.c.a.a0.d;
import com.discord.api.hubs.EmailVerificationCode;
import com.discord.stores.StoreStream;
import com.discord.stores.utilities.Success;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import xyz.discord.R;
/* compiled from: WidgetHubAuthentication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/stores/utilities/Success;", "Lcom/discord/api/hubs/EmailVerificationCode;", "response", "", "invoke", "(Lcom/discord/stores/utilities/Success;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubAuthentication$configureUi$1 extends o implements Function1<Success<? extends EmailVerificationCode>, Unit> {
    public final /* synthetic */ WidgetHubAuthentication this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubAuthentication$configureUi$1(WidgetHubAuthentication widgetHubAuthentication) {
        super(1);
        this.this$0 = widgetHubAuthentication;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Success<? extends EmailVerificationCode> success) {
        invoke2((Success<EmailVerificationCode>) success);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Success<EmailVerificationCode> success) {
        m.checkNotNullParameter(success, "response");
        WidgetHubAuthentication widgetHubAuthentication = this.this$0;
        widgetHubAuthentication.hideKeyboard(widgetHubAuthentication.getView());
        EmailVerificationCode invoke = success.invoke();
        if (invoke.b()) {
            FragmentActivity activity = this.this$0.e();
            if (activity != null) {
                activity.setResult(-1, d.g2(new AuthenticationResult(invoke.a().r())));
                activity.finish();
            }
            StoreStream.Companion.getNux().updateNux(WidgetHubAuthentication$configureUi$1$1$2.INSTANCE);
            return;
        }
        b.a.d.m.i(this.this$0, R.string.error_generic_title, 0, 4);
    }
}
