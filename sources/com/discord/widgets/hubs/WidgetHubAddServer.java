package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import b.a.d.e0;
import b.a.d.j;
import b.a.k.b;
import b.d.b.a.a;
import com.discord.app.AppFragment;
import com.discord.app.LoggingConfig;
import com.discord.databinding.WidgetHubAddServerBinding;
import com.discord.models.guild.Guild;
import com.discord.utilities.hubs.HubUtilsKt;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegate;
import com.discord.utilities.viewbinding.FragmentViewBindingDelegateKt;
import com.discord.utilities.views.SimpleRecyclerAdapter;
import com.discord.views.ScreenTitleView;
import com.discord.views.segmentedcontrol.SegmentedControlContainer;
import com.discord.widgets.guilds.create.CreateGuildTrigger;
import com.discord.widgets.guilds.create.WidgetGuildCreate;
import com.discord.widgets.nux.GuildCreateArgs;
import com.discord.widgets.nux.GuildTemplateAnalytics;
import com.discord.widgets.nux.GuildTemplateArgs;
import com.discord.widgets.nux.GuildTemplateTypes;
import com.discord.widgets.nux.WidgetHubGuildTemplates;
import d0.z.d.a0;
import d0.z.d.m;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.reflect.KProperty;
import xyz.discord.R;
/* compiled from: WidgetHubAddServer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 22\u00020\u0001:\u00012B\u0007¢\u0006\u0004\b1\u0010\u0013J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J%\u0010\f\u001a\u00020\u00042\n\u0010\t\u001a\u00060\u0007j\u0002`\b2\b\b\u0002\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\f\u0010\rJ\u0017\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0004H\u0016¢\u0006\u0004\b\u0012\u0010\u0013R$\u0010\u0017\u001a\u0010\u0012\f\u0012\n \u0016*\u0004\u0018\u00010\u00150\u00150\u00148\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u001d\u0010\u001e\u001a\u00020\u00198F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001dR\u001c\u0010 \u001a\u00020\u001f8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R%\u0010'\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020&0$8\u0006@\u0006¢\u0006\f\n\u0004\b'\u0010(\u001a\u0004\b)\u0010*R\u001d\u00100\u001a\u00020+8F@\u0006X\u0086\u0084\u0002¢\u0006\f\n\u0004\b,\u0010-\u001a\u0004\b.\u0010/¨\u00063"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubAddServer;", "Lcom/discord/app/AppFragment;", "Lcom/discord/widgets/hubs/HubAddServerState;", "state", "", "configureUI", "(Lcom/discord/widgets/hubs/HubAddServerState;)V", "", "Lcom/discord/primitives/GuildId;", "guildId", "", "isNewGuild", "onServerClickListener", "(JZ)V", "Landroid/view/View;", "view", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "()V", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "launcher", "Landroidx/activity/result/ActivityResultLauncher;", "Lcom/discord/widgets/hubs/WidgetHubAddServerViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lcom/discord/widgets/hubs/WidgetHubAddServerViewModel;", "viewModel", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/models/guild/Guild;", "Lcom/discord/widgets/hubs/DiscordHubAddServerViewHolder;", "adapter", "Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "getAdapter", "()Lcom/discord/utilities/views/SimpleRecyclerAdapter;", "Lcom/discord/databinding/WidgetHubAddServerBinding;", "binding$delegate", "Lcom/discord/utilities/viewbinding/FragmentViewBindingDelegate;", "getBinding", "()Lcom/discord/databinding/WidgetHubAddServerBinding;", "binding", HookHelper.constructorName, "Companion", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubAddServer extends AppFragment {
    private static final int CHOOSE_GUILD = 0;
    private static final int GUILDS_ADDED = 1;
    private final ActivityResultLauncher<Intent> launcher;
    public static final /* synthetic */ KProperty[] $$delegatedProperties = {a.b0(WidgetHubAddServer.class, "binding", "getBinding()Lcom/discord/databinding/WidgetHubAddServerBinding;", 0)};
    public static final Companion Companion = new Companion(null);
    private final FragmentViewBindingDelegate binding$delegate = FragmentViewBindingDelegateKt.viewBinding$default(this, WidgetHubAddServer$binding$2.INSTANCE, null, 2, null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, a0.getOrCreateKotlinClass(WidgetHubAddServerViewModel.class), new WidgetHubAddServer$appActivityViewModels$$inlined$activityViewModels$1(this), new e0(WidgetHubAddServer$viewModel$2.INSTANCE));
    private final SimpleRecyclerAdapter<Guild, DiscordHubAddServerViewHolder> adapter = new SimpleRecyclerAdapter<>(null, new WidgetHubAddServer$adapter$1(this), 1, null);
    private final LoggingConfig loggingConfig = new LoggingConfig(false, null, WidgetHubAddServer$loggingConfig$1.INSTANCE, 3);

    /* compiled from: WidgetHubAddServer.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\n\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubAddServer$Companion;", "", "Lcom/discord/app/AppFragment;", "fragment", "", "startScreenForResult", "(Lcom/discord/app/AppFragment;)V", "", "CHOOSE_GUILD", "I", "GUILDS_ADDED", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes2.dex */
    public static final class Companion {
        private Companion() {
        }

        public final void startScreenForResult(AppFragment appFragment) {
            m.checkNotNullParameter(appFragment, "fragment");
            j.g.f(appFragment.requireContext(), HubUtilsKt.getAddServerActivityResultHandler(appFragment), WidgetHubAddServer.class, null);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public WidgetHubAddServer() {
        super(R.layout.widget_hub_add_server);
        ActivityResultLauncher<Intent> registerForActivityResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() { // from class: com.discord.widgets.hubs.WidgetHubAddServer$launcher$1
            public final void onActivityResult(ActivityResult activityResult) {
                WidgetGuildCreate.Result result;
                m.checkNotNullExpressionValue(activityResult, "activityResult");
                Intent data = activityResult.getData();
                if (data != null) {
                    if (!(activityResult.getResultCode() == -1)) {
                        data = null;
                    }
                    if (data != null && (result = (WidgetGuildCreate.Result) data.getParcelableExtra("intent_args_key")) != null) {
                        WidgetHubAddServer.this.onServerClickListener(result.getGuildId(), true);
                    }
                }
            }
        });
        m.checkNotNullExpressionValue(registerForActivityResult, "registerForActivityResul… isNewGuild = true) }\n  }");
        this.launcher = registerForActivityResult;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void configureUI(final HubAddServerState hubAddServerState) {
        List<Guild> list;
        CharSequence e;
        SimpleRecyclerAdapter<Guild, DiscordHubAddServerViewHolder> simpleRecyclerAdapter = this.adapter;
        int selectedIndex = hubAddServerState.getSelectedIndex();
        if (selectedIndex == 0) {
            list = hubAddServerState.getSelectableGuilds();
        } else if (selectedIndex == 1) {
            list = hubAddServerState.getAddedGuilds();
        } else {
            return;
        }
        simpleRecyclerAdapter.setData(list);
        getBinding().e.setSelectedIndex(hubAddServerState.getSelectedIndex());
        ScreenTitleView screenTitleView = getBinding().c;
        e = b.e(this, R.string.hub_choose_guild_title, new Object[]{hubAddServerState.getHubName()}, (r4 & 4) != 0 ? b.a.j : null);
        screenTitleView.setTitle(e);
        getBinding().f2445b.setOnClickListener(new View.OnClickListener() { // from class: com.discord.widgets.hubs.WidgetHubAddServer$configureUI$1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ActivityResultLauncher<Intent> activityResultLauncher;
                CharSequence b2;
                Context context = WidgetHubAddServer.this.getContext();
                if (context != null) {
                    m.checkNotNullExpressionValue(context, "context ?: return@setOnClickListener");
                    j jVar = j.g;
                    activityResultLauncher = WidgetHubAddServer.this.launcher;
                    CreateGuildTrigger createGuildTrigger = CreateGuildTrigger.DIRECTORY_CHANNEL;
                    b2 = b.b(context, R.string.hub_create_or_add_guild_title, new Object[]{hubAddServerState.getHubName()}, (r4 & 4) != 0 ? b.C0034b.j : null);
                    String obj = b2.toString();
                    String string = context.getString(R.string.hub_create_or_add_guild_subtitle);
                    m.checkNotNullExpressionValue(string, "context.getString(R.stri…te_or_add_guild_subtitle)");
                    jVar.f(context, activityResultLauncher, WidgetHubGuildTemplates.class, new GuildCreateArgs(false, GuildTemplateAnalytics.IN_APP_LOCATION_TEMPLATE, createGuildTrigger, new GuildTemplateArgs(obj, string, GuildTemplateTypes.INSTANCE.getHUB(), false, true), true));
                }
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void onServerClickListener(long j, boolean z2) {
        Context context = getContext();
        if (context != null) {
            m.checkNotNullExpressionValue(context, "context ?: return");
            Long channelId = getViewModel().getChannelId();
            if (channelId != null) {
                getViewModel().withViewState(new WidgetHubAddServer$onServerClickListener$1(this, j, context, channelId.longValue(), z2));
            }
        }
    }

    public static /* synthetic */ void onServerClickListener$default(WidgetHubAddServer widgetHubAddServer, long j, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = false;
        }
        widgetHubAddServer.onServerClickListener(j, z2);
    }

    public final SimpleRecyclerAdapter<Guild, DiscordHubAddServerViewHolder> getAdapter() {
        return this.adapter;
    }

    public final WidgetHubAddServerBinding getBinding() {
        return (WidgetHubAddServerBinding) this.binding$delegate.getValue((Fragment) this, $$delegatedProperties[0]);
    }

    @Override // com.discord.app.AppFragment, com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    public final WidgetHubAddServerViewModel getViewModel() {
        return (WidgetHubAddServerViewModel) this.viewModel$delegate.getValue();
    }

    @Override // com.discord.app.AppFragment
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
        super.onViewBound(view);
        RecyclerView recyclerView = getBinding().d;
        recyclerView.setAdapter(this.adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        getBinding().g.setText(getString(R.string.hub_choose_guild_choose_tab));
        getBinding().f.setText(getString(R.string.hub_choose_guild_added_tab));
        SegmentedControlContainer segmentedControlContainer = getBinding().e;
        SegmentedControlContainer.b(segmentedControlContainer, 0, 1);
        segmentedControlContainer.setOnSegmentSelectedChangeListener(new WidgetHubAddServer$onViewBound$$inlined$apply$lambda$1(this));
    }

    @Override // com.discord.app.AppFragment
    public void onViewBoundOrOnResume() {
        super.onViewBoundOrOnResume();
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.bindToComponentLifecycle$default(getViewModel().observeViewState(), this, null, 2, null), WidgetHubAddServer.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new WidgetHubAddServer$onViewBoundOrOnResume$1(this));
    }
}
