package com.discord.widgets.hubs;

import com.discord.api.channel.Channel;
import com.discord.api.channel.ChannelUtils;
import com.discord.models.hubs.DirectoryEntryCategory;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHubDescription.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/hubs/HubDescriptionState;", "state", "", "Lcom/discord/widgets/hubs/RadioSelectorItem;", "invoke", "(Lcom/discord/widgets/hubs/HubDescriptionState;)Ljava/util/List;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubDescription$selectorArgs$1 extends o implements Function1<HubDescriptionState, List<? extends RadioSelectorItem>> {
    public final /* synthetic */ WidgetHubDescription this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubDescription$selectorArgs$1(WidgetHubDescription widgetHubDescription) {
        super(1);
        this.this$0 = widgetHubDescription;
    }

    public final List<RadioSelectorItem> invoke(HubDescriptionState hubDescriptionState) {
        m.checkNotNullParameter(hubDescriptionState, "state");
        DirectoryEntryCategory.Companion companion = DirectoryEntryCategory.Companion;
        Channel channel = hubDescriptionState.getChannel();
        List<DirectoryEntryCategory> categories = companion.getCategories(channel != null && ChannelUtils.u(channel));
        ArrayList arrayList = new ArrayList(d0.t.o.collectionSizeOrDefault(categories, 10));
        for (DirectoryEntryCategory directoryEntryCategory : categories) {
            int key = directoryEntryCategory.getKey();
            DirectoryEntryCategory category = this.this$0.getViewModel().getCategory();
            arrayList.add(new RadioSelectorItem(key, directoryEntryCategory.getTitle(this.this$0.requireContext()), category != null && category.getKey() == directoryEntryCategory.getKey()));
        }
        return arrayList;
    }
}
