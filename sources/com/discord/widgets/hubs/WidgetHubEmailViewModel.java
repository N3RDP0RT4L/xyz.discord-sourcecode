package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import androidx.core.app.NotificationCompat;
import com.discord.app.AppViewModel;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.restapi.RestAPIParams;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallStateKt;
import com.discord.utilities.features.GrowthTeamFeatures;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubEmailViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001b\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e¢\u0006\u0004\b\u0018\u0010\u0019J\u0015\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\r\u0010\b\u001a\u00020\u0005¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\f\u0010\rR\u0019\u0010\u000f\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0014\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lcom/discord/widgets/hubs/WidgetHubEmailViewModel;", "Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/hubs/HubEmailState;", "", NotificationCompat.CATEGORY_EMAIL, "", "submitEmail", "(Ljava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "()V", "Lcom/discord/widgets/hubs/HubWaitlistResult;", "waitlistResult", "setHubWaitlistResult", "(Lcom/discord/widgets/hubs/HubWaitlistResult;)V", "", "multiDomainEnabled", "Z", "getMultiDomainEnabled", "()Z", "Lcom/discord/utilities/rest/RestAPI;", "restAPI", "Lcom/discord/utilities/rest/RestAPI;", "getRestAPI", "()Lcom/discord/utilities/rest/RestAPI;", HookHelper.constructorName, "(Lcom/discord/utilities/rest/RestAPI;Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubEmailViewModel extends AppViewModel<HubEmailState> {
    private final boolean multiDomainEnabled;
    private final RestAPI restAPI;

    public WidgetHubEmailViewModel() {
        this(null, false, 3, null);
    }

    public /* synthetic */ WidgetHubEmailViewModel(RestAPI restAPI, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? RestAPI.Companion.getApi() : restAPI, (i & 2) != 0 ? GrowthTeamFeatures.INSTANCE.isMultiDomainEnabled() : z2);
    }

    public final boolean getMultiDomainEnabled() {
        return this.multiDomainEnabled;
    }

    public final RestAPI getRestAPI() {
        return this.restAPI;
    }

    public final void reset() {
        HubEmailState viewState = getViewState();
        m.checkNotNull(viewState);
        updateViewState(HubEmailState.copy$default(viewState, null, null, null, Default.INSTANCE, 5, null));
    }

    public final void setHubWaitlistResult(HubWaitlistResult hubWaitlistResult) {
        m.checkNotNullParameter(hubWaitlistResult, "waitlistResult");
        HubEmailState viewState = getViewState();
        m.checkNotNull(viewState);
        updateViewState(HubEmailState.copy$default(viewState, null, null, hubWaitlistResult, null, 11, null));
    }

    public final void submitEmail(String str) {
        m.checkNotNullParameter(str, NotificationCompat.CATEGORY_EMAIL);
        RestCallStateKt.executeRequest(RestCallStateKt.logNetworkAction(ObservableExtensionsKt.ui$default(this.restAPI.verifyEmail(new RestAPIParams.VerifyEmail(str, null, this.multiDomainEnabled, false, 10, null)), this, null, 2, null), WidgetHubEmailViewModel$submitEmail$1.INSTANCE), new WidgetHubEmailViewModel$submitEmail$2(this));
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubEmailViewModel(RestAPI restAPI, boolean z2) {
        super(new HubEmailState(null, null, null, null, 15, null));
        m.checkNotNullParameter(restAPI, "restAPI");
        this.restAPI = restAPI;
        this.multiDomainEnabled = z2;
    }
}
