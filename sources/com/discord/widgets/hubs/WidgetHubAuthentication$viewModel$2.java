package com.discord.widgets.hubs;

import com.discord.app.AppViewModel;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: WidgetHubAuthentication.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0002\u0010\u0003"}, d2 = {"Lcom/discord/app/AppViewModel;", "Lcom/discord/widgets/hubs/HubAuthenticationState;", "invoke", "()Lcom/discord/app/AppViewModel;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubAuthentication$viewModel$2 extends o implements Function0<AppViewModel<HubAuthenticationState>> {
    public final /* synthetic */ WidgetHubAuthentication this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubAuthentication$viewModel$2(WidgetHubAuthentication widgetHubAuthentication) {
        super(0);
        this.this$0 = widgetHubAuthentication;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // kotlin.jvm.functions.Function0
    public final AppViewModel<HubAuthenticationState> invoke() {
        return new WidgetHubAuthenticationViewModel(this.this$0.getArgs().getEmail(), this.this$0.getArgs().getGuildId(), null, false, 12, null);
    }
}
