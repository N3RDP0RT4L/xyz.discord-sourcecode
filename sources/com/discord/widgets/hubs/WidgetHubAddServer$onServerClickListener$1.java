package com.discord.widgets.hubs;

import android.content.Context;
import b.a.d.j;
import com.discord.api.directory.DirectoryEntryGuild;
import com.discord.utilities.directories.DirectoryUtils;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHubAddServer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/widgets/hubs/HubAddServerState;", "state", "", "invoke", "(Lcom/discord/widgets/hubs/HubAddServerState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubAddServer$onServerClickListener$1 extends o implements Function1<HubAddServerState, Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ Context $context;
    public final /* synthetic */ long $guildId;
    public final /* synthetic */ boolean $isNewGuild;
    public final /* synthetic */ WidgetHubAddServer this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubAddServer$onServerClickListener$1(WidgetHubAddServer widgetHubAddServer, long j, Context context, long j2, boolean z2) {
        super(1);
        this.this$0 = widgetHubAddServer;
        this.$guildId = j;
        this.$context = context;
        this.$channelId = j2;
        this.$isNewGuild = z2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(HubAddServerState hubAddServerState) {
        invoke2(hubAddServerState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(HubAddServerState hubAddServerState) {
        Object obj;
        boolean z2;
        m.checkNotNullParameter(hubAddServerState, "state");
        Iterator<T> it = hubAddServerState.getAddedDirectories().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((DirectoryEntryGuild) obj).e().h() == this.$guildId) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        DirectoryEntryGuild directoryEntryGuild = (DirectoryEntryGuild) obj;
        if (directoryEntryGuild != null) {
            DirectoryUtils.INSTANCE.showServerOptions(this.this$0, directoryEntryGuild, hubAddServerState.getHubName(), true, new WidgetHubAddServer$onServerClickListener$1$$special$$inlined$let$lambda$1(directoryEntryGuild, this, hubAddServerState));
            return;
        }
        WidgetHubAddServer widgetHubAddServer = this.this$0;
        j.g(j.g, widgetHubAddServer.getParentFragmentManager(), this.$context, WidgetHubDescription.class, 0, true, null, new HubDescriptionArgs(this.$guildId, this.$channelId, false, widgetHubAddServer.getViewModel().getHubName(), this.$isNewGuild, null, null, 96, null), 40);
    }
}
