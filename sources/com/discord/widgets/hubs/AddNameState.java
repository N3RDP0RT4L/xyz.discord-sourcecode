package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.models.guild.Guild;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallState;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubAddNameViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B/\u0012\b\b\u0002\u0010\f\u001a\u00020\u0002\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\t0\b¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\t0\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ8\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00052\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\t0\bHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R!\u0010\u000e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\t0\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0019\u001a\u0004\b\u001a\u0010\u000bR\u001b\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001e\u0010\u0004¨\u0006!"}, d2 = {"Lcom/discord/widgets/hubs/AddNameState;", "", "", "component1", "()Ljava/lang/String;", "Lcom/discord/models/guild/Guild;", "component2", "()Lcom/discord/models/guild/Guild;", "Lcom/discord/stores/utilities/RestCallState;", "Ljava/lang/Void;", "component3", "()Lcom/discord/stores/utilities/RestCallState;", "nickname", "guild", "changeNicknameAsync", "copy", "(Ljava/lang/String;Lcom/discord/models/guild/Guild;Lcom/discord/stores/utilities/RestCallState;)Lcom/discord/widgets/hubs/AddNameState;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/stores/utilities/RestCallState;", "getChangeNicknameAsync", "Lcom/discord/models/guild/Guild;", "getGuild", "Ljava/lang/String;", "getNickname", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/models/guild/Guild;Lcom/discord/stores/utilities/RestCallState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class AddNameState {
    private final RestCallState<Void> changeNicknameAsync;
    private final Guild guild;
    private final String nickname;

    public AddNameState() {
        this(null, null, null, 7, null);
    }

    public AddNameState(String str, Guild guild, RestCallState<Void> restCallState) {
        m.checkNotNullParameter(str, "nickname");
        m.checkNotNullParameter(restCallState, "changeNicknameAsync");
        this.nickname = str;
        this.guild = guild;
        this.changeNicknameAsync = restCallState;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ AddNameState copy$default(AddNameState addNameState, String str, Guild guild, RestCallState restCallState, int i, Object obj) {
        if ((i & 1) != 0) {
            str = addNameState.nickname;
        }
        if ((i & 2) != 0) {
            guild = addNameState.guild;
        }
        if ((i & 4) != 0) {
            restCallState = addNameState.changeNicknameAsync;
        }
        return addNameState.copy(str, guild, restCallState);
    }

    public final String component1() {
        return this.nickname;
    }

    public final Guild component2() {
        return this.guild;
    }

    public final RestCallState<Void> component3() {
        return this.changeNicknameAsync;
    }

    public final AddNameState copy(String str, Guild guild, RestCallState<Void> restCallState) {
        m.checkNotNullParameter(str, "nickname");
        m.checkNotNullParameter(restCallState, "changeNicknameAsync");
        return new AddNameState(str, guild, restCallState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AddNameState)) {
            return false;
        }
        AddNameState addNameState = (AddNameState) obj;
        return m.areEqual(this.nickname, addNameState.nickname) && m.areEqual(this.guild, addNameState.guild) && m.areEqual(this.changeNicknameAsync, addNameState.changeNicknameAsync);
    }

    public final RestCallState<Void> getChangeNicknameAsync() {
        return this.changeNicknameAsync;
    }

    public final Guild getGuild() {
        return this.guild;
    }

    public final String getNickname() {
        return this.nickname;
    }

    public int hashCode() {
        String str = this.nickname;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Guild guild = this.guild;
        int hashCode2 = (hashCode + (guild != null ? guild.hashCode() : 0)) * 31;
        RestCallState<Void> restCallState = this.changeNicknameAsync;
        if (restCallState != null) {
            i = restCallState.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        StringBuilder R = a.R("AddNameState(nickname=");
        R.append(this.nickname);
        R.append(", guild=");
        R.append(this.guild);
        R.append(", changeNicknameAsync=");
        R.append(this.changeNicknameAsync);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ AddNameState(String str, Guild guild, RestCallState restCallState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? null : guild, (i & 4) != 0 ? Default.INSTANCE : restCallState);
    }
}
