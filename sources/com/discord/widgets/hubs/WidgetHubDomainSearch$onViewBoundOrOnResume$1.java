package com.discord.widgets.hubs;

import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: WidgetHubDomainSearch.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lcom/discord/widgets/hubs/DomainsState;", "kotlin.jvm.PlatformType", "viewState", "", "invoke", "(Lcom/discord/widgets/hubs/DomainsState;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class WidgetHubDomainSearch$onViewBoundOrOnResume$1 extends o implements Function1<DomainsState, Unit> {
    public final /* synthetic */ WidgetHubDomainSearch this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public WidgetHubDomainSearch$onViewBoundOrOnResume$1(WidgetHubDomainSearch widgetHubDomainSearch) {
        super(1);
        this.this$0 = widgetHubDomainSearch;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(DomainsState domainsState) {
        invoke2(domainsState);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(DomainsState domainsState) {
        WidgetHubDomainSearch widgetHubDomainSearch = this.this$0;
        m.checkNotNullExpressionValue(domainsState, "viewState");
        widgetHubDomainSearch.updateView(domainsState);
    }
}
