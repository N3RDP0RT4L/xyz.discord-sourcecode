package com.discord.widgets.hubs;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import com.discord.stores.utilities.Default;
import com.discord.stores.utilities.RestCallState;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: WidgetHubDomainsViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0086\b\u0018\u00002\u00020\u0001B)\u0012\u0010\b\u0002\u0010\t\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003\u0012\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006¢\u0006\u0004\b\u001b\u0010\u001cJ\u0018\u0010\u0004\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0003HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ2\u0010\u000b\u001a\u00020\u00002\u0010\b\u0002\u0010\t\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00032\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u000b\u0010\fJ\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u0010HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u001a\u0010\u0015\u001a\u00020\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0015\u0010\u0016R!\u0010\t\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0017\u001a\u0004\b\u0018\u0010\u0005R\u001f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0019\u001a\u0004\b\u001a\u0010\b¨\u0006\u001d"}, d2 = {"Lcom/discord/widgets/hubs/DomainsState;", "", "", "Lcom/discord/primitives/GuildId;", "component1", "()Ljava/lang/Long;", "Lcom/discord/stores/utilities/RestCallState;", "component2", "()Lcom/discord/stores/utilities/RestCallState;", "selectedGuildId", "verifyEmailAsync", "copy", "(Ljava/lang/Long;Lcom/discord/stores/utilities/RestCallState;)Lcom/discord/widgets/hubs/DomainsState;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getSelectedGuildId", "Lcom/discord/stores/utilities/RestCallState;", "getVerifyEmailAsync", HookHelper.constructorName, "(Ljava/lang/Long;Lcom/discord/stores/utilities/RestCallState;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes2.dex */
public final class DomainsState {
    private final Long selectedGuildId;
    private final RestCallState<Object> verifyEmailAsync;

    public DomainsState() {
        this(null, null, 3, null);
    }

    public DomainsState(Long l, RestCallState<? extends Object> restCallState) {
        m.checkNotNullParameter(restCallState, "verifyEmailAsync");
        this.selectedGuildId = l;
        this.verifyEmailAsync = restCallState;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ DomainsState copy$default(DomainsState domainsState, Long l, RestCallState restCallState, int i, Object obj) {
        if ((i & 1) != 0) {
            l = domainsState.selectedGuildId;
        }
        if ((i & 2) != 0) {
            restCallState = domainsState.verifyEmailAsync;
        }
        return domainsState.copy(l, restCallState);
    }

    public final Long component1() {
        return this.selectedGuildId;
    }

    public final RestCallState<Object> component2() {
        return this.verifyEmailAsync;
    }

    public final DomainsState copy(Long l, RestCallState<? extends Object> restCallState) {
        m.checkNotNullParameter(restCallState, "verifyEmailAsync");
        return new DomainsState(l, restCallState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DomainsState)) {
            return false;
        }
        DomainsState domainsState = (DomainsState) obj;
        return m.areEqual(this.selectedGuildId, domainsState.selectedGuildId) && m.areEqual(this.verifyEmailAsync, domainsState.verifyEmailAsync);
    }

    public final Long getSelectedGuildId() {
        return this.selectedGuildId;
    }

    public final RestCallState<Object> getVerifyEmailAsync() {
        return this.verifyEmailAsync;
    }

    public int hashCode() {
        Long l = this.selectedGuildId;
        int i = 0;
        int hashCode = (l != null ? l.hashCode() : 0) * 31;
        RestCallState<Object> restCallState = this.verifyEmailAsync;
        if (restCallState != null) {
            i = restCallState.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("DomainsState(selectedGuildId=");
        R.append(this.selectedGuildId);
        R.append(", verifyEmailAsync=");
        R.append(this.verifyEmailAsync);
        R.append(")");
        return R.toString();
    }

    public /* synthetic */ DomainsState(Long l, RestCallState restCallState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : l, (i & 2) != 0 ? Default.INSTANCE : restCallState);
    }
}
